function cambiarCHK (chk,grid){//FODEA-033-2014
	var grid = Ext.getCmp('grid');
	var store = Ext.getCmp('grid').getStore();
	store.suspendEvents(); // avoid view update after each row

	if("chkModificar"==chk.id){
		store.each(function(rec){ rec.set('modificar', chk.checked) })
		store.resumeEvents();
		grid.getView().refresh();
	}
	
	if("chkValidar01"==chk.id || "chkValida"==chk.id){//////////////////////////////////////
		store.each(function(rec){ rec.set('validar', chk.checked) })	
		store.each(function(rec){ rec.set('invalidar', !chk.checked) })	
		store.resumeEvents();
		grid.getView().refresh();
		var x=document.getElementsByName("VALIDAR");
		document.getElementById("chkInvalidar01").checked = false;
		for (var i=0;i<x.length;i++) {
				x[i].checked=chk.checked;
		}
	}
	
	if("chkInvalidar01"==chk.id){//////////////////////////////////////
		store.each(function(rec){ rec.set('invalidar', chk.checked) })	
		store.each(function(rec){ rec.set('validar', !chk.checked) })	
		store.resumeEvents();
		grid.getView().refresh();
		var x=document.getElementsByName("INVALIDAR");
		document.getElementById("chkValidar01").checked = false;
		for (var i=0;i<x.length;i++) {
			x[i].checked=chk.checked;
		}		
		
	}
}
var ic_clabes_validar = [];
var ic_clabes_invalidar = [];
function SeleccionValidar(chk,indice,grid){//FODEA-033-2014
	var validar=document.getElementsByName("VALIDAR");
	var invalidar=document.getElementsByName("INVALIDAR");
	if(validar[indice].checked ){
		invalidar[indice].checked=false;
	}else{
	}
}
function SeleccionInvalidar(chk,indice,grid){//FODEA-033-2014
	var validar=document.getElementsByName("VALIDAR");
	var invalidar=document.getElementsByName("INVALIDAR");
	if(invalidar[indice].checked ){
		validar[indice].checked=false;
	}else{
	}
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
Ext.onReady(function() {
	
var afiliadoReasigna='';
var accionMod_Rea='';
var no_cuenta='';
var ic_epo='';
var archivo1='';
var archivo2 = '';
var tipoMon = '';
//------------------Handlers----------------------------
	function procesarDescargaArchivos(opts, success, response) {//FODEA-033-2014
		if(Ext.util.JSON.decode(response.responseText).descarga == true){
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.urlArchivo;				
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			}else {
				NE.util.mostrarConnError(response,opts);
			}
		
		}else{
			//MOSTRAR MSG DE DESCARGA NO PERMITIDA
		}
	}
var cuentaClabeOnBlurHandler 		= function(field){
		return soloNumeros(field,			'La Cuenta Clabe no es v�lida',40);
}
var cuentaSwiftOnBlurHandler 		= function(field){
		soloAlfaNumericos(field,	'La Cuenta no es v�lida. S�lo se permiten caracteres alfanum�ricos.');
}
var cuentaClabeProveedorOnBlurHandler 		= function(field){
		return soloNumeros(field,			'La Cuenta Clabe no es v�lida',400);
}
var confimaCuentaClabe				= function(field){//FODEA-033-2014
			return soloNumeros(field,			'La Cuenta Clabe no es v�lida',0);
}
function soloNumeros(field,mensaje,clave){
	
		var CadenaValida="0123456789"; /*variable con caracteres validos*/
		
		if( Ext.isEmpty(field.getValue()) ){ return; } // Si est� vac�o salir
		
		var numero = String(field.getValue());
		for(var i=0;i<numero.length;i++){
			if(CadenaValida.indexOf(numero.charAt(i))==-1){
				field.markInvalid(mensaje);
				return false;
			}
		}
		
		if (clave == "1"){
			
			if (numero.length != 11){
				field.markInvalid('La longitud no es v�lida, debe de ser de 11 caracteres');
				return false;
			}
			
		}
		
		if (clave == "40"){
			
			if (numero.length != 18){
				field.markInvalid('La longitud no es v�lida, debe de ser de 18 caracteres');
				return false;
			}
	
			var verificar = validaDigitoVerificador(numero);
			if (verificar == "N"){
				field.markInvalid('La Cuenta Clabe no es correcta (D�gito Verificador)');
				return false;
			}
			
		}
		if (clave == "0"){//FODEA-033-2014
			if (numero.length != 18){
				field.markInvalid('La longitud no es v�lida, debe de ser de 18 caracteres');
				return false;
			}
			if(numero != Ext.getCmp('txtCuentaClave').getValue()){
				field.markInvalid('La Confirmaci�n de la Cuenta Clabe no es correcta');
				return false;
			}
		}
		if (clave =='400'){
			if (numero.length != 18){
				field.markInvalid('La longitud no es v�lida, debe de ser de 18 caracteres');
				return false;
			}
	
			var verificar = validaDigitoVerificador(numero);
			if (verificar == "N"){
				field.markInvalid('La Cuenta Clabe no es correcta (D�gito Verificador)');
				return false;
			}
			var ic_banco =Ext.getCmp('cboBancos').getValue();
			//Si los primeros tres digitos no corresonden al banco seleccionado marcar error
			var aux ='';
			var ic_banco_cuenta = numero.substring(0,3);
			if(ic_banco.length<3){
			//anteponer 0
				for(var i = 0; i<3-ic_banco.length;i++){
					aux = aux.concat('0');
				}
				aux = aux.concat(ic_banco);
				if(aux!=ic_banco_cuenta){
					field.markInvalid('La Cuenta Clabe no corresponde al banco');
					return false;
				}
			}else{
				if(ic_banco!=ic_banco_cuenta){
					field.markInvalid('La Cuenta Clabe no corresponde al banco');
					return false;
				}
			}
		}
		
		
		return true;
		
	}

function validaDigitoVerificador(contenido){
		
		var ruta 				= document.registro;			
		var car 					= "";
		var resultado 			= "";
		var residuo 			= 0;    
		var mulIndividual 	= 0;
		var digVerificador 	= 0;  
		var contenedor 		= 0;
		var cadena				= "";
		cadena 					= String(contenido);
 
		digVerificador 		= parseInt(cadena.substr(17),10);
		
		for (var i=0; i <= 16; i++){
			car = cadena.substring(i,i+1);
			switch(i){
				case 0: case 3: case 6: case 9: case 12: case 15:		/*3*/
					mulIndividual 	= 	parseInt(car)*3;
					contenedor 		+= mulIndividual;
					break;
				case 1: case 4: case 7: case 10: case 13: case 16:		/*7*/
					mulIndividual 	= 	parseInt(car)*7;
					contenedor 		+= mulIndividual;
					break;
				case 2: case 5: case 8: case 11: case 14:					/*1*/
					mulIndividual 	= 	parseInt(car)*1;
					contenedor 		+= mulIndividual;
					break;
			}
		}
		
		residuo = contenedor % 10;  /*Obtiene el residuo */
		
		if (residuo > 0)  
			contenedor = 10 - residuo;
		else
			contenedor = 0;
		
		if (digVerificador == contenedor)
			return "S"; 
		else
			return "N"; 
		
	}
  
var accionConsulta = function(estadoSiguiente, respuesta){
if(  estadoSiguiente == "OBTENER_NOMBRE_PYME" ){
		
			fp.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15consulta01Ext.data.jsp',	
				params: {
							numeroDeProveedor:	respuesta.noNafinElec,
							ic_banco_fondeo:		respuesta.ic_banco_fondeo,
							ic_epo:					respuesta.ic_epo,
							informacion: 			"obtenNombrePyme" 
				},
				callback: procesaConsulta
			});

		}else if(  estadoSiguiente == "RESPUESTA_OBTENER_NOMBRE_PYME"){
		
			Ext.getCmp('hid_ic_pyme').setValue(respuesta.ic_pyme);
			Ext.getCmp('_txt_nombre').setValue(respuesta.txtCadenasPymes);
			Ext.getCmp('rfc').setValue(respuesta.rfc);
			fp.el.unmask();
			if (respuesta.muestraMensaje != undefined && respuesta.muestraMensaje){
				Ext.Msg.alert("Mensaje de p�gina web.",	respuesta.textoMensaje);
				Ext.getCmp('_txt_nafelec').setValue('');
			}else{
				var comboAfil = Ext.getCmp('idAfiliado');
				var comboProd = Ext.getCmp('idProducto');
				if(comboAfil.getValue()=='I' || comboAfil.getValue()=='B'){
					catalogoEpo.load({
						params: {
								producto: comboProd.getValue(),
								tipoAfiliado: comboAfil.getValue(),
								claveIf: respuesta.ic_pyme
							}
					});
				}

			}


			var moneda = Ext.getCmp('cboMoneda01_').getValue();
			var txt_nafelec = Ext.getCmp('_txt_nafelec').getValue();
			var cm = grid.getColumnModel(); 
			if(moneda==1 && txt_nafelec !=''){ //Si la moneda seleccionada es MN
				//mostrar la columna Modificar
				grid.getColumnModel().setHidden(cm.findColumnIndex('modificar'), false);
				Ext.getCmp('btnModificar').show();
			}else{
				//ocultar la columna Modificar
				grid.getColumnModel().setHidden(cm.findColumnIndex('modificar'), true);
				Ext.getCmp('btnModificar').hide();
			}
				

		}else if (	estadoSiguiente == "OBTENER_INFO_VENTANA_MODIFICAR"){//FODEA-033-2014
			var store =Ext.getCmp('grid').getStore();
			var forma = Ext.getCmp('forma').getForm();
			Ext.Ajax.request({
				url: '15consulta01Ext.data.jsp',	
				params: Ext.apply(forma.getValues(),{
							informacion: 			"obtener_info_ventana_modificar" 
				}),
				callback: procesaConsulta
			});
			
		
		}else if	(	estadoSiguiente	==	"MOSTRAR_VENTANA_MODIFICAR"){//FODEA-033-2014
			var store =Ext.getCmp('grid').getStore();
			var data =store.getModifiedRecords( ) ;
			var ic_cuentas =[] ;
			var item = [];
			for( var i = 0; i<data.length;i++){
				if(data[i].data.modificar){
					item.push([
						data[i].data.TIPOAFILIADO,
						data[i].data.EPOREL,
						data[i].data.BANCO,
						data[i].data.CUENTACLABE
					]);
					ic_cuentas.push(data[i].data.IC_CUENTA);
				}
				
			} 
			var datosModificar 			= new Ext.data.ArrayStore	({	storeId: 'datosModificarDataStore',	autoDestroy: true,fields: [{ name:'TIPOAFILIADO', mapping:0 },{ name:'EPOREL', mapping:1 },{ name:'BANCO', mapping:2  },{ name: 'CUENTACLABE', mapping:3  }	],	autoLoad:false	});	
			datosModificar.loadData(item);
			var catalogoBanco01 = new Ext.data.JsonStore
			  ({
					id: 'catalogoBanco01',
					root:'registros',
					fields : ['clave', 'descripcion', 'loadMsg'],
					url : '15consulta01Ext.data.jsp',
					baseParams: 
					{
					 informacion: 'catalogoBanco01'
					},
					autoLoad: true//,
					/*listeners:
					{
					 //load: {fn:function(){Ext.getCmp('banco').setValue(Ext.getCmp('banco').getValue());}},
					 exception: NE.util.mostrarDataProxyError,
					 beforeload: NE.util.initMensajeCargaCombo
					}*/
			  });
				var winModificaCuentasMN = new Ext.Window({
					title	:	'Modificar Cuenta',
					id		:	'wimModMN',
					width	:	700,
					height:	600,
					modal	:	true,
					closable	:false,
					items	:[
						{
							xtype	:	'form',
							id		:	'formaModificar_',
							frame: true,
							collapsible: false,
							style: 'margin:0 auto;',
							bodyStyle: 'padding: 6px',
							labelWidth: 180,
							defaults: {
								msgTarget: 'side',
								anchor: '-20',
								width	: 300,
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							monitorValid: true,
							items	:[
								{
									xtype	:	'displayfield',
									id		:	'producto',
									fieldLabel	:	'Producto',
									value		:	Ext.getCmp('idProducto').getRawValue()//Valor mostrado en pantalla
								},
								{
									xtype	:	'hidden',
									id		:	'hdnproducto',
									name	:	'Producto',
									value		:	respuesta.Producto//Valor enviardo en la peticion ajax para llevar acabo la modificacion
								},
								{
									xtype	:	'displayfield',
									id		:	'noNafinElect',
									fieldLabel	:	'No. Nafin Electr�nico',
									value		:	respuesta.NoNafinElect//Valor mostrado en pantalla
								},
								{
									xtype	:	'hidden',
									id		:	'hdnnoNafinElect',
									name	:	'numElectronico',
									value		:	respuesta.NoNafinElect//Valor enviardo en la peticion ajax para llevar acabo la modificacion
								},
								{
									xtype	:	'displayfield',
									id		:	'nombre',
									fieldLabel	:	'Nombre',
									value		:	respuesta.Nombre
								},
								{
									xtype	:	'displayfield',
									id		:	'rfc_',
									fieldLabel	:	'RFC',
									value		:	respuesta.RFC
								},
								{
									xtype	:	'displayfield',
									id		:	'moneda',
									fieldLabel	:	'Moneda',
									value		:	Ext.getCmp('cboMoneda01_').getRawValue()//Valor mostrado en pantalla
								},
								{
									xtype	:	'hidden',
									id		:	'hdnmoneda',
									name	:	'hdnMoneda',
									value	:	respuesta.Moneda//Valor enviardo en la peticion ajax para llevar acabo la modificacion
								},
								{
									xtype	:	'combo',
									id		:	'cboBancos',
									name	:	'banco01',
									hiddenName: 'banco01',
									fieldLabel	:	'Banco',
									displayField: 'descripcion',
									valueField: 'clave',
									triggerAction: 'all',
									typeAhead: true,
									minChars: 1,
									mode: 'local',
									forceSelection : true,
									allowBlank: false,
									emptyText: 'Seleccionar...',
									store: catalogoBanco01
								},
								{
									xtype	:	'textfield',
									id		:	'txtCuentaClave',
									name	:	'cuentaClabe',
									fieldLabel	:	'Cuenta Clabe',
									allowBlank: false,
									 autoCreate: {tag: 'input', type: 'text',  maxlength: '18'},
											width: 110,
											msgTarget: 'side',
									  listeners:{
										 blur: cuentaClabeProveedorOnBlurHandler
									  }
								},
								{
									xtype	:	'textfield',
									id		:	'txtCuentaClaveConfirma',
									name	:	'cuentaClabeConfirma',
									fieldLabel	:	'Confirmaci�n Cuenta Clabe',
									allowBlank: false,
									autoCreate: {tag: 'input', type: 'text',  maxlength: '18'},
                  enableKeyEvents: true,
									listeners:{
										blur: confimaCuentaClabe,
                    keydown:function(obj, e){
                        var c = e.keyCode;
                        if(e.ctrlKey && c==86){
                          e.stopEvent();
                        }
                      }
									}
								},
								{
									xtype	:	'grid',
									store :	datosModificar,
									height:	200,
									viewConfig:{
										forceFit :true
									},
									columns	:	[
										{
											header	:	'Tipo de Afifliado',
											dataIndex	:	'TIPOAFILIADO',
											width		:	150,
											renderer	:	function(v){
												if(v=='E'){
													return 'EPO';
												}else if(v=='P'){
													return 'Proveedor';
												}else if(v=='D'){
													return 'Distribuidor';
												}else if(v=='I'){
													return 'IF Bancario/No Bancario';
												}else if(v=='B'){
													return 'IF/ Beneficiario';
												}
											}
										},{
											header	:	'EPO',
											dataIndex	:	'EPOREL',
											width		:	150
										},
										{
											header	:	'Banco',
											dataIndex	:	'BANCO',
											width		:	150
										},
										{
											header	:	'Cuenta Clabe',
											dataIndex	:	'CUENTACLABE',
											width		:	150
										}
									]
								}
							],
							buttons	:	[
								{
									text	:	'Modificar',
									id		:	'btnModificarMN',
									iconCls	:	'icoModificar',
									//formBind	: true,
									errorActivo: false,
									handler	:	function(boton, evento){
										//Preparar para el Update masivo
										boton.errorActivo = false;
										var forma = Ext.getCmp('formaModificar_');//.getForm();
										forma.items.each(function(item,index,totalCount){
											if(	item.xtype == 'textfield' || item.xtype == 'combo' ){
													
												if(!item.isValid() || !Ext.isEmpty(item.getActiveError( ))){
													boton.errorActivo = true;
													return;
												}
											}
										});
										if(boton.errorActivo){
											return;
										}
										Ext.Ajax.request({
											url: '15consulta01Ext.data.jsp',	
											params: Ext.apply(forma.getForm().getValues(),{
														informacion: 			"guargar_info_ventana_modificar" ,
														ic_cuentas : ic_cuentas
											}),success:function(){
												Ext.getCmp('btnModificar').enable();
												Ext.getCmp('btnModificar').setIconClass('');	
											},
											callback: accionModReaf
										});
									}
								},
								{
									text	:	'Cancelar',
									id		:	'btnCacelarWin',
									iconCls	:	'icoCancelar',
									handler	:function(){
										Ext.getCmp('wimModMN').destroy();
										datosModificar.destroy();
										store.rejectChanges();
										Ext.getCmp('btnModificar').enable();
										Ext.getCmp('btnModificar').setIconClass('');
									}
								}
							]
						}//Fin del form
					]
				}).show(); 
		}
}
var procesaConsulta = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var botonPDF=Ext.getCmp('btnGenerarPDF');
		var botonCSV=Ext.getCmp('btnGenerarArchivo');
		fp.el.unmask();
		if (arrRegistros != null) {
					
			if(store.getTotalCount() > 0) {
          var cm = grid.getColumnModel();
			 
//------------------------------------------------------------------------------FODEA-033-2014
			 var moneda = Ext.getCmp('cboMoneda01_').getValue();
			 if(moneda	==''){
				//Ocultar la columna Modificar
				grid.getColumnModel().setHidden(cm.findColumnIndex('modificar'), true);
				//Ocultar el boton Modificr del Grid
				Ext.getCmp('btnModificar').hide();
			 }else{
					//Mostar si  se selecciona tipo de moneda
			 }
			 
			  var txt_nafelec = Ext.getCmp('_txt_nafelec').getValue();
			
				if(moneda==1 && txt_nafelec !=''){ //Si la moneda seleccionada es MN
					//mostrar la columna Modificar
					grid.getColumnModel().setHidden(cm.findColumnIndex('modificar'), false);
					Ext.getCmp('btnModificar').show();
				}else{
						//ocultar la columna Modificar
						grid.getColumnModel().setHidden(cm.findColumnIndex('modificar'), true);
						Ext.getCmp('btnModificar').hide();
				}
					
					
			 if(moneda	=='1'){
				//Mostar la columna cuneta clabe
				grid.getColumnModel().setHidden(cm.findColumnIndex('CUENTACLABE'), false);
				//Ocultar columnas (Cuenta, Plaza y Sucursal)
				grid.getColumnModel().setHidden(cm.findColumnIndex('CUENTASWIFT'), true);
				grid.getColumnModel().setHidden(cm.findColumnIndex('PLAZA_SWIFT'), true);
				grid.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_SWIFT'), true);
				grid.getColumnModel().setHidden(cm.findColumnIndex('ABA'), true);
			 }else if(moneda == '54' ){
				//Ocultar la columna cuneta clabe
				grid.getColumnModel().setHidden(cm.findColumnIndex('CUENTACLABE'), true);
				//Mostrar columnas (Cuenta, Plaza y Sucursal)
				grid.getColumnModel().setHidden(cm.findColumnIndex('CUENTASWIFT'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('PLAZA_SWIFT'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_SWIFT'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('ABA'), false);
			 }else{
				//Ocultar la columna cuneta clabe
				grid.getColumnModel().setHidden(cm.findColumnIndex('CUENTACLABE'), false);
				//Mostrar columnas (Cuenta, Plaza y Sucursal)
				grid.getColumnModel().setHidden(cm.findColumnIndex('CUENTASWIFT'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('PLAZA_SWIFT'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_SWIFT'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('ABA'), false);
			 }
//------------------------------------------------------------------------------FODEA-033-2014          
          if(Ext.getCmp('idProducto').getValue()!=''){
            grid.getColumnModel().setHidden(cm.findColumnIndex('PRODUCTO'), true);
          }else{
            grid.getColumnModel().setHidden(cm.findColumnIndex('PRODUCTO'), false);
          }
					grid.el.unmask();
					botonPDF.enable();
					botonCSV.enable();
				grid.getView().refresh();//-----------------------------------------FODEA-033-2014	
			} else {
			
				botonPDF.disable();
				botonCSV.disable();
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
		var cargaCuenta= function(opts, success, response) {
				if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
					
					fpModifica.getForm().setValues(Ext.util.JSON.decode(response.responseText).cuenta);
					if(afiliadoReasigna!=''){
						catalogoEpo2.load({
										params: {
												producto: Ext.util.JSON.decode(response.responseText).cuenta.Producto,
												clave: Ext.util.JSON.decode(response.responseText).cuenta.clave,
												tipo_afiliado: afiliadoReasigna
											}
									});
								}
					catalogoMoneda.load({
								params:{
									moneda:Ext.util.JSON.decode(response.responseText).cuenta.cbMoneda
								}
					});
					var bancoCatalogo=Ext.util.JSON.decode(response.responseText).cuenta.Hbanco;
					if(accionMod_Rea=='M')
						bancoCatalogo='';
						
					catalogoBanco.load({
							params: {
												banco: bancoCatalogo
											}
					
					});
					catalogoProducto2.load();
					if(Ext.util.JSON.decode(response.responseText).cuenta.esCuentaSwift==true){
						Ext.getCmp('plaza_swiftMod').setVisible(true);
						Ext.getCmp('sucursal_swiftMod').setVisible(true);
            Ext.getCmp('cgSwift').setVisible(true);
            Ext.getCmp('cgAba').setVisible(true);
            Ext.getCmp('cuentaDL').setVisible(true);
            Ext.getCmp('cuentaMod').setVisible(false);
            Ext.getCmp('cuentaMod').allowBlank = true;
            Ext.getCmp('cuentaDL').allowBlank = false;
            tipoMon = 'D';
            
					}else{
						Ext.getCmp('plaza_swiftMod').setVisible(false);
						Ext.getCmp('sucursal_swiftMod').setVisible(false);
            Ext.getCmp('cgSwift').setVisible(false);
            Ext.getCmp('cgAba').setVisible(false);
            Ext.getCmp('cuentaDL').setVisible(false);
            Ext.getCmp('cuentaMod').setVisible(true);
            Ext.getCmp('cuentaDL').allowBlank = true;
            Ext.getCmp('cuentaMod').allowBlank = false;
            tipoMon = 'P';
					}
				}
			
			}
			
			
		var accionModReaf = function(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				
				if(Ext.util.JSON.decode(response.responseText).banderaConfirma==true){
						
						Ext.Msg.show({
										title:	"Mensaje",
										msg:		Ext.util.JSON.decode(response.responseText).mensaje,
										buttons:	Ext.Msg.OK,
										fn: function resultMsj(btn){
											 if (btn == 'ok') {
												location.reload();
											}
										},
										closable:false,
										icon: Ext.MessageBox.QUESTION
									});
						}
						else{
						
							Ext.Msg.show({
										title:	"Mensaje",
										msg:		Ext.util.JSON.decode(response.responseText).mensaje,
										buttons:	Ext.Msg.OK,
										fn: function resultMsj(btn){
											 if (btn == 'ok') {
												//location.reload();
											}
										},
										closable:false,
										icon: Ext.MessageBox.QUESTION
									});
						
						}
					}else {
						NE.util.mostrarConnError(response,opts);
					}
			
		}
			
			
			var descargaArchivos = function(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
					archivo1=Ext.util.JSON.decode(response.responseText).urlArchivo1;
					archivo2=Ext.util.JSON.decode(response.responseText).urlArchivo2;
					Ext.getCmp('ctasClabe').setVisible(false);
					Ext.getCmp('ctasClabeEXCEL').setVisible(true);
					Ext.getCmp('ctasClabeTXT').setVisible(true);
					var botonPdf=Ext.getCmp('ctasClabe');
					botonPdf.setIconClass('');
					botonPdf.enable();
					}else {
						NE.util.mostrarConnError(response,opts);
					}
			
		}
			//Invalida la cuenta Inicio
			var accionInvalida = function(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				
				if(Ext.util.JSON.decode(response.responseText).banderaConfirma==true){
						
						consultaData.load({ params: Ext.apply(fp.getForm().getValues())});
						}
						else{
						
							Ext.Msg.show({
										title:	"Mensaje",
										msg:		Ext.util.JSON.decode(response.responseText).mensaje,
										buttons:	Ext.Msg.OK,
										fn: function resultMsj(btn){
											 if (btn == 'ok') {
												//location.reload();
											}
										},
										closable:false,
										icon: Ext.MessageBox.QUESTION
									});
						
						}
					}else {
						NE.util.mostrarConnError(response,opts);
					}
			
		}
			//Invalida la cuenta FIN
		//Elimina Registro Inicio
		
	var accionEliminado = function(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				
				if(Ext.util.JSON.decode(response.responseText).banderaConfirma==true){
						Ext.Msg.show({
										title:	"Mensaje",
										msg:		Ext.util.JSON.decode(response.responseText).mensaje,
										buttons:	Ext.Msg.OK,
										fn: function resultMsj(btn){
											 if (btn == 'ok') {
												location.reload();
											}
										},
										closable:false,
										icon: Ext.MessageBox.QUESTION
									});
						}
						else{
						
							Ext.Msg.show({
										title:	"Mensaje",
										msg:		Ext.util.JSON.decode(response.responseText).mensaje,
										buttons:	Ext.Msg.OK,
										fn: function resultMsj(btn){
											 if (btn == 'ok') {
												//location.reload();
											}
										},
										closable:false,
										icon: Ext.MessageBox.QUESTION
									});
						
						}
					}else {
						NE.util.mostrarConnError(response,opts);
					}
			
		}
	var procesarValidaUsuario = function(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				if( Ext.util.JSON.decode(response.responseText).accion != 'E'){
					Ext.Msg.show({title:	"Mensaje",msg:		Ext.util.JSON.decode(response.responseText).mensaje});
				}else{
						Ext.Ajax.request({
							url: '15consulta01Ext.data.jsp',
							params: Ext.apply({
								informacion:'eliminaRegistroValida',
								cuenta: Ext.util.JSON.decode(response.responseText).cuenta
							}),
							callback: accionEliminado
						});
				}
			}else {
				NE.util.mostrarConnError(response,opts);
			}
		}
	var procesaEliminado = function(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				if( Ext.util.JSON.decode(response.responseText).banderaElimina == true){
			
					if(Ext.util.JSON.decode(response.responseText).banderaValida == true){
						Ext.Msg.show({
										title:	"Mensaje",
										msg:		Ext.util.JSON.decode(response.responseText).mensaje,
										buttons:	Ext.Msg.OKCANCEL,
										fn: function resultMsj(btn){
											 if (btn == 'ok') {
												var ventana = Ext.getCmp('verValidacion');
												if (ventana) {
													ventana.show();
												} else {
													new Ext.Window({
														modal: true,
														resizable: true,
														layout: 'form',
														x: 600,
														width: 300,
														height: 150,
														id: 'verValidacion',
														closeAction: 'hide',
														bodyStyle: 'padding: 6px',
														items: [elementosFormaConfirmacion],
														title: 'Confirmaci�n de clave',
														bbar: {
															xtype: 'toolbar',
															buttons: ['->','-',
															{
															  //Bot�n BUSCAR
															  xtype: 'button',
															  text: 'Aceptar',
															  name: 'btnAceptar',
															  iconCls: 'icoAceptar',
															  hidden: false,
															  formBind: true,
															  handler: function(boton, evento) 
																{	
																	Ext.getCmp('cuentaConfirma').setValue(Ext.util.JSON.decode(response.responseText).cuenta);
																	
																		var usuario = Ext.getCmp('claveUsuario');
																		var contrasenia = Ext.getCmp('contrasenia');
																		if(Ext.isEmpty(usuario.getValue())){
																			usuario.markInvalid('El campo de Login no puede estar vacio');
																			usuario.focus();
																			return;
																		}
																		if(Ext.isEmpty(contrasenia.getValue())){
																			contrasenia.markInvalid('El campo de Password no puede estar vacio');
																			contrasenia.focus();
																			return;
																		}
																		boton.disable();
																		boton.setIconClass('loading-indicator');
																		Ext.Ajax.request({
																			url: '15consulta01Ext.data.jsp',
																			params: Ext.apply({							
																				informacion: 'ValidaUsuario',
																				claveUsuario : Ext.getCmp('claveUsuario').getValue(),
																				contrasenia : Ext.getCmp('contrasenia').getValue(),
																				cuentaConfirma : Ext.getCmp('cuentaConfirma').getValue()
																			}),success: function(){
																				boton.enable();
																				boton.setIconClass('icoAceptar');
																			},	
																			callback: procesarValidaUsuario
																		});
																}
															 },{
																text:'Cancelar',
																iconCls: 'icoCancelar',
																handler: function() {
																	window.location.href='15consulta01Ext.jsp';
																}
															 }
															 ]
														}
													}).show();
												}
											}
										},
										closable:false,
										icon: Ext.MessageBox.QUESTION
									});
				
					}else{
						Ext.Msg.show({
										title:	"Mensaje",
										msg:		Ext.util.JSON.decode(response.responseText).mensaje,
										buttons:	Ext.Msg.OKCANCEL,
										fn: function resultMsj(btn){
											 if (btn == 'ok') {
												Ext.Ajax.request({
													url: '15consulta01Ext.data.jsp',
													params: Ext.apply({informacion:'eliminaRegistro',afiliadoE: Ext.util.JSON.decode(response.responseText).afiliadoE,
													cuenta: Ext.util.JSON.decode(response.responseText).cuenta,
													estatus_cecoban:  Ext.util.JSON.decode(response.responseText).estatus_cecoban,cuenta_dolares:Ext.util.JSON.decode(response.responseText).cuenta_dolares  }),
													callback: accionEliminado
												});
											}
										},
										closable:false,
										icon: Ext.MessageBox.QUESTION
									});
							}
							
						}else{
								Ext.Msg.alert("Mensaje de p�gina web.", Ext.util.JSON.decode(response.responseText).mensaje );
					}
			}else {
				NE.util.mostrarConnError(response,opts);
			}
		}
	
	var procesarEliminar = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consultaData.getAt(rowIndex);
		var esDolar=false;
		if(registro.get('TIPO_MONEDA')=='54')
			esDolar=true
					Ext.Ajax.request({
							url: '15consulta01Ext.data.jsp',
							params: Ext.apply({informacion:'procesoEliminaRegistro',afiliadoE: registro.get('TIPOAFILIADO'),
							cuenta: registro.get('IC_CUENTA') ,estatus_cecoban:  registro.get('IC_ESTATUS_CECOBAN'),cuenta_dolares:esDolar  }),
							callback: procesaEliminado
						});
		}
		//Fin Borra registro
			
			
			
function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('btnGenerarArchivo');
			boton.setIconClass('');
			boton.enable();
			var botonPdf=Ext.getCmp('btnGenerarPDF');
			botonPdf.setIconClass('');
			botonPdf.enable();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

function procesarArchivoRuta(rutaArchivo) {		
			var archivo = rutaArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
	}
	
function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	

var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	
 	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg' ],
		url : '15consulta01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	

//-------------------Stores--------------------------
 var catalogoBanco = new Ext.data.JsonStore
  ({
	   id: 'catalogoBanco',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consulta01Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoBanco'
		},
		autoLoad: false,
		listeners:
		{
		 load: {fn:function(){Ext.getCmp('banco').setValue(Ext.getCmp('banco').getValue());}},
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consulta01Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoMoneda'
		},
		autoLoad: false,
		listeners:
		{
		 load: {fn:function(){Ext.getCmp('cbMoneda').setValue(Ext.getCmp('cbMoneda').getValue());}},
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
	var catalogoMoneda01 = new Ext.data.JsonStore({//FODEA-033-2014
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consulta01Ext.data.jsp',
		baseParams: {
			informacion: 'Catagolo.Moneda.01'
		},
		autoLoad: true,
		listeners:{
			//load: {fn:function(){Ext.getCmp('cbMoneda').setValue(Ext.getCmp('cbMoneda').getValue());}},
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
  
  var catalogoAfiliado = new Ext.data.ArrayStore({
        fields: [
            'clave',
            'descripcion'
        ],
        data: [['', 'Todos'],['E', 'EPO'], ['P', 'Proveedor'], ['D', 'Distribuidor'],
		  ['I', 'IF Bancario/No Bancario'], ['B', 'IF/ Beneficiario']]
    });
	 
   var catalogoProducto = new Ext.data.JsonStore
  ({
	   id: 'catalogoProducto',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consulta01Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoProducto'	
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
   var catalogoProducto2 = new Ext.data.JsonStore
  ({
	   id: 'catalogoProducto2',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consulta01Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoProducto2'	
		},
		autoLoad: false,
		listeners:
		{
		 load: {fn:function(){Ext.getCmp('idProductoMod').setValue(Ext.getCmp('idProductoMod').getValue());}},
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
   var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consulta01Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
   var catalogoEpo2 = new Ext.data.JsonStore
  ({
	   id: 'catalogoEpo2',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consulta01Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoEpo2'
		},
		autoLoad: false,
		listeners:
		{
		 load: {fn:function(){Ext.getCmp('icEpoMod').setValue(ic_epo);}},
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
	


var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15consulta01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
			fields: [
					{name: 'IC_CUENTA'},//
					{name: 'TIPOAFILIADO'},//
					{name: 'NOMBRE'},//
					{name: 'BANCO'},//
					{name: 'CUENTACLABE'},//
					{name: 'CUENTASPEUA'},
					{name: 'ESTATUS_TEF'},//
					{name: 'ESTATUS_CECOBAN'},//
					{name: 'FECHA_ULT_MOD'},//
					{name: 'USUARIO'},//
					{name: 'EPOREL'},//
					{name: 'PRODUCTO'},//
					{name: 'IC_ESTATUS_CECOBAN'},//
					{name: 'IC_EPO_REL'},
					{name: 'CUENTASWIFT'},
					{name: 'TIPO_MONEDA'},
					{name: 'ESTATUS_DOLARES'},
					{name: 'ESTATUS_CUENTA'},
					{name: 'ES_CUENTA_SPEUA'},//
					{name: 'ES_CUENTA_DOLARES'},//
					{name: 'PLAZA_SWIFT'},//
					{name: 'SUCURSAL_SWIFT'},
					{name:  'NOMBRE_MONEDA'},
					{name: 'validar', type:'boolean',defaultValue : false},//FODEA-033-2014
					{name: 'invalidar', type:'boolean',defaultValue : false},//FODEA-033-2014
					{name: 'modificar', type:'boolean',defaultValue : false},//FODEA-033-2014
					{name: 'ABA'}
					
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	
//-------------------Forma--------------------------
var elementosModifica = [
{
				xtype: 'combo',
				name: 'Producto',
				id:	'idProductoMod',
				fieldLabel: 'Producto',
				emptyText: 'Seleccionar',
				mode: 'local',
				//anchor: '-150',
				displayField: 'descripcion',
				valueField: 'clave',
				hiddenName : 'Producto',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store: catalogoProducto2,
				tpl : NE.util.templateMensajeCargaCombo,
				listeners: {
							select: {
								fn: function(combo) {
									catalogoEpo.load({
										params: {
												producto: combo.getValue(),
												tipoAfiliado: Ext.getCmp('idAfiliado').getValue(),
												claveIf: respuesta.ic_pyme
											}
									});
									
								}
							}
					}
			},{
			xtype: 'numberfield',
			name: 'numElectronico',
			fieldLabel: 'N�mero de Nafin Electr�nico',
			id: 'numElectronicoMod',
			maxLength: 9,
			width: 110,
			msgTarget: 'side'
		},{
			xtype: 'textfield',
			name: 'nombre',
			fieldLabel: 'Nombre',
			id: 'nombreMod',
			width: 110,
			msgTarget: 'side'
		},{
			xtype 		: 'textfield',
			name  	: 'R_F_C',
			id    		: 'rfc_eMod',
			fieldLabel	: 'R.F.C.',
			maxLength	: 20,
			width			: 230,
			allowBlank 	: false,
			hidden		: false,
			margins		: '0 20 0 0',
			msgTarget	: 'side',
			regex			:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
			regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
			'en el formato NNN-AAMMDD-XXX donde:<br>'+
			'NNN:son las iniciales del nombre de la empresa<br>'+
			'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
			'XXX:es la homoclave'
		},{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			name:'HcbMoneda',
			id: 'cbMoneda',
			mode: 'local',
			forceSelection : true,
			//allowBlank: false,
			hiddenName: 'HcbMoneda',
			hidden: false,
			emptyText: 'Seleccionar Moneda',
			store: catalogoMoneda,
			tpl: NE.util.templateMensajeCargaCombo
		},{
				xtype: 'combo',
				editable:false,
				fieldLabel: 'Banco',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				align: 'left',
				name:'Hbanco',
				store: catalogoBanco,
				id: 'banco',
				mode: 'local',
				hiddenName: 'Hbanco',
				emptyText: 'Seleccionar'
			},{
				name: 'cuenta',
				fieldLabel: 'cuenta',
				id: 'cuentaMod',
				maxLength: 20,
        allowBlank:true,
        autoCreate: {tag: 'input', type: 'text',  maxlength: '20'},
				width: 110,
				msgTarget: 'side',
        listeners:{
          blur: function(obj){
            cuentaClabeOnBlurHandler
          }
        }
			},{
				xtype: 'textfield',
				name: 'cuentaDL',
				fieldLabel: 'cuenta',
				id: 'cuentaDL',
				maxLength: 20,
        allowBlank:true,
        autoCreate: {tag: 'input', type: 'text',  maxlength: '20'},
				width: 110,
				msgTarget: 'side',
        listeners:{
          blur: function(obj){
            cuentaSwiftOnBlurHandler
          }
        }
			},{
				xtype: 'textfield',
				name: 'plaza_swift',
				fieldLabel: 'Plaza',
				id: 'plaza_swiftMod',
				width: 110,
				msgTarget: 'side'
			},{
				xtype: 'textfield',
				name: 'sucursal_swift',
				fieldLabel: 'Sucursal',
				id: 'sucursal_swiftMod',
				width: 110,
				msgTarget: 'side'
			},
      {
        xtype: 				'textfield',
        name: 				'cgswift',
        id: 					'cgSwift',
        fieldLabel: 		'Swift',
        //hidden:				true,
        allowBlank: 		true,
        maxLength: 			20,
        msgTarget: 			'side',
        autoCreate: {tag: 'input', type: 'text',  maxlength: '20'},
        anchor:				'45%',
        margins: 			'0 20 0 0'
      },
      {
        xtype: 				'textfield',
        name: 				'cgaba',
        id: 					'cgAba',
        fieldLabel: 		'Aba',
        //hidden:				true,
        allowBlank: 		true,
        maxLength: 			20,
        msgTarget: 			'side',
        autoCreate: {tag: 'input', type: 'text',  maxlength: '20'},
        anchor:				'45%',
        margins: 			'0 20 0 0'
        /*listeners:{
          blur: validaClaveAba
        }*/
      }
			,{
				xtype: 'combo',
				fieldLabel: 'EPO',
				emptyText: 'Seleccionar una EPO',
				displayField: 'descripcion',
				//allowBlank: false,
				//anchor: '93%',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo2,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'HicEpo',
				id: 'icEpoMod',
				mode: 'local',
				hiddenName: 'HicEpo',
				forceSelection: true
			},{
				xtype: 'hidden',
				name: 'clave',
				hiddenName: 'clave',
				id: 'clave'
			},{
				xtype: 'hidden',
				name: 'tipo_cuenta',
				hiddenName: 'tipo_cuenta',
				id: 'tipo_cuenta'
			}
]
var elementosFormaConfirmacion = [
	{   
					xtype:'textfield',
					fieldLabel: 'Clave de usuario',
					name: 'claveUsuario',
					msgTarget: 'side',
					id: 'claveUsuario', //campo clave de confirmacion
					style:'padding:4px 3px;',   
					allowBlank: false,
					maxLength : 8,
					minLength : 8,
					vtype:'alphanum'
	},
	{   
					xtype:'textfield',
					fieldLabel: 'Contrase�a',
					name: 'contrasenia',
					id: 'contrasenia', //campo clave de confirmacion
					inputType: 'password', 
					style:'padding:4px 3px;',   
					blankText: 'Digite su contrase�a',
					allowBlank: false,
					msgTarget: 'side',
					maxLength : 8,
					minLength : 8,
					vtype:'alphanum'
	},
	{   
					xtype:'textfield',
					name: 'cuentaConfirma',
					id: 'cuentaConfirma', 
					hidden:true
					
	}
];

var elementosForma = [
{
				xtype: 'combo',
				editable:false,
				fieldLabel: 'Tipo de Afiliado',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				align: 'left',
				//anchor: '80%',
				name:'Afiliado',
        id: 'idAfiliado',
				store: catalogoAfiliado,
				id: 'idAfiliado',
				mode: 'local',
				value:	'',
				hiddenName: 'Afiliado',
				emptyText: 'Seleccionar',
				listeners: {
							select: {
								fn: function(combo) {
									var cm = grid.getColumnModel();
									
									Ext.getCmp('icEpo').setValue('');
									Ext.getCmp('icEpo1').setValue('');
									Ext.getCmp('idProducto').setValue('');
									Ext.getCmp('_txt_nafelec').setValue('');
									Ext.getCmp('_txt_nombre').setValue('');
									Ext.getCmp('rfc').setValue('');
									if(combo.getValue()==''){
										grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOAFILIADO'), false);
									}else{
										grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOAFILIADO'), true);
                    //if(combo.getValue()=='E'){
                     
										if(combo.getValue()=='' || combo.getValue() == 'I' || combo.getValue()=='B' || combo.getValue()=='P') {
										 grid.getColumnModel().setHidden(cm.findColumnIndex('EPOREL'), false);
										}else{
										  grid.getColumnModel().setHidden(cm.findColumnIndex('EPOREL'), true);
										}
									}
									
									if(combo.getValue()=='P'){
										Ext.getCmp('_txt_nombre').setVisible(true);
										Ext.getCmp('compoBot').setVisible(true);
										Ext.getCmp('icEpo').setVisible(true);
										
										//FODEA-033-2014
										//Mostar catalogo Moneda
										Ext.getCmp('cboMoneda01_').show();
										Ext.getCmp('cboMoneda01_').reset();
										//Num. Nafin Elec obligatorio
										//Ext.getCmp('_txt_nafelec').allowBlank = false;
										//Ext.getCmp('_txt_nafelec').validate();
										
										//Mostar las nuevas columnas
										//grid.getColumnModel().setHidden(15, false);
										grid.getColumnModel().setHidden(cm.findColumnIndex('validar'), false);
										grid.getColumnModel().setHidden(cm.findColumnIndex('invalidar'), false);
										//si la moneda es MN mostrar
										grid.getColumnModel().setHidden(cm.findColumnIndex('modificar'), false);
										
										//Mostar los botones Modificar y Guardar en el grid
										Ext.getCmp('btnModificar').show();
										Ext.getCmp('btnGuardar').show();
										grid.getView().refresh();
									}else{
										Ext.getCmp('_txt_nombre').setVisible(true);
										Ext.getCmp('compoBot').setVisible(true);
										Ext.getCmp('icEpo').setVisible(true);
										//FODEA-033-2014
										//Ocultar catalogo Moneda
										Ext.getCmp('cboMoneda01_').hide();
										Ext.getCmp('cboMoneda01_').reset();
										//Num. Nafin Elec no obligatorio
										//Ext.getCmp('_txt_nafelec').allowBlank = true;
										//Ext.getCmp('_txt_nafelec').validate();
										//Ocultar las nuevas columnas
										grid.getColumnModel().setHidden(cm.findColumnIndex('validar'), true);
										grid.getColumnModel().setHidden(cm.findColumnIndex('invalidar'), true);
										grid.getColumnModel().setHidden(cm.findColumnIndex('modificar'), true);
										//Ocultar los botones Modificar y Guardar en el grid
										Ext.getCmp('btnModificar').hide();
										Ext.getCmp('btnGuardar').hide();
										grid.getView().refresh();
									}
									if(combo.getValue()=='I'||combo.getValue()=='B'){
										Ext.getCmp('icEpo1').setVisible(true);
									
									}else{
										Ext.getCmp('icEpo1').setVisible(false);
									}
									
                  grid.hide();
										//Mostar los botones cuando ....	//FODEA-033-2014
										if(Ext.getCmp('idProducto').getValue()==1 && combo.getValue()=='P' ){//FODEA-033-2014
											Ext.getCmp('btnDescargaCuentaClaveTXT').show();
											Ext.getCmp('btnDescargaCuentaClaveXLS').show();
											Ext.getCmp('btnDescargaGralCuentaClaveCSV').show(); 
										}else{
											Ext.getCmp('btnDescargaCuentaClaveTXT').hide();
											Ext.getCmp('btnDescargaCuentaClaveXLS').hide();
											Ext.getCmp('btnDescargaGralCuentaClaveCSV').hide();  
										}
										
										
								}
								
								
							}
					}
				},{
				xtype: 'combo',
				name: 'Producto',
				id:	'idProducto',
				fieldLabel: 'Producto',
				emptyText: 'Seleccionar',
				mode: 'local',
				//anchor: '-150',
				displayField: 'descripcion',
				valueField: 'clave',
				hiddenName : 'Producto',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store: catalogoProducto,
				tpl : NE.util.templateMensajeCargaCombo,
				listeners: {
							select: {
								fn: function(combo) {
									var comboAfil = Ext.getCmp('idAfiliado');
									if(comboAfil.getValue()=='P'){
										catalogoEpo.load({
											params: {
													producto: combo.getValue(),
													tipoAfiliado: comboAfil.getValue()
												}
										});
									}
									
									if(combo.getValue()==1 && Ext.getCmp('idAfiliado').getValue()=='P' ){
										Ext.getCmp('btnDescargaCuentaClaveTXT').show();
										Ext.getCmp('btnDescargaCuentaClaveXLS').show();
										Ext.getCmp('btnDescargaGralCuentaClaveCSV').show();
									}else{
										Ext.getCmp('btnDescargaCuentaClaveTXT').hide();
										Ext.getCmp('btnDescargaCuentaClaveXLS').hide();
										Ext.getCmp('btnDescargaGralCuentaClaveCSV').hide(); 
									}
								}
							}
					}
			},{
				xtype: 'combo',
				fieldLabel: 'EPO',
				emptyText: 'Seleccionar una EPO',
				displayField: 'descripcion',
				//allowBlank: false,
				//anchor: '93%',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				hidden: true,
				store: catalogoEpo,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'HicEpo',
				id: 'icEpo',
				mode: 'local',
				hiddenName: 'HicEpo',
				forceSelection: true,
				listeners: {
							select: {
								fn: function(combo) {
								}
							}
					}
			},{
					xtype:'hidden',
					id:	'hid_nombre',
					value:''
				},{
					xtype:'hidden',
					id:	'hid_ic_pyme',
					value:''
				},{
					xtype:		'textfield',
					name:			'txt_nafelec',
					id:			'_txt_nafelec',
					maskRe:		/[0-9]/,
					allowBlank:	true,
					//disabled:	true,
					hidden:		false,
					fieldLabel: 'N�mero de Nafin Electr�nico',
					maxLength:	25,
					listeners:	{
						'change':function(field){
										if ( !Ext.isEmpty(field.getValue()) && !Ext.isEmpty(Ext.getCmp('idAfiliado').getValue())) {

											var respuesta = new Object();
											respuesta["noNafinElec"]		= field.getValue();
											//respuesta["ic_banco_fondeo"]	= Ext.getCmp('_ic_banco_fondeo').getValue();
											//respuesta["ic_epo"]				= Ext.getCmp('ic_epo').getValue();
											
											accionConsulta("OBTENER_NOMBRE_PYME",respuesta);

										}else{
											Ext.getCmp('hid_ic_pyme').setValue('');
											Ext.getCmp('_txt_nombre').setValue('');
											Ext.getCmp('rfc').setValue('');
										}
									}
					}
				},{
					xtype:			'textfield',
					name:				'txt_nombre',
					//fieldLabel: 	'Nombre de proveedor',
					id:				'_txt_nombre',
					width:			270,
					disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
					disabled:		true,
					allowBlank:		true,
					maxLength:		100
				},{
					xtype: 'compositefield',
					combineErrors: false,
					id: 'compoBot',
					msgTarget: 'side',
					items: [
						{
							xtype:'displayfield',
							id:'disEspacio',
							text:''
						},
						{
							xtype:	'button',
							id:		'btnBuscaA',
							iconCls:	'icoBuscar',
							text:		'B�squeda Avanzada',
							handler: function(boton, evento) {
										
										var winVen = Ext.getCmp('winBuscaA');
											if (winVen){
												Ext.getCmp('fpWinBusca').getForm().reset();
												Ext.getCmp('fpWinBuscaB').getForm().reset();
												Ext.getCmp('cmb_num_ne').setValue();
												Ext.getCmp('cmb_num_ne').store.removeAll();
												Ext.getCmp('cmb_num_ne').reset();
												winVen.show();
											}else{
												var winBuscaA = new Ext.Window ({
													id:'winBuscaA',
													height: 320,
													x: 300,
													y: 100,
													width: 550,
													heigth: 100,
													modal: true,
													closeAction: 'hide',
													title: 'B�squeda Avanzada',
													items:[
														{
															xtype:'form',
															id:'fpWinBusca',
															frame: true,
															border: false,
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 130,
															items:[
																{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:'Utilice el * para b�squeda gen�rica'
																},{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:''
																},{
																	xtype: 'textfield',
																	name: 'nombre_pyme',
																	id:	'txtNombre',
																	fieldLabel:'Nombre',
																	maxLength:	100
																},{
																	xtype: 'textfield',
																	name: 'rfc_pyme',
																	id:	'txtRfc',
																	fieldLabel:'RFC',
																	maxLength:	20
																},{
																	xtype: 'textfield',
																	name: 'num_pyme',
																	id:	'txtNe',
																	fieldLabel:'N�mero Proveedor',
																	maxLength:	25
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Buscar',
																	iconCls:'icoBuscar',
																	handler: function(boton) {
																					catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues(),{HicEpo:Ext.getCmp('icEpo').getValue()}) });
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {
																					Ext.getCmp('winBuscaA').hide();
																				}
																}
															]
														},{
															xtype:'form',
															frame: true,
															id:'fpWinBuscaB',
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															monitorValid: true,
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 80,
															items:[
																{
																	xtype: 'combo',
																	id:	'cmb_num_ne',
																	name: 'ic_pyme',
																	hiddenName : 'ic_pyme',
																	fieldLabel: 'Nombre',
																	emptyText: 'Seleccione Proveedor. . .',
																	displayField: 'descripcion',
																	valueField: 'clave',
																	triggerAction : 'all',
																	forceSelection:true,
																	allowBlank: false,
																	typeAhead: true,
																	mode: 'local',
																	minChars : 1,
																	store: catalogoNombreProvData,
																	tpl : NE.util.templateMensajeCargaCombo
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Aceptar',
																	iconCls:'aceptar',
																	formBind:true,
																	//hidden:true,
																	handler: function() {
																					if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																						var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																						var cveP = disp.substr(0,disp.indexOf(" "));
																						var desc = disp.slice(disp.indexOf(" ")+1);
																						Ext.getCmp('hid_ic_pyme').setValue(Ext.getCmp('cmb_num_ne').getValue())
																						Ext.getCmp('_txt_nafelec').setValue(cveP);
																						Ext.getCmp('_txt_nombre').setValue(desc);
																						Ext.getCmp('winBuscaA').hide();
                                            
                                            var respuesta = new Object();
                                            respuesta["noNafinElec"]		= Ext.getCmp('_txt_nafelec').getValue();
                                            accionConsulta("OBTENER_NOMBRE_PYME",respuesta);
																					}
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {	Ext.getCmp('winBuscaA').hide();	}
																}
															]
														}
													]
												}).show();
											}
                      
                      if(Ext.getCmp('icEpo').getValue()=='')
                        Ext.getCmp('txtNe').hide();
                      else
                        Ext.getCmp('txtNe').show();
										}//cierre handler
						}
					]
				},{
				xtype 		: 'textfield',
				name  		: 'R_F_C',
				id    		: 'rfc',
				fieldLabel	: 'R.F.C.',
				maxLength	: 20,
				width			: 150,
				hidden		: false,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				regex			:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
				regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
				'en el formato NNN-AAMMDD-XXX donde:<br>'+
				'NNN:son las iniciales del nombre de la empresa<br>'+
				'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
				'XXX:es la homoclave'
			},{//FODEA-033-2014
				xtype	: 'combo',
				id		: 'cboMoneda01_',
				fieldLabel: 'Moneda',
				store: catalogoMoneda01,
				name	: 'cboMoneda01',
				hiddenName	: 'cboMoneda01',
				forceSelection	: true,
				emptyText: 'Seleccionar...',
				displayField: 'descripcion',
				allowBlank: true,
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				mode: 'local',
				anchor: '60%' ,
				hidden	:true,
				tpl: NE.util.templateMensajeCargaCombo,
				listeners: {
						select: {
							fn: function(combo) {
								
								///
								if(!fp.getForm().isValid()){
									return;
								}
								if(verificaFechas('fechaEm1','fechaEm2')){
									
									var cm = grid.getColumnModel();
									if(combo.getValue()==1){//Si la moneda seleccionada es MN
										//mostrar la columna Modificar
										grid.getColumnModel().setHidden(cm.findColumnIndex('modificar'), false);
										Ext.getCmp('btnModificar').show();
									}else{
									//ocultar la columna Modificar
										grid.getColumnModel().setHidden(cm.findColumnIndex('modificar'), true);
										Ext.getCmp('btnModificar').hide();
									}
									
									grid.show();
									fp.el.mask('Procesando...', 'x-mask-loading');
									
									Ext.getCmp('ctasClabeTXT').setVisible(false);
									Ext.getCmp('ctasClabeEXCEL').setVisible(false);
									if(Ext.getCmp('cuentas').getValue())
										{
											Ext.getCmp('ctasClabe').setVisible(true);	
											
										}else{
											Ext.getCmp('ctasClabe').setVisible(false);
										}
			
									consultaData.load({ 
										params: Ext.apply(fp.getForm().getValues())
									});
								}
								
							}
							
						}
				}
			},{
				xtype: 'combo',
				fieldLabel: 'EPO',
				emptyText: 'Seleccionar una EPO',
				displayField: 'descripcion',
				//allowBlank: false,
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				hidden:		true,
				store: catalogoEpo,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'HicEpo',
				id: 'icEpo1',
				mode: 'local',
				hiddenName: 'HicEpo',
				forceSelection: true,
				listeners: {
							select: {
								fn: function(combo) {
									
									
									
								}
								
							}
					}
			},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de Registro',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaEm1',
									id: 'fechaEm1',
									allowBlank: true,
									startDay: 0,
									minValue: '01/01/1901',
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaEm2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaEm2',
									id: 'fechaEm2',
									minValue: '01/01/1901',
									allowBlank: true,
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: 'fechaEm1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},{
								xtype: 'checkbox',
								name: 'Cuentas',
								fieldLabel: 'Cuentas sin Validar',
								id: 'cuentas',
								hiddenName:'Cuentas',
                inputValue: 'S',
								listeners:{
										check: function(checbox){
											}	
								
								}
								}


]
//---------------------------------

//----------------------------	 

 var grid = new Ext.grid.EditorGridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consultaData,
				style: 'margin:0 auto;',
				clicksToEdit : 1,
				columns:[
				//CAMPOS DEL GRID
						{
						
						header:'Tipo de Afiliado',
						tooltip: 'Tipo de Afiliado',
						sortable: true,
						dataIndex: 'TIPOAFILIADO',
						width: 130,
						align: 'center',
						renderer: function (value,record,rowindex,colindex,store){
								var auxTipoAfiliado="";
								
									if("E"==(value))
										auxTipoAfiliado = "EPO";
									else if("P"==(value))
										auxTipoAfiliado = "Proveedor";
									else if("D"==(value))
										auxTipoAfiliado = "Distribuidor";
									else if("I"==(value))
										auxTipoAfiliado = "IF Bancario/No Bancario";
									else if("B"==(value))
										auxTipoAfiliado = "IF Beneficiario";
								return auxTipoAfiliado;
							}
						},
						{
						
						header: 'Producto',
						tooltip: 'Producto',
						sortable: true,
						dataIndex: 'PRODUCTO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Nombre',
						tooltip: 'Nombre',
						sortable: true,
						dataIndex: 'NOMBRE',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Banco',
						tooltip: 'Banco',
						sortable: true,
						dataIndex: 'BANCO',
						width: 150,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Cuenta Clabe',
						tooltip: 'Cuenta Clabe',
						sortable: true,
						dataIndex: 'CUENTACLABE',
						width: 140,
						align: 'center',
						renderer: function (value,metadata,record,rowindex,colindex,store){
								if(record.get('TIPO_MONEDA')=='1'){
									return value;
								}else{
									return 'N/A';
								}
							}
						
						},
						{
						align:'center',//'Cuenta Swift',
						header: 'Cuenta ',
						tooltip: 'Cuenta ',
						sortable: true,
						dataIndex: 'CUENTASWIFT',
						width: 140,
						renderer: function (value,metadata,record,rowindex,colindex,store){
								if(record.get('TIPO_MONEDA')=='54'){
									return value;
								}else{
									return 'N/A';
								}
							}
						},{
						header:'Plaza�',
						tooltip: 'Plaza�',
						sortable: true,
						dataIndex: 'PLAZA_SWIFT',
						width: 130,
						align: 'center',
						renderer: function (value,metadata,record,rowindex,colindex,store){
								if(record.get('TIPO_MONEDA')=='54'){
									return value;
								}else{
									return 'N/A';
								}
							}
						},
						{
						
						header: 'Sucursal ',
						tooltip: 'Sucursal ',
						sortable: true,
						dataIndex: 'SUCURSAL_SWIFT',
						width: 150,
						align: 'center',
						renderer: function (value,metadata,record,rowindex,colindex,store){
								if(record.get('TIPO_MONEDA')=='54'){
									return value;
								}else{
									return 'N/A';
								}
							}
						},
						{
						header: 'ABA',
						tooltip: 'ABA',
						sortable: true,
						dataIndex: 'ABA',
						width: 150,
						align: 'center',
						renderer: function (value,metadata,record,rowindex,colindex,store){
								if(record.get('TIPO_MONEDA')=='54'){
									return value;
								}else{
									return 'N/A';
								}
							}
						},
						{
						header: 'Tipo�de Moneda',
						tooltip: 'Tipo�de Moneda',
						sortable: true,
						dataIndex: 'TIPO_MONEDA',
						width: 130,
						align: 'center'
						},
						{
						header: 'EPO',
						tooltip: 'EPO',
						sortable: true,
						dataIndex: 'EPOREL',
						width: 150,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Descripci�n TEF',
						tooltip: 'Descripci�n TEF',
						sortable: true,
						dataIndex: 'ESTATUS_TEF',
						width: 130,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						
						},
						{
						align:'left',
						header: 'Descripci�n CECOBAN',
						tooltip: 'Descripci�n CECOBAN',
						sortable: true,
						dataIndex: 'ESTATUS_CECOBAN',
						width: 130,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Descripci�n DOLARES',
						tooltip: 'Descripci�n DOLARES',
						sortable: true,
						dataIndex: 'ESTATUS_DOLARES',
						width: 130,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								if(record.get('TIPO_MONEDA')=='54'){
									return value;
								}else{
									return 'N/A';
								}
							}
						},
						{
						header: 'Fecha y hora de �ltima actualizaci�n',
						tooltip: 'Fecha y hora de �ltima actualizaci�n',
						sortable: true,
						dataIndex: 'FECHA_ULT_MOD',
						width: 130,
						align: 'center'
						},
						{
						header: 'Usuario',
						tooltip: 'Usuario',
						sortable: true,
						dataIndex: 'USUARIO',
						width: 150,
						align: 'center'
						},
						{
							header	: 'Validar<br><input type="checkbox" name="HEADERVALIDAR" id="chkValidar01"  class="x-grid-checkcolumn" onClick="cambiarCHK (this,grid)">',
							dataIndex	: 'validar',
							tooltip: 'Validar',
							align: 'center',
							hidden: true,
							width: 55,
							renderer:function(v,column,registro,rowIndex,colIndex,store){
								if(registro.get('ESTATUS_CUENTA')!='99'){
									return '<input type="checkbox" name="VALIDAR" id="chkValidar001"  onClick="SeleccionValidar(this,'+rowIndex+',grid)">';
								}else{
									return '<input type="checkbox" name="VALIDAR" id="chkValidar001" disabled onClick="SeleccionValidar(this,'+rowIndex+',grid)">';
								}
							}
						},
						{
							header	: 'Invalidar<br><input type="checkbox" name="HEADERINVALIDAR" id="chkInvalidar01"  class="x-grid-checkcolumn" onClick="cambiarCHK (this,grid)">',
							dataIndex	: 'invalidar',
							tooltip: 'Invalidar',
							align: 'center',
							hidden: true,
							width: 55,
							renderer:function(v,column,registro,rowIndex,colIndex,store){
								if(registro.get('ESTATUS_CUENTA')=='99'){
									return '<input type="checkbox" name="INVALIDAR" id="chkInvalidar001"  onClick="SeleccionInvalidar(this,'+rowIndex+',grid)">';
								}else{
										return '<input type="checkbox" name="INVALIDAR" id="chkInvalidar001"  disabled onClick="SeleccionInvalidar(this,'+rowIndex+',grid)">';
								}
							}
						},
						{
							xtype	: 'checkcolumn',
							id	: 'chkMod',
							header	: 'Modificar<br><input type="checkbox" name="HCHKMod" id="chkModificar"  class="x-grid-checkcolumn" onClick="cambiarCHK (this,grid)">',
							dataIndex	: 'modificar',
							tooltip: 'modificar',
							hidden: true,
							align: 'center',
							width: 55,
							listeners:{
								change:function(a,b,c){
									alert('Works');
								}
							}
							
						},
						{
						xtype: 'actioncolumn',
						header: 'Seleccionar',
						tooltip: 'Seleccionar',
						sortable: true,
						dataIndex: 'ACTION',
						width: 150,
						align: 'center',
						items: [
						
							{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Invalidar';	
									 var tipo =Ext.getCmp('idAfiliado').getValue();
									 if(registro.get('ESTATUS_CUENTA')=='99' && tipo!='P'){//FODEA-O33-2014
										return 'invalidarCuenta';	
									 }
								 }
								 ,handler:  function(grid, rowIndex, colIndex, item, event){
										var registro = consultaData.getAt(rowIndex);
													Ext.Msg.show({
													title:	"Mensaje",
													msg:		'"�Est� seguro de querer Invalidar la cuenta?"',
													buttons:	Ext.Msg.OKCANCEL,
													fn: function resultMsj(btn){
														 if (btn == 'ok') {
																Ext.Ajax.request({
																		url: '15consulta01Ext.data.jsp',
																		params: Ext.apply({informacion:'invalidaCuenta',cuenta: registro.get('IC_CUENTA') ,esCuentaSpeua:registro.get('ES_CUENTA_SPEUA')  }),
																		callback: accionInvalida
																	});
														}
													},
													closable:false,
													icon: Ext.MessageBox.QUESTION
												});	
								 }
					      },
							{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[1].tooltip = 'Validar';	
									 var tipo =Ext.getCmp('idAfiliado').getValue();
									 if(registro.get('ESTATUS_CUENTA')!='99' && tipo!='P'){//FODEA-033-2014
										return 'icoValidar ';	
									 }
								 }
								 ,handler: function(grid, rowIndex, colIndex, item, event){
										var registro = consultaData.getAt(rowIndex);
													Ext.Msg.show({
													title:	"Mensaje",
													msg:		'"�Est� seguro de querer Validar la cuenta?"',
													buttons:	Ext.Msg.OKCANCEL,
													fn: function resultMsj(btn){
														 if (btn == 'ok') {
																Ext.Ajax.request({
																		url: '15consulta01Ext.data.jsp',
																		params: Ext.apply({informacion:'validaCuenta',cuenta: registro.get('IC_CUENTA') ,esCuentaSpeua:registro.get('ES_CUENTA_SPEUA')  }),
																		callback: accionInvalida
																	});
														}
													},
													closable:false,
													icon: Ext.MessageBox.QUESTION
												});	
											}
					      },{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[2].tooltip = 'Modificar';								
									 var tipo =Ext.getCmp('idAfiliado').getValue();
									 var moneda =Ext.getCmp('cboMoneda01_').getValue();
									 if(tipo!='P'||(tipo=='P' && moneda !=1)){//FODEA-033-2014
									 return 'modificar';	
									 }
							       
								 }
								 ,handler: function(grid, rowIndex, colIndex, item, event){
												var registro = consultaData.getAt(rowIndex);
												ventanaModifica.setVisible(true);
												accionMod_Rea='M';
												ic_epo = registro.get('IC_EPO_REL');
												no_cuenta=registro.get('IC_CUENTA');
												Ext.getCmp('idProductoMod').setDisabled(false);
												Ext.getCmp('numElectronicoMod').setDisabled(false);
												Ext.getCmp('nombreMod').setDisabled(false);
												Ext.getCmp('rfc_eMod').setDisabled(false);
												Ext.getCmp('cuentaMod').setDisabled(false);
												Ext.getCmp('plaza_swiftMod').setDisabled(false);
												Ext.getCmp('sucursal_swiftMod').setDisabled(false);
												Ext.getCmp('icEpoMod').setVisible(false);
                        Ext.getCmp('cgSwift').setDisabled(false);
                        Ext.getCmp('cgAba').setDisabled(false);
                        Ext.getCmp('cuentaDL').setDisabled(false);
                        
												Ext.Ajax.request({
													url: '15consulta01Ext.data.jsp',
													params: Ext.apply({informacion:'obtenerCuenta',cuenta: registro.get('IC_CUENTA')  }),
													callback: cargaCuenta
												});
								 }
					      },
							{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[3].tooltip = 'Reasignar';	
									 if(registro.get('TIPOAFILIADO')=='P'||registro.get('TIPOAFILIADO')=='I'||registro.get('TIPOAFILIADO')=='B'){
										return 'icoEditarForma ';	
									 }
								 }
								 ,handler: function(grid, rowIndex, colIndex, item, event){
												var registro = consultaData.getAt(rowIndex);
												ventanaModifica.setVisible(true);
												accionMod_Rea='R';
												ic_epo = registro.get('IC_EPO_REL');
												no_cuenta = registro.get('IC_CUENTA');
												if(registro.get('TIPOAFILIADO')=='P'||registro.get('TIPOAFILIADO')=='I'||registro.get('TIPOAFILIADO')=='B'){
													Ext.getCmp('icEpoMod').setVisible(true);
													afiliadoReasigna=registro.get('TIPOAFILIADO');
												}else{
													afiliadoReasigna='';
													Ext.getCmp('icEpoMod').setVisible(false);
												}
												Ext.getCmp('idProductoMod').setDisabled(true);
												Ext.getCmp('numElectronicoMod').setDisabled(true);
												Ext.getCmp('nombreMod').setDisabled(true);
												Ext.getCmp('rfc_eMod').setDisabled(true);
												Ext.getCmp('cuentaMod').setDisabled(true);
												Ext.getCmp('plaza_swiftMod').setDisabled(true);
												Ext.getCmp('sucursal_swiftMod').setDisabled(true);
                        Ext.getCmp('cgSwift').setDisabled(true);
                        Ext.getCmp('cgAba').setDisabled(true);
                        //Ext.getCmp('cuentaDL').setDisabled(true);
                        //Ext.getCmp('cuentaMod').setDisabled(false);
												Ext.Ajax.request({
													url: '15consulta01Ext.data.jsp',
													params: Ext.apply({informacion:'obtenerCuenta',cuenta: registro.get('IC_CUENTA')  }),
													callback: cargaCuenta
												});
								 }
					      },{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[4].tooltip = 'Eliminar';
									 if(registro.get('TIPOAFILIADO')=='P'||registro.get('TIPOAFILIADO')=='I'||registro.get('TIPOAFILIADO')=='B'){
										return 'borrar';	
									 }
								 }
								 ,handler: procesarEliminar
					      }]	
						}
						
						
				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					items: ['->','-',
					{
							xtype: 'button',
							text: 'Cuentas sin Validar CLABE',
							id: 'ctasClabe',
							handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '15cuenta01ExtCSV.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'GenerarArchivo'}),
									callback: descargaArchivos
								});
							}
						},{
							xtype: 'button',
							text: 'Generar Archivo EXCEL',
							id: 'ctasClabeEXCEL',
							handler: function(boton, evento) {
								procesarArchivoRuta(archivo1);
							}
						},{
							xtype: 'button',
							text: 'Generar Archivo CLABE',
							id: 'ctasClabeTXT',
							handler: function(boton, evento) {
								procesarArchivoRuta(archivo2);
							}
						},'-',
						{//FODEA-033-2014
							xtype	:	'button',
							text	:	'Modificar',
							id		:	'btnModificar',
							hidden	:	true,
							handler	:	function(	boton,evento	){
										
								//Abrir una ventana con la infoamci�n a modificar masivamente
								var forma = Ext.getCmp('forma').getForm().getValues();
								var store =Ext.getCmp('grid').getStore();
								var modificados= store.getModifiedRecords( ); 
								var modificar=[]; //almacema las ic_cuentas a ser modificadas
								if(!Ext.isEmpty(modificados)){
									boton.disable();
										boton.setIconClass('loading-indicator');
									var consultar = false;
									for(var i = 0 ; i<modificados.length; i++){
										if(modificados[i].dirty && modificados[i].data.modificar){
											consultar = true;	
											modificar.push(modificados[i].data.IC_CUENTA);
										}
									}
									if(consultar){
										accionConsulta("OBTENER_INFO_VENTANA_MODIFICAR",forma);
									}else{
										Ext.Msg.alert('','No se hay registros seleccionados...');
										boton.enable();
										boton.setIconClass('');
										store.rejectChanges();
									}
								}else{
									Ext.Msg.alert('','No se hay registros seleccionados...');
									boton.enable();
										boton.setIconClass('');
										store.rejectChanges();
								}
							}
						},
						'-',
						{
							xtype: 'button',
							text: 'Generar Archivo',
							id: 'btnGenerarArchivo',
							handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '15consulta01Ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'GenerarArchivo'}),
									callback: procesarArchivoSuccess
								});
							}
						},
						'-',
						{
							xtype: 'button',
							text: 'Generar PDF',
							id: 'btnGenerarPDF',
							handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								
								Ext.Ajax.request({
									url: '15consulta01Ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
											informacion: 'GenerarPDF'
										}),
									callback: procesarArchivoSuccess
								});
							}
						},
						'-',
						{//FODEA-033-2014
							xtype :	'button',
							text	:	'Guardar',
							id		:	'btnGuardar',
							hidden	:	true,
							handler	:	function(	boton, evento	){
								//Validar  e invalidar todas las cuentas seleccionadas
								var procesar = false;
								var validar=document.getElementsByName("VALIDAR");
								var invalidar=document.getElementsByName("INVALIDAR");
								var store = grid.getStore();
								var validar_=[];
								var invalidar_=[];
								for(var i = 0;i<validar.length;i++){
									var record=store.getAt(i);
									if(validar[i].checked){
										validar_.push('V'+record.get('IC_CUENTA'));
									}
									if(invalidar[i].checked){
										invalidar_.push('I'+record.get('IC_CUENTA'));
									}
								}
								var cuentas =validar_.concat(invalidar_) ;
								if(!Ext.isEmpty(validar_)||!Ext.isEmpty(invalidar_)){
									Ext.Msg.show({
										title:	"Mensaje",
										msg:		'�Est� seguro de querer Validar/Invalidar las cuentas seleccionadas?',
										buttons:	Ext.Msg.OKCANCEL,
										fn: function resultMsj(btn){
											 if (btn == 'ok') {
													Ext.Ajax.request({
														url: '15consulta01Ext.data.jsp',
														params: Ext.apply(fp.getForm().getValues(),{
																informacion: 'Validar_Invalidar',
																ic_cuentas : cuentas
															}),success:function(response){
																store.rejectChanges( );
															},callback: accionInvalida
													});	
											}
										},
										closable:false,
										icon: Ext.MessageBox.QUESTION
									});	
								}else{
									Ext.Msg.alert('','No hay registros seleccionados');
								}
							}
						}
					]
				},
				listeners	:{
					 show:function(grid){
						grid.getView().refresh(true);
					}
				}
		});
		
 var fpModifica = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		//bodyStyle: 'padding: 8px',
		labelWidth: 100,
		defaultType: 'textfield',
		frame: true,
		id: 'formaMod',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosModifica,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Modificar',
			  name: 'btnBuscar',
			  iconCls: '',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
            if( tipoMon=='P' && !cuentaClabeOnBlurHandler(Ext.getCmp('cuentaMod')) )return;
            
           /* if( tipoMon=='P' && Ext.getCmp('cuentaMod').getValue()=='' ){
              Ext.getCmp('cuentaMod').markInvalid('Este campo es obloigatorio');
              return;
            }
            
            if( tipoMon=='D' && Ext.getCmp('cuentaDL').getValue()=='' ){
              Ext.getCmp('cuentaDL').markInvalid('Este campo es obloigatorio');
              return;
            }*/
            
						if(!fpModifica.getForm().isValid())
						{
							return;
						}
						
						
						Ext.Ajax.request({
							url: '15consulta01Ext.data.jsp',
							params: Ext.apply(fpModifica.getForm().getValues(),{accion:accionMod_Rea,informacion:'ModificaReasigna', no_cuenta:no_cuenta}),
							callback: accionModReaf
						});
						/*//Mi acci�n
						grid.show();
						consulta.load({ params: Ext.apply(fp.getForm().getValues(),{txt_ne: Ext.getCmp('txt_ne').getValue()})
						});				
*/
					}
			 },{
				text:'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {
						ventanaModifica.hide();
				}
			 }
			 
		]
	});
		var ventanaModifica = new Ext.Window({
			width: 700,
			height: 'auto',
			//maximizable: true,
			modal: true,
			closable: true,
			closeAction: 'hide',
			resizable: true,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			//layout: 'fit',
			items: [fpModifica]
});
/*var fpConfirmacion = new Ext.form.FormPanel
  ({
		hidden: true,
		height: 'auto',
		width: 250,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		labelWidth: 100,
		defaultType: 'textfield',
		frame: true,
		id: 'formaConfirmacion',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaConfirmacion,
		monitorValid : true,//FODEA-033-2014 
		buttons:[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Aceptar',
			  name: 'btnAceptar',
			  iconCls: 'icoAceptar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
				{	
					
						var usuario = Ext.getCmp('claveUsuario');
						var contrasenia = Ext.getCmp('contrasenia');
						if(Ext.isEmpty(usuario.getValue())){
							alert('El campo de Login no puede estar vacio');
							return;
						}
						if(Ext.isEmpty(contrasenia.getValue())){
							alert('El campo de Password no puede estar vacio');
							return;
						}
						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15consulta01Ext.data.jsp',
							params: Ext.apply(fpConfirmacion.getForm().getValues(),{							
								informacion: 'ValidaUsuario'
							}),success: function(){
								boton.enable();
								boton.setIconClass('icoAceptar');
							},	
							callback: procesarValidaUsuario
						});
						
				}
			 },{
				text:'Cancelar',
				iconCls: 'icocancelar',
				handler: function() {
					//window.location.href='15consulta01Ext.jsp';
				}
			 }
			 
		]
	});*/

var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 700,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		//bodyStyle: 'padding: 8px',
		labelWidth: 130,
		defaultType: 'textfield',
		frame: true,
		
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid : true,//FODEA-033-2014 
		buttons:[
				
			{//FODEA-033-2014
				text	:	'Descarga  Gral. Cuentas Clabe',  
				id		:	'btnDescargaGralCuentaClaveCSV',
				hidden:	true,
				tooltip  : 'Archivo que descargar� la informaci�n de cuentas clabe general de acuerdo a los campos seleccionados ',
				iconCls: 'icoXls',
				handler	: function(boton, evento){
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '15consulta01Ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'GenerarArchivosCSV',
							tipo	: 'CSV'
						}),success: function(){
							boton.enable();
							boton.setIconClass('icoXls');
						},	
						callback: procesarDescargaArchivos
					});
				}
			},
		
		
			{//FODEA-033-2014
				text	:	'Descarga Cuenta Clabe .txt', 
				id		:	'btnDescargaCuentaClaveTXT',
				hidden:	true,
				tooltip  : 'Archivo que descargar� la cuenta clabe del d�a en formato .txt ',
				iconCls: 'icoTxt',
				handler	: function(boton, evento){
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '15consulta01Ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'GenerarArchivos',
							tipo	: 'TXT'
						}),success: function(){
							boton.enable();
							boton.setIconClass('icoTxt');
						},	
						callback: procesarDescargaArchivos
					});
				}
			},
			{
				text	:	'Descarga Cuenta Clabe',
				id		:	'btnDescargaCuentaClaveXLS',
				hidden:	true,
				tooltip : 'Archivo que descargar� la cuenta clabe del d�a en formato .xls ',
				iconCls: 'icoXls',
				handler	: function(boton, evento){
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '15consulta01Ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'GenerarArchivos',
							tipo	: 'XLS'
						}),success: function(){
							boton.enable();
							boton.setIconClass('icoXls');
						},	
						callback: procesarDescargaArchivos
					});
				}
			},	
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						if(!fp.getForm().isValid())
						{
							return;
						}

						if(verificaFechas('fechaEm1','fechaEm2')){
						grid.show();
						fp.el.mask('Procesando...', 'x-mask-loading');
						
						Ext.getCmp('ctasClabeTXT').setVisible(false);
						Ext.getCmp('ctasClabeEXCEL').setVisible(false);
						if(Ext.getCmp('cuentas').getValue())
							{
								Ext.getCmp('ctasClabe').setVisible(true);	
								
							}else{
								Ext.getCmp('ctasClabe').setVisible(false);
							}

						consultaData.load({ params: Ext.apply(fp.getForm().getValues())
								});
						}
						/*//Mi acci�n
						grid.show();
						consulta.load({ params: Ext.apply(fp.getForm().getValues(),{txt_ne: Ext.getCmp('txt_ne').getValue()})
						});				
*/
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});
	
	
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
	  fp,NE.util.getEspaciador(30),grid,ventanaModifica
		]
  });
  catalogoProducto.load();
  Ext.getCmp('_txt_nombre').setVisible(false);
	Ext.getCmp('compoBot').setVisible(false);
  
});