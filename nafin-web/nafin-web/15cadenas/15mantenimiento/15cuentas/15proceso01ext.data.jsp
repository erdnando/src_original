<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.io.*,
		com.netro.xls.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		com.netro.model.catalogos.CatalogoEPOSwift,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.descuento.BusquedaAvanzadaI,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		com.netro.descuento.ConsCuentasCA,
		javax.naming.Context"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 			= (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if ( informacion.equals("CatalogoProducto") )	{
	List lstCveProd = new ArrayList();
	lstCveProd.add("1");
	lstCveProd.add("4");
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_producto_nafin");
	cat.setCampoDescripcion("ic_nombre");
	cat.setTabla("comcat_producto_nafin");
	cat.setCondicionIn(lstCveProd);
	cat.setOrden("ic_nombre");
	
	/*
		Cuando el tipo de afiliado es "D" (Distribuidor) se utiliza el
		producto "4", pero para admin epo esta opcion no se utiliza.
	*/
	infoRegresar = cat.getJSONElementos();
	
}else if ( informacion.equals("CargaArchivo.Iniciar") ){
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);

	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String PATH_FILE = strDirectorioTemp;

	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {

		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(2097152);
		myUpload.upload();
		
		int liNoArchivos = myUpload.save(PATH_FILE);

	} catch(Exception e) {

		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 2097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 2 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}

	}

	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	String optCuenta			= (myRequest.getParameter("optCuenta")			== null)?"":myRequest.getParameter("optCuenta");
	String tipoArchivo 			= (myRequest.getParameter("tipoArchivo")			== null)?"":myRequest.getParameter("tipoArchivo");
	System.out.println("optCuenta ==== "+optCuenta);
	System.out.println("tipoArchivo ==== "+tipoArchivo);

	// Guardar Archivo en Disco
	int numFiles = 0;
	String lsRuta = "";
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		//myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," ") );
		//myFile.saveAs(strDirectorioTemp + myFile.getFileName().replaceAll("\\s+"," ") );
		//resultado.put("fileName", 	myFile.getFileName().replaceAll("\\s+"," ") );
		lsRuta = PATH_FILE+myFile.getFileName();

	}catch(Exception e){
		
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}

	java.io.File lofArchivo=new java.io.File(lsRuta);

	BufferedReader lobrDatos=new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo)));

	String lsLinea = "";
	StringBuffer lsbDocumentos = new StringBuffer();

	if(tipoArchivo.equals("archTxt")){

		while((lsLinea=lobrDatos.readLine())!=null) {

			lsLinea = lsLinea.replace('\'',' ');
			lsbDocumentos.append(lsLinea.trim()+"\n");
		}//while linea
	}
	
	if(tipoArchivo.equals("archExcel")){

		lsbDocumentos.delete(0, lsbDocumentos.length()); //Borrar el StrinBuffer para cargar los datos extraidos del archivo XLS

		ComunesXLS oComunesXLS = new ComunesXLS();
		List myLista = new ArrayList();
		myLista = oComunesXLS.LeerXLS(lsRuta, true);
		String nueva_linea = "";
		java.util.Date fecha = new java.util.Date();
		String fechaFormateada = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fecha);

		/*Este bloque es tratado ya que si el archivo a leer no contiene como minimo 6
		  columnas lanzara una excepcion al leer la lista*/
		try{
			for(int i = 1; i < myLista.size(); ++ i){
				List myLista2 = new ArrayList();
				myLista2 = (ArrayList)myLista.get(i);

				nueva_linea += "1;";
				nueva_linea += fechaFormateada.replace('/', '-') + ";";
				nueva_linea += fechaFormateada.replace('/', '-') + ";";
				nueva_linea += fechaFormateada.replace('/', '-') + ";";
				nueva_linea += myLista2.get(1).toString().replace('\'',' ') + ";";
				nueva_linea += "40;";
				nueva_linea += myLista2.get(4).toString().replace('\'',' ') + ";";
				nueva_linea += "abc;";
				nueva_linea += "abc;";
				nueva_linea += myLista2.get(6).toString().replace('\'',' ');

				lsbDocumentos.append(nueva_linea.trim()).append("\n");
				nueva_linea = "";
			}
		}catch(Exception e){
			out.println("<script>" +
							"	alert('Se genero un error al leer el archivo, verifique que este sea correcto'); " +
							"	location.href='15proceso1.jsp'; " +
							"</script>");
		 }
	}
	String lsSiguienteLlave = "";
	Hashtable lohResultados = null;
	
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);

	if ("T".equalsIgnoreCase(optCuenta)){
		lsSiguienteLlave = dispersion.sgetNumaxDispersion("TEF");
		lohResultados = dispersion.ohprocesarDispersionTEF(lsbDocumentos.toString(), lsSiguienteLlave);
	}
	if ("C".equalsIgnoreCase(optCuenta)){
		lsSiguienteLlave = dispersion.sgetNumaxDispersion("CECOBAN");
		lohResultados = dispersion.ohprocesarDispersionCECOBAN(lsbDocumentos.toString(), lsSiguienteLlave);
	}

	boolean bTodosOk = new Boolean(lohResultados.get("bTodosOk").toString()).booleanValue();

	//Actualizamos y mostramos los pedidos cargados masivamente
	boolean bOkActualiza = false;

	/*Vectores comunes*/
	Vector lovFechaRegistro 		= (Vector)lohResultados.get("lovFechaRegistro");
  	Vector lovFechaTransferencia	= (Vector)lohResultados.get("lovFechaTransferencia");
  	Vector lovFechaAplicacion 		= (Vector)lohResultados.get("lovFechaAplicacion");
	Vector lovRFC 						= (Vector)lohResultados.get("lovRFC");
  	Vector lovTipoCuenta				= (Vector)lohResultados.get("lovTipoCuenta");
  	Vector lovNumeroCuenta 			= (Vector)lohResultados.get("lovNumeroCuenta");
	Vector lovReferenciaRastreo 	= (Vector)lohResultados.get("lovReferenciaRastreo");
  	Vector lovLeyendaRastreo		= (Vector)lohResultados.get("lovLeyendaRastreo");

	/*Vectores Especificos*/
	if ("T".equalsIgnoreCase(optCuenta)){
	  	Vector lovNombre				= (Vector)lohResultados.get("lovNombre");
	  	Vector lovAdscripcion 			= (Vector)lohResultados.get("lovAdscripcion");
		Vector lovClaveBanco 			= (Vector)lohResultados.get("lovClaveBanco");
	  	Vector lovMotivoRechazo 		= (Vector)lohResultados.get("lovMotivoRechazo");
	}
	if ("C".equalsIgnoreCase(optCuenta)){
		Vector lovRastreo		 		= (Vector)lohResultados.get("lovRastreo");
	  	Vector lovEstatus		 		= (Vector)lohResultados.get("lovEstatus");
	}
	
	String lsEstError = lohResultados.get("lsbError").toString();
	String lsEstCorrecto = lohResultados.get("lsbCorrecto").toString();
	String esEstError="", esEstCorrecto="";
	
	VectorTokenizer lvtValores = null;
	Vector lvDatos = new Vector();
	String contenido = "";
	String lineas = "";
	int contador = 0;
	if (!"".equals(lsEstCorrecto.trim())) {
		lvtValores = new VectorTokenizer(lsEstCorrecto+" ","|");
		lvDatos = lvtValores.getValuesVector();
		for (int i = 0; i < lvDatos.size()-1; i++) {
			contenido = (String)lvDatos.get(i);
			lineas += contenido;
			if (!contenido.equals("")) {
				contador++;
			}
		}
	}
	resultado.put("numRegCorrectos", String.valueOf(contador));
	resultado.put("lsEstCorrecto", lineas 	);
	
	lvtValores = null;
	lvDatos = new Vector();
	contenido = "";
	lineas = "";
	contador = 0;
	if (!"".equals(lsEstError.trim())) {
		lvtValores = new VectorTokenizer(lsEstError+" ","|");
		lvDatos = lvtValores.getValuesVector();
		for (int i = 0; i < lvDatos.size(); i++) {
			contenido = (String)lvDatos.get(i);
			lineas += contenido;
			if (!contenido.equals("")) {
				contador++;
			}
		}
		if(contador>0){
			String contArchivo = lohResultados.get("lsbContArchivo").toString();
			String nombreArchivo = "";
			CreaArchivo archivo = new CreaArchivo();
			if (archivo.make(contArchivo, strDirectorioTemp, ".txt"))
				nombreArchivo = archivo.nombre;
			
			resultado.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo);
		}
	}
	
	
	resultado.put("lsEstError", lineas 	);
	resultado.put("numRegErroneos", String.valueOf(contador));
	// Especificar el estado siguiente
	// Enviar resultado de la operacion
	resultado.put("lsProcSirac", 					lsSiguienteLlave		);
	resultado.put("success", 					new Boolean(success)		);
 
	infoRegresar = resultado.toString();

}else if ( informacion.equals("ProcesarInformacion") ){
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String lsProcSirac = (request.getParameter("lsProcSirac")==null)?"":request.getParameter("lsProcSirac");
	String cuenta = (request.getParameter("optCuenta") == null) ? "" : request.getParameter("optCuenta");
	String lsNumeroCuentas = "";
	Vector lovAnticipos = null;
	List lstNumeroCuenta = new ArrayList();
	int contador = 0;

	if ("T".equalsIgnoreCase(cuenta))
		lovAnticipos = dispersion.ovgetDispersionesProcesadasTEF(lsProcSirac);

	if ("C".equalsIgnoreCase(cuenta))
		lovAnticipos = dispersion.ovgetDispersionesProcesadasCECOBAN(lsProcSirac);

	int liTotal = lovAnticipos.size();

	for(int i=0; i<liTotal; i++) {
		contador++;
		Vector lovRegistro = (Vector)lovAnticipos.get(i);

		if ("T".equalsIgnoreCase(cuenta)){
			lsNumeroCuentas = lovRegistro.get(8).toString()+"\n"; 
			List arraySingle = new ArrayList();
			arraySingle.add(lsNumeroCuentas);
			lstNumeroCuenta.add(arraySingle);
		}

		if ("C".equalsIgnoreCase(cuenta)){
			lsNumeroCuentas = lovRegistro.get(6).toString()+"\n"; 
			List arraySingle = new ArrayList();
			arraySingle.add(lsNumeroCuentas);
			lstNumeroCuenta.add(arraySingle);
		}
		
		
	}
	boolean bOkBorra = false;
			
	if ("T".equalsIgnoreCase(cuenta))			
		bOkBorra = dispersion.bborrarDispersionesTmp(lsProcSirac, "TEF");
			
	if ("C".equalsIgnoreCase(cuenta))
		bOkBorra = dispersion.bborrarDispersionesTmp(lsProcSirac, "CECOBAN");
	
	
	resultado.put("lsNumeroCuenta", lstNumeroCuenta); 
	resultado.put("totalContador", String.valueOf(contador)); 
	resultado.put("success", 					new Boolean(success)		); 
	infoRegresar = resultado.toString();

}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>