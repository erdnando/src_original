<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.descuento.*,
		com.netro.cadenas.*,
		com.netro.xls.*,
		com.netro.dispersion.*,
		com.netro.model.catalogos.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	String infoRegresar ="";
/******************************************************************************************************
*	Genera los archivos con las cuentas TEF o SPEUA
******************************************************************************************************/
public StringBuffer generaArchivos(String tipo, String tipo2, String tipoArchivo, HttpServletRequest request, AccesoDB con)  throws Exception {

	
	String cmb_tipo_afiliado 	= (request.getParameter("Afiliado") == null) ? "" : request.getParameter("Afiliado");
	String no_producto 			= (request.getParameter("Producto") == null) ? "" : request.getParameter("Producto");
	String no_epo 				= (request.getParameter("HicEpo") == null) ? "" : request.getParameter("HicEpo");
	String no_nafin 			= (request.getParameter("txt_nafelec") == null) ? "" : request.getParameter("txt_nafelec");
	String rfc 					= (request.getParameter("R_F_C") == null) ? "" : request.getParameter("R_F_C");
	String fecha_registro_de	= (request.getParameter("fechaEm1") == null) ? "" : request.getParameter("fechaEm1");
	String fecha_registro_a		= (request.getParameter("fechaEm2") == null) ? "" : request.getParameter("fechaEm2");

	
	Dispersion dispersionEJB = dispersionEJB = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	SimpleDateFormat sdf = new SimpleDateFormat ("dd-MM-yyyy");
	
	//StringBuffer contArchivo = new StringBuffer();
	StringBuffer strConsulta = new StringBuffer();
	
	String nombreArchivoClabe = "";
	String nombreArchivoSpeua = "";
	String nombreArchivoExcel = "";
	
	StringBuffer archivoClabe = new StringBuffer();
	StringBuffer archivoSpeua = new StringBuffer();
	StringBuffer archivoExcel = new StringBuffer();
	
	int _numRegistrosExcel = 0; //Contador del numero de registros que llevara el archivo Excel
	
	CreaArchivo archivo = new CreaArchivo();
	
	String sFechaActual = sdf.format(new java.util.Date());
	Calendar cFechaSigHabil = new GregorianCalendar();
	cFechaSigHabil.setTime(new java.util.Date());
	cFechaSigHabil.add(Calendar.DATE, 1);
	if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // Sábado
		cFechaSigHabil.add(Calendar.DATE, 2);
	if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
		cFechaSigHabil.add(Calendar.DATE, 1);
	Vector vFecha = dispersionEJB.bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil);
	boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
	cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
	while(bInhabil) {
		vFecha = dispersionEJB.bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil);
		bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
		if(!bInhabil)
			break;
		cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
	}
	String sFechaSigHabil = sdf.format(cFechaSigHabil.getTime());
	
	
	strConsulta.delete(0,strConsulta.length());		/*Limpia variable*/
	strConsulta.append("SELECT /*+index(c CP_COM_CUENTAS_PK) use_nl(c n)*/ c.ic_nafin_electronico,n.ic_epo_pyme_if, n.cg_tipo, ");
	strConsulta.append("TO_CHAR(c.df_registro,'dd-MM-yyyy') as df_registro, TO_CHAR(c.df_transferencia,'dd-MM-yyyy') as df_transferencia, ");
	strConsulta.append("TO_CHAR(c.df_aplicacion,'dd-MM-yyyy') as df_aplicacion, c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta ");
	if (tipo.equals("CLABE")) {
		strConsulta.append("FROM com_cuentas c, comrel_nafin n WHERE ic_estatus_cecoban is null AND ic_tipo_cuenta = 40  ");
	} else {
		strConsulta.append("FROM com_cuentas c, comrel_nafin n WHERE ic_estatus_tef is null  AND ic_tipo_cuenta = 1 ");
	}
	strConsulta.append(" AND ((cg_tipo_afiliado = 'E' AND ic_epo is null) OR (cg_tipo_afiliado != 'E' AND ic_epo is not null)) ");
	if("H".equals(tipoArchivo)) {
		strConsulta.append("AND c.df_registro >= trunc(sysdate)");
		strConsulta.append("AND c.df_registro < trunc(sysdate+1)");
	}
	if(!"".equals(fecha_registro_de)) {
		strConsulta.append("    AND c.df_registro >= TO_DATE('"+fecha_registro_de+"', 'dd/mm/yyyy')");
	}
	if(!"".equals(fecha_registro_a)) {
		strConsulta.append("    AND c.df_registro < (TO_DATE('"+fecha_registro_a+"', 'dd/mm/yyyy')+1)");
	}
	if(!"".equals(cmb_tipo_afiliado)) {
		strConsulta.append("    AND c.cg_tipo_afiliado = '"+cmb_tipo_afiliado+"'  ");
	}
	if(!"".equals(no_producto)) {
		strConsulta.append("    AND c.ic_producto_nafin = "+no_producto+"  ");
	}
	if(!"".equals(no_epo)) {
		strConsulta.append("    AND c.ic_epo = "+no_epo+"  ");
	}
	if(!"".equals(no_nafin)) {
		strConsulta.append("    AND c.ic_nafin_electronico = "+no_nafin+"  ");
	}
	
	strConsulta.append(" AND c.ic_nafin_electronico = n.ic_nafin_electronico ");
	String Aux = strConsulta.toString();
	String Aux2 = "";
	ResultSet rs= con.queryDB(strConsulta.toString());
	ResultSet rse = null;
System.out.println("<br>Consulta Archivo Speua "+strConsulta);

	while (rs.next())
	{
		strConsulta.delete(0,strConsulta.length());		/*Limpia variable*/
		if ("E".equals(rs.getString("CG_TIPO")))
			strConsulta.append("SELECT ic_epo, SUBSTR(NVL(cg_razon_social,'Sin Nombre'), 0, 30) as empresa , cg_rfc as rfc FROM comcat_epo WHERE ic_epo = ").append(rs.getString("IC_EPO_PYME_IF"));
		else if ("P".equals(rs.getString("CG_TIPO")))
			strConsulta.append("SELECT ic_pyme, SUBSTR(NVL(cg_razon_social,cg_nombre||' '||cg_appat||' '||cg_apmat  ), 0, 30) as empresa, cg_rfc as rfc FROM comcat_pyme WHERE ic_pyme = ").append(rs.getString("IC_EPO_PYME_IF"));
		else if ("I".equals(rs.getString("CG_TIPO")))
			strConsulta.append("SELECT ic_if, SUBSTR(NVL(cg_razon_social,'Sin Nombre'), 0, 30) as empresa, cg_rfc as rfc FROM comcat_if WHERE ic_if = ").append(rs.getString("IC_EPO_PYME_IF"));
		Aux2 = strConsulta.toString();
		//out.print("<br>Consulta Archivo Speua "+strConsulta);
		rse = con.queryDB(strConsulta.toString());
		
		if (rse.next()){
			if ("1".equals(rs.getString("IC_TIPO_CUENTA"))){
				archivoSpeua.append(sFechaActual).append(";").append(sFechaSigHabil).append(";");
				archivoSpeua.append(sFechaSigHabil).append(";").append(formatoRFC(rse.getString("rfc"))).append(";");
				archivoSpeua.append(rse.getString("empresa")).append(";").append("HDA").append(";");
				archivoSpeua.append(rs.getString("IC_BANCOS_TEF")).append(";").append(rs.getString("IC_TIPO_CUENTA")).append(";");
				archivoSpeua.append(Comunes.rellenaCeros(rs.getString("CG_CUENTA"),11)).append(";").append("TEF-NE").append(";");
				archivoSpeua.append("VALIDACIÓN DE CUENTAS PARA TEF").append(";\n");
			} else {
				archivoClabe.append(sFechaActual).append(";").append(sFechaSigHabil).append(";");
				archivoClabe.append(sFechaSigHabil).append(";").append(formatoRFC(rse.getString("rfc"))).append(";");
				archivoClabe.append(rse.getString("empresa")).append(";").append("HDA").append(";");
				archivoClabe.append(rs.getString("IC_BANCOS_TEF")).append(";").append(rs.getString("IC_TIPO_CUENTA")).append(";");
				archivoClabe.append(Comunes.rellenaCeros(rs.getString("CG_CUENTA"),18)).append(";").append("TEF-NE").append(";");
				archivoClabe.append("VALIDACIÓN DE CUENTAS PARA TEF").append(";\n");				
			}
			
			if(tipo.equals("CLABE") && tipo2.equals("EXCEL")){
				/*Generar Archivo tipo EXCEL*/
				_numRegistrosExcel ++;
				
				archivoExcel.append(_numRegistrosExcel).append("|").append(formatoRFC(rse.getString("rfc"))).append("|");
				archivoExcel.append(rse.getString("empresa")).append("|").append(rs.getString("IC_BANCOS_TEF")).append("|");
				archivoExcel.append(rs.getString("CG_CUENTA")).append("|").append("HDE-EF-15").append("@");
		   }
		}
				
		rse.close();
		con.cierraStatement();
	}
	rs.close();
	con.cierraStatement();
	if (tipo.equals("CLABE") && tipo2.equals("")) {
		return archivoClabe;
	} 
	if(tipo.equals("CLABE") && tipo2.equals("EXCEL")){
		return archivoExcel;
	}
	else {
		return archivoSpeua;
	}
}
%>



<%!
public String formatoRFC (String cadena){
	String cad = "", car = "";
		for(int i=0;i < cadena.length();i++) {
			car = cadena.substring(i,i+1);
			if ((i==4) && ("0".equals(car) || "1".equals(car) || "2".equals(car) || "3".equals(car) || "4".equals(car) || "5".equals(car) || "6".equals(car) || "7".equals(car) || "8".equals(car) || "9".equals(car)))
				cad += " ";
			
            if (!"-".equals(car))
				cad += car;
        }
	return cad;
}
%>
<%
	String tipoArchivo 	=		"C";
	String generar_archivo_excel= "si";
	TimeZone.setDefault(TimeZone.getTimeZone("CST"));
	SimpleDateFormat sdf = new SimpleDateFormat ("dd-MM-yyyy");

	AccesoDB con = new AccesoDB();
	StringBuffer strConsulta = new StringBuffer();
	StringBuffer strTabla = new StringBuffer();
	StringBuffer strCampo = new StringBuffer();
	try {
	    con.conexionDB();%>

<%	String nombreArchivoClabe = "";
	String nombreArchivoSpeua = "";
	String nombreArchivoExcel = "";
	
	StringBuffer archivoClabe = generaArchivos("CLABE", "", tipoArchivo, request, con);
	StringBuffer archivoSpeua = new StringBuffer();
	StringBuffer archivoExcel = generaArchivos("CLABE", "EXCEL", tipoArchivo, request, con);
	
	if(!"C".equals(tipoArchivo))
		archivoSpeua = generaArchivos("SPEUA", "", tipoArchivo, request, con);
	CreaArchivo archivo = new CreaArchivo();%>

<%
						if(generar_archivo_excel.equals("si")){
								
							nombreArchivoExcel	=	archivo.nombreArchivo()+".xls";
							ComunesXLS xlsDoc	= new ComunesXLS(strDirectorioTemp+nombreArchivoExcel,"CUENTAS CLABE");
							
							/*java.util.Date fecha = new java.util.Date();
							String fechaFormateada = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fecha);
							
							xlsDoc.creaFila();
							xlsDoc.setCelda("REPORTE DE CUENTAS POR ESTATUS", "formasb", ComunesXLS.CENTER, 8);
							xlsDoc.setCelda("FECHA: " + fechaFormateada, "formasb", ComunesXLS.RIGHT, 8);*/
							
							xlsDoc.setCelda("CUENTAS CLABE POR VALIDAR", "titulo", ComunesXLS.CENTER, 6);
							xlsDoc.setTabla(6);
							xlsDoc.setCelda("NÚMERO", "celda01", ComunesXLS.CENTER, 1);
							xlsDoc.setCelda("RFC", "celda01", ComunesXLS.CENTER, 1);
							xlsDoc.setCelda("TITULAR", "celda01", ComunesXLS.CENTER, 1);
							xlsDoc.setCelda("BANCO", "celda01", ComunesXLS.CENTER, 1);
							xlsDoc.setCelda("CLABE", "celda01", ComunesXLS.CENTER, 1);
							xlsDoc.setCelda("CAP.EMPRESARIAL", "celda01", ComunesXLS.CENTER, 1);
							
							String[] contenido = archivoExcel.toString().split("@");
							
							for(int i = 0; i < contenido.length; ++ i){
							
								String[] contenido_final = new String[20];
								String cadena = new String(contenido[i].replace('|','@'));
								contenido_final =  cadena.split("@");
								
								if(contenido_final.length >= 6){ //Si tiene como minimo 6 registros que requieren las 6 colunmas del archivo

									xlsDoc.setCelda(contenido_final[0], "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(contenido_final[1], "formas", ComunesXLS.LEFT, 1);	
									xlsDoc.setCelda(contenido_final[2], "formas", ComunesXLS.LEFT, 1);	
									xlsDoc.setCelda(contenido_final[3], "formas", ComunesXLS.CENTER, 1);	
									xlsDoc.setCelda(contenido_final[4], "formas", ComunesXLS.LEFT, 1);	
									xlsDoc.setCelda(contenido_final[5], "formas", ComunesXLS.LEFT, 1);
								}
							}
							
							xlsDoc.cierraTabla();
							xlsDoc.cierraXLS();
}
					
						if (archivo.make(archivoClabe.toString(), strDirectorioTemp, ".txt")){
							nombreArchivoClabe = archivo.nombre; 
						}					
					else {
								//out.println("Error en la generacion del archivo Clabe"); 
						  } 
						if (!"C".equals(tipoArchivo) && archivo.make(archivoSpeua.toString(), strDirectorioTemp, ".txt")){
							nombreArchivoSpeua = archivo.nombre; 

								//<a href="<%out.print(strDirecVirtualTemp+nombreArchivoSpeua);"  onmouseout="window.status=''; return true;" onmouseover ="window.status='Generar Archivo SPEUA'; return true;">Generar Archivo SPEUA</a>
							
						} else if (!"C".equals(tipoArchivo)) {
							//out.println("Error en la generacion del archivo Speua");
						}
						JSONObject jsonObj			= new JSONObject();
						jsonObj.put("success", new Boolean(true));
						jsonObj.put("urlArchivo1", strDirecVirtualTemp+nombreArchivoExcel);
						jsonObj.put("urlArchivo2",strDirecVirtualTemp+nombreArchivoClabe);
						infoRegresar = jsonObj.toString();
						

} catch (Exception e) {	
			JSONObject jsonObj			= new JSONObject();
			jsonObj.put("success", new Boolean(false));
			infoRegresar = jsonObj.toString(); e.printStackTrace();
} finally{
		if(con.hayConexionAbierta()) 
			con.cierraConexionDB();
}
%>
<%=infoRegresar%>
