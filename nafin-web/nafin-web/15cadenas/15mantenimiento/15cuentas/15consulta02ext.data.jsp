<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.descuento.BusquedaAvanzadaI,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		com.netro.descuento.ConsCuentasCA,
		javax.naming.Context"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String accion 			= (request.getParameter("accion")		== null)?"":request.getParameter("accion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("accion = <"+accion+">");

if (        accion.equals("valoresIniciales") )	{
	
	JSONObject	resultado	= new JSONObject();
 
	// Consultar Catalogo de Moneda
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	
	resultado.put("catalogoMoneda",cat.getJSONArray());
	
	// Enviar resultado de la operacion
	resultado.put("success", new Boolean(true));
	infoRegresar = resultado.toString();

} else if ( accion.equals("CatalogoTipoAfiliado")	){

	JSONObject	resultado	= new JSONObject();
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	
	registro.put("clave",			"P");
	registro.put("descripcion",	"Proveedor");
	
	registros.add(registro); 
	
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
	
	/*
	 Nota: no se pueden agregar cuentas a distribuidores,
	 ya que el tipo de pyme sería "D" por lo que este combo no
	 podría ser seleccionado
	*/
	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("CatalogoProducto")		){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_producto_nafin"	);
	cat.setCampoDescripcion("ic_nombre"		);
	cat.setTabla("comcat_producto_nafin"	);
	cat.setCondicionNotIn("2", "String"		);	// <--- Este valor no es necesario, se pone por "compatibilidad"
	cat.setCondicionIn("1", 	"String"		);
	cat.setOrden("ic_nombre"					);
	
	/*
		Nota: cuando el tipo de afiliado es "D" (Distribuidor) se utiliza el
		producto "4", pero para admin epo esta opcion no se utiliza.
	*/
	infoRegresar = cat.getJSONElementos();

} else if (	accion.equals("CatalogoMoneda")			){
	
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	
	infoRegresar = cat.getJSONElementos();
 	
} else if (	accion.equals("CatalogoBanco")			){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_bancos_tef");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_bancos_tef");
	cat.setOrden("cd_descripcion");
	
	infoRegresar = cat.getJSONElementos();

} else if ( accion.equals("BusquedaProveedor")		){
	
	JSONObject	resultado					= new JSONObject();
	boolean		success						= true;
	
	String 		numeroNafinElectronico 	= request.getParameter("numeroNafinElectronico") 	== null?"":request.getParameter("numeroNafinElectronico");
	String 		tipoAfiliado 				= request.getParameter("tipoAfiliado") 				== null?"":request.getParameter("tipoAfiliado");
	String 		producto 					= request.getParameter("producto") 						== null?"":request.getParameter("producto");
		
	// Obtener instancia de EJB de Afiliacion
	Afiliacion afiliacion = null;
	try {
				
		afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
				
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Afiliación";
		log.error(msg);
		e.printStackTrace();
		
		success	= false;
		resultado.put( "msg", msg);
		
	}
	
 	if(success){	
 		
		// Realizar busqueda del afiliado	
		JSONObject 	busqueda 				= JSONObject.fromObject( afiliacion.buscaAfiliado(numeroNafinElectronico) );
		String 		clave 					= (String) busqueda.get("clave");
		boolean 		afialidoEncontrado	= true;
		if("".equals(clave)){ 
			String nombreEpo		= strNombre;
			afialidoEncontrado 	= false;
			resultado.put("msg", 			"El No. Nafin Electrónico no está registrado");
		}
		resultado.put("encontrado", 			new Boolean(afialidoEncontrado)		);
		
		/*
		// Validar que la clave de afiliado proporcionada pertenezca a un proveedor
		boolean esProveedor					= false;
		if(afialidoEncontrado){
			String 	clavePyme 				= clave;
			// Buscar afiliado
			String 	tipoCliente				= afiliacion.getTipoCliente(clavePyme);
			esProveedor							= tipoAfiliado.equals(tipoCliente)?true:false;
			if(!esProveedor){
				resultado.put("msg", 		"La clave proporcionada no corresponde con el tipo de afiliado seleccionado");
				/ *
				busqueda.put("nombre", 		""	);
				busqueda.put("rfc",			"" );
				busqueda.put("clave",		"" );
				* /
			}
		}
		resultado.put("esProveedor", 			new Boolean(esProveedor)				);
		
		// Validar que la pyme proporcionada este parametrizada con la EPO
		boolean pymeParametrizada 			= false;
		if(esProveedor){
			String 	clavePyme 				= clave;
			String 	claveEPO 				= iNoCliente;
			String 	claveProducto			= producto; // Producto Descuento Electronico
			pymeParametrizada 				= afiliacion.pymeParametrizadaConEPO(clavePyme,claveEPO,claveProducto);
			
			if(!pymeParametrizada){
				resultado.put("msg", 		"La Pyme no está parametrizada para esta EPO, por favor verifique");
				/ *
				busqueda.put("nombre", 		""	);
				busqueda.put("rfc",			"" );
				busqueda.put("clave",		"" );
				* /
			}
		}
		resultado.put("pymeParametrizada",	new Boolean(pymeParametrizada)		);
		*/
		
		resultado.put("nombre", 			busqueda.get("nombre")	);
		resultado.put("rfc",					busqueda.get("rfc")		);
		resultado.put("clave",				busqueda.get("clave")	);
		
	}
	
	resultado.put("success", 				new Boolean(success)		);
	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("BusquedaAvanzada")		){
		
	String 		claveEpo 			= iNoCliente;
	String 		rfcPyme 				= request.getParameter("rfcPyme")		== null?"":request.getParameter("rfcPyme");
	String 		nombrePyme 			= request.getParameter("nombrePyme")	== null?"":request.getParameter("nombrePyme");
	String 		numeroPyme			= request.getParameter("numeroPyme")	== null?"":request.getParameter("numeroPyme");

	String 		ic_producto_nafin	= "";
	String 		ic_pyme				= "";
	String      pantalla				= "";
	String  		ic_if					= "";
	String    	ret_num_pyme		= "";
	
	JSONArray	registros			= BusquedaAvanzadaI.getProveedores(
												ic_producto_nafin,
												claveEpo,
												numeroPyme,
												rfcPyme,
												nombrePyme,
												ic_pyme,
												pantalla,
												ic_if,
												ret_num_pyme
											);
	JSONObject	resultado			= new JSONObject();
	
	resultado.put("success", 		new Boolean("true") 					);
	resultado.put("total",	 		String.valueOf(registros.size()) );
	resultado.put("registros",		registros 								);
	
	infoRegresar = resultado.toString();

} else if (	accion.equals("ConsultaCuentas")		){
 
	// Leer parametros
	String 	claveProducto				= (request.getParameter("claveProducto") 				== null) ? "" : request.getParameter("claveProducto");
	String 	numeroNafinElectronico  = (request.getParameter("numeroNafinElectronico") 	== null) ? "" : request.getParameter("numeroNafinElectronico");
	String 	rfcProveedor 				= (request.getParameter("rfcProveedor") 				== null) ? "" : request.getParameter("rfcProveedor");
	String 	fechaRegistroDe 			= (request.getParameter("fechaRegistroDe") 			== null) ? "" : request.getParameter("fechaRegistroDe");
	String 	fechaRegistroA 			= (request.getParameter("fechaRegistroA") 			== null) ? "" : request.getParameter("fechaRegistroA");
	String 	cuentasSinValidar 		= (request.getParameter("cuentasSinValidar") 		== null) ? "" : request.getParameter("cuentasSinValidar");
	String 	claveTipoAfiliado 		= (request.getParameter("claveTipoAfiliado") 		== null) ? "" : request.getParameter("claveTipoAfiliado");
	String 	claveEPO						= iNoCliente;
 
	// Validar perfil del usuario que realiza la consulta
	String tipoUsuario = strTipoUsuario;
	if(!"EPO".equals(tipoUsuario)){
		log.error("ConsultaCuentas");
		throw new AppException("No tiene permiso para realizar la consulta");
	}
		
	// Crear instancia del paginador
	CuentasClabeSwiftCA paginador = new CuentasClabeSwiftCA();
	paginador.setNo_producto(				claveProducto				); // no_producto
	paginador.setNo_nafin(					numeroNafinElectronico	); // no_nafin
	paginador.setFecha_registro_de(		fechaRegistroDe			); // fecha_registro_de
	paginador.setFecha_registro_a(		fechaRegistroA				); // fecha_registro_a
	paginador.setCuentas_sin_validar(	cuentasSinValidar			); // cuentas_sin_validar
	paginador.setCmb_tipo_afiliado(		claveTipoAfiliado			); // cmb_tipo_afiliado
	paginador.setNo_epo(						claveEPO						); // no_epo
	paginador.setRfc(							rfcProveedor				); // rfc
 
	// Crear instancia del query helper y el objeto de resultado
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	JSONObject 				resultado	= new JSONObject();
	
	// Leer posicion de los registros
	int start = 0;
	int limit = 0;
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		log.error("ConsultaCuentas");
		e.printStackTrace();
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	// Realizar consulta
	try {
		
		if ("NuevaConsulta".equals(operacion)) {	//Nueva consulta
			queryHelper.executePKQuery(request);   //Obtiene las llaves primarias con base a los parametros de consulta
		}
		String  consultar = queryHelper.getJSONPageResultSet(request,start,limit);	
				
		resultado 			= JSONObject.fromObject(consultar);
				
	} catch(Exception e) {
		log.error("ConsultaCuentas");
		e.printStackTrace();
		throw new AppException("Error en la paginacion", e);
	}	
							
	infoRegresar = resultado.toString();	

} else if (	accion.equals("GeneraArchivoPaginaPDF")		){
	
	// Leer parametros
	String 	claveProducto				= (request.getParameter("claveProducto") 				== null) ? "" : request.getParameter("claveProducto");
	String 	numeroNafinElectronico  = (request.getParameter("numeroNafinElectronico") 	== null) ? "" : request.getParameter("numeroNafinElectronico");
	String 	rfcProveedor 				= (request.getParameter("rfcProveedor") 				== null) ? "" : request.getParameter("rfcProveedor");
	String 	fechaRegistroDe 			= (request.getParameter("fechaRegistroDe") 			== null) ? "" : request.getParameter("fechaRegistroDe");
	String 	fechaRegistroA 			= (request.getParameter("fechaRegistroA") 			== null) ? "" : request.getParameter("fechaRegistroA");
	String 	cuentasSinValidar 		= (request.getParameter("cuentasSinValidar") 		== null) ? "" : request.getParameter("cuentasSinValidar");
	String 	claveTipoAfiliado 		= (request.getParameter("claveTipoAfiliado") 		== null) ? "" : request.getParameter("claveTipoAfiliado");
	String 	claveEPO						= iNoCliente;
	
	// Validar perfil del usuario que realiza la consulta
	String tipoUsuario = strTipoUsuario;
	if(!"EPO".equals(tipoUsuario)){
		log.error("GeneraArchivoPaginaPDF");
		throw new AppException("No tiene permiso para realizar la consulta");
	}
	
	// Crear instancia del paginador	
	CuentasClabeSwiftCA paginador = new CuentasClabeSwiftCA();
	paginador.setNo_producto(				claveProducto				); // no_producto
	paginador.setNo_nafin(					numeroNafinElectronico	); // no_nafin
	paginador.setFecha_registro_de(		fechaRegistroDe			); // fecha_registro_de
	paginador.setFecha_registro_a(		fechaRegistroA				); // fecha_registro_a
	paginador.setCuentas_sin_validar(	cuentasSinValidar			); // cuentas_sin_validar
	paginador.setCmb_tipo_afiliado(		claveTipoAfiliado			); // cmb_tipo_afiliado
	paginador.setNo_epo(						claveEPO						); // no_epo
	paginador.setRfc(							rfcProveedor				); // rfc
	
	// Crear instancia del query helper y el objeto de resultado
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	JSONObject 				resultado	= new JSONObject();
	
	// Leer posicion de los registros
	int start = 0;
	int limit = 0;
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		log.error("GeneraArchivoPaginaPDF");
		e.printStackTrace();
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	// Generar archivo PDF
	try {
																								
		String nombreArchivo = queryHelper.getCreatePageCustomFile( request, start, limit, strDirectorioTemp, "PDF");
		
		resultado.put("success", 		new Boolean(true));
		resultado.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo);
		
	} catch(Exception e) {
		log.error("GeneraArchivoPaginaPDF");
		e.printStackTrace();
		throw new AppException("Error al generar el archivo PDF", e);
	}
	
	infoRegresar = resultado.toString();

} else if (	accion.equals("GeneraArchivoCSV")	){
	
	// Leer parametros
	String 	claveProducto				= (request.getParameter("claveProducto") 				== null) ? "" : request.getParameter("claveProducto");
	String 	numeroNafinElectronico  = (request.getParameter("numeroNafinElectronico") 	== null) ? "" : request.getParameter("numeroNafinElectronico");
	String 	rfcProveedor 				= (request.getParameter("rfcProveedor") 				== null) ? "" : request.getParameter("rfcProveedor");
	String 	fechaRegistroDe 			= (request.getParameter("fechaRegistroDe") 			== null) ? "" : request.getParameter("fechaRegistroDe");
	String 	fechaRegistroA 			= (request.getParameter("fechaRegistroA") 			== null) ? "" : request.getParameter("fechaRegistroA");
	String 	cuentasSinValidar 		= (request.getParameter("cuentasSinValidar") 		== null) ? "" : request.getParameter("cuentasSinValidar");
	String 	claveTipoAfiliado 		= (request.getParameter("claveTipoAfiliado") 		== null) ? "" : request.getParameter("claveTipoAfiliado");
	String 	claveEPO						= iNoCliente;
	
	// Validar perfil del usuario que realiza la consulta
	String tipoUsuario = strTipoUsuario;
	if(!"EPO".equals(tipoUsuario)){
		log.error("GeneraArchivoCSV");
		throw new AppException("No tiene permiso para realizar la consulta");
	}
	
	// Crear instancia del paginador	
	CuentasClabeSwiftCA paginador = new CuentasClabeSwiftCA();
	paginador.setNo_producto(				claveProducto				); // no_producto
	paginador.setNo_nafin(					numeroNafinElectronico	); // no_nafin
	paginador.setFecha_registro_de(		fechaRegistroDe			); // fecha_registro_de
	paginador.setFecha_registro_a(		fechaRegistroA				); // fecha_registro_a
	paginador.setCuentas_sin_validar(	cuentasSinValidar			); // cuentas_sin_validar
	paginador.setCmb_tipo_afiliado(		claveTipoAfiliado			); // cmb_tipo_afiliado
	paginador.setNo_epo(						claveEPO						); // no_epo
	paginador.setRfc(							rfcProveedor				); // rfc
	
	// Crear instancia del query helper y el objeto de resultado
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	JSONObject 				resultado	= new JSONObject();
	
	// Generar Archivo CSV
	try {
																								
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
		
		resultado.put("success", 		new Boolean(true));
		resultado.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo);
		
	} catch(Exception e) {
		log.error("GeneraArchivoCSV");
		e.printStackTrace();
		throw new AppException("Error al Generar el Archivo CSV", e);
	}
	
	infoRegresar = resultado.toString();
 
} else if (	accion.equals("ValidarCuenta")		){
	
	JSONObject	resultado 	= new JSONObject();
	boolean		success		= true;
	
	String 		claveCuentaModificarEstatus = request.getParameter("claveCuentaModificarEstatus");
	String 		nombreEPO 		= strNombre;
	String 		loginUsuario	= iNoUsuario;
	
	claveCuentaModificarEstatus = claveCuentaModificarEstatus == null || claveCuentaModificarEstatus.trim().equals("")?"":claveCuentaModificarEstatus;
	
	// Obtener instancia de EJB de Dispersion
	Dispersion  dispersion 		= null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		success		= false;
		resultado.put( "msg", msg);
		
	}
	
	if( "".equals(claveCuentaModificarEstatus) ){
		
		String msg	= "Ocurrió un error al leer la clave de la cuenta";
		log.error(msg);
		
		success		= false;
		resultado.put( "msg", msg);
		
	}
	
 	if(success){
 		
 		HashMap parametrosAdicionales = new HashMap();
		parametrosAdicionales.put("DESCRIPCION_VALIDACION",		"CUENTA CORRECTA\n"+nombreEPO	);
		parametrosAdicionales.put("PANTALLA_ORIGEN", 				"CONSCTAS" 							);
		parametrosAdicionales.put("IC_USUARIO", 						loginUsuario						);
		parametrosAdicionales.put("STR_LOGIN",strLogin);
		System.out.println("\n\n\n\n\n"+strLogin);
		dispersion.validarCuenta(claveCuentaModificarEstatus,parametrosAdicionales);
 
 	}
 	resultado.put("success", 		new Boolean(success));
 	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("InvalidarCuenta")		){
	
	JSONObject	resultado 	= new JSONObject();
	boolean		success		= true;
	
	String 		claveCuentaModificarEstatus = request.getParameter("claveCuentaModificarEstatus");
	String 		nombreEPO 		= strNombre;
	String 		loginUsuario	= iNoUsuario;
 
	claveCuentaModificarEstatus = claveCuentaModificarEstatus == null || claveCuentaModificarEstatus.trim().equals("")?"":claveCuentaModificarEstatus;
	
	// Obtener instancia de EJB de Dispersion
	Dispersion  dispersion 		= null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		success		= false;
		resultado.put( "msg", msg);
		
	}
	
	if( "".equals(claveCuentaModificarEstatus) ){
		
		String msg	= "Ocurrió un error al leer la clave de la cuenta";
		log.error(msg);
		
		success		= false;
		resultado.put( "msg", msg);
		
	}
	
 	if(success){
 		
 		HashMap parametrosAdicionales = new HashMap();
		parametrosAdicionales.put("DESCRIPCION_SIN_VALIDACION",	"SIN VALIDAR\n"+nombreEPO	);
		parametrosAdicionales.put("PANTALLA_ORIGEN", 				"CONSCTAS" 						);
		parametrosAdicionales.put("IC_USUARIO", 						loginUsuario					);
			
		dispersion.invalidarCuenta(claveCuentaModificarEstatus,parametrosAdicionales);
 
 	}
 	resultado.put("success", 		new Boolean(success));
 	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("EliminarCuenta")	&&	operacion.equals("VerificarUsoCuenta") ){
	
	// Inicializar respuesta
	JSONObject	resultado 		= new JSONObject();
	boolean		success			= true;
	
	// Leer parametros
	String 		claveCuenta 	= request.getParameter("claveCuenta");
	String 		estatusCecoban = request.getParameter("estatusCecoban");
	String 		rowIndex			= request.getParameter("rowIndex");
	
	claveCuenta			= claveCuenta 		== null?"":claveCuenta;
	estatusCecoban		= estatusCecoban	== null?"":estatusCecoban;
	rowIndex				= rowIndex 			== null?"":rowIndex;
	
	// Obtener instancia del EJB de Dispersion
	Dispersion  dispersion 		= null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		log.error("EliminarCuenta.VerificarUsoCuenta");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
 
	}
	
	try {
		
		// Revisar uso de la cuenta
		boolean hayOperacionesAsociadas = false;
		if( dispersion.getExisteCuenta(claveCuenta) > 0 ){	
			hayOperacionesAsociadas = true;
			resultado.put( "msg", "Esta cuenta no se puede eliminar ya que tiene operaciones asociadas.");
		}
		resultado.put("hayOperacionesAsociadas", 	new Boolean(hayOperacionesAsociadas));
		
		// Determinar el Estado Siguiente
		String 		estadoSiguiente 	= null;
		estadoSiguiente 	= estatusCecoban.equals("99")?"VALIDAR_USUARIO":"ELIMINAR_CUENTA";
		estadoSiguiente 	= hayOperacionesAsociadas?"FIN":estadoSiguiente;
		resultado.put("estadoSiguiente", estadoSiguiente);
		
	}catch(Exception e){
		
		log.error("EliminarCuenta.VerificarUsoCuenta(Exception)");
		log.error("claveCuenta    = <" + claveCuenta    + ">");
		log.error("estatusCecoban = <" + estatusCecoban + ">");
		log.error("rowIndex       = <" + rowIndex       + ">");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al validar uso de la cuenta.");
		
	}
	
	// Enviar respuesta
 	resultado.put("rowIndex", 						rowIndex);
 	resultado.put("claveCuenta",					claveCuenta);
 	resultado.put("success", 						new Boolean(success));
 	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("EliminarCuenta")	&&	operacion.equals("ValidarUsuario") 		){
	
	// Inicializar respuesta
	JSONObject	resultado 		= new JSONObject();
	boolean		success			= true;
	
	// Leer parametros
	String 		claveCuenta 	= request.getParameter("claveCuenta");
	String 		rowIndex			= request.getParameter("rowIndex");
	String 		claveUsuario 	= request.getParameter("claveUsuario");
	String      contrasena		= request.getParameter("contrasena");
	String 		loginUsuario	= strLogin;
	
	claveCuenta			= claveCuenta 		== null?"":claveCuenta;
	rowIndex				= rowIndex 			== null?"":rowIndex;
	loginUsuario		= loginUsuario 	== null?"":loginUsuario;
	contrasena			= contrasena 		== null?"":contrasena;
	
	// Obtener instancia del EJB de Dispersion
	Dispersion  dispersion 		= null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		log.error("EliminarCuenta.ValidarUsuario");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
 
	}
 
	boolean esUsuarioCorrecto = false;
	try {
		
		// Verificar usuario
		if( !loginUsuario.equals(claveUsuario) ){
			
			resultado.put( "msg", "La Clave de Usuario proporcionada no corresponde con la que se ha firmado.");
			esUsuarioCorrecto = false;
			
		} else {
			
			esUsuarioCorrecto = dispersion.esUsuarioCorrecto(loginUsuario, claveUsuario, contrasena);
			
			// Si el usuario es incorrecto la siguiente seccion no se ejecuta porque se lanza una excepcion
			// solo se agrega el mensaje por compatibilidad con la version anterior
			if(!esUsuarioCorrecto){
				resultado.put( "msg", "El login y/o password del usuario es incorrecto, operación cancelada.");
			}
			
		}
 
	} catch (NafinException ne) {
		
		esUsuarioCorrecto = false;
		resultado.put( "msg", ne.getMessage() );
		
	} catch(Exception e) {
		
		log.error("EliminarCuenta.ValidarUsuario(Exception)");
		log.error("claveCuenta        = <" + claveCuenta         + ">");
		log.error("rowIndex           = <" + rowIndex            + ">");
		log.error("claveUsuario       = <" + claveUsuario        + ">");
		log.error("contrasena.length  = <" + contrasena.length() + ">");
		log.error("loginUsuario       = <" + loginUsuario        + ">");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al validar uso de la cuenta.");
		
	} finally {
 
		resultado.put("esUsuarioCorrecto", new Boolean(esUsuarioCorrecto));
		
	}
	
	// Determinar el Estado Siguiente
	String 		estadoSiguiente 	= esUsuarioCorrecto?"ELIMINAR_CUENTA":"FIN";
	resultado.put("estadoSiguiente", estadoSiguiente);
	
	// Enviar respuesta
 	resultado.put("rowIndex", 						rowIndex);
 	resultado.put("claveCuenta",					claveCuenta);
 	resultado.put("success", 						new Boolean(success));
 	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("EliminarCuenta")	&&	operacion.equals("Eliminar") 				){
	
	// Inicializar respuesta
	JSONObject	resultado 		= new JSONObject();
	boolean		success			= true;
	
	// Leer parametros
	String 		claveCuenta 	= request.getParameter("claveCuenta");
	String 		rowIndex			= request.getParameter("rowIndex");
	String 		nombreEPO 		= strNombre;
	String  		loginUsuario	= iNoUsuario;
	
	claveCuenta			= claveCuenta 		== null?"":claveCuenta;
	rowIndex				= rowIndex 			== null?"":rowIndex;

	// Obtener instancia del EJB de Dispersion
	Dispersion  dispersion 		= null;
	try {
				
		
		
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		log.error("EliminarCuenta.Eliminar");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
 
	}
 
	// Eliminar cuenta
	boolean exitoEliminar = false;
	try {
		
		HashMap parametrosAdicionales = new HashMap();
		parametrosAdicionales.put("DESCRIPCION_ELIMINAR",	"Registro Eliminado\n"+nombreEPO	);
		dispersion.eliminarCuenta(claveCuenta, loginUsuario, parametrosAdicionales);
		exitoEliminar		= true;
		
	} catch (Exception e) {
		
		log.error("EliminarCuenta.Eliminar(Exception)");
		log.error("claveCuenta  = <" + claveCuenta   + ">");
		log.error("rowIndex     = <" + rowIndex      + ">");
		log.error("nombreEPO    = <" + nombreEPO     + ">");
		log.error("loginUsuario = <" + loginUsuario  + ">");
		e.printStackTrace();
		exitoEliminar		= false;
		
	}
	resultado.put("exitoEliminar", new Boolean(exitoEliminar));
	if(exitoEliminar){
		resultado.put( "msg","Registro eliminado satisfactoriamente");
	}else{
		resultado.put( "msg","Error al eliminar la cuenta");
	}
 
	// Determinar el Estado Siguiente
	String 		estadoSiguiente 	= exitoEliminar?"ACTUALIZAR_GRID":"FIN";
	resultado.put("estadoSiguiente", estadoSiguiente);
 
	// Enviar respuesta
 	resultado.put("rowIndex",		rowIndex);
 	resultado.put("claveCuenta",	claveCuenta);
 	resultado.put("success",		new Boolean(success));
 	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("EliminarCuenta")	&&	operacion.equals("Fin") 					){
	
	JSONObject	resultado 		= new JSONObject();
	boolean		success			= true;
	
	// Leer parametros
	String 		claveCuenta 	= request.getParameter("claveCuenta");
	String 		rowIndex			= request.getParameter("rowIndex");
	
	claveCuenta			= claveCuenta 		== null?"":claveCuenta;
	rowIndex				= rowIndex 			== null?"":rowIndex;
	
	// Determinar el Estado Siguiente
	String 		estadoSiguiente 	= "FIN";
	resultado.put("estadoSiguiente", estadoSiguiente);
	
	// Enviar respuesta
 	resultado.put("rowIndex",		rowIndex);
 	resultado.put("claveCuenta",	claveCuenta);
 	resultado.put("success",		new Boolean(success));
 
	infoRegresar = resultado.toString();
 
} else if (	accion.equals("ModificarCuenta")		){
	
	JSONObject	resultado 		= new JSONObject();
   boolean 		success			= true;
   
	String 		nombreEPO 		= strNombre;
	String 		loginUsuario 	= iNoUsuario;
 
	String plazaSwift 				= (request.getParameter("plazaSwiftMC")					== null)?"":request.getParameter("plazaSwiftMC");
	String sucursalSwift 			= (request.getParameter("sucursalSwiftMC")				== null)?"":request.getParameter("sucursalSwiftMC");
	String claveCuenta 				= (request.getParameter("claveCuentaMC")					== null)?"":request.getParameter("claveCuentaMC"); 
	String numeroNafinElectronico = (request.getParameter("numeroNafinElectronicoMC")	== null)?"":request.getParameter("numeroNafinElectronicoMC");
	String claveProducto 			= (request.getParameter("claveProductoMC")				== null)?"":request.getParameter("claveProductoMC");
	String claveMoneda 				= (request.getParameter("claveMonedaMC")					== null)?"":request.getParameter("claveMonedaMC");
	String claveBanco	 				= (request.getParameter("claveBancoMC")					== null)?"":request.getParameter("claveBancoMC");
	String cuentaClabe 				= (request.getParameter("cuentaClabeMC")					== null)?"":request.getParameter("cuentaClabeMC");
	String cuentaSwift 				= (request.getParameter("cuentaSwiftMC")					== null)?"":request.getParameter("cuentaSwiftMC");
	String tipoCuenta 				= (request.getParameter("tipoCuentaMC")					== null)?"":request.getParameter("tipoCuentaMC");
 
	String cuenta						= null;
	
	// Alguna validaciones de seguridad adicionales
	if( claveCuenta.trim().equals("") ){
		throw new AppException("Error en los parametros recibidos");
	}else if( !"54".equals(claveMoneda) && !"1".equals(claveMoneda) ){
		throw new AppException("Error en los parametros recibidos");
	}
	
	// Obtener instancia del EJB de Dispersion
	Dispersion  dispersion 		= null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		log.error("ModificarCuenta");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
 
	}
	
	try {
			
		// Si es cuenta swift agregar los campos plaza_swift y sucursal_swift para ser actualizados
		boolean esCuentaSwift 			= ("54".equals(claveMoneda) && "50".equals(tipoCuenta))?true:false;
		
		// Asignar la cuenta correspondiente 
		if(esCuentaSwift){
			cuenta = cuentaSwift;
		}else{
			cuenta = cuentaClabe;
		}
		
		// Preparar parametros adicionales
		HashMap parametrosAdicionales = new HashMap();				
		parametrosAdicionales.put("DESCRIPCION_MODIFICACION",		"REGISTRO MODIFICADO\n"+nombreEPO);		
		parametrosAdicionales.put("PANTALLA_ORIGEN",					"CONSCTAS");
		if(esCuentaSwift){
			parametrosAdicionales.put("PLAZA_SWIFT",		plazaSwift		);
			parametrosAdicionales.put("SUCURSAL_SWIFT",	sucursalSwift	);
		}
				
		// Modificar Cuenta
		dispersion.ovactualizarCuenta(
			claveCuenta, 
			numeroNafinElectronico, // no_nafin, 
			claveProducto, 
			claveMoneda, 
			claveBanco, 
			cuenta, 
			loginUsuario, 
			tipoCuenta, 
			parametrosAdicionales
		);
				
	} catch(NafinException neError) {
		
		log.error("ModificarCuenta(NafinException)");
		log.error("plazaSwift             = <" + plazaSwift             + ">");
		log.error("sucursalSwift          = <" + sucursalSwift          + ">");
		log.error("claveCuenta            = <" + claveCuenta            + ">");
		log.error("numeroNafinElectronico = <" + numeroNafinElectronico + ">");
		log.error("claveProducto          = <" + claveProducto          + ">");
		log.error("claveMoneda            = <" + claveMoneda            + ">");
		log.error("claveBanco             = <" + claveBanco             + ">");
		log.error("cuentaClabe            = <" + cuentaClabe            + ">");
		log.error("cuentaSwift            = <" + cuentaSwift            + ">");
		log.error("tipoCuenta             = <" + tipoCuenta             + ">");
		neError.printStackTrace();
		
		success = false;
		resultado.put("msg", "Error al modificar la Cuenta\n" + neError.getMsgError());
		
	} catch(Exception e) {
	
		log.error("ModificarCuenta(Exception)");
		log.error("plazaSwift             = <" + plazaSwift             + ">");
		log.error("sucursalSwift          = <" + sucursalSwift          + ">");
		log.error("claveCuenta            = <" + claveCuenta            + ">");
		log.error("numeroNafinElectronico = <" + numeroNafinElectronico + ">");
		log.error("claveProducto          = <" + claveProducto          + ">");
		log.error("claveMoneda            = <" + claveMoneda            + ">");
		log.error("claveBanco             = <" + claveBanco             + ">");
		log.error("cuentaClabe            = <" + cuentaClabe            + ">");
		log.error("cuentaSwift            = <" + cuentaSwift            + ">");
		log.error("tipoCuenta             = <" + tipoCuenta             + ">");
		e.printStackTrace();
		
		throw new AppException("Ocurrió un error al modificar la cuenta");
		
	}
 
	resultado.put("success",		new Boolean(success));
	infoRegresar = resultado.toString();
	
}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>
