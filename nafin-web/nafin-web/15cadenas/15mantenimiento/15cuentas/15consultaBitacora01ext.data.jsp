<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,
	com.netro.descuento.ConsBitacoraCuentasClabe,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%@ include file="/appComun.jspf" %>
<%!
private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
//Variables para la consulta
String informacion   = request.getParameter("informacion")  == null ? "" : (String)request.getParameter("informacion");
String operacion     = request.getParameter("operacion")    == null ? "" : (String)request.getParameter("operacion");
String fecha_inicio  = request.getParameter("fecha_inicio") == null ? "" : (String)request.getParameter("fecha_inicio");
String fecha_final   = request.getParameter("fecha_final")  == null ? "" : (String)request.getParameter("fecha_final");
//Variables para la descarga del archivo
String ic_descarga   = request.getParameter("ic_descarga")  == null ? "" : (String)request.getParameter("ic_descarga");
String extension     = request.getParameter("extension")    == null ? "" : (String)request.getParameter("extension");
//Variables globales
String mensaje       = "";
String consulta      = "";
String nombreArchivo = "";
String infoRegresar  = "";
JSONObject resultado = new JSONObject();
boolean success      = true;
//Paginación
int start = 0;
int limit = 0;

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("Consultar_Datos")){

	/***** Le doy formato a las fechas [dd/mm/aaaa] *****/
	if(!fecha_inicio.equals("")){
		fecha_inicio = fecha_inicio.substring(8,10) + "/" + fecha_inicio.substring(5,7) + "/" + fecha_inicio.substring(0,4);
	}
	if(!fecha_final.equals("")){
		fecha_final = fecha_final.substring(8,10) + "/" + fecha_final.substring(5,7) + "/" + fecha_final.substring(0,4);
	}

	ConsBitacoraCuentasClabe paginador = new ConsBitacoraCuentasClabe();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	paginador.setFechaInicio(fecha_inicio);
	paginador.setFechaFinal(fecha_final);

	try{
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e){
		start = 0;
		limit = 15;
		throw new AppException("Error en los parametros start y limit recibidos", e);
	}

	try{
		if (operacion.equals("generar")){ //Nueva consulta
			queryHelper.executePKQuery(request);
		}
		consulta = queryHelper.getJSONPageResultSet(request, start, limit);
	} catch(Exception e){
		throw new AppException(" Error en la paginacion. ", e);
	}

	resultado = JSONObject.fromObject(consulta);
	infoRegresar = resultado.toString();

} else if(informacion.equals("Descargar_Archivos")){

	ConsBitacoraCuentasClabe consBitacora = new ConsBitacoraCuentasClabe();
	consBitacora.setIcBitDescarga(ic_descarga);
	consBitacora.setRutaDestino(strDirectorioTemp);
	consBitacora.setExtension(extension);
	try{
		nombreArchivo = consBitacora.descargaArchivo();
		resultado.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
	} catch(Exception e){
		success = false;
		throw new AppException("Error al generar el archivo.", e);
	}

	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();
}
%>
<%=infoRegresar%>