<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>


<html>

<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
	<%if(esEsquemaExtJS) {%>
		<%@ include file="/01principal/menu.jspf"%>
	<%}%>
<script type="text/javascript" src="15consultaBitacora01ext.js?<%=session.getId()%>"></script>
<script language="JavaScript1.2" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
</head>

<% if(esEsquemaExtJS){%>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%@ include file="/01principal/01epo/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">
				<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
				<div id="areaContenido"><div style="height:230px"></div></div>
			</div>
		</div>
		<%@ include file="/01principal/01epo/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
	</body>
<%} else{%>
	<body>
	<div id='areaContenido' style="margin-left: 3px; margin-top: 3px; text-align: center"></div>
		<form id='formAux' name="formAux" target='_new'></form>
	</body>
<%}%>
</html>