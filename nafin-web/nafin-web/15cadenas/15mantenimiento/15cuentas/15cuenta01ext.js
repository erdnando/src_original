Ext.onReady(function() {
	
	//--------------------------------- VALIDACIONES ------------------------------
	
	function trim(str){
		
		var cadena = str;
		
		// Si la cadena proporcionada es NULL no realizar ninguna operacion
		if (str == null ){
			return str;
		}
		
		// Suprimir los caracteres "vacios" al inicio y al final
		cadena = cadena.replace(/^\s+/, "");
		cadena = cadena.replace(/\s+$/, "");
		return cadena;
		
	}

	function validaDigitoVerificador(contenido){
		
		var ruta 				= document.registro;			
		var car 					= "";
		var resultado 			= "";
		var residuo 			= 0;    
		var mulIndividual 	= 0;
		var digVerificador 	= 0;  
		var contenedor 		= 0;
		var cadena				= "";
		cadena 					= String(contenido);
 
		digVerificador 		= parseInt(cadena.substr(17),10);
		
		for (var i=0; i <= 16; i++){
			car = cadena.substring(i,i+1);
			switch(i){
				case 0: case 3: case 6: case 9: case 12: case 15:		/*3*/
					mulIndividual 	= 	parseInt(car)*3;
					contenedor 		+= mulIndividual;
					break;
				case 1: case 4: case 7: case 10: case 13: case 16:		/*7*/
					mulIndividual 	= 	parseInt(car)*7;
					contenedor 		+= mulIndividual;
					break;
				case 2: case 5: case 8: case 11: case 14:					/*1*/
					mulIndividual 	= 	parseInt(car)*1;
					contenedor 		+= mulIndividual;
					break;
			}
		}
		
		residuo = contenedor % 10;  /*Obtiene el residuo */
		
		if (residuo > 0)  
			contenedor = 10 - residuo;
		else
			contenedor = 0;
		
		if (digVerificador == contenedor)
			return "S"; 
		else
			return "N"; 
		
	}	
	
	function soloAlfaNumericos(field,mensaje,extrachars){
		
	  var InString = field.getValue();
	  if(InString.length==0){ 
	  	  return; 
	  }
		
	  var RefString = "01234567890AaBbCcDdEeFfGgHhIiJjKkLlMmNn��OoPpQqRrSsTtUuVvWwXxYyZz";
	  if(extrachars != null && extrachars != undefined){
		  RefString += extrachars;
	  }
	 
	  for(var Count=0;Count < InString.length;Count++){
	  	  
		 var TempChar = InString.substring(Count,Count+1);
		 if(RefString.indexOf(TempChar,0)==-1){ 
			field.markInvalid(mensaje);
			return;
		 }
		 
	  }
	  
	  return;
	  
	}
	
	function soloNumeros(field,mensaje,clave){
	
		var CadenaValida="0123456789"; /*variable con caracteres validos*/
		
		if( Ext.isEmpty(field.getValue()) ){ return; } // Si est� vac�o salir
		
		var numero = String(field.getValue());
		for(var i=0;i<numero.length;i++){
			if(CadenaValida.indexOf(numero.charAt(i))==-1){
				field.markInvalid(mensaje);
				return false;
			}
		}
		
		if (clave == "1"){
			
			if (numero.length != 11){
				field.markInvalid('La longitud no es v�lida, debe de ser de 11 caracteres');
				return false;
			}
			
		}
		
		if (clave == "40"){
			
			if (numero.length != 18){
				field.markInvalid('La longitud no es v�lida, debe de ser de 18 caracteres');
				return false;
			}
	
			var verificar = validaDigitoVerificador(numero);
			if (verificar == "N"){
				field.markInvalid('La Cuenta Clabe no es correcta (D�gito Verificador)');
				return false;
			}
			
		}
		
		return true;
		
	}
	
	//------------------------------- HANDLERS -------------------------------
	
	var procesaValoresIniciales = function(opts, success, response) {
		
		if ( success == true && Ext.util.JSON.decode(response.responseText).success == true ) {
 
			var resp 		= Ext.util.JSON.decode(response.responseText);
			
			if(resp.permisoAgregarCuentas){

				Ext.getCmp("contenedorPrincipal").enable();
				Ext.getCmp("botonBusquedaAvanzada").enable();
				
				// Cargar valores de los combos
				var catalogoTipoAfialiadoData = Ext.StoreMgr.key('catalogoTipoAfialiadoDataStore');
				catalogoTipoAfialiadoData.load({
					params: { defaultValue: "P" }
				});
				//catalogoTipoAfialiadoData.on('load', 	function(){ Ext.getCmp("cmbTipoAfiliado").setValue("P"); } );
				
				var catalogoProductoData = Ext.StoreMgr.key('catalogoProductoDataStore');
				catalogoProductoData.load({
					params: { defaultValue: "1" }
				});
				//catalogoProductoData.on('load', 			function(){ Ext.getCmp("cmbProducto").setValue("1"); } );
				
				var catalogoEPOData = Ext.StoreMgr.key('catalogoEPODataStore');
				catalogoEPOData.load({
					params: { defaultValue: 0 } // Valor del primer registro
				});
				//catalogoEPOData.on('load',					function( store, records, options){  Ext.getCmp("cmbEPO").setValue(records[0].data['clave']); } );
				
				var catalogoMonedaData = Ext.StoreMgr.key('catalogoMonedaDataStore');
				catalogoMonedaData.load();
				
				var catalogoBancoData = Ext.StoreMgr.key('catalogoBancoDataStore');
				catalogoBancoData.load();
			
			// No hay permiso para agregar las cuentas, bloquear la pantalla
			} else {
 
				var msg = Ext.isEmpty(resp.msg)?"Ocurri� un error inesperado al validar acceso a la pantalla.":resp.msg;	
				Ext.Msg.alert("Aviso", msg );
				
			}
			
		} else {
 
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesaBusquedaProveedor = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			var clavePyme					= Ext.getCmp('clavePyme');
			var nombre						= Ext.getCmp('nombreProveedor');
			var rfc							= Ext.getCmp('rfcProveedor');
			
			clavePyme.setValue(	resp.clave	);
			nombre.setValue(		resp.nombre	); 
			rfc.setValue(			resp.rfc		);
				
			if(!resp.encontrado){ 
				
				var  numeroNafinElectronico = Ext.getCmp('numeroNafinElectronico');
				numeroNafinElectronico.markInvalid(resp.msg);
 
			}else if(!resp.esProveedor){
				
				var  numeroNafinElectronico = Ext.getCmp('numeroNafinElectronico');
				numeroNafinElectronico.markInvalid(resp.msg);
 
			}else if(!resp.pymeParametrizada){
				
				var  numeroNafinElectronico = Ext.getCmp('numeroNafinElectronico');
				numeroNafinElectronico.markInvalid(resp.msg);
 
			}else{
			
				// Consultar cuentas registradas con el proveedor
				var cuentasAgregadasData = Ext.StoreMgr.key("cuentasAgregadasDataStore");
				cuentasAgregadasData.load({
					params: Ext.apply(Ext.getCmp("panelForma").getForm().getValues(),{
						operacion: 	'NuevaConsulta', //Generar datos para la consulta
						start: 		0,
						limit: 		15
					})
				});	
				Ext.getCmp('botonImprimirPDF').disable();
				Ext.getCmp('botonBajarPDF').hide();
			
			}
			
			/*
			// Obtener registro
			var gridFianzas 						= Ext.getCmp('gridFianzas');
			var record 								= gridFianzas.getStore().getAt(resp.rowIndex);
			*/
 
		} else {
			
			Ext.getCmp('clavePyme').setValue(		"" );
			Ext.getCmp('nombreProveedor').setValue("" );
			Ext.getCmp('rfcProveedor').setValue(	"" );
			
			Ext.getCmp('numeroNafinElectronico').markInvalid("Ocurri� un error inesperado al buscar proveedor");
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesaAgregarCuenta = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var respuesta = Ext.util.JSON.decode(response.responseText);
			
			if(!Ext.isEmpty(respuesta.msg)){
				Ext.Msg.alert(
					'Mensaje',
					respuesta.msg,
					function(btn, text){
						
						// Realizar consulta de cuentas 
						if( respuesta.successAgregarCuenta ){
							
							var cuentasAgregadasData = Ext.StoreMgr.key("cuentasAgregadasDataStore");
							cuentasAgregadasData.load({
								params: Ext.apply(Ext.getCmp("panelForma").getForm().getValues(),{
									operacion: 	'NuevaConsulta', //Generar datos para la consulta
									start: 		0,
									limit: 		15
								})
							});	
							Ext.getCmp('botonImprimirPDF').disable();
							Ext.getCmp('botonBajarPDF').hide();
							
						}
						
					}
				);
			} else {
				
				// Realizar consulta de cuentas 
				if( respuesta.successAgregarCuenta ){
					
					var cuentasAgregadasData = Ext.StoreMgr.key("cuentasAgregadasDataStore");
					cuentasAgregadasData.load({
						params: Ext.apply(Ext.getCmp("panelForma").getForm().getValues(),{
							operacion: 	'NuevaConsulta', //Generar datos para la consulta
							start: 		0,
							limit: 		15
						})
					});	
					Ext.getCmp('botonImprimirPDF').disable();
					Ext.getCmp('botonBajarPDF').hide();
					
				}
				
			}
			
		} else {
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	var numeroNafinElectronicoOnBlurHandler = function(field){
							
		var numero = field.getValue();
							
		// Limpiar campos
		if(Ext.isEmpty(numero)){
								
			var clavePyme	= Ext.getCmp('clavePyme');
			var nombre		= Ext.getCmp('nombreProveedor');
			var rfc			= Ext.getCmp('rfcProveedor');
								
			clavePyme.setValue('');
			nombre.setValue('');
			rfc.setValue('');
 				
		// Consultar campos
		} else {
							
			// onBlur="soloNumeros(this.form.name,this.name,'El No. Nafin Electr�nico no es v�lido',0);recargar();"
			if(soloNumeros(field,'El No. Nafin Electr�nico no es v�lido',0)){
			
				var tipoAfiliado	= Ext.getCmp("cmbTipoAfiliado");
				var producto		= Ext.getCmp("cmbProducto");
										
				if(Ext.isEmpty(tipoAfiliado.getValue())){
					tipoAfiliado.markInvalid("Este campo es requerido");
					return;
				}
				if(Ext.isEmpty(producto.getValue())){
					producto.markInvalid("Este campo es requerido");
					return;
				}
									
				Ext.Ajax.request({
					url: '15cuenta01ext.data.jsp',
					params: {
						accion: 							'BusquedaProveedor',
						numeroNafinElectronico: 	numero,
						tipoAfiliado:					Ext.getCmp("cmbTipoAfiliado").getValue(),
						producto:						Ext.getCmp("cmbProducto").getValue()
					},
					callback: procesaBusquedaProveedor
				});
			
			}
			
		}
							
	}
 
	var cuentaClabeOnBlurHandler 		= function(field){
		soloNumeros(field,			'La Cuenta Clabe no es v�lida',40);
	}
	
	var cuentaSwiftOnBlurHandler 		= function(field){
		soloAlfaNumericos(field,	'La Cuenta Swift no es v�lida. S�lo se permiten caracteres alfanum�ricos.');
	}
	
	var plazaSwiftOnBlurHandler 		= function(field){
		soloAlfaNumericos(field,	'La Plaza Swift no es v�lida. S�lo se permiten caracteres alfanum�ricos.','-');
		
	}
	
	var sucursalSwiftOnBlurHandler 	= function(field){
		soloAlfaNumericos(field,	'La Sucursal Swift no es v�lida. S�lo se permiten caracteres alfanum�ricos.');
	}
	
	var procesarConsultaCuentasAgregadas = function(store, registros, opts){
 
		var panelForma 				= Ext.getCmp('panelForma');
		panelForma.el.unmask();
		
		var gridCuentasAgregadas 	= Ext.getCmp('gridCuentasAgregadas');
 
		if (registros != null) {
			
			if (!gridCuentasAgregadas.isVisible()) {
				gridCuentasAgregadas.show();
			}			
			 
			var botonImprimirPDF 	= Ext.getCmp('botonImprimirPDF');
			var botonBajarPDF 		= Ext.getCmp('botonBajarPDF');				
			
			var el 						= gridCuentasAgregadas.getGridEl();		
				
			if(store.getTotalCount() > 0) {
				
				/*
				console.dir(opts.params);
				if(!botonBajarPDF.isVisible()) {
					botonImprimirPDF.enable();
				} else {
					botonImprimirPDF.disable();
				}		
				*/
				
				botonImprimirPDF.enable();
				botonBajarPDF.disable();
				
				el.unmask();
				
			} else {					
					
				botonImprimirPDF.disable();
				botonBajarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
 
			gridCuentasAgregadas.setTitle("Cuentas Registradas: "+opts.params.nombreProveedor);
			
		}
		
	}
 
	var procesarSuccessFailureGeneraArchivoPaginaPDF =  function(opts, success, response) {
 
		var botonImprimirPDF = Ext.getCmp('botonImprimirPDF');
		botonImprimirPDF.setIconClass('');
		
		var botonBajarPDF = Ext.getCmp('botonBajarPDF');
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			botonBajarPDF.show();
			botonBajarPDF.enable();
			botonBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			botonBajarPDF.setHandler( function(boton, evento) {
				var forma 		= Ext.getDom('formAux');
				forma.action 	= Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
			
		} else {
 
			botonImprimirPDF.enable();
			botonBajarPDF.disable();
			NE.util.mostrarConnError(response,opts);
			
		}
 
	}
 
	//-------------------------------- STORES --------------------------------
	
	var catalogoTipoAfialiadoData = new Ext.data.JsonStore({
		id: 			'catalogoTipoAfialiadoDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15cuenta01ext.data.jsp',
		baseParams: {
			accion: 'CatalogoTipoAfiliado'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load: function(store,records,options){
				
								// Leer valor default
								var defaultValue 		= null;
								var existeParametro	= true;
								try {
									defaultValue = String(options.params.defaultValue);
								}catch(err){
									existeParametro	= false;
									defaultValue 		= null;
								}
								
								// Si se especific� un valor por default
								if( existeParametro ){ 
									
									var index = store.findExact( 'clave', defaultValue ); 
									// El registro fue encontrado por lo que hay que seleccionarlo
									if (index >= 0){
										Ext.getCmp("cmbTipoAfiliado").setValue(defaultValue);
									}
									Ext.getCmp("cmbTipoAfiliado").setReadOnly(true);
									
								} 
								
							},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	var catalogoProductoData = new Ext.data.JsonStore({
		id: 			'catalogoProductoDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15cuenta01ext.data.jsp',
		baseParams: {
			accion: 'CatalogoProducto'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load: function(store,records,options){
				
								// Leer valor default
								var defaultValue 		= null;
								var existeParametro	= true;
								try {
									defaultValue = String(options.params.defaultValue);
								}catch(err){
									existeParametro	= false;
									defaultValue 		= null;
								}
								
								// Si se especific� un valor por default
								if( existeParametro ){ 
									
									var index = store.findExact( 'clave', defaultValue ); 
									// El registro fue encontrado por lo que hay que seleccionarlo
									if (index >= 0){
										Ext.getCmp("cmbProducto").setValue(defaultValue);
									}
									Ext.getCmp("cmbProducto").setReadOnly(true);
									
								} 
								
							},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 			'catalogoEPODataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15cuenta01ext.data.jsp',
		baseParams: {
			accion: 'CatalogoEPO'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load: function(store,records,options){
				
								// Leer valor default
								var defaultValue 		= null;
								var existeParametro	= true;
								try {
									defaultValue = String(options.params.defaultValue);
								}catch(err){
									existeParametro	= false;
									defaultValue 		= null;
								}
								
								// Si se especific� un valor por default
								if( existeParametro ){ 
									
									// Hay registros
									if (records.length > 0){
										Ext.getCmp("cmbEPO").setValue(records[defaultValue].data['clave']);
									}
									Ext.getCmp("cmbEPO").setReadOnly(true);
									
								} 
								
							},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 			'catalogoMonedaDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15cuenta01ext.data.jsp',
		baseParams: {
			accion: 'CatalogoMoneda'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoBancoData = new Ext.data.JsonStore({
		id: 			'catalogoBancoDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15cuenta01ext.data.jsp',
		baseParams: {
			accion: 'CatalogoBanco'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var cuentasAgregadasData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'cuentasAgregadasDataStore',
		url: 		'15cuenta01ext.data.jsp',
		baseParams: {
			accion: 'ConsultaCuentasAgregadas'
		},
		fields: [
			{name: 'TIPO_AFILIADO'	},
			{name: 'NOMBRE_EPO'		}, 
			{name: 'MONEDA'			}, 
			{name: 'BANCO'				}, 
			{name: 'CLABE'				}, 
			{name: 'SWIFT'				}, 
			{name: 'PLAZA_SWIFT'		}, 				
			{name: 'SUCURSAL_SWIFT' }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload:	{
				fn: function(store, options){
					/*
					console.log("options.params(beforeload)");
					console.dir(options.params);
					*/
				}
			}, 
			load: 	procesarConsultaCuentasAgregadas,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCuentasAgregadas(null, null, null);						
				}
			}
		}
		
	});
	
	//------------------------------- BUSQUEDA AVANZADA ---------------------------
	
	var resetFormaBusquedaAvanzada = function(){
		
		var fpBusquedaAvanzada = Ext.getCmp('busquedaAvanzada');
		fpBusquedaAvanzada.getForm().reset();
		
		var busquedaAvanzadaData = Ext.StoreMgr.key('busquedaAvanzadaDataStore');
		Ext.Ajax.abort(busquedaAvanzadaData.proxy.getConnection().transId);
		busquedaAvanzadaData.removeAll();
		
		// Deshabilitar boton aceptar
		Ext.getCmp("botonAceptar").disable();
		
		// Poner mensaje de empty text por default
		var cmbPyme = Ext.getCmp('cmbPyme1');
		cmbPyme.emptyText =  "Seleccione...";
		cmbPyme.setRawValue('');
		cmbPyme.applyEmptyText();
		cmbPyme.setDisabled(true);
		
		// Habilitar boton de Busqueda
		var botonBuscar = Ext.getCmp("botonBuscar");
		botonBuscar.enable();
		botonBuscar.setIconClass('iconoLupa');
		
	}
	
	var busquedaAvanzadaData = new Ext.data.JsonStore({
		id: 					'busquedaAvanzadaDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion','loadMsg'],
		url: 					'15cuenta01ext.data.jsp',
		baseParams: {
			accion: 			'BusquedaAvanzada'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					// Remover animacion de carga
					var busquedaAvanzadaData = Ext.StoreMgr.key('busquedaAvanzadaDataStore');
					busquedaAvanzadaData.removeAll();		
					// Poner mensaje de empty text por default
					var cmbPyme = Ext.getCmp('cmbPyme1');
					cmbPyme.emptyText =  "Seleccione...";
					cmbPyme.setRawValue('');
					cmbPyme.applyEmptyText();
					cmbPyme.setDisabled(true);
					// Habilitar boton de Busqueda
					var botonBuscar = Ext.getCmp("botonBuscar");
					botonBuscar.enable();
					botonBuscar.setIconClass('iconoLupa');					
				}
			},
			beforeload:		NE.util.initMensajeCargaCombo,
			load:				function(store,records,options){
				
				// Verificar que el load realmente sea ocasionado por una busqueda del usuario
				// y no como consecuencia de la carga que se realiza para mostrar la animacion
				// de carga de datos.
				var busquedaAvanzadaData 	= false;
				try {
					busquedaAvanzadaData 	= options.params.busquedaAvanzadaData;
				}catch(err){
					busquedaAvanzadaData		= false;
				}
								
				// La busqueda es aut�ntica
				if( busquedaAvanzadaData ){ 
					
					// El registro fue encontrado por lo que hay que seleccionarlo
					var myCombo = Ext.getCmp("cmbPyme1");
					if ( store.getTotalCount() == 0){
						myCombo.emptyText =  "No existe informaci�n con los criterios determinados.";
						myCombo.setRawValue('');
						myCombo.applyEmptyText();
						Ext.getCmp('botonAceptar').setDisabled(true);
					} else {
						/*myCombo.emptyText =  "Seleccione...";
						myCombo.setRawValue('');
						myCombo.applyEmptyText();*/
						Ext.getCmp('botonAceptar').setDisabled(false);
					}
					
					// Habilitar boton de Busqueda
					var botonBuscar = Ext.getCmp("botonBuscar");
					botonBuscar.enable();
					botonBuscar.setIconClass('iconoLupa');
					
				}
				
			}
		}
		
	});
	
	var elementosFormaBusquedaAvanzada = [
		{
			xtype: 				'panel',
			id: 					'forma2',
			width: 				600,			
			frame: 				true,		
			titleCollapse: 	false,
			bodyStyle: 			'padding: 6px',			
			defaultType: 		'textfield',
			align: 				'center',
			html: 				'Utilice el * para realizar una b�squeda gen�rica.'
		},
		NE.util.getEspaciador(20),
		{ 
			xtype: 		'textfield',
			name: 		'nombrePyme',
			id: 			'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 	100,	
			width: 		80,
			msgTarget: 	'side',
			margins: 	'0 20 0 0'  
		},
		{ 
			xtype: 		'textfield',
			name: 		'rfcPyme',
			id: 			'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 	15,	
			width: 		80,
			msgTarget: 	'side',
			margins: 	'0 20 0 0'  
		},	
		{ 
			xtype: 		'textfield',
			name: 		'numeroPyme',
			id: 			'numeroPyme1',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 	25,	
			width: 		20,
			msgTarget: 	'side',
			margins: 	'0 20 0 0'  
		},	
		{
			xtype: 		'panel',
			items: [
				{
					xtype: 		'button',
					width: 		80,
					height:		10,
					text: 		'Buscar',
					iconCls: 	'iconoLupa',
					name: 		'botonBuscar',
					id: 			'botonBuscar',
					style: 		'float:right',
					handler:    function(boton, evento) {
								
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							var nombrePyme	= Ext.getCmp("nombrePyme1");
							var rfcPyme		= Ext.getCmp("rfcPyme1");
							var numeroPyme	= Ext.getCmp("numeroPyme1");
 
							/*
							// Nota: Esta validacion no es necesaria
							if( 
									Ext.isEmpty(nombrePyme.getValue())  		&&  
									Ext.isEmpty(rfcPyme.getValue())				&&
									Ext.isEmpty(numeroPyme.getValue())		
								){
								nombrePyme.markInvalid('Es necesario que ingrese datos en al menos un campo');						
								return;
							}
							*/
							
							/*
								Las siguientes tres l�neas podr�an ir en un handler
							*/
							var cmbPyme = Ext.getCmp('cmbPyme1');
							cmbPyme.emptyText =  "Seleccione...";
							cmbPyme.setRawValue('');
							cmbPyme.applyEmptyText();
							cmbPyme.setDisabled(false);
 
							var botonAceptar = Ext.getCmp('botonAceptar');
							botonAceptar.setDisabled(true);
							
							var busquedaAvanzada = Ext.getCmp("busquedaAvanzada");
							busquedaAvanzadaData.load({
									params: Ext.apply(busquedaAvanzada.getForm().getValues(),
									{
										busquedaAvanzadaData: true
									}
								)
							});
 
						}						
				}
			]
		},	
		NE.util.getEspaciador(5),
		{
			xtype: 				'combo',
			name: 				'cmbPyme',
			id: 					'cmbPyme1',
			mode: 				'local',
			autoLoad: 			false,
			displayField: 		'descripcion',
			emptyText: 			'Seleccione...',
			valueField: 		'clave',
			hiddenName: 		'clavePyme1',
			fieldLabel: 		'Nombre',
			disabled: 			true,
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				busquedaAvanzadaData,
			tpl:					NE.util.templateMensajeCargaCombo		
		}
	];
 
	var fpBusquedaAvanzada = new Ext.form.FormPanel({
		id: 					'busquedaAvanzada',
		width: 				500,
		title: 				'B�squeda Avanzada',
		frame: 				true,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 6px',
		labelWidth: 		110,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		items: 				elementosFormaBusquedaAvanzada,
		monitorValid: 		false,  // true,
		buttons: [
			{
				text: 		'Aceptar',
				name:			'botonAceptar',
				id:			'botonAceptar',
				iconCls: 	'aceptar',
				formBind: 	true,
				disabled: 	true,
				align: 		'center',
				handler: 
					function(boton, evento){
						
						var cmbPyme							= Ext.getCmp("cmbPyme1");
 
						if (Ext.isEmpty(cmbPyme.getValue())) {
							cmbPyme.markInvalid('Seleccione una Pyme');
							return;
						}
 
						var numeroNafinElectronico = Ext.getCmp('numeroNafinElectronico');
						/*
						var clavePyme					= Ext.getCmp('clavePyme');
						var nombre						= Ext.getCmp('nombreProveedor');
						var rfc							= Ext.getCmp('rfcProveedor');
						*/
						busquedaAvanzadaData.each(function(record){
							if(record.data['clave'] == cmbPyme.getValue()){
								numeroNafinElectronico.setValue(	record.json.nafin_electronico	);
								/*
								clavePyme.setValue(					cmbPyme.getValue()				);
								nombre.setValue(						record.json.nombre_pyme			); 
								rfc.setValue(							record.json.rfc					);
								*/
								return;
							}
						});
 
						var ventana 	= Ext.getCmp('windowBusquedaAvanzada');
						resetFormaBusquedaAvanzada();
						ventana.hide();
						
						numeroNafinElectronicoOnBlurHandler(numeroNafinElectronico);
						
					}
					
			},
			{
				text: 		'Cancelar',
				iconCls: 	'icoLimpiar',
				handler: 
					function() {					
						var ventana = Ext.getCmp('windowBusquedaAvanzada');
						resetFormaBusquedaAvanzada();
						ventana.hide();					
					}				
			}
		]
	});
 
	//--------------------------- ELEMENTOS DE LA FORMA ----------------------
 
	var elementosFormaConsulta = [
		// COMBO TIPO DE AFILIADO
		{
			xtype: 				'combo',
			name: 				'tipoAfiliado',
			id: 					'cmbTipoAfiliado',
			fieldLabel: 		'Tipo de Afiliado',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveTipoAfiliado',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTipoAfialiadoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%'
		},
		// COMBO PRODUCTO
		{
			xtype: 				'combo',
			name: 				'producto',
			id: 					'cmbProducto',
			fieldLabel: 		'Producto',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveProducto',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoProductoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%'
		},
		// INPUT TEXT NUMERO NAFIN ELECTRONICO
		{
			xtype: 				'compositefield',
			fieldLabel: 		'No. Nafin Electr�nico',			
			msgTarget: 			'side',
			id: 					'compositefield1',
			combineErrors: 	false,
			items: [
				{ 
					xtype: 		'numberfield',
					name: 		'numeroNafinElectronico',
					id: 			'numeroNafinElectronico',
					allowBlank: true,
					hidden: 		false,
					maxLength: 	15,	
					msgTarget: 	'side',
					margins: 	'0 20 0 0',  //necesario para mostrar el icono de error
					listeners:{
						blur: numeroNafinElectronicoOnBlurHandler
					}
					
				},
				{
					xtype: 		'button',
					hidden: 		false,
					text: 		'B�squeda Avanzada...',
					disabled:	true,
					id: 			'botonBusquedaAvanzada',
					handler: 
						function(boton, evento) {	
							
							/*
							var cboMoneda = Ext.getCmp("cboMoneda1");
							if (Ext.isEmpty(cboMoneda.getValue())  ) {
								cboMoneda.markInvalid('Debe Seleccionar  el tipo de Moneda');					
								return;
							}
							*/
				
							var ventana = Ext.getCmp('windowBusquedaAvanzada');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									layout: 			'fit',
									width: 			500,
									height: 			330,
									minWidth: 		500,
									minHeight: 		330,
									id: 				'windowBusquedaAvanzada',
									closeAction: 	'hide',
									items: [
										fpBusquedaAvanzada
									],
									listeners: 		{
										hide: resetFormaBusquedaAvanzada
									}
								}).show();
							}
						}
				}
				
			]
			
		},	
		{  
        xtype:	'hidden',  
        name:	'clavePyme',
        id: 	'clavePyme'
      },
		// INPUT TEXT NOMBRE DEL PROVEEDOR
		{
			xtype: 				'textfield',
			name: 				'nombreProveedor',
			id: 					'nombreProveedor',
			readOnly: 			true,
			fieldLabel: 		'Nombre',
			allowBlank: 		true,
			maxLength: 			100,	
			msgTarget: 			'side',
			anchor:				'95%',
			margins: 			'0 20 0 0'  //necesario para mostrar el icono de error
		},
		// INPUT TEXT RFC
		{
			xtype: 				'textfield',
			name: 				'rfcProveedor',
			id: 					'rfcProveedor',
			readOnly: 			true,
			fieldLabel: 		'RFC',
			allowBlank: 		true,
			maxLength: 			15,	
			msgTarget: 			'side',
			anchor:				'42.5%',
			margins: 			'0 20 0 0'  //necesario para mostrar el icono de error
		},
		// COMBO EPO
		{
			xtype: 				'combo',
			name: 				'EPO',
			id: 					'cmbEPO',
			fieldLabel: 		'EPO',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPO',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%'
		},
		// COMBO MONEDA
		{
			xtype: 				'combo',
			name: 				'moneda',
			id: 					'cmbMoneda',
			fieldLabel: 		'Moneda',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveMoneda',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMonedaData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%',
			listeners:			{
				collapse: function(cmbMoneda){
					
					var claveMoneda = cmbMoneda.getValue();
					if(			claveMoneda == 1  ){ // Pesos
						
						Ext.getCmp("cuentaClabe").setVisible(true);
						Ext.getCmp("cuentaSwift").setVisible(false);
						Ext.getCmp("plazaSwift").setVisible(false);
						Ext.getCmp("sucursalSwift").setVisible(false);
						
					}else if(	claveMoneda == 54 ){ // Dolares Americanos
						
						Ext.getCmp("cuentaClabe").setVisible(false);
						Ext.getCmp("cuentaSwift").setVisible(true);
						Ext.getCmp("plazaSwift").setVisible(true);
						Ext.getCmp("sucursalSwift").setVisible(true);
						
					}else{ // Cualquier otro caso
						
						Ext.getCmp("cuentaClabe").setVisible(false);
						Ext.getCmp("cuentaSwift").setVisible(false);
						Ext.getCmp("plazaSwift").setVisible(false);
						Ext.getCmp("sucursalSwift").setVisible(false);
						
					}
					// Ext.Msg.alert('Status',""+cmbMoneda.getValue());
				}
			}
		},
		// COMBO BANCO
		{
			xtype: 				'combo',
			name: 				'banco',
			id: 					'cmbBanco',
			fieldLabel: 		'Banco',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveBanco',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoBancoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%'
		},
		// INPUT TEXT CUENTA CLABE
		{
			xtype: 				'textfield',
			name: 				'cuentaClabe',
			id: 					'cuentaClabe',
			fieldLabel: 		'Cuenta Clabe',
			hidden:				true,
			allowBlank: 		true,
			maxLength: 			18,
			msgTarget: 			'side',
			anchor:				'45%',
			margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
			listeners:{
				blur: cuentaClabeOnBlurHandler
			}
		},
		// INPUT TEXT CUENTA SWIFT
		{
			xtype: 				'textfield',
			name: 				'cuentaSwift',
			id: 					'cuentaSwift',
			fieldLabel: 		'Cuenta Swift',
			hidden:				true,
			allowBlank: 		true,
			maxLength: 			20,	
			msgTarget: 			'side',
			anchor:				'45%',
			margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
			listeners:{
				blur: cuentaSwiftOnBlurHandler
			}
		},
		// INPUT TEXT PLAZA SWIFT
		{
			xtype: 				'textfield',
			name: 				'plazaSwift',
			id: 					'plazaSwift',
			fieldLabel: 		'Plaza Swift',
			hidden:				true,
			allowBlank: 		true,
			maxLength: 			20,	
			msgTarget: 			'side',
			anchor:				'45%',
			margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
			listeners:{
				blur: plazaSwiftOnBlurHandler
			}
		},
		// INPUT TEXT SUCURSAL SWIFT
		{
			xtype: 				'textfield',
			name: 				'sucursalSwift',
			id: 					'sucursalSwift',
			fieldLabel: 		'Sucursal Swift',
			hidden:				true,
			allowBlank: 		true,
			maxLength: 			20,	
			width: 				20,
			msgTarget: 			'side',
			anchor:				'45%',
			margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
			listeners:{
				blur: sucursalSwiftOnBlurHandler
			}
		}
	];
	
	var panelForma = new Ext.form.FormPanel({
		id: 				'panelForma',
		width: 			700,
		title: 			'Registro de Cuentas',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		defaultType: 	'textfield',
		items: 			elementosFormaConsulta,
		monitorValid: 	false,
		buttons: [
			// BOTON AGREGAR CUENTA
			{
				id:		'botonAgregar',
				text: 	'Agregar',
				hidden: 	false,
				iconCls: 'aceptar',
				errorActivo: false, 
				handler: function() {
					
					
					var panelForma = Ext.getCmp("panelForma");
					Ext.getCmp("botonAgregar").errorActivo = false;
					panelForma.items.each(function(panelItem, index, totalCount){
							
						if(panelItem.xtype       == 'hidden'   ){
							//No hacer nada
						}else if(panelItem.xtype == 'compositefield'){
							panelItem.items.each(function(subPanelItem, index, totalCount){
									if(!Ext.isEmpty(subPanelItem.getActiveError())){
										Ext.getCmp("botonAgregar").errorActivo = true;
										return;
									}
							});                          
							if(Ext.getCmp("botonAgregar").errorActivo){
								return;
							}
						}else{
							if(!Ext.isEmpty(panelItem.getActiveError())){
								Ext.getCmp("botonAgregar").errorActivo = true;
								return;
							}
						}
						
					});
					
					
					if(Ext.getCmp("botonAgregar").errorActivo){
						//Ext.MessageBox.alert('Error','La forma tiene elementos inv�lidos, por favor verifique');
						return;
					}
					
					var cmbTipoAfiliado = Ext.getCmp("cmbTipoAfiliado");
					if ( Ext.isEmpty(cmbTipoAfiliado.getValue()) ){
						cmbTipoAfiliado.markInvalid('Debe seleccionar un tipo de afiliado');
						return;
					}
					
					var numeroNafinElectronico = Ext.getCmp("numeroNafinElectronico").getValue();
					var nombreProveedor			= Ext.getCmp("nombreProveedor").getValue();
					var rfcProveedor				= Ext.getCmp("rfcProveedor").getValue();	
					if ( (!isdigit(numeroNafinElectronico)) || Ext.isEmpty(nombreProveedor) || Ext.isEmpty(rfcProveedor) ){
						Ext.getCmp("numeroNafinElectronico").markInvalid('El No. Nafin Electr�nico no es v�lido');
						return;
					}
					
					var cmbProducto = Ext.getCmp("cmbProducto");
					if (Ext.isEmpty(cmbProducto.getValue())){
						cmbProducto.markInvalid('El Producto no es v�lido');
						return;
					}
					
					var tipoAfiliado = cmbTipoAfiliado.getValue();
					if ( tipoAfiliado == "I" || tipoAfiliado =="B" || tipoAfiliado == "P" ){
							var cmbEPO = Ext.getCmp("cmbEPO");
							if( Ext.isEmpty(cmbEPO.getValue()) ){
								cmbEPO.markInvalid('Debe Seleccionar una EPO');
								return;
							}
					}  
					
					var cmbMoneda = Ext.getCmp("cmbMoneda");
					if (Ext.isEmpty(cmbMoneda.getValue()) ){
							cmbMoneda.markInvalid('La Moneda no es v�lida');
							return;
					}
					
					var cmbBanco = Ext.getCmp("cmbBanco");
					if ( Ext.isEmpty(cmbBanco.getValue()) ){
							cmbBanco.markInvalid('El Banco no es v�lido');
							return;
					} 
					
					var claveMoneda 		= cmbMoneda.getValue();
					var inputCuentaClabe	= Ext.getCmp("cuentaClabe")
					var cuentaClabe 		= trim(inputCuentaClabe.getValue());
					inputCuentaClabe.setValue(cuentaClabe);
					
					if ( (claveMoneda == 1) && Ext.isEmpty(cuentaClabe)){
							inputCuentaClabe.markInvalid('Debe introducir una Cuenta al menos');
							return;
					}
					
					var inputCuentaSwift	= Ext.getCmp("cuentaSwift")
					var cuentaSwift 		= trim(inputCuentaSwift.getValue());
					inputCuentaSwift.setValue(cuentaSwift);
						
					if ( (claveMoneda == 54) && Ext.isEmpty(cuentaSwift) ){
							inputCuentaSwift.markInvalid('Debe introducir una Cuenta');
							return;
					}
					
					var inputPlazaSwift	= Ext.getCmp("plazaSwift")
					var plazaSwift 		= trim(inputPlazaSwift.getValue());
					inputPlazaSwift.setValue(plazaSwift);
					
					if ( (claveMoneda == 54) && Ext.isEmpty(plazaSwift) ){
							inputPlazaSwift.markInvalid('Debe especificar la Plaza Swift');
							return;
					}
					
					var inputSucursalSwift	= Ext.getCmp("sucursalSwift")
					var sucursalSwift 		= trim(inputSucursalSwift.getValue());
					inputSucursalSwift.setValue(sucursalSwift);
					
					if ( (claveMoneda == 54) && Ext.isEmpty(sucursalSwift) ){
							inputSucursalSwift.markInvalid('Debe especificar la Sucursal Swift');
							return;
					}
						
					if(claveMoneda == 1 || claveMoneda == 54){
						
						Ext.MessageBox.confirm(
							'Confirm', 
							'�Est� seguro de querer registrar la cuenta?',
							function(boton) {
								
								if (boton == 'yes') {
									
									var claveMoneda 		= cmbMoneda.getValue();
									// Si se ha seleccionado la opcion de moneda nacional, ignorar lo especificado
									// en la cuenta swift
									if(claveMoneda == 1){
										inputCuentaSwift.setValue("");
										inputPlazaSwift.setValue("");
										inputSucursalSwift.setValue("");
									}
									
									// Si se ha seleccionado la opcion de dolares americanos, ignorar lo que se haya especificado
									// en la cuentas clabe
									if(claveMoneda == 54){
										inputCuentaClabe.setValue("");
									}
									
									var panelForma = Ext.getCmp("panelForma");
									Ext.Ajax.request({
										url: 		'15cuenta01ext.data.jsp',
										params: 	Ext.apply(
											panelForma.getForm().getValues(),{
												accion: 'AgregarCuenta'
											}
										),
										callback: procesaAgregarCuenta
									});
					
								} else if (boton == 'no') {
									return;
								}
								
							});
						
					}
						
					
			
				}
				
			},
			// BOTON CANCERLAR
			{
				text: 'Cancelar',
				hidden: false,
				iconCls: 'borrar',
				handler: function() {
					
					Ext.getCmp("panelForma").getForm().reset();
					
					Ext.getCmp("cmbTipoAfiliado").setValue("P");
					Ext.getCmp("cmbProducto").setValue("1");
					var catalogoEPOData = Ext.StoreMgr.key("catalogoEPODataStore");
					Ext.getCmp("cmbEPO").setValue(catalogoEPOData.getRange()[0].data['clave']);
					
					/*
					No es necesario ocultar el grid
					Ext.getCmp("gridCuentasAgregadas").hide();
					*/
				}
				
			},
			// BOTON LIMPIAR
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15cuenta01ext.jsp';
				}
			}
		]
	});
	
	// Grid con las cuentas registradas
	var grid = new Ext.grid.GridPanel({
		store: 	cuentasAgregadasData,
		id:		'gridCuentasAgregadas',
		hidden: 	true,
		margins: '20 0 0 0',
		title: 	'Cuentas Registradas',
		columns: [
			{
				header: 		'Tipo de Afiliado',
				tooltip: 	'Tipo de Afiliado',
				dataIndex: 	'TIPO_AFILIADO',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'EPO',
				tooltip: 	'EPO',
				dataIndex: 	'NOMBRE_EPO',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Moneda',
				tooltip: 	'Moneda',
				dataIndex: 	'MONEDA',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Banco',
				tooltip: 	'Banco',
				dataIndex: 	'BANCO',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Cuenta Clabe',
				tooltip: 	'Cuenta Clabe',
				dataIndex: 	'CLABE',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Cuenta Swift',
				tooltip: 	'Cuenta Swift',
				dataIndex: 	'SWIFT',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Plaza Swift',
				tooltip: 	'Plaza Swift',
				dataIndex: 	'PLAZA_SWIFT',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false
			},
			{
				header: 		'Sucursal Swift',
				tooltip: 	'Sucursal Swift',
				dataIndex: 	'SUCURSAL_SWIFT',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false
			}
		],
		stripeRows: true,
		loadMask: 	true,
		height: 		250,
		width: 		943,		
		frame: 		true,
		bbar: {
			
			xtype: 			'paging',
			pageSize: 		15,
			buttonAlign: 	'left',
			id: 				'barraPaginacion',
			opts:				null,
			displayInfo: 	true,
			store: 			cuentasAgregadasData,
			displayMsg: 	'{0} - {1} de {2}',
			emptyMsg: 		"No hay registros.",
			items: [
					'->',
					'-',
					{
						xtype:	'button',
						text: 	'Imprimir PDF',
						id: 		'botonImprimirPDF',
						handler: function(boton, evento) {
							
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							// Copiar los parametros base
							var cuentasAgregadasData 	= Ext.StoreMgr.key("cuentasAgregadasDataStore");
							var baseParams					= new Object();
							Ext.apply(baseParams, 		cuentasAgregadasData.baseParams);
							
							// Generar archivo PDF
							var barraPaginacion 			= Ext.getCmp("barraPaginacion");
							Ext.Ajax.request({
								url: 			'15cuenta01ext.data.jsp',
								params: 		Ext.apply( baseParams, { 
									accion: 	'GeneraArchivoPaginaPDF',
									start: 	barraPaginacion.cursor,
									limit: 	barraPaginacion.pageSize
								}),
								callback: 	procesarSuccessFailureGeneraArchivoPaginaPDF
							});
							 
						}
					},
					{
						xtype: 	'button',
						text: 	'Bajar PDF',
						id: 		'botonBajarPDF',
						hidden: 	true
					}					
				]
			}
	});
	
	//-------------------------------- PRINCIPAL -----------------------------------	
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		disabled: true,
		items: 	[
			NE.util.getEspaciador(10),
			panelForma,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10)
		]
	});
 
	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------
	//Peticion para realizar las validaciones iniciales
	Ext.Ajax.request({
		url: '15cuenta01ext.data.jsp',
		params: {
			accion: "valoresIniciales"                                                                 
		},
		callback: procesaValoresIniciales
	});
 
});