<%@ page 	contentType		= "text/html;charset=Cp1252"
				import			= "java.sql.*"
				import			= "java.util.*"
				import			= "java.io.*" 
				import			= "java.text.*"  
				import			= "java.math.*"  
				import			= "netropology.utilerias.*"
				import			= "com.netro.exception.*" 
				import			= "com.netro.dispersion.*"
				import			= "javax.naming.*"
				import			= "com.netro.xls.*"
				pageEncoding	= "Cp1252" 
				isErrorPage		= "false" 
%><%
	OutputStream 	stream 	= null;
	PrintStream 	info 		= null;

	response.setDateHeader("Expires", 0);
	response.setHeader("Pragma", "noCache");
%><%
	String tipoArchivo 				= (request.getParameter("tipoArchivo") == null) ? "" : request.getParameter("tipoArchivo");

	String nombreArchivoAttach = "DetalleCuentaCLABE.txt";
	if(tipoArchivo.equals("T")){
		nombreArchivoAttach = "DetalleTodasLasCuentasCLABE.txt";
	}		
	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-Disposition", "attachment;filename="+nombreArchivoAttach);
	
	
	AccesoDB 		con 			= new AccesoDB();
	StringBuffer 	strConsulta = new StringBuffer();
	StringBuffer 	strTabla 	= new StringBuffer();
	StringBuffer 	strCampo 	= new StringBuffer();
	
	try {
	    con.conexionDB();

		 String 			nombreArchivoClabe 	= "";
		 StringBuffer 	archivoClabe 			= generaArchivos("CLABE", "", tipoArchivo, request, con);
		 CreaArchivo 	archivo 					= new CreaArchivo();
		 String 			strDirectorioTemp 	= (String)application.getAttribute("strDirectorioPublicacion")+"00tmp/15cadenas/";
    
		 if (archivo.make(archivoClabe.toString(), strDirectorioTemp, ".txt")){
			nombreArchivoClabe = archivo.nombre; 
		 }
 
		 File 						f 			= null;
		 BufferedInputStream 	in 		= null; 
		 byte[] 					buffer 	= new byte[1024 * 4];  
		 int 						r 			= 0; 
		
		 try { 
			
			f 			= new File(strDirectorioTemp+nombreArchivoClabe);
			in 		= new BufferedInputStream(new FileInputStream(f));
			stream 	= response.getOutputStream();
			//info		= new PrintStream(stream,false,"Cp1252");
			info		= new PrintStream(stream);
			while ((r = in.read(buffer, 0, buffer.length)) != -1) { 
				info.write(buffer, 0, r); 
				stream.flush();
			}
			stream.flush();
			
		} finally {
			if(stream != null) 	stream.close();
			if(in != null) 		in.close(); 
		} 

	} catch (Exception e) {	
		out.println("Error:" + e.getMessage()); e.printStackTrace();
	} finally {
		if(con.hayConexionAbierta()) 
			con.cierraConexionDB();
	}
%><%!
public StringBuffer generaArchivos(String tipo, String tipo2, String tipoArchivo, HttpServletRequest request, AccesoDB con)  throws Exception {

	String cmb_tipo_afiliado 	= (request.getParameter("cmb_tipo_afiliado") == null) ? "" : request.getParameter("cmb_tipo_afiliado");
	String no_producto 			= (request.getParameter("no_producto") == null) ? "" : request.getParameter("no_producto");
	String no_epo 				= (request.getParameter("no_epo") == null) ? "" : request.getParameter("no_epo");
	String no_nafin 			= (request.getParameter("no_nafin") == null) ? "" : request.getParameter("no_nafin");
	String rfc 					= (request.getParameter("rfc") == null) ? "" : request.getParameter("rfc");
	String fecha_registro_de	= (request.getParameter("fecha_registro_de") == null) ? "" : request.getParameter("fecha_registro_de");
	String fecha_registro_a		= (request.getParameter("fecha_registro_a") == null) ? "" : request.getParameter("fecha_registro_a");

	
	Dispersion dispersionEJB = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	
	SimpleDateFormat sdf = new SimpleDateFormat ("dd-MM-yyyy");
	
	//StringBuffer contArchivo = new StringBuffer();
	StringBuffer strConsulta = new StringBuffer();
	
	String nombreArchivoClabe = "";
	String nombreArchivoSpeua = "";
	String nombreArchivoExcel = "";
	
	StringBuffer archivoClabe = new StringBuffer();
	StringBuffer archivoSpeua = new StringBuffer();
	StringBuffer archivoExcel = new StringBuffer();
	
	int _numRegistrosExcel = 0; //Contador del numero de registros que llevara el archivo Excel
	
	CreaArchivo archivo = new CreaArchivo();
	
	String sFechaActual = sdf.format(new java.util.Date());
	Calendar cFechaSigHabil = new GregorianCalendar();
	cFechaSigHabil.setTime(new java.util.Date());
	cFechaSigHabil.add(Calendar.DATE, 1);
	if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // Sábado
		cFechaSigHabil.add(Calendar.DATE, 2);
	if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
		cFechaSigHabil.add(Calendar.DATE, 1);
	Vector vFecha = dispersionEJB.bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil);
	boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
	cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
	while(bInhabil) {
		vFecha = dispersionEJB.bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil);
		bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
		if(!bInhabil)
			break;
		cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
	}
	String sFechaSigHabil = sdf.format(cFechaSigHabil.getTime());
	
	
	strConsulta.delete(0,strConsulta.length());		/*Limpia variable*/
	strConsulta.append("SELECT /*+index(c CP_COM_CUENTAS_PK) use_nl(c n)*/ c.ic_nafin_electronico,n.ic_epo_pyme_if, n.cg_tipo, ");
	strConsulta.append("TO_CHAR(c.df_registro,'dd-MM-yyyy') as df_registro, TO_CHAR(c.df_transferencia,'dd-MM-yyyy') as df_transferencia, ");
	strConsulta.append("TO_CHAR(c.df_aplicacion,'dd-MM-yyyy') as df_aplicacion, c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta ");
	if (tipo.equals("CLABE")) {
		strConsulta.append("FROM com_cuentas c, comrel_nafin n WHERE ic_estatus_cecoban is null AND ic_tipo_cuenta = 40  ");
	} else {
		strConsulta.append("FROM com_cuentas c, comrel_nafin n WHERE ic_estatus_tef is null  AND ic_tipo_cuenta = 1 ");
	}
	strConsulta.append(" AND ((cg_tipo_afiliado = 'E' AND ic_epo is null) OR (cg_tipo_afiliado != 'E' AND ic_epo is not null)) ");
	if("H".equals(tipoArchivo)) {
		strConsulta.append("AND c.df_registro >= trunc(sysdate)");
		strConsulta.append("AND c.df_registro < trunc(sysdate+1)");
	}
	if(!"".equals(fecha_registro_de)) {
		strConsulta.append("    AND c.df_registro >= TO_DATE('"+fecha_registro_de+"', 'dd/mm/yyyy')");
	}
	if(!"".equals(fecha_registro_a)) {
		strConsulta.append("    AND c.df_registro < (TO_DATE('"+fecha_registro_a+"', 'dd/mm/yyyy')+1)");
	}
	if(!"".equals(cmb_tipo_afiliado)) {
		strConsulta.append("    AND c.cg_tipo_afiliado = '"+cmb_tipo_afiliado+"'  ");
	}
	if(!"".equals(no_producto)) {
		strConsulta.append("    AND c.ic_producto_nafin = "+no_producto+"  ");
	}
	if(!"".equals(no_epo)) {
		strConsulta.append("    AND c.ic_epo = "+no_epo+"  ");
	}
	if(!"".equals(no_nafin)) {
		strConsulta.append("    AND c.ic_nafin_electronico = "+no_nafin+"  ");
	}
	
	strConsulta.append(" AND c.ic_nafin_electronico = n.ic_nafin_electronico ");
	String Aux = strConsulta.toString();
	String Aux2 = "";
	ResultSet rs= con.queryDB(strConsulta.toString());
	ResultSet rse = null;
System.out.println("<br>Consulta Archivo Speua "+strConsulta);

	while (rs.next())
	{
		strConsulta.delete(0,strConsulta.length());		/*Limpia variable*/
		if ("E".equals(rs.getString("CG_TIPO")))
			strConsulta.append("SELECT ic_epo, SUBSTR(NVL(cg_razon_social,'Sin Nombre'), 0, 30) as empresa , cg_rfc as rfc FROM comcat_epo WHERE ic_epo = ").append(rs.getString("IC_EPO_PYME_IF"));
		else if ("P".equals(rs.getString("CG_TIPO")))
			strConsulta.append("SELECT ic_pyme, SUBSTR(NVL(cg_razon_social,cg_nombre||' '||cg_appat||' '||cg_apmat  ), 0, 30) as empresa, cg_rfc as rfc FROM comcat_pyme WHERE ic_pyme = ").append(rs.getString("IC_EPO_PYME_IF"));
		else if ("I".equals(rs.getString("CG_TIPO")))
			strConsulta.append("SELECT ic_if, SUBSTR(NVL(cg_razon_social,'Sin Nombre'), 0, 30) as empresa, cg_rfc as rfc FROM comcat_if WHERE ic_if = ").append(rs.getString("IC_EPO_PYME_IF"));
		Aux2 = strConsulta.toString();
		System.err.println("<br>Consulta Archivo Speua "+strConsulta);
		rse = con.queryDB(strConsulta.toString());
		
		if (rse.next()){
			if ("1".equals(rs.getString("IC_TIPO_CUENTA"))){
				archivoSpeua.append(sFechaActual).append(";").append(sFechaSigHabil).append(";");
				archivoSpeua.append(sFechaSigHabil).append(";").append(formatoRFC(rse.getString("rfc"))).append(";");
				archivoSpeua.append(rse.getString("empresa")).append(";").append("HDA").append(";");
				archivoSpeua.append(rs.getString("IC_BANCOS_TEF")).append(";").append(rs.getString("IC_TIPO_CUENTA")).append(";");
				archivoSpeua.append(Comunes.rellenaCeros(rs.getString("CG_CUENTA"),11)).append(";").append("TEF-NE").append(";");
				archivoSpeua.append("VALIDACIÓN DE CUENTAS PARA TEF").append(";\n");
			} else {
				archivoClabe.append(sFechaActual).append(";").append(sFechaSigHabil).append(";");
				archivoClabe.append(sFechaSigHabil).append(";").append(formatoRFC(rse.getString("rfc"))).append(";");
				archivoClabe.append(rse.getString("empresa")).append(";").append("HDA").append(";");
				archivoClabe.append(rs.getString("IC_BANCOS_TEF")).append(";").append(rs.getString("IC_TIPO_CUENTA")).append(";");
				archivoClabe.append(Comunes.rellenaCeros(rs.getString("CG_CUENTA"),18)).append(";").append("TEF-NE").append(";");
				archivoClabe.append("VALIDACIÓN DE CUENTAS PARA TEF").append(";\r\n");				
			}
			
			if(tipo.equals("CLABE") && tipo2.equals("EXCEL")){
				/*Generar Archivo tipo EXCEL*/
				_numRegistrosExcel ++;
				
				archivoExcel.append(_numRegistrosExcel).append("|").append(formatoRFC(rse.getString("rfc"))).append("|");
				archivoExcel.append(rse.getString("empresa")).append("|").append(rs.getString("IC_BANCOS_TEF")).append("|");
				archivoExcel.append(rs.getString("CG_CUENTA")).append("|").append("HDE-EF-15").append("@");
		   }
		}
				
		rse.close();
		con.cierraStatement();
	}
	rs.close();
	con.cierraStatement();
	if (tipo.equals("CLABE") && tipo2.equals("")) {
		return archivoClabe;
	} 
	if(tipo.equals("CLABE") && tipo2.equals("EXCEL")){
		return archivoExcel;
	}
	else {
		return archivoSpeua;
	}
}
%><%!
public String formatoRFC (String cadena){
	String cad = "", car = "";
		for(int i=0;i < cadena.length();i++) {
			car = cadena.substring(i,i+1);
			if ((i==4) && ("0".equals(car) || "1".equals(car) || "2".equals(car) || "3".equals(car) || "4".equals(car) || "5".equals(car) || "6".equals(car) || "7".equals(car) || "8".equals(car) || "9".equals(car)))
				cad += " ";
			
            if (!"-".equals(car))
				cad += car;
        }
	return cad;
}
%>