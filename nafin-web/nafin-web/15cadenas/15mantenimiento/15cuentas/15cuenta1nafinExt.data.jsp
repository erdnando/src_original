<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		com.netro.model.catalogos.CatalogoEPOSwift,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.descuento.BusquedaAvanzadaI,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		com.netro.descuento.ConsCuentasCA,
		javax.naming.Context"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String accion 			= (request.getParameter("accion")!=null)?request.getParameter("accion"):"";
String infoRegresar	= "";

log.debug("accion = <"+accion+">");

if (        accion.equals("valoresIniciales") )	{
	
	JSONObject	resultado					= new JSONObject();
	boolean		success						= true;
	boolean		permisoAgregarCuentas 	= true;
	
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		success	= false;
		resultado.put( "msg", msg);
		
	}
	
	// 2. Determinar si la EPO es susceptible de dispersion
	if( success ){
		
		String claveEPO	= iNoCliente;
		try {
			
			if("EPO".equals(strTipoUsuario)) {
				dispersion.getSuceptibleDispersion(claveEPO);
			}
		} catch(NafinException neError){
			
			String msg	= neError.getMsgError();
			log.error(msg);
			neError.printStackTrace();
			permisoAgregarCuentas	= false;
			resultado.put("msg", msg);
			
		}
		
	}
	
	// 3. Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success));
	resultado.put("permisoAgregarCuentas", new Boolean(permisoAgregarCuentas));
	
	infoRegresar = resultado.toString();

} else if ( accion.equals("CatalogoTipoAfiliado")	){

	JSONObject	resultado	= new JSONObject();
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	
	registro.put("clave",			"E");
	registro.put("descripcion",	"EPO");
	registros.add(registro);
	registro.put("clave",			"P");
	registro.put("descripcion",	"Proveedor");
	registros.add(registro);
	registro.put("clave",			"D");
	registro.put("descripcion",	"Distribuidor");
	registros.add(registro);
	registro.put("clave",			"I");
	registro.put("descripcion",	"IF Bancario/No Bancario");
	registros.add(registro);
	registro.put("clave",			"B");
	registro.put("descripcion",	"IF/Beneficiario");
	registros.add(registro);
	
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
	
	/*
	 No se pueden agregar cuentas a distribuidores,
	 ya que el tipo de pyme sería "D" por lo que este combo no
	 podría ser seleccionado
	*/
	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("CatalogoProducto")		){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_producto_nafin"	);
	cat.setCampoDescripcion("ic_nombre"		);
	cat.setTabla("comcat_producto_nafin"	);
	cat.setCondicionNotIn("2", "String"		);	// <--- Este valor no es necesario, se pone por "compatibilidad"
	cat.setCondicionIn("1", 	"String"		);
	cat.setOrden("ic_nombre"					);
	
	/*
		Cuando el tipo de afiliado es "D" (Distribuidor) se utiliza el
		producto "4", pero para admin epo esta opcion no se utiliza.
	*/
	infoRegresar = cat.getJSONElementos();
	
} else if (	accion.equals("CatalogoEPO")				){
	
	JSONObject	resultado	= new JSONObject();
	String 		tipoAfiliado 				= request.getParameter("tipoAfiliado") 				== null?"":request.getParameter("tipoAfiliado");
	String 		producto 					= request.getParameter("producto") 						== null?"":request.getParameter("producto");
	String 		cvePyme 					= request.getParameter("cvePyme") 						== null?"":request.getParameter("cvePyme");

	System.out.println("cvePyme === "+cvePyme);
	CatalogoEPOSwift cat = new CatalogoEPOSwift();
	
	
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setTipoAfiliado(tipoAfiliado);
	cat.setCveProducto(producto);
	if("P".equals(tipoAfiliado))
		cat.setCvePyme(cvePyme);
	else
		cat.setCveIf(cvePyme);
	
	infoRegresar = cat.getJSONElementos();
		
} else if (	accion.equals("CatalogoMoneda")			){
	
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
  cat.setSeleccionCesion("1,54");
	cat.setOrden("1");
	
	infoRegresar = cat.getJSONElementos();
		
} else if (	accion.equals("CatalogoBanco")			){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_bancos_tef");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_bancos_tef");
	cat.setOrden("cd_descripcion");
	
	infoRegresar = cat.getJSONElementos();
	
} else if ( accion.equals("BusquedaProveedor")		){
	
	JSONObject	resultado					= new JSONObject();
	boolean		success						= true;
	
	String 		numeroNafinElectronico 	= request.getParameter("numeroNafinElectronico") 	== null?"":request.getParameter("numeroNafinElectronico");
	String 		tipoAfiliado 				= request.getParameter("tipoAfiliado") 				== null?"":request.getParameter("tipoAfiliado");
	String 		producto 					= request.getParameter("producto") 						== null?"":request.getParameter("producto");
		
	// Obtener instancia de EJB de Afiliacion
	Afiliacion afiliacion = null;
	try {
				
		afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		//Integer.parseInt("a");		
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Afiliación";
		log.error(msg);
		e.printStackTrace();
		
		success	= false;
		resultado.put( "msg", msg);
		
	}
	
 	if(success){	
 		
		// Realizar busqueda del afiliado	
		JSONObject 	busqueda 				= JSONObject.fromObject( afiliacion.buscaAfiliado(numeroNafinElectronico) );
		String 		clave 					= (String) busqueda.get("clave");
		boolean 		afialidoEncontrado	= true;
		if("".equals(clave)){ 
			String nombreEpo		= strNombre;
			afialidoEncontrado 	= false;
			resultado.put("msg", "El No. Nafin Electrónico no esta registardo: ");
		}
		resultado.put("encontrado", 			new Boolean(afialidoEncontrado)		);
		
		// Validar que la clave de afiliado proporcionada pertenezca a un proveedor
		boolean esProveedor					= false;
		if(afialidoEncontrado && "P".equals(tipoAfiliado)){
			String 	clavePyme 				= clave;
			// Buscar afiliado
			String 	tipoCliente				= afiliacion.getTipoCliente(clavePyme);
			//esProveedor							= tipoAfiliado.equals(tipoCliente)?true:false;
			if(tipoCliente.equals("P") || tipoCliente.equals("PD") )  { 
				esProveedor= true;
			}			
			if(!esProveedor){
				resultado.put("msg", 		"La clave proporcionada no corresponde con el tipo de afiliado seleccionado");
				/*
				busqueda.put("nombre", 		""	);
				busqueda.put("rfc",			"" );
				busqueda.put("clave",		"" );
				*/
			}
		}
		resultado.put("esProveedor", 			new Boolean(esProveedor)				);
		
		// Validar que la pyme proporcionada este parametrizada con la EPO
		boolean pymeParametrizada 			= false;
		if(esProveedor && "P".equals(tipoAfiliado)){
			String 	clavePyme 				= clave;
			String 	claveEPO 				= iNoCliente;
			String 	claveProducto			= producto; // Producto Descuento Electronico
			pymeParametrizada 				= afiliacion.pymeParametrizadaConEPO(clavePyme,claveEPO,claveProducto);
			
			if(!pymeParametrizada){
				resultado.put("msg", 		"La Pyme no está parametrizada para esta EPO, por favor verifique");
				/*
				busqueda.put("nombre", 		""	);
				busqueda.put("rfc",			"" );
				busqueda.put("clave",		"" );
				*/
			}
		}
		resultado.put("pymeParametrizada",	new Boolean(pymeParametrizada)		);
		
		resultado.put("nombre", 			busqueda.get("nombre")	);
		resultado.put("rfc",					busqueda.get("rfc")		);
		resultado.put("clave",				busqueda.get("clave")	);
		
	}
	
	resultado.put("success", 				new Boolean(success)		);
	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("BusquedaAvanzada")		){
	
  String claveEpo 					= (request.getParameter("claveEPO") 					== null) ? "" : request.getParameter("claveEPO");
  
  if("EPO".equals(strTipoUsuario)) {
    claveEpo 			= iNoCliente;
  }
	
	String 		rfcPyme 				= request.getParameter("rfcPyme")		== null?"":request.getParameter("rfcPyme");
	String 		nombrePyme 			= request.getParameter("nombrePyme")	== null?"":request.getParameter("nombrePyme");
	String 		numeroPyme			= request.getParameter("numeroPyme")	== null?"":request.getParameter("numeroPyme");

	String 		ic_producto_nafin	= "";
	String 		ic_pyme				= "";
	String      pantalla				= "";
	String  		ic_if					= "";
	String    	ret_num_pyme		= "";
	
	JSONArray	registros			= BusquedaAvanzadaI.getProveedores(
												ic_producto_nafin,
												claveEpo,
												numeroPyme,
												rfcPyme,
												nombrePyme,
												ic_pyme,
												pantalla,
												ic_if,
												ret_num_pyme
											);
	JSONObject	resultado			= new JSONObject();
	
	resultado.put("success", 		new Boolean("true") 					);
	resultado.put("total",	 		String.valueOf(registros.size()) );
	resultado.put("registros",		registros 								);
	
	infoRegresar = resultado.toString();
 
} else if (	accion.equals("AgregarCuenta")					){

	boolean 		success 					= true;
	boolean		successAgregarCuenta	= true;
	JSONObject	resultado				= new JSONObject();
	
	// 1. Leer argumentos de entrada
	String nombreEPO 					= strNombre;
	String loginUsuario 				= iNoUsuario; 
	
	String claveTipoAfiliado 		= (request.getParameter("claveTipoAfiliado") 		== null) ? "" : request.getParameter("claveTipoAfiliado");
	String claveProducto 			= (request.getParameter("claveProducto") 				== null) ? "" : request.getParameter("claveProducto");
	String numeroNafinElectronico = (request.getParameter("numeroNafinElectronico") 	== null) ? "" : request.getParameter("numeroNafinElectronico");
	String nombreProveedor 			= (request.getParameter("nombreProveedor") 			== null) ? "" : request.getParameter("nombreProveedor");
	String rfcProveedor 				= (request.getParameter("rfcProveedor") 				== null) ? "" : request.getParameter("rfcProveedor");
	String claveEPO 					= (request.getParameter("claveEPO") 					== null) ? "" : request.getParameter("claveEPO");
	String claveMoneda 				= (request.getParameter("claveMoneda") 				== null) ? "" : request.getParameter("claveMoneda");
	String claveBanco 				= (request.getParameter("claveBanco") 					== null) ? "" : request.getParameter("claveBanco");
	String cuentaClabe 				= (request.getParameter("cuentaClabe") 				== null) ? "" : request.getParameter("cuentaClabe");
	String cuentaSwift 				= (request.getParameter("cuentaSwift") 				== null) ? "" : request.getParameter("cuentaSwift");
	String plazaSwift 				= (request.getParameter("plazaSwift") 					== null) ? "" : request.getParameter("plazaSwift");
	String sucursalSwift 			= (request.getParameter("sucursalSwift") 				== null) ? "" : request.getParameter("sucursalSwift");
  String cgAba 			= (request.getParameter("cgAba") 				== null) ? "" : request.getParameter("cgAba");
  String cgSwift 			= (request.getParameter("cgSwift") 				== null) ? "" : request.getParameter("cgSwift");
  
  String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
  
 
	// 2. Insertar Cuenta
	
	// 2.1 Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		success	= false;
		resultado.put( "msg", msg);
		
	}
	
	// 2.2 Insertar Cuenta Clabe
  List lstCta = new ArrayList();
	if( success && !"".equals(cuentaClabe) ){
		
		try{
			
			HashMap parametrosAdicionales = new HashMap();
			parametrosAdicionales.put("DESCRIPCION_ALTA","ALTA\n" + nombreEPO	); 
			parametrosAdicionales.put("PANTALLA_ORIGEN", "REGINDCTAS" 	);
			
      
      if(!"P".equals(claveTipoAfiliado)){
        dispersion.ovinsertarCuenta(
                numeroNafinElectronico, 
                claveProducto, 
                claveMoneda, 
                claveBanco, 
                cuentaClabe, 
                loginUsuario, 
                "40", 
                claveEPO, 
                claveTipoAfiliado, 
                rfcProveedor, 
                parametrosAdicionales
              );
      }else{
        HashMap hmCtas = new HashMap();
        List arrRegistros = JSONArray.fromObject(jsonRegistros);
        Iterator itReg = arrRegistros.iterator();
        while (itReg.hasNext()) {
          hmCtas = new HashMap();
          JSONObject registro = (JSONObject)itReg.next();
          hmCtas.put("CVECTA", registro.getString("CVECUENTA"));
          hmCtas.put("CVEEPO", registro.getString("CVEEPO"));
          lstCta.add(hmCtas);
        }
        
        dispersion.ovinsertarCuentaProveedor(
                numeroNafinElectronico, 
                claveProducto, 
                claveMoneda, 
                claveBanco, 
                cuentaClabe, 
                loginUsuario, 
                "40", 
                lstCta, 
                claveTipoAfiliado, 
                rfcProveedor, 
                parametrosAdicionales
              );
      }
		} catch(NafinException neError){
			
			String msg	= "Error al agregar la Cuenta.<br/>" + neError.getMsgError() ;
			log.error(msg);
			neError.printStackTrace();
		
			successAgregarCuenta	= false;
			resultado.put("msg", msg);
						
		}
	
	// 2.3 Insertar Cuenta Swift
	} else if( success && !"".equals(cuentaSwift) ){
		
		try{
			
			// Agregar parametros adicionales
			HashMap parametrosAdicionales = new HashMap();
			parametrosAdicionales.put("PLAZA_SWIFT",		plazaSwift		);
			parametrosAdicionales.put("SUCURSAL_SWIFT",	sucursalSwift	);
			parametrosAdicionales.put("DESCRIPCION_ALTA","ALTA\n" + nombreEPO ); 
			parametrosAdicionales.put("PANTALLA_ORIGEN", "REGINDCTAS" 	);
      parametrosAdicionales.put("SWIFT",	cgSwift	);
      parametrosAdicionales.put("ABA",	cgAba	);
			
			// Agregar cuenta swift
      
      if(!"P".equals(claveTipoAfiliado)){
        dispersion.ovinsertarCuenta(
                numeroNafinElectronico, 
                claveProducto, 
                claveMoneda, 
                claveBanco, 
                cuentaSwift, 
                loginUsuario, 
                "50", 
                claveEPO, 
                claveTipoAfiliado, 
                rfcProveedor, 
                parametrosAdicionales
              );
      }else{
        HashMap hmCtas = new HashMap();
        List arrRegistros = JSONArray.fromObject(jsonRegistros);
        Iterator itReg = arrRegistros.iterator();
        while (itReg.hasNext()) {
          hmCtas = new HashMap();
          JSONObject registro = (JSONObject)itReg.next();
          hmCtas.put("CVECTA", registro.getString("CVECUENTA"));
          hmCtas.put("CVEEPO", registro.getString("CVEEPO"));
          lstCta.add(hmCtas);
        }
        
        dispersion.ovinsertarCuentaProveedor(
              numeroNafinElectronico, 
              claveProducto, 
              claveMoneda, 
              claveBanco, 
              cuentaSwift, 
              loginUsuario, 
              "50", 
              lstCta, 
              claveTipoAfiliado, 
              rfcProveedor, 
              parametrosAdicionales
            );
      
      }
			
		} catch(NafinException neError){
			
			String msg	= "Error al agregar la Cuenta.<br/>" + neError.getMsgError() ;
			log.error(msg);
			neError.printStackTrace();
		
			successAgregarCuenta	= false;
			resultado.put("msg", msg);
			
		}
		
	}
	
	// 3. Consultar valor
	resultado.put("success", 					new Boolean(success)						);
	resultado.put("successAgregarCuenta", 	new Boolean(successAgregarCuenta)	);
	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("ConsultaCuentasAgregadas")		){
	
	// Leer parametros
	String 	claveTipoAfiliado 		= (request.getParameter("claveTipoAfiliado") 		== null) ? "" : request.getParameter("claveTipoAfiliado");
	String 	tipoUsuario					= strTipoUsuario; 
	String 	claveEPO						= (request.getParameter("claveEPO") 		== null) ? "" : request.getParameter("claveEPO");
	String 	numeroNafinElectronico  = (request.getParameter("numeroNafinElectronico") 	== null) ? "" : request.getParameter("numeroNafinElectronico");
	String 	claveProducto				= (request.getParameter("claveProducto") 				== null) ? "" : request.getParameter("claveProducto");
	
	String   operacion					= (request.getParameter("operacion") 					== null) ? "" : request.getParameter("operacion");
	
	// Validar perfil del usuario que realiza la consulta
	if(!"EPO".equals(tipoUsuario)){
		//throw new AppException("No tiene permiso para realizar la consulta");
	}
		
	// Crear instancia del paginador
	ConsCuentasCA 			paginador 	= new ConsCuentasCA();
	paginador.setTipoAfiliado(claveTipoAfiliado); 
	paginador.setTipoUsuario(tipoUsuario); 
	paginador.setClaveEPO(claveEPO); 
	paginador.setNumeroNafinElectronico(numeroNafinElectronico); 
	paginador.setClaveProducto(claveProducto); 
	
	// Crear instancia del query helper y el objeto de resultado
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	JSONObject 				resultado	= new JSONObject();
	
	// Leer posicion de los registros
	int start = 0;
	int limit = 0;
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	// Realizar consulta
	try {
		
		if ("NuevaConsulta".equals(operacion)) {	//Nueva consulta
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		String  consultar = queryHelper.getJSONPageResultSet(request,start,limit);	
				
		resultado 			= JSONObject.fromObject(consultar);
				
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}	
							
	infoRegresar = resultado.toString();	
	
} else if (	accion.equals("GeneraArchivoPaginaPDF")		){
	
	// Leer parametros
	String 	claveTipoAfiliado 		= (request.getParameter("claveTipoAfiliado") 		== null) ? "" : request.getParameter("claveTipoAfiliado");
	String 	tipoUsuario					= strTipoUsuario; 
	String 	claveEPO						= (request.getParameter("claveEPO") 		== null) ? "" : request.getParameter("claveEPO");
	String 	numeroNafinElectronico  = (request.getParameter("numeroNafinElectronico") 	== null) ? "" : request.getParameter("numeroNafinElectronico");
	String 	claveProducto				= (request.getParameter("claveProducto") 				== null) ? "" : request.getParameter("claveProducto");
	
	// Validar perfil del usuario que realiza la consulta
	if(!"EPO".equals(tipoUsuario)){
	//	throw new AppException("No tiene permiso para realizar la consulta");
	}
	
	// Crear instancia del paginador	
	ConsCuentasCA 			paginador 	= new ConsCuentasCA();
	paginador.setTipoAfiliado(claveTipoAfiliado); 
	paginador.setTipoUsuario(tipoUsuario); 
	paginador.setClaveEPO(claveEPO); 
	paginador.setNumeroNafinElectronico(numeroNafinElectronico); 
	paginador.setClaveProducto(claveProducto); 
	
	// Crear instancia del query helper y el objeto de resultado
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	JSONObject 				resultado	= new JSONObject();
	
	// Leer posicion de los registros
	int start = 0;
	int limit = 0;
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	// Generar archivo PDF
	try {
																								
		String nombreArchivo = queryHelper.getCreatePageCustomFile( request, start, limit, strDirectorioTemp, "PDF");
		
		resultado.put("success", 		new Boolean(true));
		resultado.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo);
		
	} catch(Exception e) {
		throw new AppException("Error al generar el archivo PDF", e);
	}
	
	infoRegresar = resultado.toString();
	
} else if (	accion.equals("consultaCuentasProvEpo")		){
  JSONObject	resultado					= new JSONObject();
  String 	claveTipoAfiliado 		= (request.getParameter("claveTipoAfiliado") 		== null) ? "" : request.getParameter("claveTipoAfiliado");
	String 	tipoUsuario					= strTipoUsuario; 
	String 	claveEPO						= (request.getParameter("claveEPO") 		== null) ? "" : request.getParameter("claveEPO");
	String 	numeroNafinElectronico  = (request.getParameter("numeroNafinElectronico") 	== null) ? "" : request.getParameter("numeroNafinElectronico");
	String 	claveProducto				= (request.getParameter("claveProducto") 				== null) ? "" : request.getParameter("claveProducto");
  String claveMoneda 				= (request.getParameter("claveMoneda") 				== null) ? "" : request.getParameter("claveMoneda");
  String clavePyme 				= (request.getParameter("clavePyme") 				== null) ? "" : request.getParameter("clavePyme"); 
  List lstCtas = new ArrayList();
  boolean success = true;
  
  
  Dispersion dispersion = null;
	try {
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
    
    lstCtas = dispersion.getCtasPymeCLABESwift(claveTipoAfiliado, claveProducto, numeroNafinElectronico, clavePyme,  claveMoneda);
    
	}catch(Exception e){
		String msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		success	= false;
		resultado.put( "msg", msg);
	}
  
	JSONArray registros = JSONArray.fromObject(lstCtas);
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
  
}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>
