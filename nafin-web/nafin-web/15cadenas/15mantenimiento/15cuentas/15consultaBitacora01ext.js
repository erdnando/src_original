Ext.onReady(function(){

	/********** Regresa la pantalla a su condici�n inicial **********/
	function procesoLimpiar(){
		Ext.getCmp('formaPrincipal').getForm().reset();
		Ext.getCmp('gridConsulta').hide();
	}

	/***** Realiza la consulta para llenar el grid *****/
	function procesoBuscarDatos(){
		if((Ext.getCmp('fecha_inicio').getValue() == '' && Ext.getCmp('fecha_final').getValue() != '') || 
			(Ext.getCmp('fecha_inicio').getValue() != '' && Ext.getCmp('fecha_final').getValue() == '')){
			Ext.getCmp('fecha_inicio').markInvalid('Debe ingresar ambas fechas');
			Ext.getCmp('fecha_final').markInvalid('Debe ingresar ambas fechas');
			return;
		}
		if( Ext.getCmp('formaPrincipal').getForm().isValid()){
			formaPrincipal.el.mask('Procesando...', 'x-mask-loading');
			consultaData.load({
				params: Ext.apply({
					operacion   : 'generar',
					fecha_inicio: Ext.getCmp('fecha_inicio').getValue(),
					fecha_final : Ext.getCmp('fecha_final').getValue(),
					start       : 0,
					limit       : 15
				})
			});
		}
	}

	/********** Proceso para llenar el grid **********/
	var procesarConsultaData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('formaPrincipal');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();
		if (arrRegistros != null){
			Ext.getCmp('gridConsulta').show();
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0){
				el.unmask();
			} else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

	/********** Realiza la consulta para descargar el archivo seleccionado en el grid **********/
	function buscarArchivo(rec, extension){
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();
		el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '15consultaBitacora01ext.data.jsp',
			params: {
				informacion: 'Descargar_Archivos',
				ic_descarga: rec.get('IC_BIT_DESCARGA'),
				extension  : extension
			},
			callback: descargaArchivo
		});
	}

	/***** Descargar archivos *****/
	function descargaArchivo(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			formaPrincipal.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			formaPrincipal.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();
		el.unmask();
	}

	/********** Se crea el store para el Grid **********/
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url:  '15consultaBitacora01ext.data.jsp',
		baseParams: {
			informacion: 'Consultar_Datos'
		},
		fields: [
			{name: 'IC_BIT_DESCARGA'  },
			{name: 'FECHA_DESCARGA'   },
			{name: 'CG_NOMBRE_USUARIO'},
			{name: 'ARCHIVO_TXT'      },
			{name: 'ARCHIVO_EXCEL'    }
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	/********** Se crea el grid de consulta**********/
	var gridConsulta = new Ext.grid.EditorGridPanel({
		width:          600,
		height:         410,
		id:             'gridConsulta',
		style:          'margin:0 auto;',
		margins:        '20 0 0 0',
		align:          'center',
		hidden:         true,
		displayInfo:    true,
		loadMask:       true,
		stripeRows:     true,
		frame:          true,
		border:         true,
		store:          consultaData,
		columns: [{
			width:       160,
			header:      'Fecha y Hora de descarga',
			dataIndex:   'FECHA_DESCARGA',
			align:       'center',
			sortable:    true,
			resizable:   true
		},{
			width:       228,
			header:      'Usuario',
			dataIndex:   'CG_NOMBRE_USUARIO',
			align:       'center',
			sortable:    true,
			resizable:   true
		},{
			width:       98,
			xtype:       'actioncolumn',
			header:      'Archivo TXT',
			align:       'center',
			sortable:    false,
			resizable:   false,
			items: [{
				getClass: function(v, meta, rec){
					if (rec.get('ARCHIVO_TXT') != '0'){
						this.items[0].tooltip = 'Ver archivo TXT';
						return 'icoBuscar';
					} else{
						return null;
					}
				},
				handler: function(gridConsulta, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					buscarArchivo(rec, 'txt');
				}
			}]
		},{
			width:       98,
			xtype:       'actioncolumn',
			header:      'Archivo EXCEL',
			align:       'center',
			sortable:    false,
			resizable:   false,
			items: [{
				getClass: function(v, meta, rec){
					if (rec.get('ARCHIVO_EXCEL') != '0'){
						this.items[0].tooltip = 'Ver archivo XLS';
						return 'icoBuscar';
					} else{
						return null;
					}
				},
				handler: function(gridConsulta, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					buscarArchivo(rec, 'xls');
				}
			}]
		}],
		bbar: {
			xtype:       'paging',
			id:          'barraPaginacion',
			displayMsg:  '{0} - {1} de {2}',
			emptyMsg:    'No hay registros.',
			buttonAlign: 'left',
			displayInfo: true,
			pageSize:    15,
			store:       consultaData
		}
	});

	/********** Form Panel Principal **********/
	var formaPrincipal = new Ext.form.FormPanel({
		width:                  450,
		labelWidth:             120,
		id:                     'formaPrincipal',
		title:                  'Consultar Bit�cora',
		layout:                 'form',
		style:                  'margin: 0 auto;',
		hidden:                 false,
		frame:                  true,
		border:                 true,
		autoHeight:             true,
		items: [{
			xtype:               'compositefield',
			fieldLabel:          '&nbsp;&nbsp; Fecha de descarga',
			msgTarget:           'side',
			combineErrors:       false,
			items: [{
				width:            120,
				xtype:            'datefield',
				id:               'fecha_inicio',
				name:             'fecha_inicio',
				msgTarget:        'side',
				vtype:            'rangofecha',
				minValue:         '01/01/1901',
				campoFinFecha:    'fecha_final',
				margins:          '0 20 0 0',
				value:            new Date(),
				allowBlank:       true,
				startDay:         0
			},{
				width:            20,
				xtype:            'displayfield',
				value:            'al'
			},{
				width:            120,
				xtype:            'datefield',
				id:               'fecha_final',
				name:             'fecha_final',
				msgTarget:        'side',
				vtype:            'rangofecha',
				minValue:         '01/01/1901',
				campoInicioFecha: 'fecha_inicio',
				margins:          '0 20 0 0',
				value:            new Date(),
				allowBlank:       true,
				startDay:         1
			}]
		}],
		buttons: [{
				xtype:            'button',
				text:             'Buscar',
				id:               'btnBuscar',
				iconCls:          'icoBuscar',
				handler:          procesoBuscarDatos
			},{
				xtype:            'button',
				text:             'Limpiar',
				id:               'btnLimpiar',
				iconCls:          'icoLimpiar',
				handler:          procesoLimpiar
		}]
	});

	/********** Contenedor Principal **********/
	var pnl = new Ext.Container({
		width:   949,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin: 0 auto;',
		items: [
			NE.util.getEspaciador(20),
			formaPrincipal,
			NE.util.getEspaciador(10),
			gridConsulta
		]
	});

});