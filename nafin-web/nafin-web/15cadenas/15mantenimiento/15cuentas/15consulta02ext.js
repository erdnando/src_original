Ext.onReady(function() {
	
	var catalogoMoneda = null;
	
	//--------------------------------- VALIDACIONES ------------------------------
 
	function trim(str){
		
		var cadena = str;
		
		// Si la cadena proporcionada es NULL no realizar ninguna operacion
		if (str == null ){
			return str;
		}
		
		// Suprimir los caracteres "vacios" al inicio y al final
		cadena = cadena.replace(/^\s+/, "");
		cadena = cadena.replace(/\s+$/, "");
		return cadena;
		
	}

	function validaDigitoVerificador(contenido){
		
		var ruta 				= document.registro;			
		var car 					= "";
		var resultado 			= "";
		var residuo 			= 0;    
		var mulIndividual 	= 0;
		var digVerificador 	= 0;  
		var contenedor 		= 0;
		var cadena				= "";
		cadena 					= String(contenido);
 
		digVerificador 		= parseInt(cadena.substr(17),10);
		
		for (var i=0; i <= 16; i++){
			car = cadena.substring(i,i+1);
			switch(i){
				case 0: case 3: case 6: case 9: case 12: case 15:		/*3*/
					mulIndividual 	= 	parseInt(car)*3;
					contenedor 		+= mulIndividual;
					break;
				case 1: case 4: case 7: case 10: case 13: case 16:		/*7*/
					mulIndividual 	= 	parseInt(car)*7;
					contenedor 		+= mulIndividual;
					break;
				case 2: case 5: case 8: case 11: case 14:					/*1*/
					mulIndividual 	= 	parseInt(car)*1;
					contenedor 		+= mulIndividual;
					break;
			}
		}
		
		residuo = contenedor % 10;  /*Obtiene el residuo */
		
		if (residuo > 0)  
			contenedor = 10 - residuo;
		else
			contenedor = 0;
		
		if (digVerificador == contenedor)
			return "S"; 
		else
			return "N"; 
		
	}	

	function soloAlfaNumericos(field,mensaje,extrachars){
		
	  var InString = field.getValue();
	  if(InString.length==0){ 
	  	  return; 
	  }
		
	  var RefString = "01234567890AaBbCcDdEeFfGgHhIiJjKkLlMmNn��OoPpQqRrSsTtUuVvWwXxYyZz";
	  if(extrachars != null && extrachars != undefined){
		  RefString += extrachars;
	  }
	 
	  for(var Count=0;Count < InString.length;Count++){
	  	  
		 var TempChar = InString.substring(Count,Count+1);
		 if(RefString.indexOf(TempChar,0)==-1){ 
			field.markInvalid(mensaje);
			return;
		 }
		 
	  }
	  
	  return;
	  
	}
	
	function soloNumeros(field,mensaje,clave){
	
		var CadenaValida="0123456789"; /*variable con caracteres validos*/
		
		if( Ext.isEmpty(field.getValue()) ){ return; } // Si est� vac�o salir
		
		var numero = String(field.getValue());
		for(var i=0;i<numero.length;i++){
			if(CadenaValida.indexOf(numero.charAt(i))==-1){
				field.markInvalid(mensaje);
				return false;
			}
		}
		
		if (clave == "1"){
			
			if (numero.length != 11){
				field.markInvalid('La longitud no es v�lida, debe de ser de 11 caracteres');
				return false;
			}
			
		}
		
		if (clave == "40"){
			
			if (numero.length != 18){
				field.markInvalid('La longitud no es v�lida, debe de ser de 18 caracteres');
				return false;
			}
	
			var verificar = validaDigitoVerificador(numero);
			if (verificar == "N"){
				field.markInvalid('La Cuenta Clabe no es correcta (D�gito Verificador)');
				return false;
			}
			
		}
		
		return true;
		
	}
	
	//------------------------------ OTRAS FUNCIONES -------------------------
	var resetWindowConfirmacionClave = function(){
		var fpConfirmacionClave = Ext.getCmp('confirmacionClave');
		fpConfirmacionClave.getForm().reset();
	}
	
	//------------------------------- HANDLERS -------------------------------
	var procesaValoresIniciales = function(opts, success, response) {
		
		if ( success == true && Ext.util.JSON.decode(response.responseText).success == true ) {
 
			var resp 		= Ext.util.JSON.decode(response.responseText);
			catalogoMoneda = resp.catalogoMoneda;
			
			// Cargar valores de los combos
			var catalogoTipoAfialiadoData = Ext.StoreMgr.key('catalogoTipoAfialiadoDataStore');
			catalogoTipoAfialiadoData.load({
         	params: { defaultValue: "P" }
        	});
			
			var catalogoProductoData 		= Ext.StoreMgr.key('catalogoProductoDataStore');
			catalogoProductoData.load({
         	params: { defaultValue: "1" }
         });
 
		} else {
 
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
 
	var procesaBusquedaProveedor = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			var clavePyme					= Ext.getCmp('clavePyme');
			var nombre						= Ext.getCmp('nombreProveedor');
			var rfc							= Ext.getCmp('rfcProveedor');
			
			clavePyme.setValue(	resp.clave	);
			nombre.setValue(		resp.nombre	); 
			rfc.setValue(			resp.rfc		);
				
			if(!resp.encontrado){ 
				
				var  numeroNafinElectronico = Ext.getCmp('numeroNafinElectronico');
				numeroNafinElectronico.markInvalid(resp.msg);
 	
			}else{
			
				// Consultar cuentas registradas con el proveedor ... la pantalla por default
				// no funciona as�, por lo que se propone un posible soluci�n en caso se decida 
				// implementarlo
				/*
				var cuentasAgregadasData = Ext.StoreMgr.key("cuentasAgregadasDataStore");
				cuentasAgregadasData.load({
					params: Ext.apply(Ext.getCmp("panelForma").getForm().getValues(),{
						operacion: 	'NuevaConsulta', //Generar datos para la consulta
						start: 		0,
						limit: 		15
					})
				});
				Ext.getCmp('botonGenerarArchivoCSV').disable();
				Ext.getCmp('botonBajarArchivoCSV').hide();
				Ext.getCmp('botonImprimirPDF').disable();
				Ext.getCmp('botonBajarPDF').hide();
				*/
			}
 
		} else {
			
			Ext.getCmp('clavePyme').setValue(		"" );
			Ext.getCmp('nombreProveedor').setValue("" );
			Ext.getCmp('rfcProveedor').setValue(	"" );
			
			Ext.getCmp('numeroNafinElectronico').markInvalid("Ocurri� un error inesperado al buscar proveedor");
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesaValidarCuenta = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			// Refrescar Grid Panel con la cuenta actualizada
			var cuentasConsultadasData = Ext.StoreMgr.key("cuentasConsultadasDataStore");
			cuentasConsultadasData.load();
 
			Ext.getCmp('botonGenerarArchivoCSV').disable();
			Ext.getCmp('botonBajarArchivoCSV').hide();
			Ext.getCmp('botonImprimirPDF').disable();
			Ext.getCmp('botonBajarPDF').hide();
			
		}else{
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesaInvalidarCuenta = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			// Refrescar Grid Panel con la cuenta actualizada
			var cuentasConsultadasData = Ext.StoreMgr.key("cuentasConsultadasDataStore");
			cuentasConsultadasData.load();
 
			Ext.getCmp('botonGenerarArchivoCSV').disable();
			Ext.getCmp('botonBajarArchivoCSV').hide();
			Ext.getCmp('botonImprimirPDF').disable();
			Ext.getCmp('botonBajarPDF').hide();
			
		}else{
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesaEliminarCuenta = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						eliminarCuenta(resp.estadoSiguiente,opts.params.rowIndex,opts.params.claveCuenta);
					}
				);
			} else {
				eliminarCuenta(resp.estadoSiguiente,opts.params.rowIndex,opts.params.claveCuenta);
			}
			
		}else{
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	var numeroNafinElectronicoOnBlurHandler = function(field){
							
		var numero = field.getValue();
							
		// Limpiar campos
		if(Ext.isEmpty(numero)){
								
			var clavePyme	= Ext.getCmp('clavePyme');
			var nombre		= Ext.getCmp('nombreProveedor');
			var rfc			= Ext.getCmp('rfcProveedor');
								
			clavePyme.setValue('');
			nombre.setValue('');
			rfc.setValue('');
 				
		// Consultar campos
		} else {
							
			// onBlur="soloNumeros(this.form.name,this.name,'El No. Nafin Electr�nico no es v�lido',0);recargar();"
			if(soloNumeros(field,'El No. Nafin Electr�nico no es v�lido',0)){
			
				var tipoAfiliado	= Ext.getCmp("cmbTipoAfiliado");
				var producto		= Ext.getCmp("cmbProducto");
										
				if(Ext.isEmpty(tipoAfiliado.getValue())){
					tipoAfiliado.markInvalid("Este campo es requerido");
					return;
				}
				if(Ext.isEmpty(producto.getValue())){
					producto.markInvalid("Este campo es requerido");
					return;
				}
									
				Ext.Ajax.request({
					url: '15consulta02ext.data.jsp',
					params: {
						accion: 							'BusquedaProveedor',
						numeroNafinElectronico: 	numero,
						tipoAfiliado:					Ext.getCmp("cmbTipoAfiliado").getValue(),
						producto:						Ext.getCmp("cmbProducto").getValue()
					},
					callback: procesaBusquedaProveedor
				});
			
			}
			
		}
							
	}
 
	var procesarConsultaCuentas = function(store, registros, opts){
 
		var panelForma 				= Ext.getCmp('panelForma');
		panelForma.el.unmask();
		
		var gridCuentasConsultadas = Ext.getCmp('gridCuentasConsultadas');
 
		if (registros != null) {
			
			if (!gridCuentasConsultadas.isVisible()) {
				gridCuentasConsultadas.show();
			}			
			
			var botonGenerarArchivoCSV = Ext.getCmp('botonGenerarArchivoCSV');
			var botonBajarArchivoCSV 	= Ext.getCmp('botonBajarArchivoCSV');
			var botonImprimirPDF 		= Ext.getCmp('botonImprimirPDF');
			var botonBajarPDF 			= Ext.getCmp('botonBajarPDF');	
 
			var el 							= gridCuentasConsultadas.getGridEl();		
				
			if(store.getTotalCount() > 0) {
 
				botonGenerarArchivoCSV.enable();
				botonBajarArchivoCSV.disable();
				botonImprimirPDF.enable();
				botonBajarPDF.disable();
				
				el.unmask();
				
			} else {					
				
				botonGenerarArchivoCSV.disable();
				botonBajarArchivoCSV.disable();
				botonImprimirPDF.disable();
				botonBajarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
			
			if(!Ext.isEmpty(opts.params.nombreProveedor)){
				gridCuentasConsultadas.setTitle("Cuentas Registradas: "+opts.params.nombreProveedor);
			}else{
				gridCuentasConsultadas.setTitle("Cuentas Registradas:");
			}
			
		}
		
	}
	
	var procesarSuccessFailureGeneraArchivoCSV = function(opts, success, response){
		
		var botonGenerarArchivoCSV = Ext.getCmp('botonGenerarArchivoCSV');
		botonGenerarArchivoCSV.setIconClass('');
		
		var botonBajarArchivoCSV 	= Ext.getCmp('botonBajarArchivoCSV');
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if ( success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			botonBajarArchivoCSV.show();
			botonBajarArchivoCSV.enable();
			botonBajarArchivoCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			botonBajarArchivoCSV.setHandler( function(boton, evento) {
				var forma 		= Ext.getDom('formAux');
				forma.action 	= Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
			
		} else {
			 
			botonGenerarArchivoCSV.enable();
			botonBajarArchivoCSV.disable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarSuccessFailureGeneraArchivoPaginaPDF =  function(opts, success, response) {
 
		var botonImprimirPDF = Ext.getCmp('botonImprimirPDF');
		botonImprimirPDF.setIconClass('');
		
		var botonBajarPDF = Ext.getCmp('botonBajarPDF');
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			botonBajarPDF.show();
			botonBajarPDF.enable();
			botonBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			botonBajarPDF.setHandler( function(boton, evento) {
				var forma 		= Ext.getDom('formAux');
				forma.action 	= Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
			
		} else {
			 
			botonImprimirPDF.enable();
			botonBajarPDF.disable();
			NE.util.mostrarConnError(response,opts);
			
		}
 
	}
 
	// Se implementa una "maqina de estado" para simplificar la complejidad
	var eliminarCuenta = function(estado,rowIndex,claveCuentaValidar){
		
		if( 			estado == "VERIFICAR_USO_CUENTA" 		){
 
			var record 				= Ext.StoreMgr.key('cuentasConsultadasDataStore').getAt(rowIndex);
			var claveCuenta		= record.json['IC_CUENTA'];
         var estatusCecoban	= record.json['IC_ESTATUS_CECOBAN'];
 
         // Aunque es muy improbable, se agrega la siguiente validacion para validar la perdida de sincronia
         if( claveCuentaValidar != claveCuenta  ){
         	Ext.Msg.alert(
					'Mensaje',
					'Se perdi� la sincron�a, la operaci�n ha sido cancelada.',
					function(btn, text){
						Ext.Ajax.request({
								url: 		'15consulta02ext.data.jsp',
								params: 	{
									accion:			'EliminarCuenta',
									operacion:		'Fin',
									claveCuenta: 	null,
									rowIndex:		null
								},
								callback: 			procesaEliminarCuenta
						});
						
					}
				);
				return;
         }
         
			Ext.Ajax.request({
				url: 		'15consulta02ext.data.jsp',
				params: 	{
					accion:				'EliminarCuenta',
					operacion:			'VerificarUsoCuenta',
					claveCuenta: 		claveCuenta,
					estatusCecoban:	estatusCecoban,
					rowIndex:			rowIndex
				},
				callback: 				procesaEliminarCuenta
			});
			
		}else if(  	estado == "VALIDAR_USUARIO" 	){ // => estatusCecoban == "99"
			
			var record 					= Ext.StoreMgr.key('cuentasConsultadasDataStore').getAt(rowIndex);
			var claveCuenta			= record.json['IC_CUENTA'];
			var esCuentaEnDolares	= "true" == record.json['ES_CUENTA_DOLARES']?true:false;
			
			 // Aunque es muy improbable, se agrega la siguiente validacion para validar la perdida de sincronia
         if( claveCuentaValidar != claveCuenta  ){
         	Ext.Msg.alert(
					'Mensaje',
					'Se perdi� la sincron�a, la operaci�n ha sido cancelada.',
					function(btn, text){
						Ext.Ajax.request({
								url: 		'15consulta02ext.data.jsp',
								params: 	{
									accion:			'EliminarCuenta',
									operacion:		'Fin',
									claveCuenta: 	null,
									rowIndex:		null
								},
								callback: 			procesaEliminarCuenta
						});
						
					}
				);
				return;
         }
         
			if(esCuentaEnDolares){
				
				Ext.MessageBox.confirm(
					'Confirmar', 
					'Ya existe la validaci�n de esta Cuenta Swift, �Realmente desea eliminarla?',
					function(boton) {
												
						if (boton == 'yes') { 
 
							Ext.getCmp("indiceRegistro").setValue(rowIndex);
							
							var ventana = Ext.getCmp('windowConfirmacionClave');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									layout: 			'fit',
									width: 			250,
									height: 			165,
									minWidth: 		250,
									minHeight: 		165,
									id: 				'windowConfirmacionClave',
									modal:			true,
									closeAction: 	'hide',
									items: [
										Ext.getCmp("confirmacionClave")
									],
									listeners: 		{
										hide: resetWindowConfirmacionClave
									}
								}).show();
							}
 
						} else if (boton == 'no') {
							
							Ext.Ajax.request({
								url: 		'15consulta02ext.data.jsp',
								params: 	{
									accion:			'EliminarCuenta',
									operacion:		'Fin',
									claveCuenta: 	claveCuenta,
									rowIndex:		rowIndex
								},
								callback: 			procesaEliminarCuenta
							});
							
						}
													
				});
				
			} else {	
				
				Ext.MessageBox.confirm(
					'Confirmar', 
					'Ya existe la validaci�n de esta cuenta CLABE, �Realmente desea eliminarla?',
					function(boton) {
												
						if (boton == 'yes') { 
 
							Ext.getCmp("indiceRegistro").setValue(rowIndex);
							
							var ventana = Ext.getCmp('windowConfirmacionClave');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									layout: 			'fit',
									width: 			250,
									height: 			165,
									minWidth: 		250,
									minHeight: 		165,
									modal:			true,
									id: 				'windowConfirmacionClave',
									closeAction: 	'hide',
									items: [
										Ext.getCmp("confirmacionClave")
									],
									listeners: 		{
										hide: resetWindowConfirmacionClave
									}
								}).show();
							}
 		
						} else if (boton == 'no') {
							
							Ext.Ajax.request({
								url: 		'15consulta02ext.data.jsp',
								params: 	{
									accion:			'EliminarCuenta',
									operacion:		'Fin',
									claveCuenta: 	claveCuenta,
									rowIndex:		rowIndex
								},
								callback: 			procesaEliminarCuenta
							});

						}
													
					});
				
			}
			
		} else if(	estado == "ELIMINAR_CUENTA" 		){
			
			var record 					= Ext.StoreMgr.key('cuentasConsultadasDataStore').getAt(rowIndex);
			var claveCuenta			= record.json['IC_CUENTA'];
			var estatusCecoban		= record.json['IC_ESTATUS_CECOBAN'];
			
			 // Aunque es muy improbable, se agrega la siguiente validacion para validar la perdida de sincronia
         if( claveCuentaValidar != claveCuenta  ){
         	Ext.Msg.alert(
					'Mensaje',
					'Se perdi� la sincron�a, la operaci�n ha sido cancelada.',
					function(btn, text){
						Ext.Ajax.request({
								url: 		'15consulta02ext.data.jsp',
								params: 	{
									accion:			'EliminarCuenta',
									operacion:		'Fin',
									claveCuenta: 	null,
									rowIndex:		null
								},
								callback: 			procesaEliminarCuenta
						});
						
					}
				);
				return;
         }
         
			// Si la cuenta se encuentra con estatus 99, como ya se ha preguntado previamente
			// no volver a preguntar y pasar a eliminar directamente la cuenta
			if( estatusCecoban == "99" ){
				
				Ext.Ajax.request({
					url: 		'15consulta02ext.data.jsp',
					params: 	{
						accion:			'EliminarCuenta',
						operacion:		'Eliminar',
						claveCuenta: 	claveCuenta,
						rowIndex:		rowIndex
					},
					callback: 			procesaEliminarCuenta
				});
				
			} else {
				
				Ext.MessageBox.confirm(
					'Confirmar', 
					'�Esta seguro de querer eliminar el registro?',
					function(boton) {
													
						if (boton == 'yes') { 
								
							Ext.Ajax.request({
								url: 		'15consulta02ext.data.jsp',
								params: 	{
									accion:			'EliminarCuenta',
									operacion:		'Eliminar',
									claveCuenta: 	claveCuenta,
									rowIndex:		rowIndex
								},
								callback: 			procesaEliminarCuenta
							});
											
						} else if (boton == 'no') {
							
							Ext.Ajax.request({
								url: 		'15consulta02ext.data.jsp',
								params: 	{
									accion:			'EliminarCuenta',
									operacion:		'Fin',
									claveCuenta: 	claveCuenta,
									rowIndex:		rowIndex
								},
								callback: 			procesaEliminarCuenta
							});
							
						}
														
				});
				
			}
				
		} else if(  estado == "ACTUALIZAR_GRID"                  ){
 
			var record 					= Ext.StoreMgr.key('cuentasConsultadasDataStore').getAt(rowIndex);
			var claveCuenta			= record.json['IC_CUENTA'];
			
			 // Aunque es muy improbable, se agrega la siguiente validacion para validar la perdida de sincronia
         if( claveCuentaValidar != claveCuenta  ){
         	Ext.Msg.alert(
					'Mensaje',
					'Se perdi� la sincron�a, la operaci�n ha sido cancelada.',
					function(btn, text){
						Ext.Ajax.request({
								url: 		'15consulta02ext.data.jsp',
								params: 	{
									accion:			'EliminarCuenta',
									operacion:		'Fin',
									claveCuenta: 	null,
									rowIndex:		null
								},
								callback: 			procesaEliminarCuenta
						});
						
					}
				);
				return;
         }
         
         // Actualizar "pagina" actual para que se refleje que se ha eliminado la cuenta
			cuentasConsultadasData.load({
				params: {
					operacion: 	'NuevaConsulta'
				}
			});
			
			Ext.getCmp('botonGenerarArchivoCSV').disable();
			Ext.getCmp('botonBajarArchivoCSV').hide();
			Ext.getCmp('botonImprimirPDF').disable();
			Ext.getCmp('botonBajarPDF').hide();
			
			Ext.Ajax.request({
				url: 		'15consulta02ext.data.jsp',
				params: 	{
					accion:			'EliminarCuenta',
					operacion:		'Fin',
					claveCuenta: 	claveCuenta,
					rowIndex:		null // Debido a que el registro fue eliminado
				},
				callback: 			procesaEliminarCuenta
			});
			
		} else if(  estado == "FIN" ){
			
			return;
			
		}
		
		return;
		
	}
	
	//-------------------------------- STORES --------------------------------
	
	var catalogoTipoAfialiadoData = new Ext.data.JsonStore({
		id: 			'catalogoTipoAfialiadoDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15consulta02ext.data.jsp',
		baseParams: {
			accion: 'CatalogoTipoAfiliado'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load: function(store,records,options){
				
								// Leer valor default
								var defaultValue 		= null;
								var existeParametro	= true;
								try {
									defaultValue = String(options.params.defaultValue);
								}catch(err){
									existeParametro	= false;
									defaultValue 		= null;
								}
								
								// Si se especific� un valor por default
								if( existeParametro ){ 
									
									var index = store.findExact( 'clave', defaultValue ); 
									// El registro fue encontrado por lo que hay que seleccionarlo
									if (index >= 0){
										Ext.getCmp("cmbTipoAfiliado").setValue(defaultValue);
									}
									Ext.getCmp("cmbTipoAfiliado").setReadOnly(true);
									
								} 
								
							},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	var catalogoProductoData = new Ext.data.JsonStore({
		id: 			'catalogoProductoDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15consulta02ext.data.jsp',
		baseParams: {
			accion: 'CatalogoProducto'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load: function(store,records,options){
				
								// Leer valor default
								var defaultValue 		= null;
								var existeParametro	= true;
								try {
									defaultValue = String(options.params.defaultValue);
								}catch(err){
									existeParametro	= false;
									defaultValue 		= null;
								}
								
								// Si se especific� un valor por default
								if( existeParametro ){ 
									
									var index = store.findExact( 'clave', defaultValue ); 
									// El registro fue encontrado por lo que hay que seleccionarlo
									if (index >= 0){
										Ext.getCmp("cmbProducto").setValue(defaultValue);
									}
									Ext.getCmp("cmbProducto").setReadOnly(true);
									
								} 
								
							},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
 
	var decodeClaveMoneda = function(value, record){
		
		var descripcionMoneda = "";
		
		if(Ext.isEmpty(catalogoMoneda)){
			
			descripcionMoneda = "Error: No se pudo leer el Catalogo de Monedas";
			
		}else{
			
			for(var i=0;i<catalogoMoneda.length;i++){
				
				if( catalogoMoneda[i].clave == value){
					descripcionMoneda = catalogoMoneda[i].descripcion;
					break;
				}
				
			}
			
		}
		
		return descripcionMoneda;
		
	}
	
	var decodeCuentaClabe  			= function(value, record){
		
		var respuesta = null;
		if( "true" == record['ES_CUENTA_DOLARES'] ){
			respuesta = 'N/A';
		}else{
			respuesta = value;
		}
		return respuesta;
		
	}
	
	var decodeCuentaSwift  			= function(value, record){
		
		var respuesta = null;
		if( "true" == record['ES_CUENTA_DOLARES'] ){
			respuesta = value;
		}else{
			respuesta = 'N/A';
		}
		return respuesta;
		
	}
	
	var decodePlazaSwift  			= function(value, record){
		
		var respuesta = null;
		if( "true" == record['ES_CUENTA_DOLARES'] ){
			respuesta = value;
		}else{
			respuesta = 'N/A';
		}
		return respuesta;
		
	}
	
	var decodeSucursalSwift  		= function(value, record){
		
		var respuesta = null;
		if( "true" == record['ES_CUENTA_DOLARES'] ){
			respuesta = value;
		}else{
			respuesta = 'N/A';
		}
		return respuesta;
		
	}
	
	var decodeDescripcionTef  		= function(value, record){
		
		var respuesta = null;
		if( "true" == record['ES_CUENTA_DOLARES'] ){
			respuesta = 'N/A';
		}else{
			respuesta = value;
		}
		return respuesta;
		
	}
	
	var decodeDescripcionCecoban  = function(value, record){
		
		var respuesta = null;
		if( "true" == record['ES_CUENTA_DOLARES'] ){
			respuesta = 'N/A';
		}else{
			respuesta = value;
		}
		return respuesta;
		
	}
	
	var decodeDescripcionDolares  = function(value, record){
		
		var respuesta = null;
		if( "true" == record['ES_CUENTA_DOLARES'] ){
			if( value == "" ){
				respuesta = "Sin Validar";
			}else{
				respuesta = value;
			}
		}else{
			respuesta = 'N/A';
		}
		return respuesta;
		
	}
 
	var cuentasConsultadasData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'cuentasConsultadasDataStore',
		url: 		'15consulta02ext.data.jsp',
		baseParams: {
			accion: 'ConsultaCuentas'
		},
		fields: [
			{name: 'TIPO_AFILIADO'					,mapping: 'TIPOAFILIADO'	,convert: function(value, record){ return (value == "P" ? "Proveedor" : "");} },
			{name: 'PRODUCTO'							},
			{name: 'NOMBRE'							},
			{name: 'BANCO'								},
			{name: 'CUENTA_CLABE'					,mapping: 'CUENTACLABE'		,convert: decodeCuentaClabe  			},
			{name: 'CUENTA_SWIFT'					,mapping: 'CUENTASWIFT' 	,convert: decodeCuentaSwift  			},
			{name: 'PLAZA_SWIFT'															,convert: decodePlazaSwift   			},
			{name: 'SUCURSAL_SWIFT'														,convert: decodeSucursalSwift 		},
			{name: 'MONEDA'							,mapping: 'TIPO_MONEDA'		,convert: decodeClaveMoneda 			},
			{name: 'NOMBRE_EPO'						,mapping: 'EPOREL' 															},
			{name: 'DESCRIPCION_TEF'				,mapping: 'ESTATUS_TEF' 	,convert: decodeDescripcionTef		},
			{name: 'DESCRIPCION_CECOBAN'			,mapping: 'ESTATUS_CECOBAN',convert: decodeDescripcionCecoban	},
			{name: 'DESCRIPCION_DOLARES'			,mapping: 'ESTATUS_DOLARES',convert: decodeDescripcionDolares	},
			{name: 'FECHA_ULTIMA_ACTUALIZACION'	,mapping: 'FECHA_ULT_MODSS',convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd-m-Y h:i:s')); } },
			{name: 'USUARIO'							},
			{name: 'SELECCIONAR'						}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload:	{
				fn: function(store, options){
					/*
					console.log("options.params(beforeload)");
					console.dir(options.params);
					*/
				}
			}, 
			load: 	procesarConsultaCuentas,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCuentas(null, null, null);						
				}
			}
		}
		
	});
 
	//------------------------------- FORMA BUSQUEDA AVANZADA ---------------------------
	
	var resetFormaBusquedaAvanzada = function(){
		
		var fpBusquedaAvanzada = Ext.getCmp('busquedaAvanzada');
		fpBusquedaAvanzada.getForm().reset();
		
		var busquedaAvanzadaData = Ext.StoreMgr.key('busquedaAvanzadaDataStore');
		Ext.Ajax.abort(busquedaAvanzadaData.proxy.getConnection().transId);
		busquedaAvanzadaData.removeAll();
		
		// Deshabilitar boton aceptar
		Ext.getCmp("botonAceptar").disable();
		
		// Poner mensaje de empty text por default
		var cmbPyme = Ext.getCmp('cmbPyme1');
		cmbPyme.emptyText =  "Seleccione...";
		cmbPyme.setRawValue('');
		cmbPyme.applyEmptyText();
		cmbPyme.setDisabled(true);
		
		// Habilitar boton de Busqueda
		var botonBuscar = Ext.getCmp("botonBuscar");
		botonBuscar.enable();
		botonBuscar.setIconClass('iconoLupa');
		
	}
	
	var busquedaAvanzadaData = new Ext.data.JsonStore({
		id: 					'busquedaAvanzadaDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion','loadMsg'],
		url: 					'15consulta02ext.data.jsp',
		baseParams: {
			accion: 			'BusquedaAvanzada'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					// Remover animacion de carga
					var busquedaAvanzadaData = Ext.StoreMgr.key('busquedaAvanzadaDataStore');
					busquedaAvanzadaData.removeAll();		
					// Poner mensaje de empty text por default
					var cmbPyme = Ext.getCmp('cmbPyme1');
					cmbPyme.emptyText =  "Seleccione...";
					cmbPyme.setRawValue('');
					cmbPyme.applyEmptyText();
					cmbPyme.setDisabled(true);
					// Habilitar boton de Busqueda
					var botonBuscar = Ext.getCmp("botonBuscar");
					botonBuscar.enable();
					botonBuscar.setIconClass('iconoLupa');					
				}
			},
			beforeload:		NE.util.initMensajeCargaCombo,
			load:				function(store,records,options){
				
				// Verificar que el load realmente sea ocasionado por una busqueda del usuario
				// y no como consecuencia de la carga que se realiza para mostrar la animacion
				// de carga de datos.
				var busquedaAvanzadaData 	= false;
				try {
					busquedaAvanzadaData 	= options.params.busquedaAvanzadaData;
				}catch(err){
					busquedaAvanzadaData		= false;
				}
								
				// La busqueda es aut�ntica
				if( busquedaAvanzadaData ){ 
					
					// El registro fue encontrado por lo que hay que seleccionarlo
					var myCombo = Ext.getCmp("cmbPyme1");
					if ( store.getTotalCount() == 0){
						myCombo.emptyText =  "No existe informaci�n con los criterios determinados.";
						myCombo.setRawValue('');
						myCombo.applyEmptyText();
						Ext.getCmp('botonAceptar').setDisabled(true);
					} else {
						/*myCombo.emptyText =  "Seleccione...";
						myCombo.setRawValue('');
						myCombo.applyEmptyText();*/
						Ext.getCmp('botonAceptar').setDisabled(false);
					}
					
					// Habilitar boton de Busqueda
					var botonBuscar = Ext.getCmp("botonBuscar");
					botonBuscar.enable();
					botonBuscar.setIconClass('iconoLupa');
					
				}
			}
			
		}
		
	});
	
	var elementosFormaBusquedaAvanzada = [
		{
			xtype: 				'panel',
			id: 					'forma2',
			width: 				600,			
			frame: 				true,		
			titleCollapse: 	false,
			bodyStyle: 			'padding: 6px',			
			defaultType: 		'textfield',
			align: 				'center',
			html: 				'Utilice el * para realizar una b�squeda gen�rica.'
		},
		NE.util.getEspaciador(20),
		{ 
			xtype: 		'textfield',
			name: 		'nombrePyme',
			id: 			'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 	100,	
			width: 		80,
			msgTarget: 	'side',
			margins: 	'0 20 0 0'  
		},
		{ 
			xtype: 		'textfield',
			name: 		'rfcPyme',
			id: 			'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 	15,	
			width: 		80,
			msgTarget: 	'side',
			margins: 	'0 20 0 0'  
		},	
		{ 
			xtype: 		'textfield',
			name: 		'numeroPyme',
			id: 			'numeroPyme1',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 	25,	
			width: 		20,
			msgTarget: 	'side',
			margins: 	'0 20 0 0'  
		},	
		{
			xtype: 		'panel',
			items: [
				{
					xtype: 		'button',
					width: 		80,
					height:		10,
					text: 		'Buscar',
					iconCls: 	'iconoLupa',
					name: 		'botonBuscar',
					id: 			'botonBuscar',
					style: 		'float:right',
					handler:    function(boton, evento) {
								
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							var nombrePyme	= Ext.getCmp("nombrePyme1");
							var rfcPyme		= Ext.getCmp("rfcPyme1");
							var numeroPyme	= Ext.getCmp("numeroPyme1");
							 
							/*
							// Nota: Esta validacion no es necesaria
							if( 
									Ext.isEmpty(nombrePyme.getValue())  		&&  
									Ext.isEmpty(rfcPyme.getValue())				&&
									Ext.isEmpty(numeroPyme.getValue())		
								){
								nombrePyme.markInvalid('Es necesario que ingrese datos en al menos un campo');						
								return;
							}
							*/
							 
							var cmbPyme = Ext.getCmp('cmbPyme1');
							cmbPyme.emptyText =  "Seleccione...";
							cmbPyme.setRawValue('');
							cmbPyme.applyEmptyText();
							cmbPyme.setDisabled(false);
							
							var botonAceptar = Ext.getCmp('botonAceptar');
							botonAceptar.setDisabled(true);
							
							var busquedaAvanzada = Ext.getCmp("busquedaAvanzada");
							busquedaAvanzadaData.load({
									params: Ext.apply(busquedaAvanzada.getForm().getValues(),
									{
										busquedaAvanzadaData: true
									}
								)
							});
							
						}						
				}
			]
		},	
		NE.util.getEspaciador(5),
		{
			xtype: 				'combo',
			name: 				'cmbPyme',
			id: 					'cmbPyme1',
			mode: 				'local',
			autoLoad: 			false,
			displayField: 		'descripcion',
			emptyText: 			'Seleccione...',
			valueField: 		'clave',
			hiddenName: 		'clavePyme1',
			fieldLabel: 		'Nombre',
			disabled: 			true,
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				busquedaAvanzadaData,
			tpl:					NE.util.templateMensajeCargaCombo
		}
	];
 
	var fpBusquedaAvanzada = new Ext.form.FormPanel({
		id: 					'busquedaAvanzada',
		width: 				500,
		title: 				'B�squeda Avanzada',
		frame: 				true,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 6px',
		labelWidth: 		110,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		items: 				elementosFormaBusquedaAvanzada,
		monitorValid: 		false, // true,
		buttons: [
			{
				text: 		'Aceptar',
				name:			'botonAceptar',
				id:			'botonAceptar',
				iconCls: 	'aceptar',
				formBind: 	true,
				disabled: 	true,
				align: 		'center',
				handler: 
					function(boton, evento){
						
						var cmbPyme							= Ext.getCmp("cmbPyme1");
 
						if (Ext.isEmpty(cmbPyme.getValue())) {
							cmbPyme.markInvalid('Seleccione una Pyme');
							return;
						}
 
						var numeroNafinElectronico = Ext.getCmp('numeroNafinElectronico');
						/*
						var clavePyme					= Ext.getCmp('clavePyme');
						var nombre						= Ext.getCmp('nombreProveedor');
						var rfc							= Ext.getCmp('rfcProveedor');
						*/
						busquedaAvanzadaData.each(function(record){
							if(record.data['clave'] == cmbPyme.getValue()){
								numeroNafinElectronico.setValue(	record.json.nafin_electronico	);
								/*
								clavePyme.setValue(					cmbPyme.getValue()				);
								nombre.setValue(						record.json.nombre_pyme			); 
								rfc.setValue(							record.json.rfc					);
								*/
								return;
							}
						});
 
						var ventana 	= Ext.getCmp('windowBusquedaAvanzada');
						resetFormaBusquedaAvanzada();
						ventana.hide();
						
						numeroNafinElectronicoOnBlurHandler(numeroNafinElectronico);
						
					}
					
			},
			{
				text: 		'Cancelar',
				iconCls: 	'icoLimpiar',
				handler: 
					function() {					
						var ventana = Ext.getCmp('windowBusquedaAvanzada');
						resetFormaBusquedaAvanzada();
						ventana.hide();					
					}				
			}
		]
	});
 
	//----------------------- FORMA CONFIRMAR CONTRASE�A -----------------
	
	var elementosFormaContrasena = [
		// INPUT TEXT CLAVE USUARIO
		{
			xtype: 				'textfield',
			name: 				'claveUsuario',
			id: 					'claveUsuario',
			fieldLabel: 		'Clave de Usuario',
			allowBlank: 		true,
			maxLength: 			8,	
			width: 				80,
			msgTarget: 			'side',
			//anchor:			'95%',
			margins: 			'0 20 0 0'  
		},
		// INPUT TEXT CONTRASE�A
		{
			xtype: 				'textfield',
			name: 				'contrasena',
			id: 					'contrasena',
			fieldLabel: 		'Contrase�a',
			inputType: 			'password',
			allowBlank: 		true,
			maxLength: 			8,	
			width: 				80,
			msgTarget: 			'side',
			//anchor:			'95%',
			margins: 			'0 20 0 0'  
		},	
		// INPUT HIDDEN INDICE REGISTRO - ROW INDEX DE LA CUENTA SELECCIONADA
		{  
        xtype:	'hidden',  
        name:	'indiceRegistro',
        id: 	'indiceRegistro'
      }
			
	];
	
	var fpConfirmacionClave = new Ext.form.FormPanel({
		id: 					'confirmacionClave',
		width: 				244,
		height:				160,
		title: 				'Confirmaci�n de Clave',
		frame: 				true,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 6px',
		labelWidth: 		100,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		items: 				elementosFormaContrasena,
		monitorValid: 		false,
		buttons: [
			{
				xtype: 		'button',
				width: 		80,
				height:		10,
				text: 		'Aceptar',
				iconCls: 	'icoAceptar',
				id: 			'botonAceptarContrasena',
				style: 		'float:right',
				handler:    function(boton, evento) {
					
					var claveUsuario = Ext.getCmp("claveUsuario").getValue();
					if(Ext.isEmpty(claveUsuario)){
						Ext.getCmp("claveUsuario").markInvalid("Este campo es requerido");
						return;
					}
					
					var contrasena	  = Ext.getCmp("contrasena").getValue();
					if(Ext.isEmpty(contrasena)){
						Ext.getCmp("contrasena").markInvalid("Este campo es requerido");
						return;
					}
					
					var rowIndex 		= Ext.getCmp("indiceRegistro").getValue();
					var record 			= Ext.StoreMgr.key('cuentasConsultadasDataStore').getAt(rowIndex);
					var claveCuenta	= record.json['IC_CUENTA'];
			
					Ext.Ajax.request({
						url: 		'15consulta02ext.data.jsp',
						params: 	{
							accion:				'EliminarCuenta',
							operacion:			'ValidarUsuario',
							claveCuenta: 		claveCuenta,
							rowIndex:			rowIndex,
							claveUsuario:		claveUsuario,
							contrasena:			contrasena
						},
						callback: 				procesaEliminarCuenta
					});
					
					var ventana = Ext.getCmp('windowConfirmacionClave');
					resetWindowConfirmacionClave();
					ventana.hide();
					
				}
			},
			{
				xtype: 		'button',
				width: 		80,
				height:		10,
				text: 		'Cancelar',
				iconCls: 	'icoLimpiar',
				id: 			'botonCancelar',
				style: 		'float:right',
				handler:    function(boton, evento) {
					var ventana = Ext.getCmp('windowConfirmacionClave');
					resetWindowConfirmacionClave();
					ventana.hide();	
				}
			}
		]
	});
						
	//----------------------------- FORMA MODIFICAR CUENTA ---------------------------
	
	var resetFormaModificarCuenta 		= function(){
		
		// Poner en "ceros" todos los campos
		Ext.getCmp('claveTipoAfiliadoMC').setValue(			'' );
      Ext.getCmp('cmbProductoMC').setValue(					'' );
      Ext.getCmp('numeroNafinElectronicoMC').setValue(	''	);
      Ext.getCmp('nombreProveedorMC').setValue(				'' );
      Ext.getCmp('rfcProveedorMC').setValue(					'' );
      Ext.getCmp('cmbMonedaMC').setValue(						'' );
      Ext.getCmp('cmbBancoMC').setValue(						'' );
      Ext.getCmp('cuentaClabeMC').setValue(					'' );
      Ext.getCmp('cuentaSwiftMC').setValue(					'' );
      Ext.getCmp('plazaSwiftMC').setValue(					'' );
      Ext.getCmp('sucursalSwiftMC').setValue(				'' );
      Ext.getCmp('tipoCuentaMC').setValue(					'' );
      Ext.getCmp('claveCuentaMC').setValue(					'' );
      
      // Quitar propiedad readonly
      Ext.getCmp("cmbProductoMC").setReadOnly(false);
      Ext.getCmp("cmbMonedaMC").setReadOnly(false);
      Ext.getCmp("cmbBancoMC").setReadOnly(false);
 
      Ext.getCmp("cmbProductoMC").getStore().removeAll();
      Ext.getCmp("cmbProductoMC").getStore().removeAll();
      Ext.getCmp("cmbProductoMC").getStore().removeAll();
       
      // Simular evento onCollapse en el campo moneda
      cuentaClabeMCOnCollapseHandler(Ext.getCmp('cmbMonedaMC'));
      
	}
	
	var procesaModificarCuenta = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			// Ocultar ventana de modificar cuenta
			var ventana = Ext.getCmp('windowModificarCuenta');
			resetFormaModificarCuenta();
			ventana.hide();
					
			// Refrescar "pagina" actual para que se reflejen los cambios realizados a la cuenta
			Ext.StoreMgr.key('cuentasConsultadasDataStore').load();	
			Ext.getCmp('botonGenerarArchivoCSV').disable();
			Ext.getCmp('botonBajarArchivoCSV').hide();
			Ext.getCmp('botonImprimirPDF').disable();
			Ext.getCmp('botonBajarPDF').hide();
						
			
		} else {
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var catalogoProductoMCData = new Ext.data.JsonStore({
		id: 			'catalogoProductoMCDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15consulta02ext.data.jsp',
		baseParams: {
			accion: 'CatalogoProducto'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load:			function(store,records,options){
 
								// Leer valor default
								var defaultValue 		= null;
								var existeParametro	= true;
								try {
									defaultValue = String(options.params.defaultValue);
								}catch(err){
									existeParametro	= false;
									defaultValue 		= null;
								}
								
								// Si se especific� un valor por default
								if( existeParametro ){ 
									var index = store.findExact( 'clave', defaultValue ); 
									// El registro fue encontrado por lo que hay que seleccionarlo
									if (index >= 0){
										Ext.getCmp("cmbProductoMC").setValue(defaultValue);
									}
									Ext.getCmp("cmbProductoMC").setReadOnly(true);
								} 
								
							},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMonedaMCData = new Ext.data.JsonStore({
		id: 			'catalogoMonedaMCDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15consulta02ext.data.jsp',
		baseParams: {
			accion: 'CatalogoMoneda'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load:			function(store,records,options){
				
								// Leer valor default
								var defaultValue 		= null;
								var existeParametro	= true;
								try {
									defaultValue = String(options.params.defaultValue);
								}catch(err){
									existeParametro	= false;
									defaultValue 		= null;
								}
								
								// Si se especific� un valor por default
								if( existeParametro ){ 
									
									var index = store.findExact( 'clave', defaultValue ); 
									// El registro fue encontrado por lo que hay que seleccionarlo
									if (index >= 0){
										Ext.getCmp("cmbMonedaMC").setValue(defaultValue);
									}
									Ext.getCmp("cmbMonedaMC").setReadOnly(true);
									// Simular evento onCollapse para mostrar los campos de las cuentas
									cuentaClabeMCOnCollapseHandler(Ext.getCmp('cmbMonedaMC'));
									
								} 
							},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoBancoMCData = new Ext.data.JsonStore({
		id: 			'catalogoBancoMCDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15consulta02ext.data.jsp',
		baseParams: {
			accion: 'CatalogoBanco'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load:			function(store,records,options){
								
								// Leer valor default
								var defaultValue 		= null;
								var existeParametro	= true;
								try {
									defaultValue = String(options.params.defaultValue);
								}catch(err){
									existeParametro	= false;
									defaultValue 		= null;
								}
								
								// Si se especific� un valor por default
								if( existeParametro ){ 
									var index = store.findExact( 'clave', defaultValue ); 
									// El registro fue encontrado por lo que hay que seleccionarlo
									if (index >= 0){
										Ext.getCmp("cmbBancoMC").setValue(defaultValue);
									}
								} 
								 
							},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var cuentaClabeMCOnCollapseHandler 	= function(field){
					
		var claveMoneda = field.getValue();
		if(			claveMoneda == 1  ){ // Pesos
						
			Ext.getCmp("cuentaClabeMC").setVisible(true);
			Ext.getCmp("cuentaSwiftMC").setVisible(false);
			Ext.getCmp("plazaSwiftMC").setVisible(false);
			Ext.getCmp("sucursalSwiftMC").setVisible(false);
						
		}else if(	claveMoneda == 54 ){ // Dolares Americanos
						
			Ext.getCmp("cuentaClabeMC").setVisible(false);
			Ext.getCmp("cuentaSwiftMC").setVisible(true);
			Ext.getCmp("plazaSwiftMC").setVisible(true);
			Ext.getCmp("sucursalSwiftMC").setVisible(true);
						
		}else{ // Cualquier otro caso
						
			Ext.getCmp("cuentaClabeMC").setVisible(false);
			Ext.getCmp("cuentaSwiftMC").setVisible(false);
			Ext.getCmp("plazaSwiftMC").setVisible(false);
			Ext.getCmp("sucursalSwiftMC").setVisible(false);
						
		}
		// Ext.Msg.alert('Status',""+field.getValue());
		
	}
	
	var cuentaClabeMCOnBlurHandler 		= function(field){
		soloNumeros(field,			'La Cuenta Clabe no es v�lida',40);
	}
	
	var cuentaSwiftMCOnBlurHandler 		= function(field){
		soloAlfaNumericos(field,	'La Cuenta Swift no es v�lida. S�lo se permiten caracteres alfanum�ricos.');
	}
	
	var plazaSwiftMCOnBlurHandler 		= function(field){
		soloAlfaNumericos(field,	'La Plaza Swift no es v�lida. S�lo se permiten caracteres alfanum�ricos.','-');
	}
	
	var sucursalSwiftMCOnBlurHandler 	= function(field){
		soloAlfaNumericos(field,	'La Sucursal Swift no es v�lida. S�lo se permiten caracteres alfanum�ricos.');
	}
 
	var elementosFormaModificarCuenta = [
		// TIPO DE AFILIADO
		{  
        xtype:	'hidden',  
        name:	'claveTipoAfiliadoMC',
        id: 	'claveTipoAfiliadoMC'
      },
		// COMBO PRODUCTO
		{
			xtype: 				'combo',
			name: 				'productoMC',
			id: 					'cmbProductoMC',
			fieldLabel: 		'Producto',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveProductoMC',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoProductoMCData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%'
		},
		// INPUT TEXT NUMERO NAFIN ELECTRONICO
		{
			xtype: 		'numberfield',
			name: 		'numeroNafinElectronicoMC',
			id: 			'numeroNafinElectronicoMC',
			readOnly: 	true,
			fieldLabel: 'No. Nafin Electr�nico',
			allowBlank: true,
			hidden: 		false,
			maxLength: 	15,	
			width: 		80,
			msgTarget: 	'side',
			anchor:		'95%',
			margins: 	'0 20 0 0'
		},
      // INPUT TEXT NOMBRE DEL PROVEEDOR
		{
			xtype: 				'textfield',
			name: 				'nombreProveedorMC',
			id: 					'nombreProveedorMC',
			readOnly: 			true,
			fieldLabel: 		'Nombre',
			allowBlank: 		true,
			maxLength: 			100,	
			width: 				80,
			msgTarget: 			'side',
			anchor:				'95%',
			margins: 			'0 20 0 0'  //necesario para mostrar el icono de error
		},
		// INPUT TEXT RFC
		{
			xtype: 				'textfield',
			name: 				'rfcProveedorMC',
			id: 					'rfcProveedorMC',
			readOnly: 			true,
			fieldLabel: 		'RFC',
			allowBlank: 		true,
			maxLength: 			15,	
			width: 				80,
			msgTarget: 			'side',
			anchor:				'95%',
			margins: 			'0 20 0 0'  //necesario para mostrar el icono de error
		},
		// COMBO MONEDA
		{
			xtype: 				'combo',
			name: 				'monedaMC',
			id: 					'cmbMonedaMC',
			fieldLabel: 		'Moneda',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveMonedaMC',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMonedaMCData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%',
			listeners:			{
				collapse: 		cuentaClabeMCOnCollapseHandler
			}
		},
		// COMBO BANCO
		{
			xtype: 				'combo',
			name: 				'bancoMC',
			id: 					'cmbBancoMC',
			fieldLabel: 		'Banco',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveBancoMC',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoBancoMCData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%'
		},
		// INPUT TEXT CUENTA CLABE
		{
			xtype: 				'textfield',
			name: 				'cuentaClabeMC',
			id: 					'cuentaClabeMC',
			fieldLabel: 		'Cuenta Clabe',
			hidden:				true,
			allowBlank: 		true,
			maxLength: 			18,	
			width: 				20,
			msgTarget: 			'side',
			anchor:				'95%',
			margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
			listeners:{
				blur: cuentaClabeMCOnBlurHandler
			}
		},
		// INPUT TEXT CUENTA SWIFT
		{
			xtype: 				'textfield',
			name: 				'cuentaSwiftMC',
			id: 					'cuentaSwiftMC',
			fieldLabel: 		'Cuenta Swift',
			hidden:				true,
			allowBlank: 		true,
			maxLength: 			20,	
			width: 				20,
			msgTarget: 			'side',
			anchor:				'95%',
			margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
			listeners:{
				blur: cuentaSwiftMCOnBlurHandler
			}
		},
		// INPUT TEXT PLAZA SWIFT
		{
			xtype: 				'textfield',
			name: 				'plazaSwiftMC',
			id: 					'plazaSwiftMC',
			fieldLabel: 		'Plaza Swift',
			hidden:				true,
			allowBlank: 		true,
			maxLength: 			20,	
			width: 				20,
			msgTarget: 			'side',
			anchor:				'95%',
			margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
			listeners:{
				blur: plazaSwiftMCOnBlurHandler
			}
		},
		// INPUT TEXT SUCURSAL SWIFT
		{
			xtype: 				'textfield',
			name: 				'sucursalSwiftMC',
			id: 					'sucursalSwiftMC',
			fieldLabel: 		'Sucursal Swift',
			hidden:				true,
			allowBlank: 		true,
			maxLength: 			20,	
			width: 				20,
			msgTarget: 			'side',
			anchor:				'95%',
			margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
			listeners:{
				blur: sucursalSwiftMCOnBlurHandler
			}
		},
		// INPUT HIDDEN: TIPO CUENTA
		{  
        xtype:	'hidden',  
        name:	'tipoCuentaMC',
        id: 	'tipoCuentaMC'
      },
      // INPUT HIDDEN: ID DE LA CUENTA
		{  
        xtype:	'hidden',  
        name:	'claveCuentaMC',
        id: 	'claveCuentaMC'
      }
      
	];
	
	var fpModificarCuenta = new Ext.form.FormPanel({
		id: 					'modificarCuenta',
		width: 				600,
		height:				335,
		title: 				'Modificar Cuenta',
		frame: 				true,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 6px',
		labelWidth: 		130,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		items: 				elementosFormaModificarCuenta,
		monitorValid: 		false,
		buttons: [
			{
				id:			'botonModificar',
				xtype: 		'button',
				width: 		80,
				height:		10,
				text: 		'Modificar',
				iconCls: 	'icoAceptar',
				style: 		'float:right',
				errorActivo: false,
				handler:    function(boton, evento) {
					
					var panelModificarCuenta = Ext.getCmp("modificarCuenta");
					Ext.getCmp("botonModificar").errorActivo = false;
					panelModificarCuenta.items.each(function(panelItem, index, totalCount){
							
						if(panelItem.xtype       == 'hidden'   ){
							//No hacer nada
						} else {
							if(!Ext.isEmpty(panelItem.getActiveError())){
								Ext.getCmp("botonModificar").errorActivo = true;
								return;
							}
						}
						
					});
 
					if(Ext.getCmp("botonModificar").errorActivo){
						//Ext.MessageBox.alert('Error','La forma tiene elementos inv�lidos, por favor verifique');
						return;
					}
					
					var numeroNafinElectronico = Ext.getCmp("numeroNafinElectronicoMC").getValue();
					var nombreProveedor			= Ext.getCmp("nombreProveedorMC").getValue();
					var rfcProveedor				= Ext.getCmp("rfcProveedorMC").getValue();
					
					nombreProveedor = trim(nombreProveedor);
					rfcProveedor	 = trim(rfcProveedor);
					Ext.getCmp("nombreProveedorMC").setValue(nombreProveedor);
					Ext.getCmp("rfcProveedorMC").setValue(rfcProveedor);
					
					if ( (!isdigit(numeroNafinElectronico)) || Ext.isEmpty(nombreProveedor) || Ext.isEmpty(rfcProveedor) ){
						Ext.getCmp("numeroNafinElectronicoMC").markInvalid('El No. Nafin Electr�nico no es v�lido');
						return;
					}
					
					var cmbProducto = Ext.getCmp("cmbProductoMC");
					if (Ext.isEmpty(cmbProducto.getValue())){
						cmbProducto.markInvalid('El Producto no es v�lido');
						return;
					}
					
					var cmbMoneda = Ext.getCmp("cmbMonedaMC");
					if (Ext.isEmpty(cmbMoneda.getValue()) ){
						cmbMoneda.markInvalid('La Moneda no es v�lida');
						return;
					}
					
					var cmbBanco = Ext.getCmp("cmbBancoMC");
					if ( Ext.isEmpty(cmbBanco.getValue()) ){
						cmbBanco.markInvalid('El Banco no es v�lido');
						return;
					} 
					
					var claveMoneda 		= cmbMoneda.getValue();
					var inputCuentaClabe	= Ext.getCmp("cuentaClabeMC")
					var cuentaClabe 		= trim(inputCuentaClabe.getValue());
					inputCuentaClabe.setValue(cuentaClabe);
					
					if ( (claveMoneda == 1) && Ext.isEmpty(cuentaClabe)){
						inputCuentaClabe.markInvalid('Debe introducir una Cuenta');
						return;
					}
					
					var inputCuentaSwift	= Ext.getCmp("cuentaSwiftMC")
					var cuentaSwift 		= trim(inputCuentaSwift.getValue());
					inputCuentaSwift.setValue(cuentaSwift);
						
					if ( (claveMoneda == 54) && Ext.isEmpty(cuentaSwift) ){
						inputCuentaSwift.markInvalid('Debe introducir una Cuenta');
						return;
					}
					
					var inputPlazaSwift	= Ext.getCmp("plazaSwiftMC")
					var plazaSwift 		= trim(inputPlazaSwift.getValue());
					inputPlazaSwift.setValue(plazaSwift);
					
					if ( (claveMoneda == 54) && Ext.isEmpty(plazaSwift) ){
							inputPlazaSwift.markInvalid('Debe especificar la Plaza Swift');
							return;
					}
					
					var inputSucursalSwift	= Ext.getCmp("sucursalSwiftMC")
					var sucursalSwift 		= trim(inputSucursalSwift.getValue());
					inputSucursalSwift.setValue(sucursalSwift);
					
					if ( (claveMoneda == 54) && Ext.isEmpty(sucursalSwift) ){
							inputSucursalSwift.markInvalid('Debe especificar la Sucursal Swift');
							return;
					}
					
					// Enviar parametros
					if(claveMoneda == 1 || claveMoneda == 54){
						
						// Si se ha seleccionado la opcion de moneda nacional, ignorar lo especificado
						// en la cuenta swift
						if(claveMoneda == 1){
							inputCuentaSwift.setValue("");
							inputPlazaSwift.setValue("");
							inputSucursalSwift.setValue("");
						}
									
						// Si se ha seleccionado la opcion de dolares americanos, ignorar lo que se haya especificado
						// en la cuentas clabe
						if(claveMoneda == 54){
							inputCuentaClabe.setValue("");
						}
									
						Ext.Ajax.request({
							url: 		'15consulta02ext.data.jsp',
							params: 	Ext.apply(
								panelModificarCuenta.getForm().getValues(),{
									accion: 'ModificarCuenta'
								}
							),
							callback: procesaModificarCuenta
						});
						
					}
 
				}
			},
			{
				xtype: 		'button',
				width: 		80,
				height:		10,
				text: 		'Cancelar',
				iconCls: 	'icoLimpiar',
				style: 		'float:right',
				handler:    function(boton, evento) {
					var ventana = Ext.getCmp('windowModificarCuenta');
					resetFormaModificarCuenta();
					ventana.hide();	
				}
			}
		]
	});
	
	//------------------------ ELEMENTOS DE LA FORMA PRINICIPAL ----------------------		
	var elementosFormaConsulta = [
		// COMBO TIPO DE AFILIADO
		{
			xtype: 				'combo',
			name: 				'tipoAfiliado',
			id: 					'cmbTipoAfiliado',
			fieldLabel: 		'Tipo de Afiliado',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveTipoAfiliado',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTipoAfialiadoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%'
		},
		// COMBO PRODUCTO
		{
			xtype: 				'combo',
			name: 				'producto',
			id: 					'cmbProducto',
			fieldLabel: 		'Producto',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveProducto',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoProductoData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%'
		},
		// INPUT TEXT NUMERO NAFIN ELECTRONICO
		{
			xtype: 				'compositefield',
			fieldLabel: 		'No. Nafin Electr�nico',			
			msgTarget: 			'side',
			id: 					'compositefield1',
			combineErrors: 	false,
			items: [
				{ 
					xtype: 		'numberfield',
					name: 		'numeroNafinElectronico',
					id: 			'numeroNafinElectronico',
					allowBlank: true,
					hidden: 		false,
					maxLength: 	15,	
					msgTarget: 	'side',
					margins: 	'0 20 0 0',  //necesario para mostrar el icono de error
					listeners:{
						blur: numeroNafinElectronicoOnBlurHandler
					}
					
				},
				{
					xtype: 		'button',
					hidden: 		false,
					text: 		'B�squeda Avanzada...',
					id: 			'botonBusquedaAvanzada',
					handler: 
						function(boton, evento) {	
							
							/*
							var cboMoneda = Ext.getCmp("cboMoneda1");
							if (Ext.isEmpty(cboMoneda.getValue())  ) {
								cboMoneda.markInvalid('Debe Seleccionar  el tipo de Moneda');					
								return;
							}
							*/
				
							var ventana = Ext.getCmp('windowBusquedaAvanzada');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									layout: 			'fit',
									width: 			500,
									height: 			330,
									minWidth: 		500,
									minHeight: 		330,
									id: 				'windowBusquedaAvanzada',
									closeAction: 	'hide',
									items: [
										fpBusquedaAvanzada
									],
									listeners: 		{
										hide: resetFormaBusquedaAvanzada
									}
								}).show();
							}
						}
				}
				
			]
			
		},	
		{  
        xtype:	'hidden',  
        name:	'clavePyme',
        id: 	'clavePyme'
      },
		// INPUT TEXT NOMBRE DEL PROVEEDOR
		{
			xtype: 				'textfield',
			name: 				'nombreProveedor',
			id: 					'nombreProveedor',
			readOnly: 			true,
			fieldLabel: 		'Nombre',
			allowBlank: 		true,
			maxLength: 			100,	
			msgTarget: 			'side',
			anchor:				'95%',
			//cls:				'x-item-disabled',
			margins: 			'0 20 0 0'  //necesario para mostrar el icono de error
		},
		// INPUT TEXT RFC
		{
			xtype: 				'textfield',
			name: 				'rfcProveedor',
			id: 					'rfcProveedor',
			fieldLabel: 		'RFC',
			allowBlank: 		true,
			maxLength: 			15,
			msgTarget: 			'side',
			anchor:				'42.5%',
			margins: 			'0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 						'compositefield',
			fieldLabel: 				'Fecha de Registro',
			combineErrors: 			false,
			msgTarget: 					'side',
			items: [
				{
					xtype: 				'datefield',
					name: 				'fechaRegistroDe',
					id: 					'fechaRegistroDe',
					allowBlank: 		true,
					startDay: 			0,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoFinFecha: 	'fechaRegistroA',
					margins: 			'0 20 0 0'  
				},
				{
					xtype: 				'displayfield',
					value: 				'al',
					width: 				20
				},
				{
					xtype: 				'datefield',
					name: 				'fechaRegistroA',
					id: 					'fechaRegistroA',
					allowBlank: 		true,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoInicioFecha: 'fechaRegistroDe',
					margins: 			'0 20 0 0'  
				}
			]
		},
		// CHECKBOX CUENTAS SIN VALIDAR
		{
			xtype: 			'checkbox',
			name: 			'cuentasSinValidar',
			id: 				'cuentasSinValidar',
			fieldLabel: 	'Cuentas Sin Validar',
			inputValue:		'S'
		}
		
	];
	
	var panelForma = new Ext.form.FormPanel({
		id: 				'panelForma',
		width: 			700,
		title: 			'Consulta de Cuentas',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	130,
		defaultType: 	'textfield',
		items: 			elementosFormaConsulta,
		monitorValid: 	false,
		buttons: [
			/*
			// BOTON AGREGAR CUENTA
			{
				id:		'botonAgregar',
				text: 	'Agregar',
				hidden: 	false,
				iconCls: 'aceptar',
				errorActivo: false, 
				handler: function() {
					
					
					var panelForma = Ext.getCmp("panelForma");
					Ext.getCmp("botonAgregar").errorActivo = false;
					panelForma.items.each(function(panelItem, index, totalCount){
							
						if(panelItem.xtype       == 'hidden'   ){
							//No hacer nada
						}else if(panelItem.xtype == 'compositefield'){
							panelItem.items.each(function(subPanelItem, index, totalCount){
									if(!Ext.isEmpty(subPanelItem.getActiveError())){
										Ext.getCmp("botonAgregar").errorActivo = true;
										return;
									}
							});                          
							if(Ext.getCmp("botonAgregar").errorActivo){
								return;
							}
						}else{
							if(!Ext.isEmpty(panelItem.getActiveError())){
								Ext.getCmp("botonAgregar").errorActivo = true;
								return;
							}
						}
						
					});
					
					
					if(Ext.getCmp("botonAgregar").errorActivo){
						//Ext.MessageBox.alert('Error','La forma tiene elementos inv�lidos, por favor verifique');
						return;
					}
					
					var cmbTipoAfiliado = Ext.getCmp("cmbTipoAfiliado");
					if ( Ext.isEmpty(cmbTipoAfiliado.getValue()) ){
						cmbTipoAfiliado.markInvalid('Debe seleccionar un tipo de afiliado');
						return;
					}
					
					var numeroNafinElectronico = Ext.getCmp("numeroNafinElectronico").getValue();
					var nombreProveedor			= Ext.getCmp("nombreProveedor").getValue();
					var rfcProveedor				= Ext.getCmp("rfcProveedor").getValue();	
					if ( (!isdigit(numeroNafinElectronico)) || Ext.isEmpty(nombreProveedor) || Ext.isEmpty(rfcProveedor) ){
						Ext.getCmp("numeroNafinElectronico").markInvalid('El No. Nafin Electr�nico no es v�lido');
						return;
					}
					
					var cmbProducto = Ext.getCmp("cmbProducto");
					if (Ext.isEmpty(cmbProducto.getValue())){
						cmbProducto.markInvalid('El Producto no es v�lido');
						return;
					}
					
					var tipoAfiliado = cmbTipoAfiliado.getValue();
					if ( tipoAfiliado == "I" || tipoAfiliado =="B" || tipoAfiliado == "P" ){
							var cmbEPO = Ext.getCmp("cmbEPO");
							if( Ext.isEmpty(cmbEPO.getValue()) ){
								cmbEPO.markInvalid('Debe Seleccionar una EPO');
								return;
							}
					}  
					
					var cmbMoneda = Ext.getCmp("cmbMoneda");
					if (Ext.isEmpty(cmbMoneda.getValue()) ){
							cmbMoneda.markInvalid('La Moneda no es v�lida');
							return;
					}
					
					var cmbBanco = Ext.getCmp("cmbBanco");
					if ( Ext.isEmpty(cmbBanco.getValue()) ){
							cmbBanco.markInvalid('El Banco no es v�lido');
							return;
					} 
					
					var claveMoneda 		= cmbMoneda.getValue();
					var inputCuentaClabe	= Ext.getCmp("cuentaClabe")
					var cuentaClabe 		= trim(inputCuentaClabe.getValue());
					inputCuentaClabe.setValue(cuentaClabe);
					
					if ( (claveMoneda == 1) && Ext.isEmpty(cuentaClabe)){
							inputCuentaClabe.markInvalid('Debe introducir una Cuenta al menos');
							return;
					}
					
					var inputCuentaSwift	= Ext.getCmp("cuentaSwift")
					var cuentaSwift 		= trim(inputCuentaSwift.getValue());
					inputCuentaSwift.setValue(cuentaSwift);
						
					if ( (claveMoneda == 54) && Ext.isEmpty(cuentaSwift) ){
							inputCuentaSwift.markInvalid('Debe introducir una Cuenta');
							return;
					}
					
					var inputPlazaSwift	= Ext.getCmp("plazaSwift")
					var plazaSwift 		= trim(inputPlazaSwift.getValue());
					inputPlazaSwift.setValue(plazaSwift);
					
					if ( (claveMoneda == 54) && Ext.isEmpty(plazaSwift) ){
							inputPlazaSwift.markInvalid('Debe especificar la Plaza Swift');
							return;
					}
					
					var inputSucursalSwift	= Ext.getCmp("sucursalSwift")
					var sucursalSwift 		= trim(inputSucursalSwift.getValue());
					inputSucursalSwift.setValue(sucursalSwift);
					
					if ( (claveMoneda == 54) && Ext.isEmpty(sucursalSwift) ){
							inputSucursalSwift.markInvalid('Debe especificar la Sucursal Swift');
							return;
					}
						
					if(claveMoneda == 1 || claveMoneda == 54){
						
						Ext.MessageBox.confirm(
							'Confirmar', 
							'�Est� seguro de querer registrar la cuenta?',
							function(boton) {
								
								if (boton == 'yes') {
									
									var claveMoneda 		= cmbMoneda.getValue();
									// Si se ha seleccionado la opcion de moneda nacional, ignorar lo especificado
									// en la cuenta swift
									if(claveMoneda == 1){
										inputCuentaSwift.setValue("");
										inputPlazaSwift.setValue("");
										inputSucursalSwift.setValue("");
									}
									
									// Si se ha seleccionado la opcion de dolares americanos, ignorar lo que se haya especificado
									// en la cuentas clabe
									if(claveMoneda == 54){
										inputCuentaClabe.setValue("");
									}
									
									var panelForma = Ext.getCmp("panelForma");
									Ext.Ajax.request({
										url: 		'15consulta02ext.data.jsp',
										params: 	Ext.apply(
											panelForma.getForm().getValues(),{
												accion: 'AgregarCuenta'
											}
										),
										callback: procesaAgregarCuenta
									});
					
								} else if (boton == 'no') {
									return;
								}
								
							});
						
					}
						
					
			
				}
				
			},
			// BOTON CANCERLAR
			{
				text: 'Cancelar',
				hidden: false,
				iconCls: 'borrar',
				handler: function() {
					
					Ext.getCmp("panelForma").getForm().reset();
					
					Ext.getCmp("cmbTipoAfiliado").setValue("P");
					Ext.getCmp("cmbProducto").setValue("1");
					var catalogoEPOData = Ext.StoreMgr.key("catalogoEPODataStore");
					Ext.getCmp("cmbEPO").setValue(catalogoEPOData.getRange()[0].data['clave']);
					
					
					//No es necesario ocultar el grid
					//Ext.getCmp("gridCuentasAgregadas").hide();
				
				}
				
			},
			*/
			// BOTON CONSULTAR
			{
				id:				'botonConsultar',
				text: 			'Consultar',
				hidden: 			false,
				errorActivo:	false,
				iconCls: 		'icoBuscar',
				handler: 		function() {
					
					var panelForma = Ext.getCmp("panelForma");
					Ext.getCmp("botonConsultar").errorActivo = false;
					panelForma.items.each(function(panelItem, index, totalCount){
							
						if(panelItem.xtype       == 'hidden'   ){
							//No hacer nada
						}else if(panelItem.xtype == 'compositefield'){
							panelItem.items.each(function(subPanelItem, index, totalCount){
									if(!Ext.isEmpty(subPanelItem.getActiveError())){
										Ext.getCmp("botonConsultar").errorActivo = true;
										return;
									}
							});                          
							if(Ext.getCmp("botonConsultar").errorActivo){
								return;
							}
						}else{
							if(!Ext.isEmpty(panelItem.getActiveError())){
								Ext.getCmp("botonConsultar").errorActivo = true;
								return;
							}
						}
						
					});
					
					if(Ext.getCmp("botonConsultar").errorActivo){
						//Ext.MessageBox.alert('Error','La forma tiene elementos inv�lidos, por favor verifique');
						// Se cancela la consulta debido a que la forma tiene elementos invalidos
						return;
					}
					
					var cmbTipoAfiliado = Ext.getCmp("cmbTipoAfiliado");
					if ( Ext.isEmpty(cmbTipoAfiliado.getValue()) ){
						cmbTipoAfiliado.markInvalid('Debe seleccionar un tipo de afiliado');
						return;
					}
					
					var numeroNafinElectronico = Ext.getCmp("numeroNafinElectronico").getValue();
					if(!Ext.isEmpty(numeroNafinElectronico)){
						var nombreProveedor			= Ext.getCmp("nombreProveedor").getValue();
						var rfcProveedor				= Ext.getCmp("rfcProveedor").getValue();	
						if ( (!isdigit(numeroNafinElectronico)) || Ext.isEmpty(nombreProveedor) ){ // || Ext.isEmpty(rfcProveedor)
							Ext.getCmp("numeroNafinElectronico").markInvalid('El No. Nafin Electr�nico no es v�lido');
							return;
						}
					}
					
					var cmbProducto = Ext.getCmp("cmbProducto");
					if (Ext.isEmpty(cmbProducto.getValue())){
						cmbProducto.markInvalid('El Producto no es v�lido');
						return;
					}
					 
					var cuentasConsultadasData = Ext.StoreMgr.key("cuentasConsultadasDataStore");
					cuentasConsultadasData.load({
						params: Ext.apply(Ext.getCmp("panelForma").getForm().getValues(),{
							operacion: 	'NuevaConsulta', //Generar datos para la consulta
							start: 		0,
							limit: 		15,
							// Debido a un bug en el env�o de par�metros de los checkbox, se realiza lo siguiente:
							cuentasSinValidar: (Ext.getCmp("cuentasSinValidar").getValue()?Ext.getCmp("cuentasSinValidar").inputValue:"")
						})
					});
					Ext.getCmp('botonGenerarArchivoCSV').disable();
					Ext.getCmp('botonBajarArchivoCSV').hide();
					Ext.getCmp('botonImprimirPDF').disable();
					Ext.getCmp('botonBajarPDF').hide();
 
				}
			},
			// BOTON LIMPIAR
			{
				text: 		'Limpiar',
				hidden: 		false,
				iconCls: 	'icoLimpiar',
				handler: 	function() {
					window.location = '15consulta02ext.jsp';
				}
			}
		]
	});
	
	// Grid con las cuentas registradas
	var grid = new Ext.grid.GridPanel({
		store: 	cuentasConsultadasData,
		id:		'gridCuentasConsultadas',
		hidden: 	true,
		margins: '20 0 0 0',
		title: 	'Cuentas Registradas',
		columns: [
			{
				header: 		'Tipo de Afiliado',
				tooltip: 	'Tipo de Afiliado',
				dataIndex: 	'TIPO_AFILIADO',
				sortable: 	true,
				resizable: 	true,
				width: 		90,
				hidden: 		false
			},
			{
				header: 		'Producto',
				tooltip: 	'Producto',
				dataIndex: 	'PRODUCTO',
				sortable: 	true,
				resizable: 	true,
				width: 		120,
				hidden: 		false
			},
			{
				header: 		'Nombre',
				tooltip: 	'Nombre',
				dataIndex: 	'NOMBRE',
				sortable: 	true,
				resizable: 	true,
				width: 		215,
				hidden: 		false
			},
			{
				header: 		'Banco',
				tooltip: 	'Banco',
				dataIndex: 	'BANCO',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Cuenta Clabe',
				tooltip: 	'Cuenta Clabe',
				dataIndex: 	'CUENTA_CLABE',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Cuenta Swift',
				tooltip: 	'Cuenta Swift',
				dataIndex: 	'CUENTA_SWIFT',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Plaza Swift',
				tooltip: 	'Plaza Swift',
				dataIndex: 	'PLAZA_SWIFT',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Sucursal Swift',
				tooltip: 	'Sucursal Swift',
				dataIndex: 	'SUCURSAL_SWIFT',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Tipo de Moneda',
				tooltip: 	'Tipo de Moneda',
				dataIndex: 	'MONEDA',
				sortable: 	true,
				resizable: 	true,
				width: 		110,
				hidden: 		false
			},
			{
				header: 		'EPO',
				tooltip: 	'EPO',
				dataIndex: 	'NOMBRE_EPO',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Descripci�n TEF',
				tooltip: 	'Descripci�n TEF',
				dataIndex: 	'DESCRIPCION_TEF',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Descripci�n CECOBAN',
				tooltip: 	'Descripci�n CECOBAN',
				dataIndex: 	'DESCRIPCION_CECOBAN',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Descripci�n D�LARES',
				tooltip: 	'Descripci�n D�LARES',
				dataIndex: 	'DESCRIPCION_DOLARES',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false
			},
			{
				header: 		'Fecha y hora de �ltima actualizaci�n',
				tooltip: 	'Fecha y hora de �ltima actualizaci�n',
				dataIndex: 	'FECHA_ULTIMA_ACTUALIZACION',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false,
				renderer: 	Ext.util.Format.dateRenderer('d-m-Y h:i:s') 
			},
			{
				header: 		'Usuario',
				tooltip: 	'Usuario',
				dataIndex: 	'USUARIO',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			}
			/*
				//	se quita esta columna a solitud del usaurio la EPo no debe de hacer ninguna acci�n
			 , {
            xtype: 		'actioncolumn',
            header: 		'Seleccionar',
				tooltip: 	'Seleccionar',
            width: 		75,
            hidden: 		false,
            items: [
            	 // ACCIONES INVALIDAR Y VALIDAR	
            	 {
                    getClass: function(value, metadata, record) { 
                    	  	 
                        if (record.json['ESTATUS_CUENTA'] == "99" ) {
                            this.items[0].tooltip = 'Invalidar Cuenta';
                            return 'invalidarCuenta';
                        } else {
                            this.items[0].tooltip = 'Validar Cuenta';
                            return 'validarCuenta';
                        }
                        
                    },
                    handler: function(grid, rowIndex, colIndex) {
                    	  
                        var record = Ext.StoreMgr.key('cuentasConsultadasDataStore').getAt(rowIndex);
                        // Invalidar Cuenta
                        if (record.json['ESTATUS_CUENTA'] == "99" ) {
                        	
                        	Ext.MessageBox.confirm(
										'Confirmar', 
										'�Est� seguro de querer Invalidar la cuenta?',
										function(boton) {
											
											if (boton == 'yes') { 
												
												Ext.Ajax.request({
													url: 		'15consulta02ext.data.jsp',
													params: 	{
														claveCuentaModificarEstatus: record.json['IC_CUENTA'],
														accion: 'InvalidarCuenta'
													},
													callback: procesaInvalidarCuenta
												});
								
											} else if (boton == 'no') {
												return;
											}
											
										});
									
								// Validar Cuenta	
                        } else {
                        	
                        	Ext.MessageBox.confirm(
										'Confirmar', 
										'�Est� seguro de querer Validar la cuenta?',
										function(boton) {
											
											if (boton == 'yes') { 
												
												Ext.Ajax.request({
													url: 		'15consulta02ext.data.jsp',
													params: 	{
														claveCuentaModificarEstatus: record.json['IC_CUENTA'],
														accion: 'ValidarCuenta'
													},
													callback: procesaValidarCuenta
												});
								
											} else if (boton == 'no') {
												return;
											}
											
										});
									
                        }
                        
                    }
                },
                // ACCION MODIFICAR CUENTA
                {
                	 
                	 getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
                	 	 
							this.items[1].tooltip = 'Modificar Cuenta';
							return 'modificar';
							
						 },	
						 handler: function(grid, rowIndex, colIndex) {
						 	 
                     var record = Ext.StoreMgr.key('cuentasConsultadasDataStore').getAt(rowIndex);
 
                     // Leer los parametros iniciales
                     var claveTipoAfiliado 		= record.json['TIPOAFILIADO'];
                     var producto 					= record.json['IC_PRODUCTO_NAFIN'];
                     var numeroNafinElectronico	= record.json['IC_NAFIN_ELECTRONICO'];
                     var nombreProveedor			= record.json['NOMBRE'];
                     var rfcProveedor				= record.json['PYME_RFC'];
                     var moneda						= record.json['TIPO_MONEDA'];
                     var banco						= record.json['IC_BANCOS_TEF'];
                     var cuentaClabe				= record.json['CUENTACLABE'];
                     var cuentaSwift				= record.json['CUENTASWIFT'];
                     var plazaSwift					= record.json['PLAZA_SWIFT'];
                     var sucursalSwift				= record.json['SUCURSAL_SWIFT'];
                     var tipoCuenta					= record.json['TIPO_CUENTA'];
                     var claveCuenta				= record.json['IC_CUENTA'];
                     
                     // Inicializar campos
                     Ext.getCmp('claveTipoAfiliadoMC').setValue(			claveTipoAfiliado 		);
                     //Ext.getCmp('cmbProductoMC').setValue(				producto 					);
                     Ext.getCmp('numeroNafinElectronicoMC').setValue(	numeroNafinElectronico	);
                     Ext.getCmp('nombreProveedorMC').setValue(				nombreProveedor 			);
                     Ext.getCmp('rfcProveedorMC').setValue(					rfcProveedor 				);
                     //Ext.getCmp('cmbMonedaMC').setValue(					moneda 						);
                     //Ext.getCmp('cmbBancoMC').setValue(					banco 						);
                     Ext.getCmp('cuentaClabeMC').setValue(					cuentaClabe 				);
                     Ext.getCmp('cuentaSwiftMC').setValue(					cuentaSwift 				);
                     Ext.getCmp('plazaSwiftMC').setValue(					plazaSwift 					);
                     Ext.getCmp('sucursalSwiftMC').setValue(				sucursalSwift 				);
                     Ext.getCmp('tipoCuentaMC').setValue(					tipoCuenta					);
                     Ext.getCmp('claveCuentaMC').setValue(					claveCuenta					);
                     
                     // Recargar combos
                     Ext.StoreMgr.key("catalogoProductoMCDataStore").load({
                     		params: { defaultValue: producto }
                     });
                     Ext.StoreMgr.key("catalogoMonedaMCDataStore").load({
                     		params: { defaultValue: moneda   }
                     });
                     Ext.StoreMgr.key("catalogoBancoMCDataStore").load({
                     		params: { defaultValue: banco    }
                     });
 
                     // Mostrar ventana
                     var ventana = Ext.getCmp('windowModificarCuenta');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									layout: 			'fit',
									width: 			606,
									height: 			341,
									minWidth: 		606,
									minHeight: 		341,
									id: 				'windowModificarCuenta',
									modal:			true,
									closeAction: 	'hide',
									listeners: 		{
										hide: resetFormaModificarCuenta
									},
									items: [
										Ext.getCmp("modificarCuenta")
									]
								}).show();
							}
 
                   }
                   
					 },
					 // ACCION ELIMINAR CUENTA
                {
                	 
                    getClass: function(value, metadata, record) {  
 
                        if ( record.json['TIPOAFILIADO'] == "P" ) {
                            this.items[2].tooltip = 'Eliminar Cuenta';       
                            return 'eliminarCuenta';
                        } else {
                            return null;
                        }
                        
                    },
                    handler: function(grid, rowIndex, colIndex) {
                    	  	
                        var record 			= Ext.StoreMgr.key('cuentasConsultadasDataStore').getAt(rowIndex);
                        var claveCuenta 	= record.json['IC_CUENTA'];
                        if ( record.json['TIPOAFILIADO'] == "P" ) {
                        	eliminarCuenta('VERIFICAR_USO_CUENTA',rowIndex,claveCuenta);
                        } else {
                        	return null;
                        }
                        
                    }
                }
            ]
         }
			*/
		],
		stripeRows: true,
		loadMask: 	true,
		height: 		400,
		width: 		943,		
		frame: 		true,
		bbar: {
			
			xtype: 			'paging',
			pageSize: 		15,
			buttonAlign: 	'left',
			id: 				'barraPaginacion',
			opts:				null,
			displayInfo: 	true,
			store: 			cuentasConsultadasData,
			displayMsg: 	'{0} - {1} de {2}',
			emptyMsg: 		"No hay registros.",
			items: [                             
					'->',
					'-',
					{
						xtype: 	'button',
						text: 	'Generar Archivo',
						id: 		'botonGenerarArchivoCSV',
						handler: function(boton, evento) {
							
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							// Copiar los parametros base
							var cuentasConsultadasData 	= Ext.StoreMgr.key("cuentasConsultadasDataStore");
							var baseParams						= new Object();
							Ext.apply(baseParams, 			cuentasConsultadasData.baseParams);
							
							// Generar Archivo CSV
							var barraPaginacion 			= Ext.getCmp("barraPaginacion");
							Ext.Ajax.request({
								url: 			'15consulta02ext.data.jsp',
								params: 		Ext.apply( baseParams, { 
									accion: 	'GeneraArchivoCSV'
								}),
								callback: 	procesarSuccessFailureGeneraArchivoCSV
							});
							 
						}
					},
					{
						xtype: 	'button',
						text: 	'Bajar Archivo',
						id: 		'botonBajarArchivoCSV',
						hidden: 	true
					},
					'-',
					{
						xtype:	'button',
						text: 	'Imprimir PDF',
						id: 		'botonImprimirPDF',
						handler: function(boton, evento) {
							
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							// Copiar los parametros base
							var cuentasConsultadasData 	= Ext.StoreMgr.key("cuentasConsultadasDataStore");
							var baseParams					= new Object();
							Ext.apply(baseParams, 		cuentasConsultadasData.baseParams);
							
							// Generar archivo PDF
							var barraPaginacion 			= Ext.getCmp("barraPaginacion");
							Ext.Ajax.request({
								url: 			'15consulta02ext.data.jsp',
								params: 		Ext.apply( baseParams, { 
									accion: 	'GeneraArchivoPaginaPDF',
									start: 	barraPaginacion.cursor,
									limit: 	barraPaginacion.pageSize
								}),
								callback: 	procesarSuccessFailureGeneraArchivoPaginaPDF
							});
							 
						}
					},
					{
						xtype: 	'button',
						text: 	'Bajar PDF',
						id: 		'botonBajarPDF',
						hidden: 	true
					}					
				]
			}
	});
	
	//-------------------------------- FORMA PRINCIPAL -----------------------------------	
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			panelForma,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10)
		]
	});
 
	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '15consulta02ext.data.jsp',
		params: {
			accion: "valoresIniciales"                                                                 
		},
		callback: procesaValoresIniciales
	});
 
});