function cambioSinRecurso(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);

	
	if(check.checked==true)  {
		reg.set('AUXSINRECURSO','S');
		reg.set('AUXCONRECURSO','');
	}
	
	store.commitChanges();	
}

function cambioConRecurso(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);

	if(check.checked==true)  {
		reg.set('AUXCONRECURSO','S');
		reg.set('AUXSINRECURSO','');
	}
	store.commitChanges();	
}

function cambioDistri(check, rowIndex, colIds){	
	check.checked=true;
	var  gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);

	if(check.checked==true)  {
		reg.set('AUXDISTRIBUIDORES','S');
	}
}

function cambioDerechos(check, rowIndex, colIds){
	check.checked=true;
	var  gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);

	if(check.checked==true)  {
		reg.set('AUXCESION','S');
	}
}

function cambioFianza(check, rowIndex, colIds){

	check.checked=true;
	var  gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);

	if(check.checked==true)  {
		reg.set('AUXFIANZA','S');
	}
}

function cambioDescuentoIF(check,rowIndex,colIds){
	var gridConsulta = Ext.getCmp('gridDetallado');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	
	if( reg.get('CLICK')=='noSelecc'){
		check.checked = false;
	}	
	else if (reg.get('CLICK') == 'deseleccionado'){
		check.checked = true;
	}	
}

function cambioDistribuidoresIF(check,rowIndex,colIds){
	var gridConsulta = Ext.getCmp('gridDetallado');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	
	if(reg.get('CHKDISTRIBUIDORES') == 'checked'){
		check.checked = true
	}else{
		if(check.checked == true)  {
			reg.set('AUXDISTRIBUIDORES','S');
		}else{
			reg.set('AUXDISTRIBUIDORES','');
		}	
	}
}

function cambioCesionIF(check,rowIndex,colIds){
	var gridConsulta = Ext.getCmp('gridDetallado');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	
	if(reg.get('CHKCESION') == 'checked'){
		check.checked = true
	}else{	
		if(check.checked==true)  {
			reg.set('AUXCESION','S');
		}else{
			reg.set('AUXCESION','');
		}	
	}
	
}

function cambioFianzaIF(check,rowIndex,colIds){
	var gridConsulta = Ext.getCmp('gridDetallado');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	
	if(check.checked==true)  {
		reg.set('AUXFIANZA','S');
	}else{
		reg.set('AUXFIANZA','');
	}	
}


Ext.onReady(function() { 

	var claveEpoIf = "";
	
	var procesarGuardado = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnAsignarEpo').enable();
			Ext.getCmp('btnAsignarIF').enable();
			Ext.MessageBox.alert("Mensaje",info.MENSAJE,function(){
				window.location = '15asignaprodExt.jsp'; 
			});
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	
	var asignaEpo = function(){
		var grid = Ext.getCmp('grid');
		var columnModelGrid = grid.getColumnModel();
		var store = grid.getStore();
		var errorValidacion = false;
		
		var objeto = new Array();
		var strHiden = new Array();
		var txtNumChk = 0;
		
		store.each(function(record) {
			if(record.data['AUXSINRECURSO'] == 'S')
				objeto.push('1');
			else
				objeto.push('2');
				
			strHiden.push('1'+'|'+record.data['CLAVEEPO']);
			txtNumChk++;
			objeto.push(record.data['AUXDISTRIBUIDORES']);
			strHiden.push('4'+'|'+record.data['CLAVEEPO']);
			txtNumChk++;
			objeto.push(record.data['AUXCESION']);
			strHiden.push('9'+'|'+record.data['CLAVEEPO']);
			txtNumChk++;		
			objeto.push(record.data['AUXFIANZA']);
			strHiden.push('10'+'|'+record.data['CLAVEEPO']);
			txtNumChk++;
		});
	
		Ext.getCmp('btnAsignarEpo').disable();
			
		Ext.Ajax.request({
			url: '15asignaprodExt.data.jsp',
			params:	Ext.apply(fp.getForm().getValues(),
						{
							informacion:'AsignaEpo',
							objeto: objeto,
							strHiden: strHiden,
							txtNumChk: txtNumChk
						}
			),
			callback: procesarGuardado
		});	
	} 
	
	
	var asignaIF = function(){
		var grid = Ext.getCmp('gridDetallado');
		var columnModelGrid = grid.getColumnModel();
		var store = grid.getStore();
		var errorValidacion = false;
		
		var objeto = new Array();
		var strHiden = new Array();
		var txtNumChk = 0;
		
		store.each(function(record) {
			
			objeto.push(record.data['AUXDESCUENTO']);	
			strHiden.push('1'+'|'+record.data['CLAVEIF']);
			txtNumChk++;
			objeto.push(record.data['AUXDISTRIBUIDORES']);
			strHiden.push('4'+'|'+record.data['CLAVEIF']);
			txtNumChk++;
			objeto.push(record.data['AUXCESION']);
			strHiden.push('9'+'|'+record.data['CLAVEIF']);
			txtNumChk++;		

		});
	
		Ext.getCmp('btnAsignarIF').disable();
		Ext.Ajax.request({
			url: '15asignaprodExt.data.jsp',
			params:	Ext.apply(fp.getForm().getValues(),
						{
							informacion:'AsignaIf',
							objeto: objeto,
							strHiden: strHiden,
							txtNumChk: txtNumChk,
							claveEpoIf: claveEpoIf
						}
			),
			callback: procesarGuardado
		});	
	} 	
	
		
	var procesarCadena = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "0", 
		descripcion: "Seleccionar Cadena", 
		loadMsg: ""})); 
		
		store.insert(1,new Todas({ 
		clave: "-1", 
		descripcion: "Todas las Cadenas", 
		loadMsg: ""})); 		
		store.commitChanges(); 

		Ext.getCmp('idComboCadena').setValue('0');
	}
	
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');	
		if(arrRegistros!=null){
			grid.show();
			if(Ext.getCmp('idComboCadena').getValue()=='-1'){
				grid.setHeight(600);
			}else{
				grid.setHeight(160);	
			}
						
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				var store = grid.getStore(); 
			
				store.each(function(record) {
				
					if(record.data['SINRECURSO'] == 'checked'){
						record.data['AUXSINRECURSO'] = 'S';
					}else{
						record.data['AUXSINRECURSO'] = '';
					}
					
					if(record.data['CONRECURSO'] == 'checked'){
						record.data['AUXCONRECURSO'] = 'S';
					}else{
						record.data['AUXCONRECURSO'] = '';
					}

					if(record.data['CHKDISTRIBUIDORES'] == 'checked'){
						record.data['AUXDISTRIBUIDORES'] = 'S';
					}else{
						record.data['AUXDISTRIBUIDORES'] = '';
					}

					if(record.data['CHKCESION'] == 'checked'){
						record.data['AUXCESION'] = 'S';
					}else{
						record.data['AUXCESION'] = '';
					}											
					
					if(record.data['CHKFIANZA'] == 'checked'){
						record.data['AUXFIANZA'] = 'S';
					}else{
						record.data['AUXFIANZA'] = '';
					}					
					
				});
				fp.el.unmask();
				el.unmask();
				Ext.getCmp('btnConsultar').enable();
				Ext.getCmp('btnAsignarEpo').enable();
			}else{
				Ext.getCmp('btnAsignarEpo').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarDatosDetallado = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('gridDetallado');	
		if(arrRegistros!=null){
			grid.show();						
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				if(store.getTotalCount()<5 ){
					grid.setHeight(240);
				}else{
					grid.setHeight(500);	
				}
							
				var store = grid.getStore(); 
				store.each(function(record) {
				
					if(record.data['CHKDESCUENTO'] == 'checked'){
						record.data['AUXDESCUENTO'] = 'S';
					}else{
						record.data['AUXDESCUENTO'] = '';
					}
					
					if(record.data['CHKDISTRIBUIDORES'] == 'checked'){
						record.data['AUXDISTRIBUIDORES'] = 'S';
					}else{
						record.data['AUXDISRIBUIDORES'] = '';
					}

					if(record.data['CHKCESION'] == 'checked'){
						record.data['AUXCESION'] = 'S';
					}else{
						record.data['AUXCESION'] = '';
					}

				});			
				
				
				el.unmask();
				Ext.getCmp('btnAsignarEpo').enable();
			}else{
				Ext.getCmp('btnAsignarIF').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}


	var verDetallado = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var producto1 = registro.get('PROD1'); 
		var producto4 = registro.get('PROD4');
		var clave = registro.get('CLAVEEPO');
		
		Ext.getCmp('idComboCadena').setValue(clave);
		
		claveEpoIf = clave;
		consultaDataGrid.load({
				params:{ComboEpo: Ext.getCmp('idComboCadena').getValue() }
		});
		
		consultaDataGridDetallado.load({ params:{
					prod1: producto1, 
					prod4: producto4,
					Epo: clave}
		}); 
	}


//-------------Stores-----------------------------------------------------------	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15asignaprodExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'CADENA'},
			{name: 'SINRECURSO'},
			{name: 'CONRECURSO'},
			{name: 'PROD1'},
			{name: 'PROD4'},
			{name: 'CHKDISTRIBUIDORES'},
			{name: 'CHKCESION'},
			{name: 'CHKFIANZA'},
			{name: 'AUXSINRECURSO'},
			{name: 'AUXCONRECURSO'},
			{name: 'AUXDISTRIBUIDORES'},
			{name: 'AUXCESION'},
			{name: 'CLAVEEPO'},
			{name: 'AUXFIANZA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});

	var consultaDataGridDetallado = new Ext.data.JsonStore({
		root : 'registros',
		url : '15asignaprodExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGridDetallado'
		},
		fields: [		
			{name: 'CADENA'},
			{name: 'CLAVEIF'},
			{name: 'CHKDESCUENTO'},
			{name: 'CHKDISTRIBUIDORES'},
			{name: 'CHKCESION'},
			{name: 'CHKFIANZA'},
			{name: 'AUXDISTRIBUIDORES'},
			{name: 'AUXCESION'},
			{name: 'CLICK'},
			{name: 'AUXDESCUENTO'},
			{name: 'AUXFIANZA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatosDetallado,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});


	var CatalogoCadena = new Ext.data.JsonStore
	  ({
			id: 'catIf',
			root : 'registros',
			fields : ['clave', 'descripcion', 'loadMsg'],
			url : '15asignaprodExt.data.jsp',
			baseParams: 
			{
			 informacion: 'CatalogoCadena'
			},
			autoLoad: true,
			listeners:
			{
			 load: procesarCadena,
			 exception: NE.util.mostrarDataProxyError,
			 beforeload: NE.util.initMensajeCargaCombo
			}
	  });
	
	
	var Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
	
	

//--------------Componentes-----------------------------------------------------	
	
	var elementosForma = 
	[	
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de Cadena',
			forceSelection: true,
			autoSelect: true,
			name:'claveCadena',
			id:'idComboCadena',
			displayField: 'descripcion',
			allowBlank: true,
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: CatalogoCadena,  
			width:235
		}
	]
	
		var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
		buttons:[{
						xtype:	'button',
						id:		'btnConsultar',
						text:		'Consultar',
						iconCls: 'icoBuscar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
									if(Ext.getCmp('idComboCadena').getValue() == "0"){
										Ext.getCmp('idComboCadena').markInvalid('Debe seleccionar una Cadena');
										return ;
									}
									boton.disable();
									fp.el.mask('Procesando...','x-mask-loading');
									
									Ext.getCmp('gridDetallado').hide();
									consultaDataGrid.load({
										params:{ComboEpo: Ext.getCmp('idComboCadena').getValue() }
									});
									
									},
						style: 'margin:0 auto;',
						align:'rigth'
					},{
						xtype:	'button',
						id:		'btnCancel',
						text:		'Limpiar',
						iconCls: 'icoLimpiar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							window.location = '15asignaprodExt.jsp';
						},
						style: 'margin:0 auto;'
					}],
		style: 'margin:0 auto;',	   
		height: 120,
		width: 700
	});
	
	
		var grupo = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '  ', colspan: 1, align: 'center'},
					{header: 'Descuento Electr�nico', colspan: 2, align: 'center'},
					{header: '  ', colspan: 4, align: 'center'}
					
				]
			]
		});
	
	

	var gridDetallado = new Ext.grid.GridPanel({
		store: consultaDataGridDetallado,
		style: 'margin:0 auto;',
		hidden:true,
		id: 'gridDetallado',
		columns: [
			{
				header: '<center>IF</center>',
				dataIndex: 'CADENA',
				width: 300,
				align: 'left'				
			},			{
				header:'Descuento <BR> Electr�nico',
				dataIndex : 'CHKDESCUENTO',
				width : 100,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['AUXDESCUENTO'] == ''){
							if(record.data['CHKDESCUENTO']=='checked' ){
								return '<input  id="chkdesc"  type="checkbox" checked  onclick="cambioDescuentoIF(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkdesc" type="checkbox"  onclick="cambioDescuentoIF(this, '+rowIndex +','+colIndex+');" />';
							}
					
					}else{
						if(record.data['AUXDESCUENTO'] == 'S'){
							return '<input  id="chkdesc" type="checkbox" checked  onclick="cambioDescuentoIF(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkdesc" type="checkbox"  onclick="cambioDescuentoIF(this, '+rowIndex +','+colIndex+');" />';
						}
					}
				}
			},
			
			{
				header:'Financiamiento a <BR> Distribuidores',
				dataIndex : 'CHKDISTRIBUIDORES',
				width : 100,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				;
					if(record.data['AUXDISTRIBUIDORES'] == ''){
							if(record.data['CHKDISTRIBUIDORES']=='checked' ){
								return '<input  id="chkPRODUCTO41" type="checkbox" checked  onclick="cambioDistribuidoresIF(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkPRODUCTO41" type="checkbox"  onclick="cambioDistribuidoresIF(this, '+rowIndex +','+colIndex+');" />';
							}
					
					}else{
						if(record.data['AUXDISTRIBUIDORES'] == 'S'){
							return '<input  id="chkPRODUCTO41"  type="checkbox" checked  onclick="cambioDistribuidoresIF(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkPRODUCTO41" type="checkbox"  onclick="cambioDistribuidoresIF(this, '+rowIndex +','+colIndex+');" />';
						}
					}
				}
			},
			
			{
				header:'Cesi�n de <BR> Derechos',
				dataIndex : 'CHKCESION',
				width : 100,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['AUXCESION'] == ''){
							if(record.data['CHKCESION']=='checked' ){
								return '<input  id="chkPRODUCTO42" type="checkbox" checked  onclick="cambioCesionIF(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkPRODUCTO42" type="checkbox"   onclick="cambioCesionIF(this, '+rowIndex +','+colIndex+');" />';
							}
					
					}else{
						if(record.data['AUXCESION'] == 'S'){
							return '<input  id="chkPRODUCTO42" type="checkbox" checked  onclick="cambioCesionIF(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkPRODUCTO42" type="checkbox" onclick="cambioCesionIF(this, '+rowIndex +','+colIndex+');" />';
						}
					}
				}
			}
	
			],
			loadMask: true,
			bbar: {
					autoScroll:true,
					id: 'barraDetallado',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnAsignarIF',
						text:		'Asignar Producto a IF',
						//iconCls: 'icoGuardar ',
						width: 100,
						weight: 70,
						style: 'margin:0 auto;',
						handler: asignaIF //momentaneo
					}
					]
				},			
		
			height: 700,
			width: 640,
			frame: true
	});


	var grid = new Ext.grid.GridPanel({
		store: consultaDataGrid,
		id: 'grid',
		style: 'margin:0 auto;',
		hidden: true,
		frame: true,
		plugins: grupo,		
		columns: [
			{
				header: '<center>Cadena</center>',
				dataIndex: 'CADENA',
				width: 300,
				align: 'left'				
			},{
				header:'Sin Recurso',
				dataIndex : 'SINRECURSO',
				width : 100,
				align: 'center',
				sortable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){					
					if(record.data['AUXSINRECURSO'] == ''){
							if(record.data['SINRECURSO']=='checked' ){
								return '<input  id="chkPRODUCTO1"   name="desco'+rowIndex+'"  type="Radio" checked  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkPRODUCTO1"  name="desco'+rowIndex+'"   type="Radio"  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
							}					
					}else{
						if(record.data['AUXSINRECURSO'] == 'S'){
							return '<input  id="chkPRODUCTO1"   name="desco'+rowIndex+'"  type="Radio" checked  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkPRODUCTO1"  name="desco'+rowIndex+'"   type="Radio"  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
						}
					}

				}
			},
			{
				header:'Con Recurso',
				dataIndex : 'CONRECURSO',
				width : 100,
				align: 'center',
				sortable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['AUXCONRECURSO'] == ''){
							if(record.data['CONRECURSO']=='checked' ){
								return '<input  id="chkPRODUCTO4" type="Radio" name="desco'+rowIndex+'" checked  onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkPRODUCTO4" type="Radio"  name="desco'+rowIndex+'" onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
							}		
					}else{
						if(record.data['AUXCONRECURSO'] == 'S'){
							return '<input  id="chkPRODUCTO4" type="Radio" name="desco'+rowIndex+'" checked  onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkPRODUCTO4" type="Radio"  name="desco'+rowIndex+'" onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
						}
					}
				}
			},
			
			{
				header:'Financiamiento a <BR> Distribuidores',
				dataIndex : 'CHKDISTRIBUIDORES',
				width : 100,
				align: 'center',
				sortable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['AUXDISTRIBUIDORES'] == ''){
							if(record.data['CHKDISTRIBUIDORES']=='checked' ){
								return '<input  id="chkPRODUCTO41" type="checkbox" checked  onclick="cambioDistri(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkPRODUCTO41" type="checkbox"  onclick="cambioDistri(this, '+rowIndex +','+colIndex+');" />';
							}
					}else{
						if(record.data['AUXDISTRIBUIDORES'] == 'S'){
							return '<input  id="chkPRODUCTO41" type="checkbox" checked  onclick="cambioDistri(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkPRODUCTO41" type="checkbox"  onclick="cambioDistri(this, '+rowIndex +','+colIndex+');" />';
						}
					}
				}
			},			
			{
				header:'Cesi�n de <BR> Derechos',
				dataIndex : 'CHKCESION',
				width : 100,
				align: 'center',
				sortable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['AUXCESION'] == ''){
							if(record.data['CHKCESION']=='checked' ){
								return '<input  id="chkPRODUCTO42" type="checkbox" checked  onclick="cambioDerechos(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkPRODUCTO42" type="checkbox"   onclick="cambioDerechos(this, '+rowIndex +','+colIndex+');" />';
							}
					
					}else{
						if(record.data['AUXCESION'] == 'S'){
							return '<input  id="chkPRODUCTO42" type="checkbox" checked  onclick="cambioDerechos(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkPRODUCTO42" type="checkbox"   onclick="cambioDerechos(this, '+rowIndex +','+colIndex+');" />';
						}
					}
				}
			},
			{
				header:'Fianza <BR> Electr�nica',
				dataIndex : 'CHKFIANZA',
				width : 100,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['AUXFIANZA'] == ''){
							if(record.data['CHKFIANZA']=='checked' ){
								return '<input  id="chkPRODUCTO43"'+ rowIndex +' value='+ value + ' type="checkbox" checked  onclick="cambioFianza(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkPRODUCTO43" type="checkbox"  onclick="cambioFianza(this, '+rowIndex +','+colIndex+');" />';
							}
					
					}else{
						if(record.data['AUXFIANZA'] == 'S'){
							return '<input  id="chkPRODUCTO43" type="checkbox" checked  onclick="cambioFianza(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkPRODUCTO43" type="checkbox"  onclick="cambioFianza(this, '+rowIndex +','+colIndex+');" />';
						}
					}
				}
			} ,
			{
				 xtype: 'actioncolumn',
				header: 'Detallado',
				width: 100,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoLupa';										
						}
						,	
						handler: verDetallado
					}
				]				
			}
			
			],
			loadMask: true,
			
			bbar: {
					autoScroll:true,
					id: 'barra',
					displayInfo: true,
					items: ['->','-',					
					{	
						xtype:	'button',
						id:		'btnAsignarEpo',
						text:		'Asignar Producto a EPO',
						width: 100,
						handler: asignaEpo
					}
					]
				},				
			height: 160,
			width: 930
			
	});


	//Contenedor Principal
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [fp
		,NE.util.getEspaciador(10),grid,NE.util.getEspaciador(10),gridDetallado ]
	});

});