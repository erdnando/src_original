<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.procesos.*,
		com.netro.parametrosgrales.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	if (informacion.equals("CatalogoCadena")){						
		CatalogoEPO cat = new CatalogoEPO();
			
		cat.setCampoClave("CE.ic_epo");
		cat.setCampoDescripcion("CE.cg_razon_social");
		cat.setHabilitado("S");	
		infoRegresar=cat.getJSONElementos(); 	
	
	}else if (informacion.equals("ConsultaGrid")){	
		
		String cveEpo  = (request.getParameter("ComboEpo") != null)?request.getParameter("ComboEpo"):"";

		//AfiliacionHome afiliacionHome =	(AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB",AfiliacionHome.class); 
		//Afiliacion 		afiliacion 		= afiliacionHome.create();
		Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

		List estados = new ArrayList();	
		estados = (ArrayList)afiliacion.estadoAsignacionProductos(cveEpo);
	
		JSONObject jsonObj = new JSONObject();	
		JSONArray registros = new JSONArray();
		registros = JSONArray.fromObject(estados);
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		
		jsonObj = JSONObject.fromObject(consulta);	
			
		infoRegresar = jsonObj.toString();	  
	}else if (informacion.equals("AsignaEpo")){	
		String ArrayObjeto[]	= request.getParameterValues("objeto");
		String ArraystrHiden []	= request.getParameterValues("strHiden");
		String txtNumChk  = (request.getParameter("txtNumChk") != null)?request.getParameter("txtNumChk"):"";
	
		//AfiliacionHome afiliacionHome =	(AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB",AfiliacionHome.class); 
		//Afiliacion 		afiliacion 		= afiliacionHome.create();
		Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

		String mensaje = afiliacion.asignaProductos(ArrayObjeto,ArraystrHiden,txtNumChk,"EPO","");
		JSONObject jsonObj = new JSONObject();	
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("MENSAJE", mensaje);

		infoRegresar = jsonObj.toString();	
	  
	}else if (informacion.equals("AsignaIf")){	
		String ArrayObjeto[]	= request.getParameterValues("objeto");
		String ArraystrHiden []	= request.getParameterValues("strHiden");
		String txtNumChk  = (request.getParameter("txtNumChk") != null)?request.getParameter("txtNumChk"):"";
		String txtCveEPO  = (request.getParameter("claveEpoIf") != null)?request.getParameter("claveEpoIf"):"";
		
		//AfiliacionHome afiliacionHome =	(AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB",AfiliacionHome.class); 
		//Afiliacion 		afiliacion 		= afiliacionHome.create();
		Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

		String mensaje = afiliacion.asignaProductos(ArrayObjeto,ArraystrHiden,txtNumChk,"IF",txtCveEPO);
		JSONObject jsonObj = new JSONObject();	
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("MENSAJE", mensaje);
	
		infoRegresar = jsonObj.toString();	
	  
	}	else if (informacion.equals("ConsultaGridDetallado")){	

		String cveEpo  = (request.getParameter("Epo") != null)?request.getParameter("Epo"):"";
		String prod1 = (request.getParameter("prod1") != null)?request.getParameter("prod1"):"";
		String prod4 = (request.getParameter("prod4") != null)?request.getParameter("prod4"):"";

		//AfiliacionHome afiliacionHome =	(AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB",AfiliacionHome.class); 
		//Afiliacion 		afiliacion 		= afiliacionHome.create();
		Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

		List estados = new ArrayList();	
		estados = (ArrayList)afiliacion.estadoAsignacionIf(prod1,prod4,cveEpo);
	
		JSONObject jsonObj = new JSONObject();	
		JSONArray registros = new JSONArray();
		registros = JSONArray.fromObject(estados);
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		
		jsonObj = JSONObject.fromObject(consulta);	
			
		infoRegresar = jsonObj.toString();
	} 	
	
%>
<%= infoRegresar %>