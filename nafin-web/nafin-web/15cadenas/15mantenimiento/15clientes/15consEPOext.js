Ext.onReady(function(){

	var idMenu =  Ext.getDom('idMenu').value;
	var strPerfil = Ext.getDom('strPerfil').value;
	var permisosGlobales;
	

	function procesaValoresIniciales(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var  info = Ext.util.JSON.decode(response.responseText);			
			permisosGlobales=info.permisos;	
			
		}else {			
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//----------------------Descargar Archivos--------------------------
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnImprimir').setIconClass('icoPdf');
			Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
			//Ext.getCmp('btnImprimirC').setIconClass('icoPdf');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var catalogoPerfil = new Ext.data.JsonStore({
		id: 'catalogoPerfil',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: '15consEPO.data.jsp',
		baseParams: {
			informacion: 'catalogoPerfil'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {				
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	

	function modificarPerfil(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert('Mensaje',info.mensaje,
				function(){
					Ext.getCmp('ProcesarCuentas').hide();
				}	
			);
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	
	//procesar Cuentas
	
		function procesarInformacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			if(info.mensaje =='') {		
				Ext.getCmp('lblNombreEmpresa1').update(info.lblNombreEmpresa);
				Ext.getCmp('lblNafinElec1').update(info.lblNafinElec);
				Ext.getCmp('lblNombre1').update(info.lblNombre);
				Ext.getCmp('lblApellidoPaterno1').update(info.lblApellidoPaterno);
				Ext.getCmp('lblApellidoMaterno1').update(info.lblApellidoMaterno);
				Ext.getCmp('lblEmail1').update(info.lblEmail);
				Ext.getCmp('lblPerfilActual1').update(info.lblPerfilActual);
				Ext.getCmp('txtLogin1').setValue(info.txtLogin);
				Ext.getCmp('sTipoAfiliado').setValue(info.sTipoAfiliado);					
				
				Ext.getCmp('internacional').setValue(info.internacional);	
				Ext.getCmp('sTipoAfiliado').setValue(info.sTipoAfiliado);	
				Ext.getCmp('txtNafinElectronico').setValue(info.txtNafinElectronico);	
				Ext.getCmp('txtPerfilAnt').setValue(info.txtPerfilAnt);	
				Ext.getCmp('txtLoginC').setValue(info.txtLogin);				
				
				if(info.modificar=='S')  {
					Ext.getCmp('txtPerfil_1').show();
					catalogoPerfil.load({
						params:{ 
							tipoAfiliado: info.sTipoAfiliado, 
							txtLogin:info.txtLogin
						}	
					});
			}else  {
				Ext.getCmp('txtPerfil_1').hide();
			}
				
			}else  {  		/* no hay datos */   		}
			
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
	
	var modificarCuenta = function(grid, rowIndex, colIndex, item, event) {  		
		
			var registro = grid.getStore().getAt(rowIndex);
			var txtLogin = registro.get('CUENTA');  
			Ext.getCmp('btnModificar').show();
			Ext.getCmp('id_modificacion').show();
			Ext.getCmp('id_informacion').hide();
			 
			Ext.Ajax.request({
				url: '15consEPO.data.jsp',
				params: {
					informacion: 'informacionUsuario',		
					txtLogin:txtLogin,
					modificar:'S'
				},
				callback:procesarInformacion 
			});
			ventanaCuenta();
		
		var registro = grid.getStore().getAt(rowIndex);
		var txtLogin = registro.get('CUENTA');  
		Ext.getCmp('btnModificar').show();
		Ext.getCmp('id_modificacion').show();
		Ext.getCmp('id_informacion').hide();
		 
		Ext.Ajax.request({
			url: '15consEPO.data.jsp',
			params: {
				informacion: 'informacionUsuario',		
				txtLogin:txtLogin,
				modificar:'S'
			},
			callback:procesarInformacion 
		});
		ventanaCuenta();
	}
	
	var procesaCuenta = function(grid, rowIndex, colIndex, item, event) {  
		
		var registro = grid.getStore().getAt(rowIndex);
		var txtLogin = registro.get('CUENTA');  
		 
		 Ext.getCmp('btnModificar').hide();
		 Ext.getCmp('id_modificacion').hide();
		 Ext.getCmp('id_informacion').show();
		 
		Ext.Ajax.request({
			url: '15consEPO.data.jsp',
			params: {
				informacion: 'informacionUsuario',		
				txtLogin:txtLogin,
				modificar:'N'
			},
			callback:procesarInformacion 
		});
		ventanaCuenta();
	}
	
	
	var ventanaCuenta = function() {
	
		var ProcesarCuentas = Ext.getCmp('ProcesarCuentas');
		if(ProcesarCuentas){
			ProcesarCuentas.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 700,
				height: 400,												
				resizable: false,
				closable:false,
				id: 'ProcesarCuentas',
				closeAction: 'hide',
				items: [					
					fpDespliega					
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	xtype: 'button',	text: 'Salir',	iconCls: 'icoLimpiar', 	id: 'btnCerraP', handler: function(){Ext.getCmp('ProcesarCuentas').hide();} },
						'-'							
					]
				}
			}).show().setTitle('');
		}	
	}

		
		//***************************Informaci�n dle Usuario************************+		
	var elementosDespliega =[
		{ 	xtype: 'textfield',  hidden: true, id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'internacional', 	value: '' },		
		{ 	xtype: 'textfield',  hidden: true,  id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtNafinElectronico', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtPerfilAnt', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtLoginC', 	value: '' },			
		{
			xtype: 'panel',
			id: 'id_informacion', 
			allowBlank: false,
			title:'INFORMACION USUARIO',			
			value: '',
			hidden: true
		},
		{
			xtype: 'panel',
			id: 'id_modificacion', 
			allowBlank: false,
			title:'CAMBIO DE PERFIL',			
			value: '',
			hidden: true
		},
		{
			xtype: 'displayfield',
			name: 'txtLogin',
			id: 'txtLogin1',
			allowBlank: false,			
			fieldLabel: 'Clave de Usuario',
			value:''
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			fieldLabel: 'Empresa',
			items: [
				{
					xtype: 'displayfield',
					name: 'lblNombreEmpresa',
					id: 'lblNombreEmpresa1',
					allowBlank: false,
					width			: 230,
					value:''
				},
				{
					xtype: 'displayfield',
					id:'muestraCol',
					value: 'Nafin Electr�nico:',
					hidden: false
				},		
				{		
					xtype: 'displayfield',
					name: 'lblNafinElec',
					id: 'lblNafinElec1',
					allowBlank: false,
					anchor:'70%',
					width			: 230,				
					value:''
				}
			]
		},	
		{
			xtype: 'displayfield',
			name: 'lblNombre',
			id: 'lblNombre1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Nombre',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblApellidoPaterno',
			id: 'lblApellidoPaterno1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Apellido Paterno',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblApellidoMaterno',
			id: 'lblApellidoMaterno1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Apellido Materno',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblEmail',
			id: 'lblEmail1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Email',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblPerfilActual',
			id: 'lblPerfilActual1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Perfil Actual',
			value:''
		},
		{
			xtype: 'combo',
			fieldLabel: 'Perfil',
			name: 'txtPerfil',
			id: 'txtPerfil_1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'txtPerfil',
			emptyText: 'Seleccionar...',			
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,			
			minChars: 1,
			store: catalogoPerfil,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120,
			hidden : true
		}
	];

	var fpDespliega = new Ext.form.FormPanel({
		id					: 'fpDespliega',		
		layout			: 'form',
		width				: 800,
		height: 380,
		style				: ' margin:0 auto;',
		frame				: true,
		hidden			: false,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 160,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			elementosDespliega
		],
		buttons: [		
			{
				text: 'Modificar',
				id: 'btnModificar',
				iconCls: 'modificar',
				hidden: true,
				formBind: true,				
				handler: function(boton, evento) {	
					Ext.Ajax.request({
						url: '15consEPO.data.jsp',
						params: Ext.apply(fpDespliega.getForm().getValues(),{
							informacion: 'ModifiarPerfil',
							idMenuP:idMenu,
							ccPermiso:'BTMODICAR_CE'
						}),
						callback: modificarPerfil
					});				
				}
			}			
		]	
	});
	
	
	//---------------Acci�n Inhabiltar----------------
	
	function TransmiteInhabilitar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert('Mensaje',info.msg,
				function(){					
		
					VerventanaCuenta(info.ic_epo,info.naE, info.nombreEPO , info.tituloF, info.tituloC );
				}	
			);

		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	
	
	var procesarInhabilitar= function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_epo = registro.get('IC_EPO');  
	  var naE = registro.get('NUM_NAFIN_ELEC'); 
		var nombreEPO = registro.get('NOMBRE_EPO'); 
		var tituloF = 'Las siguientes cuentas de usuarios de la EPO, deben ser deshabilitadas dentro del Oracle Internet Directory para evitar su ingreso al sistema ';
		var tituloC = 'N@E:'+ naE +' '+nombreEPO;
		
		Ext.Ajax.request({
			url: '15consEPO.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Inhabilitar',
				ic_epo:ic_epo,
				naE:naE,
				nombreEPO:nombreEPO,
				tituloF:tituloF,
				tituloC:tituloC
				
			}),
			callback: TransmiteInhabilitar
		});
	}
	

 //**************************************Ver cuentas*****************************
			
	var procesarVerCuentas= function(grid, rowIndex, colIndex, item, event) {
	
		var registro = grid.getStore().getAt(rowIndex);
		var ic_epo = registro.get('IC_EPO');  
		var naE = registro.get('NUM_NAFIN_ELEC'); 
		var nombreEPO = registro.get('NOMBRE_EPO'); 
		var tituloC = 'N@E:'+ naE +' '+nombreEPO;
		var tituloF ='';
	
		consCuentasData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						ic_epo: ic_epo,
						informacion: 'ConsCuentas',
						tipoAfiliado:'E',
						tituloC:tituloC
					})
				});
				
		new Ext.Window({					
					modal: true,
					width: 340,
					height: 380,
					modal: true,					
					closeAction: 'hide',
					resizable: true,
					constrain: true,
					closable:false,
					id: 'Vercuentas',
					items: [					
						gridCuentas					
					],
					bbar: {
						xtype: 'toolbar',	buttonAlign:'center',	
						buttons: ['-',
							{	xtype: 'button',	text: 'Cerrar',	iconCls: 'icoLimpiar', 	id: 'btnCerrar',
								handler: function(){																		
									Ext.getCmp('Vercuentas').hide();
								} 
						},
							'-',
							{
								xtype: 'button',
								text: 'Imprimir',
								tooltip:	'Imprimir',
								id: 'btnImprimirC',
								iconCls: 'icoPdf',
								handler: function(boton, evento) {																
									Ext.Ajax.request({
										url: '15consEPO.data.jsp',
										params:	{
											ic_epo: ic_epo,
											informacion: 'ImprimirConsCuentas',
											tipoAfiliado:'E',
											tituloF:tituloF,
											tituloC:tituloC											
										}					
										,callback: descargaArchivo
									});
								}
							}	
						]
					}
				}).show().setTitle(tituloF);
				
					
			//}	
	}
	
	
		//***************************Grid de las cuentas ************************
	var procesarConCuentasData = function(store,arrRegistros,opts){
	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros != null){			
			var gridCuentas = Ext.getCmp('gridCuentas');
			if (!gridCuentas.isVisible()) {
				gridCuentas.show();
			}	
			var el = gridCuentas.getGridEl();			
			var info = store.reader.jsonData;				
			gridCuentas.setTitle(info.tituloC);
			
			if(store.getTotalCount()>0){				
				Ext.getCmp('btnImprimirC').enable();
			}else{
				Ext.getCmp('btnImprimirC').disable();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	
	var consCuentasData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15consEPO.data.jsp',
		baseParams: {
			informacion: 'ConsCuentas'			
		},		
		fields: [				
			{ name: 'CUENTA'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConCuentasData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConCuentasData(null, null, null);					
				}
			}
		}					
	});
		
	var gridCuentas = {
		//xtype: 'editorgrid',
		xtype: 'grid',
		store: consCuentasData,
		id: 'gridCuentas',		
		hidden: false,
		title: 'Usuarios',	
		columns: [			
			{
				xtype:	'actioncolumn',
				header: 'Login del Usuario',
				tooltip: 'Login del Usuario',
				dataIndex: 'CUENTA',
				align: 'center',
				width: 150,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					return (record.get('CUENTA'));
				},
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';							
						}
						,handler:	procesaCuenta  
					}
				]
			},
			{
				xtype:	'actioncolumn',
				header: 'Cambio de Perfil',
				tooltip: 'Cambio de Perfil',
				dataIndex: 'CAMBIO_PERFIL',
				align: 'center',								
				width: 150,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							for(i=0;i<permisosGlobales.length;i++){ 
								boton=  permisosGlobales[i];										
								if(boton=="CAMBIO_PERFIL") {
									this.items[0].tooltip = 'Modificar';
									return 'modificar';							
								}
							}
						}
						,handler:	modificarCuenta  
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 360,
		width: 320,		
		frame: true
	}
	//});
	
	
	
	//---------------Acci�n Borrar----------------
	function TransmiteBorrar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);			
			Ext.MessageBox.alert('Mensaje',info.msg,
				function(){
					consultaData.load({	
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15						
						})
					});					
				}	
			);

		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
		
	var procesarBorrar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_epo = registro.get('IC_EPO');  
			
		Ext.Ajax.request({
				url: '15consEPO.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Borrar',
					ic_epo:ic_epo					
				}),
				callback: TransmiteBorrar
			});
	}
	
	
	//---------------Acci�n Modificar----------------
	var procesarModificar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_epo = registro.get('IC_EPO');   
		//window.location ="15forma08Ext.jsp?ic_epo="+ic_epo+"&txtaction=mod&internacional=N";		
		window.location ="15forma08Ext.jsp?ic_epo="+ic_epo+"&idMenu="+idMenu+"&txtaction=mod&internacional=N";
	}
	
	//------------Generaci�n de la Consula -------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		var jsonData = store.reader.jsonData;
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {					
				el.unmask();
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarCSV').enable();				
			} else {		
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarCSV').disable();				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15consEPO.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [	
			{	name: 'NUM_NAFIN_ELEC'},	
			{	name: 'IC_EPO'},	
			{	name: 'NOMBRE_EPO'},	
			{	name: 'DOMICILIO'},	
			{	name: 'ESTADO'},	
			{	name: 'TELEFONO'},	
			{	name: 'BANCO_FODEO'},	
			{	name: 'CONVENIO_UNICO'},	
			{	name: 'OBSERVACIONES'},
			{	name: 'CS_HABILITADO'},
			{  name: 'LIDER_PROMOTOR'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});
	
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta- Cadenas Productivas',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'N�mero de Nafin Electr�nico',
				tooltip: 'N�mero de Nafin Electr�nico',
				dataIndex: 'NUM_NAFIN_ELEC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'IC_EPO',
				tooltip: 'IC_EPO',
				dataIndex: 'IC_EPO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Nombre o Raz�n Social',
				tooltip: 'Nombre o Raz�n Social',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){  
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Domicilio',
				tooltip: 'Domicilio',
				dataIndex: 'DOMICILIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Estado',
				tooltip: 'Estado',
				dataIndex: 'ESTADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Tel�fono',
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Banco Fondeo ',
				tooltip: 'Banco Fondeo ',
				dataIndex: 'BANCO_FODEO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',	
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'L�der Promotor ',
				tooltip: 'L�der Promotor ',
				dataIndex: 'LIDER_PROMOTOR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',	
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Convenio �nico',
				tooltip: 'Convenio �nico ',
				dataIndex: 'CONVENIO_UNICO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				xtype		: 'actioncolumn',
				header: 'Seleccionar',
				tooltip: 'Seleccionar ',
				dataIndex: 'COLUM_ACCIONES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Modificar';
							return 'modificar';
						}
						,handler: procesarModificar
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							for(i=0;i<permisosGlobales.length;i++){ 
								boton=  permisosGlobales[i];										
								if(boton=="ACCI_ELI_C") {	
									this.items[1].tooltip = 'Borrar';
									return 'borrar';
								}
							}
						}
						,handler: procesarBorrar
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							for(i=0;i<permisosGlobales.length;i++){ 
								boton=  permisosGlobales[i];										
								if(boton=="ACCI_INHA_C") {	
									if(record.get('CS_HABILITADO') =='S'){
										this.items[2].tooltip = 'Inhabilitar';
										return 'aceptar';
									}
								}
							}							
						}
						,handler: procesarInhabilitar
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Cuentas de Usuario',
				tooltip: 'Cuentas de Usuario ',
				ataIndex: 'COLUM_CUENTAS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}
						,handler: procesarVerCuentas
					}
				]
			},
			{
				header: 'Observaciones',
				tooltip: 'Observaciones',
				dataIndex: 'OBSERVACIONES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			}
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo ',
					iconCls: 'icoXls',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15consEPO.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GeneraArchivoCSV'			
							}),							
							callback: descargaArchivo
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir',
					handler: function(boton, evento) {
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15consEPO.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GeneraArchivoPDF',
								start: barraPaginacion.cursor,
								limit: barraPaginacion.pageSize			
							}),							
							callback: descargaArchivo
						});
					}
				}				
			]
		}
	});
			
	
	//****************catalogo Estado**********************
	var catalogoEstado = new Ext.data.JsonStore({
		id: 'catalogoEstado',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15consEPO.data.jsp',
		baseParams: {
			informacion: 'catalogoEstado'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
  	function creditoElec(pagina) {
		if (pagina == 0) { //Afianzadora
			window.location  = "15forma05AfianzadoraExt.jsp?idMenu="+idMenu; 	
		}	else if (pagina == 1 ) { // cadena productiva
			window.location ="15consEPOext.jsp?idMenu="+idMenu;
		}	else if (pagina == 2) { //provedores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15forma05ProveedoresExt.jsp?idMenu="+idMenu;
		}	else if (pagina == 3) { //Provedor Internacional
			
		}	else if (pagina == 4) { // provedor sin numero
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp?idMenu="+idMenu;
		}	else if (pagina == 5) { // Distribuidores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp?idMenu="+idMenu;		
		}	else if (pagina == 6) { // Fiados
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jspidMenu="+idMenu;;		
		}	else if (pagina == 7) { // Intermediario financiero Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B&idMenu="+idMenu; 	
		} else if (pagina == 8) { // Intermediario financiero NO Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB&idMenu="+idMenu;  	 	
		} else if (pagina == 9) { // Intermediario financiero Bancario /No Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB&idMenu="+idMenu;  		 	
		} else if (pagina == 10) { // Provedor Carga Masiva EContrac
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon_ext.jsp?idMenu="+idMenu; 
		}	else if (pagina == 11) { // Afiliados a Credito Electronico
			window.location = "15forma05ext_AfCredElectronico.jsp?idMenu="+idMenu; 
		}	else if (pagina == 12) { // Cadena Productiva Internacional
			
		}	else if (pagina == 13) { // Contragarante
			
		}	else if (pagina == 14) { // Mandante
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp?idMenu="+idMenu;	
		}	else if (pagina == 15) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp?idMenu="+idMenu;
		}	else if (pagina == 16) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp?idMenu="+idMenu; 		
		}		

	}

	var storeAfiliacion = new Ext.data.SimpleStore({
		fields: ['clave', 'afiliacion'],	 
		autoLoad: false	  
	});
	
	if(strPerfil== "CALL CENTER CAD")   { // F021 - 2015
		storeAfiliacion.loadData(	[
			['1','Cadena Productiva'],
			['2','Proveedores'],	
			['5','Distribuidores'],
			['9','Intermediarios Bancarios/No Bancarios']	
		]);
	} else {
		storeAfiliacion.loadData( [
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],
			//['3','Proveedor Internacional'],
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],	
			//['12','Cadena Productiva Internacional'],
			//['13','Contragarante'],
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo'],
			['16','Cliente Externo']
		]);
	}
	
	var  elementosForma =  [
		{
			xtype				: 'combo',
			id					: 'cmbTipoAfiliado1',
			name				: 'cmbTipoAfiliado',
			hiddenName 		:'cmbTipoAfiliado', 
			fieldLabel		: 'Tipo de Afiliado',
			width				: 250,
			forceSelection	: true,
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'clave',
			displayField	:'afiliacion',
			value				: '1',	
			store				: storeAfiliacion,
			listeners: {
				select: {
					fn: function(combo) {						
						var valor = Ext.getCmp('cmbTipoAfiliado1').getValue();						
						creditoElec(valor);						
					}
				}
			}
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'N�mero de Nafin Electr�nico',		
			name: 'txtNumElectronico',
			id: 'txtNumElectronico1',
			allowBlank: true,
			maxLength: 30,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Nombre',		
			name: 'txtNombre',
			id: 'txtNombre1',
			allowBlank: true,
			maxLength: 30,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'combo',
			fieldLabel: 'Estado',
			name: 'sel_edo',
			id: 'sel_edo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'sel_edo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			forceSelection : true,
			store: catalogoEstado			
		},
		{
			xtype			: 'checkbox',
			id				: 'convenio_unico1',
			name			: 'convenio_unico',
			hiddenName	: 'convenio_unico',
			fieldLabel	: 'Opera Convenio �nico',			
			tabIndex		: 31,
			enable 		: true			
		}
		,{ 	xtype: 'textfield',  hidden: true, id: 'permisos', 	value: '' }	
	];
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Consulta -Cadenas Productivas',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
					
					fp.el.mask('Enviando...', 'x-mask-loading');		
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15						
						})
					});					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15consEPOext.jsp';					
				}
			}
		]	
		
	});
	
	
		var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,			
			NE.util.getEspaciador(20)
		]
	});
	
	catalogoEstado.load();
	 
	Ext.Ajax.request({
		url: '15consEPO.data.jsp',
		params: {
			informacion: "valoresIniciales",
			idMenuP:idMenu
		},
		callback: procesaValoresIniciales
	});	
	
});