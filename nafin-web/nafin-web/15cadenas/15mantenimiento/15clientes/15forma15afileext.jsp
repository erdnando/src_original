<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		com.netro.afiliacion.*,
		com.netro.zip.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	JSONObject archivoCSV = new JSONObject();
	JSONObject archivoPDF = new JSONObject();
	JSONObject registroAcuse = new JSONObject();
	JSONArray arrAcuse = new JSONArray();
	boolean flag = true;
	boolean flagExt = true;
	boolean flagReg = true;
	ParametrosRequest req = null;
	String rutaArchivo = "";
	String PATH_FILE	=	strDirectorioTemp;
	String itemArchivo = "";
	String extension = "";
	String folio = "";
	String fechaHora = "";
	String numProceso = "";
	String numTotal = "";
	String totalMonto = "";
	String msgError = "";
	int totalRegistros = 0;
	int tamanio = 0;
	boolean bTodoOK = false;
	boolean bSinError = false;
	String ruta = "";
	String sArchivoErrores = "";
	String sPymesOK = "";
	String NoIf = "";
	Hashtable hAfiliaCE = new Hashtable();
	List lstrRegAfil = new ArrayList();
	
	BigDecimal montoTotal = new BigDecimal("0.0");
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		extension = (req.getParameter("hidExtension") == null)? "" : req.getParameter("hidExtension");
		String TipoPYME = (req.getParameter("TipoPYME") == null)? "" : req.getParameter("TipoPYME");
		NoIf = (req.getParameter("NoIf") == null)? "" : req.getParameter("NoIf");
		
		
		try{
			int tamaPer = 0;
			if (extension.equals("txt")){
				upload.setSizeMax(200 * 1024);	//700 Kb
				tamaPer = (200 * 1024);
			}

			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName();
			InputStream archivoCsv = fItem.getInputStream();
			tamanio			= (int)fItem.getSize();

			rutaArchivo = PATH_FILE+itemArchivo;
			
			fItem.write(new File(rutaArchivo));		
			System.out.println("tamanio=="+tamanio);
			System.out.println("tamaPer=="+tamaPer);
			System.out.println("itemArchivo=="+itemArchivo);
			
			if (tamanio > tamaPer){
				flag = false;
				msgError = "El tama�o del archivo TXT es mayor al permitido";
			}
			if (flag){
				fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				try{
					
					Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
				
					if(!("TXT".equals(extension.toUpperCase()))) {
						flagExt = false;
						msgError = "El archivo no es TXT";
					}else{
						BufferedReader brt=  null;
						String linea = "";
						
						java.io.File ft = new java.io.File(rutaArchivo);
						brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
						
						String numContrato	= "";
						String nombreIF		= "";
						String cveSirac	= "";
						String saldoHoy	= "";
						String cveMoneda	= "";
						
						VectorTokenizer vtd = null;
						Vector vecdet = null;
		
						StringBuffer sbDatosPymes = new StringBuffer();
						while((linea=brt.readLine())!=null) {
							sbDatosPymes.append(linea.trim()+"\n");
						}
						hAfiliaCE = BeanAfiliacion.afiliaPymeCE(sbDatosPymes.toString(), "", "", "", NoIf, iNoUsuario, strTipoUsuario);
						bTodoOK = new Boolean(hAfiliaCE.get("bTodoOK").toString()).booleanValue();
						
						if(bTodoOK){
							StringBuffer contenidoArchivo = new StringBuffer("PYMES Afiliadas a Cr�dito Electr�nico\nN�mero de Cliente SIRAC,Nombre Completo o Razon Social,RFC,Tipo Persona,Sector Econ�mico,Subsector,Rama,Clase,Estrato,N�mero de Folio"+(strTipoUsuario.equals("NAFIN")?",N�mero de Cliente TROYA":"")+"\n");
							String sPymesDadasAlta = hAfiliaCE.get("sNoPymesAlta").toString();
							
							
							HashMap hmData = null;
							Vector vDatosPyme = BeanAfiliacion.getPymeAfiliadasCE(sPymesDadasAlta);
							for(int i=0; i<vDatosPyme.size(); i++) {
								hmData = new HashMap();
								Vector vPyme = (Vector)vDatosPyme.get(i);
								contenidoArchivo.append(vPyme.get(0).toString()+","+(vPyme.get(6).toString().equals("F")?vPyme.get(1).toString()+" "+vPyme.get(2).toString()+" "+vPyme.get(3).toString():vPyme.get(4).toString())+","+vPyme.get(5).toString()+","+(vPyme.get(6).toString().equals("F")?"F�sica":"Moral")+","+vPyme.get(7).toString().replace(',',' ')+","+vPyme.get(8).toString().replace(',',' ')+","+vPyme.get(9).toString().replace(',',' ')+","+vPyme.get(10).toString().replace(',',' ')+","+vPyme.get(11).toString()+","+vPyme.get(12).toString()+","+(strTipoUsuario.equals("NAFIN")?vPyme.get(13).toString():"")+"\n");
								
								hmData.put("NUMSIRAC", vPyme.get(0).toString());
								hmData.put("NOMBRECOMPLETO", (vPyme.get(6).toString().equals("F")?vPyme.get(1).toString()+" "+vPyme.get(2).toString()+" "+vPyme.get(3).toString():vPyme.get(4).toString()));
								hmData.put("RFC", vPyme.get(5).toString());
								hmData.put("TIPOPERSONA", (vPyme.get(6).toString().equals("F")?"F�sica":"Moral"));
								hmData.put("SECTORECO", vPyme.get(7).toString());
								hmData.put("SUBSECTOR", vPyme.get(8).toString());
								hmData.put("RAMA", vPyme.get(9).toString());
								hmData.put("CLASE", vPyme.get(10).toString());
								hmData.put("ESTRATO", vPyme.get(11).toString());
								hmData.put("NUMFOLIO", vPyme.get(12).toString());
								hmData.put("NUMTROYA", vPyme.get(13).toString());
								
								
								lstrRegAfil.add(hmData);
							}
							
							String nombreArchivo="";
							CreaArchivo archivo = new CreaArchivo();
							if (archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
								nombreArchivo = archivo.nombre;
								ruta = strDirecVirtualTemp + nombreArchivo ;
							}
							
							
						}else{
							String sSinError = hAfiliaCE.get("sSinError").toString();
							if(sSinError.length() > 0) { 
								bSinError = true;
								sArchivoErrores = hAfiliaCE.get("sArchivoErrores").toString();
								sPymesOK = hAfiliaCE.get("sPymesOK").toString();
							
							}
						}

						/*
						if(totalRegistros>0 && totalRegistros<=30000){
							numProceso = OperElec.cargaDatosTmpFondoLiquido(rutaArchivo);
						}else{
							flagReg = false;
							msgError = "'El archivo CSV no puede contener mas de 30000 registros";
						}*/
					}
				}catch (Exception e){
						e.printStackTrace();
						throw new AppException("Ocurrio un error al generar el archivo", e);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}
	
%>	
{
	"success": true,
	"flag":	<%=(flag)?"true":"false"%>,
	"flagExt":	<%=(flagExt)?"true":"false"%>,
	"flagReg":	<%=(flagReg)?"true":"false"%>,
	"bTodoOK":	<%=bTodoOK%>,
	"bSinError":	<%=bSinError%>,
	"urlArchivo": '<%=ruta%>',
	"sArchivoErrores": '<%=sArchivoErrores%>',
	"sPymesOK": '<%=sPymesOK%>',
	"NoIf": '<%=NoIf%>',
	"registros":	<%=JSONArray.fromObject(lstrRegAfil)%>,
	"hAfiliaCE":	<%=JSONObject.fromObject(hAfiliaCE)%>,
	"totalRegistros":	'<%=totalRegistros%>',
	"msgError":	'<%=msgError%>'
	
}