Ext.onReady(function(){
	
var visibleNumProv=false;
//-------------------Handers-----------------
		
		var procesarConsultaData = function(store,arrRegistros,opts){
		//accionBotones();
		var boton=Ext.getCmp('btnGenerarPDF');
			
			boton.enable();
			
			var botonE=Ext.getCmp('btnGenerarArchivoTodas');
			botonE.enable();
			
			grid.setVisible(true);
			fp.el.unmask();
			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					botonE.disable();
					boton.disable();
				}
			}
		}

	function procesarArchivoSuccess(opts, success, response) {
		
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('btnGenerarPDF');
			boton.setIconClass('icoPdf');
			boton.enable();
			
			var botonE=Ext.getCmp('btnGenerarArchivoTodas');
			botonE.setIconClass('icoXls');
			botonE.enable();
			
			
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var accionConsulta = function(estadoSiguiente, respuesta){
if(  estadoSiguiente == "OBTENER_NOMBRE_PYME" ){
		
			fp.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15EstatusEcontractAfiExt.data.jsp',	
				params: {
							numeroDeProveedor:	respuesta.noNafinElec,
							ic_banco_fondeo:		respuesta.ic_banco_fondeo,
							ic_epo:					respuesta.ic_epo,
							informacion: 			"obtenNombrePyme" 
				},
				callback: procesaConsulta
			});

		}else if(  estadoSiguiente == "RESPUESTA_OBTENER_NOMBRE_PYME"){
		
			Ext.getCmp('hid_ic_pyme').setValue(respuesta.ic_pyme);
			Ext.getCmp('_txt_nombre').setValue(respuesta.txtCadenasPymes);
			fp.el.unmask();
			if (respuesta.muestraMensaje != undefined && respuesta.muestraMensaje){
				Ext.Msg.alert("Mensaje de p�gina web.",	respuesta.textoMensaje);
				Ext.getCmp('_txt_nafelec').setValue('');
			}else{

			}

		}
}
var procesaConsulta = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}
//-------------------STORES---------------

var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '15EstatusEcontractAfiExt.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'NOMBRE_EPO'},
				{name: 'NOMBRE_IF'},
				{name: 'NUMERO_PROVEEDOR'},
				{name: 'NAFIN_ELECTRONICO'},
				{name: 'NUMERO_SIRAC'},
				{name: 'NOMBRE_PYME'},
				{name: 'ESTATUS_AFILIACION'},
				{name: 'NOMBRE_PROCESO'},
				{name: 'CAUSA_RECHAZO'},
				{name: 'NOTA'},
				{name: 'NOTA_IF'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
						}	},
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
 var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15EstatusEcontractAfiExt.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
   var catalogoIF = new Ext.data.JsonStore
  ({
	   id: 'catologoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15EstatusEcontractAfiExt.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoIF'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
	
	var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	
	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15EstatusEcontractAfiExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//---------------------------COMPONENTES-------------------
var elementosForma = [
{
				xtype: 'combo',
				fieldLabel: 'Nombre de la EPO',
				emptyText: 'Seleccionar una EPO',
				displayField: 'descripcion',
				//allowBlank: false,
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'HicEpo',
				id: 'icEpo',
				mode: 'local',
				hiddenName: 'HicEpo',
				forceSelection: true,
				listeners: {
			
				}
			},{
						
				xtype: 'combo',
				width: 250,
				emptyText: 'Seleccionar un IF',
				fieldLabel: 'Nombre del IF',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoIF,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'Hif',
				id: 'if',
				mode: 'local',
				hiddenName: 'Hif',
				//allowBlank: false,
				forceSelection: true
				},
				{
					xtype:'hidden',
					id:	'hid_nombre',
					value:''
				},{
					xtype:'hidden',
					id:	'hid_ic_pyme',
					value:''
				},
				{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
						xtype:		'textfield',
						name:			'txt_nafelec',
						id:			'_txt_nafelec',
						maskRe:		/[0-9]/,
						allowBlank:	true,
						
						fieldLabel: 'N�mero N@E',
						maxLength:	25,
						listeners:	{
							'change':function(field){
											if ( !Ext.isEmpty(field.getValue()) ) {
	
												var respuesta = new Object();
												respuesta["noNafinElec"]		= field.getValue();
												//respuesta["ic_banco_fondeo"]	= Ext.getCmp('_ic_banco_fondeo').getValue();
												//respuesta["ic_epo"]				= Ext.getCmp('ic_epo').getValue();
												
												accionConsulta("OBTENER_NOMBRE_PYME",respuesta);
	
											}
										}
						}
					},{
						xtype:			'textfield',
						name:				'txt_nombre',
						id:				'_txt_nombre',
						width:			270,
						hidden:			true,
						disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
						disabled:		true,
						allowBlank:		true,
						maxLength:		100
					},{
							xtype:	'button',
							id:		'btnBuscaA',
							iconCls:	'icoBuscar',
							text:		'B�squeda Avanzada',
							handler: function(boton, evento) {
										var winVen = Ext.getCmp('winBuscaA');
										var valorEpo=Ext.getCmp('icEpo').getValue();
										if(valorEpo!=''){
											visibleNumProv=true;
											//Ext.getCmp('txtNe').setVisible(true);
										}else{
											visibleNumProv=false;
											//Ext.getCmp('txtNe').setVisible(false);
										}
											if (winVen){
												Ext.getCmp('fpWinBusca').getForm().reset();
												Ext.getCmp('fpWinBuscaB').getForm().reset();
												
												Ext.getCmp('cmb_num_ne').setValue();
												Ext.getCmp('cmb_num_ne').store.removeAll();
												Ext.getCmp('cmb_num_ne').reset();
												
												
												
												Ext.getCmp('txtNe').setVisible(visibleNumProv);
												winVen.show();
											}else{
												var winBuscaA = new Ext.Window ({
													id:'winBuscaA',
													height: 320,
													x: 300,
													y: 100,
													width: 550,
													heigth: 100,
													modal: true,
													closeAction: 'hide',
													title: 'B�squeda Avanzada',
													items:[
														{
															xtype:'form',
															id:'fpWinBusca',
															frame: true,
															border: false,
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 130,
															items:[
																{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:'Utilice el * para b�squeda gen�rica'
																},{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:''
																},{
																	xtype: 'textfield',
																	name: 'nombre_pyme',
																	id:	'txtNombre',
																	fieldLabel:'Nombre',
																	maxLength:	100
																},{
																	xtype: 'textfield',
																	name: 'rfc_pyme',
																	id:	'txtRfc',
																	fieldLabel:'RFC',
																	maxLength:	20
																},{
																	xtype: 'textfield',
																	name: 'num_pyme',
																	id:	'txtNe',
																	hidden:!visibleNumProv,
																	fieldLabel:'N�mero Proveedor',
																	maxLength:	25
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Buscar',
																	iconCls:'icoBuscar',
																	handler: function(boton) {
																					catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {
																					Ext.getCmp('winBuscaA').hide();
																				}
																}
															]
														},{
															xtype:'form',
															frame: true,
															id:'fpWinBuscaB',
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															monitorValid: true,
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 80,
															items:[
																{
																	xtype: 'combo',
																	id:	'cmb_num_ne',
																	name: 'ic_pyme',
																	hiddenName : 'ic_pyme',
																	fieldLabel: 'Nombre',
																	emptyText: 'Seleccione Proveedor. . .',
																	displayField: 'descripcion',
																	valueField: 'clave',
																	triggerAction : 'all',
																	forceSelection:true,
																	allowBlank: false,
																	typeAhead: true,
																	mode: 'local',
																	minChars : 1,
																	store: catalogoNombreProvData,
																	tpl : NE.util.templateMensajeCargaCombo
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Aceptar',
																	iconCls:'aceptar',
																	formBind:true,
																	//hidden:true,
																	handler: function() {
																					if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																						var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																						var cveP = disp.substr(0,disp.indexOf(" "));
																						var desc = disp.slice(disp.indexOf(" ")+1);
																						Ext.getCmp('hid_ic_pyme').setValue(Ext.getCmp('cmb_num_ne').getValue())
																						Ext.getCmp('_txt_nafelec').setValue(cveP);
																						Ext.getCmp('_txt_nombre').setValue(desc);
																						Ext.getCmp('winBuscaA').hide();
																					}
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {	Ext.getCmp('winBuscaA').hide();	}
																}
															]
														}
													]
												}).show();
											}
										}
						}
				]
				},{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:'displayfield',
							id:'disEspacio',
							text:''
						}
						
					]
				},{
					xtype: 'textfield',
					name: 'noProveedor',
					id:	'txtProv',
					anchor: '55%',
					fieldLabel:'No. Proveedor',
					maxLength:	100
				}
	
]

	var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						
						{
						
						header: 'EPO',
						tooltip: 'EPO',
						sortable: true,
						dataIndex: 'NOMBRE_EPO',
						width: 150,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},{
						
						header: 'IF',
						tooltip: 'IF',
						sortable: true,
						dataIndex: 'NOMBRE_IF',
						width: 150,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'No. Proveedor',
						tooltip: 'No. Proveedor',
						sortable: true,
						dataIndex: 'NUMERO_PROVEEDOR',
						width: 150,
						align: 'center'
						},
						{
						header: 'No. Nafin Electr�nico',
						tooltip: 'No. Nafin Electr�nico',
						sortable: true,
						dataIndex: 'NAFIN_ELECTRONICO',
						width: 150,
						align: 'center'
						},
						{
						header: 'No. SIRAC',
						tooltip: 'No. SIRAC',
						sortable: true,
						dataIndex: 'NUMERO_SIRAC',
						width: 130,
						align: 'center'
						
						},
						{
						align:'center',
						header: 'Nombre o Raz�n Social',
						tooltip: 'Nombre o Raz�n Social',
						sortable: true,
						dataIndex: 'NOMBRE_PYME',
						width: 130,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},{
						header: 'Estatus',
						tooltip: 'Estatus',
						sortable: true,
						dataIndex: 'ESTATUS_AFILIACION',
						width: 150,
						align: 'center'
						},
						{
						
						header:'Nombre del Proceso',
						tooltip: 'Nombre del Proceso',
						sortable: true,
						dataIndex: 'NOMBRE_PROCESO',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'Causa del Rechazo',
						tooltip: 'Causa del Rechazo',
						sortable: true,
						dataIndex: 'CAUSA_RECHAZO',
						width: 200,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Nota',
						tooltip: 'Nota',
						sortable: true,
						dataIndex: 'NOTA_IF',
						width: 200,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						}
						
						
				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					autoScroll:true,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					
					items: ['->','-',
							
								
								{
									xtype: 'button',
									//height: 40,
									width: 50,
									text: 'Generar PDF',
									iconCls: 'icoPdf',
									id: 'btnGenerarPDF',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
										Ext.Ajax.request({
											url: '15EstatusEcontractAfiExt.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoPDF',
												start: cmpBarraPaginacion.cursor,
												limit: cmpBarraPaginacion.pageSize}),
											callback: procesarArchivoSuccess
										});
									}
								},
								{
									xtype: 'button',
									
									//height: 40,
									width: 50,
									iconCls: 'icoXls',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivoTodas',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '15EstatusEcontractAfiExt.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV'}),
											callback: procesarArchivoSuccess
										});
									}
								},
								
								'-']
				}
		});

var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 550,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Consultar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						if(Ext.getCmp('icEpo').getValue()==''){
							Ext.getCmp('icEpo').markInvalid('Este campo es obligatorio');
							Ext.getCmp('icEpo').focus();
							return;
						
						}
						fp.el.mask('Procesando...', 'x-mask-loading');
						
						consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15})
								});
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});

var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
			fp,  NE.util.getEspaciador(30),grid
	  ]
  });
  catalogoEpo.load();
  catalogoIF.load();
});