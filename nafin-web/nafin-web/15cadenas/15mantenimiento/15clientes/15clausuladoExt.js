	
Ext.onReady(function() {
var banderaEpo = false;
var banderaComboEpo = false;
//var ahora=new Date();
//var mes = (parseInt(ahora.getMonth()) + 1)<10?0+(parseInt(ahora.getMonth()) + 1).toString():(parseInt(ahora.getMonth()) + 1).toString();

function verificaFechas(fec,fec2){		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambas fechas son necesarias');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambas fechas son necesarias');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	

	var busqAvanzadaSelect=0;
	Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);

//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT

var procesarCatalogoBancoFondeo = function(store, arrRegistros, opts) {
Ext.getCmp("BancoFondeo").setValue((store.getRange()[0].data).clave);
Ext.getCmp('BancoFondeo').show();
}

var procesarCatalogoNombreEpo = function(store, arrRegistros, opts) {
	store.insert(0,new Todas({ 
   clave: "", 
   descripcion: "Seleccionar Todos", 
   loadMsg: ""})); 
	 
	store.commitChanges(); 
	Ext.getCmp("NombreEpo").setValue((store.getRange()[0].data).clave);
	Ext.getCmp('NombreEpo').show();
}

var procesarConsultaData = function(store, arrRegistros, opts) {
	
		var grid = Ext.getCmp('grid');
		
		if (arrRegistros != null) {			
			if (!grid.isVisible()) {
				grid.show();
			}
			
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');

			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnBajarPDF').hide();
				el.unmask();
				
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnBajarPDF').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

var returnInit= function(opts, success, response) { 
	banderaEpo=Ext.util.JSON.decode(response.responseText).BanderaEpo;
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {				
		if(Ext.util.JSON.decode(response.responseText).msg!='no result')
		{
			var jsonObj = Ext.util.JSON.decode(response.responseText);				
			Ext.getCmp('ic_if').setValue(jsonObj.ic_if);
			Ext.getCmp('cvePerf').setValue(jsonObj.cvePerf);
			var cveAux=jsonObj.cvePerf;			
			if( cveAux==8 || cveAux==4 )  {	
				catalogoNombreEpo.load({
						params: Ext.apply({
						cvePerf: cveAux
						//cvePerf:'8'
					})
				});
			}
						/*
						catalogoBancoFondeo.load({
								params: Ext.apply({
								cvePerf: cveAux
								//cvePerf:'8'
							})
						});*/
			}		
	} else {
				NE.util.mostrarConnError(response,opts);
	}
}

var successAjaxFn = function(opts, success, response) { 

if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
					Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
					Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);			
		}/* else {
			NE.util.mostrarConnError(response,opts);
		}*/
}

	var bajarContrato =  function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	/*	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			
		} else {
			alert("error");
		}*/
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			//var boton = Ext.getCmp('btnGenerarArchivo');
			//boton.enable();
			//Ext.getCmp('btnImprimir').enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}		
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton = Ext.getCmp('btnGenerarArchivo');
			boton.enable();
			var boton2 = Ext.getCmp('btnGenerarPDF');
			boton2.enable();		
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}	


	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			Ext.getCmp('btnBuscar').enable();
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT

	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15clausuladoExt.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'IC_EPO'},
			{name: 'IC_CONSECUTIVO'},
			{name: 'NOMPYME'},
			{name: 'PROVEEDOR'},
			{name: 'NOMEPO'},
			{name: 'IC_USUARIO'},
			{name: 'DF_ACEPTACION'},
			{name: 'ORIGEN'},
			{name:'DOC_CONTRATO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
		
	});

 var catalogoBancoFondeo = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15clausuladoExt.data.jsp',
			listeners: {
			
			load: procesarCatalogoBancoFondeo,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoBancoFondeo'
			
		},
		totalProperty : 'total',
		autoLoad: false
		
	});
	
	var catalogoNombreEpo = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '15clausuladoExt.data.jsp',
		listeners: {			
			load: procesarCatalogoNombreEpo,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoNombreEpo'			
		},
		totalProperty : 'total',
		autoLoad: false		
	});

var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion','ic_pyme','loadMsg'],
		url : '15clausuladoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
var grid = {
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		columns: [
			{
				header: 'N�m PyME', tooltip: 'N�m PyME',
				dataIndex: 'PROVEEDOR',
				width: 200,
				resizable: true,
				sortable: true,	align: 'center',
				align: 'left'
			},{
				header: 'Nombre PyME', tooltip: 'Nombre PyME',
				dataIndex: 'NOMPYME',
				sortable: true,	align: 'center',
				width: 200, resizable: true,
				align: 'left'
			},{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',
				dataIndex: 'NOMEPO',
				sortable: true,	align: 'center',
				width: 200, resizable: true,
				align: 'left'
			},{
				header: 'Usuario', tooltip: 'Usuario',
				dataIndex: 'IC_USUARIO',
				sortable: true,	align: 'center',
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Fecha/Hora', tooltip: 'Fecha/Hora',
				dataIndex: 'DF_ACEPTACION',
				sortable: true,	align: 'center',
				width: 160, resizable: true,
				align: 'left'
			},{
				xtype: 'actioncolumn',
				header: 'Contrato', tooltip: 'Contrato',
				dataIndex: '',
				width: 200, resizable: true,
				align: 'center', 
				/*renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('DOC_CONTRATO') ==0 ) {
						return 'N/A';
					}
				},*/
				items: [
					{
						iconCls: 'icoLupa',
						tooltip: 'Descargar Clausulado',
					   getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('DOC_CONTRATO') !=0 ) {
								this.items[0].tooltip = 'Buscar';
								return 'icoBuscar';		
							}
						},handler: function(grid, rowIdx, colIds){
							var reg = grid.getStore().getAt(rowIdx);
							 if(reg.get('DOC_CONTRATO') !=0 ) {
								Ext.Ajax.request({
									url: '15clausuladoExt.data.jsp',
									params: {
										informacion: 'Contrato',
										ic_epo:reg.get('IC_EPO'),
										ic_consecutivo:reg.get('IC_CONSECUTIVO')
									},
									callback: bajarContrato
								});
							}else{
								alert("No hay archivo");
							}
 						}
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		colunmWidth: true,
		frame: true,
		collapsible: true,
		bbar: {
		xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						//boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					
						
						Ext.Ajax.request({
							url: '15clausuladoExt.data.jsp',
							params: Ext.apply(paramSubmit,{
							operacion:'XLS',
							informacion: 'Consulta'
							}),
							//callback: procesarSuccessFailureGenerarArchivo
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
					var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						boton.disable();
						//boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					
						
						Ext.Ajax.request({
							url: '15clausuladoExt.data.jsp',
							params: Ext.apply(paramSubmit,{
							operacion:'PDF',
							informacion: 'Consulta',
							start: cmpBarraPaginacion.cursor,
							limit: cmpBarraPaginacion.pageSize
							}),
							//callback: procesarSuccessFailureGenerarPDF
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
	};
	

	var elementosFecha = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Aceptaci�n de',
			combineErrors: false,
						msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'txt_fecha_acep_de',
					id: 'df_consultaMin',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_consultaMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
					
					
					},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txt_fecha_acep_a',
					id: 'df_consultaMax',
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_consultaMin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
					
				}
			]
		}
	];
	var combosForma=[
	/*{
			xtype: 'combo',
			name: 'BancoFondeo',
			id: 'BancoFondeo',
			hiddenName : 'noBancoFondeo',
			fieldLabel: 'Banco de Fondeo',
			store: catalogoBancoFondeo,
			displayField: 'descripcion',
			valueField: 'clave',
			typeAhead: true,
			forceSelection : true,
			triggerAction : 'all',
			minChars : 1,
			mode: 'local',
			width:300,
			hidden:true
			},
		*/	
			{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'NombreEpo',
			hiddenName : 'ic_epo',
			fieldLabel: 'Nombre de la Epo',
			store: catalogoNombreEpo,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			typeAhead: true,
			forceSelection : true,
			triggerAction : 'all',
			minChars : 1,
			listeners:{
				select: function(combo){
					if(combo.getValue() == ''){
						banderaComboEpo = false;
					}else{
						banderaComboEpo = true;
					}
				}
			},			
			hidden:true
			}
	]
	var elementosForma = [
		{
				xtype: 'hidden',
				name: 'ic_pyme',
				id: 'ic_pyme'
		},
		{
				xtype: 'hidden',
				name: 'cvePerf',
				id: 'cvePerf'
		},
		/*{
				xtype: 'hidden',
				name: 'ic_epo',
				id: 'ic_epo'
		},*/
		{
				xtype: 'hidden',
				name: 'ic_if',
				id: 'ic_if'
		},
		{
		xtype: 'compositefield',
		width: 900,
		fieldLabel: 'Nombre de la PYME',
		labelWidth: 200,
		items:[			
		{
			xtype: 'numberfield',
			name: 'txt_nafelec',
			id:'txt_nafelec',
			width:90,
			listeners: {
				'blur': function(){
				// Petici�n b�sica  
				Ext.Ajax.request({  
            url: '15clausuladoExt.data.jsp',  
            method: 'POST',  
            callback: successAjaxFn,  
            params: {  
                informacion: 'pymeNombre' ,
					 txt_nafelec: Ext.getCmp('txt_nafelec').getValue()
            }  
        });  

				}
			}
		},
		{
			xtype: 'textfield',
			value: '',
			width:290,
			disabled: true,
			id:'hid_nombre',
			name:'hid_nombre',
			hiddenName : 'hid_nombre',
			mode: 'local',
			resizable: true,
			triggerAction : 'all'
		}
		]},
		{
		xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					id:'disEspacio',
					text:''
				},
				{
		
				xtype: 'button',
				text: 'Busqueda Avanzada',
				id: 'btnAvanzada',
				width: 100,
				iconCls:	'icoBuscar',
				handler: function(boton, evento) {
								var winVen = Ext.getCmp('winBuscaA');
									if (winVen){
										Ext.getCmp('fpWinBusca').getForm().reset();
										Ext.getCmp('fpWinBuscaB').getForm().reset();
										Ext.getCmp('cmb_num_ne').store.removeAll();
										Ext.getCmp('cmb_num_ne').reset();
										
										if(banderaComboEpo || banderaEpo)
											Ext.getCmp('txtNe').setVisible(true);
										else
											Ext.getCmp('txtNe').setVisible(false);
										
										winVen.show();										
									}else{
										var winBuscaA = new Ext.Window ({
											id:'winBuscaA',
											height: 300,
											x: 300,
											y: 100,
											width: 600,
											
											modal: true,
											closeAction: 'hide',
											title: '',
											items:[
												{
													xtype:'form',
													id:'fpWinBusca',
													frame: true,
													border: false,
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													labelWidth: 140,
													items:[
														{
															xtype:'displayfield',
															frame:true,
															border: false,
															value:'Utilice el * para b�squeda gen�rica'
														},{
															xtype:'displayfield',
															frame:true,
															border: false,
															value:''
														},{
															xtype: 'textfield',
															name: 'nombre_pyme',
															id:	'txtNombre',
															fieldLabel:'Nombre',
															maxLength:	100
														},{
															xtype: 'textfield',
															name: 'rfc_pyme',
															id:	'txtRfc',
															fieldLabel:'RFC',
															maxLength:	20
														},{
															xtype: 'textfield',
															name: 'num_pyme',
															id:	'txtNe',
															fieldLabel:'N�mero Proveedor',
															hidden: true,
															maxLength:	25
														}
													],
													buttons:[
														{
															text:'Buscar',
															id:'btnBuscar',
															iconCls:'icoBuscar',
															handler: function(boton) {
																			/*if ( Ext.isEmpty(Ext.getCmp('txtNombre').getValue()) && Ext.isEmpty(Ext.getCmp('txtRfc').getValue()) && Ext.isEmpty(Ext.getCmp('txtNe').getValue()) ){
																				Ext.Msg.alert(boton.text,'No existe informaci�n con los criterios determinados');
																				return;
																			}*/
															
															catalogoNombreData.load({ 
																params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues(),{
																						ComboEpo: Ext.getCmp('NombreEpo').getValue()	
																						}
																)																																				
															  });
															}
														},{
															text:'Cancelar',
															iconCls: 'icoLimpiar',
															handler: function() {
																			Ext.getCmp('winBuscaA').hide();
																		}
														}
													]
												},{
													xtype:'form',
													frame: true,
													id:'fpWinBuscaB',
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													monitorValid: true,
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													items:[
														{
															xtype: 'combo',
															id:	'cmb_num_ne',
															name: 'cmb_num_ne',
															hiddenName : 'Hcmb_num_ne',
															fieldLabel: 'Nombre',
															emptyText: 'Seleccione . . .',
															displayField: 'descripcion',
															valueField: 'clave',
															triggerAction : 'all',
															forceSelection:true,
															allowBlank: false,
															typeAhead: true,
															mode: 'local',
															minChars : 1,
															store: catalogoNombreData,
															tpl : NE.util.templateMensajeCargaCombo,
															listeners: {
																	 select: function(combo, record, index) {
																			 busqAvanzadaSelect=index;
																			 }
																	 }
														}
													],
													buttons:[
														{
															text:'Aceptar',
															iconCls:'aceptar',
															formBind:true,
															handler: function() {
																			if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																				var reg = Ext.getCmp('cmb_num_ne').getStore().getAt(busqAvanzadaSelect).get('ic_pyme');
																				Ext.getCmp('ic_pyme').setValue(reg);
																				var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																				var desc = disp.slice(disp.indexOf(" ")+1);
																				Ext.getCmp('txt_nafelec').setValue(Ext.getCmp('cmb_num_ne').getValue());
																				Ext.getCmp('hid_nombre').setValue(desc);
																				Ext.getCmp('winBuscaA').hide();
																			}
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoLimpiar',
															handler: function() {	Ext.getCmp('winBuscaA').hide();	}
														}
													]
												}
											]
										}).show();		
										Ext.getCmp('txtNe').setVisible(banderaEpo);
									}
								}
				
			
			
		}]},
		{
		xtype: 'compositefield',
			items:[
		{
		xtype: 'textfield',
      fieldLabel: 'Usuario',
      name: 'txt_usuario',
		id:'idUsuario',
		width:250
		}
		]}
		
		
	];
	//Forma para hacer la busqueda filtrada
	var fp = new Ext.form.FormPanel ({
		//xtype: 'FormPanel',
		id: 'forma',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 600,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[combosForma,elementosForma,elementosFecha],
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					var df_consultaNotMin = Ext.getCmp("df_consultaMin");
					var df_consultaNotMax = Ext.getCmp("df_consultaMax");				
				
					var Desde =  Ext.util.Format.date(df_consultaNotMin.getValue(),'d/m/Y');   
					var Hasta =  Ext.util.Format.date(df_consultaNotMax.getValue(),'d/m/Y'); 
					
					if (!Ext.isEmpty(df_consultaNotMin.getValue()) && Ext.isEmpty(df_consultaNotMax.getValue()) || Ext.isEmpty(df_consultaNotMin.getValue()) && !Ext.isEmpty(df_consultaNotMax.getValue())) {
						if(!Ext.isEmpty(df_consultaNotMin.getValue()) && Ext.isEmpty(df_consultaNotMax.getValue())){
                     df_consultaNotMax.markInvalid('Debe capturar la fecha de Notificaci�n final');	 
                  }
                  if(Ext.isEmpty(df_consultaNotMin.getValue()) && !Ext.isEmpty(df_consultaNotMax.getValue())){
                     df_consultaNotMin.markInvalid('Debe capturar ambas fechas de  Notificaci�n  o dejarlas en blanco');
                  }
                  consultaDataGrid.removeAll();
                  grid.hide();
						return;
					}									
				
				if (!Ext.isEmpty(df_consultaNotMin.getValue()) &&  !Ext.isEmpty(df_consultaNotMax.getValue())){
						if(!isdate(Desde)){
							df_consultaNotMin.markInvalid("Favor de teclear la fecha con el formato  (dd/mm/aaaa)");				
							Ext.getCmp('grid').hide();
							df_consultaNotMin.focus();		
							return;
						}
						 if(!isdate(Hasta)){
							df_consultaNotMax.markInvalid("Favor de teclear la fecha con el formato  (dd/mm/aaaa)");				
							Ext.getCmp('grid').hide();
							df_consultaNotMax.focus();		
							return;
						}
				}				
				
				if(verificaFechas('df_consultaMin','df_consultaMax')){
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.getCmp('grid').setVisible(true);
						consultaDataGrid.load({
								params: Ext.apply(paramSubmit,{
								operacion: 'Generar',
								start:0,
								limit:15,
								hid_nombre:Ext.getCmp('hid_nombre').getValue()
							})
						});
					}
				} //fin handler
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function(boton, evento) {
					location.reload();
				}
				
			}
			
		]
	});//FIN DE LA FORMA
	
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});
	
	Ext.Ajax.request({  
            url: '15clausuladoExt.data.jsp',  
            method: 'POST',  
            callback: returnInit,  
            params: {  
                informacion: 'initPage'
					 }  
        });

});
