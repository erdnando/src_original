Ext.onReady(function(){
	
	
	function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		}else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} 	else if (pagina == 2) { 	// Distribuidor						
			window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 		 
		} else if (pagina == 3) { 	//Proveedor
			window.location = '15forma01Ext.jsp'; 
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} else if (pagina == 6) { 	//Contragarante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 	
		} else if (pagina == 9) { 	// Mandante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
		} else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		}	else if (pagina == 11) { 	//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 	
		}	else if (pagina == 12) { 	//Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliaClienteExternoExt.jsp"; 	
		}
	}
	
	
	var dataStatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['0','Cadena Productiva'],
			['1','Intermediario Financiero'],
			['2','Distribuidores'],			
			['3','Proveedores'],			
			['4','Afiliados Credito Electronico'],
			['5','Cadena Productiva Internacional'],			
			['6','Contragarante'],						
			['9','Mandante'],				
			['10','Afianzadora'],							
			['11','Universidad-Programa Cr�dito Educativo'],
			['12','Cliente Externo'] 
		]
	});
	

	var  elementosForma =  [
		{
			xtype: 'combo',	
			name: 'status',	
			id: 'cmbTipoAfiliado',	
			fieldLabel: 'Tipo de Afiliado', 
			mode: 'local',	
			hiddenName : 'status',	
			emptyText: 'Seleccionar  ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : dataStatus,	
			displayField : 'descripcion',	
			valueField : 'clave',
			listeners: {
				select: {
					fn: function(combo) {
						var valor  = combo.getValue();
						creditoElec(valor);		
					}
				}
			}
		}
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Consulta',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true	
	});
	
	       
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp		
		]
	});

});