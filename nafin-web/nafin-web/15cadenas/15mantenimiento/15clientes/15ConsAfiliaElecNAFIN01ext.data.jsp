<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*, 
				com.netro.procesos.*, 
				netropology.utilerias.*, 
				com.netro.cadenas.*,
				com.netro.afiliacion.*,
				com.netro.model.catalogos.*,   
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%  
String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String descripcion = (request.getParameter("txtDescripcion") !=null)?request.getParameter("txtDescripcion"):"";
String claveEPO = (request.getParameter("claveEPO") !=null)?request.getParameter("claveEPO"):"";
String txtNombre = (request.getParameter("txtNombre") !=null)?request.getParameter("txtNombre"):"";
String txtRFC = (request.getParameter("txtRFC") !=null)?request.getParameter("txtRFC"):"";
String txtProveedor = (request.getParameter("txtNumProveedor") !=null)?request.getParameter("txtNumProveedor"):"";
String numPyme = (request.getParameter("numPyme") !=null)?request.getParameter("numPyme"):"";
String numIF = (request.getParameter("numIF") !=null)?request.getParameter("numIF"):"";
String clavePyme = (request.getParameter("clavePyme") !=null)?request.getParameter("clavePyme"):"";
String claveEstatus = (request.getParameter("claveEstatus") !=null)?request.getParameter("claveEstatus"):"";
String fechaInicio = (request.getParameter("fechaInicio") !=null)?request.getParameter("fechaInicio"):"";
String fechaFin = (request.getParameter("fechaFin") !=null)?request.getParameter("fechaFin"):"";

String infoRegresar = "";

//Afiliacion bean = null;
//AfiliacionHome manejoHome = (AfiliacionHome)netropology.utilerias.ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);	
//bean = manejoHome.create();
Afiliacion bean = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

if(informacion.equals("consultaEPO")){
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	List lista = cat.getListaElementos();
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lista);
	infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";

}else if(informacion.equals("consultaIF")){
	CatalogoIF cat = new CatalogoIF();
	cat.setCampoClave("I.ic_if");
	cat.setCampoDescripcion("I.cg_razon_social");
	if(!claveEPO.equals("")){
		cat.setClaveEpo(claveEPO);
	}
	//cat.setG_vobonafin("S");
	List lista = cat.getListaElementosGral();
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lista);
	infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";

}else if(informacion.equals("catalogoBusquedaAvanzada")){
	System.out.println("EPO " +claveEPO);
	System.out.println("NOMBRE " +txtNombre);
	System.out.println("RFC " +txtRFC);
	System.out.println("NUMERO " +txtProveedor);
	List lista = bean.getBusquedaAvanzada(claveEPO, txtNombre, txtRFC, txtProveedor);
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	JSONArray jsonA = new JSONArray();
	
	if(lista.isEmpty()){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if(lista.size() > 0 && lista.size() < 1001 ){
		jsonA = JSONArray.fromObject(lista);
		jsonObj.put("total",  Integer.toString(jsonA.size()));
		jsonObj.put("registros", lista );
	}else if(lista.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();
	System.out.println(infoRegresar);

}else if (informacion.equals("obtenerNombrePyme")){
	List lista = bean.getNombrePyme(claveEPO, numPyme);
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if(!lista.isEmpty()){
		jsonObj.put("idPyme", lista.get(0));
		jsonObj.put("nombrePyme", lista.get(1));
	}else{
		jsonObj.put("muestraMensaje", new Boolean(true));
		if(!claveEPO.equals(""))
			jsonObj.put("textoMensaje", "El nafin electrónico no corresponde a una PyME afiliada a la EPO o no existe.");
		else
			jsonObj.put("textoMensaje", "El nafin electrónico no corresponde a una PyME ó no existe");
	}
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("consultaGeneral") || informacion.equals("ArchivoCSV")){
	System.out.println("\n\n\nIC_IF "+ numIF);
	System.out.println("IC_EPO "+ claveEPO);
	System.out.println("IC_PYME "+ clavePyme);
	System.out.println("FECHA DE "+ fechaInicio);
	System.out.println("FECHA A "+ fechaFin);
	System.out.println("ESTATUS "+ claveEstatus);
	List lista = new ArrayList();	
	List lsConsulta = bean.getDatosPyME_Nafin(numIF, claveEPO, clavePyme, fechaInicio, fechaFin, claveEstatus, "1");
	for(int i = 0 ; i < lsConsulta.size() ; i++){
		HashMap registro = new HashMap();
		List lDatos = (ArrayList)lsConsulta.get(i);
		registro.put("BANCO_FONDEO", lDatos.get(17).toString());
		registro.put("NOMBRE_IF", lDatos.get(13).toString());
		registro.put("NOMBRE_EPO", lDatos.get(0).toString());
		registro.put("NAFIN_E", lDatos.get(14).toString());
		registro.put("RSOCIAL", lDatos.get(1).toString());
		registro.put("RFC", lDatos.get(2).toString());
		registro.put("DOMICILIO", lDatos.get(3).toString());
		registro.put("FECHA_SOLIC", lDatos.get(4).toString());
		String param = ((lDatos.get(18).toString()).equals("Sin"))?"N/A":"Moneda Nacional";
		registro.put("MONEDA", param);
		registro.put("CTA_BANCARIA", lDatos.get(6).toString());
		registro.put("BANCO_SERVICIO", lDatos.get(7).toString());
		registro.put("SUCURSAL", lDatos.get(8).toString());
		registro.put("PLAZA", lDatos.get(9).toString());
		String estatus = ((lDatos.get(10).toString()).equals("E"))?"Enviado" : ((lDatos.get(10).toString()).equals("A")) ?"Autorizado" :"";
		registro.put("ESTATUS", lDatos.get(10).toString());
		registro.put("EXPEDIENTE", param);
		
		HashMap doctoscausas = bean.getDoctosCausas(lDatos.get(5).toString(),lDatos.get(15).toString(),lDatos.get(12).toString());
		String causas = (String)doctoscausas.get("descripcion");
		String result = "";
		String delimiters = "+-*/(),.| ";
		StringTokenizer st = new StringTokenizer(causas, delimiters, true);
		while (st.hasMoreTokens()) {
        String w = st.nextToken();
		  if (w.equals("|")) {
            result = result + "<br>-";
        } else {
            result = result + w;
        }
		}
		String causasrecortado = "-" + result;
		if(!(lDatos.get(18).toString()).equals("Sin")){
			causasrecortado = causasrecortado.substring(0,causasrecortado.length()-1);
		}else{
			causasrecortado = "N/A";
		}
		registro.put("CAUSA_RECHAZO", causasrecortado);
		registro.put("NOTAS", lDatos.get(16).toString());
		lista.add(registro);
	}
	if(informacion.equals("consultaGeneral")){
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("total",  Integer.toString(lista.size()));
		jsonObj.put("registros", lista );
		infoRegresar = jsonObj.toString();

	}
	if(informacion.equals("ArchivoCSV")){
		String nombreArchivo = bean.crearCustomFileFromList(lista, strDirectorioTemp, "CSV");
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		infoRegresar = jsonObj.toString();
	}
	
}


%>
<%=  infoRegresar%>