<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		com.netro.cadenas.* ,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	JSONObject resultado = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
/// ***** ***** ***** ***** CATALOGOS ***** ***** ***** ***** ///
 if(informacion.equals("CatalogoPais")) {		
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionNotIn("52", Integer.class);
		infoRegresar = cat.getJSONElementos();	
	}
 else if(informacion.equals("CatalogoEstado")){
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setClavePais(Pais);//Mexico clave 24
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();	
 }
 else if(informacion.equals("CatalogoMunicipio"))	{
		
		String Edo= (request.getParameter("estado")==null)?"":request.getParameter("estado");
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoMunicipio cat=new CatalogoMunicipio();
		cat.setClave("ic_municipio");
		cat.setDescripcion("cd_nombre");
		cat.setPais(Pais);
		cat.setEstado(Edo);
		cat.setOrden("cd_nombre");
		List elementos=cat.getListaElementos();
		
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
 }
 else if(informacion.equals("CatalogoDomicilio")) {
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_dom_correspondencia");
		cat.setCampoClave("ic_dom_correspondencia");
		cat.setCampoDescripcion("initcap(CD_NOMBRE)");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("CatalogoNumeroIF"))	{		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_financiera");
		cat.setCampoClave("ic_financiera");
		cat.setCampoDescripcion("'(' || ic_financiera ||')'|| initcap(cd_nombre)");
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("CatalogoAvales"))	{		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_aval");
		cat.setCampoClave("ic_aval");
		cat.setCampoDescripcion("initcap(CD_NOMBRE)");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();			
 } 
  else if(informacion.equals("CatalogoTipoRiesgo")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_tipo_riesgo");
		cat.setCampoClave("ic_tipo_riesgo");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("ic_tipo_riesgo");
		infoRegresar = cat.getJSONElementos();			
 }
 else if(informacion.equals("CatalogoViabilidad")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_Viabilidad");
		cat.setCampoClave("ic_viabilidad");
		cat.setCampoDescripcion("cd_nombre");
		infoRegresar = cat.getJSONElementos();			
 }
  else if(informacion.equals("CatalogoOficina_controladora")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_of_controladora");
		cat.setCampoClave("ic_of_controladora");
		cat.setCampoDescripcion("initcap(cd_nombre)");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();			
 } 
 else if(informacion.equals("CatalogoClaveIfSIAG")) {		
		CatalogoSimple cat = new CatalogoSimple(); 
		cat.setTabla("gti_if_siag");
		cat.setCampoClave("ic_if_siag");
		cat.setCampoDescripcion("ic_financiera||' - '||cg_descripcion");
		cat.setOrden("ic_if_siag");
		infoRegresar = cat.getJSONElementos();			
 }
  else if(informacion.equals("CatalogoClaveSUCRE")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_sucre");
		cat.setCampoClave("ic_sucre");
		cat.setCampoDescripcion("ic_sucre||' - '||cd_descripcion");
		cat.setOrden("ic_sucre");
		infoRegresar = cat.getJSONElementos();			
 }
 else if(informacion.equals("CatalogoDescontante")) {		
		try {
			//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
			//Afiliacion BeanAfiliacion = afiliacionHome.create();
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			List listDescontantes = new ArrayList();
			listDescontantes = BeanAfiliacion.obtiene_datos_Descontante();
	
			if(!listDescontantes.isEmpty()){
				jsObjArray = JSONArray.fromObject(listDescontantes);
				infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		  }
		} catch(Exception e) {
			throw new AppException("AfiliacionBean_obtiene_datos_Descontante :: ERROR ::..", e);
		  }
 }

 /// ***** ***** ***** ***** ***** ***** ***** ***** ***** ///
 else if(informacion.equals("ConsultaRFCexistente"))	{
	try	
	{
		
		//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
		//Afiliacion BeanAfiliacion = afiliacionHome.create();
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		String rfc= (request.getParameter("rf")==null)?"":request.getParameter("rf");
		
		
		boolean res = BeanAfiliacion.verificaRFC(rfc,"I");
		if(res) {
			resultado.put("success",new Boolean(true));  //El RFC es correcto
			infoRegresar = resultado.toString();
		} else {
			resultado.put("success",new Boolean(false));  //El RFC es correcto
			infoRegresar = resultado.toString();
		}
		
	} catch(Exception e) { //catch(NafinException lexError){
		throw new AppException("Error en la afiliacion", e);
	  }
	
 }
 else if(informacion.equals("AltaIF"))	{
	try	
		{

		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		DisenoIF dis = new DisenoIF();
		
		String 	Razon_Social  						= (request.getParameter("Razon_Social") == null) ? "" : request.getParameter("Razon_Social").replaceAll(",", ""),
					Nombre_comercial 					= (request.getParameter("nombre_comercial") == null) ? "" : request.getParameter("nombre_comercial").replaceAll(",", ""),
					R_F_C         						= (request.getParameter("rfc")    == null) ? "" : request.getParameter("rfc").replaceAll(",", "");
					//Domicilio							
		String   Calle        						= (request.getParameter("calle")    == null) ? "" : request.getParameter("calle").replaceAll(",", ""),
					Colonia       						= (request.getParameter("colonia")  == null) ? "" : request.getParameter("colonia").replaceAll(",", ""),
					Codigo_postal 						= (request.getParameter("code") == null) ? "" : request.getParameter("code").replaceAll(",", ""),
					Pais          						= (request.getParameter("cmb_pais")     == null) ? "" : request.getParameter("cmb_pais").replaceAll(",", ""),
					Estado        						= (request.getParameter("cmb_estado")   == null) ? "" : request.getParameter("cmb_estado"),
					Delegacion_o_municipio 			= (request.getParameter("cmb_delegacion") == null) ? "" : request.getParameter("cmb_delegacion").replaceAll(",", ""),
					Telefono      						= (request.getParameter("telefono") == null) ? "" : request.getParameter("telefono").replaceAll(",", ""),
					Email         						= (request.getParameter("email")    == null) ? "" : request.getParameter("email").replaceAll(",", ""),
					Fax           						= (request.getParameter("fax")      == null) ? "" : request.getParameter("fax").replaceAll(",", "");
		
		String   Domicilio_correspondencia		=(request.getParameter("Domicilio_correspondencia") == null) ? "" : request.getParameter("Domicilio_correspondencia").replaceAll(",", ""), 					
					Ejecutivo_de_cuenta 				= (request.getParameter("Ejecutivo_de_cuenta") == null) ? "" : request.getParameter("Ejecutivo_de_cuenta").replaceAll(",", ""),
					Numero_de_IF  						= (request.getParameter("Numero_de_IF")  == null) ? "" : request.getParameter("Numero_de_IF").replaceAll(",", ""),
					Numero_Clie_Sirac 				= (request.getParameter("numCliSirac")  == null) ? "" : request.getParameter("numCliSirac").replaceAll(",", ""),
					Tipo_de_IF 	  						= (request.getParameter("Tipo_de_IF")    == null) ? "" : request.getParameter("Tipo_de_IF").replaceAll(",", ""),
					Tipo_de_IF_piso 					= (request.getParameter("Tipo_de_IF_piso") == null) ? "" : request.getParameter("Tipo_de_IF_piso").replaceAll(",", ""),
					Avales 		  						= (request.getParameter("Avales")        == null) ? "" : request.getParameter("Avales").replaceAll(",", ""),
					tipo_riesgo							= (request.getParameter("tipo_riesgo")==null)?"":request.getParameter("tipo_riesgo").replaceAll(",", ""),
					Facultades    						= (request.getParameter("Facultades")    == null) ? "" : request.getParameter("Facultades").replaceAll(",", ""),
					Limite_maximo_endeudamiento	= (request.getParameter("Limite_maximo_endeudamiento") == null) ? "" : request.getParameter("Limite_maximo_endeudamiento").replaceAll(",", ""),
					Ente_contable 						= (request.getParameter("Ente_contable") == null) ? "" : request.getParameter("Ente_contable").replaceAll(",", ""),
					Porcentaje_capital_contable 	= (request.getParameter("Porcentaje_capital_contable") == null) ? "" : request.getParameter("Porcentaje_capital_contable").replaceAll(",", ""),					
					Viabilidad    						= (request.getParameter("Viabilidad")    == null) ? "" : request.getParameter("Viabilidad").replaceAll(",", ""),
					Oficina_controladora 			= (request.getParameter("Oficina_controladora") == null) ? "" : request.getParameter("Oficina_controladora").replaceAll(",", "");
		
		String   if_siag								= (request.getParameter("if_siag")==null)?"":request.getParameter("if_siag").replaceAll(",", ""),
				   nombre_tabla 						= (request.getParameter("nombre_tabla") == null) ? "" : request.getParameter("nombre_tabla").replaceAll(",", ""),	
				   if_sucre          				= (request.getParameter("if_sucre")== null) ? "" : request.getParameter("if_sucre").replaceAll(",", "");
		String   descontanteIf1 					= request.getParameter("descontanteIf1")== null?"":request.getParameter("descontanteIf1").replaceAll(",", "");//FODEA 012 - 2010 ACF
      String   descontanteIf2 					= request.getParameter("descontanteIf2")== null?"":request.getParameter("descontanteIf2").replaceAll(",", "");//FODEA 012 - 2010 ACF
		String   tipoEpoInstruccion 				= request.getParameter("tipoEpoInstruccion")== null?"":request.getParameter("tipoEpoInstruccion").replaceAll(",", "");//FODEA 033 - 2010 ACF
		/* ---- Check's box ---- */			
		String  autorizauto_opersf					= (request.getParameter("autorizauto_opersf")      == null) ? "" : request.getParameter("autorizauto_opersf").replaceAll(",", ""),
				  msChkFinan							= (request.getParameter("chkFinanciamiento")       == null) ? "" : request.getParameter("chkFinanciamiento").replaceAll(",", ""),
				  enlace_automatico					= (request.getParameter("enlace_automatico")       == null) ? "" : request.getParameter("enlace_automatico").replaceAll(",", ""),	
				  convenio_unico						= (request.getParameter("convenio_unico")      		== null) ? "" : request.getParameter("convenio_unico").replaceAll(",", ""),
				  mandato_documento					= (request.getParameter("mandato_documento")       == null) ? "" : request.getParameter("mandato_documento").replaceAll(",", ""),
		     	  fideicomiso		= (request.getParameter("fideicomiso")       == null) ? "" : request.getParameter("fideicomiso").replaceAll(",", "");
		String chkFactDistribuido		= (request.getParameter("chkFactDistribuido")       == null) ? "" : request.getParameter("chkFactDistribuido").replaceAll(",", "");
                String idOperaMontosMenores = (request.getParameter("id_OperaMontosMenores")    == null) ? "N" : request.getParameter("id_OperaMontosMenores");
		
		
		
		if(!autorizauto_opersf.equals("")) {
			if(autorizauto_opersf.equals("on"))
				autorizauto_opersf = "S";
			else
				autorizauto_opersf = "N";
		}
		if(!msChkFinan.equals(""))
		{ 
			if ( msChkFinan.equals("on") )
				msChkFinan = "S";
			else
				msChkFinan = "N";
		}		
		if(!enlace_automatico.equals(""))
		{ 
			if ( enlace_automatico.equals("on") )
				enlace_automatico = "S";
			else
				enlace_automatico = "N";
		}
		if(!convenio_unico.equals(""))
		{
			if ( convenio_unico.equals("on") )
				convenio_unico = "S";
			else
				convenio_unico = "N";
		}
		if(!mandato_documento.equals(""))
		{
			if ( mandato_documento.equals("on") )
				mandato_documento = "S";
			else
				mandato_documento = "N";
		}
		
		//Fodea 017-2013
		if(!fideicomiso.equals("")) 	{
			if ( fideicomiso.equals("on") ){
				fideicomiso = "S";
			}else{
				fideicomiso = "N";
			}
		}else  {
			fideicomiso = "N";
		}
		
		
		
		if( !Delegacion_o_municipio.equals("") && Delegacion_o_municipio != null)
		{
			CatalogoMunicipio cat=new CatalogoMunicipio();
			cat.setClave("ic_municipio");
			cat.setDescripcion("cd_nombre");
			cat.setPais(Pais);
			cat.setEstado(Estado);
			cat.setSeleccion(Delegacion_o_municipio);
			cat.setOrden("cd_nombre");
			List elementos=cat.getListaElementos();
		
			Iterator it = elementos.iterator();
			while(it.hasNext()) {
				ElementoCatalogo obj = (ElementoCatalogo)it.next();
				String iDelMun = obj.getClave();	String strDelMun = obj.getDescripcion();
				Delegacion_o_municipio = iDelMun + "|" + strDelMun;
			}
			System.out.println("Delegacion_o_municipio " +  Delegacion_o_municipio ) ;
		}
		
		String  strArchivo	= (request.getParameter("strArchivo")==null)?"":request.getParameter("strArchivo"),
				  path_destino = (request.getParameter("path_destino")==null)?"":request.getParameter("path_destino");
		

		String sDatosIF = "Razon_Social-" + Razon_Social +"|" +"Nombre_comercial-" + Nombre_comercial +"|"+ "R_F_C-" + R_F_C +"|"+
							"Calle-" + Calle+"|"+"Colonia-" + Colonia+"|"+"Codigo_postal-" + Codigo_postal+"|"+"Pais-" + Pais+"|"+ 
							"Estado-"+	Estado +"|" +"Delegacion_o_municipio-" + Delegacion_o_municipio+"|"+"Telefono-" + Telefono+"|"+
							"Email-" + Email+"|"+ "Fax-" + Fax+"|"+ "Domicilio_correspondencia-" + Domicilio_correspondencia+"|"+
							"Ejecutivo_de_cuenta-" + Ejecutivo_de_cuenta+"|"+ "Numero_de_IF-" + Numero_de_IF+"|"+ "Tipo_de_IF-" + Tipo_de_IF+"|"+
						   "Numero de Cliente SIRAC-" + Numero_Clie_Sirac+"|"+"Tipo_de_IF_piso-" + Tipo_de_IF_piso+"|"+"Avales-" + Avales+"|"+"tipo_riesgo-" + tipo_riesgo+"|"+
							"Facultades-" + Facultades+"|"+"Limite_maximo_endeudamiento-" + Limite_maximo_endeudamiento+"|"+"Ente_contable-" + Ente_contable+"|"+
							"Porcentaje_capital_contable-" + Porcentaje_capital_contable+"|"+"Viabilidad-" + Viabilidad+"|"+"Oficina_controladora-" + Oficina_controladora+"|"+
							"if_siag-" + if_siag+"|"+"nombre_tabla-" + nombre_tabla+"|"+"if_sucre-" + if_sucre+"|"+"autorizauto_opersf-" + autorizauto_opersf+"|"+
							"msChkFinan-" + msChkFinan+"|"+"enlace_automatico-" + enlace_automatico+"|"+ "convenio_unico-" + convenio_unico+"|"+
							"mandato_documento-" + mandato_documento+"|"+"descontanteIf1-" + descontanteIf1+"|"+"descontanteIf2-" + descontanteIf2+"|"+
							"tipoEpoInstruccion-" + tipoEpoInstruccion+"|"+"strArchivo-" + strArchivo+"|"+"path_destino-" + path_destino+"\n";
		
		
		String lsNoNafinElectronico = BeanAfiliacion.afiliaIF(iNoUsuario, Razon_Social,Nombre_comercial,R_F_C,Ejecutivo_de_cuenta,Domicilio_correspondencia,
					Numero_de_IF,Tipo_de_IF,Avales,Limite_maximo_endeudamiento,Facultades,Porcentaje_capital_contable,Ente_contable,
					Oficina_controladora,Viabilidad,Calle,Colonia,Estado,Pais,Delegacion_o_municipio,Codigo_postal,Telefono, Email,Fax,
					""/*Fecha_convenio*/,""/*Fecha_convenio*/,Tipo_de_IF_piso, msChkFinan, enlace_automatico, nombre_tabla, autorizauto_opersf,tipo_riesgo,if_siag,if_sucre, convenio_unico, //FODEA 020 - 2009
					mandato_documento, descontanteIf1, descontanteIf2, tipoEpoInstruccion, fideicomiso,Numero_Clie_Sirac, chkFactDistribuido.equals("on")?"S":"N", idOperaMontosMenores );//FODEA 033 - 2010 ACF
			
				
///pregunto como nos fue, ajajaj
				if( lsNoNafinElectronico != null  &&  !lsNoNafinElectronico.equals("") )
				{
					String ic_if = BeanAfiliacion.obtiene_ic_if(lsNoNafinElectronico); 
					if(!strArchivo.equals("") && !path_destino.equals("")){
						String archivo1=strDirectorioTemp+path_destino+strArchivo;
						
						String nombreArchivo=ic_if+".gif";
						String archivo2=strDirectorioPublicacion+"00archivos/if/logos/"+nombreArchivo;
						dis.Clonar(archivo1,archivo2);
						dis.guardar(ic_if,archivo2);
					}
					resultado.put("success", new Boolean(true));
					resultado.put("lsNoNafinElectronico",lsNoNafinElectronico);
					infoRegresar = resultado.toString();	
				}
			}
			catch(Exception e) { 

				throw new AppException("Error en la afiliacion", e);
			}
 }else if(informacion.equals("rfc_Sirac"))	{
 
	String Numero_Clie_Sirac = (request.getParameter("numCliSirac")  == null) ? "" : request.getParameter("numCliSirac").replaceAll(",", "");

	try	
	{
		
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
		
		String rfcSirac = BeanAfiliacion.getRfcSirac(Numero_Clie_Sirac);
		resultado.put("success",new Boolean(true));  //El RFC es correcto
		resultado.put("rfc", rfcSirac);  //El RFC es correcto
		infoRegresar = resultado.toString();
	} catch(Exception e) { //catch(NafinException lexError){
		throw new AppException("Error en la afiliacion", e);
	  }
	
 }


//System.out.println("infoRegresar============"+infoRegresar); 


%>
<%=infoRegresar%>



