<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.fondojr.*,
		com.netro.cadenas.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";

/*	SE CREA NUEVO OBJETO PARA ACCESAR EL EJB */
//FondoJuniorHome fondoJuniorHome = (FondoJuniorHome)ServiceLocator.getInstance().getEJBHome("FondoJuniorEJB", FondoJuniorHome.class);
//FondoJunior fondoJunior = fondoJuniorHome.create();
FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB",FondoJunior.class);

if(informacion.equals("catalogoEpo")) {
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("ic_if");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();	

}else if (informacion.equals("consultaCargasMasivasEcon") || informacion.equals("generarArchivoConsulta") ) {
	JSONObject jsonObj = new JSONObject();
	int numReemb = 0;
	
	String cveEpo  = request.getParameter("ic_epo")==null?"":request.getParameter("ic_epo");
	String numResumen  = request.getParameter("numResumen")==null?"":request.getParameter("numResumen");
	String valProcesado  = request.getParameter("valProcesado1")==null?"":request.getParameter("valProcesado1");
	String fecIni  = request.getParameter("fec_ini")==null?"":request.getParameter("fec_ini");
	String fecFin  = request.getParameter("fec_fin")==null?"":request.getParameter("fec_fin");
	
	System.out.println("cveEpo===="+cveEpo);
	System.out.println("numResumen===="+numResumen);
	System.out.println("valProcesado===="+valProcesado);

	//Se crea la instacia del Paginador
	ConsProvCargaMasEcon paginador = new ConsProvCargaMasEcon();
	paginador.setNumResumen(numResumen);
	paginador.setTxtCadProductiva(cveEpo);
	paginador.setValProcesado(valProcesado);
	paginador.setFecIni(fecIni);
	paginador.setFecFin(fecFin);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	if (informacion.equals("consultaCargasMasivasEcon")) {	//Datos para la Consulta con Paginacion

		//if (informacion.equals("consultaMonitor")) {	//Datos para la Consulta con Paginacion
			int start = 0;
			int limit = 0;
			//String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}
			jsonObj = JSONObject.fromObject(queryHelper.getJSONPageResultSet(request,start,limit));
			
			infoRegresar = jsonObj.toString();
	
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	} else if (informacion.equals("generarArchivoConsulta") ) {	
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
			infoRegresar = jsonObj.toString();		
		
		}
}else if (informacion.equals("generarArchivoDetalle") ) {	
	JSONObject jsonObj = new JSONObject();
	boolean archVacio = false;
	String numResumen  = request.getParameter("numResumen")==null?"":request.getParameter("numResumen");
	String contAfil  = request.getParameter("contAfil")==null?"":request.getParameter("contAfil");
	String contReafil  = request.getParameter("contReafil")==null?"":request.getParameter("contReafil");
	
	DescargaArchCargaMasivaEcon archivoDetEcon = new DescargaArchCargaMasivaEcon();
	String nombreArchivo= archivoDetEcon.generarArchivo(strDirectorioTemp, numResumen, contAfil, contReafil);
	
	if("Vacio".equals(nombreArchivo)){
		archVacio = true;
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("archVacio", new Boolean(archVacio));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	infoRegresar = jsonObj.toString();	
}

System.out.println("infoRegresar = "+infoRegresar);

%>
<%=infoRegresar%>