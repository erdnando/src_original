<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject, 
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,	
		com.netro.cadenas.*,
		netropology.utilerias.*,
		java.text.SimpleDateFormat,
		java.util.Date,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String operacion = request.getParameter("operacion") == null?"":(String)request.getParameter("operacion");
String numClienteSirac = request.getParameter("no_cliente_sirac") == null?"":(String)request.getParameter("no_cliente_sirac"); 
String rfc = request.getParameter("rfc") == null?"":(String)request.getParameter("rfc");
String razonSocial = request.getParameter("razon_social") == null?"":(String)request.getParameter("razon_social");
String fechaDe = request.getParameter("fecha_alta_de") == null?"":(String)request.getParameter("fecha_alta_de");
String fechaA = request.getParameter("fecha_alta_a") == null?"":(String)request.getParameter("fecha_alta_a");
String icIf = request.getParameter("ic_if") == null?"":(String)request.getParameter("ic_if");
String modifPorPropietario = request.getParameter("modif_propietario") == null?"":(String)request.getParameter("modif_propietario");
String icPyme = (request.getParameter("ic_pyme") != null)?request.getParameter("ic_pyme"):"";
//---Variables para realizar la actualización de datos
String tipoPersona = (request.getParameter("tipo_persona") == null) ? "" : request.getParameter("tipo_persona");
String paterno = (request.getParameter("ap_paterno") == null) ? "" : request.getParameter("ap_paterno").trim();
String materno = (request.getParameter("ap_materno") == null) ? "" : request.getParameter("ap_materno").trim();
String nombre = (request.getParameter("nombre") == null) ? "" : request.getParameter("nombre").trim();
String sexo = (request.getParameter("sexo") == null) ? "" : request.getParameter("sexo");
String fechaNacimiento = (request.getParameter("fecha_nacimiento") == null) ? "" : request.getParameter("fecha_nacimiento").trim();
String curp = (request.getParameter("curp") == null) ? "" : request.getParameter("curp");
String fiel = (request.getParameter("fiel") == null) ? "" : request.getParameter("fiel");
String paisOrigen = (request.getParameter("pais_origen") == null) ? "" : request.getParameter("pais_origen");
String calle = (request.getParameter("calle") == null) ? "" : request.getParameter("calle").trim();
String colonia = (request.getParameter("colonia") == null) ? "" : request.getParameter("colonia").trim();
String pais = (request.getParameter("pais") != null)?request.getParameter("pais"):"";
String estado = (request.getParameter("estado") == null) ? "" : request.getParameter("estado");
String delegacionMunicipio = (request.getParameter("deleg_municipio") == null) ? "" : request.getParameter("deleg_municipio");
String ciudad = (request.getParameter("ciudad") == null) ? "" : request.getParameter("ciudad");
String codigoPostal = (request.getParameter("codigo_postal") == null) ? "" : request.getParameter("codigo_postal").trim();
String telefono = (request.getParameter("telefono") == null) ? "" : request.getParameter("telefono");
String email = (request.getParameter("email") == null) ? "" : request.getParameter("email");
String icDomicilio = (request.getParameter("ic_domicilio") == null) ? "" : request.getParameter("ic_domicilio");
String ventasNetasTotales = (request.getParameter("ventas_netas_tot") == null) ? "" : request.getParameter("ventas_netas_tot").trim();
String numEmpleados = (request.getParameter("num_empleados") == null) ? "" : request.getParameter("num_empleados").trim();
String sectorEcon = (request.getParameter("sector_econ") != null)?request.getParameter("sector_econ"):"";
String subSector = (request.getParameter("subsector") != null)?request.getParameter("subsector"):"";
String rama = (request.getParameter("rama") != null)?request.getParameter("rama"):"";
String clase = (request.getParameter("clase") == null) ? "" : request.getParameter("clase");
String numSirac = (request.getParameter("num_sirac") == null) ? "" : request.getParameter("num_sirac");
String invalidar = (request.getParameter("invalidar") != null && request.getParameter("invalidar").equals("true") )?"S":"N";
String actualizarSirac = (request.getParameter("actualizaSirac") != null && request.getParameter("actualizaSirac").equals("S")) ? "S" : "N";

int start=0, limit=0;

String nombreArchivo = "", consulta = "", infoRegresar = "";
JSONObject jsonObj = new JSONObject();
JSONArray registros = new JSONArray();

AfiliadoCredElectronico paginador = new AfiliadoCredElectronico();

log.debug("-----> Estoy en el if: " + informacion);

if(informacion.equals("catalogoIF")){
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_if");
   cat.setDistinc(false);
   cat.setCampoClave("ic_if");
   cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
} else if(informacion.equals("catalogoPais")){
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_pais");
	cat.setCampoClave("ic_pais");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setValoresCondicionNotIn("52", Integer.class);
	infoRegresar = cat.getJSONElementos();	
		
} else if(informacion.equals("catalogoEstado")){

	CatalogoEstado catalogo = new CatalogoEstado();
	catalogo.setCampoClave("ic_estado");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setClavePais(pais);
	catalogo.setOrden("cd_nombre");
	infoRegresar = catalogo.getJSONElementos();
	
} else if(informacion.equals("catalogoMunicipio")){

	CatalogoMunicipio catalogo = new CatalogoMunicipio();
	catalogo.setPais(pais);
	catalogo.setClave("IC_MUNICIPIO");
	catalogo.setDescripcion("upper(CD_NOMBRE)");
	catalogo.setEstado(estado);
	List elementos = catalogo.getListaElementos();
	JSONArray jsonArr = new JSONArray();
	Iterator it = elementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
			
} else if(informacion.equals("catalogoCiudad")){
try{
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_ciudad");
	cat.setCampoClave("codigo_ciudad");
	cat.setCampoDescripcion("nombre");
	cat.setCondicionIn(pais, "codigo_pais");
	cat.setCondicionIn(estado, "codigo_departamento");
	cat.setOrden("nombre");
	infoRegresar = cat.getJSONElementos();	
} catch(Exception e){
	log.warn("-----> Error en "+informacion+": " + e);
}
} else if(informacion.equals("catalogoSectorEconomico")){
try{
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_sector_econ");
	cat.setCampoClave("IC_SECTOR_ECON");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setOrden("cd_nombre");
	cat.setValoresCondicionNotIn("1", Integer.class);
	infoRegresar = cat.getJSONElementos();	
} catch(Exception e){
	log.warn("-----> Error en "+informacion+": " + e);
}		
} else if(informacion.equals("catalogoSubSector")){
try{
	CatalogoSubSector cat = new CatalogoSubSector();
	cat.setTabla("comcat_subsector");
	cat.setCampoClave("IC_SUBSECTOR");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setOrden("CD_NOMBRE");
	cat.setSector(sectorEcon);
	infoRegresar = cat.getJSONElementos();	
} catch(Exception e){
	log.warn("-----> Error en "+informacion+": " + e);
}		
} else if(informacion.equals("catalogoRama")){
try{
	CatalogoRama cat = new CatalogoRama();
	cat.setTabla("comcat_rama");
	cat.setCampoClave("ic_rama");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setOrden("CD_NOMBRE");
	cat.setSector(sectorEcon);
	cat.setSubSector(subSector);
	infoRegresar = cat.getJSONElementos();	
} catch(Exception e){
	log.warn("-----> Error en "+informacion+": " + e);
}		
} else if(informacion.equals("catalogoClase")){
try{
	CatalogoClase cat = new CatalogoClase(); 
	cat.setTabla("comcat_clase");
	cat.setCampoClave("IC_CLASE");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setOrden("CD_NOMBRE");
	cat.setSector(sectorEcon);
	cat.setSubSector(subSector);
	cat.setRama(rama);
	infoRegresar = cat.getJSONElementos();	
} catch(Exception e){
	log.warn("-----> Error en "+informacion+": " + e);
}		
} else if(informacion.equals("consultar") || informacion.equals("imprimir") || informacion.equals("generarArchivo")){
	
	paginador.setNumClienteSirac(numClienteSirac);
	paginador.setRfc(rfc);
	paginador.setRazonSocial(razonSocial);
	paginador.setFechaDe(fechaDe);
	paginador.setFechaA(fechaA);
	paginador.setIcIf(icIf);
	paginador.setDatosPersModif(modifPorPropietario);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	if (informacion.equals("consultar") || informacion.equals("imprimir")){
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
			if(informacion.equals("consultar")) {		
				try {
					if (operacion.equals("generar")) {	//Nueva consulta					
						queryHelper.executePKQuery(request);
					}	
					consulta = queryHelper.getJSONPageResultSet(request,start,limit);				
				} catch(Exception e) {
					throw new AppException(" Error en la paginacion", e);
				}
				jsonObj = JSONObject.fromObject(consulta);				
			} else if( informacion.equals("imprimir") ) {
				try {
					nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
					log.debug("-----> urlArchivo"+ strDirecVirtualTemp+" <> "+nombreArchivo + "<------------");
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
		}
	}else if( informacion.equals("generarArchivo") ) {
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}			
	}		
	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("consultaModificar")){
	
	paginador.setIcPyme(icPyme);
	HashMap datos = new HashMap();
	datos = paginador.consultaModificar(); 
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("registro", datos);
	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("actualizaNe")){
	
	paginador.setIcPyme(icPyme);
	HashMap datos = new HashMap();
	datos = paginador.consultaModificar(); 
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("registro", datos);
	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("actualizaDatos")){
	String msg="";
	StringBuffer sDatosPyme = new StringBuffer();
	Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	if (numSirac.equals("")){
		numSirac = "0";
	}
	/**			log.debug("--->rfc " + rfc + "| nombre " + nombre + "|paterno " + paterno + "|materno " + materno + "|sexo "+ 
							 sexo + "|razonSocial " + razonSocial + "|numEmpleados " + numEmpleados + "|ventasNetasTotales " + ventasNetasTotales + "|sectorEcon "+ 
							 sectorEcon + "|subSector " + subSector + "|rama " + rama + "|clase " + clase + "| estado " + estado + "|claveMunicipio  "+ 
							 delegacionMunicipio + "|calle " + calle + "|colonia " + colonia + "|codigoPostal " + codigoPostal + "|numSirac " + numSirac + "|email "+ 
							 email + "|telefono " + telefono + "|invalidar " + invalidar + "| actualizarSirac " + actualizarSirac + "|paisOrigen "+ 
							 paisOrigen + "|curp " + curp + "|fiel" + fiel + "|ciudad " + ciudad + "|iNoUsuario " +  
							 iNoUsuario + "|icPyme " + icPyme + "|tipoPersona " + tipoPersona + "|fechaNacimiento " + 
							 fechaNacimiento + "|icDomicilio " + icDomicilio);
	*/
						 
	sDatosPyme.append( rfc + "|" + nombre + "|" + paterno + "|" + materno + "|"+ 
							 sexo + "|" + razonSocial + "|" + numEmpleados + "|" + ventasNetasTotales + "|"+ 
							 sectorEcon + "|" + subSector + "|" + rama + "|" + clase + "|" + estado + "|"+ 
							 delegacionMunicipio + "|" + calle + "|" + colonia + "|" + codigoPostal + "|" + numSirac + "|"+ 
							 email + "|" + telefono + "|" + invalidar + "|" + actualizarSirac + "|"+ 
							 paisOrigen + "|" + curp + "|" + fiel + "|" + ciudad + "\n");

	try{
		msg = BeanAfiliacion.actualizaPymeCE(iNoUsuario, icPyme, sDatosPyme.toString(), tipoPersona, fechaNacimiento, icDomicilio);
		jsonObj.put("success", new Boolean(true));
	} catch(Exception e){
		log.warn("Error al actualizar los datos: " + e);
		jsonObj.put("success", new Boolean(false));
	}	
	jsonObj.put("msg", msg);
	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("actualizaNafin")){
	
}

%>
<%=infoRegresar%>