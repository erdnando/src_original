Ext.onReady(function(){

	//*****************Descarga Archivos Consulta *******************
 function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnImprimir').setIconClass('icoPdf');
			Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	
	function procesarReafiliacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			if(info.mensaje !='') {		
				Ext.MessageBox.alert('Mensaje',info.mensaje,
				function(){
					window.location = '15forma5MandanteExt.jsp';
				});
			}
			
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
	
	
	var procesarCatEPO_Mandante= function(store, records, oprion){
			
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){			
			var jsonData = store.reader.jsonData;  
			
			var txtrRFC =  Ext.getCmp('txtrRFC1');
			var txtNombreMandante =  Ext.getCmp('txtNombreMandante1');
			
			var ic_mandante =  Ext.getCmp('ic_mandante');			
			var txtNumElectronico_2 =  Ext.getCmp('txtNumElectronico_2');
			var txtNombreMandante_2 =  Ext.getCmp('txtNombreMandante_2');
			var txtRFC_2 =  Ext.getCmp('txtRFC_2');
									
			txtrRFC.update(jsonData.txtRFC);
			txtNombreMandante.update(jsonData.txtNombreMandante);	
			
			ic_mandante.setValue(jsonData.ic_mandante);	 
			txtNumElectronico_2.setValue(jsonData.txtNumElectronico);	 
			txtNombreMandante_2.setValue(jsonData.txtNombreMandante);	 
			txtRFC_2.setValue(jsonData.txtRFC);	 
		
				
			
		}
  }
    
	  
	var CatEPO_Mandante = new Ext.data.JsonStore({
		id: 'CatEPO_Mandante',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15forma5MandanteExt.data.jsp',
		baseParams: {
			informacion: 'CatEPO_Mandante'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	
			load: procesarCatEPO_Mandante,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	
	
	//**************************************Reafiliaci�n*****************************
		
		
	var procesarReafiliacionData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridReafiliacion = Ext.getCmp('gridReafiliacion');	
		var el = gridReafiliacion.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridReafiliacion.isVisible()) {
				gridReafiliacion.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			Ext.getCmp('claveEpos').setValue(jsonData.claveEpos);	
			
			if(store.getTotalCount() > 0) {					
				el.unmask();					
				Ext.getCmp('btnAceptar').show();
			} else {		
				Ext.getCmp('btnAceptar').hide();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}		
	
	var storeReafiliacionData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15forma5MandanteExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaReafiliacion'			
		},
		hidden: true,
		fields: [
			{name: 'IC_EPO'},
			{name: 'CADENA_PRODUCTIVA'},
			{name: 'NAFIN_ELECTRONICO'},	
			{name: 'IC_MANDANTE'},
			{name: 'RAZON_SOCIAL'},
			{name: 'RFC'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarReafiliacionData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarReafiliacionData(null, null, null);					
				}
			}
		}					
	})
	
	var gridReafiliacion =[{
		id: 'gridReafiliacion',
		hidden: true,
		xtype:'grid',
		title:'',
		store: storeReafiliacionData,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'Cadena Productiva',
				tooltip: 'Cadena Productiva',
				dataIndex: 'CADENA_PRODUCTIVA',
				sortable: true,
				width: 150,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'No. Nafin Electr�nico',
				tooltip: 'No. Nafin Electr�nico',
				dataIndex: 'NAFIN_ELECTRONICO',
				sortable: true,
				width: 150,
				align: 'center'
			},
			{
				header: 'N�mero de Mandante',
				tooltip: 'N�mero de Mandante',
				dataIndex: 'IC_MANDANTE',
				sortable: true,				
				align: 'center',
				width: 150			
			},
			{
				header: 'Nombre o Razon Social',
				tooltip: 'Nombre o Razon Social',
				dataIndex: 'RAZON_SOCIAL',
				sortable: true,				
				align: 'left',
				width: 150,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'RFC ',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,				
				align: 'center',
				width: 150				
			}		
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 700,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	
	
	var procesarReafiliar= function(grid, rowIndex, colIndex, item, event) {
	
		var registro = grid.getStore().getAt(rowIndex);
		var ic_mandante = registro.get('IC_MANDANTE');  
		var txtRFC = registro.get('RFC'); 
		var txtNombreMandante = registro.get('RAZON_SOCIAL'); 
		var txtNumElectronico = registro.get('NUM_NAFIN_ELEC'); 
	
			
		CatEPO_Mandante.load({
			params:{ 
				ic_mandante:ic_mandante,
				txtNombreMandante:txtNombreMandante,
				txtRFC:txtRFC,
				txtNumElectronico:txtNumElectronico
			}	
		});
		
		var venReafiliacion = Ext.getCmp('venReafiliacion');
		if(venReafiliacion){
			venReafiliacion.show();
		}else{
			new Ext.Window({			
				id: 'venReafiliacion',
				width: 710,	
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'destroy',
				closable:true,
				autoDestroy:true,					
				items: [					
					fpReafiliacion,
					gridReafiliacion
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	
							xtype: 'button',	
							text: 'Aceptar',	
							iconCls: 'aceptar', 	
							hidden:true,
							id: 'btnAceptar', 
							handler: function(){
								var  ic_mandante =  Ext.getCmp('ic_mandante').getValue();
								var  claveEpos =  Ext.getCmp('claveEpos').getValue();
							
								Ext.Ajax.request({
									url: '15forma5MandanteExt.data.jsp',
									params: {
										informacion: 'AgregarReafiliacion',		
										ic_mandante:ic_mandante,
										claveEpos:claveEpos	
									},
									callback:procesarReafiliacion
								});
								
							} 
						},
						'-',						
						{	
							xtype: 'button',	
							text: 'Cancelar',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerraR', 
							handler: function(){
								Ext.getCmp('venReafiliacion').destroy();
							} 
						}							
					]
				}
			}).show().setTitle('');
		}		
	}
	
	var elementosReafiliacion =[
		{
			xtype: 'displayfield',
			name: 'txtNombreMandante',
			id: 'txtNombreMandante1',
			allowBlank: false,			
			fieldLabel: 'Nombre del Mandante',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'txtrRFC',
			id: 'txtrRFC1',
			allowBlank: false,			
			fieldLabel: 'RFC',
			value:''
		},		
		{
			xtype: 'multiselect',
			fieldLabel: 'Nombre de la EPO',
			name: 'txtEPO',
			hiddenName: 'txtEPO',
			id: 'txtEPO1',
			width: 500,
			height: 200, 
			autoLoad: false,
			hidden: false,
			mode: 'local',
			store:CatEPO_Mandante,
			displayField: 'descripcion',
			valueField: 'clave',		 
			tbar:[
				{
					text: 'Limpiar',
					handler: function(){
						fp.getForm().findField('txtEPO').reset();
					}
				}
			],
			ddReorder: true
		}	
		,	
		{ 	xtype: 'textfield',  hidden: true,  id: 'ic_mandante', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtNumElectronico_2', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtNombreMandante_2', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtRFC_2', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtRFC_2', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'claveEpos', 	value: '' }
	];
	
	var fpReafiliacion = {
		xtype: 'form',		
		id					: 'fpReafiliacion',		
		layout			: 'form',
		width				: 700,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 140,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				:  [ 	elementosReafiliacion 	],
		monitorValid: true,
		buttons: [		
			{
				text: 'Agregar',
				id: 'btnAgregar',
				iconCls: 'modificar',			
				formBind: true,				
				handler: function(boton, evento) {	
				
					storeReafiliacionData.load({
						params: {
							informacion: 'ConsultaReafiliacion',
							ic_mandante:Ext.getCmp('ic_mandante').getValue(),
							txtNumElectronico_2:Ext.getCmp('txtNumElectronico_2').getValue(),
							txtNombreMandante_2:Ext.getCmp('txtNombreMandante_2').getValue(),
							txtRFC_2:Ext.getCmp('txtRFC_2').getValue(),
							txtEPO:Ext.getCmp('txtEPO1').getValue()					
						}
					});							
								
				}
			}			
		]	
	};
	
	
	
	 //**************************************Ver cuentas y Modificar Perfil*****************************
	
	
	var catalogoPerfil = new Ext.data.JsonStore({
		id: 'catalogoPerfil',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: '15forma5MandanteExt.data.jsp',
		baseParams: {
			informacion: 'catalogoPerfil'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {				
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	

	function modificarPerfil(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert('Mensaje',info.mensaje,
				function(){
					Ext.getCmp('ProcesarCuentas').hide();
				}	
			);
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	
	//procesar Cuentas
	
		function procesarInformacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			if(info.mensaje =='') {		
				Ext.getCmp('lblNombreEmpresa1').update(info.lblNombreEmpresa);
				Ext.getCmp('lblNafinElec1').update(info.lblNafinElec);
				Ext.getCmp('lblNombre1').update(info.lblNombre);
				Ext.getCmp('lblApellidoPaterno1').update(info.lblApellidoPaterno);
				Ext.getCmp('lblApellidoMaterno1').update(info.lblApellidoMaterno);
				Ext.getCmp('lblEmail1').update(info.lblEmail);
				Ext.getCmp('lblPerfilActual1').update(info.lblPerfilActual);
				Ext.getCmp('txtLogin1').setValue(info.txtLogin);
				Ext.getCmp('sTipoAfiliado').setValue(info.sTipoAfiliado);					
				
				Ext.getCmp('internacional').setValue(info.internacional);	
				Ext.getCmp('sTipoAfiliado').setValue(info.sTipoAfiliado);	
				Ext.getCmp('txtNafinElectronico').setValue(info.txtNafinElectronico);	
				Ext.getCmp('txtPerfilAnt').setValue(info.txtPerfilAnt);	
				Ext.getCmp('txtLoginC').setValue(info.txtLogin);				
				
				if(info.modificar=='S')  {
					Ext.getCmp('txtPerfil_1').show();
					catalogoPerfil.load({
						params:{ 
							tipoAfiliado: info.sTipoAfiliado, 
							txtLogin:info.txtLogin
						}	
					});
			}else  {
				Ext.getCmp('txtPerfil_1').hide();
			}
				
			}else  {  		/* no hay datos */   		}
			
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
	
	var modificarCuenta = function(grid, rowIndex, colIndex, item, event) {  
		
		var registro = grid.getStore().getAt(rowIndex);
		var txtLogin = registro.get('CUENTA');  
		Ext.getCmp('btnModificar').show();
		Ext.getCmp('id_modificacion').show();
		Ext.getCmp('id_informacion').hide();
		 
		Ext.Ajax.request({
			url: '15forma5MandanteExt.data.jsp',
			params: {
				informacion: 'informacionUsuario',		
				txtLogin:txtLogin,
				modificar:'S'
			},
			callback:procesarInformacion 
		});
		ventanaCuenta();
	}
	
	var procesaCuenta = function(grid, rowIndex, colIndex, item, event) {  
		
		var registro = grid.getStore().getAt(rowIndex);
		var txtLogin = registro.get('CUENTA');  
		 
		 Ext.getCmp('btnModificar').hide();
		 Ext.getCmp('id_modificacion').hide();
		 Ext.getCmp('id_informacion').show();
		 
		Ext.Ajax.request({
			url: '15forma5MandanteExt.data.jsp',
			params: {
				informacion: 'informacionUsuario',		
				txtLogin:txtLogin,
				modificar:'N'
			},
			callback:procesarInformacion 
		});
		ventanaCuenta();
	}
	
	
	var ventanaCuenta = function() {
	
		var ProcesarCuentas = Ext.getCmp('ProcesarCuentas');
		if(ProcesarCuentas){
			ProcesarCuentas.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 700,
				height: 400,												
				resizable: false,
				closable:false,
				id: 'ProcesarCuentas',
				closeAction: 'destroy',
				items: [					
					fpDespliega					
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	xtype: 'button',	
							text: 'Salir',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerraP', 
							handler: function(){
								Ext.getCmp('ProcesarCuentas').destroy();
							} 
						},
						'-'							
					]
				}
			}).show().setTitle('');
		}	
	}

		
	//***************************Informaci�n dle Usuario************************+		
	var elementosDespliega =[
		{ 	xtype: 'textfield',  hidden: true, id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'internacional', 	value: '' },		
		{ 	xtype: 'textfield',  hidden: true,  id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtNafinElectronico', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtPerfilAnt', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtLoginC', 	value: '' },			
		{
			xtype: 'panel',
			id: 'id_informacion', 
			allowBlank: false,
			title:'INFORMACION USUARIO',			
			value: '',
			hidden: true
		},
		{
			xtype: 'panel',
			id: 'id_modificacion', 
			allowBlank: false,
			title:'CAMBIO DE PERFIL',			
			value: '',
			hidden: true
		},
		{
			xtype: 'displayfield',
			name: 'txtLogin',
			id: 'txtLogin1',
			allowBlank: false,			
			fieldLabel: 'Clave de Usuario',
			value:''
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			fieldLabel: 'Empresa',
			items: [
				{
					xtype: 'displayfield',
					name: 'lblNombreEmpresa',
					id: 'lblNombreEmpresa1',
					allowBlank: false,
					width			: 230,
					value:''
				},
				{
					xtype: 'displayfield',
					id:'muestraCol',
					value: 'Nafin Electr�nico:',
					hidden: false
				},		
				{		
					xtype: 'displayfield',
					name: 'lblNafinElec',
					id: 'lblNafinElec1',
					allowBlank: false,
					anchor:'70%',
					width			: 230,				
					value:''
				}
			]
		},	
		{
			xtype: 'displayfield',
			name: 'lblNombre',
			id: 'lblNombre1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Nombre',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblApellidoPaterno',
			id: 'lblApellidoPaterno1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Apellido Paterno',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblApellidoMaterno',
			id: 'lblApellidoMaterno1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Apellido Materno',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblEmail',
			id: 'lblEmail1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Email',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblPerfilActual',
			id: 'lblPerfilActual1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Perfil Actual',
			value:''
		},
		{
			xtype: 'combo',
			fieldLabel: 'Perfil',
			name: 'txtPerfil',
			id: 'txtPerfil_1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'txtPerfil',
			emptyText: 'Seleccionar...',			
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,			
			minChars: 1,
			store: catalogoPerfil,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120,
			hidden : true
		}
	];

	var fpDespliega = new Ext.form.FormPanel({
		id					: 'fpDespliega',		
		layout			: 'form',
		width				: 800,
		height: 380,
		style				: ' margin:0 auto;',
		frame				: true,
		hidden			: false,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 160,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			elementosDespliega
		],
		buttons: [		
			{
				text: 'Modificar',
				id: 'btnModificar',
				iconCls: 'modificar',
				hidden: true,
				formBind: true,				
				handler: function(boton, evento) {	
					Ext.Ajax.request({
					url: '15forma5MandanteExt.data.jsp',
					params: Ext.apply(fpDespliega.getForm().getValues(),{
						informacion: 'ModifiarPerfil'							
					}),
					callback: modificarPerfil
				});				
				}
			}			
		]	
	});
	
	
			
	var procesarVerCuentas= function(grid, rowIndex, colIndex, item, event) {
	
		var registro = grid.getStore().getAt(rowIndex);
		var ic_mandante = registro.get('IC_MANDANTE');  
		var naE = registro.get('NUM_NAFIN_ELEC'); 
		var razon_social = registro.get('RAZON_SOCIAL'); 
		var tituloC = 'N@E:'+ naE +' '+razon_social;
		var tituloF ='';
	
		consCuentasData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						ic_mandante: ic_mandante,
						informacion: 'ConsCuentas',
						tipoAfiliado:'M',
						tituloC:tituloC
					})
				});
				
		new Ext.Window({					
					modal: true,
					width: 340,
					height: 380,
					modal: true,					
					closeAction: 'destroy',
					resizable: true,
					constrain: true,
					closable:false,
					id: 'Vercuentas',
					items: [					
						gridCuentas					
					],
					bbar: {
						xtype: 'toolbar',	buttonAlign:'center',	
						buttons: ['-',
							{	xtype: 'button',	
								text: 'Cerrar',	
								iconCls: 'icoLimpiar', 	
								id: 'btnCerrar',
								handler: function(){																		
									Ext.getCmp('Vercuentas').destroy();
								} 
							},
							'-',
							{
								xtype: 'button',
								text: 'Imprimir',
								tooltip:	'Imprimir',
								id: 'btnImprimirC',
								iconCls: 'icoPdf',
								handler: function(boton, evento) {																
									Ext.Ajax.request({
										url: '15forma5MandanteExt.data.jsp',
										params:	{
											ic_mandante: ic_mandante,
											informacion: 'ImprimirConsCuentas',
											tipoAfiliado:'M',
											tituloF:tituloF,
											tituloC:tituloC											
										}					
										,callback: descargaArchivo
									});
								}
							}	
						]
					}
				}).show().setTitle(tituloF);
				
					
			//}	
	}
	
	
	//***************************Grid de las cuentas ************************
	var procesarConCuentasData = function(store,arrRegistros,opts){
	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros != null){			
			var gridCuentas = Ext.getCmp('gridCuentas');
			if (!gridCuentas.isVisible()) {
				gridCuentas.show();
			}	
			var el = gridCuentas.getGridEl();			
			var info = store.reader.jsonData;				
			gridCuentas.setTitle(info.tituloC);
			
			if(store.getTotalCount()>0){				
				Ext.getCmp('btnImprimirC').enable();
			}else{
				Ext.getCmp('btnImprimirC').disable();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	
	var consCuentasData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15forma5MandanteExt.data.jsp',
		baseParams: {
			informacion: 'ConsCuentas'			
		},		
		fields: [				
			{ name: 'CUENTA'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConCuentasData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConCuentasData(null, null, null);					
				}
			}
		}					
	});
		
	var gridCuentas = {
		//xtype: 'editorgrid',
		xtype: 'grid',
		store: consCuentasData,
		id: 'gridCuentas',		
		hidden: false,
		title: 'Usuarios',	
		columns: [			
			{
				xtype:	'actioncolumn',
				header: 'Login del Usuario',
				tooltip: 'Login del Usuario',
				dataIndex: 'CUENTA',
				align: 'center',
				width: 150,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					return (record.get('CUENTA'));
				},
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';							
						}
						,handler:	procesaCuenta  
					}
				]
			},
			{
				xtype:	'actioncolumn',
				header: 'Cambio de Perfil',
				tooltip: 'Cambio de Perfil',					
				align: 'center',				
				width: 150,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Modificar';
							return 'modificar';							
						}
						,handler:	modificarCuenta  
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 360,
		width: 320,		
		frame: true
	}
	//});
	
	
	
	//---------------Acci�n Borrar----------------
	function TransmiteBorrar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);			
			Ext.MessageBox.alert('Mensaje',info.msg,
				function(){
					consultaData.load({	
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15						
						})
					});					
				}	
			);

		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
			
	var procesarBorrar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_mandante = registro.get('IC_MANDANTE');  
			
		Ext.Ajax.request({
				url: '15forma5MandanteExt.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Borrar',
					ic_mandante:ic_mandante					
				}),
				callback: TransmiteBorrar
			});
	}
	
	//Modificar Mandante 
	
	var procesarModificar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_mandante = registro.get('IC_MANDANTE'); 
		var tipoPersona = registro.get('TIPO_PERSONA'); 		
		
		window.location ="15forma2Man_ModExt.jsp?ic_mandante="+ic_mandante+"&tipoPersona="+tipoPersona;		
	}	
	
	
	//------------Generaci�n de la Consula -------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {					
				el.unmask();
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarCSV').enable();				
			} else {		
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarCSV').disable();				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15forma5MandanteExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [
			{	name: 'IC_MANDANTE'},
			{	name: 'CADENA_PRODUCTIVA'},	
			{	name: 'NUM_NAFIN_ELEC'},
			{	name: 'RAZON_SOCIAL'},	
			{	name: 'RFC'},
			{	name: 'DOMICILIO'},
			{	name: 'ESTADO'},	
			{	name: 'TELEFONO'},
			{  name: 'TIPO_PERSONA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	})
	
	
	
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta- Mandante',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Cadena Productiva',
				tooltip: 'Cadena Productiva',
				dataIndex: 'CADENA_PRODUCTIVA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},			
			{
				header: 'N�mero de Nafin Electr�nico',
				tooltip: 'N�mero de Nafin Electr�nico',
				dataIndex: 'NUM_NAFIN_ELEC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Nombre o Razon Social',
				tooltip: 'Nombre o Razon Social',
				dataIndex: 'RAZON_SOCIAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},	
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'				
			},		
			{
				header: 'Domicilio',
				tooltip: 'Domicilio',
				dataIndex: 'DOMICILIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Estado',
				tooltip: 'Estado',
				dataIndex: 'ESTADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Tel�fono',
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				xtype		: 'actioncolumn',
				header: 'Cuentas de Usuario',
				tooltip: 'Cuentas de Usuario ',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}
						,handler: procesarVerCuentas
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Seleccionar',
				tooltip: 'Seleccionar ',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Reafiliar';
							return 'autorizar';
						}
						,handler: procesarReafiliar
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[1].tooltip = 'Modificar';
							return 'modificar';
						}
						,handler: procesarModificar
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[1].tooltip = 'Borrar';
							return 'borrar';
						}
						,handler: procesarBorrar
					}
				]
			}			
		
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo ',
					iconCls: 'icoXls',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15forma5MandanteExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GeneraArchivoCSV'			
							}),							
							callback: descargaArchivo
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir',
					handler: function(boton, evento) {
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15forma5MandanteExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GeneraArchivoPDF',
								start: barraPaginacion.cursor,
								limit: barraPaginacion.pageSize			
							}),							
							callback: descargaArchivo
						});
					}
				}				
			]
		}
	});
			
			
	//********************* Criterios de Busqueda ******************************

	
		var catalogoEstado = new Ext.data.JsonStore({
		id: 'catalogoEstado',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15forma5MandanteExt.data.jsp',
		baseParams: {
			informacion: 'catalogoEstado'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});	
	  
		
	var catalogoEpo = new Ext.data.JsonStore({
		id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15forma5MandanteExt.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});


	var storeAfiliacion = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],
			['3','Proveedor Internacional'],
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],	
			['12','Cadena Productiva Internacional'],
			['13','Contragarante'],
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo']
		]
	});
	
	
 	function creditoElec(pagina) {
		if (pagina == 0) { //Afianzadora
			window.location  = "15forma05AfianzadoraExt.jsp"; 	
		}	else if (pagina == 1 ) { // cadena productiva
			window.location ="15consEPOext.jsp";
		}	else if (pagina == 2) { //provedores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15forma05ProveedoresExt.jsp";
		}	else if (pagina == 3) { //Provedor Internacional
			
		}	else if (pagina == 4) { // provedor sin numero
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp";
		}	else if (pagina == 5) { // Distribuidores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp";		
		}	else if (pagina == 6) { // Fiados
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp";		
		}	else if (pagina == 7) { // Intermediario financiero Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B"; 	
		} else if (pagina == 8) { // Intermediario financiero NO Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB"; 	 	
		} else if (pagina == 9) { // Intermediario financiero Bancario /No Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB"; 		 	
		} else if (pagina == 10) { // Provedor Carga Masiva EContrac
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon_ext.jsp";
		}	else if (pagina == 11) { // Afiliados a Credito Electronico
			window.location = "15forma05ext_AfCredElectronico.jsp";
		}	else if (pagina == 12) { // Cadena Productiva Internacional
			
		}	else if (pagina == 13) { // Contragarante
			
		}	else if (pagina == 14) { // Mandante
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp"; 	
		}	else if (pagina == 15) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp"; 	
		}	else if (pagina == 16) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp"; 		
		}		

	}

	var storeAfiliacion = new Ext.data.SimpleStore({
		fields: ['clave', 'afiliacion'],
		data : [
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],
			//['3','Proveedor Internacional'],
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],	
			//['12','Cadena Productiva Internacional'],
			//['13','Contragarante'],
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo'],
			['16','Cliente Externo']
		]
	});
	
	
	var elementosForma = [
		{
			xtype				: 'combo',
			id					: 'cmbTipoAfiliado1',
			name				: 'cmbTipoAfiliado',
			hiddenName 		:'cmbTipoAfiliado', 
			fieldLabel		: 'Tipo de Afiliado',
			width				: 250,
			forceSelection	: true,
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'clave',
			displayField	:'afiliacion',
			value				: '14',	
			store				: storeAfiliacion,
			listeners: {
				select: {
					fn: function(combo) {						
						var valor = Ext.getCmp('cmbTipoAfiliado1').getValue();						
						creditoElec(valor);						
					}
				}
			}
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'N�mero de Nafin Electr�nico',		
			name: 'txtNumElectronico',
			id: 'txtNumElectronico1',
			allowBlank: true,
			maxLength: 30,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Nombre',		
			name: 'txtNombre',
			id: 'txtNombre1',
			allowBlank: true,
			maxLength: 30,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'combo',
			fieldLabel: 'Estado',
			name: 'sel_edo',
			id: 'sel_edo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'sel_edo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			forceSelection : true,
			store: catalogoEstado			
		},
		{
			xtype: 'textfield',
			fieldLabel: 'RFC',		
			name: 'txtRFC',
			id: 'txtRFC1',
			allowBlank: true,
			maxLength: 15,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'combo',
			fieldLabel: 'EPO',
			name: 'sel_EPO',
			id: 'sel_EPO1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'sel_EPO',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			forceSelection : true,
			store: catalogoEpo			
		}		
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Consulta Mandante',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		monitorValid: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
					
					fp.el.mask('Enviando...', 'x-mask-loading');							
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15						
						})
					});					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15forma5MandanteExt.jsp';					
				}
			}
		]
	});
	
//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		style: 'margin:0 auto;',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			gridConsulta,
			NE.util.getEspaciador(10)
		]

	});
	
	catalogoEstado.load();
	catalogoEpo.load();
	
});