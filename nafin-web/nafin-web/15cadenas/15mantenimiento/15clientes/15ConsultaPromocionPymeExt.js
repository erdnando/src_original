Ext.onReady(function() {
	var cvePerf = "";
	var TipoModificacion	= ""; 
	var busqAvanzadaSelect=0;
   var rfc='';
   

	//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
	var RenAcepto = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.ACEPTASERVICIO == "S")
									return 'SI';
								else 
									return 'NO';
						}		
	
	var procesarCatalogoNombreEPO = function(store, arrRegistros, opts) {
												store.insert(0,new Todas({ 
													clave: "", 
													descripcion: "Seleccionar ...", 
													loadMsg: ""})); 
												store.commitChanges(); 
												Ext.getCmp('id_noIc_epo').setValue('');		
	}
   
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton = Ext.getCmp('btnGenerarArchivo');
			boton.enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		Ext.getCmp('btnConsultar').enable();
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnBajarArchivo').hide();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	

	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		store.insert(0,new Todas({ 
			clave: "", 
			descripcion: "Seleccione proveedor", 
			loadMsg: ""
		})); 
		store.commitChanges(); 
		Ext.getCmp('cmb_num_ne').setValue('');		
		Ext.getCmp('btnBuscar2').enable();
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN

//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	Todas = Ext.data.Record.create([ 
		{name: "clave",         type: "string"}, 
		{name: "descripcion",   type: "string"}, 
		{name: "loadMsg",       type: "string"}
	]);
	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15ConsultaPromocionPymeExt.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NAFINELECTRONICO'},
			{name: 'NOMBREPYME'},
			{name: 'CLAVEUSUARIO'},
			{name: 'NOMBREUSUARIO'},
			{name: 'FECHA'},
			{name: 'NOMBRECONTACTO'},
			{name: 'ACEPTASERVICIO'},
			{name: 'EMAIL'},
			{name: 'CELULAR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});

	var catalogoNombreEPO = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '15ConsultaPromocionPymeExt.data.jsp',
		listeners: {		
			load: procesarCatalogoNombreEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoNombreEPO'			
		},
		totalProperty : 'total',
		autoLoad: true
	});
	
	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion','ic_pyme','loadMsg'],
		url : '15ConsultaPromocionPymeExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN  
   var gruposHeader = new Ext.ux.grid.ColumnHeaderGroup({
      rows: [
         [
            {header: '&nbsp;', colspan: 7, align: 'center', id:'c1' },
            {header: 'Datos del correo', colspan: 1, align: 'center', id:'c2'},					
				{header: 'Datos del celular', colspan: 1, align: 'center', id:'c3'}
			]
      ]
	});

	var grid = new Ext.grid.GridPanel({ 
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
      sortable: true, 
		plugins: gruposHeader,
      columnLines: true,
		hidden: true,
		columns: [{
				header: 'Num Pyme', 
				//tooltip: '',
				dataIndex: 'NAFINELECTRONICO',
				sortable: true,
				width: 120,
				align: 'center'
			},{
				header: '<center>Nombre PyME</center>',
				//tooltip: '',
				dataIndex: 'NOMBREPYME',
				sortable: true,	
				width: 380,
				align: 'left'
			},{
               header: 'Clave Usuario',
					//tooltip: '',
               dataIndex: 'CLAVEUSUARIO',
					align: 'center',
               width: 150 
            },{
               header: '<center>Nombre Usuario</center>', 
					//tooltip: 'N�mero Electronico',
               dataIndex: 'NOMBREUSUARIO',
               sortable: true,	
					align: 'left',
               width: 315
            },{
               header: 'Fecha/Hora', 
					//tooltip: '',
               dataIndex: 'FECHA',
               sortable: true,	
					align: 'center',
               width: 150
            },{
               header: '<center>Nombre del Contacto</center>', 
					//tooltip: '',
               dataIndex: 'NOMBRECONTACTO',
               sortable: true,	
               width: 315
            },{
               header: 'Acepto',
					//tooltip: '',
               dataIndex: 'ACEPTASERVICIO',
               sortable: true,	
					align: 'center',
               width: 60,
					renderer: RenAcepto
            },{
               header: '<center>Email</center>', 
					//tooltip: '',
               dataIndex: 'EMAIL',
               sortable: true,	
               width: 220, 
               align: 'left'
            },{
               header: '<center>Celular</center>', 
					//tooltip: '',
               dataIndex: 'CELULAR',
               sortable: true,	
					align: 'center',
               width: 130 
            }
		],
		loadMask: true,
		deferRowRender: false,
		height: 513,
		width: 916,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',
		collapsible: false,
		bbar: {
			autoScroll:true,
			id: 'barra',
			displayInfo: true,
			buttonAlign: 'right',
			items: [				
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						//boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '15ConsultaPromocionPymeExt.data.jsp',
							params: Ext.apply(paramSubmit,{
								informacion: 'ArchivoCSV',
								modificacion: TipoModificacion	
							}),
							//callback: procesarSuccessFailureGenerarArchivo
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				}
			]		
		}
	});
	
	var elementosFecha = [
	{
		xtype: 'textfield',
      fieldLabel: 'Usuario',
      name: 'txt_usuario',
		id:'idUsuario',
		maxLength: 8,
		anchor: '35%',
		width:250
	},{
		xtype: 'compositefield',
		fieldLabel: 'Fecha de Aceptaci�n de',
		combineErrors: false,
		msgTarget: 'side',
		width: 500,
		items: [			
		{
			xtype: 'datefield',
			name: 'txt_fecha_acep_de',
			allowBlank: false,
			value:new Date(),
			id: 'df_consultaMin',
			width: 100,
			msgTarget: 'side', 
			margins: '0 20 0 0'  //necesario para mostrar el icono de error

		},{
			xtype: 'displayfield',
			value: 'a:',
			width: 20
		},{
			xtype: 'datefield',
			name: 'txt_fecha_acep_a',
			allowBlank: false,
			id: 'df_consultaMax',
			value: new Date(),
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		}
			]
		},
		{
			xtype: 'compositefield',
			id: 'cfModificacion',
			hidden: false,
			fieldLabel:'Modificaci�n',
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'tipo_modificacion',
					id: 'radioCorreo',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoModificacion = "EMAIL"

										}
									}
					}
				},	{
					xtype: 'displayfield',
					value: 'Correo	',
					width: 120
				}, { 
					xtype: 'radio',
					name: 'tipo_modificacion',
					id: 'radioCelular',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoModificacion = "CELULAR"
										}
									}
					}
				},{
					xtype: 'displayfield',
					value: 'Celular ',
					width: 120
				}
				
			]
		}
	];

	var combo=[
	{
			xtype: 'combo',
			name: 'ic_nombreepo',
			id: 'id_noIc_epo',
			hiddenName : 'noIc_epo',
			fieldLabel: 'Nombre de la EPO',
			//emptyText: 'Seleccione EPO',
			forceSelection: true,
			store: catalogoNombreEPO,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			width:300
		}];
	
	var elementosForma = [
		{
			xtype: 'hidden',
			name: 'ic_pyme',
			id: 'ic_pyme'
		},
		{
			xtype: 'hidden',
			name: 'nafelec',
			id: 'nafelec'
		},		
		{
			xtype: 'compositefield',
			width: 800,
			fieldLabel: 'Nombre de la PYME',
			disabled: true,
			labelWidth: 200,
			items:[{
				xtype: 'textfield',
				name: 'txt_nafelec',
				hiddenName: 'txt_nafelec', 
				id:'txt_nafelec',
				width:60
			},
			{
				xtype: 'textfield',
				readOnly: true,
				id:'hid_nombre',
				name:'hid_nombre',
				disabled: true,
				hiddenName : 'hid_nombre',
				mode: 'local',
				resizable: true,
				triggerAction : 'all',
				width: 250
			},{
				xtype: 'compositefield',
				combineErrors: false,
				msgTarget: 'side',
				items: [
				{
						xtype:'displayfield',
						id:'disEspacio',
						text:''
				},{
						xtype: 'button',
						text: 'B�squeda Avanzada',
						id: 'btnAvanzada',
						iconCls:	'icoBuscar',
						handler: function(boton, evento) {
							var winVen = Ext.getCmp('winBuscaA');
								if (winVen){
									Ext.getCmp('fpWinBusca').getForm().reset();
									Ext.getCmp('fpWinBuscaB').getForm().reset();
									Ext.getCmp('cmb_num_ne').store.removeAll();
									Ext.getCmp('cmb_num_ne').reset();
									if(Ext.getCmp('id_noIc_epo').getValue() == '')
										Ext.getCmp('txtNoProveedor').hide();
									else
										Ext.getCmp('txtNoProveedor').show();									
									winVen.show();
								}else{
									var winBuscaA = new Ext.Window ({
										id:'winBuscaA',
										height: 300,
										x: 300,
										y: 100,
										width: 600,
										heigth: 100,
										modal: true,
										closeAction: 'hide',
										//title: 'Tipo de Afiliado',
										items:[{
											xtype:'form',
											id:'fpWinBusca',
											frame: true,
											border: false,
											style: 'margin: 0 auto',
											bodyStyle:'padding:10px',
											defaults: {
												msgTarget: 'side',
												anchor: '-20'
											},
											labelWidth: 140,
											items:[{
												xtype:'displayfield',
												frame:true,
												border: false,
												value:'Utilice el * para b�squeda gen�rica'
											},{
												xtype:'displayfield',
												frame:true,
												border: false,
												value:''
											},{
												xtype: 'textfield',
												name: 'nombre_pyme',
												id:	'txtNombre',
												fieldLabel:'Nombre',
												maxLength:	100
											},{
												xtype: 'textfield',
												name: 'rfc_pyme',
												id:	'txtRfc',
												fieldLabel:'RFC',
												maxLength:	20
											},{
												xtype: 'textfield',
												name: 'claveProveedor',
												id: 'txtNoProveedor',
												fieldLabel: 'N�mero Proveedor',
												allowBlank: true,
												hidden:true,
												maxLength: 15,
												width: 80
											}
											],
											buttons:[{
												text:'Buscar',
												id: 'btnBuscar2',
												iconCls:'icoBuscar',
												handler: function(boton) {
													if(Ext.getCmp('txtNombre').getValue() == '' && Ext.getCmp('txtRfc').getValue() == '' && Ext.getCmp('txtNoProveedor').getValue() == ''){
														Ext.Msg.alert(boton.text,'No existe informaci�n con los criterios determinados');
														return;
													}
													boton.disable();
													catalogoNombreData.load({ 
                                          params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues(),
														{epo: Ext.getCmp('id_noIc_epo').getValue()}) 
                                       });
												}
											},{
												text:'Cancelar',
												iconCls: 'icoLimpiar',
												handler: function() {
													Ext.getCmp('btnAceptar').disable();
													Ext.getCmp('winBuscaA').hide();
												}
											}]
										},{
											xtype:'form',
											frame: true,
											id:'fpWinBuscaB',
											style: 'margin: 0 auto',
											bodyStyle:'padding:10px',
											monitorValid: true,
											defaults: {
												msgTarget: 'side',
												anchor: '-20'
											},
											items:[{
												xtype: 'combo',
												id:	'cmb_num_ne',
												name: 'cmb_num_neName',
												hiddenName : 'cmb_num_neName',
												fieldLabel: 'Nombre',
												displayField: 'descripcion',
												valueField: 'clave',
												triggerAction : 'all',
												//emptyText: 'Seleccione proveedor',
												forceSelection:true,
												allowBlank: false,
												typeAhead: true,
												mode: 'local',
												minChars : 1,
												store: catalogoNombreData,
												tpl : NE.util.templateMensajeCargaCombo,
												listeners: {
													select: function(combo, record, index) {
														if(combo.getValue() == '' ){
															Ext.getCmp('btnAceptar').disable();
														}else{
															Ext.getCmp('btnAceptar').enable();
														}
														busqAvanzadaSelect=index;
													}
												}
											}],
											buttons:[{
												text:'Aceptar',
												iconCls:'aceptar',
												disabled:true,
												id:'btnAceptar',
												//formBind:true,
												handler: function() {
													if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
														var reg = Ext.getCmp('cmb_num_ne').getStore().getAt(busqAvanzadaSelect).get('ic_pyme');
														Ext.getCmp('ic_pyme').setValue(reg);
														var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
														var desc = disp.slice(disp.indexOf(" ")+1);
														Ext.getCmp('txt_nafelec').setValue(Ext.getCmp('cmb_num_ne').getValue());
														Ext.getCmp('nafelec').setValue(Ext.getCmp('cmb_num_ne').getValue());
														Ext.getCmp('hid_nombre').setValue(desc);
														Ext.getCmp('btnAceptar').disable();
														Ext.getCmp('winBuscaA').hide();
													}
												}
											},{
												text:'Cancelar',
												iconCls: 'icoLimpiar',
												handler: function() {	
													Ext.getCmp('btnAceptar').disable();
													Ext.getCmp('winBuscaA').hide();	
												}
											}]
										}]
									}).show();
									if(Ext.getCmp('id_noIc_epo').getValue() == '')
										Ext.getCmp('txtNoProveedor').hide();
									else
										Ext.getCmp('txtNoProveedor').show();									

								}
							}
						}]
				}]
		}
		];
	
	//Forma para hacer la busqueda filtrada
	var fp = new Ext.form.FormPanel({
		//xtype: 'form',
		id: 'forma',
		style: ' margin:0 auto;',
		collapsible: false,
		frame:true,
		width: 600,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[combo,elementosForma,elementosFecha],
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					var df_consultaNotMin = Ext.getCmp("df_consultaMin");
					var df_consultaNotMax = Ext.getCmp("df_consultaMax");		
               var grid              = Ext.getCmp('grid');
					
					var fechaDe =  Ext.util.Format.date(df_consultaNotMin.getValue(),'d/m/Y');  
					var fechaA  =  Ext.util.Format.date(df_consultaNotMax.getValue(),'d/m/Y');  
					
					if(!isdate(fechaDe)){
						df_consultaNotMin.markInvalid('La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa');
					   consultaDataGrid.removeAll();
                  grid.hide();						
						return ;
					}
					if(!isdate(fechaA)){
						df_consultaNotMax.markInvalid('La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa');
					   consultaDataGrid.removeAll();
                  grid.hide();						
						return ;
					}

				/********   *********/
               boton.disable();
					grid.show();
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaDataGrid.load({
						params: Ext.apply(paramSubmit,{
						modificacion: TipoModificacion							
					})
				});
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15ConsultaPromocionPymeExt.jsp';
				}
			}
		]
	
	});//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});

});
