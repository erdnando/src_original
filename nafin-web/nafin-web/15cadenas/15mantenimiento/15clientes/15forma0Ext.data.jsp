<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	JSONObject resultado = new JSONObject();


 if(informacion.equals("catalogoBanco"))	{
		//String banco = (request.getParameter("cmb_Banco")==null)?"":request.getParameter("cmb_Banco");
	
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_banco_fondeo");
		cat.setCampoClave("ic_banco_fondeo");
		cat.setCampoDescripcion("cd_descripcion");
		//cat.setCondicionIn(banco, "String");
		cat.setOrden("ic_banco_fondeo");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("catalogoPais")) {		
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionNotIn("52", Integer.class);
		infoRegresar = cat.getJSONElementos();	
	}
 else if(informacion.equals("catalogoEstado")){
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setClavePais(Pais);//Mexico clave 24
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();	
 }
 else if(informacion.equals("catalogoMunicipio"))	{
		
		String Edo= (request.getParameter("estado")==null)?"":request.getParameter("estado");
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoMunicipio cat=new CatalogoMunicipio();
		cat.setClave("ic_municipio");
		cat.setDescripcion("cd_nombre");
		cat.setPais(Pais);
		cat.setEstado(Edo);
		cat.setOrden("cd_nombre");
		List elementos=cat.getListaElementos();
		
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
 }
 else if(informacion.equals("catalogoIdentificacion"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_identificacion");
		cat.setCampoClave("ic_identificacion");
		cat.setCampoDescripcion("cd_nombre");
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
 }
 else if(informacion.equals("catalogoRamoSIAFF"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_siaff_ramos");
		cat.setCampoClave("cc_ramo_siaff");
		cat.setCampoDescripcion("cc_ramo_siaff || '- ' || cg_descripcion");
		cat.setOrden("cc_ramo_siaff");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("catalogoUnidadSIAFF"))	{
		String ramo= (request.getParameter("ramo")==null)?"":request.getParameter("ramo");
	
		CatalogoUnidadSIAFF cat = new CatalogoUnidadSIAFF();
		cat.setClave("cc_unidad_siaff");
		cat.setDescripcion("cc_unidad_siaff || ' - ' || cg_descripcion");		
		cat.setSeleccion(ramo);
		cat.setOrden("cc_unidad_siaff");
		infoRegresar = cat.getJSONElementos();		
 }
 ////////////////FODEA2012//////////////////
 else if(informacion.equals("catalogoSectorECO"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_sector_econ_epo");
		cat.setCampoClave("ic_sector_econ_epo");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("catalogoSubSectorECO"))	{
		String sectorECO= (request.getParameter("sectorECO")==null)?"":request.getParameter("sectorECO");
	
		CatalogoSubsectorEconomico cat = new CatalogoSubsectorEconomico();
		cat.setClave("ic_subsector_epo");
		cat.setDescripcion("cg_descripcion");
		cat.setSeleccion(sectorECO);
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 
 else if(informacion.equals("catalogoRamaECO"))	{
		String sectorECO= (request.getParameter("sectorECO")==null)?"":request.getParameter("sectorECO");
		String subsectorECO= (request.getParameter("subsectorECO")==null)?"":request.getParameter("subsectorECO");
	
		CatalogoRamaEconomica cat = new CatalogoRamaEconomica();
		cat.setClave("ic_rama_econ_epo");
		cat.setDescripcion("cg_descripcion");
		cat.setSectorEcon(sectorECO);
		cat.setSubSectorEcon(subsectorECO);
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 ////////////////FODEA2012//////////////////
 
  else if(informacion.equals("catalogoSectorEPO"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_sector_epo");
		cat.setCampoClave("ic_sector_epo");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("catalogoSubdireccion"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_subdireccion");
		cat.setCampoClave("ic_subdireccion");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 
 else if(informacion.equals("catalogoPromotor"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_lider_promotor");
		cat.setCampoClave("ic_lider_promotor");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("ConsultaRFCexistente"))	{
	try	
	{
		
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		String rfc= (request.getParameter("rf")==null)?"":request.getParameter("rf");
		System.out.println("RFC " + rfc);
		
		boolean res = BeanAfiliacion.verificaRFC(rfc,"E");
		if(res) {
			resultado.put("success",new Boolean(true));  //El RFC es correcto
			infoRegresar = resultado.toString();
		} else {
			resultado.put("success",new Boolean(false));  //El RFC es correcto
			infoRegresar = resultado.toString();
		}
		
	} catch(Exception e) { //catch(NafinException lexError){
		throw new AppException("Error en la afiliacion", e);
	  }
	
 }

 else if(informacion.equals("AltaEPO"))	{
	try	
		{

		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		

		String 	Razon_Social  				= (request.getParameter("Razon_Social") == null) ? "" : request.getParameter("Razon_Social"),
					Nombre_comercial 			= (request.getParameter("nombre_comercial") == null) ? "" : request.getParameter("nombre_comercial"),
					R_F_C         				= (request.getParameter("rfc")    == null) ? "" : request.getParameter("rfc"),
					Numero_Escritura			= (request.getParameter("num_escritura") == null) ? "" : request.getParameter("num_escritura"),
					Fecha_Constitucion		= (request.getParameter("fecha_cons") == null) ? "" : request.getParameter("fecha_cons");
					//Domicilio							
		String   Calle        				= (request.getParameter("calle")    == null) ? "" : request.getParameter("calle"),
					Numero_Exterior 			= (request.getParameter("num_ext") == null) ? "" : request.getParameter("num_ext"),
					Numero_interior 			= (request.getParameter("num_int") == null) ? "" : request.getParameter("num_int"),
					Colonia       				= (request.getParameter("colonia")  == null) ? "" : request.getParameter("colonia"),
					Pais          				= (request.getParameter("cmb_pais")     == null) ? "" : request.getParameter("cmb_pais"),
					Estado        				= (request.getParameter("cmb_estado")   == null) ? "" : request.getParameter("cmb_estado"),
					Delegacion_o_municipio 	= (request.getParameter("cmb_delegacion") == null) ? "" : request.getParameter("cmb_delegacion"),
					//Delegacion_o_municipio  = clave_municipio + " | " + nombre_municipio,
					Codigo_postal 				= (request.getParameter("code") == null) ? "" : request.getParameter("code"),
					Telefono      				= (request.getParameter("telefono") == null) ? "" : request.getParameter("telefono"),
					Email         				= (request.getParameter("email")    == null) ? "" : request.getParameter("email"),
					Fax           				= (request.getParameter("fax")      == null) ? "" : request.getParameter("fax");
					//Datos del Representate Legal
		String 	Apellido_paterno_L       = (request.getParameter("paterno")       == null) ? "" : request.getParameter("paterno"),
					Apellido_materno_L       = (request.getParameter("materno")       == null) ? "" : request.getParameter("materno"),
					Nombre_L                 = (request.getParameter("nombre")        == null) ? "" : request.getParameter("nombre"),
					No_Identificacion			 = (request.getParameter("num_iden") == null) ? "" : request.getParameter("num_iden"),
					Fecha_del_poder_notarial = (request.getParameter("fch_poder") == null) ? "" : request.getParameter("fch_poder"),
					Numero_de_escritura      = (request.getParameter("poderes")      == null) ? "" : request.getParameter("poderes");
					//Datos del Contacto
		String	Apellido_paterno_C 		 = (request.getParameter("paterno_contacto") == null) ? "" : request.getParameter("paterno_contacto"),
					Apellido_materno_C 		 = (request.getParameter("materno_contacto") == null) ? "" : request.getParameter("materno_contacto"),
					Nombre_C           		 = (request.getParameter("nombre_contacto") 			 == null) ? "" : request.getParameter("nombre_contacto"),
					Telefono_C         		 = (request.getParameter("telc") 		 == null) ? "" : request.getParameter("telc"),
					Fax_C              		 = (request.getParameter("faxc") 			 == null) ? "" : request.getParameter("faxc"),
					Email_C            		 = (request.getParameter("emailc") 			 == null) ? "" : request.getParameter("emailc");
					//Otros Datos
		String 	c_proveedores   			 = (request.getParameter("c_proveedores")   == null) ? "" : request.getParameter("c_proveedores"),
					ramo_siaff 					 = (request.getParameter("cmb_ramo") == null) ? "" : request.getParameter("cmb_ramo"),//FODEA 034 - 2009 ACF
					unidad_siaff 			    = (request.getParameter("cmb_unidad") == null) ? "" : request.getParameter("cmb_unidad"),//FODEA 034 - 2009 ACF					
					no_sirac        			 = (request.getParameter("num_sirac")  == null) ? "" : request.getParameter("num_sirac"),
					msChkFinan  				 = (request.getParameter("ditribuidores")  == null) ? "N" : request.getParameter("ditribuidores"),
					cg_banco_retiro 			 = (request.getParameter("bco_retiro") == null) ? "" : request.getParameter("bco_retiro"),
					convenio_unico 			 = (request.getParameter("convenio") == null) ? "" : request.getParameter("convenio"),
					cg_cta_retiro 				 = (request.getParameter("cuenta") == null) ? "" : request.getParameter("cuenta"), 
					mandato_documento 		 = (request.getParameter("irrevocable")==null)?"":request.getParameter("irrevocable"),//FODEA 041 - 2009						
					cmb_sector_epo 			 = (request.getParameter("cmb_sector")== null) ? "" : request.getParameter("cmb_sector"),//FODEA 057-2010 FVR
					cmb_subdireccion 			 = (request.getParameter("cmb_subdirec")== null) ? "" : request.getParameter("cmb_subdirec"),//FODEA 057-2010 FVR
					cmb_lider_promotor 		 = (request.getParameter("cmb_promotor")== null) ? "" : request.getParameter("cmb_promotor");//FODEA 057-2010 FVR
					//solo porque los pide
		String cliente_primer_piso 		 = (request.getParameter("cliente_primer_piso") == null) ? "" : request.getParameter("cliente_primer_piso");
		String Identificacion				 = (request.getParameter("cmb_identificacion") == null) ? "" : request.getParameter("cmb_identificacion");
		String noBancoFondeo     			 = "1";
		String SSId_Number 					 = (request.getParameter("SSId_Number") == null) ? "" : request.getParameter("SSId_Number");
		String Dab_Number 					 = (request.getParameter("Dab_Number") == null) ? "" : request.getParameter("Dab_Number");
		String internacional 				 = (request.getParameter("internacional") == null) ? "" : request.getParameter("internacional");
		/**Fodea2012**/
		String 	cmb_sector_eco   			 = (request.getParameter("cmb_economico")   == null) ? "" : request.getParameter("cmb_economico"),
					cmb_subsector_eco			 = (request.getParameter("cmb_subsectoreco") == null) ? "" : request.getParameter("cmb_subsectoreco"),
					cmb_rama_eco			    = (request.getParameter("cmb_ramaECO") == null) ? "" : request.getParameter("cmb_ramaECO");
		
		   
		if(!msChkFinan.equals(""))
		{
			if ( msChkFinan.equals("on") )
				msChkFinan = "S";
			else
				msChkFinan = "N";
		}
		if(!convenio_unico.equals(""))
		{
			if ( convenio_unico.equals("on") )
				convenio_unico = "S";
			else
				convenio_unico = "N";
		}
		if(!mandato_documento.equals(""))
		{
			if ( mandato_documento.equals("on") )
				mandato_documento = "S";
			else
				mandato_documento = "N";
		}
		if (internacional.equals("")){
			internacional = "N";
		}
		
		if( !Delegacion_o_municipio.equals("") && Delegacion_o_municipio != null)
		{
			CatalogoMunicipio cat=new CatalogoMunicipio();
			cat.setClave("ic_municipio");
			cat.setDescripcion("cd_nombre");
			cat.setPais("24");
			cat.setEstado(Estado);
			cat.setSeleccion(Delegacion_o_municipio);
			cat.setOrden("cd_nombre");
			List elementos=cat.getListaElementos();
		
			Iterator it = elementos.iterator();
			while(it.hasNext()) {
				ElementoCatalogo obj = (ElementoCatalogo)it.next();
				
				//iDelMun + "|" + strDelMun ;
				String iDelMun = obj.getClave();
				String strDelMun = obj.getDescripcion();
				Delegacion_o_municipio = iDelMun + "|" + strDelMun;
			}
			System.out.println("Delegacion_o_municipio " +  Delegacion_o_municipio ) ;
		}
		
		String sDatosEpo =	Razon_Social+"|"+Nombre_comercial+"|"+R_F_C+"|"+Numero_Escritura+"|"+
						Fecha_Constitucion+"|"+Calle+"|"+Numero_Exterior+"|"+Numero_interior+"|"+
						Colonia+"|"+Pais+"|"+Estado+"|"+Delegacion_o_municipio+"|"+Codigo_postal+"|"+Telefono+"|"+
						Email+"|"+Fax+"|"+Apellido_paterno_L+"|"+Apellido_materno_L+"|"+Nombre_L+"|"+No_Identificacion+"|"+
						Fecha_del_poder_notarial+"|"+Numero_de_escritura+"|"+Apellido_paterno_C+"|"+Apellido_materno_C+"|"+Nombre_C+"|"+
						Telefono_C+"|"+Fax_C+"|"+Email_C+"|"+c_proveedores+"|"+ramo_siaff+"|"+unidad_siaff+"|"+
						no_sirac+"|"+msChkFinan+"|"+cg_banco_retiro+"|"+convenio_unico+"|"+cg_cta_retiro+"|"+mandato_documento+"|"+
						cmb_sector_epo+"|"+cmb_subdireccion+"|"+cmb_lider_promotor+"|"+
						cmb_sector_eco+"|"+cmb_subsector_eco+"|"+cmb_rama_eco+"\n";
		//System.out.println(sDatosEpo);			
		
		
		String lsNoNafinElectronico  = BeanAfiliacion.afiliaEpo(iNoUsuario, Razon_Social.toUpperCase(), Nombre_comercial.toUpperCase(), R_F_C.toUpperCase(),
							Calle.toUpperCase(),    Numero_Exterior, Numero_interior, Colonia.toUpperCase(), Estado, 
							Delegacion_o_municipio, Codigo_postal,   Pais,			  Telefono,              Email.toLowerCase(),
							Fax,                    Fecha_del_poder_notarial,         Numero_Escritura,   Apellido_paterno_L,
							Apellido_materno_L,		Nombre_L,        Apellido_paterno_C,                     Apellido_materno_C,
							Nombre_C,Telefono_C,    Fax_C,Email_C,   c_proveedores,   cliente_primer_piso,   no_sirac,
							msChkFinan, cg_banco_retiro, cg_cta_retiro, Identificacion, No_Identificacion,internacional,SSId_Number,Dab_Number,
							Numero_Escritura,Fecha_Constitucion,noBancoFondeo,ramo_siaff,unidad_siaff,convenio_unico, mandato_documento,
							cmb_sector_epo,cmb_subdireccion,cmb_lider_promotor,cmb_sector_eco,cmb_subsector_eco,cmb_rama_eco);
///pregunto como nos fue, ajajaj
				if( lsNoNafinElectronico != null  &&  !lsNoNafinElectronico.equals("") )
				{
					resultado.put("success", new Boolean(true));
					resultado.put("lsNoNafinElectronico",lsNoNafinElectronico);
					infoRegresar = resultado.toString();	
				}
			}
			catch(Exception e) { //catch(NafinException lexError){
				//out.print("<script language=\"JavaScript\">alert(\"" + lexError.getMsgError() + "\"); history.back(-1);</script>");
				throw new AppException("Error en la afiliacion", e);
			}
 }


//System.out.println("infoRegresar============"+infoRegresar); 


%>
<%=infoRegresar%>



