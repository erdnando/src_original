<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject, 
		com.netro.seguridad.*,
		java.io.*,
		com.jspsmart.upload.*,
		java.sql.*,
		netropology.utilerias.usuarios.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,	
		com.netro.cadenas.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%

	// Nota: corregir este JSP, para que vaya leyendo archivos de bloque en bloque y guardando archivos
	// de bloque en bloque
	String infoRegresar ="";
	String procesoID = (request.getParameter("procesoID") == null) ? "" : request.getParameter("procesoID");

	CreaArchivo 	archivo 				= new CreaArchivo();
	StringBuffer 	contenidoArchivo 	= new StringBuffer();
	String 			nombreArchivo 		= null;

	// Leer lista de lineas con errores
	AccesoDB 		con 						= new AccesoDB();
	List 				lvarbind 				= new ArrayList();
	Registros 		registrosConError 	= null;
	Registros 		registrosConErrorEcon 	= null;
	String 			query 					= "";
	int x =0, y=0;
	try{
		con.conexionDB();
		query =
			"SELECT " +
				"CG_NUMERO_PROVEEDOR, CG_RFC, CG_APPAT, CG_APMAT, CG_NOMBRE, CG_RAZON_SOCIAL, CG_CALLE, CG_COLONIA, CG_ESTADO, CG_PAIS,"+
				"CG_MUNICIPIO, CG_CP, CG_TELEFONO, CG_FAX, CG_EMAIL, CG_APPAT_C, CG_APMAT_C, CG_NOMBRE_C, CG_TELEFONO_C, CG_FAX_C,"+
				"CG_EMAIL_C, CG_TIPO_CLIENTE, CG_SUSCEPTIBLE_DESCONTAR " +
			"FROM COMTMP_AFILIA_PYME_MASIVA " +
			"WHERE " +
				"IC_NUMERO_PROCESO = ? AND "+
				"CG_MENSAJES_ERROR IS NOT NULL " +
			"ORDER BY IC_NUMERO_LINEA ASC ";
		lvarbind.add(procesoID);
		registrosConError = con.consultarDB(query,lvarbind,false);

		/*****/
		query =
			"SELECT " +
				"CG_NUMERO_PROVEEDOR, CG_RFC, CG_APPAT, CG_APMAT, CG_NOMBRE, CG_RAZON_SOCIAL, CG_CALLE, CG_COLONIA, CG_ESTADO, CG_PAIS,"+
				"CG_MUNICIPIO, CG_CP, CG_TELEFONO, CG_FAX, CG_EMAIL, CG_APPAT_C, CG_APMAT_C, CG_NOMBRE_C, CG_TELEFONO_C, CG_FAX_C,"+
				"CG_EMAIL_C, CG_TIPO_CLIENTE, CG_SUSCEPTIBLE_DESCONTAR " +
			"FROM ECON_AFILIA_PYME_MASIVA " +
			"WHERE " +
				"IC_NUMERO_PROCESO = ? AND "+
				"CG_MENSAJES_ERROR IS NOT NULL " +
			"ORDER BY IC_NUMERO_LINEA ASC ";
		//lvarbind.add(procesoID);
		registrosConErrorEcon = con.consultarDB(query,lvarbind,false);
	}catch (Exception e){
		e.printStackTrace();
		throw new NafinException("SIST0001");
	}finally{
		if(con.hayConexionAbierta()) con.cierraConexionDB();
	}

	// Formatear lineas con errores
		while(registrosConError != null && registrosConError.next()) {
			x++;
			if(x==1){
				contenidoArchivo.append("Nafin Electronico");
				contenidoArchivo.append("\n");
				contenidoArchivo.append("\n");
			}

			// Extraer registros

			contenidoArchivo.append(registrosConError.getString("CG_NUMERO_PROVEEDOR")); contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_RFC"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_APPAT"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_APMAT"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_NOMBRE"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_RAZON_SOCIAL"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_CALLE"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_COLONIA"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_ESTADO"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_PAIS"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_MUNICIPIO"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_CP"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_TELEFONO"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_FAX"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_EMAIL"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_APPAT_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_APMAT_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_NOMBRE_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_TELEFONO_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_FAX_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_EMAIL_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_TIPO_CLIENTE"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_SUSCEPTIBLE_DESCONTAR"));contenidoArchivo.append("|");

			// Agregar registro a archivo con errores
			contenidoArchivo.append("\n");

		}

		// Formatear lineas con errores
				while(registrosConErrorEcon != null && registrosConErrorEcon.next()) {

					y++;
					if(y==1){
						contenidoArchivo.append("Econtract");
						contenidoArchivo.append("\n");
						contenidoArchivo.append("\n");
					}
					// Extraer registros
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_NUMERO_PROVEEDOR")); contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_RFC"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_APPAT"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_APMAT"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_NOMBRE"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_RAZON_SOCIAL"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_CALLE"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_COLONIA"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_ESTADO"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_PAIS"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_MUNICIPIO"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_CP"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_TELEFONO"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_FAX"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_EMAIL"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_APPAT_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_APMAT_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_NOMBRE_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_TELEFONO_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_FAX_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_EMAIL_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_TIPO_CLIENTE"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_SUSCEPTIBLE_DESCONTAR"));contenidoArchivo.append("|");

					// Agregar registro a archivo con errores
					contenidoArchivo.append("\n");

		}


	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt")){
		//int("<--!Error al generar el archivo-->");
	}else{
		nombreArchivo = archivo.nombre;
	}

	String retorno = strDirecVirtualTemp+nombreArchivo;
	JSONObject jsonObj 	      = new JSONObject();
	
	jsonObj.put("urlArchivo",retorno);
	jsonObj.put("success",new Boolean(true));
	infoRegresar = jsonObj.toString();
							
%>
<%=infoRegresar%>