<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		com.netro.afiliacion.*,
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.seguridadbean.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
    String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
    String infoRegresar = "";

    if(informacion.equals("initPage")) {
        String cvePerf = "";
        //SeguridadHome seguridadHome =(SeguridadHome)ServiceLocator.getInstance().getEJBHome("SeguridadEJB",SeguridadHome.class);
        //com.netro.seguridadbean.Seguridad BeanSegFacultad = seguridadHome.create();
        com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);

        cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);
	
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("cvePerf",cvePerf);
		infoRegresar = jsonObj.toString();	
	
    }else if(informacion.equals("CatalogoEPO")){
        CatalogoEPO catalogo = new CatalogoEPO();
        catalogo.setCampoClave("ic_epo");
        catalogo.setCampoDescripcion("cg_razon_social");
		//catalogo.setClaveIf(String claveIf)	
		//catalogo.setClaveIf(ic_if);
        infoRegresar = catalogo.getJSONElementos();		

    }else if(informacion.equals("CatalogoIF")){
        String cveEpo = (request.getParameter("comboEpo")!=null)?request.getParameter("comboEpo"):"";
        CatalogoIF cat = new CatalogoIF();
        cat.setCampoClave("ic_if");
        cat.setCampoDescripcion("cg_razon_social");
        cat.setClaveEpo( cveEpo);
        cat.setOrden("cg_razon_social");
        cat.setEstatusAfiliacion("S");
        infoRegresar = cat.getJSONElementos();
    }
    else if(informacion.equals("Consulta")||informacion.equals("ArchivoCSV")	||informacion.equals("ArchivoXpaginaPDF")){
        int start = 0;
        int limit = 0;
        String operacion = (request.getParameter("operacion") == null) ? "" : request.getParameter("operacion");
        String claveIf  						= (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
        String claveEpo  						= (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
        String nafinElectronico  			= (request.getParameter("numeroNae") == null)?"":request.getParameter("numeroNae");
        String numProveedor 					= (request.getParameter("numProveedor") == null)?"":request.getParameter("numProveedor");
        String fechaHabilitacionMax		= (request.getParameter("fecha_sol_a")==null)?"":request.getParameter("fecha_sol_a");
        String fechaHabilitacionMin		= (request.getParameter("fecha_sol_de")==null)?"":request.getParameter("fecha_sol_de");
        String pymesSN					= (request.getParameter("pymesSN")==null)?"":request.getParameter("pymesSN");
        String pymesHab				= (request.getParameter("pymesHab")==null)?"":request.getParameter("pymesHab");
		
        String chkPymesH = (request.getParameter("chkPymesH")==null)?"":request.getParameter("chkPymesH");
        System.out.println("******* strTipoUsuario: "+strTipoUsuario);
                
        String cveEpo = "", mensaje ="", numProveedor1 ="";
        if(strTipoUsuario.equals("EPO")){ //??
            cveEpo = iNoCliente; 		
        }else{
            cveEpo = claveEpo;
        }		
		
        if(!nafinElectronico.equals("")){
            HashMap	registros = this.getObtienePYME( nafinElectronico ,cveEpo );
            numProveedor1 = registros.get("NUM_PROVEEDOR").toString();
            mensaje = registros.get("MENSAJE").toString();
            if(!numProveedor.equals("") ){
                numProveedor =numProveedor1;
            }			
        }	
        ConsEstatusAfiliacion paginador = new ConsEstatusAfiliacion();
        paginador.setClaveEpo(cveEpo);
        paginador.setClaveIf(claveIf);
        paginador.setNafinElectronico(nafinElectronico);
        paginador.setNoProveedor(numProveedor);
        paginador.setFechaHabilitacionMin(fechaHabilitacionMin);
        paginador.setFechaHabilitacionMax(fechaHabilitacionMax);
        paginador.setPymesSN(pymesSN);
        paginador.setPymesHab(pymesHab);
        paginador.setChkPymesH(chkPymesH);
        paginador.setPerfil(strPerfil);
        CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
        if(informacion.equals("Consulta")){
            try{
                start = Integer.parseInt(request.getParameter("start"));
                limit = Integer.parseInt(request.getParameter("limit"));
            }catch(Exception e){
                throw new AppException("Error en los parámetros recibidos",e);
            }
            try{
                if(operacion.equals("Generar")){//Nueva consulta
                    queryHelper.executePKQuery(request);
                }
				
                String  consulta = queryHelper.getJSONPageResultSet(request,start,limit);
                JSONObject jsonObj = new JSONObject();
                jsonObj = JSONObject.fromObject(consulta);
                jsonObj.put("success", new Boolean(true));
                jsonObj.put("numProveedor",numProveedor);
                jsonObj.put("nafinElectronico",nafinElectronico);
                jsonObj.put("claveEpo",claveEpo);
                jsonObj.put("mensaje",mensaje);
                infoRegresar = jsonObj.toString();				
				
            }catch(Exception e){
                throw new AppException("Error en la paginación", e);
            }
        }
        else if(informacion.equals("ArchivoCSV")){
            try{
                String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("success", new Boolean(true));
                jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
                infoRegresar = jsonObj.toString();
            }catch(Throwable e){
                throw new AppException("Error al generar el archivo CSV",e);
            }
        }
        else if (informacion.equals("ArchivoXpaginaPDF")) {
            try {
                start = Integer.parseInt(request.getParameter("start"));
                limit = Integer.parseInt(request.getParameter("limit"));
                String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp, "PDF");
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("success", new Boolean(true));
                jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
                infoRegresar = jsonObj.toString();
                } catch(Throwable e) {
                    throw new AppException("Error al generar el archivo PDF", e);
                }
		}
	}
	else if(informacion.equals("CatalogoProveedor")){
			String clavePymeSelecionada = (request.getParameter("cmb_num_ne") == null)?"":request.getParameter("cmb_num_ne");
			String nombrePymeIntroducido = (request.getParameter("nombre_pyme") == null)?"":request.getParameter("nombre_pyme");
			String rfcIntroducido = (request.getParameter("rfc_prov") == null)?"":request.getParameter("rfc_prov");
			String claveProveedor = (request.getParameter("num_pyme") == null)?"":request.getParameter("num_pyme");
			System.out.println("**iNoCliente: "+(iNoCliente == "0" ? "" : iNoCliente)+", clavePymeSelecionada: "+clavePymeSelecionada+", claveProveedor:"+claveProveedor+", rfcIntroducido:"+rfcIntroducido+", nombrePymeIntroducido:"+nombrePymeIntroducido );
			
			Afiliacion afiliacionBean = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);						
			List catalogoProveedor = afiliacionBean.obtenerProveedores((iNoCliente == "0" ? "" : iNoCliente), clavePymeSelecionada, claveProveedor, rfcIntroducido, nombrePymeIntroducido);
			//List catalogoProveedor = afiliacionBean.obtenerProv((iNoCliente == "0" ? "" : iNoCliente), clavePymeSelecionada, claveProveedor, rfcIntroducido, nombrePymeIntroducido);
			
			JSONObject jsonObj = new JSONObject();
			JSONArray jsonArray = JSONArray.fromObject(catalogoProveedor);
			jsonObj.put("success", new Boolean(true));
			int num = catalogoProveedor.size();
			System.out.println("** "+num);
			if(num == 0){
				jsonObj.put("total", "0");
				jsonObj.put("registros", "");
			}else if( num >  0 && num <= 1000){
				jsonObj.put("total", catalogoProveedor.size());
				jsonObj.put("registros", jsonArray.toString());
			}else if(num > 1000){
				jsonObj.put("total", "excede");
				jsonObj.put("registros", "");
			}
			//System.out.println(jsonObj.toString());
			infoRegresar =jsonObj.toString();					
	
	}
	
%>
<%=infoRegresar%>


<%!
	public HashMap   getObtienePYME(String  nafinElectronico ,String ic_epo  ){
		log.info("getObtienePYME (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;			
		List  lVarBind		= new ArrayList();	
		PreparedStatement ps = null;
		String numProveedor ="", mensaje ="";
		HashMap	registros = new HashMap();
		
		try{
			con.conexionDB();
					
			StringBuffer strSQL = new StringBuffer(); 
			strSQL.append("     SELECT pym.ic_pyme as ic_pyme , pym.cg_razon_social"   +
            "    FROM comrel_nafin crn, comrel_pyme_epo cpe, comcat_pyme pym"   +
            "    WHERE crn.ic_epo_pyme_if = cpe.ic_pyme"   +
            "    AND cpe.ic_pyme = pym.ic_pyme"   +
            "    AND crn.ic_nafin_electronico = ?"   +
            "    AND crn.cg_tipo =   ? "   +
            "    AND cpe.cs_habilitado =  ?  "   +
            "    AND cpe.cg_pyme_epo_interno IS NOT NULL ");
				
				lVarBind.add(nafinElectronico);
				lVarBind.add("P");
				lVarBind.add("S");
						
				if(!"".equals(ic_epo)) {
					strSQL.append( "    AND cpe.ic_epo =  ? ")  ;
					lVarBind.add(ic_epo);
					mensaje = "El nafin electronico no corresponde a una <br> PyME afiliada a la EPO o no existe.";  
				}else {
				  mensaje = "El nafin electronico no corresponde a una PyME o no existe.";
				}						
				
			log.info("strSQL.toString() "+strSQL.toString());
			log.info("lVarBind) " +lVarBind);
			
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind );			
			rs = ps.executeQuery();
			if(rs.next()) {
				numProveedor = rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				registros.put("NUM_PROVEEDOR", numProveedor);	
				registros.put("MENSAJE", "");	
			}else {
				registros.put("MENSAJE", mensaje);	
				registros.put("NUM_PROVEEDOR", "");	
			}
			rs.close();
			ps.close();
					
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getObtienePYME (S) ");
		return registros;
	}
	
%>