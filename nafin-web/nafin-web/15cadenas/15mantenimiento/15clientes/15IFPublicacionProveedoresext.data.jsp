	<%@ page contentType="application/json;charset=UTF-8" import="   
  com.netro.anticipos.*,
	java.util.*,
	com.netro.exception.*,  
	com.netro.model.catalogos.*,
	netropology.utilerias.usuarios.*,   
	net.sf.json.JSONObject,
	com.netro.descuento.*,
	com.netro.cadenas.IFPublicacionProveedores,
	com.netro.cadenas.*,
	org.apache.commons.beanutils.BeanUtils,
	net.sf.json.JSONArray"
	errorPage="/00utils/error_extjs.jsp"
%>
	
<%@ include file="/15cadenas/015secsession.jspf" %>
  
<% 
	String usuario                = iNoCliente;
	String moneda                 = request.getParameter("HcbMoneda");            	if (moneda== null) { moneda=""; }
	String epo                    = request.getParameter("HicEPO");              	if (epo== null) { epo= ""; }  
	String estatus                = request.getParameter("HcbEstatus");             //if (estatus== null) { estatus= ""; }
        //String anio_calculo           = request.getParameter("ic_anio_calculo");      if (anio_calculo== null) { anio_calculo= ""; }
	String nomEPO									= request.getParameter("nomEpo");              	if (nomEPO== null) { nomEPO= ""; }
	String monto									= request.getParameter("montoPublicacion");     if (monto== null) { monto= ""; }
	CreaArchivo archivo           = new CreaArchivo();
	String contenidoArchivo       = "";
	BufferedReader	brt            =  null;
	String nombreArchivo          = null;
	boolean mostrarParametros     = false;
	int start=0, limit=0;
	JSONObject resultado 	      = new JSONObject();
	String informacion            =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String operacion              =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
	String infoRegresar           = "";
		
	
	//Validaciones
	
	if(informacion.equals("catalogoEPODist")){
	
		CatalogoIFEpoProveedores cat = new CatalogoIFEpoProveedores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setUsuario(iNoCliente);
		cat.setValorInicial(true);
		cat.setDescripcionAdicional("TODAS LAS EPOS's");
		cat.setClaveAdicional("0");
			
		infoRegresar = cat.getJSONElementos();
	  	
	} else	if (informacion.equals("catologoMonedaDist")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	} else	if (informacion.equals("obtenFecha")) {
		IFPublicacionProveedores paginadorProv = new IFPublicacionProveedores();
		paginadorProv.setUsuario(usuario);
		paginadorProv.setIc_moneda(moneda);
		paginadorProv.setIc_epo(epo);
		
		String montoPublicacionVigente="";
		AccesoDB conn = new AccesoDB();					
		try {
                        conn.conexionDB();
                        Registros registros = conn.consultarDB(paginadorProv.getMontoPublicacionVigente(),paginadorProv.getConditions());
                        registros.next();
                        montoPublicacionVigente = registros.getString("publicacion_vigente");
                        montoPublicacionVigente = "$ "+Comunes.formatoDecimal(montoPublicacionVigente,2);
                
                }catch(Exception e){
                                e.printStackTrace();
                                montoPublicacionVigente = "¡ERROR!";
                }finally{
                    if(conn.hayConexionAbierta()){
                    conn.cierraConexionDB();
                                                }
                }
		JSONObject jsonObj = new JSONObject();
		//
		Parametro parametro = ServiceLocator.getInstance().lookup("ParametroEJB",Parametro.class);
 
		// Obtener la Fecha de Ejecucion del Proceso de llenado --
		String dia ="", mes ="", anio="", hora =""; 
		List fechas = parametro.FecchaEjecucionProceso(); 
		if(fechas.size()>0) { 
			dia 	= (String)fechas.get(0); 
			mes 	= (String)fechas.get(1); 
			anio 	= (String)fechas.get(2); 
			hora 	= (String)fechas.get(3); 
		} 
		String fecha="\"La información fue actualizada por última vez el día "+dia+" de "+mes+" de "+anio+" a las "+hora+"\"";
		jsonObj.put("textoFecha", fecha);
		
		jsonObj.put("montoPublicacion",montoPublicacionVigente);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("Consulta")|| informacion.equals("ArchivoCSV")|| informacion.equals("ArchivoPDF")){
	
		String consulta="";
		//if(epo.equals("0")){
			
			IFReporteEPOs paginador = new IFReporteEPOs();
			//  Se copian las propiedades al bean del Paginador	
			paginador.setUsuario(usuario);
			paginador.setIc_moneda(moneda);
                        paginador.setCs_vobo_if(estatus);
                        //paginador.setAnio_calculo(anio_calculo);
                        paginador.setEpo(epo);
				
			// Realizar consulta
		CQueryHelperRegExtJS cqh = new CQueryHelperRegExtJS(paginador);
				try{
				start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));	
			}catch(Exception e){
					System.out.println("Error en parametros");
					
			}
				
			//Primer consulta
				
			 try {
				if(informacion.equals("Consulta")) {
						
					if (operacion.equals("Generar")) {	//Nueva consulta
						cqh.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
					}
					String  consultar = cqh.getJSONPageResultSet(request,start,limit);	
					resultado = JSONObject.fromObject(consultar);
					infoRegresar=resultado.toString();
				}
				else 	if (informacion.equals("ArchivoCSV")) 
				{
					 try {
						
						String nomArchivo = cqh.getCreateCustomFile(request, strDirectorioTemp,"CSV");
						JSONObject jsonObj = new JSONObject();
						jsonObj.put("success", new Boolean(true));
						jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
						infoRegresar = jsonObj.toString();
						} catch(Throwable e) {
						 throw new AppException("Error al generar el archivo CSV", e);
						}
				}
				else if (informacion.equals("ArchivoPDF")) 
				{
					try 
					{
						String nomArchivo = cqh.getCreatePageCustomFile(request,start,limit, strDirectorioTemp,"PDF");
						JSONObject jsonObj = new JSONObject();
						jsonObj.put("success", new Boolean(true));
						jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
						infoRegresar = jsonObj.toString();
					}
					catch(Throwable e) {	 throw new AppException("Error al generar el archivo PDF", e);		}
				} 
										
										
										
										
				} catch(Exception e) {
					throw new AppException("Error en la paginacion", e);
				}
			/*}else{
                                        IFReporteEPOs paginador = new IFReporteEPOs();
                                        //  Se copian las propiedades al bean del Paginador	
                                        paginador.setUsuario(usuario);
                                        paginador.setIc_moneda(moneda);
                                        paginador.setCs_vobo_if(estatus);
                                        paginador.setAnio_calculo(anio_calculo);
                                        paginador.setEpo(epo);


					IFPublicacionProveedores paginadorProv = new IFPublicacionProveedores();
					paginadorProv.setUsuario(usuario);
					paginadorProv.setIc_moneda(moneda);
					paginadorProv.setIc_epo(epo);
				
					CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginadorProv);
					
					try{
							  start = Integer.parseInt(request.getParameter("start"));
								limit = Integer.parseInt(request.getParameter("limit"));	
						}catch(Exception e){
								System.out.println("Error en parametros");
								
							}
				
			//Primer consulta
				
			 try {
					if(informacion.equals("Consulta")) {
						
						if (operacion.equals("Generar")) {	//Nueva consulta
							cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
							
							
						}
						String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
						resultado = JSONObject.fromObject(consultar);
						
						infoRegresar=resultado.toString();
					} else if (informacion.equals("ArchivoCSV")) {
										 try {
										  
											paginadorProv.setNomEpo(nomEPO);
											paginadorProv.setMontoPublicacion(monto);
											String nomArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
											JSONObject jsonObj = new JSONObject();
											jsonObj.put("success", new Boolean(true));
											jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
											infoRegresar = jsonObj.toString();
											} catch(Throwable e) {
											 throw new AppException("Error al generar el archivo CSV", e);
											}
										}else if (informacion.equals("ArchivoPDF")) {
										 try {
											paginadorProv.setNomEpo(nomEPO);
											paginadorProv.setMontoPublicacion(monto);
											String nomArchivo = cqhelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp,"PDF");
											JSONObject jsonObj = new JSONObject();
											jsonObj.put("success", new Boolean(true));
											jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
											infoRegresar = jsonObj.toString();
											} catch(Throwable e) {
											 throw new AppException("Error al generar el archivo PDF", e);
											}
										}
				} catch(Exception e) {
					throw new AppException("Error al generar archivo", e);
				}
			
			
			
			}*/
						
		}
			
			
%>

<%=infoRegresar%>
