Ext.onReady(function() {

//------------------------------------------------------------------------------
/**
var procesarCatalogoInter = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('id_intermediario').setValue('1');
		}
	}
*/	
/** 01(INI). Modificado: ABR. Fecha: 07/03/2014. */
	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);
	
  	var procesarCatalogoInter = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var id_intermediario = Ext.getCmp('id_intermediario');
				Ext.getCmp('id_intermediario').setValue("");
				if(id_intermediario.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_intermediario.setValue("");			
				}	
			}
		}
  };
  function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		}else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} 	else if (pagina == 2) { 	// Distribuidor						
			window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 		 
		} else if (pagina == 3) { 	//Proveedor
			window.location = '15forma01Ext.jsp'; 
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} else if (pagina == 6) { 	//Contragarante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 	
		} else if (pagina == 9) { 	// Mandante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
		} else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		}	else if (pagina == 11) { 	//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 	
		}
	}
 	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [[/*'EPO'*/  '0' ,'Cadena Productiva']
				,[/*'IF'*/	'1' ,'Intermediario Financiero']
				,[ 			'12','Distribuidor de fondos']
				,[/*'2'*/	'2' ,'Distribuidor']
				,[/*'1'*/	'3' ,'Proveedor']	
				,[/*'ACE'*/	'4' ,'Afiliados Cr�dito Electr�nico']
				//,[/*'EPOI'*/'5','Cadena Productiva Internacional']
				//,[/*'CONTRA'*/'6','Contragarante']
				//,[/*'PI'*/	'7','Proveedor Internacional']	
				//,[/*'EDOC'*/'8','Cliente Estado de Cuenta']	
				//,[/*'M'*/	'9','Mandante']	
				//,[/*'AF'*/	'10','Afianzadora']
				//,[/*'UNI'*/ '11','Universidad-Programa Cr�dito Educativo']
					
				]
	}); 
/** 01(FIN) */ 
  
var procesarConsultaData = function(store,arrRegistros,opts){
		if(arrRegistros!=null){
			gridAfiliado.show();
			var el = gridAfiliado.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
var resultProcesoHand =  function(response) {
		
		if (Ext.util.JSON.decode(response.responseText).success == true) {
			var resp= Ext.util.JSON.decode(response.responseText);
			
			fpCarga.hide();
			Ext.getCmp('tabRegResult1').hide();
			
			storeAfiliadosCE.loadData(resp);
			Ext.getCmp('btnArchivo').setHandler(function(btn)
			{
				var archivo = resp.urlArchivo;				
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};

				fpCarga.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fpCarga.getForm().getEl().dom.submit();
			});

		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//------------------------------------------------------------------------------

var storeCatIntermData = new Ext.data.JsonStore({
	   id				: 'storeCatIntermData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15aext.data.jsp',
		baseParams	: { informacion: 'catalogoIntermediario'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoInter,
			exception: NE.util.mostrarDataProxyError
		}
	});

var storeAfiliadosCE = new Ext.data.JsonStore({
	root: 'registros',
	//url: '15forma15aext.data.jsp',
	fields: [
				{name: 'NUMSIRAC'},
				{name: 'NOMBRECOMPLETO'},
				{name: 'RFC'},
				{name: 'TIPOPERSONA'},
				{name: 'SECTORECO'},
				{name: 'SUBSECTOR'},
				{name: 'RAMA'},
				{name: 'CLASE'},
				{name: 'ESTRATO'},
				{name: 'NUMFOLIO'},
				{name: 'NUMTROYA'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
						procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

//------------------------------------------------------------------------------


elementosFormCarga = [
				{
					xtype: 'compositefield',
					msgTarget: 'side',
					combineErrors: false,
					items: [
						{
							xtype: 'displayfield',
							width: 100
						},{
							xtype			: 'radiogroup',
							msgTarget	: 'side',
							id				: 'id_fp_dir',
							name			: 'fp_dir',
							hiddenName	: 'fp_sdir',
							cls			: 'x-check-group-alt',
							columns		: [100, 100],
							valueField	:'I',
							items			: [
								{boxLabel	: 'Individual', name : 'fp_dir', inputValue: 'I', handler: function() { window.location  = '15forma15ext.jsp';}},
								{boxLabel	: 'Masiva',	name: 'fp_dir', inputValue: 'M', checked : true }
							]
						}	
					]
				},{
					xtype				: 'combo',
					id					: 'id_afiliacion',
					name				: 'cmbAfiliacion',
					hiddenName 		:'cmbAfiliacion', 
					fieldLabel		: 'Afiliaci�n',
					width				: 250,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',
					value				: '4',
					store				: storeAfiliacion,
					listeners: {
						select: {
							fn: function(combo) {
								var valor  = combo.getValue();
								creditoElec(valor);		
							}
						}
					}
					
				},{
			xtype				: 'combo',
			id					: 'id_intermediario',
			name				: 'cmbIntermediario',
			hiddenName 		: 'cmbIntermediario',
			fieldLabel		: 'Intermediario',
			width				: 400,
			forceSelection	: true,
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'clave',
			displayField	: 'descripcion',
			store				: storeCatIntermData,
			anchor			: '90%'
		},
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				/*{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){
						var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
						
						if (!gridLayout.isVisible()) {
							gridLayout.show();
						}else{
							gridLayout.hide();
						}
					
					}
				},*/{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .99,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 97,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side'
						//anchor: '-10'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo1',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									width: 312,
									//emptyText: 'Ruta del Archivo',
									fieldLabel: 'Ruta del Archivo',
									name: 'Ruta del Archivo',
									buttonText: 'Examinar...',
									buttonCfg: {
									  //iconCls: 'upload-icon'
									},
									anchor: '90%',
									regex: /^.*\.(txt|TXT)$/,
									regexText:'el archivo debe tener formato TXT'
								},
								{
									xtype: 'hidden',
									id:	'hidExtension',
									name:	'hidExtension',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'TipoPYME',
									name:	'TipoPYME',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'NoIf',
									name:	'NoIf',
									value:	''
								},
								{
									xtype: 	'button',
									text: 	'Enviar',
									id: 		'btnContinuar',
									iconCls: 'icoContinuar',
									style: { 
										  marginBottom:  '10px' 
									},
									handler: function(){
										var cargaArchivo = Ext.getCmp('archivo');
										if (!cargaArchivo.isValid()){
											cargaArchivo.focus();
											return;
										}else if (cargaArchivo.getValue()==''){
											Ext.Msg.alert("Aviso", "Seleccione un archivo");
											return;
										}else if (Ext.getCmp('id_intermediario').getValue()==''){
											Ext.Msg.alert("Aviso", "Debe seleccionar un intermediario");
											return;
										}
										
										var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
										var extArchivo = Ext.getCmp('hidExtension');
										var NoIf = Ext.getCmp('NoIf').setValue(Ext.getCmp('id_intermediario').getValue());
										var TipoPYME = Ext.getCmp('TipoPYME').setValue('1');
										var objMessage = Ext.getCmp('pnlMsgValid1');
										
										
										objMessage.hide();
										//numTotal.setValue(Ext.getCmp('numtotalvenc1').getValue());
										//totalMonto.setValue(Ext.getCmp('montototalvenc1').getValue());
										
										if (/^.*\.(txt)$/.test(ifile)){
											extArchivo.setValue('txt');
										}
										
										fpCarga.getForm().submit({
											url: '15forma15afileext.jsp',
											waitMsg: 'Enviando datos...',
											success: function(form, action) {
												var resp = action.result;
												var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
												var objMsg = Ext.getCmp('pnlMsgValid1');

												
												if(!resp.flag){
													//SE MOSTRARAN LEYENDAS DE VALIDACIONES
													objMsg.show();
													//objMsg.body.update('El tama�o del archivo ZIP es mayor al permitido');
													objMsg.body.update(resp.msgError);
												}else if(!resp.flagExt){
													objMsg.show();
													objMsg.body.update('El archivo contenido dentro del ZIP no es un archivo TXT');
												}else if(!resp.flagReg){
													objMsg.show();
													objMsg.body.update('El archivo TXT no puede contener mas de 30000 registros');
												}else{
													
													if(!resp.bTodoOK){
														Ext.getCmp('vRegErrores').setValue(resp.hAfiliaCE.sConError);
														Ext.getCmp('vRegCorrectos').setValue(resp.hAfiliaCE.sSinError);
														Ext.getCmp('totRegSiracBien').setValue(resp.hAfiliaCE.iTotalRegBien);
														Ext.getCmp('totRegSiracMal').setValue(resp.hAfiliaCE.iTotalRegMal);
														Ext.getCmp('totRegTroyaBien').setValue(resp.hAfiliaCE.iTotRegBienTroya);
														Ext.getCmp('totRegTroyaMal').setValue(resp.hAfiliaCE.iTotRegMalTroya);
														Ext.getCmp('tabRegResult1').show();
														gridAfiliado.hide();
														
														if(resp.bSinError){
															Ext.getCmp('btnProcesar').show();
															Ext.getCmp('btnProcesar').setHandler(function(){
																Ext.Ajax.request({  
																	url: '15forma15aext.data.jsp',
																	success: resultProcesoHand,
																	params: {
																		informacion: 'obtieneAfiliadosCE',
																		sArchivoErrores: resp.sArchivoErrores,
																		sPymesOK: resp.sPymesOK,
																		NoIf: resp.NoIf
																	} 
																});
															
															})
															
														}
													}else{
														fpCarga.hide();
														Ext.getCmp('tabRegResult1').hide();
														storeAfiliadosCE.loadData(resp);
														Ext.getCmp('btnArchivo').setHandler(function(btn)
														{
															var archivo = resp.urlArchivo;				
															archivo = archivo.replace('/nafin','');
															var params = {nombreArchivo: archivo};
										
															fpCarga.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
															fpCarga.getForm().getEl().dom.submit();
														});
													}
												
												}

											},
											failure: NE.util.mostrarSubmitError
										})	 										
									}
								},{
									xtype: 	'button',
									text: 	'Cancelar',
									id: 		'btnCancel',
									iconCls: 'icoCancelar',
									style: { 
										  marginBottom:  '10px' 
									},
									handler: function(){
										window.location = '15forma15ext.jsp';
									}
								}
							]
						}
							
					]
				},
				{
					xtype: 'panel',
					name: 'pnlMsgValid',
					id: 'pnlMsgValid1',
					width: 500,
					style: 'margin:0 auto;',
					frame: false,
					hidden: true
				
				}
			]
		},
		{
			xtype: 'panel',
			name: 'pnlMsgFormato',
			id: 'pnlMsgFormato',
			width: 500,
			style: 'margin:0 auto;',
			frame: false,
			html : '<table width="300" border="0" align="center" bgcolor="#CAC6C6"><tr><td align="center">El archivo debe tener formato TXT</td></tr></table>'
		
		}
		//barraProgreso
	];

//---------------------------------------------------------------------------

var gridAfiliado = new Ext.grid.GridPanel({
		id: 'gridAfiliado',
		store: storeAfiliadosCE,
		margins: '20 0 0 0',
		hidden : true,
		columns: [
			{
				header : 'N�mero de Cliente SIRAC',
				dataIndex : 'NUMSIRAC',
				width : 150,
				sortable : true
			},
			{
				header : 'Nombre Completo o Raz�n Social',
				dataIndex : 'NOMBRECOMPLETO',
				width : 150,
				sortable : true
			},
			{
				header : 'R.F.C.',
				dataIndex : 'RFC',
				width : 150,
				sortable : true
			},
			{
				header : 'Tipo Persona',
				dataIndex : 'TIPOPERSONA',
				width : 150,
				sortable : true
			},
			{
				header : 'Sector Econ�mico',
				dataIndex : 'SECTORECO',
				width : 150,
				sortable : true
			},
			{
				header : 'Subsector',
				dataIndex : 'SUBSECTOR',
				width : 150,
				sortable : true
			},
			{
				header : 'Rama',
				dataIndex : 'RAMA',
				width : 150,
				sortable : true
			},
			{
				header : 'Clase',
				dataIndex : 'CLASE',
				width : 150,
				sortable : true
			},
			{
				header : 'Estrato',
				dataIndex : 'ESTRATO',
				width : 150,
				sortable : true
			},
			{
				header : 'N�mero de Folio',
				dataIndex : 'NUMFOLIO',
				width : 150,
				sortable : true
			},
			{
				header : 'N�mero de Cliente Troya',
				dataIndex : 'NUMTROYA',
				width : 150,
				sortable : true
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 800,
		height: 200,
		style: 'margin:0 auto;',
		//autoHeight : true,
		title: '<p align="center">PYMES Afiliadas a Cr&eacute;dito Electr&oacute;nico</p>',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Generar Archivo',
					id: 'btnArchivo'
				},
				'-',
				{
				text: 'Terminar',
				id: 'btnTerminar',
				handler: function(){ window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15aext.jsp'; }
				}
			]
		}
	});

var tabRegCorrectos = new Ext.Panel({
		title:'<center>Pymes sin Errores</center>',
		frame: true,
		id:'tabRegCorrectos1',
		height: 310,
		//width: 300,
		//autoHeight : true,
		items:[
			{
				xtype: 'textarea',
				id: 'vRegCorrectos',
				width: 395,
				height: 290
			}
		]
		
	});

var tabRegErrores = new Ext.Panel({
		title:'<center>Pymes con Errores</center>',
		frame: true,
		id:'tabRegErrores1',
		height: 310,
		//width: 300,
		//autoScroll: true,
		items:[
			{
				xtype: 'textarea',
				id: 'vRegErrores',
				width: 395,
				height: 290
			}
		]
	});

var pnlAcumulado = new Ext.Panel({
		title:'',
		frame: true,
		id:'pnlAcumulado',
		height: 100,
		width: 820,
		colspan: 2,
		layout: {
			 type: 'table',
			 columns: 2
		},
		style: 'margin:0 auto;',
		//autoScroll: true,
		items:[
			{xtype:'panel',
				layout: 'form',
				labelWidth: 230,
				width: 410,
				items:[{
					xtype:'textfield',
					name: 'totRegSiracBien',
					id: 'totRegSiracBien',
					fieldLabel: 'Total de Registros Cargados',
					readOnly: true
				}]
			},
			{xtype:'panel',
				layout: 'form',
				labelWidth: 230,
				items:[{
					xtype:'textfield',
					name: 'totRegSiracMal',
					id: 'totRegSiracMal',
					readOnly: true,
					fieldLabel: 'Total de Registros Rechazados'
				}]
			},
			{xtype:'panel',
				layout: 'form',
				labelWidth: 230,
				width: 410,
				items:[{
					xtype:'textfield',
					name: 'totRegTroyaBien',
					id: 'totRegTroyaBien',
					fieldLabel: 'Total de Registros Cargados en Troya',
					readOnly: true
				}]
			},
			{xtype:'panel',
				layout: 'form',
				labelWidth: 230,
				items:[{
					xtype:'textfield',
					name: 'totRegTroyaMal',
					id: 'totRegTroyaMal',
					readOnly: true,
					fieldLabel: 'Total de Registros Rechazados en Troya'
				}]
			}
		]
	});

var tabRegResult = new Ext.Panel({
		title:'',
		frame: true,
		hidden: true,
		id:'tabRegResult1',
		height: 500,
		width: 825,
		layout: {
			 type: 'table',
			 columns: 2
		},
		style: 'margin:0 auto;',
		items:[
			tabRegCorrectos,
			tabRegErrores,
			pnlAcumulado
		]/**,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				{
				text: 'Procesar',
				id: 'btnProcesar',
				hidden: false
				}
			]
		}*/
	});

 var fpCarga = new Ext.form.FormPanel({
		id: 'formaCarga',
		width: 600,
		title: 'Carga de Archivo',
		frame: true,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px; text-align:left;',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormCarga,		
		monitorValid: true
	});
	
	var fpArchivo = new Ext.form.FormPanel({
		id: 'fpArchivo1',
		width: 600,
		title: '',
		frame: true,
		hidden: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;'
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fpArchivo,
			NE.util.getEspaciador(10),
			fpCarga,
			NE.util.getEspaciador(20),
			tabRegResult,
			gridAfiliado
			//gridLayout
		]
	});
	
	storeCatIntermData.load();
	
});