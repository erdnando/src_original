Ext.onReady(function(){
	
	var ObjCatalogo = {
		cat1:false,
		cat2:false,
		cat3:false,
		cat4:false,
		cat5:false,
		cat6:false,
		cat7:false,
		cat8:false,
		cat9:false,
		cat10:false
	}
	
	var sTipoUsuario =  Ext.getDom('sTipoUsuario').value;	
	var iTipoPerfil  =  Ext.getDom('iTipoPerfil').value;	
	var idMenu  =  Ext.getDom('idMenu').value;	
	var permisosGlobales;
	var ic_if = window.location.search 
                 ? Ext.urlDecode(window.location.search.substring(1)).ic_if : '1'; //: ''; cambiar!!!
   
	var txtaction = window.location.search 
   ? Ext.urlDecode(window.location.search.substring(1)).txtaction  : 'mod'; //: ''; 
   
	var primera_vez = true;		
	var primera_vez_estado = true, primera_vez_municipio = true, primera_vez_desc1 = true, primera_vez_desc2 = true;
	var pais, estado, municipio, desc1, desc2;
	var solicitudesDescontante1, solicitudesDescontante2;	
	var total;
	var RFC_IF, IF_SIAG;
	
	
	function procesaValoresIniciales(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var  info = Ext.util.JSON.decode(response.responseText);			
			permisosGlobales=info.permisos;	
				
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	function procesarPermiso(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var  info = Ext.util.JSON.decode(response.responseText);			
			 
			if(info.ccPermiso=='BTNACEPTARIN'){  	enviar(); 		}
			if(info.ccPermiso=='BTNCARGARIMAGENIN'){  	existeImagen(); 		}
			
			if(info.ccPermiso=='BTNCANCELARIN'){  	
				Ext.Ajax.request({
					url: '15forma09Ext.data.jsp',
					params: Ext.apply({
						informacion: 'ConsultaIF',
						ic_if : ic_if ,
						idMenuP:idMenu
					}),				
						callback: procesarSuccessFailureConsultaIF
					});
					
					consultaData.load({	params : Ext.apply({	informacion: 'ConsultaProducto',	ic_if: ic_if, if_siag: Ext.getCmp('id_if_siag').getValue(), idMenuP:idMenu  })	});
			}
			
				
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function validaPermisos(ccPermiso) {	
		Ext.Ajax.request({
			url: '15forma09Ext.data.jsp', 
			params: {
				informacion: "validaPermiso", 
				idMenuP:idMenu,
				ccPermiso:ccPermiso					
			},
			callback: procesarPermiso
		});
	}
	
/*--------------------------------- Handler's -------------------------------*/
		//las peticiones ajax son asincronas por lo tanto es necesario validar primero la carga total de los catalogo antes de escoger un valor
	var validaCatalogos = function(){
		if(NE.util.allTrue(ObjCatalogo) && primera_vez == true){
			primera_vez = false;
			//ajax
			Ext.Ajax.request({
				url: '15forma09Ext.data.jsp',
				params: Ext.apply({
					informacion: 'ConsultaIF',
					ic_if : ic_if,
					idMenuP:idMenu
				}),				
				callback: procesarSuccessFailureConsultaIF
			});
			//consultaData.load({	params : Ext.apply({	informacion: 'ConsultaProducto',	ic_if: ic_if })	});
		}
	}
	
	var procesarCatalogoPais = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat1=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoDomicilio = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat2=true;
			validaCatalogos();
		}
	}	
	var procesarCatalogoNumeroIF = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat3=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoAvales = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat4=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoTipoRiesgo = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat5=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoViabilidad = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat6=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoOficina_controladora = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat7=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoClaveIfSIAG = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat8=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoClaveSUCRE = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat9=true;
			validaCatalogos();
		}
	}
	
	var procesarCatalogoDescontante = function(store, arrRegistros, opts) {
		if (primera_vez == true){ //aun no hace la peticion de llenado
			if(store.getTotalCount()>0) {
				ObjCatalogo.cat10=true;		
				validaCatalogos();
			}
		}
		else {
			if(primera_vez_desc1 == true)	{
				primera_vez_desc1 = false;
				Ext.getCmp('id_descontanteIf1').setValue(desc1);
				if (desc1 != null && desc1 != '') {
					Ext.Ajax.request({ url: '15forma09Ext.data.jsp', params: Ext.apply({	informacion: 'SolicitudesDesc',	ic_if: ic_if, descontanteIf1: desc1	}),				
						callback: procesarSuccessFailureSolicitudesDesc1
					});
				}
			}
			if(primera_vez_desc2 == true)	{
				primera_vez_desc2 = false;
				Ext.getCmp('id_descontanteIf2').setValue(desc2);
				if (desc2 != null && desc2 != '') {
					Ext.Ajax.request({ url: '15forma09Ext.data.jsp', params: Ext.apply({	informacion: 'SolicitudesDesc',	ic_if: ic_if, descontanteIf1: desc2	}),				
						callback: procesarSuccessFailureSolicitudesDesc2
					});
				}
			}
		}
	}	
	
	var procesarCatalogoEstado = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			if(primera_vez_estado == true) {
				Ext.getCmp('id_cmb_estado').setValue(estado);
			}
		}
	}
	var procesarCatalogoMunicipio = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			if (primera_vez_municipio == true) {
				Ext.getCmp('id_cmb_delegacion').setValue(municipio);
			}
		}
	}
	var procesarSuccessFailureConsultaIF = function(opts, success, response) 
   {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			var reg = Ext.util.JSON.decode(response.responseText).registros[0];
			if (reg != null) 
			{	
				Ext.getCmp('Razon_Social').setValue(reg.Razon_social);
				Ext.getCmp('nombre_comercial').setValue(reg.Nombre_comercial);
				Ext.getCmp('rfc').setValue(reg.R_F_C);
				Ext.getCmp('calle').setValue(reg.Calle);
				Ext.getCmp('colonia').setValue(reg.Colonia);
				Ext.getCmp('id_cmb_delegacion').setValue(reg.Delegacion_o_municipio);
				Ext.getCmp('id_cmb_estado').setValue(reg.Estado);
				Ext.getCmp('code').setValue(reg.Codigo_postal);
				Ext.getCmp('id_cmb_pais').setValue(reg.Pais);				
				Ext.getCmp('telefono').setValue(reg.Telefono);
				Ext.getCmp('fax').setValue(reg.Fax);
				Ext.getCmp('id_email').setValue(reg.Email);
				
				
				Ext.getCmp('id_Domicilio_correspondencia').setValue(reg.Domicilio_correspondencia);
				Ext.getCmp('id_Ejecutivo_de_cuenta').setValue(reg.Ejecutivo_de_cuenta);
				Ext.getCmp('id_Numero_de_IF').setValue(reg.Numero_de_IF);
				Ext.getCmp('idnumCliSirac').setValue(reg.num_clie_sirac);
				Ext.getCmp('id_Tipo_de_IF').setValue(reg.Tipo_de_IF);
				Ext.getCmp('id_Avales').setValue(reg.Avales);
				Ext.getCmp('Limite_maximo_endeudamiento').setValue(reg.Limite_maximo_endeudamiento);
				Ext.getCmp('Facultades').setValue(reg.Facultades);
				Ext.getCmp('Porcentaje_capital_contable').setValue(reg.Porcentaje_capital_contable);
				Ext.getCmp('Ente_contable').setValue(reg.Ente_contable);
				Ext.getCmp('id_Oficina_controladora').setValue(reg.Oficina_controladora);
				Ext.getCmp('id_Viabilidad').setValue(reg.Viabilidad);
			
				Ext.getCmp('id_domicilio').setValue(reg.ic_domicilio);
				Ext.getCmp('Fecha_convenio').setValue(reg.Fecha_convenio);
				Ext.getCmp('Fecha_convenio_anticipo').setValue(reg.Fecha_convenio_anticipo);
				
				Ext.getCmp('id_Tipo_de_IF_piso').setValue(reg.Tipo_de_IF_piso);
				Ext.getCmp('nombre_tabla').setValue(reg.nombre_tabla);
				Ext.getCmp('id_tipo_riesgo').setValue(reg.tipo_riesgo);
				Ext.getCmp('id_if_siag').setValue(reg.if_siag);
				
				Ext.getCmp('id_if_sucre').setValue(reg.if_sucre);
				Ext.getCmp('id_descontanteIf1').setValue(reg.descontanteIf1);
				Ext.getCmp('id_descontanteIf2').setValue(reg.descontanteIf2);
                Ext.getCmp('id_tipoEpoInstruccion').setValue(reg.tipoEpoInstruccion);
                
                Ext.getCmp('id_OperaMontosMenores').setValue(reg.id_OperaMontosMenores);

				total = reg.Total;
				/* Checar en true o false los checks dependiendo de su valor */
				var check = reg.convenio_unico;
				if(check != null && check == 'S'){	
					Ext.getCmp('id_convenio_unico').setValue(true); 
				}else {
					Ext.getCmp('id_convenio_unico').setValue(false); 
				}
				
				var mandato = reg.mandato_documento;
				if(mandato != null && mandato == 'S'){	
					Ext.getCmp('id_mandato_documento').setValue(true); 
				}else {
					Ext.getCmp('id_mandato_documento').setValue(false); 
				}
				
				var autorizacion = reg.autorizauto_opersf;
				//alert();
				if(autorizacion != null && autorizacion == 'S'){	
					Ext.getCmp('id_autorizauto_opersf').setValue(true); 
				}else {
					Ext.getCmp('id_autorizauto_opersf').setValue(false); 
				}
				if (!mandato){
					Ext.getCmp('id_descontanteIf1').setDisabled(true);
					Ext.getCmp('id_descontanteIf2').setDisabled(true);
					Ext.getCmp('id_tipoEpoInstruccion').setDisabled(true);
				}
				var tabla = reg.nombre_tabla;
				if (tabla != null && tabla != ''){	
					Ext.getCmp('id_enlace_automatico').setValue(true); 
				}else {
					Ext.getCmp('id_enlace_automatico').setValue(false); 
				}
				
				var fideicomiso = reg.fideicomiso;
				if(fideicomiso != null && fideicomiso == 'S'){	
					Ext.getCmp('id_fideicomiso').setValue(true); 
				}else {
					Ext.getCmp('id_fideicomiso').setValue(false); 
				}
				
				var chkFactDistribuido = reg.chkFactDistribuido;
				
				if(chkFactDistribuido !== null && chkFactDistribuido === 'S'){	
					Ext.getCmp('chkFactDistribuido1').setValue('on'); 
				}else {
					Ext.getCmp('chkFactDistribuido1').setValue(''); 
				}
				
				
				/* Guardo la informacion para llenarla en los combos */
				pais = reg.Pais;
				estado 	 = reg.Estado;
				municipio = reg.Delegacion_o_municipio;
				desc1 = reg.descontanteIf1;
				desc2 = reg.descontanteIf2;
				RFC_IF = reg.R_F_C;	
				//IF_SIAG=reg.if_siag;
				/* Carga de combos */
				catalogoEstado.load({
								params : Ext.apply({
									pais : pais
								})
							});
				catalogoMunicipio.load({
								params : Ext.apply({
									pais : pais,
									estado : estado
								})
							});
				
				consultaData.load({	params : Ext.apply({	informacion: 'ConsultaProducto',	ic_if: ic_if, if_siag: reg.if_siag, idMenuP:idMenu  })	});
				
				var  info = Ext.util.JSON.decode(response.responseText);	
				
				var grid = Ext.getCmp('grid');
				var cm = grid.getColumnModel();
				var total = permisosGlobales.length; 
				if(total>0){
					for(i=0;i<permisosGlobales.length;i++){ 
						boton=  permisosGlobales[i];
						if(boton!="LINEA_FONDEO"  &&  boton!="CL_MODIFICA"  ) {	
							Ext.getCmp(boton).show();
						}			
					}	
				}
			//F021-2015 (E)
			}
		}
	}
	
	var procesarSuccessFailureSolicitudesDesc1  = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			var solicitud = Ext.util.JSON.decode(response.responseText).solicitudes;
			if (solicitud != null && solicitud != "") 
				solicitudesDescontante1 = solicitud;
		}
	}
	
	var procesarSuccessFailureSolicitudesDesc2  = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			var solicitud = Ext.util.JSON.decode(response.responseText).solicitudes;
			if (solicitud != null && solicitud != "") 
				solicitudesDescontante2 = solicitud;
		}
	}
	
	var procesarSuccessFailureModificaIF = function(opts, success, response) {
		var cmpForma = Ext.getCmp('formaAfiliacion');
		cmpForma.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('',Ext.util.JSON.decode(response.responseText).msg, function() {
				window.location = '15forma5ext.jsp?idMenu='+idMenu;  
			}, this);	
			
		}
		else {
			Ext.Msg.alert('','Ocurrio un error durante la actualizacion de datos.');	
		}
	}
	
	var procesarExisteImagen = function(opts, success, response) {
		var ventana = Ext.getCmp('winCargaArchivo');
		if (ventana) {
			fpcargaArchivo.getForm().reset();
			ventana.show(); 
		} 
		else 
		{
			new Ext.Window({
					title			: 'Nafin@net Documentos',
					id				: 'winCargaArchivo',
					layout		: 'fit',
					heigth		:	100,										
					width			:	500,
					minWidth		:	400,
					minHeight	: 	100,
					//buttonAlign	: 'center',
					modal			: true,
					closeAction	: 'hide',
					items			: [fpcargaArchivo]
				}).show();   				
		}
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var formaCarga = Ext.getCmp('fcargaArchivo');
		contenedorPrincipalCmp.add(fpcargaArchivo);
	
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var existe = Ext.util.JSON.decode(response.responseText).existe;
			if (existe == 0) {
				Ext.getCmp('R_F_C').setValue(RFC_IF);
				Ext.getCmp('R_F_C').setVisible(true);
				Ext.getCmp('Imagen_ic_if').setVisible(false);
			}
			else {
				Ext.getCmp('R_F_C').setVisible(false);
				Ext.getCmp('Imagen_ic_if').setVisible(true);}
		}
	}
	

	var procesarSuccessFailureActualizaFecha  = function(opts, success, response) {
		var cmpForma = Ext.getCmp('fModProducto');
		cmpForma.el.unmask();
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('',Ext.util.JSON.decode(response.responseText).msg, function() {
				Ext.getCmp('winModProd').hide();
			}, this);	
			consultaData.load({	params : Ext.apply({	informacion: 'ConsultaProducto',	ic_if: ic_if, if_siag: Ext.getCmp('id_if_siag').getValue(), idMenuP:idMenu  })	});
		}
	
		else 
			Ext.Msg.alert('','Ocurrio un error durante la actualizacion de datos.');	
	}
	
	var procesarSuccessFailureLineaFondeo = function (opts, success, response) {
		var cmpForma = Ext.getCmp('fDetalleReporte');
		cmpForma.el.unmask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			if(8 == iTipoPerfil){
				Ext.getCmp('det_disponibilidad').disable(true);
			}else {
				Ext.getCmp('det_disponibilidad').enable(true);
			}
			
			var reg = Ext.util.JSON.decode(response.responseText).registros[0];
			if (reg != null) 
			{				
				var check = reg.chk_disponibilidad;
				if(check != null && check == 'S'){	
					Ext.getCmp('det_disponibilidad').setValue(true); 
				}else {
					Ext.getCmp('det_disponibilidad').setValue(false); 
				}
				Ext.getCmp('id_det_lineaF').setValue(reg.linea_fondeoG);
				Ext.getCmp('det_fchAct').setValue(reg.fch_actualizacion);
				Ext.getCmp('det_usuario').setValue(reg.usuario_mod);
				Ext.getCmp('det_montoIni').setValue(reg.monto_inicial);
				Ext.getCmp('det_montoDisp').setValue(reg.monto_disponible);				
			}	
			else {
				Ext.getCmp('btnAceptarDetalle').hide();
				Ext.getCmp('btnCancelarDetalle').hide();
				Ext.getCmp('btnActualizarDetalle').hide();
			}
			
		}
		else 
			Ext.Msg.alert('','Ocurrio un error durante la consulta de datos.');	
	}
	
	var procesarSuccessFailureDisponible = function (opts, success, response) {
		var cmpForma = Ext.getCmp('fDetalleReporte');
		cmpForma.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('',Ext.util.JSON.decode(response.responseText).msg, function() {
				Ext.getCmp('winDetalleReporte').hide();
			}, this);	
			consultaData.load({	params : Ext.apply({	informacion: 'ConsultaProducto',	ic_if: ic_if, if_siag: Ext.getCmp('id_if_siag').getValue(), idMenuP:idMenu  })	});
		}
		else 
			Ext.Msg.alert('','Ocurrio un error durante la actualizaci�n de datos.');	
	}
	
/*------------------------------- End Handler's -----------------------------*/
	function leeRespuesta(){
		window.location = '15forma09Ext.jsp';
	}
	function Guardar () {
		var cargaArchivo = Ext.getCmp('archivo');
		
		if(Ext.isEmpty(cargaArchivo.getValue())){
			cargaArchivo.markInvalid();
			cargaArchivo.focus();
			return;
		}
		Ext.getCmp('winCargaArchivo').hide();						
		fpcargaArchivo.getForm().submit({
			url: '15forma03_file.data.jsp',
			waitMsg: 'Enviando datos...',
			success: function(form, action) {
//tengo que recuperar path_destino

				var resp = action.result;
				Ext.getCmp('strArchivo').setValue(resp.archivo);
				
				Ext.getCmp('path_destino').setValue(resp.path_destino);
				Ext.MessageBox.alert('Mensaje de p�gina web',resp.msgError );
			},
			failure: NE.util.mostrarSubmitError
		})	
	}
	function existeImagen() {
		var cmpForma = Ext.getCmp('formaAfiliacion');
		var params = (cmpForma)?cmpForma.getForm().getValues():{};				
				
		Ext.Ajax.request({
			url: '15forma09Ext.data.jsp',
			params: Ext.apply(params, {informacion: 'ExisteImagen',ic_if: ic_if}),
			callback: procesarExisteImagen
		})
	}
	
	var ModificaProducto  = function(grid, rowIndex, colIndex, item, event)
	{
		var record = grid.getStore().getAt(rowIndex);
		
		var ic_producto 		= '';
		var nombre_producto  = '';
		var fch_convenio		= '';
		var dispersion			= '';
		if(record.get('ic_producto')!=''){
			ic_producto 		= record.get('ic_producto');
			nombre_producto   = record.get('nombre_producto');
			fch_convenio		= record.get('fechaConv');
			dispersion			= record.get('auxDisp');
		}
		
		var ventana = Ext.getCmp('winModProd');
		if (ventana) {
			fpModProducto.getForm().reset();
			ventana.show(); 
			//
		} 
		else 
		{
			new Ext.Window({
					title			: 'Modificar',
					id				: 'winModProd',
					layout		: 'fit',
					heigth		:	400,										
					width			:	450,
					minWidth		:	400,
					minHeight	: 	200,
					//buttonAlign	: 'center',
					modal			: true,
					closeAction	: 'hide',
					items			: [fpModProducto]
				}).show();   
				//
		}
		Ext.getCmp('ic_producto').setValue(ic_producto);
		Ext.getCmp('var_prod').setValue(nombre_producto);
		Ext.getCmp('var_fchC').setValue(fch_convenio);
		
		if (dispersion == 'NA') {
			Ext.getCmp('var_disp').setValue(dispersion);
			Ext.getCmp('var_disp').setVisible(true);
			Ext.getCmp('var_chkDisp').setVisible(false);	
		}
		else {
			if (dispersion == 'Si') 
				Ext.getCmp('var_chkDisp').setValue(true); 
			else if(dispersion == 'No') 				
				Ext.getCmp('var_chkDisp').setValue(false);						
			Ext.getCmp('var_disp').setVisible(false);
			Ext.getCmp('var_chkDisp').setVisible(true);			
		}
	}
	
	var detalleReporte  = function(grid, rowIndex, colIndex, item, event)
	{
		catalogoLineaFondeo.load({ params: Ext.apply({ Numero_de_IF : Ext.getCmp('id_Numero_de_IF').getValue()}) });
		
		var record = grid.getStore().getAt(rowIndex);
		
		var ic_producto_detalle 		= '';
		var nombre_producto_detalle   = '';
		
		if(record.get('ic_producto_detalle')!=''){
			ic_producto_detalle 			= record.get('ic_producto');
			nombre_producto_detalle   	= record.get('nombre_producto');
		}
		Ext.getCmp('btnAceptarDetalle').setVisible(true);
		Ext.getCmp('btnCancelarDetalle').setVisible(true);
		Ext.getCmp('btnActualizarDetalle').setVisible(true);
		
		var ventana = Ext.getCmp('winDetalleReporte');
		if (ventana) {
			fpDetalleReporte.getForm().reset();
			ventana.show(); 
		} 
		else 
		{
			new Ext.Window({
					title			: 'Nafi@net-Afiliacion',
					id				: 'winDetalleReporte',
					layout		: 'fit',
					heigth		:	400,										
					width			:	840,
					minWidth		:	400,
					minHeight	: 	200,
					//buttonAlign	: 'center',
					modal			: true,
					closeAction	: 'hide',
					items			: [fpDetalleReporte]
				}).show();   
				//
		}
			
		Ext.getCmp('ic_producto_detalle').setValue(ic_producto_detalle);
		Ext.getCmp('det_producto').setValue(nombre_producto_detalle);
		
		Ext.Ajax.request ({
			url:'15forma09Ext.data.jsp',
			params: Ext.apply ({ informacion: 'ObtieneDatosLineaFondeo',ic_producto_nafin:ic_producto_detalle,  ic_if:ic_if }),
			callback: procesarSuccessFailureLineaFondeo
		});
		
	}
	
	/*****************Validaciones***************/
	function enviar()
	{	
		var descontante1 = Ext.getCmp('id_descontanteIf1');
		var descontante2 = Ext.getCmp('id_descontanteIf2');
		var tipoEpoInst  = Ext.getCmp('id_tipoEpoInstruccion');
		var mandato = (Ext.getCmp('id_mandato_documento')).getValue();
	
		if (mandato) {
			//FODEA 012 - 2010
			if(!Ext.isEmpty(descontante1.getValue()) && !Ext.isEmpty(descontante2.getValue()) && descontante1.getValue() == descontante2.getValue()) {
				Ext.MessageBox.alert('Mensaje de p�gina web','Los descontantes seleccionados no pueden ser iguales.' );
				return;
			}
			//FODEA 033 - 2010 
			if (Ext.isEmpty(tipoEpoInst.getValue())) {
				Ext.MessageBox.alert('Mensaje dee p�gina web','Seleccione un Tipo de Cadena.' );
				tipoEpoInst.markInvalid('Seleccione un Tipo de Cadena.');
				return;
			}
		}
		
		Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
			if (botonConf == 'ok' || botonConf == 'yes') {
				var cmpForma = Ext.getCmp('formaAfiliacion');
				var params = (cmpForma)?cmpForma.getForm().getValues():{};				
				cmpForma.el.mask("Procesando", 'x-mask-loading');
				//peticion de Registro de Afiliaci�n
				Ext.Ajax.request({
						url: '15forma09Ext.data.jsp',
						params: Ext.apply(params,{ informacion: 'ModificaIF', ic_if: ic_if}),
						callback: procesarSuccessFailureModificaIF
				});
			}
		});		
	}
	var procesarRfcAyuda =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var rfc_sirac = resp.rfc;
			var win =new Ext.Window({
				modal			: false,
				resizable	: false,
				frame       : false,
				layout		: 'form',
				x				: 670,
				y				:550,
				width			: 250,
				id				: 'winAyuRFC',
				items			: [{
					xtype: 'panel',
					items:[{
							xtype: 'label',
							style:'margin:5px',
							html: 'El RFC correspondiente al n�mero de cliente SIRAC ingresado es <br> ( '+rfc_sirac+' )',
							listeners: {
								render: function(c){
									c.getEl().on({
										click: function(el){
										win.close();
										 if(rfc_sirac==''){
											Ext.getCmp('idnumCliSirac').setValue('');
											}
										},
										scope: c
									});
								 }
							}
						}
					]
				}]
			}).show();
		}
   }
/*****************************************************************************/
 
/*---------------------------------- Store's --------------------------------*/


	/// ***** ***** ***** ***** CATALOGOS ***** ***** ***** ***** ///
	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoPais,
			exception: NE.util.mostrarDataProxyError
		}
  });
  
  // catalogo Estado
	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
			 beforeload: NE.util.initMensajeCargaCombo,
				load:procesarCatalogoEstado,
			 exception: NE.util.mostrarDataProxyError
		}
	});
	
	//catalogo Municipio
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
			 beforeload: NE.util.initMensajeCargaCombo,
			 load:procesarCatalogoMunicipio,
			 exception: NE.util.mostrarDataProxyError
		}
	});
	//catalogo Domicilio correspondencia
	var catalogoDomicilio = new Ext.data.JsonStore({
	   id				: 'catDomicilio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoDomicilio'},
		autoLoad		: false,
		listeners	:
		{
			beforeload: NE.util.initMensajeCargaCombo,
			load:procesarCatalogoDomicilio,
			exception: NE.util.mostrarDataProxyError
		}
	});	
	
	//catalogo Ejecutivo de cuenta
	var catalogoEjecutivo = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['HVARGAS','HVARGAS'],
			['MCASTANE','MCASTANE'],
			['PMACEDO','PMACEDO']
		 ]
	}) ;
	
	//catalogo Numero IF
	var catalogoNumeroIF= new Ext.data.JsonStore
	({
		id				: 'catNumeroIF',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoNumeroIF'	},
		autoLoad		: false,
		listeners	:
		{
			beforeload: NE.util.initMensajeCargaCombo,
			load:procesarCatalogoNumeroIF,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	//catalogo Avales
	var catalogoAvales= new Ext.data.JsonStore
	({
		id				: 'catAvales',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoAvales'	},
		autoLoad		: false,
		listeners	:
		{
			beforeload: NE.util.initMensajeCargaCombo,
			load:procesarCatalogoAvales,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	//catalogo TipoRiesgo
	var catalogoTipoRiesgo= new Ext.data.JsonStore
	({
		id				: 'catTipoRiesgo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoTipoRiesgo'	},
		autoLoad		: false,
		listeners	:
		{
			beforeload: NE.util.initMensajeCargaCombo,
			load:procesarCatalogoTipoRiesgo,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	//catalogo Viabilidad
	var catalogoViabilidad= new Ext.data.JsonStore
	({
		id				: 'catViabilidad',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoViabilidad'	},
		autoLoad		: false,
		listeners	:
		{
			 beforeload: NE.util.initMensajeCargaCombo,
			 load	:procesarCatalogoViabilidad,
			 exception: NE.util.mostrarDataProxyError
		}
	});
		
	// catalogoOficina_controladora
	var catalogoOficina_controladora= new Ext.data.JsonStore
	({
		id				: 'catOficina_controladora',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoOficina_controladora'	},
		autoLoad		: false,
		listeners	:
		{
			beforeload: NE.util.initMensajeCargaCombo,
			load:procesarCatalogoOficina_controladora,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	// catalogo ClaveIfSIAG
	var catalogoClaveIfSIAG= new Ext.data.JsonStore
	({
		id				: 'catClaveIfSIAG',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoClaveIfSIAG'	},
		autoLoad		: false,
		listeners	:
		{
			beforeload: NE.util.initMensajeCargaCombo,
			load:procesarCatalogoClaveIfSIAG,
			exception: NE.util.mostrarDataProxyError
		}
	});	
	// catalogo ClaveSUCRE
	var catalogoClaveSUCRE= new Ext.data.JsonStore
	({
		id				: 'catClaveSUCRE',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoClaveSUCRE'	},
		autoLoad		: false,
		listeners	:
		{
			beforeload: NE.util.initMensajeCargaCombo,
			load:procesarCatalogoClaveSUCRE,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	//catalogo Descontante 
	var catalogoDescontante= new Ext.data.JsonStore
	({
		id				: 'catDescontante',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoDescontante'	},
		autoLoad		: false,
		listeners	:
		{
			beforeload: NE.util.initMensajeCargaCombo,
			load:procesarCatalogoDescontante,
			exception: NE.util.mostrarDataProxyError
		}
	});
	//catalogo Tipo de Cadena 
	var catalogoTipoCadena = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','PUBLICA'],
			['2','PRIVADA'],
			['3','AMBAS']
		 ]
	}) ;
	
	var catalogoLineaFondeo = new Ext.data.JsonStore({
	   id				: 'catLineaFondeo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoLineaFondeo'},
		totalProperty : 'total',
		autoLoad		: true,
		listeners	:
		{
			//load:procesarCatalogoEpo,
			exception: NE.util.mostrarDataProxyError 
		}
	});		
/*--------------------------------- End Store's -----------------------------*/
/*****************************************************************************/

	// *** Pop up *** //

	var elementoscargaArchivo = [
		{
			xtype: 		'panel',
			defaults: {	msgTarget: 'side',	anchor: '-20'	},
			bodyStyle: 	'padding: 12px; padding-right:6px;padding-left:6px;',
			//layout: 'hbox',
			items: [					
				{	xtype: 'displayfield',	id: 'R_F_C', value: '', width: 150	},				
				
				{	id: 'Imagen_ic_if', width:143, border:false,
					html:'<img src="/nafin/00archivos/if/logos/'+ic_if+'.gif" alt="" width="143" height="35" border="0">'	
				}
			]
		},
		{
			xtype: 		'panel',
			defaults: {	msgTarget: 'side',	anchor: '-20'	},
			bodyStyle: 	'padding: 12px; padding-right:6px;padding-left:6px;',
			layout: 'hbox',
			items: [		
				
				{	xtype: 'displayfield',	value: 'Carga archivo de Imagen IF:', width: 150	},				
				{
					xtype: 'fileuploadfield',
					id: 'archivo',
					width: 280,	  
					name: 'archivoCesion',
					buttonText: 'Examinar...',
					//buttonCfg: {  iconCls: 'upload-icon' },
				  anchor: '60%'
				  //vtype: 'archivopdf' ---> para imagenes... Deysi �???
				}
			],
			buttons: [
				{	
					text: 'Subir Archivo',
					iconCls: 'icoContinuar',
					formBind: true,
					handler: Guardar	
				},
				{
					text:'Cancelar', iconCls: 'icoLimpiar',	
					handler: function() {					
							var ventana = Ext.getCmp('winCargaArchivo');
							fpcargaArchivo.getForm().reset();
							ventana.hide();
					}	
				}
			]
		}	
	];
	
	var fpcargaArchivo = new Ext.form.FormPanel({
		id				: 'fcargaArchivo',
      frame			: true,
		width			: 480,
		height		: 180,
		//autoHeight	: true,
		layout		: 'form',
		fileUpload: true,
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		items			: [elementoscargaArchivo],
		monitorValid: true		
	});
	/*-------------------------- Popup Modificar --------------------------*/
		var elementosModProducto = [		
		{
			xtype			: 'panel',
			layout		: 'table',	
			width			: 450,
			autoHeight	: true,//height		: 450,
			layoutConfig:{ columns: 6 },
			defaults		: {frame:false, align:'center',border: true,width:100, bodyStyle:'padding:2px,'},
			items:[			
				{	width:200,frame:true,	border:false,	colspan: 2,	html:'<div align="center"><b>Producto</b>&nbsp;</div>'	},
				{	width:150,frame:true,	border:false,	colspan: 2,	html:'<div align="center"><b>Fecha de Convenio</b>&nbsp;</div>'	},
				{	width:100,frame:true,	border:false,	colspan: 2,	html:'<div align="center"><b>Dispersi�n</b>&nbsp;</div>'	},
				
				{	xtype: 'displayfield',  border: true,  id :'var_prod',	colspan: 2,		name : 'var_prod', 	width:200, 	height: 40 },
				{	xtype: 'datefield',     border: true, 	id :'var_fchC', 	colspan: 2,		name : 'var_fchC',	width:120,	height: 40 },
				{	xtype: 'checkbox',  		border: true,  id :'var_chkDisp',colspan: 1,		name : 'var_chkDisp',	width:50,	height: 40, hidden: true, style: {  width: '115%',margin:10,marginBottom: '10px'} },
				{	xtype: 'displayfield',  border: true, 	id :'var_disp',	colspan: 1,		name : 'var_disp',	width:50,	height: 40, hidden: true },
				{	xtype: 'hidden',  id: 'ic_producto',	name : 'ic_producto' },
				{	xtype: 'hidden',  id: 'chk_dispersion1',	name : 'chk_dispersion1' }
			]//items
		},
		{
			xtype: 		'panel',
			defaults: {	msgTarget: 'side',	anchor: '-10'	},
			bodyStyle: 	'padding: 18px; padding-right:2px;padding-left:70px;',
			layout: 'hbox',
			items: [
				{	xtype: 'displayfield',	width: 170	},
				{	xtype: 'button',			text: 'Aceptar',	id: 'btnAceptar',	width:90,
					handler: function(boton, evento) 
						{
							var chk_dispersion = '';
							if(Ext.getCmp('var_disp').getValue() == 'NA') {
								chk_dispersion = Ext.getCmp('var_disp').getValue();
							}else{
								if (Ext.getCmp('var_chkDisp').getValue() == true)
									chk_dispersion = 'S';
								else
									chk_dispersion = 'N';
							}	
							var cmpForma = Ext.getCmp('fModProducto');
							var params = (cmpForma)?cmpForma.getForm().getValues():{};				
							cmpForma.el.mask("Procesando", 'x-mask-loading');
							
							Ext.Ajax.request({
								url: '15forma09Ext.data.jsp',
								params : Ext.apply(params, {informacion: 'actualizaFechaConvenio', ic_if: ic_if,
									nombre_producto: Ext.getCmp('var_prod').getValue(),
									chk_dispersion: chk_dispersion 
								}),
								callback: procesarSuccessFailureActualizaFecha
							});
						}
				},	
				{	xtype: 'displayfield',	width: 10	},
				{	xtype: 'button',			text: 'Cancelar',	width:90,
					handler: function(){					
						Ext.getCmp('winModProd').hide();
					}				
				}
			]
		}			  		
	];
	
	var elementosDetalleReporte = [		
		{
			xtype			: 'panel',
			layout		: 'table',	
			width			: 840,
			autoHeight	: true,
			layoutConfig:{ columns: 7 },
			defaults		: {frame:false, align:'center',border: true,width:100, bodyStyle:'padding:2px,'},
			items:[
			
				{	width:150,frame:true,	border:false,	html:'<div align="center"><b><br>Producto</b>&nbsp;&nbsp;</div>'	},
				{	width:100,frame:true,	border:false,	html:'<div align="center"><b>No. L�nea de Fondeo</b>&nbsp;</div>'	},
				{	width:100,frame:true,	border:false,	html:'<div align="center"><b>Validar Disponibilidad</b>&nbsp;</div>'	},
				{	width:100,frame:true,	border:false,	html:'<div align="center"><b>Fecha/Hora de actualizaci�n</b>&nbsp;</div>'	},
				{	width:150,frame:true,	border:false,	html:'<div align="center"><b>Usuario �ltima Modificaci�n</b>&nbsp;</div>'	},
				{	width:120,frame:true,	border:false,	html:'<div align="center"><b><br>Monto Inicial</b>&nbsp;&nbsp;</div>'	},
				{	width:120,frame:true,	border:false,	html:'<div align="center"><b>Monto <br>Disponible</b>&nbsp;</div>'	},
				
				{	xtype: 'displayfield',  border: true,  id :'det_producto',			name : 'det_producto', 			width:150, 	height: 40 },
				{	xtype: 'combo',  			border: true,  id :'id_det_lineaF',			name : 'det_lineaF', 			width:95, 	height: 40,		hiddenName 		: 'det_lineaF',		forceSelection	: true,
					triggerAction	: 'all',	mode: 'local',	valueField: 'clave',			displayField: 'descripcion',	emptyText		: 'Seleccionar',	store: catalogoLineaFondeo },
					
				{	xtype: 'checkbox',  		border: true,  id :'det_disponibilidad',	name : 'det_disponibilidad', 	width:50, 	height: 40, 	style: {  width: '135%',margin:15,marginBottom: '15px'} },
				{	xtype: 'displayfield',  border: true, 	id :'det_fchAct',				name : 'det_fchAct',				width:100,	height: 40 },
				{	xtype: 'displayfield',  border: true, 	id :'det_usuario',			name : 'det_usuario',			width:150,	height: 40 },
				{	xtype: 'displayfield',  border: true, 	id :'det_montoIni',			name : 'det_montoIni',			width:120,	height: 40 },
				{	xtype: 'displayfield',  border: true, 	id :'det_montoDisp',			name : 'det_montoDisp',			width:120,	height: 40 },
				
				{	xtype: 'hidden',  id: 'ic_producto_detalle',	name : 'ic_producto_detalle' }
				
			]//items
		},
		{
			xtype: 		'panel',
			defaults: {	msgTarget: 'side',	anchor: '-10'	},
			bodyStyle: 	'padding: 18px; padding-right:2px;padding-left:250px;',
			layout: 'hbox',
			items: [
				{	xtype: 'displayfield',	width: 170	},
				{	xtype: 'button',			text: 'Aceptar',	id: 'btnAceptarDetalle',	width:90, //hidden:true,
					handler: function(boton, evento) 
						{
							/*
							*/
							var check = Ext.getCmp('det_disponibilidad').getValue();
							var linea_fondeo =Ext.getCmp('id_det_lineaF').getValue();
							var actualiza_disp = '';
							if(check == true) {
								if(Ext.isEmpty(linea_fondeo)) {
									Ext.MessageBox.alert('Mensaje de P�gina Web', 'Es necesario que proporcione un numero v�lido');
									return;
								}
								else {
									if (isdigit(linea_fondeo)){ 
										actualiza_disp = 'N';
									}else{
										Ext.MessageBox.alert('Mensaje de P�gina Web', 'Por favor, proporcione un numero valido.');
										return;
									}
								}
							}
							var cmpForma = Ext.getCmp('fDetalleReporte');
							var params = (cmpForma)?cmpForma.getForm().getValues():{};				
							cmpForma.el.mask("Procesando", 'x-mask-loading');
							
							Ext.Ajax.request({
								url: '15forma09Ext.data.jsp',
								params : Ext.apply(params, {informacion: 'actualizaDisponible', 
									ic_if: ic_if,
									ic_producto_nafin : Ext.getCmp('ic_producto_detalle').getValue(),									
									nombre_producto: Ext.getCmp('det_producto').getValue(),
									linea_fondeo :  Ext.getCmp('id_det_lineaF').getValue(),
									chk_disponibilidad: Ext.getCmp('det_disponibilidad').getValue(),
									act_disponible :actualiza_disp
								}),
								callback: procesarSuccessFailureDisponible
							});
						}
				},	
				{	xtype: 'displayfield',	width: 10	},
				{	xtype: 'button',			text: 'Cancelar',	id: 'btnCancelarDetalle',	width:90,
					handler: function(boton, evento){					
						Ext.getCmp('winDetalleReporte').hide();
					}				
				},
				{	xtype: 'displayfield',	width: 10	},
				{	xtype: 'button',			text: 'Actualizar Disponible',	id: 'btnActualizarDetalle', width:90,
					handler: function(boton, evento) 
						{
							var check = Ext.getCmp('det_disponibilidad').getValue();
							var linea_fondeo =Ext.getCmp('id_det_lineaF').getValue();
							var actualiza_disp = '';
							if(check == true) {
								if(Ext.isEmpty(linea_fondeo)) {
									Ext.MessageBox.alert('Mensaje de P�gina Web', 'Es necesario que proporcione la l�nea de fondeo');
									return;
								}
								else {
									if (isdigit(linea_fondeo)){ 
										actualiza_disp = 'S';
									}else{
										Ext.MessageBox.alert('Mensaje de P�gina Web', 'Por favor, proporcione un numero valido.');
										return;
									}
								}
							}
							else { // check == false
								Ext.MessageBox.alert('Mensaje de P�gina Web', 'Es necesario marcar la opcion de validar disponibilidad.');
								return;
							}
							var cmpForma = Ext.getCmp('fDetalleReporte');
							var params = (cmpForma)?cmpForma.getForm().getValues():{};		
							
							cmpForma.el.mask("Procesando", 'x-mask-loading');
							
							Ext.Ajax.request({
								url: '15forma09Ext.data.jsp',
								params : Ext.apply(params, {informacion: 'actualizaDisponible', 
									ic_if: ic_if,
									ic_producto_nafin : Ext.getCmp('ic_producto_detalle').getValue(),									
									nombre_producto: Ext.getCmp('det_producto').getValue(),
									linea_fondeo :  Ext.getCmp('id_det_lineaF').getValue(),
									chk_disponibilidad: Ext.getCmp('det_disponibilidad').getValue(),
									act_disponible :actualiza_disp
								}),
								callback: procesarSuccessFailureDisponible
							});
						}
				}
			]
		}			  		
	];
	
	var fpModProducto = new Ext.form.FormPanel({
		id				: 'fModProducto',
      frame			: false,
		width			: 450,
		autoHeight	: true,
		layout		: 'form',
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		items			: elementosModProducto,
		monitorValid: true		
	});
	
	var fpDetalleReporte = new Ext.form.FormPanel({
		id				: 'fDetalleReporte',
      frame			: false,
		width			: 840,
		autoHeight	: true,
		layout		: 'form',
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		items			: elementosDetalleReporte,
		monitorValid: true		
	});
	
/*-------------------------- Fin Popup Dispersion --------------------------*/
	// ____ ***** ***** ***** ***** TABLA DE PRODUCTOS ***** ***** ***** ***** ____ //
	/* 2. Store�s Grid�s */
	var consultaData = new Ext.data.JsonStore
	({		
		root:'registros',	
		url:'15forma09Ext.data.jsp',	
		totalProperty: 'total',		
			fields: [	
					{name: 'ic_producto'}, 
					{name: 'nombre_producto'}, 										
					{name: 'fechaConv'},
					{name: 'auxDisp'},
					{name: 'CL_MODIFICA'},
					{name: 'LINEA_FONDEO'}
					],
		messageProperty: 'msg',
		autoLoad: false,
		listeners: { exception: NE.util.mostrarDataProxyError	}		
	});
	
	/* 3. Grid */
	var grid = new Ext.grid.GridPanel
	({
		title      : '',
		id 		  : 'grid',
		store 	  : consultaData,
		style 	  : 'margin:0 auto;',
		stripeRows : true,
		loadMask	  : false,
		hidden	  : false,
		
		width		  : 601,
		height	  : 150,
		//autoWidth  : true,		autoHeight : true,
		frame		  : false, 
		header	  : true,
		columns    : [
			{
				header		: 'Producto',
				tooltip		: 'Producto',			
				dataIndex	: 'nombre_producto',	
				sortable		: true,	
				resizable	: true,	
				width			: 200,
				align			: 'center',
				renderer	: function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('nombre_producto') != ''){
						nombre_producto = record.get('nombre_producto');
						return value;
					}
				}
			},
			{
				header		: 'Fecha de Convenio',
				tooltip		: 'Fecha de Convenio',			
				dataIndex	: 'fechaConv',	
				sortable		: true,	
				resizable	: true,	
				width			: 100,
				align			: 'center'
				
			},
			{
				header		: 'Dispersi�n',
				tooltip		: 'Dispersi�n',
				dataIndex	: 'auxDisp',	
				sortable		: true,
				resizable	: true,	
				width			: 80, 
				align			: 'center'
				
			},
			{
				dataIndex:	'ic_producto',	
				hidden	: true
			},
			{
				xtype		: 'actioncolumn',
				header 	: 'Modificar', 
				tooltip	: 'Modificar',
				dataIndex: 'CL_MODIFICA',				
				width		:  100,	
				align		: 'center', 				
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('ic_producto') == 'SIAG' ){
						return 'NA';
					}
				},
				items		: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							if (record.get('ic_producto') == 'SIAG'){
								return "NA";
							}else	{
								for(i=0;i<permisosGlobales.length;i++){ 
									boton=  permisosGlobales[i];										
									if(boton=="CL_MODIFICA") {	
										this.items[0].tooltip = 'Modificar';
										return 'modificar';
									}
								}
							}
						},
						handler: ModificaProducto
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header 	: 'L�nea de Fondeo', 
				tooltip	: 'L�nea de Fondeo',
				dataIndex: 'LINEA_FONDEO',				
				width		:  100,	
				align		: 'center', 				
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('ic_producto') == '0' || record.get('ic_producto') == '1' || record.get('ic_producto') == '4'){
						return value;
					}
					else{
						return 'NA';
					}
				},
				items		: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							if (record.get('ic_producto') == '0' || record.get('ic_producto') == '1' || record.get('ic_producto') == '4'){
								for(i=0;i<permisosGlobales.length;i++){ 
										boton=  permisosGlobales[i];										
										if(boton=="LINEA_FONDEO") {											
											this.items[0].tooltip = 'Ver';
											return 'iconoLupa';
										}
									}
							}else	{
								return "NA";
							}
						},
						handler: detalleReporte
					}
				]
			}
		]
		
	});	
	function muestraAyuda(btn){
		Ext.Ajax.request({
				url: '15forma09Ext.data.jsp',
				params:	Ext.apply(
							{
								informacion: 'rfc_Sirac',
								numCliSirac: Ext.getCmp('idnumCliSirac').getValue()
							}
				),
				callback: procesarRfcAyuda
		});
     
   }
	

/****************************************************************************/

/*-------------------------------- Componentes ------------------------------*/
	
	var afiliacion = {
		xtype		: 'panel',
		id 		: 'afiliacion',
		layout	: 'hbox',
		border	: false,		
		width		: 900,		
		bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:192px;',
		items		: 
		[{
			layout		: 'form',
			width			:400,
			labelWidth	: 60,
			border		: false,
			items			:[
				{	xtype: 'hidden',	id	  : 'path_destino',	name : 'path_destino' },
				{	xtype: 'hidden',	id   : 'strArchivo',		name : 'strArchivo'   },
				{	xtype: 'hidden',  id   : 'id_domicilio',	name : 'id_domicilio' },
				{	xtype: 'hidden',  id   : 'Fecha_convenio',				name : 'Fecha_convenio' },
				{	xtype: 'hidden',  id   : 'Fecha_convenio_anticipo',	name : 'Fecha_convenio_anticipo' }
			]
		}]
	};
	

	var afiliacionIFPanel = {
		xtype		: 'panel',
		id 		: 'afiliacionIFPanel',
		width		: 450,
		items		: [
			{
				layout		: 'form',
				labelWidth	: 150,
				bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:2px;',
				items		:	
				[
					{
						xtype			: 'textfield',
						name			: 'Razon_Social',
						id				: 'Razon_Social',
						fieldLabel	: '* Raz�n Social',
						margins		: '0 20 0 0',
						msgTarget	: 'side',
						maxLength	: 100,
						width			: 350,
						allowBlank	: false,
						tabIndex		:	1
					},
					{
						xtype			: 'textfield',
						name			: 'nombre_comercial',
						id				: 'nombre_comercial',
						fieldLabel	: '* Nombre Comercial',
						margins		: '0 20 0 0',
						msgTarget	: 'side',
						maxLength	: 40,
						width			: 250,
						allowBlank	: false,
						tabIndex		:	2
					},
					{
						xtype 		: 'textfield',
						name  		: 'rfc',
						id    		: 'rfc',
						fieldLabel	: '* R.F.C.',
						maxLength	: 20,
						width			: 150,
						allowBlank 	: false,
						hidden		: false,
						margins		: '0 20 0 0',
						msgTarget	: 'side',
						regex			:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
						regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
						'en el formato NNN-AAMMDD-XXX donde:<br>'+
						'NNN:son las iniciales del nombre de la empresa<br>'+
						'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
						'XXX:es la homoclave',
						tabIndex		:	3
					}
			]}
	]};
//---------------------------------------------------------------------------//

	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		layout   : 'form',
		width  	: 450,
		labelWidth	: 170,
		bodyStyle: 	'padding: 4px; padding-right:0px;padding-left:2px;',
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calle',
				name			: 'calle',
				fieldLabel  : '* Calle No. Exterior y No. Interior',
				maxLength	: 80,
				width			: 250,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				allowBlank	: false,
				tabIndex		:	4

			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_estado',
				name				: 'cmb_estado',
				hiddenName 		: 'cmb_estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins		   : '0 20 0 0',
				msgTarget	   : 'side',
				tabIndex			: 6,
				store				: catalogoEstado,
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_delegacion').reset();
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais')).getValue()
							})
						});
					}
				}				
			},
			{
				xtype			: 'textfield',
				id          : 'code',
				name			: 'code',
				fieldLabel  : '* C�digo Postal',
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				maxLength	: 5,
				minLength	: 5,
				width			: 90,
				allowBlank	: false,
				//regex			:/^(([0-9]{5})$)/,
				regex			: /^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/,
				regexText	:'S�lo debe de introducir n�meros',
				tabIndex		: 8
			},
			{
				xtype			: 'textfield',
				id          : 'telefono',
				name			: 'telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				maxLength	: 20,
				regex			:/^(([0-9\-]*)$)/,	
				regexText	:'El campo solo puede contener n�meros',
				tabIndex		: 10	//validacion en evento on blur de si isTelFax
				
			},
			{
				xtype			: 'textfield',
				id          : 'fax',
				name			: 'fax',
				fieldLabel  : ' Fax',
				width			: 230,
				maxLength	: 20,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				allowBlank	: true,
				regex			:/^(([0-9\-]*)$)/,	
				regexText	:'El campo solo puede contener n�meros',
				tabIndex		:	12 //validacion en evento on blur de si isTelFax
			},
			{
				xtype				: 'combo',
				id          	: 'id_Domicilio_correspondencia',
				name				: 'Domicilio_correspondencia',
				hiddenName 		: 'Domicilio_correspondencia',
				fieldLabel  	: '* Domicilio correspondencia',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins			: '0 20 0 0',
				msgTarget		: 'side',
				tabIndex			: 13,
				store				: catalogoDomicilio
			},
			{
				xtype				: 'combo',
				id          	: 'id_Numero_de_IF',
				name				: 'Numero_de_IF',
				hiddenName 		: 'Numero_de_IF',
				fieldLabel  	: '* N�mero de IF',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins			: '0 20 0 0',
				msgTarget		: 'side',
				tabIndex			: 15,
				store				: catalogoNumeroIF,
				tpl:'<tpl for=".">' +
					'<tpl if="!Ext.isEmpty(loadMsg)">'+
					'<div class="loading-indicator">{loadMsg}</div>'+
					'</tpl>'+
					'<tpl if="Ext.isEmpty(loadMsg)">'+
					'<div class="x-combo-list-item">' +
					'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
					'</div></tpl></tpl>',
					setNewEmptyText: function(emptyTextMsg){
					this.emptyText = emptyTextMsg;
					this.setValue('');
					this.applyEmptyText();
					this.clearInvalid();								
				}
			},
			//{ 	xtype: 'displayfield',  value: '' },
			{
				xtype: 'compositefield',
				combineErrors: false,
				msgTarget: 'side',
				items: [				
					{
						xtype: 'textfield',
						name: 'numCliSirac',
						id: 'idnumCliSirac',
						fieldLabel: 'N�mero Cliente SIRAC',
						allowBlank: true,
						maxLength: 25,	//ver el tama�o maximo del numero en BD para colocar este igual
						width: 180,
						msgTarget: 'side',
						margins: '0 20 0 0', //n
						maskRe:/[0-9]/
					},
					{
						xtype:'button',
						id:'ayudaRFC',
						iconCls:'icoAyuda',
						handler:muestraAyuda
					}
				]	
			},
			{
				xtype				: 'combo',
				id          	: 'id_Avales',
				name				: 'Avales',
				hiddenName 		: 'Avales',
				fieldLabel  	: '* Avales',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins			: '0 20 0 0',
				msgTarget		: 'side',
				tabIndex			: 18,
				store				: catalogoAvales
			},
			{ 
				xtype: 			'bigdecimal',
				name: 			'Facultades',
				id: 				'Facultades',
				allowDecimals: true,
				allowNegative: false,
				fieldLabel: 	'* Facultades',
				blankText:		'El campo tiene un n�mero mayor de enteros y/o decimales de lo permitido.\nPor favor escriba un n�mero menor o igual a 19 enteros y 2 decimales.',
				allowBlank: 	false,
				hidden: 			false,				
				msgTarget: 		'side',
				anchor:			'-20',				
				maxValue: 		'9,999,999,999,999,999,999.99',
				format:			'0,000.00',
				width			: 90				
			},
			{
				xtype			: 'textfield',
				id          : 'Ente_contable',
				name			: 'Ente_contable',
				fieldLabel  : '* Ente contable',
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				maxLength	: 6,
				width			: 90,
				allowBlank	: false,				
				regex			:/^(([0-9]+)$)/,
				regexText	:'S�lo debe de introducir n�meros',
				tabIndex		: 22 //validacion en evento on blur de si soloNumeros
			},
			{
				xtype				: 'combo',
				id          	: 'id_Viabilidad',
				name				: 'Viabilidad',
				hiddenName 		: 'Viabilidad',
				fieldLabel  	: '* Viabilidad',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins			: '0 20 0 0',
				msgTarget		: 'side',
				tabIndex			: 24,
				store				: catalogoViabilidad,
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_delegacion').reset();
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais')).getValue()
							})
						});
					}
				}
				
			}
			
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		layout   : 'form',
		width  	: 450,
		labelWidth	: 170,
		bodyStyle: 	'padding: 4px; padding-right:0px;padding-left:2px;',	
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'colonia',
				name			: 'colonia',
				fieldLabel  : 'Colonia ',
				maxLength	: 60,
				width			: 230,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				allowBlank	: true,
				tabIndex		: 5
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais',
				name				: 'cmb_pais',
				hiddenName 		: 'cmb_pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: false,
				margins		   : '0 20 0 0',
				msgTarget	   : 'side',
				width				: 150,
				tabIndex			: 7,
				store				: catalogoPais,
				listeners: {
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado').reset();
						Ext.getCmp('id_cmb_delegacion').reset();
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});		
					}
				}				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacion',
				name				: 'cmb_delegacion',
				hiddenName 		: 'cmb_delegacion',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				triggerAction	: 'all',
				emptyText		:'Seleccionar...',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: false,
				margins			: '0 20 0 0',
				width				: 230,
				tabIndex			: 9,
				store				: catalogoMunicipio				
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_email',
				name			: 'email',
				fieldLabel	: ' E-mail',
				allowBlank	: true,
				maxLength	: 100,
				width			: 230,				
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				tabIndex		: 11,
				vtype: 'email' //validacion en evento on blur de si verEmail
			},
			{ 	xtype: 'displayfield',  value: '' },
			///////
			{
				xtype				: 'combo',
				id          	: 'id_Ejecutivo_de_cuenta',
				name				: 'Ejecutivo_de_cuenta',
				hiddenName 		: 'Ejecutivo_de_cuenta',
				fieldLabel  	: '* Ejecutivo de cuenta',
				forceSelection	: true,
				triggerAction	: 'all',
				emptyText		:'Seleccionar...',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: false,
				margins			: '0 20 0 0',
				width				: 230,
				tabIndex			: 14,
				store				: catalogoEjecutivo				
			},
			{
				xtype			: 'radiogroup',
				msgTarget	: 'side',
				id				: 'id_Tipo_de_IF',
				name			: 'Tipo_de_IF',
				hiddenName	: 'Tipo_de_IF',
				fieldLabel	: '* Tipo de IF',
				cls			: 'x-check-group-alt',
				columns		: [50, 50],
				valueField	:'base',
				tabIndex		: 16,
				items			: [
					{	boxLabel	: 'IFNB',	name : 'Tipo_de_IF', inputValue: 'NB',	checked : true },
					{	boxLabel	: 'IFB',	name : 'Tipo_de_IF', inputValue: 'B' }
			]},
			{
				xtype			: 'radiogroup',
				msgTarget	: 'side',
				id				: 'id_Tipo_de_IF_piso',
				name			: 'Tipo_de_IF_piso',
				hiddenName	: 'Tipo_de_IF_piso',
				fieldLabel	: '',
				cls			: 'x-check-group-alt',
				columns		: [100, 100],
				valueField	:'base',
				width			: 230,
				tabIndex		: 17,
				items			: [
					{	boxLabel	: 'Primer Piso',	name : 'Tipo_de_IF_piso', inputValue: '1'	},
					{	boxLabel	: 'Segundo P�so',	name : 'Tipo_de_IF_piso', inputValue: '2',	checked : true }
			]},
			{
				xtype				: 'combo',
				id          	: 'id_tipo_riesgo',
				name				: 'tipo_riesgo',
				hiddenName 		: 'tipo_riesgo',
				fieldLabel  	: 'Tipo de Riesgo',
				forceSelection	: true,
				triggerAction	: 'all',
				emptyText		:'Seleccionar...',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: true,
				margins			: '0 20 0 0',
				width				: 230, 
				tabIndex			: 19,
				store				: catalogoTipoRiesgo				
			},	
			{ 
				xtype: 			'bigdecimal',
				name: 			'Limite_maximo_endeudamiento',
				id: 				'Limite_maximo_endeudamiento',
				allowDecimals: true,
				allowNegative: false,
				fieldLabel: 	'* L�mite m�ximo endeudamieto',
				blankText:		'El campo tiene un n�mero mayor de enteros y/o decimales de lo permitido.\nPor favor escriba un n�mero menor o igual a 19 enteros y 2 decimales.',
				allowBlank: 	false,
				hidden: 			false,				
				msgTarget: 		'side',
				anchor:			'-20',				
				maxValue: 		'9,999,999,999,999,999,999.99',
				format:			'0,000.00',
				width			: 90				
			},	
			{ 
				xtype: 			'bigdecimal',
				name: 			'Porcentaje_capital_contable',
				id: 				'Porcentaje_capital_contable',
				allowDecimals: true,
				allowNegative: false,
				fieldLabel: 	'* Porcentaje capital contable',
				blankText:		'El campo tiene un n�mero mayor de enteros y/o decimales de lo permitido.\nPor favor escriba un n�mero menor o igual a 7 enteros y 2 decimales.',
				allowBlank: 	false,
				hidden: 			false,				
				msgTarget: 		'side',
				anchor:			'-20',				
				maxValue: 		'9,999,999.99',
				format:			'0,000.00',
				width			: 90				
			},
			
			{
				xtype				: 'combo',
				id          	: 'id_Oficina_controladora',
				name				: 'Oficina_controladora',
				hiddenName 		: 'Oficina_controladora',
				fieldLabel  	: '* Oficina controladora',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: false,
				margins			: '0 20 0 0',
				width				: 230,
				tabIndex			: 25,
				store				: catalogoOficina_controladora				
			}
			
		]
	};
	
	var domicilioPanel = {
		xtype			: 'panel',
		id 			: 'domicilioPanel',
		layout		: 'hbox',
		border		: false,		
		width			: 900,		
		labelWidth	: 150,
		bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:13px;',
		items		: 
		[{
				layout	: 'form',
				width		:900,
				border	: false,
				items		:[
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype			: 'checkbox',
								id				: 'id_autorizauto_opersf',
								name			: 'autorizauto_opersf',
								hiddenName	: 'autorizauto_opersf',
								fieldLabel	: 'Autorizaci�n Autom�tica de Operaciones sin fondeo',
								tabIndex		: 26,
								enable 		: true
							},
							{	xtype: 'displayfield',	value: '', width: 260	},
							{
								xtype			: 'checkbox',
								id				: 'id_enlace_automatico',
								name			: 'enlace_automatico',
								hiddenName	: 'enlace_automatico',
								//fieldLabel	: 'Enlace Autom�tico en la Operaci�n del FISO',
								enable 		: true,
								tabIndex		: 27,
								listeners	: {
									blur: function() {
										var enlace = (Ext.getCmp('id_enlace_automatico')).getValue();
										var tabla  = (Ext.getCmp('nombre_tabla'));
										if (enlace == true && Ext.isEmpty(tabla.getValue())) {
											//Ext.Msg.alert('Mensaje de P�gina Web','Debe capturar el nombre de la tabla');
											tabla.markInvalid('Debe capturar el nombre de la tabla');
											tabla.focus();
										}
										else if(enlace == false) {
											tabla.setValue('');
										}
									}								
								}
							},
							{	xtype: 'displayfield',	value: 'Enlace Autom�tico en la Operaci�n del FISO', width: 120	},
							{	xtype: 'displayfield',	value: '', width: 11	},
							{	xtype: 'displayfield',	value: 'Nombre de la tabla', width: 110	},
							{
								xtype			: 'textfield',
								id          : 'nombre_tabla',
								name			: 'nombre_tabla',
								margins		: '0 20 0 0',
								msgTarget	: 'side',
								//maxLength	: 6,
								width			: 128,
								allowBlank	: true,
								tabIndex		: 28, //validacion en evento on blur "JavaScript:validaTabla();"
								listeners	: {
									blur: function() {
										var enlace = (Ext.getCmp('id_enlace_automatico')).getValue();
										var tabla  = (Ext.getCmp('nombre_tabla'));
										
										if (Ext.isEmpty(tabla.getValue()) && enlace == true) {
											tabla.markInvalid('Debe capturar el nombre de la tabla');
											tabla.focus();
										}
										if (!Ext.isEmpty(tabla.getValue()) && enlace == false) {
											(Ext.getCmp('id_enlace_automatico')).setValue(true);
										}
									}								
								}
							}
						]
					},	//compo
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype				: 'combo',
								id          	: 'id_if_siag',
								name				: 'if_siag',
								hiddenName 		: 'if_siag',
								fieldLabel  	: 'Clave del Intermediario relacionado en SIAG',
								forceSelection	: true,
								triggerAction	: 'all',
								mode				: 'local',
								typeAhead		: true,
								emptyText		:'Seleccionar...',
								valueField		: 'clave',
								displayField	: 'descripcion',
								width				: 230,								
								margins			: '0 20 0 0',
								tabIndex			: 29,
								store				: catalogoClaveIfSIAG
							},
							{	xtype: 'displayfield',	value: '', width: 38	},
							{
								xtype			: 'checkbox',
								id				: 'id_convenio_unico',
								name			: 'convenio_unico',
								hiddenName	: 'convenio_unico',
								//fieldLabel	: 'Opera Convenio �nico',
								tabIndex		: 30,
								enable 		: true
							},
							{	xtype: 'displayfield',	value: 'Opera Convenio �nico', width: 150	}
						]
					},	//compo
					{
						xtype: 'compositefield',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{	xtype: 'displayfield',	value: '',	width: 289	},		
							{
								xtype			: 'checkbox',
								id				: 'id_fideicomiso',
								name			: 'fideicomiso',
								hiddenName	: 'fideicomiso',								
								tabIndex		: 32,
								enable 		: true
							},
							{	xtype: 'displayfield',	value: 'Opera Fideicomiso para Desarrollo de Proveedores',	width: 300	}		
						]
					},					
					{
						xtype: 'compositefield',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype				: 'combo',
								id          	: 'id_if_sucre',
								name				: 'if_sucre',
								hiddenName 		: 'if_sucre',
								fieldLabel  	: 'Clave del Intermediario en el SUCRE',
								forceSelection	: true,
								triggerAction	: 'all',
								mode				: 'local',
								typeAhead		: true,
								emptyText		:'Seleccionar...',
								valueField		: 'clave',
								displayField	: 'descripcion',
								width				: 230,								
								margins			: '0 20 0 0',
								tabIndex			: 31,
								store				: catalogoClaveSUCRE,
								tpl:'<tpl for=".">' +
								'<tpl if="!Ext.isEmpty(loadMsg)">'+
								'<div class="loading-indicator">{loadMsg}</div>'+
								'</tpl>'+
								'<tpl if="Ext.isEmpty(loadMsg)">'+
								'<div class="x-combo-list-item">' +
								'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
								'</div></tpl></tpl>',
								setNewEmptyText: function(emptyTextMsg){
									this.emptyText = emptyTextMsg;
									this.setValue('');
									this.applyEmptyText();
									this.clearInvalid();								
								}
							},
							{	xtype: 'displayfield',	value: '',	width: 38	},		
							{
								xtype			: 'checkbox',
								id				: 'id_mandato_documento',
								name			: 'mandato_documento',
								hiddenName	: 'mandato_documento',
								//fieldLabel	: 'Opera Instrucci�n Irrevocable',
								tabIndex		: 32,
								enable 		: true, //evento onclick="javascript:modificaDescontantes();
								listeners	: {
									check: function(field) {
										var mandato = (Ext.getCmp('id_mandato_documento')).getValue();
										if (mandato) {
											Ext.getCmp('id_descontanteIf1').setDisabled(false);
											Ext.getCmp('id_descontanteIf2').setDisabled(false);
											Ext.getCmp('id_tipoEpoInstruccion').setDisabled(false);
										}
										else {
											Ext.getCmp('id_descontanteIf1').setDisabled(true);
											Ext.getCmp('id_descontanteIf2').setDisabled(true);
											Ext.getCmp('id_tipoEpoInstruccion').setDisabled(true);
										}
										if(mandato == false && total >0) {
											Ext.getCmp('id_mandato_documento').setValue(true); 
											Ext.MessageBox.alert('Mensaje de p�gina web','Tiene Instrucciones Irrevocables pendientes.' );
										}
									}								
								}
							},
							{	xtype: 'displayfield',	value: 'Opera Instrucci�n Irrevocable',	width: 190	}		
						]
					},	//compo
					{
                        xtype			: 'radiogroup',
                        msgTarget	: 'side',
                        id				: 'id_OperaMontosMenores',
                        name			: 'id_OperaMontosMenores',
                        hiddenName	: 'id_OperaMontosMenores',
                        fieldLabel	: 'Opera Montos Menores',
                        cls			: 'x-check-group-alt',
						width: 150,
						height: 18,
                        columns		: [40, 40],
                        valueField	:'base',
                        tabIndex		: 16,
                        items			: [
                            {	boxLabel	: 'SI',	name : 'id_OperaMontosMenores', inputValue: 'S'},
                            {	boxLabel	: 'NO',	name : 'id_OperaMontosMenores', inputValue: 'N'}
						]
					},		
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
						{	xtype: 'displayfield',	value: '',	width: 287	},		
						{
							xtype: 'checkbox',							
							name: 'chkFactDistribuido',
							id: 'chkFactDistribuido1',
							hiddenName:'chkFactDistribuido'						
						},
						{	xtype: 'displayfield',	value: 'Opera Factoraje Distribuido:',	width: 170	}				
						]
					},//composi
					
					
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
						{	xtype: 'displayfield',	value: '',	width: 287	},		
						{	xtype: 'displayfield',	value: 'Descontante 1:',	width: 170	},		
						{
							xtype				: 'combo',
							id          	: 'id_descontanteIf1',
							name				: 'descontanteIf1',
							hiddenName 		: 'descontanteIf1',
							//fieldLabel  	: 'Descontante 1',
							forceSelection	: true,
							triggerAction	: 'all',
							emptyText		:'Sin Descontante',
							mode				: 'local',
							valueField		: 'clave',
							displayField	: 'descripcion',
							//enable			: false,
							disabled			: true,
							margins			: '0 20 0 0',
							width				: 230,
							tabIndex			: 33,
							store				: catalogoDescontante,
							listeners: {
								select: function(combo) {
									if (solicitudesDescontante1 != null && solicitudesDescontante1 != '' && solicitudesDescontante1 > 0 ) {
										Ext.getCmp('id_descontanteIf1').setValue(desc1);
										Ext.MessageBox.alert('Mensaje de p�gina web','El descontante tiene Instrucciones Irrevocables pendientes.');
									}
								}
							}
						}
						]
					},//composi
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{	xtype: 'displayfield',	value: '',	width: 287	},		
							{	xtype: 'displayfield',	value: 'Descontante 2:',	width: 170	},		
							{
								xtype				: 'combo',
								id          	: 'id_descontanteIf2',
								name				: 'descontanteIf2',
								hiddenName 		: 'descontanteIf2',
								forceSelection	: true,
								triggerAction	: 'all',
								mode				: 'local',
								emptyText		:'Sin Descontante',
								valueField		: 'clave',
								displayField	: 'descripcion',
								disabled			: true,
								//enable			: false,
								margins			: '0 20 0 0',
								width				: 230,
								tabIndex			: 34,
								store				: catalogoDescontante,
								listeners		: 	{
									select: function(combo) {
										if (solicitudesDescontante2!= null && solicitudesDescontant2 != '') {
										Ext.getCmp('id_descontanteIf2').setValue(desc2);
											Ext.MessageBox.alert('Mensaje de p�gina web','El descontante tiene Instrucciones Irrevocables pendientes.');
										}
									}
								}
							}
						]
					},//composit
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{	xtype: 'displayfield',	value: '',	width: 287	},		
							{	xtype: 'displayfield',	value: 'Tipo de Cadena:',	width: 170	},		
							{
								xtype				: 'combo',
								id          	: 'id_tipoEpoInstruccion',
								name				: 'tipoEpoInstruccion',
								hiddenName 		: 'tipoEpoInstruccion',
								forceSelection	: true,
								triggerAction	: 'all',
								mode				: 'local',
								emptyText		:'Seleccionar...',
								valueField		: 'clave',
								displayField	: 'descripcion',
								//allowBlank		: false,
								disabled			: true,
								//enable			: false,
								margins			: '0 20 0 0',
								msgTarget		: 'side',
								width				: 230,
								tabIndex			: 35,
								store				: catalogoTipoCadena				
							}
						]
					}//composit
				]
		}]
	};
	
	/*--____Fin Domicilio ____--*/
			
//---------------------------------------------------------------------------//	

	
	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 900,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		//defaults			: { msgTarget: 'side',anchor: '-20' },
		monitorValid	: true,
		items				: 
		[
			{
				layout	: 'hbox',
				title		: '',
				width		: 900,
				items		: [afiliacion ]
			},
			{
				layout	: 'hbox',
				title		: '',
				//height	: 500,
				width		: 900,
				items		: [afiliacionIFPanel ]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 900,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			},
			{
				layout	: 'hbox',
				title		: '',
				width		: 900,
				items		: [domicilioPanel ]
			}
		],
		buttons: [ 
			{
				text: 'Aceptar',
				id: 'BTNACEPTARIN',
				hidden: true,
				iconCls:'aceptar',
				formBind: true,
				handler: function() {					
					validaPermisos('BTNACEPTARIN');
				}
			},
			{
				text: 'Cargar Imagen',
				id: 'BTNCARGARIMAGENIN',
				hidden: true,
				handler: function() {					
					validaPermisos('BTNCARGARIMAGENIN');
					
				}
			},
			{
				text: 'Regresar',
				handler: function() {
					window.location = '15forma5ext.jsp?idMenu='+idMenu;  
				}
			},
			{
				text:'Cancelar',
				id: 'BTNCANCELARIN',
				hidden: true,
				handler: function() {
				
					validaPermisos('BTNCANCELARIN')
				
				}
			}
	  ]	 
	});			
	       
	
	var pnl = new Ext.Container({
		id  		: 'contenedorPrincipal',
		applyTo	: 'areaContenido',
		width		: 'auto',
		height	: 'auto',
		items		: [
			fpDatos, 
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10)
		]
	});

		
	
	//catalogoMunicipio.load();
	//catalogoEstado.load();

	catalogoPais.load();
	catalogoDomicilio.load();
	catalogoNumeroIF.load();
	catalogoAvales.load();
	catalogoTipoRiesgo.load();
	catalogoViabilidad.load();
	catalogoOficina_controladora.load();
	catalogoClaveIfSIAG.load();
	catalogoClaveSUCRE.load();
	catalogoDescontante.load();
	
	
	Ext.Ajax.request({
		url: '15forma09Ext.data.jsp', 
		params: {
			informacion: "valoresIniciales", 
			idMenuP:idMenu
		},
		callback: procesaValoresIniciales
	});
	

});