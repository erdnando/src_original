<%@ page import = "java.text.*"%>
<%@ page import = "java.util.*"%>
<%@ page import = "com.itextpdf.text.BaseColor"%>
<%@ page import = "com.itextpdf.text.FontFactory" %>
<%@ page import = "com.itextpdf.text.Font" %>
<%@ page import = "com.itextpdf.text.Phrase" %>
<%@ page import = "com.itextpdf.text.Element" %>
<%@ page import = "com.itextpdf.text.pdf.PdfReader" %>
<%@ page import = "com.itextpdf.text.pdf.PdfStamper" %>
<%@ page import = "com.itextpdf.text.pdf.PdfContentByte" %>
<%@ page import = "com.itextpdf.text.pdf.BaseFont" %>
<%@ page import = "com.itextpdf.text.pdf.PdfPTable" %>
<%@ page import = "com.itextpdf.text.pdf.PdfPCell" %>
<%@ page import = "net.sf.json.JSONObject" %>
<%@ page contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/15cadenas/015secsession.jspf"%>

<%!public final int ORIENTACION_HORIZONTAL = 2;%>
<%
String informacion = (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String clave_if 	= (request.getParameter("clave_if")==null)?"":request.getParameter("clave_if");
String clave_pyme 	= (request.getParameter("clave_pyme")==null)?"":request.getParameter("clave_pyme");

CreaArchivo archivo = new CreaArchivo();
String nombreArchivo = archivo.nombreArchivo()+".pdf";

AccesoDB con = new AccesoDB();
PreparedStatement pst = null;
ResultSet rst = null;
StringBuffer strSQL = new StringBuffer();
List varBind = new ArrayList();
StringBuffer errores = new StringBuffer();
int numero_registro = 1;
int registrados_cadenas = 0;
int no_registrados_cadenas = 0;
boolean cargaOK = true;
JSONObject jsonObj = new JSONObject();
	
try {
	con.conexionDB();

	strSQL.append(" SELECT DISTINCT pym.cg_rfc rfc,");
	strSQL.append(" pym.cg_razon_social nombre_pyme,");
	strSQL.append(" crn.ic_nafin_electronico numero_nafele,");
	strSQL.append(" pym.in_numero_sirac numero_sirac,");
	strSQL.append(" TO_CHAR(bcu.df_autorizacion_if, 'dd/mm/yyyy hh:mi') || ' Hrs. ' || bcu.ic_usuario fecha_aut_if,");
	strSQL.append(" TO_CHAR(bcu.df_aceptacion_pyme, 'dd/mm/yyyy hh:mi') || ' Hrs.' fecha_acep_pyme,");
	strSQL.append(" TO_CHAR(pym.df_firma_conv_unico, 'dd/mm/yyyy hh:mi') || ' Hrs.' fecha_firma_pyme");
	strSQL.append(" FROM bit_convenio_unico bcu");
	strSQL.append(" , comcat_if cif");
	strSQL.append(" , comcat_pyme pym");
	strSQL.append(" , comrel_nafin crn");
	strSQL.append(" , comrel_pyme_epo cpe");
	strSQL.append(" , comcat_epo epo");
	strSQL.append(" WHERE bcu.ic_if = cif.ic_if");
	strSQL.append(" AND bcu.ic_pyme = pym.ic_pyme");
	strSQL.append(" AND pym.ic_pyme = crn.ic_epo_pyme_if");
	strSQL.append(" AND crn.cg_tipo = ?");
	strSQL.append(" AND bcu.ic_pyme = cpe.ic_pyme");
	strSQL.append(" AND cpe.ic_epo = epo.ic_epo");
	strSQL.append(" AND cpe.cs_habilitado = ?");
	strSQL.append(" AND pym.cs_convenio_unico = ?");
	strSQL.append(" AND bcu.ic_if = ?");
	strSQL.append(" AND bcu.ic_pyme = ?");
	
	varBind.add("P");
	varBind.add("S");
	varBind.add("S");
	varBind.add(new Long(clave_if));
	varBind.add(new Long(clave_pyme));
	
	List datos_convenio_unico = new ArrayList();
	String rfc_pyme = "";
	String nombre_pyme = "";
	
	pst = con.queryPrecompilado(strSQL.toString(), varBind);
	
	rst = pst.executeQuery();
	
	while(rst.next()){
		rfc_pyme =rst.getString(1);
		nombre_pyme = rst.getString(2);
		datos_convenio_unico.add(rst.getString(3));
		datos_convenio_unico.add(rst.getString(4));
		datos_convenio_unico.add(rst.getString(5));
		datos_convenio_unico.add(rst.getString(6));
		datos_convenio_unico.add(rst.getString(7));
	}
	
	rst.close();
	pst.close();
	
	//Estas fuentes va a servir para ejemplificar como se puede dar formato al contenido que se va a sobreescribir en el pdf.
	Font		formas = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL);
	Font		formasB = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD);
	Font		formasrep = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL);
	Font		formasrepB = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD);
	Font		formasmen = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.NORMAL);
	Font		formasmenB = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);
	Font		formasG = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD);
	Font		celda02G = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD);
	Font    	subrayado = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.UNDERLINE);

	int	LEFT 			= Element.ALIGN_LEFT;
	int	CENTER 		= Element.ALIGN_CENTER;
	int	RIGHT 		= Element.ALIGN_RIGHT;
	int	JUSTIFIED 	= Element.ALIGN_JUSTIFIED;
	
	//Estableciendo encabezados del response
	response.setHeader("Content-Disposition","attachment;filename=Clausulado_de_Convenio_"+Comunes.cadenaAleatoria(4)+".pdf");
	response.setHeader("Expires", "0");
	response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
	response.setHeader("Pragma", "public");
	
	//Estableciendo Content Type
	response.setContentType("application/download");

	//Se crean los respectivos reader y stamper del pdf.
	PdfReader reader = new PdfReader(strDirTrabajo+"contrato_unico_2009.pdf");
	PdfStamper stamper = new PdfStamper(reader, response.getOutputStream());
	
	//Al reader se le pide el numero de paginas con lo que sabremos el numero de paginas del archivo.
	int paginas = reader.getNumberOfPages();
	
	//float[] datos_pyme = {20f, 30f, 20f, 30f};
	float[] datos_pyme = {20f, 30f, 50f};
	
	PdfPTable tabla_datos_pyme = new PdfPTable(datos_pyme);
	
	tabla_datos_pyme.setTotalWidth(400);
	tabla_datos_pyme.getDefaultCell().setBorderColor(BaseColor.WHITE);
	tabla_datos_pyme.getDefaultCell().setVerticalAlignment(CENTER);
	tabla_datos_pyme.getDefaultCell().setPadding(30f);
	tabla_datos_pyme.getDefaultCell().setBorderWidth(30f);
	
	PdfPCell cell  = new PdfPCell(new Phrase("RFC: ", formasB));
	cell.setBorderColor( BaseColor.WHITE);
	cell.setVerticalAlignment(CENTER);
	cell.setHorizontalAlignment(CENTER);
	tabla_datos_pyme.addCell(cell);

	cell  = new PdfPCell(new Phrase(rfc_pyme, formasB));
	cell.setBorderColor( BaseColor.WHITE);
	cell.setVerticalAlignment(CENTER);
	cell.setHorizontalAlignment(CENTER);
	tabla_datos_pyme.addCell(cell);

	cell  = new PdfPCell(new Phrase(nombre_pyme, formasB));
	cell.setBorderColor( BaseColor.WHITE);
	cell.setVerticalAlignment(CENTER);
	cell.setHorizontalAlignment(CENTER);
	tabla_datos_pyme.addCell(cell);
	
	tabla_datos_pyme.writeSelectedRows(0, -1, 60f, 730f, stamper.getOverContent(reader.getNumberOfPages()));
	
	float[] contenidoEncabezado  = {20f, 20f, 20f, 20f, 20f};
	
	PdfPTable tablaEncabezado = new PdfPTable(contenidoEncabezado);
	
	tablaEncabezado.setTotalWidth(490);
	tablaEncabezado.getDefaultCell().setBorderColor(BaseColor.BLACK);
	tablaEncabezado.getDefaultCell().setVerticalAlignment(CENTER);
	tablaEncabezado.getDefaultCell().setPadding(30f);
	tablaEncabezado.getDefaultCell().setBorderWidth(30f);

	cell  = new PdfPCell(new Phrase("N�mero Electr�nico", formasB) );
	cell.setBorderColor( BaseColor.BLACK);
	cell.setVerticalAlignment(CENTER);
	cell.setHorizontalAlignment(CENTER);
	tablaEncabezado.addCell(cell);

	cell  = new PdfPCell(new Phrase("Clave SIRAC", formasB) );
	cell.setBorderColor( BaseColor.BLACK);
	cell.setVerticalAlignment(CENTER);
	cell.setHorizontalAlignment(CENTER);
	tablaEncabezado.addCell(cell);

	cell  = new PdfPCell(new Phrase("Fecha de Aceptaci�n IF de Cuenta Bancaria / Usuario", formasB) );
	cell.setBorderColor( BaseColor.BLACK);
	cell.setVerticalAlignment(CENTER);
	cell.setHorizontalAlignment(CENTER);
	tablaEncabezado.addCell(cell);

	cell  = new PdfPCell(new Phrase("Fecha de Aceptaci�n del Convenio �nico por la PyME", formasB) );
	cell.setBorderColor( BaseColor.BLACK);
	cell.setVerticalAlignment(CENTER);
	cell.setHorizontalAlignment(CENTER);
	tablaEncabezado.addCell(cell);

	cell  = new PdfPCell(new Phrase("Fecha de Firma Aut�grafa de Convenio �nico", formasB) );
	cell.setBorderColor( BaseColor.BLACK);
	cell.setVerticalAlignment(CENTER);
	cell.setHorizontalAlignment(CENTER);
	tablaEncabezado.addCell(cell);

	
	for(int i = 0; i < datos_convenio_unico.size(); i++){
		cell  = new PdfPCell(new Phrase((String)datos_convenio_unico.get(i), formas) );
		cell.setBorderColor( BaseColor.BLACK);
		cell.setVerticalAlignment(CENTER);
		cell.setHorizontalAlignment(CENTER);
		tablaEncabezado.addCell(cell);
	}
	
	tablaEncabezado.writeSelectedRows(0, -1, 60f, 700f, stamper.getOverContent(reader.getNumberOfPages()));

	if(informacion.equals("VersionExtJS")){
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
		stamper.close();
} finally {
	if (con.hayConexionAbierta()) {
		con.cierraConexionDB();
	}
}
if(informacion.equals("VersionExtJS")){
%>
<%=jsonObj%>
<% } %>