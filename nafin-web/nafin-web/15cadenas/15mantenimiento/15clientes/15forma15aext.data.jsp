<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		com.netro.model.catalogos.CatalogoEstado,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	JSONObject resultado = new JSONObject();


	if(informacion.equals("catalogoIntermediario"))	{
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_if");
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("obtieneAfiliadosCE"))	{
		
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		String sPymesOK = (request.getParameter("sPymesOK")==null)?"":request.getParameter("sPymesOK");
		String sArchivoErrores = (request.getParameter("sArchivoErrores")==null)?"":request.getParameter("sArchivoErrores");
		String NoIf = (request.getParameter("NoIf")==null)?"":request.getParameter("NoIf");
		
		Hashtable hAfiliaCE = BeanAfiliacion.afiliaPymeCE(sPymesOK, "", "", "SI", NoIf, iNoUsuario, strTipoUsuario);
		String sPymesDadasAlta = hAfiliaCE.get("sNoPymesAlta").toString();
		CreaArchivo archivo = new CreaArchivo();
		
		StringBuffer contenidoArchivo = new StringBuffer("PYMES Afiliadas a Crédito Electrónico\nNúmero de Cliente SIRAC,Nombre Completo o Razon Social,RFC,Tipo Persona,Sector Económico,Subsector,Rama,Clase,Estrato,Número de Folio"+(strTipoUsuario.equals("NAFIN")?",Número de Cliente TROYA":"")+"\n");
		
		List lstrRegAfil = new ArrayList();
		HashMap hmData = null;
		Vector vDatosPyme = BeanAfiliacion.getPymeAfiliadasCE(sPymesDadasAlta);
		for(int i=0; i<vDatosPyme.size(); i++) {
			hmData = new HashMap();
			Vector vPyme = (Vector)vDatosPyme.get(i);
			contenidoArchivo.append(vPyme.get(0).toString()+","+(vPyme.get(6).toString().equals("F")?vPyme.get(1).toString()+" "+vPyme.get(2).toString()+" "+vPyme.get(3).toString():vPyme.get(4).toString())+","+vPyme.get(5).toString()+","+(vPyme.get(6).toString().equals("F")?"Física":"Moral")+","+vPyme.get(7).toString().replace(',',' ')+","+vPyme.get(8).toString().replace(',',' ')+","+vPyme.get(9).toString().replace(',',' ')+","+vPyme.get(10).toString().replace(',',' ')+","+vPyme.get(11).toString()+","+vPyme.get(12).toString()+","+(strTipoUsuario.equals("NAFIN")?vPyme.get(13).toString():"")+"\n");
			
			hmData.put("NUMSIRAC", vPyme.get(0).toString());
			hmData.put("NOMBRECOMPLETO", (vPyme.get(6).toString().equals("F")?vPyme.get(1).toString()+" "+vPyme.get(2).toString()+" "+vPyme.get(3).toString():vPyme.get(4).toString()));
			hmData.put("RFC", vPyme.get(5).toString());
			hmData.put("TIPOPERSONA", (vPyme.get(6).toString().equals("F")?"Física":"Moral"));
			hmData.put("SECTORECO", vPyme.get(7).toString());
			hmData.put("SUBSECTOR", vPyme.get(8).toString());
			hmData.put("RAMA", vPyme.get(9).toString());
			hmData.put("CLASE", vPyme.get(10).toString());
			hmData.put("ESTRATO", vPyme.get(11).toString());
			hmData.put("NUMFOLIO", vPyme.get(12).toString());
			hmData.put("NUMTROYA", vPyme.get(13).toString());
			
			lstrRegAfil.add(hmData);
		}
		
		String nombreArchivo="";
		String ruta = "";
		
		if (archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
			nombreArchivo = archivo.nombre;
			ruta = strDirecVirtualTemp + nombreArchivo ;
		}
		
		resultado.put("success", new Boolean(true));
		resultado.put("urlArchivo", ruta);
		resultado.put("registros", JSONArray.fromObject(lstrRegAfil));
		
		infoRegresar= resultado.toString();
	
	}
	System.out.println("infoRegresar = = "+infoRegresar);
%>
<%=infoRegresar%>