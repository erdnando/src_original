<!DOCTYPE html>
<%@ page import="java.util.*,com.netro.exception.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<html>
      <head>
        <title>Nafinet</title>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

        <%@ include file="/extjs.jspf" %>		  
			<%@ include file="/01principal/menu.jspf"%>
        <script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
        <script type="text/javascript" src="15clausuladoExt.js?<%=session.getId()%>"></script>
      </head>
		
  <%boolean bandera;
  
if(strTipoUsuario.equals("EPO")){
	bandera=true;
}else{
	bandera=false;
}
%>
		    
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%if(bandera){%>
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<%}else{%>
	<%@ include file="/01principal/01if/cabeza.jspf"%>
	<%}%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%if(bandera){%>
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<%}else{%>
	<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
	<%}%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%if(bandera){%>
	<%@ include file="/01principal/01epo/pie.jspf"%>
	<%}else{%>
	<%@ include file="/01principal/01if/pie.jspf"%>
	<%}%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>