<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.io.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
									/*	MIGRACION ADMIN PROMO NAFIN Administracion-Afiliados-Bitacora de Cambios */

String infoRegresar 	= "";
String informacion 	= (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion 		= (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";

if (informacion.equals("cargaFecha") )	{
  JSONObject		 jsonObj  = new JSONObject();
  String fechaHoy = Fecha.getFechaActual();
  jsonObj.put("success"	,  new Boolean(true));
  jsonObj.put("fechaHoy", fechaHoy);
  infoRegresar = jsonObj.toString();

} else if(informacion.equals("catalogoEpo")) {
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setHabilitado("S");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();	

}else if (informacion.equals("consultaGeneral") | informacion.equals("detalle") | informacion.equals("GeneraArchivoCSV")  | informacion.equals("GeneraArchivoPDF") | informacion.equals("GeneraArchivoTxt")   ) { 
	
	JSONObject jsonObj = new JSONObject();
	String cveEpo  = request.getParameter("_cmb_epo")==null?"":request.getParameter("_cmb_epo");
	String fecIni  = request.getParameter("_fechaReg")==null?"":request.getParameter("_fechaReg");
	String fecFin  = request.getParameter("_fechaFinReg")==null?"":request.getParameter("_fechaFinReg");
	String tipoReg = request.getParameter("tipoReg")==null?"":request.getParameter("tipoReg");
	String icBitac = request.getParameter("icBitac")==null?"":request.getParameter("icBitac");
	
	BitacoraCambiosCargaMasiva paginador = new BitacoraCambiosCargaMasiva();
	paginador.setClaveEpo(cveEpo);
	paginador.setFechaIni(fecIni);
	paginador.setFechaFin(fecFin);
	 
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	if (informacion.equals("consultaGeneral")) {				
		try {
			Registros reg	=	queryHelper.doSearch();
			String consulta	=	"{\"success\": true, \"total\": \""	+ reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta); 
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	
	} else if (informacion.equals("GeneraArchivoTxt") ) {	
			try {
				//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
				//Afiliacion afiliacion = afiliacionHome.create();
				Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
				String rutaNombreArchivo = afiliacion.getDetalleBitCargaPymeMasiva(icBitac,strDirectorioTemp,tipoReg);
				File ft = new java.io.File(rutaNombreArchivo);
				StringBuffer 	contenidoArchivo 	= new StringBuffer();
				BufferedReader 	brt					=  null;
				CreaArchivo archivo = new CreaArchivo();
				String linea = "",nombreArchivo = null;;
				brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
				while((linea=brt.readLine())!=null){
					VectorTokenizer vtd		= new VectorTokenizer(linea,"|");
					Vector vecdet	= vtd.getValuesVector();
					System.out.println("(String)vecdet.get(vecdet.size()-1)::::::"+(String)vecdet.get(vecdet.size()-1));
					if("T".equals(tipoReg) )
						contenidoArchivo.append(linea+"\n");
					if("C".equals(tipoReg) && ((String)vecdet.get(vecdet.size()-1)==null || "".equals((String)vecdet.get(vecdet.size()-1))) )
						contenidoArchivo.append(linea+"\n");
					if("E".equals(tipoReg) && (String)vecdet.get(vecdet.size()-1)!=null && !"".equals((String)vecdet.get(vecdet.size()-1)))
						contenidoArchivo.append(linea+"\n");
				}
				if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt")){
					out.print("<--!Error al generar el archivo-->");
				} else{
					nombreArchivo = archivo.nombre;
				}
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", "/nafin/00tmp/15cadenas/"+nombreArchivo);	
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			}
			
	} else if(informacion.equals("GeneraArchivoPDF") ){
		try {
				int start=0, limit=15;	
				queryHelper.executePKQuery(request); 
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				jsonObj.put("tipoarchivo", "GeneraArchivoPDF");
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
	
	} else if (informacion.equals("GeneraArchivoCSV") ) {	
		try {
					String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					jsonObj.put("tipoarchivo", "GeneraArchivoCSV");
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo CSV", e);
				}
		}
	
	infoRegresar = jsonObj.toString(); 
}



%>
<%=infoRegresar%>