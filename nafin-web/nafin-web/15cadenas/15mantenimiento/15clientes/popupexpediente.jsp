<%@ page import= "java.sql.*, java.text.*, java.util.*,
				javax.naming.*,com.netro.afiliacion.*, netropology.utilerias.*, java.io.* " %>
<%@ page import = "net.sf.json.JSONObject" %>				
<%@ include file="../../015secsession.jspf"%>
<% 
String rfc = (request.getParameter("rfc")==null)?"":request.getParameter("rfc");
String version = (request.getParameter("version")==null)?"":request.getParameter("version");
String expediente = (request.getParameter("expediente")==null)?"":request.getParameter("expediente");
String accion = (request.getParameter("accion")==null)?"":request.getParameter("accion");
AccesoDB con         = new AccesoDB();
JSONObject jsonObj = new JSONObject();
	
try{
	
	Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	//saca el expediente de efile y lo copia a nafin
	BeanAfiliacion.getExpedientePymeEFile(rfc);
	
	//consultar el expediente en tabla de nafin
	FileOutputStream fos = null;
	File file            = null;	
	String pathname      = "";
	PreparedStatement ps = null;
	ResultSet rs         = null;
	String qryDocto      = ""; 
	String nombreArchivo ="";
  String ContentType = "application/pdf";
  OutputStream os = response.getOutputStream();
  Blob bin = null;                
  InputStream inStream = null;                
  
	boolean existearchivo=false;
  int size = 0;
	con.conexionDB();
	qryDocto = 	" SELECT pym.IC_PYME,  efdoc.BI_DOCUMENTO, efdoc.CG_EXTENSION, 'popupexpediente.jsp'  " +
							" FROM COMTMP_DOC_PYME efdoc, comcat_pyme  pym " +
							" WHERE efdoc.IC_PYME = pym.ic_pyme " +
							" and pym.cg_rfc = '" + rfc + "' ";
	System.out.println("qryDocto : "+qryDocto);

	ps = con.queryPrecompilado(qryDocto);
	rs = ps.executeQuery();
	if(rs.next())
	{
    // SE INCORPORA EL LLAMADO A LA BITACORA DE ACCESOS DE EXPEDIENTES DEL EFILE
    CallableStatement cs = con.ejecutaSP("PROC_BIT_ACCESOS (?, ?, ?, ?)");

    cs.setString(1, rfc);
    cs.setInt(2, 2);
    cs.setString(3, strLogin);
    cs.setString(4, "Nafin Electr�nico");

    cs.execute();

    cs.close(); 
    con.terminaTransaccion(true);

		pathname = strDirectorioTemp + "Efile_"+nombreArchivo+ rs.getString("CG_EXTENSION");                
		//System.out.println("pathname : "+pathname);
		file = new File(pathname);                
		fos = new FileOutputStream(file);                                    
		bin = rs.getBlob("BI_DOCUMENTO");                
		inStream = bin.getBinaryStream();                
		size = (int)bin.length();                
    int length = 0;
    response.setContentType(ContentType);
	  response.setHeader("Content-Disposition", "attachment;filename=ExpedienteDigital.pdf");
    //response.setContentLength(size);
    
    
    byte b[] = new byte[1048576]; //1MB de buffer
    while ((length = inStream.read(b)) != -1) {
      os.write(b, 0, length);
    }
    os.flush();
    os.close();
    response.flushBuffer();
    existearchivo = true;
}	
%>
<%if(!existearchivo){%>

<html>
<head>
	<title>Nafin@net</title>
	<meta http-equiv="Content-Type" content="application/pdf; charset=iso-8859-1">
	<link rel="stylesheet" href="<%=strDirecVirtualCSS%>css/<%=strClase%>">
	<script language="JavaScript" src="../../../00utils/valida.js"></script>
	<script>
		function fnIr(){
			window.open("http://aplicaciones.nafin.com/docman/","_blank","");
			window.close();
			
		}
	</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<table width="280">
	<tr>
		<td align="center">
				<br><br>
				<table cellpadding="4" cellspacing="0" border="1" bordercolor="#A5B8BF">
					<tr>
						<td class="celda01" align="center">No se encontr&oacute; ning&uacute;n registro.</td>
					</tr>
				</table>
				<br>
				<table>
<% if(version.equals("ext")){%>				
					<tr><td height="20" width="100" align="center">&nbsp;</td></tr>
<% }else{%>
					<tr><td class="celda02" height="20" width="100" align="center"><a href="javascript:window.close();">Cerrar</a></td></tr>
<% }%>
				</table>
			<%}%>
		</td>
	</tr>
</table>
</body>
</html>
<%
if(rs!=null)rs.close();
if(ps!=null)ps.close();

jsonObj.put("success", new Boolean(true));
jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
   
}catch(Exception e){
  con.terminaTransaccion(false);
  e.printStackTrace(); 
  out.println("Error::: "+e);
}
finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
}
%>

<script>
window.resizable='yes';
</script>
