Ext.onReady(function(){

	var idMenu =  Ext.getDom('idMenu').value;

	/**
     * Funcion que recoje los valores pasados por get de una url.
     * Tiene que recibir el nombre de la variable a devolver su valor.
     * Ejemplo: Si la url es del tipo:
     *  http://lawebdelprogramador.com/index.php?nombre=valor
     * Podemos llamar a la funcion de esta manera:
     *  getURLParameter("nombre")
     */
	function getURLParameter(name){
        return decodeURI(
            (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
        );
    }
	
	 var estado,paisO, pais,municipio,ciudad,identificacion,claveSector,claveSubSector,claveRama,claveClase,tipoCredito,ic_domicilio;
		var sectorEconomico,subSectorEconomico,rama,lcase;
	 var tipo='' ;// usada en el fieldLabel de Numero de (distibuidor/proveedor) asignado por la EPO
	 var tipoPersona, tipoAfiliacion, actualizaSirac,RFC;
	var primera_vez_estado = true, primera_vez_municipio = true, primera_vez_ciudad = true;
	var primera_vez_subsector = true, primera_vez_rama = true, primera_vez_clase=true;
//------------------------------------------------------------------------------
//------------------------------HANDLER's---------------------------------------
//------------------------------------------------------------------------------


	
	
	
	var procesarIdentificacion= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catalogo = Ext.getCmp('cboCatalogoIdentificacion');
			if(catalogo.getValue()==''){
				catalogo.setValue(identificacion);
				
			}
		}
	}
	
	var  procesarActualizar= function(opts, success, response)    {
		var cmpForma = Ext.getCmp('formaDatosModificar');
		cmpForma.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData.msg==''){
				Ext.Msg.alert('','Los nuevos datos fueron almacenados', function() {
				window.location = '15formaDist05ext.jsp';
			}, this);		
			}else{
				Ext.Msg.alert('','Ocurrio un error durante la actualizacion de datos', function() {
				window.location = '15formaDist05ext.jsp';
			}, this);		
			
			}
			
			
		}
		else {
			Ext.Msg.alert('','Ocurrio un error durante la actualizacion de datos.');	
		}
	}
	
	
	function procesarPermiso(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var  info = Ext.util.JSON.decode(response.responseText);			
			 
			if(info.ccPermiso=='BTNACTUALIZANE_DM'){  	actualizarNAE(); 		}
			if(info.ccPermiso=='BTNACTUASIRAC_DM'){  	actualizaSirac(); 		}
			
				
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function validaPermisos(ccPermiso) {	
		Ext.Ajax.request({
			url: '15formaDistModificar05ext.data.jsp', 
			params: {
				informacion: "validaPermiso", 
				idMenuP:idMenu,
				ccPermiso:ccPermiso					
			},
			callback: procesarPermiso
		});
	}
	
	
	
	function actualizaSirac() {	
	
						
					var cmpForma = Ext.getCmp('formaDatosModificar');
					var todoCorrecto=false;
					/********************** Validaciones ********************/
					if(tipoPersona=='M'){
						var actualiza=(Ext.getCmp('chkInvalidar').getValue());
						var rfcActual= Ext.getCmp('txtRFC').getValue();//persona moral
						var rfcSinModificar=(rfcActual==RFC);
						if((Ext.getCmp('txtRFC').getValue()==(RFC))){
							actualizaSirac='S';
						}else if(actualiza){
							Ext.Msg.alert('Actualizar N@E/Sirac ',"No se puede actualizar en SIRAC un registro de N@E invalidado");
							return;
						}else if(rfcSinModificar==false){
							Ext.Msg.alert('Actualizar N@E/Sirac ',"EL RFC fue modificado. No se puede actualizar en SIRAC");
							return;
						}
						//Datos de la empresa
						 if(Ext.getCmp('txtRazonSocial').getValue()==""){
							return;
						}else if(Ext.getCmp('txtRFC').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNumEscritura').getValue()==""){
							return;
						}else if(Ext.getCmp('txtFechaConstitucion').getValue()==""){
							return;
						}//Domicilio
						else if(Ext.getCmp('txtCalle').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoEstado').getValue()==""){
							return;
						}else if(Ext.getCmp('txtCodigoPostal').getValue()==""){
							return;
						}else if(Ext.getCmp('txtColonia').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoDelegacion').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoPais').getValue()==""){
							return;
						}else if(Ext.getCmp('txtTelefono').getValue()==""){
							return;
						}//Datos del representante legal
						else if(Ext.getCmp('txtApPaterno').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNombre').getValue()==""){
							return;
						}else if(Ext.getCmp('txtTelefonoRep').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNumIdentificacion').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNumEscritura').getValue()==""){
							return;
						}else if(Ext.getCmp('txtApMaterno').getValue()==""){
							return;
						}else if(Ext.getCmp('txtRFCRep').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoIdentificacion').getValue()==""){
							return;
						}else if(Ext.getCmp('txtFechaPoderNotarial').getValue()==""){
							return;
						}//Datos del contacto
						else if(Ext.getCmp('txtApPaternoContacto').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNombreContacto').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoTipoCategoria').getValue()==""){
							return;
						}else if(Ext.getCmp('txtApMaternoContacto').getValue()==""){
							return;
						}else if(Ext.getCmp('txtTelefonoContacto').getValue()==""){
							return;
						}else if(Ext.getCmp('txtEmailContacto').getValue()==""){
							return;
						}//Informacion inicial para el alta del credito
						else if(Ext.getCmp('txtVentasNetas').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNumEmpleados').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoSubSector').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoClase').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoTipoEmpresa')==""){
							return;
						}else if(Ext.getCmp('txtNumDistribuidor').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoSectorEconomico').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoRama').getValue()==""){
							return;
						}else if(Ext.getCmp('txtProductosPrincipales').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNumDistribuidor').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoTipoCredito').getValue()==""){
							Ext.getCmp('cboCatalogoTipoCredito').markInvalid("Debe seleccionar el tipo de credito");
							return;
						}else{
							todoCorrecto=true;
						}
					}else {//TIPO PERSONA F
						
						var actualiza=(Ext.getCmp('invalidar_modF1').getValue());
						var rfcActual= Ext.getCmp('rfc_modF1').getValue();//persona moral
						var rfcSinModificar=(rfcActual==RFC);
						if((Ext.getCmp('rfc_modF1').getValue()==(RFC))){
							actualizaSirac='S';
						}else if(actualiza){
							Ext.Msg.alert('Actualizar N@E/Sirac ',"No se puede actualizar en SIRAC un registro de N@E invalidado");
							return;
						}else if(rfcSinModificar==false){
							Ext.Msg.alert('Actualizar N@E/Sirac ',"EL RFC fue modificado. No se puede actualizar en SIRAC");
							return;
						}
						//NOMBRE COMPLETO
						if(Ext.getCmp('txtApellidiPaternoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtNombreFisica').getValue()==""){
							return
						}else if(Ext.getCmp('sexo_mod1').getValue()==""){
							return
						}else if(Ext.getCmp('txtFechaNacioFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoGradoEscolar').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoIdentificacionFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtApMaternoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('rfc_modF1').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatlogoEstadoCivil').getValue()==""){
							return
						}else if(Ext.getCmp('txtNumIdentificacionFisica').getValue()==""){
							return
						}//dOMICILIO
						else if(Ext.getCmp('txtCalleFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoEstadoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtCodigoPostalFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtColoniaFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoDelegacionFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoPaisFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtTelefonoFisica').getValue()==""){
							return
						}//DATOS DEL CONTACTO
						else if(Ext.getCmp('txtApPaternoContactoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtNombreContactoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtApMaternoContactoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtTelefonoContactoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtEmailContactoFisica').getValue()==""){
							return
						}//OTROS DATOS
						else if(Ext.getCmp('txtVentasNetasFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtNumEmpleadosFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoSubSectorFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoClaseFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoTipoEmpresaFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtNumDistribuidorFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoSectorEconomicoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoRamaFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtProductosPrincipalesFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoTipoCreditoFisica').getValue()==""){
							Ext.getCmp('cboCatalogoTipoCreditoFisica').markInvalid("Debe seleccionar el tipo de credito");
							return;
						}else{
							todoCorrecto=true;
						}
					}
					if(todoCorrecto){
						cmpForma.el.mask("Procesando", 'x-mask-loading');
						Ext.Ajax.request({
								url: '15formaDistModificar05ext.data.jsp',
								params:Ext.apply(fpDatosModificar.getForm().getValues(), {//fpDatosModificar
										informacion: 'actualizarSirac',
										clavePyme:getURLParameter("clavePyme"),
										claveEpo	:getURLParameter("claveEpo"),
										tipoAfiliacion:tipoAfiliacion,
										tipoPersona:tipoPersona,
										razonSocial:razonSocial,
										numSirac:numSirac,
										ic_domicilio:ic_domicilio,
										claveContacto:claveContacto,
										CG_NO_ESCRITURA:CG_NO_ESCRITURA,
										actualizaSirac:actualizaSirac
								}),
								callback:procesarActualizar
						});
					}
		}
	function actualizarNAE() {
	
	
					var cmpForma = Ext.getCmp('formaDatosModificar');
					var todoCorrecto=false;
					/********************** Validaciones ********************/
					if(tipoPersona=='M'){
						//Datos de la empresa
						if(Ext.getCmp('txtRazonSocial').getValue()==""){
							return;
						}else if(Ext.getCmp('txtRFC').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNumEscritura').getValue()==""){
							return;
						}else if(Ext.getCmp('txtFechaConstitucion').getValue()==""){
							return;
						}//Domicilio
						else if(Ext.getCmp('txtCalle').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoEstado').getValue()==""){
							return;
						}else if(Ext.getCmp('txtCodigoPostal').getValue()==""){
							return;
						}else if(Ext.getCmp('txtColonia').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoDelegacion').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoPais').getValue()==""){
							return;
						}else if(Ext.getCmp('txtTelefono').getValue()==""){
							return;
						}//Datos del representante legal
						else if(Ext.getCmp('txtApPaterno').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNombre').getValue()==""){
							return;
						}else if(Ext.getCmp('txtTelefonoRep').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNumIdentificacion').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNumEscritura').getValue()==""){
							return;
						}else if(Ext.getCmp('txtApMaterno').getValue()==""){
							return;
						}else if(Ext.getCmp('txtRFCRep').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoIdentificacion').getValue()==""){
							return;
						}else if(Ext.getCmp('txtFechaPoderNotarial').getValue()==""){
							return;
						}//Datos del contacto
						else if(Ext.getCmp('txtApPaternoContacto').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNombreContacto').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoTipoCategoria').getValue()==""){
							return;
						}else if(Ext.getCmp('txtApMaternoContacto').getValue()==""){
							return;
						}else if(Ext.getCmp('txtTelefonoContacto').getValue()==""){
							return;
						}else if(Ext.getCmp('txtEmailContacto').getValue()==""){
							return;
						}//Informacion inicial para el alta del credito
						else if(Ext.getCmp('txtVentasNetas').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNumEmpleados').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoSubSector').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoClase').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoTipoEmpresa')==""){
							return;
						}else if(Ext.getCmp('txtNumDistribuidor').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoSectorEconomico').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoRama').getValue()==""){
							return;
						}else if(Ext.getCmp('txtProductosPrincipales').getValue()==""){
							return;
						}else if(Ext.getCmp('txtNumDistribuidor').getValue()==""){
							return;
						}else if(Ext.getCmp('cboCatalogoTipoCredito').getValue()==""){
							Ext.getCmp('cboCatalogoTipoCredito').markInvalid("Debe seleccionar el tipo de credito");
							return;
						}else{
							todoCorrecto=true;
						}
					}else {
						//NOMBRE COMPLETO
						if(Ext.getCmp('txtApellidiPaternoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtNombreFisica').getValue()==""){
							return
						}else if(Ext.getCmp('sexo_mod1').getValue()==""){
							return
						}else if(Ext.getCmp('txtFechaNacioFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoGradoEscolar').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoIdentificacionFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtApMaternoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('rfc_modF1').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatlogoEstadoCivil').getValue()==""){
							return
						}else if(Ext.getCmp('txtNumIdentificacionFisica').getValue()==""){
							return
						}//dOMICILIO
						else if(Ext.getCmp('txtCalleFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoEstadoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtCodigoPostalFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtColoniaFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoDelegacionFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoPaisFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtTelefonoFisica').getValue()==""){
							return
						}//DATOS DEL CONTACTO
						else if(Ext.getCmp('txtApPaternoContactoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtNombreContactoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtApMaternoContactoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtTelefonoContactoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtEmailContactoFisica').getValue()==""){
							return
						}//OTROS DATOS
						else if(Ext.getCmp('txtVentasNetasFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtNumEmpleadosFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoSubSectorFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoClaseFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoTipoEmpresaFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtNumDistribuidorFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoSectorEconomicoFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoRamaFisica').getValue()==""){
							return
						}else if(Ext.getCmp('txtProductosPrincipalesFisica').getValue()==""){
							return
						}else if(Ext.getCmp('cboCatalogoTipoCreditoFisica').getValue()==""){
							Ext.getCmp('cboCatalogoTipoCreditoFisica').markInvalid("Debe seleccionar el tipo de credito");
							return;
						}else{
							todoCorrecto=true;
						}
					}
					if(todoCorrecto){
					cmpForma.el.mask("Procesando", 'x-mask-loading');
					Ext.Ajax.request({
									url: '15formaDistModificar05ext.data.jsp',
									params:Ext.apply(fpDatosModificar.getForm().getValues(), {//fpDatosModificar
											informacion: 'actualizarNAE',
											clavePyme:getURLParameter("clavePyme"),
											claveEpo	:getURLParameter("claveEpo"),
											tipoAfiliacion:tipoAfiliacion,
											tipoPersona:tipoPersona,
											//razonSocial:razonSocial,
											numSirac:numSirac,
											ic_domicilio:ic_domicilio,
											claveContacto:claveContacto,
											CG_NO_ESCRITURA:CG_NO_ESCRITURA,
											actualizaSirac:actualizaSirac
									}),
									callback:procesarActualizar
					});
					}
					
	
	
	}
	
	
	
 function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
			
				var  total= jsonData.permisos.length;				 
				for(i=0;i<total;i++){ 
					boton=  jsonData.permisos[i];				
					Ext.getCmp(boton).show();	 	
				}			
			
			
				identificacion=jsonData.registros[0].IC_IDENTIFICACION_RL;
				pais=jsonData.registros[0].IC_PAIS;
				estado=jsonData.registros[0].IC_ESTADO;
				municipio=jsonData.registros[0].MUNICIPIO;
				ciudad=jsonData.registros[0].CIUDAD;
				sectorEconomico=jsonData.registros[0].IC_SECTOR_ECON;
				claveSector=jsonData.registros[0].IC_SECTOR_ECON;
				claveSubSector=jsonData.registros[0].IC_SUBSECTOR;
				claveRama=jsonData.registros[0].IC_RAMA;
				claveClase=jsonData.registros[0].IC_CLASE;
				tipoCredito=jsonData.registros[0].TIPO_CREDITO_EPO;
				tipoPersona=jsonData.registros[0].CS_TIPO_PERSONA;
				tipoAfiliacion=jsonData.registros[0].CS_TIPO_AFILIA_DISTRI;
				razonSocial=jsonData.registros[0].CG_RAZON_SOCIAL;
				numSirac=jsonData.registros[0].IN_NUMERO_SIRAC;
				claveContacto=jsonData.registros[0].IC_CONTACTO;
				CG_NO_ESCRITURA=jsonData.registros[0].CG_NO_ESCRITURA;
				RFC=jsonData.registros[0].CG_RFC;
				 ic_domicilio=jsonData.registros[0].IC_DOMICILIO;
				
				
				if(jsonData.registros[0].CS_TIPO_PERSONA=='M'){
					//OCULTAR forma para tipoPerosona Fisica
					Ext.getCmp('datosEmpresa').show();
					Ext.getCmp('domicilio').show();
					Ext.getCmp('datosRepLegal').show();
					Ext.getCmp('datosContacoto').show();
					Ext.getCmp('infoInicialAltaCredito').show();
					
					Ext.getCmp('nombreCompletoFisica').hide();
					Ext.getCmp('domicilioFisica').hide();
					Ext.getCmp('datosContactoFisica').hide();
					Ext.getCmp('datosOtrosFisica').hide();
					
					//datos de la empresa
					Ext.getCmp('txtRazonSocial').setValue(jsonData.registros[0].CG_RAZON_SOCIAL);
					Ext.getCmp('txtRFC').setValue(jsonData.registros[0].CG_RFC);
					Ext.getCmp('txtFiel').setValue(jsonData.registros[0].FIEL);
					
					Ext.getCmp('txtNumEscritura').setValue(jsonData.registros[0].CG_NO_ESCRITURA);
					Ext.getCmp('txtFechaConstitucion').setValue(jsonData.registros[0].DF_CONSTITUCION);
					Ext.getCmp('cboCatalogoPaisOrigen').setValue(jsonData.registros[0].IC_PAIS_ORIGEN);
					//DATOS DOMICILIO 
					Ext.getCmp('txtCalle').setValue(jsonData.registros[0].CG_CALLE);//OK
					//Ext.getCmp('cboCatalogoEstado').setValue(jsonData.registros[0].IC_ESTADO);//? IC_ESTADO
					Ext.getCmp('txtCodigoPostal').setValue(jsonData.registros[0].CN_CP);//OK
					//Ext.getCmp('cbocatalogoCiudad').setValue(jsonData.registros[0].CIUDAD);
					Ext.getCmp('txtemail').setValue(jsonData.registros[0].CG_EMAIL);//OK
					
					Ext.getCmp('txtColonia').setValue(jsonData.registros[0].CG_COLONIA);//OK
					//Ext.getCmp('cboCatalogoDelegacion').setValue(jsonData.registros[0].MUNICIPIO);//OK
					Ext.getCmp('cboCatalogoPais').setValue(jsonData.registros[0].IC_PAIS);//OK
					Ext.getCmp('txtTelefono').setValue(jsonData.registros[0].CG_TELEFONO1);//OK
					Ext.getCmp('txtFax').setValue(jsonData.registros[0].CG_FAX);//OK
					//DATOS DEL REPRESENTANTE LEGAL
					Ext.getCmp('txtApPaterno').setValue(jsonData.registros[0].CG_APPAT_REP_LEGAL);//OK
					Ext.getCmp('txtNombre').setValue(jsonData.registros[0].CG_NOMBREREP_LEGAL);//OK
					Ext.getCmp('txtTelefonoRep').setValue(jsonData.registros[0].CG_TELEFONO_RL);//OK
					Ext.getCmp('txtemailRep').setValue(jsonData.registros[0].CG_EMAIL_RL);//OK
					Ext.getCmp('txtNumIdentificacion').setValue(jsonData.registros[0].CG_NO_IDENTIFICACION_RL);//OK
					Ext.getCmp('txtNumEscrituraRep').setValue(jsonData.registros[0].CG_NO_ESCRITURA_RL);//OK

					Ext.getCmp('txtApMaterno').setValue(jsonData.registros[0].CG_APMAT_LEGAL);//OK
					Ext.getCmp('txtRFCRep').setValue(jsonData.registros[0].CG_RFC_LEGAL);//OK
					Ext.getCmp('txtfaxRep').setValue(jsonData.registros[0].CG_FAX_RL);//OK
					Ext.getCmp('cboCatalogoIdentificacion').setValue(jsonData.registros[0].IC_IDENTIFICACION_RL);//OK
					Ext.getCmp('txtFechaPoderNotarial').setValue(jsonData.registros[0].DF_PODER_NOTARIAL_RL);//OK
					//DATOS DEL CONTACTO
					Ext.getCmp('txtApPaternoContacto').setValue(jsonData.registros[0].APPAT_C);
					Ext.getCmp('txtNombreContacto').setValue(jsonData.registros[0].NOMBRE_C);
					Ext.getCmp('txtFaxContacto').setValue(jsonData.registros[0].FAX_C);
					Ext.getCmp('cboCatalogoTipoCategoria').setValue(jsonData.registros[0].IC_TIPO_CATEGORIA);//? OK
										
					Ext.getCmp('txtApMaternoContacto').setValue(jsonData.registros[0].APMAT_C);//OK
					Ext.getCmp('txtTelefonoContacto').setValue(jsonData.registros[0].TEL_C);//OK
					Ext.getCmp('txtEmailContacto').setValue(jsonData.registros[0].EMAIL_C);//OK
					
					//Informaci�n inicial para el alta del cr�dito
					
					Ext.getCmp('txtNumSIRAC').setValue(jsonData.registros[0].IN_NUMERO_SIRAC);	//OK
					Ext.getCmp('txtVentasNetas').setValue(jsonData.registros[0].FN_VENTAS_NET_TOT);//OK
					Ext.getCmp('txtNumEmpleados').setValue(jsonData.registros[0].IN_NUMERO_EMP);	//OK
					//Ext.getCmp('cboCatalogoSubSector').setValue(jsonData.registros[0].IC_SUBSECTOR);	//0K
					//Ext.getCmp('cboCatalogoClase').setValue(jsonData.registros[0].IC_CLASE);//OK	
					Ext.getCmp('cboCatalogoTipoEmpresa').setValue(jsonData.registros[0].IC_TIPO_EMPRESA);	//OK
					Ext.getCmp('txtNumDistribuidor').setValue(jsonData.registros[0].NUM_DISTRIBUIDOR);	//OK
					Ext.getCmp('cboCatalogoOficinaTramitadora').setValue(jsonData.registros[0].IC_OFICINA_TRAMITADORA);	//OK
					
					Ext.getCmp('chkFinanDistribuidores').setValue(jsonData.registros[0].FINANCIAMIENTO);	//? OK
					Ext.getCmp('cboCatalogoSectorEconomico').setValue(jsonData.registros[0].IC_SECTOR_ECON);//OK	
					//Ext.getCmp('cboCatalogoRama').setValue(jsonData.registros[0].IC_RAMA);//OK	
					Ext.getCmp('txtProductosPrincipales').setValue(jsonData.registros[0].CG_PRODUCTOS);	//OK
					//Ext.getCmp('cboCatalogoTipoCredito').setValue(jsonData.registros[0].TIPO_CREDITO_EPO);	
					if(jsonData.registros[0].IC_TIPO_CLIENTE!='2'){
						Ext.getCmp('txtNumSIRAC').show();	//OK
						Ext.getCmp('chkFinanDistribuidores').show();	//? OK
						
					}
					
				}else if(jsonData.registros[0].CS_TIPO_PERSONA=='F'){
					Ext.getCmp('nombreCompletoFisica').show();
					Ext.getCmp('domicilioFisica').show();
					Ext.getCmp('datosContactoFisica').show();
					Ext.getCmp('datosOtrosFisica').show();
				
					Ext.getCmp('datosEmpresa').hide();
					Ext.getCmp('domicilio').hide();
					Ext.getCmp('datosRepLegal').hide();
					Ext.getCmp('datosContacoto').hide();
					Ext.getCmp('infoInicialAltaCredito').hide();
					
					//nombre personal
					Ext.getCmp('txtApellidiPaternoFisica').setValue(jsonData.registros[0].CG_APPAT);
					Ext.getCmp('txtNombreFisica').setValue(jsonData.registros[0].CG_NOMBRE);
					Ext.getCmp('txtCurpFisica').setValue(jsonData.registros[0].CURP);
					Ext.getCmp('sexo_mod1').setValue(jsonData.registros[0].CG_SEXO);
					Ext.getCmp('txtFechaNacioFisica').setValue(jsonData.registros[0].DF_NACIMIENTO);
					Ext.getCmp('cboCatalogoGradoEscolar').setValue(jsonData.registros[0].IC_GRADO_ESC);
					Ext.getCmp('cboCatalogoIdentificacionFisica').setValue(jsonData.registros[0].IC_IDENTIFICACION_RL);
					
					Ext.getCmp('txtApMaternoFisica').setValue(jsonData.registros[0].CG_APMAT);
					Ext.getCmp('rfc_modF1').setValue(jsonData.registros[0].CG_RFC);
					//Ext.getCmp('invalidar_modF1').setValue(jsonData.registros[0].CG_NO_ESCRITURA);
					Ext.getCmp('txtFielFisica').setValue(jsonData.registros[0].FIEL);
					Ext.getCmp('cboCatlogoRegimenMatrimonial').setValue(jsonData.registros[0].CG_REGIMEN_MAT);
					Ext.getCmp('cboCatlogoEstadoCivil').setValue(jsonData.registros[0].IC_ESTADO_CIVIL);
					Ext.getCmp('cboCatalogoPaisOrigenFisica').setValue(jsonData.registros[0].IC_PAIS_ORIGEN);
					Ext.getCmp('txtNumIdentificacionFisica').setValue(jsonData.registros[0].CG_NO_IDENTIFICACION_RL);
					
					//Domicilio
					Ext.getCmp('txtCalleFisica').setValue(jsonData.registros[0].CG_CALLE);//OK
					//Ext.getCmp('cboCatalogoEstadoFisica').setValue(jsonData.registros[0].IC_ESTADO);//? IC_ESTADO
					Ext.getCmp('txtCodigoPostalFisica').setValue(jsonData.registros[0].CN_CP);//OK
					//Ext.getCmp('cboCatalogoCiudadFisica').setValue(jsonData.registros[0].CIUDAD);
					Ext.getCmp('txtemailFisica').setValue(jsonData.registros[0].CG_EMAIL);//OK
					Ext.getCmp('txtFaxContactoDomicilioFisica').setValue(jsonData.registros[0].CG_FAX);//OK
					
					Ext.getCmp('txtColoniaFisica').setValue(jsonData.registros[0].CG_COLONIA);//OK
					//Ext.getCmp('cboCatalogoDelegacionFisica').setValue(jsonData.registros[0].MUNICIPIO);//OK
					Ext.getCmp('cboCatalogoPaisFisica').setValue(jsonData.registros[0].IC_PAIS);//OK
					Ext.getCmp('txtTelefonoFisica').setValue(jsonData.registros[0].CG_TELEFONO1);//OK
					
					//DATOS DEL CONTACTO PERSONA TIPO F
					Ext.getCmp('txtApPaternoContactoFisica').setValue(jsonData.registros[0].APPAT_C);
					Ext.getCmp('txtNombreContactoFisica').setValue(jsonData.registros[0].NOMBRE_C);
					Ext.getCmp('txtFaxContactoFisica').setValue(jsonData.registros[0].FAX_C);
										
					Ext.getCmp('txtApMaternoContactoFisica').setValue(jsonData.registros[0].APMAT_C);//OK
					Ext.getCmp('txtTelefonoContactoFisica').setValue(jsonData.registros[0].TEL_C);//OK
					Ext.getCmp('txtEmailContactoFisica').setValue(jsonData.registros[0].EMAIL_C);//OK
					
					//OTROS DATOS
						// VALIDACION -- if ( TipoPYME.equals("2") )--
					//if(jsonData.registros[0].IC_TIPO_CLIENTE=='2'	){	
						
						Ext.getCmp('txtVentasNetasFisica').setValue(jsonData.registros[0].FN_VENTAS_NET_TOT);	//OK
						Ext.getCmp('txtNumEmpleadosFisica').setValue(jsonData.registros[0].IN_NUMERO_EMP);	//OK
						//Ext.getCmp('cboCatalogoSubSectorFisica').setValue(jsonData.registros[0].IC_SUBSECTOR);	//0K
						//Ext.getCmp('cboCatalogoClaseFisica').setValue(jsonData.registros[0].IC_CLASE);//OK	
						Ext.getCmp('cboCatalogoTipoEmpresaFisica').setValue(jsonData.registros[0].IC_TIPO_EMPRESA);	//OK
						Ext.getCmp('txtNumDistribuidorFisica').setValue(jsonData.registros[0].NUM_DISTRIBUIDOR);	//OK
						Ext.getCmp('cboCatalogoOficinaTramitadoraFisica').setValue(jsonData.registros[0].IC_OFICINA_TRAMITADORA);	//OK
						
						Ext.getCmp('cboCatalogoSectorEconomicoFisica').setValue(jsonData.registros[0].IC_SECTOR_ECON);	//? OK
						//Ext.getCmp('cboCatalogoRamaFisica').setValue(jsonData.registros[0].IC_RAMA);//OK	
						Ext.getCmp('txtProductosPrincipalesFisica').setValue(jsonData.registros[0].CG_PRODUCTOS);	//OK
						//Ext.getCmp('cboCatalogoTipoCreditoFisica').setValue(jsonData.registros[0].TIPO_CREDITO_EPO);
					
					//}	
					
				}
				
				if(jsonData.registros[0].IC_TIPO_CLIENTE=='2'	){	
					tipo='distribuidor';
				}else{tipo='proveedor';}

				catalogoTipoCredito.load({
					params : Ext.apply({
						claveEpo : getURLParameter('claveEpo')//jsonData.registros[0].TIPO_CREDITO_EPO
					})
				});
				
				//para el panel domicilio 
				catalogoEstado.load({
					params : Ext.apply({
						pais : pais
					})
				});
							
				catalogoMunicipio.load({
					params : Ext.apply({
						pais : pais,
						estado: estado
					})
				});
				catalogoCiudad.load({
					params : Ext.apply({
						pais : pais,
						estado: estado
					})
				});			
				//catalogoSectorEconomico.load();
				catalogoSubSector.load({
					params : Ext.apply({
						claveSector 	:	claveSector//Ext.getCmp('cboCatalogoSectorEconomicoFisica').gatValue()
					})
				});
						
				catalogoRama.load({
					params	:	Ext.apply({
						claveSector 	: claveSector,//Ext.getCmp('cboCatalogoSectorEconomicoFisica').gatValue(),
						claveSubSector	:	claveSubSector	//Ext.getCmp('cboCatalogoSubSectorFisica').gatValue()
					})
				});
					
				catalogoClase.load({
					params	:	Ext.apply({
						claveSector 	:	claveSector,// Ext.getCmp('cboCatalogoSectorEconomicoFisica').gatValue(),
						claveSubSector	:	claveSubSector,//Ext.getCmp('cboCatalogoSubSectorFisica').gatValue(),
						claveRama		:	claveRama//Ext.getCmp('cboCatalogoRamaFisica').gatValue()
					})
				});
				
			}	
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarCatalogoEstado= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			
			var catalogoM = Ext.getCmp('cboCatalogoEstado');
			var catalogoF = Ext.getCmp('cboCatalogoEstadoFisica');
			var clave =records[0].data.clave;
			if(primera_vez_estado ){
				catalogoM.setValue(estado);
			}
			if(primera_vez_estado){
				catalogoF.setValue(estado);
			}
		}
	} 
	var procesarCatalogoMunicipio= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catalogoM = Ext.getCmp('cboCatalogoDelegacion');
			var catalogoF = Ext.getCmp('cboCatalogoDelegacionFisica');
			if(primera_vez_municipio){
				catalogoM.setValue(municipio);
			}
			if(primera_vez_municipio){
				catalogoF.setValue(municipio);
			}
		}
	} 
	var procesarCatalogoCiudad= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catalogoM = Ext.getCmp('cbocatalogoCiudad');
			var catalogoF = Ext.getCmp('cboCatalogoCiudadFisica');
			if(primera_vez_ciudad){
				catalogoM.setValue(ciudad);
			}
			if(primera_vez_ciudad){
				catalogoF.setValue(ciudad);
			}
		}
	} 
	
	
	var procesarCatalogoSectorEconomico= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catalogoM = Ext.getCmp('cboCatalogoSubSectorEconomico');
			var catalogoF = Ext.getCmp('cboCatalogoSubSectorEconomicoFisica');
			if(catalogoM.getValue()==''){
				catalogoM.setValue(sectorEconomico);
			}
			if(catalogoF.getValue()==''){
				catalogoF.setValue(sectorEconomico);
			}
		}
	}
	var procesarCatalogoSubSector = function(store, records, oprion){
		
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catalogoM = Ext.getCmp('cboCatalogoSubSector');
			var catalogoF = Ext.getCmp('cboCatalogoSubSectorFisica');
			if(primera_vez_subsector){
				catalogoM.setValue(claveSubSector);
			}
			if(primera_vez_subsector){
				catalogoF.setValue(claveSubSector);
			}
		}
	}
	var procesarCatalogoRama = function(store, records, oprion){
		
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catalogoM = Ext.getCmp('cboCatalogoRama');
			var catalogoF = Ext.getCmp('cboCatalogoRamaFisica');
			if(primera_vez_rama){
				catalogoM.setValue(claveRama);
			}
			if(primera_vez_rama){
				catalogoF.setValue(claveRama);
			}
		}
	}
	
	var procesarCatalogoClase = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catalogoM = Ext.getCmp('cboCatalogoClase');
			var catalogoF = Ext.getCmp('cboCatalogoClaseFisica');
			var jsonData = store.reader.jsonData;
			
			if(primera_vez_clase){
				catalogoM.setValue(claveClase);
			}
			if(primera_vez_clase){
				catalogoF.setValue(claveClase);
			}
		}
	}
	
	var procesarCatalogoTipoCredito = function(store, records, option){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catalogo = Ext.getCmp('cboCatalogoTipoCredito');
			var catalogo1 = Ext.getCmp('cboCatalogoTipoCreditoFisica');
			if(tipoCredito==records[0].data['clave']){
				catalogo.setValue(tipoCredito);
				
			}
			if (tipoCredito==records[0].data['clave']){
						catalogo1.setValue(tipoCredito);
			}
		}		
	}
	
	
	
//------------------------------------------------------------------------------
//-----------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------
////CATALOGO GRADO DE ESCOLARIDAD
	var catalogoGradoEscolar = new Ext.data.JsonStore({
		id				: 'catalogoGradoEscolar',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoGradoEscolar'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo//,
		 //load 		: procesarCatalogoRamoSIAFF
		}
	});
	
//// CATALOGO REGIMEN MATRIMONIAL
	var catalogoRegimenMatrimonial = new Ext.data.JsonStore({
		id				: 'catalogoRegimenMatrimonial',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoRegimenMatrimonial'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo//,
		 //load 		: procesarCatalogoRamoSIAFF
		}
	});
//// CATALOGO ESTADO CIVIL
 var catlogoEstadoCivil = new Ext.data.JsonStore({
		id				: 'catlogoEstadoCivil',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catlogoEstadoCivil'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo//,
		 //load 		: procesarCatalogoRamoSIAFF
		}
	});

//catalogo Pais origen
	var catalogoPaisOrigen= new Ext.data.JsonStore({
		id				: 'catalogoPaisOrigen',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoPaisOrigen'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo
		}
	});
		// catalogo Estado
	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catalogoEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'},
		autoLoad		: false,
		listeners	:
		{
			load		:procesarCatalogoEstado,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
		//catalogo Municipio 0 delegacion
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catalogoMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
			load 		: procesarCatalogoMunicipio,
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo
		 
		}
	});
	
	// catalogo Ciudad
	var catalogoCiudad =  new Ext.data.JsonStore({
		id				: 'catalogoCiudad',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoCiudad'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoCiudad
		}
	});
	//catalogo pais
	var catalogoPais = new Ext.data.JsonStore({
		id				: 'catalogoPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoPais'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo
		}
	});
	//catalogo identificacion
	var catalogoIdentificacion = new Ext.data.JsonStore({
		id				: 'catalogoIdentificacion',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoIdentificacion'	},
		autoLoad		: false,
		listeners	:
		{
			load		:	procesarIdentificacion,
		 exception	: 	NE.util.mostrarDataProxyError,
		 beforeload	: 	NE.util.initMensajeCargaCombo
		
		}
	});
	// catalogo tipo categoria
	var catalogoTipoCategoria =  new Ext.data.JsonStore({
		id				: 'catalogoTipoCategoria',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoTipoCategoria'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo//,
		 //load 		: procesarCatalogoTipoCategoria
		}
	});
	//CATALOGO SECTOR ECONOMICO
	var catalogoSectorEconomico = new Ext.data.JsonStore({
		id				: 'catalogoSectorEconomico',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoSectorEconomico'	},
		totalProperty : 'total',
		autoLoad		: false,
		listeners	:
		{
			select: function(){
				Ext.getCmp('cboCatalogoSubSector').reset();
				Ext.getCmp('cboCatalogoRama').reset();
				Ext.getCmp('cboCatalogoClase').reset();
				
				Ext.getCmp('cboCatalogoSubSectorFisica').reset();
				Ext.getCmp('cboCatalogoRamaFisica').reset();
				Ext.getCmp('cboCatalogoClaseFisica').reset();
				primera_vez_subsector = false;
					catalogoSubSector.load({
						params : Ext.apply({
							claveSector : record.json.clave
						})
					});
			},
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo//,
		 //load			: procesarCatalogoSectorEconomico
		}
	});
	//CATALOGO SUBSECTOR
	var catalogoSubSector = new Ext.data.JsonStore({
		id				: 'catalogoSubSector',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoSubSector'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoSubSector
		}
	});
	//CATALOGO RAMA
	var catalogoRama = new Ext.data.JsonStore({
		id				: 'catalogoRama',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoRama'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoRama
		}
	});
	//CATALOGO CLASE M
	var catalogoClase = new Ext.data.JsonStore({
		id				: 'catalogoClase',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoClase'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoClase
		}
	}); 
	
	//CATALOGO TIPO EMPRESA
	var catalogoTipoEmpresa = new Ext.data.JsonStore({
		id				: 'catalogoTipoEmpresa',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoTipoEmpresa'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo//,
		 //load 		: procesarCatalogoTipoEmpresa
		}
	}); 
	//CATALOGO OFICINA TRAMITADORA
	var catalogoOficinaTramitadora = new Ext.data.JsonStore({
		id				: 'catalogoOficinaTramitadora',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoOficinaTramitadora'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo//,
		 //load 		: procesarCatalogoOficinaTramitadora
		}
	});
	//CATALOGO TIPO CREDITO
	var catalogoTipoCredito = new Ext.data.JsonStore({
		id				: 'catalogoTipoCredito',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoTipoCredito'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load			:procesarCatalogoTipoCredito
		}
	});
	// CATALOGO PAIS
	var catalogoPais = new Ext.data.JsonStore({
		id				: 'catalogoPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15formaDistModificar05ext.data.jsp',
		baseParams	: { informacion: 'catalogoPais'	},
		autoLoad		: false,
		listeners	:
		{
			select: function(combo, record, index) {
				Ext.getCmp('cboCatalogoEstado').reset();
				Ext.getCmp('cboCatalogoDelegacion').reset();
				Ext.getCmp('cbocatalogoCiudad').reset();
				
				Ext.getCmp('cboCatalogoEstadoFisica').reset();
				Ext.getCmp('cboCatalogoDelegacionFisica').reset();
				Ext.getCmp('cboCatalogoCiudadFisica').reset();
				primera_vez_estado = false;
					catalogoEstado.load({
						params : Ext.apply({
							pais : record.json.clave
						})
					});
					
					
			}
		}
	});
//------------------------------------------------------------------------------
//---------------------------COMPONENTES----------------------------------------
//------------------------------------------------------------------------------
	//___________TIPO DE PERSONA FISICA_________________________________________
	var datosNombreCompletoPersonaFisicaPanelIzq =(
		{
			xtype			:'fieldset',
			id				: 'datosNombreCompletoPersonaFisicaPanelIzq',
			border		:false,
			labelWidth	: 150,
			items			: [
				{	
					xtype: 'textfield',
					msgTarget: 'side',
					name: 'apellidoPaternoFisica',
					id: 'txtApellidiPaternoFisica',
					fieldLabel	: '* Apellido paterno:',
					maxLength	: 100,
					width			: 230,
					allowBlank	: false,
					tabIndex		:	1
				},
				{
					xtype			: 'textfield',
					id          : 'txtNombreFisica',
					name			: 'nombreFisica',
					fieldLabel  : '* Nombre(s) ',
					width			: 230,
					maxLength	: 30,
					margins		: '0 20 0 0',
					allowBlank	: false,
					msgTarget: 'side',
					tabIndex		:	3
				},
				{
					xtype			: 'textfield',
					id          : 'txtCurpFisica',
					name			: 'curpFisica',
					fieldLabel  : 'CURP ',
					width			: 230,
					maxLength	: 18,
					margins		: '0 20 0 0',
					allowBlank	: true,
					msgTarget: 'side',
					tabIndex		:	3
				},
				{	
					width:200,	
					heigth: 300,
					msgTarget: 'side',
					mode: 'local',
					fieldLabel	:'* Sexo',
					xtype:'radiogroup',	
					style: { width: '80%', 	marginLeft: '10px'},	
					align:'center',	
					id:'sexo_mod1',	
					name:'Sexo',	
					cls:'x-check-group-alt',	
					items: 
						[  
							{	boxLabel	: 'F',	name : 'Sexo', inputValue: 'F' },
							{	boxLabel	: 'M',	name : 'Sexo', inputValue: 'M' }
						],
					allowBlank	: false,
					tabIndex	:5
				},
				{
					xtype			: 'datefield',
					id          : 'txtFechaNacioFisica',
					name			: 'fechaNacioFisica',
					allowBlank	: false,
					width			: 130,
					fieldLabel  : '* Fecha de nacimiento ',
					margins		: '0 20 0 0',
					tabIndex		: 7
				},
				{
					xtype				: 'combo',
					id          	: 'cboCatalogoGradoEscolar',
					name				: 'catalogoGradoEscolar',
					hiddenName 		: 'catalogoGradoEscolar',
					fieldLabel  	: '* Grado de Escolaridad',
					forceSelection	: true,
					allowBlank		: false,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccione ...',
					width				: 150,
					tabIndex			: 25,
					store				: catalogoGradoEscolar,
					listeners: {}
				},
				{
					xtype				: 'combo',
					id          	: 'cboCatalogoIdentificacionFisica',
					name				: 'catalogoIdentificacionFisica',
					hiddenName 		: 'catalogoIdentificacionFisica',
					fieldLabel  	: '* Identfiaci�n',
					forceSelection	: true,
					allowBlank		: false,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccione ...',
					width				: 150,
					tabIndex			: 25,
					store				: catalogoIdentificacion,
					listeners: {}
				}
			]
		}
	);
	var datosNombreCompletoPersonaFisicaPanelDer =(
		{
			xtype		: 'fieldset',
			id 		: 'datosNombreCompletoPersonaFisicaPanelDer',
			border	: false,
			labelWidth	: 150,
			items		: 
			[
				{
					xtype			: 'textfield',
					id          : 'txtApMaternoFisica',
					name			: 'apMaternoFisica',
					fieldLabel  : '* Apellido materno:',
					width			: 230,
					maxLength	: 25,
					margins		: '0 20 0 0',
					allowBlank	: false,
					tabIndex		:	2
				},
				{
					xtype: 'compositefield',
					msgTarget: 'side',
					combineErrors: false,
					items: [
						{	
							width: 150,	
							xtype: 'textfield',
							maxLength: 20,
							msgTarget: 'side',
							fieldLabel	:'* RFC',
							margins: '0 10 0 0',
							tabIndex	:4,
							name: 'rfcFisica',
							id: 'rfc_modF1',
							regex	:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
							regexText:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
										  'en el formato NNN-AAMMDD-XXX donde:<br>'+
										  'NNN:son las iniciales del nombre de la empresa<br>'+
										  'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
										  'XXX:es la homoclave'
						},{	
							width: 80,
							xtype: 'checkbox',	
							boxLabel: 'Invalidar',
							name: 'invalidar_modF',
							id: 'invalidar_modF1',
							listeners: {check: function(){
								Ext.getCmp('rfc_modF1').setValue('XXXX-000101-XXX');
								Ext.getCmp('actualiza_sirac1').setValue('');
							}}
						},{xtype: 'displayfield', value: '', width: 237}	
					]
				},
				{
					xtype			: 'textfield',
					name			: 'fielFisica',
					id				: 'txtFielFisica',
					fieldLabel	: 'FIEL',
					maxLength	: 25,
					width			: 100,
					allowBlank	: true,
					tabIndex		:	6
				},
				{
					xtype: 'combo',
					name: 'catlogoRegimenMatrimonial',
					id: 'cboCatlogoRegimenMatrimonial',
					hiddenName: 'catlogoRegimenMatrimonial',
					fieldLabel	:'R�gimen Matrimonial',
					forceSelection: true,
					allowBlank: true,
					triggerAction: 'all',
					msgTarget: 'side',
					mode: 'local',
					valueField: 'clave',
					displayField: 'descripcion',
					width: 230,
					tabIndex			: 8,
					store: catalogoRegimenMatrimonial
				},
				{
					xtype: 'combo',
					name: 'catlogoEstadoCivil',
					id: 'cboCatlogoEstadoCivil',
					hiddenName: 'catlogoEstadoCivil',
					fieldLabel	:'* Estado civil:: ',
					forceSelection: true,
					allowBlank: false,
					triggerAction: 'all',
					msgTarget: 'side',
					mode: 'local',
					valueField: 'clave',
					displayField: 'descripcion',
					width: 230,
					tabIndex			: 10,
					store: catlogoEstadoCivil
				},
				{
					xtype				: 'combo',
					id          	: 'cboCatalogoPaisOrigenFisica',
					name				: 'catalogoPaisOrigenFisica',
					hiddenName 		: 'catalogoPaisOrigenFisica',
					fieldLabel  	: 'Pa�s de origen',
					forceSelection	: true,
					allowBlank		: true,
					triggerAction	: 'all',
					mode				: 'local',
					width				: 150,
					emptyText		: 'Seleccionar...',
					valueField		: 'clave',
					displayField	: 'descripcion',
					autoWidth		: true,
					tabIndex			: 12,
					store				: catalogoPaisOrigen
				},
				{
					xtype			:'numberfield',
					name			: 'numIdentificacionFisica',
					id				: 'txtNumIdentificacionFisica',
					fieldLabel	: '* No. de Identificaci�n: ',
					maxLength	: 30,
					width			: 100,
					allowBlank	: false,
					tabIndex		:	14				
				}
			]
		}
	);			
	//FIN PANEL NOMBRE COMPLETO
	
	var datosDomicilioPersonaFisicaPanelIzq =(
		{
			xtype		: 'fieldset',
			id 		: 'datosDomicilioPersonaFisicaPanelIzq',
			border	: false,
			labelWidth	: 150,
			items		:[
				{
					xtype			: 'textfield',
					id          : 'txtCalleFisica',
					name			: 'calleFisica',
					fieldLabel  : '* Calle, No. Exterior y No. Interior:',
					maxLength	: 100,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: false,
					tabIndex		:15
	
				},
				{
					xtype				: 'combo',
					id          	: 'cboCatalogoEstadoFisica',
					name				: 'catalogoEstadoFisica',
					hiddenName 		: 'catalogoEstadoFisica',
					fieldLabel  	: '* Estado ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					typeAhead		: true,
					allowBlank		: false,
					emptyText		:'Seleccione...',
					valueField		: 'clave',
					displayField	: 'descripcion',
					width				: 230,
					tabIndex			: 17,
					store				: catalogoEstado,
					listeners: {
						select: function(combo, record, index) {
							 Ext.getCmp('cboCatalogoDelegacionFisica').reset();
							 Ext.getCmp('cboCatalogoCiudadFisica').reset();
							 primera_vez_municipio = false;
							 catalogoMunicipio.load({
								params: Ext.apply({
									estado : record.json.clave,
									pais : (Ext.getCmp('cboCatalogoPaisFisica')).getValue()
								})
							});
						}
					}
				},
				{
					xtype			:'textfield',
					name			: 'codigoPostalFisica',
					id				: 'txtCodigoPostalFisica',
					fieldLabel	: '* C�digo postal: ',
					maxLength	: 5,
					width			: 100,
					allowBlank	: false,
					tabIndex		:	19
				},
				{
					xtype				: 'combo',
					id          	: 'cboCatalogoCiudadFisica',
					name				: 'catalogoCiudadFisica',
					hiddenName 		: 'catalogoCiudadFisica',
					fieldLabel  	: 'Ciudad ',
					forceSelection	: true,
					allowBlank		: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccionar...',
					width				: 150,
					tabIndex			: 21,
					store				: catalogoCiudad,
					listeners		: {
						select: function(combo, record, index) {
							
						}
					}
				},
				{
					// E-mail
					xtype			: 'textfield',
					id				: 'txtemailFisica',
					name			: 'emailFIsica',
					fieldLabel: ' E-mail',
					allowBlank	: true,
					maxLength	: 100,
					width			: 230,
					tabIndex		: 23,
					margins		: '0 20 0 0',
					vtype: 'email'
				},
				{
					xtype			: 'textfield',
					id          : 'txtFaxContactoDomicilioFisica',
					name			: 'faxContactoDomicilioFisica',
					fieldLabel  : 'Fax ',
					allowBlank	: true,
					width			: 230,
					margins		: '0 20 0 0',
					maxLength	: 30,
					tabIndex		: 24
				}
			]
		}
	);
	
	var datosDomicilioPersonaFisicaPanelDer =(
		{
			xtype		: 'fieldset',
			id 		: 'datosDomicilioPersonaFisicaPanelDer',
			border	: false,
			//title		: 'derecho',
			labelWidth	: 150,
			items		:
			[
				{
					xtype			: 'textfield',
					id          : 'txtColoniaFisica',
					name			: 'coloniaFisica',
					fieldLabel  : '* Colonia',
					maxLength	: 100,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: false,
					tabIndex		: 16
				},	
				{
					xtype				: 'combo',
					id          	: 'cboCatalogoDelegacionFisica',
					name				: 'catalogoDelegacionFisica',
					hiddenName 		: 'catalogoDelegacionFisica',
					fieldLabel  	: '* Delegaci�n o municipio',
					forceSelection	: true,
					allowBlank		: false,
					triggerAction	: 'all',
					emptyText		:'Seleccione...',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					width				: 230,
					tabIndex			: 18,
					store				: catalogoMunicipio,
					listeners		:{
						select: function(combo, record, index) {
						primera_vez_municipio=false;
							Ext.getCmp('cboCatalogoCiudadFisica').reset();
							primera_vez_ciudad = false;
								catalogoCiudad.load({
									params : Ext.apply({
										pais	 : Ext.getCmp('cboCatalogoPaisFisica').getValue(),
										estado :  Ext.getCmp('cboCatalogoEstadoFisica').getValue(),
										municipio: record.json.clave
									})
								});
						}
					}
				},
				{
					xtype				: 'combo',
					id          	: 'cboCatalogoPaisFisica',
					name				: 'catalogoPaisFisica',
					hiddenName 		: 'catalogoPaisFisica',
					fieldLabel  	: '* Pa�s',
					forceSelection	: true,
					allowBlank		: false,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					//emptyText		: 'Seleccione Pa�s',
					width				: 150,
					tabIndex			: 20,
					store				: catalogoPais,
					listeners: {
						select: function(combo, record, index) {
							Ext.getCmp('cboCatalogoEstadoFisica').reset();
							Ext.getCmp('cboCatalogoDelegacionFisica').reset();
	 					   Ext.getCmp('cboCatalogoCiudadFisica').reset();
							primera_vez_estado = false;
								catalogoEstado.load({
									params : Ext.apply({
										pais : record.json.clave
									})
								});
						}
					}
				},
				{
					xtype			: 'textfield',
					id          : 'txtTelefonoFisica',
					name			: 'telefonoFisica',
					fieldLabel  : '* Tel�fono',
					allowBlank	: false,
					width			: 230,
					margins		: '0 20 0 0',
					maxLength	: 30,
					tabIndex		: 22
					
				}
			]
		}
	);//	FIN PANEL DOMICILIO FISICA
	
	var datosContactoFisicaPanelIzq = (
		{
			xtype		: 'fieldset',
			id 		: 'datosContactoFisicaPanelIzq',
			border	: false,
			//title		: 'derecho',
			labelWidth	: 150,
			items		:
			[
				{
					xtype			: 'textfield',
					id          : 'txtApPaternoContactoFisica',
					name			: 'apPaternoContactoFisica',
					fieldLabel  : '* Apellido paterno:',
					width			: 230,
					maxLength	: 25,
					margins		: '0 20 0 0',
					allowBlank	: false,
					tabIndex		:	25
				},
				{
					xtype			: 'textfield',
					id          : 'txtNombreContactoFisica',
					name			: 'nombreContactoFisica',
					fieldLabel  : '* Nombre(s) ',
					width			: 230,
					maxLength	: 30,
					margins		: '0 20 0 0',
					allowBlank	: false,
					tabIndex		:	27
				},
				{
					xtype			: 'textfield',
					id          : 'txtFaxContactoFisica',
					name			: 'faxContactoFisica',
					fieldLabel  : 'Fax ',
					allowBlank	: true,
					width			: 230,
					margins		: '0 20 0 0',
					maxLength	: 30,
					tabIndex		: 29
				}
			]
		}
	);
	var datosContactoFisicaPanelDer = (
		{
			xtype		: 'fieldset',
			id 		: 'datoDatosContactoFisicaPanelDer',
			border	: false,
			//title		: 'derecho',
			labelWidth	: 150,
			items		: 
			[
				{
					xtype			: 'textfield',
					id          : 'txtApMaternoContactoFisica',
					name			: 'apMaternoContactoFisica',
					fieldLabel  : '* Apellido materno:',
					width			: 230,
					maxLength	: 25,
					margins		: '0 20 0 0',
					allowBlank	: false,
					tabIndex		:	26
				},
				{
					xtype			: 'textfield',
					id          : 'txtTelefonoContactoFisica',
					name			: 'telefonoContactoFisica',
					fieldLabel  : '* Tel�fono',
					allowBlank	: false,
					width			: 230,
					margins		: '0 20 0 0',
					maxLength	: 30,
					tabIndex		: 28
				},
				{
					// E-mail
					xtype			: 'textfield',
					id				: 'txtEmailContactoFisica',
					name			: 'emailContactoFisica',
					fieldLabel	: ' * E-mail',
					allowBlank	: false,
					maxLength	: 100,
					width			: 230,
					tabIndex		: 30,
					margins		: '0 20 0 0',
					vtype: 'email'
				}
			]
		}
	);///FIN DATOS DEL CONTACTO FISICA
	
	var datosOtrosPanelIzq =(
		{
			xtype		: 'fieldset',
			id 		: 'datosOtrosPanelIzq',
			border	: false,
			labelWidth	: 150,
			items		: 
			[
				{
					xtype			: 'textfield'	,
					id				: 'txtVentasNetasFisica'	,
					name			: 'ventasNetasFisica'	,
					fieldLabel	: '* Ventas Netas �ltimo ejercicio anual: '	,
					allowBlank	: false	,
					maxLength	: 100	,
					width			: 225	,
					tabIndex		: 31	,
					margins		: '0 20 0 0'
				},
				{
					xtype			:'numberfield',
					name			: 'numEmpleadosFisica',
					id				: 'txtNumEmpleadosFisica',
					fieldLabel	: '* N�mero de empleados: ',
					maxLength	: 6,
					width			: 100,
					allowBlank	: false,
					tabIndex		:	33
				},
				{
					//CATALOGO SUBSECTOR
					xtype				: 'combo',
					id          	: 'cboCatalogoSubSectorFisica',
					name				: 'catalogoSubSectorFisica',
					hiddenName 		: 'catalogoSubSectorFisica',
					fieldLabel  	: '* Subsector',
					forceSelection	: true,
					allowBlank		: false,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccione ...',
					width				: 230,
					tabIndex			: 35,
					store				: catalogoSubSector,
					tpl:'<tpl for=".">' +
						'<tpl if="!Ext.isEmpty(loadMsg)">'+
						'<div class="loading-indicator">{loadMsg}</div>'+
						'</tpl>'+
						'<tpl if="Ext.isEmpty(loadMsg)">'+
						'<div class="x-combo-list-item">' +
						'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
						'</div></tpl></tpl>',
						setNewEmptyText: function(emptyTextMsg){
							this.emptyText = emptyTextMsg;
							this.setValue('');
							this.applyEmptyText();
							this.clearInvalid();						
						},
					listeners		: {
						select: function(combo, record, index) {
						primera_vez_rama = false;
						Ext.getCmp('cboCatalogoRamaFisica').reset();
					
							catalogoRama.load({
								params	:	Ext.apply({
									claveSector 	:	Ext.getCmp('cboCatalogoSectorEconomicoFisica').getValue(),
									claveSubSector	:	Ext.getCmp('cboCatalogoSubSectorFisica').getValue()
								})
							});
						Ext.getCmp('cboCatalogoClaseFisica').reset();	
						}
					}	
				},
				{
					//CATALOGO CLASE
					xtype				: 'combo',
					id          	: 'cboCatalogoClaseFisica',
					name				: 'catalogoClaseFisica',
					hiddenName 		: 'catalogoClaseFisica',
					fieldLabel  	: '* Clase',
					forceSelection	: true,
					allowBlank		: false,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccione ...',
					width				: 230,
					tabIndex			: 37,
					store				: catalogoClase,
						tpl:'<tpl for=".">' +
						'<tpl if="!Ext.isEmpty(loadMsg)">'+
						'<div class="loading-indicator">{loadMsg}</div>'+
						'</tpl>'+
						'<tpl if="Ext.isEmpty(loadMsg)">'+
						'<div class="x-combo-list-item">' +
						'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
						'</div></tpl></tpl>',
						setNewEmptyText: function(emptyTextMsg){
							this.emptyText = emptyTextMsg;
							this.setValue('');
							this.applyEmptyText();
							this.clearInvalid();						
						},
					listeners		:{
						
					}
				},
				{
					//CATALOGO TIPO EMPRESA // SQLcatSector="SELECT IC_TIPO_EMPRESA,CD_NOMBRE from COMCAT_TIPO_EMPRESA ";
					xtype				: 'combo',
					id          	: 'cboCatalogoTipoEmpresaFisica',
					name				: 'catalogoTipoEmpresaFisica',
					hiddenName 		: 'catalogoTipoEmpresaFisica',
					fieldLabel  	: '* Tipo Empresa',
					forceSelection	: true,
					allowBlank		: false,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccione ...',
					width				: 230,
					tabIndex			: 39,
					store				: catalogoTipoEmpresa
				},
				{
					xtype			:'textfield',
					name			: 'numDistribuidorFisica',
					id				: 'txtNumDistribuidorFisica',
					fieldLabel	: '* N�mero de distribuidor asignado por la EPO ',///El valor se genera dinamicamente 
					//N&uacute;mero de <%if("2".equals(TipoPYME)) out.print("distribuidor"); else  out.print("proveedor");
					maxLength	: 25,
					width			: 100,
					allowBlank	: false,
					tabIndex		:	41
				},
				{
					//CATALOGO OFICINA TRAMITADORA // SQLcatSector="SELECT IC_TIPO_EMPRESA,CD_NOMBRE from COMCAT_TIPO_EMPRESA ";
					xtype				: 'combo',
					id          	: 'cboCatalogoOficinaTramitadoraFisica',
					name				: 'catalogoOficinaTramitadoraFisica',
					hiddenName 		: 'catalogoOficinaTramitadoraFisica',
					fieldLabel  	: 'Oficina Tramitadora',
					forceSelection	: true,
					allowBlank		: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccione ...',
					width				: 230,
					tabIndex			: 42,
					store				: catalogoOficinaTramitadora
				}
			]
		}
	);
	
	var datosOtrosPanelDer =({
		xtype		: 'fieldset',
		id 		: 'datosOtrosPanelDer',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{
				xtype: 'displayfield',
				width: 50
			},//tabindex: 32
			{
				//CATALOGO SECTOR ECONOMICO // 
				xtype				: 'combo',
				id          	: 'cboCatalogoSectorEconomicoFisica',
				name				: 'catalogoSectorEconomicoFisica',
				hiddenName 		: 'catalogoSectorEconomicoFisica',
				fieldLabel  	: '* Sector econ�mico',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccione ...',
				width				: 230,
				tabIndex			: 34,
				store				: catalogoSectorEconomico,
				tpl:'<tpl for=".">' +
						'<tpl if="!Ext.isEmpty(loadMsg)">'+
						'<div class="loading-indicator">{loadMsg}</div>'+
						'</tpl>'+
						'<tpl if="Ext.isEmpty(loadMsg)">'+
						'<div class="x-combo-list-item">' +
						'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
						'</div></tpl></tpl>',
						setNewEmptyText: function(emptyTextMsg){
							this.emptyText = emptyTextMsg;
							this.setValue('');
							this.applyEmptyText();
							this.clearInvalid();						
						},
				listeners: {
					select: function(combo, record, index) {
					
						Ext.getCmp('cboCatalogoRamaFisica').setValue('');
						Ext.getCmp('cboCatalogoClaseFisica').setValue('');
						Ext.getCmp('cboCatalogoSubSector').setValue('');
							
						catalogoSubSector.load({
							params: Ext.apply({
								claveSector: combo.getValue()
							})			
						});
							
					}
				}
			},
			{
				//CATALOGO RAMA // 
				xtype				: 'combo',
				id          	: 'cboCatalogoRamaFisica',
				name				: 'catalogoRamaFisica',
				hiddenName 		: 'catalogoRamaFisica',
				fieldLabel  	: '* Rama',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione ...',
				width				: 230,
				tabIndex			: 36,
				store				: catalogoRama,
				tpl:'<tpl for=".">' +
						'<tpl if="!Ext.isEmpty(loadMsg)">'+
						'<div class="loading-indicator">{loadMsg}</div>'+
						'</tpl>'+
						'<tpl if="Ext.isEmpty(loadMsg)">'+
						'<div class="x-combo-list-item">' +
						'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
						'</div></tpl></tpl>',
						setNewEmptyText: function(emptyTextMsg){
							this.emptyText = emptyTextMsg;
							this.setValue('');
							this.applyEmptyText();
							this.clearInvalid();						
				},
				listeners: {
					select: function(combo, record, index) {
						primera_vez_rama = false;
						Ext.getCmp('cboCatalogoClaseFisica').reset();
						primera_vez_clase = false;
						catalogoClase.load({
							params	:	Ext.apply({
								informacion		:	'catalogoClase',
								claveSector 	:	Ext.getCmp('cboCatalogoSectorEconomicoFisica').getValue(),
								claveSubSector	:	Ext.getCmp('cboCatalogoSubSectorFisica').getValue(),
								claveRama		:	Ext.getCmp('cboCatalogoRamaFisica').getValue()
							})
						});
					}
				}
			},
			{
				xtype			: 'textfield',
				id          : 'txtProductosPrincipalesFisica',
				name			: 'productosPrincipalesFisica',
				fieldLabel  : '* Principales productos: ',
				maxLength	: 100,
				width			: 150,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 38,
				listeners:{
					//ver isCharIntSymb(this.form.name,this.name);
					//blur	: function(){};
				}
			},
			{
				//CATALOGO TIPO CREDITO // 
				xtype				: 'combo',
				id          	: 'cboCatalogoTipoCreditoFisica',
				name				: 'catalogoTipoCreditoFisica',
				hiddenName 		: 'catalogoTipoCreditoFisica',
				fieldLabel  	: 'Tipo de Cr�dito',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccione ...',
				width				: 230,
				tabIndex			: 40,
				store				: catalogoTipoCredito,
				tpl:'<tpl for=".">' +
					 '<tpl if="!Ext.isEmpty(loadMsg)">'+
					 '<div class="loading-indicator">{loadMsg}</div>'+
					 '</tpl>'+
					 '<tpl if="Ext.isEmpty(loadMsg)">'+
					 '<div class="x-combo-list-item">' +
					 '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
					 '</div></tpl></tpl>',
					 setNewEmptyText: function(emptyTextMsg){
					  this.emptyText = emptyTextMsg;
					  this.setValue('');
					  this.applyEmptyText();
					  this.clearInvalid();						
					},
				listeners: {
					select: function(combo, record, index) {
						
						/*	catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});*/
					}
				}
			}
		]
	});//FIN OTROS DATOS FISICA
	
//FIN ELEMENTOS PARA PERSONAS FISICAS
	
	
//PERSONA MORAL	
//________DATOS DE LA EMPRESAS__________//
	//____________PANEL IZQUIERDO________________
	
	var datosEmpresaPanelIzq = {
		xtype		: 'fieldset',
		id 		: 'datosEmpresaPanelIzq',
		defaults			: { msgTarget: 'side' },
		border	: false,
		labelWidth	: 150,
		items		:[
			{
				xtype			: 'textfield',
				name			: 'razonSocial',
				id				: 'txtRazonSocial',
				fieldLabel	: '* Raz�n Social',
				maxLength	: 100,
				width			: 230,
				allowBlank	: false,
				margins		: '0 -20 0 0',
				msgTarget: 'side',
				tabIndex		:	1
			},
			{
				xtype: 'compositefield',
				msgTarget: 'side',
				combineErrors: false,
				items: [
					{	
						width: 110,	
						xtype: 'textfield',
						maxLength: 20,
						fieldLabel	:'* RFC',
						margins: '0 10 0 0',
						allowBlank	: false,
						name: 'rfc',
						id: 'txtRFC',
						tabIndex	:3,
						regex	:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
						regexText:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
									  'en el formato NNN-AAMMDD-XXX donde:<br>'+
									  'NNN:son las iniciales del nombre de la empresa<br>'+
									  'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
									  'XXX:es la homoclave'
					},{	
						width: 80,
						xtype: 'checkbox',	
						boxLabel: 'Invalidar',
						name: 'invalidar',
						id: 'chkInvalidar',
						tabIndex	:4,
						listeners: {check: function(){
							Ext.getCmp('txtRFC').setValue('XXXX-000101-XXX');
							//Ext.getCmp('actualiza_sirac1').setValue('');//???????????????????????????
						}}
					},{xtype: 'displayfield', value: '', width: 237}	
				]
			},
			{
				xtype			: 'textfield',
				name			: 'fiel',
				id				: 'txtFiel',
				fieldLabel	: 'FIEL',
				maxLength	: 25,
				width			: 100,
				allowBlank	: true,
				tabIndex		:	6
			}
		]
	};
	
	//____________PANEL IZQUIERDO________________
		
	var datosEmpresaPanelDer={
		xtype		: 'fieldset',
		id 		: 'datosEmpresaPanelDer',
		border	: false,
		defaults:{msgTarget: 'side'},
		labelWidth	: 150,
		items		:[
			{
				xtype			:'numberfield',
				name			: 'numEscritura',
				id				: 'txtNumEscritura',
				fieldLabel	: '* N�mero de Escritura ',
				maxLength	: 40,
				width			: 100,
				allowBlank	: false,
				tabIndex		:	2
			},
			{
				xtype			: 'datefield',
				id          : 'txtFechaConstitucion',
				name			: 'fechaConstitucion',
				allowBlank	: false,
				width			: 130,
				fieldLabel  : '* Fecha de Constituci�n',
				margins		: '0 20 0 0',
				tabIndex		: 5
			},
			{
				xtype				: 'combo',
				id          	: 'cboCatalogoPaisOrigen',
				name				: 'catalogoPaisOrigen',
				hiddenName 		: 'catalogoPaisOrigen',
				fieldLabel  	: 'Pa�s de origen',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 150,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 7,
				store				: catalogoPaisOrigen
			}
		
		]
	};		
//______________FIN DATOS DE LA EMPRESA________________

//______________DOMICILIO inicio_____________________________________
	//_______________PANEL IZQuierdo___________________________________________
	var datosDomicilioPanelIzq={
		xtype		: 'fieldset',
		id 		: 'datosDomicilioPanelIzq',
		border	: false,
		defaults:{msgTarget: 'side'},
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'txtCalle',
				name			: 'calle',
				fieldLabel  : '* Calle, No. Exterior y No. Interior:',
				maxLength	: 100,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	8

			},
			{
				xtype				: 'combo',
				id          	: 'cboCatalogoEstado',
				name				: 'catalogoEstado',
				hiddenName 		: 'catalogoEstado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				allowBlank		: false,
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				tabIndex			: 10,
				store				: catalogoEstado,
				listeners: {
					select: function(combo, record, index) {
						 
						Ext.getCmp('cboCatalogoDelegacion').reset();
						Ext.getCmp('cbocatalogoCiudad').reset();

						 primera_vez_municipio = false;
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('cboCatalogoPais')).getValue()
							})
						});
					}
				}
			},
			{
				xtype			:'textfield',
				name			: 'codigoPOstal',
				id				: 'txtCodigoPostal',
				fieldLabel	: '* C�digo postal: ',
				maxLength	: 5,
				width			: 100,
				allowBlank	: false,
				tabIndex		:	12
			},
			{
				xtype				: 'combo',
				id          	: 'cbocatalogoCiudad',
				name				: 'catalogoCiudad',
				hiddenName 		: 'catalogoCiudad',
				fieldLabel  	: 'Ciudad ',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccionar...',
				width				: 150,
				tabIndex			: 14,
				store				: catalogoCiudad,
				listeners: {
					
				}
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'txtemail',
				name			: 'email',
				fieldLabel: ' E-mail',
				allowBlank	: true,
				maxLength	: 100,
				width			: 230,
				tabIndex		: 16,
				margins		: '0 20 0 0',
				vtype: 'email'
			}
		]
	};
	//_______________PANEL DERECHO___________________________________________	
	var datosDomicilioPanelDer = {
		xtype		: 'fieldset',
		layout	:'form',
		id 		: 'datosDomicilioPanelDer',
		border	: false,
		defaults:{msgTarget: 'side'},
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'txtColonia',
				name			: 'colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 100,
				width			: 230,
				margins		: '0 20 0 0',
				msgTarget: 'side',
				allowBlank	: false,
				
				tabIndex		: 9
			},	
			{
				xtype				: 'combo',
				id          	: 'cboCatalogoDelegacion',
				name				: 'catalogoDelegacion',
				hiddenName 		: 'catalogoDelegacion',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				tabIndex			: 11,
				store				: catalogoMunicipio,
				listeners		:{
					select: function(combo, record, index) {
							Ext.getCmp('cbocatalogoCiudad').reset();
							primera_vez_ciudad = false;
								catalogoCiudad.load({
									params : Ext.apply({
										pais	 : Ext.getCmp('cboCatalogoPais').getValue(),
										estado :  Ext.getCmp('cboCatalogoEstado').getValue(),
										municipio: record.json.clave
									})
								});
						}
				}
			},
			{
				xtype				: 'combo',
				id          	: 'cboCatalogoPais',
				name				: 'catalogoPais',
				hiddenName 		: 'catalogoPais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccione Pa�s...',
				width				: 150,
				tabIndex			: 13,
				store				: catalogoPais,
				listeners: {
					select: function(combo, record, index) {
							Ext.getCmp('cboCatalogoEstado').reset();
							Ext.getCmp('cboCatalogoDelegacion').reset();
	 					   Ext.getCmp('cbocatalogoCiudad').reset();
							primera_vez_estado = false;
								catalogoEstado.load({
									params : Ext.apply({
										pais : record.json.clave
									})
								});
						}
				}
				
			},
			{
				xtype			: 'textfield',
				id          : 'txtTelefono',
				name			: 'telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30,
				tabIndex		: 15
				
			},
			{
				xtype			: 'textfield',
				id          : 'txtFax',
				name			: 'fax',
				fieldLabel  : ' Fax',
				width			: 230,
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		:	17
			}
		]
	};
//__________________________________________FIN DATOS DOMICILIO


//______DATOS DEL REPRESENTANTE LEGAL_______________

	//______________DATOS REPRESNTANTE lEGAL IZQUIERDO___________________________
	var datosRepresentantePanelIzq = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentantePanelIzq',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'txtApPaterno',
				name			: 'apPaterno',
				fieldLabel  : '* Apellido paterno:',
				width			: 230,
				maxLength	: 25,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	18
			},
			{
				xtype			: 'textfield',
				id          : 'txtNombre',
				name			: 'nombre',
				fieldLabel  : '* Nombre(s) ',
				width			: 230,
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	20
			},
			{
				xtype			: 'textfield',
				id          : 'txtTelefonoRep',
				name			: 'telefonoRep',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30,
				tabIndex		: 22
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'txtemailRep',
				name			: 'emailRep: ',
				fieldLabel: ' E-mail',
				allowBlank	: true,
				maxLength	: 100,
				width			: 230,
				tabIndex		: 24,
				margins		: '0 20 0 0',
				vtype: 'email'
			},
			{
				xtype			:'numberfield',
				name			: 'numIdentificacion',
				id				: 'txtNumIdentificacion',
				fieldLabel	: '* No. de Identificaci�n: ',
				maxLength	: 30,
				width			: 100,
				allowBlank	: false,
				tabIndex		:	26
			},
			{
				xtype			:'numberfield',
				name			: 'numEscrituraRep',
				id				: 'txtNumEscrituraRep',
				fieldLabel	: '* Poderes y Facultades No. de escritura: ',
				maxLength	: 40,
				width			: 100,
				allowBlank	: false,
				tabIndex		:	28
			}
		]
	};
	//FIN___________DATOS REPRESNTANTE lEGAL IZQUIERDO___________________________
	
	//______________DATOS REPRESNTANTE lEGAL DERECHO_____________________________
	var datosRepresentantePanelDer ={
		xtype		: 'fieldset',
		id 		: 'datosRepresentantePanelDer',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'txtApMaterno',
				name			: 'apMaterno',
				fieldLabel  : '* Apellido materno:',
				width			: 230,
				maxLength	: 25,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	19
			},
			{
				xtype 		: 'textfield',
				name  		: 'rfcRep',
				id    		: 'txtRFCRep',
				fieldLabel	: '* R.F.C.',
				maxLength	: 20,
				width			: 150,
				allowBlank 	: false,
				regex			:/^([A-Z]{4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
				regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
				'en el formato NNNN-AAMMDD-XXX donde:<br>'+
				'NNNN:son las iniciales del nombre <br>'+
				'AAMMDD: es la fecha de nacimiento menor al dia de hoy<br>'+
				'XXX:es la homoclave',
				tabIndex		:	21
			},
			{
				xtype			: 'textfield',
				id          : 'txtfaxRep',
				name			: 'faxRep',
				fieldLabel  : 'Fax',
				width			: 230,
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		:	23
			},
			{
				xtype				: 'combo',
				id          	: 'cboCatalogoIdentificacion',
				name				: 'catalogoIdentificacion',
				hiddenName 		: 'catalogoIdentificacion',
				fieldLabel  	: '* Identfiaci�n',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione ...',
				width				: 150,
				tabIndex			: 25,
				store				: catalogoIdentificacion,
				listeners: {
				}
			},
			{
				xtype: 'displayfield', value: '', width: 237
			},
			{
				xtype			: 'datefield',
				id          : 'txtFechaPoderNotarial',
				name			: 'fechaPoderNotarial',
				allowBlank	: false,
				width			: 130,
				fieldLabel  : '* Fecha del poder notarial ',
				margins		: '0 20 0 0',
				tabIndex		: 29
			}
		]
	};
	//FIN___________DATOS REPRESNTANTE lEGAL DERECHO__________________	
//_____________________________________________FIN DATOS REPRESNTANTE lEGAL_____


//______DATOS DEL CONTACTO_______________________________
	//______DATOS DEL CONTACTO PANEL IZQUIERDO_______________
	var datosContactoPanelIzq={
		xtype		: 'fieldset',
		id 		: 'datosRepresentantePanelIzq',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'txtApPaternoContacto',
				name			: 'apPaternoContacto',
				fieldLabel  : '* Apellido paterno:',
				width			: 230,
				maxLength	: 25,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	30
			},
			{
				xtype			: 'textfield',
				id          : 'txtNombreContacto',
				name			: 'nombreContacto',
				fieldLabel  : '* Nombre(s) ',
				width			: 230,
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	32
			},
			{
				xtype			: 'textfield',
				id          : 'txtFaxContacto',
				name			: 'faxContacto',
				fieldLabel  : 'Fax ',
				allowBlank	: true,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30,
				tabIndex		: 34
			},
			{
			//CATALOGO TIPO CATEGORIA
				xtype				: 'combo',
				id          	: 'cboCatalogoTipoCategoria',
				name				: 'catalogoTipoCategoria',
				hiddenName 		: 'catalogoTipoCategoria',
				fieldLabel  	: '* Tipo Categoria',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione ...',
				width				: 150,
				tabIndex			: 36,
				store				: catalogoTipoCategoria,
				listeners: {
				}
			}
		]
	};
	//FIN___________________________DATOS CONTACTO PANEL IZQUIERDO_______________
	
	//______________DATOS CONTACTO PANEL DERECHO_____________________________
	var datosContactoPanelDer={
		xtype		: 'fieldset',
		id 		: 'datosContactoPanelDer',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'txtApMaternoContacto',
				name			: 'apMaternoContacto',
				fieldLabel  : '* Apellido materno:',
				width			: 230,
				maxLength	: 25,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	31
			},
			{
				xtype			: 'textfield',
				id          : 'txtTelefonoContacto',
				name			: 'telefonoContacto',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30,
				tabIndex		: 33
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'txtEmailContacto',
				name			: 'emailContacto',
				fieldLabel	: ' * E-mail',
				allowBlank	: false,
				maxLength	: 100,
				width			: 230,
				tabIndex		: 35,
				margins		: '0 20 0 0',
				vtype: 'email'
			}
		]
	};
	//FIN___________________________DATOS CONTACTO PANEL DERECHO_________________
//_____________________________________________FIN DATOS CONTACTO_______________

//_____________INFORMACION INICIAL PARA EL ALTA DE CREDITO______________________
	//_______________________DATOS INFORMACION ALTA CREDITO PANEL IZQUIERDO______
	var datosInfoAltaCreditoPanelIzq = {
		xtype		: 'fieldset',
		id 		: 'datosInfoAltaCreditoPanelIzq',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{//????????????????????????????????????????????Este campo solo sera visible si (!"2".equals(TipoPYME)) 
				xtype			:'numberfield',
				name			: 'numSIRAC',
				id				: 'txtNumSIRAC',
				hidden		: true,
				fieldLabel	: 'N�mero SIRAC: ',
				maxLength	: 12,
				width			: 100,
				allowBlank	: true,
				tabIndex		:	36
			},
			{
				xtype			: 'textfield',
				id				: 'txtVentasNetas',
				name			: 'ventasNetas',
				fieldLabel	: '* Ventas Netas �ltimo ejercicio anual: ',
				allowBlank	: false,
				maxLength	: 100,
				width			: 225,
				tabIndex		: 38,/////////////////////////////
				margins		: '0 20 0 0'
			},
			{
				xtype			:'numberfield',
				name			: 'numEmpleados',
				id				: 'txtNumEmpleados',
				fieldLabel	: '* N�mero de empleados: ',
				maxLength	: 6,
				width			: 100,
				allowBlank	: false,
				tabIndex		:	40
			},
			{
				//CATALOGO SUBSECTOR
				xtype				: 'combo',
				id          	: 'cboCatalogoSubSector',
				name				: 'catalogoSubSector',
				hiddenName 		: 'catalogoSubSector',
				fieldLabel  	: '* Subsector',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione ...',
				width				: 225,
				tabIndex			: 42,
				store				: catalogoSubSector,
				listeners: {
					select: function(combo, record, index) {
					
					
					Ext.getCmp('cboCatalogoRamaFisica').setValue('');
					Ext.getCmp('cboCatalogoClaseFisica').setValue('');					
					Ext.getCmp('cboCatalogoRama').setValue('');	
					Ext.getCmp('cboCatalogoClase').setValue('');	
					
						catalogoRama.load({
							params: Ext.apply({
								claveSector 	: Ext.getCmp('cboCatalogoSectorEconomico').getValue(),
								claveSubSector	: combo.getValue()
							})			
						});							
					}
				}
			},
			{
				//CATALOGO CLASE
				xtype				: 'combo',
				id          	: 'cboCatalogoClase',
				name				: 'catalogoClase',
				hiddenName 		: 'catalogoClase',
				fieldLabel  	: '* Clase',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccione ...',
				width				: 225,
				tabIndex			: 44,
				store				: catalogoClase,
				listeners: {
					select: function(combo, record, index) {
					}
				}
			},
			{
				//CATALOGO TIPO EMPRESA // SQLcatSector="SELECT IC_TIPO_EMPRESA,CD_NOMBRE from COMCAT_TIPO_EMPRESA ";
				xtype				: 'combo',
				id          	: 'cboCatalogoTipoEmpresa',
				name				: 'catalogoTipoEmpresa',
				hiddenName 		: 'catalogoTipoEmpresa',
				fieldLabel  	: '* Tipo Empresa',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccione ...',
				width				: 225,
				tabIndex			: 46,
				store				: catalogoTipoEmpresa
			},
			{
				xtype			:'textfield',
				name			: 'numDistribuidor',
				id				: 'txtNumDistribuidor',
				fieldLabel	: '* N�mero de distribuidor asignado por la EPO ',///El valor se genera dinamicamente 
				//N&uacute;mero de <%if("2".equals(TipoPYME)) out.print("distribuidor"); else  out.print("proveedor");
				maxLength	: 25,
				width			: 100,
				allowBlank	: false,
				tabIndex		:	48
			},
			{
				//CATALOGO OFICINA TRAMITADORA // SQLcatSector="SELECT IC_TIPO_EMPRESA,CD_NOMBRE from COMCAT_TIPO_EMPRESA ";
				xtype				: 'combo',
				id          	: 'cboCatalogoOficinaTramitadora',
				name				: 'catalogoOficinaTramitadora',
				hiddenName 		: 'catalogoOficinaTramitadora',
				fieldLabel  	: 'Oficina Tramitadora',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccione ...',
				width				: 225,
				tabIndex			: 50,
				store				: catalogoOficinaTramitadora
			}
		]
	};
	//FIN________DATOS INFORMACION INICIAL ALTA CREDITO PANEL IZQUIEDO___________
	//______________DATOS INFORMACION INICIAL ALTA CREDITO PANEL DERECHO_________
	var datosInfoAltaCreditoPanelDer = {
		xtype		: 'fieldset',
		id 		: 'datosInfoAltaCreditoPanelDer',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{//????????????????????????????????????????????Este campo solo sera visible si (!"2".equals(TipoPYME)) 
				xtype			: 'checkbox',
				id				: 'chkFinanDistribuidores',
				hidden		: true,
				name			: 'finanDistribuidores',
				hiddenName	: 'finanDistribuidores',
				fieldLabel	: 'Financiamiento a Distribuidores: ',
				tabIndex		: 37,
				enable 		: true	
			},
			{xtype: 'displayfield', value: '', width: 237},//{xtype: 'displayfield', value: '', width: 237},
			{
				//CATALOGO SECTOR ECONOMICO // 
				xtype				: 'combo',
				id          	: 'cboCatalogoSectorEconomico',
				name				: 'catalogoSectorEconomico',
				hiddenName 		: 'catalogoSectorEconomico',
				fieldLabel  	: '* Sector econ�mico',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccione ...',
				width				: 250,
				tabIndex			: 41,
				store				: catalogoSectorEconomico,
				listeners: {
					select: function(combo, record, index) {
					claveSector=record.json.clave;
						Ext.getCmp('cboCatalogoSubSector').reset();
						Ext.getCmp('cboCatalogoRama').reset();
						Ext.getCmp('cboCatalogoClase').reset();
						primera_vez_subsector = false;
							catalogoSubSector.load({
								params : Ext.apply({
									claveSector : record.json.clave
								})
							});
							
					}
				}
			},
			{
				//CATALOGO RAMA // 
				xtype				: 'combo',
				id          	: 'cboCatalogoRama',
				name				: 'catalogoRama',
				hiddenName 		: 'catalogoRama',
				fieldLabel  	: '* Rama',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione ...',
				width				: 250,
				tabIndex			: 43,
				store				: catalogoRama,
				listeners: {
					select: function(combo, record, index) {
						//ver cambiarSector(3);???????????????????????????????????????????????
						primera_vez_clase = false;
						Ext.getCmp('cboCatalogoClase').reset();
						catalogoClase.load({
							params : Ext.apply({
								claveSector:claveSector,
								claveSubSector:claveSubSector,
								claveRama : record.json.clave
							})
						});
					}
				}
			},
			{
				xtype			: 'textfield',
				id          : 'txtProductosPrincipales',
				name			: 'productosPrincipales',
				fieldLabel  : '* Principales productos: ',
				maxLength	: 100,
				width			: 250,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 45,
				listeners:{
					//ver isCharIntSymb(this.form.name,this.name);
					//blur	: function(){};
				}
			},
			{
				//CATALOGO TIPO CREDITO // 
				xtype				: 'combo',
				id          	: 'cboCatalogoTipoCredito',
				name				: 'catalogoTipoCredito',
				hiddenName 		: 'catalogoTipoCredito',
				fieldLabel  	: 'Tipo de Cr�dito',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccione ...',
				width				: 250,
				tabIndex			: 47,
				store				: catalogoTipoCredito,
				margins: '0 20 0 0',  //necesario para mostrar el icono de error
				listeners: {
					select: function(combo, record, index) {
						//ver cambiarSector(3);???????????????????????????????????????????????
						/*	catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});*/
					}
				}
			}
		]
	
	}; 
	//FIN_______________DATOS INFORMACION INICIAL ALTA CREDITO PANEL DERECHOO____
//_____________________FIN DATOS INFORMACION INICIAL ALTA CREDITO_______________

	var fpDatosModificar = new Ext.form.FormPanel({
		id					: 'formaDatosModificar',
		layout			: 'form',
		width				: 900,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		
		labelWidth			: 250,
		defaults: { 	msgTarget: 'side', 	anchor: '-20',
		layoutConfig: {
					 padding: 10
				}
		},
		
		items				: 
		[
			{
				layout	: 'hbox',
				title 	: 'Nombre Completo',
				cllapsible	:true,
				id			: 'nombreCompletoFisica',
				//hidden	:true,
				autoScroll : true,
				width		: 940,
				items		: [datosNombreCompletoPersonaFisicaPanelIzq	, datosNombreCompletoPersonaFisicaPanelDer	]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				cllapsible	:true,
				id			: 'domicilioFisica',
				//hidden	:true,
				width		: 940,
				items		: [datosDomicilioPersonaFisicaPanelIzq	, datosDomicilioPersonaFisicaPanelDer	]
			},
			{
				layout	: 'hbox',
				title 	: 'Datos del contacto',
				cllapsible	:true,
				id			: 'datosContactoFisica',
				//hidden	; true,
				width		: 940,
				items		: [datosContactoFisicaPanelIzq	, datosContactoFisicaPanelDer	]
			},
			{
				layout	: 'hbox',
				title 	: 'Otros Datos ',
				cllapsible	:true,
				id			: 'datosOtrosFisica',
				//hidden	:true,
				width		: 940,
				items		: [datosOtrosPanelIzq	, datosOtrosPanelDer	]
			},//fin persona fisica
			{
				layout	: 'hbox',
				title 	: 'Datos de la Empresa',
				id			: 'datosEmpresa',
				cllapsible	:true,
				//hidden	:true,
				width		: 940,
				items		: [datosEmpresaPanelIzq	, datosEmpresaPanelDer	]
			},
			{
				layout	: 'hbox',
				title		: 'Domicilio',
				id			: 'domicilio',
				//hidden	:true,
				width		: 940,
				items		: [datosDomicilioPanelIzq, datosDomicilioPanelDer ]
			},
			{
				layout	: 'hbox',
				title 	: 'Datos del representante legal',
				id			: 'datosRepLegal',
				//hidden	:true,
				width		: 940,
				items		: [datosRepresentantePanelIzq	, datosRepresentantePanelDer	]
			},
			{
				layout	: 'hbox',
				title		: 'Datos del contacto',
				id			: 'datosContacoto',
				//hidden	:true,
				layoutConfig: {
					 padding: 10
				},
				width		: 940,
				items		: [datosContactoPanelIzq, datosContactoPanelDer ]
			},
			{
				layout	: 'hbox',
				title 	: 'Informaci�n inicial para el alta del cr�dito',
				id			: 'infoInicialAltaCredito',
				autoScroll : true,
				layoutConfig: {
					 padding: 10
				},
				//hidden	:true,
				//height : 150,
				width		: 940,
				items		: [datosInfoAltaCreditoPanelIzq	, datosInfoAltaCreditoPanelDer	]
			}
		],
		buttons			: [
			{
				text: 'Actualizar N@E ',
				id: 'BTNACTUALIZANE_DM',
				hidden : true,
				iconCls:'aceptar',
				handler: function(boton, evento) {
					validaPermisos  ('BTNACTUALIZANE_DM');
				}
			},
			{
				text:'Regresar',
				handler: function() {
					window.location = '15formaDist05ext.jsp?idMenu='+idMenu;
				}
			},
			{
				text:'Actualizar N@E/Sirac ',
				id: 'BTNACTUASIRAC_DM',
				hidden : true,
				handler:  function(boton, evento) {
				
					validaPermisos  ('BTNACTUASIRAC_DM');

				}
			}
		]
	});
	
		var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fpDatosModificar,
			NE.util.getEspaciador(20),
				
			NE.util.getEspaciador(20)
		]
	});
	
	catalogoPaisOrigen.load();
	catalogoPais.load();
	catalogoIdentificacion.load();
	catalogoTipoCategoria.load();
	
	catlogoEstadoCivil.load();
	catalogoGradoEscolar.load();
	catalogoRegimenMatrimonial.load();
	catalogoTipoEmpresa.load();
	catalogoOficinaTramitadora.load();
	catalogoSectorEconomico.load();
	//catalogoSubSector.load();
	Ext.Ajax.request({
		url: '15formaDistModificar05ext.data.jsp',
		params: {
			informacion: "valoresIniciales",
			clavePyme:getURLParameter("clavePyme"),
			claveEpo	:getURLParameter("claveEpo"),
			tipoPersona:getURLParameter("tipoPersona"),
			pais			: Ext.getCmp('cboCatalogoPais').getValue(),
			idMenuP:idMenu 
		},
		callback: procesaValoresIniciales
	});	

	

});//fin de js
		