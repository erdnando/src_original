<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	JSONObject resultado = new JSONObject();


 if(informacion.equals("catalogoPais")) {
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionIn("24", Integer.class);	//Solo México
		infoRegresar = cat.getJSONElementos();	
	}
 else if(informacion.equals("catalogoCadenas")){
		
		List lista = new ArrayList();
		lista.add("S");
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_epo");
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setCampoLlave("cs_habilitado");
		cat.setValoresCondicionIn(lista);
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();	
 }
 else if(informacion.equals("catalogoEstado")){
		
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setClavePais("24");//Mexico clave 24
		cat.setOrden("ic_estado");
		infoRegresar = cat.getJSONElementos();	
 }
 else if(informacion.equals("catalogoMunicipio"))	{
		
		String Edo= (request.getParameter("estado")==null)?"":request.getParameter("estado");
		
		CatalogoMunicipio cat=new CatalogoMunicipio();
		cat.setClave("ic_municipio");
		cat.setDescripcion("cd_nombre");
		cat.setPais("24");
		cat.setEstado(Edo);
		cat.setOrden("cd_nombre");
		List elementos=cat.getListaElementos();
		
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
 }
 else if(informacion.equals("catalogoPromotor"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_lider_promotor");
		cat.setCampoClave("ic_lider_promotor");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();
		
 }else if(informacion.equals("guardaDatos"))	{
		
		Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
 
		String cboTipoAfiliacion  = (request.getParameter("afiliacion")!=null)?request.getParameter("afiliacion"):"";
		String razon_Social   = (request.getParameter("razon_social_moral")!=null)?request.getParameter("razon_social_moral"):"";
		String persona   = (request.getParameter("cmb_persona")!=null)?request.getParameter("cmb_persona"):"";
		String apellido_paterno   = (request.getParameter("paterno")!=null)?request.getParameter("paterno"):"";
		String apellido_materno     = (request.getParameter("materno")!=null)?request.getParameter("materno"):"";
		String nombre   = (request.getParameter("nombre")!=null)?request.getParameter("nombre"):"";
		String rfc  = (request.getParameter("rfc")!=null)?request.getParameter("rfc"):"";
		String calle   = (request.getParameter("calle")!=null)?request.getParameter("calle"):"";
		String colonia   = (request.getParameter("colonia")!=null)?request.getParameter("colonia"):"";
		String pais   = (request.getParameter("cmb_pais")!=null)?request.getParameter("cmb_pais"):"";
		String estado   = (request.getParameter("cmb_edo")!=null)?request.getParameter("cmb_edo"):"";
		String del_mun  = (request.getParameter("cmb_del_mun")!=null)?request.getParameter("cmb_del_mun"):"";
		String cod_post  = (request.getParameter("cod_post")!=null)?request.getParameter("cod_post"):"";
		String telefono  = (request.getParameter("telefono")!=null)?request.getParameter("telefono"):"";
		String fax   = (request.getParameter("fax")!=null)?request.getParameter("fax"):"";
		String email  = (request.getParameter("email")!=null)?request.getParameter("email"):"";
		String apellido_paterno_c     = (request.getParameter("paterno_contacto")!=null)?request.getParameter("paterno_contacto"):"";
		String apellido_materno_c  = (request.getParameter("materno_contacto")!=null)?request.getParameter("materno_contacto"):"";
		String nombre_c   = (request.getParameter("nombre_contacto")!=null)?request.getParameter("nombre_contacto"):"";
		String telefono_c   = (request.getParameter("telc")!=null)?request.getParameter("telc"):"";
		String celular   = (request.getParameter("txt_cel_cont")!=null)?request.getParameter("txt_cel_cont"):"";
		String fax_c   = (request.getParameter("faxc")!=null)?request.getParameter("faxc"):"";
		String email_c   = (request.getParameter("emailc")!=null)?request.getParameter("emailc"):"";
		String conf_email_c   = (request.getParameter("emailc_conf")!=null)?request.getParameter("emailc_conf"):"";
		String cve_epo   = (request.getParameter("cmb_cadenas")!=null)?request.getParameter("cmb_cadenas"):"";
		
		String cve = afiliacion.afiliaMandate("M", cve_epo, apellido_paterno, apellido_materno, nombre, rfc, calle, colonia, cod_post, telefono, email, fax, 
											apellido_paterno_c, apellido_materno_c, nombre_c, telefono_c, fax_c, email_c, conf_email_c, "", pais, del_mun, 
											estado, persona, celular, razon_Social, "", "N");
								
		resultado.put("success", new Boolean(true));
		resultado.put("num_cve", cve);
		
		infoRegresar = resultado.toString();
		
 }

%>
<%=infoRegresar%>




