<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject, 
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,	
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		com.netro.seguridadbean.*, 
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
String informacion      = request.getParameter("informacion")       == null?"": (String)request.getParameter("informacion");
String operacion        = request.getParameter("operacion")         == null?"": (String)request.getParameter("operacion");
String nafinElectronico = request.getParameter("nafin_electronico") == null?"": (String)request.getParameter("nafin_electronico");
String nombre           = request.getParameter("nombre")            == null?"": (String)request.getParameter("nombre");
String estado           = request.getParameter("estado")            == null?"": (String)request.getParameter("estado");
String rfc              = request.getParameter("rfc")               == null?"": (String)request.getParameter("rfc");
String icEpo            = request.getParameter("ic_epo")            == null?"": (String)request.getParameter("ic_epo");
String numeroProveedor  = request.getParameter("numero_proveedor")  == null?"": (String)request.getParameter("numero_proveedor");
String numeroSirac      = request.getParameter("numero_sirac")      == null?"": (String)request.getParameter("numero_sirac");
String icPyme           = request.getParameter("ic_pyme")           == null?"": (String)request.getParameter("ic_pyme");
String tipoAfiliado     = request.getParameter("tipo_afiliado")     == null?"": (String)request.getParameter("tipo_afiliado");
String comboPais        = request.getParameter("combo_pais")        == null?"": (String)request.getParameter("combo_pais");
String comboEstado      = request.getParameter("combo_estado")      == null?"": (String)request.getParameter("combo_estado");
String sectorEcon       = request.getParameter("sector_econ")       == null?"": (String)request.getParameter("sector_econ");
String subSector        = request.getParameter("subsector")         == null?"": (String)request.getParameter("subsector");
String rama             = request.getParameter("rama")              == null?"": (String)request.getParameter("rama");
String clase            = request.getParameter("clase")             == null?"": (String)request.getParameter("clase");
String busca_edita      = request.getParameter("busca_edita")       == null?"": (String)request.getParameter("busca_edita");
String filtroEpo        = request.getParameter("filtro_epo")        == null?"": (String)request.getParameter("filtro_epo");

String modifPropietario = (request.getParameter("modif_propietario")  != null && request.getParameter("modif_propietario").equals("true")) ?"S":"";
String pymeSinProveedor = (request.getParameter("pyme_sin_proveedor") != null && request.getParameter("pyme_sin_proveedor").equals("true"))?"S":"";
String convenioUnico    = (request.getParameter("convenio_unico")     != null && request.getParameter("convenio_unico").equals("true"))    ?"S":"";
String operaFideicomiso = (request.getParameter("operaFideicomiso")   != null && request.getParameter("operaFideicomiso").equals("true"))  ?"S":"";
String suceptibleFloating = (request.getParameter("suceptibleFloating")   != null && request.getParameter("suceptibleFloating").equals("true"))  ?"S":"";
String chkFactDistribuidoCons = (request.getParameter("chkFactDistribuidoCons")   != null && request.getParameter("chkFactDistribuidoCons").equals("true"))  ?"S":"";
String chkProvExtranjeroCons = (request.getParameter("chkProvExtranjeroCons")   != null && request.getParameter("chkProvExtranjeroCons").equals("true"))  ?"S":"";
String chkOperaDescAutoEPOCons = (request.getParameter("chkOperaDescAutoEPOCons")   != null && request.getParameter("chkOperaDescAutoEPOCons").equals("true"))  ?"S":"";



int start=0, limit=0;

String nombreArchivo = "", consulta = "", infoRegresar = "", bancoFondeo = "";
HashMap datos = new HashMap();
JSONObject resultado = new JSONObject();
JSONArray registros = new JSONArray();

ConsAfiliadoProveedores paginador = new ConsAfiliadoProveedores();


com.netro.seguridadbean.Seguridad seguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);

String idMenuP   = request.getParameter("idMenuP")==null?"15FORMA5":request.getParameter("idMenuP");
String ccPermiso = (request.getParameter("ccPermiso")!=null)?request.getParameter("ccPermiso"):"";
log.debug("<<<informacion: " + informacion + ">>>>"); 

/*log.debug("<<<nafinElectronico: " + nafinElectronico + ">>>>");
log.debug("<<<nombre: " + nombre + ">>>");
log.debug("<<<estado: " + estado + ">>>>");
log.debug("<<<rfc: " + rfc + ">>>>");
log.debug("<<<bancoFondeo: " + bancoFondeo + ">>>>");
log.debug("<<<icEpo: " + icEpo + ">>>>");
log.debug("<<<numeroProveedor: " + numeroProveedor + ">>>>");
log.debug("<<<numeroSirac: " + numeroSirac + ">>>>");
log.debug("<<<modifPropietario: " + modifPropietario + ">>>>");
log.debug("<<<pymeSinProveedor: " + pymeSinProveedor + ">>>>");
log.debug("<<<convenioUnico: " + convenioUnico + ">>>>");
log.debug("<<<operaFideicomiso: " + operaFideicomiso + ">>>>");
log.debug("<<<icPyme: " + icPyme + ">>>>");
*/
if (informacion.equals("valoresIniciales")) {
	
	JSONObject jsonObj = new JSONObject();
	//** Fodea 021-2015 (E);
	String  [] permisosSolicitados = { "ACCI_ELI_P", "ACCI_REAFILIA_P",  "BTNACTUALIZARNE",  "BTNACTUALIZARNESIRAC", "CAMBIO_PERFIL" };       
		
	List permisos =  seguridadBean.getPermisosPorMenu( iTipoPerfil, strPerfil, idMenuP, permisosSolicitados );     
	System.out.println("permisos===> "+permisos ); 
	//** Fodea 021-2015 (S);   
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("permisos",  permisos);   
	infoRegresar = jsonObj.toString();	

}else  if(informacion.equals("catalogoEstado")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_estado");
	cat.setCampoClave("ic_estado");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("cd_nombre");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("catalogoEstadoMod")){

	CatalogoEstado catalogo = new CatalogoEstado();
	catalogo.setClavePais(comboPais);
	catalogo.setCampoClave("ic_estado");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setOrden("cd_nombre");
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("catalogoBancoFondeo")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_banco_fondeo");
	cat.setCampoClave("ic_banco_fondeo");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setOrden("cd_descripcion");
	infoRegresar = cat.getJSONElementos();
	
} else if(informacion.equals("catalogoEPO")){
	
	CatalogoEPO cat = new CatalogoEPO();
   cat.setCampoClave("CE.ic_epo");
   cat.setCampoDescripcion("CE.cg_razon_social");
	cat.setIDIF(bancoFondeo);
	cat.setOrden("CE.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
} else if(informacion.equals("catalogoPaisOrigen")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_pais");
	cat.setCampoClave("ic_pais");
	cat.setCampoDescripcion("cd_descripcion");
	infoRegresar = cat.getJSONElementos();	
	
} else if(informacion.equals("catalogoIdentificacion")){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_identificacion");
	cat.setCampoClave("IC_IDENTIFICACION");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setOrden("IC_IDENTIFICACION");
	infoRegresar = cat.getJSONElementos();		
	
} else if(informacion.equals("catalogoGradoEscolaridad")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_grado_esc");
	cat.setCampoClave("IC_GRADO_ESC");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setOrden("IC_GRADO_ESC");
	infoRegresar = cat.getJSONElementos();	

} else if(informacion.equals("catalogoEstadoCivil")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_estado_civil");
	cat.setCampoClave("IC_ESTADO_CIVIL");
	cat.setCampoDescripcion("CD_DESCRIPCION");
	cat.setOrden("IC_ESTADO_CIVIL");
	infoRegresar = cat.getJSONElementos();	

} else if(informacion.equals("catalogoDelegMun")){

	CatalogoMunicipio cat = new CatalogoMunicipio();
	cat.setPais(comboPais);
	cat.setEstado(comboEstado);
	cat.setClave("IC_MUNICIPIO");
	cat.setDescripcion("upper(CD_NOMBRE)");
	List elementos = cat.getListaElementos();
	JSONArray jsonArr = new JSONArray();
	Iterator it = elementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
	

} else if(informacion.equals("catalogoCiudad")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_ciudad");
	cat.setCampoClave("codigo_ciudad");
	cat.setCampoDescripcion("nombre");
	cat.setCondicionIn(comboPais, "codigo_pais");
	cat.setCondicionIn(comboEstado, "codigo_departamento");
	cat.setOrden("nombre");
	infoRegresar = cat.getJSONElementos();	

} else if(informacion.equals("catalogoRegimenMatrimonial")){

	datos = new HashMap();
	datos.put("clave", "S");
	datos.put("descripcion", "SEPARACIÓN DE BIENES");
	registros.add(datos);
	datos = new HashMap();
	datos.put("clave", "M");
	datos.put("descripcion", "BIENES MANCOMUNADOS");
	registros.add(datos);
	resultado.put("success", new Boolean(true));
	resultado.put("total", ""+registros.size());
	resultado.put("registros", registros.toString());
	infoRegresar = resultado.toString();

} else if(informacion.equals("catalogoTipoCategoria")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_tipo_categoria");
	cat.setCampoClave("IC_TIPO_CATEGORIA");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setOrden("CD_NOMBRE");
	infoRegresar = cat.getJSONElementos();	
		
} else if(informacion.equals("catalogoSectorEconomico")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_sector_econ");
	cat.setCampoClave("IC_SECTOR_ECON");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setOrden("CD_NOMBRE");
	infoRegresar = cat.getJSONElementos();	
		
} else if(informacion.equals("catalogoSubSector")){

	if( sectorEcon.equals("") ){
		resultado.put("success", new Boolean(true));
		resultado.put("total", "0");
		resultado.put("registros", "");
		infoRegresar = resultado.toString();
	} else {
		CatalogoSubSector cat = new CatalogoSubSector();
		cat.setTabla("comcat_subsector");
		cat.setCampoClave("IC_SUBSECTOR");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setOrden("CD_NOMBRE");
		cat.setSector(sectorEcon);
		infoRegresar = cat.getJSONElementos();	
	}
	
} else if(informacion.equals("catalogoRama")){

	if( sectorEcon.equals("") || subSector.equals("") ){
		resultado.put("success", new Boolean(true));
		resultado.put("total", "0");
		resultado.put("registros", "");
		infoRegresar = resultado.toString();
	} else {
		CatalogoRama cat = new CatalogoRama();
		cat.setTabla("comcat_rama");
		cat.setCampoClave("ic_rama");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setOrden("CD_NOMBRE");
		cat.setSector(sectorEcon);
		cat.setSubSector(subSector);
		infoRegresar = cat.getJSONElementos();	
	}
	
} else if(informacion.equals("catalogoClase")){
	
	if( sectorEcon.equals("") || subSector.equals("") || rama.equals("") ){
		resultado.put("success", new Boolean(true));
		resultado.put("total", "0");
		resultado.put("registros", "");
		infoRegresar = resultado.toString();
	} else {
		CatalogoClase cat = new CatalogoClase(); 
		cat.setTabla("comcat_clase");
		cat.setCampoClave("IC_CLASE");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setOrden("CD_NOMBRE");
		cat.setSector(sectorEcon);
		cat.setSubSector(subSector);
		cat.setRama(rama);
		infoRegresar = cat.getJSONElementos();	
	}
			
} else if(informacion.equals("catalogoTipoEmpresa")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_tipo_empresa");
	cat.setCampoClave("ic_tipo_empresa");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setOrden("CD_NOMBRE");
	infoRegresar = cat.getJSONElementos();	
		
} else if(informacion.equals("catalogoDomicilioCorrespondencia")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_dom_correspondencia");
	cat.setCampoClave("ic_dom_correspondencia");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setOrden("CD_NOMBRE");
	infoRegresar = cat.getJSONElementos();	
		
} else if(informacion.equals("catalogoVersionConvenio")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_version_convenio");
	cat.setCampoClave("ic_version_convenio");
	cat.setCampoDescripcion("TRIM(cd_version_convenio)");
	cat.setOrden("cd_version_convenio");
	infoRegresar = cat.getJSONElementos();	
		
} else if(informacion.equals("catalogoAgenciaSirac")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_agencia_sirac");
	cat.setCampoClave("ic_estado");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setOrden("cd_descripcion");
	infoRegresar = cat.getJSONElementos();	
		
} else if(informacion.equals("catalogoSolicitanteEPO")){
	
	CatalogoSolicitanteEPO cat = new CatalogoSolicitanteEPO();
	cat.setClave("ce.ic_epo");
	cat.setDescripcion("ce.cg_razon_social");
	cat.setSeleccion(icPyme);
	infoRegresar = cat.getJSONElementos();	
		
} else if(informacion.equals("consultar") || informacion.equals("imprimir") || informacion.equals("generarArchivo")){

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	paginador.setNumeroBancoFondeo(bancoFondeo);
	paginador.setSinNumeroProveedor(pymeSinProveedor);
	paginador.setNafinElectronico(nafinElectronico);
	paginador.setRfc(rfc);
	paginador.setCadenaProductiva(icEpo);
	paginador.setNumeroProveedor(numeroProveedor);
	paginador.setNumeroClienteSIRAC(numeroSirac);
	paginador.setDatosPersModif(modifPropietario);
	paginador.setConvenioUnico(convenioUnico);
	paginador.setNumDistribuidor("");
	paginador.setEstado(estado);
	paginador.setNombre(nombre);
	paginador.setOperaFideicomiso(operaFideicomiso);
	paginador.setOperaFactDistribuido(chkFactDistribuidoCons);
	paginador.setSuceptibleFloating(suceptibleFloating);        
	paginador.setProvExtranjero(chkProvExtranjeroCons); 	 
	paginador.setOperaDescAutoEPO(chkOperaDescAutoEPOCons);	//chkOperaDescAutoEPOCons
         
	if (informacion.equals("consultar") || informacion.equals("imprimir")){	
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos (star y limit)", e);
		}
			if(informacion.equals("consultar")) {		
				try {
					if (operacion.equals("generar")) {				
						queryHelper.executePKQuery(request);
					}	
					consulta = queryHelper.getJSONPageResultSet(request,start,limit);				
				} catch(Exception e) {
					throw new AppException(" Error en la paginacion", e);
				}
				resultado = JSONObject.fromObject(consulta);
			} else if( informacion.equals("imprimir") ) {
				try {
					nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
					resultado.put("success", new Boolean(true));
					resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
		}
	} else if( informacion.equals("generarArchivo") ) {
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}			
	}		
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("eliminarRegistro")){
	
	String msg = "";

	Afiliacion beanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

	try{
		msg = beanAfiliacion.borraPyme(iNoUsuario, icPyme, icEpo);
		if(msg.equals("Se borro la PYME")){
			msg = "El registro se ha eliminado correctamente";
		} else{
			msg = "Ocurrió un error al intentar eliminar la PYME.";
		}
		resultado.put("success", new Boolean(true));
		resultado.put("msg", msg);
	}catch(NafinException lexError){
		log.warn("Error en " + informacion + " : " + lexError);
		resultado.put("success", new Boolean(true));
		resultado.put("msg", "Ocurrió un error al intentar eliminar la PYME");
	}
	infoRegresar = resultado.toString();

} else if(informacion.equals("consultaModificar")){
	
				//** Fodea 021-2015 (E);
	String  [] permisosSolicitados = {"BTNACTUALIZARNE",  "BTNACTUALIZARNESIRAC",  "BTNCANCELARM"  };        
	
	List permisos =  seguridadBean.getPermisosPorMenu( iTipoPerfil, strPerfil, idMenuP, permisosSolicitados );     
	
	//** Fodea 021-2015 (S);   
	
	String icEpoTmp = ("EPO".equals(strTipoUsuario)?iNoCliente:icEpo);
	
	Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	
	boolean reqOficina1 = BeanAfiliacion.requiereOficinaTramitadora("1");
	boolean reqOficina2 = BeanAfiliacion.requiereOficinaTramitadora("2");
	boolean reqOficina5 = BeanAfiliacion.requiereOficinaTramitadora("5");
	
	List oficina_descuento  =  BeanAfiliacion.getOficinaTramitadora( "1", icEpoTmp, icPyme);
	List oficina_anticipo   =  BeanAfiliacion.getOficinaTramitadora( "2", icEpoTmp, icPyme);
	List oficina_inventario =  BeanAfiliacion.getOficinaTramitadora( "5", icEpoTmp, icPyme);

	String ic_oficina_tramitadora_descuento  = (oficina_descuento.size()  > 0 )?(String)oficina_descuento.get(0): "";
	String ic_oficina_tramitadora_anticipo   = (oficina_anticipo.size()   > 0 )?(String)oficina_anticipo.get(0):  "";
	String ic_oficina_tramitadora_inventario = (oficina_inventario.size() > 0 )?(String)oficina_inventario.get(0):"";	
	
	VerificaRequisitos requisitos = new VerificaRequisitos();
	boolean flagAnticipo = requisitos.validaRequisitos(Integer.parseInt(iNoCliente),Integer.parseInt(icPyme));
	boolean flagCrediCadenas = requisitos.validaRequisitos(Integer.parseInt(icEpoTmp),Integer.parseInt(icPyme), "5" );
		
	ConsultaProveeodresModificar consultaProveeodresModificar = new ConsultaProveeodresModificar();
	consultaProveeodresModificar.setIcPyme(Integer.parseInt(icPyme));
	consultaProveeodresModificar.setIcEpo(Integer.parseInt(icEpoTmp));
	consultaProveeodresModificar.setIcProductoNafin(2);
	String strHabAnt = consultaProveeodresModificar.consultaStrHab();
	consultaProveeodresModificar.setIcProductoNafin(5);
	String strHabInv = consultaProveeodresModificar.consultaStrHab();
	if(strHabAnt.equals("")){
		strHabAnt = "N";
	}
	if(strHabInv.equals("")){
		strHabInv = "N";
	}
	
	HashMap datosModificar = new HashMap();
	paginador.setPyme(icPyme);
	paginador.setIcEpo(icEpo);
	try{
	datosModificar = paginador.obtieneDatosPyme();
	registros.add(datosModificar);
	resultado.put("success", new Boolean(true));
	}catch(Exception e){
		log.warn("Error en el HashMap: " + e);
		resultado.put("success", new Boolean(false));
	}
	
	String strTAfilia =  consultaProveeodresModificar.getstrTAfilia(icPyme, icEpo );
		
	resultado.put("strTipoUsuario",                    strTipoUsuario);
	resultado.put("total",                             ""+ registros.size());
	resultado.put("registro",                          datosModificar);
	resultado.put("ic_oficina_tramitadora_descuento",  ic_oficina_tramitadora_descuento );
	resultado.put("ic_oficina_tramitadora_anticipo",   ic_oficina_tramitadora_anticipo  );
	resultado.put("ic_oficina_tramitadora_inventario", ic_oficina_tramitadora_inventario);	
	resultado.put("strHabAnt",                         strHabAnt);
	resultado.put("strHabInv",                  			strHabInv);
	resultado.put("flagAnticipo",                      new Boolean(flagAnticipo));
	resultado.put("flagCrediCadenas",                  new Boolean(flagCrediCadenas));
	resultado.put("reqOficina1",                       new Boolean(reqOficina1));
	resultado.put("reqOficina2",                       new Boolean(reqOficina2));
	resultado.put("reqOficina5",                       new Boolean(reqOficina5));
	resultado.put("permisos",   permisos);	
	resultado.put("strTAfilia",   strTAfilia); 	
	infoRegresar = resultado.toString();

} else if(informacion.equals("GuardaDatos")){
	
	String Sin_Num_Prov                 = (request.getParameter("Sin_Num_Prov")                        == null? "N": request.getParameter("Sin_Num_Prov"));
	String strTAfilia 		            = (request.getParameter("strTAfilia")                       	== null) ?"": request.getParameter("strTAfilia");
	String tipo_persona1 		         = (request.getParameter("tipo_persona1")                       == null) ?"": request.getParameter("tipo_persona1");
	String ic_pyme1 				         = (request.getParameter("ic_pyme1")                            == null) ?"": request.getParameter("ic_pyme1");
	String ic_epo_mod1 				      = (request.getParameter("ic_epo_mod1")                         == null) ?"": request.getParameter("ic_epo_mod1");
	String ic_domicilio1 				   = (request.getParameter("ic_domicilio1")                       == null) ?"": request.getParameter("ic_domicilio1");
	String ic_contacto1 				      = (request.getParameter("ic_contacto1")                        == null) ?"": request.getParameter("ic_contacto1");
	String Numero_Exterior 				   = (request.getParameter("Numero_Exterior1")                    == null) ?"": request.getParameter("Numero_Exterior1");
	String actualiza_sirac1 				= (request.getParameter("actualiza_sirac1")                    == null) ?"": request.getParameter("actualiza_sirac1");
																																		
	String solicitante_mod1 		      = (request.getParameter("solicitante_mod1") 	                  == null) ?"": request.getParameter("solicitante_mod1");
	String combo_solicitante_mod1 	   = (request.getParameter("combo_solicitante_mod1") 	            == null) ?"": request.getParameter("combo_solicitante_mod1");
																																		
	String apellido_paterno_mod1 			= (request.getParameter("apellido_paterno_mod1") 					== null) ?"": request.getParameter("apellido_paterno_mod1");
	String apellido_materno_mod1 			= (request.getParameter("apellido_materno_mod1") 					== null) ?"": request.getParameter("apellido_materno_mod1");
	String nombre_mod1 						= (request.getParameter("nombre_mod1") 								== null) ?"": request.getParameter("nombre_mod1");
	String rfc_mod1 							= (request.getParameter("rfc_mod1") 									== null) ?"": request.getParameter("rfc_mod1");
	String invalidar_modF1 					= (request.getParameter("invalidar_modF1") 							== null) ?"": request.getParameter("invalidar_modF1");
	String curp_mod1 							= (request.getParameter("curp_mod1") 									== null) ?"": request.getParameter("curp_mod1");
	String fiel_mod1 							= (request.getParameter("fiel_mod1") 									== null) ?"": request.getParameter("fiel_mod1");
	String apellido_casada_mod1 			= (request.getParameter("apellido_casada_mod1") 					== null) ?"": request.getParameter("apellido_casada_mod1");
	String regimen_matrimonial_mod1 		= (request.getParameter("regimen_matrimonial_mod1") 				== null) ?"": request.getParameter("regimen_matrimonial_mod1");
	String sexo_mod1							= (request.getParameter("sexo_mod1") 									== null) ?"": request.getParameter("sexo_mod1");
	String estado_civil_mod1 				= (request.getParameter("estado_civil_mod1") 						== null) ?"": request.getParameter("estado_civil_mod1");
	String fecha_nac_mod1 					= (request.getParameter("fecha_nac_mod1") 							== null) ?"": request.getParameter("fecha_nac_mod1");
	String pais_origen_modF1 				= (request.getParameter("pais_origen_modF1") 						== null) ?"": request.getParameter("pais_origen_modF1");
	String identificacion_mod1    		= (request.getParameter("identificacion_mod1") 						== null) ?"": request.getParameter("identificacion_mod1");
	String grado_escolaridad_mod1 		= (request.getParameter("grado_escolaridad_mod1") 					== null) ?"": request.getParameter("grado_escolaridad_mod1");
	String razon_social_mod1 				= (request.getParameter("razon_social_mod1") 						== null) ?"": request.getParameter("razon_social_mod1");
	String nombre_comer_mod1 				= (request.getParameter("nombre_comer_mod1") 						== null) ?"": request.getParameter("nombre_comer_mod1");
	String rfc_modM1 							= (request.getParameter("rfc_modM1") 									== null) ?"": request.getParameter("rfc_modM1");
	String invalidar_modM1 					= (request.getParameter("invalidar_modM1") 							== null) ?"": request.getParameter("invalidar_modM1");
	String fiel_modM1 						= (request.getParameter("fiel_modM1") 									== null) ?"": request.getParameter("fiel_modM1");
	String pais_origen_modM1 				= (request.getParameter("pais_origen_modM1") 						== null) ?"": request.getParameter("pais_origen_modM1");
	String direccion_mod1 					= (request.getParameter("direccion_mod1") 							== null) ?"": request.getParameter("direccion_mod1");
	String colonia_mod1 						= (request.getParameter("colonia_mod1") 								== null) ?"": request.getParameter("colonia_mod1");
	String cod_postal_mod1 					= (request.getParameter("cod_postal_mod1") 							== null) ?"": request.getParameter("cod_postal_mod1");
	String pais_mod1 							= (request.getParameter("pais_mod1") 									== null) ?"": request.getParameter("pais_mod1");
	String estado_mod1 						= (request.getParameter("estado_mod1") 								== null) ?"": request.getParameter("estado_mod1");
	String deleg_mun_mod1 					= (request.getParameter("deleg_mun_mod1") 							== null) ?"": request.getParameter("deleg_mun_mod1");
	String ciudad_mod1 						= (request.getParameter("ciudad_mod1") 								== null) ?"": request.getParameter("ciudad_mod1");
	String telefono_mod1 					= (request.getParameter("telefono_mod1") 								== null) ?"": request.getParameter("telefono_mod1");
	String fax_mod1 							= (request.getParameter("fax_mod1") 									== null) ?"": request.getParameter("fax_mod1");
	String ap_paterno_rep_mod1 			= (request.getParameter("ap_paterno_rep_mod1") 						== null) ?"": request.getParameter("ap_paterno_rep_mod1");
	String ap_materno_rep_mod1 			= (request.getParameter("ap_materno_rep_mod1") 						== null) ?"": request.getParameter("ap_materno_rep_mod1");
	String nombre_rep_mod1 					= (request.getParameter("nombre_rep_mod1") 							== null) ?"": request.getParameter("nombre_rep_mod1");
	String telefono_rep_mod1 				= (request.getParameter("telefono_rep_mod1") 						== null) ?"": request.getParameter("telefono_rep_mod1");
	String fax_rep_mod1 						= (request.getParameter("fax_rep_mod1") 								== null) ?"": request.getParameter("fax_rep_mod1");
	String email_rep_mod1 					= (request.getParameter("email_rep_mod1") 							== null) ?"": request.getParameter("email_rep_mod1");
	String rfc_rep_mod1						= (request.getParameter("rfc_rep_mod1") 								== null) ?"": request.getParameter("rfc_rep_mod1");
	String no_identificacion_rep_mod1 	= (request.getParameter("no_identificacion_rep_mod1") 			== null) ?"" : request.getParameter("no_identificacion_rep_mod1");
	String identificacion_rep_mod1	 	= (request.getParameter("identificacion_rep_mod1") 				== null) ?"" : request.getParameter("identificacion_rep_mod1");
	String ap_paterno_cont_mod1 			= (request.getParameter("ap_paterno_cont_mod1") 					== null) ?"": request.getParameter("ap_paterno_cont_mod1");
	String ap_materno_cont_mod1 			= (request.getParameter("ap_materno_cont_mod1") 					== null) ?"": request.getParameter("ap_materno_cont_mod1");
	String nombre_cont_mod1 				= (request.getParameter("nombre_cont_mod1") 							== null) ?"": request.getParameter("nombre_cont_mod1");
	String telefono_cont_mod1 				= (request.getParameter("telefono_cont_mod1") 						== null) ?"": request.getParameter("telefono_cont_mod1");
	String fax_cont_mod1 					= (request.getParameter("fax_cont_mod1") 								== null) ?"": request.getParameter("fax_cont_mod1");
	String email_cont_mod1 					= (request.getParameter("email_cont_mod1") 							== null) ?"": request.getParameter("email_cont_mod1");
	String celular_cont_mod1 				= (request.getParameter("celular_cont_mod1") 						== null) ?"": request.getParameter("celular_cont_mod1");
	String num_proveedor_cont_mod1		= (request.getParameter("num_proveedor_cont_mod1") 				== null) ?"": request.getParameter("num_proveedor_cont_mod1");
	String tipo_categoria_mod1 			= (request.getParameter("tipo_categoria_mod1") 						== null) ?"": request.getParameter("tipo_categoria_mod1");
	String ejecut_cuenta_mod1 				= (request.getParameter("ejecut_cuenta_mod1") 						== null) ?"": request.getParameter("ejecut_cuenta_mod1");
	String no_escritura_mod1 				= (request.getParameter("no_escritura_mod1") 						== null) ?"": request.getParameter("no_escritura_mod1");
	String no_notaria_mod1 					= (request.getParameter("no_notaria_mod1") 							== null) ?"": request.getParameter("no_notaria_mod1");
	String empleos_gen_mod1 				= (request.getParameter("empleos_gen_mod1") 							== null) ?"": request.getParameter("empleos_gen_mod1");
	String activo_total_mod1 				= (request.getParameter("activo_total_mod1") 						== null) ?"": request.getParameter("activo_total_mod1");
	String cap_contable_mod1 				= (request.getParameter("cap_contable_mod1") 						== null) ?"": request.getParameter("cap_contable_mod1");
	String ventas_exp_mod1 					= (request.getParameter("ventas_exp_mod1") 							== null) ?"": request.getParameter("ventas_exp_mod1");
	String ventas_tot_mod1 					= (request.getParameter("ventas_tot_mod1") 							== null) ?"": request.getParameter("ventas_tot_mod1");
	String fecha_const_mod1 				= (request.getParameter("fecha_const_mod1") 							== null) ?"": request.getParameter("fecha_const_mod1");
	String no_empleados_mod1 				= (request.getParameter("no_empleados_mod1") 						== null) ?"": request.getParameter("no_empleados_mod1");
	String no_proveedor_diversos_mod1 	= (request.getParameter("num_proveedor_diversos_mod1") 			== null) ?"": request.getParameter("num_proveedor_diversos_mod1");
	String sector_mod1 						= (request.getParameter("sector_mod1") 								== null) ?"": request.getParameter("sector_mod1");
	String subsector_mod1 					= (request.getParameter("subsector_mod1") 							== null) ?"": request.getParameter("subsector_mod1");
	String rama_mod1 							= (request.getParameter("rama_mod1") 									== null) ?"": request.getParameter("rama_mod1");
	String clase_mod1 						= (request.getParameter("clase_mod1") 									== null) ?"": request.getParameter("clase_mod1");
	String tipo_empresa_mod1 				= (request.getParameter("tipo_empresa_mod1") 						== null) ?"": request.getParameter("tipo_empresa_mod1");
	String domicilio_corr_mod1 			= (request.getParameter("domicilio_corr_mod1") 						== null) ?"": request.getParameter("domicilio_corr_mod1");
	String princip_productos_mod1 		= (request.getParameter("princip_productos_mod1") 					== null) ?"": request.getParameter("princip_productos_mod1");
	String num_sirac_mod1 					= (request.getParameter("num_sirac_mod1") 							== null) ?"": request.getParameter("num_sirac_mod1");
	String num_troya_mod1 					= (request.getParameter("num_troya_mod1") 							== null) ?"": request.getParameter("num_troya_mod1");
	String alta_cliente_troya_mod1 		= (request.getParameter("alta_cliente_troya_mod1") 				== null) ?"": request.getParameter("alta_cliente_troya_mod1");
	String autoriza_mod1 					= (request.getParameter("autoriza_mod1") 								== null) ?"": request.getParameter("autoriza_mod1");
	String susceptible_desconstar_mod1 	= (request.getParameter("susceptible_desconstar_mod1") 			== null) ?"": request.getParameter("susceptible_desconstar_mod1");
	String ic_oficina_tram_descuento1 	= (request.getParameter("ic_oficina_tramitadora_descuento1") 	== null) ?"": request.getParameter("ic_oficina_tramitadora_descuento1");
	String version_convenio_mod1 			= (request.getParameter("version_convenio_mod1") 					== null) ?"": request.getParameter("version_convenio_mod1");
	String anticipo_mod1 					= (request.getParameter("anticipo_mod1") 								== null) ?"": request.getParameter("anticipo_mod1");
	String ic_oficina_tram_anticipo1 	= (request.getParameter("ic_oficina_tramitadora_anticipo1") 	== null) ?"": request.getParameter("ic_oficina_tramitadora_anticipo1");
	String fecha_consulta_convenio_mod1 = (request.getParameter("fecha_consulta_convenio_mod1") 			== null) ?"": request.getParameter("fecha_consulta_convenio_mod1");
	String credicadenas_mod1 				= (request.getParameter("credicadenas_mod1") 						== null) ?"": request.getParameter("credicadenas_mod1");
	String ic_oficina_tram_inventario1	= (request.getParameter("ic_oficina_tramitadora_inventario1") 	== null) ?"": request.getParameter("ic_oficina_tramitadora_inventario1");
	String convenio_unico_prod_mod1	 	= (request.getParameter("convenio_unico_prod_mod1")	 			== null) ?"": request.getParameter("convenio_unico_prod_mod1");
	String cesion_der_prod_mod1 			= (request.getParameter("cesion_der_prod_mod1") 					== null) ?"": request.getParameter("cesion_der_prod_mod1");
	String fideicomiso_desarrollo		   = (request.getParameter("fideicomiso_desarrollo_prod_mod1") 	== null) ?"": request.getParameter("fideicomiso_desarrollo_prod_mod1");
	String fecha_convenio_prod_mod1 		= (request.getParameter("fecha_convenio_prod_mod1") 				== null) ?"": request.getParameter("fecha_convenio_prod_mod1");
	String fianza_electronica_mod1 		= (request.getParameter("fianza_electronica_mod1") 				== null) ?"": request.getParameter("fianza_electronica_mod1");
	String chkEntidadGobierno 		= (request.getParameter("chkEntidadGobierno") 				== null) ?"": request.getParameter("chkEntidadGobierno");
	String chkFactDistribuido = (request.getParameter("chkFactDistribuido")   != null && request.getParameter("chkFactDistribuido").equals("true"))  ?"S":"N";
	String chkProvExtranjero = (request.getParameter("chkProvExtranjero")   != null && request.getParameter("chkProvExtranjero").equals("true"))  ?"S":"N";
	String chkOperaDescAutoEPO = (request.getParameter("chkOperaDescAutoEPO")   != null && request.getParameter("chkOperaDescAutoEPO").equals("true"))  ?"S":"N";
        
	
	String  qryActDiscl         = "";  
	String  campo               = "";
	String  strFechaNac         = "";
	String  qryAceptaAnt        = "";
	String  msg                 = "";
	String  solicitante         = "";
	String  icNafinElectronico  = "";
	String  rfcM					 = "";
	String  paisOrigen			 = "";
	String  fiel					 = "";
	String  emailRegistrado     = "";
	String  modifEmail          = "";
	String  Numero_distribuidor = "";
	String  nombreMunicipio     = "";
	String  nombreEpoSolicitante= "";
	String  ic_epo_1            = ("EPO".equals(strTipoUsuario)?iNoCliente:ic_epo_mod1);
	boolean actualizacion       = false;
	boolean actualizar          = false;
	boolean bExisteSIRAC        = false;
	boolean invalidar           = false;
	Vector  vResultado          = null;
	
	/** 	Se trabaja con las variables tipo boolean	 */	
	boolean actualizarSirac = (request.getParameter("actualiza_sirac1") != null && request.getParameter("actualiza_sirac1").equals("S") )?true:false;	
	if( tipo_persona1.equals("F") ){
		invalidar  = (request.getParameter("invalidar_modF1")     != null && request.getParameter("invalidar_modF1").equals("S")  )?true:false;
		rfcM       = rfc_mod1;
		paisOrigen = pais_origen_modF1;
		fiel       = fiel_mod1;
		/** Limpio los campos que no son requeridos para el tipo de persona F */
		razon_social_mod1 = "";
		nombre_comer_mod1 = "";
		autoriza_mod1     = "";
		num_sirac_mod1    = "";
		num_troya_mod1    = "N";
		
	} else if( tipo_persona1.equals("M") ){
		invalidar  = (request.getParameter("invalidar_modM1")     != null && request.getParameter("invalidar_modM1").equals("S")  )?true:false;
		rfcM       = rfc_modM1;
		paisOrigen = pais_origen_modM1;
		fiel       = fiel_modM1;
	}
	if( !Sin_Num_Prov.equals("") && Sin_Num_Prov.equals("true") ){
		Sin_Num_Prov = "S";
	} else {
		Sin_Num_Prov = "N";
	}
	if( !susceptible_desconstar_mod1.equals("") && susceptible_desconstar_mod1.equals("true") ){
		susceptible_desconstar_mod1 = "S";
	} else {
		susceptible_desconstar_mod1 = "";
	}
	if( !anticipo_mod1.equals("") &&  anticipo_mod1.equals("true") ){
		anticipo_mod1 = "S";
	} else {
		anticipo_mod1 = "N";
	}
	if( !credicadenas_mod1.equals("") && credicadenas_mod1.equals("true") ){
		credicadenas_mod1 = "S";
	} else {
		credicadenas_mod1 = "N";
	}
	if( !cesion_der_prod_mod1.equals("") && cesion_der_prod_mod1.equals("true") ){
		cesion_der_prod_mod1 = "S";
	} else {
		cesion_der_prod_mod1 = "N";
	}
	if( !fideicomiso_desarrollo.equals("") && fideicomiso_desarrollo.equals("true") ){
		fideicomiso_desarrollo = "S";
	} else {
		fideicomiso_desarrollo = "N";
	}
	if( !fianza_electronica_mod1.equals("")  && fianza_electronica_mod1.equals("true") ){
		fianza_electronica_mod1 = "S";
	} else {
		fianza_electronica_mod1 = "N";
	}
	if( !convenio_unico_prod_mod1.equals("")  && convenio_unico_prod_mod1.equals("true") ){
		convenio_unico_prod_mod1 = "S";
	} else {
		convenio_unico_prod_mod1 = "";
	}
	/** Se da formato a las fechas */
	if( !fecha_nac_mod1.equals("") ){
		fecha_nac_mod1 = fecha_nac_mod1.substring(8,10)+ "/" +fecha_nac_mod1.substring(5,7)+ "/" + fecha_nac_mod1.substring(0,4);
	}
	if( !fecha_const_mod1.equals("")  ){
		fecha_const_mod1 = fecha_const_mod1.substring(8,10)+ "/" +fecha_const_mod1.substring(5,7)+ "/" + fecha_const_mod1.substring(0,4);
	}
	if( !fecha_consulta_convenio_mod1.equals("") ){
		fecha_consulta_convenio_mod1 = fecha_consulta_convenio_mod1.substring(8,10)+ "/" +fecha_consulta_convenio_mod1.substring(5,7)+ "/" + fecha_consulta_convenio_mod1.substring(0,4);
	}
	if( !fecha_convenio_prod_mod1.equals("") ){
		fecha_convenio_prod_mod1 = fecha_convenio_prod_mod1.substring(8,10)+ "/" +fecha_convenio_prod_mod1.substring(5,7)+ "/" + fecha_convenio_prod_mod1.substring(0,4);
	}
	
	/** Obtengo el nombre del Municipio */
	ConsultaProveeodresModificar consultaMod = new ConsultaProveeodresModificar();
	if(!deleg_mun_mod1.equals("")){
		consultaMod.setIcPais(Integer.parseInt(pais_mod1));
		consultaMod.setIcEstado(Integer.parseInt(estado_mod1));
		consultaMod.setIcMunicipio(Integer.parseInt(deleg_mun_mod1));
		nombreMunicipio = consultaMod.consultaNombreMunicipio();
		deleg_mun_mod1 = deleg_mun_mod1 + "|" + nombreMunicipio;
	}		
	/** Se determina el solicitante */
	if( solicitante_mod1.equals("P") ){
		solicitante = " SOLICITANTE PROVEEDOR ";
	} else if( solicitante_mod1.equals("E") ){
		consultaMod.setIcEpo( Integer.parseInt(combo_solicitante_mod1) );
		nombreEpoSolicitante = consultaMod.consultaNombreEpo();
		solicitante = " SOLICITANTE " + nombreEpoSolicitante;
	}
	
	/**	Si la acción es invalidar. NO se realizan validaciones 	*/
	if( invalidar ){
		actualizar = true;
	} else {
		int total = 0;
		
		consultaMod.setNumeroCliente( no_proveedor_diversos_mod1 );
		consultaMod.setNumeroSirac( num_sirac_mod1 );
		consultaMod.setIcEpo( Integer.parseInt(ic_epo_1) );
		consultaMod.setIcPyme( Integer.parseInt(ic_pyme1) );
		consultaMod.setNumeroConsulta(1);
		
		total = consultaMod.consultaNumeroProveedor();
		if( total <= 1 ){	//EXISTE EL NUMERO DE PROVEEDOR PARA LA EPO Y LA PYME Y ES EL MISMO NO SE MODIFICO O NO EXISTE
			consultaMod.setNumeroConsulta(2);
			total = consultaMod.consultaNumeroProveedor();
			if( total == 0){ //EXISTE EL NUMERO DE PROVEEDOR PARA LA EPO EN OTRA PYME
				if( !num_sirac_mod1.equals("") ){ 
					total = consultaMod.consultaNumeroSirac();
					if( total > 0 ){ //EXISTE EL NUMERO DE SIRAC
						bExisteSIRAC = true;
					}
				}
				if(!bExisteSIRAC){
					//Si llega hasta aqui se puede actualizar el registro, dado que pasó todas las validaciones
					actualizar = true; 
				} else {
					msg = "El número de SIRAC ya existe.";
				}
			} else {
				msg = "El número de proveedor ya existe en la cadena.";
			}
		} else {
			msg = "El número de proveedor ya existe en la cadena.";
		}
		
		icNafinElectronico = consultaMod.consultaNafinElectronico();
		emailRegistrado = consultaMod.consultaEmail();
		
	}
	
	if( !chkEntidadGobierno.equals("")  && chkEntidadGobierno.equals("true") ){
		chkEntidadGobierno = "S";
	} else {
		chkEntidadGobierno = "N";
	}

/*	
System.err.println(iNoUsuario); 
System.err.println(ic_epo_1); 
System.err.println(ic_pyme1); 
System.err.println(razon_social_mod1); 
System.err.println(nombre_comer_mod1); 
System.err.println(rfcM); 
System.err.println(susceptible_desconstar_mod1); 
System.err.println(num_sirac_mod1); 
System.err.println(Numero_Exterior); 
System.err.println(colonia_mod1);
System.err.println(estado_mod1); 
System.err.println(fecha_nac_mod1); 
System.err.println(anticipo_mod1);
System.err.println(estado_civil_mod1); 
System.err.println(paisOrigen);
System.err.println(strTAfilia); 
System.err.println(sector_mod1); 
System.err.println(no_empleados_mod1); 
System.err.println(apellido_paterno_mod1); 
System.err.println(apellido_materno_mod1); 
System.err.println(nombre_mod1); 
System.err.println(telefono_mod1);
System.err.println(fax_mod1); 
System.err.println(sexo_mod1); 
System.err.println(regimen_matrimonial_mod1); 
System.err.println(apellido_casada_mod1); 
System.err.println(no_identificacion_rep_mod1);
System.err.println(tipo_persona1); 
System.err.println(direccion_mod1); 
System.err.println(cod_postal_mod1);
System.err.println(deleg_mun_mod1);
System.err.println(pais_mod1); 
System.err.println(grado_escolaridad_mod1); 
System.err.println(no_notaria_mod1); 
System.err.println(ap_paterno_rep_mod1); 
System.err.println(ap_materno_rep_mod1); 
System.err.println(nombre_rep_mod1); 
System.err.println(telefono_rep_mod1); 
System.err.println(fax_rep_mod1);
System.err.println(email_rep_mod1); 
System.err.println(rfc_rep_mod1); 
System.err.println(identificacion_rep_mod1); 
System.err.println(tipo_categoria_mod1);
System.err.println(ejecut_cuenta_mod1); 
System.err.println(empleos_gen_mod1); 
System.err.println(activo_total_mod1);
System.err.println(cap_contable_mod1); 
System.err.println(ventas_tot_mod1); 
System.err.println(ventas_exp_mod1);
System.err.println(fecha_const_mod1); 
System.err.println(subsector_mod1); 
System.err.println(rama_mod1); 
System.err.println(clase_mod1); 
System.err.println(tipo_empresa_mod1); 
System.err.println(domicilio_corr_mod1); 
System.err.println(princip_productos_mod1);
System.err.println(ic_domicilio1); 
System.err.println(no_escritura_mod1); 
System.err.println(ic_contacto1); 
System.err.println(ap_paterno_cont_mod1); 
System.err.println(ap_materno_cont_mod1); 
System.err.println(nombre_cont_mod1); 
System.err.println(telefono_cont_mod1); 
System.err.println(email_cont_mod1); 
System.err.println(fax_cont_mod1);
System.err.println(no_proveedor_diversos_mod1); 
System.err.println(Numero_distribuidor);
System.err.println(fideicomiso_desarrollo); 
System.err.println(credicadenas_mod1); 
System.err.println(num_troya_mod1);
System.err.println(ic_oficina_tram_descuento1); 
System.err.println(ic_oficina_tram_anticipo1);
System.err.println(ic_oficina_tram_inventario1); 
System.err.println(invalidar); 
System.err.println(actualizarSirac); 
System.err.println("null"); 
System.err.println(version_convenio_mod1); 
System.err.println(Sin_Num_Prov);
System.err.println(convenio_unico_prod_mod1); 
System.err.println(fecha_convenio_prod_mod1); 
System.err.println(cesion_der_prod_mod1); 
System.err.println(curp_mod1); 
System.err.println(fiel); 
System.err.println(ciudad_mod1); 
System.err.println(solicitante); 
System.err.println(fianza_electronica_mod1); 
System.err.println(autoriza_mod1);
System.err.println(fideicomiso_desarrollo);	
*/

	//if(actualizar){

		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		try{

if(tipo_persona1.equals("F")){
	identificacion_rep_mod1 = identificacion_mod1;

}

                    //------------QC- 2019----------
                    HashMap datosAnteriores = new HashMap();
                    paginador.setPyme(ic_pyme1);
                    paginador.setIcEpo(ic_epo_1);                            
                    datosAnteriores = paginador.obtieneDatosPyme();
                                                   
		                 
			vResultado = BeanAfiliacion.actualizaPyme(iNoUsuario, ic_epo_1, ic_pyme1, razon_social_mod1, 
										nombre_comer_mod1, rfcM, susceptible_desconstar_mod1, 
										num_sirac_mod1, Numero_Exterior, colonia_mod1,
										estado_mod1, fecha_nac_mod1, anticipo_mod1,
										estado_civil_mod1, paisOrigen,
										strTAfilia, sector_mod1, no_empleados_mod1, 
										apellido_paterno_mod1, apellido_materno_mod1, nombre_mod1, telefono_mod1,
										fax_mod1, sexo_mod1, regimen_matrimonial_mod1, apellido_casada_mod1, no_identificacion_rep_mod1,
										tipo_persona1, direccion_mod1, cod_postal_mod1, deleg_mun_mod1, 
										pais_mod1, grado_escolaridad_mod1, no_notaria_mod1, ap_paterno_rep_mod1, 
										ap_materno_rep_mod1, nombre_rep_mod1, telefono_rep_mod1, fax_rep_mod1,
										email_rep_mod1, rfc_rep_mod1, identificacion_rep_mod1, tipo_categoria_mod1,
										ejecut_cuenta_mod1, empleos_gen_mod1, activo_total_mod1,
										cap_contable_mod1, ventas_tot_mod1, ventas_exp_mod1,
										fecha_const_mod1, subsector_mod1, rama_mod1, clase_mod1, 
										tipo_empresa_mod1, domicilio_corr_mod1, princip_productos_mod1,
										ic_domicilio1, no_escritura_mod1, ic_contacto1, ap_paterno_cont_mod1, 
										ap_materno_cont_mod1, nombre_cont_mod1, telefono_cont_mod1, email_cont_mod1, fax_cont_mod1,
										no_proveedor_diversos_mod1, Numero_distribuidor,
										fideicomiso_desarrollo, credicadenas_mod1, num_troya_mod1,
										ic_oficina_tram_descuento1, ic_oficina_tram_anticipo1,
										ic_oficina_tram_inventario1, invalidar, actualizarSirac, null, version_convenio_mod1, Sin_Num_Prov,
										convenio_unico_prod_mod1, fecha_convenio_prod_mod1, cesion_der_prod_mod1, curp_mod1, fiel, ciudad_mod1, 
										solicitante, fianza_electronica_mod1, autoriza_mod1, fideicomiso_desarrollo, chkEntidadGobierno ,chkFactDistribuido, 
                                                                                chkProvExtranjero, chkOperaDescAutoEPO );


                            
                           
                           HashMap datosActuales = new HashMap();
                            paginador.setPyme(ic_pyme1);
                            paginador.setIcEpo(ic_epo_1);                            
                            datosActuales = paginador.obtieneDatosPyme();                            
                    
                            HashMap datosPyme = new HashMap();                            
                            datosPyme.put("iNoUsuario", iNoUsuario);
                            datosPyme.put("icPyme", ic_pyme1);
                            datosPyme.put("icEpo", ic_epo_1);
                            datosPyme.put("icNafinElectronico", icNafinElectronico);                            
                            
                            paginador.validaAfiliados( datosAnteriores, datosActuales,  datosPyme );
                            

			if(emailRegistrado.equals(email_cont_mod1)){
				modifEmail = "N";
			} else {
			 modifEmail = "S";
			}
			BeanAfiliacion.modificaPymeFactorajeMovil(icNafinElectronico, strLogin, strNombreUsuario, celular_cont_mod1, modifEmail);																						
			
			msg = (vResultado == null)?"":(String)vResultado.get(1);
			if( msg .equals("") ){
				msg = "Los datos han sido actualizados.";
			}
			actualizacion = true;
					
		} catch(Exception e){
			msg = (vResultado == null)?"":(String)vResultado.get(1);
			if( msg .equals("") ){
				msg = "Ocurrió un error al actualizar los datos. " + e;
			}
			actualizacion = false;
		}
		

	//}	

	resultado.put("success", new Boolean(true));
	resultado.put("mensaje", msg);
	resultado.put("actualizacion", new Boolean(actualizacion));
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("verCuentasPorAfiliado") || informacion.equals("imprimirCuentasPorAfiliado")){

	String datosAfiliadoStr = "";
	String tmpItCuentas = "";
	HashMap mapaAfiliado = new HashMap();
	List datosCuentas = new ArrayList();
	paginador.setTipoAfiliado(tipoAfiliado);
	paginador.setPyme(icPyme);
	List datosAfiliado = paginador.obtieneDatosAfiliado();
	if (datosAfiliado.size() >0) {
		//N@E: RazonSocial - Numero de Nafin Electronico
		datosAfiliadoStr = "N@E:" + (String)datosAfiliado.get(0) + " - " + (String) datosAfiliado.get(1);
	}

	UtilUsr utilUsr = new UtilUsr();
	List cuentas = utilUsr.getUsuariosxAfiliado(icPyme, tipoAfiliado);
	Iterator itCuentas = cuentas.iterator();
	while (itCuentas.hasNext()) {
		tmpItCuentas = (String)itCuentas.next();
		mapaAfiliado = new HashMap();
		mapaAfiliado.put("CUENTA", tmpItCuentas);
		datosCuentas.add(tmpItCuentas);
		registros.add(mapaAfiliado);
	}
	
	if(informacion.equals("verCuentasPorAfiliado")){
		resultado.put("total", ""+registros.size());
		resultado.put("registros", registros.toString());
		resultado.put("datos_afiliado", datosAfiliadoStr);	
	} else if(informacion.equals("imprimirCuentasPorAfiliado")){
		paginador.setListaCuentas(datosCuentas);
		paginador.setDatosAfiliado(datosAfiliadoStr);
		nombreArchivo = paginador.imprimeCuentasUsuariosPorAfiliado(request, strDirectorioTemp);
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	}

	resultado.put("success", new Boolean(true));
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("consultaReafiliacion")){

	HashMap datosReafiliacion = new HashMap();
	ConsultaProveeodresReafiliacion reafiliacion = new ConsultaProveeodresReafiliacion();
	reafiliacion.setIcPyme(icPyme);
	datosReafiliacion = reafiliacion.consultaReafilacion();
	resultado.put("registro", datosReafiliacion);
	resultado.put("success", new Boolean(true));
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("catalogoGrupoEpo")){

	CatalogoGrupo cat = new CatalogoGrupo();
	cat.setClave("IC_GRUPO_EPO");
	cat.setDescripcion("CG_DESCRIPCION");
	List elementos = cat.getListaElementos();
	JSONArray jsonArr = new JSONArray();
	Iterator it = elementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
	
} else if(informacion.equals("catalogoEpoReafiliacion")){

	CatalogoEpoReafiliar cat = new CatalogoEpoReafiliar();
	cat.setClave("IC_EPO");
	cat.setDescripcion("CG_RAZON_SOCIAL");
	cat.setIcPyme(Integer.parseInt(icPyme));
	cat.setFiltroEpo(filtroEpo);
	List elementos = cat.getListaElementos();

	HashMap datosCombo = new HashMap();
	JSONArray jsonArr = new JSONArray();
	Iterator it = elementos.iterator();
	String descripcion = "";
	String clave = "";
	String textoBuscado = " ";
	String descripcionTmp = "";
	int contador = 0;

	while(it.hasNext()) {
		Object obj = it.next();
		descripcionTmp = "";
		contador = 0;
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			clave = ec.getClave();
			descripcion = ec.getDescripcion();
			if(descripcion.length() > 45){
				descripcionTmp = descripcion.substring(0,45);
				contador = descripcionTmp.lastIndexOf(textoBuscado);
				descripcion = descripcion.substring(0,contador) + "<br>" + descripcion.substring(contador,descripcion.length());
			}
			datosCombo = new HashMap();
			datosCombo.put("clave", clave);
			datosCombo.put("descripcion", descripcion);
			jsonArr.add(datosCombo);
		}
	}

	infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	
} else if(informacion.equals("consultaGridReafiliacion")){
	
	String reafiliarPor = request.getParameter("reafiliar_por") == null?"0":(String)request.getParameter("reafiliar_por"); 
	String icEpoReafiliar = request.getParameter("nombre_epo_reafiliar") == null?"0":(String)request.getParameter("nombre_epo_reafiliar");
	String icGrupoEpo = request.getParameter("grupo_epo") == null?"0":(String)request.getParameter("grupo_epo");
		
	ConsultaProveeodresReafiliacion reafilia = new ConsultaProveeodresReafiliacion(); 
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(reafilia);
	reafilia.setTipoReafiliacion(reafiliarPor);
	reafilia.setIcPyme(icPyme);
	if(reafiliarPor.equals("EPO")){
		reafilia.setIcEpo(icEpoReafiliar);
	} else if(reafiliarPor.equals("GRUPO")){
		reafilia.setIcGrupoEpo(icGrupoEpo);
		reafilia.setNumProveedor(numeroProveedor);
	}
	
	Registros listaRegistros = new Registros();
	listaRegistros = queryHelper.doSearch();
	
	infoRegresar =  "{\"success\": true, \"total\": \"" + listaRegistros.getNumeroRegistros()+ "\", \"registros\": " + listaRegistros.getJSONData()+"}";

} else if(informacion.equals("validaGridReafiliacion")){

	String datosGrid = (request.getParameter("datos") == null) ? "" : request.getParameter("datos");
	JSONArray arrayRegNuevo = JSONArray.fromObject(datosGrid);
	int camposVacios = 0;
	StringBuffer epoEnTurno = new StringBuffer();
	
	if(arrayRegNuevo.size() > 0){
		for(int i=0; i<arrayRegNuevo.size(); i++){
			JSONObject auxiliar = arrayRegNuevo.getJSONObject(i);
			if(auxiliar.getString("PROV_SIN_NUMERO").equals("false") && auxiliar.getString("NO_PROVEEDOR").equals("")){
				camposVacios ++ ;
			}
		}
	}
	infoRegresar =  "{\"success\": true, \"total\": \""+camposVacios+"\"}";

} else if(informacion.equals("consultaAceptaReafiliacion")){

	String datosGrid = (request.getParameter("datos") == null) ? "" : request.getParameter("datos");
	boolean success = false;
	JSONArray registros1 = new JSONArray();
	String numProveedor = "";
	String claveEpoEnTurno = "";
	String Sin_Num_Prov = ""; 
	String tipoPyme = "P";
	StringBuffer condicion = new StringBuffer();
	JSONArray arrayRegNuevo = JSONArray.fromObject(datosGrid);
	
	List listDatos = new ArrayList();
	List numProveedorErroneos = new ArrayList();
	List numEpoErroneos = new ArrayList();
	List numerosCliente = new ArrayList();
	List clavesEpo = new ArrayList();
	List sinNumProv = new ArrayList();

	ConsultaProveeodresReafiliacion consultaProv = new ConsultaProveeodresReafiliacion();
	
	Afiliacion beanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

	for(int i=0; i<arrayRegNuevo.size(); i++){
		JSONObject auxiliar = arrayRegNuevo.getJSONObject(i);
		claveEpoEnTurno = auxiliar.getString("IC_EPO"); 
		if(auxiliar.getString("PROV_SIN_NUMERO").equals("true")){
			Sin_Num_Prov ="S";
		} else if(auxiliar.getString("PROV_SIN_NUMERO").equals("false")) {
			Sin_Num_Prov ="N";
		}
		numProveedor = auxiliar.getString("NO_PROVEEDOR");
		// Si el numero de cliente ya existe, no se puede volver a usar
		if (beanAfiliacion.existeNumeroCliente(numProveedor, claveEpoEnTurno, tipoPyme)) {
			numProveedorErroneos.add(numProveedor);
			numEpoErroneos.add(claveEpoEnTurno);
		} else {
			numerosCliente.add(numProveedor);
			clavesEpo.add(claveEpoEnTurno);
			sinNumProv.add(Sin_Num_Prov);
		}
	}
	
	if (numProveedorErroneos.size() > 0) {
	
		for(int i = 0; i < numEpoErroneos.size(); i++){
			if(i > 0){
				condicion.append(",");
			}
			condicion.append(numEpoErroneos.get(i));
	   }
		
		consultaProv.setIcPyme(icPyme);
		consultaProv.setCondicionEpo(condicion.toString());
		//consultaProv.setNumProveedor(Integer.parseInt(icPyme));
		registros1 = consultaProv.consultaDatosReafilacion();
		success = true;
	
	} else {
		try{

			beanAfiliacion.reafiliarPymeEnEpos(iNoUsuario, icPyme, icEpo, clavesEpo, numerosCliente, sinNumProv);
			success = true;
			log.info("La pyme se reafilió correctamente");
		} catch(Exception e){
			success = false;
			log.warn("No se reafilió correctamente la pyme: " + e);
		}
	}

	infoRegresar = "{\"success\": "+new Boolean(success)+", \"total\":"+numProveedorErroneos.size()+", \"registros\":"+registros1.toString()+"}";

} else if(informacion.equals("catalogoPerfilModificar")){

	/** El siguiente bloque de codigo es para obtener los datos a mostrar en el combo Perfil a modificar */
	
	com.netro.seguridadbean.Seguridad objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
	
	String strTipoPerfil = "";
	int tipoPerfil = 0;
	HashMap listaPerfil = new HashMap();
	List listaPerfiles = null;
	
	strTipoPerfil = (request.getParameter("strTipoPerfil") == null) ?"": request.getParameter("strTipoPerfil");
	if( !strTipoPerfil.equals("") ){
		tipoPerfil = Integer.parseInt(strTipoPerfil);
	}
	listaPerfiles  = objSeguridad.consultarPerfiles(tipoPerfil);
	
	Iterator it = listaPerfiles.iterator();
	while( it.hasNext() ) {
		listaPerfil = new HashMap();
		List campos = (List)it.next();
		listaPerfil.put( "clave",       (String)campos.get(0) );
		listaPerfil.put( "descripcion", (String)campos.get(0) );
		registros.add(listaPerfil);
	}
	resultado.put("success", new Boolean(true));
	resultado.put("total", ""+registros.size());
	resultado.put("registros", registros.toString());
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("DatosCambioPerfil")){
	
	com.netro.seguridadbean.Seguridad objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
	
	Usuario oUsuario = null;
	UtilUsr oUtilUsr = new UtilUsr();
	String nombreEmpresa = "", internacional = "";
	HashMap datosPerfil = new HashMap();
	int noElectronico= 0;
	
	String afiliados = "";
	String claveAfiliado = "";
	String sTipoAfiliado = "";
	String sClaveAfiliado = "";
	String txtLogin = "";
	String strTipoPerfil = "";
	
	afiliados      = "S";
	txtLogin       = (request.getParameter("cuenta")==null)?"": request.getParameter("cuenta");
	oUsuario       = oUtilUsr.getUsuario(txtLogin);
	sTipoAfiliado  = oUsuario.getTipoAfiliado();
	sClaveAfiliado = oUsuario.getClaveAfiliado();
	
	if( !sTipoAfiliado.equals("") ){
	
		CambioPerfilProveedores cambioPerfilProveedores = new CambioPerfilProveedores();
		cambioPerfilProveedores.setSClaveAfiliado(sClaveAfiliado);
		cambioPerfilProveedores.setSTipoAfiliado(sTipoAfiliado);
		cambioPerfilProveedores.setAfiliados(afiliados);
		
		datosPerfil = cambioPerfilProveedores.obtieneDatosPerfil();
		
		strTipoPerfil = (String)datosPerfil.get("TIPOPERFIL");
		
		/** Se arma el Hash Map con los datos */
		datosPerfil.put("AFILIADOS",			afiliados							);
		datosPerfil.put("CLAVE_USUARIO",		txtLogin								);
		datosPerfil.put("S_TIPO_AFILIADO",	sTipoAfiliado						);
		datosPerfil.put("I_NO_ELECTRONICO",	oUsuario.getClaveAfiliado()	);
		datosPerfil.put("NOMBRE",				oUsuario.getNombre()				);
		datosPerfil.put("APELLIDO_PATERNO",	oUsuario.getApellidoPaterno()	);
		datosPerfil.put("APELLIDO_MATERNO",	oUsuario.getApellidoMaterno()	);
		datosPerfil.put("EMAIL",				oUsuario.getEmail()				);
		datosPerfil.put("PERFIL_ACTUAL",		oUsuario.getPerfil()				);
		
		resultado.put("success",       new Boolean(true));
		resultado.put("registro",      datosPerfil);
		resultado.put("strTipoPerfil", strTipoPerfil);
		resultado.put("accion",        busca_edita);
		
	} else {

		resultado.put("success",       new Boolean(false));
		resultado.put("mensaje",       "No se encontró el usuario con la clave especificada.<br>Por favor vuelva a intentarlo.");
		
	}
	
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("ModificaPerfil")){

	String afiliados = 				(request.getParameter("pfl_afiliados_id")			==null)?"":	request.getParameter("pfl_afiliados_id");
	String txtLogin = 				(request.getParameter("pfl_clave_usr_id")			==null)?"":	request.getParameter("pfl_clave_usr_id");
	String txtPerfil = 				(request.getParameter("pfl_modificar_id")			==null)?"":	request.getParameter("pfl_modificar_id");
	String txtPerfilAnt = 			(request.getParameter("pfl_actual_id")				==null)?"":	request.getParameter("pfl_actual_id");
	String txtNafinElectronico = 	(request.getParameter("pfl_nafin_elec_id")		==null)?"":	request.getParameter("pfl_nafin_elec_id");
	String txtTipoAfiliado = 		(request.getParameter("pfl_tipo_afiliado_id")	==null)?"":	request.getParameter("pfl_tipo_afiliado_id");
	String claveAfiliado = 			(request.getParameter("pfl_i_no_elec_id")			==null)?"":	request.getParameter("pfl_i_no_elec_id");
	String internacional = 			(request.getParameter("pfl_internacional_id")	==null)?"":	request.getParameter("pfl_internacional_id");
	
	UtilUsr oUtilUsr               = null;
	boolean exito                  = true;
	String mensaje                 = "";
	String[] nombres               = {"Perfil"};
	StringBuffer valoresAnteriores = new StringBuffer();
	StringBuffer valoresActuales   = new StringBuffer();
	List datosAnteriores           = new ArrayList();
	List datosActuales             = new ArrayList();
		
	if( txtPerfil.equals("") || txtLogin.equals("") ){
		exito = false;
		mensaje = "No se recibieron todos los parámetros.";
	} else {

		oUtilUsr = new UtilUsr();
		oUtilUsr.setPerfilUsuario(txtLogin, txtPerfil);
		
		datosAnteriores.add(0, txtLogin + " - " + txtPerfilAnt);
		datosActuales.add(0, txtLogin + " - " + txtPerfil);
		List cambios = Bitacora.getCambiosBitacora(datosAnteriores, datosActuales, nombres);
		valoresAnteriores.append(cambios.get(0));
		valoresActuales.append(cambios.get(1));
		
		CambioPerfilProveedores cambioPerfilProveedores = new CambioPerfilProveedores();
		cambioPerfilProveedores.setTxtTipoAfiliado(txtTipoAfiliado+internacional);	
		cambioPerfilProveedores.setTxtNafinElectronico(txtNafinElectronico);
		cambioPerfilProveedores.setTxtSesion((String)session.getAttribute("Clave_usuario"));
		cambioPerfilProveedores.setValoresAnteriores(valoresAnteriores.toString());
		cambioPerfilProveedores.setValoresActuales(valoresActuales.toString());
		exito = cambioPerfilProveedores.guardaCambiosEnBitacora();
		
	}
	
	resultado.put("success", new Boolean(exito));
	resultado.put("mensaje", mensaje);	
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("inicializacion")){

	infoRegresar =  "{\"success\": true, \"strTipoUsuario\": \"" + strTipoUsuario + "\"}";	



}else if (informacion.equals("validaPermiso")) {
	
	JSONObject jsonObj = new JSONObject();
	
	seguridadBean.validaPermiso(  iTipoPerfil,  strPerfil,  idMenuP,  ccPermiso,  strLogin );  
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	infoRegresar = jsonObj.toString();	
	
 
}
	
%>
<%=infoRegresar%>