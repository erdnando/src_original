<!DOCTYPE html>
<%@ page import="java.util.*, com.netro.exception.*" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>

<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="15reafiliaDistext.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<div id="areaContenido" style="width:700px">
		</div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>

	<form id='formAux' name="formAux" target='_new'></form>
	<form id='formParametros' name="formParametros">
		<input type="hidden" id="icPyme"        value="<%=(request.getParameter("icPyme")        ==null)?"":request.getParameter("icPyme")%>">
		<input type="hidden" id="razonSocial"   value="<%=(request.getParameter("razonSocial")   ==null)?"":request.getParameter("razonSocial")%>">
		<input type="hidden" id="rfc"           value="<%=(request.getParameter("rfc")           ==null)?"":request.getParameter("rfc")%>">	
		<input type="hidden" id="pyme_epo_int"  value="<%=(request.getParameter("pyme_epo_int")  ==null)?"":request.getParameter("pyme_epo_int")%>">
		<input type="hidden" id="cs_aceptacion" value="<%=(request.getParameter("cs_aceptacion") ==null)?"":request.getParameter("cs_aceptacion")%>">
	</form>
</body>
</html>