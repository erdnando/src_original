Ext.onReady(function(){

var IC_Pyme='';

//---------------Handlers----------------------------------



var procesarNafinElec = function (opts, success, response) {
	if(success == true&&Ext.util.JSON.decode(response.responseText).success == true){
		IC_Pyme=(Ext.util.JSON.decode(response.responseText).ic_pyme);
		Ext.getCmp('txt_nombre').setValue((Ext.util.JSON.decode(response.responseText).txt_nombre));
	}else{
		IC_Pyme='';
		Ext.getCmp('txt_nombre').setValue('');
	}
}

function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}


	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) 
 {
	 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	 var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
	 
	 btnGenerarArchivo.setIconClass('');
	 
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
		btnBajarArchivo.show();
		btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivo.setHandler(function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	 } else {
		 btnGenerarArchivo.enable();
		 NE.util.mostrarConnError(response,opts);
   }
  }




	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	
	var procesarConsulta = function(store,arrRegistros,opts){
			Ext.getCmp('btnBajarArchivo').hide();
			Ext.getCmp('btnBajarPDF').hide();
			if(arrRegistros!=null){
					var el = grid.getGridEl();
					
					if(store.getTotalCount()>0){
							el.unmask();
							Ext.getCmp('btnGenerarArchivo').enable();
							Ext.getCmp('btnGenerarPDF').enable();
					}else{
						el.mask('No se encontr� ning�n registro', 'x-mask');
						Ext.getCmp('btnGenerarArchivo').disable();
						Ext.getCmp('btnGenerarPDF').disable();
			}
	}
}

//---------------Store-------------------------------------


	var consulta = new Ext.data.JsonStore({
		root: 'registros',
		url: '15BiSolicAfiliaext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
					{name: 'EPO'},
					{name: 'RAZON'},
					{name: 'RFC'},
					{name: 'DOMICILIO'},
					{name: 'CONTACTO'},
					{name: 'TELEFONO'},
					{name: 'FECHA'}
		],
		//totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsulta,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							
					}
		}
	}
	});

	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg' ],
		url : '15BiSolicAfiliaext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});



var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15BiSolicAfiliaext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });



//---------------Componentes-------------------------------

var elementosForma = [
		{
				//EPO
				xtype: 'combo',
				fieldLabel: 'Nombre de la EPO',
				emptyText: 'Seleccionar EPO',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'icEpo',
				id: 'icEpo',
				mode: 'local',
				hiddenName: 'HicEpo',
				forceSelection: false
    },{
			xtype: 'compositefield',
			fieldLabel: 'Nombre de la PYME',
			id: 'pyme',
			combineErrors: false,
			forceSelection: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					name: 'txt_ne',
					id:	'txt_ne',
					maxLength:	25,
					msgTarget: 'side',
					flex:1,
					margins: '0 5 0 0',
					listeners: {
						blur: {
							fn: function(field) {
								Ext.Ajax.request({
											url: '15BiSolicAfiliaext.data.jsp',
											params: Ext.apply({txt_nafelec: field.getValue(),
												informacion: 'nafinElec'}),
											callback: procesarNafinElec
										});								
							}
						}
				}
				},{
					xtype: 'textfield',
					name: 'txt_nombre',
					id:	'txt_nombre',
					maxLength:	50,
					disabled:	true,
					msgTarget: 'side',
					flex:2,
					margins: '0 5 0 0'
				}
			]
		},{
			xtype: 'compositefield',
			combineErrors: false,
			forceSelection: false,
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					id:'disEspacio',
					text:''
				},{
					xtype:	'button',
					id:		'btnBuscaA',
					iconCls:	'icoBuscar',
					text:		'B�squeda Avanzada',
					handler: function(boton, evento) {
								var winVen = Ext.getCmp('winBuscaA');
									if (winVen){
										Ext.getCmp('fpWinBusca').getForm().reset();
										Ext.getCmp('fpWinBuscaB').getForm().reset();
										Ext.getCmp('cmb_num_ne').setValue();
										Ext.getCmp('cmb_num_ne').store.removeAll();
										Ext.getCmp('cmb_num_ne').reset();
										winVen.show();
										
									}else{
										var winBuscaA = new Ext.Window ({
											id:'winBuscaA',
											height: 285,
											x: 300,
											y: 100,
											width: 600,
											heigth: 100,
											modal: true,
											closeAction: 'hide',
											title: 'Nafinet',
											items:[
												{
													xtype:'form',
													id:'fpWinBusca',
													frame: true,
													border: false,
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													labelWidth: 140,
													items:[
														{
															xtype:'displayfield',
															frame:true,
															border: false,
															value:'Utilice el * para b�squeda gen�rica'
														},{
															xtype: 'textfield',
															name: 'nombre',
															id:	'txtNombre',
															fieldLabel:'Nombre',
															maxLength:	100
														},{
															xtype: 'textfield',
															name: 'rfc',
															id:	'txtRfc',
															fieldLabel:'RFC',
															maxLength:	20
														},{
															xtype: 'textfield',
															name: 'num_pyme',
															id:	'txtNe',
															fieldLabel:'N�mero de Proveedor',
															maxLength:	25
														}
													],
													buttons:[
														{
															text:'Buscar',
															iconCls:'icoBuscar',
															handler: function(boton) {
																			if ( Ext.isEmpty(Ext.getCmp('txtNombre').getValue()) && Ext.isEmpty(Ext.getCmp('txtRfc').getValue()) && Ext.isEmpty(Ext.getCmp('txtNe').getValue()) ){
																				Ext.Msg.alert(boton.text,'No existe informaci�n con los criterios determinados');
																				return;
																			}
															  catalogoNombreData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoLimpiar',
															handler: function() {
																			Ext.getCmp('winBuscaA').hide();
																		}
														}
													]
												},{
													xtype:'form',
													frame: true,
													id:'fpWinBuscaB',
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													monitorValid: true,
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													items:[
														{
															xtype: 'combo',
															id:	'cmb_num_ne',
															name: 'cmb_num_ne',
															hiddenName : 'Hcmb_num_ne',
															fieldLabel: 'Nombre',
															emptyText: 'Seleccione . . .',
															displayField: 'descripcion',
															valueField: 'clave',
															triggerAction : 'all',
															forceSelection:true,
															allowBlank: false,
															typeAhead: true,
															mode: 'local',
															minChars : 1,
															store: catalogoNombreData,
															tpl : NE.util.templateMensajeCargaCombo
														}
													],
													buttons:[
														{
															text:'Aceptar',
															iconCls:'aceptar',
															formBind:true,
															handler: function() {
																			if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																				var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																				var desc = disp.slice(disp.indexOf(" ")+1);
																				Ext.getCmp('txt_ne').setValue(Ext.getCmp('cmb_num_ne').getValue());
																				Ext.getCmp('txt_nombre').setValue(disp);
																				Ext.getCmp('winBuscaA').hide();
																				Ext.Ajax.request({
																				url: '15BiSolicAfiliaext.data.jsp',
																				params: Ext.apply({txt_nafelec: Ext.getCmp('txt_ne').getValue(),
																					informacion: 'nafinElec'}),
																				callback: procesarNafinElec
										});	
																				
																			}
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoLimpiar',
															handler: function() {	Ext.getCmp('winBuscaA').hide();	}
														}
													]
												}
											]
										}).show();
									}
								}
				}
			]
		},{
			 xtype: 'compositefield',
			 fieldLabel: 'Fecha de Solicitud',
			 combineErrors: false,
			 forceSelection: false,
			 msgTarget: 'side',
			 items: [
            {
			    // Fecha Inicio
			   xtype: 'datefield',
         name: 'dFechaRegIni',
         id: 'dFechaRegIni',
         vtype: 'rangofecha',
         campoFinFecha: 'dFechaRegFin',
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 margins: '0 20 0 0'
         },
			   {
				 xtype: 'displayfield',
				 value: 'a',
				 width: 50
			   },
		     {
			    // Fecha Final
			   xtype: 'datefield',
         name: 'dFechaRegFin',
         id: 'dFechaRegFin',
         vtype: 'rangofecha',
         campoInicioFecha: 'dFechaRegIni',
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 margins: '0 20 0 0'
         },
				 {
				 xtype: 'displayfield',
				 value: '(dd/mm/aaaa)',
				 width: 40
			  }
      ]
	 }
		
		];

		var grid = new Ext.grid.GridPanel({
				id: 'grid',
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				hidden: true,
				stripeRows: true,
				loadMask: true,
				title: 'Datos de la PyME',
				height: 400,
				width: 940,
				frame: true,
				columns:[
				//CAMPOS DEL GRID
						{
						header:'Nombre EPO',
						tooltip: 'Nombre EPO',
						sortable: true,
						dataIndex: 'EPO',
						width: 150,
						align: 'center',
						renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
								}
						},
						{
						align: 'center',
						header: 'Nombre o Razon Social',
						tooltip: 'Nombre o Razon Social',
						sortable: true,
						dataIndex: 'RAZON',
						width: 150
						},
						{
						align: 'center',
						header: 'RFC',
						tooltip: 'RFC',
						sortable: true,
						dataIndex: 'RFC',
						width: 150
						},
						{
						align: 'center',
						header: 'Domicilio',
						tooltip: 'Domicilio',
						sortable: true,
						dataIndex: 'DOMICILIO',
						width: 150,
						renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
								}
						},
						{
						header: 'Nombre del Contacto',
						tooltip: 'Nombre del Contacto',
						sortable: true,
						dataIndex: 'CONTACTO',
						width: 170
						},
						{
						align: 'center',
						header: 'Tel�fono del Contacto',
						tooltip: 'Tel�fono del Contacto ',
						sortable: true,
						dataIndex: 'TELEFONO',
						width: 150
						},
						{
						align: 'center',
						header: 'Fecha de Solicitud',
						tooltip: 'Fecha de Solicitud',
						sortable: true,
						dataIndex: 'FECHA',
						width: 150
						}
				
				],
				bbar: {
							xtype: 'toolbar',
							items: ['->','-',
								{
									xtype: 'button',
									text: 'Generar PDF',
									id: 'btnGenerarPDF',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '15BiSolicAfiliaext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoPDF',
												txt_ne: IC_Pyme}),
											callback: procesarSuccessFailureGenerarPDF
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarPDF',
									hidden: true
								},
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '15BiSolicAfiliaext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV',
												txt_ne: IC_Pyme}),
											callback: procesarSuccessFailureGenerarArchivo
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivo',
									hidden: true
								},
								'-']
				}
		});










var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		labelWidth: 130,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						
						//Mi acci�n
						if(verificaFechas('dFechaRegIni','dFechaRegFin')){
							grid.show();
							consulta.load({ params: Ext.apply(fp.getForm().getValues(),{txt_ne: IC_Pyme})
							});				
						}
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						location.reload();
				}
			 }
			 
		]
	});
//----------------Principal------------------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [ 
		  fp,
	    NE.util.getEspaciador(30),
			grid
			 
		 ]
  });

catalogoEpo.load();




});