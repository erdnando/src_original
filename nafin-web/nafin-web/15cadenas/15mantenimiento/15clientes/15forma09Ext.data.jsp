<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		com.netro.cadenas.* ,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		com.netro.seguridadbean.*, 
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	JSONObject resultado = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	JSONObject jsonObj = new JSONObject();
	String  ic_if	= (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");

	com.netro.seguridadbean.Seguridad seguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
		
	String idMenuP   = request.getParameter("idMenuP")==null?"15FORMA5":request.getParameter("idMenuP");
	String ccPermiso   = request.getParameter("ccPermiso")==null?"":request.getParameter("ccPermiso");

if (informacion.equals("valoresIniciales")) {  


	jsonObj = new JSONObject();
	//** Fodea 021-2015 (E);
		String  [] permisosSolicitados = { "BTNACEPTARIN","BTNCARGARIMAGENIN", "BTNCANCELARIN", "LINEA_FONDEO", "CL_MODIFICA"  };       
		
	List permisos =  seguridadBean.getPermisosPorMenu( iTipoPerfil, strPerfil, idMenuP, permisosSolicitados );     
	//** Fodea 021-2015 (S);   
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("permisos",  permisos);   
	infoRegresar = jsonObj.toString();	


}else   if(informacion.equals("CatalogoPais")) {		
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionNotIn("52", Integer.class);
		infoRegresar = cat.getJSONElementos();	
	}
 else if(informacion.equals("CatalogoEstado")){
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setClavePais(Pais);//Mexico clave 24
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();	
 }
 else if(informacion.equals("CatalogoMunicipio"))	{
		
		String Edo= (request.getParameter("estado")==null)?"":request.getParameter("estado");
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoMunicipio cat=new CatalogoMunicipio();
		cat.setClave("ic_municipio");
		cat.setDescripcion("cd_nombre");
		cat.setPais(Pais);
		cat.setEstado(Edo);
		cat.setOrden("cd_nombre");
		List elementos=cat.getListaElementos();
		
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
 }
 else if(informacion.equals("CatalogoDomicilio")) {
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_dom_correspondencia");
		cat.setCampoClave("ic_dom_correspondencia");
		cat.setCampoDescripcion("initcap(CD_NOMBRE)");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("CatalogoNumeroIF"))	{		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_financiera");
		cat.setCampoClave("ic_financiera");
		cat.setCampoDescripcion("'(' || ic_financiera ||')'|| initcap(cd_nombre)");
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();			
 }
 else if(informacion.equals("CatalogoAvales"))	{		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_aval");
		cat.setCampoClave("ic_aval");
		cat.setCampoDescripcion("initcap(CD_NOMBRE)");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();			
 } 
  else if(informacion.equals("CatalogoTipoRiesgo")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_tipo_riesgo");
		cat.setCampoClave("ic_tipo_riesgo");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("ic_tipo_riesgo");
		infoRegresar = cat.getJSONElementos();			
 }
 else if(informacion.equals("CatalogoViabilidad")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_Viabilidad");
		cat.setCampoClave("ic_viabilidad");
		cat.setCampoDescripcion("cd_nombre");
		infoRegresar = cat.getJSONElementos();			
 }
  else if(informacion.equals("CatalogoOficina_controladora")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_of_controladora");
		cat.setCampoClave("ic_of_controladora");
		cat.setCampoDescripcion("initcap(cd_nombre)");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();			
 } 
 else if(informacion.equals("CatalogoClaveIfSIAG")) {		
		CatalogoSimple cat = new CatalogoSimple(); 
		cat.setTabla("gti_if_siag");
		cat.setCampoClave("ic_if_siag");
		cat.setCampoDescripcion("ic_financiera||' - '||cg_descripcion");
		cat.setOrden("ic_if_siag");
		infoRegresar = cat.getJSONElementos();			
 }
  else if(informacion.equals("CatalogoClaveSUCRE")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_sucre");
		cat.setCampoClave("ic_sucre");
		cat.setCampoDescripcion("ic_sucre||' - '||cd_descripcion");
		cat.setOrden("ic_sucre");
		infoRegresar = cat.getJSONElementos();			
 }
 else if(informacion.equals("CatalogoDescontante")) {		
		try {
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			List listDescontantes = new ArrayList();
			listDescontantes = BeanAfiliacion.obtiene_datos_Descontante();
	
			if(!listDescontantes.isEmpty()){
				jsObjArray = JSONArray.fromObject(listDescontantes);
				infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		  }
		} catch(Exception e) {
			throw new AppException("AfiliacionBean_obtiene_datos_Descontante :: ERROR ::..", e);
		  }
 }

 /// ***** ***** ***** ***** ***** ***** ***** ***** ***** ///
 else if(informacion.equals("ConsultaIF"))	{
	try	
		{
			
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			Registros reg ;	
			reg = BeanAfiliacion.getDatosIF(ic_if);
			//reg = BeanAfiliacion.getDatosEPO("100");
			
			//HashMap resultado = new HashMap();
			ArrayList lista_res = new ArrayList();
			/*Barro  la informacion del Query */
			while(reg.next()) {
				resultado.put("Razon_social",						(reg.getString(1) == null) ? "":reg.getString(1).trim());
				resultado.put("Nombre_comercial",				(reg.getString(2) == null) ? "":reg.getString(2).trim());
				resultado.put("R_F_C",								(reg.getString(3) == null) ? "":reg.getString(3).trim());
				
				resultado.put("Calle",								(reg.getString(4) == null) ? "":reg.getString(4).trim());
				resultado.put("Colonia",							(reg.getString(5) == null) ? "":reg.getString(5).trim());
				resultado.put("Delegacion_o_municipio",		(reg.getString(6) == null) ? "":reg.getString(6).trim());
				resultado.put("Estado",								(reg.getString(8) == null) ? "":reg.getString(8).trim());
				resultado.put("Codigo_postal",					(reg.getString(9) == null) ? "":reg.getString(9).trim());
				resultado.put("Pais",								(reg.getString(10) == null) ? "":reg.getString(10).trim());
				resultado.put("Telefono",							(reg.getString(11) == null) ? "":reg.getString(11).trim());
				resultado.put("Fax",									(reg.getString(12) == null) ? "":reg.getString(12).trim());
				resultado.put("Email",								(reg.getString(13) == null) ? "":reg.getString(13).trim());
				
				resultado.put("Domicilio_correspondencia",	(reg.getString(14) == null) ? "":reg.getString(14).trim());
				resultado.put("Ejecutivo_de_cuenta",			(reg.getString(15) == null) ? "":reg.getString(15).trim());
				resultado.put("Numero_de_IF",						(reg.getString(16) == null) ? "":reg.getString(16).trim());
				resultado.put("Tipo_de_IF",						(reg.getString(17) == null) ? "":reg.getString(17).trim());
				resultado.put("Avales",								(reg.getString(18) == null) ? "":reg.getString(18).trim());
				resultado.put("Limite_maximo_endeudamiento",	(reg.getString(19) == null) ? "":reg.getString(19).trim());
				resultado.put("Facultades",						(reg.getString(20) == null) ? "":reg.getString(20).trim());
				resultado.put("Porcentaje_capital_contable",	(reg.getString(21) == null) ? "":reg.getString(21).trim());
				resultado.put("Ente_contable",					(reg.getString(22) == null) ? "":reg.getString(22).trim());
				resultado.put("Oficina_controladora",			(reg.getString(23) == null) ? "":reg.getString(23).trim());
				resultado.put("Viabilidad",						(reg.getString(24) == null) ? "":reg.getString(24).trim());
				resultado.put("ic_domicilio", 					(reg.getString(25) == null) ? "":reg.getString(25).trim());
				resultado.put("Fecha_convenio",					(reg.getString(26) == null) ? "":reg.getString(26).trim());
				resultado.put("Fecha_convenio_anticipo",		(reg.getString(27) == null) ? "":reg.getString(27).trim());
				resultado.put("Tipo_de_IF_piso",					(reg.getString(28) == null) ? "":reg.getString(28).trim());
				
				resultado.put("nombre_tabla",						(reg.getString(29) == null) ? "":reg.getString(29).trim());
				resultado.put("tipo_riesgo",						(reg.getString(30) == null) ? "":reg.getString(30).trim());
				resultado.put("if_siag",							(reg.getString(31) == null) ? "":reg.getString(31).trim());
				resultado.put("if_sucre",							(reg.getString(32) == null) ? "":reg.getString(32).trim());
				resultado.put("convenio_unico",					(reg.getString(33) == null) ? "":reg.getString(33).trim());
				resultado.put("mandato_documento",				(reg.getString(34) == null) ? "":reg.getString(34).trim());
				resultado.put("fideicomiso",				      (reg.getString(35) == null) ? "":reg.getString(35).trim());				
				resultado.put("autorizauto_opersf",				(reg.getString(35) == null) ? "":reg.getString(35).trim());
				resultado.put("num_clie_sirac",					(reg.getString(36) == null) ? "":reg.getString(36).trim());
				resultado.put("chkFactDistribuido",					(reg.getString(37) == null) ? "":reg.getString(37).trim());				
                                resultado.put("id_OperaMontosMenores",						(reg.getString("cs_opera_monto_min") == null) ? "":reg.getString("cs_opera_monto_min"));
			}
			String autorizauto_opersf 	= BeanAfiliacion.getAutorizacionAutomaticaOP(ic_if);
			String Total 					= BeanAfiliacion.getTotalSolicitudes(ic_if, "ic_if");
			resultado.put("autorizauto_opersf",autorizauto_opersf);
			resultado.put("Total",Total);
			reg = BeanAfiliacion.getDescontantesIF(ic_if);
			while(reg.next()) {
				resultado.put("descontanteIf1",			(reg.getString(1) == null) ? "":reg.getString(1).trim());
				resultado.put("descontanteIf2",			(reg.getString(2) == null) ? "":reg.getString(2).trim());
				resultado.put("tipoEpoInstruccion",		(reg.getString(3) == null) ? "":reg.getString(3).trim());
			}
			/*Recupero la informacion del hashmap para enviarla del lado del js */
			
			lista_res.add(resultado);
			jsObjArray = JSONArray.fromObject(lista_res);
			String consulta = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
			
			jsonObj = JSONObject.fromObject(consulta);	
			infoRegresar = jsonObj.toString();	
		
				
		} catch(Exception e) {
				throw new AppException("Error en la recuperación de datos del Intermediario Financiero", e);
			}
 }//fin ConsultaIf
 else if (informacion.equals("SolicitudesDesc")) {
	try {

		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		String   descontante = (request.getParameter("descontanteIf1") == null) ? "" : request.getParameter("descontanteIf1");
						
		String solicitudes = BeanAfiliacion.getSolicitudesConDescontantesIF(ic_if,descontante);
		resultado.put("solicitudes",solicitudes);
		resultado.put("success",new Boolean(true));
		infoRegresar = resultado.toString();
	} 
	catch(Exception e) { throw new AppException("Error en la consulta de Solicitudes Descontantes", e);	}
 }
 else if(informacion.equals("ConsultaProducto"))	{
	try	
		{
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			String if_siag =  (request.getParameter("if_siag")!=null)?request.getParameter("if_siag"):"";
			Registros reg ;		
			reg = BeanAfiliacion.getProductoIF(ic_if);
			
			HashMap hmProducto = new HashMap();
			ArrayList lista_res = new ArrayList();
			String fechaConv = "",dispersion = "",nombre_producto = "", ic_producto = "";
			/*Barro  la informacion del Query */
			while(reg.next()) {
				hmProducto = new HashMap();
				
				fechaConv 	= reg.getString(2)==null?"":reg.getString(2);
				dispersion = reg.getString(4)==null?"":reg.getString(4);
				String auxDisp = "";
				if(!"".equals(dispersion)){
					auxDisp = "S".equals(dispersion)?"Si":"No";
				}
				
				nombre_producto	= reg.getString(1) == null? "":reg.getString(1).trim();   
				ic_producto		= reg.getString(3) == null? "":reg.getString(3);   
				if (!ic_producto.equals("1")) 
					auxDisp = "NA";				
				
				hmProducto.put("ic_producto",ic_producto);
				hmProducto.put("nombre_producto",nombre_producto);
				hmProducto.put("fechaConv",fechaConv);
				hmProducto.put("auxDisp",auxDisp);
				lista_res.add(hmProducto);
			}
			if(if_siag!=null&&!if_siag.equals("")){
				String fecha = BeanAfiliacion.getFechaSIAG(if_siag);
				hmProducto = new HashMap();
				
				hmProducto.put("ic_producto","SIAG");
				hmProducto.put("nombre_producto","Programa de Garantía Automática");
				hmProducto.put("fechaConv",fecha);
				hmProducto.put("auxDisp","NA");
				lista_res.add(hmProducto);
			}
			/*Recupero la informacion en un hashmap para enviarla del lado del js */
			
			jsObjArray = JSONArray.fromObject(lista_res);
			String  consulta = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";				
			
			jsonObj = JSONObject.fromObject(consulta);		
			infoRegresar = jsonObj.toString();	
			
		} catch(Exception e) {
				throw new AppException("Error en la recuperacion de datos", e);
			}
 }
 else if(informacion.equals("ExisteImagen")) {
	try
	{
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		int reg = BeanAfiliacion.getExisteArchivo(ic_if);
		String existe = String.valueOf(reg);
		resultado = new JSONObject();
		resultado.put("success", new Boolean(true));
		resultado.put("existe", existe);
		infoRegresar = resultado.toString();			
	} catch(Exception e) { throw new AppException("Error en la consulta getExisteArchivo Intermediario Financiero", e); }
 }

 else if(informacion.equals("ModificaIF")) {
	try
	{
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		DisenoIF dis = new DisenoIF();
		
		String 	ic_domicilio						= (request.getParameter("id_domicilio") == null) ? "" : request.getParameter("id_domicilio").replaceAll(",", "");
		String 	Razon_Social  						= (request.getParameter("Razon_Social") == null) ? "" : request.getParameter("Razon_Social").replaceAll(",", ""),
					Nombre_comercial 					= (request.getParameter("nombre_comercial") == null) ? "" : request.getParameter("nombre_comercial").replaceAll(",", ""),
					R_F_C         						= (request.getParameter("rfc")    == null) ? "" : request.getParameter("rfc").replaceAll(",", "");
					
					//Domicilio							
		String   Calle        						= (request.getParameter("calle")    == null) ? "" : request.getParameter("calle").replaceAll(",", ""),
					Colonia       						= (request.getParameter("colonia")  == null) ? "" : request.getParameter("colonia").replaceAll(",", ""),
					Estado        						= (request.getParameter("cmb_estado")   == null) ? "" : request.getParameter("cmb_estado").replaceAll(",", ""),
					Delegacion_o_municipio 			= (request.getParameter("cmb_delegacion") == null) ? "" : request.getParameter("cmb_delegacion").replaceAll(",", ""),
					Codigo_postal 						= (request.getParameter("code") == null) ? "" : request.getParameter("code").replaceAll(",", ""),
					Pais          						= (request.getParameter("cmb_pais")     == null) ? "" : request.getParameter("cmb_pais").replaceAll(",", ""),
					Telefono      						= (request.getParameter("telefono") == null) ? "" : request.getParameter("telefono").replaceAll(",", ""),
					Email         						= (request.getParameter("email")    == null) ? "" : request.getParameter("email").replaceAll(",", ""),
					Fax           						= (request.getParameter("fax")      == null) ? "" : request.getParameter("fax").replaceAll(",", "");
		
		String   Domicilio_correspondencia		=(request.getParameter("Domicilio_correspondencia") == null) ? "" : request.getParameter("Domicilio_correspondencia").replaceAll(",", ""), 					
					Ejecutivo_de_cuenta 				= (request.getParameter("Ejecutivo_de_cuenta") == null) ? "" : request.getParameter("Ejecutivo_de_cuenta").replaceAll(",", ""),
	            Numero_Clie_Sirac 				= (request.getParameter("numCliSirac")  == null) ? "" : request.getParameter("numCliSirac").replaceAll(",", ""),
					Numero_de_IF  						= (request.getParameter("Numero_de_IF")  == null) ? "" : request.getParameter("Numero_de_IF").replaceAll(",", ""),
					Tipo_de_IF 	  						= (request.getParameter("Tipo_de_IF")    == null) ? "" : request.getParameter("Tipo_de_IF").replaceAll(",", ""),
					Tipo_de_IF_piso 					= (request.getParameter("Tipo_de_IF_piso") == null) ? "" : request.getParameter("Tipo_de_IF_piso").replaceAll(",", ""),
					Avales 		  						= (request.getParameter("Avales")        == null) ? "" : request.getParameter("Avales").replaceAll(",", ""),
					tipo_riesgo							= (request.getParameter("tipo_riesgo")==null)?"":request.getParameter("tipo_riesgo").replaceAll(",", ""),
					Facultades    						= (request.getParameter("Facultades")    == null) ? "" : request.getParameter("Facultades").replaceAll(",", ""),
					Limite_maximo_endeudamiento	= (request.getParameter("Limite_maximo_endeudamiento") == null) ? "" : request.getParameter("Limite_maximo_endeudamiento").replaceAll(",", ""),
					Ente_contable 						= (request.getParameter("Ente_contable") == null) ? "" : request.getParameter("Ente_contable").replaceAll(",", ""),
					Porcentaje_capital_contable 	= (request.getParameter("Porcentaje_capital_contable") == null) ? "" : request.getParameter("Porcentaje_capital_contable").replaceAll(",", ""),					
					Viabilidad    						= (request.getParameter("Viabilidad")    == null) ? "" : request.getParameter("Viabilidad").replaceAll(",", ""),
					Oficina_controladora 			= (request.getParameter("Oficina_controladora") == null) ? "" : request.getParameter("Oficina_controladora").replaceAll(",", "");
		
		String   if_siag								= (request.getParameter("if_siag")==null)?"":request.getParameter("if_siag").replaceAll(",", ""),
				   nombre_tabla 						= (request.getParameter("nombre_tabla") == null) ? "" : request.getParameter("nombre_tabla").replaceAll(",", ""),	
				   if_sucre          				= (request.getParameter("if_sucre")== null) ? "" : request.getParameter("if_sucre").replaceAll(",", "");
		String   descontanteIf1 					= request.getParameter("descontanteIf1")== null?"":request.getParameter("descontanteIf1").replaceAll(",", "");//FODEA 012 - 2010 ACF
      String   descontanteIf2 					= request.getParameter("descontanteIf2")== null?"":request.getParameter("descontanteIf2").replaceAll(",", "");//FODEA 012 - 2010 ACF
		String   tipoEpoInstruccion 				= request.getParameter("tipoEpoInstruccion")== null?"":request.getParameter("tipoEpoInstruccion").replaceAll(",", "");//FODEA 033 - 2010 ACF
		/* ---- Check's box ---- */			
		String  autorizauto_opersf					= (request.getParameter("autorizauto_opersf")      == null) ? "" : request.getParameter("autorizauto_opersf").replaceAll(",", ""),
				  //msChkFinan							= (request.getParameter("chkFinanciamiento")       == null) ? "" : request.getParameter("chkFinanciamiento"),
				  enlace_automatico					= (request.getParameter("enlace_automatico")       == null) ? "" : request.getParameter("enlace_automatico").replaceAll(",", ""),	
				  convenio_unico						= (request.getParameter("convenio_unico")      		== null) ? "" : request.getParameter("convenio_unico").replaceAll(",", ""),
				  mandato_documento					= (request.getParameter("mandato_documento")       == null) ? "" : request.getParameter("mandato_documento").replaceAll(",", "");

		
	String Fecha_convenio 							= (request.getParameter("Fecha_convenio")       == null) ? "" : request.getParameter("Fecha_convenio").replaceAll(",", "");
	String Fecha_convenio_anticipo 				= (request.getParameter("Fecha_convenio_anticipo")== null)? "" : request.getParameter("Fecha_convenio_anticipo").replaceAll(",", "");///???
  	String  fideicomiso		= (request.getParameter("fideicomiso")       == null) ? "" : request.getParameter("fideicomiso").replaceAll(",", "");
	String chkFactDistribuido = (request.getParameter("chkFactDistribuido")   != null && request.getParameter("chkFactDistribuido").equals("on"))  ?"S":"N";
        String idOperaMontosMenores = (request.getParameter("id_OperaMontosMenores")    == null) ? "N" : request.getParameter("id_OperaMontosMenores");
	
	if(!fideicomiso.equals("")) 	{
		if ( fideicomiso.equals("on") ){
			fideicomiso = "S";
		}else{
			fideicomiso = "N";
		}
	}else  {
		fideicomiso = "N";
	}
	
		
		if(!autorizauto_opersf.equals("")) {
			if(autorizauto_opersf.equals("on")) 	autorizauto_opersf = "S";
		}
		else
			autorizauto_opersf = "N";
		
		if(!enlace_automatico.equals("")) { 
			if ( enlace_automatico.equals("on"))	enlace_automatico = "S";
		}
		else
			enlace_automatico = "N";
			
		if(!convenio_unico.equals("")) { 
			if ( convenio_unico.equals("on"))		convenio_unico = "S";
		}
		//else			convenio_unico = "N";
			
		if(!mandato_documento.equals("")) {
			if ( mandato_documento.equals("on"))	mandato_documento = "S";
		}
		else
			mandato_documento = "N";
		
		if( !Delegacion_o_municipio.equals("") && Delegacion_o_municipio != null)
		{
			CatalogoMunicipio cat=new CatalogoMunicipio();
			cat.setClave("ic_municipio");
			cat.setDescripcion("cd_nombre");
			cat.setPais(Pais);
			cat.setEstado(Estado);
			cat.setSeleccion(Delegacion_o_municipio);
			cat.setOrden("cd_nombre");
			List elementos=cat.getListaElementos();
		
			Iterator it = elementos.iterator();
			while(it.hasNext()) {
				ElementoCatalogo obj = (ElementoCatalogo)it.next();
				String iDelMun = obj.getClave();	String strDelMun = obj.getDescripcion();
				Delegacion_o_municipio = iDelMun + "|" + strDelMun;
			}
			System.out.println("Delegacion_o_municipio " +  Delegacion_o_municipio ) ;
		}
		
		String  strArchivo	= (request.getParameter("strArchivo")==null)?"":request.getParameter("strArchivo"),
				  path_destino = (request.getParameter("path_destino")==null)?"":request.getParameter("path_destino");
		
		String msg = "Los datos han sido actualizados";
		String archivo1,archivo2="";
		
		if("".equals(tipo_riesgo))
			tipo_riesgo = "null";

		R_F_C = R_F_C.toUpperCase();
		
		BeanAfiliacion.actualizaIf(iNoUsuario, ic_if, Razon_Social.toUpperCase(), 
				Nombre_comercial.toUpperCase(), R_F_C, Ejecutivo_de_cuenta, 
				Domicilio_correspondencia, Numero_de_IF, 
				Tipo_de_IF, Avales, Limite_maximo_endeudamiento, Facultades, 
				Porcentaje_capital_contable, Ente_contable, 
				Oficina_controladora, Viabilidad, Fecha_convenio,Fecha_convenio_anticipo, 
				ic_domicilio, Calle.toUpperCase(), "", "", 
				Colonia.toUpperCase(), Estado, Pais, Delegacion_o_municipio, 
				Codigo_postal, Telefono, Email.toLowerCase(), 
				Fax, Tipo_de_IF_piso, enlace_automatico, nombre_tabla, 
        autorizauto_opersf,tipo_riesgo,if_siag,if_sucre,convenio_unico,mandato_documento,
        descontanteIf1, descontanteIf2, tipoEpoInstruccion, fideicomiso,Numero_Clie_Sirac,chkFactDistribuido, idOperaMontosMenores );//FODEA 033 - 2010
			
			//Guarda el archivo de Imagen
			if(!strArchivo.equals("") && !path_destino.equals("")){
				archivo1=strDirectorioTemp+path_destino+strArchivo;
				String nombreArchivo=ic_if+".gif";
				System.out.println(":::: strDirectorioPublicacion :::: "+strDirectorioPublicacion);
				archivo2=strDirectorioPublicacion+"/00archivos/if/logos/"+nombreArchivo;
				dis.Clonar(archivo1,archivo2);
				dis.guardar(ic_if,archivo2);
				System.out.println("::::: Se copio y guardo correctamente::::");
			}
			
			resultado = new JSONObject();
			resultado.put("success", new Boolean(true));
			resultado.put("msg", msg);
			infoRegresar = resultado.toString();	
			
	} catch(Exception e) { throw new AppException("Error en la Actualización del Intermediario Financiero", e); }
 }

 //---	
 else if(informacion.equals("actualizaFechaConvenio")) {
	try
	{
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
				
		String ic_producto_nafin = (request.getParameter("ic_producto")==null)?"":request.getParameter("ic_producto"),
			nombre_producto = (request.getParameter("nombre_producto")==null)?"":request.getParameter("nombre_producto"),
			fecha_convenio = (request.getParameter("var_fchC")==null)?"":request.getParameter("var_fchC"),
			chk_dispersion = (request.getParameter("chk_dispersion")==null)?"":request.getParameter("chk_dispersion");
			
		BeanAfiliacion.actualizaFechaConvenio(fecha_convenio, ic_producto_nafin, ic_if,chk_dispersion);	
		String msg = "Se actualizaron los valores con éxito";
		
		resultado = new JSONObject();
		resultado.put("success", new Boolean(true));
		resultado.put("msg", msg);
		infoRegresar = resultado.toString();	
			
	} catch(Exception e) { throw new AppException("Error en la Actualización del Intermediario Financiero", e); }
 }
 
 else if(informacion.equals("CatalogoLineaFondeo")) {
	try
	{
		String Numero_de_IF = (request.getParameter("Numero_de_IF")==null)?"":request.getParameter("Numero_de_IF");
		if(!Numero_de_IF.equals("") && Numero_de_IF != null ) {
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			ArrayList lista_res = new ArrayList();
			Registros reg ;	
			reg = BeanAfiliacion.numeroLineaFondeo(Numero_de_IF);	
			while(reg.next()) {
				resultado = new JSONObject();
				resultado.put("clave",			(reg.getString(1) == null) ? "":reg.getString(1).trim());
				resultado.put("descripcion",	(reg.getString(1) == null) ? "":reg.getString(1).trim());
				lista_res.add(resultado);
			}			
			//lista_res.add(resultado);
			jsObjArray = JSONArray.fromObject(lista_res);
			infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		}
		else 
			infoRegresar = "{\"success\": true, \"total\": \"" + 0 + "\", \"registros\": " + "{}"+"}";
	} catch(Exception e) { throw new AppException("Error en la Actualización del Intermediario Financiero", e); }

 }
 
 else if(informacion.equals("ObtieneDatosLineaFondeo")) {
	try
	{
		
		String ic_producto_nafin = (request.getParameter("ic_producto_nafin")==null)?"":request.getParameter("ic_producto_nafin");
		if(!ic_producto_nafin.equals("") && ic_producto_nafin != null && !ic_if.equals("") && ic_if != null) {
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			Hashtable ohDatosLinea = new Hashtable();
			String linea_fondeoG = "";
			ArrayList lista_res = new ArrayList();
			HashMap datosLineaFondeo = null;
			String chk_disponibilidad = "", fch_actualizacion="",usuario_mod="",monto_inicial="",monto_disponible="" ;
				
			try {
				ohDatosLinea = BeanAfiliacion.getDatosLineaFondeo(ic_producto_nafin,  ic_if);
				if(ohDatosLinea.size() > 0) {
					linea_fondeoG   = (String)ohDatosLinea.get("IG_NUMERO_LINEA_FONDEO");
					
					datosLineaFondeo = new HashMap();
					datosLineaFondeo.put("linea_fondeoG",linea_fondeoG);
					chk_disponibilidad 	= ohDatosLinea.get("CS_VALIDAR_DISP").toString();
					fch_actualizacion 	= (String)ohDatosLinea.get("DF_ACT_DISP");
					usuario_mod 			= (String)ohDatosLinea.get("CG_NOMBRE_USUARIO");
					monto_inicial 			= "$" + Comunes.formatoDecimal((String)ohDatosLinea.get("FN_MONTO_INIC_LINEA_FONDEO"),2);
					monto_disponible 		= "$" + Comunes.formatoDecimal((String)ohDatosLinea.get("FN_MONTO_DISP_LINEA_FONDEO"),2);
					datosLineaFondeo.put("chk_disponibilidad",chk_disponibilidad);
					datosLineaFondeo.put("fch_actualizacion",fch_actualizacion);
					datosLineaFondeo.put("usuario_mod",usuario_mod);
					datosLineaFondeo.put("monto_inicial",monto_inicial);
					datosLineaFondeo.put("monto_disponible",monto_disponible);
					
					lista_res.add(datosLineaFondeo);
				}
				else
					infoRegresar = "{\"success\": true, \"total\": \"" + 0 + "\", \"registros\": " + "{}"+"}";
			} catch(NafinException ne) { out.println(ne.getMessage()); }
			
			jsObjArray = JSONArray.fromObject(lista_res);
			infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		
		} //if
		
	} catch(Exception e) { throw new AppException("Error en la Actualización del Intermediario Financiero", e); }

 }
 // ----------------------------------------------------------------------------------------------------
  else if(informacion.equals("actualizaDisponible")) {
	try
	{
		
		String 	ic_producto_nafin 	= (request.getParameter("ic_producto_nafin")==null)?"":request.getParameter("ic_producto_nafin"),
					nombre_producto 		= (request.getParameter("nombre_producto")==null)?"":request.getParameter("nombre_producto"),
					//actualiza 				= (request.getParameter("actualiza")==null)?"":request.getParameter("actualiza"),
					linea_fondeo 			= (request.getParameter("linea_fondeo")==null)?"":request.getParameter("linea_fondeo"),
					chk_disponibilidad 	= (request.getParameter("chk_disponibilidad")==null)?"N":request.getParameter("chk_disponibilidad"),
					act_disponible 		= (request.getParameter("act_disponible")==null)?"N":request.getParameter("act_disponible");
		String nombreUsuario 			= iNoCliente +" "+ strNombreUsuario;			 
		
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		String msg = "Ocurrio un error";
		String sDisponibilidad = "";
		if(chk_disponibilidad.equals("true"))
			sDisponibilidad = "S";
		else
			sDisponibilidad = "N";
			
		BeanAfiliacion.actualizaLineaFondeo(ic_producto_nafin,  ic_if, linea_fondeo, sDisponibilidad, act_disponible, nombreUsuario);				
		msg = "Se actualizaron los valores con éxito";
	
		resultado = new JSONObject();
		resultado.put("success", new Boolean(true));
		resultado.put("msg", msg);
		infoRegresar = resultado.toString();		
		
	} catch(Exception e) { throw new AppException("Error en la Actualización del Intermediario Financiero", e); }

 }else if(informacion.equals("rfc_Sirac"))	{
 
	String Numero_Clie_Sirac = (request.getParameter("numCliSirac")  == null) ? "" : request.getParameter("numCliSirac").replaceAll(",", "");
	try	
	{
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
		
		String rfcSirac = BeanAfiliacion.getRfcSirac(Numero_Clie_Sirac);
		resultado.put("success",new Boolean(true));  //El RFC es correcto
		resultado.put("rfc", rfcSirac);  //El RFC es correcto
		infoRegresar = resultado.toString();
	} catch(Exception e) { //catch(NafinException lexError){
		throw new AppException("Error en la afiliacion", e);
	  }


}else if (informacion.equals("validaPermiso")) {
	
	jsonObj = new JSONObject();
	
	seguridadBean.validaPermiso(  iTipoPerfil,  strPerfil,  idMenuP,  ccPermiso,  strLogin );  
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 
	jsonObj.put("ccPermiso",  ccPermiso); 	
	infoRegresar = jsonObj.toString();	
	
		
 }
//System.out.println("infoRegresar============"+infoRegresar); 


%>
<%=infoRegresar%>



