<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.exception.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.descuento.*,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoEPO,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
		
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if(informacion.equals("catalogoEPO") ) {

	CatalogoEPO catEpo = new CatalogoEPO();
	catEpo.setCampoClave("IC_EPO");
	catEpo.setCampoDescripcion("CG_RAZON_SOCIAL");
	
	infoRegresar = catEpo.getJSONElementos();
	
}else if(informacion.equals("Consultar") ||  informacion.equals("ArchivoCSV")) {

	int start = 0;
	int limit = 0;

	String NomPyme		=	(request.getParameter("_txt_nom_pyme") == null)?"":request.getParameter("_txt_nom_pyme");
	String rfc		=	(request.getParameter("_txt_nom_rfc")==null)?"":request.getParameter("_txt_nom_rfc");
	String epo		=	(request.getParameter("cmb_epo")==null)?"":request.getParameter("cmb_epo");
	String txt_fecha_de			=	(request.getParameter("_fecha_carga_ini") == null) ? "":request.getParameter("_fecha_carga_ini");
	String txt_fecha_a			=	(request.getParameter("_fecha_carga_fin") == null) ? "": request.getParameter("_fecha_carga_fin");
	String publicacion			= (request.getParameter("publicacion") == null) ? "": request.getParameter("publicacion");
	String operacion			= (request.getParameter("operacion") == null) ? "": request.getParameter("operacion");


	ConsGenAutCvesRFCCA2 paginador = new ConsGenAutCvesRFCCA2();
	paginador.setNOMPYME(NomPyme.toUpperCase());
	paginador.setRFC(rfc);
	paginador.setEPO(epo);
	paginador.setFECHADE(txt_fecha_de);
	paginador.setFECHAA(txt_fecha_a);
	paginador.setPUBLICACION(publicacion);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if(informacion.equals("Consultar")){
		
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));

		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}

			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		
	}else if(informacion.equals("ArchivoCSV")){
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
	
}
	
%>
<%=infoRegresar%>
