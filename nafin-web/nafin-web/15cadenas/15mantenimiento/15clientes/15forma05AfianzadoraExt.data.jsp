<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject, 
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,	
		com.netro.cadenas.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String nombre = (request.getParameter("nombre") != null)?request.getParameter("nombre"):"";
String estado = (request.getParameter("Estado") != null)?request.getParameter("Estado"):"";
String nafinEle = (request.getParameter("numElectronico") != null)?request.getParameter("numElectronico"):"";
Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
String infoRegresar ="";
int start=0,limit=0;
JSONObject jsonObj 	      = new JSONObject();


 if(informacion.equals("catalogoEstado")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setTabla("comcat_estado");
		
		infoRegresar = cat.getJSONElementos();
 }else if (informacion.equals("Consulta")||informacion.equals("ArchivoCSV")||informacion.equals("ArchivoPDF")){
	ConsAfianzadora paginador =  new ConsAfianzadora();
	paginador.setNoNE(nafinEle);
	paginador.setNombre(nombre);
	paginador.setEstado(estado);
	
	
	CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
		
		if (informacion.equals("Consulta")){
		Registros reg = cqhelper.doSearch();
	  infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";

		}	else if(informacion.equals("ArchivoPDF")){
			try{
				String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
				jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF");
			}
		}else if(informacion.equals("ArchivoCSV")){
			String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
}else if(informacion.equals("eliminaRegistro")){
			String NOAFIANZA = (request.getParameter("NOAFIANZA") != null)?request.getParameter("NOAFIANZA"):"";
			String eliminacion = afiliacion.setDeleteAfianzadora(NOAFIANZA);
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("eliminacion",eliminacion);
			infoRegresar = jsonObj.toString();
}else if(informacion.equals("obtenerRegistro")){
	String NOAFIANZA = (request.getParameter("NOAFIANZA") != null)?request.getParameter("NOAFIANZA"):"";
	List datos  = afiliacion.getConsAfianzadora(NOAFIANZA);

		String razonSocial = (String)datos.get(0);
		String nombreComercial  = (String)datos.get(1);
		String rfc  =(String)datos.get(2);
		String numero  = (String)datos.get(3);
		String calle  = (String)datos.get(4);
		String colonia  = (String)datos.get(5);
		String codigo  = (String)datos.get(6);
		String pais  = (String)datos.get(7);
		String Estado1 = (String)datos.get(8);
		String Estado = (String)datos.get(8);
		String Delegacion_o_municipio = (String)datos.get(9);
		String telefono = (String)datos.get(10);
		String email = (String)datos.get(11);
		String fax = (String)datos.get(12);
		String url = (String)datos.get(13);
		HashMap infoAfianzadora= new HashMap();
		
		infoAfianzadora.put("Razon_Social",razonSocial);
		infoAfianzadora.put("Nombre_Comercial",nombreComercial);
		infoAfianzadora.put("R_F_C",rfc);
		infoAfianzadora.put("CNSF",numero);
		infoAfianzadora.put("Calle",calle);
		infoAfianzadora.put("Colonia",colonia);
		infoAfianzadora.put("code",codigo);
		infoAfianzadora.put("Pais",pais);
		infoAfianzadora.put("Estado",Estado1);
		infoAfianzadora.put("Delegacion_o_municipio",Delegacion_o_municipio);
		infoAfianzadora.put("telefono",telefono);
		infoAfianzadora.put("Email_Rep_Legal",email);
		infoAfianzadora.put("fax_domicilio",fax);
		infoAfianzadora.put("Estado",Estado);
		infoAfianzadora.put("URL",url);
		
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
			jsonObj.put("registro",infoAfianzadora);
		infoRegresar = jsonObj.toString();

		/*if("S".equals(valCnsf)){
			if (BeanAfiliacion.existeNumeroCnsf(numero)) {
				cnsfActual = numero;
				if(!((String)datos.get(3)).equals(numero)){
					numero  = (String)datos.get(3);
					existCnsf = true;
				}
			}
		}*/
	


}else if(informacion.equals("AfiliacionUpdate")){

			String razonSocial = (request.getParameter("Razon_Social") != null)?request.getParameter("Razon_Social"):"";
			String nombreComercial = (request.getParameter("Nombre_Comercial") != null)?request.getParameter("Nombre_Comercial"):"";
			String rfc = (request.getParameter("R_F_C") != null)?request.getParameter("R_F_C"):"";
			String  numeroCnsf= (request.getParameter("CNSF") != null)?request.getParameter("CNSF"):"";
			String calleYNumero        	=(request.getParameter("Calle")    != null) ?   request.getParameter("Calle") :"";
			String colonia = (request.getParameter("Colonia") != null)?request.getParameter("Colonia"):"";
			String codigoPostal = (request.getParameter("code") != null)?request.getParameter("code"):"";
			String claveMunicipio = (request.getParameter("Delegacion_o_municipio") != null)?request.getParameter("Delegacion_o_municipio"):"";
			String  numeroTelefono= (request.getParameter("telefono") != null)?request.getParameter("telefono"):"";
			String correoElectronico        	=(request.getParameter("Email_Rep_Legal")    != null) ?   request.getParameter("Email_Rep_Legal") :"";
			String numeroFax = (request.getParameter("Fax") != null)?request.getParameter("Fax"):"";
			String urlValidacion = (request.getParameter("URL") != null)?request.getParameter("URL"):"";
			String clavePais = (request.getParameter("Pais") != null)?request.getParameter("Pais"):"";
			String claveEstado = (request.getParameter("Estado") != null)?request.getParameter("Estado"):"";
			String NOAFIANZA = (request.getParameter("NOAFIANZA") != null)?request.getParameter("NOAFIANZA"):"";
			List datos = new ArrayList ();
			datos.add(razonSocial);
			datos.add(nombreComercial);
			datos.add(rfc);
			datos.add(numeroCnsf);
			datos.add(calleYNumero);
			datos.add(colonia);
			datos.add(codigoPostal);
			datos.add(clavePais);
			datos.add(claveEstado);
			datos.add(claveMunicipio);
			datos.add(correoElectronico);
			datos.add(numeroTelefono);
			datos.add(numeroFax);
			datos.add(urlValidacion);
			datos.add(NOAFIANZA);
			String resultado = afiliacion.setUpdateAfianzadora(datos);
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("resultado",resultado);
			infoRegresar = jsonObj.toString();
}else if(informacion.equals("catalogoPais")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionNotIn("52", Integer.class);
		infoRegresar = cat.getJSONElementos();	
	} else if(informacion.equals("catalogoEstado2"))	{
		String pais = (request.getParameter("pais") != null)?request.getParameter("pais"):"";
		CatalogoEstado cat = new CatalogoEstado();
		//cat.setTabla("comcat_version_convenio");
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre");
		cat.setClavePais(pais);
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoMunicipio"))	{
		String pais = (request.getParameter("pais") != null)?request.getParameter("pais"):"";
		String estados = (request.getParameter("estado") != null)?request.getParameter("estado"):"";
		CatalogoMunicipio cat = new CatalogoMunicipio();
		cat.setPais(pais);
		cat.setClave("IC_MUNICIPIO");
		cat.setDescripcion("upper(CD_NOMBRE)");
		cat.setEstado(estados);
		List elementos = cat.getListaElementos();
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar =  "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

		 
 }else if(informacion.equals("obtenerAfiliado")){

			com.netro.seguridadbean.Seguridad seguridad  = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			
			String claveAfiliado = (request.getParameter("claveAfiliado") != null)?request.getParameter("claveAfiliado"):"";
			UtilUsr utilUsr = new UtilUsr();
			List cuentas = utilUsr.getUsuariosxAfiliado(claveAfiliado, "A"); //Cuentas Asociadas
			ConsUsuariosNafinSeg usuarios = new ConsUsuariosNafinSeg();
			List datos = usuarios.getDatosAfiliado("A", claveAfiliado); //Datos  del Afiliado
			List registros = new ArrayList();
			HashMap reg= new HashMap();
			String datoAfiliado="" ;
			if (datos.size()>0){
				datoAfiliado = "<center>N@E: "+(String) datos.get(1)+" - "+(String)datos.get(0)+"</center>";
			}
			boolean modificar = false; 
			if ("NAFIN".equals(strTipoUsuario) && 	!strPerfil.equals("NAFIN OPERADOR")) {
					try{
					seguridad.validaFacultad( (String) strLogin, (String) strPerfil, "PBMXAFICON", "15FORMA5", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
					}catch(SeguException segur){
						try{
						seguridad.validaFacultad( (String) strLogin, (String) strPerfil, "PBMXTAFILI", "15FORMA5", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
						}catch(SeguException segu){
							modificar=true;
						}
					}
				}
			Iterator itCuentas = cuentas.iterator();
			while(itCuentas.hasNext()){
				reg= new HashMap();
				String cuentausr=(String)itCuentas.next();
				reg.put("CUENTA",cuentausr);
				registros.add(reg);
			}
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("registros",registros);
			jsonObj.put("AFILIADO",datoAfiliado);
			jsonObj.put("MODIFICAR",new Boolean(modificar));
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("archivoUsuario")){
			String nombreArchivo="";
			com.netro.seguridadbean.Seguridad seguridad  = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			String claveAfiliado = (request.getParameter("claveAfiliado") != null)?request.getParameter("claveAfiliado"):"";
			UtilUsr utilUsr = new UtilUsr();
			List cuentas = utilUsr.getUsuariosxAfiliado(claveAfiliado, "A"); //Cuentas Asociadas
			ConsUsuariosNafinSeg usuarios = new ConsUsuariosNafinSeg();
			List datos = usuarios.getDatosAfiliado("A", claveAfiliado); //Datos  del Afiliado
			List registros = new ArrayList();
			HashMap reg= new HashMap();
			String datoAfiliado="" ;
			if (datos.size()>0){
				datoAfiliado = "N@E: "+(String) datos.get(0)+" - "+(String)datos.get(1);
			}
			boolean modificar = false; 
			if ("NAFIN".equals(strTipoUsuario) && 	!strPerfil.equals("NAFIN OPERADOR")) {
					try{
					seguridad.validaFacultad( (String) strLogin, (String) strPerfil, "PBMXAFICON", "15FORMA5", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
					}catch(SeguException segur){
						try{
						seguridad.validaFacultad( (String) strLogin, (String) strPerfil, "PBMXTAFILI", "15FORMA5", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
						}catch(SeguException segu){
							modificar=true;
						}
					}
				}
			Iterator itCuentas = cuentas.iterator();
			
			try{
				//HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				"",
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("México, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.addText(datoAfiliado,"formas",ComunesPDF.CENTER);				
				
				//pdfDoc.setTable(16, 100);
				List lEncabezados = new ArrayList();
				
			
				
				lEncabezados.add("Login de Usuario");
				//if(modificar)
				//lEncabezados.add("Cambio de Perfil");
				
				pdfDoc.setTable(lEncabezados, "formasmenB", 30);
				
				while(itCuentas.hasNext()){
					reg= new HashMap();
					String cuentausr=(String)itCuentas.next();
					pdfDoc.setCell(cuentausr,"formas",ComunesPDF.CENTER);
					//if(modificar)
					//pdfDoc.setCell("Modificar","formas",ComunesPDF.CENTER);
				
				}
				
				//pdfDoc.addTable();
				
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("informacionUsuario")){
			CambioPerfil perfil = new  CambioPerfil();
			 jsonObj = new JSONObject();
			jsonObj.put("success",  new Boolean(true));
			String mensaje ="";
			String txtLogin = (request.getParameter("txtLogin") != null) ? request.getParameter("txtLogin") : "";
			String afiliados = "A";

			HashMap catPerfil = perfil.getConsultaUsuario( txtLogin,  afiliados  );	
			HashMap infoPerfil = new HashMap();
			if(catPerfil.size()>0){
			
				List perfiles = (List)catPerfil.get("PERFILES");
			
				String lblNombreEmpresa = (String)catPerfil.get("EMPRESA");
				String lblNafinElec = (String)catPerfil.get("NAELECTRONICO");
				String lblNombre = (String)catPerfil.get("NOMBRE_USUARIO");
				String lblApellidoPaterno = (String)catPerfil.get("APELLIDO_PATERNO");
				String lblApellidoMaterno = (String)catPerfil.get("APELLIDO_MATERNO");
				String lblEmail = (String)catPerfil.get("EMAIL");
				String lblPerfilActual = (String)catPerfil.get("PERFIL_ACTUAL");
				String internacional = (String)catPerfil.get("INTERNACIONAL");
				String sTipoAfiliado = (String)catPerfil.get("TIPOAFILIADO");		
					
				infoPerfil.put("dEmpresa", lblNombreEmpresa);
				infoPerfil.put("dNE", lblNafinElec);
				infoPerfil.put("dNombre", lblNombre);
				infoPerfil.put("dPaterno", lblApellidoPaterno);
				infoPerfil.put("dMaterno", lblApellidoMaterno);
				infoPerfil.put("dEmail", lblEmail);
				infoPerfil.put("perfil", lblPerfilActual);
				
				infoPerfil.put("internacional", internacional);
				infoPerfil.put("sTipoAfiliado", sTipoAfiliado);	
				infoPerfil.put("txtNafinElectronico", lblNafinElec);	
				infoPerfil.put("dPerfil", lblPerfilActual);
				infoPerfil.put("dClaveUsr", txtLogin);
				infoPerfil.put("txtPerfilAnt", lblPerfilActual);
				infoPerfil.put("txtLogin", txtLogin);
				jsonObj.put("PERFIL",infoPerfil);
				jsonObj.put("mensaje", mensaje);			
			}else  {
				mensaje = "No se encontró el usuario con la clave especificada, por favor vuelva a intentarlo";
				jsonObj.put("mensaje", mensaje);
			}
			
				infoRegresar = jsonObj.toString();
		}else if(informacion.equals("modificaUsuario")){
			CambioPerfil perfil = new  CambioPerfil();
			 jsonObj = new JSONObject();
			jsonObj.put("success",  new Boolean(true));
			String mensaje ="";
			String txtLogin = (request.getParameter("txtLogin") != null) ? request.getParameter("txtLogin") : "";
			String afiliados = "A";
			
			String txtPerfil = (request.getParameter("perfil")==null)?"":request.getParameter("perfil");
			String txtNafinElectronico = (request.getParameter("txtNafinElectronico")==null)?"":request.getParameter("txtNafinElectronico");
			String txtPerfilAnt = (request.getParameter("txtPerfilAnt")==null)?"":request.getParameter("txtPerfilAnt");
			String internacional = (request.getParameter("internacional")==null)?"":request.getParameter("internacional");
			String txtTipoAfiliado = (request.getParameter("sTipoAfiliado")==null)?"":request.getParameter("sTipoAfiliado");
			String clave_usuario = (String)session.getAttribute("Clave_usuario");
			
			netropology.utilerias.usuarios.Usuario oUsuario = null;
			UtilUsr oUtilUsr = null;
			AccesoDB con = new AccesoDB();
			List datosAnteriores = new ArrayList();
			List datosActuales = new ArrayList();
			StringBuffer valoresAnteriores = new StringBuffer();
			StringBuffer valoresActuales = new StringBuffer();
			String[] nombres = {
				"Perfil"
			};
			boolean exito = true;
			try{	
			con.conexionDB();
			oUtilUsr = new UtilUsr();
			oUtilUsr.setPerfilUsuario(txtLogin, txtPerfil);

			datosAnteriores.add(0, txtLogin + " - " + txtPerfilAnt);
			datosActuales.add(0, txtLogin + " - " + txtPerfil);
			List cambios = Bitacora.getCambiosBitacora(datosAnteriores, datosActuales, nombres);
			valoresAnteriores.append(cambios.get(0));
			valoresActuales.append(cambios.get(1));
			
			Bitacora.grabarEnBitacora(con, "SEGCAMBIOPERFIL", txtTipoAfiliado+internacional,
					txtNafinElectronico, (String)session.getAttribute("Clave_usuario"),
					valoresAnteriores.toString(), valoresActuales.toString());
					
			}catch (Exception e){
				exito = false;
				e.printStackTrace();
			} finally {
				if (con.hayConexionAbierta() == true)
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
					
			jsonObj = new JSONObject();
			jsonObj.put("success",  new Boolean(true)); 	
			jsonObj.put("mensaje", mensaje);
			infoRegresar = jsonObj.toString();
		} else if(informacion.equals("catalogoPerfil") ){  
			CambioPerfil perfil = new  CambioPerfil();
			String txtLogin = (request.getParameter("txtLogin") != null) ? request.getParameter("txtLogin") : "";
			String afiliados = "A";
			
			HashMap catPerfil = perfil.getConsultaUsuario( txtLogin,  afiliados  );	
			List perfiles = (List)catPerfil.get("PERFILES");	
			Iterator it = perfiles.iterator();	
			HashMap datos = new HashMap();
			List registros  = new ArrayList();	
			while(it.hasNext()) {
				List campos = (List) it.next();
				String clave = (String) campos.get(0);
				datos = new HashMap();
				datos.put("clave", clave );
				datos.put("descripcion", clave );
				registros.add(datos);
			}
			
			 jsonObj = new JSONObject();
			jsonObj.put("success",  new Boolean(true)); 	
			jsonObj.put("registros", registros);
			infoRegresar = jsonObj.toString();
		
		}


%>
<%=infoRegresar%>