
function selecREAFILIACION(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
	var fecha_hoy=  Ext.util.Format.date(Ext.getCmp('fecha_hoy').getValue(),'d/m/Y')
 
	if(check.checked==true)  {
		reg.set('REAFILIACION', "S");
		 reg.set('FECHA_ACTIVACION', fecha_hoy);
	}else  {
	 reg.set('REAFILIACION', "N"); 	
	 reg.set('FECHA_ACTIVACION', "");
	}	
	store.commitChanges();	
}

Ext.onReady(function() {

	var claveEPO = [];
	var chkREAFILIACION = [];
	var fecha_activacion = [];
	var proveedores =[];


	var cancelar =  function() {  
		claveEPO = [];
		chkREAFILIACION = [];
		fecha_activacion = [];
		proveedores =[];
	}
	
	
	function transmiteCaptura(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			if(info.mensaje!='') {				
				Ext.Msg.alert('Aviso',info.mensaje,function(btn){	
				window.location = '15reafiliacion_autExt.jsp';
				});			
			}
		} else {
				NE.util.mostrarConnError(response,opts);
				
		}
	}
	
	var procesarGrabar = function(store, arrRegistros, opts) 	{
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		var jsonData = store.data.items;
		var numRegistros=0;
			cancelar();
	
		Ext.each(jsonData, function(item,inx,arrItem){
			claveEPO.push(item.data.IC_EPO);	
			chkREAFILIACION.push(item.data.REAFILIACION);	
			fecha_activacion.push(item.data.FECHA_ACTIVACION);
			proveedores.push(item.data.PROVEEDORES);
			numRegistros++;
		});		
			
		Ext.Ajax.request({
				url: '15reafiliacion_autExt.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Captura',
					claveEPO:claveEPO,
					chkREAFILIACION:chkREAFILIACION,
					fecha_activacion:fecha_activacion,
					numRegistros:numRegistros,
					proveedores:proveedores
				}),
				callback: transmiteCaptura
			});
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;
			Ext.getCmp('fecha_hoy').setValue(jsonData.fecha_hoy);
			
			
			if(store.getTotalCount() > 0) {		
			Ext.getCmp('btnGrabar').enable();
				el.unmask();					
			} else {	
				Ext.getCmp('btnGrabar').disable();
				el.mask('No se encontró ningún registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15reafiliacion_autExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'IC_EPO'},
			{	name: 'NOMBRE_EPO'},
			{	name: 'REAFILIACION'},
			{	name: 'FECHA_ACTIVACION'},
			{	name: 'USUARIO'},
			{  name: 'PROVEEDORES' }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});

	var catalogoProveedor = new Ext.data.ArrayStore({
		 fields : ['clave', 'descripcion','loadMsg'],			 
		 data : [
			['AF','Afiliados'],
			['PA','Por Afiliar'],
			['A','Ambos']			
		]
	});
	
	var comboProveedor = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
		mode: 'local',
      store :catalogoProveedor
	});
	
	Ext.util.Format.comboRenderer = function(comboProveedor){
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var record = comboProveedor.findRecord(comboProveedor.valueField, value);
			return record ? record.get(comboProveedor.displayField) : comboProveedor.valueNotFoundText;					
		}
	}	

	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 200,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},					
			{	
				header:'Reafiliación Automática....',
				tooltip: 'Reafiliación Automática',
				dataIndex : 'REAFILIACION',
				width : 150,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['REAFILIACION']=='S' ){
						return '<input  id="chkREAFILIACION"'+ rowIndex + ' value='+ value + ' type="checkbox"  checked  onclick="selecREAFILIACION(this, '+rowIndex +','+colIndex+');" />';				
					}else {
						return '<input  id="chkREAFILIACION"'+ rowIndex + '  type="checkbox"  onclick="selecREAFILIACION(this, '+rowIndex +','+colIndex+');" />';				
					}
				}
			},	
			{
				header: 'Proveedores',
				tooltip: 'Proveedores',
				dataIndex: 'PROVEEDORES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				editor: comboProveedor,
				renderer: Ext.util.Format.comboRenderer(comboProveedor)
			},	
			{
				header: 'Fecha de activación',
				tooltip: 'Fecha de activación',
				dataIndex: 'FECHA_ACTIVACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Usuario',
				tooltip: 'Usuario',
				dataIndex: 'USUARIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 200,
		width: 660,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Grabar',					
					tooltip:	'Grabar ',
					iconCls: 'autorizar',
					id: 'btnGrabar',
					handler: procesarGrabar  
				}
			]
		}
	});

	
	/**********CRITERIOS DE BUSQUEDA ***************/		

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: '<b>Parametrizar</b>',			
				id: 'btnCaptura',					
				handler: function() {
					window.location = '15reafiliacion_autExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Consulta',			
				id: 'btnConsulta',					
				handler: function() {
					window.location = '15reafiliacion_aut_consExt.jsp';
				}
			}	
		]
	});
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15reafiliacion_autExt.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var elementosForma=[
		{
			xtype: 'combo',
			fieldLabel: 'EPO',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar EPO...',			
			valueField: 'clave',	
			hiddenName : 'ic_epo',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						fp.el.mask('Enviando...', 'x-mask-loading');			
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
								ic_epo:combo.getValue()							
							})
						});					
					}
				}
			}
		},
		{ 	xtype: 'datefield', hidden: true, allowBlank: true, startDay: 1,	width: 100, msgTarget: 'side',  vtype: 'rangofecha',  id: 'fecha_hoy', 	value: '' }
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Captura Reafiliaciones Automaticas ',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			fpBotones,
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	

	catalogoEPO.load();
	
});	