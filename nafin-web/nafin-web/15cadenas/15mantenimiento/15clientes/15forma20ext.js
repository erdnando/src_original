Ext.onReady(function() {
   var sTipoAfiliado = 1;
   
	var busqAvanzadaSelect=0;
	Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);
	//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT

	var procesarCatalogoEpo = function(store, arrRegistros, opts) {
		//Ext.getCmp("sIcEpo1").setValue((sIcEpo));	
	}
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
	
			var el = grid.getGridEl();
			var cm = grid.getColumnModel();			
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnImprimirPDF').enable();
				
				if(sTipoAfiliado!==1){							
					grid.getColumnModel().setHidden(cm.findColumnIndex('TASA_FLOATING'), true);	
					grid.getColumnModel().setHidden(cm.findColumnIndex('FECHA_PARAM_FLOATING'), true);	
				}				
				
				el.unmask();
			} else {
				Ext.getCmp('btnBajarArchivo').hide();				
				Ext.getCmp('btnImprimirPDF').disable();
				btnGenerarArchivo.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);			
		}
	}

	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	// Procesar Imprimir PDF
	var procesarSuccessFailureImprimirPDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	var storeTipoAfiliado = new Ext.data.ArrayStore({
		fields : ['clave', 'descripcion'],  
		data : [  
			[1, 'Proveedor'],  
			[2, 'Distribuidor']
		]
	});
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15forma20ext.data.jsp',
		baseParams: {
			informacion: 'Consulta',
         start :  0,
         limit :  15
		},
		fields: [
			{name: 'EPO'},
			{name: 'NOMBRE'},
			{name: 'RFC'},
			{name: 'DOMICILIO'},
			{name: 'ESTADO'},
			{name: 'TEL'},
			{name: 'MAIL'},
			{name: 'CONTACTO'},
			{name: 'AUTORIZADAS'},
			{ name: 'TASA_FLOATING'       },
			{ name: 'FECHA_PARAM_FLOATING' }
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
         beforeLoad:	{
				fn: function(store, options){
					Ext.apply(options.params, {
						sTipoAfiliado:	Ext.getCmp('id_sTipoAfiliado').getValue()
					});
				}		
			},
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	var catalogoEstados = new Ext.data.JsonStore({
		id: 'catalogoEstados',
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15forma20ext.data.jsp',
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoEstados'
		},
		totalProperty : 'total',
		autoLoad: false
	});
	var catalogoEpo = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15forma20ext.data.jsp',
		listeners: {
			load: procesarCatalogoEpo,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: true
	});

//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		columns: [{
				header: 'Cadena Productiva', tooltip: 'Cadena Productiva',
				dataIndex: 'EPO',
				sortable: true,
				width: 200, resizable: true,
				align: 'left'
			},{
				header: 'Nombre o Razon Social', tooltip: 'Nombre o Razon Social',
				dataIndex: 'NOMBRE',
				sortable: true,	align: 'center',
				width: 110, resizable: true,
				align: 'left'
			},{
				header: 'RFC', tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,	align: 'center',
				width: 150, resizable: true,
				align: 'left'
			},{
				header: 'Domicilio', tooltip: 'Domicilio',
				dataIndex: 'DOMICILIO',
				sortable: true,	align: 'center',
				width: 150, resizable: true,
				align: 'left'
			},{
				header: 'Estado', tooltip: 'Estado',
				dataIndex: 'ESTADO',
				sortable: true,	align: 'center',
				width: 150, resizable: true,
				align: 'left'
			},{
				header: 'Telefono', tooltip: 'Telefono',
				dataIndex: 'TEL',
				sortable: true,	align: 'center',
				width: 100, resizable: true,
				align: 'left'
			},{
				header: 'E-mail', tooltip: 'E-mail',
				dataIndex: 'MAIL',
				sortable: true,	align: 'center',
				width: 150, resizable: true,
				align: 'left'
			},{
				header: 'Contacto', tooltip: 'Contacto',
				dataIndex: 'CONTACTO',
				sortable: true,	align: 'center',
				width: 150, resizable: true,
				align: 'left'
			},{
				header: 'Autorizada', tooltip: 'Autorizada',
				dataIndex: 'AUTORIZADAS',
				sortable: true,	align: 'center',
				width: 50, resizable: true,
				align: 'left',
				renderer: function(value, metadata, registro){
					var dato = registro.data['AUTORIZADAS'];
					return parseInt(dato) > 0 ? 'SI' : 'NO';
				}
			},
			{
				header:    'Proveedores <br> susceptibles a Floating ',
				dataIndex: 'TASA_FLOATING',
				sortable:  true,
				resizable: true,
				align:     'center',				
				width:     150
			},		
			{
				header:    'Fecha Parametrizaci�n <br>Floating',
				dataIndex: 'FECHA_PARAM_FLOATING',
				sortable:  true,
				resizable: true,
				align:     'center',				
				width:     150,
				renderer:function(value,metadata,registro){
					if(registro.data['TASA_FLOATING']==='Si') {
						return  value;
					}
				}
			}
			],
		viewConfig: {
         stripeRows: true
      },
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 900,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		bbar: {
         xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						var cmpBarraPaginacion = Ext.getCmp('barraPaginacion');
						Ext.Ajax.request({
							url: '15forma20ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'XLS',
								informacion: 'Consulta',
								suceptibleFloating: Ext.getCmp('suceptibleFloating1').getValue(),
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					iconCls: 'icoXls',
					id: 'btnBajarArchivo',
					hidden: true
				},				
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',
					id: 'btnImprimirPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '15forma20ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'PDFPrint',
								informacion: 'Consulta',
								start: cmpBarraPaginacion.cursor,
								suceptibleFloating: Ext.getCmp('suceptibleFloating1').getValue(),
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureImprimirPDF
						});
					}
				}
			]
		}
	};

	
	var elementosForma = [{
			xtype: 'combo',
			name: 'sTipoAfiliado',
			hiddenName : 'sTipoAfiliado',
			id: 'id_sTipoAfiliado',
			fieldLabel: 'Tipo de Afiliado',
			minChars : 1,
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			emptyText: 'Seleccione Tipo de Afiliado',
			triggerAction : 'all',
			typeAhead: true,
			anchor: '60%',
			store : storeTipoAfiliado,
         value :  '1',
			listeners:{
				select:function(comboField, record, index){
               var sIcEdo = Ext.getCmp('id_sIcEdo1');
               sTipoAfiliado = record.get('clave');
               
               catalogoEstados.removeAll();
               catalogoEstados.load({ params: {sTipoAfiliado: record.get('clave')} });
               catalogoEpo.removeAll();
               catalogoEpo.load({ params: {sTipoAfiliado: record.get('clave')} });
										
					if(sTipoAfiliado===2){
						Ext.getCmp('suceptibleFloating1').hide();
					}else if(sTipoAfiliado===1){
						Ext.getCmp('suceptibleFloating1').show();
					}
					
					
				}
			}
		},{
			xtype: 		'textfield',
			fieldLabel: 'Nombre',
			id:			'sNombre',
			name:			'sNombre'
		},{
			xtype: 'combo',
			name: 'sIcEdo',
			id: 'id_sIcEdo1',
			mode: 'local',
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'sIcEdo',
			fieldLabel: 'Estado',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEstados
		},{
			xtype: 'textfield',
			fieldLabel: 'RFC',
			name: 'sRFC',
			id: 'sRFC',
			anchor:'50%'
		},{
			xtype: 'combo',
			name: 'sIcEpo',
			id: 'sIcEpo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'sIcEpo',
			fieldLabel: 'Cadena Productiva',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEpo
		},
		{
			width:          300,
			xtype:          'checkbox',
			id:             'suceptibleFloating1',
			name:           'suceptibleFloating',
			boxLabel:       'Proveedores susceptibles a Floating'
		}
	];
	
		var fp = {
		xtype: 'form',
		id: 'forma',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 500,
		titleCollapse: false,
		labelWidth: 110,
		defaultType: 'textfield',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[elementosForma],
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					var cmpForma = Ext.getCmp('forma');
               Ext.getCmp('grid').show();
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():"";				
					consultaDataGrid.load({
						params: Ext.apply(paramSubmit,{
						operacion: 'Generar',
                  sTipoAfiliado: Ext.getCmp('id_sTipoAfiliado').getValue(),
						suceptibleFloating: Ext.getCmp('suceptibleFloating1').getValue(),
						start:0,
						limit:15							
                  })
               });
            } //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15forma20ext.jsp';
				}
			}
		]
	};//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});
   
   catalogoEstados.load({
      params: {
         sTipoAfiliado: '1'
      }
   });
   
});
