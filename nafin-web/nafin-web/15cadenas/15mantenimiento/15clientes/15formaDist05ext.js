Ext.onReady(function(){
	
	var strPerfil = Ext.getDom('strPerfil').value;
	var idMenu =  Ext.getDom('idMenu').value;
	var permisosGlobales;
	
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
 	function creditoElec(pagina) {
		if (pagina == 0) { //Afianzadora
			window.location  = "15forma05AfianzadoraExt.jsp?idMenu="+idMenu; 	
		}	else if (pagina == 1 ) { // cadena productiva
			window.location ="15consEPOext.jsp?idMenu="+idMenu;
		}	else if (pagina == 2) { //provedores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15forma05ProveedoresExt.jsp?idMenu="+idMenu;
		}	else if (pagina == 3) { //Provedor Internacional
			
		}	else if (pagina == 4) { // provedor sin numero
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp?idMenu="+idMenu;
		}	else if (pagina == 5) { // Distribuidores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp?idMenu="+idMenu;		
		}	else if (pagina == 6) { // Fiados
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp?idMenu="+idMenu; 	
		}	else if (pagina == 7) { // Intermediario financiero Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B&idMenu="+idMenu;  	
		} else if (pagina == 8) { // Intermediario financiero NO Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB&idMenu="+idMenu;  	 	
		} else if (pagina == 9) { // Intermediario financiero Bancario /No Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB&idMenu="+idMenu; 		 	
		} else if (pagina == 10) { // Provedor Carga Masiva EContrac
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon_ext.jsp?idMenu="+idMenu; 
		}	else if (pagina == 11) { // Afiliados a Credito Electronico
			window.location = "15forma05ext_AfCredElectronico.jsp?idMenu="+idMenu; 
		}	else if (pagina == 12) { // Cadena Productiva Internacional
			
		}	else if (pagina == 13) { // Contragarante
			
		}	else if (pagina == 14) { // Mandante
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp?idMenu="+idMenu;  	
		}	else if (pagina == 15) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp?idMenu="+idMenu;  	
		} else if (pagina == 16) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp?idMenu="+idMenu; 		
		}			

	}
	
	function procesaValoresIniciales(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var  info = Ext.util.JSON.decode(response.responseText);			
			permisosGlobales=info.permisos;	 		
				
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;		
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarReafiliar = function(grid, rowIndex, colIndex, item, event){
	
		var registro = grid.getStore().getAt(rowIndex);
		var clavePyme = registro.get('CLIENTE');  
		var claveEpo     = registro.get('IC_EPO');
		var rfc          = registro.get('RFC');
		var razonSocial  = registro.get('RAZONSOCIAL');
		var csAceptacion = registro.get('CS_ACEPTACION');
		window.location="15reafiliaDistext.jsp?rfc="+rfc+"&icPyme="+clavePyme+"&razonSocial="+razonSocial+"&cs_aceptacion="+csAceptacion;
	
	}

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {					
				el.unmask();
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarCSV').enable();				
			} else {		
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarCSV').disable();				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	var procesarVerCuentas= function(grid, rowIndex, colIndex, item, event) {
	
		var registro = grid.getStore().getAt(rowIndex);
		var clavePyme = registro.get('CLIENTE');  
		var naE = registro.get('ELECT'); 
		var nombreEPO = registro.get('RAZONSOCIAL'); 
		var tituloC = 'N@E:'+ naE +' '+nombreEPO;
		var tituloF ='';
	
	var consCuentasData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15formaDist05ext.data.jsp',
		baseParams: {
			informacion: 'ConsCuentas'			
		},		
		fields: [				
			{ name: 'CUENTA'}	,
			{ name: 'CAMBIO_PERFIL'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConCuentasData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConCuentasData(null, null, null);					
				}
			}
		}					
	});
	
		consCuentasData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						clavePyme: clavePyme,
						informacion: 'ConsCuentas',
						tipoAfiliado:'P',
						tituloC:tituloC
					})
				});
			var gridCuentas = {
		//xtype: 'editorgrid',
		xtype: 'grid',
		store: consCuentasData,
		id: 'gridCuentas',		
		hidden: false,
		title: 'Usuarios',	
		columns: [			
			{
				xtype:	'actioncolumn',
				header: 'Login del Usuario',
				tooltip: 'Login del Usuario',
				dataIndex: 'CUENTA',
				align: 'center',
				width: 150,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					return (record.get('CUENTA'));
				},
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';							
						}
						,handler:	procesaCuenta  
					}
				]
			},
			{
				xtype:	'actioncolumn',
				header: 'Cambio de Perfil',
				tooltip: 'Cambio de Perfil',	
				dataIndex: 'CAMBIO_PERFIL',
				align: 'center',				
				width: 150,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							for(i=0;i<permisosGlobales.length;i++){ 
								boton=  permisosGlobales[i];										
								if(boton=="CAMBIO_PERFIL") {	
									this.items[0].tooltip = 'Modificar';
									return 'modificar';	
								}
							}
						}
						,handler:	modificarCuenta  
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 360,
		width: 320,		
		frame: true
	}				
				
		new Ext.Window({					
					modal: true,
					width: 340,
					height: 380,
					modal: true,					
					closeAction: 'hide',
					resizable: true,
					constrain: true,
					closable:false,
					id: 'Vercuentas',
					items: [					
						gridCuentas					
					],
					bbar: {
						xtype: 'toolbar',	buttonAlign:'center',	
						buttons: ['-',
							{	xtype: 'button',	text: 'Cerrar',	iconCls: 'icoLimpiar', 	id: 'btnCerrar',
								handler: function(){																		
									Ext.getCmp('Vercuentas').setTitle('');
									Ext.getCmp('Vercuentas').destroy();
									
								} 
						},
							'-',
							{
								xtype: 'button',
								text: 'Imprimir',
								tooltip:	'Imprimir',
								id: 'btnImprimirC',
								iconCls: 'icoPdf',
								handler: function(boton, evento) {																
									Ext.Ajax.request({
										url: '15formaDist05ext.data.jsp',
										params:	{
											clavePyme: clavePyme,
											informacion: 'ImprimirConsCuentas',
											tipoAfiliado:'P',
											tituloF:tituloF,
											tituloC:tituloC											
										}					
										,callback: procesarDescargaArchivos
									});
								}
							}	
						]
					}
				}).show().setTitle(tituloC);
				//cuentasUsuario(tituloC);
					
			//}	
	}
	
	
	var procesarConCuentasData = function(store,arrRegistros,opts){
	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros != null){	
		
			var gridCuentas = Ext.getCmp('gridCuentas');
			
			if (!gridCuentas.isVisible()) {
				gridCuentas.show();
			}	
			var el = gridCuentas.getGridEl();			
			var info = store.reader.jsonData;	
		
			if(store.getTotalCount()>0){				
				Ext.getCmp('btnImprimirC').enable();
			}else{
				Ext.getCmp('btnImprimirC').disable();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	function modificarPerfil(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert('Mensaje',info.mensaje,
				function(){
					Ext.getCmp('ProcesarCuentas').hide();
				}	
			);
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	var procesarModificar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var claveEpo = registro.get('IC_EPO'); 
		var clavePyme = registro.get('CLIENTE'); //clavePyme
		var tipoPersona = registro.get('CS_TIPO_PERSONA');
		var tipoAfiliacion =registro.get('CS_TIPO_AFILIA_DISTRI');
		
		//window.location="15forma10b.jsp?iPyme=<%= ic_num %>&txtaction=mod&cboTipoPersona=<%= cs_tipo_persona %>&TipoPYME=<%= iniCon %>&cboEPO=<%= ic_epo %>&R_F_C=<%=rfc%>&operacion=M&cboTipoAfiliacion=2&url_origen=<%=url_origen%>"
		window.location ="15formaDistModificar05ext.jsp?claveEpo="+claveEpo+"&clavePyme="+clavePyme+"&tipoPersona="+tipoPersona+"&txtaction=mod&internacional=N&tipoAfiliacion="+tipoAfiliacion+"&idMenu="+idMenu;
	}
	
	function procesarInformacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			if(info.mensaje =='') {		
				Ext.getCmp('lblNombreEmpresa1').update(info.lblNombreEmpresa);
				Ext.getCmp('lblNafinElec1').update(info.lblNafinElec);
				Ext.getCmp('lblNombre1').update(info.lblNombre);
				Ext.getCmp('lblApellidoPaterno1').update(info.lblApellidoPaterno);
				Ext.getCmp('lblApellidoMaterno1').update(info.lblApellidoMaterno);
				Ext.getCmp('lblEmail1').update(info.lblEmail);
				Ext.getCmp('lblPerfilActual1').update(info.lblPerfilActual);
				Ext.getCmp('txtLogin1').setValue(info.txtLogin);
				Ext.getCmp('sTipoAfiliado').setValue(info.sTipoAfiliado);					//????????????
				
				Ext.getCmp('internacional').setValue(info.internacional);					//????????????	
				Ext.getCmp('sTipoAfiliado').setValue(info.sTipoAfiliado);					//????????????	
				Ext.getCmp('txtNafinElectronico').setValue(info.txtNafinElectronico);	//????????????	
				Ext.getCmp('txtPerfilAnt').setValue(info.txtPerfilAnt);						//????????????	
				Ext.getCmp('txtLoginC').setValue(info.txtLogin);								//????????????
				
				if(info.modificar=='S')  {
					Ext.getCmp('txtPerfil_1').show();
					catalogoPerfil.load({
						params:{ 
							tipoAfiliado: info.sTipoAfiliado, 
							txtLogin:info.txtLogin
						}	
					});
			}else  {
				Ext.getCmp('txtPerfil_1').hide();
			}
				
			}else  {  		/* no hay datos */   		}
			
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
	//__________________PARA VER LA INFORMACION DEL USUARIO Y MODIFICAR LOS CAMPOE EDITABLES_____________
	var modificarCuenta = function(grid, rowIndex, colIndex, item, event) {  
		
		var registro = grid.getStore().getAt(rowIndex);
		var txtLogin = registro.get('CUENTA');  
		Ext.getCmp('btnModificar').show();
		Ext.getCmp('id_modificacion').show();
		Ext.getCmp('id_informacion').hide();
		 
		Ext.Ajax.request({
			url: '15formaDist05ext.data.jsp',
			params: {
				informacion: 'informacionUsuario',		
				txtLogin:txtLogin,
				modificar:'S'
			},
			callback:procesarInformacion 
		});
		ventanaCuenta();
	}

	
	//__________________PARA VER LA INFORMACION DEL USUARIO EN SOLO LECTURA__________________
	var procesaCuenta = function(grid, rowIndex, colIndex, item, event) {  
		
		var registro = grid.getStore().getAt(rowIndex);
			
		var txtLogin = registro.get('CUENTA');  
		 
		 Ext.getCmp('btnModificar').hide();
		 Ext.getCmp('id_modificacion').hide();
		 Ext.getCmp('id_informacion').show();
		 
		Ext.Ajax.request({
			url: '15formaDist05ext.data.jsp',
			params: {
				informacion: 'informacionUsuario',		
				txtLogin:txtLogin,
				modificar:'N'//SOLO LECTURA
			},
			callback:procesarInformacion 
		});
		ventanaCuenta();
	}
	//______VENTANA DE INFORMACION DEL USUARIO
	var ventanaCuenta = function() {
	
		var ProcesarCuentas = Ext.getCmp('ProcesarCuentas');
		if(ProcesarCuentas){
			ProcesarCuentas.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 700,
				height: 400,												
				resizable: false,
				closable:false,
				id: 'ProcesarCuentas',
				closeAction: 'hide',
				items: [					
					fpDespliega					
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	xtype: 'button',	text: 'Salir',	iconCls: 'icoLimpiar', 	id: 'btnCerraP', handler: function(){Ext.getCmp('ProcesarCuentas').hide();} },
						'-'							
					]
				}
			}).show().setTitle('');
		}	
	}

	
	
//------------------------------------------------------------------------------
//-------------------------------STORE's----------------------------------------
//------------------------------------------------------------------------------
	var dataStatus = new Ext.data.ArrayStore({	 
	  fields: ['clave','descripcion'  ],	 
	  autoLoad: false	  
	}); 
	
	if(strPerfil== "ADMIN NAFIN")  {
		dataStatus.loadData(	[	
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],			
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],				
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo'],
			['16','Cliente Externo']
		]);
		
	}else  if(strPerfil== "PROMO NAFIN FAC" || strPerfil== "PROMO NAFIN" ||  strPerfil== "NAFIN OPERADOR" ||   strPerfil== "NAFIN CON PROM"  ||   strPerfil== "NAFIN CON"    ||  strPerfil== "SUPER CALL CENT" ||  strPerfil== "ADMIN NAFIN EXT"  )  {
		dataStatus.loadData(	[			
			['2','Proveedores'],			
			['5','Distribuidores']
		]);	
		
	}else  if(strPerfil== "CALL CENTER CAD")   { // F021 - 2015
		dataStatus.loadData(	[
			['1','Cadena Productiva'],
			['2','Proveedores'],	
			['5','Distribuidores'],
			['9','Intermediarios Bancarios/No Bancarios']	
		]);
	}
	
	
	var catalogoEstado = new Ext.data.JsonStore({
		id: 'catalogoEstado',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15formaDist05ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEstado'  ///hacer el metodo en el data
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoCadenaProductiva = new Ext.data.JsonStore({
		id: 'catalogoEstado',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15formaDist05ext.data.jsp',
		baseParams: {
			informacion: 'catalogoCadenaProductiva'  ///hacer el metodo en el data
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15formaDist05ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [
			{	name: 'CG_RAZON_SOCIAL'},
			{	name: 'NUM_DISTRIBUIDOR'},	
			{	name: 'ELECT'},
			{	name: 'IN_NUMERO_SIRAC'},	
			{	name: 'RAZONSOCIAL'},
			{	name: 'RFC'},
			{	name: 'CALLE'},	
			{	name: 'COLONIA'},
			{	name:	'ESTADO'},
			{	name:	'TELEFONO'},
			{	name: 'CD_VERSION_CONVENIO'},	
			{	name: 'FECHA_CONSULTA_CONVENIO'},
			{	name: 'CONVENIO_UNICO'},
			{	name:	'IC_EPO'},
			{	name:	'CLIENTE'},
			{	name:	'CS_TIPO_PERSONA'},
			{	name:	'CS_TIPO_AFILIA_DISTRI'},
			{	name:	'IC_TIPO_CLIENTE'},
			{	name:	'CS_ACEPTACION' }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	})
	
	
	var procesarBorrar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clavePyme = registro.get('CLIENTE');  
		var claveEpo=registro.get('IC_EPO'); 
		var tipoCliente=registro.get('IC_TIPO_CLIENTE'); 
		Ext.Ajax.request({
				url: '15formaDist05ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion	: 'Borrar',
					clavePyme	:clavePyme,
					claveEpo		:claveEpo,
					tipoCliente:tipoCliente
				}),
				callback: TransmiteBorrar
			});
	}
	
	function TransmiteBorrar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);	
			
				Ext.MessageBox.alert('Mensaje',info.msg,
					function(){
						if(info.exito){
							consultaData.load({	
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar',
									informacion: 'Consultar',
									start:0,
									limit:15						
								})
							});
						}
					}	
				);
			
			

		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	var catalogoPerfil = new Ext.data.JsonStore({
		id: 'catalogoPerfil',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: '15formaDist05ext.data.jsp',
		baseParams: {
			informacion: 'catalogoPerfil'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {				
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
//------------------------------------------------------------------------------
//------------------------------ELEMENTOS---------------------------------------
//------------------------------------------------------------------------------


	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta- Distribuidores',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
		
			{
				header: 'Cadena Productiva',
				tooltip: 'Cadena Productiva',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,
				width: 250,			
				resizable: true,				
				align: 'left',
				renderer:function(v){
					return '<div align=left >'+v+'</div>';
				}
			},
			{
				header: 'N�mero de distribuidor',
				tooltip: 'N�mero de distribuidor',
				dataIndex: 'NUM_DISTRIBUIDOR',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'N�mero de Nafin Electr�nico',
				tooltip: 'N�mero de Nafin Electr�nico',
				dataIndex: 'ELECT',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'N�mero de SIRAC',
				tooltip: 'N�mero de SIRAC',
				dataIndex: 'IN_NUMERO_SIRAC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Nombre o Raz�n Social',
				tooltip: 'Nombre o Raz�n Social',
				dataIndex: 'RAZONSOCIAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Domicilio',
				tooltip: 'Domicilio',
				dataIndex: 'DOMICILIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer: function(v, params, record){
					return (record.data.CALLE+' '+  record.data.COLONIA);
				}
			},
			{
				header: 'Estado',
				tooltip: 'Estado',
				dataIndex: 'ESTADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Tel�fono',
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var cs_habilitado = registro.data['CS_HABILITADO'];	
					if(cs_habilitado =='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Versi�n Convenio ',
				tooltip: 'Versi�n Convenio ',
				dataIndex: 'CD_VERSION_CONVENIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',	
				renderer:function(value,metadata,registro){                                
					var version = registro.data['CD_VERSION_CONVENIO'];	
					if(version =='') {
						return '--';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Fecha de Consulta convenio',
				tooltip: 'Fecha de Consulta convenio',
				dataIndex: 'FECHA_CONSULTA_CONVENIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){                                
					var version = registro.data['CD_VERSION_CONVENIO'];	
					if(version =='') {
						return '--';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Convenio �nico',
				tooltip: 'Convenio �nico ',
				dataIndex: 'CONVENIO_UNICO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
					if(record.data.CONVENIO_UNICO=='S'){
						return 'SI';
					} 
					else{
						return 'NO';
					}
				}
			},
			{
				xtype:        'actioncolumn',
				header:       'Modificar',
				align:        'center',
				sortable: true,
				resizable: true,				
				menuDisabled: true,
				width:        80,
				items: [{
					iconCls:   'modificar',
					tooltip:   'Modificar',
					handler:   procesarModificar
				}]
			},
			{
				xtype:        'actioncolumn',
				header:       'Eliminar',
				align: 'center',
				sortable: true,
				resizable: true,				
				menuDisabled: true,
				width:        80,
				items: [{
					iconCls:   'borrar',
					tooltip:   'Eliminar',
					handler:   procesarBorrar
				}]
					},
					{
				xtype:        'actioncolumn',
				header:       'Reafiliaci�n',
				tooltip:      'Reafiliaci�n',
				align:        'center',
				sortable:     true,
				resizable:    true,
				hidden:       false,
				width:        80,
				items: [{
					iconCls:   'modificar',
					tooltip:   'Reafiliar',
					handler:   procesarReafiliar
				}]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Cuentas de Usuario',
				tooltip: 'Cuentas de Usuario ',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}
						,handler: procesarVerCuentas
					}
				]
			}
		],	
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 400,
		width				: 900,
		align				: 'center',
		frame				: false,
		bbar: 
			{
			xtype			: 'paging',
			pageSize		: 15,
			buttonAlign	: 'left',
			id				: 'barraPaginacion',
			displayInfo	: true,
			store			: consultaData,
			displayMsg	: '{0} - {1} de {2}',
			emptyMsg		: "No hay registros.",
			items			: [
				'->','-',
				{
					xtype		: 'button',
					text		: 'Generar Archivo',					
					tooltip	:	'Generar Archivo ',
					iconCls	: 'icoXls',
					id			: 'btnGenerarCSV',
					handler	: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						boton.setDisabled(true);
						Ext.Ajax.request({
							url: '15formaDist05ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GeneraArchivoCSV'			
							}),
							success : function(response) {
								boton.setIconClass('icoXls');     
								boton.setDisabled(false);

							},
							callback: procesarDescargaArchivos
						});
					}
				},
				'-',
				{
					xtype		: 'button',
					text		: 'Imprimir',					
					tooltip	:	'Imprimir ',
					iconCls	: 'icoPdf',
					id			: 'btnImprimir',
					handler	: function(boton, evento) {
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						boton.setIconClass('loading-indicator');
						boton.setDisabled(true);
						Ext.Ajax.request({
							url: '15formaDist05ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GeneraArchivoPDF',
								start: barraPaginacion.cursor,
								limit: barraPaginacion.pageSize			
							}),
							success : function(response) {
								boton.setIconClass('icoPdf');     
								boton.setDisabled(false);

							},
							callback: procesarDescargaArchivos
						});
					}
				}				
			]
		}
	});
	//_________________ELEMENTOS QUE SE DESPLIEGAN CUANDO SE CONSULTA LA INFO DEL USUARIO__________________(VER CUENTAS)
	var elementosDespliega =[
		{ 	xtype: 'textfield',  hidden: true, id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'internacional', 	value: '' },		
		{ 	xtype: 'textfield',  hidden: true,  id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtNafinElectronico', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtPerfilAnt', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtLoginC', 	value: '' },			
		{
			xtype: 'panel',
			id: 'id_informacion', 
			allowBlank: false,
			title:'INFORMACION USUARIO',			
			value: '',
			hidden: true
		},
		{
			xtype: 'panel',
			id: 'id_modificacion', 
			allowBlank: false,
			title:'CAMBIO DE PERFIL',			
			value: '',
			hidden: true
		},
		{
			xtype: 'displayfield',
			name: 'txtLogin',
			id: 'txtLogin1',
			allowBlank: false,			
			fieldLabel: 'Clave de Usuario',
			value:''
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			fieldLabel: 'Empresa',
			items: [
				{
					xtype: 'displayfield',
					name: 'lblNombreEmpresa',
					id: 'lblNombreEmpresa1',
					allowBlank: false,
					width			: 230,
					value:''
				},
				{
					xtype: 'displayfield',
					id:'muestraCol',
					value: 'Nafin Electr�nico:',
					hidden: false
				},		
				{		
					xtype: 'displayfield',
					name: 'lblNafinElec',
					id: 'lblNafinElec1',
					allowBlank: false,
					anchor:'70%',
					width			: 230,				
					value:''
				}
			]
		},	
		{
			xtype: 'displayfield',
			name: 'lblNombre',
			id: 'lblNombre1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Nombre',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblApellidoPaterno',
			id: 'lblApellidoPaterno1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Apellido Paterno',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblApellidoMaterno',
			id: 'lblApellidoMaterno1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Apellido Materno',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblEmail',
			id: 'lblEmail1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Email',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblPerfilActual',
			id: 'lblPerfilActual1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Perfil Actual',
			value:''
		},
		{
			xtype: 'combo',
			fieldLabel: 'Perfil',
			name: 'txtPerfil',
			id: 'txtPerfil_1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'txtPerfil',
			emptyText: 'Seleccionar...',			
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,			
			minChars: 1,
			store: catalogoPerfil,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120,
			hidden : true
		}
	];
	//_________FORM QUE CONTIENE LOS ELEMENTOS QUE SE DESPLIEGAN CUANDO SE CONSULTA LA INFO DEL USUARIO__________________(VER CUENTAS)
	var fpDespliega = new Ext.form.FormPanel({
		id					: 'fpDespliega',		
		layout			: 'form',
		width				: 800,
		height: 380,
		style				: ' margin:0 auto;',
		frame				: true,
		hidden			: false,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 160,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			elementosDespliega
		],
		buttons: [		
			{
				text: 'Modificar',
				id: 'btnModificar',
				iconCls: 'modificar',
				hidden: true,
				formBind: true,				
				handler: function(boton, evento) {	
					Ext.Ajax.request({
					url: '15formaDist05ext.data.jsp',
					params: Ext.apply(fpDespliega.getForm().getValues(),{
						informacion: 'ModificarPerfil'							
					}),
					callback: modificarPerfil
				});				
				}
			}			
		]	
	});

	var elementosForma=[
			{
			width:          455,
			xtype:          'combo',
			id:             'cmbTipoAfiliado',
			name:           'status',
			hiddenName:     'status',
			fieldLabel:     '&nbsp; Tipo de Afiliado', 
			mode:           'local',
			displayField:   'descripcion',
			valueField:     'clave',
			emptyText:      'Seleccionar...',
			triggerAction:  'all',
			forceSelection: true,
			typeAhead:      true,
			minChars:       1,
			value				: '5',	
			store:          dataStatus,
			listeners:{
				select:{
					fn: function(combo){
						var valor = combo.getValue();
						creditoElec(valor);
					}
				}
			}
		},
		{
			xtype			:	'textfield',
			fieldLabel 	: 'N�mero de Nafin Electr�nico',
			name 			: 'numNafinElectro',
			id 			: 'txtNumNafinElectro',
			allowBlank	: true,
			maxLength 	: 30,
			width 		: 100,
			msgTarget 	: 'side',
			tabIndex		: 2,
			margins		: '0 20 0 0',
			regex			:/^\d{0,30}$/
		},
		{
			xtype			:	'textfield',
			fieldLabel 	: 'Nombre',
			name 			: 'nombre',
			id 			: 'txtNombre',
			allowBlank	: true,
			maxLength 	: 100,
			width 		: 100,
			msgTarget 	: 'side',
			tabIndex		: 3,
			margins		: '0 20 0 0'
		},{
			xtype				: 'combo',
			fieldLabel		: 'Estado',
			name				: 'catalogoEstadoVal',
			id					: 'cmbCatalogoEstadoVal',
			mode				: 'local',
			autoLoad			: false,
			displayField	: 'descripcion',
			emptyText		: 'Seleccionar ...',			
			valueField		: 'clave',
			hiddenName 		: 'catalogoEstadoVal',					
			forceSelection : true,
			triggerAction 	: 'all',
			typeAhead		: true,
			minChars 		: 1,
			tabIndex			: 4,
			forceSelection : true,
			store				: catalogoEstado	
		},
		{
			xtype				:	'textfield',
			fieldLabel 		: 'RFC ',
			name 				: 'claveRFC',
			id 				: 'txtClaveRFC',
			allowBlank		: true,
			maxLength 		: 15,
			width 			: 100,
			tabIndex			: 5,
			msgTarget 		: 'side',
			tabIndex			: 1,
			margins			: '0 20 0 0',
			regex:			/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,							
			regexText:		'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
								'en el formato NNN-AAMMDD-XXX donde:<br>'+
								'NNN:son las iniciales del nombre de la empresa<br>'+
								'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
								'XXX:es la homoclave'

		},
		{
			xtype				: 'combo',
			fieldLabel		: 'Cadena Productiva',
			name				: 'catalogoCadenaProductiva',
			id					: 'cmbCatalogoCadenaProductiva',
			mode				: 'local',
			autoLoad			: false,
			displayField	: 'descripcion',
			emptyText		: 'Seleccionar ...',			
			valueField		: 'clave',
			hiddenName 		: 'catalogoCadenaProductiva',					
			forceSelection : true,
			triggerAction 	: 'all',
			typeAhead		: true,
			minChars 		: 1,
			tabIndex			: 6,
			forceSelection : true,
			store				: catalogoCadenaProductiva	
		},
		{
			xtype				:	'textfield',
			fieldLabel 		: 'N�mero de Distribuidor',
			name 				: 'numDistribuidor',
			id 				: 'txtNumDistribuidor',
			allowBlank		: true,
			maxLength 		: 25,
			width 			: 100,
			msgTarget 		: 'side',
			invalidText 	:'El campo debe ser alfanum�rico',
			tabIndex			: 7,
			margins			: '0 20 0 0',
			regex				:/^[0-9a-z-A-Z_������������&.,\s]+$/
		},
		{
			xtype				:	'textfield',
			fieldLabel 		: 'N�mero de SIRAC',
			name 				: 'numSIRAC',
			id 				: 'txtNumSIRAC',
			allowBlank		: true,
			maxLength 		: 12,
			width 			: 100,
			msgTarget 		: 'side',
			tabIndex			: 8,
			margins			: '0 20 0 0',
			regex				:/^\d{0,12}$/
		},{
			xtype				: 'checkbox',
			id					: 'chkModPorPropietario',
			name				: 'modPorPropietario',
			hiddenName		: 'modPorPropietario',
			fieldLabel		: 'Modificado por Propietario ',			
			tabIndex			: 9,
			enable 			: true			
		}
	];

var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Consulta -Distribuidores',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
				
					//caracter(Ext.getCmp('txtNumDistribuidor').getValue());
					if(Ext.getCmp('txtNumDistribuidor').getValue()!=''){
						if(Ext.getCmp('cmbCatalogoCadenaProductiva').getValue()==''){
							Ext.Msg.alert('','Debe Seleccionar la Cadena Productiva');
							return;
						}	
					}
					fp.el.mask('Enviando...', 'x-mask-loading');		
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15						
						})
					});					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {				
					window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp?idMenu="+idMenu;						
					
				}
			}
		]	
		
	});
	
		var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,			
			NE.util.getEspaciador(20)
		]
	});
	
	catalogoEstado.load();
	catalogoCadenaProductiva.load();

	Ext.Ajax.request({
		url: '15formaDist05ext.data.jsp',
		params: {
			informacion: "valoresIniciales",
			idMenuP:idMenu
		},
		callback: procesaValoresIniciales
	});	
	
});