Ext.onReady(function() {
	
	/**********CRITERIOS DE BUSQUEDA ***************/		

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: '<b>Pymes con R.F.C</b>',			
				id: 'btnCaptura',					
				handler: function() {
					window.location = '15entregaAutCvebExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Pymes sin R.F.C',			
				id: 'btnConsulta',					
				handler: function() {
					window.location = '15entregaAutCveExt.jsp';
				}
			}	
		]
	});	
	
	function procesarSuccessFailureArchivoCSV(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			boton = Ext.getCmp('btnDescargar');
			boton.setIconClass('icoXls');
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	var procesarConsultaRegistros = function(store, registros, opts){

		var forma = Ext.getCmp('forma');
		forma.el.unmask();

		if (registros != null) {

			var grid  = Ext.getCmp('gridConsulta');

			if (!grid.isVisible()) {
				grid.show();
			}

			Ext.getCmp('barraPaginacion').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnDescargar').enable();
				el.unmask();
			} else {
				Ext.getCmp('btnDescargar').disable();				
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}
	}
	
	//----------------------------------- STORE'S -------------------------------------
	
	//Catalogo para el combo de seleccion de EPO
	var catalogoEPOData = new Ext.data.JsonStore({
		id:				'catalogoEPODataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15entregaAutCvebExt.data.jsp',
		baseParams:		{	informacion: 'catalogoEPO'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosData',
			url: 			'15entregaAutCvebExt.data.jsp',
			baseParams: {	informacion:	'Consultar'	},
			fields: [
				{ name: 'EPO'},
				{ name: 'NAFIN_ELECTRONICO'},
				{ name: 'PYME'},
				{ name: 'PYME_RFC'},
				{ name: 'REPRESENTANTE_LEGAL'},
				{ name: 'CG_CLAVE_USUARIO'},
				{ name: 'TELEFONO'},
				{ name: 'CELULAR'},
				{ name: 'CG_NOMBRE'},
				{ name: 'CG_EMAIL_REGISTRADO'},
				{ name: 'CG_EMAIL_PROP'},
				{ name: 'FECHA'},
				{ name: 'CS_ENTREGO_USUARIO'},
				{ name: 'CS_PUBLICACION'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null,null);
					}
				}
			}
	});
	

	
	//----------------------------------- MAQUINA DE EDOS. -------------------------------------
	
	var accionConsulta = function(estadoSiguiente, respuesta){
	
		if(  estadoSiguiente == "CONSULTAR" ){
			bandera = true;
			var fechaMin = Ext.getCmp('_fecha_carga_ini');
			var fechaMax = Ext.getCmp('_fecha_carga_fin');
			var cmb_epo = Ext.getCmp('_cmb_epo').getValue();
			var a = Ext.getCmp('_chk_pub').getValue();
			var publicacion="";
			if(a)
				publicacion = 's';
			if(fechaMin.getValue()!=""){
				var fechaDe = Ext.util.Format.date(fechaMin.getValue(),'d/m/Y');
				var fechaA = Ext.util.Format.date(fechaMax.getValue(),'d/m/Y');
				if(!isdate(fechaDe)){
					fechaMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}else if(!isdate(fechaA)){
					fechaMax.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}
			}
			
			if(bandera){
				var forma = Ext.getCmp("forma");
				forma.el.mask('Buscando...','x-mask-loading');
	
				// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
				Ext.getCmp("gridConsulta").hide();
				Ext.StoreMgr.key('registrosConsultadosData').load({
					params:	Ext.apply(fp.getForm().getValues(),
								{
									informacion: 'Consultar',
									operacion: 'Generar',
									cmb_epo:cmb_epo,
									publicacion: publicacion,
									start: 0,
									limit: 15
								}
					)
				});
			}
		}
	
	}
	
	//----------------------------------- COMPONENTES -------------------------------------
	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 1, align: 'center'},
					{header: 'DATOS DE LA PYME', colspan: 5, align: 'center'},
					{header: '', colspan: 8, align: 'center'}
				]
			]
	});
	
	//Elementos del grid de la consulta
		var grid = new Ext.grid.GridPanel({
			store: 		registrosConsultadosData,			           
			id:			'gridConsulta',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			width: 		922,
			frame: 		false,
			columns: [
					{
					header: 		'EPO',
					tooltip: 	'Nombre de la EPO',
					dataIndex: 	'EPO',
					sortable: 	true,
					align:		'left',
					resizable: 	true,
					width: 		210,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Nafin Electr�nico',
					tooltip: 	'N�mero de N@E',
					dataIndex: 	'NAFIN_ELECTRONICO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		98,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Nombre o Raz�n Social',
					tooltip: 	'Nombre o Raz�n Social',
					dataIndex: 	'PYME',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		200,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'R.F.C',
					tooltip: 	'R.F.C',
					dataIndex: 	'PYME_RFC',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		110,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Representante Legal',
					tooltip: 	'Nombre del representante legal',
					dataIndex: 	'REPRESENTANTE_LEGAL',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		180,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Clave de Usuario',
					tooltip: 	'Clave del usuario',
					dataIndex: 	'CG_CLAVE_USUARIO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		94,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Telefono',
					tooltip: 	'N�mero de telefono',
					dataIndex: 	'TELEFONO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		105,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Celular',
					tooltip: 	'N�mero celular',
					dataIndex: 	'CELULAR',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		105,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Nombre del Contacto',
					tooltip: 	'Nombre del contacto',
					dataIndex: 	'CG_NOMBRE',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Email Registrado',
					tooltip: 	'Email registrado',
					dataIndex: 	'CG_EMAIL_REGISTRADO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		100,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Email Proporcionado',
					tooltip: 	'Email proporcionado',
					dataIndex: 	'CG_EMAIL_PROP',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		112,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Fecha de Solicitud',
					tooltip: 	'Fecha de Solicitud',
					dataIndex: 	'FECHA',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		110,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Se entrego Usuario',
					tooltip: 	'Se entrego usuario',
					dataIndex: 	'CS_ENTREGO_USUARIO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		106,
					hidden: 		false,
					hideable:	false,
					renderer:	function(value,metadata,registro, rowIndex, colIndex){
										if(registro.data['CS_ENTREGO_USUARIO'] == "0" ){
											return 'No';
										}else
											return "Si"
									}
				},{
					header: 		'Tiene Publicaci�n',
					tooltip: 	'Tiene publicaci�n',
					dataIndex: 	'CS_PUBLICACION',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		102,
					hidden: 		false,
					hideable:	false,
					renderer:	function(value,metadata,registro, rowIndex, colIndex){
										if(registro.data['CS_PUBLICACION'] == "0" ){
											return 'No';
										}else
											return "Si"
									}
				}
			],bbar: {
			xtype: 			'paging',
			pageSize: 		15,
			buttonAlign: 	'left',
			id: 				'barraPaginacion',
			displayInfo: 	true,
			store: 			registrosConsultadosData,
			displayMsg: 	'{0} - {1} de {2}',
			emptyMsg: 		"No hay registros.",
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Descargar',
					width:	100,
					iconCls:	'icoXls',
					id: 		'btnDescargar',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						var cmb_epo = Ext.getCmp('_cmb_epo').getValue();
						var a = Ext.getCmp('_chk_pub').getValue();
						var publicacion="";
						if(a)
							publicacion = 's';
						Ext.Ajax.request({
								url: '15entregaAutCvebExt.data.jsp',
								params:	Ext.apply(fp.getForm().getValues(),
												{
														informacion:'ArchivoCSV',
														cmb_epo: cmb_epo,
														publicacion: publicacion
												}
								),
								callback: procesarSuccessFailureArchivoCSV
						});
					}
				},'-'
			]
		}
	});
	
	var elementosFormaConsulta = [
				{
						xtype:		'textfield',
						fieldLabel: 'Nombre de la Pyme',
						id:			'_txt_nom_pyme',
						allowBlank:	true,
						maxLength:	100,
						hidden: false,
						anchor:	'95%',
						msgTarget: 'side',
						listeners:{change:function(field, newValue){  field.setValue(newValue.toUpperCase()); 	}}
				},{
						xtype:		'textfield',
						fieldLabel: 'R.F.C',
						id:			'_txt_nom_rfc',
						allowBlank:	true,
						maxLength:	15,
						hidden: false,
						anchor:	'95%',
						msgTarget: 'side',
						regex			:/^([A-Z|a-z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
										regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
										'en el formato NNN-AAMMDD-XXX � NNNN-AAMMDD-XXX donde:<br>'+
										'NNNN:son las iniciales del nombre de la empresa<br>'+
										'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
										'XXX:es la homoclave',
						listeners:{change:function(field, newValue){  field.setValue(newValue.toUpperCase()); 	}}
				},
				{
					xtype: 'combo',
					id: '_cmb_epo',
					fieldLabel: 'EPO',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					emptyText: 'Seleccionar...',
					forceSelection : true,
					triggerAction : 'all',
					editable: true,
					typeAhead: true,
					minChars : 1,
					store: 		catalogoEPOData,
					anchor:	'95%'
				},{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de registro',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'datefield',
						//name: 'fecha_carga_ini',
						id: '_fecha_carga_ini',
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoFinFecha: '_fecha_carga_fin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'a',
						width: 18,
						margins: '0 15 0 0'
					},
					{
						xtype: 'datefield',
						//name: 'fecha_carga_fin',
						id: '_fecha_carga_fin',
						allowBlank: true,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoInicioFecha: '_fecha_carga_ini',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			},{
				xtype: 'checkbox',
				id: '_chk_pub',
				fieldLabel: 'Pymes con Publicaci�n'
			}
		];
	
	var fp = new Ext.form.FormPanel({
			id:				'forma',
			width:			570,
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			labelWidth:		150,
			items:			elementosFormaConsulta,
			monitorValid:	true,
			buttons: [
				{
				text:		'Buscar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:true,
				handler: function(boton, evento) {
									//grid.show();
									var ban = true;
									var fechaMin = Ext.getCmp('_fecha_carga_ini');
									var fechaMax = Ext.getCmp('_fecha_carga_fin');
									if (!Ext.isEmpty(fechaMin.getValue()) || !Ext.isEmpty(fechaMax.getValue()) ) {
										if(Ext.isEmpty(fechaMin.getValue()))	{
											fechaMin.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
											fechaMin.focus();
											ban=false;
											return;
										}else if (Ext.isEmpty(fechaMax.getValue())){
											fechaMax.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
											fechaMax.focus();
											ban=false;
											return;
										}
									}
									if(ban)
										accionConsulta("CONSULTAR",null);
								}//fin handler
				},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								grid.hide();
								Ext.getCmp('forma').getForm().reset();
								asignaFechas();
								//accionConsulta("LIMPIAR",null);
							}
				}
			]
		});
	
	//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	930,
		//height: 	'auto',
		align: 'center',
		items: 	[
			fpBotones,
			NE.util.getEspaciador(15),
			fp,
			NE.util.getEspaciador(15),
			grid
		]

	});	
	
	function asignaFechas(){
		var dt = new Date();
		Ext.getCmp('_fecha_carga_ini').setValue(dt);
		Ext.getCmp('_fecha_carga_fin').setValue(dt);
	}
	
	Ext.StoreMgr.key('catalogoEPODataStore').load();
	asignaFechas();

});