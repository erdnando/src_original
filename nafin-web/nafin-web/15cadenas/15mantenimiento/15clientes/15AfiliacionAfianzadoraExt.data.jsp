<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.seguridadbean.SeguException,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,	
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar = "";
if(informacion.equals("catalogoPais")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionNotIn("52", Integer.class);
		infoRegresar = cat.getJSONElementos();	
	} else if(informacion.equals("catalogoEstado"))	{
		String pais = (request.getParameter("pais") != null)?request.getParameter("pais"):"";
		CatalogoEstado cat = new CatalogoEstado();
		//cat.setTabla("comcat_version_convenio");
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre");
		cat.setClavePais(pais);
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoMunicipio"))	{
		String pais = (request.getParameter("pais") != null)?request.getParameter("pais"):"";
		String estado = (request.getParameter("estado") != null)?request.getParameter("estado"):"";
		CatalogoMunicipio cat = new CatalogoMunicipio();
		cat.setPais(pais);
		cat.setClave("IC_MUNICIPIO||'|'||CD_NOMBRE");
		cat.setDescripcion("upper(CD_NOMBRE)");
		cat.setEstado(estado);
		List elementos = cat.getListaElementos();
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar =  "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

		 
 }else if(informacion.equals("AfiliacionSave")){
			
			//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
			//Afiliacion afiliacion = afiliacionHome.create();
			Afiliacion afiliacion  = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			String razonSocial = (request.getParameter("Razon_Social") != null)?request.getParameter("Razon_Social"):"";
			String nombreComercial = (request.getParameter("Nombre_Comercial") != null)?request.getParameter("Nombre_Comercial"):"";
			String rfc = (request.getParameter("R_F_C") != null)?request.getParameter("R_F_C"):"";
			String  numeroCnsf= (request.getParameter("CNSF") != null)?request.getParameter("CNSF"):"";
			String calleYNumero        	=(request.getParameter("Calle")    != null) ?   request.getParameter("Calle") :"";
			String colonia = (request.getParameter("Colonia") != null)?request.getParameter("Colonia"):"";
			String codigoPostal = (request.getParameter("code") != null)?request.getParameter("code"):"";
			String claveMunicipio = (request.getParameter("Delegacion_o_municipio") != null)?request.getParameter("Delegacion_o_municipio"):"";
			String  numeroTelefono= (request.getParameter("telefono") != null)?request.getParameter("telefono"):"";
			String correoElectronico        	=(request.getParameter("Email_Rep_Legal")    != null) ?   request.getParameter("Email_Rep_Legal") :"";
			String numeroFax = (request.getParameter("Fax") != null)?request.getParameter("Fax"):"";
			String urlValidacion = (request.getParameter("URL") != null)?request.getParameter("URL"):"";
			String clavePais = (request.getParameter("Pais") != null)?request.getParameter("Pais"):"";
			String claveEstado = (request.getParameter("Estado") != null)?request.getParameter("Estado"):"";
			
			Afianzadora afianzadora = new Afianzadora();
			afianzadora.setRazonSocial(razonSocial);
			afianzadora.setNombreComercial(nombreComercial);
			afianzadora.setRfc(rfc);
			afianzadora.setNumeroCnsf(numeroCnsf);
			afianzadora.setUrlValidacion(urlValidacion);
			afianzadora.setCalleYNumero(calleYNumero);
			afianzadora.setColonia(colonia);
			afianzadora.setCodigoPostal(codigoPostal);
			afianzadora.setClavePais(clavePais);
			afianzadora.setClaveEstado(claveEstado);
			afianzadora.setClaveMunicipio(claveMunicipio);
			afianzadora.setNumeroTelefono(numeroTelefono);
			afianzadora.setCorreoElectronico(correoElectronico);
			afianzadora.setNumeroFax(numeroFax);
			afianzadora.setLoginUsuario(strLogin);
			//ertyUtils.copyProperties(afianzadora, afiliacionAfianzadoraForm);
			String numeroNafinElectronico = afiliacion.afiliaAfianzadora(afianzadora);
			JSONObject jsonObj;
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("numeroNafinElectronico",numeroNafinElectronico);
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("obtenerAfiliado")){
			//com.netro.seguridadbean.SeguridadHome _seguridadHome = (com.netro.seguridadbean.SeguridadHome)netropology.utilerias.ServiceLocator.getInstance().getEJBHome("SeguridadEJB", com.netro.seguridadbean.SeguridadHome.class);
			//com.netro.seguridadbean.Seguridad seguridad  = _seguridadHome.create();
			com.netro.seguridadbean.Seguridad seguridad  = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			
			String claveAfiliado = (request.getParameter("claveAfiliado") != null)?request.getParameter("claveAfiliado"):"";
			UtilUsr utilUsr = new UtilUsr();
			List cuentas = utilUsr.getUsuariosxAfiliado(claveAfiliado, "A"); //Cuentas Asociadas
			ConsUsuariosNafinSeg usuarios = new ConsUsuariosNafinSeg();
			List datos = usuarios.getDatosAfiliado("A", claveAfiliado); //Datos  del Afiliado
			List registros = new ArrayList();
			HashMap reg= new HashMap();
			String datoAfiliado="" ;
			if (datos.size()>0){
				datoAfiliado = "<center>N@E: "+(String) datos.get(0)+" - "+(String)datos.get(1)+"</center>";
			}
			boolean modificar = false; 
			if ("NAFIN".equals(strTipoUsuario) && 	!strPerfil.equals("NAFIN OPERADOR")) {
					try{
					seguridad.validaFacultad( (String) strLogin, (String) strPerfil, "PBMXAFICON", "15FORMA5", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
					}catch(SeguException segur){
						try{
						seguridad.validaFacultad( (String) strLogin, (String) strPerfil, "PBMXTAFILI", "15FORMA5", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
						}catch(SeguException segu){
							modificar=true;
						}
					}
				}
			Iterator itCuentas = cuentas.iterator();
			while(itCuentas.hasNext()){
				reg= new HashMap();
				String cuentausr=(String)itCuentas.next();
				reg.put("CUENTA",cuentausr);
				registros.add(reg);
			}
			JSONObject jsonObj;
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("registros",registros);
			jsonObj.put("MODIFICAR",new Boolean(modificar));
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("informacionUsuario")){
			CambioPerfil perfil = new  CambioPerfil();
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",  new Boolean(true));
			String mensaje ="";
			String txtLogin = (request.getParameter("txtLogin") != null) ? request.getParameter("txtLogin") : "";
			String afiliados = "A";

			HashMap catPerfil = perfil.getConsultaUsuario( txtLogin,  afiliados  );	
			HashMap infoPerfil = new HashMap();
			if(catPerfil.size()>0){
			
				List perfiles = (List)catPerfil.get("PERFILES");
			
				String lblNombreEmpresa = (String)catPerfil.get("EMPRESA");
				String lblNafinElec = (String)catPerfil.get("NAELECTRONICO");
				String lblNombre = (String)catPerfil.get("NOMBRE_USUARIO");
				String lblApellidoPaterno = (String)catPerfil.get("APELLIDO_PATERNO");
				String lblApellidoMaterno = (String)catPerfil.get("APELLIDO_MATERNO");
				String lblEmail = (String)catPerfil.get("EMAIL");
				String lblPerfilActual = (String)catPerfil.get("PERFIL_ACTUAL");
				String internacional = (String)catPerfil.get("INTERNACIONAL");
				String sTipoAfiliado = (String)catPerfil.get("TIPOAFILIADO");		
					
				infoPerfil.put("dEmpresa", lblNombreEmpresa);
				infoPerfil.put("dNE", lblNafinElec);
				infoPerfil.put("dNombre", lblNombre);
				infoPerfil.put("dPaterno", lblApellidoPaterno);
				infoPerfil.put("dMaterno", lblApellidoMaterno);
				infoPerfil.put("dEmail", lblEmail);
				infoPerfil.put("perfil", lblPerfilActual);
				
				infoPerfil.put("internacional", internacional);
				infoPerfil.put("sTipoAfiliado", sTipoAfiliado);	
				infoPerfil.put("txtNafinElectronico", lblNafinElec);	
				infoPerfil.put("dPerfil", lblPerfilActual);	
				infoPerfil.put("dClaveUsr", txtLogin);
				jsonObj.put("PERFIL",infoPerfil);
				jsonObj.put("mensaje", mensaje);			
			}else  {
				mensaje = "No se encontró el usuario con la clave especificada, por favor vuelva a intentarlo";
				jsonObj.put("mensaje", mensaje);
			}
			
				infoRegresar = jsonObj.toString();
		}else if(informacion.equals("modificaUsuario")){
			CambioPerfil perfil = new  CambioPerfil();
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",  new Boolean(true));
			String mensaje ="";
			String txtLogin = (request.getParameter("txtLogin") != null) ? request.getParameter("txtLogin") : "";
			String afiliados = "A";
			
			String txtPerfil = (request.getParameter("txtPerfil")==null)?"":request.getParameter("txtPerfil");
			String txtNafinElectronico = (request.getParameter("txtNafinElectronico")==null)?"":request.getParameter("txtNafinElectronico");
			String txtPerfilAnt = (request.getParameter("txtPerfilAnt")==null)?"":request.getParameter("txtPerfilAnt");
			String internacional = (request.getParameter("internacional")==null)?"":request.getParameter("internacional");
			String txtTipoAfiliado = (request.getParameter("sTipoAfiliado")==null)?"":request.getParameter("sTipoAfiliado");
			String clave_usuario = (String)session.getAttribute("Clave_usuario");
			String txtLoginC = (request.getParameter("txtLoginC") != null) ? request.getParameter("txtLoginC") : "";
			
			mensaje =  perfil.getConsultaUsuario( txtLoginC,  txtPerfil, txtPerfilAnt,  txtNafinElectronico ,  internacional,  txtTipoAfiliado, 	 clave_usuario ) ;
		
			jsonObj = new JSONObject();
			jsonObj.put("success",  new Boolean(true)); 	
			jsonObj.put("mensaje", mensaje);
			infoRegresar = jsonObj.toString();
		} else if(informacion.equals("catalogoPerfil") ){  
			CambioPerfil perfil = new  CambioPerfil();
			String txtLogin = (request.getParameter("txtLogin") != null) ? request.getParameter("txtLogin") : "";
			String afiliados = "A";
			
			HashMap catPerfil = perfil.getConsultaUsuario( txtLogin,  afiliados  );	
			List perfiles = (List)catPerfil.get("PERFILES");	
			Iterator it = perfiles.iterator();	
			HashMap datos = new HashMap();
			List registros  = new ArrayList();	
			while(it.hasNext()) {
				List campos = (List) it.next();
				String clave = (String) campos.get(0);
				datos = new HashMap();
				datos.put("clave", clave );
				datos.put("descripcion", clave );
				registros.add(datos);
			}
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",  new Boolean(true)); 	
			jsonObj.put("registros", registros);
			infoRegresar = jsonObj.toString();
		
		}
%>
<%=infoRegresar%>