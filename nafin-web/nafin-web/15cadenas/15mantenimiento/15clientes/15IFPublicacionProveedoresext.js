Ext.onReady(function(){
//-----------------------HANDLERS--------------------------------------
	var montoPublicacion="";


	
var procesarSuccessFailureGenerarPDF = function(opts,success,response)
{
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true)
		{
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
        
  
    function buscarDatos(){
            gridEpo.show();
                    consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
                                            operacion: 'Generar', //Generar datos para la consulta
                                            start: 0,
                                            limit: 15})
            });    
    }        
	
var procesarSuccessFailureGenerarArchivo = function(opts, success, response) 
 {
	 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	 var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
	 
	 btnGenerarArchivo.setIconClass('');
	 
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
		btnBajarArchivo.show();
		btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivo.setHandler(function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	 } else {
		 btnGenerarArchivo.enable();
		 NE.util.mostrarConnError(response,opts);
   }
  }


	

var procesarConsultaDataEpo = function(store,arrRegistros,opts){
	var fp = Ext.getCmp('forma');
	fp.el.unmask();	
	var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
	var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	var btnBajarPDF = Ext.getCmp('btnBajarPDF');
	var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
	btnBajarPDF.hide();	
	btnGenerarPDF.enable();
	btnBajarArchivo.hide();	
	btnGenerarArchivo.enable();
	btnBajarArchivo.hide();	
	if(arrRegistros!=null){
		var el = gridEpo.getGridEl();
			if(store.getTotalCount()>0){
					
				el.unmask();
			}else{
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
					el.mask('No se encontr� ning�n registro', 'x-mask');
			}
	}
}

//-----------------------STORES--------------------------
//Store moneda
var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '15IFPublicacionProveedoresext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
            {name: 'CG_RAZON_SOCIAL'},
            {name: 'PROVEEDOR'},
            {name: 'CG_RFC'},
            {name: 'NOM_CONTAC'},
            {name: 'CG_TEL'},
            {name: 'CG_EMAIL'},
            {name: 'CD_NOMBRE'},
            {name: 'TOT_DOCTOS'},
            {name: 'TOT_MN_DOCTOS'},
            {name: 'TOT_DOCTOS_MES'},
            {name: 'TOT_MN_DOCTOS_MES'},
            {name: 'CS_VOBO_IF'},
            {name: 'DF_VOBO_IF'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, { HcbMoneda: Ext.getCmp('cbMoneda').getValue(),
                                                                                    HicEPO: Ext.getCmp('icEPO').getValue(),
                                                                                    HcbEstatus: Ext.getCmp('cbEstatus').getValue()
                                                                                    });
						}	},
		load: procesarConsultaDataEpo,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaDataEpo(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catologoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15IFPublicacionProveedoresext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  
	
  //Store EPO
  
   var catalogoEPO = new Ext.data.JsonStore
  ({
	  id: 'catologoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15IFPublicacionProveedoresext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoEPODist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{		 
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });


var catalogoEstatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['S','LIBERADO'],
			['N','POR LIBERAR']
		 ]
	}) ;
  
//-----------------------COMPONENTES---------------------------------

	var elementosForma = [
		
		{
			xtype: 'combo',
			fieldLabel: 'Seleccionar EPO',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			name:'HicEPO',
			id: 'icEPO',
			mode: 'local',
			forceSelection : true,
			allowBlank: false,
			hiddenName: 'HicEPO',
			hidden: false,
			emptyText: 'Seleccionar una EPO',
			store: catalogoEPO,
			tpl: NE.util.templateMensajeCargaCombo
		},{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			name:'cbMoneda',
			id: 'cbMoneda',
			mode: 'local',
			forceSelection : true,
			allowBlank: false,
			hiddenName: 'HcbMoneda',
			hidden: false,
			emptyText: 'Seleccionar Moneda',
			store: catalogoMoneda,
			tpl: NE.util.templateMensajeCargaCombo
		},{
			xtype: 'combo',
			name:'cbEstatus',
                        id: 'cbEstatus',
                        hiddenName: 'HcbEstatus',
                        fieldLabel: 'Estatus',
			emptyText: 'Seleccionar Estatus',
                        forceSelection : false,
                        triggerAction: 'all',
                        typeAhead: true,
                        hidden: false,
                        minChars: 1,
                        mode: 'local',
                        displayField: 'descripcion',
			valueField: 'clave',
			allowBlank: true,
			store: catalogoEstatus
		}
	];

	var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		labelWidth: 130,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		  [
			 {
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento){
                                            fp.el.mask('Buscando...', 'x-mask');
                                            buscarDatos();
					}
			 },
			 {
			  xtype: 'button',
			  text: 'Limpiar',
			  name: 'btnLimpiar',
			  hidden: false,
			  iconCls: 'icoLimpiar',
			  handler: function(boton, evento) 
			  {
			   fp.getForm().reset();
			   gridEpo.hide();
			  }
			 }
		  ]
  });

	var gridEpo = new Ext.grid.GridPanel({
				id: 'gridEpo',
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				hidden: true,
				stripeRows: true,
				loadMask: true,
				height: 420,
				width: 880,
				frame: true,
				columns:[
                                            {
                                            header:'EPO',
                                            tooltip: 'EPO',
                                            sortable: true,
                                            dataIndex: 'CG_RAZON_SOCIAL',
                                            width: 220,
                                            align: 'left'
                                            },
                                            {
                                            header: 'Proveedor',
                                            tooltip: 'Proveedor',
                                            sortable: true,
                                            dataIndex: 'PROVEEDOR',
                                            width: 220,
                                            align: 'left'
                                            },
                                            {
                                            header: 'RFC',
                                            tooltip: 'RFC',
                                            sortable: true,
                                            align: 'left',
                                            dataIndex: 'CG_RFC',
                                            width: 150
                                            },
                                            {
                                            header: 'Nombre de contacto',
                                            tooltip: 'Nombre de contacto',
                                            align: 'left',
                                            sortable: true,
                                            dataIndex: 'NOM_CONTAC',
                                            width: 200
                                            },
                                            {
                                            header: 'Tel�fono contacto',
                                            tooltip: 'Tel�fono contacto',
                                            sortable: true,
                                            dataIndex: 'CG_TEL',
                                            width: 120
                                            },
                                            {
                                            header: 'Correo contacto',
                                            tooltip: 'Correo contacto',
                                            sortable: true,
                                            dataIndex: 'CG_EMAIL',
                                            width: 200
                                            },
                                            {
                                            header: 'Moneda',
                                            tooltip: 'Moneda',
                                            sortable: true,
                                            dataIndex: 'CD_NOMBRE',
                                            width: 125
                                            },
                                            {
                                            header: 'N�m. total<br/>de documentos',
                                            tooltip: 'N�m. total de documentos',
                                            sortable: true,
                                            align:'right',
                                            dataIndex: 'TOT_DOCTOS',
                                            width: 110
                                            },
                                            {
                                            align:'right',
                                            header: 'Monto total<br/>de publicaci�n',
                                            tooltip: 'Monto total de publicaci�n',
                                            sortable: true,
                                            dataIndex: 'TOT_MN_DOCTOS',
                                            width: 130,
                                            renderer: Ext.util.Format.numberRenderer('$0,0.00')
                                            },
                                            {
                                            header: 'N�m. documentos con <br/>vencimiento mes actual',
                                            tooltip: 'N�m. documentos con vencimiento mes actual',
                                            sortable: true,
                                            align:'right',
                                            dataIndex: 'TOT_DOCTOS_MES',
                                            width: 140
                                            },
                                            {
                                            align:'right',
                                            header: 'Monto vencimientos <br/>mes actual',
                                            tooltip: 'Monto vencimientos mes actual',
                                            sortable: true,
                                            dataIndex: 'TOT_MN_DOCTOS_MES',
                                            width: 130,
                                            renderer: Ext.util.Format.numberRenderer('$0,0.00')
                                            },
                                            {
                                            header: 'Estatus con el IF',
                                            tooltip: 'Estatus con el IF',
                                            sortable: true,
                                            align:'right',
                                            dataIndex: 'CS_VOBO_IF',
                                            width: 110
                                            },
                                            {
                                            header: 'Fecha de asignaci�n IF',
                                            tooltip: 'Fecha de asignaci�n IF',
                                            sortable: true,
                                            align:'center',
                                            dataIndex: 'DF_VOBO_IF',
                                            width: 140
                                            }				
				],
				bbar: ['->','-',
                                /*{
					xtype: 'paging',
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion2',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					
					items: ['->','-',*/
                                                        {
                                                                xtype: 'button',
                                                                text: 'Generar PDF',
                                                                id: 'btnGenerarPDF',
                                                                iconCls: 'icoPdf',
                                                                handler: function(boton, evento){
                                                                        boton.disable();
                                                                        boton.setIconClass('loading-indicator');
                                                                        //var cmpBarraPaginacion = Ext.getCmp("barraPaginacion2");
                                                                        Ext.Ajax.request({
                                                                                url: '15IFPublicacionProveedoresext.data.jsp',
                                                                                params: Ext.apply(fp.getForm().getValues(),{
                                                                                        informacion: 'ArchivoPDF',
                                                                                        start: 0,//cmpBarraPaginacion.cursor,
                                                                                        limit: 1000,//cmpBarraPaginacion.pageSize,
                                                                                        montoPublicacion: montoPublicacion,nomEpo:Ext.getCmp('icEPO').lastSelectionText}),
                                                                                callback: procesarSuccessFailureGenerarPDF
                                                                        });
                                                                }
								},
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarPDF',
									hidden: true
								},
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
                                                                        iconCls: 'icoXls',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '15IFPublicacionProveedoresext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV',montoPublicacion: montoPublicacion,nomEpo:Ext.getCmp('icEPO').lastSelectionText}),
											callback: procesarSuccessFailureGenerarArchivo
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivo',
									hidden: true
								},
                                                                /*,
								'-']
				}*/
                                '-']
		});

//-----------------------PRINCIPAL---------------------------------

var conenedorPrincipal = new Ext.Container
    ({
        id: 'contenedorPrincipal',
        applyTo: 'areaContenido',
        style: 'margin:0 auto;',
        width: 940,
        items: 
        [ 
            fp,
            NE.util.getEspaciador(20),
            gridEpo
        ]
    });

	catalogoEPO.load();
	catalogoMoneda.load();

});