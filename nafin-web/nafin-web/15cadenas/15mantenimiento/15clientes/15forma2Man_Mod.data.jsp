<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		com.netro.cadenas.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
	<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_mandante = (request.getParameter("ic_mandante")!=null)?request.getParameter("ic_mandante"):"";
String tipoPersona = (request.getParameter("tipoPersona")!=null)?request.getParameter("tipoPersona"):"";
String infoRegresar	= "",  cmb_pais ="", apellido_paterno ="", nombre="",  fecha_de_nacimiento="", apellido_materno ="", R_F_CF="", 
razon_Social ="", comercial="", R_F_C="";


if (tipoPersona.equals("1")) { 
			
//Persona Fisica
apellido_paterno = (request.getParameter("apellido_paterno")!=null)?request.getParameter("apellido_paterno"):"";
 nombre = (request.getParameter("nombre")!=null)?request.getParameter("nombre"):"";
fecha_de_nacimiento = (request.getParameter("fecha_de_nacimiento")!=null)?request.getParameter("fecha_de_nacimiento"):"";
apellido_materno = (request.getParameter("apellido_materno")!=null)?request.getParameter("apellido_materno"):"";
R_F_C = (request.getParameter("R_F_CF")!=null)?request.getParameter("R_F_CF"):"";
cmb_pais = (request.getParameter("cmb_pais")!=null)?request.getParameter("cmb_pais"):"24";

}else if (tipoPersona.equals("5"))  {
//persona moral
	razon_Social = (request.getParameter("razon_Social")!=null)?request.getParameter("razon_Social"):"";
	comercial = (request.getParameter("comercial")!=null)?request.getParameter("comercial"):"";
	R_F_C = (request.getParameter("R_F_C_M")!=null)?request.getParameter("R_F_C_M"):"";
}
//Domicilio
String calle = (request.getParameter("calle")!=null)?request.getParameter("calle"):"";
String estado = (request.getParameter("estado")!=null)?request.getParameter("estado"):"";
String codigo_postal = (request.getParameter("codigo_postal")!=null)?request.getParameter("codigo_postal"):"";
String telefono = (request.getParameter("telefono")!=null)?request.getParameter("telefono"):"";
String colonia = (request.getParameter("colonia")!=null)?request.getParameter("colonia"):"";
String delegacion_o_municipio = (request.getParameter("delegacion_o_municipio")!=null)?request.getParameter("delegacion_o_municipio"):"";
cmb_pais = (request.getParameter("cmb_paisM")!=null)?request.getParameter("cmb_paisM"):"24";
String fax = (request.getParameter("fax")!=null)?request.getParameter("fax"):"";

//Datos del Contacto
String Apellido_paterno_C = (request.getParameter("Apellido_paterno_C")!=null)?request.getParameter("Apellido_paterno_C"):"";
String Nombre_C = (request.getParameter("Nombre_C")!=null)?request.getParameter("Nombre_C"):"";
String Fax_C = (request.getParameter("Fax_C")!=null)?request.getParameter("Fax_C"):"";
String txt_cel_cont = (request.getParameter("txt_cel_cont")!=null)?request.getParameter("txt_cel_cont"):"";
String materno_contacto = (request.getParameter("materno_contacto")!=null)?request.getParameter("materno_contacto"):"";
String telefonoC = (request.getParameter("telefonoC")!=null)?request.getParameter("telefonoC"):"";
String email = (request.getParameter("email")!=null)?request.getParameter("email"):"";

String confirmacion_Email_C = (request.getParameter("confirmacion_Email_C")!=null)?request.getParameter("confirmacion_Email_C"):"";
String numero_de_cliente = (request.getParameter("numero_de_cliente")!=null)?request.getParameter("numero_de_cliente"):"";


JSONObject jsonObj = new JSONObject();  
JSONArray jsonArr = new JSONArray();

ConMandante paginador = new  ConMandante();

Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);


if(informacion.equals("valoresIniciales"))	{   
 
  HashMap registros = paginador.getConsultaMandante( ic_mandante );     

 jsonObj.put("success", new Boolean(true));
 jsonObj.put("Razon_Social", registros.get("Razon_Social"));	
 jsonObj.put("Apellido_paterno", registros.get("Apellido_paterno"));
 jsonObj.put("Apellido_materno", registros.get("Apellido_materno"));
 jsonObj.put("fecha_de_nacimiento", registros.get("DF_Nacimiento"));
 jsonObj.put("Nombre", registros.get("Nombre"));
 jsonObj.put("R_F_C", registros.get("R_F_C"));
 jsonObj.put("Calle", registros.get("Calle"));
 jsonObj.put("Colonia", registros.get("Colonia"));
 jsonObj.put("Pais", registros.get("Pais"));
 jsonObj.put("Estado1", registros.get("Estado1"));
 jsonObj.put("Delegacion_o_municipio", registros.get("Delegacion_o_municipio"));
 jsonObj.put("Codigo_postal", registros.get("Codigo_postal"));
 jsonObj.put("Telefono", registros.get("Telefono"));
 jsonObj.put("Fax", registros.get("Fax"));
 jsonObj.put("Apellido_paterno_C", registros.get("Apellido_paterno_C"));
 jsonObj.put("Apellido_materno_C", registros.get("Apellido_materno_C"));
 jsonObj.put("Nombre_C", registros.get("Nombre_C")); 
 jsonObj.put("Telefono_C", registros.get("Telefono_C"));
 jsonObj.put("numeroCelular", registros.get("numeroCelular"));
 jsonObj.put("Email_C", registros.get("Email_C"));
 jsonObj.put("numero_de_cliente", registros.get("Numero_de_cliente")); 
 jsonObj.put("Comercial", registros.get("Comercial"));
 jsonObj.put("Fax_C", registros.get("Fax_C"));
 jsonObj.put("Municipio", registros.get("Municipio"));
 jsonObj.put("Estado2", registros.get("Estado2"));
 jsonObj.put("tipoPersona", tipoPersona);
 jsonObj.put("confirmacion_Email_C", registros.get("confirmacion_Email_C") ); 
 jsonObj.put("ic_mandante", ic_mandante);
 jsonObj.put("fecha_de_nacimiento",  registros.get("DF_Nacimiento"));
 
 
 infoRegresar = jsonObj.toString();	          
 
 
}else   if(informacion.equals("catalogoPaisData")) {
 
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_pais");
	cat.setCampoClave("ic_pais");
	cat.setCampoDescripcion("cd_descripcion");		
	
	jsonObj = JSONObject.fromObject(cat.getJSONElementos());	
	jsonObj.put("cmb_pais", cmb_pais);
	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("catalogoEstadoData")){
		
	CatalogoEstado cat = new CatalogoEstado();
	cat.setCampoClave("ic_estado");
	cat.setCampoDescripcion("cd_nombre"); 	
	cat.setClavePais(cmb_pais);	
	cat.setOrden("ic_estado");
	
	jsonObj = JSONObject.fromObject(cat.getJSONElementos());	
	jsonObj.put("estado", estado);
	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("catalogoMunicipio"))	{

	CatalogoMunicipio cat=new CatalogoMunicipio();
	cat.setClave("ic_municipio");
	cat.setDescripcion("cd_nombre");
	cat.setPais(cmb_pais);
	cat.setEstado(estado);
	cat.setOrden("cd_nombre");
	List elementos=cat.getListaElementos();
	
	jsonArr = new JSONArray();
	Iterator it = elementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();	
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	String consulta = "{\"success\": true, \"total\": \"" + 	jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
 
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("delegacion_o_municipio", delegacion_o_municipio);
	infoRegresar = jsonObj.toString();
	   
 } else if(informacion.equals("ActualizarMandante"))	{  
 
 String reafilia = "R", mensaje ="";

 try {
	
	mensaje  = BeanAfiliacion.actualizaMandate(apellido_paterno,apellido_materno, 
												  nombre,R_F_C,calle,colonia,codigo_postal,telefono,email, fax,
												  Apellido_paterno_C,materno_contacto,Nombre_C,telefonoC,Fax_C,
												  email,confirmacion_Email_C, numero_de_cliente,cmb_pais, 
												  delegacion_o_municipio ,estado,tipoPersona, txt_cel_cont, 
												  razon_Social,ic_mandante, fecha_de_nacimiento, comercial,reafilia );			
		
		mensaje= "Los datos han sido actualizados";
		
	} catch(NafinException lexError){
		mensaje=	lexError.getMsgError();
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", mensaje);	
	infoRegresar  = jsonObj.toString();	
}

%>
<%=infoRegresar%>

