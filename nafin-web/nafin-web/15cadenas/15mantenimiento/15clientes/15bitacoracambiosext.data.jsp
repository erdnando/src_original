<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.exception.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.descuento.*,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoCargaGrupo,
		com.netro.model.catalogos.CatalogoPantallaBitacora,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if(informacion.equals("catalogoBancoFondeo") ) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_banco_fondeo");
	cat.setCampoClave("ic_banco_fondeo");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setOrden("ic_banco_fondeo");

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("CargaNom_Nae")){
	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion afiliacion = afiliacionHome.create();
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	
	String cad = "";
	String cmb_afiliado		=	(request.getParameter("cve_Afi")==null)?"":request.getParameter("cve_Afi");
	String txt_ne			=	(request.getParameter("num_Nae") == null) ? "":request.getParameter("num_Nae");
	
	Registros registros = afiliacion.getNumeros(cmb_afiliado, "", "", txt_ne, "");
	
	while (registros.next()){
		cad = registros.getString("DESCRIPCION");
	}
	
	System.out.println("\nCad. "+cad);
	
	JSONObject jsonObj = new JSONObject();
	if(!cad.equals("")){
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("nom_Nae", cad);
	}else
		jsonObj.put("success", new Boolean(false));
		
	infoRegresar = jsonObj.toString();
	
}else if (	informacion.equals("CatalogoBuscaAvanzada") ) {
	
	String cmb_afiliado			=	(request.getParameter("hid_ic_pyme")==null)?"":request.getParameter("hid_ic_pyme");
	String cmb_banco_fondeo				=	(request.getParameter("hid_ic_banco") == null) ? "":request.getParameter("hid_ic_banco");
	String rfc_prov = request.getParameter("rfc_prov")==null?"":request.getParameter("rfc_prov");
	String txt_ne			=	(request.getParameter("num_pyme") == null) ? "": request.getParameter("num_pyme");
	String txt_nombre			=	(request.getParameter("nombre_pyme") == null) ? "": request.getParameter("nombre_pyme");
	
	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion afiliacion = afiliacionHome.create();
	
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	
	System.out.println("El valor de cmb_afiliado enviado es:"+ cmb_afiliado);
	System.out.println("El valor de cmb_banco_fondeo enviado es:"+ cmb_banco_fondeo);
	System.out.println("El valor de rfc_prov enviado es:"+ rfc_prov);
	System.out.println("El valor de txt_ne enviado es:"+ txt_ne);
	System.out.println("El valor de txt_nombre enviado es:"+ txt_nombre);
	Registros registros = afiliacion.getNumeros(cmb_afiliado, txt_nombre, rfc_prov, txt_ne, cmb_banco_fondeo);

	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (registros.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(registros.getString("CLAVE"));
		elementoCatalogo.setDescripcion(registros.getString("CLAVE")+" "+registros.getString("DESCRIPCION"));
		coleccionElementos.add(elementoCatalogo);
	}
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("CatalogoGrupo")){
	System.out.println("Se inicia la consulta de grupos");
	CatalogoCargaGrupo cat = new CatalogoCargaGrupo();
	cat.setClave("ic_grupo_epo");
	cat.setDescripcion("cg_descripcion");
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();
}
else if(informacion.equals("catalogoPantalla")){
	System.out.println("Se inicia la consulta de pantallas");
	String icPantalla		=	(request.getParameter("icPantalla") == null)?"":request.getParameter("icPantalla");
	CatalogoPantallaBitacora cat = new CatalogoPantallaBitacora();
   cat.setCampoClave("cc_pantalla_bitacora");
   cat.setCampoDescripcion("cg_descripcion");
	cat.setClaveAfiliacion(icPantalla);
	infoRegresar = cat.getJSONElementos();
}
else if (informacion.equals("Consultar") ||  informacion.equals("ArchivoXPDF")	||  informacion.equals("ArchivoCSV")) {

	int start = 0;
	int limit = 0;

	String operacion					=	(request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	String cmb_afiliado			=	(request.getParameter("claveAfiliado")==null)?"":request.getParameter("claveAfiliado");
	String cmb_banco_fondeo						=	(request.getParameter("ic_banco_fondeo") == null) ? "":request.getParameter("ic_banco_fondeo");
	String cmb_grupo						=	(request.getParameter("ic_grupo") == null) ? "": request.getParameter("ic_grupo");
	String txt_ne						=	(request.getParameter("txt_ne") == null) ? "": request.getParameter("txt_ne");
	String txt_nombre						=	(request.getParameter("txt_nombre") == null) ? "": request.getParameter("txt_nombre");
	String cmb_pantalla						=	(request.getParameter("icPantalla") == null) ? "": request.getParameter("icPantalla");
	String txt_usuario						=	(request.getParameter("txt_usuario") == null) ? "": request.getParameter("txt_usuario");
	String fecha_carga_ini =	(request.getParameter("fecha_carga_ini") == null) ? "": request.getParameter("fecha_carga_ini");
	String fecha_carga_fin =	(request.getParameter("fecha_carga_fin") == null) ? "" : request.getParameter("fecha_carga_fin");
	
	String pantalla = request.getParameter("pantalla")==null?"":request.getParameter("pantalla");//SE AGREGA POR FODEA 057-2010
	String rfc_prov = request.getParameter("rfc_prov")==null?"":request.getParameter("rfc_prov").toUpperCase();//SE AGREGA POR FODEA 057-2010

	String numeroProveedor 	= (request.getParameter("numeroProveedor")==null)?"":request.getParameter("numeroProveedor"); 	// Fodea 057 - 2010.
	String claveEpo 			= (request.getParameter("claveEpo")			==null)?"":request.getParameter("claveEpo"); 			// Fodea 057 - 2010.
	claveEpo			= "EPO".equals(strTipoUsuario)?iNoCliente:"";
	numeroProveedor 	= numeroProveedor.trim(); // Fodea 057 - 2010
	claveEpo 			= claveEpo.trim();			// Fodea 057 - 2010

	BitCambiosNafin paginador = new BitCambiosNafin();
	paginador.setClaveAfiliado(cmb_afiliado);
	paginador.setCmbPantalla(cmb_pantalla);
	paginador.setBancoFondeo(cmb_banco_fondeo);
	paginador.setGrupo(cmb_grupo);
	paginador.setClaveEpo(claveEpo);
	paginador.setNoNafin(txt_ne);
	paginador.setUsuario(txt_usuario);
	paginador.setRfc(rfc_prov);
	paginador.setFechaInicial(fecha_carga_ini);
	paginador.setFechaFinal(fecha_carga_fin);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if(informacion.equals("Consultar")){
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));

		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}

			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("ArchivoXPDF")) {
		
		System.out.println("****SE GENERARA EL ARCHIVO .PDF*****");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
			//String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");			
			//String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}

	} else if (informacion.equals("ArchivoPDF")) {

		try {

			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}

	} else if (informacion.equals("ArchivoCSV")) {
		System.out.println("****SE GENERARA EL ARCHIVO .CSV*****");
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}

} else if (informacion.equals("ArchivoPDFBajaProv")) {
		System.out.println("****SE GENERARA EL ARCHIVO .CSV*****");
    String ic_cambio =	(request.getParameter("ic_cambio") == null) ? "" : request.getParameter("ic_cambio");
		try {
			//String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	
      //IMantenimientoHome mantenimientoHome = (IMantenimientoHome)ServiceLocator.getInstance().getEJBHome("MantenimientoEJB", IMantenimientoHome.class);
    	//IMantenimiento mantenimiento = mantenimientoHome.create();
    	IMantenimiento mantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB",IMantenimiento.class);
      
      String nombreArchivo =  mantenimiento.getDescargaArchivoBajaProv(strDirectorioTemp, ic_cambio, "PDF");

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			throw new AppException("Error en descarga del archivo PDF", e);
		}
	} else if (informacion.equals("ArchivoZIPBajaProv")) {
		System.out.println("****SE GENERARA EL ARCHIVO .CSV*****");
    String ic_cambio =	(request.getParameter("ic_cambio") == null) ? "" : request.getParameter("ic_cambio");
		try {
			//String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	
      //IMantenimientoHome mantenimientoHome = (IMantenimientoHome)ServiceLocator.getInstance().getEJBHome("MantenimientoEJB", IMantenimientoHome.class);
    	//IMantenimiento mantenimiento = mantenimientoHome.create();
    	IMantenimiento mantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB",IMantenimiento.class);
      
      String nombreArchivo =  mantenimiento.getDescargaArchivoBajaProv(strDirectorioTemp, ic_cambio, "ZIP");

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			throw new AppException("Error en descarga del archivo PDF", e);
		}
	}

%>
<%=infoRegresar%>

