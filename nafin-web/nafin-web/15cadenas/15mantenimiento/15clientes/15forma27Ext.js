Ext.onReady(function(){
	
	
	
/*--------------------------------- Handler's -------------------------------*/

	var procesarAltaUniversidad =  function(opts, success, response) {
	
		var cmpForma = Ext.getCmp('formaAfiliacion');
		cmpForma.el.unmask();
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			cmpForma.getForm().reset();
			var numero_Nafin = Ext.util.JSON.decode(response.responseText).lsNoNafinElectronico;
			if(numero_Nafin != null && numero_Nafin != ""){
				Ext.Msg.alert('Mensaje de p�gina web','Su n�mero de Nafin Electr�nico: ' + numero_Nafin,function() {
				window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
				},this);
			}
		}
		else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	

	
/*------------------------------- End Handler's -----------------------------*/
/*****************************************************************************/
 
	var procesarCatalogoPais = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('id_cmb_pais').setValue('24');
		}
	}
/*---------------------------------- Store's --------------------------------*/

	function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		}else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} 	else if (pagina == 2) { 	// Distribuidor						
			window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 		 
		} else if (pagina == 3) { 	//Proveedor
			window.location = '15forma01Ext.jsp'; 
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} else if (pagina == 6) { 	//Contragarante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 	
		} else if (pagina == 9) { 	// Mandante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
		} else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		}	else if (pagina == 11) { 	//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 	
		}	else if (pagina == 12) { 	//Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliaClienteExternoExt.jsp"; 
		}
	}
	
	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [[/*'EPO'*/  '0','Cadena Productiva']
				,[/*'IF'*/	'1','Intermediario Financiero']
				,[/*'2'*/	'2','Distribuidor']
				,[/*'1'*/	'3','Proveedor']	
				,[/*'ACE'*/	'4','Afiliados Credito Electronico']
				,[/*'EPOI'*/'5','Cadena Productiva Internacional']
				,[/*'CONTRA'*/'6','Contragarante']				
				,[/*'M'*/	'9','Mandante']	
				,[/*'AF'*/	'10','Afianzadora']	
				,[/*'UNI'*/ '11','Universidad-Programa Cr�dito Educativo'],
				['12','Cliente Externo']
				]
	});
				
	var catalogoGrupo = new Ext.data.JsonStore({
		id				: 'catGrupo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma29Ext.data.jsp',
		baseParams	: { informacion: 'catalogoGrupo'	},
		autoLoad		: false,
		listeners	: { exception: NE.util.mostrarDataProxyError }
	});	
	
	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoPais,
			exception: NE.util.mostrarDataProxyError
		}
  });
  
  // catalogo Estado
	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Municipio
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		
	//catalogo Identificacion
	var catalogoIdentificacion= new Ext.data.JsonStore
	({
		id				: 'catIdentificacion',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoIdentificacion'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});

/*--------------------------------- End Store's -----------------------------*/
/*****************************************************************************/


/*-------------------------------- Componentes ------------------------------*/
	
	var afiliacion = {
		xtype		: 'panel',
		id 		: 'afiliacion',
		layout	: 'hbox',
		border	: false,		
		width		: 900,		
		bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:110px;',
		items		: 
		[{
			layout		: 'form',
			width			:330,
			labelWidth	: 60,
			border		: false,
			items			:[{
					xtype				: 'combo',
					id					: 'id_afiliacion',
					name				: 'afiliacion',
					hiddenName 		:'afiliacion', 
					fieldLabel		: 'Afiliaci�n',
					width				: 250,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',
					value				: '11',
					store				: storeAfiliacion,
					listeners: {
						select: {
							fn: function(combo) {
								var valor  = combo.getValue();
								creditoElec(valor);		
							}
						}
					}					
				}]
			}
	]};
	

	var afiliacionPanel = {
		xtype		: 'panel',
		id 		: 'afiliacionPanel',
		items		: [
			{
				layout		: 'form',
				labelWidth	: 160,
				bodyStyle: 	'padding: 6px; padding-right:0px;padding-left:2px;',
				items		:	
				[
					{
						xtype				: 'combo',
						id          	: 'id_grupo',
						name				: 'grupo',
						hiddenName 		: 'grupo',
						fieldLabel  	: '* Grupo',
						forceSelection	: true,
						triggerAction	: 'all',
						mode				: 'local',
						typeAhead		: true,
						emptyText		:'Seleccione...',
						valueField		: 'clave',
						displayField	: 'descripcion',
						width				: 400,
						allowBlank		: false,
						margins			: '0 20 0 0',
						tabIndex			: 0,
						store				: catalogoGrupo
					},
					
					{
						xtype			: 'textfield',
						name			: 'universidad',
						id				: 'universidad',
						fieldLabel	: '* Nombre Universidad',
						maxLength	: 100,
						width			: 300,
						allowBlank	: false,
						margins		: '0 20 0 0',  //necesario para mostrar el icono de error
						tabIndex		:	1
					},
					{
						xtype 		: 'textfield',
						name  		: 'rfc',
						id    		: 'rfc',
						fieldLabel	: '* R.F.C.',
						maxLength	: 14,
						width			: 150,
						allowBlank 	: false,
						margins		: '0 20 0 0', 
						hidden		: false,
						regex			:/^([A-Z|a-z|&amp;]{3})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
						regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
						'en el formato NNN-AAMMDD-XXX donde:<br>'+
						'NNN:son las iniciales del nombre de la empresa<br>'+
						'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
						'XXX:es la homoclave',
						tabIndex		:	2
					},
					
					{
						xtype			: 'textfield',
						name			: 'campus',
						id				: 'campus',
						fieldLabel	: '* Nombre Campus',
						maxLength	: 100,
						width			: 250,
						allowBlank	: false,
						margins		: '0 20 0 0',
						tabIndex		:	3
					},
					
					{ 	xtype: 'displayfield',  value: '' }	
			]}
	]};
//---------------------------------------------------------------------------//

	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		labelWidth	: 150,
		bodyStyle: 	'padding: 6px; padding-right:0px;padding-left:2px;',
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calle',
				name			: 'calle',
				fieldLabel  : '* Calle No. Exterior y No. Interior',
				maxLength	: 100,
				width			: 250,
				margins		: '0 20 0 0',
				allowBlank	: false,
				margins		: '0 20 0 0',
				tabIndex		:	4

			},
			{
				xtype			: 'textfield',
				id          : 'code',
				name			: 'code',
				fieldLabel  : '* C�digo Postal',
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 90,
				allowBlank	: false,
				margins		: '0 20 0 0',
				tabIndex		: 6
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_estado',
				name				: 'cmb_estado',
				hiddenName 		: 'cmb_estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins			: '0 20 0 0',
				tabIndex			: 8,
				store				: catalogoEstado,
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_delegacion').reset();
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais')).getValue()
							})
						});
					}
				}
				
			},
			{
				xtype			: 'textfield',
				id          : 'telefono',
				name			: 'telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30,
				tabIndex		: 10
				
			},
			{
				xtype			: 'textfield',
				id          : 'fax',
				name			: 'fax',
				fieldLabel  : ' Fax',
				width			: 230,
				maxLength	: 30,
				allowBlank	: true,
				tabIndex		:	12
			}
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		labelWidth	: 150,
		bodyStyle: 	'padding: 6px; padding-right:0px;padding-left:2px;',	
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'colonia',
				name			: 'colonia',
				fieldLabel  : 'Colonia',
				maxLength	: 100,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 5
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais',
				name				: 'cmb_pais',
				hiddenName 		: 'cmb_pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: false,
				margins			: '0 20 0 0',
				width				: 150,
				tabIndex			: 7,
				store				: catalogoPais,
				listeners: {
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado').reset();
						Ext.getCmp('id_cmb_delegacion').reset();
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
							
							
					}
				}				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacion',
				name				: 'cmb_delegacion',
				hiddenName 		: 'cmb_delegacion',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: false,
				margins			: '0 20 0 0',
				width				: 230,
				tabIndex			: 9,
				store				: catalogoMunicipio				
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_email',
				name			: 'email',
				fieldLabel: ' * E-mail',
				allowBlank	: true,
				maxLength	: 100,
				width			: 230,
				allowBlank		: false,
				margins			: '0 20 0 0',
				tabIndex		: 11,
				vtype: 'email'
			},
			{ 	xtype: 'displayfield',  value: '' }	
		]
	};
	
	/*--____Fin Domicilio ____--*/
//---------------------------------------------------------------------------//

	/*--____ Datos del Representante Legal ____--*/
	
	var datosRepresentante_izq = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_izq',
		border	: false,
		labelWidth	: 150,
		bodyStyle: 	'padding: 6px; padding-right:0px;padding-left:2px;',	
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_legal',
				name			: 'paterno',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 13
			},
			{
				xtype			: 'textfield',
				id          : 'nombre_legal',
				name			: 'nombre',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 15
			},
			{
				xtype			: 'textfield',
				id          : 'num_iden',
				name			: 'num_iden',
				fieldLabel  : 'No. de Identificaci�n',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 17
			},
			{
				xtype			: 'textfield',
				id          : 'num_poderes',
				name			: 'poderes',
				fieldLabel  : 'Poderes y Facultades No. de Escritura',
				maxLength	: 40,
				width			: 230,
				allowBlank	: true,
				tabIndex		: 18
			}
		]
	};
		
	var datosRepresentante_der = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_der',
		border	: false,
		labelWidth	: 150,
		bodyStyle: 	'padding: 6px; padding-right:0px;padding-left:2px;',	
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_legal',
				name			: 'materno',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				width			: 230,
				allowBlank	: false,
				margins		: '0 20 0 0',
				tabIndex		: 14
			},			
			{
				xtype				: 'combo',
				id          	: 'id_cmb_identificacion',
				name				: 'cmb_identificacion',
				hiddenName 		: 'cmb_identificacion',
				fieldLabel  	: 'Identificaci�n ',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 16,
				store				: catalogoIdentificacion
			},
			
			{ 	xtype: 'displayfield',  value: '',height:20 }	,
			{
				xtype			: 'datefield',
				id          : 'fch_poder',
				name			: 'fch_poder',
				allowBlank	: false,
				width			: 130,
				fieldLabel  : '* Fecha del poder notarial',
				margins		: '0 20 0 0',
				tabIndex		: 19
			}
		]
	};
	
	/*--____Fin Datos del Representante Legal ____--*/
//---------------------------------------------------------------------------//	

	
	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			{
				layout	: 'hbox',
				title		: '',
				width		: 940,
				items		: [afiliacion ]
			},
			{
				layout	: 'hbox',
				title		: '',
				//height	: 280,
				width		: 940,
				items		: [afiliacionPanel ]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 940,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			},
			{
				layout	: 'hbox',
				title		: 'Datos del Representante Legal',
				width		: 940,
				items		: [datosRepresentante_izq, datosRepresentante_der ]
			}
		],
		monitorValid	: false,
		buttons			: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				formBind: true,
				
				handler: function(boton, evento) 
				{
					/*****************Validaciones***************/
					
					// Campos Obligatorios ---------------------------------------------------------
					var variable = Ext.getCmp('id_grupo');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del Grupo es requerido');							
						variable.focus();
						return;
					}
					var variable = Ext.getCmp('universidad');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del Nombre de Universidad es requerido');							
						variable.focus();
						return;
					}
					variable = Ext.getCmp('rfc');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del R.F.C. es requerido ' );
						variable.focus();
						return;
					}
					variable = Ext.getCmp('campus');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del Nombre del Campus es requerido ' );
						variable.focus();
						return;
					}
					
					variable = Ext.getCmp('calle');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor de la Calle y el N�mero Interior y/o Exterior  es requerido ' );
						variable.focus();
						return;
					}
					variable = Ext.getCmp('code');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del Codigo Postal es requerido ' );
						variable.focus();
						return;
					}
					variable = Ext.getCmp('id_cmb_pais');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del Pa�s  es requerido ' );
						variable.focus();
						return;
					}
					variable = Ext.getCmp('id_cmb_estado');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del Estado es requerido ' );
						variable.focus();
						return;
					}
					variable = Ext.getCmp('id_cmb_delegacion');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor de la Delegaci�n o Municipio es requerido ' );
						variable.focus();
						return;
					}
					variable = Ext.getCmp('telefono');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del Telefono es requerido ' );
						variable.focus();
						return;
					}
					variable = Ext.getCmp('id_email');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del E-mail es requerido ' );
						variable.focus();
						return;
					}
					
					variable = Ext.getCmp('id_paterno_legal');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del Apellido Paterno es requerido ' );
						variable.focus();
						return;
					}
					variable = Ext.getCmp('id_materno_legal');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del Apellido Materno es requerido ' );
						variable.focus();
						return;
					}
					variable = Ext.getCmp('nombre_legal');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor del Nombre(s) es requerido ' );
						variable.focus();
						return;
					}
					variable = Ext.getCmp('fch_poder');
					if(Ext.isEmpty(variable.getValue())){
						variable.markInvalid('El valor de la Fecha de poder notarial es requerido ' );
						variable.focus();
						return;
					}
					//*---------------------------------------------------------------------------*//
					// --Numericas -----------------------------------------------------------------
					
					var num_cod = Ext.getCmp('code');
					if (!esVacio(num_cod.getValue())){
						if (!isdigit(num_cod.getValue())){
							num_cod.markInvalid('Verifique el formato del campo C�digo Postal ' );
							num_cod.focus();
							return;
					}}		
					
					var num_tel = Ext.getCmp('telefono');
					if (!esVacio(num_tel.getValue())){//if(!Ext.isEmpty(num_tel)){
						if (!isdigit(num_tel.getValue())){
							num_tel.markInvalid('Verifique el formato del campo Tel�fono ' );
							num_tel.focus();
							return;
					}}	
					
					var num_fax = Ext.getCmp('fax');
					if (!esVacio(num_fax.getValue())){//if(!Ext.isEmpty(num_fax)){
						if (!isdigit(num_fax.getValue())){
							num_fax.markInvalid('Verifique el formato del campo Fax ' );
							num_fax.focus();
							return;
					}}		
					
					var num_pod = Ext.getCmp('num_poderes');
					if (!esVacio(num_pod.getValue())){//if(!Ext.isEmpty(num_cta)){
						if (!isdigit(num_pod.getValue())){
							num_pod.markInvalid('Verifique el formato del campo Poderes y Facultades No. de Escritura' );
							num_pod.focus();
							return;
					}}		
					
					// --FIN Numericas ---------------------------------------------------------
									
					
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							var cmpForma = Ext.getCmp('formaAfiliacion');
							var params = (cmpForma)?cmpForma.getForm().getValues():{};				
							cmpForma.el.mask("Procesando", 'x-mask-loading');
							//peticion de Registro de Afiliaci�n
							Ext.Ajax.request({
									url: '15forma27Ext.data.jsp',
									params: Ext.apply(params,{										
											informacion: 'AltaUniversidad'
									}),
									callback: procesarAltaUniversidad
							});
						}
					});		
				}
			
			},
			{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15forma27Ext.jsp';
				}
			}
		]
	});			
	
	       
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items: //fp
		[ fpDatos ]
	});

	catalogoGrupo.load();
	catalogoPais.load();
	catalogoEstado.load({ params : Ext.apply({
									pais : '24'	})
							});
	catalogoIdentificacion.load();
	

});