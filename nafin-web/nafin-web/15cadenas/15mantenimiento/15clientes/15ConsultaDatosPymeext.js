Ext.onReady(function(){
//---------------------------------VARIABLES------------------------------------
var clavePyme = "";

//----------------------------------HANDLERS------------------------------------

function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambas fechas son necesarias');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambas fechas son necesarias');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	

var procesarValidarPyme = function(opts, success, response){
	var txtNafinE = Ext.getCmp('txtNafinE');
	var txtNombre = Ext.getCmp('txtNombre');
	if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
		if (Ext.util.JSON.decode(response.responseText).nombrePyme==""){
			Ext.Msg.alert('Mensaje informativo','El nafin electronico no corresponde a una PyME afiliada a la EPO o no existe.',function(btn){  
				txtNafinE.setValue('');
				txtNombre.setValue('');
				return;	 
			});
		}else{
			var nombre = Ext.util.JSON.decode(response.responseText).nombrePyme;
			txtNombre.setValue(nombre);
			clavePyme = Ext.util.JSON.decode(response.responseText).clavePyme;	
		}
	}	
}
var procesarBusquedaAvanzada = function(store,arrRegistros,opts){
	if(arrRegistros!=null){
		if(store.getTotalCount()>0){
			if(store.getTotalCount()>1000){
				Ext.Msg.alert('Mensaje informativo', 'Demasiados registros encontrados, favor de ser m�s espec�fico en la b�squeda.', function(btn){
					return;
				});
			}else{
				return;
			}
		}else{
			Ext.Msg.alert('Mensaje informativo', 'No existe informaci�n con los criterios determinados.', function(btn){
				return;
			});
		}
	}
}
var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
	var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	btnGenerarArchivo.setIconClass('');
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		btnBajarArchivo.show();
		btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivo.setHandler( function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	} else {
		btnGenerarArchivo.enable();
		NE.util.mostrarConnError(response,opts);
	}
}
var procesarSuccessFailureGenerarXpaginaPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarXpaginaPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarXpaginaPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
}
/*var procesarSuccessFailureGenerarTotalPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarTotalPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarTotalPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
}*/
var procesarCatalogoEpo = function(store,arrRegistros,opts){
		if(arrRegistros!=null){
		var comboEpo=Ext.getCmp('icEPO');
			if(store.getTotalCount()==1){
				
				comboEpo.hide();
			}else
			comboEpo.show();
		}
		
}


var procesarConsultaData = function(store,arrRegistros,opts){
	var fp = Ext.getCmp('forma');
	fp.el.unmask();
	if(arrRegistros!=null){
		var el = grid.getGridEl();
		if(!grid.isVisible()){
			grid.show();
		}
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		var btnBajarXpaginaPDF = Ext.getCmp('btnBajarXpaginaPDF');
		var btnGenerarXpaginaPDF = Ext.getCmp('btnGenerarXpaginaPDF');

			if(store.getTotalCount()>0){
				btnGenerarXpaginaPDF.enable();
				btnBajarXpaginaPDF.hide();
				btnBajarArchivo.hide();		
				if(!btnBajarArchivo.isVisible()){
					btnGenerarArchivo.enable();
				}else{
					btnGenerarArchivo.disable();
				}
				el.unmask();
			}else{
					btnGenerarArchivo.disable();
					btnGenerarXpaginaPDF.disable();
					btnBajarXpaginaPDF.hide();
					btnBajarArchivo.hide();	
					
					el.mask('No se encontr� ning�n registro', 'x-mask');
			}
	}
}
//-----------------------------------STORE--------------------------------------
var catalogoPais = new Ext.data.JsonStore({
		id: 'catalogoPaisStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ConsultaDatosPymeext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPais'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
});
var catalogoEstado = new Ext.data.JsonStore({
		id: 'catalogoEstadoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ConsultaDatosPymeext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstado'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
});
var catalogoMunicipio = new Ext.data.JsonStore({
		id: 'catalogoMunicipioStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ConsultaDatosPymeext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMunicipio'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
});
var storeBusquedaAvanzada = new Ext.data.JsonStore({
		id: 'catalogoProveedoresDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ConsultaDatosPymeext.data.jsp',
		baseParams: {
			informacion: 'CatalogoProveedor'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarBusquedaAvanzada,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
});
var consultaData = new Ext.data.JsonStore({
	root: 'registros',
	url: '15ConsultaDatosPymeext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'CG_RAZON_SOCIAL'},
				{name: 'CG_NOMBRE_CONTACTO'},
				{name: 'CG_TELEFONO_CONTACTO'},
				{name: 'CG_EMAIL_CONTACTO'},
				{name: 'DIRECCION'},
				{name: 'FECHA_ACTUALIZACION'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var catalogoEPO = new Ext.data.JsonStore
  ({
	  id: 'catologoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ConsultaDatosPymeext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoEPO'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{		 
		load: procesarCatalogoEpo,
		
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

//--------------------------------COMPONENTE------------------------------------
	var elementosBusquedaAvanzada = [
				{
					xtype: 'textfield',
					name: 'nombrePymeIntroducido',
					id: 'txtnombre',
					fieldLabel: 'Nombre',
					allowBlank: true,
					maxLength: 100,
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'rfcIntroducido',
					id: 'txtRfc',
					fieldLabel: 'RFC',
					allowBlank: true,
					maxLength: 15,
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'numberfield',
					name: 'clavePymeIntroducida',
					id: 'txtNoProveedor',
					fieldLabel: 'N�mero Proveedor',
					allowBlank: true,
					hidden: true,
					allowDecimals: false,
					allowNegative: false,
					maxLength: 15,
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0' //Necesario para mostrar el icono de error
				},
				{
					xtype: 'panel',
					items: [
								{
									xtype: 'button',
									width: 80,
									height: 10,
									text: 'Buscar',
									iconCls: 'iconoLupa',
									id: 'btnBuscar',
									anchor: '',
									style: 'float:right',
									handler: function(boton,evento){
										storeBusquedaAvanzada.load({
												params: Ext.apply(fpBusquedaAvanzada.getForm().getValues())
										});
									}
								}
					]
				},
				NE.util.getEspaciador(20),
				{
					xtype: 'combo',
					name: 'clavePymeSelecionada',
					id: 'cmbProveedor',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccione proveedor',
					valueField: 'clave',
					hiddenName: 'proveedor',
					fieldLabel: 'Nombre',
					forceSelection: true,
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					store: storeBusquedaAvanzada,
					listeners: {
						select: {
							/*fn: function(combo){
									storeBusquedaAvanzada.load({
										params: Ext.apply(fpBusquedaAvanzada.getForm().getValues(),{
											clavePymeSelecionada: combo.getValue()
										})
									});			
							}*/
						},
						change: {
							fn: function(combo){
									var btnAceptar = Ext.getCmp('btnAceptar');
									if(combo.getValue()==''){
										btnAceptar.hide();
									}else{
										btnAceptar.show();
									} 
							}
						}
					}
				}
];
	var elementosForma = [
				{
					xtype: 'combo',
					fieldLabel: 'Seleccionar EPO',
					displayField: 'descripcion',
					valueField: 'clave',
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					name:'icEPO',
					id: 'icEPO',
					mode: 'local',
					hiddenName: 'HicEPO',
					hidden: false,
					emptyText: 'Seleccionar una EPO',
					store: catalogoEPO,
					tpl: NE.util.templateMensajeCargaCombo
			}	,{
					xtype: 'compositefield',
					fieldLabel: 'Nombre de la PYME',
					combineError: false,
					msgTarget: 'side',
					items:[
								{
									xtype: 'numberfield',
									name: 'claveNafinElectronico',
									id: 'txtNafinE',
									allowBlank: true,
									allowDecimals: false,
									allowNegative: false,
									maxLength: 15,
									width: 80,
									msgTarget: 'side',
									margins: '0 20 0 0',
									listeners: {
										'blur': function(){
										 // Petici�n b�sica  
											if(Ext.getCmp('txtNafinE').getValue()==""){
												clavePyme="";
												var txtNafinE = Ext.getCmp('txtNafinE');
												var txtNombre = Ext.getCmp('txtNombre');
												txtNafinE.setValue('');
												txtNombre.setValue('');
												return;
											}
											Ext.Ajax.request({  
												url: '15ConsultaDatosPymeext.data.jsp',  
												method: 'POST',  
												callback: procesarValidarPyme,  
												params: {  
													 informacion: 'ValidarPyme' ,
													 claveNafinElectronico: Ext.getCmp('txtNafinE').getValue()
												}  
											}); 																			
										}
									}
								},
								{
									xtype: 'textfield',
									name: 'nombrePyme',
									id: 'txtNombre',
									allowBlank: true,
									maxLength: 100,
									width: 300,
									msgTarget: 'side',
									margins: '0 20 0 0'//Necesario para mostrar el icono de error
								},
								{
									xtype: 'button',
									id: 'btnBusqueda',
									text: 'B�squeda avanzada...',
									hidden: false,
									handler: function(boton,evento){
										var ventana = Ext.getCmp('winBusqAvan');
										if(ventana){
											ventana.show();
										}else{
											new Ext.Window({
												layout: 'fit',
												width: 400,
												height: 300,
												id: 'winBusqAvan',
												closeAction: 'hide',
												items: [
													fpBusquedaAvanzada
												]
											}).show();
										}
									}
								}
					]
				},
				{
					xtype: 'combo',
					name: 'pais',
					id: 'cmbPais',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccione pa�s',
					valueField: 'clave',
					hiddenName: 'pais',
					fieldLabel: 'Pa�s',
					forceSelection: false,
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					store: catalogoPais,
					tpl: NE.util.templateMensajeCargaCombo,
					listeners: {
						select: {
							fn: function(combo){
								var cmbEstado = Ext.getCmp('cmbEstado');
								cmbEstado.setValue('');
								cmbEstado.setDisabled(false);
								cmbEstado.store.load({
									params: {
										pais: combo.getValue()
									}
								});
							}
						},
						change:{
							fn: function(combo){
								if(combo.getValue()==''){
									catalogoEstado.removeAll(true);
								}
							}
						}
					}
				},
				{
					xtype: 'combo',
					name: 'estado',
					id: 'cmbEstado',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccione estado',
					valueField: 'clave',
					hiddenName: 'estado',
					fieldLabel: 'Estado',
					forceSelection: false,
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					store: catalogoEstado,
					tpl: NE.util.templateMensajeCargaCombo,
					listeners: {
						select: {
							fn: function(combo){
								var cmbDelegacion = Ext.getCmp('cmbDelegacion');
								var cmbPais = Ext.getCmp('cmbPais');
								cmbDelegacion.setValue('');
								cmbDelegacion.setDisabled(false);
								cmbDelegacion.store.load({
									params: {
										pais: cmbPais.getValue(),
										estado: combo.getValue()
									}
								});
							}
						},
						change: {
							fn: function(combo){
								if(combo.getValue()==''){
									catalogoMunicipio.removeAll();
								}
							}
						}
					}
				},
				{
					xtype: 'combo',
					name: 'delegacion',
					id: 'cmbDelegacion',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccione...',
					valueField: 'clave',
					hiddenName: 'delegacion',
					fieldLabel: 'Delegaci�n o municipio',
					forceSelection: false,
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					store: catalogoMunicipio,
					tpl: NE.util.templateMensajeCargaCombo
				},
				{
					xtype: 'compositefield',
					fieldLabel: 'Fecha de Actualizaci�n de',
					combineErrors: false,
					msgTarget: 'side',
					items: [
					
					
								
								{
									xtype: 'datefield',
									name: 'fechaActualizacion_Min',
									id: 'fechaActualizacionMin',
									allowBlank: true,
									startDay: 0,
									width: 100,
									editable: false,
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoFinFecha: 'fechaActualizacionMax',
									margins: '0 20 0 0' //Necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaActualizacion_Max',
									id: 'fechaActualizacionMax',
									allowBlank: true,
									startDay: 1,
									width: 100,
									editable: false,
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: 'fechaActualizacionMin',
									margins: '0 20 0 0' //Necesario para mostrar el icono de error									
								}
					]
				}
	];
	var grid = new Ext.grid.GridPanel({
		id: 'grid',
		store: consultaData,
		style: 'margin:0 auto;',
		hidden: true,
		columns: [
						{
							header: 'Nombre de la Empresa',
							tooltip: 'Nombre de la Empresa',
							sortable: true,
							dataIndex: 'CG_RAZON_SOCIAL',
							width: 250							
						},
						{
							header: 'Contacto',
							tooltip: 'Contacto',
							sortable: true,
							dataIndex: 'CG_NOMBRE_CONTACTO',
							width: 150							
						},
						{
							header: 'Tel�fono',
							tooltip: 'Tel�fono',
							sortable: true,
							dataIndex: 'CG_TELEFONO_CONTACTO',
							width: 150							
						},
						{
							header: 'Correo Electr�nico',
							tooltip: 'Correo Electr�nico',
							sortable: true,
							dataIndex: 'CG_EMAIL_CONTACTO',
							width: 150							
						},
						{
							header: 'Direcci�n',
							tooltip: 'Direcci�n',
							sortable: true,
							dataIndex: 'DIRECCION',
							width: 350							
						},
						{
							header: 'Fecha de actualizaci�n',
							tooltip: 'Fecha de actualizaci�n',
							sortable: true,
							dataIndex: 'FECHA_ACTUALIZACION',
							width: 150							
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		bbar: {
					xtype: 'paging',
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consultaData,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					items: [
								'-',
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '15ConsultaDatosPymeext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV'}),
											callback: procesarSuccessFailureGenerarArchivo
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivo',
									hidden: true
								},
								'-',
								{
									xtype: 'button',
									text: 'Imprimir PDF',
									id: 'btnGenerarXpaginaPDF',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
										Ext.Ajax.request({
											url: '15ConsultaDatosPymeext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoXpaginaPDF',
												start: cmpBarraPaginacion.cursor,
												limit: cmpBarraPaginacion.pageSize
											}),
											callback: procesarSuccessFailureGenerarXpaginaPDF
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarXpaginaPDF',
									hidden: true
								},
								'-'
					]
		}
	});
	var busquedaAvanzada = ["Utilice el * para b�squeda gen�rica"];
	//Panel para mostrar el texto
	var fpB = {
		xtype: 'panel',
		id: 'forma1',
		width: 600,
		frame: true,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		defaultType: 'textfield',
		align: 'center',
		html: busquedaAvanzada.join('')
	}
	var fpBusquedaAvanzada = new Ext.form.FormPanel({
		id: 'formBusquedaAvanzada',
		width: 400,
		title: 'Busqueda Avanzada',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
					fpB,
					NE.util.getEspaciador(10),
					elementosBusquedaAvanzada
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoBuscar',
				hidden: true,
				formBind: true,
				disabled: true,
				align: 'center',
				handler: function(boton, evento){
					var txtNafinE = Ext.getCmp('txtNafinE');
					var txtNombre = Ext.getCmp('txtNombre');
					var cmbProveedor = Ext.getCmp('cmbProveedor');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbProveedor.getValue())){
						cmbProveedor.markInvalid('Seleccione el Proveedor');
						return;
					}
					var record = cmbProveedor.findRecord(cmbProveedor.valueField, cmbProveedor.getValue());
					record = record ? record.get(cmbProveedor.displayField):cmbProveedor.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nafinElectronico = a[0];
					var nombre = record.substring(a[0].length,record.length);
					txtNafinE.setValue(nafinElectronico);
					txtNombre.setValue(nombre);
					ventana.hide();
					fpBusquedaAvanzada.getForm().reset();
					storeBusquedaAvanzada.removeAll();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function(){
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusquedaAvanzada.getForm().reset();
					storeBusquedaAvanzada.removeAll();
					ventana.hide();
				}
			}
		]
	});
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title: 'Consultas Bimbo PYME',
		width: 740,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
						msgTarget: 'side',
						anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
						{
							text: 'Consultar',
							iconCls: 'icoBuscar',
							formBind: true,
							handler: function(boton,evento){
								if(!verificaFechas('fechaActualizacionMin','fechaActualizacionMax'))
								{
									return;
								}
								consultaData.load({
									params: Ext.apply(fp.getForm().getValues(),{
										operacion: 'Generar',//Generar datos para consulta
										start: 0,
										limit: 15
									})
								});								
							}
						},
						{
							text: 'Limpiar',
							iconCls: 'icoLimpiar',
							handler: function(){
								fp.getForm().reset();
								var grid = Ext.getCmp('grid');
								grid.hide();
							}
						}
		]
	});
//-----------------------------------PRINCIPAL----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
					fp,
					NE.util.getEspaciador(20),
					grid
		]
	});
	catalogoPais.load();
	catalogoEPO.load();
	
});