<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject, 
		com.netro.seguridad.*,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.File,
		java.io.FileInputStream,
		java.io.BufferedOutputStream,
		java.io.FileInputStream,
		java.io.FileOutputStream,
		java.io.InputStream,
		java.io.OutputStream,
		com.netro.parametrosgrales.*,
		com.jspsmart.upload.*,
		java.sql.*,
		com.netro.pdf.*,
		netropology.utilerias.usuarios.*,
		netropology.utilerias.caracterescontrol.*,
		org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileUploadException,
                org.apache.commons.io.FilenameUtils,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,	
		com.netro.cadenas.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String checkEContract = (request.getParameter("cargaEContract") != null)?request.getParameter("cargaEContract"):"";


String infoRegresar ="";
int start=0,limit=0;
JSONObject jsonObj 	      = new JSONObject();

	if (informacion.equals("valoresIniciales")  ) {

	jsonObj.put("success", new Boolean(true));	 
	infoRegresar = jsonObj.toString();	

	}else   if(informacion.equals("catalogoEpo")){
	if(!checkEContract.equals("true")){
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}else{
		CatalogoEpoEContract cat = new CatalogoEpoEContract();
		cat.setCampoClave("econ_cat_epos_pk");
		cat.setCampoDescripcion("vrazon_social"); 
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}
}else if (informacion.equals("catalogoFolio")){
		String epo = (request.getParameter("epo") != null)?request.getParameter("epo"):"";
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("com_carga_docto_pyme");
		cat.setCampoClave("ic_folio");
		cat.setCampoDescripcion("ic_folio");
		cat.setCampoLlave("ic_estatus_carga_pyme = 3 and ic_epo ");
		cat.setValoresCondicionIn(epo,Integer.class);
		cat.setOrden("df_fecha_alta");
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("catalogoProductos")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("ECON_CAT_PRODUCTOS");
		cat.setCampoClave("ECON_CAT_PRODUCTOS_PK");
		cat.setCampoDescripcion("VDESCRIPCION");
		cat.setCampoLlave(" CREGISTRO_ACTIVO_CK = 'S' and ECON_CAT_PRODUCTOS_PK ");
		cat.setValoresCondicionIn("1,11",Integer.class);
		cat.setOrden("VDESCRIPCION");
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("catalogoPromotoria")){
		String epo = (request.getParameter("epo") != null)?request.getParameter("epo"):"";
		String producto = (request.getParameter("producto") != null)?request.getParameter("producto"):"";
		CatalogoPromotoriaECON cat = new CatalogoPromotoriaECON();
		cat.setCampoClave("econ_cat_promotoria_pk");
		cat.setCampoDescripcion("vrazon_social");
		cat.setEpo(epo);
		cat.setProducto(producto);
		cat.setOrden("vrazon_social");
		infoRegresar = cat.getJSONElementos();

}else if(informacion.equals("AfiliacionSave")){
		

		// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
 
	
	String Ruta   = (request.getParameter("ruta")== null) ? "" : request.getParameter("ruta");
	boolean		success						= true;
	String 				PATH_FILE			=	strDirectorioTemp;
	String 				linea					=	"";
	int 					numFiles				=	0;
	int 					total					=	0;
	int 					totaldoctos			=	0;
	int 					tamMaximoArch		=  1000000;
	int					numMaxRegistros 	=	2500;
	boolean 				lbOK 					= 	true;
	AccesoDB 			con 					=	new AccesoDB();
	BufferedReader 	brt					=  null;
	BufferedReader 	br						= 	null;
	String 				nombreArchivo 		= ""; // Fodea 057 - 2010
	ParametrosRequest req = null;
	List						fileItemList	= null; 
	String itemArchivo	="";
	String rutaArchivoTemporal ="", rutaArchivo ="";
	String name								="";
	String value							="";
	Hashtable hiddens = new Hashtable();
	
	// Create a factory for disk-based file items
		DiskFileItemFactory 	factory 			= new DiskFileItemFactory(DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD, new File(PATH_FILE) );
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		fileItemList = upload.parseRequest(request);
	

	try {
		
		
		//Enumeration en = myUpload.getRequest().getParameterNames();

	for(int i=0;i<fileItemList.size();i++){
		FileItem fileItem = (FileItem) fileItemList.get(i);
		name = fileItem.getFieldName();
		value = fileItem.getString();
		hiddens.put(name,value);
	}
		req = new ParametrosRequest(fileItemList);
		
		FileItem fItem = (FileItem)req.getFiles().get(0);
		itemArchivo		= (String)fItem.getName();
		InputStream archivo = fItem.getInputStream();
		rutaArchivo = PATH_FILE+ FilenameUtils.getName(itemArchivo);
		int tamanio			= (int)fItem.getSize();
		//nombreArchivo= Comunes.cadenaAleatoria(16)+ ".txt";
                nombreArchivo= itemArchivo;
		rutaArchivoTemporal = PATH_FILE + nombreArchivo;
		fItem.write(new File(rutaArchivoTemporal));
		
	} catch(Throwable t) {
			
		success		= false;
		log.error(" Cargar en memoria o disco el contenido del archivo");
		t.printStackTrace();
		throw new AppException("Problemas al Subir el Archivo."); 
			
	}	
	 ParametrosGrales paramGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
	boolean continua = true;
	String codificacionArchivo ="";
	//boolean resul_cod
	if(paramGrales.validaCodificacionArchivoHabilitado()){
		String[]  	CODIFICACION	= {""};
		CodificacionArchivo  valida_codificacion= new CodificacionArchivo();
		valida_codificacion.setProcessTxt(true);
			//codificaArchivo.setProcessZip(true);
		if(valida_codificacion.esCharsetNoSoportado(rutaArchivoTemporal,CODIFICACION)){
				codificacionArchivo = CODIFICACION[0];
				continua = false;
			
		}
	}
	
	if(continua==true){ 
	try{
	
	
		java.io.File ft = new java.io.File(rutaArchivoTemporal);
		System.out.println("ft");
		brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
	
		while((linea=brt.readLine())!=null)
			totaldoctos++;
	
		if( totaldoctos > numMaxRegistros  || totaldoctos == 0 ){ //
			if(totaldoctos == 0){
				//El archivo proporcionado est&aacute; vac&iacute;o.
			}else{
				//El archivo proporcionado tiene <%=totaldoctos registros, por lo que excede el l&iacute;mite de registros permitidos, que es <%=numMaxRegistros registros.
			}
		
		}else{
			
		String cboTipoAfiliacion 	="";
		String Epo			="";
		String folioArchivo	="";
		cboTipoAfiliacion ="";
		
		if("NAFIN".equals(strTipoUsuario)){
			Epo = hiddens.get("Epo").toString();
		 cboTipoAfiliacion = "1";
	  }
		
	
		String cboProducto			= hiddens.get("cboProducto").toString();
		String cboPromotoria		= hiddens.get("cboPromotoria").toString();
		folioArchivo		= hiddens.get("folioArchivo").toString();		
			
			jsonObj.put("nombreArchivo",nombreArchivo);
			jsonObj.put("cboProducto",cboProducto);
			jsonObj.put("cboPromotoria",cboPromotoria);
			jsonObj.put("folioArchivo",folioArchivo);
			jsonObj.put("Epo",Epo);
			jsonObj.put("cboTipoAfiliacion",cboTipoAfiliacion);			
			jsonObj.put("estadoSiguiente","VALIDA_CARACTERES_CONTROL");
						
		}
		}catch (SecurityException se){
			//El archivo excede el  tamaño
		}catch (Exception e){
			lbOK = false;
			e.printStackTrace();
			//error.append("Error:" + e.getMessage() + "\n");
			throw new NafinException("SIST0001");
		}finally{
				if(brt != null ) brt.close();
				if(br  != null ) br.close();
				//con.terminaTransaccion(lbOK);
				if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}else{
		jsonObj.put("estadoSiguiente", "MENSAJE_CODIFICACION"		);
		jsonObj.put("codificacionArchivo",codificacionArchivo	);
	}
	jsonObj.put("success",new Boolean(true));
	infoRegresar = jsonObj.toString();
	System.out.println("infoRegresar infoRegresar "+infoRegresar);
} else if (    informacion.equals("CargaMasiva.validaCaracteresControl") 				)	{
		JSONObject	resultado	= new JSONObject();
		String nombreArchivo 	= (request.getParameter("nombreArchivo")	== null)?"":request.getParameter("nombreArchivo");
		session.removeAttribute("ValidaCaracteresControl");
		BuscaCaracteresControlThread ValidaCaractCon= new BuscaCaracteresControlThread();
		
		ResumenBusqueda resCaractCon = new ResumenBusqueda();
		
		ValidaCaractCon.setRutaArchivo(strDirectorioTemp+nombreArchivo);
		ValidaCaractCon.setRegistrador(resCaractCon);
		ValidaCaractCon.setCreaArchivoDetalle(true);
		ValidaCaractCon.setDirectorioTemporal(strDirectorioTemp);
		ValidaCaractCon.setSeparadorCampo("|");
		new Thread(ValidaCaractCon).start();
		session.setAttribute("ValidaCaracteresControl", ValidaCaractCon);
	//ValidaCaractCon.getMensaje();
		resultado.put("success",new Boolean(true));
		resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_THREAD"		);
		infoRegresar = resultado.toString();
	System.out.println("TERMINO Y EMPEZO EL THREAD ");
	System.out.println("validaCaracteresControl "+infoRegresar);
	
}else if (    informacion.equals("CargaMasiva.avanceThreadCaracteres") 				)	{
		JSONObject	resultado	= new JSONObject();
		BuscaCaracteresControlThread ValidaCaractCon = (BuscaCaracteresControlThread)session.getAttribute("ValidaCaracteresControl");
		String[] mensa = {""};
		ResumenBusqueda resCaractCon = new ResumenBusqueda(); 
		int estatusThread = ValidaCaractCon.getStatus(mensa);
		System.out.println("ESTATUS "+estatusThread);
		if(estatusThread==200){
			resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_THREAD");	
		}else if(estatusThread==300 ){
			resCaractCon = (ResumenBusqueda)ValidaCaractCon.getRegistrador();
			boolean hayCaracteres = resCaractCon.hayCaracteresControl();
			if(hayCaracteres==true){
				resultado.put("estadoSiguiente", "HAY_CARACTERES_CONTROL");
				resultado.put("mns",resCaractCon.getMensaje());
				resultado.put("nombreArchivoCaractEsp",resCaractCon.getNombreArchivoDetalle());
				
			}else{
				resultado.put("estadoSiguiente", "REVISAR_VALIDACION_DOCTO");
			}
		}else if(estatusThread==400 ){
			resultado.put("estadoSiguiente", "ERROR_THREAD_CARACTERES");	
			resultado.put("mns", "Error en el archivo, favor de verificarlo");	
		}
		resultado.put("success",new Boolean(true));
		infoRegresar = resultado.toString();
		System.out.println("TERMINO Y EMPEZO EL THREAD "+infoRegresar);
	
	
}else if (informacion.equals("descargaArchivoDetalle")	){
		String nombreArchivo 	= (request.getParameter("archivo")	== null)?"":request.getParameter("archivo");
		JSONObject jsonObj3 = new JSONObject();
		jsonObj3.put("success", new Boolean(true));
		jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj3.toString();
}else if (informacion.equals("CargaMasiva.revisaValidacionDocto")	){
		String nombreArchivo 	= (request.getParameter("nombreArchivo")	== null)?"":request.getParameter("nombreArchivo");
		String cboProducto 	= (request.getParameter("cboProducto")	== null)?"":request.getParameter("cboProducto");
		String cboPromotoria 	= (request.getParameter("cboPromotoria")	== null)?"":request.getParameter("cboPromotoria");
		String valCargaNaf 	= (request.getParameter("valCargaNaf")	== null)?"":request.getParameter("valCargaNaf");
		String valCargaEco 	= (request.getParameter("valCargaEco")	== null)?"":request.getParameter("valCargaEco");
		String valCruce 	= (request.getParameter("valCruce")	== null)?"":request.getParameter("valCruce");
		String folioArchivo 	= (request.getParameter("folioArchivo")	== null)?"":request.getParameter("folioArchivo");
		String Epo 	= (request.getParameter("Epo1")	== null)?"":request.getParameter("Epo1");
		String cboTipoAfiliacion 	= (request.getParameter("cboTipoAfiliacion")	== null)?"":request.getParameter("cboTipoAfiliacion");
		
		String Ruta = strDirectorioTemp+nombreArchivo;
		
		int accion							=0;
	
		String 				query					=	"";
		String 				ses_ic_epo			=	iNoCliente;
		BufferedReader 	brt					=  null;
		BufferedReader 	br						= 	null;
		AccesoDB 			con 					=	new AccesoDB();
		boolean 				lbOK 					= 	true;

	VectorTokenizer 	vtd	 		 = null;
	Vector 				vecdet 		 = null;
	int 					iExistePD	 = 0;
	int 					resultado	 = 0;
	int 					tipo_cliente = 0;
	int 					numero_sirac = 0;
	int 					iTotalPymes  = 0;
	int 					iTotalReg    = 0;
	StringBuffer error = new StringBuffer();
	StringBuffer bien = new StringBuffer();
	StringBuffer lineabien = new StringBuffer();
	StringBuffer lineaerror = new StringBuffer();
	String 				tipoclient	 ="1";
	try{
	java.io.File f=new java.io.File(Ruta);
	br		=	new BufferedReader(new InputStreamReader(new FileInputStream(f)));

		Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);

	con.conexionDB();
	
			if(!"".equals(cboTipoAfiliacion)){
				tipoclient = cboTipoAfiliacion;		// Los parámetros son diferentes de ""
				iNoCliente = Epo;					//cuando la afiliación se hace como NAFIN
			}


		// Obtener numero unico que identifica al proceso de carga de datos
		int 			numeroProceso 			= 0;
		String 		qry 						= "SELECT SEQ_COMTMP_AFILIA_PYME_MASIVA.NEXTVAL FROM DUAL";
		ResultSet	rsNumeroProceso		= con.queryDB(qry);
		if( rsNumeroProceso.next()){
			numeroProceso	= rsNumeroProceso.getInt(1);
		}
		rsNumeroProceso.close();
		con.cierraStatement();
				
		if(valCargaNaf!=null && valCargaNaf.equals("true")){
			afiliacion.setDatosPymeMasiva(Ruta,numeroProceso,Integer.parseInt(Epo));
			accion=1;
		}

		if((valCargaEco!=null && valCargaEco.equals("true")) || (valCruce!=null && valCruce.equals("true"))){
			System.out.println("Si entro a  esta opcion valCargaEco>>>>"+valCargaEco);
			afiliacion.setDatosPymeMasivaEco(Ruta,numeroProceso,Integer.parseInt(iNoCliente));
			if(accion==1)accion=3;
			else accion=2;
		}
		session.removeAttribute("ValidaDatos");
		ValidaDatosPymeMasivaThread ValidaDatos= new ValidaDatosPymeMasivaThread();
		// Realizar la validacion mediante un thread
		ValidaDatos.setProcessID(numeroProceso);
		ValidaDatos.setNombreArchivo(nombreArchivo);// Fodea 057 - 2010
		ValidaDatos.setAccion(accion);
		ValidaDatos.setRunning(true);
		new Thread(ValidaDatos).start();
		session.setAttribute("ValidaDatos", ValidaDatos);
		System.out.println("TERMINO Y EMPEZO EL THREAD");
		// Enviar resultado de la operacion
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("estadoSiguiente", "VALIDA_THREAD");	
		infoRegresar = jsonObj.toString();
	}catch (SecurityException se){
		//El archivo excede el  tamaño
	}catch (Exception e){
		lbOK = false;
		e.printStackTrace();
		throw new NafinException("SIST0001");
	}finally{
			if(brt != null ) brt.close();
			if(br  != null ) br.close();
			if(con.hayConexionAbierta()) con.cierraConexionDB();
	}
	
}else if(informacion.equals("validaThread")) {		
		ValidaDatosPymeMasivaThread ValidaDatos = (ValidaDatosPymeMasivaThread)session.getAttribute("ValidaDatos");
		int percent=0;
		
		if (ValidaDatos.isRunning()) { 
		
		
		}
		
		if(!ValidaDatos.hasError()){
			percent = (int) ValidaDatos.getPercent();
		}
		float porcentaje = percent/100;
		
		jsonObj.put("percent",porcentaje+"");
		jsonObj.put("isCompleted",new Boolean(ValidaDatos.isCompleted()));
		jsonObj.put("hasError",new Boolean(ValidaDatos.hasError()));
		jsonObj.put("numeroRegs",ValidaDatos.getNumberOfRegisters()+"");
		jsonObj.put("numeroRegsProc",ValidaDatos.getProcessedRegisters()+"");
		jsonObj.put("processID",ValidaDatos.getProcessID()+"");
		jsonObj.put("success",new Boolean(true));
		
		infoRegresar = jsonObj.toString();
		
		if (ValidaDatos.isCompleted() && !ValidaDatos.hasError()) {

		/***PROCESO DE IDENTIFICACION DE REGISTROS FACTIBLES PARA ENVIAR A ECONTRACT***/
		AccesoDB con =	new AccesoDB();
		try{

			con.conexionDB();

			String valCruce=(request.getParameter("valCruce") != null)?request.getParameter("valCruce"):"";
			if(!valCruce.equals("") && valCruce.equals("on")){
				CallableStatement cs = null;
				System.out.println("INICIO::::Ejecucion de SP_CRUCE_NAFELEC_ECON");
				cs = con.ejecutaSP("SP_CRUCE_NAFELEC_ECON("+ValidaDatos.getProcessID()+")");
				cs.execute();
				if (cs !=null) cs.close();
				con.cierraStatement();
				System.out.println("FIN::::Ejecucion de SP_CRUCE_NAFELEC_ECON");
			}
		
		
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}
		
 }else if(informacion.equals("Resultados")) {
		ValidaDatosPymeMasivaThread ValidaDatos = (ValidaDatosPymeMasivaThread)session.getAttribute("ValidaDatos");
		
        Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
	
	HashMap mapa = afiliacion.resultadoCargaMasivaProveedores(ValidaDatos);
	
	List regsNE=new ArrayList();
	List regsECO=new ArrayList();
	HashMap	datosMap;
	datosMap=new HashMap();
	datosMap.put("CG_DESCRIPCION","N&uacute;mero de registros sin errores:");
	datosMap.put("ID_RESULTADO",mapa.get("iTotalPymesSinError"));
	datosMap.put("ID_DESCARGA","S");
	regsNE.add(datosMap);
	
	datosMap=new HashMap();
	datosMap.put("CG_DESCRIPCION","N&uacute;mero de registros con errores:");
	datosMap.put("ID_RESULTADO",mapa.get("iTotalPymesConError"));
	datosMap.put("ID_DESCARGA","C");
	regsNE.add(datosMap);
	
	datosMap=new HashMap();
	datosMap.put("CG_DESCRIPCION","Proveedores ya existentes:");
	datosMap.put("ID_RESULTADO",mapa.get("iTotalPymesPrevReg"));
	datosMap.put("ID_DESCARGA","R");
	regsNE.add(datosMap);
	
	
	//ECONTRACT
	datosMap=new HashMap();
	datosMap.put("CG_DESCRIPCION","N&uacute;mero de registros sin errores:");
	datosMap.put("ID_RESULTADO",mapa.get("iTotalPymesSinErrorEcon"));
	datosMap.put("ID_DESCARGA","S");
	regsECO.add(datosMap);
	
	datosMap=new HashMap();
	datosMap.put("CG_DESCRIPCION","N&uacute;mero de registros con errores:");
	datosMap.put("ID_RESULTADO",mapa.get("iTotalPymesConErrorEcon"));
	datosMap.put("ID_DESCARGA","C");
	regsECO.add(datosMap);
	
	String realizarCargaEcon = "S"; //FODEA N@E 034 Afiliación Masiva
	String realizarCarga = "N";
	
	if(mapa.get("hayLineasSinError").toString().equals("true")){
		realizarCarga= "S";
	}
	
	if(mapa.get("hayLineasSinErrorEcon").toString().equals("true")){
		realizarCargaEcon= "S";
	}
	
	jsonObj.put("realizarCargaEcon",realizarCargaEcon);
	jsonObj.put("realizarCarga",realizarCarga);
	
	
	jsonObj.put("regsECO",regsECO);
	jsonObj.put("regsNE",regsNE);
	jsonObj.put("iTotalPymes",mapa.get("iTotalPymes"));
	jsonObj.put("hayLineasSinError",mapa.get("hayLineasSinError"));
	jsonObj.put("hayLineasConError",mapa.get("hayLineasConError"));
	jsonObj.put("hayPymesReg",mapa.get("hayPymesReg"));
	
	jsonObj.put("hayLineasSinErrorEcon",mapa.get("hayLineasSinErrorEcon"));
	jsonObj.put("hayLineasConErrorEcon",mapa.get("hayLineasConErrorEcon"));
	
	jsonObj.put("iTotalPymesSinError",mapa.get("iTotalPymesSinError"));
	jsonObj.put("iTotalPymesConError",mapa.get("iTotalPymesConError"));
	jsonObj.put("iTotalPymesPrevReg",mapa.get("iTotalPymesPrevReg"));
	
	jsonObj.put("success",new Boolean(true));
		
	infoRegresar = jsonObj.toString();
 }else if(informacion.equals("Procesar")){
 
	ValidaDatosPymeMasivaThread ValidaDatos = (ValidaDatosPymeMasivaThread)session.getAttribute("ValidaDatos");

	
	String cboTipoAfiliacion = "1";//(request.getParameter("cboTipoAfiliacion") == null) ? "" : request.getParameter("cboTipoAfiliacion");
	String Epo = (request.getParameter("Epo") == null) ? "" : request.getParameter("Epo");
	String hayLineasConError 		= (request.getParameter("hayLineasConError") 	== null) 	? "false" : request.getParameter("hayLineasConError");
	String sTotalPymes 				= (request.getParameter("iTotalPymes") 			== null) 	? "" : request.getParameter("iTotalPymes");
	String sTotalPymesSinError 	= (request.getParameter("iTotalPymesSinError") 	== null) 	? "" : request.getParameter("iTotalPymesSinError");
	String sTotalPymesConError 	= (request.getParameter("iTotalPymesConError") 	== null) 	? "" : request.getParameter("iTotalPymesConError");
	String sTotalPymesPrevReg 		= (request.getParameter("iTotalPymesPrevReg") 	== null) 	? "" : request.getParameter("iTotalPymesPrevReg");
	
	String procesoID 					= (request.getParameter("procesoID") 				== null) 	? "" : request.getParameter("procesoID");
	
	/****variables agregadas por FODEA ? ECONTACT****/
	String valoresForma			= "";
	String cboEpo					= request.getParameter("Epo")!=null?request.getParameter("Epo"):"";
	String cboProducto			= request.getParameter("cboProducto")!=null?request.getParameter("cboProducto"):"";
	String valActualizaDat		= request.getParameter("valActualizaDat")!=null?request.getParameter("valActualizaDat"):"";
	String valDirectProm			= request.getParameter("valDirectProm")!=null?request.getParameter("valDirectProm"):"";
	String cboPromotoria			= request.getParameter("cboPromotoria")!=null?request.getParameter("cboPromotoria"):"";
	String valCruzada				= request.getParameter("valCruzada")!=null?request.getParameter("valCruzada"):"";
	String valCargaNaf			= request.getParameter("valCargaNaf")!=null?request.getParameter("valCargaNaf"):"";
	String valCargaEco			= request.getParameter("valCargaEco")!=null?request.getParameter("valCargaEco"):"";
	String valCruce				= request.getParameter("valCruce")!=null?request.getParameter("valCruce"):"";
	String realizarCarga			= request.getParameter("realizarCarga")!=null?request.getParameter("realizarCarga"):"";
	String realizarCargaEcon	= request.getParameter("realizarCargaEcon")!=null?request.getParameter("realizarCargaEcon"):"";
	String folioArchivo			= request.getParameter("folioArchivo")!=null?request.getParameter("folioArchivo"):"";
		
	if (valCruce.equals("on")){//on cambiar valor
		valCruce="cbd";
	}
	if (valCargaNaf.equals("on")){//on cambiar valor
		valCargaNaf="nae";
	}
	if (valCargaEco.equals("on")){//on cambiar valor
		valCargaEco="eco";
	}
	if (valCruzada.equals("on")){//on cambiar valor
		valCruzada="S";
	}
	if (valActualizaDat.equals("on")){//on cambiar valor
		valActualizaDat="S";
	}
	if (valDirectProm.equals("on")){//on cambiar valor
		valDirectProm="S";
	}
	
	
	valoresForma = valCargaNaf+"|";
	valoresForma += valCargaEco+"|";
	valoresForma += valCruce+"|";
	valoresForma += cboEpo+"|";
	valoresForma += cboProducto+"|";
	valoresForma += valActualizaDat+"|";
	valoresForma += valDirectProm+"|";
	valoresForma += cboPromotoria+"|";
	valoresForma += valCruzada+"|";
	valoresForma += folioArchivo+"|";
	String hayLineasConErrorEcon 		= (request.getParameter("hayLineasConErrorEcon") 	== null) 	? "false" : request.getParameter("hayLineasConErrorEcon");
	
	AccesoDB con = new AccesoDB();
	
	
	
	try{
	con.conexionDB();
	
	VectorTokenizer vtd=null; Vector vecdet=null;
	VectorTokenizer vtlinea=null; Vector vecl=null;
	List vecPintar = new ArrayList();
	List lstResumenArch = new ArrayList();
	ArrayList vecAux = null;
	int  pos = 0;
	String login="",cveusuario="",consecutivo="",password="",cvepyme="",iEdo="33",iMun="1",campo_sirac="",strTipoPersona="",iPyme="",rfc="";
	String strRazonSocial="",razon_social="",nombre="",nombre_contacto="",num_sirac="",cg_pyme_epo_interno="";
	String aceptacion="",estado="",passwdlog2="",ap_paterno="",ap_materno="",ap_pat_contacto="",ap_mat_contacto="",municipio="";
	String pais="",calle="",colonia="",cp="",telefono="",email="",fax="",tel_contacto="",fax_contacto="",email_contacto="";
	String tipoclient="1", numerosirac="";
	String lineaf = "";
	String Resultado="";
	String sImprimir = "";
	String accion = "";
	int idResumen = 0;
	
	int iTotalPymes 			= Integer.parseInt(sTotalPymes);
	int iTotalPymesSinError = Integer.parseInt(sTotalPymesSinError);
	//int iTotalPymesConError = Integer.parseInt(sTotalPymesConError);
	//int iTotalPymesPrevReg 	= Integer.parseInt(sTotalPymesPrevReg);
	
	Calendar fecActual = Calendar.getInstance();
	String dia = Integer.toString(fecActual.get(Calendar.DATE));
	String mes = Integer.toString(fecActual.get(Calendar.MONTH)+1);
	String anio = Integer.toString(fecActual.get(Calendar.YEAR));
	String errores = "";
	
	
	String iTotalPymesSinErrorEcon	= request.getParameter("iTotalPymesSinErrorEcon")!=null?request.getParameter("iTotalPymesSinErrorEcon"):"";
	String iTotalPymesConErrorEcon	= request.getParameter("iTotalPymesConErrorEcon")!=null?request.getParameter("iTotalPymesConErrorEcon"):"";
	
	if (iTotalPymesSinErrorEcon.equals("0") && !iTotalPymesConErrorEcon.equals("0")) {
	errores = "S";
	}else {
	errores = "N";
	}
	/*
	if (hayLineasConErrorEcon.equals("true") ){
		 errores = "N";
	}else {
	 errores = "S";
	}*/
	
		//System.err.println("-----------------------------------------------> nombreArchivo = <"+ValidaDatos.getNombreArchivo()+">");// Debug info
		
	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion afiliacion = afiliacionHome.create();
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	String regAMostrar = "";
	if(!valCargaNaf.equals("") && valCargaNaf.equals("nae")){
		regAMostrar = "N";
	}
		
		if("S".equals(realizarCarga) && (!valCargaNaf.equals("") && valCargaNaf.equals("nae"))){
		vecPintar = afiliacion.procesarPymeMasiva(iNoUsuario,procesoID);		
		System.out.println("cboEpo:::"+cboEpo); 
		afiliacion.bitacoraCargaPymeMasiva(cboEpo,iNoUsuario,strNombreUsuario, procesoID, strDirectorioTemp,ValidaDatos.getNombreArchivo());
	}
	if((!valCargaEco.equals("") && valCargaEco.equals("eco")) || (!valCruce.equals("") && valCruce.equals("cbd"))){
		regAMostrar = "N".equals(regAMostrar)?"A":"E";
	}
	if( "S".equals(realizarCargaEcon) && ((!valCargaEco.equals("") && valCargaEco.equals("eco")) || (!valCruce.equals("") && valCruce.equals("cbd")))){

		if(!valCargaEco.equals("") && valCargaEco.equals("eco")) accion=valCargaEco;
		if(!valCruce.equals("") && valCruce.equals("cbd")) accion=valCruce;

		lstResumenArch = afiliacion.cargaPymeMasivaEco(iNoUsuario,procesoID, valoresForma, accion, strDirectorioTemp, errores,sTotalPymes,sTotalPymesSinError,sTotalPymesConError,sTotalPymesPrevReg );
	}
	
	List lstRegPintar = afiliacion.setRegAProcCargaMasivaProv(procesoID, regAMostrar);
	
	ComunesPDF pdfDoc = new ComunesPDF();
	String nombreArchivo = "";		
	CreaArchivo archivoPDF = new CreaArchivo();
	nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
	pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		"",
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
	
		
	HashMap datosMap=new HashMap();
	List listaEcon = new ArrayList(1);
	List listaNaf = new ArrayList();
	
	
	jsonObj.put("iTotalPymes",iTotalPymes+"");
	int regsEco = 0;
	
	if(((valCargaEco != null && valCargaEco.equals("eco")) ||   (valCruce != null && valCruce.equals("cbd"))) &&   lstResumenArch!=null && lstResumenArch.size()>0){
		pdfDoc.setTable(4, 80);
		pdfDoc.setCell("Resumen Proceso de Carga Masiva Proveedores Econtract", "celda01rep", ComunesPDF.CENTER,4);
		
		pdfDoc.setCell("Número de registros del archivo:", "formas", ComunesPDF.LEFT,2);
		pdfDoc.setCell(iTotalPymes+"", "formas", ComunesPDF.LEFT,2);
		
		pdfDoc.setCell("Número de registros a ser procesados por Econtract:", "formas", ComunesPDF.LEFT,2);
		pdfDoc.setCell(lstResumenArch.get(3)+"", "formas", ComunesPDF.LEFT,2);
		
		pdfDoc.setCell("Número Proceso", "celda01rep", ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre Archivo", "celda01rep", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Carga de Archivo", "celda01rep", ComunesPDF.CENTER);
		pdfDoc.setCell("Número Folio", "celda01rep", ComunesPDF.CENTER);
		
		pdfDoc.setCell(lstResumenArch.get(0)+"", "formas", ComunesPDF.LEFT);
		pdfDoc.setCell(lstResumenArch.get(2)+"", "formas", ComunesPDF.LEFT);
		pdfDoc.setCell((dia.length()<2?"0"+dia:dia)+"/"+(mes.length()<2?"0"+mes:mes)+"/"+anio+"", "formas", ComunesPDF.LEFT);
		pdfDoc.setCell(lstResumenArch.get(6)+"", "formas", ComunesPDF.LEFT);
		
		pdfDoc.addTable();
		
		datosMap.put("PROCESO",lstResumenArch.get(0));
		datosMap.put("NOMBRE_ARCHIVO",lstResumenArch.get(2));
		datosMap.put("FECHA_CARGA",(dia.length()<2?"0"+dia:dia)+"/"+(mes.length()<2?"0"+mes:mes)+"/"+anio);
		datosMap.put("FOLIO",lstResumenArch.get(6));
		listaEcon.add(datosMap);
		
		regsEco=Integer.parseInt(lstResumenArch.get(3).toString());
		
	}
	jsonObj.put("RegsEContract",regsEco+"");
	jsonObj.put("listaEcon",listaEcon);
	jsonObj.put("iTotalPymesSinError",iTotalPymesSinError+"");
	jsonObj.put("RegsError",iTotalPymes - iTotalPymesSinError+"");
	
	
	if(valCargaNaf.equals("nae") && lstResumenArch.size()>0){ 
	
		pdfDoc.setTable(5, 100);
		pdfDoc.setCell("Resumen Proceso de Carga Masiva Proveedores N@E", "celda01rep", ComunesPDF.CENTER,5);
					
		pdfDoc.setCell("Número de registros del archivo:", "formas", ComunesPDF.LEFT,3);
		pdfDoc.setCell(iTotalPymes+"", "formas", ComunesPDF.LEFT,2);
			
		pdfDoc.setCell("Número de registros a ser procesados:", "formas", ComunesPDF.LEFT,3);
		//pdfDoc.setCell(lstResumenArch.get(3)+"", "formas", ComunesPDF.LEFT,2);
		pdfDoc.setCell(iTotalPymesSinError+"", "formas", ComunesPDF.LEFT,2);
			
		pdfDoc.setCell("Número de registros para el archivo de errores:", "formas", ComunesPDF.LEFT,3);
		pdfDoc.setCell(iTotalPymes - iTotalPymesSinError  +"", "formas", ComunesPDF.LEFT,2);
			
		pdfDoc.addTable();
				
		/*for(int x=0;x<vecPintar.size();x++){
			vecAux = (ArrayList)vecPintar.get(x);
				
				pdfDoc.setCell(vecAux.get(0)+"", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(vecAux.get(1)+"", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(vecAux.get(2)+"", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(vecAux.get(3)+"", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(vecAux.get(4)+"", "formas", ComunesPDF.LEFT);
				
				//sImprimir += vecAux.get(0)+","+vecAux.get(1)+","+vecAux.get(2)+","+vecAux.get(3)+",";//+vecAux.get(4)+","+vecAux.get(5)+","+vecAux.get(6)+",";
				datosMap = new HashMap();
				datosMap.put("PROVEEDOR",vecAux.get(0));
				datosMap.put("NAF_ELEF",vecAux.get(1));
				datosMap.put("RAZON_SOCIAL",vecAux.get(2));
				datosMap.put("RFC",vecAux.get(3));
				datosMap.put("OPERACIONES",vecAux.get(4));
				listaNaf.add(datosMap);
		}*/
		}
		
	
	int columns = 5;
	if("A".equals(regAMostrar) || "N".equals(regAMostrar)){
		columns = columns+2;
	}
	if("A".equals(regAMostrar) || "E".equals(regAMostrar)){
		columns = columns+2;
	}
	pdfDoc.setTable(columns, 100);
	
	pdfDoc.setCell("Número de proveedor", "celda01rep", ComunesPDF.CENTER);
	pdfDoc.setCell("Número Nafin Electrónico", "celda01rep", ComunesPDF.CENTER);
	pdfDoc.setCell("Nombre Completo o Razon Social", "celda01rep", ComunesPDF.CENTER);
	pdfDoc.setCell("RFC", "celda01rep", ComunesPDF.CENTER);
	pdfDoc.setCell("Operación", "celda01rep", ComunesPDF.CENTER);
	
	if("A".equals(regAMostrar) || "N".equals(regAMostrar)){
		pdfDoc.setCell("Con error N@E", "celda01rep", ComunesPDF.CENTER);
		pdfDoc.setCell("Detalle error N@E", "celda01rep", ComunesPDF.CENTER);
	}
	if("A".equals(regAMostrar) || "E".equals(regAMostrar)){
		pdfDoc.setCell("Con error EContract", "celda01rep", ComunesPDF.CENTER);
		pdfDoc.setCell("Detalle error EContract", "celda01rep", ComunesPDF.CENTER);
	}
	
	HashMap hmRegPintar = new HashMap();
	for(int x=0;x<lstRegPintar.size();x++){
		hmRegPintar = (HashMap)lstRegPintar.get(x);
			
			pdfDoc.setCell(hmRegPintar.get("PROVEEDOR")+"", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(hmRegPintar.get("NAF_ELEC")+"", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(hmRegPintar.get("RAZON_SOCIAL")+"", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(hmRegPintar.get("RFC")+"", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(hmRegPintar.get("OPERACIONES")+"", "formas", ComunesPDF.LEFT);
			if("A".equals(regAMostrar) || "N".equals(regAMostrar)){
				pdfDoc.setCell(hmRegPintar.get("CS_ERROR_NE")+"", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(hmRegPintar.get("DET_ERROR_NE")+"", "formas", ComunesPDF.LEFT);
			}
			if("A".equals(regAMostrar) || "E".equals(regAMostrar)){
				pdfDoc.setCell(hmRegPintar.get("CS_ERROR_EC")+"", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(hmRegPintar.get("DET_ERROR_EC")+"", "formas", ComunesPDF.LEFT);
			}
	}
		
	pdfDoc.addTable();
	pdfDoc.endDocument();
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("listaNaf",lstRegPintar);
	jsonObj.put("regAMostrar",regAMostrar);
	
	
	
	
	
	}finally{
			if(con.hayConexionAbierta())
					con.cierraConexionDB();
	}
	
	jsonObj.put("success",new Boolean(true));
		
	infoRegresar = jsonObj.toString();
		
		
 }


%>
<%=infoRegresar%>