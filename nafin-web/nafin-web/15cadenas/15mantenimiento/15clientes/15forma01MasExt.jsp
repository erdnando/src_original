 <!DOCTYPE html>
  <%@ page import="java.util.*, netropology.utilerias.*"	
  contentType="text/html;charset=windows-1252"
  errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf"%>

<html>
  <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
   <%@ include file="/extjs.jspf" %>
	<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
	<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
  	
	<%if(esEsquemaExtJS) {%>
		<%@ include file="/01principal/menu.jspf"%>
	<%}%>
   <script type="text/javascript" src="/nafin/00utils/NECaracteresEspeciales.js?<%=session.getId()%>"></script>
   <script type="text/javascript" src="15forma01MasExt.js?<%=session.getId()%>"></script>
	
<%if(esEsquemaExtJS) {%>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">	
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
				<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
	</body>
	 
<%}else  if(!esEsquemaExtJS) {%>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<div id='areaContenido' style="margin-left: 3px; margin-top: 3px;"></div>
		<form id='formAux' name="formAux" target='_new'></form>		
	</body>		
<%}%>

</html>

