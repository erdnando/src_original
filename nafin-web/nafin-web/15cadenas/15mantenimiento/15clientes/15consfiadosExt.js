Ext.onReady(function(){

 	function creditoElec(pagina) {
		if (pagina == 0) { //Afianzadora
			window.location  = "15forma05AfianzadoraExt.jsp"; 	
		}	else if (pagina == 1 ) { // cadena productiva
			window.location ="15consEPOext.jsp";
		}	else if (pagina == 2) { //provedores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15forma05ProveedoresExt.jsp";
		}	else if (pagina == 3) { //Provedor Internacional
			
		}	else if (pagina == 4) { // provedor sin numero
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp";
		}	else if (pagina == 5) { // Distribuidores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp";		
		}	else if (pagina == 6) { // Fiados
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp";		
		}	else if (pagina == 7) { // Intermediario financiero Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B"; 	
		} else if (pagina == 8) { // Intermediario financiero NO Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB"; 	 	
		} else if (pagina == 9) { // Intermediario financiero Bancario /No Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB"; 		 	
		} else if (pagina == 10) { // Provedor Carga Masiva EContrac
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon_ext.jsp";
		}	else if (pagina == 11) { // Afiliados a Credito Electronico
			window.location = "15forma05ext_AfCredElectronico.jsp";
		}	else if (pagina == 12) { // Cadena Productiva Internacional
			
		}	else if (pagina == 13) { // Contragarante
			
		}	else if (pagina == 14) { // Mandante
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp"; 	
		}	else if (pagina == 15) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp"; 	
		}	else if (pagina == 16) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp"; 		
		}		

	}

	var storeAfiliacion = new Ext.data.SimpleStore({
		fields: ['clave', 'afiliacion'],
		data : [
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],
			//['3','Proveedor Internacional'],
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],	
			//['12','Cadena Productiva Internacional'],
			//['13','Contragarante'],
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo'],
			['16','Cliente Externo']
			
		]
	});
	var NaE='';
//------------------------------------------------------------------------------
//-----------------------HANDDLER's---------------------------------------------

	//*****************Descarga Archivos Consulta *******************
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnImprimir').setIconClass('icoPdf');
			Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	//*******Modificar Fiado
	var procesarModificar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var icFiado = registro.get('IC_FIADO'); 
		window.location ="15consfiadosModExt.jsp?cveFiado="+icFiado;
	}
	//********MODIFICAR PERFIL
	function modificarPerfil(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert('Mensaje',info.mensaje,
				function(){
					Ext.getCmp('ProcesarCuentas').hide();
				}	
			);
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}

	//******PROCESAR CUENTAS
	function procesarInformacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			if(info.mensaje =='') {		
				Ext.getCmp('lblNombreEmpresa1').update(info.lblNombreEmpresa);
				Ext.getCmp('lblNafinElec1').update(info.lblNafinElec);
				Ext.getCmp('lblNombre1').update(info.lblNombre);
				Ext.getCmp('lblApellidoPaterno1').update(info.lblApellidoPaterno);
				Ext.getCmp('lblApellidoMaterno1').update(info.lblApellidoMaterno);
				Ext.getCmp('lblEmail1').update(info.lblEmail);
				Ext.getCmp('lblPerfilActual1').update(info.lblPerfilActual);
				Ext.getCmp('txtLogin1').setValue(info.txtLogin);
				Ext.getCmp('sTipoAfiliado').setValue(info.sTipoAfiliado);					
				
				Ext.getCmp('internacional').setValue(info.internacional);	
				Ext.getCmp('sTipoAfiliado').setValue(info.sTipoAfiliado);	
				Ext.getCmp('txtNafinElectronico').setValue(info.txtNafinElectronico);	
				Ext.getCmp('txtPerfilAnt').setValue(info.txtPerfilAnt);	
				Ext.getCmp('txtLoginC').setValue(info.txtLogin);
				Ext.getCmp('txtPerfil_1').setValue(info.txtPerfilAnt);
				
				if(info.modificar=='S')  {
					Ext.getCmp('txtPerfil_1').show();
					catalogoPerfil.load({
						params:{ 
							tipoAfiliado: info.sTipoAfiliado, 
							txtLogin:info.txtLogin
						}	
					});
			}else  {
				Ext.getCmp('txtPerfil_1').hide();
			}
				
			}else  {  		/* no hay datos */   		}
			
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
	
	var modificarCuenta = function(grid, rowIndex, colIndex, item, event) {  
		
		var registro = grid.getStore().getAt(rowIndex);
		var txtLogin = registro.get('CUENTA');  
		Ext.getCmp('btnModificar').show();
		Ext.getCmp('id_modificacion').show();
		Ext.getCmp('id_informacion').hide();
		 
		Ext.Ajax.request({
			url: '15consfiadosExt.data.jsp',
			params: {
				informacion: 'informacionUsuario',		
				txtLogin:txtLogin,
				modificar:'S'
			},
			callback:procesarInformacion 
		});
		ventanaModificarPerfil();
	}
	
	var procesaCuenta = function(grid, rowIndex, colIndex, item, event) {  
		
		var registro = grid.getStore().getAt(rowIndex);
		var txtLogin = registro.get('CUENTA');  
		 
		 Ext.getCmp('btnModificar').hide();
		 Ext.getCmp('id_modificacion').hide();
		 Ext.getCmp('id_informacion').show();
		 
		Ext.Ajax.request({
			url: '15consfiadosExt.data.jsp',
			params: {
				informacion: 'informacionUsuario',		
				txtLogin:txtLogin,
				modificar:'N'
			},
			callback:procesarInformacion 
		});
		ventanaModificarPerfil();
	}
	
	var ventanaModificarPerfil = function() {//Ventana Moficar Perfil
		var ProcesarCuentas = Ext.getCmp('ventanaModificarPerfil');
		if(ProcesarCuentas){
			ProcesarCuentas.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 700,
				height: 400,												
				resizable: false,
				closable:false,
				id: 'ventanaModificarPerfil',
				closeAction: 'destroy',
				items: [					
					fpDespliega					
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	
							xtype: 'button',	
							text: 'Salir',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerraP', 
							handler: function(){
								Ext.getCmp('ventanaModificarPerfil').hide();
							} 
						},
						'-'							
					]
				}
			}).show().setTitle('');
		}	
	}

	//************Manegador Generaci�n de la Consula
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {					
				el.unmask();
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarCSV').enable();				
			} else {		
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarCSV').disable();				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}

	function procesaInfo (opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var jsonData = Ext.util.JSON.decode(response.responseText);
			 NaE=jsonData.registros[0].IC_NAFIN_ELECTRONICO;
			var nombreEPO=jsonData.registros[0].CG_RAZON_SOCIAL;
		} else {
			NE.util.mostrarConnError(response,opts);				
		}
	}
	var procesarVerCuentas= function(grid, rowIndex, colIndex, item, event) {
	
		var registro = grid.getStore().getAt(rowIndex);
		var numFiado = registro.get('IC_FIADO');  
		var razon_social = registro.get('CG_NOMBRE'); 
		var tituloC = 'N@E:'+ NaE +' '+razon_social;
		var tituloF ='';
		Ext.Ajax.request({ //peticion par obtener el numero electronico y nombre
			url: '15consfiadosExt.data.jsp',
			params: {
				informacion: 'InfoAfiliado',		
				numFiado:numFiado
			},callback:procesaInfo,
			success:function(){
				if(NaE!=''){
					consCuentasData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ConsCuentas',
							numFiado: numFiado,
							tipoAfiliado:'F',
							tituloC:tituloC
						})
					});
					 var newG =new Ext.grid.EditorGridPanel({	
						store: consCuentasData,
						id: 'gridCuentasNew',
						margins: '20 0 0 0',		
						style: 'margin:0 auto;',
						clicksToEdit: 1,
						hidden: true,
						columns: [			
							{
								xtype:	'actioncolumn',
								header: 'Login del Usuario',
								tooltip: 'Login del Usuario',
								dataIndex: 'CUENTA',
								align: 'center',
								width: 150,
								
								renderer: function(value, metadata, record, rowindex, colindex, store) {
									return (record.get('CUENTA'));
								},
								
								items: [
									{
										getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
											this.items[0].tooltip = 'Ver';
											return 'iconoLupa';							
										}
										,handler:	procesaCuenta  
									}
								]
							},
							{
								xtype:	'actioncolumn',
								header: 'Cambio de Perfil',
								tooltip: 'Cambio de Perfil',					
								align: 'center',				
								width: 150,
								items: [
									{
										getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
											this.items[0].tooltip = 'Modificar';
											return 'modificar';							
										}
										,handler:	modificarCuenta  
									}
								]
							}
						],
						displayInfo			: true,		
						emptyMsg			: "No hay registros.",		
						loadMask			: true,
						stripeRows			: true,
						height				: 360,
						width				: 320,
						align				: 'center',
						frame				: true
						
						
					});//fin grid Cuentas new	
					new Ext.Window({//Nueva Ventana para ver el grid de las cuentas de usuario
						modal: true,
						width: 340,
						height: 380,
						modal: true,					
						//closeAction: 'destroy',
						resizable: true,
						constrain: true,
						closable:false,
						id: 'VerCuentas',
						items: [					
							//gridCuentas
							newG
						],
						bbar: {
							xtype: 'toolbar',	buttonAlign:'center',	
							buttons: ['-',
								{	xtype: 'button',	
									text: 'Cerrar',	
									iconCls: 'icoLimpiar', 	
									id: 'btnCerrar',
									handler: function(){																		
										Ext.getCmp('VerCuentas').destroy();
										//Ext.getCmp('VerCuentas').hide();
									} 
								},
								'-',
								{
									xtype: 'button',
									text: 'Imprimir',
									tooltip:	'Imprimir',
									id: 'btnImprimirC',
									iconCls: 'icoPdf',
									handler: function(boton, evento) {																
										Ext.Ajax.request({
											url: '15consfiadosExt.data.jsp',
											params:	{
												numFiado: numFiado,
												informacion: 'ImprimirConsCuentas',
												tipoAfiliado:'F',
												tituloF:tituloF,
												tituloC:tituloC											
											}					
											,callback: descargaArchivo
										});
									}
								}	
							]
						}
					}).show().setTitle(tituloC);
				}	
			}
		});
	}
	//************************  Grid de las cuentas ***********************
	var procesarConCuentasData = function(store,arrRegistros,opts){
	
		var fp = Ext.getCmp('forma');
		var info = Ext.util.JSON.decode(store.responseText);
		fp.el.unmask();
		if(arrRegistros != null){			
			var gridCuentas = Ext.getCmp('gridCuentasNew');
			if (!gridCuentas.isVisible()) {
				gridCuentas.show();
			}	
			var el = gridCuentas.getGridEl();			
			var info = store.reader.jsonData;				
			
			if(store.getTotalCount()>0){				
				Ext.getCmp('btnImprimirC').enable();
			}else{
				
				Ext.getCmp('btnImprimirC').disable();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	//***************Acci�n Borrar
	function TransmiteBorrar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);	
			Ext.MessageBox.alert('Mensaje',info.msg,
				function(){
					if(info.borrado){
						consultaData.load({	
							params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar',
								informacion: 'Consultar',
								start:0,
								limit:15						
							})
						});
					}
				}	
			);
		} else {
			NE.util.mostrarConnError(response,opts);				
		}
	}
	var procesarBorrar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var numFiado = registro.get('IC_FIADO');  
			
		Ext.Ajax.request({
			url: '15consfiadosExt.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion	: 'Borrar',
				numFiado	: numFiado
			}),
			callback: TransmiteBorrar
		});
	}
	
//-------------------------------STORES-----------------------------------------
//------------------------------------------------------------------------------
	//*******************Ver cuentas y Modificar Perfil*
	var catalogoPerfil = new Ext.data.JsonStore({
		id: 'catalogoPerfil',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: '15consfiadosExt.data.jsp',
		baseParams: {
			informacion: 'catalogoPerfil'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {				
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var consCuentasData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15consfiadosExt.data.jsp',
		baseParams: {
			informacion: 'ConsCuentas'			
		},		
		fields: [				
			{ name: 'CUENTA'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		//autoDestroy: true, // DEbug info: agregado pruebas
		listeners: {
			load: procesarConCuentasData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConCuentasData(null, null, null);					
				}
			}
		}					
	});
		
	//**************DATOS DE LA CONSULTA INICIAL
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15consfiadosExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [
			{	name: 'IC_FIADO'},
			{	name: 'CG_NOMBRE'},	
			{	name: 'CG_RFC'},
			{	name:	'CG_ESTADO'},
			{	name: 'IC_ESTRATO'},	
			{	name: 'CD_NOMBRE'},	
			{	name: 'CG_RL_TELEFONO'},
			{	name: 'DF_ALTA'},
			{	name: 'CS_ACEPTACION_TERMINOS'},
			{	name: 'CG_VERSION_TERMINOS'},
			{	name: 'DF_ACEPTA_TERMINOS'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	})
	
//---------------------------COMPONENTES----------------------------------------	

	//*******Informaci�n del Usuario a ser modificarda (Modificar perif)		
	var elementosDespliega =[
		{ 	xtype: 'textfield',  hidden: true, id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'internacional', 	value: '' },		
		{ 	xtype: 'textfield',  hidden: true,  id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtNafinElectronico', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtPerfilAnt', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtLoginC', 	value: '' },			
		{
			xtype: 'panel',
			id: 'id_informacion', 
			allowBlank: false,
			title:'INFORMACION USUARIO',			
			value: '',
			hidden: true
		},
		{
			xtype: 'panel',
			id: 'id_modificacion', 
			allowBlank: false,
			title:'CAMBIO DE PERFIL',			
			value: '',
			hidden: true
		},
		{
			xtype: 'displayfield',
			name: 'txtLogin',
			id: 'txtLogin1',
			allowBlank: false,			
			fieldLabel: 'Clave de Usuario',
			value:''
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			fieldLabel: 'Empresa',
			items: [
				{
					xtype: 'displayfield',
					name: 'lblNombreEmpresa',
					id: 'lblNombreEmpresa1',
					allowBlank: false,
					width			: 230,
					value:''
				},
				{
					xtype: 'displayfield',
					id:'muestraCol',
					value: 'Nafin Electr�nico:',
					hidden: false
				},		
				{		
					xtype: 'displayfield',
					name: 'lblNafinElec',
					id: 'lblNafinElec1',
					allowBlank: false,
					anchor:'70%',
					width			: 230,				
					value:''
				}
			]
		},	
		{
			xtype: 'displayfield',
			name: 'lblNombre',
			id: 'lblNombre1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Nombre',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblApellidoPaterno',
			id: 'lblApellidoPaterno1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Apellido Paterno',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblApellidoMaterno',
			id: 'lblApellidoMaterno1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Apellido Materno',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblEmail',
			id: 'lblEmail1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Email',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblPerfilActual',
			id: 'lblPerfilActual1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Perfil Actual',
			value:''
		},
		{
			xtype: 'combo',
			fieldLabel: 'Perfil',
			name: 'txtPerfil',
			id: 'txtPerfil_1',			
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'txtPerfil',
			emptyText: 'Seleccionar...',			
			forceSelection: true, 
			triggerAction: 'all',
			typeAhead: true,			
			minChars: 1,
			store: catalogoPerfil,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '90%',
			width: 120,
			hidden : true
		}
	];
	//**************formulario que se muestra en la ventana de modificar Perfil
	var fpDespliega = new Ext.form.FormPanel({
		id		: 'fpDespliega',		
		layout		: 'form',
		width		: 800,
		height		: 380,
		style		: ' margin:0 auto;',
		frame		: true,
		hidden		: false,
		collapsible	: false,
		titleCollapse	: false	,
		labelWidth	: 160,
		bodyStyle	: 'padding: 8px',
		defaults	: { msgTarget: 'side',anchor: '-20' },
		items		: 
		[
			elementosDespliega
		],
		buttons: [		
			{
				text: 'Modificar',
				id: 'btnModificar',
				iconCls: 'modificar',
				hidden: true,
				formBind: true,				
				handler: function(boton, evento) {	
					Ext.Ajax.request({
					url: '15consfiadosExt.data.jsp',
					params: Ext.apply(fpDespliega.getForm().getValues(),{
						informacion: 'ModifiarPerfil'							
					}),
					callback: modificarPerfil
				});				
				}
			}			
		]	
	});
		
	//**********GRID CONSULTA FIADOS
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta- Fiados',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'No. Fiado',
				tooltip: 'No. Fiado',
				dataIndex: 'IC_FIADO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Nombre o Raz�n Social',
				tooltip: 'Nombre o Raz�n Social',
				dataIndex: 'CG_NOMBRE',
				sortable: true,
				width: 300,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'CG_RFC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Estado',
				tooltip: 'Estado',
				dataIndex: 'CG_ESTADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Estrato',
				tooltip: 'Estrato',
				dataIndex: 'CD_NOMBRE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Tel�fono Representante Legal',
				tooltip: 'Tel�fono Representante Legal',
				dataIndex: 'CG_RL_TELEFONO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'Fecha de Alta',
				tooltip: 'Fecha de Alta',
				dataIndex: 'DF_ALTA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'//,
				//renderer: Ext.util.Format.dateRenderer('d/m/Y  H:i:s')
			},
			{
				header: 'Aceptaci�n t�rminos y condiciones',
				tooltip: 'Aceptaci�n t�rminos y condiciones',
				dataIndex: 'CS_ACEPTACION_TERMINOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer :function(value){
					if(value==("S")){
						return "SI";
					}else{
						return "NO";
					}
				}
				
			},
			{
				header: 'Versi�n t�rminos y condiciones',
				tooltip: 'Versi�n t�rminos y condiciones',
				dataIndex: 'CG_VERSION_TERMINOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'/*,
				renderer:function(value,metadata,registro){                                
					
				}*/
			},
			{
				header: 'Fecha de Aceptaci�n ',
				tooltip: 'Fecha de Aceptaci�n ',
				dataIndex: 'DF_ACEPTA_TERMINOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'/*,	
				renderer:function(value,metadata,registro){                                
					
				}*/
			},
			{
				xtype		: 'actioncolumn',
				header: 'Seleccionar',
				tooltip: 'Seleccionar ',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Modificar';
							return 'modificar';
						}
						,handler: procesarModificar
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[1].tooltip = 'Borrar';
							return 'borrar';
						}
						,handler: procesarBorrar
					}
					
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Cuentas de Usuario',
				tooltip: 'Cuentas de Usuario ',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}
						,handler:procesarVerCuentas
					}
				]
			}
		],	
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 400,
		width				: 900,
		align				: 'center',
		frame				: false,
		bbar: 
			{
			xtype			: 'paging',
			pageSize		: 15,
			buttonAlign	: 'left',
			id				: 'barraPaginacion',
			displayInfo	: true,
			store			: consultaData,
			displayMsg	: '{0} - {1} de {2}',
			emptyMsg		: "No hay registros.",
			items			: [
				'->','-',
				{
					xtype		: 'button',
					text		: 'Generar Archivo',					
					tooltip	:	'Generar Archivo ',
					iconCls	: 'icoXls',
					id			: 'btnGenerarCSV',
					handler	: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						boton.setDisabled(true);
						Ext.Ajax.request({
							url: '15consfiadosExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GeneraArchivoCSV'			
							}),
							success : function(response) {
								boton.setIconClass('icoXls');     
								boton.setDisabled(false);

							},
							callback: descargaArchivo
						});
					}
				},
				'-',
				{
					xtype		: 'button',
					text		: 'Imprimir',					
					tooltip	:	'Imprimir ',
					iconCls	: 'icoPdf',
					id			: 'btnImprimir',
					handler	: function(boton, evento) {
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						boton.setIconClass('loading-indicator');
						boton.setDisabled(true);
						Ext.Ajax.request({
							url: '15consfiadosExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GeneraArchivoPDF',
								start: barraPaginacion.cursor,
								limit: barraPaginacion.pageSize			
							}),
							success : function(response) {
								boton.setIconClass('icoPdf');     
								boton.setDisabled(false);

							},
							callback: descargaArchivo
						});
					}
				}				
			]
		}
	});//fin grid Consulta	
	
	//******************************GRID CUENTAS****************************
	
//FIN GRID CUENTAS

			
	//********************* Criterios de Busqueda ******************************
	var elementosForma=[
		{
			xtype				: 'combo',
			id					: 'cmbTipoAfiliado1',
			name				: 'cmbTipoAfiliado',
			hiddenName 		: 'cmbTipoAfiliado', 
			fieldLabel		: 'Tipo de Afiliado',
			width				: 250,
			forceSelection	: true,
			triggerAction	: 'all',
			mode		: 'local',
			valueField	: 'clave',
			displayField	: 'afiliacion',
			value		: '6',	
			store		: storeAfiliacion,
			tabIndex	: 1,
			listeners: {
				select: {
					fn: function(combo) {	
						var valor = Ext.getCmp('cmbTipoAfiliado1').getValue();						
						creditoElec(valor);		///funcion para redireccionar mediante un window.location valor del combo				
					}
				}
			}
		},
		{
			xtype			:	'numberfield',
			fieldLabel 	: 'N�mero de Fiado',
			name 			: 'numFiado',
			id 			: 'txtNumFiado',
			allowBlank	: true,
			maxLength 	: 8,
			width 		: 50,
			msgTarget	: 'side',
			tabIndex		: 2,
			margins		: '0 20 0 0'
		},
		{
			xtype			: 'textfield',
			fieldLabel 	: 'Nombre',
			name 			: 'nombre',
			id 			: 'txtNombre',
			allowBlank	: true,
			maxLength 	: 50,
			width 		: 100,
			msgTarget 	: 'side',
			tabIndex		: 3,
			margins		: '0 20 0 0'
		},
		{
			xtype			: 'textfield',
			fieldLabel 	: 'RFC',
			name 			: 'claveRFC',
			id 			: 'txtClaveRFC',
			allowBlank	: true,
			maxLength 	: 15,
			width 		: 100,
			tabIndex		: 4,
			msgTarget 	: 'side',
			tabIndex		: 1,
			margins		: '0 20 0 0',
			regex			:/^([A-Z|a-z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d]) | ([1-9]{2})([0][2])([0][1-9] | [12][0-8]) )-([a-zA-Z0-9]{3}$)/,
               regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
               'en el formato NNN-AAMMDD-XXX donde:<br>'+
               'NNN:son las iniciales del nombre y apellido<br>'+
               'AAMMDD: es la fecha de nacimiento menor al dia de hoy<br>'+
               'XXX:es la homoclave',
			listeners:{change:function(field, newValue){  field.setValue(newValue.toUpperCase()); 	}}		
		},
		{
			xtype			:	'textfield',
			fieldLabel 	: 'Estado',
			name 			: 'estado',
			id 			: 'txtEstado',
			allowBlank	: true,
			maxLength 	: 50, 
			width 		: 100,
			msgTarget 	: 'side',
			tabIndex		: 5,
			margins		: '0 20 0 0'
		},{
			xtype			: 'checkbox',
			id				: 'chkTermCond',
			name			: 'terminosCondiciones',
			hiddenName	: 'terminosCondiciones',
			fieldLabel	: 'T�rminos y Condiciones Aceptados  ',			
			tabIndex		: 6,
			enable 		: true			
		}
	];	

	var fp = new Ext.form.FormPanel({
		id					: 'forma',
		width				: 500,
		title				: 'Consulta -Fiados',
		frame				: true,
		collapsible		: false,
		titleCollapse	: true,
		style				: 'margin:0 auto;',
		bodyStyle		: 'padding: 6px',
		labelWidth		: 150,
		defaultType		: 'textfield',
		defaults			: {
			msgTarget	: 'side',
			anchor		: '-20'
		},
		items				: elementosForma,			
		monitorValid	: true,
		buttons			: [		
			{
				text		: 'Buscar',
				id			: 'btnBuscar',
				iconCls	: 'icoBuscar',				
				formBind	: true,				
				handler	: function(boton, evento) {	
					fp.el.mask('Enviando...', 'x-mask-loading');							
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion	: 'Generar',
							informacion	: 'Consultar',
							start			:0,
							limit			:15						
						})
					});					
				}
			},
			{
				text			: 'Limpiar',
				id				: 'limpiar',
				iconCls		: 'icoLimpiar',
				handler		: function() {
					window.location = '15consfiadosExt.jsp';					
				}
			}
		]
	});



//----------------------------------- CONTENEDOR -------------------------------------
	var contenedorPrincipal = new Ext.Container({
		id			: 'contenedorPrincipal',
		applyTo	: 'areaContenido',
		width		: 	940,
		style		: 'margin:0 auto;',
		items		: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			gridConsulta,
			NE.util.getEspaciador(10)
		]
	});
	
});