<!DOCTYPE html> 
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="java.util.*" errorPage="/00utils/error.jsp"%>
 <script language="JavaScript" src="../../../00utils/valida.js?<%=session.getId()%>"></script>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<% String version = (String)session.getAttribute("version"); %>

<html>
  <head>
    <!-- <meta http-equiv="Content-Type" content="text/html;  charset=iso-8859-1"> -->
    <title>Nafi@net - Afiliación</title>
	 <%@ include file="/extjs.jspf" %>
		<%if(version!=null){%>
		<%@ include file="/01principal/menu.jspf"%>
		<%}%>	 
	 <link rel="stylesheet" type="text/css" href="<%=appWebContextRoot%>/00utils/extjs/ux/FileUploadField.css" />
	 <script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/FileUploadField.js"></script>
	 <script type="text/javascript" src="15forma21Ext.js?<%=session.getId()%>"></script> 
  </head>
  
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%if(version!=null){%>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<%}%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%if(version!=null){%>
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<%}%>
			<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
		</div>
	</div>
	<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<%}%>
	<form id='formAux' name="formAux" target='_new'></form>
	
	
	<form id='formParametros' name="formParametros">
		<input type="hidden" id="sTipoUsuario" name=" " value="<%=strTipoUsuario%>"/>	
	</form>
		     
  </body>
</html>