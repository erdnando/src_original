<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoEstado,
		com.netro.model.catalogos.CatalogoIFEpo,
		com.netro.mandatodoc.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if (informacion.equals("CatalogoEstados")){

		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre");
		cat.setOrden("cd_nombre");
		cat.setClavePais("24");
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoIfDist")){

	CatalogoIFEpo cat = new CatalogoIFEpo();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveEpo(iNoCliente);
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta")	||	informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")){

	String numNafinElec = (request.getParameter("numNafinElec")!=null)?request.getParameter("numNafinElec"):"";
	//String nombreMandante = (request.getParameter("nombreMandante")!=null)?request.getParameter("nombreMandante"):"";
	String ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
	String estado = (request.getParameter("estado")!=null)?request.getParameter("estado"):"";
	
	
	int start = 0;
	int limit = 0;
	com.netro.cadenas.ConsIfEpoExt pag = new com.netro.cadenas.ConsIfEpoExt();
	pag.setNumNafinElec(numNafinElec);
	pag.setClaveIf(ic_if);
	//pag.setNombreMandante(nombreMandante);
	pag.setEstado(estado);
	pag.setCveEpo(iNoCliente);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pag);

	if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}

			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ArchivoPDF")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
}else if ( informacion.equals("obtenDetalle") ){
	
	//MandatoDocumentosHome mandatoDocumentosHome = (MandatoDocumentosHome)ServiceLocator.getInstance().getEJBHome("MandatoDocumentosEJB", MandatoDocumentosHome.class);
	//MandatoDocumentos BeanMandatoDocumentos = mandatoDocumentosHome.create();
	MandatoDocumentos BeanMandatoDocumentos = ServiceLocator.getInstance().lookup("MandatoDocumentosEJB",MandatoDocumentos.class);
	JSONObject jsonObj = new JSONObject();

	String numNafinElec = (request.getParameter("numNafinElec")!=null)?request.getParameter("numNafinElec"):"";
	Hashtable consultaDetIf = new Hashtable();
			
	consultaDetIf = BeanMandatoDocumentos.getIfDetalles(numNafinElec);
	if(!consultaDetIf.isEmpty()){
		jsonObj.put("consultaDetIf", consultaDetIf);
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>