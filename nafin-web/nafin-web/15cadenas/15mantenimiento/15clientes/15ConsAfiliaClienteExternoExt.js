Ext.onReady(function(){
	
	
	
	
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  infoR = Ext.JSON.decode(response.responseText); 
			var  fp =   Ext.getDom('formAux');  	 		
			var btnCSV  = Ext.ComponentQuery.query('#btnGenerarCSV')[0];
			var btnImp  = Ext.ComponentQuery.query('#btnImprimir')[0];
			if(btnCSV.isVisible()){
				btnCSV.setIconCls('icoXls');
				Ext.ComponentQuery.query('#btnGenerarCSV')[0].enable();
			}
			if(btnImp.isVisible()){
				btnImp.setIconCls('icoPdf');
				Ext.ComponentQuery.query('#btnImprimir')[0].enable();
			}
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin',''); 
			var params = {nombreArchivo: archivo};	
			fp.action 		= NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.method 		= 'post';
			fp.target 		= '_self';
			fp.submit();	
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function descargaArchivo1(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  infoR = Ext.JSON.decode(response.responseText); 
			var  fp =   Ext.getDom('formAux');  	 		
			var btnImpC = Ext.ComponentQuery.query('#btnImprimirC')[0];
			if(btnImpC.isVisible()){
				btnImpC.setIconCls('icoPdf');
				Ext.ComponentQuery.query('#btnImprimirC')[0].enable();
			}
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin',''); 
			var params = {nombreArchivo: archivo};	
			fp.action 		= NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.method 		= 'post';
			fp.target 		= '_self';
			fp.submit();	
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//--------------------Cuentas  Usuario--------------------------
	

	function modificarPerfil(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
		
			var  info = Ext.JSON.decode(response.responseText); 
			
			Ext.ComponentQuery.query('#btnModificar')[0].enable();
			Ext.ComponentQuery.query('#btnModificar')[0].setIconCls('modificar');		  
			
			Ext.MessageBox.alert('Mensaje',info.mensaje,
				function(){
					Ext.getCmp('ProcesarCuentas').hide();
				}	
			);
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}	
	function procesarInformacion(opts, success, response) {		
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  info = Ext.JSON.decode(response.responseText); 	
			
			Ext.ComponentQuery.query('#lblNombreEmpresa1')[0].setValue(info.lblNombreEmpresa);  
			Ext.ComponentQuery.query('#lblNafinElec1')[0].setValue(info.lblNafinElec);
			Ext.ComponentQuery.query('#lblNombre1')[0].setValue(info.lblNombre);  
			Ext.ComponentQuery.query('#lblApellidoPaterno1')[0].setValue(info.lblApellidoPaterno); 
			Ext.ComponentQuery.query('#lblApellidoMaterno1')[0].setValue(info.lblApellidoMaterno); 
			Ext.ComponentQuery.query('#lblEmail1')[0].setValue(info.lblEmail);
			Ext.ComponentQuery.query('#lblPerfilActual1')[0].setValue(info.lblPerfilActual);				
			Ext.ComponentQuery.query('#txtLogin1')[0].setValue(info.txtLogin);
			Ext.ComponentQuery.query('#sTipoAfiliado')[0].setValue(info.sTipoAfiliado);	
			Ext.ComponentQuery.query('#internacional')[0].setValue(info.internacional);	
			Ext.ComponentQuery.query('#sTipoAfiliado')[0].setValue(info.sTipoAfiliado);	
			Ext.ComponentQuery.query('#txtNafinElectronico')[0].setValue(info.txtNafinElectronico);	
			Ext.ComponentQuery.query('#txtPerfilAnt')[0].setValue(info.txtPerfilAnt);	
			Ext.ComponentQuery.query('#txtLoginC')[0].setValue(info.txtLogin);		 		
					
			if(info.modificar=='S')  {
				Ext.ComponentQuery.query('#txtPerfil_1')[0].show();
				
				catalogoPerfil.load({
					params : Ext.apply({
						tipoAfiliado: info.sTipoAfiliado, 
						txtLogin:info.txtLogin
					})
				}); 
				
			}else  {
				Ext.ComponentQuery.query('#txtPerfil_1')[0].hide();
			}
			
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
	
	//**-
	var modificarCuenta = function(grid, rowIndex, colIndex, item, event) {  
		
		var registro = grid.getStore().getAt(rowIndex);
		var txtLogin = registro.get('CUENTA');  
		
		
		
		 
		Ext.Ajax.request({
			url: '15ConsAfiliaClienteExterno.data.jsp',
			params: {
				informacion: 'informacionUsuario',		
				txtLogin:txtLogin,
				modificar:'S'
			},
			callback:procesarInformacion 
		});
		ventanaCuenta();
		Ext.ComponentQuery.query('#btnModificar')[0].show(); 
		Ext.ComponentQuery.query('#id_modificacion')[0].show();
		Ext.ComponentQuery.query('#id_informacion')[0].hide();
	}
	
	//ver datos de la cuenta 
	
	var procesaCuenta = function(grid, rowIndex, colIndex, item, event) {  
		
		var registro = grid.getStore().getAt(rowIndex);
		var txtLogin = registro.get('CUENTA');  
		 
		Ext.Ajax.request({
			url: '15ConsAfiliaClienteExterno.data.jsp',
			params: {
				informacion: 'informacionUsuario',		
				txtLogin:txtLogin,
				modificar:'N'
			},
			callback:procesarInformacion 
		});
		
		ventanaCuenta();
		Ext.ComponentQuery.query('#btnModificar')[0].hide(); 
		Ext.ComponentQuery.query('#id_modificacion')[0].hide();
		Ext.ComponentQuery.query('#id_informacion')[0].show();
		 
	}	
	
	
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 	type: 'string' },
			{ name: 'descripcion', 	type: 'string' }				
		]
	});	
	 
	var catalogoPerfil = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15ConsAfiliaClienteExterno.data.jsp', 
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoPerfil'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	 
		
	//***************************Informaci�n dle Usuario************************+		

	var elementosDespliega =[
		{ 	xtype: 'textfield',  hidden: true, id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'internacional', 	value: '' },		
		{ 	xtype: 'textfield',  hidden: true,  id: 'sTipoAfiliado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtNafinElectronico', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtPerfilAnt', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'txtLoginC', 	value: '' },			
		{
			xtype: 'panel',
			id: 'id_informacion', 
			allowBlank: false,
			title:'INFORMACION USUARIO',			
			value: '',
			hidden: true
		},
		{
			xtype: 'panel',
			id: 'id_modificacion', 
			allowBlank: false,
			title:'CAMBIO DE PERFIL',			
			value: '',
			hidden: true
		},
		{
			xtype: 'displayfield',
			name: 'txtLogin',
			id: 'txtLogin1',
			allowBlank: false,			
			fieldLabel: 'Clave de Usuario',
			value:''
		},
		{
			xtype: 'fieldcontainer',        
			labelStyle: 'font-weight:bold;padding:0',
			layout: 'hbox',
			defaultType: 'textfield',	 		
			hidden:false,
			fieldDefaults: {
				msgTarget: 'side',
				labelWidth: 150			
			},			
			fieldLabel: 'Empresa',
			items: [
				{
					xtype: 'displayfield',
					name: 'lblNombreEmpresa',
					id: 'lblNombreEmpresa1',
					allowBlank: false,
					width			: 230,
					value:''
				},
				{
					xtype: 'displayfield',
					id:'muestraCol',
					value: 'Nafin Electr�nico:',
					hidden: false
				},		
				{		
					xtype: 'displayfield',
					name: 'lblNafinElec',
					id: 'lblNafinElec1',
					allowBlank: false,
					anchor:'70%',
					width			: 230,				
					value:''
				}
			]
		},	
		{
			xtype: 'displayfield',
			name: 'lblNombre',
			id: 'lblNombre1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Nombre',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblApellidoPaterno',
			id: 'lblApellidoPaterno1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Apellido Paterno',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblApellidoMaterno',
			id: 'lblApellidoMaterno1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Apellido Materno',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblEmail',
			id: 'lblEmail1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Email',
			value:''
		},
		{
			xtype: 'displayfield',
			name: 'lblPerfilActual',
			id: 'lblPerfilActual1',
			allowBlank: false,
			anchor:'70%',
			fieldLabel: 'Perfil Actual',
			value:''
		},
		{
			xtype: 'combo',
			fieldLabel: 'Perfil',
			itemId: 'txtPerfil_1',
			name: 'txtPerfil',
			hiddenName: 'txtPerfil',
			forceSelection: true,
			hidden: true, 
			width: 350,
			store: catalogoPerfil,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave'	 		
		}
	];
	

	
	
	
	//---- Ver Cuentas---
	var procesarVerCuentas= function(grid, rowIndex, colIndex, item, event) {
	
		var registro = grid.getStore().getAt(rowIndex);		
		var num_electronico = registro.get('NUM_ELECTRONICO'); 
		var nombreCliente = registro.get('NOMBRE_RAZON_SOCIAL'); 
		var tituloC = 'N@E: '+ num_electronico +'  '+nombreCliente;
		
		var  forma =   Ext.ComponentQuery.query('#forma')[0];
		forma.query('#num_naElectronico')[0].setValue(num_electronico);
		forma.query('#tituloC')[0].setValue(tituloC);
		
			
		consCuentasData.load({	
			params: {
				informacion: 'ConsCuentas',
				tituloC:tituloC,
				num_electronico:num_electronico,
				tipoAfiliado:'C'
			}
		});			
				
		new Ext.Window({					
			modal: true,
			width: 340,
			height: 380,
			modal: true,					
			closeAction: 'destroy',
			resizable: true,
			constrain: true,
			closable:true,
			id: 'Vercuentas',
			items: [					
				gridCuentas					
			]		
		}).show().setTitle(tituloC);	
	}
	
	var procesarConCuentasData = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var gridConsulta =  Ext.ComponentQuery.query('#gridConsulta')[0];
			var json =  store.proxy.reader.rawData;
			
			Ext.ComponentQuery.query('#gridCuentas')[0].show();	
			
			if(store.getTotalCount() > 0) {				
				Ext.ComponentQuery.query('#gridCuentas')[0].getView().getEl().unmask();	
				Ext.ComponentQuery.query('#gridCuentas')[0].getView().setAutoScroll(true);	
				Ext.ComponentQuery.query('#btnImprimirC')[0].enable();
			}else {	
				Ext.ComponentQuery.query('#gridCuentas')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridCuentas')[0].getView().setAutoScroll(false);	
					Ext.ComponentQuery.query('#btnImprimirC')[0].disable();
			}			
		}
	};
	
	
	Ext.define('LisRegCuentas', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'CUENTA'}			
		 ]
	});

	var consCuentasData = Ext.create('Ext.data.Store', {
		model: 'LisRegCuentas',			
		proxy: {
			type: 'ajax',
			url: '15ConsAfiliaClienteExterno.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'ConsCuentas'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarConCuentasData
		}
	});
 
	var gridCuentas = {		
		xtype: 'grid',
		store: consCuentasData,
		id: 'gridCuentas',		
		hidden: false,
		title: 'Usuarios',	
		columns: [			
			{
				xtype:	'actioncolumn',
				header: 'Login del Usuario',
				tooltip: 'Login del Usuario',
				dataIndex: 'CUENTA',
				align: 'center',
				width: 150,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					return (record.get('CUENTA'));
				},
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';							
						}
						,handler:	procesaCuenta   
					}
				]
			},
			{
				xtype:	'actioncolumn',
				header: 'Cambio de Perfil',
				tooltip: 'Cambio de Perfil',					
				align: 'center',				
				width: 150,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Modificar';
							return 'modificar';							
						}
						,handler:	modificarCuenta  
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 340,
		width: 320,		
		frame: true,
		bbar:{		
			items: [
			'->',
				{
					xtype: 'button',
					text: 'Imprimir',
					tooltip:	'Imprimir',
					id: 'btnImprimirC',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
						var  forma =   Ext.ComponentQuery.query('#forma')[0];
						Ext.ComponentQuery.query('#btnImprimirC')[0].disable();
						Ext.ComponentQuery.query('#btnImprimirC')[0].setIconCls('x-mask-msg-text');		 
						Ext.Ajax.request({
							url: '15ConsAfiliaClienteExterno.data.jsp', 
							params:	{
								num_electronico: forma.query('#num_naElectronico')[0].getValue(),
								informacion: 'ImprimirConsCuentas',
								tipoAfiliado:'C',
								tituloC:forma.query('#tituloC')[0].getValue()
							}					
							,callback: descargaArchivo1
						});
					}
				}
			]
		}
	};
	
	
	//---------------Acci�n Modificar----------------
	var procesarModificar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var num_electronico = registro.get('NUM_ELECTRONICO');  
		window.location ="15ModifClienteExternoExt.jsp?num_electronico="+num_electronico;
	}
	
	// ---------------------Acci�n Eliminar --------------------
	function TransmiteEliminar(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  info = Ext.JSON.decode(response.responseText); 
			
			var  fp =   Ext.ComponentQuery.query('#forma')[0];
			
			Ext.MessageBox.alert('Mensaje',info.msg,
				function(){				
					consultaData.load({	
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15						
						})
					});	
			});

		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	
	var procesarEliminar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);		
			
		Ext.Ajax.request({
				url: '15ConsAfiliaClienteExterno.data.jsp',
				params:{
					informacion: 'Eliminar',					
					num_electronico: registro.get('NUM_ELECTRONICO'),
					razon_social: registro.get('NOMBRE_RAZON_SOCIAL'),  
					rfc: registro.get('RFC'),
					num_sirac : registro.get('NUM_SIRAC'),
					domicilio : registro.get('DOMICILIO'),  
					estado : registro.get('ESTADO'),
					telefono: registro.get('TELEFONO')
				},
				callback: TransmiteEliminar
			});
	}
	var procesarBusquedaAvanzada = function(store, arrRegistros, opts){
		var jsonData = store.proxy.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
		}
	}
	
	
	//-----------------------B�squeda Avanzada -----------------
	
	Ext.define('ModelCatalogos1', 	{ 
		extend: 'Ext.data.Model',  
		fields: [
			{ name: 'CLAVE', 	type: 'string' },
			{ name: 'DESCRIPCION', 	type: 'string' }				
		]
	});	
	
	var catalogoBuscaAvanzada = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos1',
		itemId: 'catBusquedaAva',
		proxy: {
			type: 'ajax',
			url: '15ConsAfiliaClienteExterno.data.jsp', 
			reader: {
				type: 'json',
				root: 'registros' 
			},
			extraParams: {
				informacion: 'catalogoBuscaAvanzada'
			}
		},
			listeners: {
				load: procesarBusquedaAvanzada,
				beforeload: NE.util.initMensajeCargaCombo,
			exception: NE.util.mostrarDataProxyError
			}		
	});
	
	
	var elementsFormBusq = [
		{
			xtype:'form',
			id:'fpWinBusca',
			frame: true,
			border: false,
			style: 'margin: 0 auto',
			bodyStyle:'padding:10px',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			labelWidth: 130,
			items:[
				{ 
					xtype:   'label',  
					html:		'<center><B>Tipo de Afiliado:</B> CLIENTE EXTERNO</center>', 
					cls:		'x-form-item', 
					style: { 
						width:   		'100%', 
						textAlign: 		'center'
					} 
				},
				{ 
					xtype:   'label',  
					html:		'<center><br>Utilice el * para realizar una b�squeda gen�rica.</center>', 
					cls:		'x-form-item', 
					style: { 
						width:   		'100%', 
						textAlign: 		'center'
					} 
				},		
				{
					xtype: 'textfield',
					fieldLabel: 'Nombre',
					itemId: 'nombrePyme1',
					name: 'nombrePyme',					
					width: 50,
					height: 20,
					anchor: '100%',
					maxLength: 100				
				},	
				{
					xtype: 'textfield',
					fieldLabel: 'RFC',
					itemId: 'rfcPyme1',
					name: 'rfcPyme',					
					width: 50,
					height: 20,
					anchor: '100%',
					maxLength: 20				
				},	
				{
					xtype: 'textfield',
					fieldLabel: 'N�mero Cliente Sirac',
					itemId: 'num_ClienteSirac1',
					name: 'num_ClienteSirac',					
					width: 50,
					height: 20,
					anchor: '100%',
					maxLength: 12				
				}
			],
			buttonAlign:'center',
			buttons:[
				{
					text: 'Buscar',
					id: 'btnBuscarAvan',
					iconCls: 'icoBuscar',
					width: 	75,
					handler: function(boton, evento) {
						var cmbPyme= Ext.ComponentQuery.query('#cbPyme1')[0];
					  var storeCmb = cmbPyme.getStore();
					  cmbPyme.setDisabled(false);
						catalogoBuscaAvanzada.load({
							params : Ext.apply({
								nombrePyme: Ext.ComponentQuery.query('#nombrePyme1')[0].getValue(),
								rfcPyme: Ext.ComponentQuery.query('#rfcPyme1')[0].getValue(),
								num_ClienteSirac: Ext.ComponentQuery.query('#num_ClienteSirac1')[0].getValue()								
							})
						}); 
						
					}
				},
				{
					text: 'Cancelar',
					id: 'btnCancelarAvan',
					iconCls: 'icoLimpiar',
					width: 	75,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winBusqAvan');
						fpBusqAvanzada.getForm().reset();
						ventana.hide();
								
					}
				}
			]
		},
		{
			xtype:'form',
			frame: true,
			id:'fpWinBuscaB',
			style: 'margin: 0 auto',
			bodyStyle:'padding:10px',
			monitorValid: true,
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			labelWidth: 80,
			items:
			[
				{
					xtype: 'combo',
					fieldLabel: 'Nombre',
					itemId: 'cbPyme1',
					name: 'cbPyme',
					hiddenName: 'cbPyme',
					//forceSelection: true,
					//hidden: true, 
					width: 350,
					store: catalogoBuscaAvanzada,
					emptyText: 'Seleccione...',
					queryMode: 'local',
					allowBlank: true,
					displayField: 'DESCRIPCION',
					valueField: 'CLAVE'	 		
				}	
			],
			buttonAlign:'center',
			buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) { 
				
					var 	cbPyme1=  Ext.ComponentQuery.query('#cbPyme1')[0].getValue();
					Ext.ComponentQuery.query('#num_electronico_1')[0].setValue(cbPyme1);
					
					if (Ext.isEmpty(Ext.ComponentQuery.query('#cbPyme1')[0].getValue())) {
						Ext.ComponentQuery.query('#cbPyme1')[0].markInvalid('Seleccione Proveedor');
						return;
					}
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
					 
				}				
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
				
			}
		]
			}
	];
	
	var fpBusqAvanzada = Ext.create('Ext.form.Panel',	{
      itemId: 'fpBusqAvanzada',
      frame: true,
		border: false,
      bodyPadding: '12 6 12 6',     
      style: 'margin: 0px auto 0px auto;',
      hidden: false,
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 120
		},		
		items: elementsFormBusq,
		monitorValid: true
	});
	
	//******************************************* Consulta********************
	
	var procesarConsultar = function(store,  arrRegistros, success, opts) {
		var fp = Ext.ComponentQuery.query('#forma')[0];
		fp.unmask();
		if (arrRegistros != null) {
			var gridConsulta =  Ext.ComponentQuery.query('#gridConsulta')[0];
			var json =  store.proxy.reader.rawData;
			
			Ext.ComponentQuery.query('#gridConsulta')[0].show();	
			
			if(store.getTotalCount() > 0) {		
				
				Ext.ComponentQuery.query('#btnGenerarCSV')[0].enable();
				Ext.ComponentQuery.query('#btnImprimir')[0].enable();
				
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().getEl().unmask();	
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(true);				
				
			}else {
			
				Ext.ComponentQuery.query('#btnGenerarCSV')[0].disable();
				Ext.ComponentQuery.query('#btnImprimir')[0].disable();
				
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(false);	
			
			}
			
		}
	};
	
	Ext.define('LisRegistros', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'NUM_ELECTRONICO'},	
			{ name: 'NOMBRE_RAZON_SOCIAL' },
			{ name: 'RFC' },
			{ name: 'NUM_SIRAC' },
			{ name: 'DOMICILIO' },
			{ name: 'ESTADO' },
			{ name: 'TELEFONO' }				
		 ]
	});

	var consultaData = Ext.create('Ext.data.Store', {
		model: 'LisRegistros',			
		proxy: {
			type: 'ajax',
			url: '15ConsAfiliaClienteExterno.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consultar'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsultar
			}
	});
	
	
	var  gridConsulta = Ext.create('Ext.grid.Panel',{			
		itemId: 'gridConsulta',
		store: consultaData,
		height: 350,
		width: 900,	
		style: 'margin: 10px auto 0px auto;',	
		style: 'margin:0 auto;',
		xtype: 'cell-editing',
      title: 'Consulta - 1ER PISO',
      frame: false,	
		hidden: true,
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		columns: [			
			{
				header: 'N�mero de Nafin Electr�nico',				
				dataIndex: 'NUM_ELECTRONICO',
				sortable: true,				
				width: 150,		
				align: 'center',
				resizable: true				
         },	
			{
				header: 'Nombre O Raz�n Social',				
				dataIndex: 'NOMBRE_RAZON_SOCIAL',
				sortable: true,				
				width: 150,		
				align: 'left',
				resizable: true				
         },	
			{
				header: 'RFC',				
				dataIndex: 'RFC',
				sortable: true,				
				width: 150,	
				align: 'center',
				resizable: true				
         },	
			{
				header: 'N�m. Sirac',				
				dataIndex: 'NUM_SIRAC',
				sortable: true,				
				width: 150,	
				align: 'center',
				resizable: true				
         },	
			{
				header: 'Domicilio',				
				dataIndex: 'DOMICILIO',
				sortable: true,				
				width: 150,		
				align: 'left',
				resizable: true				
         },	
			{
				header: 'Estado',				
				dataIndex: 'ESTADO',
				sortable: true,				
				width: 150,		
				align: 'center',
				resizable: true				
         },	
			{
				header: 'Tel�fono',				
				dataIndex: 'TELEFONO',
				sortable: true,				
				width: 150,	
				align: 'center',
				resizable: true				
         },
			{
				xtype		: 'actioncolumn',
				header: 'Seleccionar',
				tooltip: 'Seleccionar ',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Modificar';
							return 'modificar';
						}
						,handler: procesarModificar
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[1].tooltip = 'Borrar';
							return 'borrar';
						}
						,handler: procesarEliminar
					}					
				]
			},			
			{
				xtype		: 'actioncolumn',
				header: 'Cuentas de Usuario',
				tooltip: 'Cuentas de Usuario ',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}
						,handler: procesarVerCuentas 
					}
				]
			}
		],		
		bbar: [
			Ext.create('Ext.PagingToolbar', {
				itemId: 'barraPaginacion',
				store: consultaData,
				displayInfo: true,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No hay registros"
			}),
			'->','-',	
			{
				text: 'Generar Archivo',
				id: 'btnGenerarCSV',
				iconCls: 	'icoXls',
				handler: function(){
						
					var  fp =   Ext.ComponentQuery.query('#forma')[0];
					var button = Ext.ComponentQuery.query('#btnGenerarCSV')[0];		
					button.disable();
					button.setIconCls('x-mask-msg-text');	
					Ext.Ajax.request({
						url: '15ConsAfiliaClienteExterno.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'GeneraArchivoCSV'			
						}),							
						callback: descargaArchivo
					});									
				}
			},
			{
				text: 'Imprimir',
				id: 'btnImprimir',
				iconCls:'icoPdf',
				handler: function(){
				
					var  fp =   Ext.ComponentQuery.query('#forma')[0];
					var button = Ext.ComponentQuery.query('#btnImprimir')[0];		
					button.disable();
					button.setIconCls('x-mask-msg-text');						
						
					var barraPaginacion =  Ext.ComponentQuery.query('#barraPaginacion')[0];		
							
					Ext.Ajax.request({
						url: '15ConsAfiliaClienteExterno.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'GeneraArchivoPDF',
							start: 0,
							limit: 15									 
						}),							
						callback: descargaArchivo
					});										
				}
			}
		]
	});
	
	function creditoElec(pagina) {
		if (pagina == 0) { //Afianzadora
			window.location  = "15forma05AfianzadoraExt.jsp"; 	
		}	else if (pagina == 1 ) { // cadena productiva
			window.location ="15consEPOext.jsp";
		}	else if (pagina == 2) { //provedores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15forma05ProveedoresExt.jsp";
		}	else if (pagina == 3) { //Provedor Internacional
			
		}	else if (pagina == 4) { // provedor sin numero
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp";
		}	else if (pagina == 5) { // Distribuidores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp";		
		}	else if (pagina == 6) { // Fiados
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp";		
		}	else if (pagina == 7) { // Intermediario financiero Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B"; 	
		} else if (pagina == 8) { // Intermediario financiero NO Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB"; 	 	
		} else if (pagina == 9) { // Intermediario financiero Bancario /No Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB"; 		 	
		} else if (pagina == 10) { // Provedor Carga Masiva EContrac
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon_ext.jsp";
		}	else if (pagina == 11) { // Afiliados a Credito Electronico
			window.location = "15forma05ext_AfCredElectronico.jsp";
		}	else if (pagina == 12) { // Cadena Productiva Internacional
			
		}	else if (pagina == 13) { // Contragarante
			
		}	else if (pagina == 14) { // Mandante
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp"; 	
		}	else if (pagina == 15) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp"; 			
		}	else if (pagina == 16) { // Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp";  	
		}

	}
	
	var storeAfiliacion = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],
		data : [
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],
			//['3','Proveedor Internacional'],
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],	
			//['12','Cadena Productiva Internacional'],
			//['13','Contragarante'],
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo'],
			['16','Cliente Externo']
		]
	});
	
	
	
	var  elementosForma =  [
		{
			xtype: 'combo',
			fieldLabel: 'Afiliaci�n',
			itemId: 'cmbTipoAfiliado_1',
			name: 'cmbTipoAfiliado',
			hiddenName: 'cmbTipoAfiliado',
			forceSelection: true,
			width: 450,
			store: storeAfiliacion,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',						
			listeners: {
				select: function(obj, newVal, oldVal){
					
					var  fp =  Ext.ComponentQuery.query('#forma')[0];
						
					var valor = fp.query('#cmbTipoAfiliado_1')[0].getValue();	
					creditoElec(valor); 
				}
			}
		},
		{
			xtype: 'fieldcontainer',        
			labelStyle: 'font-weight:bold;padding:0',
			layout: 'hbox',
			defaultType: 'textfield',			
			hidden:false,
			fieldDefaults: {
			msgTarget: 'side',
			labelWidth: 150
			},
			items: [
				{
					xtype: 'textfield',
					fieldLabel: 'N�mero Nafin Electr�nico',
					itemId: 'num_electronico_1',
					name: 'num_electronico',	
					regex			:/^[0-9]*$/,
					width: 350,				
					maxLength: 15			
				},			
				{
					xtype: 	'button',
					text: 	'B�squeda Avanzada',
					itemId: 	'btnAvanzada',
					iconCls: 'icoBuscar',					
					handler: function(button,event) {
						var comboPerfil = Ext.ComponentQuery.query('#cbPyme1')[0];
						comboPerfil.setValue('');
						comboPerfil.setDisabled(true);	
						
						var ventana = Ext.getCmp('winBusqAvan');
						if (ventana) {
							ventana.show();
						} else {
							new Ext.Window({
									title: 			'B�squeda Avanzada',
									layout: 			'fit',
									width: 			400,
									height: 			340,
									minWidth: 		400,
									minHeight: 		340,
									buttonAlign: 	'center',
									id: 				'winBusqAvan',
									closeAction: 	'hide',
									items: 	fpBusqAvanzada
								}).show();
						}
						
					}
				}				
			]
		},
		{
			xtype: 'fieldcontainer',        
			labelStyle: 'font-weight:bold;padding:0',
			layout: 'hbox',
			defaultType: 'textfield',			
			hidden:false,
			fieldDefaults: {
				msgTarget: 'side',
				labelWidth: 150
			},
			items: [
				{
					xtype: 'textfield',
					fieldLabel: 'Nombre',
					itemId: 'nombre_1',
					name: 'nombre',						
					width: 400,				
					maxLength: 30			
				}
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'num_naElectronico', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'tituloC', 	value: '' }	
	];
	var fpDespliega = Ext.create('Ext.form.Panel',	{
      itemId: 'fpDespliega',
      frame: true,
		border: false,
      bodyPadding: '12 6 12 6',
      width: 790,
		height: 380,
      style: 'margin: 0px auto 0px auto;',
      hidden: false,
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 100
		},	
		title: '',
		items	: elementosDespliega,  
		buttons: [		
			{
				text: 'Modificar',
				id: 'btnModificar',
				iconCls: 'modificar',
				hidden: true,
				formBind: true,				
				handler: function(boton, evento) {	
					var  fpDespliega =   Ext.ComponentQuery.query('#fpDespliega')[0];
					Ext.ComponentQuery.query('#btnModificar')[0].disable();
					Ext.ComponentQuery.query('#btnModificar')[0].setIconCls('x-mask-msg-text');		 
				
					Ext.Ajax.request({
						url: '15ConsAfiliaClienteExterno.data.jsp',
						params: Ext.apply(fpDespliega.getForm().getValues(),{
							informacion: 'ModifiarPerfil'	,
							sTipoAfiliado:	Ext.ComponentQuery.query('#sTipoAfiliado')[0].getValue(),
							internacional: Ext.ComponentQuery.query('#internacional')[0].getValue(),								
							txtNafinElectronico:Ext.ComponentQuery.query('#txtNafinElectronico')[0].getValue(),	
							txtPerfilAnt:Ext.ComponentQuery.query('#txtPerfilAnt')[0].getValue(),	
							txtLoginC:Ext.ComponentQuery.query('#txtLoginC')[0].getValue(),
							txtPerfil: Ext.ComponentQuery.query('#txtPerfil_1')[0].getValue() 
			
						}),
						callback: modificarPerfil
					});				
				}
			}			
		]	
	});
	
		
	var ventanaCuenta = function() {
	
		var ProcesarCuentas = Ext.getCmp('ProcesarCuentas');
		if(ProcesarCuentas){
			ProcesarCuentas.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 800,
				height: 400,												
				resizable: false,
				closeAction: 'destroy',
				closable:true,
				id: 'ProcesarCuentas',
				closeAction: 'hide',
				items: [					
					fpDespliega					
				]			
			}).show().setTitle('');
		}	
	}
	
	
	var fp = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'forma',
		frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: 'Consulta - 1ER PISO',
      width: 550,		
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 150
		},
		layout: 'anchor',
		items	: elementosForma,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
					var  fp =   Ext.ComponentQuery.query('#forma')[0];
					fp.el.mask('Consultando...', 'x-mask-loading');
					consultaData.load({	
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15						
						})
					});						
				}			
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				formBind: true,
				handler: function(){	
					window.location = '15ConsAfiliaClienteExternoExt.jsp';
				}
			}	
		]
	});
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 900,
		style: 'margin:0 auto;',
		items: [
			fp,
			NE.util.getEspaciador(30),
			gridConsulta
		]
	});
	
	Ext.ComponentQuery.query('#cmbTipoAfiliado_1')[0].setValue('16');
});