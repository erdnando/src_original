<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
	
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;  charset=windows-1252">
    <title>Nafi@net - Afiliación</title>
	 <%@ include file="/extjs.jspf" %>
	  <%if(esEsquemaExtJS) {%>
		<%@ include file="/01principal/menu.jspf"%>
		<%}%>
	 <link rel="stylesheet" type="text/css" href="<%=appWebContextRoot%>/00utils/extjs/ux/FileUploadField.css" />
	 <script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/FileUploadField.js"></script>
	 <script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
	 <script type="text/javascript" src="15forma03Ext.js?<%=session.getId()%>"></script> 
  </head>
  
<%if(esEsquemaExtJS) {%>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">	
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
				<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
		
		<form id='formParametros' name="formParametros">
			<input type="hidden" id="sTipoUsuario" name=" " value="<%=strTipoUsuario%>"/>	
		</form>
	</body>
	 
<%}else  if(!esEsquemaExtJS) {%>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<div id='areaContenido' style="margin-left: 3px; margin-top: 3px;"></div>
		<form id='formAux' name="formAux" target='_new'></form>
		
		<form id='formParametros' name="formParametros">
			<input type="hidden" id="sTipoUsuario" name=" " value="<%=strTipoUsuario%>"/>	
		</form>
	</body>	
	
<%}%>
</html>