Ext.onReady(function() {

var ic_if =  Ext.getDom('ic_if').value;
var clavePerfil = '';
function limpiarPyme(op){
	Ext.getCmp('ic_pyme').setValue('');
	Ext.getCmp('txt_nafelec').setValue('');
	Ext.getCmp('hid_nombre').setValue('');
}
        
        var ObjGeneral = {  
           cvePerf :  '',
           
           successAjaxFn  :  function(response, request){
				  try{
                if (Ext.util.JSON.decode(response.responseText).success == true) {
                      var jsonData = Ext.decode(response.responseText);                       
                      this.cvePerf = jsonData.cvePerf;
							 clavePerfil = jsonData.cvePerf;
					 }
              }catch(e){
                Ext.Msg.show({
                         title    :  'Error de inicio',
                         msg      :  e.description,
                         icon     :  Ext.Msg.ERROR,
                         buttons  :  Ext.Msg.OK
                });
              }
           },
           
           failureAjaxFn  :  function(response, request){
              var errMessage = '<b>Error en la petici�n</b> ' + request.url + '<br> '  
                               + ' <b>Estatus</b> ' + response.status + ' - ' + response.statusText + '<br>'  
                               + ' ' + response.responseText + '<br>';
              Ext.Msg.show({
                 title    :  response.status + ' - ' + response.statusText,
                 msg      :  errMessage,
                 icon     :  Ext.Msg.ERROR,
                 buttons  :  Ext.Msg.OK
              });
           },
           
           callbackAjaxFn :  function(){
                Ext.getBody().unmask();
           },
           
           iniciaPrograma : function(){
              
        Ext.getBody().mask('Inicializando Pantalla ...');
              Ext.Ajax.request({
                 url   :  '15clausuladoifext.data.jsp',
                 method:  'POST',
                 params:  {informacion : 'IniciaPrograma'},
                 success  :  this.successAjaxFn,
                 failure  :  this.failureAjaxFn,
                 callback :  this.callbackAjaxFn
              });   
           },
           
           inicializaVariable   :  function(){
              this.cvePerf  =  '';
           }
        }
        ObjGeneral.inicializaVariable();
        
   
   
   /* Valida Nafin Electr�nico PyME */
   var fnValidaNafinElectronicoPyME = function( comp, newValue, oldValue ){
      Ext.getCmp('ic_pyme').setValue('');
      Ext.Ajax.request({  
         url: '15clausuladoifext.data.jsp',  
         method: 'POST',  
         callback: successAjaxFn,  
         params: {  
             informacion: 	'pymeNombre' ,
             txt_nafelec: 	newValue,
             noIc_if:		Ext.getCmp('id_noIc_if').getValue(),
             ic_pyme:		Ext.getCmp('ic_pyme').getValue()
         }  
      });
   }     

	var busqAvanzadaSelect=0;
	Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);
	//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton = Ext.getCmp('btnGenerarArchivo');
			boton.enable();
			var boton2 = Ext.getCmp('btnGenerarPDF');
			boton2.enable();		
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
		
	var procesarCatalogoBancoFondeo = function(store, arrRegistros, opts) {
		//Ext.getCmp("id_noBancoFondeo").setValue((store.getRange()[0].data).clave);	
	}
	var procesarCatalogoNombreIF = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
			clave: "", 
			descripcion: "Seleccionar IF", 
			loadMsg: ""})); 
			store.commitChanges(); 
		 
		 //if(ObjGeneral.cvePerf!='8'){
      if(clavePerfil != '8'){
			var cmbIcif = Ext.getCmp("id_noIc_if");
         var store   = cmbIcif.getStore();
         
         if(store.totalLength>0){
            cmbIcif.setValue((ic_if));		
         }
       }
		 if(clavePerfil == '4'){
			var cmbIcif = Ext.getCmp("id_noIc_if");
         var store   = cmbIcif.getStore();
			cmbIcif.setValue("");			 
		 }
	}
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
	
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnBajarPDF').hide();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnBajarArchivo.hide();
				btnBajarPDF.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			
         if(!Ext.isEmpty(jsonObj.mensaje)){
            consultaDataGrid.removeAll();
            Ext.getCmp('grid').hide();
            Ext.Msg.show({
               title    :  'No existe',
               msg      :  jsonObj.mensaje,
               buttons  :  Ext.Msg.OK,
               icon     :  Ext.Msg.INFO,
               fn       :  limpiarPyme
            });
            return false;
         }
         
         if(jsonObj.ic_pyme!="" && jsonObj.pyme!=""){
				Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
				Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);			
			}
		}
	}
	var bajarContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			/*var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();*/
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();			
		} else {
			//alert("error");
			NE.util.mostrarConnError(response,opts);
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
      Ext.getCmp('btnBuscarAvanzada').setIconClass('icoBuscar');
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.descripcion == ""){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('id_cmb_num_ne').focus();
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15clausuladoifext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'EXTENSION'},
			{name: 'IC_CONSECUTIVO'},
			{name: 'NOMPYME'},
			{name: 'PROVEEDOR'},
			{name: 'NOMEPO'},
			{name: 'IC_USUARIO'},
			{name: 'DF_ACEPTACION'},
			{name: 'ORIGEN'},
			{name: 'IC_IF'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	var catalogoBancoFondeo = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15clausuladoifext.data.jsp',
		listeners: {
			load: procesarCatalogoBancoFondeo,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoBancoFondeo'
		},
		totalProperty : 'total',
		autoLoad: false
	});
	var catalogoNombreIF = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '15clausuladoifext.data.jsp',
		listeners: {		
			load: procesarCatalogoNombreIF,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoNombreIF'
		},
		totalProperty : 'total',
		autoLoad: false
	});
	
	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion','ic_pyme','loadMsg'],
		url : '15clausuladoifext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		columns: [{
				header: 'Nombre PyME', tooltip: 'Nombre PyME',
				dataIndex: 'NOMPYME',
				sortable: true,
				width: 370, resizable: true,
				align: 'left'
			},{
				header: 'Usuario', tooltip: 'Usuario',
				dataIndex: 'IC_USUARIO',
				sortable: true,	align: 'center',
				width: 110, resizable: true,
				align: 'left'
			},{
				header: 'Fecha/Hora', tooltip: 'Fecha/Hora',
				dataIndex: 'DF_ACEPTACION',
				sortable: true,	align: 'center',
				width: 140, resizable: true,
				align: 'center'
			},{
				xtype: 'actioncolumn',
				header: 'Contrato', tooltip: 'Contrato',
				dataIndex: '',
				width: 160, resizable: true,
				align: 'left', 
				renderer: function(){ return 'Descargar Clausulado  '; },
				items: [
					{ //para cambiar icono segun extension PDF/DOC 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							var icono = '';
							switch(registro.get('EXTENSION')){
								case 'pdf' : icono = 'icoPdf'; break
								case 'doc' : icono = 'icoDoc'; break
								case 'ppt' : icono = 'icoPpt'; break
								default: icono = 'rechazar';
							}
							return icono;
						},
						tooltip: 'Descargar Clausulado',
						handler: function(grid, rowIdx, colIds){
						var reg = grid.getStore().getAt(rowIdx);
						Ext.Ajax.request({
							url: '15clausuladoifext.data.jsp',
							params: {
								informacion: 'Contrato',
								ic_consecutivo:reg.get('IC_CONSECUTIVO'),
								clave_if: reg.get('IC_IF')
							},
							callback: bajarContrato
						});	
					}				
				}
			]
		}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 445,
		width: 800,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		bbar: {
		xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						//boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '15clausuladoifext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'XLS',
								informacion: 'Consulta'
							}),
							//callback: procesarSuccessFailureGenerarArchivo
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						//boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '15clausuladoifext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'PDF',
								informacion: 'Consulta'
							}),
							//callback: procesarSuccessFailureGenerarPDF 
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
	};
	var elementosFecha = [{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Aceptaci�n de',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'txt_fecha_acep_de',
					id: 'df_consultaMin',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_consultaMax',
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					formBind: true
					//value:ahora.getDate()+'/'+mes.toString()+'/'+ahora.getFullYear()
					},	{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txt_fecha_acep_a',
					id: 'df_consultaMax',
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_consultaMin',
					margins: '0 20 0 0'
				}
			]
		}
	];
	
	var comboFondeo=[{
			xtype: 'combo',
			name: 'noBancoFondeo',
			id: 'id_noBancoFondeo',
			hiddenName : 'noBancoFondeo',
			fieldLabel: 'Banco de Fondeo',
			emptyText: 'Seleccione Banco de Fondeo',
			store: catalogoBancoFondeo,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			allowBlank : true,
			minChars : 1,
			hidden: true,
			width:300,
			listeners: {
				select: function(combo){
				Ext.getCmp('id_noIc_if').clearValue();
				catalogoNombreIF.load({
					params:{operacion:'catalogoNombreIF', noBancoFondeo: combo.getValue()}
				});
				/*
					Ext.Ajax.request({
						waitMsg: 'Please Wait',
						url:'15clausuladoifext.data.jsp',
						method:'POST',
						callback: successAjaxFn,
						params:{operacion:'catalogoNombreIF', txt_nafelec: Ext.getCmp('txt_nafelec').getValue(), ic_pyme:Ext.getCmp('ic_pyme').getValue()}
					});
					*/
				}
			}
	}];
	var comboIF=[{
			xtype: 'combo',
			name: 'noIc_if',
			id: 'id_noIc_if',
			hiddenName : 'noIc_if',
			fieldLabel: 'Nombre de la IF',
			//emptyText: 'Seleccione IF',
			store: catalogoNombreIF,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			allowBlank : true,
			minChars : 1,
			width:300,
			listeners: {
            select: {
               fn: function(combo) {	
						if(combo.getValue()!=ic_if) {
							//if(ObjGeneral.cvePerf == '4'){
							if(clavePerfil == '4'){
								ic_if= combo.getValue();
								return ;
							}
							//if(ObjGeneral.cvePerf!='8'){
                     if(clavePerfil != '8'){  
								Ext.getCmp("id_noIc_if").setValue(ic_if); 
                     }
                  }							
               }
            }
         }
	}]
   
	var elementosForma = [
		{
			xtype: 'hidden',
			name: 'ic_pyme',
			id: 'ic_pyme',
			value:''
		},
		{
			xtype: 'compositefield',
			width: 800,
			fieldLabel: 'Nombre de la PYME',
			labelWidth: 200,
			items:[{
				xtype: 'numberfield',
				name: 'txt_nafelec',
				id:'txt_nafelec',
				width:80,
				value:'',
				listeners: {
               scope:    this, 
               'change': fnValidaNafinElectronicoPyME
				}
			},
			{
				xtype: 'textfield',
				readOnly: true,
				id:'hid_nombre',
				name:'hid_nombre',
				hiddenName : 'hid_nombre',
				mode: 'local',
				resizable: true,
				triggerAction : 'all',
            width : 229
			},{
				xtype: 'button',
				text: 'Busqueda Avanzada',
				id: 'btnAvanzada',
				//width: 50,
				iconCls:	'icoBuscar',
				handler: function(boton, evento) {
					var winVen = Ext.getCmp('winBuscaA');
						if (winVen){
							Ext.getCmp('fpWinBusca').getForm().reset();
							Ext.getCmp('fpWinBuscaB').getForm().reset();
							Ext.getCmp('id_cmb_num_ne').store.removeAll();
							Ext.getCmp('id_cmb_num_ne').reset();
							winVen.show();
						}else{
							var winBuscaA = new Ext.Window ({
								id:'winBuscaA',
								height: 300,
								x: 300,
								y: 100,
								width: 600,
								heigth: 100,
								modal: true,
								closeAction: 'hide',
								title: '',
								items:[{
									xtype:'form',
									id:'fpWinBusca',
									frame: true,
									border: false,
									style: 'margin: 0 auto',
									bodyStyle:'padding:10px',
									defaults: {
										msgTarget: 'side',
										anchor: '-20'
									},
									labelWidth: 140,
									items:[{
										xtype:'displayfield',
										frame:true,
										border: false,
										value:'Utilice el * para b�squeda gen�rica'
									},{
										xtype:'displayfield',
										frame:true,
										border: false,
										value:''
									},{
										xtype: 'textfield',
										name: 'nombre_pyme',
										id:	'txtNombre',
										fieldLabel:'Nombre',
										maxLength:	100
									},{
                              xtype       :  'textfield',
                              name        :  'rfc_pyme',
                              id          :  'id_rfc_pyme',
                              fieldLabel  :  'RFC',
                              maxLength   :  25
                           }/*,{
										xtype: 'textfield',
										name: 'num_pyme',
										id:	'txtNe',
										fieldLabel:'N�mero Proveedor',
										maxLength:	25
									}*/],
									buttons:[{
										text:'Buscar',
                              id    :  'btnBuscarAvanzada',
										iconCls:'icoBuscar',
										handler: function(boton) {
                                 boton.setIconClass('loading-indicator');
											/*if ( Ext.isEmpty(Ext.getCmp('txtNombre').getValue()) && Ext.isEmpty(Ext.getCmp('txtRfc').getValue()) && Ext.isEmpty(Ext.getCmp('txtNe').getValue()) ){
												Ext.Msg.alert(boton.text,'No existe informaci�n con los criterios determinados');
												return;
											}	*/
											catalogoNombreData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
										}
									},{
										text:'Cancelar',
										iconCls: 'icoLimpiar',
										handler: function() {
											Ext.getCmp('winBuscaA').hide();
										}
									}]
								},{
									xtype:'form',
									frame: true,
									id:'fpWinBuscaB',
									style: 'margin: 0 auto',
									bodyStyle:'padding:10px',
									monitorValid: true,
									defaults: {
										msgTarget: 'side',
										anchor: '-20'
									},
									items:[{
										xtype: 'combo',
										id:	'id_cmb_num_ne',
										name: 'cmb_num_ne',
										hiddenName : 'cmb_num_ne',
										fieldLabel: 'Nombre',
										emptyText: 'Seleccione . . .',
										displayField: 'descripcion',
										valueField: 'clave',
										triggerAction : 'all',
										forceSelection:true,
										allowBlank: false,
										typeAhead: true,
										mode: 'local',
										minChars : 1,
										store: catalogoNombreData,
										tpl : NE.util.templateMensajeCargaCombo,
										listeners: {
											select: function(combo, record, index) {
												busqAvanzadaSelect=index;
											}
										}
									}],
									buttons:[{
										text:'Aceptar',
										iconCls:'aceptar',
										formBind:true,
										handler: function() {
											if (!Ext.isEmpty(Ext.getCmp('id_cmb_num_ne').getValue())){
												var reg = Ext.getCmp('id_cmb_num_ne').getStore().getAt(busqAvanzadaSelect).get('ic_pyme');
												Ext.getCmp('ic_pyme').setValue(reg);
												var disp = Ext.getCmp('id_cmb_num_ne').lastSelectionText;
												var desc = disp.slice(disp.indexOf(" ")+1);
												Ext.getCmp('txt_nafelec').setValue(Ext.getCmp('id_cmb_num_ne').getValue());
												Ext.getCmp('hid_nombre').setValue(desc);
												Ext.getCmp('winBuscaA').hide();
											}
                                 /* Valida Nafin Electr�nico PyME */
                                 fnValidaNafinElectronicoPyME(Ext.getCmp('txt_nafelec'),Ext.getCmp('id_cmb_num_ne').getValue(),'');
										}
									},{
										text:'Cancelar',
										iconCls: 'icoLimpiar',
										handler: function() {	Ext.getCmp('winBuscaA').hide();	}
									}]
								}]
							}).show();
						}
					}
				}]
		},
		{
			xtype: 'compositefield',
			items:[{
				xtype: 'textfield',
				fieldLabel: 'Usuario',
				name: 'txt_usuario',
				id:'idUsuario',
				width:250
			}
		]}	
	];
	//Forma para hacer la busqueda filtrada
	var fp = new Ext.form.FormPanel ({
		//xtype: 'form',
		id: 'forma',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 600,
      title :  'Clausulado IF',
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[comboFondeo, comboIF,elementosForma,elementosFecha],
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var df_consultaNotMin = Ext.getCmp("df_consultaMin");
					var df_consultaNotMax = Ext.getCmp("df_consultaMax");		
					
					var Desde =  Ext.util.Format.date(df_consultaNotMin.getValue(),'d/m/Y');   
					var Hasta =  Ext.util.Format.date(df_consultaNotMax.getValue(),'d/m/Y'); 
					
					if (!Ext.isEmpty(df_consultaNotMin.getValue()) &&  Ext.isEmpty(df_consultaNotMax.getValue()) || Ext.isEmpty(df_consultaNotMin.getValue()) &&  !Ext.isEmpty(df_consultaNotMax.getValue()) ) {
						if(!Ext.isEmpty(df_consultaNotMin.getValue()) &&  Ext.isEmpty(df_consultaNotMax.getValue())){
                     df_consultaNotMax.markInvalid('Debe capturar la fecha de Aceptaci�n final');	
                     Ext.getCmp('grid').hide();
                  }
                  if(Ext.isEmpty(df_consultaNotMin.getValue()) &&  !Ext.isEmpty(df_consultaNotMax.getValue())){
                     df_consultaNotMin.markInvalid('Debe capturar la fecha de Aceptaci�n Inicial');	
                     Ext.getCmp('grid').hide();
                  }
						return;
			
					}
				if (!Ext.isEmpty(df_consultaNotMin.getValue()) &&  !Ext.isEmpty(df_consultaNotMax.getValue())){
						if(!isdate(Desde)){
							df_consultaNotMin.markInvalid("Favor de teclear la fecha con el formato  (dd/mm/aaaa)");				
							Ext.getCmp('grid').hide();
							df_consultaNotMin.focus();		
							return;
						}
						 if(!isdate(Hasta)){
							df_consultaNotMax.markInvalid("Favor de teclear la fecha con el formato  (dd/mm/aaaa)");				
							Ext.getCmp('grid').hide();
							df_consultaNotMax.focus();		
							return;
						}
				}
				
				/********   *********/
               Ext.getCmp('grid').show();
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					
					consultaDataGrid.load({
						params: Ext.apply(paramSubmit,{
						operacion: 'Generar',
						start:0,
						limit:15							
					})
				});
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					if(clavePerfil == '4'){
						window.location = '15clausuladoifNaExt.jsp';
					}else{
						window.location = '15clausuladoifext.jsp';
					}
				}
			}
		]
	});//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});
   ObjGeneral.iniciaPrograma();
	catalogoNombreIF.load();
	//catalogoBancoFondeo.load();
});
