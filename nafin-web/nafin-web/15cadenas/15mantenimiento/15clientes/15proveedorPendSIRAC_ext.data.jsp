<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.descuento.*,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.model.catalogos.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");
String infoRegresar	= "";

if(informacion.equals("Consultar")){
	String c_numNE = (request.getParameter("c_numNE")	== null)?"":request.getParameter("c_numNE");
	String c_rfc = (request.getParameter("c_rfc")	== null)?"":request.getParameter("c_rfc");
	String c_estatus = (request.getParameter("c_estatus")	== null)?"":request.getParameter("c_estatus");
	String fec_ini = (request.getParameter("fec_ini")	== null)?"":request.getParameter("fec_ini");
	String fec_fin = (request.getParameter("fec_fin")	== null)?"":request.getParameter("fec_fin");
	String chkRegErrores = (request.getParameter("chkRegErrores")	== null)?"":request.getParameter("chkRegErrores");
	
	ConsProvPendSirac paginador = new  ConsProvPendSirac();
	paginador.setC_numNE(c_numNE);
	paginador.setC_rfc(c_rfc);
	paginador.setC_estatus(c_estatus);
	paginador.setFec_ini(fec_ini);
	paginador.setFec_fin(fec_fin);
	paginador.setChkRegErrores(chkRegErrores);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	try {
		Registros reg = queryHelper.doSearch();
		infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
	} 
	catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
}
%>
<%=infoRegresar%>
