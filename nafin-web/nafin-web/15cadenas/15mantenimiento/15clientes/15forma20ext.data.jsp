<%@ page language="java"%>
<%@ page contentType="application/json;charset=UTF-8" import="  java.util.*,  com.netro.afiliacion.*,  com.netro.cadenas.*,  com.netro.descuento.*,  com.netro.exception.*,  com.netro.model.catalogos.*,  net.sf.json.JSONArray,net.sf.json.JSONObject" errorPage="/00utils/error.jsp"%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<%  

/*** OBJETOS ***/
com.netro.cadenas.ConsProvDistIfCA paginador;
CQueryHelperRegExtJS queryHelperRegExtJS;
CatalogoConsultasIF catConsultasIF;
JSONObject jsonObj;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
/**** *****/
String 	sTipoAfiliado  = ( request.getParameter("sTipoAfiliado") == null ) ? "" : request.getParameter("sTipoAfiliado");
String 	sPYME 	  		= ( request.getParameter("sPYME") == null ) ? "" : request.getParameter("sPYME");
String 	sNombre   		= ( request.getParameter("sNombre") == null ) ? "" : request.getParameter("sNombre");
String 	sIcEdo   		= ( request.getParameter("sIcEdo") == null ) ? "" : request.getParameter("sIcEdo");
String 	sRFC   			= ( request.getParameter("sRFC") == null ) ? "" : request.getParameter("sRFC");
String 	sIcEpo   		= ( request.getParameter("sIcEpo") == null ) ? "" : request.getParameter("sIcEpo");
String 	ic_if   			= ( request.getParameter("ic_if") == null ) ? "" : request.getParameter("ic_if");
String suceptibleFloating = (request.getParameter("suceptibleFloating")   != null && request.getParameter("suceptibleFloating").equals("true"))  ?"S":"";

/**** ****/
String resultado			= null;
int start					= 0;
int limit 					= 2;
AccesoDB con 				= null;
/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/
//sPYME = "%";
if(strTipoUsuario.equals("IF")){
 ic_if = iNoCliente;
}

/*** INICIO CATALOGO TIPO AFILIADO ***/
if(informacion.equals("catalogoAfiliado")){
	resultado = "";
} /*** FIN TIPO AFILIADO ***/

/*** INICIO CATALOGO ESTADOS ***/
else if(informacion.equals("catalogoEstados")){
	catConsultasIF = new CatalogoConsultasIF("edo");
	catConsultasIF.setCampoClave("ic_estado");
	catConsultasIF.setCampoDescripcion("cd_nombre");
	catConsultasIF.setOrden("cd_nombre");
	catConsultasIF.setSTipoAfiliado(sTipoAfiliado);
	catConsultasIF.setTipoCatalogo(informacion);
   catConsultasIF.setINoCliente(iNoCliente);
	resultado = catConsultasIF.getJSONElementos();
} /*** FIN ESTADOS ***/
	
/*** INICIO CATALOGO EPO ***/
else if(informacion.equals("catalogoEpo")){	
	catConsultasIF = new CatalogoConsultasIF("e");
	catConsultasIF.setCampoClave("ic_epo");
	catConsultasIF.setCampoDescripcion("cg_razon_social");
	catConsultasIF.setOrden("e.cg_razon_social");
	catConsultasIF.setSTipoAfiliado(sTipoAfiliado);
	catConsultasIF.setTipoCatalogo(informacion);
   catConsultasIF.setINoCliente(iNoCliente);
	resultado = catConsultasIF.getJSONElementos();
} /*** FIN EPO ***/

/*** INICIO CONSULTA ***/
else if(informacion.equals("Consulta")) {
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));				
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	paginador = new ConsProvDistIfCA();
	System.err.println("sTipoAfiliado : " + sTipoAfiliado);
	String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
	/* SE PASAN LOS VALORES AL OBJETO PAGINADOR PARA REALIZAR LA CONSULTA */
	paginador.setSTipoAfiliado(sTipoAfiliado);
	paginador.setSNombre(sNombre);
	paginador.setSPYME(sPYME);
	paginador.setSIcEdo(sIcEdo);
	paginador.setSIcEpo(sIcEpo);
	paginador.setSRFC(sRFC);
	paginador.setSes_ic_if(ic_if);	
	paginador.setSuceptibleFloating(suceptibleFloating);
	
	queryHelperRegExtJS	= new CQueryHelperRegExtJS( paginador ); 
	
	if((informacion.equals("Consulta") && operacion.equals("Generar")) || (informacion.equals("Consulta") && operacion.equals(""))){ 
		try {
			if(operacion.equals("Generar")){ //Nueva consulta
				queryHelperRegExtJS.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			String cadena = queryHelperRegExtJS.getJSONPageResultSet(request,start,limit);			
			resultado	= cadena;
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if(operacion.equals("XLS")||operacion.equals("PDF")){
		jsonObj = new JSONObject();
		String nombreArchivo = queryHelperRegExtJS.getCreateCustomFile(request, strDirectorioTemp,operacion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
	}else if(operacion.equals("PDFPrint")){
		jsonObj = new JSONObject();
		String nombreArchivo = queryHelperRegExtJS.getCreatePageCustomFile(request, start,limit, strDirectorioTemp,operacion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
	}	
} /*** FIN DE CONSULTA ***/

%>
<%=  resultado%>
