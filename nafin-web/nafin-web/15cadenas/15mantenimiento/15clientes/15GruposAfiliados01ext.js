Ext.onReady(function(){


//--------------------------------HANDLERS-----------------------------------
	
	var resetModificar = function(){
		Ext.getCmp('txtClave').reset();
		Ext.getCmp('txtAuxiliar').reset();
		Ext.getCmp('txtModificar').reset();
		Ext.getCmp('winModificar').hide()
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts){
		var grid = Ext.getCmp('grid');
		grid.el.mask('Leyendo...','x-mask-loading');
		if(arrRegistros != null){
			if(!grid.isVisible()){
				grid.show();
			}
			if(store.getTotalCount() <= 0){
				grid.el.mask('No se encontro ning�n registro', 'x-mask');
			}else{
				grid.el.unmask();
			}
			
		}
	}
	
	var procesarSuccessFailureInsertar = function(opts, success, response){
		var boton = Ext.getCmp('btnAceptar');
		var descripcion = Ext.getCmp('txtDescripcion');
		if(success == true){
			var resp = 	Ext.util.JSON.decode(response.responseText);
			if(resp.textoMensaje == 'existe'){
				descripcion.markInvalid('El grupo ya se encuentra registrado en el cat�logo');
			}else{
				descripcion.reset();
				consultaData.load();
			}
			boton.enable();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var mostrarVentanaModificar = function(grid, rowIndex, colIndex, item, event){
		var clave = Ext.getCmp('txtClave');
		var auxiliar = Ext.getCmp('txtAuxiliar');
		var descripcion = Ext.getCmp('txtModificar');		
		var registro = grid.getStore().getAt(rowIndex);
		clave.setValue(registro.get('grupo'));
		descripcion.setValue(registro.get('descripcion'));
		auxiliar.setValue(registro.get('descripcion'));
		var aux = Ext.getCmp('winModificar').show();
	}
	
	var eliminarGrupo = function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		Ext.Msg.show({
			title: 'Confirmar',
			msg: '�Desea eliminar el registro?',
			buttons: Ext.Msg.OKCANCEL,
			fn: function peticionAjax(btn){
				if (btn == 'ok'){
					Ext.Ajax.request({
						url: '15GruposAfiliados01ext.data.jsp',
						params: {
							informacion: 'eliminarGrupo',
							claveGrupo: registro.get('grupo')
						},
						callback: procesarSuccessFailureModificar
					});
				}
			}
		});
	}
	
	var modificarGrupo = function(){
		var clave = Ext.getCmp('txtClave');
		var auxiliar = Ext.getCmp('txtAuxiliar');
		var descripcion = Ext.getCmp('txtModificar');
		if(descripcion.getValue() == auxiliar.getValue()){
			descripcion.markInvalid('El grupo ya se encuentra registrado en el cat�logo');
		}else{
			Ext.Ajax.request({
				url: '15GruposAfiliados01ext.data.jsp',
				params:{
					informacion: 'modificarGrupo',
					txtDescripcion: descripcion.getValue(),
					claveGrupo: clave.getValue()
				},
				callback: procesarSuccessFailureModificar
			});
		}
	}
	
	var procesarSuccessFailureModificar = function(opts, success, response){
		if(success == true){
			resetModificar();
			consultaData.load();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}


//---------------------------------STORES------------------------------------
	
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15GruposAfiliados01ext.data.jsp',
		baseParams:{
			informacion: 'consultaInicial'
		},
		fields: [
			{name: 'grupo'},
			{name: 'descripcion'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
				//	procesarConsultaInicialData(null, null, null);					
				}
			}
		}
	});


//------------------------------COMPONENTES----------------------------------
	
	var fpBotones = new Ext.Container ({
		layout: 'table',
		id: 'fpBotones',
		width: '300',
		heigth: 'auto',
		style: 'margin: 0 auto',
		align: 'center',
		defaults:{
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
			{
				xtype: 'button',
				text: '<b>Captura de Grupos</b>',
				id: 'btnCapturaGrupos',
				handler: function(){
					window.location = '15GruposAfiliados01ext.jsp'
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Parametrizaci�n EPO�s',
				id: 'btnParametrizacion',
				handler: function(){
					window.location = '15GruposAfiliParam01ext.jsp'
				}
			}
		]
	});
	
	var elementosForma = [{
		xtype: 'textfield',
		name:	'_txtDescripcion',
		id: 'txtDescripcion',
		hiddenName:	'txt_descripcion',
		fieldLabel: 'Descripci�n',
		maxLength: 50,
		allowBlank:	true,
		margins: '0 20 0 0'	
	}];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 300,
		frame: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 70,
		labelAlign : 'right',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid: true,
		buttons: [		
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					var descripcion = Ext.getCmp('txtDescripcion');
					descripcion.reset();

				}
			},
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoBuscar',
				formBind: false,
				handler: function(boton, evento){
					
					var descripcion = Ext.getCmp('txtDescripcion');					
					if(Ext.isEmpty(descripcion.getValue())){					
						descripcion.markInvalid('El valor de la descripci�n es requerido');
						return;	
					}	
					Ext.getCmp('btnAceptar').disable();
					Ext.Ajax.request({
						url: '15GruposAfiliados01ext.data.jsp',
						params: {
							informacion: 'insertarGrupo',
							txtDescripcion: descripcion.getValue()
						},
						callback: procesarSuccessFailureInsertar
					});					
				}
			}
		]
	});
	
	var grid = new Ext.grid.GridPanel({
		id: 'grid',
		store: consultaData,
		height: 300,
		width: 395,
		hidden: false,
		margins: '20 0 0 0',
		align: 'center',
		style: 'margin:0 auto',
		columns: [
			{
				header: 'Clave',
				tooltip: 'Clave',
				width: 70,
				dataIndex: 'grupo',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Descripci�n',
				tooltip: 'Descripci�n',
				width: 200,
				dataIndex: 'descripcion',
				sortable: true,
				resizable: true,
				align: 'left'
			},
			{
				xtype: 'actioncolumn',
				header: 'Seleccionar',
				tooltip: 'Seleccionar',
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Modificar';
							return 'icoModificar';										
						},
						handler: mostrarVentanaModificar
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[1].tooltip = 'Eliminar';
							return 'cancelar';										
						},
						handler: eliminarGrupo
					}
				]
			}
		]
	});
	
	var elementosModificar =[
		{
			xtype: 'textfield',
			name: '_txtClave',
			id: 'txtClave',
			hiddenName: 'txt_clave',
			maxLength: 10,
			allowBlank: false,
			margins: '0 20 0 0',
			hidden: true
		},
		{
			xtype: 'textfield',
			name: '_txtAuxiliar',
			id: 'txtAuxiliar',
			hiddenName: 'txt_auxiliar',
			maxLength: 50,
			allowBlank: false,
			margins: '0 20 0 0',
			hidden: true
		},
		{
			xtype: 'textfield',
			name: '_txtModificar',
			id: 'txtModificar',
			hiddenName: 'txt_modificar',
			fieldLabel: 'Descripci�n',
			maxLength: 50,
			allowBlank: false,
			margins: '0 20 0 0'		
		}
	];
	
	var fpModificar = new Ext.form.FormPanel({
		id: 'formaModificar',
		width: 290,
		style: 'margin:0 auto',
		collapsible: false,
		titleCollapse: false,
		frame: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 70,
		defaults: {	
			msgTarget: 'side',
			anchor:	'-20'	
		},
		items: elementosModificar,
		monitorValid: true,
		buttons:[
			{
				text: 'Modificar',
				id: 'btnModificar',
				iconCls: 'icoModificar',
				formBind: true,
				handler: function(boton, evento){
					modificarGrupo();		
				}
			},
			{
				text: 'Cancelar',
				id: 'btnCancelar',
				iconCls: 'cancelar',
				handler: function() {
					resetModificar();
				}
			}
		]
	});
	
	var windowModificar = new Ext.Window({
		id: 'winModificar',
		width: 300,
		height: 'auto',
		modal: true,
		closeAction: 'hide',
		title: 'Modificar Grupo',
		items:[
			fpModificar
		]
	});
	

//-------------------------COMPONENTE PRINCIPAL------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 900,
		height: 'auto',
		items:[
			NE.util.getEspaciador(25),
			fpBotones,
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});
	
	consultaData.load();
});