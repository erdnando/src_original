<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	JSONObject resultado = new JSONObject();
	
	 if(informacion.equals("catalogoContragarante")) {		

		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("gti_contragarante");
		cat.setCampoClave("ic_contragarante");
		cat.setCampoDescripcion("ic_contragarante||' - '||cg_descripcion");
		cat.setOrden("ic_contragarante");
		infoRegresar = cat.getJSONElementos();		
	} else if(informacion.equals("AltaContragarante"))	{
		
		String 	Razon_Social  				= (request.getParameter("Razon_Social") == null) ? "" : request.getParameter("Razon_Social"),
					Nombre_comercial 			= (request.getParameter("nombre_comercial") == null) ? "" : request.getParameter("nombre_comercial"),
					R_F_C         				= (request.getParameter("rfc")    == null) ? "" : request.getParameter("rfc");		
		//Domicilio		
		String   Calle        				= (request.getParameter("calle")    == null) ? "" : request.getParameter("calle"),
					Colonia       				= (request.getParameter("colonia")  == null) ? "" : request.getParameter("colonia"),
					Codigo_postal 				= (request.getParameter("code") == null) ? "" : request.getParameter("code"),
					Pais          				= (request.getParameter("cmb_pais")     == null) ? "" : request.getParameter("cmb_pais"),
					Estado        				= (request.getParameter("cmb_estado")   == null) ? "" : request.getParameter("cmb_estado"),
					Delegacion_o_municipio 	= (request.getParameter("delegacionMunicipio") == null) ? "" : request.getParameter("delegacionMunicipio"),
					Telefono      				= (request.getParameter("telefono") == null) ? "" : request.getParameter("telefono"),
					Email         				= (request.getParameter("email")    == null) ? "" : request.getParameter("email"),
					Fax           				= (request.getParameter("fax")      == null) ? "" : request.getParameter("fax"),
					Oficina_controladora 			= (request.getParameter("Oficina_controladora") == null) ? "" : request.getParameter("Oficina_controladora").replaceAll(",", ""),
					Domicilio_correspondencia		=(request.getParameter("Domicilio_correspondencia") == null) ? "" : request.getParameter("Domicilio_correspondencia").replaceAll(",", ""),
					contragarante           =  (request.getParameter("clave_contragarante")      == null) ? "" : request.getParameter("clave_contragarante");
							
		String lsNoNafinElectronico = new String();
	
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	
		System.out.println("Razon Social: "+Razon_Social+" Nombre Comercial: "+Nombre_comercial+" RFC: "+R_F_C.toUpperCase()+" Calle: "+Calle.toUpperCase()+" Colonia: "+Colonia.toUpperCase() );
		System.out.println("CP: "+Codigo_postal+" Pais: "+Pais+" Estado: "+Estado+" Del: "+Delegacion_o_municipio+" Telefono: "+Telefono);
		System.out.println("Mail: "+Email+ " Fax: "+Fax+" OficinaC: "+Oficina_controladora+" DomicilioCorrespondencia: "+Domicilio_correspondencia	);
		System.out.println("Contragarante: "+contragarante);
		
		lsNoNafinElectronico = BeanAfiliacion.afiliaContragarante(iNoUsuario, Razon_Social,Nombre_comercial,R_F_C,Domicilio_correspondencia,Oficina_controladora,Calle,Colonia,Estado,Pais,Delegacion_o_municipio,Codigo_postal,Telefono, Email,Fax,contragarante);
	
		resultado.put("success", new Boolean(true));
		resultado.put("lsNoNafinElectronico",lsNoNafinElectronico);
		infoRegresar = resultado.toString();	
	}
	
%>
<%=infoRegresar%>