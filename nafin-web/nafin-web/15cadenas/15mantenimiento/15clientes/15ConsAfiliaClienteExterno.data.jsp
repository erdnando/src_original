<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,	
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject,		
		com.netro.cadenas.*,
		netropology.utilerias.usuarios.*,
		netropology.utilerias.*,
		java.sql.*,
		com.netro.seguridad.*, 
		com.netro.exception.*, 
		com.netro.afiliacion.*,		
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	
String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String num_electronico = (request.getParameter("num_electronico") != null)?request.getParameter("num_electronico"):"";
String operacion = (request.getParameter("operacion") != null)?request.getParameter("operacion"):""; 
String nombre = (request.getParameter("nombre") != null)?request.getParameter("nombre"):""; 
String tituloC = (request.getParameter("tituloC") != null)?request.getParameter("tituloC"):"";  
String tipoAfiliado = (request.getParameter("tipoAfiliado") != null)?request.getParameter("tipoAfiliado"):"";  
String txtLogin = (request.getParameter("txtLogin") != null)?request.getParameter("txtLogin"):"";   
String modificar = (request.getParameter("modificar") != null)?request.getParameter("modificar"):"";   



String infoRegresar ="", consulta ="", msg ="",  mensaje ="";
int  start= 0, limit =0;

JSONObject jsonObj = new JSONObject();

ConsClienteExterno paginador = new ConsClienteExterno();	 
paginador.setNumElectronico(num_electronico);
paginador.setNombre(nombre); 

CambioPerfil clase = new  CambioPerfil();

Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);

if(informacion.equals("catalogoPerfil") ){  

	HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  tipoAfiliado  );	
	List perfiles = (List)catPerfil.get("PERFILES");	
	System.out.println("perfiles");
	Iterator it = perfiles.iterator();	
	HashMap datos = new HashMap();
	List registros  = new ArrayList();	
	while(it.hasNext()) {
	
		List campos = (List) it.next();
		String clave = (String) campos.get(0);
		datos = new HashMap();
		datos.put("clave", clave );
		datos.put("descripcion", clave );
		registros.add(datos);
	}
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();
	System.out.println("infoRegresar -- "+infoRegresar);


}else  if(informacion.equals("Consultar")  || informacion.equals("GeneraArchivoPDF")   ||  informacion.equals("GeneraArchivoCSV")  )	{
		
 	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if(informacion.equals("Consultar") ||  informacion.equals("GeneraArchivoPDF") ) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
					
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	
		if(informacion.equals("Consultar")) {
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);										
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
				
			jsonObj = JSONObject.fromObject(consulta);				
	
		}else  if ( informacion.equals("GeneraArchivoPDF") ) {
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
			
	}else  if ( informacion.equals("GeneraArchivoCSV") ) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}			
	}
	System.out.println("nombreArchivo "+jsonObj.toString());
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("catalogoBuscaAvanzada"))	{ 
	String nombrePyme = (request.getParameter("nombrePyme") != null)?request.getParameter("nombrePyme"):""; 
	String rfcPyme = (request.getParameter("rfcPyme") != null)?request.getParameter("rfcPyme"):""; 
	String num_ClienteSirac = (request.getParameter("num_ClienteSirac") != null)?request.getParameter("num_ClienteSirac"):""; 
	List  registros =  getBusquedaAvanzada ( nombrePyme,  rfcPyme,  num_ClienteSirac  );
	JSONArray jsonA = new JSONArray();	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	if(registros.isEmpty()){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if(registros.size() > 0 && registros.size() < 1001 ){
		jsonA = JSONArray.fromObject(registros);
		jsonObj.put("total",  Integer.toString(jsonA.size()));
		jsonObj.put("registros", registros );
	}else if(registros.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("Eliminar"))	{	

	num_electronico = (request.getParameter("num_electronico") != null)?request.getParameter("num_electronico"):""; 
	String razon_social = (request.getParameter("razon_social") != null)?request.getParameter("razon_social"):""; 
	String rfc = (request.getParameter("rfc") != null)?request.getParameter("rfc"):""; 
	String num_sirac = (request.getParameter("num_sirac") != null)?request.getParameter("num_sirac"):""; 
	String domicilio = (request.getParameter("domicilio") != null)?request.getParameter("domicilio"):""; 
	String estado = (request.getParameter("estado") != null)?request.getParameter("estado"):""; 
	String telefono = (request.getParameter("telefono") != null)?request.getParameter("telefono"):""; 
	
	List	datosCliente =   new ArrayList();
	datosCliente.add(num_electronico);
	datosCliente.add(razon_social.replace(',',' '));
	datosCliente.add(rfc);
	datosCliente.add(num_sirac);
	datosCliente.add(domicilio.replace(',',' '));
	datosCliente.add(estado);
	datosCliente.add(telefono); 
	
	try { 
		msg = "<center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+afiliacion.borraClienteExterno(num_electronico, iNoUsuario,  datosCliente)+"</center>";			
	} catch(NafinException lexError) {
		msg = lexError.getMsgError();		
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("msg", msg);	
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("ConsCuentas") ||  informacion.equals("ImprimirConsCuentas")  )	{

	UtilUsr utilUsr = new UtilUsr();
	List cuentas = utilUsr.getUsuariosxAfiliado(num_electronico, tipoAfiliado);
	Iterator itCuentas = cuentas.iterator();	
	
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	if (informacion.equals("ConsCuentas"))  {
		while (itCuentas.hasNext()) {
			String cuenta = (String) itCuentas.next();		
			datos = new HashMap();		
			datos.put("CUENTA", cuenta );				
			registros.add(datos);
		}	
	
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
	}else  if( informacion.equals("ImprimirConsCuentas") )  {  
	
		String nombreArchivo = paginador.imprimirCuentas(request, cuentas, tituloC, strDirectorioTemp );		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	}
	
	infoRegresar = jsonObj.toString();	
	
}else  if( informacion.equals("informacionUsuario") ){  
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true));
	
	HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  "S"  );	
	
	if(catPerfil.size()>0){
	
		List perfiles = (List)catPerfil.get("PERFILES");
	
		String lblNombreEmpresa = (String)catPerfil.get("EMPRESA");
		String lblNafinElec = (String)catPerfil.get("NAELECTRONICO");
		String lblNombre = (String)catPerfil.get("NOMBRE_USUARIO");
		String lblApellidoPaterno = (String)catPerfil.get("APELLIDO_PATERNO");
		String lblApellidoMaterno = (String)catPerfil.get("APELLIDO_MATERNO");
		String lblEmail = (String)catPerfil.get("EMAIL");
		String lblPerfilActual = (String)catPerfil.get("PERFIL_ACTUAL");
		String internacional = (String)catPerfil.get("INTERNACIONAL");
		String sTipoAfiliado = (String)catPerfil.get("TIPOAFILIADO");		
			
		jsonObj.put("lblNombreEmpresa", lblNombreEmpresa);
		jsonObj.put("lblNafinElec", lblNafinElec);
		jsonObj.put("lblNombre", lblNombre);
		jsonObj.put("lblApellidoPaterno", lblApellidoPaterno);
		jsonObj.put("lblApellidoMaterno", lblApellidoMaterno);
		jsonObj.put("lblEmail", lblEmail);
		jsonObj.put("lblPerfilActual", lblPerfilActual);
		jsonObj.put("txtLogin", txtLogin);
		jsonObj.put("mensaje", mensaje);	
		
		jsonObj.put("internacional", internacional);
		jsonObj.put("sTipoAfiliado", sTipoAfiliado);	
		jsonObj.put("txtNafinElectronico", lblNafinElec);	
		jsonObj.put("txtPerfilAnt", lblPerfilActual);	
		jsonObj.put("modificar", modificar);
				
	}else  {
		mensaje = "No se encontró el usuario con la clave especificada, por favor vuelva a intentarlo";
		jsonObj.put("mensaje", mensaje);
	}
	
	infoRegresar = jsonObj.toString(); 

}else  if( informacion.equals("ModifiarPerfil") ){ 

	String txtPerfil = (request.getParameter("txtPerfil")==null)?"":request.getParameter("txtPerfil");
	String txtNafinElectronico = (request.getParameter("txtNafinElectronico")==null)?"":request.getParameter("txtNafinElectronico");
	String txtPerfilAnt = (request.getParameter("txtPerfilAnt")==null)?"":request.getParameter("txtPerfilAnt");
	String internacional = (request.getParameter("internacional")==null)?"":request.getParameter("internacional");
	String txtTipoAfiliado = (request.getParameter("sTipoAfiliado")==null)?"":request.getParameter("sTipoAfiliado");
	String clave_usuario = (String)session.getAttribute("Clave_usuario");
	String txtLoginC = (request.getParameter("txtLoginC") != null) ? request.getParameter("txtLoginC") : "";
	
	
	
	mensaje =  clase.getConsultaUsuario( txtLoginC,  txtPerfil, txtPerfilAnt,  txtNafinElectronico ,  internacional,  txtTipoAfiliado, 	 clave_usuario ) ;

	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("mensaje", mensaje);
	infoRegresar = jsonObj.toString();

}
 
 
 
 
%>
<%=infoRegresar%>


<%!

	
	public List   getBusquedaAvanzada ( String nombre_pyme, String  rfc_pyme, String  num_clienSirac     ) throws AppException { 
		System.out.println("   getBusquedaAvanzada (S) ");		
		StringBuffer	qrySentencia 	= new StringBuffer();		
		List  lVarBind		= new ArrayList();
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		AccesoDB con = new AccesoDB(); 		
		boolean exito = true;
		HashMap	datos = new HashMap();	
		List  registros		= new ArrayList();
				
		try{
		
			con.conexionDB();
			
			qrySentencia 	= new StringBuffer();	
			qrySentencia.append(" select   "+
			" c.IC_NAFIN_ELECTRONICO AS  CLAVE ,   "+
			" DECODE (c.CS_TIPO_PERSONA,  'F', c.CG_NOMBRE || ' ' || c.CG_APPAT || ' ' || c.CG_APMAT ,  c.CG_RAZON_SOCIAL  ) AS DESCRIPCION "+
			"  FROM COMCAT_CLI_EXTERNO  c  "+
			"  where  1 = 1  "); 
				lVarBind		= new ArrayList(); 
		 
			if(!"".equals(nombre_pyme)){		 
			qrySentencia.append("	AND (   (UPPER (TRIM (c.cg_razon_social)) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%'))	"+
										"			 OR (UPPER (TRIM (c.cg_nombre || ' ' || c.cg_appat || ' ' || c.cg_apmat)) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%'))	"+
										"		)"
										) ;
				lVarBind.add(nombre_pyme+"%");
				lVarBind.add(nombre_pyme+"%");
				
			}
			if(!"".equals(rfc_pyme)){	
				qrySentencia.append(" AND C.cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
				lVarBind.add(rfc_pyme+"%");		 
			}
			
			if(!"".equals(num_clienSirac)){	
				qrySentencia.append( "    AND C.IN_NUMERO_SIRAC LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
				lVarBind.add(num_clienSirac+"%");		 
			}
			
			System.out.println(" qrySentencia " + qrySentencia);
			System.out.println(" lVarBind " + lVarBind); 
			
			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();			
			while(rs.next()){
			   datos = new HashMap();
				datos.put("CLAVE", rs.getString("CLAVE")==null?"":rs.getString("CLAVE"));
				datos.put("DESCRIPCION", rs.getString("DESCRIPCION")==null?"":rs.getString("DESCRIPCION"));
				registros.add(datos); 
			}
			rs.close();
			ps.close();
		
			
		
		} catch (Exception e) {
			exito = false;
			e.printStackTrace();
			System.out.println(" getBusquedaAvanzada  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				System.out.println("   getBusquedaAvanzada (S) ");
			}
		} 
		return registros;
		
	}
	%>