Ext.onReady(function(){
	
	var ObjCatalogo = {
		cat1:false,
		cat2:false,
		cat3:false,
		cat4:false,
		cat5:false,
		cat6:false,
		cat7:false
	}
	var sTipoUsuario =  Ext.getDom('sTipoUsuario').value;	
	var idMenu =  Ext.getDom('idMenu').value;
	
	var ic_epo = window.location.search 
                 ? Ext.urlDecode(window.location.search.substring(1)).ic_epo : '1'; //: ''; cambiar!!!
   
	var txtaction = window.location.search 
   ? Ext.urlDecode(window.location.search.substring(1)).txtaction  : 'mod'; //: ''; 
   
	var internacional = window.location.search 
   ? Ext.urlDecode(window.location.search.substring(1)).internacional : 'N'; //: ''; 
  
	
	var pais_s, edo, municipio, ramosiaff, unidad, sectorecon, subsectorecon, ramaeco, sector, subdir, promotor ;
	var total,ic_producto,nombre_producto,chk_dispersion,chk_doctos ;
	var primera_vez = true;		
	var primera_vez_estado = true, primera_vez_municipio = true, primera_vez_unidad = true, primera_vez_subsector = true, primera_vez_rama = true;
/*--------------------------------- Handler's -------------------------------*/
	
		
	
	function procesaValoresIniciales(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var  info = Ext.util.JSON.decode(response.responseText);			
		
			var  total= info.permisos.length;
			if(total>0) {
				for(i=0;i<total;i++){ 
					boton=  info.permisos[i];				
					Ext.getCmp(boton).show();	 	
				}		
			}
				
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var validaCatalogos = function(){
		if(NE.util.allTrue(ObjCatalogo) && primera_vez == true){
			primera_vez = false;
			//ajax
			Ext.Ajax.request({
				url: '15forma08Ext.data.jsp',
				params: Ext.apply({
					informacion: 'ConsultaEPO',
					ic_epo : ic_epo
				}),				
				callback: procesarSuccessFailureConsultaEPO
			});				
	
		}
	}
	
	var procesarSuccessFailureModificaEPO = function(opts, success, response) 
   {
		var cmpForma = Ext.getCmp('formaAfiliacion');
		cmpForma.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('',Ext.util.JSON.decode(response.responseText).msg, function() {
				window.location = '15forma5ext.jsp?idMenu='+idMenu;
			}, this);	
			
		}
		else {
			Ext.Msg.alert('','Ocurrio un error durante la actualizacion de datos.');	
		}
	}

	var procesarSuccessFailureConsultaEPO = function(opts, success, response) 
   {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			var reg = Ext.util.JSON.decode(response.responseText).registros[0];
			if (reg != null) 
			{	
			
				Ext.getCmp('id_domicilio').setValue(reg.ic_domicilio);
				Ext.getCmp('id_contacto').setValue(reg.ic_contacto);
				Ext.getCmp('Razon_Social').setValue(reg.Razon_Social);
				Ext.getCmp('nombre_comercial').setValue(reg.Nombre_comercial);
				Ext.getCmp('rfc').setValue(reg.R_F_C);				
				
				Ext.getCmp('calle').setValue(reg.Calle);
				Ext.getCmp('num_ext').setValue(reg.Numero_Exterior);
				Ext.getCmp('num_int').setValue(reg.Numero_interior);
				Ext.getCmp('colonia').setValue(reg.Colonia);
				Ext.getCmp('id_cmb_pais').setValue(reg.Pais);
				Ext.getCmp('id_cmb_estado').setValue(reg.Estado);
				Ext.getCmp('id_cmb_delegacion').setValue(reg.Delegacion_o_municipio);
				Ext.getCmp('code').setValue(reg.Codigo_postal);
				Ext.getCmp('telefono').setValue(reg.Telefono);
				Ext.getCmp('email').setValue(reg.Email);
				Ext.getCmp('fax').setValue(reg.Fax);
			
				Ext.getCmp('fch_poder').setValue(reg.Fecha_del_poder_notarial);
				Ext.getCmp('paterno').setValue(reg.Apellido_paterno_L);
				Ext.getCmp('nombre').setValue(reg.Nombre_L);
				Ext.getCmp('SSId_Number').setValue(reg.SSId_Number);
				Ext.getCmp('num_escritura').setValue(reg.Numero_de_escritura);
				Ext.getCmp('materno').setValue(reg.Apellido_materno_L);
				Ext.getCmp('num_sirac').setValue(reg.no_sirac);
				Ext.getCmp('bco_retiro').setValue(reg.cg_banco_retiro);
				Ext.getCmp('cuenta').setValue(reg.cg_cta_retiro);
				Ext.getCmp('id_cmb_economico').setValue(reg.Sector_Econ);
				Ext.getCmp('id_cmb_subsectoreco').setValue(reg.Subsector_econ);
				Ext.getCmp('id_cmb_ramaECO').setValue(reg.rama_eco);
				Ext.getCmp('id_cmb_sector').setValue(reg.Sector_Epo);
				Ext.getCmp('id_cmb_subdirec').setValue(reg.Subdireccion);
				Ext.getCmp('id_cmb_promotor').setValue(reg.Lider_Promotor);
				
				Ext.getCmp('id_cmb_ramo').setValue(reg.ramo_siaff);
				Ext.getCmp('id_cmb_unidad').setValue(reg.unidad_siaff);
				
				total = reg.Total;
				/* Checar en true o false los checks dependiendo de su valor */
				var check = reg.c_proveedores;
				if(check != null && check == 'S'){	
					Ext.getCmp('id_c_proveedores').setValue('S'); 
				}else {
					Ext.getCmp('id_c_proveedores').setValue('N'); 
				}
				
				check = reg.chkFinanciamiento;
				if(check != null && check == 'S'){	
					Ext.getCmp('id_ditribuidores').setValue(true); 
				}else {
					Ext.getCmp('id_ditribuidores').setValue(false); 
				}
				
				check = reg.convenio_unico;
				if(!Ext.isEmpty(check) && check == 'S'){
					Ext.getCmp("id_convenio").setValue(true); 
				}else{
					Ext.getCmp("id_convenio").setValue(false); 
				}
				
				check = reg.mandato_documento;
				if(check != null  && check == 'S'){	
					Ext.getCmp('id_irrevocable').setValue(true); 
				}else {
					Ext.getCmp('id_irrevocable').setValue(false); 
				}

	
				/* Guardo la informacion para llenarla en los combos */
				pais_s = reg.Pais;
				edo 	 = reg.Estado;
				municipio = reg.Delegacion_o_municipio;
				ramosiaff = reg.ramo_siaff;
				unidad = reg.unidad_siaff;
				sectorecon = reg.Sector_Econ;
				subsectorecon = reg.Subsector_econ;
				ramaeco = reg.rama_eco;
				sector = reg.Sector_Epo;
				subdir = reg.Subdireccion;
				promotor = reg.Lider_Promotor;
				
				/* Carga de combos */
				catalogoEstado.load({
								params : Ext.apply({
									pais : pais_s
								})
							});
				catalogoMunicipio.load({
								params : Ext.apply({
									pais : pais_s,
									estado : edo
								})
							});
				catalogoUnidadSIAFF.load({
								params: Ext.apply({
									ramo: ramosiaff
							})
						});
				if(!Ext.isEmpty(sectorecon)) {
					catalogoSubSectorECO.load({
							params: Ext.apply({
								sectorECO :sectorecon
							})
					});
					catalogoRamaECO.load({
							params: Ext.apply({
								sectorECO :sectorecon,
								subsectorECO : subsectorecon					
							})
					});
				}
				
			}
		}
	}
	
	
	function procesarPermiso(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var  info = Ext.util.JSON.decode(response.responseText);			
				
			//********************** Validaciones ********************/
					//FODEA 034 - 2009 ACF
				var ramo = Ext.getCmp('id_cmb_ramo');
					if(!Ext.isEmpty(ramo.getValue())){
						var unidad = Ext.getCmp('id_cmb_unidad');
						if(Ext.isEmpty(unidad.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Seleccione una unidad SIAFF para la Cadena Productiva.' );
							unidad.focus();
							return;
						}
					}
					var chkFinanciamiento = Ext.getCmp('id_ditribuidores');
					if(chkFinanciamiento.getValue() == 'S' ) {
						var cboTipoAfiliacion = Ext.getCmp('id_afiliacion');
						if(!Ext.isEmpty(cboTipoAfiliacion.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Seleccione una Afiliacion' );
							cboTipoAfiliacion.focus();
							return;
						}
					}
					
					
					// --N�mericas--
					var num_ext = Ext.getCmp('num_ext');
					var num_int = Ext.getCmp('num_int');
					var num_cod = Ext.getCmp('code');
					var num_tel = Ext.getCmp('telefono');
					var num_fax = Ext.getCmp('fax');
					var num_esc = Ext.getCmp('num_escritura');
					var num_sir = Ext.getCmp('num_sirac');
					var num_cta = Ext.getCmp('cuenta');
			
					if (!esVacio(num_ext.getValue())){//if(!Ext.isEmpty(num_ext)){
						if (!isdigit(num_ext.getValue())){
							num_ext.focus();
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo N�mero Exterior ' );
							
							return;
					}}							
					if (!esVacio(num_int.getValue())){//if(!Ext.isEmpty(num_int)){
						if (!isdigit(num_int.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo N�mero Interior ' );
							num_int.focus();
							return;
					}}		
					if (!esVacio(num_cod.getValue())){//if(!Ext.isEmpty(num_cod)){
						if (!isdigit(num_cod.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo C�digo Postal ' );
							num_cod.focus();
							return;
					}}		
					if (!esVacio(num_fax.getValue())){//if(!Ext.isEmpty(num_fax)){
						if (!isdigit(num_fax.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo Fax ' );
							num_fax.focus();
							return;
					}}		
					
					if (!esVacio(num_esc.getValue())){//if(!Ext.isEmpty(num_esc)){
						if (!isdigit(num_esc.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo N�mero de Escritura ' );
							num_esc.focus();
							return;
					}}		
					
					if (!esVacio(num_sir.getValue())){//if(!Ext.isEmpty(num_sir)){
						
						if (!isdigit(num_sir.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo N�mero Sirac ' );
							num_sir.focus();
							return;
					}}
					if (!esVacio(num_cta.getValue())){//if(!Ext.isEmpty(num_cta)){
						if (!isdigit(num_cta.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo N�mero de la Cuenta de Retiro' );
							num_cta.focus();
							return;
					}}		
					if (!esVacio(num_tel.getValue())){//if(!Ext.isEmpty(num_tel)){
						if (!isdigit(num_tel.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo Tel�fono ' );
							num_tel.focus();
							return;
					}}		
					
					
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							var cmpForma = Ext.getCmp('formaAfiliacion');
							var params = (cmpForma)?cmpForma.getForm().getValues():{};				
							cmpForma.el.mask("Procesando", 'x-mask-loading');
																	
							Ext.Ajax.request({
									url: '15forma08Ext.data.jsp',
									params: Ext.apply(params, {										
											informacion: 'ModificaEPO',
											ic_epo: ic_epo
									}),
									callback: procesarSuccessFailureModificaEPO
							});
						}
					});	
					
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
  
  var procesarCatalogoBanco = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {			
			ObjCatalogo.cat1=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoPais = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			//Ext.getCmp('id_cmb_pais').setValue(pais);
			ObjCatalogo.cat2=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoEstado = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			if(primera_vez_estado == true) 
				Ext.getCmp('id_cmb_estado').setValue(edo);
		}
	}
	var procesarCatalogoMunicipio = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			if (primera_vez_municipio == true)
				Ext.getCmp('id_cmb_delegacion').setValue(municipio);
		}
	}

	var procesarCatalogoRamoSIAFF = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			//Ext.getCmp('id_cmb_ramo').setValue(ramosiaff);
			ObjCatalogo.cat3=true;
			validaCatalogos();
		}
	}
	 
	var procesarCatalogoUnidadSIAFF = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			if (primera_vez_unidad == true)
				Ext.getCmp('id_cmb_unidad').setValue(unidad);
		}
	}
	
	var procesarCatalogoSectorECO = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			//Ext.getCmp('id_cmb_economico').setValue(sectorecon);
			ObjCatalogo.cat4=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoSubSectorECO = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			if (primera_vez_subsector == true)
				Ext.getCmp('id_cmb_subsectoreco').setValue(subsectorecon);
		}
	}
	var procesarCatalogoRamaECO = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			if (primera_vez_rama == true)
				Ext.getCmp('id_cmb_ramaECO').setValue(ramaeco);
		}
	}
	var procesarCatalogoSectorEPO = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			//Ext.getCmp('id_cmb_sector').setValue(sector);
			ObjCatalogo.cat5=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoSubdireccion = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			//Ext.getCmp('id_cmb_subdirec').setValue(subdir);
			ObjCatalogo.cat6=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoPromotor = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			//Ext.getCmp('id_cmb_promotor').setValue(promotor);
			ObjCatalogo.cat7=true;
			validaCatalogos();
		}
	}
	
	
/*---------------------------------- Store's --------------------------------*/

	var catalogoBanco = new Ext.data.JsonStore({
	   id				: 'catBanco',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoBanco'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoBanco,
			exception: NE.util.mostrarDataProxyError
		}
	});		
	
	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoPais,
			exception: NE.util.mostrarDataProxyError
		}
  });
  
  // catalogo Estado
	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo,
		 load : procesarCatalogoEstado
		}
	});
	
	//catalogo Municipio
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoMunicipio
		}
	});
	
	
	//catalogo RamoSIAFF
	var catalogoRamoSIAFF= new Ext.data.JsonStore
	({
		id				: 'catRamoSIAFF',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoRamoSIAFF'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoRamoSIAFF
		}
	});
	
	//catalogo UnidadSIAFF
	var catalogoUnidadSIAFF= new Ext.data.JsonStore
	({
		id				: 'catUnidadSIAFF',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoUnidadSIAFF'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoUnidadSIAFF
		}
	});
	
	//catalogo Sector ECON�MICO
	var catalogoSectorECO= new Ext.data.JsonStore
	({
		id				: 'catSectorECO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoSectorECO'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoSectorECO
		}
	});
	
	//catalogo SubSector ECON�MICO
	var catalogoSubSectorECO= new Ext.data.JsonStore
	({
		id				: 'catSubSectorECO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoSubSectorECO'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoSubSectorECO
		}
	});
	
	//catalogo Rama ECON�MICA
	var catalogoRamaECO= new Ext.data.JsonStore
	({
		id				: 'catRamaECO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoRamaECO'	},
		autoLoad		: false,
		listeners	:
		{
		 exception 	: NE.util.mostrarDataProxyError,
		 beforeload : NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoRamaECO
		}
	});	
	
	//catalogo SectorEPO
	var catalogoSectorEPO= new Ext.data.JsonStore
	({
		id				: 'catSectorEPO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoSectorEPO'	},
		autoLoad		: false,
		listeners	:
		{
		 exception  : NE.util.mostrarDataProxyError,
		 beforeload : NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoSectorEPO
		}
	});
	
	//catalogo Subdireccion
	var catalogoSubdireccion= new Ext.data.JsonStore
	({
		id				: 'catSubdireccion',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoSubdireccion'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoSubdireccion
		}
	});
	
	//catalogo Promotor
	var catalogoPromotor= new Ext.data.JsonStore
	({
		id				: 'catPromotor',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma08Ext.data.jsp',
		baseParams	: { informacion: 'catalogoPromotor'	},
		autoLoad		: false,
		listeners	:
		{
		 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo,
		 load 		: procesarCatalogoPromotor
		}
	});	  


/*-------------------------------- Componentes ------------------------------*/


	var afiliacionPanel = {
		xtype		: 'panel',
		id 		: 'afiliacionPanel',
		items		: [{
				layout		: 'form',
				labelWidth	: 150,
				bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:2px;',
				items		:	
				[
					{
					/*-----------------------------*/
						xtype			: 'textfield',
						name			: 'ic_domicilio',
						id				: 'id_domicilio',
						hidden		: true
					},
					{
						xtype			: 'textfield',
						name			: 'ic_contacto',
						id				: 'id_contacto',
						hidden		: true
					},
					/*-----------------------------*/
					{
						xtype			: 'textfield',
						name			: 'Razon_Social',
						id				: 'Razon_Social',
						fieldLabel	: '* Raz�n Social',
						maxLength	: 100,
						width			: 250,
						allowBlank	: false,
						tabIndex		:	1
					},
					{
						xtype			: 'textfield',
						name			: 'nombre_comercial',
						id				: 'nombre_comercial',
						fieldLabel	: '* Nombre Comercial',
						maxLength	: 150,
						width			: 250,
						allowBlank	: false,
						tabIndex		:	2
					},
					// Internacional ////////////////////////////////////////////////
					
					{
						xtype : 'panel', 
						border: false,
						width : 900,
						layout: 'hbox',
						items: [
							{
								layout : 'form',
								width  : 413,
								border : false,
								items	 :[{
										xtype 		: 'textfield',
										name  		: 'rfc',
										id    		: 'rfc',
										fieldLabel	: '* R.F.C.',
										maxLength	: 20,
										width			: 150,
										allowBlank 	: true,
										hidden		: true,
										regex			:/^([A-Z|a-z|&amp;]{3})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
										regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
										'en el formato NNN-AAMMDD-XXX donde:<br>'+
										'NNN:son las iniciales del nombre de la empresa<br>'+
										'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
										'XXX:es la homoclave',
										tabIndex		:	3
									},
									{
										xtype 		: 'textfield',
										name  		: 'rfc',
										id    		: 'tax',
										fieldLabel	: '* Tax ID Number',
										maxLength	: 20,
										width			: 150,
										allowBlank 	: true,
										hidden		: true,
										tabIndex		:	3
									}]
							},
							
							{
								layout : 'form',	
								width  :400,
								border : false,
								items  :[{
										xtype			: 'textfield',
										name			: 'Dab_Number',
										id				: 'Dab_Number',
										maxLength	: 9,
										width			: 150,
										fieldLabel	: 'DaB Number',
										hidden		: true,
										allowBlank	: true,
										hidden		: false,
										tabIndex		:	4
									}
									]
							}
					]}
				]
		}]
	};
//---------------------------------------------------------------------------//

	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calle',
				name			: 'calle',
				fieldLabel  : '* Calle',
				maxLength	: 100,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	5

			},
			{
				xtype			: 'textfield',
				id          : 'num_int',
				name			: 'num_int',
				fieldLabel  : 'N�mero interior',
				maxLength	: 25,
				width			: 90,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 7
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais',
				name				: 'cmb_pais',
				hiddenName 		: 'cmb_pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 150,
				tabIndex			: 9,
				store				: catalogoPais,
				listeners: {
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado').reset();
						Ext.getCmp('id_cmb_delegacion').reset();
						primera_vez_estado = false;
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
							
							
					}
				}
				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacion',
				name				: 'cmb_delegacion',
				hiddenName 		: 'cmb_delegacion',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				tabIndex			: 11,
				store				: catalogoMunicipio
			},
			
			
			{
				xtype			: 'textfield',
				id          : 'telefono',
				name			: 'telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: true,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30,
				tabIndex		: 13
				
			},
			{
				xtype			: 'textfield',
				id          : 'fax',
				name			: 'fax',
				fieldLabel  : '* Fax',
				width			: 230,
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	15
			}
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'num_ext',
				name			: 'num_ext',
				fieldLabel  : '* N�mero Exterior',
				allowBlank	: true,
				margins		: '0 20 0 0',
				maxLength	: 25,
				width			: 90,
				tabIndex		: 6
			},
			{
				xtype			: 'textfield',
				id          : 'colonia',
				name			: 'colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 100,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 8
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_estado',
				name				: 'cmb_estado',
				hiddenName 		: 'cmb_estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				allowBlank		: false,
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				tabIndex			: 10,
				store				: catalogoEstado,
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_delegacion').reset();
						 primera_vez_municipio = false;
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais')).getValue()
							})
						});
					}
				}
				
			},
			{
				xtype			: 'textfield',
				id          : 'code',
				name			: 'code',
				fieldLabel  : '* C�digo Postal',
				allowBlank	: true,
				hidden		: true,
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 90,
				tabIndex		: 12
			},
			{
				xtype			: 'textfield',
				id          : 'code2',
				name			: 'code',
				fieldLabel  : '* Zip Code',
				allowBlank	: true,
				hidden		: true,
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 90,
				tabIndex		: 12
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'email',
				name			: 'email',
				fieldLabel: ' E-mail',
				allowBlank	: true,
				maxLength	: 100,
				width			: 230,
				tabIndex		: 14,
				margins		: '0 20 0 0',
				vtype: 'email'
			}
		]
	};
	
	/*--____Fin Domicilio ____--*/
//---------------------------------------------------------------------------//

	/*--____ Datos del Representante Legal ____--*/
	
	var datosRepresentante_izq = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_izq',
		//title 	: 'Domicilio',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'datefield',
				id          : 'fch_poder',
				name			: 'fch_poder',
				allowBlank	: false,
				width			: 130,
				fieldLabel  : '* Fecha del poder notarial',
				margins		: '0 20 0 0',
				tabIndex		: 16
			},
			{
				xtype			: 'textfield',
				id          : 'paterno',
				name			: 'paterno',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 18
			},
			{
				xtype			: 'textfield',
				id          : 'nombre',
				name			: 'nombre',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 20
			},
			
			{
				xtype			: 'textfield',
				id          : 'SSId_Number',
				name			: 'SSId_Number',
				fieldLabel  : 'SS Id Number',
				maxLength	: 8,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: true,
				hidden		: true,
				vtype			: 'alpha',
				tabIndex		: 22
			}
			
		]
	};
		
	var datosRepresentante_der = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_der',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'num_escritura',
				name			: 'num_escritura',
				fieldLabel  : '** N�mero de Escritura',
				maxLength	: 20,
				width			: 150,
				allowBlank	: false,
				tabIndex		:	17
			},
			{
				xtype			: 'textfield',
				id          : 'materno',
				name			: 'materno',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 19
			}
		]
	};
	
	/*--____Fin Datos del Representante Legal ____--*/
//---------------------------------------------------------------------------//


/*--____ Otros Datos ____--*/
	
	var otrosDatos_izq = {
		xtype		: 'fieldset',
		id 		: 'otrosDatos_izq',
		border	: false,
		labelWidth	: 150,
		items		: 
		[		
			{
				xtype			: 'textfield',
				id          : 'num_sirac',
				name			: 'num_sirac',
				fieldLabel  : 'No. Cliente SIRAC',
				maxLength	: 12,
				width			: 120,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 26
			},
			{
				xtype			: 'textfield',
				id          : 'bco_retiro',
				name			: 'bco_retiro',
				fieldLabel  : 'Banco de Retiro',
				maxLength	: 100,
				width			: 160,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 28
			},
			
			{
				xtype			: 'numberfield',
				id          : 'cuenta',
				name			: 'cuenta',
				fieldLabel  : 'Cuenta de Retiro',
				maxLength	: 25,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 30
			},
			{	xtype: 'displayfield', value: ''		}
		]
	};
		
	var otrosDatos_der = {
		xtype		: 'fieldset',
		id 		: 'otrosDatos_der',
		border	: false,
		labelWidth	: 160,
		
		items		: 
		[
			{
				xtype			: 'checkbox',
				id				: 'id_ditribuidores',
				name			: 'ditribuidores',
				hiddenName	: 'ditribuidores',
				fieldLabel	: 'Financiamiento a Distribuidores ',
				tabIndex		: 27,
				enable 		: true
			},
			{
				xtype			: 'checkbox',
				id				: 'id_convenio',
				name			: 'convenio',
				hiddenName	: 'convenio',
				fieldLabel	: 'Opera Convenio �nico',
				tabIndex		: 29,
				enable 		: true
			},
			/*------FODEA 012 - 2010 ACF--------*/
			{
				xtype			: 'checkbox',
				id				: 'id_irrevocable',
				name			: 'irrevocable',
				hiddenName	: 'irrevocable',
				fieldLabel	: 'Opera Instrucci�n Irrevocable',
				hidden		: true,
				tabIndex		: 31,
				enable 		: true,
				listeners   : {
				//este escuchador verifica el total checked en true y alerta
					
					check: function(field) {
						if(Ext.getCmp('id_irrevocable').getValue() == false && total > 0 ) {
							Ext.getCmp('id_irrevocable').setValue(true); 
							Ext.MessageBox.alert('Mensaje de p�gina web','Tiene solicitudes de Mandato pendientes.' );
						}
					}
				}
			}
			
		]
	};
	
	var otrosDatos_ = {
		xtype		: 'panel',
		id 		: 'otrosDatos_',
		layout	: 'form',
		title 	: 'Otros Datos',
		border	: false,
	   allowBlank:false,
		labelWidth	: 150,
		items		: 
		[{
				xtype			: 'radiogroup',
				msgTarget	: 'side',
				id				: 'id_c_proveedores',
				name			: 'c_proveedores',
				hiddenName	: 'c_proveedores',
				fieldLabel	: 'Base de Operaci�n Especifica',
				cls			: 'x-check-group-alt',
				columns		: [50, 50],
				//valueField	:'c_proveedores',
				tabIndex		:23,
				items			: [
					{	boxLabel	: 'SI',	name : 'c_proveedores', inputValue: 'S'	},
					{	boxLabel	: 'NO',	name : 'c_proveedores', inputValue: 'N' }
		]},
		/*------FODEA 034 - 2009 ACF (I)---------*/
		{
				xtype				: 'combo',
				id          	: 'id_cmb_ramo',
				name				: 'cmb_ramo',
				hiddenName 		: 'cmb_ramo',
				fieldLabel  	: 'Ramo SIAFF',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 600,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 24,
				store				: catalogoRamoSIAFF,
				listeners: {
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_unidad').reset();
						primera_vez_unidad = false;
							catalogoUnidadSIAFF.load({
								params: Ext.apply({
									ramo: record.json.clave
							})
						});
					}
				}
			}, 
			{
				xtype				: 'combo',
				id          	: 'id_cmb_unidad',
				name				: 'cmb_unidad',
				hiddenName 		: 'cmb_unidad',
				fieldLabel  	: 'Unidad SIAFF',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 350,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 25,
				store				: catalogoUnidadSIAFF
			}
		]
		
	};
	
	
	var otrosDatos_sectores = {
		xtype		: 'panel',
		id 		: 'otrosDatos_sectores',
		items		: [{
				layout		: 'form',
				labelWidth	: 150,
				bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:2px;',
				items		:	
				[
					/*-------------F0DEA-2012-------------*/
					{
						xtype				: 'combo',
						id          	: 'id_cmb_economico',
						name				: 'cmb_economico',
						hiddenName 		: 'cmb_economico',
						fieldLabel  	: 'Sector Econ�mico',
						forceSelection	: true,
						allowBlank		: false,
						triggerAction	: 'all',
						mode				: 'local',
						width				: 200,
						emptyText		: 'Seleccione Sector Econ�mico',
						valueField		: 'clave',
						displayField	: 'descripcion',
						autoWidth		: true,
						tabIndex			: 32,
						store				: catalogoSectorECO,
						listeners: {
							select: function(combo, record, index) {
								 primera_vez_subsector = false;
								 Ext.getCmp('id_cmb_subsectoreco').reset();
								  Ext.getCmp('id_cmb_ramaECO').reset();
								 catalogoSubSectorECO.load({
									params: Ext.apply({
										sectorECO :record.json.clave
									})
								});
							}
						}
					},
					{
						xtype				: 'combo',
						id          	: 'id_cmb_subsectoreco',
						name				: 'cmb_subsectoreco',
						hiddenName 		: 'cmb_subsectoreco',
						fieldLabel  	: 'Subsector Econ�mico',
						forceSelection	: true,
						allowBlank		: false,
						triggerAction	: 'all',
						mode				: 'local',
						width				: 270,
						emptyText		: 'Seleccione Subsector Econ�mico',
						valueField		: 'clave',
						displayField	: 'descripcion',
						autoWidth		: true,
						tabIndex			: 33,
						store				: catalogoSubSectorECO,
						listeners: {
							select: function(combo, record, index) {
								 
								 Ext.getCmp('id_cmb_ramaECO').reset();
								 primera_vez_rama = false;
								 catalogoRamaECO.load({
									params: Ext.apply({
										sectorECO	: (Ext.getCmp('id_cmb_economico')).getValue(),
										subsectorECO :record.json.clave							
									})
								});
							}
						}
					},
					{
						xtype				: 'combo',
						id          	: 'id_cmb_ramaECO',
						name				: 'cmb_ramaECO',
						hiddenName 		: 'cmb_ramaECO',
						fieldLabel  	: 'Rama Econ�mica',
						forceSelection	: true,
						allowBlank		: false,
						triggerAction	: 'all',
						mode				: 'local',
						width				: 270,
						emptyText		: 'Seleccione Rama Econ�mica',
						valueField		: 'clave',
						displayField	: 'descripcion',
						autoWidth		: true,
						tabIndex			: 34,
						store				: catalogoRamaECO
					},
					/*-------------Fin FODEA 2012-------------*/
					
					/*-------------F057-2010-------------*/
					{
						xtype				: 'combo',
						id          	: 'id_cmb_sector',
						name				: 'cmb_sector',
						hiddenName 		: 'cmb_sector',
						fieldLabel  	: 'Sector EPO',
						forceSelection	: true,
						allowBlank		: false,
						triggerAction	: 'all',
						mode				: 'local',
						width				: 160,
						emptyText		: 'Seleccione Sector',
						valueField		: 'clave',
						displayField	: 'descripcion',
						autoWidth		: true,
						tabIndex			: 35,
						store				: catalogoSectorEPO
					},
					{
						xtype				: 'combo',
						id          	: 'id_cmb_subdirec',
						name				: 'cmb_subdirec',
						hiddenName 		: 'cmb_subdirec',
						fieldLabel  	: 'Subdireccion',
						forceSelection	: true,
						allowBlank		: false,
						triggerAction	: 'all',
						mode				: 'local',
						width				: 270,
						emptyText		: 'Seleccione Subdirecci�n',
						valueField		: 'clave',
						displayField	: 'descripcion',
						autoWidth		: true,
						tabIndex			: 36,
						store				: catalogoSubdireccion
					},
					{
						xtype				: 'combo',
						id          	: 'id_cmb_promotor',
						name				: 'cmb_promotor',
						hiddenName 		: 'cmb_promotor',
						fieldLabel  	: 'Lider Promotor',
						forceSelection	: true,
						allowBlank		: false,
						triggerAction	: 'all',
						mode				: 'local',
						width				: 270,
						emptyText		: 'Seleccione Lider Promotor',
						valueField		: 'clave',
						displayField	: 'descripcion',
						autoWidth		: true,
						tabIndex			: 37,
						store				: catalogoPromotor 
					}
			   ]
		}]
	};

	/*--____ Fin Otros Datos ____--*/
//---------------------------------------------------------------------------//	
	
	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			
			{
				layout	: 'hbox',
				title		: '',
				width		: 940,
				items		: [afiliacionPanel ]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 940,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			},
			{
				layout	: 'hbox',
				title		: 'Datos del Representante Legal',
				width		: 940,
				items		: [datosRepresentante_izq, datosRepresentante_der ]
			},
			
			otrosDatos_ ,
			{
				layout	: 'hbox',
				title		: '',
				width		: 940,
				bodyStyle: 	'padding: 12px; padding-right:0px;padding-left:0px;',
				items		: [otrosDatos_izq, otrosDatos_der ]
			},				
			{
				layout	: 'hbox',
				title		: '',
				width		: 940,
				items		: [otrosDatos_sectores]
			}
		],
		monitorValid	: true,
		buttons			: [
			{
				text: 'Aceptar',
				id: 'BTNACEPTAR_CM',
				iconCls:'aceptar',
				hidden : true,
				formBind: true,				
				handler: function(boton, evento) 	{
				
				Ext.Ajax.request({
					url: '15forma08Ext.data.jsp', 
					params: {
						informacion: "validaPermiso", 
						idMenuP:idMenu,
						ccPermiso:'BTNACEPTAR_CM'
						
					},
					callback: procesarPermiso
				});	

				}
			
			}, //fin boton
			{
				text:'Regresar',
				id: 'btnRegresar',				
				handler: function() {
					window.location = '15forma5ext.jsp?idMenu='+idMenu;
				}
			},
			{
				text:'Cancelar',
				id: 'BTNCANCELAR_CM',
				hidden : true,
				handler: function() {
					Ext.Ajax.request({
							url: '15forma08Ext.data.jsp',
							params: Ext.apply({
								informacion: 'ConsultaEPO',
								ic_epo : ic_epo
								}),	

							callback: procesarSuccessFailureConsultaEPO
					});
				}
			}
		]
	});			
	

	/*-------------------------- Fin Popup Dispersion --------------------------*/
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items: //fp
		[
			fpDatos,
			NE.util.getEspaciador(10)			
		]
	});

/*Muestra y oculta componentes segun el tipo de Cadena Productiva Nacional/Internacional*/
	if(internacional == 'S'){ 
		Ext.getCmp('tax').setVisible(true);
		Ext.getCmp('Dab_Number').setVisible(true);
		Ext.getCmp('code2').setVisible(true);
		Ext.getCmp('SSId_Number').setVisible(true);
		Ext.getCmp('rfc').setVisible(false);
		Ext.getCmp('id_irrevocable').setVisible(false);		
		Ext.getCmp('id_convenio').setVisible(false);
	}
	else{
		Ext.getCmp('rfc').setVisible(true);
		Ext.getCmp('code').setVisible(true);
		Ext.getCmp('id_irrevocable').setVisible(true);		
		Ext.getCmp('id_convenio').setVisible(true);
		Ext.getCmp('tax').setVisible(false);
		Ext.getCmp('Dab_Number').setVisible(false);
		Ext.getCmp('SSId_Number').setVisible(false);
	}

	/*Peticiones Ajax para la carga automatica de informaci�n*/	
	catalogoBanco.load( );
	catalogoPais.load();
	catalogoRamoSIAFF.load();
	catalogoSectorECO.load();
	catalogoSectorEPO.load();
	catalogoSubdireccion.load(); 
	catalogoPromotor.load();

	
	
	Ext.Ajax.request({
		url: '15forma08Ext.data.jsp',
		params: {
			informacion: "validaPermiso",
			idMenuP:idMenu 
		},
		callback: procesaValoresIniciales
	});	
	
	
});