

Ext.onReady(function(){
	var noAfianza;
	var valorFormulario;
	var cuentaActiva;
//-------------------Handers-----------------

	function validaFormulario(forma){
	var bandera = true;
         forma.getForm().items.each(function(f){
			if(f.xtype != 'label'&&f.xtype != undefined&&bandera){
           if(f.isVisible()){
				   if(!f.isValid()){
						f.focus();
						f.markInvalid('Este campo es Obligatorio');
						bandera= false;
					}
           }
			}
        });
        return bandera;
    }
	 
	function procesarAlta(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			respuesta=Ext.util.JSON.decode(response.responseText);
		
				Ext.Msg.show({
									title:	"Mensaje",
									msg:		"El cambio de perfil fue realizado con �xito",
									buttons:	Ext.Msg.OK,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											location.reload();
										}
									},
									closable:false
								});
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
}

	function procesarAlta2(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			respuesta=Ext.util.JSON.decode(response.responseText);
		
				Ext.Msg.show({
									title:	"Mensaje",
									msg:		"El cambio de perfil fue realizado con �xito.",
									buttons:	Ext.Msg.OK,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											location.reload();
										}
									},
									closable:false
								});
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
}

	var procesarConsultaData = function(store,arrRegistros,opts){
		//accionBotones();
		var boton=Ext.getCmp('btnGenerarPDF');
			
			boton.enable();
			
			var botonE=Ext.getCmp('btnGenerarArchivoTodas');
			botonE.enable();
			
			grid.setVisible(true);
			fp.el.unmask();
			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					botonE.disable();
					boton.disable();
				}
			}
		}

	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('btnGenerarPDF');
			boton.setIconClass('icoPdf');
			boton.enable();
			
			var botonE=Ext.getCmp('btnGenerarArchivoTodas');
			botonE.setIconClass('icoXls');
			botonE.enable();
			
			
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesaVerCuenta = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
				ventanaCuentas.setVisible(true);
				fpDespliega.setVisible(false);
				gridUsuarios.setVisible(true);
				storeUsuarios.loadData(Ext.util.JSON.decode(response.responseText).registros);
				gridUsuarios.setTitle(Ext.util.JSON.decode(response.responseText).AFILIADO);
				var boton=Ext.getCmp('imprimirCuenta');
				boton.enable();
				
				fp.el.unmask();
				var el = gridUsuarios.getGridEl();
				if(storeUsuarios.getTotalCount()>0){
					el.unmask();
				}else{
						el.mask('No se encontr� ning�n registro', 'x-mask');
						
						boton.disable();
					}
				
				
		}else{
				Ext.Msg.alert("Mensaje de p�gina web.",'Error al eliminar la Afianzadora' );
		}
	}
	
	var procesaModificacion = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			valorFormulario = Ext.util.JSON.decode(response.responseText).registro;
			fpDatos.setVisible(true);
			fp.setVisible(false);
			grid.setVisible(false);
			fpDatos.getForm().setValues(valorFormulario);
			
			catalogoEstado2.load({
								params : Ext.apply({
									pais : Ext.getCmp('id_cmb_pais_domicilio').getValue()
								})
							});
			catalogoMunicipio.load({
							params: Ext.apply({
								estado : Ext.getCmp('id_cmb_estado_domicilio2').getValue(),
								pais : (Ext.getCmp('id_cmb_pais_domicilio')).getValue()
							})
						});
		}else{
				Ext.Msg.alert("Mensaje de p�gina web.",'Error al eliminar la Afianzadora' );
		}
	}
	var procesarModificar = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consulta.getAt(rowIndex);
		noAfianza = registro.get('NOAFIANZA');
					Ext.Ajax.request({
							url: '15forma05AfianzadoraExt.data.jsp',
							params: Ext.apply({informacion:'obtenerRegistro',NOAFIANZA: registro.get('NOAFIANZA') }),
							callback: procesaModificacion
						});
	}
	var procesarVerCuentas = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consulta.getAt(rowIndex);
		noAfianza = registro.get('NOAFIANZA');
					Ext.Ajax.request({
							url: '15forma05AfianzadoraExt.data.jsp',
							params: Ext.apply({informacion:'obtenerAfiliado',claveAfiliado: registro.get('NOAFIANZA') }),
							callback: procesaVerCuenta
						});
	}
	
	
	
		var procesarCuentaCliente = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fpDespliega.getForm().setValues( Ext.util.JSON.decode(response.responseText).PERFIL);
			//Ext.getCmp('muestraCol').setVisible(false);
			var nomEmpresa = Ext.getCmp('dEmpresa').getValue();
			var listCad = nomEmpresa.split(' ');
			if(listCad.length==1){
				var cadAux="";
				var aux = nomEmpresa.split('');
				for (var i=0; i<aux.length; i++) {
					if((i!=0)&&(i%30)==0){
						cadAux +='<br>';
					}
					cadAux += aux[i];
				}
				Ext.getCmp('dEmpresa').update(cadAux);
			}
			catalogoPerfil.load({
					params: {txtLogin:cuentaActiva  }});  
				
		}else{
				Ext.Msg.alert("Mensaje de p�gina web.",'Error al eliminar la Afianzadora' );
		}
	}
	var procesaCuenta = function(grid, rowIndex, colIndex, item, event) {  
		var registro = storeUsuarios.getAt(rowIndex);
		gridUsuarios.setVisible(false);
		fpDespliega.setVisible(true);
		if(colIndex==0){
			Ext.getCmp('cmbPerfil').setVisible(false);
			Ext.getCmp('dPerfil').setVisible(true);
			Ext.getCmp('btnAceptaCuenta').setVisible(false);
		}else{
			Ext.getCmp('cmbPerfil').setVisible(true);
			//Ext.getCmp('dPerfil').setVisible(false);
			Ext.getCmp('btnAceptaCuenta').setVisible(true);
		}
		cuentaActiva = registro.get('CUENTA');
					Ext.Ajax.request({
							url: '15forma05AfianzadoraExt.data.jsp',
							params: Ext.apply({informacion:'informacionUsuario',txtLogin: registro.get('CUENTA') }),
							callback: procesarCuentaCliente
						});
	}
	
		var procesaEliminado = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		if( Ext.util.JSON.decode(response.responseText).eliminacion = "Se Elimino"){
		Ext.Msg.show({
									title:	"Mensaje",
									msg:		'Se Borr� la Afianzadora',
									buttons:	Ext.Msg.OK,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											location.reload();
										}
									},
									closable:false,
									icon: Ext.MessageBox.QUESTION
								});
			}
		}else{
				Ext.Msg.alert("Mensaje de p�gina web.",'Error al eliminar la Afianzadora' );
		}
	}

	var procesarEliminar = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consulta.getAt(rowIndex);
					Ext.Ajax.request({
							url: '15forma05AfianzadoraExt.data.jsp',
							params: Ext.apply({informacion:'eliminaRegistro',NOAFIANZA: registro.get('NOAFIANZA') }),
							callback: procesaEliminado
						});
	}
 	function creditoElec(pagina) {
		if (pagina == 0) { //Afianzadora
			window.location  = "15forma05AfianzadoraExt.jsp"; 	
		}	else if (pagina == 1 ) { // cadena productiva
			window.location ="15consEPOext.jsp";
		}	else if (pagina == 2) { //provedores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15forma05ProveedoresExt.jsp";
		}	else if (pagina == 3) { //Provedor Internacional
			
		}	else if (pagina == 4) { // provedor sin numero
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp";
		}	else if (pagina == 5) { // Distribuidores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp";		
		}	else if (pagina == 6) { // Fiados
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp";		
		}	else if (pagina == 7) { // Intermediario financiero Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B"; 	
		} else if (pagina == 8) { // Intermediario financiero NO Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB"; 	 	
		} else if (pagina == 9) { // Intermediario financiero Bancario /No Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB"; 		 	
		} else if (pagina == 10) { // Provedor Carga Masiva EContrac
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon_ext.jsp";
		}	else if (pagina == 11) { // Afiliados a Credito Electronico
			window.location = "15forma05ext_AfCredElectronico.jsp";
		}	else if (pagina == 12) { // Cadena Productiva Internacional
			
		}	else if (pagina == 13) { // Contragarante
			
		}	else if (pagina == 14) { // Mandante
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp"; 	
		}	else if (pagina == 15) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp"; 	
		}	else if (pagina == 16) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp"; 	
		}			

	}
//-------------------STORES---------------

var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '15forma05AfianzadoraExt.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'NOAFIANZA'},
				{name: 'NONAFIN'},
				{name: 'RAZONSOCIAL'},
				{name: 'DOMICILIO'},
				{name: 'ESTADO'},
				{name: 'TELEFONO'},
				{name: 'FECHAALTA'},
				{name: 'CNSF'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
						}	},
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

  	var catalogoEstado2 = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma05AfianzadoraExt.data.jsp',
		baseParams	: { informacion: 'catalogoEstado2'	},
		autoLoad		: false,
		listeners	:
		{
		 load: function(){
			
		 if(Ext.getCmp('id_cmb_estado_domicilio2').getValue()!=''){
				Ext.getCmp('id_cmb_estado_domicilio2').setValue(Ext.getCmp('id_cmb_estado_domicilio2').getValue());
			}
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
  	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma05AfianzadoraExt.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
var catalogoPerfil = new Ext.data.JsonStore({
		id: 'catalogoPerfil',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: '15forma05AfianzadoraExt.data.jsp',
		baseParams: {
			informacion: 'catalogoPerfil'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {				
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var dataStatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],
			//['3','Proveedor Internacional'],
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],	
			//['12','Cadena Productiva Internacional'],
			//['13','Contragarante'],
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo'],
			['16','Cliente Externo']
		]
	});
  

	

//---------------------------COMPONENTES-------------------
var elementosForma = [
			{
			xtype: 'combo',	
			name: 'status',	
			id: 'cmbTipoAfiliado',	
			fieldLabel: 'Tipo de Afiliado', 
			mode: 'local',	
			hiddenName : 'status',	
			emptyText: 'Seleccionar  ',
			forceSelection : true,	
			value : 0,
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : dataStatus,	
			displayField : 'descripcion',	
			valueField : 'clave',
			listeners: {
				select: {
					fn: function(combo) {
						var valor  = combo.getValue();
						creditoElec(valor);						
					}
				}
			}
		},{
			xtype: 'numberfield',
			name: 'numElectronico',
			fieldLabel: 'N�mero de Nafin Electr�nico',
			id: 'numElectronico',
			maxLength: 30,
			width: 110,
			msgTarget: 'side'
		},{
			xtype: 'textfield',
			name: 'nombre',
			maxLength: 30,
			fieldLabel: 'Nombre',
			id: 'nombre',
			width: 110,
			msgTarget: 'side'
		},{
			xtype				: 'combo',
			id          	: 'id_cmb_estado_domicilio',
			name				: 'Estado',
			hiddenName 		: 'Estado',
			fieldLabel  	: 'Estado ',
			forceSelection	: true,
			triggerAction	: 'all',
			mode				: 'local',
			typeAhead		: true,
			msgTarget	: 'side',
			emptyText		:'Seleccione...',
			valueField		: 'clave',
			displayField	: 'descripcion',
			width				: 230,
			store				: catalogoEstado
				
			}
	
]

	var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						
						{
						
						header: 'No. Afianzadora CNSF',
						tooltip: 'No. Afianzadora CNSF',
						sortable: true,
						dataIndex: 'CNSF',
						width: 80,
						align: 'center'
						},{
						
						header: 'N�mero de Nafin Electr�nico',
						tooltip: 'N�mero de Nafin Electr�nico',
						sortable: true,
						dataIndex: 'NONAFIN',
						width: 100,
						align: 'center'
						},{
						align:'center',
						header: 'Nombre o Raz�n Social',
						tooltip: 'Nombre o Raz�n Social',
						sortable: true,
						dataIndex: 'RAZONSOCIAL',
						width: 200,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},{
						align:'center',
						header: 'Domicilio',
						tooltip: 'Domicilio',
						sortable: true,
						dataIndex: 'DOMICILIO',
						width: 200,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Estado',
						tooltip: 'Estado',
						sortable: true,
						dataIndex: 'ESTADO',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tel�fono',
						tooltip: 'Tel�fono',
						sortable: true,
						dataIndex: 'TELEFONO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Fecha de Alta Afianzadora',
						tooltip: 'Fecha de Alta Afianzadora',
						sortable: true,
						dataIndex: 'FECHAALTA',
						width: 130,
						align: 'center'
						
						},
						{
						xtype: 'actioncolumn',
						width: 100,
						header: 'Seleccionar',
						tooltip: 'Seleccionar',
						menuDisabled: true,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Modificar';								
							       return 'modificar';	
								 }
								 ,handler: procesarModificar
					      },{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[1].tooltip = 'Borrar';								
							       return 'borrar';	
								 }
								 ,handler: procesarEliminar
					      }]	
			       },
						{
						xtype: 'actioncolumn',
						width: 100,
						header: 'Cuentas de Usuario',
						tooltip: 'Cuentas de Usuario',
						menuDisabled: true,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							       return 'iconoLupa';	
								 }
								 ,handler: procesarVerCuentas
					      }]	
			       }
						
						
				
				],
				stripeRows: true,
				loadMask: true,
				height: 350,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
				
					
					items: ['->','-',
							
								
								{
									xtype: 'button',
									//height: 40,
									text: 'Generar PDF',
									iconCls: 'icoPdf',
									id: 'btnGenerarPDF',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '15forma05AfianzadoraExt.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoPDF'}),
											callback: procesarArchivoSuccess
										});
									}
								},
								{
									xtype: 'button',
									//height: 40,
									text: 'Generar Archivo',
									iconCls: 'icoXls',
									id: 'btnGenerarArchivoTodas',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '15forma05AfianzadoraExt.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV'}),
											callback: procesarArchivoSuccess
										});
									}
								},
								
								'-']
				}
		});
//Modifica Panel
 	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma05AfianzadoraExt.data.jsp',
		baseParams	: { informacion: 'catalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			exception: NE.util.mostrarDataProxyError
		}
  });
 
	
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma05AfianzadoraExt.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 load: function(){
			
		 if(Ext.getCmp('id_cmb_delegacion_domicilio').getValue()!=''){
				Ext.getCmp('id_cmb_delegacion_domicilio').setValue(Ext.getCmp('id_cmb_delegacion_domicilio').getValue());
			}
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	



//--------------------Domicilio--------------------
	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calle_domicilio',
				name			: 'Calle',
				fieldLabel  : '* Calle, No Exterior y No Interior',
				maxLength	: 100,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false

			},{
				xtype			: 'textfield',
				id          : 'Codigo_postal',
				name			: 'code',
				fieldLabel  : '* C�digo Postal',
				allowBlank	: false,
				hidden		: false,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 90
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_estado_domicilio2',
				name				: 'Estado',
				hiddenName 		: 'Estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				msgTarget	: 'side',
				allowBlank		: false,
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				store				: catalogoEstado2,
				listeners: {
					
					select: function(combo, record, index) {
						 
						 
						 Ext.getCmp('id_cmb_delegacion_domicilio').reset();
						 
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais_domicilio')).getValue()
							})
						});
						
					}
				}
				
			},
			{
				xtype			: 'textfield',
				id          : 'telefono',
				name			: 'telefono',
				regex			:/^[0-9]*$/,
				regexText	: 'El Tel�fono solo de contener n�meros',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30
				
			},
			{
				xtype			: 'textfield',
				id          : 'fax_domicilio',
				name			: 'Fax',
				regex			:/^[0-9]*$/,
				regexText	: 'El Fax solo de contener n�meros',
				fieldLabel  : 'Fax',
				width			: 230,
				msgTarget	: 'side',
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: true
			},
			{
				xtype			: 'textfield',
				id          : 'URL',
				name			: 'URL',
				fieldLabel  : '* URL para la Validaci�n de fianza',
				maxLength	: 100,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false

			},
			{ 	xtype: 'displayfield',  value: '',width			: 237 }
			
			
			
		
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			
			{
				xtype			: 'textfield',
				id          : 'colonia_domicilio',
				name			: 'Colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 100,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais_domicilio',
				name				: 'Pais',
				hiddenName 		: 'Pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				msgTarget	: 'side',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 230,
				store				: catalogoPais,
				listeners: {
				
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado_domicilio').reset();
						Ext.getCmp('id_cmb_delegacion_domicilio').reset();
							catalogoEstado2.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
							
							
					}
				}
				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacion_domicilio',
				name				: 'Delegacion_o_municipio',
				hiddenName 		: 'Delegacion_o_municipio',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				store				: catalogoMunicipio
				
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_email_legal',
				name			: 'Email_Rep_Legal',
				fieldLabel: '* E-mail Notificaciones',
				maxLength	: 100,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				vtype			: 'email',
				allowBlank	: false
			}			
			,{ 	xtype: 'displayfield',  value: '',width			: 237 }
		]
	};
var elementosFormaMod = [
		{
			xtype: 'panel',
			id: 'miPanel',
			width: 900,
			title: 'Nombre Completo'
		},
		{
				xtype			: 'textfield',
				name			: 'Razon_Social',
				id				: 'razon_Social',
				fieldLabel	: '* Raz�n Social',
				maxLength	: 100,
				anchor			: '60%' ,
				msgTarget	: 'side',
				allowBlank	: false
			},
			{
				xtype			: 'textfield',
				name			: 'Nombre_Comercial',
				id				: 'nombre_comercial',
				fieldLabel	: '* Nombre Comercial',
				maxLength	: 100,
				anchor			: '60%' ,
				
				msgTarget	: 'side',
				allowBlank	: false
			},{
				layout	: 'hbox',
				width		:940,
				items		: [
							{
							xtype		: 'fieldset',
							id 		: 'paneliz',
							border	: false,
							labelWidth	: 150,
							items		: 
							[
									{
													xtype 		: 'textfield',
													name  	: 'R_F_C',
													id    		: 'rfc_e',
													fieldLabel	: '* R.F.C.',
													maxLength	: 20,
													width			: 230,
													allowBlank 	: false,
													hidden		: false,
													margins		: '0 20 0 0',
													msgTarget	: 'side',
													regex			:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
													regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
													'en el formato NNN-AAMMDD-XXX donde:<br>'+
													'NNN:son las iniciales del nombre de la empresa<br>'+
													'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
													'XXX:es la homoclave'
												},
												{ 	xtype: 'displayfield',  value: '',width			: 237 }
								]
								}, 
								{
							xtype		: 'fieldset',
							id 		: 'panelder',
							border	: false,
							labelWidth	: 150,
							items		: 
							[
							{
								xtype			: 'textfield',
								id          : 'CNSF',
								name			: 'CNSF',
								fieldLabel  : '* N�mero Asignado por la CNSF',
								maxLength	: 20,
								regex			:/^[0-9]*$/,
								regexText	: 'El campo solo de contener n�meros',
								allowBlank 	: false,
								width			: 230,
								msgTarget	: 'side',
								margins		: '0 20 0 0'
								},
								{ 	xtype: 'displayfield',  value: '',width			: 237 }
								]
							}
						]
			}
]

//-----------------Formularios-----------------------
var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		hidden			: false,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 160,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			elementosFormaMod,
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 930,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			}
		],
		buttons			: [
			{
				text: 'Actualizar',
				id: 'btnAceptar',
				iconCls:'icoActualizar',
				handler: function(boton, evento) 
				{
					/*****************Validaciones***************/

				
					if(!validaFormulario(fpDatos)){
					
						return;
					}
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							var params = (fpDatos)?fpDatos.getForm().getValues():{};				
							//fpDatosInter.el.mask("Procesando", 'x-mask-loading');
							//peticion de Registro de Afiliaci�n
							Ext.Ajax.request({
									url: '15forma05AfianzadoraExt.data.jsp',
									params: Ext.apply(params,{
											informacion: 'AfiliacionUpdate',
											NOAFIANZA:noAfianza
									}),
									callback: procesarAlta
							});
						}
					});		
				}
			
			},
			{
				text:'Regresar',
				iconCls: 'icoRegresar',
				handler: function() {
					window.location = '15forma5ext.jsp';
				}
			},
			{
				text:'Cancelar',
				iconCls: 'icoCancelar',
				handler: function() {
					fpDatos.getForm().setValues(valorFormulario);
					
					catalogoEstado2.load({
										params : Ext.apply({
											pais : Ext.getCmp('id_cmb_pais_domicilio').getValue()
										})
									});
					catalogoMunicipio.load({
									params: Ext.apply({
										estado : Ext.getCmp('id_cmb_estado_domicilio2').getValue(),
										pais : (Ext.getCmp('id_cmb_pais_domicilio')).getValue()
									})
								});
				}
			}
		]
	});	


//Modifica Panel FIn

//Cuentas de Usuario
var elementosDespliega =[
	{
		xtype: 'panel',
		allowBlank: false,
		title:'INFORMACION USUARIO',
		value: ''
	},
	{
		xtype: 'displayfield',
		name: 'dClaveUsr',
		id: 'dClaveUsr',
		allowBlank: false,
		//anchor:'70%',
		fieldLabel: 'Clave de Usuario',
		value:''
	},
	{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					fieldLabel: 'Empresa',
					items: [
			{
			xtype: 'displayfield',
			name: 'dEmpresa',
			id: 'dEmpresa',
			allowBlank: false,
			width			: 230,
			//anchor:'70%',
			
			value:''
			}
			
		,
		{
			xtype: 'displayfield',id:'muestraCol',value: 'Nafin Electr�nico:',hidden: false},
			
			{
		
			xtype: 'displayfield',
			name: 'dNE',
			id: 'dNE',
			allowBlank: false,
			anchor:'70%',
			width			: 230,
			
			value:''
			}
			]
		
		},
		{
		xtype: 'displayfield',
		name: 'dNombre',
		id: 'dNombre',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Nombre',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dPaterno',
		id: 'dPaterno',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Apellido Paterno',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dMaterno',
		id: 'dMaterno',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Apellido Materno',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dEmail',
		id: 'dEmail',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Email',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dPerfil',
		id: 'dPerfil',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Perfil Actual',
		value:''
	},{
		xtype: 'combo',	
		name: 'perfil',	
		id: 'cmbPerfil',	
		fieldLabel: 'Perfil a modificar', 
		mode: 'local',	
		hiddenName : 'perfil',	
		emptyText: 'Seleccionar  ',
		forceSelection : true,	
		value : 0,
		triggerAction : 'all',	
		typeAhead: true,
		minChars : 1,	
		anchor: '60%',
		store : catalogoPerfil,	
		displayField : 'descripcion',	
		valueField : 'clave'
	},
	{ 	xtype: 'textfield',  hidden: true,name: 'internacional', id: 'internacional', 	value: '' },		
	{ 	xtype: 'textfield',  hidden: true, id: 'sTipoAfiliado', name: 'sTipoAfiliado', 	value: '' },
	{ 	xtype: 'textfield',  hidden: true,name: 'txtNafinElectronico', id: 'txtNafinElectronico', 	value: '' },
	{ 	xtype: 'textfield',  hidden: true,name: 'txtPerfilAnt', id: 'txtPerfilAnt', 	value: '' },
	{ 	xtype: 'textfield',  hidden: true,name: 'txtLogin', id: 'txtLogin', 	value: '' }
]

var fpDespliega = new Ext.form.FormPanel({
		id					: 'formaDespliega',
		layout			: 'form',
		width				: 600,
		style				: ' margin:0 auto;',
		frame				: true,
		hidden			: false,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 160,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			elementosDespliega
		],
		buttons			: [
			{
				text: 'Modificar',
				id: 'btnAceptaCuenta',
				//iconCls:'aceptar',
				iconCls:'icoModificar',
				handler: function(boton, evento) 
				{
					
							var params = (fpDespliega)?fpDespliega.getForm().getValues():{};				
							//fpDatosInter.el.mask("Procesando", 'x-mask-loading');
							//peticion de Registro de Afiliaci�n
							Ext.Ajax.request({
									url: '15forma05AfianzadoraExt.data.jsp',
									params: Ext.apply(params,{
											informacion: 'modificaUsuario'
									}),
									callback: procesarAlta2
							});
							
				}
			
			},
			{
				text:'Salir',
				iconCls: 'icoRegresar',
				handler: function() {
					ventanaCuentas.setVisible(false);
				}
			}
		]
	});	

var storeUsuarios = new Ext.data.JsonStore({
	
	fields: [
				{name: 'IDCUENTA'},
				{name: 'CUENTA'}

	]
	
});
var gridUsuarios = new Ext.grid.GridPanel({
		store: storeUsuarios,
		id: 'gridUsuarios',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: false,
		title: ' ',
		columns: [	
			{
				xtype:	'actioncolumn',
				header: 'Login del Usuario',
				tooltip: 'Login del Usuario',
				dataIndex: 'CUENTA',
				sortable: true,
				width: 225,				
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return (record.get('CUENTA'));
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							
						}
						,handler:	procesaCuenta  
					}
				]
			},	
			{
				xtype:	'actioncolumn',
				header: 'Cambio de Perfil',
				tooltip: 'Cambio de Perfil',
				dataIndex: 'CUENTAMOD',
				sortable: true,
				width: 225,				
				resizable: true,				
				align: 'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							
								this.items[0].tooltip = 'Modificar';
								return 'modificar';
							
						}
						,handler:	procesaCuenta  
					}
				]
			}
		],			
		bbar: {
					items: ['->','-',
							
								
								{
									xtype: 'button',
									//height: 40,
									text: 'Imprimir',
									iconCls: 'icoPdf',
									id: 'imprimirCuenta',
									handler: function(boton, evento){
										Ext.Ajax.request({
											url: '15forma05AfianzadoraExt.data.jsp',
											params: Ext.apply({informacion:'archivoUsuario',claveAfiliado:noAfianza }),
											callback: procesarArchivoSuccess
										});
									}
								},
								{
									xtype: 'button',
									//height: 40,
									iconCls: 'icoRegresar',
									text: 'Salir',
									id: 'salir',
									handler: function(boton, evento){
										ventanaCuentas.setVisible(false);
									}
								},
								
								'-']
				},
     	stripeRows: true,
		loadMask: true,
		height: 250,
		width: 470,		
		frame: true
	});
	
	var ventanaCuentas = new Ext.Window({
			width: 600,
			height: 'auto',
			//maximizable: true,
			modal: true,
			closable: true,
			closeAction: 'hide',
			resizable: false,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			//layout: 'fit',
			items: [gridUsuarios,fpDespliega]
			
});
// Ventana Cuentas
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 550,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Consultar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
					
						fp.el.mask('Procesando...', 'x-mask-loading');
						
						consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								
								limit: 15})
								});
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});

var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
			fp,  NE.util.getEspaciador(30),grid,fpDatos,ventanaCuentas
	  ]
  });
	catalogoEstado.load();
	catalogoPais.load();
	fpDatos.setVisible(false);
	fpDespliega.setVisible(false);
	//ventanaCuentas.setVisible(true);
});