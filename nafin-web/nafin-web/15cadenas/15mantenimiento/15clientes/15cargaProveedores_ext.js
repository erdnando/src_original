var dt = new Date();
Ext.onReady(function() {

	function verificaPanel(){
		var myPanel = Ext.getCmp('formaBitacora');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype != 'label')	{	//El metodo isValid() no aplica para los tipos de objetos label.
				if (!panelItem.isValid())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}

	var formBit = {
		fecIni:	null,
		fecFin:	null,
		noProv:	null,
		rfcProv:	null,
		noNafin:	null,
		nameNafin:null,
		pantalla:null,
		afilia:	null
	}
	var valInicio = {
		iNoCliente : null,
		strTipoUsuario: null,
		strPerfil: null,
		cmb_grupo: null,
		noBancoFondeo: null
	}
	
	function procesaValoresIniciales(opts, success, response) {
		tp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			valInicio.iNoCliente = jsonValoresIniciales.iNoCliente;
			valInicio.strTipoUsuario = jsonValoresIniciales.strTipoUsuario;
			valInicio.strPerfil = jsonValoresIniciales.strPerfil;
			valInicio.cmb_grupo = jsonValoresIniciales.cmb_grupo;
			valInicio.noBancoFondeo = jsonValoresIniciales.noBancoFondeo;
			if (jsonValoresIniciales.strTipoUsuario == "NAFIN"){
				Ext.getCmp('gridCargaProv').setWidth(940);
				Ext.getCmp('numeroProveedor').hide();
				Ext.getCmp('cmb_pantalla').hide();
				Ext.getCmp('txt_usuario').show();
				fpCargaProv.el.mask('Enviando...', 'x-mask-loading');
				catalogoEPOData.load();
				var cboEpo = Ext.getCmp('cboEpo');
				Ext.getCmp('cboEpo').show();
				Ext.getCmp('formaBitacora').setTitle('Bit�cora de Movimientos de Proveedores');
					var tabCargaProv = tp.getItem('tabCargaProv');
					tp.remove(tabCargaProv,true);
					//tabCargaProv.setDisabled(true);
					//tp.setActiveTab(1);
			}else{
				Ext.getCmp('formaBitacora').setTitle('Bit�cora de Actualizaci�n Masiva de Afiliados');
				Ext.getCmp('numeroProveedor').show();
				Ext.getCmp('cmb_pantalla').show();
				Ext.getCmp('txt_usuario').hide();
//				if (jsonValoresIniciales.strTipoUsuario == "NAFIN"){

//				}
			}
			var tipoPesta = Ext.get('pestana').getValue();
			if (!Ext.isEmpty(tipoPesta)){
				tp.setActiveTab(1);
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDFBit =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFBit');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFBit');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivoBit = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivoBit');
		btnGenerarArchivo.setIconClass('');
		
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivoBit');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaBitData = function(store, arrRegistros, opts) {
		tp.el.unmask();
		if (arrRegistros != null) {
			if (!gridBitacora.isVisible()) {
				gridBitacora.show();
			}
			Ext.getCmp('btnBajarArchivoBit').hide();
			Ext.getCmp('btnBajarPDFBit').hide();
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivoBit');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDFBit');

			var cm = gridBitacora.getColumnModel();
			cm.setHidden(cm.findColumnIndex('CG_RFC'), true);
			cm.setHidden(cm.findColumnIndex('IC_USUARIO'), true);
			cm.setHidden(cm.findColumnIndex('CG_NOMBRE_USUARIO'), true);
			var el = gridBitacora.getGridEl();

			if(store.getTotalCount() > 0) {
				if( valInicio.strTipoUsuario == "EPO" ){
					cm.setHidden(cm.findColumnIndex('CG_RFC'), false);
				}
				if( valInicio.strTipoUsuario != "EPO" ){
					cm.setHidden(cm.findColumnIndex('IC_USUARIO'), false);
					cm.setHidden(cm.findColumnIndex('CG_NOMBRE_USUARIO'), false);
				}
				btnGenerarPDF.enable();
				btnGenerarArchivo.enable();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	function procesaDetalles(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fpCargaProv.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpCargaProv.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var muestraDetalle = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var icBitacora = registro.get('IC_BIT_CARGA');
		var tipoRegistro = "";
		if (colIndex == 4){
			tipoRegistro = "T";
		}else if (colIndex == 5){
			tipoRegistro = "C";
		}else if (colIndex == 6){
			tipoRegistro = "E";
		}
		Ext.Ajax.request({url: '15cargaProveedoresExt.data.jsp',params: Ext.apply({informacion: "obtenDetalles",icBitacora: icBitacora,tipoRegistro:tipoRegistro}),callback: procesaDetalles});
	}

	function procesaConsultaCargaProv(opts, success, response) {
		tp.el.unmask();
		cargaProvData.loadData('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				if (!gridCargaProv.isVisible()){
					gridCargaProv.show();
				}
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnBajarPDF').hide();
				if (infoR.registros != undefined){
					cargaProvData.loadData(infoR.registros);
					var cm = gridCargaProv.getColumnModel();
					cm.setHidden(cm.findColumnIndex('NOMBRE_EPO'), true);
					cm.setHidden(cm.findColumnIndex('CG_NOMBRE_ARCH'), true);
					cm.setHidden(cm.findColumnIndex('CG_NOMBRE_USUARIO'), true);
					if( valInicio.strTipoUsuario == "NAFIN" ){
						cm.setHidden(cm.findColumnIndex('NOMBRE_EPO'), false);
						cm.setHidden(cm.findColumnIndex('CG_NOMBRE_ARCH'), false);
						cm.setHidden(cm.findColumnIndex('CG_NOMBRE_USUARIO'), false);
					}
					Ext.getCmp('btnGenerarPDF').enable();
					Ext.getCmp('btnGenerarArchivo').enable();
					gridCargaProv.getGridEl().unmask();
				}else{
					gridCargaProv.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					Ext.getCmp('btnGenerarPDF').disable();
					Ext.getCmp('btnGenerarArchivo').disable();
				}
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarCatalogoEPO = function(store, arrRegistros, opts) {
		if(store.getTotalCount()>0 ){
		
			if(Ext.isEmpty(Ext.getCmp('cboEpo').getValue())){
				if (catalogoEPOData.findExact("clave", valInicio.iNoCliente) != -1 ) {
					Ext.getCmp('cboEpo').setValue(valInicio.iNoCliente);
					Ext.getCmp('cboEpo').label.update("Nombre de la EPO:  " + valInicio.iNoCliente);
				}
			}
		}
		fpCargaProv.el.unmask();
	}
	
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}

	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPOStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15cargaProveedoresExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoEPO,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoPantallaData = new Ext.data.JsonStore({
		id: 'catalogoPantallaStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15cargaProveedoresExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoPantalla'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15cargaProveedoresExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaBitData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15cargaProveedoresExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaBitacora'
		},
		fields: [
			{name: 'CG_RFC'},
			{name: 'IC_CAMBIO'},
			{name: 'IC_NAFIN_ELECTRONICO'},
			{name: 'PANTALLA'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'IC_USUARIO'},
			{name: 'CG_NOMBRE_USUARIO'},
			{name: 'FECHA_CAMBIO'},
			{name: 'CG_ANTERIOR'},
			{name: 'CG_ACTUAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			beforeLoad:	{fn: function(store, options){
								Ext.apply(options.params, {
									txt_fecha_act_de: formBit.fecIni,
									txt_fecha_act_a:	formBit.fecFin,
									numeroProveedor:	formBit.noProv,
									rfc_prov:			formBit.rfcProv,
									txt_ne:				formBit.noNafin,
									txt_nombre:			formBit.nameNafin,
									cmb_pantalla:		formBit.pantalla,
									cmb_grupo:			valInicio.cmb_grupo,
									noBancoFondeo:		valInicio.noBancoFondeo,
									cmb_afiliado:		formBit.afilia
								});
							}	},
			load: procesarConsultaBitData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaBitData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var cargaProvData = new Ext.data.JsonStore({
		fields:	[{name:	'IC_BIT_CARGA'},{name: 'NOMBRE_EPO'},{name: 'DF_CARGA'},{name: 'CG_NOMBRE_ARCH'},{name: 'CG_NOMBRE_USUARIO'},{name: 'IG_TOTAL_REG'},{name: 'IG_REG_PROC'},{name: 'IG_REG_ERROR'}],
		data:		[{'IC_BIT_CARGA':'','NOMBRE_EPO':'','DF_CARGA':'','CG_NOMBRE_ARCH':'','CG_NOMBRE_USUARIO':'','IG_TOTAL_REG':'','IG_REG_PROC':'','IG_REG_ERROR':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var catalogoBuscaData = new Ext.data.JsonStore({
		id: 'catalogoEPOStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15cargaProveedoresExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoEPO,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var gridBitacora = new Ext.grid.GridPanel({
		store: consultaBitData,
		hidden:true,
		columns: [
			{
				header: 'Num. Nafin Electronico', tooltip: 'Num. Nafin Electronico',
				dataIndex: 'IC_NAFIN_ELECTRONICO',
				sortable: true,	resizable: true,	width: 150,	hideable: false, hidden: false, align:'center'
			},{
				header: 'Pantalla', tooltip: 'Pantalla',
				dataIndex: 'PANTALLA',
				sortable: true,	resizable: true,	width: 200,	hideable: false, hidden: false,	align:'center',
				renderer:  function (texto, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(texto) + '"';
						return texto;
				}
			},{
				header: 'Raz�n Social', tooltip: 'Raz�n Social',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,	resizable: true,	width: 250,	hideable: false, hidden: false,	align:'center',
				renderer:  function (texto, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(texto) + '"';
						return texto;
				}
			},{
				header: 'RFC', tooltip: 'RFC',
				dataIndex: 'CG_RFC',
				sortable: true,	resizable: true,	width: 110,	hideable: false, hidden: true, align:'center'
			},{
				header: 'Usuario - Modifico', tooltip: 'Usuario - Modifico',
				dataIndex: 'IC_USUARIO',
				sortable: true,	resizable: true,	width: 150,	hideable: false, hidden: true, align:'center',
				renderer:  function (texto, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(texto) + '"';
						return texto;
				}
			},{
				header: 'Nombre de Usuario', tooltip: 'Nombre de Usuario',
				dataIndex: 'CG_NOMBRE_USUARIO',
				sortable: true,	resizable: true,	width: 200,	hideable: false, hidden: true, align:'center',
				renderer:  function (texto, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(texto) + '"';
						return texto;
				}
			},{
				header : 'Fecha de Actualizaci�n', tooltip: 'Fecha de Actualizaci�n',
				dataIndex : 'FECHA_CAMBIO', align: 'center',
				sortable : true,	width : 150, hideable: false, hidden: false
			},{
				header: 'Datos Anteriores', tooltip: 'Datos Anteriores',
				dataIndex: 'CG_ANTERIOR',
				sortable: true,	resizable: true,	width: 200,	hideable: false, hidden: false, align:'center',
				renderer:  function (texto, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(texto) + '"';
						return texto;
				}
			},{
				header: 'Datos Nuevos', tooltip: 'Datos Nuevos',
				dataIndex: 'CG_ACTUAL',
				sortable: true,	resizable: true,	width: 250,	hideable: false, hidden: false, align:'center',
				renderer:  function (texto, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(texto) + '"';
						return texto;
				}
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaBitData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFBit',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '15cargaProveedoresExt.data.jsp',
							params: Ext.apply(fpCargaProv.getForm().getValues(),{
										informacion: 'ArchivoPDFBit',
										start: cmpBarraPaginacion.cursor,
										limit: cmpBarraPaginacion.pageSize,
										txt_fecha_act_de: formBit.fecIni,
										txt_fecha_act_a:	formBit.fecFin,
										numeroProveedor:	formBit.noProv,
										rfc_prov:			formBit.rfcProv,
										txt_ne:				formBit.noNafin,
										txt_nombre:			formBit.nameNafin,
										cmb_pantalla:		formBit.pantalla,
										cmb_grupo:			valInicio.cmb_grupo,
										noBancoFondeo:		valInicio.noBancoFondeo,
										cmb_afiliado:		formBit.afilia
										}),
							callback: procesarSuccessFailureGenerarPDFBit
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFBit',
					hidden: true
				},'-',{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivoBit',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15cargaProveedoresExt.data.jsp',
							params:	Ext.apply(fpCargaProv.getForm().getValues(),{ 
											informacion: 'ArchivoCSVBit',
											txt_fecha_act_de: formBit.fecIni,
											txt_fecha_act_a:	formBit.fecFin,
											numeroProveedor:	formBit.noProv,
											rfc_prov:			formBit.rfcProv,
											txt_ne:				formBit.noNafin,
											txt_nombre:			formBit.nameNafin,
											cmb_pantalla:		formBit.pantalla,
											cmb_grupo:			valInicio.cmb_grupo,
											noBancoFondeo:		valInicio.noBancoFondeo,
											cmb_afiliado:		formBit.afilia
											} ),
							callback: procesarSuccessFailureGenerarArchivoBit
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivoBit',
					hidden: true
				}
			]
		}
	});

	var elementosBitacora = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'txt_fecha_act_de',
					id: 'txt_fecha_act_de',
					value: dt.format('d/m/Y'),
					allowBlank: false,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecha: 'txt_fecha_act_a',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txt_fecha_act_a',
					id: 'txt_fecha_act_a',
					value: dt.format('d/m/Y'),
					allowBlank: false,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'txt_fecha_act_de',
					margins: '0 20 0 0'
				}
			]
		},{
			xtype:	'textfield',
			id:		'numeroProveedor',
			name:		'numeroProveedor',
			fieldLabel:	'No. de Proveedor',
			maxLength:	25,
			hidden:	true,
			anchor:	'60%'
		},{
			xtype:	'textfield',
			id:		'rfc_prov',
			name:		'rfc_prov',
			fieldLabel:	'R.F.C. del Proveedor',
			maxLength:	25,
			hidden:	false,
			anchor:	'60%'
		},{
			xtype: 'compositefield',
			fieldLabel: 'No. Nafin Electr�nico',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'txt_ne',
					id:	'txt_ne',
					maxLength:	25,
					msgTarget: 'side',
					flex:1,
					margins: '0 5 0 0'
				},{
					xtype: 'textfield',
					name: 'txt_nombre',
					id:	'txt_nombre',
					maxLength:	50,
					disabled:	true,
					msgTarget: 'side',
					flex:2,
					margins: '0 5 0 0'
				}
			]
		},{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					id:'disEspacio',
					text:''
				},{
					xtype:	'button',
					id:		'btnBuscaA',
					iconCls:	'icoBuscar',
					text:		'B�squeda Avanzada',
					handler: function(boton, evento) {
								var winVen = Ext.getCmp('winBuscaA');
									if (winVen){
										Ext.getCmp('fpWinBusca').getForm().reset();
										Ext.getCmp('fpWinBuscaB').getForm().reset();
										Ext.getCmp('cmb_num_ne').setValue();
										Ext.getCmp('cmb_num_ne').store.removeAll();
										Ext.getCmp('cmb_num_ne').reset();
										winVen.show();
										
									}else{
										var winBuscaA = new Ext.Window ({
											id:'winBuscaA',
											height: 285,
											x: 300,
											y: 100,
											width: 600,
											heigth: 100,
											modal: true,
											closeAction: 'hide',
											title: 'Tipo de Afiliado',
											items:[
												{
													xtype:'form',
													id:'fpWinBusca',
													frame: true,
													border: false,
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													labelWidth: 140,
													items:[
														{
															xtype:'displayfield',
															frame:true,
															border: false,
															value:'Utilice el * para b�squeda gen�rica'
														},{
															xtype: 'textfield',
															name: 'nombre',
															id:	'txtNombre',
															fieldLabel:'Nombre',
															maxLength:	100
														},{
															xtype: 'textfield',
															name: 'rfc',
															id:	'txtRfc',
															fieldLabel:'RFC',
															maxLength:	20
														},{
															xtype: 'textfield',
															name: 'num_ne',
															id:	'txtNe',
															fieldLabel:'No. Nafin Electr�nico',
															maxLength:	25
														}
													],
													buttons:[
														{
															text:'Buscar',
															iconCls:'icoBuscar',
															handler: function(boton) {
																			if ( Ext.isEmpty(Ext.getCmp('txtNombre').getValue()) && Ext.isEmpty(Ext.getCmp('txtRfc').getValue()) && Ext.isEmpty(Ext.getCmp('txtNe').getValue()) ){
																				Ext.Msg.alert(boton.text,'No existe informaci�n con los criterios determinados');
																				return;
																			}
															  catalogoNombreData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues(),{cmb_afiliado: Ext.getCmp('hidAfilia').getValue(),noBancoFondeo: valInicio.noBancoFondeo }) });
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoLimpiar',
															handler: function() {
																			Ext.getCmp('winBuscaA').hide();
																		}
														}
													]
												},{
													xtype:'form',
													frame: true,
													id:'fpWinBuscaB',
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													monitorValid: true,
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													items:[
														{
															xtype: 'combo',
															id:	'cmb_num_ne',
															name: 'cmb_num_ne',
															hiddenName : 'cmb_num_ne',
															fieldLabel: 'Nombre',
															emptyText: 'Seleccione . . .',
															displayField: 'descripcion',
															valueField: 'clave',
															triggerAction : 'all',
															forceSelection:true,
															allowBlank: false,
															typeAhead: true,
															mode: 'local',
															minChars : 1,
															store: catalogoNombreData,
															tpl : NE.util.templateMensajeCargaCombo
														}
													],
													buttons:[
														{
															text:'Aceptar',
															iconCls:'aceptar',
															formBind:true,
															handler: function() {
																			if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																				var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																				var desc = disp.slice(disp.indexOf(" ")+1);
																				Ext.getCmp('txt_ne').setValue(Ext.getCmp('cmb_num_ne').getValue());
																				Ext.getCmp('txt_nombre').setValue(desc);
																				Ext.getCmp('winBuscaA').hide();
																			}
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoLimpiar',
															handler: function() {	Ext.getCmp('winBuscaA').hide();	}
														}
													]
												}
											]
										}).show();
									}
								}
				}
			]
		},{
			xtype: 'combo',
			id:	'cmb_pantalla',
			name: 'cmb_pantalla',
			hiddenName : 'cmb_pantalla',
			fieldLabel: 'Pantalla',
			emptyText: 'Seleccion Pantalla',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			mode: 'local',
			minChars : 1,
			store: catalogoPantallaData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype:'textfield',
			id:'txt_usuario',
			name:'txt_usuario',
			fieldLabel:'Usuario',
			maxLength:	25,
			hidden:	true
		},{
			xtype:'hidden',
			id:	'hidAfilia',
			value: 'P'
		}
	];

	var fpBitacora = new Ext.form.FormPanel({
		id: 'formaBitacora',
		width: 600,
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		style: 'margin: 0 auto',
		bodyStyle:'padding:10px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		labelWidth: 130,
		defaultType: 'textfield',
		items: elementosBitacora,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					gridBitacora.hide();
					if(!verificaPanel()) {
						return;
					}
					var fecIni = Ext.getCmp('txt_fecha_act_de').getValue();
					var fecFin = Ext.getCmp('txt_fecha_act_a').getValue();
					var diferenciaEnDias = mtdCalculaPlazo(fecIni.format('d/m/Y'), fecFin.format('d/m/Y'));
					if(diferenciaEnDias > 7 ){
						Ext.getCmp('txt_fecha_act_a').markInvalid("El periodo m�ximo de consulta es de 7 d�as.");
						return;
					}
					var panta = "";

					if( valInicio.strTipoUsuario == "NAFIN" ){
						panta = 'REGAFILMAS';
					}else{
						panta = Ext.getCmp('cmb_pantalla').getValue();
					}

					formBit.fecIni		=	fecIni.format('d/m/Y');
					formBit.fecFin		=	fecFin.format('d/m/Y');
					formBit.noProv		=	Ext.getCmp('numeroProveedor').getValue();
					formBit.rfcProv	=	Ext.getCmp('rfc_prov').getValue();
					formBit.noNafin	=	Ext.getCmp('txt_ne').getValue();
					formBit.nameNafin	=	Ext.getCmp('txt_nombre').getValue();
					formBit.pantalla	=	panta;
					formBit.afilia		=	Ext.getCmp('hidAfilia').getValue();
					tp.el.mask('Enviando...', 'x-mask-loading').getValue();
					consultaBitData.load({params: Ext.apply({operacion: 'Generar',start: 0,limit: 15})});
				}
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15cargaProveedores_ext.jsp?tipoPesta=Bita';
					//fpBitacora.getForm().reset();
					//gridBitacora.hide();
				}
			},{
				text: 'Regresar',
				id:	'btnRegresar',
				hidden: true,
				iconCls: 'icoLimpiar',
				handler: function(boton,ev) {
								Ext.Msg.alert(boton.text,'Redireccionar al jsp original: 15bitacoracambios');
								var formaHTML = fpBitacora.getForm().getEl().dom;
								formaHTML.action = NE.appWebContextRoot + '/15cadenas/15mantenimiento/15clientes/15bitacoracambios.jsp?cmb_afiliado=P';
								formaHTML.method = "POST";
								formaHTML.submit();
							}
			}
		]
	});

	var gridCargaProv = new Ext.grid.GridPanel({
		id:'gridCargaProv',
		style: 'margin: 0 auto',
		viewConfig: {forceFit: true},
		store: cargaProvData,
		hidden:true,
		columns: [
			{
				header: 'EPO', tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',	align: 'center',
				sortable: true,	resizable: true,	width: 250,	hideable: false, hidden: true
			},{
				header : 'Fecha de Carga de Archivo', tooltip: 'Fecha de Carga de Archivo',
				dataIndex : 'DF_CARGA', align: 'center',
				sortable : true,	width : 150, hideable: false, hidden: false
			},{
				header: 'Nombre Archivo', tooltip: 'Nombre Archivo',
				dataIndex: 'CG_NOMBRE_ARCH',	align: 'center',
				sortable: true,	resizable: true,	width: 250,	hideable: false, hidden: true
			},{
				header: 'Usuario Responsable', tooltip: 'Usuario Responsable',
				dataIndex: 'CG_NOMBRE_USUARIO',	align: 'center',
				sortable: true,	resizable: true,	width: 250,	hideable: false, hidden: true
			},{
				xtype:	'actioncolumn',
				header: 'Total de registros', tooltip: 'Total de registros',
				dataIndex: 'IG_TOTAL_REG',	align: 'center',	sortable: true,	resizable: true,	width: 200,	hideable: false, hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return value+'&nbsp;&nbsp;';
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('IG_TOTAL_REG')	!=	'0' && registro.get('IG_TOTAL_REG')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraDetalle
					}
				]
			},{
				xtype:	'actioncolumn',
				header: 'Registros Procesados', tooltip: 'Registros Procesados',
				dataIndex: 'IG_REG_PROC',	align: 'center',	sortable: true,	resizable: true,	width: 200,	hideable: false, hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return value+'&nbsp;&nbsp;';
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('IG_REG_PROC')	!=	'0' && registro.get('IG_REG_PROC')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraDetalle
					}
				]
			},{
				xtype:	'actioncolumn',
				header: 'Registros Inv�lidos', tooltip: 'Registros Inv�lidos',
				dataIndex: 'IG_REG_ERROR',	align: 'center',	sortable: true,	resizable: true,	width: 200,	hideable: false, hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return value+'&nbsp;&nbsp;';
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('IG_REG_ERROR')	!=	'0' && registro.get('IG_REG_ERROR')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraDetalle
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		//width: 940,
		width: 600,
		frame: true,
		title:'<div align="center">*Hacer clic sobre el n&uacute;mero de registros para descargar el archivo</div>',
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15cargaProveedores_pdf.jsp',
							params: Ext.apply( fpCargaProv.getForm().getValues() ),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},'-',{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15cargaProveedores_csv.jsp',
							params:	Ext.apply( fpCargaProv.getForm().getValues() ),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				}
			]
		}
	});

	var elementosCargaProv = [
		{
			xtype: 'combo',
			id: 'cboEpo',
			name: 'cveEpo',
			hiddenName : 'cveEpo',
			fieldLabel: 'Nombre de la EPO',
			emptyText: 'Seleccione EPO',
			hidden: true,
			mode: 'local',
			minChars : 1,
			typeAhead: true,
			valueField : 'clave',
			triggerAction : 'all',
			displayField : 'descripcion',
			store : catalogoEPOData,
			anchor:'100	%',
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Carga de Archivo',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecini',
					id: 'fecha_carga_ini',
					value: dt.format('d/m/Y'),
					allowBlank: false,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecha: 'fecha_carga_fin',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecfin',
					id: 'fecha_carga_fin',
					value: dt.format('d/m/Y'),
					allowBlank: false,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_carga_ini',
					margins: '0 20 0 0'
				}
			]
		}
	];

	var fpCargaProv = new Ext.form.FormPanel({
		id: 'formCargaProv',
		width: 600,
		title: 'Bit�cora de Carga Masiva de Afiliados',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 4px',
		style: 'margin: 0 auto',
		labelWidth: 150,
		defaultType: 'textfield',
		items: elementosCargaProv,
		monitorValid: true,
		buttons:[
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
								gridCargaProv.hide();
								cargaProvData.loadData('');
								var fecIni = Ext.getCmp('fecha_carga_ini').getValue();
								var fecFin = Ext.getCmp('fecha_carga_fin').getValue();
								var diferenciaEnDias = mtdCalculaPlazo(fecIni.format('d/m/Y'), fecFin.format('d/m/Y'));
								if(diferenciaEnDias > 7 ){
									Ext.getCmp('fecha_carga_fin').markInvalid("El periodo m�ximo de consulta es de 7 d�as.");
									return;
								}
								if( valInicio.strTipoUsuario == "EPO" ){
									if( Ext.getCmp('cboEpo').getValue() != valInicio.iNoCliente ){
										if (catalogoEPOData.findExact("clave", valInicio.iNoCliente) != -1 ) {
											Ext.getCmp('cboEpo').setValue(valInicio.iNoCliente);
										}
									}
								}
								tp.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '15cargaProveedoresExt.data.jsp',
									params: Ext.apply(fpCargaProv.getForm().getValues(),{informacion: 'ConsultaCarga'}),
									callback: procesaConsultaCargaProv
								});
							}
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function(boton, evento) {
								window.location = '15cargaProveedores_ext.jsp';
								//fpCargaProv.getForm().reset();
								//gridCargaProv.hide();
								//Ext.getCmp('cboEpo').setValue(valInicio.iNoCliente);
							}				
			}
		]
	});

	var tp = new Ext.TabPanel({
		id: 'tp',
		activeTab: 0,
		width:940,
		plain:true,
		defaults:{autoHeight: true},
		items:[
			{
				title: 'Carga de Proveedores',
				id: 'tabCargaProv',
				items: [
					{
						xtype: 'panel',
						id: 'msjCargaProv',
						width: 690,
						frame: true,
						hidden: true,
						style: 'margin: 0 auto'
					},
					NE.util.getEspaciador(10),	fpCargaProv,	
					NE.util.getEspaciador(10),	gridCargaProv,
					NE.util.getEspaciador(10)
				]
			},
			{
				title: 'Bit�cora Movimientos de Proveedores',
				id: 'tabBitacora',
				items: [
					{
						xtype: 'panel',
						id: 'msjBitacora',
						width: 690,
						frame: true,
						hidden: true,
						style: 'margin: 0 auto'
					},
					NE.util.getEspaciador(10),	fpBitacora,
					NE.util.getEspaciador(10),	gridBitacora,
					NE.util.getEspaciador(10)
				]
			}
		],
		listeners:{
			tabchange:	function(tab, panel){
								if (panel.getItemId() == 'tabCargaProv') {
									if (gridCargaProv.isVisible){
										gridCargaProv.hide();
									}
								}else if (panel.getItemId() == 'tabBitacora') {
									if (gridBitacora.isVisible){
										gridBitacora.hide();
									}
									if (catalogoPantallaData.getCount() == 0) {
										catalogoPantallaData.load();
									}
								}
							}
		}
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			tp
		]
	});
	
	tp.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url:		'15cargaProveedoresExt.data.jsp',
		params:	{informacion: "valoresIniciales"},
		callback:procesaValoresIniciales
	});

});