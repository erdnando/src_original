Ext.onReady(function() 
{
//--------------------------------- HANDLERS -------------------------------
 
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
				gridGeneral.show();
			}
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
  
		var el = gridGeneral.getGridEl();			
		if(store.getTotalCount() > 0) {				
			btnGenerarArchivo.enable();
			btnBajarArchivo.hide();
  			el.unmask();
		} else {
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
	}
	}
	

 var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) 
 {
	 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	 btnGenerarArchivo.setIconClass('');
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		btnBajarArchivo.show();
		btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivo.setHandler(function(boton, evento) 
		{
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	  } else 
	  {
		btnGenerarArchivo.enable();
		NE.util.mostrarConnError(response,opts);
     }
  }

//-------------------------------- STORES -----------------------------------
  var catologoSector = new Ext.data.JsonStore
  ({
	   id: 'catologoSectorDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15proveedoresext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoSectorDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var catologoEstado = new Ext.data.JsonStore
  ({
	   id: 'catologoEstadoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15proveedoresext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEstadoDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var consultaData = new Ext.data.JsonStore
  ({
			root: 'registros',
			url: '15proveedoresext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
			         {name: 'NUMERO_PROVEEDOR'},
						{name: 'IC_NAFIN_ELECTRONICO'},
						{name: 'CG_RFC'},
						{name: 'CG_RAZON_SOCIAL'},
						{name: 'NOMCONTACTO'},
						{name: 'CG_TELEFONO1'},
						{name: 'CG_EMAIL'},
						{name: 'DIRECCION'},
						{name: 'ESTADO'},
						{name: 'TELEFONOS'},
						{name: 'SECTOR'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
					load: procesarConsultaData,
					exception: {
								fn: function(proxy,type,action,optionsRequest,response,args){
										NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
										procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
								}
					}
			}
  });
  
  var totalesData = new Ext.data.JsonStore({
			root: 'registros',
			url: '15proveedoresext.data.jsp',
			baseParams: { informacion: 'ConsultaTotales' },
			fields: [
						{name: 'TOTALREGISTROS'}
			        ],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
						exception: NE.util.mostrarDataProxyError
			}
  });

  var gridGeneral = new Ext.grid.GridPanel
  ({
		store: consultaData,
		hidden: true,
      id: 'grid',
		columns: [		
			{
				header: 'No Proveedor',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'left',
				hidden: false,
				dataIndex: 'NUMERO_PROVEEDOR'
			},
			{
				header: 'N�mero de Nafin Electr�nico',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'left',
				hidden: false,
				dataIndex: 'IC_NAFIN_ELECTRONICO'
			},
			{
				header: 'RFC',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'CG_RFC'
			},
			{
				header: 'Nombre de la empresa',
				sortable: true,
				resizable: true,
			   width: 250,	
				align: 'left',
				hidden: false,
				dataIndex: 'CG_RAZON_SOCIAL'
			},
			{
				header: 'Contacto',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'left',
				hidden: false,
				dataIndex: 'NOMCONTACTO'
			},
			{
				header: 'Tel�fono',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'CG_TELEFONO1'
			},
			{
				header: 'Correo electr�nico',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'CG_EMAIL'
			},
			{
				header: 'Direcci�n',
				sortable: true,
				resizable: true,
				width: 250,	
				align: 'left',
				hidden: false,
				dataIndex: 'DIRECCION'
			},
			{
				header: 'Estado',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'ESTADO'
			},
			{
				header: 'Tel�fonos',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'TELEFONOS'
			},
			{
				header: 'Sector',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'SECTOR'
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		title: '',
		frame: true,
			bbar: 
	      {
			 xtype: 'paging',
			 pageSize: 15,
			 buttonAlign: 'left',
			 id: 'barraPaginacion',
			 displayInfo: true,
			 store: consultaData,
			 displayMsg: '{0} - {1} de {2}',
			 emptyMsg: 'No hay registros.',
			 items: [
			       '->',/*
			       '-',
							{
								xtype: 'button',
								text: 'Totales',
								id: 'btnTotales',
								hidden: false,
								handler: function (boton,evento) {
											totalesData.load({ params: Ext.apply(fp.getForm().getValues()) });
											var gridTotales = Ext.getCmp('gridTotales');
											if(!gridTotales.isVisible()){
													gridTotales.show();
													gridTotales.el.dom.scrollIntoView();
											};
								}
							},*/
					 '-',
			       {
					  xtype: 'button',
					  text: 'Generar Archivo',
					  id: 'btnGenerarArchivo',
					  handler: function(boton, evento) 
					  {
					   boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
						url:'15proveedoresext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						        informacion: 'ArchivoCSV'}),
						callback: procesarSuccessFailureGenerarArchivo});
					  }
				    },
					 {
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarArchivo',
						hidden: true
					 }
					  ]
			 }
	});
	
	var gridTotales = {
	xtype: 'grid',
	store: totalesData,
	id: 'gridTotales',
	style: ' margin:0 auto;',
	hidden: true,
	columns: [
			    {
					header: 'Total de Registros',
					dataIndex: 'TOTALREGISTROS',
					align: 'center',
					width: 150,
					renderer: Ext.util.Format.numberRenderer('0,000')
				 }
				],
		width: 940,
		height: 93,
		title: 'Totales',
		tools: [
					{
						id: 'close',
						handler: function(evento, toolEl, panel, tc){
						}
					}
		],
		frame: false
  };	

//---------------------------------COMPONENTES-------------------------------
  var elementosForma = 
  [
		         {
				    //Seleccionar SECTOR
			       xtype: 'combo',
			       fieldLabel: 'Seleccionar Sector',
			       emptyText: 'Ninguno',
			       displayField: 'descripcion',
			       valueField: 'clave',
			       triggerAction: 'all',
			       typeAhead: true,
			       minChars: 1,
			       store: catologoSector,
			       tpl: NE.util.templateMensajeCargaCombo,
			       name:'cSector',
					 Id: 'sector',
					 mode: 'local',
					 hiddenName: 'cSector',
					 forceSelection: false
			      },
					{
					 //NO. PROVEEDOR
		           xtype: 'numberfield',
			        fieldLabel: 'N�mero de Nafin Electr�nico',
			        inputType: 'text',
			        maxLength: 38,
			        name: 'txtNafinElectronico',
					  id: 'nafinElectronico',
					  allowBlank: true
		         },
			      {
					 //NOMBRE
			       xtype: 'textfield',
			       fieldLabel: 'Nombre',
			       inputType: 'text',
			       maxLength: 100,
			       name: 'txtNombre',
					 id: 'nombre',
					 allowBlank: true
			      },
			      {
					 //Seleccionar ESTADO
			       xtype: 'combo',
			       fieldLabel: 'Estado',
			       emptyText: 'Seleccionar Estado...',
			       name:'cEstado',
			       displayField: 'descripcion',
			       valueField: 'clave',
			       triggerAction: 'all',
			       typeAhead: true,
			       minChars: 1,
			       store: catologoEstado,
			       tpl: NE.util.templateMensajeCargaCombo,
					 Id: 'estado',
					 mode: 'local',
					 hiddenName: 'cEstado',
					 forceSelection: false
			      },
			      {
					 //RFC
			       xtype: 'textfield',
			       fieldLabel: 'RFC',
			       inputType: 'text',
			       maxLength: 15,
			       name: 'txtRFC',
					 id: 'rfc',
					 allowBlank: true
			      },
			      {
					 //NO. PROVEEDOR
			       xtype: 'textfield',
			       fieldLabel: 'N�mero de Proveedor',
			       inputType: 'text',
			       maxLength: 25,
			       name: 'txtNumeroProveedor',
					 id: 'numeroProveedor',
					 allowBlank: true
			      },
			      {
					 //Checkbox PYMES
			       xtype: 'checkbox',
                boxLabel: 'Pymes sin n�mero de proveedor',
			       name: 'cbPymesSinNumProv',
					 value: 'checked'
			       }
	];
  
  var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		labelWidth: 130,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
			  {
			   /*var gGridTotales = Ext.getCmp('gridTotales');
			   gGridTotales.hide();*/
				fp.el.mask('Enviando...', 'x-mask-loading');	
			   consultaData.load({ params: Ext.apply(fp.getForm().getValues(),{
				//modalidad_plazo:modo,
									operacion: 'Generar', //Generar datos para la consulta
									start: 0,
									limit: 15})
							         });
			  }
			 }
		  ]
  });


//-------------------------------- PRINCIPAL -----------------------------------
  var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 949,
	  items: 
	    [ 
	     fp,
	     NE.util.getEspaciador(20),
		  gridGeneral/*,
		  gridTotales*/
	    ]
  });
  
  catologoSector.load();
  catologoEstado.load();

});