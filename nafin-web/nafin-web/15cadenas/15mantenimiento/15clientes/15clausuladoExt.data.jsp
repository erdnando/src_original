<%@ page import="
			java.util.*,
			java.sql.*,
			com.netro.afiliacion.*,
			com.netro.cadenas.*,
			 com.netro.descuento.*,
			com.netro.exception.*,
			com.netro.model.catalogos.*,
			netropology.utilerias.*,
			net.sf.json.JSONArray,net.sf.json.JSONObject"
contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%

//Parámetros provenienen de la página anterior y actual

String ic_consecutivo = (request.getParameter("ic_consecutivo") == null)?"":request.getParameter("ic_consecutivo");
//String ic_epo				= (request.getParameter("ic_epo")==null)?epoDefault:request.getParameter("ic_epo");
String ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String txt_nombre			= (request.getParameter("hid_nombre")==null)?"":request.getParameter("hid_nombre");
String txt_nafelec			= (request.getParameter("txt_nafelec")==null)?"":request.getParameter("txt_nafelec");
String txt_usuario			= (request.getParameter("txt_usuario")==null)?"":request.getParameter("txt_usuario");
String txt_fecha_acep_de	= (request.getParameter("txt_fecha_acep_de")==null)?"":request.getParameter("txt_fecha_acep_de");
String txt_fecha_acep_a		= (request.getParameter("txt_fecha_acep_a")==null)?"":request.getParameter("txt_fecha_acep_a");
String ic_if				= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
String noBancoFondeo			= (request.getParameter("noBancoFondeo")==null)?"":request.getParameter("noBancoFondeo");

String hidAction		= (request.getParameter("hidAction")==null)?"":request.getParameter("hidAction");
//String operacion		= (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String resultado=null;

//String ic_epo				= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");

if(informacion.equals("initPage")) {
boolean banderaEpo;
	if(strTipoUsuario.equals("IF"))
	ic_if = iNoCliente;
	String epoDefault = "";
	banderaEpo=false;
if(strTipoUsuario.equals("EPO")){
	banderaEpo=true;
	epoDefault = iNoCliente;
}

//com.netro.seguridadbean.SeguridadHome seguridadHome = (com.netro.seguridadbean.SeguridadHome)netropology.utilerias.ServiceLocator.getInstance().getEJBHome("SeguridadEJB", com.netro.seguridadbean.SeguridadHome.class);
      		//com.netro.seguridadbean.Seguridad	seguridadBean = seguridadHome.create();
com.netro.seguridadbean.Seguridad seguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);

String ic_epo				= (request.getParameter("ic_epo")==null)?epoDefault:request.getParameter("ic_epo");
	String cvePerf = seguridadBean.getTipoAfiliadoXPerfil(strPerfil);
		try{
			//seguridadBean.validaFacultad( (String) strLogin, (String) strPerfil,(String) session.getAttribute("sesCvePerfilProt"), "15CLAUSULADO", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("ic_if", ic_if);
			jsonObj.put("ic_epo", ic_epo);
			jsonObj.put("cvePerf",cvePerf);
			jsonObj.put("BanderaEpo",new Boolean(banderaEpo));
			jsonObj.put("msg","ok");
			resultado = jsonObj.toString();	
				
			
		}catch(Exception seg){
		
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("BanderaEpo",new Boolean(banderaEpo));
			jsonObj.put("success", new Boolean(false));
			resultado = jsonObj.toString();	
		}
}
else if(informacion.equals("catalogoBancoFondeo")) {
	String cvePerf	= (request.getParameter("cvePerf")==null)?"":request.getParameter("cvePerf");
	String ic_epo= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	CatalogoSimple cat = new CatalogoSimple();
	 if ("8".equals(cvePerf)){ 					 
					  cat.setTabla("comcat_banco_fondeo");
					  cat.setCampoClave("ic_banco_fondeo");
					  cat.setCampoDescripcion("cd_descripcion");
					  cat.setCondicionIn("2","ic_banco_fondeo");
					  resultado= cat.getJSONElementos();				  				  
				 }
	else if("4".equals(cvePerf))
				 {			
					  cat.setTabla("comcat_banco_fondeo");
					  cat.setCampoClave("ic_banco_fondeo");
					  cat.setCampoDescripcion("cd_descripcion");
					  cat.setOrden("1");
					  resultado= cat.getJSONElementos();				
				 }
				 else
				 {
				  JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("msg","no result");
			resultado = jsonObj.toString();
				 }
				 
				 
}
else if(informacion.equals("catalogoNombreEpo")) {
String cvePerf	= (request.getParameter("cvePerf")==null)?"":request.getParameter("cvePerf");
String ic_epo				= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
 CatalogoEPO catEpo = new CatalogoEPO();
	if ("8".equals(cvePerf)){ 				 
					  
					 
						catEpo.setCampoClave("ic_epo");
						catEpo.setCampoDescripcion("cg_razon_social"); 
						catEpo.setClaveBancoFondeo("2");
						catEpo.setOrden("2");
		            resultado = catEpo.getJSONElementos();
						
				 }
	else if ("4".equals(cvePerf)){
		  catEpo.setCampoClave("ic_epo");
						catEpo.setCampoDescripcion("cg_razon_social"); 
						catEpo.setClaveBancoFondeo(noBancoFondeo);
						catEpo.setOrden("2");
		            resultado = catEpo.getJSONElementos();					   
	}
	else if("1".equals(cvePerf))
				 {
				 if(strTipoUsuario.equals("EPO")){
				 ic_epo=iNoCliente;
				 JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("ic_epo", ic_epo);
			jsonObj.put("msg","ok");
			resultado = jsonObj.toString();
				 }
				 else {
					if(strTipoUsuario.equals("NAFIN")){
								CatalogoEPO  cEpo=new CatalogoEPO();
								cEpo.setCampoClave("ic_epo");
								cEpo.setCampoDescripcion("cg_razon_social");
								cEpo.setAceptacionRelacionIfEpo("H");
								cEpo.setOrden("2");
								resultado= cEpo.getJSONElementos();	
					}else if(strTipoUsuario.equals("IF")){
								CatalogoEPO  cEpo=new CatalogoEPO();
								cEpo.setCampoClave("ic_epo");
								cEpo.setCampoDescripcion("cg_razon_social");
								cEpo.setAceptacionRelacionIfEpo("H");
								cEpo.setClave_if(iNoCliente);
								cEpo.setOrden("2");
								resultado= cEpo.getJSONElementos();	
								}							
						}
				 }
}
else if(informacion.equals("pymeNombre")) {

	String 				qrySentencia 	= null;
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	CQueryHelper queryHelper = null;
	AccesoDB con = null;
	String ic_epo= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
try{
	con = new AccesoDB();
	con.conexionDB();
	//ic_epo=iNoCliente;
	queryHelper = new CQueryHelper(new ClausuladoNafinCA());
	queryHelper.cleanSession(request);
	if(!"".equals(txt_nafelec) && "".equals(ic_pyme)) {// && !"".equals(icEPO)
							String condicion = "";
							String mensaje = "";
							if(!"".equals(ic_epo)) {
								condicion += "    AND cpe.ic_epo = ?"  ;
								mensaje = "El nafin electronico no corresponde a una\\nPyME afiliada a la EPO o no existe.";
							} else 
								mensaje = "El nafin electronico no corresponde a una PyME o no existe.";
								qrySentencia =
									" SELECT pym.ic_pyme, pym.cg_razon_social"   +
									"   FROM comrel_nafin crn, comrel_pyme_epo cpe, comcat_pyme pym"   +
									"   WHERE crn.ic_epo_pyme_if = cpe.ic_pyme"   +
									"   AND cpe.ic_pyme = pym.ic_pyme"   +
									"   AND crn.ic_nafin_electronico = ?"   +
									"   AND crn.cg_tipo = 'P'"   +
									"   AND cpe.cs_habilitado = 'S'"   +
									"   AND cpe.cg_pyme_epo_interno IS NOT NULL"   +
									condicion;
								ps = con.queryPrecompilado(qrySentencia);
								ps.setInt(1,Integer.parseInt(txt_nafelec));
								if(!"".equals(ic_epo))
									ps.setInt(2,Integer.parseInt(ic_epo));
									rs = ps.executeQuery();
								if(rs.next()){
									ic_pyme = rs.getString(1)==null?"":rs.getString(1);
									txt_nombre = rs.getString(2)==null?"":rs.getString(2);
									
								} else {ic_pyme = "";txt_nombre = "";
								}
								rs.close();
								ps.close();
							}
	} catch(Exception e) {
	out.println(e.getMessage()); 
	e.printStackTrace();
} finally {	
	if(con.hayConexionAbierta()) 
		con.cierraConexionDB();	
}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("pyme", txt_nombre);
	jsonObj.put("ic_pyme",ic_pyme);
	resultado = jsonObj.toString();		
	}
	else if(informacion.equals("Consulta")) {
		String operacion	= (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
		String ic_epo= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		if(strTipoUsuario.equals("EPO"))
		ic_epo = iNoCliente;
		JSONObject 	resultado1	= new JSONObject();
		
		if(strTipoUsuario.equals("IF")){ //??
		if(ic_if.equals("")){
			ic_if=iNoCliente;
		}
		}
		if(!txt_nafelec.equals("")&&ic_pyme.equals("")){
			ic_pyme=txt_nafelec;
		}
		com.netro.cadenas.ConsClausulado paginador=new com.netro.cadenas.ConsClausulado();
		paginador.setIc_epo(ic_epo);
		paginador.setIc_pyme(ic_pyme);
		paginador.setTxt_usuario(txt_usuario);
		paginador.setTxt_fecha_acep_de(txt_fecha_acep_de);
		paginador.setTxt_fecha_acep_a(txt_fecha_acep_a);
		paginador.setIc_if(ic_if);
		paginador.setNoBancoFondeo(noBancoFondeo);
		CQueryHelperRegExtJS queryHelper	=	new CQueryHelperRegExtJS( paginador ); 
		
		if((informacion.equals("Consulta") && operacion.equals("Generar"))
		|| (informacion.equals("Consulta") && operacion.equals(""))
		){ 
		try {
		if(operacion.equals("Generar"))
		{
			queryHelper.executePKQuery(request);
		}
		 int start = 0;
		int limit = 0;
		start = Integer.parseInt(request.getParameter("start"));
        limit = Integer.parseInt(request.getParameter("limit"));
				 //resultado = queryHelper.getJSONPageResultSet(request,start,limit);
				Registros registros = queryHelper.getPageResultSet(request,start,limit);
				while(registros.next()){
					String epo= registros.getString("IC_EPO");
					String consecutivo =registros.getString("IC_CONSECUTIVO");
					String tan = paginador.getTamañoDoc(epo,consecutivo );
					registros.setObject("DOC_CONTRATO",tan);
				 }
				String consulta2	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
				resultado1 = JSONObject.fromObject(consulta2);
				resultado	=resultado1.toString();
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		} else if(operacion.equals("XLS")){
			JSONObject jsonObj = new JSONObject();
			
			String nombreArchivo = 
				queryHelper.getCreateCustomFile(request, strDirectorioTemp,operacion);
				jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
					resultado = jsonObj.toString();
		}else if(operacion.equals("PDF")){
			JSONObject jsonObj = new JSONObject();
			 int start = 0;
		int limit = 0;
		start = Integer.parseInt(request.getParameter("start"));
        limit = Integer.parseInt(request.getParameter("limit"));
			String nombreArchivo = 
				queryHelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp,operacion);
				jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
					resultado = jsonObj.toString();
		
		
		}
	}
	//
	else if(informacion.equals("Contrato")){
		JSONObject jsonObj = new JSONObject();
		String ic_epo= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		ContratoEpoArchivo contratoArchivo = new ContratoEpoArchivo();
		contratoArchivo.setClaveEpo(ic_epo);
		contratoArchivo.setConsecutivo(ic_consecutivo);
		String extension = contratoArchivo.getExtension();
		try{
			String nombreArchivo = contratoArchivo.getArchivoContratoEpo(strDirectorioTemp);
			jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
					resultado = jsonObj.toString();
					System.out.println("RESULTADO CONTRATO -- >> "+resultado);
		}
		catch (Exception e)
		{
			jsonObj.put("success", new Boolean(false));
					jsonObj.put("urlArchivo", strDirecVirtualTemp);
					resultado = jsonObj.toString();
		}
	}
	else if (informacion.equals("CatalogoBuscaAvanzada")) {
	String num_pyme	= (request.getParameter("num_pyme")==null ||	request.getParameter("num_pyme").equals(""))
		?"":request.getParameter("num_pyme");
	String rfc_pyme		= (request.getParameter("rfc_pyme")==null || 	request.getParameter("rfc_pyme").equals(""))
		?"":request.getParameter("rfc_pyme");
	String nombre_pyme	= (request.getParameter("nombre_pyme")==null || 	request.getParameter("nombre_pyme").equals(""))
		?"":request.getParameter("nombre_pyme");
	
	String ic_producto_nafin = (request.getParameter("ic_producto_nafin")==null)?"":request.getParameter("ic_producto_nafin");
	//String ic_epo ="";// iNoCliente;
	
	String DefaultEpo = "";
	if(strTipoUsuario.equals("EPO")){
		DefaultEpo = iNoCliente;
	}
	String ic_epo	= (request.getParameter("ComboEpo")==null)?DefaultEpo:request.getParameter("ComboEpo");	
	String pantalla = "";// "InformacionDocumentos";
	

	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion afiliacion = afiliacionHome.create();
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

	Registros registros = afiliacion.getProveedores(ic_producto_nafin, ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pyme,pantalla,ic_if);
	
	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	JSONObject jsonObjP;// = new JSONObject();
			
			/*String nombreArchivo = 
				queryHelper.getCreateCustomFile(request, strDirectorioTemp,operacion);
				jsonObj.put("success", new Boolean(true));*/
			
	while (registros.next()){
		//ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		
		jsonObjP= new JSONObject();		
		jsonObjP.put("clave", registros.getString("IC_NAFIN_ELECTRONICO"));
		jsonObjP.put("descripcion", registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("CG_RAZON_SOCIAL"));
		jsonObjP.put("ic_pyme",registros.getString("IC_PYME"));
		jsonArr.add(jsonObjP);
		//elementoCatalogo.setClave();
		//elementoCatalogo.setDescripcion(registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("CG_RAZON_SOCIAL"));
		//coleccionElementos.add(elementoCatalogo);
	}	
	/*Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		
		}
	}*/

	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	resultado = jsonObj.toString();
}
	
%>
<%=resultado%>