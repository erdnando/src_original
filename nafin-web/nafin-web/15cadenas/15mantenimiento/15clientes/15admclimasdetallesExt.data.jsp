<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject, 
		com.netro.seguridad.*,
		java.io.*,
		com.jspsmart.upload.*,
		java.sql.*,
		netropology.utilerias.usuarios.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,	
		com.netro.cadenas.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%

String infoRegresar ="";

CreaArchivo 		archivo 				= new CreaArchivo();
StringBuffer 		contenidoArchivo 	= new StringBuffer();
String 				nombreArchivo 		= null;

String tipo 		= (request.getParameter("tipo") 			== null) ? "" : request.getParameter("tipo");
String procesoID 	= (request.getParameter("procesoID") 	== null) ? "" : request.getParameter("procesoID");
String sistema 	= (request.getParameter("sistema") 	== null) ? "" : request.getParameter("sistema");

// 		Nota: Adaptar el codigo para que traiga e inserte los registros por bloques y asi evitar que se acabe
// 	la memoria disponible, si el archivo excediera los 4 MB comprimir el archivo.
//			Restricciones actuales:
//				Reporte de pymes con validacion exitosa
//					Muestra un maximo de 100000 registros exitosos o
//					el maximo que quepa en un archivo de 2MB
//				Reporte de errores
//					Muestra un maximo de 1000 errores
//					o solo 500 errores si el archivo excede los 2MB


	// Contar lineas con error
	AccesoDB 		con 						= new AccesoDB();
	List 				lvarbind 				= new ArrayList();
	Registros 		registros 				= null;
	String 			query 					= "";
	contenidoArchivo.append("No. Linea, RFC\n");

	if(tipo.equals("S")){
		// Extraer registros sin error
		try{
			con.conexionDB();
			query =
				"SELECT " +
						"IC_NUMERO_LINEA, CG_RFC " +
					"FROM " ;
					if(sistema.equals("nae"))
						query += "COMTMP_AFILIA_PYME_MASIVA ";
					if(sistema.equals("econ"))
						query += "ECON_AFILIA_PYME_MASIVA ";
					query += "WHERE " +
						"IC_NUMERO_PROCESO = ? AND "+
						"CG_MENSAJES_ERROR IS NULL AND "+
						"ROWNUM <= 100000"; // 100000 registros equivalen aproximadamente a 3MB
			lvarbind.add(procesoID);
			registros = con.consultarDB(query,lvarbind,false);
		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		// Extraer registros sin error
		while(registros != null && registros.next()){
				contenidoArchivo.append(registros.getString("IC_NUMERO_LINEA") +","+
												registros.getString("CG_RFC")				+","+"\n");
				if(contenidoArchivo.length() > 2000000){ // el archivo no puede ser mayor a 2 MB, restricciones de memoria
					contenidoArchivo.append(",El archivo ha excedido la maxima longitud permitida... la salida ha sido truncada,\n");
					break;
				}
		}
	}else if(tipo.equals("C")){ // extraer registros con errores
		// Extraer registros sin error
		try{
			con.conexionDB();
			query =
				"SELECT " +
						"CG_MENSAJES_ERROR " +
					"FROM ";
					if(sistema.equals("nae"))
						query += "COMTMP_AFILIA_PYME_MASIVA ";
					if(sistema.equals("econ"))
						query += "ECON_AFILIA_PYME_MASIVA ";
					query += "WHERE " +
						"IC_NUMERO_PROCESO = ? AND "+
						"CG_MENSAJES_ERROR IS NOT NULL AND " +
						"ROWNUM <= 1000"+ // Solo se pueden mostrar como maximo 750 mensajes de error, restricciones de memoria
						"AND CG_MENSAJES_ERROR NOT LIKE '%El proveedor ya se encuentra dado de alta con la EPO%'";
			lvarbind.add(procesoID);
			registros = con.consultarDB(query,lvarbind,false);
		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		// Extraer registros sin error
		while(registros != null && registros.next()){
				contenidoArchivo.append(registros.getString("CG_MENSAJES_ERROR")+ "\n"); 		// <-- En una version futura cambiar por un proceso que detecte si debe o no agregar la comilla al final
																														// esto se haria en la seccion donde se trunca la cadena de error en el metodo: validaDatosPymeMasiva
																														// del Bean de Afiliacion
				if(contenidoArchivo.length() > 2000000){ // el archivo de error no puede ser mayor a 2 MB, restricciones de memoria
					contenidoArchivo.append(",El archivo de errores ha excedido la maxima longitud permitida... la salida ha sido truncada,\n");
					break;
				}
		}
	}else{ // extraer registros previamente registrados
		try{
			con.conexionDB();
			query =
				"SELECT " +
						"CG_MENSAJES_ERROR " +
					"FROM " +
						"COMTMP_AFILIA_PYME_MASIVA " +
					"WHERE " +
						"IC_NUMERO_PROCESO = ? AND "+
						"CG_MENSAJES_ERROR IS NOT NULL AND " +
						"ROWNUM <= 1000"+ // Solo se pueden mostrar como maximo 750 mensajes de error, restricciones de memoria
						"AND CG_MENSAJES_ERROR LIKE '%El proveedor ya se encuentra dado de alta con la EPO%'";
			lvarbind.add(procesoID);
			registros = con.consultarDB(query,lvarbind,false);
		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		// Extraer registros sin error
		while(registros != null && registros.next()){
				contenidoArchivo.append(registros.getString("CG_MENSAJES_ERROR")+ "\n"); 		// <-- En una version futura cambiar por un proceso que detecte si debe o no agregar la comilla al final
																														// esto se haria en la seccion donde se trunca la cadena de error en el metodo: validaDatosPymeMasiva
																														// del Bean de Afiliacion
				if(contenidoArchivo.length() > 2000000){ // el archivo de error no puede ser mayor a 2 MB, restricciones de memoria
					contenidoArchivo.append(",El archivo de errores ha excedido la maxima longitud permitida... la salida ha sido truncada,\n");
					break;
				}
		}
	}

	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
		//out.print("<--!Error al generar el archivo-->");
	}else{
		nombreArchivo = archivo.nombre;
	}


	String retorno = strDirecVirtualTemp+nombreArchivo;
	JSONObject jsonObj 	      = new JSONObject();
	
	jsonObj.put("urlArchivo",retorno);
	jsonObj.put("success",new Boolean(true));
	infoRegresar = jsonObj.toString();
							
%>
<%=infoRegresar%>