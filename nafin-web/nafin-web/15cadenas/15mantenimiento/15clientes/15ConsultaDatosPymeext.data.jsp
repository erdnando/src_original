<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*,
		com.netro.exception.*,
		com.netro.afiliacion.*,
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String icEPO = (request.getParameter("HicEPO")!=null)?request.getParameter("HicEPO"):"";
	
	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion afiliacionBean = afiliacionHome.create();
	Afiliacion afiliacionBean = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	String infoRegresar = "", claveIF = "";
	if(!strTipoUsuario.equals("EPO")){   claveIF = iNoCliente;  }
	if(strTipoUsuario.equals("EPO")){	 icEPO = iNoCliente;  } 
	
	
	if(informacion.equals("catalogoEPO")){
		if(!strTipoUsuario.equals("EPO")){
			CatalogoEPO cat = new CatalogoEPO();
			cat.setCampoClave("ic_epo");
			cat.setHabilitado("H");
			cat.setCampoDescripcion("cg_razon_social");
			cat.setClaveIf(iNoCliente);
			infoRegresar = cat.getJSONElementos();
		}else{
			infoRegresar = 
							"{\"success\":false,\"total\":\"0\",\"registros\":[]}";
		}
		
		
	  	
	}
	else if(informacion.equals("CatalogoPais")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_pais");
		cat.setOrden("ic_pais");
		infoRegresar = cat.getJSONElementos();
	}
	else if(informacion.equals("CatalogoEstado")){
		String pais = (request.getParameter("pais")!=null)?request.getParameter("pais"):"";
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre");
		cat.setClavePais(pais);
		cat.setCampoLlave("ic_estado");
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}
	else if(informacion.equals("CatalogoMunicipio")){
		String pais = (request.getParameter("pais")!=null)?request.getParameter("pais"):"";
		String estado = (request.getParameter("estado")!=null)?request.getParameter("estado"):"";
		CatalogoMunicipio cat = new CatalogoMunicipio();
		cat.setClave("ic_municipio");
		cat.setDescripcion("cd_nombre");
		cat.setPais(pais);
		cat.setEstado(estado);
		cat.setOrden("2");
		List catalogo = cat.getListaElementos();
		System.out.println("pais===="+pais);
		System.out.println("estado===="+estado);
		System.out.println("catalogo===="+catalogo);
		JSONArray jsonArray = JSONArray.fromObject(catalogo);
		infoRegresar = "{\"success\": true, \"total\": \"" + 
				jsonArray.size() + "\", \"registros\": " + jsonArray.toString()+"}";
	}
	else if(informacion.equals("ValidarPyme")){
		
		String nafinElectronico = (request.getParameter("claveNafinElectronico")!=null)?request.getParameter("claveNafinElectronico"):"";
		HashMap map = new HashMap();
		try{
			map = afiliacionBean.obtenerPymeAfiliada (icEPO, nafinElectronico);
			String nombrePyme = (String)map.get("nombrePyme");
			String clavePyme = (String)map.get("clavePyme");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("nombrePyme",nombrePyme);
			jsonObj.put("clavePyme",clavePyme);
			infoRegresar = jsonObj.toString();
		}catch(Exception e){
			System.err.println(e);
			throw new AppException("Error al intentar verificar si es una pyme válida.");
		}
	}
	else if(informacion.equals("Consulta")||informacion.equals("ArchivoCSV")
	||informacion.equals("ArchivoXpaginaPDF")){
		int start = 0;
		int limit = 0;
		String operacion = (request.getParameter("operacion") == null) ? "" : request.getParameter("operacion");
		
		String clavePyme		= (request.getParameter("clavePyme") == null)?"":request.getParameter("clavePyme");
		String pais  			= (request.getParameter("pais") == null)?"":request.getParameter("pais");
		String estado			= (request.getParameter("estado") == null)?"":request.getParameter("estado");
		String delegacion		= (request.getParameter("delegacion")==null)?"":request.getParameter("delegacion");
		String fechaActualizacion_Min		= (request.getParameter("fechaActualizacion_Min")==null)?"":request.getParameter("fechaActualizacion_Min");
		String fechaActualizacion_Max				= (request.getParameter("fechaActualizacion_Max")==null)?"":request.getParameter("fechaActualizacion_Max");
		String claveIf = (request.getParameter("claveIf")==null)?"":request.getParameter("claveIf");
		String nafinElectronico = (request.getParameter("claveNafinElectronico")!=null)?request.getParameter("claveNafinElectronico"):"";
		if(!strTipoUsuario.equals("EPO")){			
			claveIf=iNoCliente;
		}		
		if(!nafinElectronico.equals("")){
			HashMap map = new HashMap();
			map = afiliacionBean.obtenerPymeAfiliada (icEPO, nafinElectronico);
			clavePyme = (String)map.get("clavePyme");
		}	
				
		ConsDatosPymesNuevo paginador = new ConsDatosPymesNuevo();
		paginador.setClaveEpo(icEPO);
		paginador.setClaveIf(claveIf);
		paginador.setClavePyme(clavePyme);
		paginador.setClavePais(pais);
		paginador.setClaveEstado(estado);
		paginador.setClaveDelegacionMunicipio(delegacion);
		paginador.setFechaActualizacionMin(fechaActualizacion_Min);
		paginador.setFechaActualizacionMax(fechaActualizacion_Max);

		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		if(informacion.equals("Consulta")){
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			}catch(Exception e){
				throw new AppException("Error en los parámetros recibidos",e);
			}
			try{
				if(operacion.equals("Generar")){//Nueva consulta
					queryHelper.executePKQuery(request);
				}
				infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
			}catch(Exception e){
				throw new AppException("Error en la paginación", e);
			}
		}
		else if(informacion.equals("ArchivoCSV")){
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo CSV",e);
			}
		}
		else if (informacion.equals("ArchivoXpaginaPDF")) {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp, "PDF");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
	}
	else if(informacion.equals("CatalogoProveedor")){
		try{
			String clavePymeSelecionada = (request.getParameter("clavePymeSelecionada") == null)?"":request.getParameter("clavePymeSelecionada");
			String clavePymeIntroducida = (request.getParameter("clavePymeIntroducida") == null)?"":request.getParameter("clavePymeIntroducida");
			String rfcIntroducido = (request.getParameter("rfcIntroducido") == null)?"":request.getParameter("rfcIntroducido");
			String nombrePymeIntroducido = (request.getParameter("nombrePymeIntroducido") == null)?"":request.getParameter("nombrePymeIntroducido");
			String nafinElectronico = (request.getParameter("claveNafinElectronico")!=null)?request.getParameter("claveNafinElectronico"):"";
		
			List parametros = new ArrayList();
			
			parametros.add("");// IF en la
			parametros.add(icEPO);					
			parametros.add(clavePymeIntroducida);						
			parametros.add(rfcIntroducido);
			parametros.add(nombrePymeIntroducido);
			

			List catalogoProveedor = afiliacionBean.obtenerProveedoresBusqueda(parametros);
			JSONArray jsonArray = JSONArray.fromObject(catalogoProveedor);
			infoRegresar = 
							"{\"success\": true, \"total\": \""+ catalogoProveedor.size() +"\", \"registros\": " + jsonArray.toString()+"}";
		}catch(Exception e){
			throw new AppException("Error al intentar generar los datos",e);
		}
	}
%>
<%=infoRegresar%>
