<% String version = (String)session.getAttribute("version"); %>
<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<html>

<%
String ic_mandante = (request.getParameter("ic_mandante")!=null)?request.getParameter("ic_mandante"):""; 
String tipoPersona = (request.getParameter("tipoPersona")!=null)?request.getParameter("tipoPersona"):""; 
%>


<head>
<title>Nafi@net - Afiliados</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="15forma2Man_Mod.js?<%=session.getId()%>"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
	<div id="_menuApp"></div>
   <div id="Contcentral">
<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
<%}%>
<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
</div>
</div>
<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>

<form id='formParametros' name="formParametros">
	<input type="hidden" id="ic_mandante" name="ic_mandante" value="<%=ic_mandante%>"/>	
	<input type="hidden" id="tipoPersona" name="tipoPersona" value="<%=tipoPersona%>"/>	
</form>

<form id='formAux' name="formAux" target='_new'></form>

</body>
</html>
