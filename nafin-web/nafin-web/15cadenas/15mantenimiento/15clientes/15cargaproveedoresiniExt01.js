														/*	MIGRACION ADMIN PROMO NAFIN Administracion-Afiliados-Bitacora de Cambios */
														
Ext.onReady(function(){

// Custom Action Column Component
		Ext.grid.CustomizedActionColumn = Ext.extend(Ext.grid.ActionColumn, {
			constructor: function(cfg) {
				  var me = this,
						items = cfg.items || (me.items = [me]),
						l = items.length,
						i,
						item;
		
				  Ext.grid.CustomizedActionColumn.superclass.constructor.call(me, cfg);
		
				  // Renderer closure iterates through items creating an <img> element for each and tagging with an identifying 
				  // class name x-action-col-{n}
				  me.renderer = function(v, meta) {
						// Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
						v = Ext.isFunction(cfg.renderer) ? cfg.renderer.apply(this, arguments)||'' : '';
		
						meta.css += ' x-action-col-cell';
						for (i = 0; i < l; i++) {
							 item = items[i];
							 if( Ext.isEmpty(item) ) continue;
							 
							 // Check for icon position
							 var iconPosition = !Ext.isEmpty(item.iconPosition) && "right" == item.iconPosition?"right":"left";
							
							 // Render icon to left side
							 if( "right" == iconPosition ){
								 
								 v += Ext.isEmpty(item.text)?'':'<a class="x-action-col-icon x-action-col-' + String(i) + '" style="color:#0000FF;text-decoration:underline;" >'+item.text+'</a>&nbsp;';
								 v += '<img alt="' + ( item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
										'" class="x-action-col-icon x-action-col-' + String(i) + ' ' + (item.iconCls || '') +
										' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope||this.scope||this, arguments) : '') + '"' +
										((item.tooltip) ? ' ext:qtip="' + item.tooltip + '"' : '') + ' align="middle" />'; 
								 
							 // Render icon to right side
							 } else if (  "left" == iconPosition  ){
								 
								 v += '<img alt="' + ( item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
										'" class="x-action-col-icon x-action-col-' + String(i) + ' ' + (item.iconCls || '') +
										' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope||this.scope||this, arguments) : '') + '"' +
										((item.tooltip) ? ' ext:qtip="' + item.tooltip + '"' : '') + ' align="middle" />'; 
								 v += Ext.isEmpty(item.text)?'':'&nbsp;<a class="x-action-col-icon x-action-col-' + String(i) + '" style="color:#0000FF;text-decoration:underline;" >'+item.text+'</a>';
								 //v += Ext.isEmpty(item.text)?"":"&nbsp;"+item.text;
							 }
							 
						}
						
						return v;
				  };
			 }
		});
		Ext.apply(Ext.grid.Column.types, { customizedactioncolumn: Ext.grid.CustomizedActionColumn }  );

//-------------------------------------------procesarFecha-------------------------------------------------
function procesarFecha(opts, success, response) {
  if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
    var info = Ext.util.JSON.decode(response.responseText);
    Ext.getCmp('fechaRegistro').setValue(info.fechaHoy);
    Ext.getCmp('fechaFinRegistro').setValue(info.fechaHoy);
  }else{
		NE.util.mostrarConnError(response,opts);		
  }
}  
//-----------------------------------------Fin procesarFecha------------------------------------------------

//-----------------------------------------descargaArchivo--------------------------------------------------
function descargaArchivo(opts, success, response) {	
if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
//			Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');			
			var archivo = infoR.urlArchivo;
			var tipoArchivo = infoR.tipoarchivo;
      //archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			fp.getForm().getEl().dom.action =  archivo;
			fp.getForm().getEl().dom.submit();
			//Ext.getCmp('btnGenerarCSV').enable();
			if(tipoArchivo=='GeneraArchivoCSV'){
				var boton=Ext.getCmp('btnGenerarCSV');
				boton.setIconClass('icoXls');
				boton.enable();
			}
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//-----------------------------------------fin descargaArchivo----------------------------------------------

//---------------------------procesarArchivos------------------------------------
	function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			
			var archivo = infoR.urlArchivo;	
			var tipoArchivo = infoR.tipoarchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			if(tipoArchivo=='GeneraArchivoPDF'){
				var boton=Ext.getCmp('btnPdf');
				boton.setIconClass('icoPdf');
				boton.enable();
			}
			
			
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------fin procesarArchivos--------------------------------

//--------------------------------------------cargaFecha----------------------------------------------------
Ext.Ajax.request({
			url: '15cargaproveedoresiniExt01.data.jsp',
			params: {
				informacion: 'cargaFecha'
			},
			callback:procesarFecha 
});
//-------------------------------------------Fin CargaFecha--------------------------------------------------

//-------------------------------------------procesarConsultaData--------------------------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
	var 	fp = Ext.getCmp('formcon');
			fp.el.unmask();				
	var 	gridConsulta = Ext.getCmp('gridConsulta');	
	var 	el = gridConsulta.getGridEl();	
	if (arrRegistros != null) {
		if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
		if(store.getTotalCount() > 0) {					
				el.unmask();
				Ext.getCmp('btnGenerarCSV').enable();	
				Ext.getCmp('btnPdf').enable();
			} else {		
				Ext.getCmp('btnGenerarCSV').disable();	
				Ext.getCmp('btnPdf').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//-------------------------------------------Fin procesarConsultaData---------------------------------------
	
//---------------------------------------------ConsultaData-------------------------------------------------
var consultaData = new Ext.data.JsonStore({
		root 			: 'registros',
		url 			: '15cargaproveedoresiniExt01.data.jsp',
		baseParams	: {
							informacion: 'consultaGeneral'
							},
		hidden		: true,
		fields		: [	
							{	name: 'IC_BIT_CARGA'},	
							{	name: 'IC_EPO'},	
							{	name: 'CG_NOMBRE_ARCH'},	
							{	name: 'IG_TOTAL_REG'},	
							{	name: 'IG_REG_PROC'},	
							{	name: 'IG_REG_ERROR'},	
							{	name: 'DF_CARGA'},	
							{	name: 'IC_USUARIO'},	
							{	name: 'CG_NOMBRE_USUARIO'},	
							{	name: 'NOMBRE_EPO'}
							],		
		totalProperty 	: 'total',
		messageProperty: 'msg',
		autoLoad			: false,
		listeners		: {
			load			: procesarConsultaData,
				exception: {
						fn	: function(proxy, type, action, optionsRequest, response, args) {
								NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
								//LLama procesar consulta, para que desbloquee los componentes.
								procesarConsultaData(null, null, null);					
								}
								}
								}					
	})
//------------------------------------------Fin ConsultaData------------------------------------------------

//--------------------------------------------------Tran---------------------------------------------------
var Epo = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//-----------------------------------------------Fin Tran--------------------------------------------------

//------------------------------------------ procesarEpo-------------------------------------------------
	var procesarEpo = function(store, arrRegistros, opts) {
		store.insert(0,new Epo({ 
		clave: "", 
		descripcion: "Seleccione EPO", 
		loadMsg: ""})); 		
    Ext.getCmp('cmbepo').setValue("");
		store.commitChanges(); 
	}	
//------------------------------------------Fin procesarEpo------------------------------------------------


//--------------------------------------------Catalogo EPO--------------------------------------------------
 var catalogoEpo = new Ext.data.JsonStore({
		id					: 'catalogoEpo',
		root 				: 'registros',
		fields 			: ['clave', 'descripcion', 'loadMsg'],
		url 				: '15cargaproveedoresiniExt01.data.jsp',
		baseParams		: {
								informacion		: 'catalogoEpo'  
								},
		totalProperty 	: 'total',
		autoLoad			: true,
		listeners		: {			
              //  load :  procesarEpo,
								beforeload: NE.util.initMensajeCargaCombo,
								exception: NE.util.mostrarDataProxyError							
								}
	})
//------------------------------------------Fin Catalogo EPO------------------------------------------------

//---------------------------------------------Elementos Forma----------------------------------------------
	var  elementosForma =  [
		{
		xtype				: 'combo',	
		hidden			: false,
		name				: 'cmb_epo',	
		hiddenName		: '_cmb_epo',	
		id					: 'cmbepo',	
		fieldLabel		: 'Nombre de la EPO', 
		mode				: 'local',
		editable			: true,	
		displayField	: 'descripcion',
		valueField 		: 'clave',
		emptyText		: 'Seleccione EPO',
		forceSelection : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,	
		store 			: catalogoEpo,
		tpl				: NE.util.templateMensajeCargaComboConDescripcionCompleta
		},{
		xtype				: 'compositefield',
		id					:'fr',
		combineErrors	: false,
		msgTarget		: 'side',
		items				:[{
							xtype			: 'datefield',
							fieldLabel	: 'Fecha de Carga de Archivo',
							name			: '_fechaReg',
							id				: 'fechaRegistro',
							hiddenName	: 'fecha_registro',
							allowBlank	: false,
							startDay		: 0,
							width			: 100,
							minValue		: '01/01/1901',
							msgTarget	: 'side',
							margins		: '0 20 0 0'
							},	{
							xtype			: 'displayfield',
							value			: 'al:',
							width			: 10
							},	{
							xtype			: 'datefield',
							name			: '_fechaFinReg',
							id				: 'fechaFinRegistro',
							hiddenName	: 'fecha_fin_reg',
							allowBlank	: false,
							startDay		: 0,
							width			: 100,
							minValue		: '01/01/1901',
							msgTarget	: 'side',
							margins		: '0 20 0 0'
							}]
		}]
//-------------------------------------------Fin Elementos Forma--------------------------------------------

//----------------------------------------------Grid Consulta-----------------------------------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store				:consultaData,
		id					:'gridConsulta',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'*Hacer click sobre el n�mero de registros para descargar el archivo.',	
		clicksToEdit	:1,
		hidden			:true,
		columns			:[{
							header		:'EPO',
							tooltip		:'EPO',
							dataIndex	:'NOMBRE_EPO',
							sortable		:true,
							width			:200,			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'Fecha de carga de Archivo',
							tooltip		:'Fecha de carga de Archivo',
							dataIndex	:'DF_CARGA',
							sortable		:true,
							width			:150,			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}				
							},{
							header		:'Nombre Archivo',
							tooltip		:'Nombre Archivo',
							dataIndex	:'CG_NOMBRE_ARCH',
							sortable		:true,
							width			:200,			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}
							},{
							header		:'Usuario responsable',
							tooltip		:'Usuario responsable',
							dataIndex	:'CG_NOMBRE_USUARIO',
							sortable		:true,
							width			:200,			
							resizable	:true,				
							align			:'center',
							renderer:function(value){
								return "<div align='left'>"+value+"</div>";
								}							
							},{
							header		:'Total de Registros',
							tooltip		:'Total de Registros',
							dataIndex	:'IG_TOTAL_REG', 
							sortable		: true,
							width			:150,			
							resizable	:true,				
							align			:'center',
							hidden		:true
							},{
							xtype			:'customizedactioncolumn',
							id				:'totalReg',
							header		:'Total de Registros',
							tooltip		:'Total de Registros',
							dataIndex	:'IG_TOTAL_REG', 
							sortable		: true,
							width			:150,			
							resizable	:true,				
							align			:'center',
							hidden		:false,
							items: [				{
								getClass: function(value, metadata, record) { 
									this.items[0].tooltip 		= 'Total de Registros';
									if(metadata.value>0)										
										this.items[0].text	 		= metadata.value;
									else	
										this.items[0].text	 		= '';
									this.items[0].iconPosition = 'left';
									return '';							
								},
								handler: function(grid, rowIndex, colIndex) {
											var store=grid.getStore();
											var reg=store.getAt(rowIndex);
											var bit= reg.get('IC_BIT_CARGA');
											fp.el.mask('Generando archivo...', 'x-mask-loading');
											Ext.Ajax.request({
												url: '15cargaproveedoresiniExt01.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
															informacion: 'GeneraArchivoTxt',
															tipoReg:'T',
															icBitac:bit
															}),
											success : function(response) {
													var info = Ext.util.JSON.decode(response.responseText);
													fp.el.unmask();
											},
											callback: procesarArchivos
											});
										}
									}]
							},{
							header		:'Registros Procesados',
							tooltip		:'Registros Procesados',
							dataIndex	:'IG_REG_PROC',
							sortable		:true,
							width			:150,			
							resizable	:true,				
							align			:'center',
							hidden		:true
							},{
							xtype			:'customizedactioncolumn',
							id				:'regProcesados',
							header		:'Registros Procesados',
							tooltip		:'Registros Procesados',
							dataIndex	:'IG_REG_PROC', 
							sortable		: true,
							width			:150,			
							resizable	:true,				
							align			:'center',
							hidden		:false,
							items: [				{
								getClass: function(value, metadata, record) { 
									this.items[0].tooltip 		= 'Registros Procesados';
									if(metadata.value>0)
										this.items[0].text	 		= metadata.value;
									else	
										this.items[0].text	 		= '';
									this.items[0].iconPosition = 'left';
									return '';							
								},
								handler: function(grid, rowIndex, colIndex) {	
									var store=grid.getStore();
											var reg=store.getAt(rowIndex);
											var bit= reg.get('IC_BIT_CARGA');
											fp.el.mask('Generando archivo...', 'x-mask-loading');
											Ext.Ajax.request({
												url: '15cargaproveedoresiniExt01.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
															informacion: 'GeneraArchivoTxt',
															tipoReg:'C',
															icBitac:bit
															}),
											success : function(response) {
													var info = Ext.util.JSON.decode(response.responseText);
													fp.el.unmask();
											},
											callback: procesarArchivos
											});
											}
									}]
							},{
							header		:'Registros Inv�lidos',
							tooltip		:'Registros Inv�lidos',
							dataIndex	:'IG_REG_ERROR',
							sortable		:true,
							width			:150,			
							resizable	:true,				
							align			:'center',
							hidden		:true
							},{
							xtype			:'customizedactioncolumn',
							id				:'regInvalidos',
							header		:'Registros Inv�lidos',
							tooltip		:'Registros Inv�lidos',
							dataIndex	:'IG_REG_ERROR', 
							sortable		: true,
							width			:150,			
							resizable	:true,				
							align			:'center',
							hidden		:false,
							items: [				{
								getClass: function(value, metadata, record) { 
									this.items[0].tooltip 		= 'Registros Inv�lidos';
									if(metadata.value>0)
										this.items[0].text	 		= metadata.value;
									else	
										this.items[0].text	 		= '';
									this.items[0].iconPosition = 'left';
									return '';							
								},
								handler: function(grid, rowIndex, colIndex) {
									var store=grid.getStore();
											var reg=store.getAt(rowIndex);
											var bit= reg.get('IC_BIT_CARGA');
											fp.el.mask('Generando archivo...', 'x-mask-loading');
											Ext.Ajax.request({
												url: '15cargaproveedoresiniExt01.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
															informacion: 'GeneraArchivoTxt',
															tipoReg:'E',
															icBitac:bit
															}),
											success : function(response) {
													var info = Ext.util.JSON.decode(response.responseText);
													fp.el.unmask();
											},
											callback: procesarArchivos
											});
											}
									}]
							}],	
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 350,
      width				: 800,
		align				: 'center',
		frame				: false,
		enableColumnHide: false,
		bbar				: {
							store			: consultaData,
							emptyMsg		: "No hay registros.",
							items			: [
											'->','-',
											{
											xtype			: 'button',
											text			: 'Imprimir',					
											tooltip		: 'Imprimir',
											iconCls		: 'icoPdf',
											id				: 'btnPdf',
											handler		: function(boton, evento) {
																fp.el.mask('Generando archivo...', 'x-mask-loading');
																Ext.getCmp('btnPdf').disable();
																boton.setIconClass('loading-indicator');
																Ext.Ajax.request({
																	url: '15cargaproveedoresiniExt01.data.jsp',
																	params: Ext.apply(fp.getForm().getValues(),{
																				informacion: 'GeneraArchivoPDF'			
																				}),
																callback: procesarArchivos
																})
															}
											},{
											xtype			: 'button',
											text			: 'Generar Archivo',					
											tooltip		:	'Generar Archivo ',
											iconCls		: 'icoXls',
											id				: 'btnGenerarCSV',
											handler		: function(boton, evento) {
																fp.el.mask('Generando archivo...', 'x-mask-loading');
																boton.setIconClass('loading-indicator');
																Ext.getCmp('btnGenerarCSV').disable();
																Ext.Ajax.request({
																	url: '15cargaproveedoresiniExt01.data.jsp',
																	params: Ext.apply(fp.getForm().getValues(),{
																				informacion: 'GeneraArchivoCSV'			
																				}),
																	callback: descargaArchivo
																})
															}
											}]
								}
	})
//-------------------------------------------Fin Grid Consulta----------------------------------------------
		
//---------------------------------------------Panel Consulta-----------------------------------------------
	var fp = new Ext.form.FormPanel({
		id					:'formcon',
		width				: 600,
		heigth			:'auto',
		title				:'Bit�cora de Carga Masiva de Afiliados.',
		layout			:'form',
		frame				:true,
		collapsible		:true,
		titleCollapse	:false,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 5px',
		labelWidth		:150,
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosForma
							],
		buttons			:[{
							text				:'Consultar',
							id					:'btnConsultar',
							iconCls			:'icoBuscar',
							//formBind			:true,
							handler			:function(boton, evento){	
                
							var fechaMinReg = Ext.getCmp('fechaRegistro');
							var fechaMaxReg = Ext.getCmp('fechaFinRegistro');
							if (!Ext.isEmpty(fechaMinReg.getValue()) | !Ext.isEmpty(fechaMaxReg.getValue()) ) {
									if(Ext.isEmpty(fechaMinReg.getValue()))	{
										fechaMinReg.markInvalid('Debe capturar ambas fechas de Registro o dejarlas en blanco.');
										fechaMinReg.focus();
										return;
									}else if (Ext.isEmpty(fechaMaxReg.getValue())){
										fechaMaxReg.markInvalid('Debe capturar ambas fechas de Registro o dejarlas en blanco.');
										fechaMaxReg.focus();
										return;
									}
								}
								
							if(Ext.getCmp('fechaRegistro').getValue() > Ext.getCmp('fechaFinRegistro').getValue()){	
								Ext.getCmp('fechaRegistro').markInvalid('La fecha inicial debe ser menor que la final.');
								return;
							} 
							/*if( Ext.getCmp('fechaFinRegistro').getValue()  > Ext.getCmp('fechaRegistro').getValue() +7 ){	
								Ext.getCmp('fechaRegistro').markInvalid('El periodo m�ximo de consulta es de 7 D�as.');
								return;
							}*/
							var fecI = fechaMinReg.value;
							var resI = fecI.split("/"); 
							var aI = parseInt(resI[2]);
							var mI = parseInt(resI[1]);
							var dI = parseInt(resI[0]);
							var fecF = fechaMaxReg.value;
							var resF = fecF.split("/"); 
							var aF = parseInt(resF[2]);
							var mF = parseInt(resF[1]);
							var dF = parseInt(resF[0]);
							if(aI == aF  ){
							} else{
								Ext.getCmp('fechaRegistro').markInvalid('El periodo m�ximo de consulta es de 7 D�as.');
								return;
							}
							if(mI==mF){
							} else{
								Ext.getCmp('fechaRegistro').markInvalid('El periodo m�ximo de consulta es de 7 D�as.');
								return;
							}
							if(dI <= dF){
								var dias=dF-dI;
								if(dias<8){
									Ext.getCmp('fechaRegistro').clearInvalid();
									Ext.getCmp('fechaFinRegistro').clearInvalid();
								} else {
									Ext.getCmp('fechaRegistro').markInvalid('El periodo m�ximo de consulta es de 7 D�as.');
									return;
								}
							} else{
								Ext.getCmp('fechaRegistro').markInvalid('La fecha inicial debe ser menor que la final.');
								return;
							}
							
													/*
							if(Ext.getCmp('fechaRegistro').getValue() == Ext.getCmp('fechaFinRegistro').getValue() + 7){	
								Ext.getCmp('fechaRegistro').clearInvalid();
								Ext.getCmp('fechaFinRegistro').clearInvalid();
							} */
							if(Ext.getCmp('fechaRegistro').isValid() & Ext.getCmp('fechaFinRegistro').isValid()){	
								Ext.getCmp('fechaRegistro').clearInvalid();
								Ext.getCmp('fechaFinRegistro').clearInvalid();
							} else {return;}
							fp.el.mask('Cargando...', 'x-mask-loading');	
							consultaData.load({
							params		:Ext.apply(fp.getForm().getValues(),{
							informacion	:'consultaGeneral'
											})
									}); 	
								}//fin handler 
							},{
							text				:'Limpiar',
							id					:'btnLimpiar',
							iconCls			:'icoLimpiar',
							//formBind			:false,
							handler			:function(boton, evento){
												Ext.getCmp('formcon').getForm().reset();
												Ext.getCmp('gridConsulta').hide();
												Ext.Ajax.request({
															url: '15cargaproveedoresiniExt01.data.jsp',
															params: {
																informacion: 'cargaFecha'
															},
															callback:procesarFecha 
												});
										}
							}]
	})
//--------------------------------------------Fin Panel Consulta--------------------------------------------

//------------------------------------------Contenedor Principal--------------------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		: 900,
		height	: 600,
		style		:'margin:0 auto;',
		items		:[
					NE.util.getEspaciador(20),
					fp,
					NE.util.getEspaciador(20),
					gridConsulta,
					NE.util.getEspaciador(20)
					]
	})
//-------------------------------------------Fin Contenedor Principal---------------------------------------

});//-------------------------------------------------------------Fin Ext.onReady(function(){}--------------