Ext.onReady(function() {
	
	function procesaConsulta(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			consultaData.loadData('');
			fp.getForm().reset();
			if (infoR.registros != undefined && !Ext.isEmpty(infoR.registros)){
				consultaData.loadData(infoR.registros);
				if (!grid.isVisible()){
					grid.show();
				}
				Ext.getCmp('btnGuardar').enable();
			}else{
				if (grid.isVisible()){
					grid.hide();
				}
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGuardar = function (opts, success, response) {
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('btnGuardar').setIconClass('');
			if (grid.isVisible()) {
				grid.hide();
			}
			Ext.Msg.alert('Guardar','Guardado con �xito');
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		if (arrRegistros != null) {
			if(store.getTotalCount() > 0) {
				if (!grid.isVisible()) {
					grid.show();
				}
				var el = grid.getGridEl();
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnGenerarPDF').enable();
				el.unmask();
			}else {
				Ext.getCmp('btnGenerarArchivo').disable();
				Ext.getCmp('btnGenerarPDF').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name: 'ELECT'},
			{name: 'RAZONSOCIAL'},
			{name: 'FEC_AFILIA'},
			{name: 'RFC'},
			{name: 'CLIENTE'},
			{name: 'EPO'},
			{name: 'FECHA_ALTA'},
			{name: 'FLAG_CHECK'},
			{name: 'PAGINA'}
		],
		data:[{'ELECT':'','RAZONSOCIAL':'','FEC_AFILIA':'','RFC':'','CLIENTE':'','EPO':'','FECHA_ALTA':'','FLAG_CHECK':'','PAGINA':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header : 'N�mero Electr�nico', tooltip: 'N�mero Electr�nico', dataIndex : 'ELECT',
				sortable: true,	align: 'center',	width: 130,	resizable: true, hidden: false, hideable:false
			},{
				header : 'Fecha Afiliaci�n', tooltip: 'Fecha Afiliaci�n', dataIndex : 'FEC_AFILIA',
				sortable: true,	align: 'center',	width: 120,	resizable: true, hidden: false, hideable:false
			},{
				header : 'Fecha Alta N�m. Proveedor', tooltip: 'Fecha Alta N�m. Proveedor', dataIndex : 'FECHA_ALTA',
				sortable: true,	align: 'center',	width: 120,	resizable: true, hidden: false, hideable:false
			},{
				header : 'R.F.C.', tooltip: 'R.F.C.', dataIndex : 'RFC',
				sortable: true,	align: 'center',	width: 130,	resizable: true, hidden: false, hideable:false
			},{				
				header : 'Nombre', tooltip: 'Nombre', dataIndex : 'RAZONSOCIAL',
				sortable: true,	align: 'center',	width: 310,	resizable: true, hidden: false, hideable:false
			},{
				xtype: 'checkcolumn',
				header: 'Restablecer Pyme', tooltip: 'Restablecer Pyme', dataIndex: 'FLAG_CHECK',
				sortable : false,	width : 110,	align: 'center', hideable: false
			}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		colunmWidth: true,
		frame: true,
		clicksToEdit: 1,
		listeners: {
			afteredit : function(e){
				var inx = e.row;
				var check = Ext.getCmp('chk'+inx.toString());
				if (check){
					chek.checked = true;
				}
				var record = e.record;
				record.commit();		
			}
		},
		bbar: {
			items: [
				'->','-',{
					xtype: 'button',
					text: 'Guardar',
					id: 'btnGuardar',
					handler: function(boton, evento) {
							var flag = false;
							var jsonData = consultaData.data.items;
							var lista = [];
							Ext.each(jsonData, function(item,index,arrItem){
								if (item.data.FLAG_CHECK){
										flag = true;
										lista.push([item.data.CLIENTE, item.data.EPO] );
								}
							});
							if (flag){
								boton.disable();
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '15bitPymeSinNumProv.data.jsp',
									params: Ext.apply({
										informacion: 'Guarda',
										lista:	Ext.encode(lista)
									}),
									callback: procesarGuardar
								});
							}else{
								Ext.getCmp('btnGuardar').setIconClass('');
								if (grid.isVisible()) {
									grid.hide();
								}
							}
					}
				}
			]
		}
	});
	var elementosForma = [
		{
			xtype:'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype:		'textfield',
					id:			'txtRFC',
					name:			'txtRFC',
					maxLength:	20,
					margins: '0 20 0 0',
					width: 150,
					fieldLabel:	'R.F.C'
				},{
					xtype:		'label',
					id:			'lblMensaje',
					name:			'lblMensaje',
					text:	'Utilice el * para busqueda gen�rica'
				}
			]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		title:	'Pymes Sin N�m. Proveedor Eliminadas',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		labelWidth: 50,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					fp.el.mask('Enviando...','x-mask-loading');
					Ext.Ajax.request({
						url: '15bitPymeSinNumProv.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{informacion: 'Consulta'}),
						callback: procesaConsulta
					});
				} //fin-handler
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});

});