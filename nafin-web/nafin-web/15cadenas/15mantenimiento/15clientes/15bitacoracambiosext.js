Ext.onReady(function() {

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -

	function procesarSuccessFailureNombreNae(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var nom_Nae = infoR.nom_Nae;
			Ext.getCmp('_txt_nombre').setValue(nom_Nae);
		}else {
			Ext.Msg.alert('Aviso','El No. Nafin Electr�nico no corresponde a una PyME afiliada o no existe.');
			//NE.util.mostrarConnError(response,opts);
		}	
	}

	//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	function procesarSuccessFailureGenerarPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('btnGenerarPDF').enable();
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			boton = Ext.getCmp('btnGenerarPDF');
			boton.setIconClass('icoPdf');
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarSuccessFailureArchivoCSV(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			boton = Ext.getCmp('btnArchivoCSV');
			boton.setIconClass('icoXls');
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}

	
	var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}

	var procesarConsultaRegistros = function(store, registros, opts){

		var forma = Ext.getCmp('forma');
		forma.el.unmask();

		if (registros != null) {

			var grid  = Ext.getCmp('gridConsulta');

			if (!grid.isVisible()) {
				grid.show();
			}

			//Ext.getCmp('btnBajarPDF').hide();
			//Ext.getCmp('btnBajarCSV').hide();
      var cPantalla = Ext.getCmp('_cmb_pantalla')
      var gridColMod = grid.getColumnModel();
      
      if(cPantalla.getValue()=='CAPT_PROV_BAJA'){
        gridColMod.setHidden(gridColMod.findColumnIndex('IC_CAMBIO'),false);
        gridColMod.setHidden(gridColMod.findColumnIndex('IC_ALGO'),false);
      }else{
        gridColMod.setHidden(gridColMod.findColumnIndex('IC_CAMBIO'),true);
        gridColMod.setHidden(gridColMod.findColumnIndex('IC_ALGO'),true);
      }
			
			//gridColMod.setHidden(gridColMod.findColumnIndex('TIPO_CAMBIO'),true);
      
			Ext.getCmp('barraPaginacion').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
				el.unmask();
			} else {
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}
	}
  
  var muestraVerPdf = function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		Ext.Ajax.request({
      url: '15bitacoracambiosext.data.jsp',
      params:	Ext.apply(fp.getForm().getValues(),
            {
              informacion:'ArchivoPDFBajaProv',
              ic_cambio: registro.data['IC_CAMBIO']
            }
      ),
      callback: procesarSuccessFailurePDFBajaProv
    });
	}
  
  var procesarSuccessFailurePDFBajaProv =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo+'.pdf';
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
  
  var muestraVerZip = function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		Ext.Ajax.request({
      url: '15bitacoracambiosext.data.jsp',
      params:	Ext.apply(fp.getForm().getValues(),
            {
              informacion:'ArchivoZIPBajaProv',
              ic_cambio: registro.data['IC_CAMBIO']
            }
      ),
      callback: procesarSuccessFailureZIPBajaProv
    });
	}
  
  var procesarSuccessFailureZIPBajaProv =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo+'.zip';
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - -
	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [['E','Cadena Productiva']
				,['EI','Cadena Productiva Internacional']
				,['P','Proveedor']
				,['PI','Proveedor Internacional']
				,['D','Distribuidor']
				,['C','Credito Electronico']
				,['DF','Distribuidor de Fondos']
				,['CON','Contragarante']
				,['I','Intermediario Financiero']
				,['N','Nafin']
				,['GA','Grupos de Afiliacion']
				]
	});
	/*var storeAfiliacion = new Ext.data.SimpleStore({
		data: datos,
		fields:['clave','afiliacion']
	});
	
	var datos =[
		{'E','Cadena Productiva'},
		{'EI','Cadena Productiva Internacional'},
		{'P','Proveedor'},
		{'PI','Proveedor Internacional'},
		{'D','Distribuidor'},
		{'C','Credito Electronico'},
		{'DF','Distribuidor de Fondos'},
		{'CON','Contragarante'},
		{'I','Intermediario Financiero'},
		{'N','Nafin'},
		{'GA','Grupos de Afiliacion'}
	];
	
	var datos2 =[
		{'clave':'E', 'afiliacion':'Cadena Productiva'},
		{'clave':'P','afiliacion':'Proveedor'},
		{'clave':'I','afiliacion':'Intermediario Financiero'}
	];*/


	//Catalogo para el combo de seleccion de Banco de Fondeo
	var catalogoBancoFondeoData = new Ext.data.JsonStore({
		id:				'catalogoBancoFondeoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15bitacoracambiosext.data.jsp',
		baseParams:		{	informacion: 'catalogoBancoFondeo'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	//Catalogo para el combo de seleccion de Grupo
	var catalogoGrupoData = new Ext.data.JsonStore({
		id: 'catalogoGrupoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15bitacoracambiosext.data.jsp',
		baseParams: {
			informacion: 'CatalogoGrupo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	//Catalogo para el combo de seleccion de Pantalla de bitacora
	var catalogoPantallaData = new Ext.data.JsonStore({
		id:				'catalogoPantallaDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15bitacoracambiosext.data.jsp',
		baseParams:		{	informacion: 'catalogoPantalla'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15bitacoracambiosext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'15bitacoracambiosext.data.jsp',
			baseParams: {	informacion:	'ConsultaRegistros'	},
			fields: [
				{ name: 'IC_CAMBIO' },
        { name: 'IC_NAFIN_ELECTRONICO' },
				{ name: 'PANTALLA'},
				{ name: 'CG_RAZON_SOCIAL'},
				{ name: 'GRUPO'},
				{ name: 'IC_USUARIO'},
				{ name: 'CG_NOMBRE_USUARIO'},
				{ name: 'FECHA_CAMBIO'},
				{ name: 'CG_ANTERIOR'},
				{ name: 'CG_ACTUAL'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
					
					}
				},
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null, null);
					}
				}
			}
	});

//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - -

	var procesaConsulta = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){

		 if(  estadoSiguiente == "CONSULTAR" 											){
			bandera = true;
			var fechaMin = Ext.getCmp('_fecha_carga_ini');
			var fechaMax = Ext.getCmp('_fecha_carga_fin');
			if(fechaMin.getValue()!=""){
				var fechaDe = Ext.util.Format.date(fechaMin.getValue(),'d/m/Y');
				var fechaA = Ext.util.Format.date(fechaMax.getValue(),'d/m/Y');
				if(!isdate(fechaDe)){
					fechaMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}else if(!isdate(fechaA)){
					fechaMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}
			}
				
			if(bandera){
				var forma = Ext.getCmp("forma");
				forma.el.mask('Buscando...','x-mask-loading');
	
				// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
				Ext.getCmp("gridConsulta").hide();
				Ext.StoreMgr.key('registrosConsultadosDataStore').load({
					params:	Ext.apply(fp.getForm().getValues(),
								{
									informacion: 'Consultar',
									operacion: 'Generar',
									start: 0,
									limit: 15
								}
					)
				});
			}
		} else if(	estadoSiguiente == "LIMPIAR"){

			Ext.getCmp('forma').getForm().reset();
			if(Ext.getCmp('_cmb_grupo').show()){
				Ext.getCmp('_cmb_grupo').hide();
			}
			if(!Ext.getCmp('_nafelec').show()){
				Ext.getCmp('_nafelec').show();
			}
			if(!Ext.getCmp('busqava').show()){
				Ext.getCmp('busqava').show();
			}
			grid.hide();

		} else if(	estadoSiguiente == "FIN"){

			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "15bitacoracambiosext.data.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

	var cambiosRenderer = function( value, metadata, record, rowIndex, colIndex, store){
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
		metadata.attr += '" ';
		return value;
	}
	
	// Grid con las cuentas registradas
	//Elementos del grid de la consulta
		var grid = new Ext.grid.GridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			title:		undefined,
			//view:			new Ext.grid.GridView({forceFit:	true}),
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			width: 		870,
			frame: 		false,
			columns: [
				//.toUpperCase();
				{
					header: 		'N�m. Nafin Electr�nico',
					tooltip: 	'N�m. Nafin Electr�nico',
					dataIndex: 	'IC_NAFIN_ELECTRONICO',
					sortable: 	true,
					align:		'center',
					resizable: 	true,
					width: 		100,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Pantalla',
					tooltip: 	'Pantalla a mostrar',
					dataIndex: 	'PANTALLA',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		115,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Raz�n Social',
					tooltip: 	'Razon Social',
					dataIndex: 	'CG_RAZON_SOCIAL',
					id:			'clmSoc',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		125,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Grupo',
					tooltip: 	'Grupo',
					id: 			'clmGrupo',
					dataIndex: 	'GRUPO',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		125,
					hidden: 		true,
					hideable:	false
				},{
					header: 		'Usuario-Modifico',
					tooltip: 	'Usuario que hizo modificacion',
					dataIndex: 	'IC_USUARIO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Nombre de Usuario',
					tooltip: 	'Nombre del usuario',
					dataIndex: 	'CG_NOMBRE_USUARIO',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Fecha de Actualizaci�n',
					tooltip: 	'Ultima fecha de actualizacion',
					dataIndex: 	'FECHA_CAMBIO',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					hidden: 		false,
					hideable:	false,
					renderer:function(value, metadata, record, rowindex, colindex, store) {
									metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
									return value;
								}
				},{
					header: 		'Datos Anteriores',
					tooltip: 	'Datos anteriores',
					dataIndex: 	'CG_ANTERIOR',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		300,
					hidden: 		false,
					hideable:	false,
					renderer:cambiosRenderer	
				},{
					header: 		'Datos Nuevos',
					tooltip: 	'Datos nuevos',
					dataIndex: 	'CG_ACTUAL',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		300,
					hidden: 		false,
					hideable:	false,
					renderer:cambiosRenderer					
				},{
				xtype:	'actioncolumn',
				header:	'Solicitud Baja EPO',	tooltip:	'Solicitud Baja EPO',	dataIndex:	'IC_CAMBIO',
				sortable:	true,	width:	105,	resizable:	true,	hideable:	false, align:	'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return ' ';
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (!Ext.isEmpty(value)){
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}else	{
								return value;
							}
						},
						handler:	muestraVerPdf
					}
				]
			},
      {
				xtype:	'actioncolumn',
				header:	'Archivo Original de Baja',	tooltip:	'Archivo Original de Baja',	dataIndex:	'IC_ALGO',
				sortable:	true,	width:	105,	resizable:	true,	hideable:	false, align:	'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return ' ';
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (!Ext.isEmpty(value)){
								this.items[0].tooltip = 'Ver';
								return 'icoZip';
							}else	{
								return value;
							}
						},
						handler:	muestraVerZip
					}
				]
			}
			],
			bbar: {
				xtype: 			'paging',
				pageSize: 		15,
				buttonAlign: 	'left',
				id: 				'barraPaginacion',
				displayInfo: 	true,
				store: 			registrosConsultadosData,
				displayMsg: 	'{0} - {1} de {2}',
				emptyMsg: 		"No hay registros.",
				items: [
					'->','-',
					{
						xtype:	'button',
						text:		'Imprimir',
						id: 		'btnGenerarPDF',
						iconCls:	'icoPdf',
						handler: function(boton, evento) {
							//boton.disable();
							boton.setIconClass('loading-indicator');
							var barraPaginacion = Ext.getCmp("barraPaginacion");
							Ext.Ajax.request({
								url: '15bitacoracambiosext.data.jsp',
								params:	Ext.apply(fp.getForm().getValues(),
											{
												informacion:'ArchivoXPDF',
												start: barraPaginacion.cursor,
												limit: barraPaginacion.pageSize
											}
								),
								callback: procesarSuccessFailureGenerarPDF
							});
						}
					},
					{
						xtype:	'button',
						iconCls: 'icoXls',
						text: 	'Generar Archivo',
						id: 		'btnArchivoCSV',
						disabled:true,
						handler: function(boton, evento) {
										boton.setIconClass('loading-indicator');
										//var barraPaginacion = Ext.getCmp("barraPaginacion");
										Ext.Ajax.request({
											url: '15bitacoracambiosext.data.jsp',
											params:	Ext.apply(fp.getForm().getValues(),
														{
															informacion:'ArchivoCSV'
														}
											),
											callback: procesarSuccessFailureArchivoCSV
										});
						}
					}
				]
			}
	});


	//Elementos que van dentro del formulario de datos para realizar la consulta
		var elementosFormaConsulta = [
			{
				xtype: 'combo',
				name: 'cmb_afiliado',
				id: '_cmb_afiliado',
				fieldLabel: 'Tipo de Afiliado',
				mode: 'local',
				hiddenName : 'claveAfiliado',
				emptyText: 'Seleccione Tipo de Afiliado',
				forceSelection : true,
				triggerAction : 'all',
				editable: true,
				typeAhead: true,
				minChars : 1,
				valueField		: 'clave',
				displayField	:'afiliacion',
				store: storeAfiliacion,
				anchor:	'95%',
				listeners:{
					select: function(combo){
						Ext.getCmp('_txt_ne').reset();
						Ext.getCmp('_txt_nombre').reset();
						Ext.getCmp('_txt_ne').enable();
						var cmbAfiliado = combo.getValue();
						if((cmbAfiliado.toString())=="N"){
							var cm=grid.getColumnModel();
							cm.setHidden(cm.findColumnIndex('CG_RAZON_SOCIAL'), false);
							cm.setHidden(cm.findColumnIndex('GRUPO'), true);
							Ext.getCmp('_cmb_grupo').hide();
							Ext.getCmp('_nafelec').hide();
							Ext.getCmp('busqava').hide();
						}
						else if((cmbAfiliado.toString())=="GA"){
							var cm=grid.getColumnModel();
							cm.setHidden(cm.findColumnIndex('CG_RAZON_SOCIAL'), true);
							cm.setHidden(cm.findColumnIndex('GRUPO'), false);
							Ext.getCmp('_cmb_grupo').show();
							Ext.getCmp('_nafelec').hide();
							Ext.getCmp('busqava').hide();
						}
						else{
							var cm=grid.getColumnModel();
							cm.setHidden(cm.findColumnIndex('CG_RAZON_SOCIAL'), false);
							cm.setHidden(cm.findColumnIndex('GRUPO'), true);
							Ext.getCmp('_cmb_grupo').hide();
							Ext.getCmp('_nafelec').show();
							Ext.getCmp('busqava').show();
						}
						Ext.getCmp('_cmb_pantalla').reset();
						catalogoPantallaData.load({
							params: {
								icPantalla: cmbAfiliado
							}
						});
					}
				}
			},{
				xtype: 'combo',
				id:				'_cmb_grupo',
				name:				'cmb_grupo',
				hiddenName:		'ic_grupo',
				fieldLabel: 'Grupo',
				emptyText: 'Seleccione Grupo',
				mode: 'local',
				hidden:	true,
				displayField: 'descripcion',
				valueField: 'clave',
				forceSelection : false,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				editable: true,
				store: catalogoGrupoData,
				tpl : NE.util.templateMensajeCargaCombo,
				anchor:	'95%'
			},{
				xtype: 'compositefield',
				id:'_nafelec',
				combineErrors: false,
				msgTarget: 'side',
				fieldLabel: 'No. Nafin Electr�nico',
				items: [
					{
						xtype:		'textfield',
						name:			'txt_ne',
						id:			'_txt_ne',
						disabled: true,
						hiddenName:		'txt_ne',
						maskRe:		/[0-9]/,
						allowBlank:	true,
						maxLength:	15,		//Lo sig. se agrego para lanzar la consulta del nombre del Num. N@E
						listeners:{
							'change':function(field){
									var cveAfiliado = Ext.getCmp('_cmb_afiliado').getValue();
									if ( !Ext.isEmpty(field.getValue()) && cveAfiliado!="") {
										var num_naf = field.getValue();
										Ext.Ajax.request({
														url: '15bitacoracambiosext.data.jsp',
														params: {
																informacion: 'CargaNom_Nae',
																cve_Afi: cveAfiliado,
																num_Nae: num_naf
														},
														callback: procesarSuccessFailureNombreNae
										});
									}
							}
						}
					},{
						xtype:			'textfield',
						name:				'txt_nombre',
						id:				'_txt_nombre',
						width:			260,
						disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
						disabled:		true,
						allowBlank:		true,
						maxLength:		100
					}
				]
			},{
				xtype: 'compositefield',
				combineErrors: false,
				id: 'busqava',
				msgTarget: 'side',
				items: [
					{
						xtype:'displayfield',
						id:'disEspacio',
						text:''
					},
					{
						xtype:	'button',
						id:		'btnBuscaA',
						iconCls:	'icoBuscar',
						text:		'B�squeda Avanzada',
						handler: function(boton, evento) {
									var cveAfiliado = Ext.getCmp('_cmb_afiliado');
									var num_naf = Ext.getCmp('_txt_ne').getValue();
									if(cveAfiliado.getValue()==""){
										cveAfiliado.markInvalid('Debe Seleccionar Un Tipo De Afiliado');
									}/*else if(num_naf !=""){ //Se agrega para realizar una busqueda antes de abrir la ventana
										Ext.Ajax.request({
														url: '15bitacoracambiosext.data.jsp',
														params: {
																informacion: 'CargaNom_Nae',
																cve_Afi: cveAfiliado.getValue(),
																num_Nae: num_naf
														},
														callback: procesarSuccessFailureNombreNae
										});
									}*/else /*if(num_naf =="")*/{
										Ext.getCmp('_txt_ne').reset();
										Ext.getCmp('_txt_nombre').reset();
										var winVen = Ext.getCmp('winBuscaA');
										if (winVen){
											Ext.getCmp('fpWinBusca').getForm().reset();
											Ext.getCmp('fpWinBuscaB').getForm().reset();

											Ext.getCmp('cmb_num_ne').setValue();
											Ext.getCmp('cmb_num_ne').store.removeAll();
											Ext.getCmp('cmb_num_ne').reset();
											winVen.show();
										}else{
											var winBuscaA = new Ext.Window ({
												id:'winBuscaA',
												height: 364,
												x: 300,
												y: 100,
												width: 550,
												modal: true,
												closeAction: 'hide',
												title: 'B�squeda Avanzada',
												items:[
													{
														xtype:'form',
														id:'fpWinBusca',
														frame: true,
														border: false,
														style: 'margin: 0 auto',
														bodyStyle:'padding:10px',
														defaults: {
															msgTarget: 'side',
															anchor: '-20'
														},
														labelWidth: 130,
														items:[
															{
																xtype:'displayfield',
																frame:true,
																border: true,
																value:'<b>Tipo de Afiliado: '+Ext.getCmp('_cmb_afiliado').lastSelectionText+'</b>'
															},
															{
																xtype:'displayfield',
																frame:true,
																border: false,
																value:'Utilice el * para b�squeda gen�rica'
															},{
																xtype:'displayfield',
																frame:true,
																border: false,
																value:''
															},{
																xtype:'hidden',
																id:	'hid_ic_pyme',
																value:''
															},{
																xtype:'hidden',
																id:	'hid_ic_banco',
																value:''
															},{
																xtype: 'textfield',
																name: 'nombre_pyme',
																id:	'txtNombre',
																fieldLabel:'Nombre',
																maxLength:	100
															},{
																xtype: 'textfield',
																name: 'rfc_prov',
																id:	'_rfc_prov',
																fieldLabel:'RFC',
																maxLength:	20
															},{
																xtype: 'textfield',
																name: 'num_pyme',
																id:	'txtNe',
																fieldLabel:'N�mero de Nafin Electr�nico',
																maxLength:	25
															}
														],
														buttonAlign:'center',
														buttons:[
															{
																text:'Buscar',
																iconCls:'icoBuscar',
																handler: function(boton) {
																			Ext.getCmp('hid_ic_pyme').setValue(Ext.getCmp('_cmb_afiliado').getValue());
																			//Ext.getCmp('hid_ic_banco').setValue(Ext.getCmp('_cmb_banco_fondeo').getValue());
																			catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues())
																														});
																		}

															},{
																text:'Cancelar',
																iconCls: 'icoRechazar',
																handler: function() {
																				Ext.getCmp('winBuscaA').hide();
																				Ext.getCmp('winBuscaA').destroy();
																			}
															}
														]
													},{
														xtype:'form',
														frame: true,
														id:'fpWinBuscaB',
														style: 'margin: 0 auto',
														bodyStyle:'padding:10px',
														monitorValid: true,
														defaults: {
															msgTarget: 'side',
															anchor: '-20'
														},
														labelWidth: 80,
														items:[
															{
																xtype: 'combo',
																id:	'cmb_num_ne',
																name: 'ic_pyme',
																hiddenName : 'ic_pyme',
																fieldLabel: 'Nombre',
																emptyText: 'Seleccione. . .',
																displayField: 'descripcion',
																valueField: 'clave',
																triggerAction : 'all',
																forceSelection:true,
																allowBlank: false,
																typeAhead: true,
																mode: 'local',
																minChars : 1,
																store: catalogoNombreProvData,
																tpl : NE.util.templateMensajeCargaCombo
															}
														],
														buttonAlign:'center',
														buttons:[
															{
																text:'Aceptar',
																iconCls:'aceptar',
																formBind:true,
																handler: function() {
																			if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																				var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																				var cveP = disp.substr(0,disp.indexOf(" "));
																				var desc = disp.slice(disp.indexOf(" ")+1);
																				Ext.getCmp('_txt_ne').setValue(cveP);
																				Ext.getCmp('_txt_nombre').setValue(desc);
																				Ext.getCmp('winBuscaA').hide();
																			}
																		}
															},{
																text:'Cancelar',
																iconCls: 'icoRechazar',
																handler: function() {	
																	Ext.getCmp('winBuscaA').hide();	
																	Ext.getCmp('winBuscaA').destroy();
																}
															}
														]
													}
												]
											}).show();
										}
										}
									}
					}
				]
			},{
				xtype: 'combo',
				name: 'cmb_pantalla',
				id:	'_cmb_pantalla',
				fieldLabel: 'Pantalla',
				editable: true,
				emptyText: 'Seleccione Pantalla',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				hiddenName : 'icPantalla',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store: catalogoPantallaData,
				tpl : NE.util.templateMensajeCargaCombo,
				anchor:	'95%'

			},{
				xtype:	'textfield',
				id:		'_txt_usuario',
				name:		'txt_usuario',
				hiddenName : 'txt_usuario',
				fieldLabel:	'Usuario',
				anchor:	'95%'
			},{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de Actualizaci�n',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'datefield',
						name: 'fecha_carga_ini',
						id: '_fecha_carga_ini',
						allowBlank: false,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						//vtype: 'rangofecha',
						//campoFinFecha: '_fecha_carga_fin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'a',
						width: 25
					},
					{
						xtype: 'datefield',
						name: 'fecha_carga_fin',
						id: '_fecha_carga_fin',
						allowBlank: false,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						//vtype: 'rangofecha',
						//campoInicioFecha: '_fecha_carga_ini',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			}
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		width:			610,
		style:			'margin:0 auto;',
		collapsible:	true,
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		150,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosFormaConsulta,
		monitorValid:	true,
		buttons: [
			{
				text:		'Consultar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:false,
				handler: function(boton, evento) {

								var fechaMin = Ext.getCmp('_fecha_carga_ini');
								var fechaMax = Ext.getCmp('_fecha_carga_fin');
								if (!Ext.isEmpty(fechaMin.getValue()) || !Ext.isEmpty(fechaMax.getValue()) ) {
									if(Ext.isEmpty(fechaMin.getValue()))	{
										fechaMin.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
										fechaMin.focus();
										return;
									}else if (Ext.isEmpty(fechaMax.getValue())){
										fechaMax.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
										fechaMax.focus();
										return;
									}
								}
								var cveAfiliado= Ext.getCmp('_cmb_afiliado');
								if(cveAfiliado.getValue()==""){
									cveAfiliado.markInvalid('Debe Seleccionar Un Tipo De Afiliado');
								}
								else if(fechaMax.getValue() < fechaMin.getValue()){
									fechaMax.markInvalid('La fecha final debe ser mayor o igual a la inicial');
								}
								else{
									accionConsulta("CONSULTAR",null)
								}
				} //fin handler
			},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR",null);
							}
			},{
				text:		'Cancelar',
				id:		'btnCancelar',
				iconCls:	'borrar',
				hidden:	true,
				handler:	function(){
								accionConsulta("FIN",null);
							}
			}
		]
	});

//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(5),
			grid
		],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Ocultar mascara del panel de resumen de carga de plantillas
			element = Ext.getCmp('gridConsulta').getGridEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Derefenrencia ultimo elemento...
			element = null;

		}

	});

	Ext.StoreMgr.key('catalogoBancoFondeoDataStore').load();
	Ext.StoreMgr.key('catalogoGrupoDataStore').load();

});
