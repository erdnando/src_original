<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		java.text.*,
		com.netro.model.catalogos.*,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.nafin.descuento.ws.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar ="";

if(informacion.equals("catalogoPais")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionNotIn("52", Integer.class);
		infoRegresar = cat.getJSONElementos();	
	}
 else if(informacion.equals("catalogoEstado")){
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setClavePais(Pais);//Mexico clave 24
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();	
 }else if(informacion.equals("catalogoEpo")){
	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setClave("ic_epo");
	catalogo.setDescripcion("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();
} else if(informacion.equals("catalogoIdentificacion"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_identificacion");
		cat.setCampoClave("ic_identificacion");
		cat.setCampoDescripcion("cd_nombre");
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoEdoCivil"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_estado_civil");
		cat.setCampoClave("ic_estado_civil");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setOrden("cd_descripcion");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoGrado"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_grado_esc");
		cat.setCampoClave("IC_GRADO_ESC");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setOrden("CD_NOMBRE");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoCategoria"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_tipo_categoria");
		cat.setCampoClave("IC_TIPO_CATEGORIA");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setOrden("CD_NOMBRE");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoSector"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_sector_econ");
		cat.setCampoClave("IC_SECTOR_ECON");
		cat.setCampoDescripcion("IC_SECTOR_ECON||' '||CD_NOMBRE");
		cat.setOrden("IC_SECTOR_ECON");
		cat.setValoresCondicionNotIn("1,0",Integer.class);
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoSubSector"))	{
		String sector = (request.getParameter("sector") != null)?request.getParameter("sector"):"";
		CatalogoSubSector cat = new CatalogoSubSector();
		cat.setTabla("comcat_subsector");
		cat.setCampoClave("IC_SUBSECTOR");
		cat.setSector(sector);
		cat.setCampoDescripcion("IC_SUBSECTOR || ' ' || CD_NOMBRE");
		cat.setOrden("IC_SUBSECTOR");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoRama"))	{
		String sector = (request.getParameter("sector") != null)?request.getParameter("sector"):"";
		String subsector = (request.getParameter("subsector") != null)?request.getParameter("subsector"):"";
		CatalogoRama cat = new CatalogoRama();
		cat.setTabla("comcat_rama");
		cat.setCampoClave("IC_RAMA");
		cat.setSector(sector);
		cat.setSubSector(subsector);
		cat.setCampoDescripcion("IC_RAMA || ' ' || CD_NOMBRE");
		cat.setOrden("IC_RAMA");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoClase"))	{
		String sector = (request.getParameter("sector") != null)?request.getParameter("sector"):"";
		String subsector = (request.getParameter("subsector") != null)?request.getParameter("subsector"):"";
		String rama = (request.getParameter("rama") != null)?request.getParameter("rama"):"";

		CatalogoClase cat = new CatalogoClase();
		cat.setTabla("comcat_clase");
		cat.setCampoClave("IC_CLASE");
		cat.setSector(sector);
		cat.setSubSector(subsector);
		cat.setRama(rama);
		cat.setCampoDescripcion("IC_CLASE || ' ' || CD_NOMBRE");
		cat.setOrden("IC_CLASE");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoEmpresa"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_tipo_empresa");
		cat.setCampoClave("ic_tipo_empresa");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setOrden("CD_NOMBRE");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoDomicilioCorres"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_dom_correspondencia");
		cat.setCampoClave("ic_dom_correspondencia");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setOrden("CD_NOMBRE");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoVersionConvenio"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_version_convenio");
		cat.setCampoClave("ic_version_convenio");
		cat.setCampoDescripcion("cd_version_convenio");
		cat.setOrden("cd_version_convenio");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoVersionConvenio"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_version_convenio");
		cat.setCampoClave("ic_version_convenio");
		cat.setCampoDescripcion("cd_version_convenio");
		cat.setOrden("cd_version_convenio");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoEstado"))	{
		String pais = (request.getParameter("pais") != null)?request.getParameter("pais"):"";

		CatalogoEstado cat = new CatalogoEstado();
		//cat.setTabla("comcat_version_convenio");
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre");
		cat.setClavePais(pais);
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoMunicipio"))	{
		String pais = (request.getParameter("pais") != null)?request.getParameter("pais"):"";
		String estado = (request.getParameter("estado") != null)?request.getParameter("estado"):"";
		CatalogoMunicipio cat = new CatalogoMunicipio();
		cat.setPais(pais);
		cat.setClave("IC_MUNICIPIO||'|'||CD_NOMBRE||'|'");
		cat.setDescripcion("upper(CD_NOMBRE)");
		cat.setEstado(estado);
		List elementos = cat.getListaElementos();
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar =  "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

		 
 } else if(informacion.equals("catalogoCiudad"))	{
		String pais = (request.getParameter("pais") != null)?request.getParameter("pais"):"";
		String estado = (request.getParameter("estado") != null)?request.getParameter("estado"):"";
		CatalogoCiudad cat = new CatalogoCiudad();
		//cat.setTabla("comcat_version_convenio");
		cat.setTabla("comcat_ciudad");
		cat.setCampoClave("codigo_ciudad");
		cat.setCampoDescripcion("nombre");
		cat.setPais(pais);
		cat.setEstado(estado);
		cat.setOrden("nombre");
		infoRegresar = cat.getJSONElementos();
 }else if (informacion.equals("consulta")){

	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	

	
	String ic_epo		= (request.getParameter("HicEpo")==null)?"0":request.getParameter("HicEpo");
	String ic_pyme		= (request.getParameter("numProv")==null)?"0":request.getParameter("numProv");
	String nafinEle		= (request.getParameter("numElectronico")==null)?"0":request.getParameter("numElectronico");
	ic_pyme = ic_pyme.equals("") ? "0":ic_pyme;
	ic_epo = ic_epo.equals("") ? "0":ic_epo;
	nafinEle = ic_pyme.equals("") ? "0":nafinEle;
	HashMap  mapaResultado = afiliacion.getConsultaAfiliacionDescuento(nafinEle,ic_epo,ic_pyme);
	HashMap  mapaInformacion ;
	boolean valida= (mapaResultado.get("VALIDA")).toString().equals("true")?true:false;
	JSONObject jsonObj = new JSONObject();
	if(valida){
		jsonObj.put("VALOR_EPO",(String)mapaResultado.get("VALOR_EPO"));
		jsonObj.put("VALOR_PYME",(String)mapaResultado.get("VALOR_PYME"));
		jsonObj.put("ES_PROVEEDOR_INTERNACIONAL",(String)mapaResultado.get("ES_PROVEEDOR_INTERNACIONAL"));
		String proveedorInt = (String)mapaResultado.get("ES_PROVEEDOR_INTERNACIONAL");
		if(proveedorInt.equals("S")){
			mapaInformacion = afiliacion.getDatosAfiliadoInternacional((String)mapaResultado.get("VALOR_EPO"),(String)mapaResultado.get("VALOR_PYME"),strLogin);
		}else{
			mapaInformacion = afiliacion.getDatosAfiliado((String)mapaResultado.get("VALOR_EPO"),(String)mapaResultado.get("VALOR_PYME"),strLogin);
		}
		jsonObj.put("registro",mapaInformacion);
	}else{
		jsonObj.put("MENSAJE",(String)mapaResultado.get("MENSAJE"));
	}
	
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("valida",new Boolean(valida));
	infoRegresar = jsonObj.toString();
} else if(informacion.equals("guardaAfiliado")){
	
	
	String iNoEPO2 = (request.getParameter("iNoEPO2") == null) ? "0" : request.getParameter("iNoEPO2");
	String iNoCliente2 = (request.getParameter("iNoCliente2") == null) ? "0" : request.getParameter("iNoCliente2");
	String txtIC_Domicilio = (request.getParameter("txtIC_Domicilio") == null) ? "" : request.getParameter("txtIC_Domicilio");
	String txtIC_Contacto = (request.getParameter("txtIC_Contacto") == null) ? "" : request.getParameter("txtIC_Contacto");
	String txtTipoPersona = (request.getParameter("txtTipoPersona") == null) ? "" : request.getParameter("txtTipoPersona");
	String Apellido_paterno = (request.getParameter("Apellido_paterno") == null) ? "" : request.getParameter("Apellido_paterno");
	String Apellido_materno = (request.getParameter("Apellido_materno") == null) ? "" : request.getParameter("Apellido_materno");
	String Nombre = (request.getParameter("Nombre") == null) ? "" : request.getParameter("Nombre");
	String NIT = (request.getParameter("NIT") == null) ? "" : request.getParameter("NIT");
	String R_F_C = (request.getParameter("R_F_C") == null) ? "" : request.getParameter("R_F_C");
	String Apellido_casada = (request.getParameter("Apellido_casada") == null) ? "" : request.getParameter("Apellido_casada");
	String Sexo = (request.getParameter("Sexo") == null) ? "" : request.getParameter("Sexo");
	String Estado_civil = (request.getParameter("Estado_civil") == null) ? "" : request.getParameter("Estado_civil");
	String Fecha_de_nacimiento = (request.getParameter("Fecha_de_nacimiento") == null) ? "" : request.getParameter("Fecha_de_nacimiento");
	String Pais_de_origen = (request.getParameter("Pais_de_origen") == null) ? "" : request.getParameter("Pais_de_origen");
	String Nombre_Comercial = (request.getParameter("Nombre_Comercial") == null) ? "" : request.getParameter("Nombre_Comercial");
	String Calle = (request.getParameter("Calle") == null) ? "" : request.getParameter("Calle");
	String Colonia = (request.getParameter("Colonia") == null) ? "" : request.getParameter("Colonia");
	String Delegacion_o_municipio = (request.getParameter("Delegacion_o_municipio") == null) ? "" : request.getParameter("Delegacion_o_municipio");
	String Estado = (request.getParameter("Estado") == null) ? "" : request.getParameter("Estado");
	String Codigo_postal = (request.getParameter("Codigo_postal") == null) ? "" : request.getParameter("Codigo_postal");
	String Pais = (request.getParameter("Pais") == null) ? "" : request.getParameter("Pais");
	String Telefono = (request.getParameter("Telefono") == null) ? "" : request.getParameter("Telefono");
	String Fax = (request.getParameter("Fax") == null) ? "" : request.getParameter("Fax");
	String Apellido_paterno_L = (request.getParameter("Apellido_paterno_L") == null) ? "" : request.getParameter("Apellido_paterno_L");
	String Apellido_materno_L = (request.getParameter("Apellido_materno_L") == null) ? "" : request.getParameter("Apellido_materno_L");
	String Nombre_L = (request.getParameter("Nombre_L") == null) ? "" : request.getParameter("Nombre_L");
	String Tel_Rep_Legal = (request.getParameter("Tel_Rep_Legal") == null) ? "" : request.getParameter("Tel_Rep_Legal");
	String Fax_Rep_Legal = (request.getParameter("Fax_Rep_Legal") == null) ? "" : request.getParameter("Fax_Rep_Legal");
	String Email_Rep_Legal = (request.getParameter("Email_Rep_Legal") == null) ? "" : request.getParameter("Email_Rep_Legal");
	String Apellido_paterno_C = (request.getParameter("Apellido_paterno_C") == null) ? "" : request.getParameter("Apellido_paterno_C");
	String Apellido_materno_C = (request.getParameter("Apellido_materno_C") == null) ? "" : request.getParameter("Apellido_materno_C");
	String Nombre_C = (request.getParameter("Nombre_C") == null) ? "" : request.getParameter("Nombre_C");
	String Telefono_C = (request.getParameter("Telefono_C") == null) ? "" : request.getParameter("Telefono_C");
	String Fax_C = (request.getParameter("Fax_C") == null) ? "" : request.getParameter("Fax_C");
	String Email_C = (request.getParameter("Email_C") == null) ? "" : request.getParameter("Email_C");
	String Identificacion = (request.getParameter("Identificacion") == null) ? "" : request.getParameter("Identificacion");
	String Grado_escolaridad = (request.getParameter("Grado_escolaridad") == null) ? "" : request.getParameter("Grado_escolaridad");
	String Tipo_categoria = (request.getParameter("Tipo_categoria") == null) ? "" : request.getParameter("Tipo_categoria");
//	String Tipo_Sector_Empresa = (request.getParameter("Tipo_Sector_Empresa") == null) ? "" : request.getParameter("Tipo_Sector_Empresa");
	String Ejecutivo_de_cuenta = (request.getParameter("Ejecutivo_de_cuenta") == null) ? "" : request.getParameter("Ejecutivo_de_cuenta");
	String No_de_escritura = (request.getParameter("No_de_escritura") == null) ? "" : request.getParameter("No_de_escritura");
	String No_de_notaria = (request.getParameter("No_de_notaria") == null) ? "" : request.getParameter("No_de_notaria");
	String Empleos_a_generar = (request.getParameter("Empleos_a_generar") == null) ? "" : request.getParameter("Empleos_a_generar");
	String Activo_total = (request.getParameter("Activo_total") == null) ? "" : request.getParameter("Activo_total");
	String Capital_contable = (request.getParameter("Capital_contable") == null) ? "" : request.getParameter("Capital_contable");
	String Ventas_netas_exportacion = (request.getParameter("Ventas_netas_exportacion") == null) ? "" : request.getParameter("Ventas_netas_exportacion");
	String Ventas_netas_totales = (request.getParameter("Ventas_netas_totales") == null) ? "" : request.getParameter("Ventas_netas_totales");
	String Fecha_de_Constitucion = (request.getParameter("Fecha_de_Constitucion") == null) ? "" : request.getParameter("Fecha_de_Constitucion");
	String Numero_de_empleados = (request.getParameter("Numero_de_empleados") == null) ? "" : request.getParameter("Numero_de_empleados");
	String Subsector = (request.getParameter("Subsector") == null) ? "" : request.getParameter("Subsector");
	String Rama = (request.getParameter("Rama") == null) ? "" : request.getParameter("Rama");
	String Principales_productos = (request.getParameter("Principales_productos") == null) ? "" : request.getParameter("Principales_productos");
	String Tipo_empresa = (request.getParameter("Tipo_empresa") == null) ? "" : request.getParameter("Tipo_empresa");
	String Domicilio_Correspondencia = (request.getParameter("Domicilio_Correspondencia") == null) ? "" : request.getParameter("Domicilio_Correspondencia");
	String Clase = (request.getParameter("Clase") == null) ? "" : request.getParameter("Clase");
	String Sector_economico = (request.getParameter("Sector_economico") == null) ? "" : request.getParameter("Sector_economico");
	String r_matrimonial = (request.getParameter("r_matrimonial") == null) ? "" : request.getParameter("r_matrimonial");
	String d_proveedores = (request.getParameter("d_proveedores") == null) ? "" : request.getParameter("d_proveedores");
	if(d_proveedores.equals("true")){d_proveedores="S";}else{d_proveedores="N";}
	String Rfc_Rep_Legal = (request.getParameter("Rfc_Rep_Legal") == null) ? "" : request.getParameter("Rfc_Rep_Legal");
	String fecha = (new SimpleDateFormat("yyyy-MM-dd")).format(new java.util.Date());
	String No_Identificacion = (request.getParameter("No_Identificacion") == null) ? "" : request.getParameter("No_Identificacion");
	String desc_automatico = (request.getParameter("desc_automatico") == null) ? "N" : request.getParameter("desc_automatico");
	
	String alta_troya = (request.getParameter("alta_troya") == null) ? "N" : request.getParameter("alta_troya");
	if(alta_troya.equals("true")){alta_troya="S";}else{alta_troya="N";}
	//String ic_oficina_tramitadora = (request.getParameter("ic_oficina_tramitadora") == null) ? "" : request.getParameter("ic_oficina_tramitadora");
	
	String esProveedorInternacional = (request.getParameter("esProveedorInternacional") == null) ? "N" : request.getParameter("esProveedorInternacional");
	String Version_Convenio = (request.getParameter("Version_Convenio") == null) ? "" : request.getParameter("Version_Convenio");
	String cuenta_usuario_asignada = (request.getParameter("cuenta_usuario_asignada") == null)?"":request.getParameter("cuenta_usuario_asignada");

  String convenio_unico = (request.getParameter("convenio_unico")==null)?"":request.getParameter("convenio_unico");//FODEA 020 - 2009
  if(convenio_unico.equals("on")){convenio_unico="S";}else{convenio_unico="N";}
  String fecha_convenio_unico = (request.getParameter("fecha_convenio_unico")==null)?"":request.getParameter("fecha_convenio_unico");//FODEA 020 - 2009

  String curp = (request.getParameter("curp")==null)?"":request.getParameter("curp");//Fodea 00-campos PLD 2010
  String fiel = (request.getParameter("fiel")==null)?"":request.getParameter("fiel");//Fodea 00-campos PLD 2010
  String ciudad = (request.getParameter("ciudad")==null)?"":request.getParameter("ciudad");//Fodea 00-campos PLD 2010
String csNotificaciones = (request.getParameter("autMensaje")==null)?"":request.getParameter("autMensaje");//Fodea 00-campos PLD 2010

	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	UtilUsr utils = new UtilUsr();
	Usuario usuario = utils.getUsuario(strLogin);
	Ejecutivo_de_cuenta = usuario.getClaveAfiliado();
	List afiliacion_d = null;
	if (esProveedorInternacional.equals("S")) { //Proveedor Internacional
  System.out.println("Proveedor Internacional");
		afiliacion_d = 
				afiliacion.afiliaDescuentoInternacional(iNoUsuario, iNoEPO2, iNoCliente2,
						txtIC_Domicilio, txtIC_Contacto, txtTipoPersona, NIT,
						Apellido_paterno, Apellido_materno, Nombre, R_F_C,
						Apellido_casada, Sexo, Estado_civil, r_matrimonial,
						Fecha_de_nacimiento, Pais_de_origen, Nombre_Comercial, Calle,
						Colonia, Delegacion_o_municipio,
						Estado, Codigo_postal, Pais, Telefono,
						Fax, Apellido_paterno_L, Apellido_materno_L, Nombre_L,
						Rfc_Rep_Legal, Tel_Rep_Legal, Fax_Rep_Legal, Email_Rep_Legal,
						Apellido_paterno_C, Apellido_materno_C,
						Nombre_C, Telefono_C, Fax_C, Email_C,
						Identificacion, No_Identificacion,
						Grado_escolaridad, Tipo_categoria,
						Ejecutivo_de_cuenta, No_de_escritura,
						No_de_notaria, Empleos_a_generar,
						Activo_total, Capital_contable,
						Ventas_netas_exportacion, Ventas_netas_totales,
						Fecha_de_Constitucion, Numero_de_empleados,
						Sector_economico, Subsector,
						Rama, Clase,
						Principales_productos, Tipo_empresa,
						Domicilio_Correspondencia, d_proveedores,
						fecha, desc_automatico,
						alta_troya, Version_Convenio,
            convenio_unico, fecha_convenio_unico);//FODEA 020 - 2009
	} else { //Proveedor Nacional
   System.out.println("Proveedor Nacional");
   System.out.println("ciudad  "+ciudad);
      
		afiliacion_d = afiliacion.afiliaDescuento(iNoUsuario, iNoEPO2, iNoCliente2, txtIC_Domicilio, txtIC_Contacto, txtTipoPersona, 
								Apellido_paterno, Apellido_materno, Nombre, R_F_C, Apellido_casada, 
								Sexo, Estado_civil, Fecha_de_nacimiento, Pais_de_origen, Nombre_Comercial, 
								Calle, Colonia, Delegacion_o_municipio, Estado, Codigo_postal, Pais,
								Telefono, Fax, Apellido_paterno_L, Apellido_materno_L, Nombre_L, 
								Tel_Rep_Legal, Fax_Rep_Legal, Email_Rep_Legal, Apellido_paterno_C, 
								Apellido_materno_C, Nombre_C, Telefono_C, Fax_C, Email_C, Identificacion, 
								Grado_escolaridad, Tipo_categoria, Ejecutivo_de_cuenta, No_de_escritura, 
								No_de_notaria, Empleos_a_generar, Activo_total, Capital_contable, 
								Ventas_netas_exportacion, Ventas_netas_totales, Fecha_de_Constitucion, 
								Numero_de_empleados, Subsector, Rama, Principales_productos, 
								Tipo_empresa, Domicilio_Correspondencia, Clase, Sector_economico, 
								r_matrimonial, d_proveedores, Rfc_Rep_Legal, fecha, No_Identificacion,
								desc_automatico, alta_troya,Version_Convenio,"", cuenta_usuario_asignada,
                convenio_unico, fecha_convenio_unico, fiel, curp, ciudad,csNotificaciones);//FODEA 020 - 2009
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean(true));
	if(afiliacion_d.get(1).equals("")){
		jsonObj.put("resultado",afiliacion_d.get(0)+"\n"+afiliacion_d.get(2)+"\n"+afiliacion_d.get(3));
		jsonObj.put("valida",new Boolean(true));
	}else{
		jsonObj.put("valida",new Boolean(false));
		jsonObj.put("resultado",afiliacion_d.get(1));
	}
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>