<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,	
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.*,
		com.netro.seguridad.*,
		com.netro.exception.*,
		com.netro.fianza.*,
		com.netro.model.catalogos.*,
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 

String numFiado = (request.getParameter("numFiado")!=null)?request.getParameter("numFiado"):""; 
String nombre = (request.getParameter("nombre")!=null)?request.getParameter("nombre"):""; 
String rfc = (request.getParameter("claveRFC")!=null)?request.getParameter("claveRFC"):""; 
String estado = (request.getParameter("estado")!=null)?request.getParameter("estado"):""; 
String terminosCodiciones = (request.getParameter("terminosCondiciones")!=null)?request.getParameter("terminosCondiciones"):""; 

String ic_mandante = (request.getParameter("ic_mandante")!=null)?request.getParameter("ic_mandante"):"";
String tipoAfiliado = (request.getParameter("tipoAfiliado")!=null)?request.getParameter("tipoAfiliado"):"";
String tituloF = (request.getParameter("tituloF")!=null)?request.getParameter("tituloF"):"";
String tituloC = (request.getParameter("tituloC")!=null)?request.getParameter("tituloC"):"";
String modificar = (request.getParameter("modificar")!=null)?request.getParameter("modificar"):""; 
String txtLogin = (request.getParameter("txtLogin")!=null)?request.getParameter("txtLogin"):""; 
String txtNombreMandante = (request.getParameter("txtNombreMandante")!=null)?request.getParameter("txtNombreMandante"):"";
String txtEPO = (request.getParameter("txtEPO")!=null)?request.getParameter("txtEPO"):"";
	
										
								
String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
List parametros   =  new ArrayList();	

JSONObject jsonObj = new JSONObject();


int  start= 0, limit =0;


//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion beanAfiliacion = afiliacionHome.create();
Afiliacion beanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	
	ConsFiados  paginador = new ConsFiados();
	CambioPerfil clase = new  CambioPerfil();
	
	if(terminosCodiciones.equals("on")){  terminosCodiciones  ="S"; }


 if(informacion.equals("catalogoPerfil") ){  

	HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  tipoAfiliado  );	
	List perfiles = (List)catPerfil.get("PERFILES");	
	Iterator it = perfiles.iterator();	
	HashMap datos = new HashMap();
	List registros  = new ArrayList();	
	while(it.hasNext()) {
		List campos = (List) it.next();
		String clave = (String) campos.get(0);
		datos = new HashMap();
		datos.put("clave", clave );
		datos.put("descripcion", clave );
		registros.add(datos);
	}
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();


} else if (informacion.equals("Consultar") ||  informacion.equals("GeneraArchivoPDF")	||  informacion.equals("GeneraArchivoCSV")) {

	//estblecer los setters de paginador
	paginador.setNumFiado(numFiado);
	paginador.setNombre(nombre);
	paginador.setRfc(rfc);
	paginador.setEstado(estado);
	paginador.setTerminosCodiciones(terminosCodiciones);
		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if(informacion.equals("Consultar") ||  informacion.equals("GeneraArchivoPDF") ) {
		
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
					
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	
		if(informacion.equals("Consultar")) {
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);										
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
				
			jsonObj = JSONObject.fromObject(consulta);				
	
		}else  if ( informacion.equals("GeneraArchivoPDF") ) {
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
			
	}else  if ( informacion.equals("GeneraArchivoCSV") ) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}			
	}
	
	infoRegresar = jsonObj.toString();	

}else  if (informacion.equals("ConsCuentas") ||   informacion.equals("ImprimirConsCuentas")  ) {
	
	UtilUsr utilUsr = new UtilUsr();
	List cuentas = utilUsr.getUsuariosxAfiliado(numFiado, tipoAfiliado);
	Iterator itCuentas = cuentas.iterator();	
	
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	
	if (informacion.equals("ConsCuentas"))  {
		while (itCuentas.hasNext()) {
			String cuenta = (String) itCuentas.next();		
			datos = new HashMap();		
			datos.put("CUENTA", cuenta );	
			registros.add(datos);
		}	
			
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		
	}else  if( informacion.equals("ImprimirConsCuentas") )  {  
	
		String nombreArchivo = paginador.imprimirCuentas(request, cuentas, tituloF, tituloC,strDirectorioTemp );		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	}
	
	jsonObj.put("tituloC", tituloC);	
	jsonObj.put("tituloF", tituloF);	
	infoRegresar = jsonObj.toString();
	System.out.println("Infornacion "+informacion+"\n"+infoRegresar);

} else  if( informacion.equals("informacionUsuario") ){  
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true));
	
	HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  "S"  );	
	
	if(catPerfil.size()>0){
	
		List perfiles = (List)catPerfil.get("PERFILES");
	
		String lblNombreEmpresa = (String)catPerfil.get("EMPRESA");
		String lblNafinElec = (String)catPerfil.get("NAELECTRONICO");
		String lblNombre = (String)catPerfil.get("NOMBRE_USUARIO");
		String lblApellidoPaterno = (String)catPerfil.get("APELLIDO_PATERNO");
		String lblApellidoMaterno = (String)catPerfil.get("APELLIDO_MATERNO");
		String lblEmail = (String)catPerfil.get("EMAIL");
		String lblPerfilActual = (String)catPerfil.get("PERFIL_ACTUAL");
		String internacional = (String)catPerfil.get("INTERNACIONAL");
		String sTipoAfiliado = (String)catPerfil.get("TIPOAFILIADO");		
			
		jsonObj.put("lblNombreEmpresa", lblNombreEmpresa);
		jsonObj.put("lblNafinElec", lblNafinElec);
		jsonObj.put("lblNombre", lblNombre);
		jsonObj.put("lblApellidoPaterno", lblApellidoPaterno);
		jsonObj.put("lblApellidoMaterno", lblApellidoMaterno);
		jsonObj.put("lblEmail", lblEmail);
		jsonObj.put("lblPerfilActual", lblPerfilActual);
		jsonObj.put("txtLogin", txtLogin);
		jsonObj.put("mensaje", mensaje);	
		
		jsonObj.put("internacional", internacional);
		jsonObj.put("sTipoAfiliado", sTipoAfiliado);	
		jsonObj.put("txtNafinElectronico", lblNafinElec);	
		jsonObj.put("txtPerfilAnt", lblPerfilActual);	
		jsonObj.put("modificar", modificar);
				
	}else  {
		mensaje = "No se encontró el usuario con la clave especificada, por favor vuelva a intentarlo";
		jsonObj.put("mensaje", mensaje);
	}
	
	infoRegresar = jsonObj.toString();

} else  if( informacion.equals("ModifiarPerfil") ){ 

	String txtPerfil = (request.getParameter("txtPerfil")==null)?"":request.getParameter("txtPerfil");
	String txtNafinElectronico = (request.getParameter("txtNafinElectronico")==null)?"":request.getParameter("txtNafinElectronico");
	String txtPerfilAnt = (request.getParameter("txtPerfilAnt")==null)?"":request.getParameter("txtPerfilAnt");
	String internacional = (request.getParameter("internacional")==null)?"":request.getParameter("internacional");
	String txtTipoAfiliado = (request.getParameter("sTipoAfiliado")==null)?"":request.getParameter("sTipoAfiliado");
	String clave_usuario = (String)session.getAttribute("Clave_usuario");
	String txtLoginC = (request.getParameter("txtLoginC") != null) ? request.getParameter("txtLoginC") : "";
	
	mensaje =  clase.getConsultaUsuario( txtLoginC,  txtPerfil, txtPerfilAnt,  txtNafinElectronico ,  internacional,  txtTipoAfiliado, 	 clave_usuario ) ;

	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("mensaje", mensaje);
	infoRegresar = jsonObj.toString();	

}else  if (informacion.equals("Borrar") ) {

	//FianzaElectronicaHome fianzaElectronicaHome = (FianzaElectronicaHome)ServiceLocator.getInstance().getEJBHome("FianzaElectronicaEJB", FianzaElectronicaHome.class);
	//FianzaElectronica fianzaElectronicaBean = fianzaElectronicaHome.create();
	FianzaElectronica fianzaElectronicaBean = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB",FianzaElectronica.class);
	boolean borrado = false;
	try{
		 borrado = fianzaElectronicaBean.borrarFiado(numFiado, iNoUsuario);
		if(borrado){
			mensaje="Se ha borrado el Fiado exitosamente.";
		}else{
			mensaje="No se puede borrar el Fiado, tiene Fianzas asociadas.";
		}	
	}catch(Exception e) { 
		mensaje= e.toString();
		throw new AppException("Error en la afiliacion", e);
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("msg", mensaje);	
	jsonObj.put("borrado", new Boolean(borrado));	
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("InfoAfiliado") ){
	paginador.setNumFiado(numFiado);
	
	Registros reg = paginador.getDatosAfiliado();
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\",\"IC_FIADO\":\""+numFiado+"\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta );
	infoRegresar=jsonObj.toString();
	
	
}
%>
<%=infoRegresar%>

