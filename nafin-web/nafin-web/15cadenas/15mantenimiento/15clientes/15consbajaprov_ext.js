
Ext.onReady(function() {

//HANDLERS------------------------------------------------------------------------------
var procesarSuccessDatCboIf = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
      fp.getForm().getEl().dom.action =  resp.urlArchivo;
      fp.getForm().getEl().dom.submit();
			fp.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//FUNCTIONS-------------------------------------------------------------------------------
var descargaDeArchivo = function(boton, evento){
  fp.el.mask('Descargando Archivo...', 'x-mask-loading');
  Ext.Ajax.request({
    url: '15consbajaprov_ext.data.jsp',
    params: Ext.apply(fp.getForm().getValues(),{
      informacion: 'GenerarArchivo'
    }),
    callback: procesarSuccessDatCboIf
  }); 
}

//STORES----------------------------------------------------------------------------------
var storeCatEpoData = new Ext.data.JsonStore({
		id: 'catalogoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consbajaprov_ext.data.jsp',
		baseParams: {
			informacion: 'getCatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, option){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
          
					}
			}
		}
	});
  
var storeLayoutData = new Ext.data.ArrayStore({
	  fields: [
		  {name: 'NUMCAMPO'},
		  {name: 'DESC'},
		  {name: 'TIPODATO'}
	  ],
    data:[
            ['1','N�mero de Proveedor','Alfanum�rico'],
            ['2','Nombre del Proveedor','Alfanum�rico'],
            ['3','RFC del Proveedor','Alfanum�rico'],
            ['4','IC_PYME','Num�rico']
        ]
 });
//OBJETOS---------------------------------------------------------------------------------------
  var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
    style: 'margin:0 auto;',
		margins: '20 0 0 0',
		columns: [
			{ header : 'No. de Campo', dataIndex : 'NUMCAMPO', width : 100, sortable : true	},
			{ header : 'Descripci�n', dataIndex : 'DESC', width : 180, sortable : true	},
      { header : 'Tipo de Dato', dataIndex : 'TIPODATO', width : 145, sortable : true	}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 460,
		height: 170,
    hidden: true,
		//title: '<div align="left">Nota:<br>Se deber� cargar un archivo de texto(.txt) comprimido (.zip), el cual deber� contener un layout con los datos de los registros que se ajustar�n; los datos deben estar separados por pipe  "|"</div>',
		frame: true,
    bbar: {
			xtype: 'toolbar', items:[{
        xtype:'panel',
        id:'pnlMsgLayout',
        style: 'margin:0 auto;',
        width:450,
        html: '<b>Nota</b>: la consulta se realizar� por el IC_PYME los dem�s campos son solo informativos,<br>no tendran ninguna validaci�n.'
      }
    ]
    }
	});
	
	var elementosForma = [	
    {
      xtype: 'combo',
      name: 'cboEpo',
      id: 'cboEpo1',
      hidden: false,
      fieldLabel: 'Nombre de la EPO',
      mode: 'local', 
      displayField : 'descripcion',
      valueField : 'clave',
      hiddenName : 'cboEpo',
      emptyText: 'Seleccionar',
      width: 580,
      forceSelection : true,
      triggerAction : 'all',
      typeAhead: true,
      minChars : 1,
      store : storeCatEpoData,
      listeners:{
        select : function(cboIf, record, index ) {
          pnl.el.unmask();
          
        }
      },
      tpl : NE.util.templateMensajeCargaCombo
    },
    {
      xtype: 'panel',
      layout:'hbox',
      bodyStyle: 'padding: 6px',
      items:[
      {
          xtype:'button',
          iconCls: 'icoAyuda',  id: 'btnAyuda',
          handler: function(boton, evento) {	
            if (!gridLayout.isVisible()) {
              gridLayout.show();
            }else{
              gridLayout.hide();
            }
          }
        },
        {
          xtype:'box',
          width:346,
          html:'Lay-out de ayuda'
        },
        {
          xtype:'button',
          text: 'Generar Archivo', id: 'btnGenArchivo', iconCls: 'icoTxt', formBind: true,
          handler: descargaDeArchivo
        },			
        {
          xtype:'button',
          text: 'Cancelar', iconCls: 'icoLimpiar', handler: function() {
            window.location = '15consbajaprov_ext.jsp';
          }
        }
      ]
    },
    {
      xtype:'panel',
      html: 'Nota: La consulta solo mostrar� los Proveedores con estatus �S� en la Afiliaci�n y que no tengan documentos publicados en factoraje'
    }
  ];


//CONTENEDORES, GRIDS------------------------------------------------------------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Consulta Proveedores Baja',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true
	});
  
  
  var pnl = new Ext.Container({
    id: 'contenedorPrincipal',
    applyTo: 'areaContenido',
    style: 'margin:0 auto;',
    width: 949,
    items: [
      NE.util.getEspaciador(20),
      fp,
      NE.util.getEspaciador(20),
      gridLayout
    ]
  });
  
  storeCatEpoData.load();
	
});