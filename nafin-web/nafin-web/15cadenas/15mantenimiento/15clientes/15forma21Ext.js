Ext.onReady(function(){
var clave_NomDelegacion	= '';

/*--------------------------------- Handler's -------------------------------*/

	var procesarAltaContragarante =  function(opts, success, response) {
	
		var cmpForma = Ext.getCmp('formaAfiliacion');
		cmpForma.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			cmpForma.getForm().reset();
			var numero_Nafin = Ext.util.JSON.decode(response.responseText).lsNoNafinElectronico;
			if(numero_Nafin != null && numero_Nafin != ""){
				Ext.Msg.alert('Mensaje de p�gina web','Su n�mero de Nafin Electr�nico: ' + numero_Nafin,function() {
				window.location = '15forma_0Ext.jsp?idMenu=15FORMA0';
				},this);
			}
		}
		else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
		
	
	var procesarSuccessRFCexistente =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			rfc_invalido = 'N';
		}
		else {
			Ext.MessageBox.alert('Mensaje de p�gina web','El RFC ya existe, favor de verificaro' );
			rfc_invalido = 'S';
			return;
		}
	}


	var procesarCatalogoPais = function(store, arrRegistros, opts) {
		//if(store.getTotalCount() > 0) {
		//	Ext.getCmp('id_cmb_pais').setValue('24');
		//}
	}


/*---------------------------------- Store's --------------------------------*/

	function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		}else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} 	else if (pagina == 2) { 	// Distribuidor						
			window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 		 
		} else if (pagina == 3) { 	//Proveedor
			window.location = '15forma01Ext.jsp'; 
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} else if (pagina == 6) { 	//Contragarante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 	
		} else if (pagina == 9) { 	// Mandante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
		} else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		}	else if (pagina == 11) { 	//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 	
		}	else if (pagina == 12) { 	//Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliaClienteExternoExt.jsp"; 
		}
	}

	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [[/*'EPO'*/  '0','Cadena Productiva']
				,[/*'IF'*/	'1','Intermediario Financiero']
				,[/*'2'*/	'2','Distribuidor']
				,[/*'1'*/	'3','Proveedor']	
				,[/*'ACE'*/	'4','Afiliados Credito Electronico']
				,[/*'EPOI'*/'5','Cadena Productiva Internacional']
				,[/*'CONTRA'*/'6','Contragarante']			
				,[/*'M'*/	'9','Mandante']	
				,[/*'AF'*/	'10','Afianzadora']
				,[/*'UNI'*/ '11','Universidad-Programa Cr�dito Educativo']	
				,['12','Cliente Externo']
				]
	});
		

	var catalogoContragarante = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma21Ext.data.jsp',
		baseParams	: { informacion: 'catalogoContragarante'},
		autoLoad		: false,
		listeners	:
		{
			//load:procesarCatalogoPais,
			exception: NE.util.mostrarDataProxyError
		}
  });

	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoPais,
			exception: NE.util.mostrarDataProxyError
		}
  });
  
  // catalogo Estado
	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});


	//catalogo Municipio
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});

//Domicilio Correspondencia
	var catalogoDomicilio = new Ext.data.JsonStore({
	   id				: 'catDomicilio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoDomicilio'},
		autoLoad		: false,
		listeners	:
		{
			exception: NE.util.mostrarDataProxyError
		}
	});

	// catalogoOficina_controladora
	var catalogoOficina_controladora= new Ext.data.JsonStore
	({
		id				: 'catOficina_controladora',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoOficina_controladora'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});

/*-------------------------------- Componentes ------------------------------*/
	
	var afiliacion = {
		xtype		: 'panel',
		id 		: 'afiliacion',
		layout	: 'hbox',
		border	: false,		
		width		: 900,		
		bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:110px;',
		items		: 
		[{
			layout		: 'form',
			width			:330,
			labelWidth	: 60,
			border		: false,
			items			:[{
					xtype				: 'combo',
					id					: 'id_afiliacion',
					name				: 'afiliacion',
					hiddenName 		:'afiliacion', 
					fieldLabel		: 'Afiliaci�n',
					width				: 250,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',
					value				: '6',
					store				: storeAfiliacion,
					listeners: {
						select: {
							fn: function(combo) {
								var valor  = combo.getValue();
								creditoElec(valor);		
							}
						}
					}
					
				}]
			}
	]};


	var afiliacionPanel = {
		width:				900,
		xtype:				'panel',
		id:					'afiliacionPanel',
		items: [{
			labelWidth:		140,
			layout:			'form',
			items:[{
				width:		380,
				xtype:		'textfield',
				id:			'id_Razon_Social',
				name:			'Razon_Social',
				fieldLabel:	'* Raz�n Social',
				msgTarget:	'side',
				margins:		'0 20 0 0',
				maxLength:	60,
				allowBlank:	false
			},{
				width:		250,
				xtype:		'textfield',
				id:			'id_nombre_comercial',
				name:			'nombre_comercial',
				fieldLabel:	'* Nombre Comercial',
				msgTarget:	'side',
				maxLength:	40,
				margins:		'0 20 0 0',
				allowBlank:	false
			},{
				width:		150,
				xtype:		'textfield',
				id:			'rfc',
				name:			'rfc',
				fieldLabel:	'* R.F.C.',
				msgTarget:	'side',
				margins:		'0 20 0 0',
				maxLength:	14,
				allowBlank:	false,
				regex:		/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,							
				regexText:	'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
								'en el formato NNN-AAMMDD-XXX donde:<br>'+
								'NNN:son las iniciales del nombre de la empresa<br>'+
								'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
								'XXX:es la homoclave'
			},{ 	
				xtype:		'displayfield',  
				value:		'' 
			}]
		}]
	};

	var domicilioPanel_izq = {
		xtype:				'fieldset',
		id:					'domicilioPanel_izq',
		bodyStyle:			'padding: 6px; padding-right:0px;padding-left:2px;',
		labelWidth:			150,
		border:				false,
		items:	[{
			width:			250,
			xtype:			'textfield',
			id:				'calle',
			name:				'calle',
			fieldLabel:		'* Calle No. Exterior y No. Interior',
			msgTarget:		'side',
			margins:			'0 20 0 0',
			maxLength:		100,
			tabIndex:		4,
			allowBlank:		false
		},
                
                /*{
			width:			90,
			xtype:			'numberfield',
			id:				'code',
			name:				'code',
			fieldLabel:		'* C�digo Postal',
			margins:			'0 20 0 0',
			msgTarget:		'side',
			maxLength:		5,
			minLength:		5,
			tabIndex:		6,
			allowBlank:		false
		}*/
                {
			width:			90,
			xtype:			'textfield',
			id:				'code',
			name:				'code',
			fieldLabel:		'* C�digo Postal',
			margins:			'0 20 0 0',
			msgTarget:		'side',
			maxLength:		5,
			minLength:		5,
			tabIndex:		6,
			allowBlank:		false,
                        regex       : /^[0-9]*$/,
                        regexText   : 'Solo de contener n�meros'
		}
                ,{
			width:			230,
			xtype:			'combo',
			id:				'id_cmb_estado',
			name:				'cmb_estado',
			hiddenName:		'cmb_estado',
			fieldLabel:		'* Estado ',
			triggerAction:	'all',
			mode:				'local',
			emptyText:		'Seleccione...',
			valueField:		'clave',
			displayField:	'descripcion',
			msgTarget:		'side',
			margins:			'0 20 0 0',
			tabIndex:		8,
			typeAhead:		true,
			forceSelection:true,
			allowBlank:		false,
			store:			catalogoEstado,
			listeners: {
				select: function(combo, record, index) {		 
					Ext.getCmp('id_cmb_delegacion').reset();
					catalogoMunicipio.load({
						params: Ext.apply({
							estado : record.json.clave,
							pais : (Ext.getCmp('id_cmb_pais')).getValue()
						})
					});
				}
			}	
		},{
			width:			230,
			xtype:			'numberfield',
			id:				'telefono',
			name:				'telefono',
			fieldLabel:		'* Tel�fono',
			msgTarget:		'side',
			margins:			'0 20 0 0',
			maxLength:		30,
			tabIndex:		10,
			allowBlank:		false
		},{
			width:			230,
			xtype:			'numberfield',
			id:				'fax',
			name:				'fax',
			fieldLabel:		'Fax',
			maxLength:		30,
			tabIndex:		12,
			allowBlank:		true
		},{
			width:			230,
			xtype:			'combo',
			id:				'id_Domicilio_correspondencia',
			name:				'Domicilio_correspondencia',
			hiddenName:		'Domicilio_correspondencia',
			fieldLabel:		'* Domicilio correspondencia',
			triggerAction:	'all',
			mode:				'local',
			emptyText:		'Seleccionar...',
			valueField:		'clave',
			displayField:	'descripcion',
			msgTarget:		'side',
			margins:			'0 20 0 0',
			tabIndex:		13,
			forceSelection:true,
			typeAhead:		true,
			allowBlank:		false,
			store:			catalogoDomicilio
		}]
	};
		
	var domicilioPanel_der = {
		width:				460,
		xtype:				'fieldset',
		id:					'domicilioPanel_der',
		bodyStyle:			'padding: 6px; padding-right:0px;padding-left:2px;',	
		labelWidth:			150,
		border:				false,
		items: [{
			width:			230,
			xtype:			'textfield',
			id:				'colonia',
			name:				'colonia',
			fieldLabel:		'Colonia',
			margins:			'0 20 0 0',
			maxLength:		100,
			tabIndex:		5,
			allowBlank:		true
		},{
			
			width:			150,
			xtype:			'combo',
			id:				'id_cmb_pais',
			name:				'cmb_pais',
			hiddenName:		'cmb_pais',
			fieldLabel:		'* Pa�s',
			triggerAction:	'all',
			mode:				'local',
			valueField:		'clave',
			displayField:	'descripcion',
			msgTarget:		'side',
			emptyText:		'Seleccione...',				
			margins:			'0 20 0 0',
			tabIndex:		7,
			forceSelection:true,
			allowBlank:		false,
			store:			catalogoPais,
			listeners:{
				select: function(combo, record, index) {
					Ext.getCmp('id_cmb_estado').reset();
					Ext.getCmp('id_cmb_delegacion').reset();
					catalogoEstado.load({
						params: Ext.apply({
							pais: record.json.clave
						})
					});				
				}
			}				
		},{
			width:			230,
			xtype:			'combo',
			id:				'id_cmb_delegacion',
			name:				'cmb_delegacion',
			hiddenName:		'cmb_delegacion',
			fieldLabel:		'* Delegaci�n o municipio',
			triggerAction:	'all',
			emptyText:		'Seleccione...',
			mode:				'local',
			valueField:		'clave',
			displayField:	'descripcion',
			msgTarget:		'side',
			tabIndex:		9,
			forceSelection:true,
			allowBlank:		false,
			store:			catalogoMunicipio,
			listeners:	{
				select:	function(combo, record, index) {
					clave_NomDelegacion = combo.getValue()+"|"+combo.getRawValue();
				}
			}
		},{
			width:			230,
			xtype:			'textfield',
			id:				'id_email',
			name:				'email',
			fieldLabel:		'E-mail',
			margins:			'0 20 0 0',
			msgTarget:		'side',
			maxLength:		100,
			tabIndex:		11,
			allowBlank:		true,
			regex:			/^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z]{2,3})$/,
			regexText:		'Este campo debe ser una direcci�n de correo electr�nico'
		},{
			width:			230,
			xtype:			'combo',
			id:				'id_Oficina_controladora',
			name:				'Oficina_controladora',
			hiddenName:		'Oficina_controladora',
			fieldLabel:		'* Oficina controladora',
			triggerAction:	'all',
			mode:				'local',
			emptyText:		'Seleccionar...',
			valueField:		'clave',
			displayField:	'descripcion',
			msgTarget:		'side',
			margins:			'0 20 0 0',
			tabIndex:		25,
			forceSelection:true,
			allowBlank:		false,
			store:			catalogoOficina_controladora				
		},{
			width:			230,
			xtype:			'combo',
			id:				'id_clave_contragarante',
			name:				'clave_contragarante',
			hiddenName:		'clave_contragarante',
			fieldLabel:		'* Clave del Contragarante relacionado en SIAG',
			triggerAction:	'all',
			mode:				'local',
			emptyText:		'Seleccionar...',
			valueField:		'clave',
			displayField:	'descripcion',
			msgTarget:		'side',
			margins:			'0 20 0 0',
			forceSelection:true,
			allowBlank:		false,
			store:			catalogoContragarante				
		}]
	};

	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		//labelWidth: 300,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			{
				layout	: 'hbox',
				title		: '',
				width		: 940,
				items		: [afiliacion ]
			},			
			{
				layout	: 'hbox',
				title		: '',
				//height	: 280,
				width		: 920,
				items		: [afiliacionPanel ]
			}
			,			
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 940,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			}
		],
		monitorValid	: false,
		buttons			: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				//formBind: true,
				
				handler: function(boton, evento){
					if( Ext.getCmp('formaAfiliacion').getForm().isValid() ){
						Ext.Msg.confirm('', '�Esta seguro de querer enviar su informaci�n? ',	function(botonConf) {
							if (botonConf == 'ok' || botonConf == 'yes') {
								var cmpForma = Ext.getCmp('formaAfiliacion');
								var params = (cmpForma)?cmpForma.getForm().getValues():{};
								cmpForma.el.mask("Procesando", 'x-mask-loading');
								Ext.Ajax.request({
										url: '15forma21Ext.data.jsp',
										params: Ext.apply(params,{
												informacion: 'AltaContragarante',
												delegacionMunicipio: clave_NomDelegacion
												//internacional: internacional
										}),
										callback: procesarAltaContragarante
								});
							}
						});
					} else{
						Ext.Msg.alert('Mensaje', 'Revise los campos obligatorios');
					}		
				}
			
			},
			{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15forma21Ext.jsp';
				}
			}
		]
	});			
	

		var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items: //fp
		[ fpDatos ]
	});
	
	catalogoContragarante.load();
	catalogoDomicilio.load();
	catalogoPais.load();
	catalogoOficina_controladora.load();

	});