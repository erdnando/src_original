
<%@ page contentType="application/json;charset=UTF-8"
	import=" 
		javax.naming.*,
		java.util.*,
		java.sql.*,
		java.text.*, 	
		java.net.*,
		netropology.utilerias.*,	
		com.netro.descuento.*,
		com.netro.cadenas.*,		
		com.netro.exception.*,
		com.netro.model.catalogos.*,
		com.netro.seguridadbean.SeguException,
		com.netro.exception.NafinException,	
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
JSONObject 	jsonObj	= new JSONObject();
//IMantenimientoHome mantenimientoHome = (IMantenimientoHome)ServiceLocator.getInstance().getEJBHome("MantenimientoEJB",IMantenimientoHome.class);	
//IMantenimiento beanMantenimiento = mantenimientoHome.create();
IMantenimiento beanMantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB",IMantenimiento.class);
try{
  if (informacion.equals("getCatalogoEPO") ) {
    
    CatalogoEPO cat = new CatalogoEPO();
    cat.setCampoClave("ic_epo");
    cat.setCampoDescripcion("cg_razon_social");
    infoRegresar = cat.getJSONElementos();

  } else if (informacion.equals("GenerarArchivo") ) {

    String cveEpo = request.getParameter("cboEpo")==null?"":request.getParameter("cboEpo");
    String nombreArchivo = beanMantenimiento.getArchivoBajaProveedores(cveEpo, strDirectorioTemp);
    
    jsonObj.put("msg_error","");
    jsonObj.put("success", new Boolean(true));
    jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
    infoRegresar = jsonObj.toString();	
    
  }
}catch(Throwable t){
  jsonObj.put("msg_error", t.getMessage());
  jsonObj.put("success", new Boolean(false));
	infoRegresar = jsonObj.toString();	
}

%>

<%=infoRegresar%>
