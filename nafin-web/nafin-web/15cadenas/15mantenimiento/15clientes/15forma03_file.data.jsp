<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		org.apache.commons.logging.Log,
		org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException,
		netropology.utilerias.*,
		com.netro.zip.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	boolean flag = true;
	ParametrosRequest req = null;
	String rutaArchivo = "";
	String rutaVirtual= "";
	String PATH_FILE	=	strDirectorioTemp+"15archcadenas/";
	String itemArchivo = "";
	String msgError = "";
	int tamanio = 0;
	int tamanioPer = (2048 * 1024);
	
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		try {
				
				// Definir el tama�o m�ximo que pueden tener los archivos
				upload.setSizeMax(tamanioPer);	//2 MB
				// Parse the request
				req = new ParametrosRequest(upload.parseRequest(request));
				FileItem fItem 		= (FileItem)req.getFiles().get(0);
				itemArchivo				= (String)fItem.getName();
				InputStream archivo 	= fItem.getInputStream();
				tamanio					= (int)fItem.getSize();
		
				if (tamanio > tamanioPer){
						flag = false;
						msgError = "El Archivo es muy Grande, excede el L�mite que es de 2 MB.";
				}
				
				rutaArchivo = PATH_FILE+itemArchivo;
				
				System.out.println("tamanio=="+tamanio);
				System.out.println("tamaPer=="+tamanioPer);
				System.out.println("itemArchivo=="+itemArchivo);
				
				if (flag){
						try {		 
							
							// Guardar Archivo en Disco
							File 		uploadedFile	= new File(rutaArchivo);
							fItem.write(uploadedFile);
							if (!Comunes.validaTamanioImg(uploadedFile, 65, 175)) {
								flag = false;
								msgError = "La imagen debe ser de 175x65 pixeles.";
							} else {
								msgError = "Imagen cargada";
								rutaVirtual= "15archcadenas/";
							}
						 }catch(Throwable e){
							
							flag		= false;
							msgError = "Ocurri� un error al guardar el archivo";
							e.printStackTrace();
							throw new AppException("Ocurri� un error al guardar el archivo");
							
						 }				
					}	
			} catch(Throwable t) {
				
				t.printStackTrace();
				if( t instanceof SizeLimitExceededException  ) {
					throw new AppException("El Archivo es muy Grande, excede el l�mite que es de " + ( tamanioPer / 1048576 ) + " MB.");
				} else {
					throw new AppException("Problemas al Subir el Archivo."); 
				}
					
			}
			
//ngo el RFC y lo imprimo... el ic_if nunca llegara igual lo imprimo�?		
	}
	
%>	
{
	"success": true,
	"flag":	<%=(flag)?"true":"false"%>,
	"msgError":	'<%=msgError%>',
	"path_destino":	'<%=rutaVirtual%>',
	"archivo":	'<%=itemArchivo%>'
}