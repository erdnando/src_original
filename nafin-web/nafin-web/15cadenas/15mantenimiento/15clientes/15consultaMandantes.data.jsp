<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoEstado,com.netro.mandatodoc.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if (informacion.equals("CatalogoEstados")){

		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre");
		cat.setOrden("cd_nombre");
		cat.setClavePais("24");
		infoRegresar = cat.getJSONElementos();

} else if (informacion.equals("Consulta")	||	informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")){

	String numNafinElec = (request.getParameter("numNafinElec")!=null)?request.getParameter("numNafinElec"):"";
	String nombreMandante = (request.getParameter("nombreMandante")!=null)?request.getParameter("nombreMandante"):"";
	String estado = (request.getParameter("estado")!=null)?request.getParameter("estado"):"";
	String rfc = (request.getParameter("rfc")!=null)?request.getParameter("rfc"):"";
	
	int start = 0;
	int limit = 0;
	com.netro.cadenas.ConsMandanteEpo pag = new com.netro.cadenas.ConsMandanteEpo();
	pag.setNumNafinElec(numNafinElec);
	pag.setNombreMandante(nombreMandante);
	pag.setEstado(estado);
	pag.setRfc(rfc);
	pag.setClave_epo(iNoCliente);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pag);

	if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}
			
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
			
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ArchivoPDF")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
}else if ( informacion.equals("obtenDetalle") ){
	
	//MandatoDocumentosHome mandatoDocumentosHome = (MandatoDocumentosHome)ServiceLocator.getInstance().getEJBHome("MandatoDocumentosEJB", MandatoDocumentosHome.class);
	//MandatoDocumentos BeanMandatoDocumentos = mandatoDocumentosHome.create();
	MandatoDocumentos BeanMandatoDocumentos = ServiceLocator.getInstance().lookup("MandatoDocumentosEJB",MandatoDocumentos.class);
	JSONObject jsonObj = new JSONObject();

	String numNafinElec = (request.getParameter("numNafinElec")!=null)?request.getParameter("numNafinElec"):"";
	Hashtable consultaDet = new Hashtable();
			
	consultaDet = BeanMandatoDocumentos.getMandanteDetalles(numNafinElec, iNoCliente);
	if(!consultaDet.isEmpty()){
		jsonObj.put("consultaDet", consultaDet);
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>