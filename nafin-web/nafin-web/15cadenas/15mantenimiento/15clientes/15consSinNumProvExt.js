Ext.onReady(function(){
	var chkELIMINA = [];
	var numero = [];
	var causa = [];
	var numPyme = [];
	var cancelar =  function() {  
		chkELIMINA = [];
		numero = [];
		causa = [];
		numPyme = [];
	}
	function procesaRecargaResultado(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				jsonData = Ext.util.JSON.decode(response.responseText);
				if (jsonData != null){	
					var fp = Ext.getCmp('forma');
					fp.el.unmask();
					fp.el.mask('Enviando...', 'x-mask-loading');		
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
				} else {
					NE.util.mostrarConnError(response,opts);
				}
		}
	}
	var procesarGrabar = function(store, arrRegistros, opts) 	{	
		
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		var columnModelGrid = gridConsulta.getColumnModel();
		var jsonData = store.data.items;
		var numRegistros=0;
		var flag = true;
		var aviso = false;
		var flag2 = false;
		var hayErrores=0;
		var fp = Ext.getCmp('forma'); 
		fp.el.unmask();
	 	cancelar();
		Ext.each(jsonData, function(item,inx,arrItem){
			chkELIMINA.push(item.data.ELIMINA);	
			numero.push(item.data.NUMERO);
			causa.push(item.data.CAUSA);
			numPyme.push(item.data.NUM_PYME);
			numRegistros++;
		});
		store.each(function(record) {
			numReg = store.indexOf(record);
			if(record.data['NUMERO'] =='' && record.data['CAUSA'] =='' && record.data['ELIMINA'] ==true ){	
				Ext.MessageBox.alert('Error de validaci�n','Capture el n�mero o la causa de la eliminaci�n del proveedor',
				function(){
					gridConsulta.startEditing(numReg, columnModelGrid.findColumnIndex('CAUSA'));
					
				});
				hayErrores++;
				flag = false
				return false;	
			}else if(!(record.data['NUMERO'] =='' && record.data['CAUSA'] ==''&& record.data['ELIMINA'] ==false )){	
					flag2 = true;
			} 
		});
		
		if(flag==true && flag2==true){
			Ext.MessageBox.alert('','Guardado con �xito');
			Ext.Ajax.request({
				url: '15consSinNumProvExt.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Captura',
					chkELIMINA:chkELIMINA,
					numero : numero,
					causa: causa,
					numPyme:numPyme,
					numRegistros:numRegistros
				}),
				callback: procesaRecargaResultado
			});
		}else{
			if(flag==false){
				//alert("Capture el n�mero o la causa de la eliminaci�n del proveedor");
			}else{
				alert("No hay registros modificados");
			}
			return;
		}
		
	}
	var catalogoBancoFodeo = new Ext.data.JsonStore({
		id: 'catalogoBancoFodeo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consSinNumProvExt.data.jsp',
		baseParams: {
			informacion: 'catalogoBancoFodeo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoEpo = new Ext.data.JsonStore({
		id: 'catalogoBancoFodeo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consSinNumProvExt.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoPerfil = new Ext.data.JsonStore({
		id: 'catalogoPerfil',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: '15consSinNumProvExt.data.jsp',
		baseParams: {
			informacion: 'catalogoPerfil'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {				
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	//------------Generaci�n de la Consula -------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {					
				el.unmask();//btnGrabar
				Ext.getCmp('btnGrabar').enable();
			} else {		
				Ext.getCmp('btnGrabar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15consSinNumProvExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [	
			{	name: 'NUM_ELECTRONICO'},	
			{	name: 'FECHA_AFILIACION'},				
			{	name: 'RFC'},	
			{	name: 'NOMBRE_PROVEEDOR'},	
			{	name: 'CONVENIO_UNICO'},	
			{	name: 'FEC_AFILIA'},	
			{	name: 'ELIMINA'},
			{	name: 'NUMERO'},
			{	name: 'CAUSA'},
			{	name: 'FECHA_ALTA'},
			{	name: 'NUM_PYME'}
			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Proveedor sin n�mero',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'N�mero Electr�nico',
				tooltip: 'N�mero Electr�nico',
				dataIndex: 'NUM_ELECTRONICO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Fecha Afiliaci�n',
				tooltip: 'Fecha Afiliaci�n',
				dataIndex: 'FECHA_AFILIACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},			
			{
				header: 'Fecha Alta N�m. Proveedor',
				tooltip: 'Fecha Alta N�m. Proveedor',
				dataIndex: 'FECHA_ALTA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			},
			{
				header: 'R.F.C.',
				tooltip: 'R.F.C.',
				dataIndex: 'RFC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'
			},
			{
				header: 'Nombre',
				tooltip: 'nombre',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Convenio �nico',
				tooltip: 'Convenio �nico',
				dataIndex: 'CONVENIO_UNICO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},	
			{
				header: 'N�mero de Proveedor',
				tooltip: 'N�mero de Proveedor',
				dataIndex: 'NUMERO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				editor: {
					xtype: 'textfield',
                    maxLength:	20
				},
				renderer:function(value,metadata,registro){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			},
			{
				xtype:'checkcolumn',
				header:		'Eliminar de la Cadena',
				tooltip:		'Eliminar de la Cadena',
				dataIndex:	'ELIMINA',
				hideable: false,
				sortable:false, 
				width:50
			},
			{
				header: 'Causa',
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				width: 130,
				align: 'center',
				editor: {
					xtype: 'textarea'
				},
				renderer:function(value,metadata,registro){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			}
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Grabar',					
					tooltip:	'Grabar ',
					iconCls: 'icoGuardar',
					id: 'btnGrabar',
					handler: procesarGrabar
				}
			
			]
		}
	});
	//****************catalogo Estado**********************
	var catalogoEstado = new Ext.data.JsonStore({
		id: 'catalogoEstado',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15consSinNumProvExt.data.jsp',
		baseParams: {
			informacion: 'catalogoEstado'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

 	function creditoElec(pagina) {
		if (pagina == 0) { //Afianzadora
			window.location  = "15forma05AfianzadoraExt.jsp"; 	
		}	else if (pagina == 1 ) { // cadena productiva
			window.location ="15consEPOext.jsp";
		}	else if (pagina == 2) { //provedores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15forma05ProveedoresExt.jsp";
		}	else if (pagina == 3) { //Provedor Internacional
			
		}	else if (pagina == 4) { // provedor sin numero
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp";
		}	else if (pagina == 5) { // Distribuidores
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp";		
		}	else if (pagina == 6) { // Fiados
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp";		
		}	else if (pagina == 7) { // Intermediario financiero Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B"; 	
		} else if (pagina == 8) { // Intermediario financiero NO Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB"; 	 	
		} else if (pagina == 9) { // Intermediario financiero Bancario /No Bancario
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB"; 		 	
		} else if (pagina == 10) { // Provedor Carga Masiva EContrac
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon_ext.jsp";
		}	else if (pagina == 11) { // Afiliados a Credito Electronico
			window.location = "15forma05ext_AfCredElectronico.jsp";
		}	else if (pagina == 12) { // Cadena Productiva Internacional
			
		}	else if (pagina == 13) { // Contragarante
			
		}	else if (pagina == 14) { // Mandante
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp"; 	
		}	else if (pagina == 15) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp"; 	
		}	else if (pagina == 16) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp"; 		
		}			

	}

	var storeAfiliacion = new Ext.data.SimpleStore({
		fields: ['clave', 'afiliacion'],
		data : [
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],
			//['3','Proveedor Internacional'],
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],	
			//['12','Cadena Productiva Internacional'],
			//['13','Contragarante'],
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo'],
			['16','Cliente Externo']
			
		]
	});
	var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	
	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consSinNumProvExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada',
			claveEPO: Ext.getCmp('cmbEpo')
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var  elementosForma =  [
		{
			xtype				: 'combo',
			id					: 'cmbTipoAfiliado1',
			name				: 'cmbTipoAfiliado',
			hiddenName 		:'cmbTipoAfiliado', 
			fieldLabel		: 'Tipo de Afiliado',
			width				: 250,
			forceSelection	: true,
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'clave',
			displayField	:'afiliacion',
			value				:  '4',	
			store				: storeAfiliacion,
			listeners: {
				select: {
					fn: function(combo) {						
						var valor = Ext.getCmp('cmbTipoAfiliado1').getValue();						
						creditoElec(valor);						
					}
				}
			}
		},
		{
			xtype				: 'combo',
			id					: 'cmbEpo',
			name				: 'epo',
			hiddenName 		:'epo', 
			fieldLabel		: 'EPO',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccionenar Cadena...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEpo,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero N@E',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					fieldLabel: 'N�mero N@E',
					name: 'Numero',
					id: 'nfNumero',
					allowBlank: true,
					maxLength: 38,
					width: 100,
					displayField: 'descripcion',			
					valueField: 'clave',
					msgTarget: 'side',				
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: '  ',
					width: 30
				},
				{
					xtype: 'button',
					text: 'B�squeda Avanzada...',
					name: 'busquedaAvanzada',
					id: 'btnBusquedaAvanzada',
					iconCls: 'icoBuscar',
					handler: function(boton, evento){
						var cveAfiliado = Ext.getCmp("cmbTipoAfiliado1");
						var epo = Ext.getCmp("cmbEpo");
						var num_naf = Ext.getCmp('nfNumero').getValue();
						if(cveAfiliado.getValue()==""){
							cveAfiliado.markInvalid('Debe Seleccionar Un Tipo De Afiliado');
						}if(epo.getValue()==""){
							epo.markInvalid('Se requiere seleccionar una EPO');
						}else{
							Ext.getCmp('nfNumero').reset();
							var winVen = Ext.getCmp('winBuscaA');
							if (winVen){
								Ext.getCmp('fpWinBusca').getForm().reset();
								Ext.getCmp('fpWinBuscaB').getForm().reset();
								Ext.getCmp('cmb_num_ne').setValue();
								Ext.getCmp('cmb_num_ne').store.removeAll();
								Ext.getCmp('cmb_num_ne').reset();
								winVen.show();
							}else{
								var winBuscaA = new Ext.Window ({
								id:'winBuscaA',
								height: 364,
								x: 300,
								y: 100,
								width: 550,
								modal: true,
								closeAction: 'hide',
								title: 'B�squeda Avanzada',
								items:[
									{
										xtype:'form',
										id:'fpWinBusca',
										frame: true,
										border: false,
										style: 'margin: 0 auto',
										bodyStyle:'padding:10px',
										defaults: {
											msgTarget: 'side',
											anchor: '-20'
										},
										labelWidth: 130,
										items:[
											{
												xtype:'displayfield',
												frame:true,
												border: true,
												value:'<b>Tipo de Afiliado: '+Ext.getCmp('cmbTipoAfiliado1').lastSelectionText+'</b>'
											},
											{
												xtype:'displayfield',
												frame:true,
												border: false,
												value:'Utilice el * para b�squeda gen�rica'
											},{
												xtype:'displayfield',
												frame:true,
												border: false,
												value:''
											},{
												xtype:'hidden',
												id:	'hid_ic_pyme',
												value:''
											},{
												xtype:'hidden',
												id:	'hid_ic_banco',
												value:''
											},{
												xtype: 'textfield',
												name: 'nombre_pyme',
												id:	'txtNombre',
												fieldLabel:'Nombre',
												maxLength:	100
											},{
												xtype: 'textfield',
												name: 'rfc_prov',
												id:	'_rfc_prov',
												fieldLabel:'RFC',
												maxLength:	20
											},{
												xtype: 'textfield',
												name: 'num_pyme',
												id:	'txtNe',
												fieldLabel:'N�mero Proveedor',
												maxLength:	25
											}
										],
										buttonAlign:'center',
										buttons:[
											{
												text:'Buscar',
												iconCls:'icoBuscar',
												handler: function(boton) {
													var claveEpo=Ext.getCmp('cmbEpo').getValue();	
														catalogoNombreProvData.load({ //params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues(), Ext.getCmp('cmbEpo'))		
														params:Ext.apply( Ext.getCmp('fpWinBusca').getForm().getValues(),{
														claveEPO :claveEpo
														})
													});
												}
   										},{
												text:'Cancelar',
												iconCls: 'icoRechazar',
												handler: function() {
													Ext.getCmp('winBuscaA').hide();
													Ext.getCmp('winBuscaA').destroy();
												}
											}
										]
									},{
										xtype:'form',
										frame: true,
										id:'fpWinBuscaB',
										style: 'margin: 0 auto',
										bodyStyle:'padding:10px',
										monitorValid: true,
										defaults: {
											msgTarget: 'side',
											anchor: '-20'
										},
										labelWidth: 80,
										items:[
											{
												xtype: 'combo',
												id:	'cmb_num_ne',
												name: 'ic_pyme',
												hiddenName : 'ic_pyme',
												fieldLabel: 'Nombre',
												emptyText: 'Seleccione. . .',
												displayField: 'descripcion',
												valueField: 'clave',
												triggerAction : 'all',
												forceSelection:true,
												allowBlank: false,
												typeAhead: true,
												mode: 'local',
												minChars : 1,
												store: catalogoNombreProvData,
												tpl : NE.util.templateMensajeCargaCombo
											}
										],
										buttonAlign:'center',
										buttons:[
											{
												text:'Aceptar',
												iconCls:'aceptar',
												formBind:true,
												handler: function() {
													if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
														var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
														var cveP = disp.substr(0,disp.indexOf(" "));
														var desc = disp.slice(disp.indexOf(" ")+1);
														Ext.getCmp('nfNumero').setValue(cveP);
														Ext.getCmp('winBuscaA').hide();
													}
												}
												},{
													text:'Cancelar',
													iconCls: 'icoRechazar',
													handler: function() {	
														Ext.getCmp('winBuscaA').hide();	
														Ext.getCmp('winBuscaA').destroy();
													}
												}
											]
										}
									]
								}).show();
							}
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Captura desde',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaCapturaIni',
					id: 'dfFechaCapturaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'dfFechaCapturaFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaCapturaFin',
					id: 'dfFechaCapturaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'dfFechaCapturaIni',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 20
				}
			]
		},
		{
			xtype			: 'checkbox',
			id				: 'cbConvenioUnico',
			name			: 'convenioUnico',
			hiddenName	: 'convenioUnico',
			fieldLabel	: 'Opera Convenio �nico',
			tabIndex		: 29,
			enable 		: true
		}
	];
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Consulta - Proveedor sin n�mero',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {	
					var epo = Ext.getCmp("cmbEpo");
					var dfFechaCapturaFin=Ext.getCmp('dfFechaCapturaFin');					
					var dfFechaCapturaIni=Ext.getCmp('dfFechaCapturaIni');
					if(epo.getValue()==""){
						epo.markInvalid('Seleccione una EPO');
						return '';
					}
					if(!Ext.isEmpty(dfFechaCapturaIni.getValue()) || !Ext.isEmpty(dfFechaCapturaFin.getValue())){
						if(Ext.isEmpty(dfFechaCapturaFin.getValue())){
							dfFechaCapturaFin.markInvalid('El valor de la fecha de captura Final es requerido');
							dfFechaCapturaFin.focus();
							return;
						}
						if(Ext.isEmpty(dfFechaCapturaIni.getValue())){
							dfFechaCapturaIni.markInvalid('El valor de la fecha de captura Inicial es requerido');
							dfFechaCapturaIni.focus();
							return;
						}
					}
					fp.el.mask('Enviando...', 'x-mask-loading');		
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});					
				}
			}
		]	
	});
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,			
			NE.util.getEspaciador(20)
		]
	});
	catalogoEpo.load();
	catalogoBancoFodeo.load();
	catalogoEstado.load();
});