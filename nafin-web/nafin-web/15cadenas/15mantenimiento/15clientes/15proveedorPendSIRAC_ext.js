Ext.onReady(function(){
	var fechaHoy = new Date();
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		pnl.el.unmask();
		if (arrRegistros != null) {
			gridProveedores.show();
			if(store.getTotalCount() > 0) {
					gridProveedores.el.unmask();
			} else {
				gridProveedores.el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

//------------------------------------------------------------------------------
	var catalogoEpo = new Ext.data.JsonStore
	({
	   id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consulta01Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEstatusAfil = new Ext.data.ArrayStore({
        fields: [
            'clave',
            'descripcion'
        ],
        data: [['N', 'Proveedor Pendiente SIRAC'],['S', 'Afiliados SIRAC']]
    });
	
	var storeProvPendSirac = new Ext.data.JsonStore({
		root : 'registros',
		url : '15proveedorPendSIRAC_ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
			fields: [
					{name: 'FECAFILIA'},//
					{name: 'NAFELEC'},//
					{name: 'NOMBREPYME'},//
					{name: 'RFC'},//
					{name: 'NUMSIRAC'},//
					{name: 'ERRORSIRAC'},
					{name: 'CSAFILIASIRAC'},
					{name: 'CSTIPOPERSONA'},
					{name: 'CSAFILIATROYA'},
					{name: 'CVEPYME'},
					{name: 'CVEEPO'},
					{name: 'LIGA'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	

	var elementosForma = [
		{
			xtype:		'textfield',
			name:			'c_numNE',
			id:			'c_numNE1',
			maskRe:		/[0-9]/,
			regex:		/^[0-9]*$/,
			regexText: 'No es un n�mero v�lido',
			allowBlank:	true,
			//disabled:	true,
			hidden:		false,
			anchor: '40%',
			fieldLabel: 'N�mero N@E',
			maxLength:	25
		},{
			xtype 		: 'textfield',
			name  		: 'c_rfc',
			id    		: 'c_rfc1',
			fieldLabel	: 'R.F.C.',
			maxLength	: 20,
			anchor		: '40%',
			hidden		: false,
			allowBlank:	true,
			margins		: '0 20 0 0',
			msgTarget	: 'side',
			regex			:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
			regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
			'en el formato NNN-AAMMDD-XXX donde:<br>'+
			'NNN:son las iniciales del nombre de la empresa<br>'+
			'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
			'XXX:es la homoclave'
		},{
			xtype: 'combo',
			fieldLabel: 'Estatus',
			emptyText: 'Seleccionar Estatus',
			displayField: 'descripcion',
			//allowBlank: false,
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			allowBlank:	true,
			minChars: 1,
			//hidden:		true,
			store: catalogoEstatusAfil,
			//tpl: NE.util.templateMensajeCargaCombo,
			name:'c_estatus',
			id: 'c_estatus1',
			mode: 'local',
			hiddenName: 'c_estatus',
			forceSelection: true
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Afiliaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fec_ini',
					id: 'fec_ini',
					allowBlank: true,
					startDay: 0,
					minValue: '01/01/1901',
					value: fechaHoy.format('d/m/Y'),
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fec_fin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 24
				},
				{
					xtype: 'datefield',
					name: 'fec_fin',
					id: 'fec_fin',
					minValue: '01/01/1901',
					value: fechaHoy.format('d/m/Y'),
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fec_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},{
			xtype: 'checkbox',
			name: 'chkRegErrores',
			fieldLabel: 'Registros con Errores',
			id: 'chkRegErrores1',
			hiddenName:'chkRegErrores',
			inputValue: 'S',
			listeners:{
					check: function(checbox){
							if(checbox.getValue()){
								Ext.getCmp('fec_ini').setValue('');
								Ext.getCmp('fec_fin').setValue('');
							}else{
								Ext.getCmp('fec_ini').setValue(fechaHoy.format('d/m/Y'));
								Ext.getCmp('fec_fin').setValue(fechaHoy.format('d/m/Y'));
							}
						}	
			
			}
		}


	]

	var formCritBusqueda = new Ext.form.FormPanel({
		hidden: false,
		height: 'auto',
		width: 500,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		labelWidth: 100,
		defaultType: 'textfield',
		frame: true,
		title: 'Proveedor Pend. SIRAC',
		id: 'formaMod',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				//Bot�n BUSCAR
				xtype: 'button',
				text: 'Buscar',
				name: 'btnBuscar',
				iconCls: '',
				hidden: false,
				formBind: true,
				handler: function(boton, evento){
          
          if(Ext.getCmp('fec_ini').getValue()!='' || Ext.getCmp('fec_fin').getValue()!=''){
            if(Ext.getCmp('fec_ini').getValue()==''){
              Ext.getCmp('fec_ini').markInvalid('Es necesario indicar rango de fechas');
              return;
            }else if (Ext.getCmp('fec_fin').getValue()==''){
              Ext.getCmp('fec_fin').markInvalid('Es necesario indicar rango de fechas');
              return;
            }
          }
          
          
					pnl.el.mask('Buscando...', 'x-mask');
					storeProvPendSirac.load({ 
						params: Ext.apply(formCritBusqueda.getForm().getValues())
					});

					/*
					Ext.Ajax.request({
						url: '15consulta01Ext.data.jsp',
						params: Ext.apply(fpModifica.getForm().getValues(),{accion:accionMod_Rea,informacion:'ModificaReasigna', no_cuenta:no_cuenta}),
						callback: accionModReaf
					});
					*/
				}
			}
		]
	});
	
	var gridProveedores = new Ext.grid.GridPanel({
		name: 'gridProveedores',
		id: 'gridProveedores1',
		width: 800,
		height: 300,
		store: storeProvPendSirac,
		style: 'margin:0 auto;',
		hidden: true,
		stripeRows: true,
		loadMask: true,
		frame: true,
		columns:[
			{
				header: 'Fecha de Afiliaci�n',
				tooltip: 'Fecha de Afiliaci�n',
				sortable: true,
				dataIndex: 'FECAFILIA',
				width: 120,
				align: 'center'
			},
			{
				header: '<center>N�mero N@E</center>',
				tooltip: 'N�mero N@E',
				sortable: true,
				dataIndex: 'NAFELEC',
				width: 100,
				align: 'center'
			},
			{
				header: '<center>Pyme</center>',
				tooltip: 'Pyme',
				sortable: true,
				dataIndex: 'NOMBREPYME',
				width: 150,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: '<center>R.F.C.</center>',
				tooltip: 'R.F.C.',
				sortable: true,
				dataIndex: 'RFC',
				width: 120,
				align: 'left'
			},
			{
				header: 'N�mero SIRAC',
				tooltip: 'N�mero SIRAC',
				sortable: true,
				dataIndex: 'NUMSIRAC',
				width: 150,
				align: 'center'
			},
			{
				header: 'Error',
				tooltip: 'Error',
				sortable: true,
				dataIndex: 'ERRORSIRAC',
				width: 150,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: ' ',
				tooltip: ' ',
				sortable: true,
				dataIndex: 'LIGA',
				width: 100,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if(record.get('CSAFILIASIRAC')=='S' && record.get('ERRORSIRAC')!=''){
						return '<a href="15forma05ProveedoresExt.jsp?ic_pyme='+record.get('CVEPYME')+'&txtaction=mod&txtTipoPersona='+record.get('CSTIPOPERSONA')+'&TipoPYME=1&ic_epo='+record.get('CVEEPO')+'&pantalla=pendSIRAC&url_origen=15proveedorPendSIRAC.jsp&csTroya='+record.get('CSAFILIATROYA')+'">Corregir</a>';
					}else{
						return ' ';
					}
				}
			}
		]
	});
	

	var pnl = new Ext.Container
	({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 940,
		items:
		[
			NE.util.getEspaciador(30),
			formCritBusqueda,
			NE.util.getEspaciador(30),
			gridProveedores
		]
	});

});