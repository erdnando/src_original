
Ext.onReady(function(){
	
 var nafinElectronico = '';
 var nombreUniversidad = '';
 var numeroUniversidad = '';
/*--------------------------------- Handler's -------------------------------*/

	var procesarConsultaData = function(store, arrRegistros, opts) 
	{
		pnl.el.unmask();
		var fp = Ext.getCmp('forma');
		
		if (arrRegistros != null )	{
			if (!grid.isVisible()) 	{ 
				grid.show();
			}
			
			/*declaro botones y una variable para manipular el grid*/
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDF'); 			
			var el = grid.getGridEl();			
			if (store.getTotalCount() <= 0 ) 	{	
				el.unmask();		
				grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				btnGenerarArchivo.disable();
				btnImprimirPDF.disable();
				
			}
			else {
				grid.getGridEl().unmask();
				btnGenerarArchivo.enable();
				btnImprimirPDF.enable();
			}
		}	
	}
	
	
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnImprimirPDF').setIconClass('icoPdf');
			Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
			Ext.getCmp('pdfUsuarios').setIconClass('icoPdf');

			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		
	var procesarSuccessFailureBorraUniversidad = function(opts, success, response) 
   {
		grid.getGridEl().unmask();
		if (success == true ) {
			Ext.Msg.alert('','El registro fue eliminado');
			consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{	
						operacion: 'Generar',
						start : 0,
						limit: 15})
					});	
		}
		else {
			Ext.Msg.alert('','Ocurrio un error durante la actualizacion de datos.');	
		}
	}
	
	var procesarConsultaDataUsuarios = function (store, arrRegistros, opts) 
	{
		if (arrRegistros != null )	{
			if (store.getTotalCount() > 0 ) {	
				var ventana = Ext.getCmp('winUsuarios');
				if (ventana) {
					fpUsuarios.getForm().reset();
					ventana.show(); 
				}
				
				else 
				{
					new Ext.Window({
							title			: 'Usuario de la Universidad',
							id				: 'winUsuarios',
							heigth		:	400,										
							width			:	300,
							minWidth		:	200,
							minHeight	: 	200,
							frame 		: true,
							modal			: true,
							closeAction	: 'hide',
							constrain	: true,
							items			: 
							[
								
								{	frame: true, html:'<div align="center"> <b>N@E: ' + nafinElectronico + ' - ' + nombreUniversidad + '<b></div>'	},
								fpUsuarios
							]
						}).show();   
					
					
				}
			}
			else {
				Ext.MessageBox.alert('Mensaje de p�gina web','No existen usuarios parametrizados para la Universidad seleccionada' );
			}
		}
	}
	
	var procesarSuccessFailureVerUsuario = function(opts, success, response) 
   {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var reg = Ext.util.JSON.decode(response.responseText).registros[0];
			if (reg != null) {
				
				var clave = reg.CLAVE;
				var empresa = reg.EMPRESA;
				var ne = reg.NAFIN_ELECTRONICO;
				var nombre = reg.NOMBRE;
				var paterno = reg.APP;
				var materno = reg.APM;
				var correo = reg.EMAIL;
				var perfil = reg.PERFIL_ACTUAL;		
				
				
				var ventana2 = Ext.getCmp('winInfoUsuarios');
				if (ventana2) {
						ventana2.show(); 
				}
					
				else 
				{
					new Ext.Window({
							title			: 'Informaci�n Usuario',
							id				: 'winInfoUsuarios',
							heigth		:	400,										
							width			:	300,
							minWidth		:	200,
							minHeight	: 	200,
							frame 		: true,
							modal			: true,
							closeAction	: 'hide',
							items			: 
							[
								{	frame: true,border:false, html:'<td  align="right">Clave de Usuario:</td>' + '<td class="formas"  align="left"><b>' + clave + '</b></td>'},
								{	frame: true,border:false, html:'<td  align="right">Empresa:</td>' + '<td class="formas"  align="left"><b>' + empresa + '</b></td>'},
								{	frame: true,border:false, html:'<td  align="right">Nafin Electr�nico:</td>' + '<td class="formas"  align="left"><b>' + ne + '</b></td>'},
								{	frame: true,border:false, html:'<td  align="right">Nombre:</td>' + '<td class="formas"  align="left"><b>' + nombre + '</b></td>'},
								{	frame: true,border:false, html:'<td  align="right">Apellido Paterno:</td>' + '<td class="formas"  align="left"><b>' + paterno + '</b></td>'},
								{	frame: true,border:false, html:'<td  align="right">Apellido Materno:</td>' + '<td class="formas"  align="left"><b>' + materno + '</b></td>'},
								{	frame: true,border:false, html:'<td  align="right">E-mail:</td>' + '<td class="formas"  align="left"><b>' + correo + '</b></td>'},
								{	frame: true,border:false, html:'<td  align="right">Perfil Actual:</td>' + '<td class="formas"  align="left"><b>' + perfil + '</b></td>'}
																						
							]
						}).show();   
					
					
				}
			}
		}
		else {
				Ext.MessageBox.alert('Mensaje de p�gina web','Ocurrio un error en la recuperacion de datos del usuario' );
		}
	}


/*------------------------------- End Handler's -----------------------------*/
/*****************************************************************************/
 
	var procesarCatalogoPais = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('id_cmb_pais').setValue('24');
		}
	}
/*---------------------------------- Store's --------------------------------*/

	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [ ['0','Afianzadora']
				,['1','Cadena Productiva']
				,['2','Proveedores']
				//,['3','Proveedor Internacional']
				,['4','Proveedor sin n�mero']	
				,['5','Distribuidores']
				,['6','Fiados']
				,['7','Intermediario Financiero Bancario']
				,['8','Intermediario Financiero No Bancario']	
				,['9','Intermediarios Bancarios/No Bancarios']	
				,['10','Proveedor Carga Masiva EContract']	
				,['11','Afiliados a Cr�dito Electr�nico']	
				//,['12','Cadena Productiva Internacional']
				//,['13','Contragarante']
				,['14','Mandante']
				,['15','Universidad-Programa Cr�dito Educativo'],
				['16','Cliente Externo']
				]
	});
	// catalogo Grupo
	var catalogoGrupo = new Ext.data.JsonStore({
		id				: 'catGrupo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma28Ext.data.jsp',
		baseParams	: { informacion: 'catalogoGrupo'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});		
	
  // catalogo Estado
	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma28Ext.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
  
 
/*--------------------------------- End Store's -----------------------------*/
/****** Store�s Grid�s *******/	
	var consultaData = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'15forma28Ext.data.jsp',	
			baseParams:{informacion:'ConsultaGrid'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'IC_UNIVERSIDAD'},
				{name: 'NONAFIN'},
				{name: 'GRUPO'},
				{name: 'RAZON_SOCIAL'},
				{name: 'CAMPUS'},
				{name: 'DOMICILIO'},
				{name: 'ESTADO'},	
				{name: 'TELEFONO'},					
				{name: 'APODERADO'},
				{name: 'ALTA'},					
				{name: 'ESTATUS'}
				
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData,
				exception: NE.util.mostrarDataProxyError
			}
	});
	
	var consultaDataUsuarios = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'15forma28Ext.data.jsp',	
			baseParams: { informacion: 'ConsultaUsuarios'},
			totalProperty: 'total',			
			fields: [	{name: 'USUARIO'}		],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{ 
				load: procesarConsultaDataUsuarios,
				exception: NE.util.mostrarDataProxyError	
			}		
	});

/*****************************************************************************/
/*------------------------------------------ Grids ------------------------------------------*/	
	 
	var grid = new Ext.grid.GridPanel
	({
		title:'',
		id: 'grid',
		store: consultaData,
		style: ' margin:0 auto;',
		stripeRows: true,
		loadMask:false,
		hidden: true,
		height:430,
		width:900,	
		frame:true, 
		header:true,
		columns: [
			{										
				dataIndex : 'IC_UNIVERSIDAD',
				hidden:true	
			},	
			{										
				header : 'N�mero de Nafin Electr�nico',
				tooltip: 'N�mero de Nafin Electr�nico',
				dataIndex : 'NONAFIN',
				sortable: true,
				width: 90,
				resizable: true,				
				align: 'center'	
			},
			{										
				header : 'Grupo',
				tooltip: 'Grupo',
				dataIndex : 'GRUPO',
				sortable: true,
				width: 90,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Nombre Universidad',
				tooltip: 'Nombre Universidad',
				dataIndex : 'RAZON_SOCIAL',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'	
			},		
			{
				header : 'Nombre Campus',
				tooltip: 'Nombre Campus',
				dataIndex : 'CAMPUS',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'
			},
			{
				header : 'Domicilio',
				tooltip: 'Domicilio',
				dataIndex : 'DOMICILIO',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'
			},
			{
				header : 'Estado',
				tooltip: 'Estado',
				dataIndex : 'ESTADO',
				sortable: true,
				width: 90,
				resizable: true,				
				align: 'center'
			},
			{										
				header : 'Tel�fono',
				tooltip: 'Tel�fono',
				dataIndex : 'TELEFONO',
				sortable: true,
				width: 80,
				resizable: true,				
				align: 'center'	
			},
			{										
				header : 'Apoderado Legal',
				tooltip: 'Apoderado Legal',
				dataIndex : 'APODERADO',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'left'	
			},
			{										
				header : 'Fecha de Alta Universidad',
				tooltip: 'Fecha de Alta Universidad',
				dataIndex : 'ALTA',
				sortable: true,
				width: 80,
				resizable: true,				
				align: 'center'	
			},
			{
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'center'	
			},
			{
				xtype:	'actioncolumn',
				header : 'Seleccionar', 
				tooltip: 'Seleccionar',
				width:	80,	
				align: 'center', 
				items: 
				[
					// Modificar 
					{	
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {									
																
								this.items[0].tooltip = 'Modificar';
								return 'modificar';					
						},
						handler: function(grid, rowIndex, colIndex, item, event) 
						{
							var registro = grid.getStore().getAt(rowIndex);
							var numNE = registro.data['IC_UNIVERSIDAD'];
							window.location = '15forma29Ext.jsp?ic_universidad=' + numNE; // cambiar la pagina aki!!!
						}
					},		
					
					//para Borrar									
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						this.items[1].tooltip = 'Borrar';
										return 'borrar';	
								
						},
						handler: function(grid, rowIndex, colIndex, item, event) 
						{
							var registro = grid.getStore().getAt(rowIndex);
							var numNE = registro.data['IC_UNIVERSIDAD'];
							
							Ext.Msg.confirm('', 'El registro seleccionado ser� eliminado, �Desea continuar?',	function(botonConf) {
										if (botonConf == 'ok' || botonConf == 'yes') {
											grid.getGridEl().mask('Procesando', 'x-mask-loading');
															
											Ext.Ajax.request({
													url: '15forma28Ext.data.jsp',
													params: Ext.apply({										
															informacion: 'borraUniversidad',
															numNE: numNE
													}),
													callback: procesarSuccessFailureBorraUniversidad
											});
										}
							});		
						} //handler
					}
				]				
			},
			{
				xtype:	'actioncolumn',
				header : 'Cuentas de Usuario', tooltip: 'Cuentas de Usuario',
				dataIndex : '',
				width:	60,	
				align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					return 'Ver';
				},
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {							
								return 'iconoLupa';
							
						},			
						handler: function(grid, rowIndex, colIndex, item, event) {	
						
							var registro = grid.getStore().getAt(rowIndex);
							var numUni = registro.data['IC_UNIVERSIDAD'];
							nafinElectronico  = registro.data['NONAFIN'];
							nombreUniversidad = registro.data['RAZON_SOCIAL'];
							numeroUniversidad = registro.data['IC_UNIVERSIDAD'];
							consultaDataUsuarios.load({
								params: Ext.apply(fp.getForm().getValues(),{
									numUni : numUni }) 
							});	
						}
					}
				]
			}
		],
		bbar: {
					xtype: 'paging',
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consultaData,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "",
					
					items: ['->','-',
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
									iconCls: 'icoXls',
									handler: function(boton, evento){
										//boton.disable();
										boton.setIconClass('loading-indicator');
										totalProperty:'total',
										Ext.Ajax.request({
											url: '15forma28Ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'GenerarCSV'}),
											callback: descargaArchivo
										});
									}
								},
								'-',
								{
				
									xtype: 'button',
									text: 'Imprimir PDF',	
									id: 'btnImprimirPDF',
									iconCls: 'icoPdf',
									handler: function(boton, evento) 
									{
									//	boton.disable();
										boton.setIconClass('loading-indicator');
										totalProperty:'total',
										Ext.Ajax.request({
											url: '15forma28Ext.data.jsp',
											params: Ext.apply (fp.getForm().getValues(),{
												informacion: 'GenerarPDF',
												start : 0,
												limit: 15 
												
												}),
												
											callback: descargaArchivo
										});
									}
								}							
					]
				}	
	});


//---------------------Grid Usuarios--------------------------------------
	var gridUsuarios = new Ext.grid.GridPanel
	({
		title      : '',
		id 		  : 'gridUsuarios',
		store 	  : consultaDataUsuarios,
		style 	  : 'margin:0 auto;',
		stripeRows : true,
		loadMask	  : false,
		hidden	  : false,
		width		  : 152,
		autoHeight : true,
		frame		  : true, 
		header	  : true,
		columns    : [
			{
				xtype		: 'actioncolumn',
				header 	: 'Login Usuario', 
				tooltip	: 'Login Usuario', 
				dataIndex	: 'USUARIO',	
				width		:  150,	
				align		: 'center', 
				hidden	: false,	
				renderer	: function(value, metadata, record, rowindex, colindex, store) {
						return value;
				}
				,
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver Usuario';
							return 'iconoLupa';
						},
						//handler: procesarVerUsuario
						handler: function(grid, rowIndex, colIndex, item, event) {	
							var registro = grid.getStore().getAt(rowIndex);
							var usuario = registro.data['USUARIO'];
							Ext.Ajax.request({
									url: '15forma28Ext.data.jsp',
									params: Ext.apply({										
											informacion: 'DetalleUsuario',
											usuario: usuario
									}),
									callback: procesarSuccessFailureVerUsuario
							});
						}
					}
				]
			}
		]
		
	});
/*-------------------------------- Componentes ------------------------------*/
	

	var elementosForma = 
	[{
		xtype: 'panel',
		labelWidth: 180,
		layout: 'form',
		items: [ 				
				{
					xtype				: 'combo',
					id					: 'id_afiliacion',
					name				: 'afiliacion',
					hiddenName 		:'afiliacion', 
					fieldLabel		: 'Tipo de Afiliado',
					width				: 250,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',
					value				: '15',
					store				: storeAfiliacion, 
					listeners: {
						select: function(combo, record, index) {
							var pagina = Ext.getCmp('id_afiliacion').getValue();
							if (pagina == 0) { 
								window.location  = "15forma05AfianzadoraExt.jsp"; 	
								//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?Distribuidores=&paginaNo=1&radio01=AFI&selecOption=AFI&consultar=N&Sin_Num_Prov=N"; 	
							}
							else if (pagina == 1 ) { 
								window.location ="15consEPOext.jsp";
								//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?Distribuidores=&paginaNo=1&radio01=EPO&internacional=N&consultar=N&InterFinan=&Proveedor=&selecOption=EPO"; 	
							} 
							else if (pagina == 2) { 								
								window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15forma05ProveedoresExt.jsp";
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?NumClienteSIRAC=&Distribuidores=&noBancoFondeo=&paginaNo=1&consultar=N&internacional=N&InterFinan=&Proveedor=&TxtNoProveedor=&TxtNumElectronico=&TxtNombre=&TxtRFC=&TxtCadProductiva=&radio01=3|PI&selecOption=3|PI&sel_edo=0"; 	
							} 
							else if (pagina == 3) { 
								//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?noBancoFondeo=&TxtNombre=&TxtNumElectronico=&Distribuidores=&paginaNo=1&radio01=1|F&internacional=N&consultar=N&InterFinan=&Proveedor=&sel_edo=0&selecOption=1|F"; 	
							} 
							else if (pagina == 4) { 
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp";
								//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProv.jsp?selecOption=PSN&consultar=N"; 	
							} 
							else if (pagina == 5) { 
								window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp";		
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?noBancoFondeo=&accion=&fec_fin=&TxtCadProductiva=&radio01=2|M&consultar=N&num_electronico=&fec_ini=&selecOption=2|M"; 	
							} 
							else if (pagina == 6) { 
								window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp";		
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consfiados.jsp?selecOption=FIA&consultar=N&valTerminos=N"; 	
							} 
							else if (pagina == 7) { 
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B"; 	
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?numFiado=&accion=&valTerminos=N&txtRfc=&radio01=IF|B&consultar=N&nomFiado=&nomEstado=&selecOption=IF|B"; 	
							} 
							else if (pagina == 8) {
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB"; 	 	
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?TxtNombre=&TxtNumElectronico=&noBancoFondeo=&Distribuidores=&paginaNo=1&radio01=IF|NB&internacional=N&consultar=N&InterFinan=&Proveedor=&sel_edo=0&selecOption=IF|NB"; 	
							} 
							else if (pagina == 9) {
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB"; 		 	
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?TxtNombre=&TxtNumElectronico=&noBancoFondeo=&Distribuidores=&paginaNo=1&radio01=IF|FB&internacional=N&consultar=N&InterFinan=&Proveedor=&sel_edo=0&selecOption=IF|FB"; 	
							} 
							else if (pagina == 10) {
								window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon_ext.jsp";
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon.jsp?selecOption=CME&consultar=N&valProcesado=A"; 	
							}
							else if (pagina == 11) { 
								window.location = "15forma05ext_AfCredElectronico.jsp";
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?numResumen=&accion=&fec_fin=&TxtCadProductiva=&radio01=ACE&selecOption=ACE&consultar=N&valProcesado=A&fec_ini="; 	
							}
							else if (pagina == 12) { 
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?Distribuidores=&TxtNumClienteSIRAC=&TxtFechaAltaDe=&paginaNo=1&radio01=EPOI&internacional=N&consultar=N&InterFinan=&TxtNombreoRS=&Proveedor=&TxtRFC=&TxtFechaAltaA=&NoIf=&selecOption=EPOI"; 	
							}
							else if (pagina == 13) { 
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?TxtNombre=&TxtNumElectronico=&Distribuidores=&paginaNo=1&radio01=IF|CO&internacional=S&consultar=N&InterFinan=&Proveedor=&sel_edo=0&selecOption=IF|CO"; 	
							}
							else if (pagina == 14) { 
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp"; 	
							//	window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?TxtNombre=&TxtNumElectronico=&Distribuidores=&paginaNo=1&radio01=Mandante&internacional=N&consultar=N&InterFinan=&Proveedor=&sel_edo=0&selecOption=Mandante"; 	
							}
							else if (pagina == 15)	{ 
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp"; 	
								//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp"; 	
							}
							else if (pagina == 16) { // Universidad Programa Credito Educativo
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp"; 		
							}	
						}
					}
					
				},
				{
						xtype			: 'textfield',
						name			: 'numeroNE',
						id				: 'numeroNE',
						fieldLabel	: 'N�mero de Nafin Electr�nico',
						maxLength	: 30,
						width			: 100,
						allowBlank	: true
				},
				{
					xtype				: 'combo',
					id					: 'id_grupo',
					name				: 'grupo',
					hiddenName 		: 'grupo',
					fieldLabel		: 'Grupo',
					width				: 400,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccionar Grupo...',
					store				: catalogoGrupo
				},
				{
						xtype			: 'textfield',
						name			: 'sUniversidad',
						id				: 'sUniversidad',
						fieldLabel	: 'Nombre Universidad',
						maxLength	: 100,
						width			: 300,
						allowBlank	: true
				},
				{
						xtype			: 'textfield',
						name			: 'sCampus',
						id				: 'sCampus',
						fieldLabel	: 'Nombre Campus',
						maxLength	: 100,
						width			: 300,
						allowBlank	: true
				},
				{
					xtype				: 'combo',
					id					: 'id_cmb_estado',
					name				: 'cmb_estado',
					hiddenName 		: 'cmb_estado',
					fieldLabel		: 'Estado',
					width				: 150,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccione Estado',
					store				: catalogoEstado
				}
			]
	}];
	
	
//---------------------------------------------------------------------------//	

	/*-------------------------- Popup Ver Usuarios --------------------------*/
	
	var fpUsuarios = new Ext.form.FormPanel({
		id				: 'fUsuarios',
      frame			: true,
		width			: 295,
		height		: 180,
		align			:'center',
		layout		: 'form',
      items			: [ gridUsuarios	],
		monitorValid: true,
		buttons: [
			{
				text: 'Imprimir PDF',
				id: 'pdfUsuarios',
				iconCls: 'icoPdf',
				formBind: true,
				handler: function(boton, evento) {					
					Ext.Ajax.request({
											url: '15forma28Ext.data.jsp',
											params: Ext.apply (fp.getForm().getValues(),{
												informacion: 'GenerarPDFUsuarios',
												numeroUniversidad: numeroUniversidad
												}),
												
											callback: descargaArchivo
										});
				} //fin handler
			},
			{	text: 'Cancelar',
					handler: function(){					
						Ext.getCmp('winUsuarios').hide();
					}				
			}
		]
	});
	
	/*-------------------------- Fin Popup Dispersion --------------------------*/
	
	var fp = new Ext.form.FormPanel
	({
		id: 'forma',
		width: 680,
		style: ' margin:0 auto;',
		title:	'',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 140,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					pnl.el.mask('Enviando...', 'x-mask-loading');
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{	
						operacion: 'Generar',
						start : 0,
						limit: 15})
					});	
					
				} //fin handler
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items: //fp
		[ 
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});

	
	
	catalogoEstado.load({ params : Ext.apply({
									pais : '24'	})
							});
	catalogoGrupo.load();
	
	

});