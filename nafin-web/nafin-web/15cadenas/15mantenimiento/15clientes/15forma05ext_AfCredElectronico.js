Ext.onReady(function() {

	function creditoElec(pagina) {
		if (pagina == 0) { 
			window.location  = "15forma05AfianzadoraExt.jsp"; 	
		}	else if (pagina == 1 ) { 
			window.location ="15consEPOext.jsp";
		}	else if (pagina == 2) { //Proveedores
			window.location = "./15forma05ProveedoresExt.jsp";
		}	else if (pagina == 3) { //Proveedor internacional ???
			//window.location = "./15forma5ext.jsp";
			//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?noBancoFondeo=&TxtNombre=&TxtNumElectronico=&Distribuidores=&paginaNo=1&radio01=1|F&internacional=N&consultar=N&InterFinan=&Proveedor=&sel_edo=0&selecOption=1|F"; 	
		}	else if (pagina == 4) {  //Proveedor sin Numero
			//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProv.jsp?selecOption=PSN&consultar=N"; 	//Modificado
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp"; 	//Modificado
		}	else if (pagina == 5) {
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp";		
			//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?noBancoFondeo=&accion=&fec_fin=&TxtCadProductiva=&radio01=2|M&consultar=N&num_electronico=&fec_ini=&selecOption=2|M"; 	
		}	else if (pagina == 6) { 
			//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consfiados.jsp?selecOption=FIA&consultar=N&valTerminos=N"; 	
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp";		
		}	else if (pagina == 7) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B"; 	
		} else if (pagina == 8) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB"; 	 	
		} else if (pagina == 9) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB"; 		 	
		} else if (pagina == 10) { // proveedor carga masiva EContract
			window.location = "./15consCargaEcon_ext.jsp";
			//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon.jsp?selecOption=CME&consultar=N&valProcesado=A"; 	
		}	else if (pagina == 11) { // Afiliados a Credito Electronico
			//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?numResumen=&accion=&fec_fin=&TxtCadProductiva=&radio01=ACE&selecOption=ACE&consultar=N&valProcesado=A&fec_ini="; 	
			window.location = "15forma05ext_AfCredElectronico.jsp";
		}	else if (pagina == 12) { // Cadena Productiva Internacional
			//window.location = "./15forma5ext.jsp";
			//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?Distribuidores=&TxtNumClienteSIRAC=&TxtFechaAltaDe=&paginaNo=1&radio01=EPOI&internacional=N&consultar=N&InterFinan=&TxtNombreoRS=&Proveedor=&TxtRFC=&TxtFechaAltaA=&NoIf=&selecOption=EPOI"; 	
		}	else if (pagina == 13) { // Contragarante
			//window.location = "./15forma5ext.jsp";
			//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?TxtNombre=&TxtNumElectronico=&Distribuidores=&paginaNo=1&radio01=IF|CO&internacional=S&consultar=N&InterFinan=&Proveedor=&sel_edo=0&selecOption=IF|CO"; 	
		}	else if (pagina == 14) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp"; 	
		}	else if (pagina == 15) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp"; 	
		}	else if (pagina == 16) { // Universidad Programa Credito Educativo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp"; 		
		}		

	}

	var storeAfiliacion = new Ext.data.SimpleStore({
		fields: ['clave', 'afiliacion'],
		data : [
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],
			//['3','Proveedor Internacional'],
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],	
			//['12','Cadena Productiva Internacional'],
			//['13','Contragarante'],
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo'],
			['16','Cliente Externo']
		]
	});
	
	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);
	
/****************************************************************************** 
 *	1. Carga de nuevo la pagina de inicio cuando se pulsa el bot�n "Regresar"	*
 * ocuando se actualizan los datos correctamente										*
 ******************************************************************************/	
	function direcciona(){
		window.location = '15forma05ext_AfCredElectronico.jsp';
	}
	
/******************************************************************************
 *										2. Descargar archivos								 	*
 ******************************************************************************/
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnImprimir').setIconClass('icoPdf');
			Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	} 

/***************************************************************************************
 * 3. Valida que por lo menos se tenga un elemento del form para realizar la b�squeda	*
 ***************************************************************************************/
 	function validaCamposVacios(){
		var flag = false;
		if(Ext.getCmp('no_cliente_sirac1').getValue() == '' && Ext.getCmp('rfc1').getValue() == '' && 
		Ext.getCmp('razon_social1').getValue() == '' && Ext.getCmp('ic_if1').getValue() == '' && 
		Ext.getCmp('fecha_alta_de').getValue() == '' && Ext.getCmp('fecha_alta_a').getValue() == '' ) { 
			flag = true;
		} else{
			flag = false;
		}
		return flag;
	}
	
/*************************************************************************************************
 * 4. Valida que las ventas totales netas sean mayores a $5000.00 antes de actualizar los datos	 *
 *************************************************************************************************/
	function validaVentasTotales(){
		var valor = parseFloat(Ext.getCmp('ventas_mod1').getValue());
		if(valor < 5000){
			return false;
		} else {
			return true;
		}
	}

/******************************************************************************
 * 		5. Valida si el RFC es invalidado o no de acuerdo al checkbox			*
 ******************************************************************************/	
	function validaRFC(){
		var flag = false;
		if(Ext.getCmp('tipo_persona1').getValue() == 'M'){
			if(Ext.getCmp('invalidar_mod1').getValue() == false){ //No se seleccion� el checkbox
				if(Ext.getCmp('rfc_mod1').getValue() == ''){
					flag = false;
				} else{
					flag = true;
				}
			} else{ //Si se seleccion� el checkbox
				flag = true;
			}
		} else if(Ext.getCmp('tipo_persona1').getValue() == 'F'){
			if(Ext.getCmp('invalidar_modF1').getValue() == false){ //No se seleccion� el checkbox
				if(Ext.getCmp('rfc_modF1').getValue() == ''){
					flag = false;
				} else{
					flag = true;
				}
			} else{ //Si se seleccion� el checkbox
				flag = true;
			}
		}
		Ext.getCmp('actualiza_sirac1').setValue('');
		return flag;
	}

/******************************************************************************
 * 			6. Valida que al actualizar SIRAC, el RFC no est� invalido			*
 ******************************************************************************/	
	function validaActualizaSirac(){
		var flag = false;
		if(Ext.getCmp('tipo_persona1').getValue() == 'M'){
			if(Ext.getCmp('invalidar_mod1').getValue() == true){
				Ext.getCmp('rfc_mod1').markInvalid('No se puede actualizar en SIRAC un registro de N@E invalidado');
				flag = false;
			} else{
				if(Ext.getCmp('rfc_mod1').getValue() != Ext.getCmp('rfc_modF1').getValue()){
					Ext.getCmp('rfc_mod1').markInvalid('EL RFC fue modificado. No se puede actualizar en SIRAC');
					flag = false;
				} else{
					flag = true;
				}
			}

		} else if(Ext.getCmp('tipo_persona1').getValue() == 'F'){
			if(Ext.getCmp('invalidar_modF1').getValue() == true){
				Ext.getCmp('rfc_modF1').markInvalid('No se puede actualizar en SIRAC un registro de N@E invalidado');
				flag = false;
			} else{
				if(Ext.getCmp('rfc_modF1').getValue() != Ext.getCmp('rfc_mod1').getValue() && Ext.getCmp('invalidar_modF1').getValue() == false){
					Ext.getCmp('rfc_modF1').markInvalid('EL RFC fue modificado. No se puede actualizar en SIRAC');
					flag = false;
				} else{
					flag = true;
				}
			}		
		}
		Ext.getCmp('actualiza_sirac1').setValue('S');
		return flag;
	}
	
/******************************************************************************
 * 		7. Ultimas validaciones antes de actualizar NE					*
 ******************************************************************************/
	function actualizaCamposNE(){
		if( !validaVentasTotales()){ // Verifica que las ventas netas totales sean mayor a $5,000.00
			Ext.getCmp('ventas_mod1').markInvalid('Las ventas netas totales deben ser mayor a $5,000.00');
		} else {
			if(!validaRFC()){
				if(Ext.getCmp('tipo_persona1').getValue() == 'M'){
					Ext.getCmp('rfc_mod1').markInvalid('El campo es obligatorio');
				}else{
					Ext.getCmp('rfc_modF1').markInvalid('El campo es obligatorio');
				}
			} else {
				Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
					if (botonConf == 'ok' || botonConf == 'yes') {
						fpModificar.el.mask('Enviando...', 'x-mask-loading');
						actualizaDatos();
					}
				});
			}
		}
	}

/******************************************************************************
 * 		8. Ultimas validaciones antes de actualizar SIRAC							*
 ******************************************************************************/
	function actualizaCamposSirac(){
		if( !validaVentasTotales()){ // Verifica que las ventas netas totales sean mayor a $5,000.00
			Ext.getCmp('ventas_mod1').markInvalid('Las ventas netas totales deben ser mayor a $5,000.00');
		} else {
			if(!validaRFC()){ //Verifica el RFC y el checkbox
				if(Ext.getCmp('tipo_persona1').getValue() == 'M'){
					Ext.getCmp('rfc_mod1').markInvalid('El campo es obligatorio');
				}else{
					Ext.getCmp('rfc_modF1').markInvalid('El campo es obligatorio');
				}
			} else {
				if(!validaActualizaSirac()){
				}else{
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							fpModificar.el.mask('Enviando...', 'x-mask-loading');
							actualizaDatos();
						}
					});
				}
			}
		}
	}	

/******************************************************************************
 * 				9. Manda a actualizar los datos											*
 ******************************************************************************/	
	function actualizaDatos(){
		var rfcTmp, paisOrigenTmp, invalidarTmp, fielTmp, sexoTmp, razSocTmp, val;
		if(Ext.getCmp('sexo_mod1').getValue()){
			var valor =	(Ext.getCmp('sexo_mod1')).getValue();
			sexoTmp = valor.getGroupValue();
		} else if(!Ext.getCmp('sexo_mod1').getValue()){
			sexoTmp = '';
		}
		if(Ext.getCmp('tipo_persona1').getValue() == 'M'){
			paisOrigenTmp = Ext.getCmp('pais_origen_mod1').getValue(); 
			fielTmp = Ext.getCmp('fiel_mod1').getValue(); 
			rfcTmp = Ext.getCmp('rfc_mod1').getValue(); 
			invalidarTmp = Ext.getCmp('invalidar_mod1').getValue(); 
			razSocTmp = Ext.getCmp('razon_social_mod1').getValue(); 
		} else if(Ext.getCmp('tipo_persona1').getValue() == 'F'){
			paisOrigenTmp = Ext.getCmp('pais_origen_modF1').getValue(); 
			fielTmp = Ext.getCmp('fiel_modF1').getValue(); 
			rfcTmp = Ext.getCmp('rfc_modF1').getValue();	
			invalidarTmp = Ext.getCmp('invalidar_modF1').getValue(); 
			razSocTmp = '';
		}
		Ext.Ajax.request({
			url: '15forma05ext_AfCredElectronico.data.jsp',
			params: {
				informacion: "actualizaDatos",
				rfc: rfcTmp,
				invalidar: invalidarTmp,
				fiel: fielTmp,
				pais_origen: paisOrigenTmp,
				ap_paterno: Ext.getCmp('paterno_mod1').getValue(), 
				ap_materno: Ext.getCmp('materno_mod1').getValue(), 
				nombre: Ext.getCmp('nombre_mod1').getValue(), 
				sexo: sexoTmp,
				fecha_nacimiento: Ext.getCmp('fecha_nac_mod1').getValue(),
				curp: Ext.getCmp('curp_mod1').getValue(), 
				razon_social: razSocTmp, 
				calle: Ext.getCmp('calle_mod1').getValue(),
				colonia: Ext.getCmp('colonia_mod1').getValue(), 
				codigo_postal: Ext.getCmp('codigo_postal_mod1').getValue(),
				pais: Ext.getCmp('pais_mod1').getValue(),  
				estado: Ext.getCmp('estado_mod1').getValue(), 
				deleg_municipio: Ext.getCmp('del_mun_mod1').getValue(), 
				ciudad: Ext.getCmp('ciudad_mod1').getValue(), 
				telefono: Ext.getCmp('telefono_mod1').getValue(),
				email: Ext.getCmp('email_mod1').getValue(), 
				ventas_netas_tot: Ext.getCmp('ventas_mod1').getValue(), 
				num_empleados: Ext.getCmp('num_empleados_mod1').getValue(),
				sector_econ: Ext.getCmp('sector_mod1').getValue(), 
				subsector: Ext.getCmp('subsector_mod1').getValue(), 
				rama: Ext.getCmp('rama_mod1').getValue(), 
				clase: Ext.getCmp('clase_mod1').getValue(), 
				num_sirac: Ext.getCmp('sirac_mod1').getValue(), 
				tipo_persona: Ext.getCmp('tipo_persona1').getValue(),
				actualizaSirac: Ext.getCmp('actualiza_sirac1').getValue(), 
				ic_pyme: Ext.getCmp('pyme_hide1').getValue(),
				ic_domicilio: Ext.getCmp('ic_dom1').getValue()
			},
			callback: procesaResultado
		});	
	}

/******************************************************************************
 * 		10. Muestra el resultado de actualizar los datos							*
 ******************************************************************************/
   var procesaResultado = function(store, records, response){
		var jsonData = Ext.util.JSON.decode(response.responseText);
		fpModificar.el.unmask();
		if (jsonData != null){
			if(jsonData.success == true){
				Ext.Msg.alert('',jsonData.msg, function() {
					fpModificar.el.mask('Espere...', 'x-mask-loading');
					window.location = '15forma5ext.jsp';
				}, this);
			} else {
				Ext.Msg.alert('Info...', 'Ocurrio un error durante la actualizaci�n');
			}
		} 
  };
 
/******************************************************************************
 *				11. Proceso y consulta para llenar el cat�logo IF						*
 ******************************************************************************/
  	var procesarCatalogoIF = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var ic_if = Ext.getCmp('ic_if1');
				Ext.getCmp('ic_if1').setValue("");
				if(ic_if.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					ic_if.setValue("");			
				}	
			} else {
			}
		}	else{
		}
  };

	//-------------------- Se crea el store para el combo IF --------------------  
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
			load: procesarCatalogoIF,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});

/******************************************************************************
 *				12. Proceso y consulta para llenar el cat�logo Pais origen			*
 ******************************************************************************/
   	var procesarCatalogoPaisOrigen = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var pais_origen_mod = Ext.getCmp('pais_origen_mod1');
				Ext.getCmp('pais_origen_mod1').setValue("");
				if(pais_origen_mod.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					pais_origen_mod.setValue("");			
				}	
			} 
		}
  };

	//------------- Se crea el store para el combo pais origen ------------------
  	var catalogoPaisOrigen = new Ext.data.JsonStore({
	   id: 'catalogoPais',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: { informacion: 'catalogoPais'},
		autoLoad: false,
		listeners:
		{
			load: procesarCatalogoPaisOrigen,
			exception: NE.util.mostrarDataProxyError
		}
  });
  
/******************************************************************************
 *				13. Proceso y consulta para llenar el cat�logo Pais					*
 ******************************************************************************/
  	var catalogoPais = new Ext.data.JsonStore({
	   id: 'catalogoPais',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: { informacion: 'catalogoPais'},
		autoLoad: false,
		listeners:
		{
			exception: NE.util.mostrarDataProxyError
		}
  });
 
/******************************************************************************
 *				14. Proceso y consulta para llenar el cat�logo Estado					*
 ******************************************************************************/
  	var catalogoEstado = new Ext.data.JsonStore({
		id: 'catalogoEstado',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: { informacion: 'catalogoEstado'	},
		autoLoad: false,
		listeners:
		{
		 load: function(){
			if(Ext.getCmp('estado_mod1').getValue()!=''){
				Ext.getCmp('estado_mod1').setValue(Ext.getCmp('estado_mod1').getValue());
			}
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
/******************************************************************************
 *		15. Proceso y consulta para llenar el cat�logo Deleg/Municipio		      *
 ******************************************************************************/
   var procesarCatalogoMunicipio = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var del_mun_mod = Ext.getCmp('del_mun_mod1');
				Ext.getCmp('del_mun_mod1').setValue("");
				if(del_mun_mod.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					del_mun_mod.setValue("");			
				}	
			} 
		}
  };

 	var catalogoMunicipio = new Ext.data.JsonStore
	({
		id: 'catalogoMunicipio',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: { informacion: 'catalogoMunicipio'},
		autoLoad: false,
		listeners:{
			 load: function(){
				if(Ext.getCmp('del_mun_mod1').getValue()!=''){
					Ext.getCmp('del_mun_mod1').setValue(Ext.getCmp('del_mun_mod1').getValue());
				}
			},
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
 
/******************************************************************************
 *				16. Proceso y consulta para llenar el cat�logo Ciudad					*
 ******************************************************************************/
  	var catalogoCiudad = new Ext.data.JsonStore({
	   id: 'catalogoCiudad',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: { informacion: 'catalogoCiudad'},
		autoLoad: false,
		listeners:
		{
		 load: function(){
			if(Ext.getCmp('ciudad_mod1').getValue()!=''){
				Ext.getCmp('ciudad_mod1').setValue(Ext.getCmp('ciudad_mod1').getValue());
			}
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });  
  
/******************************************************************************
 *		17. Proceso y consulta para llenar el cat�logo Sector economico			*
 ******************************************************************************/
  	var catalogoSector = new Ext.data.JsonStore({
	   id: 'catalogoSector',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: { informacion: 'catalogoSectorEconomico'},
		autoLoad: false,
		listeners:
		{
			exception: NE.util.mostrarDataProxyError
		}
  });  
   
/******************************************************************************
 *		18. Proceso y consulta para llenar el cat�logo Subsector 	      		*
 ******************************************************************************/
  	var catalogoSubSector = new Ext.data.JsonStore({
	   id: 'catalogoSubSector',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: { informacion: 'catalogoSubSector'},
		autoLoad: false,
		listeners:
		{
		 load: function(){
			if(Ext.getCmp('subsector_mod1').getValue()!=''){
				Ext.getCmp('subsector_mod1').setValue(Ext.getCmp('subsector_mod1').getValue());
			}
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });  
     
/******************************************************************************
 *				19. Proceso y consulta para llenar el cat�logo Rama	      		*
 ******************************************************************************/
  	var catalogoRama = new Ext.data.JsonStore({
	   id: 'catalogoRama',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: { informacion: 'catalogoRama'},
		autoLoad: false,
		listeners:
		{
		 load: function(){
			if(Ext.getCmp('rama_mod1').getValue()!=''){
				Ext.getCmp('rama_mod1').setValue(Ext.getCmp('rama_mod1').getValue());
			}
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });  
      
/******************************************************************************
 *				20. Proceso y consulta para llenar el cat�logo Clase	      		*
 ******************************************************************************/
  	var catalogoClase = new Ext.data.JsonStore({
	   id: 'catalogoClase',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: { informacion: 'catalogoClase'},
		autoLoad: false,
		listeners:
		{
		 load: function(){
			if(Ext.getCmp('clase_mod1').getValue()!=''){
				Ext.getCmp('clase_mod1').setValue(Ext.getCmp('clase_mod1').getValue());
			}
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });   
/******************************************************************************
 *							21. C�digo para generar el Grid									*
 ******************************************************************************/	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();				
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {		
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0) {
				gridConsulta.show();			
				el.unmask(); 
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarCSV').enable();			
			} else {		
				gridConsulta.show();
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarCSV').disable();				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}	
	
	//---------- Se crea el store del grid ----------
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15forma05ext_AfCredElectronico.data.jsp',
		baseParams: {
			informacion: 'consultar'		
		},
		fields: [
			{	name: 'NUM_CLIENTE'},
			{	name: 'RAZON_SOCIAL'},
			{	name: 'RFC'},
			{	name: 'TIPO_PERSONA'},
			{	name: 'SECTOR'},
			{	name: 'SUBSECTOR'},
			{	name: 'RAMA'},
			{	name: 'CLASE'},
			{	name: 'ESTRATO'},
			{	name: 'NAFIN_ELECTRONICO'},
			{	name: 'IC_PYME'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			/**beforeLoad:	{fn: function(store, options){
				Ext.apply(options.params, {
					tipo_credito: Ext.getCmp("tipo_cred_respaldo1").getValue()
				});
			}},*/
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});
	
	//---------- Se crea el grid ----------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Resultados',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'No.cliente SIRAC',
				tooltip: 'N�mero de cliente SIRAC',
				dataIndex: 'NUM_CLIENTE',
				sortable: true,		
				resizable: true,					
				align: 'center'
			},{
				header: 'Nombre o raz�n Social',
				tooltip: 'Nombre completo o raz�n social',
				dataIndex: 'RAZON_SOCIAL',
				sortable: true,		
				resizable: true,					
				align: 'left'				
			},{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,		
				resizable: true,					
				align: 'center'
			},{
				header: 'Tipo persona',
				tooltip: 'Tipo de persona',
				dataIndex: 'TIPO_PERSONA',
				sortable: true,		
				resizable: true,					
				align: 'center'
			},{
				header: 'Sector econ�mico',
				tooltip: 'Sector econ�mico',
				dataIndex: 'SECTOR',
				sortable: true,		
				resizable: true,					
				align: 'center'
			},{
				header: 'Subsector',
				tooltip: 'Subsector',
				dataIndex: 'SUBSECTOR',
				sortable: true,			
				resizable: true,					
				align: 'center'
			},{
				header: 'Rama',
				tooltip: 'Rama',
				dataIndex: 'RAMA',
				sortable: true,			
				resizable: true,					
				align: 'left'			
			},{
				header: 'Clase',
				tooltip: 'Clase',
				dataIndex: 'CLASE',
				sortable: true,			
				resizable: true,					
				align: 'left'			
			},{
				header: 'Estrato',
				tooltip: 'Estrato',
				dataIndex: 'ESTRATO',
				sortable: true,			
				resizable: true,					
				align: 'center'			
			},{
				header: 'Nafin electr�nico',
				tooltip: 'Nafin electr�nico',
				dataIndex: 'NAFIN_ELECTRONICO',
				sortable: true,		
				resizable: true,					
				align: 'center'			
			},{
				header: 'pyme',
				dataIndex: 'IC_PYME',
				sortable: false,		
				resizable: false,	
				hidden: true,
				align: 'center'			
			},{
				xtype: 'actioncolumn',
				header: 'Seleccionar',
				sortable: false,		
				resizable: false,					
				align: 'center',
            items: [{		
					iconCls: 'icoAceptar',
               tooltip: 'Link para modificar datos',
               handler: function(gridConsulta, rowIndex, colIndex) {
						var rec = consultaData.getAt(rowIndex); 
						procesarModificar(rec);
               }
            }]
         }
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 420,
		width: 800,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo ',
					iconCls: 'icoXls',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						var barraPaginacionA = Ext.getCmp("barraPaginacion");	
						Ext.Ajax.request({
							url: '15forma05ext_AfCredElectronico.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'generarArchivo'			
							}),							
							callback: descargaArchivo
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir',
					handler: function(boton, evento) {
						var barraPaginacionA = Ext.getCmp("barraPaginacion");
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15forma05ext_AfCredElectronico.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'imprimir',
								start: barraPaginacionA.cursor,
								limit: barraPaginacionA.pageSize			
							}),							
							callback: descargaArchivo
						});
					}
				}				
			]
		}
	});		

/******************************************************************************
 *					22. Proceso para mostrar los datos a modificar						*
 ******************************************************************************/
	var procesaModificacion = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			valorFormulario = Ext.util.JSON.decode(response.responseText).registro;
			fpModificar.setVisible(true);
			fp.setVisible(false);
			gridConsulta.setVisible(false);
			fpModificar.getForm().setValues(valorFormulario);
			if(Ext.getCmp('tipo_persona1').getValue() == 'M'){
					Ext.getCmp('nombre_persona').hide();
					Ext.getCmp('nombre_persona1').hide();
					Ext.getCmp('nombre_persona2').hide();
					Ext.getCmp('nombre_persona3').hide();
					Ext.getCmp('nombre_persona4').hide();
					Ext.getCmp('nombre_persona5').hide();
					Ext.getCmp('nombre_empresa').show();
					Ext.getCmp('nombre_empresa1').show();	
					Ext.getCmp('nombre_empresa2').show();
					Ext.getCmp('formaMod').setTitle('Actualizaci�n de afiliados a cr�dito electr�nico. Persona Moral');
			} else if(Ext.getCmp('tipo_persona1').getValue() == 'F'){
					Ext.getCmp('nombre_persona').show();
					Ext.getCmp('nombre_persona1').show();
					Ext.getCmp('nombre_persona2').show();
					Ext.getCmp('nombre_persona3').show();
					Ext.getCmp('nombre_persona4').show();
					Ext.getCmp('nombre_persona5').show();
					Ext.getCmp('nombre_empresa').hide();
					Ext.getCmp('nombre_empresa1').hide();		
					Ext.getCmp('nombre_empresa2').hide();
					Ext.getCmp('formaMod').setTitle('Actualizaci�n de afiliados a cr�dito electr�nico. Persona F�sica');
					Ext.getCmp('fecha_nac_hide').setValue(Ext.getCmp('fecha_nac_mod1').getValue());
			}
			//----- Inicializo los combos -----
			catalogoEstado.load({
			 params : Ext.apply({
				pais : Ext.getCmp('pais_mod1').getValue()
				})
			});
			catalogoMunicipio.load({
				params: Ext.apply({
					pais : Ext.getCmp('pais_mod1').getValue(),
					estado : (Ext.getCmp('estado_mod1')).getValue()
				})
			});
			catalogoCiudad.load({
				params: Ext.apply({
					pais : Ext.getCmp('pais_mod1').getValue(),
					estado : (Ext.getCmp('estado_mod1')).getValue()
				})
			});
			catalogoSubSector.load({
				params: Ext.apply({
					sector_econ : Ext.getCmp('sector_mod1').getValue()
				})
			});
			catalogoRama.load({
				params: Ext.apply({
					sector_econ : Ext.getCmp('sector_mod1').getValue(),
					subsector : Ext.getCmp('subsector_mod1').getValue()
				})
			});
			catalogoClase.load({
				params: Ext.apply({
					sector_econ : Ext.getCmp('sector_mod1').getValue(),
					subsector : Ext.getCmp('subsector_mod1').getValue(),
					rama : Ext.getCmp('rama_mod1').getValue()
				})
			});
		}else{
				Ext.Msg.alert('Error...','Error al obtener los datos' );
		}
	}	
		
	function procesarModificar(rec) {
		var ic_pyme = rec.get('IC_PYME');
		Ext.Ajax.request({
			url: '15forma05ext_AfCredElectronico.data.jsp',
			params: Ext.apply({informacion:'consultaModificar', ic_pyme: rec.get('IC_PYME') }),
			callback: procesaModificacion
		});
	}
	
/******************************************************************************
 *							23. Elementos de la forma principal							   *
 ******************************************************************************/
	var elementosForma = [
	{
			xtype				: 'combo',
			id					: 'cmbTipoAfiliado1',
			name				: 'cmbTipoAfiliado',
			hiddenName 		: 'cmbTipoAfiliado', 
			fieldLabel		: 'Tipo de Afiliado',
			width				: 250,
			forceSelection	: true,
			triggerAction	: 'all',
			mode		: 'local',
			valueField	: 'clave',
			displayField	: 'afiliacion',
			value		: '11',	
			store		: storeAfiliacion,
			tabIndex	: 1,
			listeners: {
				select: {
					fn: function(combo) {	
						var valor = Ext.getCmp('cmbTipoAfiliado1').getValue();						
						creditoElec(valor);		///funcion para redireccionar mediante un window.location valor del combo				
					}
				}
			}
		},
		{	xtype: 		'numberfield', 
			width: 		400,	
			fieldLabel: '&nbsp;&nbsp; No. de cliente SIRAC',			
			name: 		'no_cliente_sirac',	
			id: 			'no_cliente_sirac1',
			maxLength: 	12					
		},{xtype: 		'textfield', 	
			width: 		400,	
			fieldLabel: '&nbsp;&nbsp; RFC',			
			name: 		'rfc',	
			id: 			'rfc1',
			regex: 		/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
			regexText:	'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
							'en el formato NNN-AAMMDD-XXX donde:<br>'+
							'NNN:son las iniciales del nombre de la empresa<br>'+
							'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
							'XXX:es la homoclave'	
		},{xtype: 		'textfield', 	
			width:		 400,	
			fieldLabel: '&nbsp;&nbsp; Nombre o raz�n social',		
			name: 		'razon_social',
			id: 			'razon_social1',
			maxLength: 	100
		},{
			xtype: 			'compositefield',
			fieldLabel: 	'&nbsp;&nbsp; Fecha de alta del cliente',
			msgTarget: 		'side',
			combineErrors: false,
			items: [
				{
					width: 			180 ,
					xtype: 			'datefield',
					name: 			'fecha_alta_de',
					id: 				'fecha_alta_de',
					hiddenName: 	'fecha_alta_de',	
					msgTarget: 		'side',
					vtype: 			'rangofecha', 
					minValue: 		'01/01/1901',
					campoFinFecha: 'fecha_alta_a',
					margins: 		'0 20 0 0',
					allowBlank: 	true,
					startDay: 		0
					 
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					width: 				180,
					xtype: 				'datefield',
					name: 				'fecha_alta_a',
					id: 					'fecha_alta_a',
					hiddenName: 		'fecha_alta_a',
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					minValue: 			'01/01/1901',
					campoInicioFecha: 'fecha_alta_de',
					margins: 			'0 20 0 0',
					allowBlank: 		true,
					startDay: 			1
				}
			]
		},{
			width: 			400,	
			xtype: 			'combo',	
			name: 			'ic_if',	
			id: 				'ic_if1',	
			fieldLabel: 	'&nbsp;&nbsp; IF', 
			mode: 			'local',	
			hiddenName: 	'ic_if',	
			emptyText: 		'Seleccione ... ',	
			triggerAction: 'all',		
			displayField: 	'descripcion',	
			valueField: 	'clave',	
			forceSelection: true,
			typeAhead: 		true,
			minChars: 		1,
			store: 			catalogoIF,
			listeners: 		{
				select: 		{
					fn: 		function(combo) {}
				}
			}	
		},{
         xtype: 		 'fieldset',
         defaultType: 'checkbox', 
			border: 		 false,
         items: 		 [
				{
					fieldLabel: 'Modificado por propietario',
               name: 		'modif_propietario',
					id: 			'modif_propietario1'
            }							
			]															
		}
	];

/******************************************************************************
 *								25. Form Panel	principal   									*
 ******************************************************************************/
	var fp = new Ext.form.FormPanel({
		id:           'forma',
		title:        'Afiliados a cr�dito electr�nico',				
		layout:       'form',
		style:        'margin:0 auto;',
		width:        600,	
		labelWidth:   155,
		frame:        true,
		autoHeight:   true,
		monitorValid: true,		
      defaults:     {   
			//xtype: 		'textfield', 
			msgTarget: 	'side'
		},
		items: 			elementosForma,
		buttons: 		[
			{
				text:    'Buscar',
				id:      'btnBuscar',				
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					if(validaCamposVacios()){
					Ext.Msg.alert('Error...', 'Debe de poner al menos un criterio de b�squeda.');
					} else {
						if( Ext.getCmp('forma').getForm().isValid() ){ // 	Este if solo sirve para validar el formato de las fechas
							// --> Se valida que ambas fechas esten vacias o llenas.
							if( (Ext.getCmp('fecha_alta_de').getValue() == '' && Ext.getCmp('fecha_alta_a').getValue() == '') ||
								(Ext.getCmp('fecha_alta_de').getValue() != '' && Ext.getCmp('fecha_alta_a').getValue() != '') ) {
								
								Ext.getCmp("gridConsulta").hide();
								Ext.getCmp('formaMod').hide();
								fp.el.mask('Enviando...', 'x-mask-loading');			
								consultaData.load({
									params: Ext.apply(fp.getForm().getValues(),{
										operacion: 'generar',
										informacion: 'consultar',
										start:0,
										limit:15
									})
								});	
								
							} else {
								Ext.Msg.alert('Error...', 'Debe seleccionar ambas fechas.');
							}
						}
					}
				}
			}		
		]
	});
	
/******************************************************************************
 *							26. Form para actualizar datos									*
 ******************************************************************************/
   var fpModificar = new Ext.FormPanel({
		width:      				'90%',
		labelWidth: 				120,
		frame:     					true,
		hidden:     				true,
		autoHeight: 				true,
		id:         				'formaMod',
		style:      				'margin: 0 auto;',
		labelAlign: 				'right',
      title:      				'&nbsp;',
      bodyStyle:  				'padding: 5px 5px 0',
      items: [{					
			xtype:					'fieldset',
			title: 					'Nombre Completo',
			id: 						'nombre_persona',   
			collapsible: 			false,
			autoHeight:				true,
			items :[{
				layout:           'column',
				id:               'nombre_persona1', 
				items:[{						
					columnWidth:   .5,
					layout:        'form',
					items: [{		
						xtype:      'textfield',
						fieldLabel: '*Apellido paterno',
						id:         'paterno_mod1',	
						name:       'Apellido paterno',
						msgTarget:  'side',
						anchor:     '95%',
						maxLength:  100
					}]					
				},{					
					columnWidth:   .5,
					layout:        'form',
					items: [{		
						xtype:      'textfield',
						fieldLabel: '*Apellido materno',
						id:         'materno_mod1',	
						name:       'Apellido materno',
						msgTarget:  'side',
						anchor:     '95%',
						maxLength:  100
					}]					
				}]						
			},{						
				layout:           'column',
				id:               'nombre_persona2',
				items:[{				
					columnWidth:   .5,
					layout:        'form',
					items: [{		
						xtype:      'textfield',
						fieldLabel: '*Nombre(s)',
						id:         'nombre_mod1',	
						name:       'Nombre',
						msgTarget:  'side',
						anchor:     '95%',
						maxLength:  100
					}]					
				},{					
					columnWidth:   		.5,
					layout:        		'form',
					items: [{			
						xtype: 				'compositefield',
						msgTarget: 			'side',
						combineErrors: 	false,
						items: [				
							{					
								width: 		160,
								xtype:      'textfield',
								fieldLabel: '*RFC',
								id:         'rfc_modF1',	
								name:       'RFC.',
								msgTarget:  'side',
								anchor:     '95%',
								maxLength:  20,
								regex:      /^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
								regexText:  'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
												'en el formato NNN-AAMMDD-XXX donde:<br>'+
												'NNN:son las iniciales del nombre de la empresa<br>'+
												'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
												'XXX:es la homoclave'
							},{	
								width: 		10,	
								frame: 		false,	
								border: 		false
							},{
								width: 		90,
								xtype: 		'checkbox',	
								name: 		'invalidar_modF',
								id: 			'invalidar_modF1',
								boxLabel: 	'Invalidar',
								listeners: 	{
									check: function(){
										Ext.getCmp('fc_modF1').setValue('XXXX-000101-XXX');
										Ext.getCmp('actualiza_sirac1').setValue('');
									}
								}
							}
						]
					}]
				}]
			},{
				layout:           'column',
				id: 					'nombre_persona3',
				items:[{						
					columnWidth:   .5,
					layout:        'form',
					items: [{		
						xtype:      'radiogroup',
						fieldLabel: '*Sexo',
						id:         'sexo_mod1',	
						name:       'Sexo',
						msgTarget:  'side',
						anchor:     '80%',	
						align:      'center',		
						cls:        'x-check-group-alt',	
						style:      { width: '25%'},
						items: [  
							{	boxLabel	: 'F',	name : 'Sexo', inputValue: 'F' },
							{	boxLabel	: 'M',	name : 'Sexo', inputValue: 'M' }
						]
					}]					
				},{					
					columnWidth:       .5,
					layout:            'form',
					items: [{			 	
						xtype:          'textfield',
						fieldLabel:     '*Fecha de nacimiento',
						id:             'fecha_nac_mod1',	
						name:           'Fecha de nacimiento',
						msgTarget:      'side',
	               anchor:         '95%'
					}]					    
	         }]							 
			},{							 
				layout:               'column',
				id: 						 'nombre_persona4', 
	         items:[{						
					columnWidth:       .5,
					layout:            'form',
					items: [{		    
						xtype:          'textfield',
						fieldLabel:     'CURP',
						id:             'curp_mod1',	
						name:           'curp_mod',
						msgTarget:      'side',
						anchor:         '95%',
						minLength:      18,
						maxLength:      18
	            }]						 
				},{					 	 
					columnWidth:       .5,
					layout:            'form',
					items: [{		    
						xtype:          'textfield',
						fieldLabel:     'FIEL',
						id:             'fiel_modF1',	
						name:           'fiel_modF',
						msgTarget:      'side',
						anchor:         '95%',
						maxLength:  	 25
					}]						 
				}]							 
			},{							 
				layout:               'column',
				id: 						 'nombre_persona5',
				items:[{					 	
					columnWidth:       .5,
					layout:            'form',
					items: [{			 		
						xtype:          'combo',
						fieldLabel:     'Pa�s de origen',
						id:             'pais_origen_modF1',	
						name:           'pais_origen_modF',
						msgTarget:      'side',
						anchor:         '95%',
						triggerAction:  'all',
						mode:           'local',
						valueField:     'clave',
						displayField:   'descripcion',
						forceSelection: true,
						store:          catalogoPaisOrigen
					}]						 
				},{						 
					columnWidth:   	 .5,
					layout:            'form',
					items: [{			 	
						xtype:          'textfield',
						fieldLabel:     '*',
						id:             'fecha_nac_hide',	
						name:           'Fecha de nacimiento1',
						msgTarget:      'side',
						anchor:         '95%',
						hidden:         true
					}]					
				}]						
			}]	
		},{
			xtype:                   'fieldset',
			title:                   'Nombre de la empresa',
			id: 						    'nombre_empresa',
			collapsible:             false,
			autoHeight:              true,
			items :[{				
				layout:               'column',
				id: 						 'nombre_empresa1',
				items:[{						
					columnWidth:       .5,
					layout:            'form',
					items: [{		
						xtype:          'textfield',
						fieldLabel:     '*Raz�n social',
						id:             'razon_social_mod1',	
						name:           'Razon social',
						msgTarget:      'side',
						anchor:         '95%',
						maxLength:      100
					}]					
				},{					
					columnWidth:   		.5,
					layout:        		'form',
					items: [{			
						xtype: 				'compositefield',
						msgTarget: 			'side',
						combineErrors: 	false,
						items: [				
							{					
								width: 		160,
								xtype:      'textfield',
								fieldLabel: '*RFC',
								id:         'rfc_mod1',	
								name:       'RFC',
								msgTarget:  'side',
								anchor:     '95%',
								maxLength:  20,
								regex:      /^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
								regexText:  'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
												'en el formato NNN-AAMMDD-XXX donde:<br>'+
												'NNN:son las iniciales del nombre de la empresa<br>'+
												'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
												'XXX:es la homoclave'
							},{	
								width: 		10,	
								frame: 		false,	
								border: 		false
							},{
								width: 		90,
								xtype: 		'checkbox',	
								name: 		'invalidar_mod',
								id: 			'invalidar_mod1',
								boxLabel: 	'Invalidar',
								listeners: 	{
									check: function(){
										Ext.getCmp('rfc_mod1').setValue('XXXX-000101-XXX');
										Ext.getCmp('actualiza_sirac1').setValue('');
									}
								}
							}
						]
					}]
				}]						
			},{						
				layout:               'column',
				id:                   'nombre_empresa2',
				items:[{					 
					columnWidth:       .5,
					layout:            'form',
					items: [{		
						xtype:          'textfield',
						fieldLabel:     'FIEL',
						id:             'fiel_mod1',	
						name:           'fiel_mod',
						msgTarget:      'side',
						anchor:         '95%',
						maxLength:      25
					}]					
				},{					
					columnWidth:       .5,
					layout:            'form',
					items: [{		
						xtype:      	 'combo',
						fieldLabel: 	 'Pa�s de origen',
						id:         	 'pais_origen_mod1',	
						name:       	 'pais_origen_mod',
					   msgTarget:      'side',
						anchor:         '95%',
						triggerAction:  'all',
						mode:           'local',
						valueField:     'clave',
						displayField:   'descripcion',
						forceSelection: true,
						store:          catalogoPaisOrigen
					}]
				}]
			}]	
		},{
			xtype:                   'fieldset',
			title:                   'Domicilio',
			id: 						    'domicilio',
			collapsible:             false,
			autoHeight:              true,
			items :[{                
				layout:               'column',
				id:                   'domicilio1', 
				items:[{					 	
					columnWidth:       .5,
					layout:            'form',
					items: [{		    
						xtype:          'textfield',
						fieldLabel:     '*Calle no. ext y no. int',
						id:             'calle_mod1',	
						name:           'Direccion',
						msgTarget:      'side',
						anchor:         '95%',
						maxLength:      100
					}]					    
				},{					    
					columnWidth:       .5,
					layout:            'form',
					items: [{			 
						xtype:          'textfield',
						fieldLabel:     '*Colonia',
						id:             'colonia_mod1',	
						name:           'Colonia',
						msgTarget:      'side',
						anchor:         '95%',
						maxLength:      100
					}]						 
				}]							 
			},{							 
				layout:               'column',
				id: 						 'domicilio2',
				items:[{				    
					columnWidth:       .5,
					layout:            'form',
					items: [{		    
						xtype:          'textfield',
						fieldLabel:     '*C�digo postal',
						id:             'codigo_postal_mod1',	
						name:           'Codigo Postal',
						msgTarget:      'side',
						anchor:         '95%',
						maxLength:      5,
						minLength:      5,
						regex:          /^[0-9]*$/,
						regexText:      'Solo de contener n�meros'
					}]						 
				},{						 				
					columnWidth:   	 .5,
					layout:        	 'form',
					items: [{		
						xtype:      	 'combo',
						fieldLabel: 	 '*Pa�s',
						id:         	 'pais_mod1',	
						name:       	 'Pais',
						msgTarget:      'side',
						anchor:         '95%',
						triggerAction:  'all',
						mode:           'local',
						valueField:     'clave',
						displayField:   'descripcion',
						forceSelection: true,
						store:          catalogoPais,
						listeners:      {
							select: function(combo, record, index) {
								Ext.getCmp('estado_mod1').reset();
								Ext.getCmp('del_mun_mod1').reset();
								catalogoEstado.load({
									params: Ext.apply({
										pais: combo.getValue()
									})			
								});			
							}				
						}					
					}]						
				}]							
			},{							
				layout:              'column',
				id: 						'domicilio3',  
				items:[{						
					columnWidth:      .5,
					layout:           'form',
					items: [{		
						xtype:      	 'combo',
						fieldLabel: 	 '*Estado',
						id:         	 'estado_mod1',	
						name:       	 'Estado',
						msgTarget:      'side',
						anchor:         '95%',
						triggerAction:  'all',
						mode:           'local',
						valueField:     'clave',
						displayField:   'descripcion',
						forceSelection: true,
						store:          catalogoEstado,
						listeners:      {
							select: function(combo, record, index) {
								Ext.getCmp('del_mun_mod1').reset();
								catalogoMunicipio.load({
									params: Ext.apply({
										estado : record.json.clave,
										pais : Ext.getCmp('pais_mod1').getValue()
									})
								});
								catalogoCiudad.load({
									params: Ext.apply({
										estado : record.json.clave,
										pais : Ext.getCmp('pais_mod1').getValue()
									})
								});
							}
						}					
					}]					
				},{					
					columnWidth:       .5,
					layout:            'form',
					items: [{		
						xtype:      	 'combo',
						fieldLabel: 	 '*Deleg./ Municipio',
						id:         	 'del_mun_mod1',	
						name:       	 'Delegacion Municipio',
						msgTarget:      'side',
						anchor:         '95%',
						triggerAction:  'all',
						mode:           'local',
						valueField:     'clave',
						displayField:   'descripcion',
						forceSelection: true,
						store:          catalogoMunicipio					
					}]					    
	         }]							 
			},{							 
				layout:               'column',
				id: 						 'domicilio4',
	         items:[{						
					columnWidth:       .5,
					layout:            'form',
					items: [{		
						xtype:      	 'combo',
						fieldLabel: 	 'Ciudad',
						id:         	 'ciudad_mod1',	
						name:       	 'ciudad_mod',
						msgTarget:      'side',
						anchor:         '95%',
						triggerAction:  'all',
						mode:           'local',
						valueField:     'clave',
						displayField:   'descripcion',
						forceSelection: true,
						store:          catalogoCiudad					
					}]						 
				},{					 	 
					columnWidth:       .5,
					layout:            'form',
					items: [{		    
						xtype:          'textfield',
						fieldLabel:     '*Tel�fono',
						id:             'telefono_mod1',	
						name:           'Telefono',
						msgTarget:      'side',
						anchor:         '95%',
						maxLength:      30,
						regex:          /^[0-9]*$/,
						regexText:      'Solo de contener n�meros'
					}]						 
				}]							 
			},{							 
				layout:               'column',
				id:   					 'domicilio5', 
				items:[{					 	
					columnWidth:       .5,
					layout:            'form',
					items: [{			 		
						xtype:          'textfield',
						fieldLabel:     '*e-mail',
						id:             'email_mod1',	
						name:           'e-mail',
						msgTarget:      'side',
						anchor:         '95%'
					}]						 
				}]						
			}]	
		},{
			xtype:                   'fieldset',
			title:                   'Diversos',
			id: 						    'diversos',
			collapsible:             false,
			autoHeight:              true,
			items: [{				
				layout:               'column',
				id: 						 'diversos1',
				items:[{						
					columnWidth:       .5,
					layout:            'form',
					items: [{		
						xtype:          'textfield',
						fieldLabel:     '*Ventas netas totales',
						id:             'ventas_mod1',	
						name:           'Ventas totales',
						msgTarget:      'side',
						anchor:         '95%',
						maxLength:      21,
						renderer:       Ext.util.Format.numberRenderer('0.00'),
						regexText:      'Solo de contener n�meros'
					}]					
				},{					
					columnWidth:       .5,
					layout:            'form',
					items: [{		
						xtype:          'textfield',
						fieldLabel:     '*No. de empleados',
						id:             'num_empleados_mod1',	
						name:           'Numero de empleados',
						msgTarget:      'side',
						anchor:         '95%',
						maxLength: 		 6,
						regex:          /^[0-9]*$/,
						regexText:      'Solo de contener n�meros'
					}]					
				}]						
			},{		
			   width:          660,	
				xtype:          'combo',
				fieldLabel:     '*Sector econ�mico',
				id:             'sector_mod1',	
				name:           'Sector economico',
				msgTarget:      'side',
				triggerAction:  'all',
				mode:           'local',
				valueField:     'clave',
				displayField:   'descripcion',
				forceSelection: true,
				store:          catalogoSector,
				listeners:      {
					select: function(combo, record, index) {
						Ext.getCmp('subsector_mod1').reset();
						Ext.getCmp('rama_mod1').reset();
						Ext.getCmp('clase_mod1').reset();
						catalogoSubSector.load({
							params: Ext.apply({
								sector_econ: combo.getValue()
							})
						});	
					}
				}
			},{		
			   width:          660,	
				xtype:          'combo',
				fieldLabel:     '*Subsector',
				id:             'subsector_mod1',	
				name:           'Subsector',
				msgTarget:      'side',
				triggerAction:  'all',
				mode:           'local',
				valueField:     'clave',
				displayField:   'descripcion',
				forceSelection: true,
				store:          catalogoSubSector,
				listeners:      {
					select: function(combo, record, index) {
						Ext.getCmp('rama_mod1').reset();
						Ext.getCmp('clase_mod1').reset();
						catalogoRama.load({
							params: Ext.apply({
								sector_econ: Ext.getCmp('sector_mod1').getValue(),
								subsector: combo.getValue()
							})
						});	
					}
				}
			},{		
			   width:          660,	
				xtype:          'combo',
				fieldLabel:     '*Rama',
				id:             'rama_mod1',	
				name:           'Rama',
				msgTarget:      'side',
				triggerAction:  'all',
				mode:           'local',
				valueField:     'clave',
				displayField:   'descripcion',
				forceSelection: true,
				store:          catalogoRama,
				listeners:      {
					select: function(combo, record, index) {
						Ext.getCmp('clase_mod1').reset();
						catalogoRama.load({
							params: Ext.apply({
								sector_econ: Ext.getCmp('sector_mod1').getValue(),
								subsector: Ext.getCmp('subsector_mod1').getValue(),
								rama: combo.getValue()
							})
						});	
					}
				}
			},{		
			   width:          660,	
				xtype:          'combo',
				fieldLabel:     '*Clase',
				id:             'clase_mod1',	
				name:           'Clase',
				msgTarget:      'side',
				triggerAction:  'all',
				mode:           'local',
				valueField:     'clave',
				displayField:   'descripcion',
				forceSelection: true,
				store:          catalogoClase
			},{		
			   width:          255,	
				xtype:          'textfield',
				fieldLabel:     'N�mero SIRAC',
				id:             'sirac_mod1',	
				name:           'SIRAC',
				msgTarget:      'side',
				maxLength:      12,
				regex:          /^[0-9]*$/,
				regexText:      'Solo debe contener n�meros'
				
			},{		
			   width:          280,	
				xtype:          'textfield',
				fieldLabel:     'Tipo de persona',
				id:             'tipo_persona1',	
				name:           'tipo_persona',	
				hidden:         true
				
			},{		
			   width:          280,	
				xtype:          'textfield',
				fieldLabel:     'Actualiza SIRAC',
				id:             'actualiza_sirac1',	
				name:           'actualiza_sirac',	
				hidden:         true
				
			},{		
			   width:          280,	
				xtype:          'textfield',
				fieldLabel:     'Pyme',
				id:             'pyme_hide1',	
				name:           'pyme_hide',	
				hidden:         true
				
			},{		
			   width:          280,	
				xtype:          'textfield',
				fieldLabel:     'Ic Domicilio',
				id:             'ic_dom1',	
				name:           'IC_DOMICILIO_EPO',	
				hidden:         true
				
			}]	
		}],
		buttons: [
			{
				text:    'Actualizar N@E',
				id:      'btnActualizaNe',				
				iconCls: 'icoActualizar',
				handler: function(boton, evento) {
					if( Ext.getCmp('formaMod').getForm().isValid() ){
						if(Ext.getCmp('tipo_persona1').getValue() == 'F' && Ext.getCmp('paterno_mod1').getValue() == ''){
							Ext.getCmp('paterno_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('tipo_persona1').getValue() == 'F' && Ext.getCmp('materno_mod1').getValue() == ''){
							Ext.getCmp('materno_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('tipo_persona1').getValue() == 'F' && Ext.getCmp('nombre_mod1').getValue() == ''){
							Ext.getCmp('nombre_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('tipo_persona1').getValue() == 'F' && Ext.getCmp('sexo_mod1').getValue() == ''){
							Ext.getCmp('sexo_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('tipo_persona1').getValue() == 'F' && Ext.getCmp('fecha_nac_mod1').getValue() == ''){
							Ext.getCmp('fecha_nac_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('tipo_persona1').getValue() == 'M' && Ext.getCmp('razon_social_mod1').getValue() == ''){
							Ext.getCmp('razon_social_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('calle_mod1').getValue() == ''){
							Ext.getCmp('calle_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('colonia_mod1').getValue() == ''){
							Ext.getCmp('colonia_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('codigo_postal_mod1').getValue() == ''){
							Ext.getCmp('codigo_postal_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('pais_mod1').getValue() == ''){
							Ext.getCmp('pais_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('estado_mod1').getValue() == ''){
							Ext.getCmp('estado_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('del_mun_mod1').getValue() == ''){
							Ext.getCmp('del_mun_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('telefono_mod1').getValue() == ''){
							Ext.getCmp('telefono_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('ventas_mod1').getValue() == ''){
							Ext.getCmp('ventas_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('num_empleados_mod1').getValue() == ''){
							Ext.getCmp('num_empleados_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('sector_mod1').getValue() == ''){
							Ext.getCmp('sector_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('subsector_mod1').getValue() == ''){
							Ext.getCmp('subsector_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('rama_mod1').getValue() == ''){
							Ext.getCmp('rama_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('clase_mod1').getValue() == ''){
							Ext.getCmp('clase_mod1').markInvalid('El campo es obligatorio');
						} else if (!/^([0-9])*$/.test(Ext.getCmp('codigo_postal_mod1').getValue())) {
							Ext.getCmp('codigo_postal_mod1').markInvalid('S�lo debe contener n�meros');
						} else if (!/^([0-9])*$/.test(Ext.getCmp('telefono_mod1').getValue())) {
							Ext.getCmp('telefono_mod1').markInvalid('S�lo debe contener n�meros');
						} else if (!/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('ventas_mod1').getValue())) {
							Ext.getCmp('ventas_mod1').markInvalid('S�lo debe contener n�meros');
						} else if (!/^([0-9])*$/.test(Ext.getCmp('num_empleados_mod1').getValue())) {
							Ext.getCmp('num_empleados_mod1').markInvalid('S�lo debe contener n�meros');
						} else if (!/^([0-9])*$/.test(Ext.getCmp('sirac_mod1').getValue())) {
							Ext.getCmp('sirac_mod1').markInvalid('S�lo debe contener n�meros');
						} else if (!(/^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z]{2,3})$/.exec(Ext.getCmp('email_mod1').getValue()))) {
							Ext.getCmp('email_mod1').markInvalid('Este campo debe ser una direcci�n de correo electr�nico');
						} else {
							if (Ext.getCmp('fecha_nac_mod1').getValue() != Ext.getCmp('fecha_nac_hide').getValue()) {
								Ext.getCmp('fecha_nac_mod1').setValue(Ext.getCmp('fecha_nac_hide').getValue());
							}
							actualizaCamposNE();
						}	
					}
				}//handler
			},{
				text:    'Actualizar N@E/SIRAC',
				id:      'btnActualizaSirac',				
				iconCls: 'icoActualizar',
				handler: function(boton, evento) {
					
					if( Ext.getCmp('formaMod').getForm().isValid() ){
						if(Ext.getCmp('tipo_persona1').getValue() == 'F' && Ext.getCmp('paterno_mod1').getValue() == ''){
							Ext.getCmp('paterno_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('tipo_persona1').getValue() == 'F' && Ext.getCmp('materno_mod1').getValue() == ''){
							Ext.getCmp('materno_mod11').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('tipo_persona1').getValue() == 'F' && Ext.getCmp('nombre_mod1').getValue() == ''){
							Ext.getCmp('nombre_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('tipo_persona1').getValue() == 'F' && Ext.getCmp('sexo_mod1').getValue() == ''){
							Ext.getCmp('sexo_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('tipo_persona1').getValue() == 'F' && Ext.getCmp('fecha_nac_mod1').getValue() == ''){
							Ext.getCmp('fecha_nac_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('tipo_persona1').getValue() == 'M' && Ext.getCmp('razon_social_mod1').getValue() == ''){
							Ext.getCmp('razon_social_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('calle_mod1').getValue() == ''){
							Ext.getCmp('calle_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('colonia_mod1').getValue() == ''){
							Ext.getCmp('colonia_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('codigo_postal_mod1').getValue() == ''){
							Ext.getCmp('codigo_postal_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('pais_mod1').getValue() == ''){
							Ext.getCmp('pais_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('estado_mod1').getValue() == ''){
							Ext.getCmp('estado_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('del_mun_mod1').getValue() == ''){
							Ext.getCmp('del_mun_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('telefono_mod1').getValue() == ''){
							Ext.getCmp('telefono_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('ventas_mod1').getValue() == ''){
							Ext.getCmp('ventas_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('num_empleados_mod1').getValue() == ''){
							Ext.getCmp('num_empleados_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('sector_mod1').getValue() == ''){
							Ext.getCmp('sector_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('subsector_mod1').getValue() == ''){
							Ext.getCmp('subsector_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('rama_mod1').getValue() == ''){
							Ext.getCmp('rama_mod1').markInvalid('El campo es obligatorio');
						} else if(Ext.getCmp('clase_mod1').getValue() == ''){
							Ext.getCmp('clase_mod1').markInvalid('El campo es obligatorio');
						} else if (!/^([0-9])*$/.test(Ext.getCmp('codigo_postal_mod1').getValue())) {
							Ext.getCmp('codigo_postal_mod1').markInvalid('S�lo debe contener n�meros');
						} else if (!/^([0-9])*$/.test(Ext.getCmp('telefono_mod1').getValue())) {
							Ext.getCmp('telefono_mod1').markInvalid('S�lo debe contener n�meros');
						} else if (!/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('ventas_mod1').getValue())) {
							Ext.getCmp('ventas_mod1').markInvalid('S�lo debe contener n�meros');
						} else if (!/^([0-9])*$/.test(Ext.getCmp('num_empleados_mod1').getValue())) {
							Ext.getCmp('num_empleados_mod1').markInvalid('S�lo debe contener n�meros');
						} else if (!/^([0-9])*$/.test(Ext.getCmp('sirac_mod1').getValue())) {
							Ext.getCmp('sirac_mod1').markInvalid('S�lo debe contener n�meros');
						} else if (!(/^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z]{2,3})$/.exec(Ext.getCmp('email_mod1').getValue()))) {
							Ext.getCmp('email_mod1').markInvalid('Este campo debe ser una direcci�n de correo electr�nico');
						} else {
							if(Ext.getCmp('fecha_nac_mod1').getValue() != Ext.getCmp('fecha_nac_hide').getValue()) {
								Ext.getCmp('fecha_nac_mod1').setValue(Ext.getCmp('fecha_nac_hide').getValue());
							}
							actualizaCamposSirac();
						}
					}
				} //handler
			},{
				text: 'Regresar',
				id: 'btnREgresar',				
				iconCls: 'icoRegresar',
				handler: function() {
					direcciona();
				}
			}		
		]
    });
 
/******************************************************************************
 *								27. Contenedor Principal          							*
 ******************************************************************************/	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(10),
			gridConsulta,
			NE.util.getEspaciador(10),
			fpModificar
		]
	});

/******************************************************************************
 *								28. Inicializar parametros         							*
 ******************************************************************************/
	//fpModificar.hide();
	catalogoIF.load();
	catalogoPais.load();
	catalogoPaisOrigen.load();
	catalogoSector.load();

});	

/**
1. Carga de nuevo la pagina de inicio 
2. Descargar archivos
3. Valida que por lo menos se tenga un elemento del form para realizar la b�squeda
4. Valida que las ventas totales netas sean mayores a $5000.00 antes de actualizar los datos
5. Valida si el RFC es invalidado o no de acuerdo al checkbox
6. Valida que al actualizar SIRAC, el RFC no est� invalido
7. Ultimas validaciones antes de actualizar NE
8. Ultimas validaciones antes de actualizar SIRAC
9. Manda a actualizar los datos
10. Muestra el resultado de actualizar los datos
11. Proceso y consulta para llenar el cat�logo IF
12. Proceso y consulta para llenar el cat�logo Pais origen
13. Proceso y consulta para llenar el cat�logo Pais
14. Proceso y consulta para llenar el cat�logo Estado
15. Proceso y consulta para llenar el cat�logo Deleg/Municipio	
16. Proceso y consulta para llenar el cat�logo Ciudad
17. Proceso y consulta para llenar el cat�logo Sector economico
18. Proceso y consulta para llenar el cat�logo Subsector
19. Proceso y consulta para llenar el cat�logo Rama
20. Proceso y consulta para llenar el cat�logo Clase
21. C�digo para generar el Grid
22. Proceso para mostrar los datos a modificar
23. Elementos de la forma principal
25. Form Panel	principal 
26. Form para actualizar datos
27. Contenedor Principal 
28. Inicializar parametros 
*/