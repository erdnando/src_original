<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	com.netro.descuento.*,
	netropology.utilerias.*,	
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
/*** OBJETOS ***/
CQueryHelperRegExtJS queryHelperRegExtJS;
JSONObject jsonObj;
Registros registros;
JSONArray jsonArr;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String ic_if =(request.getParameter("noIc_if")==null)?"":request.getParameter("noIc_if");
String ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String txt_nafelec		= (request.getParameter("txt_nafelec")==null)?"":request.getParameter("txt_nafelec");
String txt_usuario		= (request.getParameter("txt_usuario")==null)?"":request.getParameter("txt_usuario");
String txt_fecha_acep_de= (request.getParameter("txt_fecha_acep_de")==null)?"":request.getParameter("txt_fecha_acep_de");
String txt_fecha_acep_a	= (request.getParameter("txt_fecha_acep_a")==null)?"":request.getParameter("txt_fecha_acep_a");
String expedienteCargado			= (request.getParameter("expediente")==null)?"":request.getParameter("expediente");
String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String ic_epo				= (request.getParameter("noIc_epo")==null)?"":request.getParameter("noIc_epo");

String resultado			= null;
/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/

if(informacion.equals("catalogoNombreEPO")){	
	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	resultado = catalogo.getJSONElementos();	
} /*** FIN CATALOGO NOMBRE EPO ***/

else	if (informacion.equals("CatalogoIF")){
	CatalogoIF catalogo = new CatalogoIF();
	JSONArray registro = new JSONArray();
	
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
		
	List regis = new ArrayList();
	regis =(ArrayList)catalogo.getListaElementosGral();
	registro = JSONArray.fromObject(regis);

	String consulta =  "{\"success\": true, \"total\": \"" + registro.size() + "\",\"registros\": " + registro.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	resultado =  jsonObj.toString();
}

/*** INICIO CONSULTA ***/
else if(informacion.equals("Consulta") || informacion.equals("ArchivoCSV")  || informacion.equals("ArchivoPDF") ) { 
		jsonObj 	      = new JSONObject();
		int start = 0, limit = 0;
		try {
			start = Integer.parseInt((request.getParameter("start")==null)?"0":request.getParameter("start"));
			limit = Integer.parseInt((request.getParameter("limit")==null)?"15":request.getParameter("limit"));				
		}catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		String operacion = (request.getParameter("operacion")== null) ? "" : request.getParameter("operacion");		

		if (!informacion.equals("ArchivoCSV")){
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));	
			}catch(Exception e){
				throw new AppException("Error en la conversi�n de tipo de datos ", e);
			}
 		}
		
		AfiliadosEfileCA datos =  new AfiliadosEfileCA();
			
		datos.setClaveBancoFondeo("");
		datos.setClaveEpo(ic_epo);
		datos.setTxt_nafelec(txt_nafelec);
		datos.setClaveIf(ic_if);
		datos.setDesde(txt_fecha_acep_de);
		datos.setHasta(txt_fecha_acep_a);
					
		datos.setExpedienteCargado("N");

		if(informacion.equals("Consulta")){
			QueryDebugger 	qd 			= new QueryDebugger(datos.getQueryPymesExpediente());		
			List	 			conditions 	= datos.getConditions();
			for(int i=0;i<conditions.size();i++){
				qd.addParameter((String)conditions.get(i));	
			}
			
			//ParametrosDescuentoHome parametrosDescuentoHome =(ParametrosDescuentoHome)ServiceLocator.getInstance().getEJBHome("ParametrosDescuentoEJB",ParametrosDescuentoHome.class);
			//ParametrosDescuento parametrosDescuento = parametrosDescuentoHome.create();
			ParametrosDescuento parametrosDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
	
			// Realizar consulta
			parametrosDescuento.actualizaPymesConExpedientesCargadosEnEfile(qd.getQueryStringWithParameters());	
		}
		
		datos.setExpedienteCargado(expedienteCargado);
		//Instancia para el uso del paginador
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(datos);
		
		if (operacion.equals("Generar")) {	//Nueva consulta
			cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
				
		if(informacion.equals("Consulta")){	
			String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
			jsonObj = JSONObject.fromObject(consultar);
			resultado=jsonObj.toString();
		}else if (informacion.equals("ArchivoCSV")){
			try {
				String nombreArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}		
			resultado = jsonObj.toString();
			
		}else if (informacion.equals("ArchivoPDF")){
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				
				String nombreArchivo = cqhelper.getCreatePageCustomFile(request, start, limit ,strDirectorioTemp,"PDF");				
				jsonObj.put("success", new Boolean(true));          
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}		
			resultado = jsonObj.toString();
		}

} /*** FIN DE CONSULTA ***/

else if(informacion.equals("pymeNombre")) {
	ic_epo				= (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
	
	//ParametrosDescuentoHome parametrosDescuentoHome =(ParametrosDescuentoHome)ServiceLocator.getInstance().getEJBHome("ParametrosDescuentoEJB",ParametrosDescuentoHome.class);
	//ParametrosDescuento parametrosDescuento = parametrosDescuentoHome.create();
	ParametrosDescuento parametrosDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
	
	String nombrePyme = parametrosDescuento.getNombrePyme(txt_nafelec,ic_epo);	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	String mensaje = "";
	jsonObj.put("pyme",nombrePyme);

	resultado = jsonObj.toString();	
}

/*** INICIA CATALOGO DE BUSQUEDA AVANZADA ***/
else if (informacion.equals("CatalogoBuscaAvanzada")) {

	String num_pyme	= (request.getParameter("claveProveedor")==null ||	request.getParameter("claveProveedor").equals(""))
		?"":request.getParameter("claveProveedor");
	String rfc_pyme		= (request.getParameter("rfc_pyme")==null || 	request.getParameter("rfc_pyme").equals(""))
		?"":request.getParameter("rfc_pyme");
	String nombre_pyme	= (request.getParameter("nombre_pyme")==null || 	request.getParameter("nombre_pyme").equals(""))
		?"":request.getParameter("nombre_pyme");
	
	String ic_producto_nafin = (request.getParameter("ic_producto_nafin")==null)?"":request.getParameter("ic_producto_nafin");
	ic_epo= request.getParameter("epo")==null ?"":request.getParameter("epo");	
	String pantalla ="AfiliadosEFile";
	
	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion afiliacion = afiliacionHome.create();
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
 
	registros = afiliacion.getProveedores(ic_producto_nafin, ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pyme,pantalla,ic_if);
	
	List coleccionElementos = new ArrayList();
	jsonArr = new JSONArray();
	JSONObject jsonObjP;
						
	while (registros.next()){
		jsonObjP= new JSONObject();		
		jsonObjP.put("clave", registros.getString("IC_NAFIN_ELECTRONICO"));
		jsonObjP.put("descripcion", registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("CG_RAZON_SOCIAL"));
		jsonObjP.put("ic_pyme",registros.getString("IC_PYME"));
		jsonArr.add(jsonObjP);
	}	
	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	resultado = jsonObj.toString();

} /*** FIN CATALOGO BUSQUEDA AVANZADA ***/

%>
<%= resultado%>
