<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,		
		java.util.Vector,
		java.io.*,
		net.sf.json.JSONObject,
                net.sf.json.JSONArray,
		netropology.utilerias.*,
                java.text.SimpleDateFormat,
                com.netro.model.catalogos.*,
                netropology.utilerias.usuarios.*,
                com.netro.afiliacion.*,
		com.netro.cadenas.*"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String claveIf	=	(request.getParameter("claveIf")!=null)?request.getParameter("claveIf"):"";
String clavePyme	=	(request.getParameter("clavePyme")!=null)?request.getParameter("clavePyme"):"";
String fechaNotifDe	=	(request.getParameter("fechaNotifDe")!=null)?request.getParameter("fechaNotifDe"):"";
String fechaNotifA=	(request.getParameter("fechaNotifA")!=null)?request.getParameter("fechaNotifA"):"";
String fechaEnteradoIni	=	(request.getParameter("fechaEnteradoIni")!=null)?request.getParameter("fechaEnteradoIni"):"";
String fechaEnteradoFinal	=	(request.getParameter("fechaEnteradoFinal")!=null)?request.getParameter("fechaEnteradoFinal"):"";
String fechaModifDe =(request.getParameter("fechaModifDe") == null) ? "":request.getParameter("fechaModifDe");
String fechaModifA =(request.getParameter("fechaModifA") == null) ? "":request.getParameter("fechaModifA");
String claveTramite	=	(request.getParameter("claveTramite")!=null)?request.getParameter("claveTramite"):"";
String txt_ne =(request.getParameter("num_Nae") == null) ? "":request.getParameter("num_Nae");
String rfc =(request.getParameter("rfc") == null) ? "":request.getParameter("rfc");
String icBitAfiliados =(request.getParameter("icBitAfiliados") == null) ? "":request.getParameter("icBitAfiliados");
 
String infoRegresar = "";
JSONObject jsonObj = new JSONObject();

Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

ConsBitaActualizacionDatos    paginador = new ConsBitaActualizacionDatos();

if(strTipoUsuario.equals("IF")) {
   claveIf = iNoCliente;
}
paginador.setClaveIf(claveIf);
paginador.setNafinElectronico(txt_ne);
paginador.setIcTramite(claveTramite);
paginador.setRfc(rfc);
paginador.setTipoConsulta(informacion);
paginador.setIcBitAfiliados(icBitAfiliados);
paginador.setFechaEnteradoIni(fechaEnteradoIni);
paginador.setFechaEnteradoFinal(fechaEnteradoFinal);
paginador.setFechaNotifDe (fechaNotifDe);
paginador.setFechaNotifA (fechaNotifA);
paginador.setFechaModifDe (fechaModifDe);
paginador.setFechaModifA (fechaModifA);
  
    
CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
Registros listaRegistros = new Registros();

if ("CargaNom_Nae".equals(informacion)   ) {  

    String cad = "";        
    Registros registros = afiliacion.getNumeros("P", "", "", txt_ne, "");
    while (registros.next()){
        cad = registros.getString("DESCRIPCION");
    }
    jsonObj = new JSONObject();
    if(!cad.equals("")){
        jsonObj.put("success", new Boolean(true));
	jsonObj.put("nom_Nae", cad);
        jsonObj.put("existe", "S");
    }else{
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("existe", "N");
    }
	
    infoRegresar = jsonObj.toString();
        
}else if ("CatalogoBuscaAvanzada".equals(informacion)   ) {

    String cmb_banco_fondeo	= (request.getParameter("hid_ic_banco") == null) ? "":request.getParameter("hid_ic_banco");
    String rfc_prov = request.getParameter("rfc_prov")==null?"":request.getParameter("rfc_prov");   
    String txt_nombre = (request.getParameter("nombre_pyme") == null) ? "": request.getParameter("nombre_pyme");
    String num_pyme = (request.getParameter("num_pyme") == null) ? "": request.getParameter("num_pyme");
   
        
    Registros registros = afiliacion.getNumeros("P", txt_nombre, rfc_prov, num_pyme, "");
    
    List coleccionElementos = new ArrayList();
    JSONArray jsonArr = new JSONArray();
    while (registros.next()){
        ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
	elementoCatalogo.setClave(registros.getString("CLAVE"));
	elementoCatalogo.setDescripcion(registros.getString("CLAVE")+" "+registros.getString("DESCRIPCION"));
	coleccionElementos.add(elementoCatalogo);
    }
    Iterator it = coleccionElementos.iterator();
    while(it.hasNext()) {
        Object obj = it.next();
	if (obj instanceof netropology.utilerias.ElementoCatalogo) {
            ElementoCatalogo ec = (ElementoCatalogo)obj;
            jsonArr.add(JSONObject.fromObject(ec));
	}
    }
    jsonObj = new JSONObject();
    jsonObj.put("success", new Boolean(true));
    if (jsonArr.size() == 0){
        jsonObj.put("total", "0");
	jsonObj.put("registros", "" );
    }else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
        jsonObj.put("total",  Integer.toString(jsonArr.size()));
	jsonObj.put("registros", jsonArr.toString() );
    }else if (jsonArr.size() > 1000 ){
        jsonObj.put("total", "excede" );
	jsonObj.put("registros", "" );
    }
    infoRegresar = jsonObj.toString();
        
}else  if ("catTramite".equals(informacion)  ) {

    CatalogoSimple catalogo = new CatalogoSimple();
    catalogo.setCampoClave("IC_TRAMITE");
    catalogo.setCampoDescripcion("CG_TRAMITE");
    catalogo.setTabla("COMCAT_TRAMITE");
    catalogo.setOrden("IC_TRAMITE");
    infoRegresar = catalogo.getJSONElementos();  
    
}else  if ("Consulta".equals(informacion)  ) {
    
    Registros reg = queryHelper.doSearch();	
    while(reg.next()){
        String ictramites = reg.getString("TRAMITE");
        String tramite =  paginador.datosTramite(ictramites, "Grid");
        reg.setObject("TRAMITE",tramite);
    }
   
    infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
  
}else  if ("GenerarArchivo".equals(informacion)  ) {

    try {
        String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
    } catch(Throwable e) {
        throw new AppException("Error al generar el archivo CSV", e);
    }
    infoRegresar = jsonObj.toString(); 


}else  if ("Cons_Enterado".equals(informacion)  ) {
        
    UtilUsr usuario = new UtilUsr();
    Registros reg  = queryHelper.doSearch();	
    while(reg.next()){
        String idUsuario = reg.getString("USUARIO");
        Usuario user = new Usuario(); 
	user = usuario.getUsuario(idUsuario);
        reg.setObject("NOMBRE_USUARIO",user.getNombreCompleto());
    }
    infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
 
}else  if ("Cons_EnvioEmail".equals(informacion)  ) {
    
    UtilUsr usuario = new UtilUsr();
    Registros reg  = queryHelper.doSearch();	
   
    infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
 
}

%>
<%=infoRegresar%>
