<%@ page contentType="application/json;charset=UTF-8" import="   
   java.util.*, java.sql.*,   
	netropology.utilerias.*,   
	com.netro.exception.*,  
	javax.naming.*,  
	com.netro.cadenas.*, 
	java.text.SimpleDateFormat,
	com.netro.model.catalogos.*,  
	net.sf.json.JSONArray,   
	net.sf.json.JSONObject" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<% 
String informacion  =(request.getParameter("informacion")!=null) ? request.getParameter("informacion"):"";
String ic_epo  =(request.getParameter("ic_epo")!=null) ? request.getParameter("ic_epo"):"";

SimpleDateFormat fecha_hoy = new SimpleDateFormat("dd/MM/yyyy");
String fecha_h = fecha_hoy.format(new java.util.Date());

String infoRegresar ="";
JSONObject   jsonObj = new JSONObject(); 

ConsReafiliacion paginador=  new ConsReafiliacion();
paginador.setIc_epo(ic_epo);
paginador.setPantalla("Captura");

CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);	
	
if(informacion.equals("catalogoEPO")){

	CatalogoEPO catalogo = new CatalogoEPO();	
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");	
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("Consultar")){
	
	try {
		Registros reg	=	queryHelper.doSearch();
			
		String 	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj.put("success", new Boolean(true));
		jsonObj = JSONObject.fromObject(consulta);
	}	catch(Exception e) {
		throw new AppException("Error en la paginación", e);
	}
	
	jsonObj.put("fecha_hoy",  fecha_h);
   infoRegresar = jsonObj.toString();


} else if(informacion.equals("Captura")){

	String claveEPO[] = request.getParameterValues("claveEPO");
	String chkREAFILIACION[] = request.getParameterValues("chkREAFILIACION");
	String fecha_activacion[] = request.getParameterValues("fecha_activacion");
	String numRegistros  =(request.getParameter("numRegistros")!=null) ? request.getParameter("numRegistros"):"";
	String proveedores[] = request.getParameterValues("proveedores");
	int numRegistros2 = Integer.parseInt(numRegistros);
	String mensaje ="";
	try {
		mensaje =  paginador.capturaReafiliacion( numRegistros2, claveEPO, chkREAFILIACION, fecha_activacion, strNombreUsuario, proveedores);		
	}	catch(Exception e) {		 
		throw new AppException("Error en la Capturar   Reafiliaciones Automaticas  ", e);
	}
	jsonObj.put("success",  new Boolean(true));
   jsonObj.put("mensaje",  mensaje);	
   infoRegresar = jsonObj.toString();
	
} 

%>

<%=infoRegresar%>
