<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.ResultSet,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	

 /* ************************* CATALOGOS ************************* */
 if(informacion.equals("catalogoGrupo")) {		

		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("cecat_grupo");
		cat.setCampoClave("ic_grupo");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setOrden("ic_grupo");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("catalogoEstatus")) {		
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionNotIn("52", Integer.class);
		infoRegresar = cat.getJSONElementos();	
 }
 else if(informacion.equals("catalogoPais")) {		
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionNotIn("52", Integer.class);
		infoRegresar = cat.getJSONElementos();	
 }
 else if(informacion.equals("catalogoEstado")){
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		System.out.println();
		System.out.println();
		System.out.println("Pais=============================================" + Pais);
		System.out.println();
		System.out.println();
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setClavePais(Pais);//Mexico clave 24
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();	
 }
 else if(informacion.equals("catalogoMunicipio"))	{
		
		String Edo= (request.getParameter("estado")==null)?"":request.getParameter("estado");
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoMunicipio cat=new CatalogoMunicipio();
		cat.setClave("ic_municipio");
		cat.setDescripcion("cd_nombre");
		cat.setPais(Pais);
		cat.setEstado(Edo);
		cat.setOrden("cd_nombre");
		List elementos=cat.getListaElementos();
		
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
 }
 else if(informacion.equals("catalogoIdentificacion")) {
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_identificacion");
		cat.setCampoClave("ic_identificacion");
		cat.setCampoDescripcion("cd_nombre");
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
 }
/* ************************* ********* ************************* */
 
 else if(informacion.equals("ConsultaUniversidad"))	{
	try	
		{			
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			String   ic_universidad = (request.getParameter("ic_universidad") == null) ? "" : request.getParameter("ic_universidad");
			
			Registros reg ;		
			reg = BeanAfiliacion.getDatosUniversidad(ic_universidad);
			
			HashMap resultado = new HashMap();
			ArrayList lst_info_univer = new ArrayList();
			/*Barro  la informacion del Query */
			while(reg.next()) {
				String Grupo      					= (reg.getString(1) == null) ? "" :reg.getString(1).trim();   
				String Razon_Social      			= (reg.getString(2) == null) ? "" :reg.getString(2).trim();   
				String R_F_C 			  				= (reg.getString(3) == null) ? "" :reg.getString(3).trim();
				String Campus  						= (reg.getString(4) == null) ? "" :reg.getString(4).trim();
				String Estatus 			  			= (reg.getString(5) == null) ? "" :reg.getString(5).trim();
				
				String Calle 			  				= (reg.getString(6) == null) ? "" : reg.getString(6).trim();   
				String Colonia 		  				= (reg.getString(7) == null) ? "" : reg.getString(7).trim();
				String Codigo_postal 	  			= (reg.getString(8) == null) ? "" : reg.getString(8).trim();
				String Pais 			  				= (reg.getString(9) == null) ? "" : reg.getString(9).trim();
				String Estado 			  				= (reg.getString(10) == null) ? "": reg.getString(10).trim();
				String Delegacion_o_municipio 	= (reg.getString(11) == null) ? "": reg.getString(11).trim();
				String Telefono 		  				= (reg.getString(12) == null) ? "": reg.getString(12).trim();
				String Email 			  				= (reg.getString(13) == null) ? "": reg.getString(13).trim();
				String Fax 	 		  					= (reg.getString(14) == null) ? "": reg.getString(14).trim();
				
				String Apellido_paterno_L 			= (reg.getString(15) == null) ? "": reg.getString(15).trim();
				String Apellido_materno_L 			= (reg.getString(16) == null) ? "": reg.getString(16).trim();
				String Nombre_L 		  				= (reg.getString(17) == null) ? "": reg.getString(17).trim();
				String Identificacion 				= (reg.getString(18) == null) ? "": reg.getString(18).trim();
				String No_Identificacion 			= (reg.getString(19) == null) ? "": reg.getString(19).trim();
				String Numero_de_escritura 		= (reg.getString(20) == null) ? "": reg.getString(20).trim();
				String Fecha_del_poder_notarial 	= (reg.getString(21) == null) ? "": reg.getString(21).trim();
				
				
				resultado.put("Grupo",Grupo);
				resultado.put("Razon_Social",Razon_Social);
				resultado.put("R_F_C",R_F_C);
				resultado.put("Campus",Campus);
				resultado.put("Estatus",Estatus);
				resultado.put("Calle",Calle);
				resultado.put("Colonia",Colonia);
				resultado.put("Codigo_postal",Codigo_postal);
				resultado.put("Pais",Pais);
				resultado.put("Estado",Estado);
				resultado.put("Delegacion_o_municipio",Delegacion_o_municipio);
				resultado.put("Telefono",Telefono);
				resultado.put("Email",Email);
				resultado.put("Fax",Fax);
				
				resultado.put("Apellido_paterno_L",Apellido_paterno_L);
				resultado.put("Apellido_materno_L",Apellido_materno_L);
				resultado.put("Nombre_L",Nombre_L);
				resultado.put("Identificacion",Identificacion);
				resultado.put("No_Identificacion",No_Identificacion);
				resultado.put("Numero_de_escritura",Numero_de_escritura);
				resultado.put("Fecha_del_poder_notarial",Fecha_del_poder_notarial);
			}
			
			
			/*Recupero la informacion en un hashmap para enviarla del lado del js */			
			lst_info_univer.add(resultado);
			JSONArray jsObjArray = JSONArray.fromObject(lst_info_univer);
			infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
				
		} catch(Exception e) { 
				throw new AppException("Error en la recuperación de datos", e);
			}
 }//fin ConsultaEpo
 
 


 else if(informacion.equals("ModificaUniversidad"))	{
	try	
		{
			
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			String   ic_universidad				 = (request.getParameter("ic_universidad") == null) ? "" : request.getParameter("ic_universidad");
			String   Grupo    					 = (request.getParameter("grupo") == null) ? "" : request.getParameter("grupo"),
					 	Razon_Social  				 = (request.getParameter("universidad") == null) ? "" : request.getParameter("universidad"),						
						R_F_C         				 = (request.getParameter("rfc")    == null) ? "" : request.getParameter("rfc"),
						Campus 			          = (request.getParameter("campus") == null) ? "" : request.getParameter("campus"),
						Estatus 			          = (request.getParameter("cmb_estatus") == null) ? "" : request.getParameter("cmb_estatus");
						
			//Domicilio							
			String   Calle        				 = (request.getParameter("calle")    == null) ? "" : request.getParameter("calle"),
						Colonia       				 = (request.getParameter("colonia")  == null) ? "" : request.getParameter("colonia"),
						Codigo_postal 				 = (request.getParameter("code") == null) ? "" : request.getParameter("code"),
						Pais          				 = (request.getParameter("cmb_pais")     == null) ? "" : request.getParameter("cmb_pais"),
						Estado        				 = (request.getParameter("cmb_estado")   == null) ? "" : request.getParameter("cmb_estado"),
						Delegacion_o_municipio 	 = (request.getParameter("cmb_delegacion") == null) ? "" : request.getParameter("cmb_delegacion"),
						Telefono      				 = (request.getParameter("telefono") == null) ? "" : request.getParameter("telefono"),
						Email         				 = (request.getParameter("email")    == null) ? "" : request.getParameter("email"),
						Fax           				 = (request.getParameter("fax")      == null) ? "" : request.getParameter("fax");
						
			//Datos del Representate Legal
			String 	Apellido_paterno_L       = (request.getParameter("paterno")       == null) ? "" : request.getParameter("paterno"),
						Apellido_materno_L       = (request.getParameter("materno")       == null) ? "" : request.getParameter("materno"),
						Nombre_L                 = (request.getParameter("nombre")        == null) ? "" : request.getParameter("nombre"),		
						Identificacion				 = (request.getParameter("cmb_identificacion") == null) ? "" : request.getParameter("cmb_identificacion"),
						No_Identificacion			 = (request.getParameter("num_iden") == null) ? "" : request.getParameter("num_iden"),
						Numero_de_escritura      = (request.getParameter("poderes") == null) ? "" : request.getParameter("poderes"),
						Fecha_del_poder_notarial = (request.getParameter("fch_poder") == null) ? "" : request.getParameter("fch_poder");
			
						
			String msg = "";
			
			if( !Delegacion_o_municipio.equals("") && Delegacion_o_municipio != null)
			{
				System.out.println("");
				System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
				System.out.println("Delegacion_o_municipio	" + Delegacion_o_municipio);
				System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
				System.out.println("");
				
				CatalogoMunicipio cat=new CatalogoMunicipio();
				cat.setClave("ic_municipio");
				cat.setDescripcion("cd_nombre");
				cat.setPais(Pais);
				cat.setEstado(Estado);
				cat.setSeleccion(Delegacion_o_municipio);
				cat.setOrden("cd_nombre");
				List elementos=cat.getListaElementos();
			
				Iterator it = elementos.iterator();
				while(it.hasNext()) {
					ElementoCatalogo obj = (ElementoCatalogo)it.next();
					String iDelMun = obj.getClave();				String strDelMun = obj.getDescripcion();
					Delegacion_o_municipio = iDelMun + "|" + strDelMun;
					System.out.println("Delegacion_o_municipio	" + Delegacion_o_municipio);
				}				
			}
			if(!Estatus.equals("") && Estatus != null)
			{
				if(Estatus.equals("Activo"))
					Estatus = "A";
				else if(Estatus.equals("Inactivo"))
					Estatus = "I";
			}
			String sDatosEpo = "ic_universidad-" + ic_universidad +"|" +"Grupo-" + Grupo +"|"+ "Razon_Social-" + Razon_Social +"|"+"R_F_C-" + 
							R_F_C+"|"+"Campus-" + Campus+"|"+"Estatus-" + Estatus+"|"+"Calle-" + Calle+"|"+ 
							"Colonia-"+	Colonia +"|" +"Codigo_postal-" + Codigo_postal+"|"+"Pais-" + Pais+"|"+
							"Estado-" + Estado+"|"+ "Delegacion_o_municipio-" + Delegacion_o_municipio+"|"+ "Telefono-" + Telefono+"|"+
							"Email-" + Email+"|"+ "Fax-" + Fax+"|"+ "Apellido_paterno_L-" + Apellido_paterno_L+"|"+
							"Apellido_materno_L-" + Apellido_materno_L+"|"+"Nombre_L-" + Nombre_L+"|"+"Identificacion-" + Identificacion+"|"+
							"No_Identificacion-" + No_Identificacion+"|"+"Numero_de_escritura-" + Numero_de_escritura+"|"+"Fecha_del_poder_notarial-"+
							Fecha_del_poder_notarial+"|"+"\n";
			System.out.println(sDatosEpo);	
			
			BeanAfiliacion.actualizaUniversidad(iNoUsuario, ic_universidad, Grupo, Razon_Social.toUpperCase(), R_F_C.toUpperCase(),Campus.toUpperCase(),Estatus,
								Calle, Colonia, Codigo_postal, Pais, Estado, Delegacion_o_municipio, Telefono, Email, Fax, Apellido_paterno_L, Apellido_materno_L,
								Nombre_L, Identificacion, No_Identificacion, Numero_de_escritura,Fecha_del_poder_notarial);
			
			msg = "Los datos han sido actualizados";
				
			JSONObject resultado = new JSONObject();
			resultado.put("success", new Boolean(true));
			resultado.put("msg", msg);
			infoRegresar = resultado.toString();	
		
		} catch(Exception e) { 
				throw new AppException("Error en la afiliacion", e);
			}
 }


//System.out.println("infoRegresar============"+infoRegresar); 


%>
<%=infoRegresar%>



