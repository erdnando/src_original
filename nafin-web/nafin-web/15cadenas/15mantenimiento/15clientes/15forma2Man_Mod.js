
Ext.onReady(function(){

	var ic_mandante =  Ext.getDom('ic_mandante').value;
	var tipoPersona =  Ext.getDom('tipoPersona').value;


	function procesaActualizar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
	
			Ext.MessageBox.alert('Mensaje',jsonData.mensaje,
				function(){ 					
					window.location = '15forma5ext.jsp';		
				});
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	function procesoActualizar(opts, success, response) {
	
		//Persona Fisica	
		if(tipoPersona==1){
		
			var apellido_paterno_1 = Ext.getCmp('apellido_paterno_1');
			var nombre_1 = Ext.getCmp('nombre_1');
			var fecha_de_nacimiento = Ext.getCmp('fecha_de_nacimiento');
			var apellido_materno_1 = Ext.getCmp('apellido_materno_1');
			var R_F_CF_1 = Ext.getCmp('R_F_CF_1');
			var cmb_pais_1 = Ext.getCmp('cmb_pais_1');			
			
			
		
			if (Ext.isEmpty(apellido_paterno_1.getValue()) ){
				apellido_paterno_1.markInvalid('Este campo es obigatorio');
				return;
			}				
			if (Ext.isEmpty(nombre_1.getValue()) ){
				nombre_1.markInvalid('Este campo es obigatorio');
				return;
			}	
			if (Ext.isEmpty(fecha_de_nacimiento.getValue()) ){
				fecha_de_nacimiento.markInvalid('Este campo es obigatorio');
				return;
			}	
			if (Ext.isEmpty(apellido_materno_1.getValue()) ){
				apellido_materno_1.markInvalid('Este campo es obigatorio');
				return;
			}	
			if (Ext.isEmpty(R_F_CF_1.getValue()) ){
				R_F_CF_1.markInvalid('Este campo es obigatorio');
				return;
			}
			if (Ext.isEmpty(cmb_pais_1.getValue()) ){
				cmb_pais_1.markInvalid('Este campo es obigatorio');
				return;
			}	
		}else  if(tipoPersona==5){
			
			var razon_Social_1 = Ext.getCmp('razon_Social_1');
			var comercial_1 = Ext.getCmp('comercial_1');
			var R_F_C_M_1 = Ext.getCmp('R_F_C_M_1');
			
		
			if (Ext.isEmpty(razon_Social_1.getValue()) ){
				razon_Social_1.markInvalid('Este campo es obigatorio');
				return;
			}	
		
			if (Ext.isEmpty(comercial_1.getValue()) ){
				comercial_1.markInvalid('Este campo es obigatorio');
				return;
			}
			if (Ext.isEmpty(R_F_C_M_1.getValue()) ){
				R_F_C_M_1.markInvalid('Este campo es obigatorio');
				return;
			}
		
		}
		if ( !Ext.isEmpty( Ext.getCmp('telefono_1').getValue())   ||  !Ext.isEmpty( Ext.getCmp('fax_1').getValue())  ){	
		
			var telefono = Ext.getCmp('telefono_1').getValue();	
			var fax = Ext.getCmp('fax_1'). getValue();	
			
			if ( telefono.length >8 ){
				telefono.markInvalid('El tama�o m�ximo para este campo es de 8');
					return;
			}
					
			if ( fax.length >20 ){
				fax.markInvalid('El tama�o m�ximo para este campo es de 20');
				return;
			}
		
		}
		
		fp.el.mask('Enviando...', 'x-mask-loading');		
		Ext.Ajax.request({
			url: '15forma2Man_Mod.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
				informacion: "ActualizarMandante",
				delegacion_o_municipio:Ext.getCmp('delegacion_o_municipio_1').getValue()
			}),
			callback: procesaActualizar 
		});		

	}


	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
				
				Ext.getCmp('fecha_de_nacimiento').setValue(jsonData.fecha_de_nacimiento);				
				//Persona Fisica	
				if(jsonData.tipoPersona==1){
					Ext.getCmp('apellido_paterno_1').setValue(jsonData.Apellido_paterno);
					Ext.getCmp('apellido_materno_1').setValue(jsonData.Apellido_materno);
					Ext.getCmp('nombre_1').setValue(jsonData.Nombre);
					Ext.getCmp('R_F_CF_1').setValue(jsonData.R_F_C);
					
					
					
					Ext.getCmp('divPersonaMoral').hide();					
					
				}else  if(jsonData.tipoPersona==5){
				
					Ext.getCmp('razon_Social_1').setValue(jsonData.Razon_Social);
					Ext.getCmp('comercial_1').setValue(jsonData.Razon_Social);
					Ext.getCmp('R_F_C_M_1').setValue(jsonData.R_F_C);
					
					Ext.getCmp('divPersonaFisica').hide();						
				}
				//Domicilio 						
				Ext.getCmp('calle_1').setValue(jsonData.Calle);
				Ext.getCmp('colonia_1').setValue(jsonData.Colonia);
			
				catalogoPaisData.load({
					params: Ext.apply({
						cmb_pais : jsonData.Pais						
					})
				});		
				
		
				catalogoEstadoData.load({
					params: Ext.apply({
						cmb_pais : jsonData.Pais,
						estado : jsonData.Estado1
					})
				});				
				
				catalogoMunicipio.load({
					params: Ext.apply({
						estado : jsonData.Estado1,
						delegacion_o_municipio:jsonData.Delegacion_o_municipio
					})
				});
				
				Ext.getCmp('codigo_postal_1').setValue(jsonData.Codigo_postal);							
				Ext.getCmp('telefono_1').setValue(jsonData.Telefono);
				Ext.getCmp('fax_1').setValue(jsonData.Fax);
				
				//Datos del contacto
				Ext.getCmp('Apellido_paterno_C_1').setValue(jsonData.Apellido_paterno_C);			
				Ext.getCmp('materno_contacto_1').setValue(jsonData.Apellido_materno_C);
				Ext.getCmp('Nombre_C_1').setValue(jsonData.Nombre_C);				
				Ext.getCmp('telefonoC_1').setValue(jsonData.Telefono_C);				
				Ext.getCmp('txt_cel_cont_1').setValue(jsonData.numeroCelular);				
				Ext.getCmp('email_1').setValue(jsonData.Email_C);
				Ext.getCmp('Fax_C_1').setValue(jsonData.Fax_C);	
				
				Ext.getCmp('confirmacion_Email_C').setValue(jsonData.confirmacion_Email_C);	
				Ext.getCmp('numero_de_cliente').setValue(jsonData.numero_de_cliente);
				Ext.getCmp('tipoPersona').setValue(jsonData.tipoPersona);
				Ext.getCmp('ic_mandante').setValue(jsonData.ic_mandante);			
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	 var procesarCatPaisData = function(store, records, oprion){
	  if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cmb_pais_1 = Ext.getCmp('cmb_pais_1');
			var cmb_paisM_1 = Ext.getCmp('cmb_paisM_1');
			var jsonData = store.reader.jsonData;
			
			if(cmb_pais_1.getValue()==''){
				cmb_pais_1.setValue(jsonData.cmb_pais);
			}			
			if(cmb_paisM_1.getValue()==''){
				cmb_paisM_1.setValue(jsonData.cmb_pais);
			}			
		}
  }
  
  
	var catalogoPaisData = new Ext.data.JsonStore({
		id:				'catalogoPaisData',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15forma2Man_Mod.data.jsp',
		baseParams:		{	informacion: 'catalogoPaisData'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: procesarCatPaisData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		//Catalogo para el combo de seleccion de Estado

  var procesarCatEstadoData = function(store, records, oprion){
	  if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var estado_1 = Ext.getCmp('estado_1');
			var jsonData = store.reader.jsonData;
			
			if(estado_1.getValue()==''){
				estado_1.setValue(jsonData.estado);
			}
		}
  }
  
	var catalogoEstadoData = new Ext.data.JsonStore({
		id:				'catalogoEstadoData',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15forma2Man_Mod.data.jsp',
		baseParams:		{	informacion: 'catalogoEstadoData'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: procesarCatEstadoData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		//catalogo Municipio
		
	var procesarCatMunicipio = function(store, records, oprion){
	  if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var delegacion_o_municipio_1 = Ext.getCmp('delegacion_o_municipio_1');
			var jsonData = store.reader.jsonData;
					
			if(delegacion_o_municipio_1.getValue()==''){
				delegacion_o_municipio_1.setValue(jsonData.delegacion_o_municipio);
			}
		}
  }
	var catalogoMunicipio= new Ext.data.JsonStore 	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma2Man_Mod.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	: 	{
		load: procesarCatMunicipio,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	//*******************Persona Fisica********************

	var datosPFisicaIzq = {
		xtype			: 'fieldset',
		id 			: 'datosPFisicaIzq',
		border		: false,
		labelWidth	: 150,
		width			: 400,
		items			: 
		[	
			{
				xtype			: 'textfield',
				id          : 'apellido_paterno_1',
				name			: 'apellido_paterno',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				margins		: '0 20 0 0',
				width			: 200,
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype			: 'textfield',
				id          : 'nombre_1',
				name			: 'nombre',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype			: 'datefield',
				id          : 'fecha_de_nacimiento',
				name			: 'fecha_de_nacimiento',
				allowBlank	: false,
				width			: 130,
				fieldLabel  : '* Fecha de nacimiento (dd/mm/aaaa)',
				margins		: '0 20 0 0',
				tabIndex		: 16
			}
		]
	};
	
	
	var datosPFisicaDer = {
		xtype			: 'fieldset',
		id 			: 'datosPFisicaDer',
		border		: false,
		labelWidth	: 150,
		width			: 400,
		items			: 
		[	
			{
				xtype			: 'textfield',
				id          : 'apellido_materno_1',
				name			: 'apellido_materno',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				margins		: '0 20 0 0',
				width			: 200,
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype			: 'textfield',
				id          : 'R_F_CF_1',
				name			: 'R_F_CF',
				fieldLabel  : '* RFC',
				maxLength	: 15,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side',
				regex			:/^([A-Z|a-z|&amp;]{4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
										regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
										'en el formato NNNN-AAMMDD-XXX donde:<br>'+
										'NNN:son las iniciales del nombre y apellido<br>'+
										'AAMMDD: es la fecha de nacimiento menor al dia de hoy<br>'+
										'XXX:es la homoclave',
				listeners:{change:function(field, newValue){  field.setValue(newValue.toUpperCase()); 	}}
			},
			{
				xtype				: 'combo',
				id          	: 'cmb_pais_1',
				hiddenName 		: 'cmb_pais',
				fieldLabel  	: '* Pa�s',
				allowBlank		: false,
				msgTarget: 'side',
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccionar...',
				width				: 200,
				store				: catalogoPaisData,
				listeners: {
					select: function(combo, record, index) {
						catalogoEstadoData.load({
							params: Ext.apply({
								cmb_pais : record.json.clave
							})
						});
						
					}
				}
			}	
		]
	};
	
	// *******************persona  Moral 
	var datosPMoralIzq = {
		xtype			: 'fieldset',
		id 			: 'datosPMoralIzq',
		border		: false,
		labelWidth	: 150,
		width			: 400,
		items			: 
		[	
			{
				xtype			: 'textfield',
				name			: 'razon_Social',
				id				: 'razon_Social_1',
				fieldLabel	: '* Raz�n Social',
				maxLength	: 100,
				width			: 200,
				hidden		: false,
				msgTarget: 'side',
				allowBlank	: false,
				tabIndex		:	1
			},
			{
				xtype			: 'textfield',
				name			: 'comercial',
				id				: 'comercial_1',
				fieldLabel	: '* Nombre Comercial',
				maxLength	: 100,
				width			: 200,
				hidden		: false,
				msgTarget: 'side',
				allowBlank	: false,
				tabIndex		:	1
			}
		]
	};
	
	var datosPMoralDer = {
		xtype			: 'fieldset',
		id 			: 'datosPMoralDer',
		border		: false,
		labelWidth	: 150,
		width			: 400,
		items			: 
		[	
			{
				xtype			: 'textfield',
				id          : 'R_F_C_M_1',
				name			: 'R_F_C_M',
				fieldLabel  : '* RFC',
				maxLength	: 15,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side',
				regex			:/^([A-Z|a-z|&amp;]{3})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
										regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
										'en el formato NNN-AAMMDD-XXX donde:<br>'+
										'NNN:son las iniciales del nombre y apellido<br>'+
										'AAMMDD: es la fecha de nacimiento menor al dia de hoy<br>'+
										'XXX:es la homoclave',
				listeners:{change:function(field, newValue){  field.setValue(newValue.toUpperCase()); 	}}
			}	
		]
	};
	
	// ***************Datos del domicilio *********
	var domicilioIzq = {
		xtype			: 'fieldset',
		id 			: 'domicilioIzq',
		border		: false,
		labelWidth	: 150,
		width			: 400,
		items			: 
		[	
			{
				xtype			: 'textfield',
				id          : 'calle_1',
				name			: 'calle',
				fieldLabel  : '* Calle, No. Exterior y No. Interior',
				maxLength	: 40,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'

			},
			{
				xtype				: 'combo',
				id          	: 'estado_1',
				hiddenName 		: 'estado',
				fieldLabel  	: '* Estado',
				allowBlank		: false,
				msgTarget: 'side',
				triggerAction	: 'all',
				emptyText		: 'Seleccionar...',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 200,
				store				: catalogoEstadoData,
				listeners: {
					select: function(combo, record, index) {
						 
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave
							})
						});
						
					}
				}
			},
			/*{
				xtype			: 'numberfield',
				id          : 'codigo_postal_1',
				name			: 'codigo_postal',
				fieldLabel  : '* Codigo Postal',
				maxLength	: 5,
				width			: 90,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},*/
                        
                        {
				xtype			: 'textfield',
				id          : 'codigo_postal_1',
				name			: 'codigo_postal',
				fieldLabel  : '* Codigo Postal',
				maxLength	: 5,
                                minLength:		5,
				width			: 90,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side',
                                regex       : /^[0-9]*$/,
                                regexText   : 'Solo de contener n�meros'
			},
			{
				xtype			: 'textfield',
				id          : 'telefono_1',
				name			: 'telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				msgTarget: 'side',
				width			: 200,
				maskRe:		/[0-9]/,
				margins		: '0 20 0 0',
				maxLength	: 8				
			}
		]
	};
	
	var domicilioDer = {
		xtype			: 'fieldset',
		id 			: 'domicilioDer',
		border		: false,
		labelWidth	: 150,
		width			: 400,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'colonia_1',
				name			: 'colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 40,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype				: 'combo',
				id          	: 'delegacion_o_municipio_1',
				name 		: 'delegacion_o_municipio',
				fieldLabel  	: '* Delegaci�n o municipio ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',				
				allowBlank		: false,
				msgTarget: 'side',
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 200,
				store				: catalogoMunicipio
			},
			{
				xtype				: 'combo',
				id          	: 'cmb_paisM_1',
				hiddenName 		: 'cmb_paisM',
				fieldLabel  	: '* Pa�s',
				allowBlank		: false,
				msgTarget: 'side',
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccionar...',
				width				: 200,
				store				: catalogoPaisData,
				listeners: {
					select: function(combo, record, index) {
						catalogoEstadoData.load({
							params: Ext.apply({
								cmb_paisM : record.json.clave
							})
						});						
					}
				}
			},
			{
				xtype			: 'textfield',
				id          : 'fax_1',
				name			: 'fax',
				fieldLabel  : 'Fax',
				maskRe:		/[0-9]/,
				width			: 200,
				maxLength	: 20,
				msgTarget: 'side',
				margins		: '0 20 0 0',
				allowBlank	: true
			}		
		]
	};

	//Datos del contacto  ********
	var contratoIzq = {
		xtype			: 'fieldset',
		id 			: 'contratoIzq',
		border		: false,
		labelWidth	: 150,
		width			: 400,
		items			: 
		[	
			{
				xtype			: 'textfield',
				id          : 'Apellido_paterno_C_1',
				name			: 'Apellido_paterno_C',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				margins		: '0 20 0 0',
				width			: 200,
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype			: 'textfield',
				id          : 'Nombre_C_1',
				name			: 'Nombre_C',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},
			
			{
				xtype			: 'textfield',
				id          : 'Fax_C_1',
				name			: 'Fax_C',
				fieldLabel  : 'Fax',
				maxLength	: 20,
				width			: 200,
				maskRe:		/[0-9]/,
				margins		: '0 20 0 0',
				allowBlank	: true,
				msgTarget: 'side'
			},			
			{
				xtype:			'textfield',
				name:				'txt_cel_cont',
				id:				'txt_cel_cont_1',
				fieldLabel:    '* Celular (Clave Lada)',
				width:			200,
				allowBlank:		false,
				msgTarget: 'side',
				maskRe:		/[0-9]/,
				maxLength:		10
			}					
		]
	};
	
	var contratoDer = {
		xtype			: 'fieldset',
		id 			: 'contratoDer',
		border		: false,
		labelWidth	: 150,
		width			: 400,
		items			: 
		[	
			{
				xtype			: 'textfield',
				id          : 'materno_contacto_1',
				name			: 'materno_contacto',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype			: 'textfield',
				id          : 'telefonoC_1',
				name			: 'telefonoC',
				fieldLabel  : '*Tel�fono',
				maxLength	: 8,
				width			: 200,
				maskRe:		/[0-9]/,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'email_1',
				name			: 'email',
				fieldLabel: ' * E-mail',
				maxLength	: 50,
				width			: 200,
				margins		: '0 20 0 0',
				vtype			: 'email',
				allowBlank	: false,
				msgTarget: 'side'
			}
		
		]
	};
	
	
	
	var fp = new Ext.form.FormPanel({
		id					: 'forma',
		layout			: 'form',
		width				: 850,
		style				: ' margin:0 auto;',
		frame				: true,		
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 8px',
		labelWidth			: 250,
		monitorValid: true,
		defaults: { 	msgTarget: 'side', 	anchor: '-20'
		},
		items				:[
			{
				layout	: 'hbox',
				title		: 'Nombre del Mandate (Persona Fisica) ',
				id			: 'divPersonaFisica',
				align    : 'center',
				width		: 800,
				items		: [datosPFisicaIzq , datosPFisicaDer ]
			},
			{
				layout	: 'hbox',
				title		: 'Nombre del Mandate (Persona Moral) ',
				id			: 'divPersonaMoral',
				align    : 'center',
				width		: 800,
				items		: [datosPMoralIzq , datosPMoralDer ]
			},
			{
				layout	: 'hbox',
				title		: 'Domicilio ',
				id			: 'divDomicilio',
				align    : 'center',
				width		: 800,
				items		: [domicilioIzq , domicilioDer ]
			},
			{
				layout	: 'hbox',
				title		: 'Datos del contacto ',
				id			: 'divContrato',
				align    : 'center',
				width		: 800,
				items		: [contratoIzq , contratoDer ]
			},
			{ 	xtype: 'textfield',  hidden: true, id: 'confirmacion_Email_C', 	value: '' }	,
			{ 	xtype: 'textfield',  hidden: true, id: 'numero_de_cliente', 	value: '' }	,
			{ 	xtype: 'textfield',  hidden: true, id: 'tipoPersona', 	value: '' }	,
			{ 	xtype: 'textfield',  hidden: true, id: 'ic_mandante', 	value: '' }	
			
		],		
		buttons: [	
			{
				text: 'Actualizar N@E',
				id: 'btnActualizar',
				iconCls: 'aceptar',				
				handler: procesoActualizar
			},			
			{
				text: 'Regresar',
				id: 'btnRegresar',
				iconCls: 'icoRegresar',
				handler: function() {
					window.location = '15forma5ext.jsp';					
				}
			},
			{
				text: 'Cancelar',
				id: 'btnCancelar',
				iconCls: 'icoCancelar',							
				handler: function() {
					window.location ="15forma2Man_ModExt.jsp?ic_mandante="+ic_mandante+"&tipoPersona="+tipoPersona;					
				}
			}		
			
		]
		
	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,						
			NE.util.getEspaciador(20)			
		]
	});

	
//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '15forma2Man_Mod.data.jsp',
		params: {
			informacion: "valoresIniciales",
			ic_mandante:ic_mandante,
			tipoPersona:tipoPersona
		},
		callback: procesaValoresIniciales
	});	
	
});
