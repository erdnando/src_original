Ext.onReady(function() {
	
	/**********CRITERIOS DE BUSQUEDA ***************/		

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: 'Pymes con R.F.C',			
				id: 'btnCaptura',					
				handler: function() {
					window.location = '15entregaAutCvebExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<b>Pymes sin R.F.C</b>',			
				id: 'btnConsulta',					
				handler: function() {
					window.location = '15entregaAutCveExt.jsp';
				}
			}	
		]
	});	
	
	//----------------------------------- HANDLER'S -------------------------------------
	
	function asignaFechas(){
		var dt = new Date();
		Ext.getCmp('_fecha_carga_ini').setValue(dt);
		Ext.getCmp('_fecha_carga_fin').setValue(dt);
	}
	
	function procesarSuccessFailureArchivoCSV(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			boton = Ext.getCmp('btnDescargar');
			boton.setIconClass('icoXls');
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	var procesarConsultaRegistros = function(store, registros, opts){

		var forma = Ext.getCmp('forma');
		forma.el.unmask();

		if (registros != null) {

			var grid  = Ext.getCmp('gridConsulta');

			if (!grid.isVisible()) {
				grid.show();
			}

			Ext.getCmp('barraPaginacion').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnDescargar').enable();
				el.unmask();
			} else {
				Ext.getCmp('btnDescargar').disable();				
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}
	}
	
	//----------------------------------- STORES -------------------------------------
	
	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'15entregaAutCveExt.data.jsp',
			baseParams: {	informacion:	'ConsultaRegistros'	},
			fields: [
				{ name: 'CG_RAZON_SOCIAL'},
				{ name: 'CG_REPRESENTANTE_LEGAL'},
				{ name: 'CG_RFC'},
				{ name: 'CG_TELEFONO'},
				{ name: 'CG_EMAIL'},
				{ name: 'DF_ALTA'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null,null);
					}
				}
			}
	});
	
	
	var accionConsulta = function(estadoSiguiente, respuesta){
	
		if(  estadoSiguiente == "CONSULTAR" ){
			var fechaMin = Ext.getCmp('_fecha_carga_ini');
			var fechaMax = Ext.getCmp('_fecha_carga_fin');
			
			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');
	
			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("gridConsulta").hide();
			Ext.StoreMgr.key('registrosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Consultar',
								operacion: 'Generar',
								start: 0,
								limit: 15
							}
				)
			});
		}else if( estadoSiguiente == "LIMPIAR" ){
			grid.hide();
			Ext.getCmp('forma').getForm().reset();
			asignaFechas();
		}
	
	}
	
	//----------------------------------- COMPONENTES -------------------------------------
	
	//Elementos del grid de la consulta
		var grid = new Ext.grid.GridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			width: 		922,
			frame: 		false,
			columns: [
				{
					header: 		'Nombre Pyme',
					tooltip: 	'Nombre de la PyME',
					dataIndex: 	'CG_RAZON_SOCIAL',
					sortable: 	true,
					align:		'left',
					resizable: 	true,
					width: 		210,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Representante Legal',
					tooltip: 	'Nombre del Representante legal',
					dataIndex: 	'CG_REPRESENTANTE_LEGAL',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		200,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'R.F.C',
					tooltip: 	'R.F.C',
					dataIndex: 	'CG_RFC',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		124,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Telefono',
					tooltip: 	'N�mero de telefono',
					dataIndex: 	'CG_TELEFONO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		105,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Email',
					tooltip: 	'Correo electr�nico',
					dataIndex: 	'CG_EMAIL',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		138,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Fecha de Registro',
					tooltip: 	'Fecha de registro',
					dataIndex: 	'DF_ALTA',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		140,
					hidden: 		false,
					hideable:	false
				}
			],bbar: {
			xtype: 			'paging',
			pageSize: 		15,
			buttonAlign: 	'left',
			id: 				'barraPaginacion',
			displayInfo: 	true,
			store: 			registrosConsultadosData,
			displayMsg: 	'{0} - {1} de {2}',
			emptyMsg: 		"No hay registros.",
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Descargar',
					width:	100,
					iconCls:	'icoXls',
					id: 		'btnDescargar',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
								url: '15entregaAutCveExt.data.jsp',
								params:	Ext.apply(fp.getForm().getValues(),
												{
														informacion:'ArchivoCSV'
												}
								),
								callback: procesarSuccessFailureArchivoCSV
						});
					}
				},'-'
			]
		}
	});
	
	
	var elementosFormaConsulta = [
				{
						xtype:		'textfield',
						fieldLabel: 'Nombre de la Pyme',
						id:			'_txt_nom_pyme',
						hiddenName:	'txt_nom_pyme',
						allowBlank:	true,
						maxLength:	100,
						hidden: false,
						anchor:	'95%',
						msgTarget: 'side'
				},{
						xtype:		'textfield',
						fieldLabel: 'R.F.C x val',
						id:			'_txt_nom_rfc',
						hiddenName:	'txt_nom_rfc',
						allowBlank:	true,
						maxLength:	15,
						hidden: false,
						anchor:	'95%',
						regex			:/^([A-Z|a-z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d]) | ([1-9]{2})([0][2])([0][1-9] | [12][0-8]) )-([a-zA-Z0-9]{3}$)/,
						regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
						'en el formato NNN-AAMMDD-XXX donde:<br>'+
						'NNN:son las iniciales del nombre y apellido<br>'+
						'AAMMDD: es la fecha de nacimiento menor al dia de hoy<br>'+
						'XXX:es la homoclave',
						listeners:{change:function(field, newValue){  field.setValue(newValue.toUpperCase()); 	}}	
				},
				{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de registro',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'datefield',
						id: '_fecha_carga_ini',
						hiddenName:	'fecha_carga_ini',
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoFinFecha: '_fecha_carga_fin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'a',
						width: 18,
						margins: '0 15 0 0'
					},
					{
						xtype: 'datefield',
						id: '_fecha_carga_fin',
						hiddenName:	'fecha_carga_fin',
						allowBlank: true,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoInicioFecha: '_fecha_carga_ini',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			}
		];
	
	var fp = new Ext.form.FormPanel({
			id:				'forma',
			width:			570,
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			labelWidth:		150,
			items:			elementosFormaConsulta,
			monitorValid:	true,
			buttons: [
				{
				text:		'Buscar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:false,
				handler: function(boton, evento) {
								if (Ext.getCmp('_txt_nom_rfc').isValid()){
									if (Ext.getCmp('_fecha_carga_ini').isValid && Ext.getCmp('_fecha_carga_fin').isValid() ){
										if ( Ext.getCmp('_fecha_carga_ini').getValue() !="" && Ext.getCmp('_fecha_carga_fin').getValue() !=""){
											accionConsulta("CONSULTAR",null);
										} else if ( Ext.getCmp('_fecha_carga_ini').getValue() =="" && Ext.getCmp('_fecha_carga_fin').getValue() ==""){
											accionConsulta("CONSULTAR",null);
										} else if ( Ext.getCmp('_fecha_carga_ini').getValue() !="" && Ext.getCmp('_fecha_carga_fin').getValue() ==""){
											Ext.getCmp('_fecha_carga_fin').focus();
										} else if ( Ext.getCmp('_fecha_carga_ini').getValue() =="" && Ext.getCmp('_fecha_carga_fin').getValue() !=""){
											Ext.getCmp('_fecha_carga_ini').focus();
										}
										
									}
								} else {
									Ext.getCmp('_txt_nom_rfc').focus();
								}
								}//fin handler
				},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
									accionConsulta("LIMPIAR",null);
							}
				}
			]
		});
	
	//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	930,
		align: 'center',
		items: 	[
			fpBotones,
			NE.util.getEspaciador(15),
			fp,
			NE.util.getEspaciador(15),
			grid
		]

	});	
	
	asignaFechas();

});