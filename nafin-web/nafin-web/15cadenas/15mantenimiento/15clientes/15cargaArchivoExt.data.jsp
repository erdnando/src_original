<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if (informacion.equals("CatalogoEstatusConsulta")){

		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("comcat_estatus_carga_pyme");
		catalogo.setOrden("ic_estatus");
		infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("Consulta")	||	informacion.equals("ArchivoCSV")){

	String icFolio = (request.getParameter("icFolio")!=null)?request.getParameter("icFolio"):"";
	String icEstatus = (request.getParameter("icEstatus")!=null)?request.getParameter("icEstatus"):"";
	String fecha_carga_ini = (request.getParameter("fecha_carga_ini")!=null)?request.getParameter("fecha_carga_ini"):"";
	String fecha_carga_fin = (request.getParameter("fecha_carga_fin")!=null)?request.getParameter("fecha_carga_fin"):"";

	int start = 0;
	int limit = 0;
	ConsultaCargaArchivoPyme cargaArchivo = new ConsultaCargaArchivoPyme();
	cargaArchivo.setClave_epo(iNoEPO);
	cargaArchivo.setIcFolio(icFolio);
	cargaArchivo.setClave_estatus(icEstatus);
	cargaArchivo.setFecha_carga_ini(fecha_carga_ini);
	cargaArchivo.setFecha_carga_fin(fecha_carga_fin);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(cargaArchivo);

	if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}
			
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
			
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
}else if (informacion.equals("DescargaZIP")){
	
	String vFolios = (request.getParameter("listFolios")!=null)?request.getParameter("listFolios"):"";
	JSONArray jsonArr = JSONArray.fromObject(vFolios);
	List listaFolios = new ArrayList();
	List fols = new ArrayList();
	
	for (int x = 0;x< jsonArr.size();x++){
		fols = new ArrayList();
		fols.add((String)jsonArr.get(x));
		listaFolios.add(fols);
	}
	CargaArchivoPyme cargaArchivo = new CargaArchivoPyme();
	cargaArchivo.setClaveEpo(iNoEPO);

	List listaDocs = new ArrayList();
	List listComprime  = new ArrayList();
	
	listaDocs = cargaArchivo.getArchivosCargados(listaFolios, strDirectorioTemp);

	for(int i=0;i<listaDocs.size();i++) {
		List lItem = (ArrayList)listaDocs.get(i);
		listComprime.add((lItem.get(0).toString()));
	}//for

	JSONObject jsonObj = new JSONObject();

	if (listComprime != null && listComprime.size() > 0){
		ComunesZIP zipear = new ComunesZIP();
		String nombreZip = Comunes.cadenaAleatoria(16)+".zip";
		String rutaNombreZip = strDirectorioTemp+nombreZip;
		zipear.comprimir(listComprime,strDirectorioTemp,rutaNombreZip);
		jsonObj.put("nombreZip", nombreZip);
		jsonObj.put("strDirecVirtualTemp", strDirecVirtualTemp);
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}else if (informacion.equals("ConsultaCargaEcon")){
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	
	String numProc = request.getParameter("numProc")==null?"":request.getParameter("numProc");
	
	System.out.println("numProcnumProcnumProcnumProcnumProc========================="+numProc);
	
	ConsCargaProvEcon consCarga = new ConsCargaProvEcon();
	List lstConsCargaArchivo = consCarga.getConsCargaEcon(numProc,"","A","","");
	jsObjArray = JSONArray.fromObject(lstConsCargaArchivo);
	
	infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
	
}else if(informacion.equals("GeneraResumenExcel")){
	JSONObject jsonObj = new JSONObject();
	String numProc = request.getParameter("numProc")==null?"":request.getParameter("numProc");
	String c_afil = request.getParameter("c_afil")==null?"":request.getParameter("c_afil");
	String c_reafil = request.getParameter("c_reafil")==null?"":request.getParameter("c_reafil");
	
	ConsCargaProvEcon consCarga = new ConsCargaProvEcon();
	String nombreArchivo = consCarga.generaArchivoPDF(strDirectorioTemp,numProc,"","");
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("GenerarCSV")){
	JSONObject jsonObj = new JSONObject();
	StringBuffer 		contenidoArchivo 	= new StringBuffer();
	CreaArchivo 		archivo 				= new CreaArchivo();
	String 				nombreArchivo 		= null;
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");	
	List lstRegistros =  JSONArray.fromObject(jsonRegistros);
	Iterator itReg = lstRegistros.iterator();
	
	contenidoArchivo.append("Numero de Proceso,EPO,Registros Totales,Registros Con Error,Registros Sin Error,Ya Existentes,Registros Totales,Duplicados,Unicos,Ya Afiliados,Reafiliaciones,Reafiliaciones Auto.,Con Contrato Unico,Por Afiliar,Ya Cargados,Por Cargar,");
	contenidoArchivo.append("Nombre de Archivo,Fecha de Carga,Datos de Carga,Procesado\n");
	
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		
		contenidoArchivo.append(registro.getString("NUMPROC")+","+(registro.getString("EPO")).replace(',',' ')+","+registro.getString("REG_TOTALES")+","+registro.getString("REG_SINERROR_NE")+","+registro.getString("REG_CONERROR_NE")+","+registro.getString("REG_PROV_YA_EXISTENTES_NE")+",");
		contenidoArchivo.append(registro.getString("REGTOTAL")+","+registro.getString("REGDUPLI")+","+registro.getString("REGUNICO")+","+registro.getString("REGYAAFIL")+","+registro.getString("REGREAFIL")+",");
		contenidoArchivo.append(registro.getString("REGREAFILAUT")+","+registro.getString("REGCONVUNICO")+","+registro.getString("REGPORAFIL")+","+registro.getString("REGCARGADOS")+","+registro.getString("REGPORCARGAR")+",");
		contenidoArchivo.append(registro.getString("NOMBREARCH")+","+"'"+registro.getString("FECHACARGA")+","+registro.getString("DATOSCARGA")+","+registro.getString("CSPROCESADO")+"\n");
	}
	
	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
	}else{
		nombreArchivo = archivo.nombre;
	}
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}
System.out.println("infoRegresar============"+infoRegresar); 
%>

<%=infoRegresar%>