Ext.onReady(function(){
	
	var sTipoUsuario =  Ext.getDom('sTipoUsuario').value;	
	var esEsquemaExtJS  =  Ext.getDom('esEsquemaExtJS').value;	
	
	var internacional = window.location.search 
                 ? Ext.urlDecode(window.location.search.substring(1)).internacional 
                 : '';
					  
	var cboTipoAfiliacion = window.location.search 
                 ? Ext.urlDecode(window.location.search.substring(1)).cboTipoAfiliacion 
                 : '0';				
	var rfc_invalido;
/*--------------------------------- Handler's -------------------------------*/

	var procesarAltaEPO =  function(opts, success, response) {
	
		var cmpForma = Ext.getCmp('formaAfiliacion');
		cmpForma.el.unmask();
		cmpForma.getForm().reset();
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var numero_Nafin = Ext.util.JSON.decode(response.responseText).lsNoNafinElectronico;
			if(numero_Nafin != null && numero_Nafin != ""){
				Ext.Msg.alert('Afiliacion EPO','Su n�mero de Nafin Electr�nico: ' + numero_Nafin,function() {
					if(internacional == 'S') 
						window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
					
					else 
						window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
				
				},this);
			}
		}
		else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	

	var procesarSuccessRFCexistente =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			rfc_invalido = 'N';
		}
		else {
			Ext.MessageBox.alert('Mensaje de p�gina web','El RFC ya existe, favor de verificaro' );
			rfc_invalido = 'S';
			return;
		}
	}
	
/*------------------------------- End Handler's -----------------------------*/
/*****************************************************************************/
 
  
  var procesarCatalogoBanco = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('id_bco_fondeo').setValue('1');
		}
	}
	
	var procesarCatalogoPais = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('id_cmb_pais').setValue('24');
		}
	}
/*---------------------------------- Store's --------------------------------*/
	function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		}else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} 	else if (pagina == 2) { 	// Distribuidor		
			if(esEsquemaExtJS=='false')  {
				window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 
			}else  {			
				window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 		 
			}
		} else if (pagina == 3) { 	//Proveedor
			if(esEsquemaExtJS=='false')  {
				window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1.jsp'; 
			}else  {
				window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma01Ext.jsp'; 
			}			
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			if(esEsquemaExtJS=='false')  {
				window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15.jsp';
			}else  {
				window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
			}
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} else if (pagina == 6) { 	//Contragarante
			if(esEsquemaExtJS=='false')  {
				window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21.jsp'; 
			}else  {
				window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 	
			}
		} else if (pagina == 9) { 	// Mandante
			if(esEsquemaExtJS=='false')  {
				window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_man.jsp'; 
			}else {
				window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
			}
		} else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		}	else if (pagina == 11) { 	//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 	
		}	else if (pagina == 12) { 	//Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliaClienteExternoExt.jsp"; 
		}
	}
	

	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [[/*'EPO'*/  '0','Cadena Productiva']
				,[/*'IF'*/	'1','Intermediario Financiero']
				,[/*'2'*/	'2','Distribuidor']
				,[/*'1'*/	'3','Proveedor']	
				,[/*'ACE'*/	'4','Afiliados Credito Electronico']
				,[/*'EPOI'*/'5','Cadena Productiva Internacional']
				,[/*'CONTRA'*/'6','Contragarante']
				//,[/*'PI'*/	'7','Proveedor Internacional']	
				//,[/*'EDOC'*/'8','Cliente Estado de Cuenta']	
				,[/*'M'*/	'9','Mandante']	
				,[/*'AF'*/	'10','Afianzadora']
				,[/*'UNI'*/ '11','Universidad-Programa Cr�dito Educativo'],
				['12','Cliente Externo']
					
				]
	});
				

	var catalogoBanco = new Ext.data.JsonStore({
	   id				: 'catBanco',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoBanco'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoBanco,
			exception: NE.util.mostrarDataProxyError
		}
	});		
	
	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoPais,
			exception: NE.util.mostrarDataProxyError
		}
  });
  
  // catalogo Estado
	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Municipio
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		
	//catalogo Identificacion
	var catalogoIdentificacion= new Ext.data.JsonStore
	({
		id				: 'catIdentificacion',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoIdentificacion'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo RamoSIAFF
	var catalogoRamoSIAFF= new Ext.data.JsonStore
	({
		id				: 'catRamoSIAFF',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoRamoSIAFF'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo UnidadSIAFF
	var catalogoUnidadSIAFF= new Ext.data.JsonStore
	({
		id				: 'catUnidadSIAFF',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoUnidadSIAFF'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Sector ECON�MICO
	var catalogoSectorECO= new Ext.data.JsonStore
	({
		id				: 'catSectorECO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoSectorECO'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo SubSector ECON�MICO
	var catalogoSubSectorECO= new Ext.data.JsonStore
	({
		id				: 'catSubSectorECO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoSubSectorECO'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Rama ECON�MICA
	var catalogoRamaECO= new Ext.data.JsonStore
	({
		id				: 'catRamaECO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoRamaECO'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});	
	
	//catalogo SectorEPO
	var catalogoSectorEPO= new Ext.data.JsonStore
	({
		id				: 'catSectorEPO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoSectorEPO'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Subdireccion
	var catalogoSubdireccion= new Ext.data.JsonStore
	({
		id				: 'catSubdireccion',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoSubdireccion'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Promotor
	var catalogoPromotor= new Ext.data.JsonStore
	({
		id				: 'catPromotor',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma0Ext.data.jsp',
		baseParams	: { informacion: 'catalogoPromotor'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});	  

/*--------------------------------- End Store's -----------------------------*/
/*****************************************************************************/


/*-------------------------------- Componentes ------------------------------*/
	
	var afiliacion = {
		xtype		: 'panel',
		id 		: 'afiliacion',
		layout	: 'hbox',
		border	: false,		
		width		: 900,		
		bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:110px;',
		items		: 
		[{
			layout		: 'form',
			width			:400,
			labelWidth	: 60,
			border		: false,
			items			:[{
					xtype				: 'combo',
					id					: 'id_afiliacion',
					name				: 'afiliacion',
					hiddenName 		:'afiliacion', 
					fieldLabel		: 'Afiliaci�n',
					width				: 250,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',
					value				: '0',
					store				: storeAfiliacion,
					listeners: {
						select: {
							fn: function(combo) {
								var valor  = combo.getValue();
								creditoElec(valor);		
							}
						}
					}					
				}]
			},
			{
				layout	: 'form',
				width		:400,
				border	: false,
				hidden	:true,
				items		:[{
					xtype				: 'combo',
					id					: 'id_bco_fondeo',
					name				: 'bco_fondeo',
					hiddenName 		: 'bco_fondeo',
					fieldLabel		: 'Banco de Fondeo',
					width				: 90,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					value				: 1,
					store				: catalogoBanco
				}]
			}
	]};
	

	var afiliacionPanel = {
		xtype		: 'panel',
		id 		: 'afiliacionPanel',
		items		: [
			{
				layout		: 'form',
				labelWidth	: 150,
				bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:2px;',
				items		:	
				[
					{
						xtype			: 'textfield',
						name			: 'Razon_Social',
						id				: '* Razon_Social',
						fieldLabel	: '* Raz�n Social',
						maxLength	: 100,
						width			: 350,
						allowBlank	: false,
						tabIndex		:	1
					},
					{
						xtype			: 'textfield',
						name			: 'nombre_comercial',
						id				: 'nombre_comercial',
						fieldLabel	: '* Nombre Comercial',
						maxLength	: 150,
						width			: 250,
						allowBlank	: false,
						tabIndex		:	2
					},
					
					// Internacional ////////////////////////////////////////////////
					
					{
						xtype : 'panel', 
						border: false,
						width : 900,
						layout: 'hbox',
						items: [
							{
								layout : 'form',
								width  : 413,
								border : false,
								items	 :[{
										xtype 		: 'textfield',
										name  		: 'rfc',
										id    		: 'rfc',
										fieldLabel	: '* R.F.C.',
										maxLength	: 20,
										width			: 150,
										allowBlank 	: true,
										//hidden		: true,
										regex			:/^([A-Z|a-z|&amp;]{3})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
										regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
										'en el formato NNN-AAMMDD-XXX donde:<br>'+
										'NNN:son las iniciales del nombre de la empresa<br>'+
										'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
										'XXX:es la homoclave',
										tabIndex		:	3,
										listeners	:
										{
											blur : {
												
												fn : function() {
													var rf = (Ext.getCmp('rfc')).getValue();
													Ext.Ajax.request({
														url: '15forma0Ext.data.jsp',
														params: Ext.apply( {										
																informacion: 'ConsultaRFCexistente',
																rf: rf
														}),
														callback: procesarSuccessRFCexistente
													});
												}
											}
										}
									},
									{
										xtype 		: 'textfield',
										name  		: 'rfc',
										id    		: 'tax',
										fieldLabel	: '* Tax ID Number',
										maxLength	: 20,
										width			: 150,
										allowBlank 	: true,
										hidden		: true,
										tabIndex		:	3,
										listeners	:
										{
											blur : {
												
												fn : function() {
													var rf = (Ext.getCmp('tax')).getValue();
													Ext.Ajax.request({
														url: '15forma0Ext.data.jsp',
														params: Ext.apply( {										
																informacion: 'ConsultaRFCexistente',
																rf: rf
														}),
														callback: procesarSuccessRFCexistente
													});
												}
											}
										}
									}]
							},
							
							{
								layout : 'form',	
								width  :400,
								border : false,
								items  :[{
										xtype			: 'textfield',
										name			: 'Dab_Number',
										id				: 'Dab_Number',
										maxLength	: 9,
										width			: 150,
										fieldLabel	: 'DaB Number',
										hidden		: true,
										allowBlank	: true,
										tabIndex		:	5
									}]
							}
					]},
			// Internacional /////////////////////////////////////////////////
					{
						xtype			: 'numberfield',
						id          : 'num_escritura',
						name			: 'num_escritura',
						fieldLabel  : '** N�mero de Escritura',
						maxLength	: 20,
						width			: 150,
						allowBlank	: false,
						tabIndex		:	6
					},
					{
						xtype			: 'datefield',
						id				: 'fecha_cons',
						name			: 'fecha_cons',					
						fieldLabel	: '** Fecha de Constituci�n',
						allowBlank 	: false,
						width			: 120,
						msgTarget	: 'side',
						tabIndex		:	7,
						margins		: '0 20 0 0' 
					},
					{
						layout		: 'form',	
						width			: 200,
						labelWidth	: 200,
						border		: false,
						items			:[{
							xtype			: 'label',
							id				:	'etiqueta',
							fieldLabel	:'** Solo Aplica para Distribuidores'
						}]
					}
			]}
	]};
//---------------------------------------------------------------------------//

	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calle',
				name			: 'calle',
				fieldLabel  : '* Calle',
				maxLength	: 100,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	8

			},
			{
				xtype			: 'numberfield',
				id          : 'num_int',
				name			: 'num_int',
				fieldLabel  : 'N�mero interior',
				maxLength	: 25,
				width			: 90,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 10
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais',
				name				: 'cmb_pais',
				hiddenName 		: 'cmb_pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 150,
				tabIndex			: 12,
				store				: catalogoPais,
				listeners: {
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado').reset();
						Ext.getCmp('id_cmb_delegacion').reset();
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
							
							
					}
				}
				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacion',
				name				: 'cmb_delegacion',
				hiddenName 		: 'cmb_delegacion',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				tabIndex			: 14,
				store				: catalogoMunicipio
				
			},
			
			
			{
				xtype			: 'textfield',
				id          : 'telefono',
				name			: 'telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: true,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30,
				tabIndex		: 16
				
			},
			{
				xtype			: 'textfield',
				id          : 'fax',
				name			: 'fax',
				fieldLabel  : '* Fax',
				width			: 230,
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	18
			}
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'numberfield',
				id          : 'num_ext',
				name			: 'num_ext',
				fieldLabel  : '* N�mero Exterior',
				allowBlank	: true,
				margins		: '0 20 0 0',
				maxLength	: 25,
				width			: 90,
				tabIndex		: 9
			},
			{
				xtype			: 'textfield',
				id          : 'colonia',
				name			: 'colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 100,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 11
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_estado',
				name				: 'cmb_estado',
				hiddenName 		: 'cmb_estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				allowBlank		: false,
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				tabIndex			: 13,
				store				: catalogoEstado,
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_delegacion').reset();
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais')).getValue()
							})
						});
					}
				}
				
			},
			{
				xtype			: 'textfield',
				id          : 'id_code',
				name			: 'code',
				fieldLabel  : '* C�digo Postal',
				allowBlank	: true,
				hidden		: true,
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 90,
				tabIndex		: 15
			},
			{
				xtype			: 'textfield',
				id          : 'id_code2',
				name			: 'code',
				fieldLabel  : '* Zip Code',
				allowBlank	: true,
				hidden		: true,
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 90,
				tabIndex		: 15
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_email',
				name			: 'email',
				fieldLabel: ' E-mail',
				allowBlank	: true,
				maxLength	: 100,
				width			: 230,
				tabIndex		: 17,
				margins		: '0 20 0 0',
				vtype: 'email'
			}
		]
	};
	
	/*--____Fin Domicilio ____--*/
//---------------------------------------------------------------------------//

	/*--____ Datos del Representante Legal ____--*/
	
	var datosRepresentante_izq = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_izq',
		//title 	: 'Domicilio',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_legal',
				name			: 'paterno',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 19
			},
			{
				xtype			: 'textfield',
				id          : 'nombre_legal',
				name			: 'nombre',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 21
			},
			{
				xtype			: 'textfield',
				id          : 'num_iden',
				name			: 'num_iden',
				fieldLabel  : 'No. de Identificaci�n',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 23
			},
			{
				xtype			: 'textfield',
				id          : 'SSId_Number',
				name			: 'SSId_Number',
				fieldLabel  : 'SS Id Number',
				maxLength	: 8,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: true,
				hidden		: true,
				tabIndex		: 24
			},
			{
				xtype			: 'numberfield',
				id          : 'num_poderes',
				name			: 'poderes',
				fieldLabel  : 'Poderes y Facultades No. de Escritura',
				maxLength	: 40,
				width			: 230,
				//margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 25
			}
		]
	};
		
	var datosRepresentante_der = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_der',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_legal',
				name			: 'materno',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 20
			},			
			{
				xtype				: 'combo',
				id          	: 'id_cmb_identificacion',
				name				: 'cmb_identificacion',
				hiddenName 		: 'cmb_identificacion',
				fieldLabel  	: 'Identificaci�n ',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 22,
				store				: catalogoIdentificacion
			},
			
			{ 	xtype: 'displayfield',  value: '',height:20 }	,
			{
				xtype			: 'datefield',
				id          : 'fch_poder',
				name			: 'fch_poder',
				allowBlank	: false,
				width			: 130,
				fieldLabel  : '* Fecha del poder notarial',
				margins		: '0 20 0 0',
				tabIndex		: 26
			},
			{ 	xtype: 'displayfield',  value: '',height:20 }	
		]
	};
	
	/*--____Fin Datos del Representante Legal ____--*/
//---------------------------------------------------------------------------//


/*--____ Datos del Contacto  ____--*/
	
	var datosContacto_izq = {
		xtype			: 'fieldset',
		id 			: 'datosContacto_izq',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_contacto',
				name			: 'paterno_contacto',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				margins		: '0 20 0 0',
				width			: 230,
				allowBlank	: false,
				tabIndex		: 27
			},
			{
				xtype			: 'textfield',
				id          : 'id_nombre_contacto',
				name			: 'nombre_contacto',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 29
			},
			
			{
				xtype			: 'textfield',
				id          : 'faxc',
				name			: 'faxc',
				fieldLabel  : 'Fax',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 31
			}
		]
	};
		
	var datosContacto_der = {
		xtype			: 'fieldset',
		id 			: 'datosContacto_der',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_contacto',
				name			: 'materno_contacto',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 28
			},
			{
				xtype			: 'textfield',
				id          : 'telc',
				name			: 'telc',
				fieldLabel  : '*Tel�fono',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 30
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_emailc',
				name			: 'emailc',
				fieldLabel: ' E-mail',
				maxLength	: 50,
				width			: 230,
				margins		: '0 20 0 0',
				vtype			: 'email',
				allowBlank	: true,
				tabIndex		: 32
			}
		]
	};
	
	/*--____Fin Datos del Contacto  ____--*/
//---------------------------------------------------------------------------//	

/*--____ Otros Datos ____--*/
	
	var otrosDatos_izq = {
		xtype		: 'fieldset',
		id 		: 'otrosDatos_izq',
		border	: false,
		labelWidth	: 150,
		items		: 
		[		
			{
				xtype			: 'textfield',
				id          : 'num_sirac',
				name			: 'num_sirac',
				fieldLabel  : 'No. Cliente SIRAC',
				maxLength	: 12,
				width			: 120,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 36,
				regex:				/^[0-9]*$/,
				regexText:			'Solo debe contener n�meros'
			},
			{
				xtype			: 'textfield',
				id          : 'bco_retiro',
				name			: 'bco_retiro',
				fieldLabel  : 'Banco de Retiro',
				maxLength	: 100,
				width			: 160,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 38
			},
			
			{
				xtype			: 'textfield',
				id          : 'cuenta',
				name			: 'cuenta',
				fieldLabel  : 'Cuenta de Retiro',
				maxLength	: 25,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 40
			},
			{
						layout		: 'form',	
						width			: 400,
						labelWidth	: 400,
						border		: false,
						items			:[{
							xtype			: 'label',
							id				:	'etiqueta2',
							fieldLabel	:'** No aplica para financiamiento a Distribuidores'
						}]
			},
			/*-------------F0DEA-2012-------------*/
			{
				xtype				: 'combo',
				id          	: 'id_cmb_economico',
				name				: 'cmb_economico',
				hiddenName 		: 'cmb_economico',
				fieldLabel  	: 'Sector Econ�mico',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 200,
				emptyText		: 'Seleccione Sector Econ�mico',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 42,
				store				: catalogoSectorECO,
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_subsectoreco').reset();
						  Ext.getCmp('id_cmb_ramaECO').reset();
						 catalogoSubSectorECO.load({
							params: Ext.apply({
								sectorECO :record.json.clave
							})
						});
					}
				}
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_subsectoreco',
				name				: 'cmb_subsectoreco',
				hiddenName 		: 'cmb_subsectoreco',
				fieldLabel  	: 'Subsector Econ�mico',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 270,
				emptyText		: 'Seleccione Subsector Econ�mico',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 43,
				store				: catalogoSubSectorECO,
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_ramaECO').reset();
						 catalogoRamaECO.load({
							params: Ext.apply({
								sectorECO	: (Ext.getCmp('id_cmb_economico')).getValue(),
								subsectorECO :record.json.clave							
							})
						});
					}
				}
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_ramaECO',
				name				: 'cmb_ramaECO',
				hiddenName 		: 'cmb_ramaECO',
				fieldLabel  	: 'Rama Econ�mica',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 270,
				emptyText		: 'Seleccione Rama Econ�mica',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 44,
				store				: catalogoRamaECO
			},
			/*-------------Fin FODEA 2012-------------*/
			
			/*-------------F057-2010-------------*/
			{
				xtype				: 'combo',
				id          	: 'id_cmb_sector',
				name				: 'cmb_sector',
				hiddenName 		: 'cmb_sector',
				fieldLabel  	: 'Sector EPO',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 160,
				emptyText		: 'Seleccione Sector',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 45,
				store				: catalogoSectorEPO
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_subdirec',
				name				: 'cmb_subdirec',
				hiddenName 		: 'cmb_subdirec',
				fieldLabel  	: 'Subdireccion',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 270,
				emptyText		: 'Seleccione Subdirecci�n',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 46,
				store				: catalogoSubdireccion
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_promotor',
				name				: 'cmb_promotor',
				hiddenName 		: 'cmb_promotor',
				fieldLabel  	: 'Lider Promotor',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 270,
				emptyText		: 'Seleccione Lider Promotor',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 47,
				store				: catalogoPromotor 
			}
		]
	};
		
	var otrosDatos_der = {
		xtype		: 'fieldset',
		id 		: 'otrosDatos_der',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'checkbox',
				id				: 'id_ditribuidores',
				name			: 'ditribuidores',
				hiddenName	: 'ditribuidores',
				fieldLabel	: 'Financiamiento a Distribuidores ',
				tabIndex		: 37,
				enable 		: true
			},
			{
				xtype			: 'checkbox',
				id				: 'id_convenio',
				name			: 'convenio',
				hiddenName	: 'convenio',
				fieldLabel	: 'Opera Convenio �nico',
				tabIndex		: 39,
				checked		: true,
				enable 		: true
			},
			/*------FODEA 012 - 2010 ACF--------*/
			{
				xtype			: 'checkbox',
				id				: 'id_irrevocable',
				name			: 'irrevocable',
				hiddenName	: 'irrevocable',
				fieldLabel	: 'Opera Instrucci�n Irrevocable',
				hidden		: true,
				tabIndex		: 41,
				enable 		: true
			}
			
		]
	};
	
	var otrosDatos_ = {
		xtype		: 'panel',
		id 		: 'otrosDatos_',
		layout	: 'form',
		title 	: 'Otros Datos',
		border	: false,
		labelWidth	: 150,
		items		: 
		[{
				xtype			: 'radiogroup',
				msgTarget	: 'side',
				id				: 'id_c_proveedores',
				name			: 'c_proveedores',
				hiddenName	: 'c_proveedores',
				fieldLabel	: 'Base de Operaci�n Especifica',
				cls			: 'x-check-group-alt',
				columns		: [50, 50],
				valueField	:'base',
				tabIndex		: 33,
				items			: [
					{	boxLabel	: 'SI',	name : 'c_proveedores', inputValue: 'S'	},
					{	boxLabel	: 'NO',	name : 'c_proveedores', inputValue: 'N',	checked : true }
		]},
		/*------FODEA 034 - 2009 ACF (I)---------*/
		{
				xtype				: 'combo',
				id          	: 'id_cmb_ramo',
				name				: 'cmb_ramo',
				hiddenName 		: 'cmb_ramo',
				fieldLabel  	: 'Ramo SIAFF',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 600,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 34,
				store				: catalogoRamoSIAFF,
				listeners: {
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_unidad').reset();
							catalogoUnidadSIAFF.load({
								params: Ext.apply({
									ramo: record.json.clave
							})
						});
					}
				}
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_unidad',
				name				: 'cmb_unidad',
				hiddenName 		: 'cmb_unidad',
				fieldLabel  	: 'Unidad SIAFF',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 350,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 35,
				store				: catalogoUnidadSIAFF
			},
			{
						layout		: 'form',	
						width			: 900,
						height		: 35,
						labelWidth	: 800,
						border		: false,
						items			:[{
							xtype			: 'label',
							id				:	'etiqueta3',
							fieldLabel	:'Se utilizar� la Base de Operaci�n Gen�rica y el plazo m�ximo de factoraje, para Moneda Nacional: 360 y para D�lares: 360.'
						}]
			}
		]
		
	};
	/*--____ Fin Otros Datos ____--*/
//---------------------------------------------------------------------------//	

	
	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			{
				layout	: 'hbox',
				title		: '',
				width		: 940,
				items		: [afiliacion ]
			},
			{
				layout	: 'hbox',
				title		: '',
				//height	: 280,
				width		: 940,
				items		: [afiliacionPanel ]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 940,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			},
			{
				layout	: 'hbox',
				title		: 'Datos del Representante Legal',
				width		: 940,
				items		: [datosRepresentante_izq, datosRepresentante_der ]
			},
			{
				layout	: 'hbox',
				title 	: 'Datos del Contacto',
				width		: 940,
				items		: [datosContacto_izq, datosContacto_der ]
			},
			otrosDatos_ ,
			{
				layout	: 'hbox',
				title		: '',
				width		: 940,
				items		: [otrosDatos_izq, otrosDatos_der ]
			}
		],
		monitorValid	: true,
		buttons			: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				formBind: true,
				
				handler: function(boton, evento) 
				{
					/*****************Validaciones***************/
					if(rfc_invalido == 'S'){
						Ext.MessageBox.alert('Mensaje de p�gina web','El RFC ya existe, favor de verificaro' );
						return;
					}
					var ramo = Ext.getCmp('id_cmb_ramo');
					if(!Ext.isEmpty(ramo.getValue())){
						var unidad = Ext.getCmp('id_cmb_unidad');
						if(Ext.isEmpty(unidad.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Seleccione una unidad SIAFF para la Cadena Productiva.' );
							unidad.focus();
							return;
						}
					}
					var chkFinanciamiento = Ext.getCmp('id_ditribuidores');
					if(chkFinanciamiento.getValue() == 'S' ) {
						var cboTipoAfiliacion = Ext.getCmp('id_afiliacion');
						if(!Ext.isEmpty(cboTipoAfiliacion.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Seleccione una Afiliacion' );
							cboTipoAfiliacion.focus();
							return;
						}
					}
					
					/* Validaciones de acuerdo a el tipo de cadena productiva
							 * Nacional/Internacional
							 */
							if(internacional == 'S'){ 
								var rfc = Ext.getCmp('tax');
								var dab = Ext.getCmp('Dab_Number');
								var cp  = Ext.getCmp('id_code2');
								
								if (Ext.isEmpty(rfc.getValue())) {
									rfc.markInvalid('El campo RFC es requerido');
									rfc.focus();
									return;
								}
								if (Ext.isEmpty(cp.getValue())) {
									cp.markInvalid('El campo C�digo Postal es requerido');
									cp.focus();
									return;
								}
							}	
							else{
								var rfc = Ext.getCmp('rfc');
								var cp  = Ext.getCmp('id_code');
								var id_i= Ext.getCmp('id_irrevocable');
								
								if (Ext.isEmpty(rfc.getValue())) {
									rfc.markInvalid('El campo RFC es requerido');
									rfc.focus();
									return;
								}
								if (Ext.isEmpty(cp.getValue())) {
									cp.markInvalid('El campo C�digo Postal es requerido');
									cp.focus();
									return;
								}
							}
							
					// --Numericas--
					
					var num_cod = Ext.getCmp('id_code');
					if (!esVacio(num_cod.getValue())){//if(!Ext.isEmpty(num_cod)){
						if (!isdigit(num_cod.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo C�digo Postal ' );
							num_cod.focus();
							return;
					}}		
					
					var num_cod2 = Ext.getCmp('id_code2');
					if (!esVacio(num_cod2.getValue())){//if(!Ext.isEmpty(num_cod)){
						if (!isdigit(num_cod2.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo C�digo Postal ' );
							num_cod2.focus();
							return;
					}}		
					
					var num_cta = Ext.getCmp('cuenta');
					if (!esVacio(num_cta.getValue())){//if(!Ext.isEmpty(num_cta)){
						if (!isdigit(num_cta.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo N�mero de la Cuenta de Retiro' );
							num_cta.focus();
							return;
					}}		
					
					var num_tel = Ext.getCmp('telefono');
					if (!esVacio(num_tel.getValue())){//if(!Ext.isEmpty(num_tel)){
						if (!isdigit(num_tel.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo Tel�fono ' );
							num_tel.focus();
							return;
					}}		
					
					var num_telc = Ext.getCmp('telc');
					if (!esVacio(num_telc.getValue())){//if(!Ext.isEmpty(num_tel)){
						if (!isdigit(num_telc.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo Tel�fono de Datos del Contacto ' );
							num_telc.focus();
							return;
					}}		
					var num_fax = Ext.getCmp('fax');
					if (!esVacio(num_fax.getValue())){//if(!Ext.isEmpty(num_fax)){
						if (!isdigit(num_fax.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo Fax ' );
							num_fax.focus();
							return;
					}}		
					
					var num_fax2 = Ext.getCmp('faxc');
					if (!esVacio(num_fax2.getValue())){//if(!Ext.isEmpty(num_fax)){
						if (!isdigit(num_fax2.getValue())){
							Ext.MessageBox.alert('Mensaje de p�gina web','Verifique el formato del campo Fax de Datos del Contacto' );
							num_fax2.focus();
							return;
					}}		
				
					
					
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							var cmpForma = Ext.getCmp('formaAfiliacion');
							var params = (cmpForma)?cmpForma.getForm().getValues():{};				
							cmpForma.el.mask("Procesando", 'x-mask-loading');
							//peticion de Registro de Afiliaci�n
							Ext.Ajax.request({
									url: '15forma0Ext.data.jsp',
									params: Ext.apply(params,{
										
											informacion: 'AltaEPO',
											internacional: internacional
									}),
									callback: procesarAltaEPO
							});
						}
					});		
				}
			
			},
			{
				text:'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15forma0Ext.jsp';
				}
			}
		]
	});			
	
	       
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items: //fp
		[ fpDatos ]
	});

	if(internacional == 'S'){ 
		Ext.getCmp('tax').setVisible(true);
		Ext.getCmp('Dab_Number').setVisible(true);
		Ext.getCmp('id_code2').setVisible(true);
		Ext.getCmp('SSId_Number').setVisible(true);
		Ext.getCmp('rfc').hide();//.setVisible(false);
		Ext.getCmp('id_irrevocable').setVisible(false);
	}
	else{
		var rt=Ext.getCmp('rfc');
		rt.show();
		Ext.getCmp('rfc').show();//setVisible(true);
		Ext.getCmp('id_code').setVisible(true);
		Ext.getCmp('id_irrevocable').setVisible(true);
		Ext.getCmp('tax').setVisible(false);
		Ext.getCmp('Dab_Number').setVisible(false);
		Ext.getCmp('SSId_Number').setVisible(false);
	}
	
	
	Ext.getCmp('id_afiliacion').setValue(cboTipoAfiliacion);
	if(cboTipoAfiliacion == null){
		Ext.getCmp('id_afiliacion').setValue('0');
	}
	Ext.getCmp('etiqueta').label.update('** Solo Aplica para Distribuidores');
	Ext.getCmp('etiqueta2').label.update('** No aplica para financiamiento a Distribuidores');
	Ext.getCmp('etiqueta3').label.update('Se utilizar� la Base de Operaci�n Gen�rica y el plazo m�ximo de factoraje, para Moneda Nacional: 360 y para D�lares: 360.');
	
	
		
	//catalogoBanco.load( );
	catalogoPais.load();
	catalogoEstado.load({
								params : Ext.apply({
									pais : '24'
								})
							} );
	catalogoIdentificacion.load();
	catalogoRamoSIAFF.load();
	catalogoSectorECO.load();
	catalogoSectorEPO.load();
	catalogoSubdireccion.load(); 
	catalogoPromotor.load();
	

});