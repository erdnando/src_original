<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	netropology.utilerias.*,	
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 

/*** OBJETOS ***/

CQueryHelperRegExtJS queryHelperRegExtJS;
JSONObject jsonObj;
Registros registros;
JSONArray jsonArr;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String ic_if ="";
String ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
//String txt_nombre			= (request.getParameter("hid_nombre")==null)?"":request.getParameter("hid_nombre");
//String txt_nafelec		= (request.getParameter("txt_nafelec")==null)?"":request.getParameter("txt_nafelec");
String txt_nafelec		= (request.getParameter("nafelec")==null)?"":request.getParameter("nafelec");
String txt_usuario		= (request.getParameter("txt_usuario")==null)?"":request.getParameter("txt_usuario");
String txt_fecha_acep_de= (request.getParameter("txt_fecha_acep_de")==null)?"":request.getParameter("txt_fecha_acep_de");
String txt_fecha_acep_a	= (request.getParameter("txt_fecha_acep_a")==null)?"":request.getParameter("txt_fecha_acep_a");
String tipoModificacion			= (request.getParameter("modificacion")==null)?"":request.getParameter("modificacion");
String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String ic_epo				= (request.getParameter("noIc_epo")==null)?"":request.getParameter("noIc_epo");

String resultado			= null;
/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/

if(informacion.equals("catalogoNombreEPO")){	
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("IC_EPO");
		catalogo.setCampoDescripcion("CG_RAZON_SOCIAL");
		catalogo.setTabla("COMCAT_EPO");
		catalogo.setOrden("CG_RAZON_SOCIAL");
	resultado = catalogo.getJSONElementos();		
} /*** FIN CATALOGO NOMBRE EPO ***/

/*** INICIO CONSULTA ***/
else if(informacion.equals("Consulta") || informacion.equals("ArchivoCSV") ) { 
	jsonObj = new JSONObject();
	ConsPromocionPyme paginador = new ConsPromocionPyme();
	String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
	/* SE PASAN LOS VALORES AL OBJETO PAGINADOR PARA REALIZAR LA CONSULTA */
	paginador.setEpo(ic_epo);
	paginador.setNafin(txt_nafelec);
	paginador.setPyme(ic_pyme);
	paginador.setUsuario(txt_usuario);
	paginador.setFechaInicial(txt_fecha_acep_de);
	paginador.setFechaFinal(txt_fecha_acep_a);
	paginador.setModificacion(tipoModificacion);

	queryHelperRegExtJS	= new CQueryHelperRegExtJS(paginador); 
	if (informacion.equals("Consulta") ){ 
		try {
			Registros reg	=	queryHelperRegExtJS.doSearch();
			String consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);
		}catch(Exception e) {
			throw new AppException("Error en la paginaci�n", e);
		}
	}else  if(informacion.equals("ArchivoCSV") ) {					
		try {
			String nombreArchivo = queryHelperRegExtJS.getCreateCustomFile(request, strDirectorioTemp, "CSV");				

			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		}catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
	resultado = jsonObj.toString();	
} /*** FIN DE CONSULTA ***/

/*** INICIA CATALOGO DE BUSQUEDA AVANZADA ***/
else if (informacion.equals("CatalogoBuscaAvanzada")) {
	String num_pyme	= (request.getParameter("claveProveedor")==null ||	request.getParameter("claveProveedor").equals(""))
		?"":request.getParameter("claveProveedor");
	String rfc_pyme		= (request.getParameter("rfc_pyme")==null || 	request.getParameter("rfc_pyme").equals(""))
		?"":request.getParameter("rfc_pyme");
	String nombre_pyme	= (request.getParameter("nombre_pyme")==null || 	request.getParameter("nombre_pyme").equals(""))
		?"":request.getParameter("nombre_pyme");
	
	String ic_producto_nafin = (request.getParameter("ic_producto_nafin")==null)?"":request.getParameter("ic_producto_nafin");
	ic_epo= request.getParameter("epo")==null ?"":request.getParameter("epo");	
	String pantalla ="";// "InformacionDocumentos";
	
	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion afiliacion = afiliacionHome.create();
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
 
	registros = afiliacion.getProveedores(ic_producto_nafin, ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pyme,pantalla,ic_if);
	
	List coleccionElementos = new ArrayList();
	jsonArr = new JSONArray();
	JSONObject jsonObjP;
						
	while (registros.next()){
		jsonObjP= new JSONObject();		
		jsonObjP.put("clave", registros.getString("IC_NAFIN_ELECTRONICO"));
		jsonObjP.put("descripcion", registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("CG_RAZON_SOCIAL"));
		jsonObjP.put("ic_pyme",registros.getString("IC_PYME"));
		jsonArr.add(jsonObjP);
	}	
	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	resultado = jsonObj.toString();

} /*** FIN CATALOGO BUSQUEDA AVANZADA ***/

%>
<%= resultado%>
