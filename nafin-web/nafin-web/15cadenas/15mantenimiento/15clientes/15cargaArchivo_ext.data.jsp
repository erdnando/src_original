<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	JSONObject archivoCSV = new JSONObject();
	JSONObject archivoPDF = new JSONObject();
	JSONObject registroAcuse = new JSONObject();
	JSONArray arrAcuse = new JSONArray();
	boolean flag = true;
	ParametrosRequest req = null;
	String rutaArchivoTemporal = null;
	String itemArchivo = "";
	String extension = "";
	String result = "";
	String folio = "";
	String fechaHora = "";
	int tamanio = 0;
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(strDirectorioTemp));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		extension = (req.getParameter("hidExtension") == null)? "" : req.getParameter("hidExtension");
		
		try{
			int tamaPer = 0;
			if (extension.equals("xls")){
				upload.setSizeMax(700 * 1024);	//700 Kb
				tamaPer = (700 * 1024);
			}else if (extension.equals("xlsx")){
				upload.setSizeMax(150 * 1024);	//150 Kb
				tamaPer = (150 * 1024);
			}

			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName();
			InputStream archivoExcel = fItem.getInputStream();
			tamanio			= (int)fItem.getSize();

			if (tamanio > tamaPer){
				flag = false;
			}
			if (flag){
				CargaArchivoPyme cargaArchivo = new CargaArchivoPyme();
				cargaArchivo.setClaveEpo(iNoEPO);
				cargaArchivo.setExtension(extension);
				cargaArchivo.setSize(tamanio);
				fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
				folio = cargaArchivo.insertaCargaArchivoPyme(itemArchivo, archivoExcel, iNoUsuario, strNombreUsuario, fechaHora);

				registroAcuse.put("IC_FOLIO",folio);
				registroAcuse.put("CG_NOMBRE_DOCTO",itemArchivo);
				registroAcuse.put("DF_CARGA",fechaHora);
				registroAcuse.put("CG_USUARIO",iNoUsuario);
				registroAcuse.put("CG_NOMBRE_USUARIO",strNombreUsuario);
				registroAcuse.put("IC_ESTATUS_CARGA_PYME",new Integer(1));
				registroAcuse.put("CG_DESCRIPCION","Recibida Nafin");
				arrAcuse.add(registroAcuse);
				
				try{
					StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
					CreaArchivo archivo = new CreaArchivo();
					String nombreArchivo = Comunes.cadenaAleatoria(16)+".csv";
					
					contenidoArchivo.append("N�mero Folio,Nombre Archivo,Fecha y Hora de Carga,Usuario,Estatus \n");
					contenidoArchivo.append(folio+","+itemArchivo+","+ fechaHora +","+ iNoUsuario + " " + strNombreUsuario.replace(',',' ') + ",Recibida Nafin," + "\n");
					String msg = "";
					if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
						msg = "Error al generar el archivo";
					} else {
						nombreArchivo = archivo.nombre;
					}
					archivoCSV.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					archivoCSV.put("msg", msg);
				}catch (Exception e){
						throw new AppException("Ocurrio un error al generar el archivo", e);
				}
			
				try{
					StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
					String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
					
					ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					
					pdfDoc.setTable(5, 100);
					pdfDoc.setCell("Acuse de recepci�n de archivo de proveedores para carga", "formasmenB", ComunesPDF.CENTER,5);
					pdfDoc.setCell("N�mero Folio", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre Archivo", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha y Hora de Carga", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("Usuario", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("Estatus", "formasmenB", ComunesPDF.CENTER);
			
					pdfDoc.setCell(folio, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(itemArchivo, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(fechaHora, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(iNoUsuario + " " + strNombreUsuario.replace(',',' '), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("Recibida Nafin", "formasmen", ComunesPDF.CENTER);
					
					pdfDoc.addTable();
					pdfDoc.endDocument();

					archivoPDF.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			
				}catch (Exception e){
						throw new AppException("Ocurrio un error al generar el archivo", e);
				}
			}
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}
	
%>
{
	"success": true,
	"flag":	<%=(flag)?"true":"false"%>,
	"acuse":	<%=arrAcuse%>,
	"archivoCSV":	<%=archivoCSV%>,
	"archivoPDF":	<%=archivoPDF%>
}