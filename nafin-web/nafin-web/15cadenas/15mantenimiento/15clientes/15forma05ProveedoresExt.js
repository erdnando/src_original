	Ext.onReady(function(){

		var strPerfil = Ext.getDom('strPerfil').value;
		var idMenu = Ext.getDom('idMenu').value;
		var permisosGlobales;
		var cadenaProductiva;
		
                
		
		function procesaValoresIniciales(opts, success, response) {
		
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var  info = Ext.util.JSON.decode(response.responseText);			
				permisosGlobales=info.permisos;	
					
				if(strPerfil!=='PROMO NAFIN' && strPerfil!=='ADMIN OPERC' ){	
				Ext.getCmp('modif_propietario1').show();
				Ext.getCmp('pyme_sin_proveedor1').show();
				Ext.getCmp('convenio_unico1').show();
				Ext.getCmp('opera_fideicomiso1').show();		
			}
				
				
			}else {
				NE.util.mostrarConnError(response,opts);
			}
		}
		
		function procesarPermiso(opts, success, response) {
			
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var  info = Ext.util.JSON.decode(response.responseText);			
				
				actualizaNafin();
					
			} else{
				NE.util.mostrarConnError(response,opts);
			}
			
		}
		
		function validaPermisos(ccPermiso) {	
			
			Ext.Ajax.request({
				url: '15forma05ProveedoresExt.data.jsp', 
				params: {
					informacion: "validaPermiso", 
					idMenuP:idMenu,
					ccPermiso:ccPermiso					
				},
				callback: procesarPermiso
		});		
		}
					
	/**************************************************************************************************************
	 *                                             OVERRIDES                                                      *
	 * Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del componente.            *
	 * Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth  *
	 * BY: Jesus Salim Hernandez David.                                                                           *
	 **************************************************************************************************************/
		Ext.override(Ext.Element,{
			getWidth: function(contentWidth){
				var me = this,
				dom    = me.dom,
				hidden = Ext.isIE && me.isStyle('display', 'none'),
				//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
				w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
				w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
				return w < 0 ? 0 : w;
			}
		});

		var reafiliarPor; //Para la secci�n de reafiliaci�n, esta bandera indica si es por grupo o por epo
		var strTipoUsuario = '';
		var reqOficina1;
		var reqOficina2;
		var reqOficina5;
		var fc = Ext.form;
		var recordType = Ext.data.Record.create([{name:'clave'},{name:'descripcion'}]);

		/*var ext_ic_pyme = Ext.getDom('ic_pyme').value;
		var ext_txtaction = Ext.getDom('txtaction').value;
		var ext_txtTipoPersona = Ext.getDom('txtTipoPersona').value;
		var ext_tipoPYME = Ext.getDom('TipoPYME').value;
		var ext_ic_epo = Ext.getDom('ic_epo').value;*/
		var ext_pantalla = Ext.getDom('hpantalla').value;
		/*var ext_url_origen = Ext.getDom('url_origen').value;
		var ext_csTroya = Ext.getDom('csTroya').value;*/

	/******************************************************************************
	 * Combo del tipo de Afiliado
	 ******************************************************************************/
		function creditoElec(pagina){
			if (pagina == 0){
				window.location  = "15forma05AfianzadoraExt.jsp";
			}	else if (pagina == 1 ){
				window.location ="15consEPOext.jsp?idMenu="+idMenu;
			}	else if (pagina == 2){
				window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15forma05ProveedoresExt.jsp?idMenu="+idMenu;
			}	else if (pagina == 3){
				window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?noBancoFondeo=&TxtNombre=&TxtNumElectronico=&Distribuidores=&paginaNo=1&radio01=1|F&internacional=N&consultar=N&InterFinan=&Proveedor=&sel_edo=0&selecOption=1|F";
			}	else if (pagina == 4){
				window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp";
			}	else if (pagina == 5){
				window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp?idMenu="+idMenu;
			}	else if (pagina == 6) { 
				window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp";
			}	else if (pagina == 7) {
				window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B";
			} else if (pagina == 8) { 
				window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB";
			} else if (pagina == 9) {
				window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB";
			} else if (pagina == 10) {
				window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon.jsp?selecOption=CME&consultar=N&valProcesado=A";
			}	else if (pagina == 11) {
				window.location = "15forma05ext_AfCredElectronico.jsp";
			}	else if (pagina == 12) {
				window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?Distribuidores=&TxtNumClienteSIRAC=&TxtFechaAltaDe=&paginaNo=1&radio01=EPOI&internacional=N&consultar=N&InterFinan=&TxtNombreoRS=&Proveedor=&TxtRFC=&TxtFechaAltaA=&NoIf=&selecOption=EPOI";
			}	else if (pagina == 13) {
				window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5.jsp?TxtNombre=&TxtNumElectronico=&Distribuidores=&paginaNo=1&radio01=IF|CO&internacional=S&consultar=N&InterFinan=&Proveedor=&sel_edo=0&selecOption=IF|CO";
			}	else if (pagina == 14) { 
				window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp"; 	
			}	else if (pagina == 15) { 
				window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp"; 	
			}else if (pagina == 16) { // Universidad Programa Credito Educativo
				window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp"; 		
			}
		}

		var dataStatus = new Ext.data.ArrayStore({	 
		fields: ['clave','descripcion'  ],	 
		autoLoad: false	  
		});
		
		if(strPerfil== "ADMIN NAFIN")  {
			dataStatus.loadData(	[	
				['0','Afianzadora'],
				['1','Cadena Productiva'],
				['2','Proveedores'],			
				['4','Proveedor sin n�mero'],
				['5','Distribuidores'],
				['6','Fiados'],
				['7','Intermediario Financiero Bancario'],
				['8','Intermediario Financiero No Bancario']	,
				['9','Intermediarios Bancarios/No Bancarios'],	
				['10','Proveedor Carga Masiva EContract'],	
				['11','Afiliados a Cr�dito Electr�nico'],				
				['14','Mandante'],
				['15','Universidad-Programa Cr�dito Educativo'],
				['16','Cliente Externo']
			]);
			
		}else  if(strPerfil== "PROMO NAFIN FAC" || strPerfil== "PROMO NAFIN" ||  strPerfil== "NAFIN OPERADOR" ||   strPerfil== "NAFIN CON PROM" ||   strPerfil== "NAFIN CON"    ||  strPerfil== "SUPER CALL CENT" ||  strPerfil== "ADMIN NAFIN EXT" )  {
			dataStatus.loadData(	[			
				['2','Proveedores'],			
				['5','Distribuidores']
			]);
		
		}else  if(strPerfil== "CALL CENTER CAD")   { // F021 - 2015
			dataStatus.loadData(	[
				['1','Cadena Productiva'],
				['2','Proveedores'],	
				['5','Distribuidores'],
				['9','Intermediarios Bancarios/No Bancarios']	
			]);
		}else  if(strPerfil=== "ADMIN OPERC")   { //QC-2018
			dataStatus.loadData(	[			
					['2','Proveedores']
				]);	
		}
		
		

	/******************************************************************************
	 * Handlers
	 ******************************************************************************/
	/********** Descargar archivos PDF o CSV **********/
		function descargaArchivo(opts, success, response){
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
				var infoR = Ext.util.JSON.decode(response.responseText);
				Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
				Ext.getCmp('btnImprimir').setIconClass('icoPdf');
				Ext.getCmp('btnImprimirCtas').setIconClass('icoPdf');
				var archivo = infoR.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			} else{
				NE.util.mostrarConnError(response,opts);
			}
		}

	/********** Regresa la pantalla a su estado inicial  **********/
		function cancelar_regresar(accion){
			if (accion == 'Cancelar'){
				Ext.getCmp('formaModificar').getForm().reset();
				Ext.getCmp('forma').show();
				Ext.getCmp('gridConsulta').show();
				Ext.getCmp('formaModificar').hide();
				Ext.getCmp('toolbarReafiliar').hide();
				Ext.getCmp('formaReafiliar').getForm().reset();
				Ext.getCmp('formaReafiliar').hide();
				Ext.getCmp('gridReafiliacion').hide();
				Ext.getCmp('gridAceptaReafiliacion').hide();

			} else if (accion == 'Regresar'){
				Ext.getCmp('formaModificar').getForm().reset();
				Ext.getCmp('forma').show();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('formaModificar').hide();
				Ext.getCmp('toolbarReafiliar').hide();
				Ext.getCmp('formaReafiliar').getForm().reset();
				Ext.getCmp('formaReafiliar').hide();
				Ext.getCmp('gridReafiliacion').hide();
				Ext.getCmp('gridAceptaReafiliacion').hide();

			} else if(accion == 'Cancelar_reafiliacion'){
				Ext.getCmp('gridReafiliacion').hide();
				Ext.getCmp('gridAceptaReafiliacion').hide();
				Ext.getCmp('formaReafiliar').show();
				Ext.getCmp('toolbarReafiliar').show();

			} else if(accion == 'Reafiliacion_exitosa'){
				Ext.getCmp('formaModificar').getForm().reset();
				Ext.getCmp('forma').show();
				Ext.getCmp('formaModificar').hide();
				Ext.getCmp('toolbarReafiliar').hide();
				Ext.getCmp('formaReafiliar').getForm().reset();
				Ext.getCmp('formaReafiliar').hide();
				Ext.getCmp('gridReafiliacion').hide();
				Ext.getCmp('gridAceptaReafiliacion').hide();
				consultaDatosGrid();

			}
		}

	/******************************************************************************
	 *	C�digo del form Modificar									
	******************************************************************************/	
	/**********		Se validan los campos del solicitante 		**********/ 
		function validaCamposSolicitante(){
			
			var errores = 0;
					/***** 	Si el solicitante es EPO, el combo_solicitante es obligatorio 	*****/
			if( (Ext.getCmp('solicitante_mod1')).getValue() == null || (Ext.getCmp('solicitante_mod1')).getValue() == 'null' ){
				errores++; 
			} else {	
				var opcionSolicitante =	(Ext.getCmp('solicitante_mod1')).getValue();
				var opcionGrupo = opcionSolicitante.getGroupValue();
				if( opcionGrupo == 'E' ){
					if( Ext.getCmp('combo_solicitante_mod1').getValue() == '' ){
						Ext.getCmp('combo_solicitante_mod1').markInvalid('Debe seleccionar la EPO que solicita la modificaci�n.');
						errores++; 
					}
				}
			}
			
			/*****	Si no hay errores regresa TRUE	*****/
			if (errores == 0){	
				return true;	
			} else {	
				return false;	
			}		
		}
		
	/**********		Se validan los campos obligatorios del form modificar 		**********/ 
		function validaCamposAModificar(){
			
			var errores = 0;
			
			/*****	Valido los campos obligatorios	*****/ 
			if( Ext.getCmp('formaModificar').getForm().isValid() ){
				
				if( Ext.getCmp('tipo_persona1').getValue() == 'F' ){
				
					if( Ext.getCmp('apellido_paterno_mod1').getValue() == '' ){  errores++; Ext.getCmp('apellido_paterno_mod1').markInvalid('Este campo es obligatorio'); }
					if( Ext.getCmp('apellido_materno_mod1').getValue() == '' ){  errores++; Ext.getCmp('apellido_materno_mod1').markInvalid('Este campo es obligatorio'); }
					if( Ext.getCmp('nombre_mod1').getValue()           == '' ){  errores++; Ext.getCmp('nombre_mod1').markInvalid('Este campo es obligatorio');           }
					if( Ext.getCmp('rfc_mod1').getValue()              == '' ){  errores++; Ext.getCmp('rfc_mod1').markInvalid('Este campo es obligatorio');              }
									if( Ext.getCmp('fecha_nac_mod1').getValue()              == '' ){  errores++; Ext.getCmp('fecha_nac_mod1').markInvalid('Este campo es obligatorio');              }
									
									if(Ext.getCmp('CS_ACEPTACION1').getValue() == 'H' || Ext.getCmp('CS_ACEPTACION1').getValue() == 'R'){
							if( Ext.getCmp('identificacion_mod1').getValue()              == '' ){  errores++; Ext.getCmp('identificacion_mod1').markInvalid('Este campo es obligatorio');              }
						if( Ext.getCmp('empleos_gen_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('empleos_gen_mod1').markInvalid('Este campo es obligatorio'); 						}
						if( Ext.getCmp('activo_total_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('activo_total_mod1').markInvalid('Este campo es obligatorio');						}
						if( Ext.getCmp('cap_contable_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('cap_contable_mod1').markInvalid('Este campo es obligatorio'); 						}
						if( Ext.getCmp('ventas_exp_mod1').getValue() 	            == '' ){	errores++; Ext.getCmp('ventas_exp_mod1').markInvalid('2. Este campo es obligatorio'); 						}
						if( Ext.getCmp('ventas_exp_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('ventas_exp_mod1').markInvalid('1. Este campo es obligatorio'); 						}
						if( Ext.getCmp('ventas_tot_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('ventas_tot_mod1').markInvalid('Este campo es obligatorio'); 							}
						if( Ext.getCmp('no_empleados_mod1').getValue() 			   	== '' ){	errores++; Ext.getCmp('no_empleados_mod1').markInvalid('Este campo es obligatorio'); 						}
						if( Ext.getCmp('sector_mod1').getValue()                 	== '' ){	errores++; Ext.getCmp('sector_mod1').markInvalid('Este campo es obligatorio'); 								} 
						if( Ext.getCmp('subsector_mod1').getValue()              	== '' ){	errores++; Ext.getCmp('subsector_mod1').markInvalid('Este campo es obligatorio'); 							}
						if( Ext.getCmp('rama_mod1').getValue()                   	== '' ){	errores++; Ext.getCmp('rama_mod1').markInvalid('Este campo es obligatorio'); 									} 
						if( Ext.getCmp('clase_mod1').getValue()                 		== '' ){	errores++; Ext.getCmp('clase_mod1').markInvalid('Este campo es obligatorio'); 								}
						if( Ext.getCmp('tipo_empresa_mod1').getValue()      			== '' ){	errores++; Ext.getCmp('tipo_empresa_mod1').markInvalid('Este campo es obligatorio'); 						} 
						if( Ext.getCmp('domicilio_corr_mod1').getValue()    			== '' ){	errores++; Ext.getCmp('domicilio_corr_mod1').markInvalid('Este campo es obligatorio'); 					}
						if( Ext.getCmp('princip_productos_mod1').getValue() 			== '' ){	errores++; Ext.getCmp('princip_productos_mod1').markInvalid('Este campo es obligatorio'); 				}
						if( Ext.getCmp('estado_civil_mod1').getValue()              == '' ){  errores++; Ext.getCmp('estado_civil_mod1').markInvalid('Este campo es obligatorio');              }
											if( Ext.getCmp('sexo_mod1').getValue()              == '' ){  errores++; Ext.getCmp('sexo_mod1').markInvalid('Este campo es obligatorio');              }
								if( Ext.getCmp('grado_escolaridad_mod1').getValue()              == '' ){  errores++; Ext.getCmp('grado_escolaridad_mod1').markInvalid('Este campo es obligatorio');              }
											if( Ext.getCmp('tipo_categoria_mod1').getValue()              == '' ){  errores++; Ext.getCmp('tipo_categoria_mod1').markInvalid('Este campo es obligatorio')}
											if( Ext.getCmp('ejecut_cuenta_mod1').getValue()              == '' ){  errores++; Ext.getCmp('ejecut_cuenta_mod1').markInvalid('Este campo es obligatorio')}
									
											if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('empleos_gen_mod1').getValue())  ){	errores++; Ext.getCmp('empleos_gen_mod1').markInvalid('Formato incorrecto'); 					}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('activo_total_mod1').getValue()) ){	errores++; Ext.getCmp('activo_total_mod1').markInvalid('Formato incorrecto');					}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('cap_contable_mod1').getValue()) ){	errores++; Ext.getCmp('cap_contable_mod1').markInvalid('Formato incorrecto'); 				}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('ventas_tot_mod1').getValue())   ){	errores++; Ext.getCmp('ventas_tot_mod1').markInvalid('Formato incorrecto'); 					}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('no_empleados_mod1').getValue()) ){	errores++; Ext.getCmp('no_empleados_mod1').markInvalid('Formato incorrecto'); 				}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('ventas_tot_mod1').getValue())   ){	errores++; Ext.getCmp('no_empleados_mod1').markInvalid('Formato incorrecto'); 				}
					
					}
									
				} else if( Ext.getCmp('tipo_persona1').getValue() == 'M' ){ 
				
					if( Ext.getCmp('razon_social_mod1').getValue()           	== '' ){	errores++; Ext.getCmp('razon_social_mod1').markInvalid('Este campo es obligatorio'); 						}
					if( Ext.getCmp('rfc_modM1').getValue()                   	== '' ){	errores++; Ext.getCmp('rfc_modM1').markInvalid('Este campo es obligatorio');                                                  }
									
									if( Ext.getCmp('tipo_categoria_mod1').getValue()                   	== '' ){	errores++; Ext.getCmp('tipo_categoria_mod1').markInvalid('Este campo es obligatorio');  }
									if( Ext.getCmp('ejecut_cuenta_mod1').getValue()                   	== '' ){	errores++; Ext.getCmp('ejecut_cuenta_mod1').markInvalid('Este campo es obligatorio');  }
									
					
									
					if(Ext.getCmp('CS_ACEPTACION1').getValue() == 'H' || Ext.getCmp('CS_ACEPTACION1').getValue() == 'R'){
				
						if( Ext.getCmp('no_escritura_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('no_escritura_mod1').markInvalid('Este campo es obligatorio'); 						}
						if( Ext.getCmp('no_notaria_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('no_notaria_mod1').markInvalid('Este campo es obligatorio'); 							}
						if( Ext.getCmp('empleos_gen_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('empleos_gen_mod1').markInvalid('Este campo es obligatorio'); 						}
						if( Ext.getCmp('activo_total_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('activo_total_mod1').markInvalid('Este campo es obligatorio');						}
						if( Ext.getCmp('cap_contable_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('cap_contable_mod1').markInvalid('Este campo es obligatorio'); 						}
						if( Ext.getCmp('ventas_exp_mod1').getValue() 	            == '' ){	errores++; Ext.getCmp('ventas_exp_mod1').markInvalid('2. Este campo es obligatorio'); 						}
						if( Ext.getCmp('ventas_exp_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('ventas_exp_mod1').markInvalid('1. Este campo es obligatorio'); 						}
						if( Ext.getCmp('ventas_tot_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('ventas_tot_mod1').markInvalid('Este campo es obligatorio'); 							}
						if( Ext.getCmp('fecha_const_mod1').getValue() 					== '' ){	errores++; Ext.getCmp('fecha_const_mod1').markInvalid('Este campo es obligatorio'); 						}
						if( Ext.getCmp('no_empleados_mod1').getValue() 			   	== '' ){	errores++; Ext.getCmp('no_empleados_mod1').markInvalid('Este campo es obligatorio'); 						}
						if( Ext.getCmp('sector_mod1').getValue()                 	== '' ){	errores++; Ext.getCmp('sector_mod1').markInvalid('Este campo es obligatorio'); 								} 
						if( Ext.getCmp('subsector_mod1').getValue()              	== '' ){	errores++; Ext.getCmp('subsector_mod1').markInvalid('Este campo es obligatorio'); 							}
						if( Ext.getCmp('rama_mod1').getValue()                   	== '' ){	errores++; Ext.getCmp('rama_mod1').markInvalid('Este campo es obligatorio'); 									} 
						if( Ext.getCmp('clase_mod1').getValue()                 		== '' ){	errores++; Ext.getCmp('clase_mod1').markInvalid('Este campo es obligatorio'); 								}
						if( Ext.getCmp('tipo_empresa_mod1').getValue()      			== '' ){	errores++; Ext.getCmp('tipo_empresa_mod1').markInvalid('Este campo es obligatorio'); 						} 
						if( Ext.getCmp('domicilio_corr_mod1').getValue()    			== '' ){	errores++; Ext.getCmp('domicilio_corr_mod1').markInvalid('Este campo es obligatorio'); 					}
						if( Ext.getCmp('princip_productos_mod1').getValue() 			== '' ){	errores++; Ext.getCmp('princip_productos_mod1').markInvalid('Este campo es obligatorio'); 				}
											if( Ext.getCmp('identificacion_rep_mod1').getValue()                   	== '' ){	errores++; Ext.getCmp('identificacion_rep_mod1').markInvalid('Este campo es obligatorio');                                                  }
											if( Ext.getCmp('no_identificacion_rep_mod1').getValue()                   	== '' ){	errores++; Ext.getCmp('no_identificacion_rep_mod1').markInvalid('Este campo es obligatorio');                                                  }
								
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('no_escritura_mod1').getValue()) ){	errores++; Ext.getCmp('no_escritura_mod1').markInvalid('Formato incorrecto'); 	         }
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('no_notaria_mod1').getValue())   ){	errores++; Ext.getCmp('no_notaria_mod1').markInvalid('Formato incorrecto'); 					}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('empleos_gen_mod1').getValue())  ){	errores++; Ext.getCmp('empleos_gen_mod1').markInvalid('Formato incorrecto'); 					}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('activo_total_mod1').getValue()) ){	errores++; Ext.getCmp('activo_total_mod1').markInvalid('Formato incorrecto');					}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('cap_contable_mod1').getValue()) ){	errores++; Ext.getCmp('cap_contable_mod1').markInvalid('Formato incorrecto'); 				}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('ventas_tot_mod1').getValue())   ){	errores++; Ext.getCmp('ventas_tot_mod1').markInvalid('Formato incorrecto'); 					}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('no_empleados_mod1').getValue()) ){	errores++; Ext.getCmp('no_empleados_mod1').markInvalid('Formato incorrecto'); 				}
						if( !/^([0-9])*[.]?[0-9]*$/.test(Ext.getCmp('ventas_tot_mod1').getValue())   ){	errores++; Ext.getCmp('no_empleados_mod1').markInvalid('Formato incorrecto'); 				}
					
					}
				}
				
			} else{
				errores++;
			}
			
			/*****	Valido el numero de proveedor	(Datos del contacto )*****/
			if(Ext.getCmp('CS_ACEPTACION1').getValue() == 'N' || Ext.getCmp('CS_ACEPTACION1').getValue() == 'S'){
				if( Ext.getCmp('Sin_Num_Prov_cont_mod1').isVisible() ){
					if( Ext.getCmp('Sin_Num_Prov_cont_mod1').getValue() == true ){
						Ext.getCmp('num_proveedor_cont_mod1').setValue('');
						Ext.getCmp('num_proveedor_diversos_mod1').setValue('');
					} else if ( Ext.getCmp('Sin_Num_Prov_cont_mod1').getValue() != true && Ext.getCmp('num_proveedor_cont_mod1').getValue() == '' ){
						Ext.getCmp('num_proveedor_cont_mod1').markInvalid('Este campo es obligatorio'); 
						errores++;
					}
				} else {
					if(Ext.getCmp('num_proveedor_cont_mod1').getValue() == ''){
						Ext.getCmp('num_proveedor_cont_mod1').markInvalid('Este campo es obligatorio'); 
						errores++;
					}
				}
				
			/*****	Valido el numero de proveedor	(Diversos)*****/
			} else if(Ext.getCmp('CS_ACEPTACION1').getValue() == 'H' || Ext.getCmp('CS_ACEPTACION1').getValue() == 'R'){
				
				if( Ext.getCmp('Sin_Num_Prov_diversos_mod1').getValue() == true ){
					Ext.getCmp('num_proveedor_diversos_mod1').setValue('');
					Ext.getCmp('num_proveedor_cont_mod1').setValue(''); // Por "reciprocidad"
				} else if ( Ext.getCmp('Sin_Num_Prov_diversos_mod1').getValue() != true && Ext.getCmp('num_proveedor_diversos_mod1').getValue() == '' ){
					Ext.getCmp('num_proveedor_diversos_mod1').markInvalid('Este campo es obligatorio'); 
					errores++;
				}
				
			}

			/*****	Valido la venta total	*****/
			if( Ext.getCmp('ventas_tot_mod1').isVisible() && Ext.getCmp('ventas_tot_mod1').getValue() != '' ){
				var cantidad = parseFloat( Ext.getCmp('ventas_tot_mod1').getValue() );
				if( cantidad < 5000 ){
					Ext.getCmp('ventas_tot_mod1').markInvalid('El par�metro de Ventas Netas Totales debe ser mayor o igual a $5,000.00'); 
					errores++;
				}
			}
			
			/*****	Valido la fecha de firma de convenio	*****/
			if( Ext.getCmp('convenio_unico_prod_mod1').isValid() && Ext.getCmp('convenio_unico_prod_mod1').getValue() == true &&  Ext.getCmp('fecha_convenio_prod_mod1').getValue() == ''){
				Ext.getCmp('fecha_convenio_prod_mod1').markInvalid('Este campo es obligatorio'); 
				errores++; 
			}
				
			/*****	Si no hay errores regresa TRUE	*****/
			if (errores == 0){	
				return true;	
			} else {	
				return false;	
			}
			
		}

	/**********		Valida las oficinas tramitadoras 		**********/ 
		function validaOficinasTramitadoras(){
			
			var errores = 0;
			
			if( reqOficina1 == true ){
				/*if( Ext.getCmp('susceptible_desconstar_mod1').isVisible() || Ext.getCmp('anticipo_mod1').isVisible() || Ext.getCmp('credicadenas_mod1').isVisible() &&
					Ext.getCmp('susceptible_desconstar_mod1').getValue() == true && Ext.getCmp('anticipo_mod1').getValue() == true && Ext.getCmp('credicadenas_mod1').getValue() == true ){
					if ( Ext.getCmp('ic_oficina_tramitadora_descuento1').getValue() == '' ){
						Ext.getCmp('ic_oficina_tramitadora_descuento1').markInvalid('Debe asignar una Oficina Tramitadora para Descuento Electr�nico'); 
						errores++;
					}
				}*/
				if( (   Ext.getCmp('susceptible_desconstar_mod1').isVisible() && Ext.getCmp('susceptible_desconstar_mod1').getValue() == true ) &&
					( ( Ext.getCmp('anticipo_mod1').isVisible() && Ext.getCmp('anticipo_mod1').getValue() == true ) ||
						( Ext.getCmp('credicadenas_mod1').isVisible() && Ext.getCmp('credicadenas_mod1').getValue() == true )
					)	 
				){
						if ( Ext.getCmp('ic_oficina_tramitadora_descuento1').getValue() == '' ){
							Ext.getCmp('ic_oficina_tramitadora_descuento1').markInvalid('Debe asignar una Oficina Tramitadora para Descuento Electr�nico'); 
							errores++;
						}		  
				}			
			}
			if( reqOficina2 ){
				if( Ext.getCmp('anticipo_mod1').isVisible() && Ext.getCmp('anticipo_mod1').getValue() == true ){
					if ( Ext.getCmp('ic_oficina_tramitadora_anticipo1').getValue() == '' ){
						Ext.getCmp('ic_oficina_tramitadora_anticipo1').markInvalid('Debe asignar una Oficina Tramitadora para Anticipos'); 
						errores++;
					}
				}
			}
			if( reqOficina5 ){
				if( Ext.getCmp('credicadenas_mod1').isVisible() && Ext.getCmp('credicadenas_mod1').getValue() == true ){
					if ( Ext.getCmp('ic_oficina_tramitadora_inventario1').getValue() == '' ){
						Ext.getCmp('ic_oficina_tramitadora_inventario1').markInvalid('Debe asignar una Oficina Tramitadora para Credicadenas'); 
						errores++;
					}
				}
			}
			
			/*****	Si no hay errores regresa TRUE	*****/
			if( errores == 0 ){
				return true;
			} else {
				return false;
			}
		}

		function enviaDatos(){
			var valor1;
			var valor2;
			var valor3 ;
			var temp;	
			
			/***** 	Procesar los datos de tipo radioButton 	*****/
			if( Ext.getCmp('solicitante_mod1').getValue() == 'null' || Ext.getCmp('solicitante_mod1').getValue() == null ){
			} else {
				temp =	(Ext.getCmp('solicitante_mod1')).getValue();
				valor1 = temp.getGroupValue();
			}
			if( Ext.getCmp('sexo_mod1').getValue() == 'null' || Ext.getCmp('sexo_mod1').getValue() == null ){
			} else {
				temp =	Ext.getCmp('sexo_mod1').getValue();
				valor2 = temp.getGroupValue();
			}
			if( Ext.getCmp('otros_datos').isVisible() ){
				if( Ext.getCmp('autoriza_mod1').getValue() == 'null' || Ext.getCmp('autoriza_mod1').getValue() == null ){
				} else {
					temp =	(Ext.getCmp('autoriza_mod1')).getValue();
					valor3 = temp.getGroupValue();
				}
			} else {
				valor3 = '';
			}
			/*****   Determinar valor correcto del campo Sin. No. Proveedor *****/
			var cs_aceptacion1 = Ext.getCmp('CS_ACEPTACION1').getValue();
			var Sin_Num_Prov;
			if(        cs_aceptacion1 === 'N' || cs_aceptacion1 === 'S' ){ // Datos del contacto
				Sin_Num_Prov = Ext.getCmp('Sin_Num_Prov_cont_mod1').getValue();
			} else if( cs_aceptacion1 === 'H' || cs_aceptacion1 === 'R' ){ // Diversos
				Sin_Num_Prov = Ext.getCmp('Sin_Num_Prov_diversos_mod1').getValue();
			} else {
				Ext.Msg.alert( 'Error', 'No se pudo obtener el valor del campo: "Sin no. proveedor".' );
				return;
			}
			
			/***** 	Si todo es correcto, mando a modificar los datos	*****/
			Ext.Msg.confirm('', '�Esta seguro de querer enviar su informaci�n? ',function(botonConf) {
				if (botonConf == 'ok' || botonConf == 'yes') {
					fm.el.mask('Procesando...', 'x-mask-loading');		
				
					Ext.Ajax.request({
						url: '15forma05ProveedoresExt.data.jsp',
						params: Ext.apply({	
							informacion: 								'GuardaDatos', 
							Sin_Num_Prov: 								Sin_Num_Prov,
							strTAfilia: 								Ext.getCmp('CS_ACEPTACION1').getValue(),
							tipo_persona1: 							Ext.getCmp('tipo_persona1').getValue(),
							ic_pyme1: 									Ext.getCmp('ic_pyme1').getValue(),
							ic_epo_mod1: 								Ext.getCmp('ic_epo_mod1').getValue(),
							actualiza_sirac1: 						Ext.getCmp('actualiza_sirac1').getValue(),
							ic_domicilio1: 						   Ext.getCmp('ic_domicilio1').getValue(),
							ic_contacto1: 						      Ext.getCmp('ic_contacto1').getValue(),
							solicitante_mod1: 					   valor1,					
							combo_solicitante_mod1: 				Ext.getCmp('combo_solicitante_mod1').getValue(),
							apellido_paterno_mod1: 					Ext.getCmp('apellido_paterno_mod1').getValue(),
							apellido_materno_mod1: 					Ext.getCmp('apellido_materno_mod1').getValue(),
							nombre_mod1: 								Ext.getCmp('nombre_mod1').getValue(),
							rfc_mod1: 									Ext.getCmp('rfc_mod1').getValue(),
							invalidar_modF1: 							Ext.getCmp('invalidar_modF1').getValue(),
							curp_mod1: 									Ext.getCmp('curp_mod1').getValue(),
							fiel_mod1: 									Ext.getCmp('fiel_modF1').getValue(),
							apellido_casada_mod1: 					Ext.getCmp('apellido_casada_mod1').getValue(),
							regimen_matrimonial_mod1: 				Ext.getCmp('regimen_matrimonial_mod1').getValue(),	
							sexo_mod1: 									valor2,
							estado_civil_mod1: 						Ext.getCmp('estado_civil_mod1').getValue(),
							fecha_nac_mod1: 							Ext.getCmp('fecha_nac_mod1').getValue(),
							pais_origen_modF1: 						Ext.getCmp('pais_origen_modF1').getValue(),
							identificacion_mod1: 					Ext.getCmp('identificacion_mod1').getValue(),
							grado_escolaridad_mod1: 				Ext.getCmp('grado_escolaridad_mod1').getValue(),
							razon_social_mod1:						Ext.getCmp('razon_social_mod1').getValue(),
							nombre_comer_mod1: 						Ext.getCmp('nombre_comer_mod1').getValue(),
							rfc_modM1: 									Ext.getCmp('rfc_modM1').getValue(),
							invalidar_modM1: 							Ext.getCmp('invalidar_modM1').getValue(),	
							fiel_modM1: 								Ext.getCmp('fiel_modM1').getValue(),
							pais_origen_modM1: 						Ext.getCmp('pais_origen_modM1').getValue(),
							direccion_mod1: 							Ext.getCmp('direccion_mod1').getValue(),
							colonia_mod1: 								Ext.getCmp('colonia_mod1').getValue(),
							cod_postal_mod1: 							Ext.getCmp('cod_postal_mod1').getValue(),
							pais_mod1: 									Ext.getCmp('pais_mod1').getValue(),
							estado_mod1: 								Ext.getCmp('estado_mod1').getValue(),
							deleg_mun_mod1: 							Ext.getCmp('deleg_mun_mod1').getValue(),
							ciudad_mod1: 								Ext.getCmp('ciudad_mod1').getValue(),
							telefono_mod1: 							Ext.getCmp('telefono_mod1').getValue(),
							fax_mod1: 									Ext.getCmp('fax_mod1').getValue(),
							ap_paterno_rep_mod1: 					Ext.getCmp('ap_paterno_rep_mod1').getValue(),
							ap_materno_rep_mod1: 					Ext.getCmp('ap_materno_rep_mod1').getValue(),
							nombre_rep_mod1: 							Ext.getCmp('nombre_rep_mod1').getValue(),
							telefono_rep_mod1: 						Ext.getCmp('telefono_rep_mod1').getValue(),
							fax_rep_mod1: 								Ext.getCmp('fax_rep_mod1').getValue(),
							email_rep_mod1: 							Ext.getCmp('email_rep_mod1').getValue(),
							rfc_rep_mod1: 								Ext.getCmp('rfc_rep_mod1').getValue(),
							no_identificacion_rep_mod1: 			Ext.getCmp('no_identificacion_rep_mod1').getValue(),
							identificacion_rep_mod1: 				Ext.getCmp('identificacion_rep_mod1').getValue(),
							ap_paterno_cont_mod1: 					Ext.getCmp('ap_paterno_cont_mod1').getValue(),
							ap_materno_cont_mod1: 					Ext.getCmp('ap_materno_cont_mod1').getValue(),
							nombre_cont_mod1: 						Ext.getCmp('nombre_cont_mod1').getValue(),
							telefono_cont_mod1: 						Ext.getCmp('telefono_cont_mod1').getValue(),
							fax_cont_mod1: 							Ext.getCmp('fax_cont_mod1').getValue(),
							email_cont_mod1: 							Ext.getCmp('email_cont_mod1').getValue(),
							celular_cont_mod1: 						Ext.getCmp('celular_cont_mod1').getValue(),
							num_proveedor_cont_mod1: 				Ext.getCmp('num_proveedor_cont_mod1').getValue(),
							tipo_categoria_mod1: 					Ext.getCmp('tipo_categoria_mod1').getValue(),
							ejecut_cuenta_mod1: 						Ext.getCmp('ejecut_cuenta_mod1').getValue(),
							no_escritura_mod1: 						Ext.getCmp('no_escritura_mod1').getValue(),
							no_notaria_mod1:							Ext.getCmp('no_notaria_mod1').getValue(),
							empleos_gen_mod1: 						Ext.getCmp('empleos_gen_mod1').getValue(),
							activo_total_mod1: 						Ext.getCmp('activo_total_mod1').getValue(),
							cap_contable_mod1: 						Ext.getCmp('cap_contable_mod1').getValue(),
							ventas_exp_mod1:							Ext.getCmp('ventas_exp_mod1').getValue(),
							ventas_tot_mod1: 							Ext.getCmp('ventas_tot_mod1').getValue(),
							fecha_const_mod1: 						Ext.getCmp('fecha_const_mod1').getValue(),
							no_empleados_mod1: 						Ext.getCmp('no_empleados_mod1').getValue(),
							num_proveedor_diversos_mod1: 			Ext.getCmp('num_proveedor_diversos_mod1').getValue(),
							sector_mod1: 								Ext.getCmp('sector_mod1').getValue(),
							subsector_mod1: 							Ext.getCmp('subsector_mod1').getValue(),
							rama_mod1: 									Ext.getCmp('rama_mod1').getValue(),
							clase_mod1: 								Ext.getCmp('clase_mod1').getValue(),
							tipo_empresa_mod1: 						Ext.getCmp('tipo_empresa_mod1').getValue(),
							domicilio_corr_mod1: 					Ext.getCmp('domicilio_corr_mod1').getValue(),
							princip_productos_mod1: 				Ext.getCmp('princip_productos_mod1').getValue(),
							num_sirac_mod1: 							Ext.getCmp('num_sirac_mod1').getValue(),
							num_troya_mod1: 							Ext.getCmp('num_troya_mod1').getValue(),
							alta_cliente_troya_mod1: 				Ext.getCmp('alta_cliente_troya_mod1').getValue(),
							autoriza_mod1: 							valor3,
							susceptible_desconstar_mod1: 			Ext.getCmp('susceptible_desconstar_mod1').getValue(),
							ic_oficina_tramitadora_descuento1: 	Ext.getCmp('ic_oficina_tramitadora_descuento1').getValue(),
							version_convenio_mod1: 					Ext.getCmp('version_convenio_mod1').getValue(),
							anticipo_mod1: 							Ext.getCmp('anticipo_mod1').getValue(),
							ic_oficina_tramitadora_anticipo1: 	Ext.getCmp('ic_oficina_tramitadora_anticipo1').getValue(),
							fecha_consulta_convenio_mod1: 		Ext.getCmp('fecha_consulta_convenio_mod1').getValue(),
							credicadenas_mod1: 						Ext.getCmp('credicadenas_mod1').getValue(),
							ic_oficina_tramitadora_inventario1: Ext.getCmp('ic_oficina_tramitadora_inventario1').getValue(),
							convenio_unico_prod_mod1: 				Ext.getCmp('convenio_unico_prod_mod1').getValue(),
							cesion_der_prod_mod1: 					Ext.getCmp('cesion_der_prod_mod1').getValue(),
							fideicomiso_desarrollo_prod_mod1: 	Ext.getCmp('fideicomiso_desarrollo_prod_mod1').getValue(),
							fecha_convenio_prod_mod1: 				Ext.getCmp('fecha_convenio_prod_mod1').getValue(),
							fianza_electronica_mod1: 				Ext.getCmp('fianza_electronica_mod1').getValue(),
							chkEntidadGobierno:						Ext.getCmp('chkEntidadGobierno1').getValue(),
							chkFactDistribuido:						Ext.getCmp('chkFactDistribuido1').getValue(),
							chkProvExtranjero:						Ext.getCmp('chkProvExtranjero').getValue(),	
							chkOperaDescAutoEPO:					Ext.getCmp('chkOperaDescAutoEPO').getValue()	
						}),
						callback: procesaDatosModificados
					});	
				}
			});
		}
		
	/**********		Se mandan a actualizar los datos 		**********/ 
		function actualizaNafin() {
			if( validaCamposSolicitante() ){
				if( validaCamposAModificar() ){
					if( validaOficinasTramitadoras() ){
						enviaDatos();
					} else {
						Ext.Msg.alert( 'Error', 'Revise las oficinas tramitadoras' );
					}
				} else {
					Ext.Msg.alert( 'Error', 'Revise los campos obligatorios' );
				}
			} else {
				Ext.Msg.alert( 'Error', 'Favor de indicar el solicitante de la modificaci�n.' );
			}
		}

	/**********		Al actualizar Sirac, valida el RFC 		**********/ 
		function actualizaSirac(){		
			
			/*****		Verifico que no se haya invalidado ning�n RFC		*****/
			if( Ext.getCmp('tipo_persona1').getValue() == 'F' ){
				if( Ext.getCmp('invalidar_modF1').getValue() == true ){
					Ext.Msg.alert('Error', '"No se puede actualizar en SIRAC un registro de N@E invalidado' );
					return false;
				}
			} else if( Ext.getCmp('tipo_persona1').getValue() == 'M' ){
				if( Ext.getCmp('invalidar_modM1').getValue() == true ){
					Ext.Msg.alert('Error', '"No se puede actualizar en SIRAC un registro de N@E invalidado' );
					return false;
				}
			}
			
			/*****		Si es Sirac, el RFC no puede ser modificado		*****/
			if( Ext.getCmp('rfc_mod1').getValue() != Ext.getCmp('rfc_modM1').getValue() ){
				Ext.Msg.alert('Error', 'EL RFC fue modificado. No se puede actualizar en SIRAC' );
				return false;
			}
			return true;
			
		}

	/**********		Procesa la respuesta despues de enviar a modificar los datos 		**********/ 
		var procesaDatosModificados = function(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				fm.el.unmask();
				var datosActualizados = Ext.util.JSON.decode(response.responseText).actualizacion;
				var msg = Ext.util.JSON.decode(response.responseText).mensaje;
				Ext.Msg.alert('Mensaje', msg ); 
				if(datosActualizados == true){

					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply({
							informacion:       'consultar',
							operacion:         'generar',
							start:             0,
							limit:             15,
							nafin_electronico: Ext.getCmp('nafin_electronico1').getValue(),
							nombre:            Ext.getCmp('nombre1').getValue(),
							estado:            Ext.getCmp('estado1').getValue(),
							rfc:               Ext.getCmp('rfc1').getValue(),
							banco_fondeo:      Ext.getCmp('banco_fondeo1').getValue(),
							ic_epo:            Ext.getCmp('ic_epo1').getValue(),
							numero_proveedor:  Ext.getCmp('numero_proveedor1').getValue(),
							numero_sirac:      Ext.getCmp('numero_sirac1').getValue(),
							modifPropietario:  Ext.getCmp('modif_propietario1').getValue(),
							pymeSinProveedor:  Ext.getCmp('pyme_sin_proveedor1').getValue(),
							convenioUnico:     Ext.getCmp('convenio_unico1').getValue(),
							operaFideicomiso:  Ext.getCmp('opera_fideicomiso1').getValue(),
							chkFactDistribuidoCons:Ext.getCmp('chkFactDistribuidoCons1').getValue(),
							chkOperaDescAutoEPO:Ext.getCmp('chkOperaDescAutoEPO').getValue()	
						})
					});
					cancelar_regresar('Cancelar');
				}
			}
		}

	/**********		Procesos para obtener y mostrar los datos de la pyme seleccionada del grid 		**********/ 
		var consultaModificarPyme = function(grid, rowIndex, colIndex, item, event) {
			var rec = grid.getStore().getAt(rowIndex);
		
			var ic_pyme  = rec.get('CLIENTE');
			var tipo_per = rec.get('TIPO_PERSONA');
			var ic_epo   = rec.get('IC_EPO');
			cadenaProductiva = "Opera Descuento Autom�tico con "+rec.get('CADENA_PRODUCTIVA');
						
			Ext.getCmp('displayF').setValue(cadenaProductiva); 
			Ext.getCmp('tipo_persona1').setValue(tipo_per); 
			Ext.getCmp('ic_pyme1').setValue(ic_pyme);
			Ext.getCmp('ic_epo_mod1').setValue(ic_epo);
			gridConsulta.el.mask('Buscando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15forma05ProveedoresExt.data.jsp',
				params: Ext.apply({
					informacion: 'consultaModificar', 
					ic_pyme: rec.get('CLIENTE'),
					ic_epo: rec.get('IC_EPO'),
					idMenuP:idMenu
				}),
				callback: datosAModificar
			});
			
		}
		
	/**********		L�gica para determinar los componentes que se mostrar�n en el form para modificar 		**********/  
		var datosAModificar = function(opts, success, response) {
		
			/*****		Inicializo los componentes		*****/
			Ext.getCmp('nombre_persona').show();
			Ext.getCmp('nombre_empresa').show();
			Ext.getCmp('domicilio').show();
			Ext.getCmp('representante_legal').show();
			Ext.getCmp('datos_contacto').show();
			Ext.getCmp('diversos').show();
			Ext.getCmp('otros_datos').show();
			Ext.getCmp('productos').show();	
			
			Ext.getCmp('nombre_persona3').show();
			Ext.getCmp('nombre_persona4').show();
			Ext.getCmp('nombre_persona5').show();
			Ext.getCmp('nombre_persona7').show();
			Ext.getCmp('datos_contacto5').show();
			Ext.getCmp('representante_legal3').show();
			Ext.getCmp('representante_legal4').show();
			Ext.getCmp('representante_legal5').show();
			Ext.getCmp('diversos1').show();
				Ext.getCmp('no_escritura_mod1').enable();	
				Ext.getCmp('no_notaria_mod1').enable();		
			Ext.getCmp('ic_oficina_tramitadora_descuento1').show();
			Ext.getCmp('version_convenio_mod1').show();
			Ext.getCmp('productos2').show();
			Ext.getCmp('anticipo_mod1').show();
			Ext.getCmp('ic_oficina_tramitadora_anticipo1').show();
			Ext.getCmp('credicadenas_mod1').show();
			Ext.getCmp('ic_oficina_tramitadora_inventario1').show();
			
			/*****		Inicia el proceso de carga de valores		*****/
			if(ext_pantalla=='pendSIRAC'){
		pnl.el.unmask();
		}else{
		gridConsulta.el.unmask();
		}
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
					
				reqOficina1 = Ext.util.JSON.decode(response.responseText).reqOficina1;
				reqOficina2 = Ext.util.JSON.decode(response.responseText).reqOficina2;
				reqOficina5 = Ext.util.JSON.decode(response.responseText).reqOficina5;


				catalogoSolicitanteEPO.load({
					params : Ext.apply({
						ic_pyme : Ext.getCmp('ic_pyme1').getValue()
					})
				});
				catalogoPais.load();
				catalogoPaisOrigenPF.load();
				catalogoPaisOrigenPM.load();
				catalogoRegimenMatrimonial.load();
				catalogoEstadoCivil.load();
				catalogoIdentificacionNC.load();
				catalogoGradoEscolaridad.load();	
				catalogoIdentificacionRL.load();
				catalogoSector.load();
				catalogoTipoCategoria.load();
				catalogoTipoEmpresa.load();
				catalogoDomicilioCorrespondencia.load();
				catalogoAgenciaSiracDescuento.load();
				catalogoAgenciaSiracAnticipo.load();
				catalogoAgenciaSiracInventario.load();	
				catalogoVersionConvenio.load();	
				
				/*****		Asigno los datos al form		*****/
				valorFormulario = Ext.util.JSON.decode(response.responseText).registro;
				fm.getForm().setValues(valorFormulario);
				
				/*****		Variables para habilitar o deshabilitar los checkbox		*****/
				var flagAnticipo     = Ext.util.JSON.decode(response.responseText).flagAnticipo;
				var flagCrediCadenas = Ext.util.JSON.decode(response.responseText).flagCrediCadenas;
				var strHabAnt        = Ext.util.JSON.decode(response.responseText).strHabAnt;
				var strHabInv        = Ext.util.JSON.decode(response.responseText).strHabInv;
				
				/*****		Obtengo el tipo de usuario		*****/
				var strTipoUsuario = Ext.util.JSON.decode(response.responseText).strTipoUsuario;
			
				/*****		Inicializo los combos relacionados con el combo pais		*****/
				catalogoEstadoMod.load({
					params : Ext.apply({
						combo_pais : Ext.getCmp('pais_mod1').getValue()
					})
				});
				catalogoDelegMun.load({
					params: Ext.apply({
						combo_pais : Ext.getCmp('pais_mod1').getValue(),
						combo_estado : (Ext.getCmp('estado_mod1')).getValue()
					})
				});
				catalogoCiudad.load({
					params: Ext.apply({
						combo_pais : Ext.getCmp('pais_mod1').getValue(),
						combo_estado : (Ext.getCmp('estado_mod1')).getValue()
					})
				});
				
				/*****		Inicializo los combos relacionados con el combo Sector Econ�mico		*****/
				if(Ext.getCmp('CS_ACEPTACION1').getValue() == 'H' || Ext.getCmp('CS_ACEPTACION1').getValue() == 'R'){
					catalogoSubSector.load({
						params: Ext.apply({
							sector_econ : Ext.getCmp('sector_mod1').getValue()
						})
					});
					catalogoRama.load({
						params: Ext.apply({
							sector_econ : Ext.getCmp('sector_mod1').getValue(),
							subsector: Ext.getCmp('subsector_mod1').getValue()					
						})
					});		
					catalogoClase.load({
						params: Ext.apply({
							sector_econ: Ext.getCmp('sector_mod1').getValue(),
							subsector: Ext.getCmp('subsector_mod1').getValue(),
							rama: Ext.getCmp('rama_mod1').getValue()
						})
					});			
				}
			
				/*****		Coloco los t�tulos correctos		*****/
				if( Ext.getCmp('tipo_persona1').getValue() == 'F' ){
					Ext.getCmp('representante_legal').setTitle('Persona autorizada');
				} else if( Ext.getCmp('tipo_persona1').getValue() == 'M' ) {
					Ext.getCmp('representante_legal').setTitle('Representante Legal');
				}		
				
				/***** 	Se valida que secciones se mostrar�n		*****/
				if(Ext.getCmp('CS_ACEPTACION1').getValue() == 'H' || Ext.getCmp('CS_ACEPTACION1').getValue() == 'R'){
						
					
					if(Ext.getCmp('tipo_persona1').getValue() == 'F'){
									
						Ext.getCmp('nombre_persona').show();
						Ext.getCmp('nombre_empresa').hide();
						Ext.getCmp('domicilio').show();
						Ext.getCmp('representante_legal').show();
						Ext.getCmp('datos_contacto').show();
						Ext.getCmp('diversos').show();
						Ext.getCmp('otros_datos').show();
						Ext.getCmp('productos').show();
						
						Ext.getCmp('representante_legal3').hide();
						Ext.getCmp('representante_legal4').hide();
						Ext.getCmp('representante_legal5').hide();
						Ext.getCmp('diversos1').hide();
							Ext.getCmp('no_escritura_mod1').disable();	// Impedir que el campo sea validado si est� oculto
							Ext.getCmp('no_notaria_mod1').disable();		// Impedir que el campo sea validado si est� oculto
						Ext.getCmp('fecha_const_mod1').hide();
											
					} else if( Ext.getCmp('tipo_persona1').getValue() == 'M' ){
										
						Ext.getCmp('nombre_persona').hide();
						Ext.getCmp('nombre_empresa').show();
						Ext.getCmp('domicilio').show();
						Ext.getCmp('representante_legal').show();
						Ext.getCmp('datos_contacto').show();
						Ext.getCmp('diversos').show();
						Ext.getCmp('otros_datos').show();
						Ext.getCmp('productos').show();
											
						if( Ext.getCmp('num_troya_mod1').getValue() != '' ){
							Ext.getCmp('num_troya_mod1').show();	
							Ext.getCmp('alta_cliente_troya_mod1').hide();	
						} else {
							Ext.getCmp('num_troya_mod1').hide();
							Ext.getCmp('num_troya_mod1').setValue('N');	
							Ext.getCmp('alta_cliente_troya_mod1').show();	
						}
						
					}
					/*****		Determino los checkBox que se mostrar�n		*****/
					Ext.getCmp('num_proveedor_cont_mod1_1').hide(); // El campo se oculta completamente
					/*
					if( Ext.getCmp('num_proveedor_cont_mod1').getValue() =='' ){
						Ext.getCmp('Sin_Num_Prov_cont_mod1').show();
					} else {
						Ext.getCmp('Sin_Num_Prov_cont_mod1').hide();
						Ext.getCmp('num_proveedor_cont_mod1').hide();	
						Ext.getCmp('num_proveedor_cont_mod1_1').hide();						
					}
					*/
					if(Ext.getCmp('CS_ACEPTACION1').getValue() == 'N'){
						Ext.getCmp('susceptible_desconstar_mod1').enable();
						Ext.getCmp('susceptible_desconstar_mod1').setValue(true);
					} else {
						Ext.getCmp('susceptible_desconstar_mod1').disable();
						Ext.getCmp('susceptible_desconstar_mod1').setValue(false);
					}
					
					if(flagAnticipo == false){
						Ext.getCmp('anticipo_mod1').enable();
					} else {
						Ext.getCmp('anticipo_mod1').disable();
					}
					if(strHabAnt != 'N'){
						Ext.getCmp('anticipo_mod1').setValue(true);
					} else {
						Ext.getCmp('anticipo_mod1').setValue(false);
					}
					if(flagCrediCadenas == false){
						Ext.getCmp('credicadenas_mod1').enable();
					} else {
						Ext.getCmp('credicadenas_mod1').disable();
					}
					if(strHabInv != 'N'){
						Ext.getCmp('credicadenas_mod1').setValue(true);
					} else {
						Ext.getCmp('credicadenas_mod1').setValue(false);
					}
				} else if(Ext.getCmp('CS_ACEPTACION1').getValue() == 'S' || Ext.getCmp('CS_ACEPTACION1').getValue() == 'N'){
				
								
					if(Ext.getCmp('tipo_persona1').getValue() == 'F'){
						
						Ext.getCmp('nombre_persona').show();
						Ext.getCmp('nombre_empresa').hide();
						Ext.getCmp('domicilio').show();
						Ext.getCmp('representante_legal').hide();
											Ext.getCmp('datos_contacto').show();
						Ext.getCmp('diversos').hide();
						Ext.getCmp('otros_datos').hide();
						Ext.getCmp('productos').show();		
						
						Ext.getCmp('nombre_persona3').hide();
						Ext.getCmp('nombre_persona4').hide();
						Ext.getCmp('nombre_persona5').hide();
						Ext.getCmp('nombre_persona7').hide();
						
						Ext.getCmp('datos_contacto5').hide();
						
						Ext.getCmp('ic_oficina_tramitadora_descuento1').hide();
						Ext.getCmp('version_convenio_mod1').hide();
						Ext.getCmp('productos2').hide();
						Ext.getCmp('anticipo_mod1').hide();
						Ext.getCmp('ic_oficina_tramitadora_anticipo1').hide();	
						Ext.getCmp('credicadenas_mod1').hide();
						Ext.getCmp('ic_oficina_tramitadora_inventario1').hide();
											
					} else if(Ext.getCmp('tipo_persona1').getValue() == 'M'){	
					
						Ext.getCmp('nombre_persona').hide();
						Ext.getCmp('nombre_empresa').show();
						Ext.getCmp('domicilio').show();
						Ext.getCmp('representante_legal').hide();
											Ext.getCmp('datos_contacto').show();
						Ext.getCmp('diversos').hide();
						Ext.getCmp('otros_datos').hide();
						Ext.getCmp('productos').show();
						
						Ext.getCmp('ic_oficina_tramitadora_descuento1').hide();
						Ext.getCmp('version_convenio_mod1').hide();
						Ext.getCmp('productos2').hide();
						Ext.getCmp('credicadenas_mod1').hide();
						Ext.getCmp('ic_oficina_tramitadora_inventario1').hide();
						
						if( Ext.getCmp('num_troya_mod1').getValue() != '' ){
							Ext.getCmp('num_troya_mod1').show();	
							Ext.getCmp('alta_cliente_troya_mod1').hide();	
						} else {
							Ext.getCmp('num_troya_mod1').hide();
							Ext.getCmp('num_troya_mod1').setValue('N');	
							Ext.getCmp('alta_cliente_troya_mod1').show();	
						}
										
					}	
					
					/*****		Determino los checkBox que se mostrar�n		*****/
					Ext.getCmp('num_proveedor_cont_mod1_1').show();
					/*
					if( Ext.getCmp('num_proveedor_cont_mod1').getValue() =='' ){
						Ext.getCmp('Sin_Num_Prov_cont_mod1').show();
					} else {
						Ext.getCmp('Sin_Num_Prov_cont_mod1').hide();
					}
					*/
					if(Ext.getCmp('CS_ACEPTACION1').getValue() == 'N'){
						Ext.getCmp('susceptible_desconstar_mod1').enable();
						Ext.getCmp('susceptible_desconstar_mod1').setValue(true);
					} else {
						Ext.getCmp('susceptible_desconstar_mod1').disable();
						Ext.getCmp('susceptible_desconstar_mod1').setValue(false);
						
						Ext.getCmp('ejecut_cuenta_mod1').hide();
						Ext.getCmp('ejecut_cuenta_mod1').setValue(false);
					}
					if(flagAnticipo == false){
						Ext.getCmp('anticipo_mod1').enable();
					} else {
						Ext.getCmp('anticipo_mod1').disable();
					}
					if(strHabAnt != 'N'){
						Ext.getCmp('anticipo_mod1').setValue(true);
					} else {
						Ext.getCmp('anticipo_mod1').setValue(false);
					}
					if(flagCrediCadenas == false){
						Ext.getCmp('credicadenas_mod1').enable();
					} else {
						Ext.getCmp('credicadenas_mod1').disable();
					}
					if(strHabInv != 'N'){
						Ext.getCmp('credicadenas_mod1').setValue(true);
					} else {
						Ext.getCmp('credicadenas_mod1').setValue(false);
					}
									
				}
				//	Se llenan los campos ic_ficinas del panel productos
				Ext.getCmp('ic_oficina_tramitadora_descuento1').setValue(Ext.util.JSON.decode(response.responseText).ic_oficina_tramitadora_descuento);
				Ext.getCmp('ic_oficina_tramitadora_anticipo1').setValue(Ext.util.JSON.decode(response.responseText).ic_oficina_tramitadora_anticipo);
				Ext.getCmp('ic_oficina_tramitadora_inventario1').setValue(Ext.util.JSON.decode(response.responseText).ic_oficina_tramitadora_inventario);
				
				gridConsulta.el.unmask();
				fp.setVisible(false);
				gridConsulta.setVisible(false);
				fm.setVisible(true);
				fm.doLayout();

				//F021-2015 (E)
				var  info = Ext.util.JSON.decode(response.responseText);		
				var  total= info.permisos.length;
				
				for(i=0;i<total;i++){ 
					boton=  info.permisos[i];				
					Ext.getCmp(boton).show();	 	
				}		
				
				//F021-2015 (E)
			
				if(info.strTAfilia=='H' ||  info.strTAfilia=='R'){ 
					Ext.getCmp('BTNACTUALIZARNESIRAC').show();
				}else if(info.strTAfilia=='N' ||  info.strTAfilia=='S'){ 
					Ext.getCmp('BTNACTUALIZARNESIRAC').hide();
				}
				
				
				
			} else{
				Ext.Msg.alert('Error', 'Error al obtener los datos' ); 
			}
		}	
			
	/**********		Cat�logo Pais 		**********/  
		var catalogoPais = new Ext.data.JsonStore({
			id:            'catalogoPais',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			autoLoad:      false,			
			fields:        ['clave', 'descripcion', 'loadMsg'],
			baseParams:    {
				informacion: 'catalogoPaisOrigen'
			},	
			listeners: {	
				load: function(){
					if(Ext.getCmp('pais_mod1').getValue()!=''){
						Ext.getCmp('pais_mod1').setValue(Ext.getCmp('pais_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		}); 

	/**********		Cat�logo Pa�s origen (presona f�sica)		**********/  
		var catalogoPaisOrigenPF = new Ext.data.JsonStore({
			id:            'catalogoPaisOrigenPF',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			autoLoad:      false,
			fields:        ['clave', 'descripcion', 'loadMsg'],
			baseParams:    {
				informacion: 'catalogoPaisOrigen'
			},
			
			listeners:     { 	
				load: function(){
					if(Ext.getCmp('pais_origen_modF1').getValue()!=''){
						Ext.getCmp('pais_origen_modF1').setValue(Ext.getCmp('pais_origen_modF1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});

	/**********		Cat�logo Pa�s origen (presona moral)		**********/  	
		var catalogoPaisOrigenPM = new Ext.data.JsonStore({
			id:            'catalogoPaisOrigenPM',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			autoLoad:      false,		
			fields:        ['clave', 'descripcion', 'loadMsg'],
			baseParams:    {
				informacion: 'catalogoPaisOrigen'
			},
			listeners:     {	
				load: function(){
					if(Ext.getCmp('pais_origen_modM1').getValue()!=''){
						Ext.getCmp('pais_origen_modM1').setValue(Ext.getCmp('pais_origen_modM1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});

	/**********		Cat�logo Estado del form modificar 		**********/ 
		var catalogoEstadoMod = new Ext.data.JsonStore({
			id:         'catalogoEstadoMod',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			baseParams: {
				informacion: 'catalogoEstadoMod'
			},
			listeners:   {
				load: function(){
					if(Ext.getCmp('estado_mod1').getValue()!=''){
						Ext.getCmp('estado_mod1').setValue(Ext.getCmp('estado_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}	
		}); 
		
	/**********		Cat�logo Delegaci�n o Municipio		**********/ 
		var catalogoDelegMun = new Ext.data.JsonStore({
			id:            'catalogoDelegMun',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			autoLoad:      false,			
			fields:        ['clave', 'descripcion', 'loadMsg'],
			baseParams:    {
				informacion: 'catalogoDelegMun'
			},					
			listeners:     {	
				load: function(){
					if(Ext.getCmp('deleg_mun_mod1').getValue()!=''){
						Ext.getCmp('deleg_mun_mod1').setValue(Ext.getCmp('deleg_mun_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});	
		
	/**********		Cat�logo Ciudad 		**********/  
		var catalogoCiudad = new Ext.data.JsonStore({
			id:            'catalogoCiudad',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			autoLoad:       false,			
			fields:         ['clave', 'descripcion', 'loadMsg'],
			baseParams:     {
				informacion: 'catalogoCiudad'
			},	
			listeners:      {	
				load: function(){
					if(Ext.getCmp('ciudad_mod1').getValue()!=''){
						Ext.getCmp('ciudad_mod1').setValue(Ext.getCmp('ciudad_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}	
		});
		
	/**********		Cat�logo Sector economico 		**********/ 
		var catalogoSector = new Ext.data.JsonStore({
		id:         'catalogoSector',
			root:       'registros',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			url:        '15forma05ProveedoresExt.data.jsp',
			autoLoad:   false,
			baseParams: { informacion: 'catalogoSectorEconomico'},
			listeners:  {	
				load: function(){
					if(Ext.getCmp('sector_mod1').getValue()!=''){
						Ext.getCmp('sector_mod1').setValue(Ext.getCmp('sector_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
	});  	

	/**********		Cat�logo Sub sector economico 		**********/
		var catalogoSubSector = new Ext.data.JsonStore({
		id:         'catalogoSubSector',
			root:       'registros',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			url:        '15forma05ProveedoresExt.data.jsp',
			autoLoad:   false,
			baseParams: { informacion: 'catalogoSubSector'},
			listeners:  {
			load: function(){
				if(Ext.getCmp('subsector_mod1').getValue()!=''){
					Ext.getCmp('subsector_mod1').setValue(Ext.getCmp('subsector_mod1').getValue());
				}
			},
			exception:  NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
			}
	}); 
		
	/**********		Cat�logo Rama 		**********/
		var catalogoRama = new Ext.data.JsonStore({
		id:         'catalogoRama',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			autoLoad:   false,
			baseParams: { informacion: 'catalogoRama'},
			listeners:  {
			load: function(){
				if(Ext.getCmp('rama_mod1').getValue()!=''){
					Ext.getCmp('rama_mod1').setValue(Ext.getCmp('rama_mod1').getValue());
				}
			},
			exception:  NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
			}
	}); 
	
	/**********		Cat�logo Clase 		**********/
		var catalogoClase = new Ext.data.JsonStore({
		id:         'catalogoClase',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			autoLoad:   false,
			baseParams: { informacion: 'catalogoClase'},
			listeners:  {
			load: function(){
				if(Ext.getCmp('clase_mod1').getValue()!=''){
					Ext.getCmp('clase_mod1').setValue(Ext.getCmp('clase_mod1').getValue());
				}
			},
			exception:  NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
			}
	});  
		
	/**********		Cat�logo Regimen matrimonial 		**********/ 
		var catalogoRegimenMatrimonial = new Ext.data.JsonStore({
			id:            'catalogoRegimenMatrimonial',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			totalProperty: 'total',
			fields:        ['clave', 'descripcion', 'loadMsg'],
			autoLoad:      false,
			baseParams:    {
				informacion:'catalogoRegimenMatrimonial'
			},		
			listeners:     {	
				load: function(){
					if(Ext.getCmp('regimen_matrimonial_mod1').getValue()!=''){
						Ext.getCmp('regimen_matrimonial_mod1').setValue(Ext.getCmp('regimen_matrimonial_mod1').getValue());
					}
				},
				exception: NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});
		
	/**********		Cat�logo estado civil 		**********/ 	
		var catalogoEstadoCivil = new Ext.data.JsonStore({
			id:            'catalogoEstadoCivil',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			autoLoad:      false,		
			fields:        ['clave', 'descripcion', 'loadMsg'],
			baseParams:    {
				informacion: 'catalogoEstadoCivil'
			},		
			listeners:     {	
				load: function(){
					if(Ext.getCmp('estado_civil_mod1').getValue()!=''){
						Ext.getCmp('estado_civil_mod1').setValue(Ext.getCmp('estado_civil_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});

	/**********		Cat�logo identificaci�n NC 		**********/ 
		var catalogoIdentificacionNC = new Ext.data.JsonStore({
			id:            'catalogoIdentificacionNC',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			autoLoad:      false,			
			fields:        ['clave', 'descripcion', 'loadMsg'],
			baseParams:    {
				informacion: 'catalogoIdentificacion'
			},
			listeners: {	
				load: function(){
					if(Ext.getCmp('identificacion_mod1').getValue()!=''){
						Ext.getCmp('identificacion_mod1').setValue(Ext.getCmp('identificacion_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});

	/**********		Cat�logo identificaci�n RL 		**********/ 
		var catalogoIdentificacionRL = new Ext.data.JsonStore({
			id:            'catalogoIdentificacionRL',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			autoLoad:      false,			
			fields:        ['clave', 'descripcion', 'loadMsg'],
			baseParams:    { informacion: 'catalogoIdentificacion' }, 
			listeners:     {	
				load: function(){
					if(Ext.getCmp('identificacion_rep_mod1').getValue()!=''){
						Ext.getCmp('identificacion_rep_mod1').setValue(Ext.getCmp('identificacion_rep_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});	

	/**********		Cat�logo grado de escolaridad 		**********/ 
		var catalogoGradoEscolaridad = new Ext.data.JsonStore({
			id:            'catalogoGradoEscolaridad',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			autoLoad:      false,		
			fields:        ['clave', 'descripcion', 'loadMsg'],
			baseParams:    {
				informacion: 'catalogoGradoEscolaridad'
			},		
			listeners: {	
				load: function(){
					if(Ext.getCmp('grado_escolaridad_mod1').getValue()!=''){
						Ext.getCmp('grado_escolaridad_mod1').setValue(Ext.getCmp('grado_escolaridad_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});

	/**********		Cat�logo Ejecutivo de cuenta 		**********/
		var catalogoEjecutivoCuenta = new Ext.data.JsonStore({
			id:            'catalogoEjecutivoCuenta',
			root:          'registros',
			url:           '15forma05ProveedoresExt.data.jsp',
			autoLoad:      false,			
			fields:        ['clave', 'descripcion', 'loadMsg'],
			baseParams:    {
				informacion: 'catalogoEjecutivoCuenta'
			},					
			listeners:     {	
				load: function(){
					if(Ext.getCmp('ejecut_cuenta_mod1').getValue()!=''){
						Ext.getCmp('ejecut_cuenta_mod1').setValue(Ext.getCmp('ejecut_cuenta_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}		
		});

	/**********		Cat�logo Tipo de categor�a 		**********/  
		var catalogoTipoCategoria = new Ext.data.JsonStore({
		id:         'catalogoTipoCategoria',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			autoLoad:   false,
			baseParams: { informacion: 'catalogoTipoCategoria'},
			listeners:  {	
				load: function(){
					if(Ext.getCmp('tipo_categoria_mod1').getValue()!=''){
						Ext.getCmp('tipo_categoria_mod1').setValue(Ext.getCmp('tipo_categoria_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
	});  	


	/**********		Cat�logo Tipo Empresa 		**********/
		var catalogoTipoEmpresa = new Ext.data.JsonStore({
		id:         'catalogoTipoEmpresa',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			autoLoad:   false,
			baseParams: { informacion: 'catalogoTipoEmpresa'},
			listeners:  {	
				load: function(){
					if(Ext.getCmp('tipo_empresa_mod1').getValue()!=''){
						Ext.getCmp('tipo_empresa_mod1').setValue(Ext.getCmp('tipo_empresa_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
	});  

	/**********		Cat�logo Domicilio de Correspondencia 		**********/
		var catalogoDomicilioCorrespondencia = new Ext.data.JsonStore({
		id:         'catalogoDomicilioCorrespondencia',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			autoLoad:   false,
			baseParams: { informacion: 'catalogoDomicilioCorrespondencia'},
			listeners:  {	
				load: function(){
					if(Ext.getCmp('domicilio_corr_mod1').getValue()!=''){
						Ext.getCmp('domicilio_corr_mod1').setValue(Ext.getCmp('domicilio_corr_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
	}); 
	
	/**********		Cat�logo Version de convenio 		**********/
		var catalogoVersionConvenio = new Ext.data.JsonStore({
		id:         'catalogoVersionConvenio',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			autoLoad:   false,
			baseParams: { informacion: 'catalogoVersionConvenio'},
			listeners:  {	
				load: function(){
					if(Ext.getCmp('version_convenio_mod1').getValue()!=''){
						Ext.getCmp('version_convenio_mod1').setValue(Ext.getCmp('version_convenio_mod1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
	}); 

	/**********		Cat�logo Agencia sirac Descuento 		**********/
		var catalogoAgenciaSiracDescuento = new Ext.data.JsonStore({
		id:         'catalogoAgenciaSiracDescuento',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			autoLoad:   false,
			baseParams: { informacion: 'catalogoAgenciaSirac'},
			listeners:  {	
				load: function(){
					if(Ext.getCmp('ic_oficina_tramitadora_descuento1').getValue()!=''){
						Ext.getCmp('ic_oficina_tramitadora_descuento1').setValue(Ext.getCmp('ic_oficina_tramitadora_descuento1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
	}); 
	
	/**********		Cat�logo Agencia sirac Anticipo 		**********/  
		var catalogoAgenciaSiracAnticipo = new Ext.data.JsonStore({
		id:         'catalogoAgenciaSiracAnticipo',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			autoLoad:   false,
			baseParams: { informacion: 'catalogoAgenciaSirac'},
			listeners:  {	
				load: function(){
					if(Ext.getCmp('ic_oficina_tramitadora_anticipo1').getValue()!=''){
						Ext.getCmp('ic_oficina_tramitadora_anticipo1').setValue(Ext.getCmp('ic_oficina_tramitadora_anticipo1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
	});   
	
	/**********		Cat�logo Agencia sirac Inventario 		**********/  
	var catalogoAgenciaSiracInventario = new Ext.data.JsonStore({
		id:         'catalogoAgenciaSiracInventario',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			autoLoad:   false,
			baseParams: { informacion: 'catalogoAgenciaSirac'},
			listeners:  {	
				load: function(){
					if(Ext.getCmp('ic_oficina_tramitadora_inventario1').getValue()!=''){
						Ext.getCmp('ic_oficina_tramitadora_inventario1').setValue(Ext.getCmp('ic_oficina_tramitadora_inventario1').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
	}); 
	
	/**********		Cat�logo Solicitante EPO 		**********/  
	var catalogoSolicitanteEPO = new Ext.data.JsonStore({
		id:         'catalogoSolicitanteEPO',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			autoLoad:   false,
			baseParams: { informacion: 'catalogoSolicitanteEPO'},
			listeners:  {
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
	}); 
	

		var fm = new Ext.FormPanel({
			width:							810,
			id:								'formaModificar',
			style:							'margin: 0 auto;',
			labelAlign: 					'right',
			title:							'Actualizar datos',
			bodyStyle:						'padding: 5px 5px 0',
			frame:							true,
			hidden:							true,
			autoHeight:						true,
			items: [{								
				xtype:							'fieldset',
				title:							'*Solicitante de la modificaci�n',
				collapsible:					false,
				autoHeight:						true,
				items :[{						
				width:							800,
				layout:							'column',
				items:[{						
				columnWidth:					.4,
					layout:						'form',
					labelWidth:					60,
					items: [{					
						xtype:					'radiogroup',
						id:						'solicitante_mod1',
						name:						'solicitante_mod',
						msgTarget:				'side',
						anchor:					'95%',
						align:					'center',
						cls:						'x-check-group-alt',
						style:					{ width: '90%'},
						allowBlank:				true,
						items:					[		
							{	boxLabel:	'A petici�n del proveedor',	name: 'solicitante_mod', inputValue: 'P' },
							{	boxLabel:	'EPO',								name: 'solicitante_mod', inputValue: 'E' }
						]						
					}]							
				},{								
					columnWidth:				.5,
					layout:						'form',
					labelWidth:					1,
					items: [{					
						xtype:					'combo',
						id:						'combo_solicitante_mod1',
						name:						'combo_solicitante_mod',
						msgTarget:				'side',
						anchor:					'95%',
						triggerAction:			'all',
						mode:						'local',
						valueField:				'clave',
						displayField:			'descripcion',
						forceSelection:		true,
						store:					catalogoSolicitanteEPO
					}]					
				}]						
			}]							
			},{						
				xtype:							'fieldset',
				title: 							'Nombre Completo',
				id:	 							'nombre_persona',   
				collapsible: 					false,
				autoHeight:						true,
				labelWidth: 					120,
				items :[{					
					layout:						'column',
					id:							'nombre_persona1', 
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Apellido paterno',
							id:					'apellido_paterno_mod1',
							name:					'apellido_paterno_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			100
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Apellido materno',
							id:					'apellido_materno_mod1',
							name:					'apellido_materno_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			100
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'nombre_persona2',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Nombre(s)',
							id:					'nombre_mod1',
							name:					'nombre_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			60
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			    
							xtype:				'compositefield',
							msgTarget:			'side',
							combineErrors:		false,
							items: [		
								{				
									width:			150,
									xtype:			'textfield',
									fieldLabel:		'*RFC',
									id:				'rfc_mod1',
									name:				'rfc_mod',
									msgTarget:		'side',
									anchor:			'95%',
									vtype:         'validaRFC',
									maxLength:		20
									/*regex:			/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,							
									regexText:		'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
														'en el formato NNN-AAMMDD-XXX donde:<br>'+
														'NNN:son las iniciales del nombre de la empresa<br>'+
														'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
														'XXX:es la homoclave'*/
								},{				
									width:			10,
									frame:			false,
									border:			false
								},{				
									width:			90,
									xtype:			'checkbox',	
									id:				'invalidar_modF1',
									name:				'invalidar_modF',
									boxLabel:		'Invalidar',
									listeners:		{
										check:		function(){
											Ext.getCmp('rfc_mod1').setValue('XXXX-000101-XXX');
										}	
									}		
								}			
							]				
						}]					
					}]						
				},{							
					layout:						'column',
					id: 							'nombre_persona3',
					items:[{					
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'CURP',
							id:					'curp_mod1',
							name:					'curp_mod',
							msgTarget:			'side',
							anchor:				'95%',
							minLength:			18,
							maxLength:			18
						}]						
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'FIEL',
							id:					'fiel_modF1',
							name:					'fiel_modF',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:	 		25
						}]						
					}]							
				},{							
					layout:						'column',
					id: 							'nombre_persona4', 
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'Apellido de casada',
							id:					'apellido_casada_mod1',	
							name:					'apellido_casada_mod',
							msgTarget:			'side',
							anchor: 				'95%',
							maxLength:			60
						}]					
					},{					 	
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'Regimen matrimonial',
							id:					'regimen_matrimonial_mod1',	
							name:					'regimen_matrimonial_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoRegimenMatrimonial
						}]						
					}]						
				},{							
					layout:						'column',
					id:							'nombre_persona5',
					items:[{				
					columnWidth:				.5,
						layout:					'form',
						items: [{			
							xtype:				'radiogroup',
							fieldLabel:			'*Sexo',
							id:					'sexo_mod1',
							name:					'sexo_mod',
							msgTarget:			'side',
							anchor:				'80%',
							align:				'center',
							cls:					'x-check-group-alt',
							style:				{ width: '25%'},
							items:				[		
								{	boxLabel: 'F', name: 'sexo_mod', inputValue: 'F' },
								{	boxLabel: 'M', name: 'sexo_mod', inputValue: 'M' }
							]				
						}]					
						},{					
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'*Estado civil',
							id:					'estado_civil_mod1',
							name:					'estado_civil_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoEstadoCivil
						}]					
					}]						
					},{						
					layout:						'column',
					id:							'nombre_persona6',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'datefield',
							fieldLabel:			'*Fecha de nacimiento',
							id:					'fecha_nac_mod1',
							name:					'fecha_nac_mod',
							msgTarget:			'side',
							anchor:				'95%',
							emptyText:			'dd/MM/aaaa ',
							startDay:			1,
							minValue:			'01/01/1901'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'Pa�s de origen',
							id:					'pais_origen_modF1',
							name:					'pais_origen_modF',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoPaisOrigenPF
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'nombre_persona7',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'*Identificaci�n',
							id:					'identificacion_mod1',
							name:					'identificacion_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store: 				catalogoIdentificacionNC
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'*Grado de escolaridad',
							id:					'grado_escolaridad_mod1',
							name:					'grado_escolaridad_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoGradoEscolaridad
						}]					
					}]						
				}]							
			},{								
				xtype:							'fieldset',
				title:							'Nombre de la empresa',
				id:								'nombre_empresa',
				collapsible:					false,
				autoHeight:						true,
				labelWidth:						120,
				items :[{					
						width:					620,
						xtype:					'textfield',
						fieldLabel:				'*Raz�n social',
						id:						'razon_social_mod1',
						name:						'razon_social_mod',
						msgTarget:				'side',
						maxLength:				100
					},{						
					layout:						'column',
					id:							'nombre_empresa2',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype: 				'textfield',
							fieldLabel:			'Nombre Comercial',
							id:					'nombre_comer_mod1',
							name: 				'nombre_comer_mod',
							msgTarget: 			'side',
							anchor:				'95%',
							maxLength:			60
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'compositefield',
							msgTarget:			'side',
							combineErrors:		false,
							items: [		
								{			
									width:			150,
									xtype:			'textfield',
									fieldLabel:		'*RFC',
									id:				'rfc_modM1',
									name:				'rfc_modM',
									msgTarget:		'side',
									anchor:			'95%',
									vtype:         'validaRFC',
									maxLength:		20
									/*regex:			/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
									regexText:		'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
														'en el formato NNN-AAMMDD-XXX donde:<br>'+
														'NNN:son las iniciales del nombre de la empresa<br>'+
														'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
														'XXX:es la homoclave'*/
								},{				
									width:			10,
									frame:			false,
									border:			false
								},{				
									width:			90,
									xtype:			'checkbox',
									name:				'invalidar_modM',
									id:				'invalidar_modM1',
									boxLabel:		'Invalidar',
									listeners:		{
										check:		function(){
											Ext.getCmp('rfc_modM1').setValue('XXXX-000101-XXX');
										}	
									}		
								}			
							]				
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'nombre_empresa3',
					items:[{				 
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel: 		'FIEL',
							id:					'fiel_modM1',
							name:					'fiel_modM',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			25
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'Pa�s de origen',
							id:					'pais_origen_modM1',
							name:					'pais_origen_modM',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoPaisOrigenPM //Persona moral
						}]					
					}]						
				}]							
			},{								
				xtype:							'fieldset',
				title:							'Domicilio',
				id:								'domicilio',
				collapsible:					false,
				autoHeight:						true,
				labelWidth:						120,
				items :[{					
					layout:						'column',
					id:							'domicilio1', 
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Calle no. ext y no. int',
							id:					'direccion_mod1',
							name:					'direccion_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			100,
							allowBlank:			false
						}]					
					},{						
						columnWidth: 			.5,
						layout: 					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Colonia',
							id:					'colonia_mod1',
							name:					'colonia_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			100,
							allowBlank:			false
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'domicilio2',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*C�digo postal',
							id:					'cod_postal_mod1',
							name:					'cod_postal_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			5,
							minLength:			5,
							allowBlank:			false,
							regex: 				/^[0-9]*$/,
							regexText:			'Solo debe contener n�meros'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'*Pa�s',
							id:					'pais_mod1',
							name:					'pais_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							allowBlank:			false,
							store:				catalogoPais,
							listeners: 			{
								select:			function(combo, record, index) {
									Ext.getCmp('estado_mod1').reset();
									Ext.getCmp('deleg_mun_mod1').reset();
									Ext.getCmp('ciudad_mod1').reset();
									catalogoEstadoMod.load({
										params: Ext.apply({
											combo_pais: combo.getValue()
										})	
									});		
								}			
							}				
						}]					
					}]						
				},{							
					layout:						'column',
					id: 							'domicilio3',  
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype: 				'combo',
							fieldLabel:			'*Estado',
							id:					'estado_mod1',
							name:					'estado_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							allowBlank:			false,
							store:				catalogoEstadoMod,
							listeners:			{
								select: function(combo, record, index) {
									Ext.getCmp('deleg_mun_mod1').reset();
									Ext.getCmp('ciudad_mod1').reset();
									catalogoDelegMun.load({
										params: Ext.apply({
											combo_estado : record.json.clave,
											combo_pais : Ext.getCmp('pais_mod1').getValue()
										})	
									});		
									catalogoCiudad.load({
										params: Ext.apply({
											combo_estado : record.json.clave,
											combo_pais : Ext.getCmp('pais_mod1').getValue()
										})	
									});		
								}			
							}				
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'*Deleg./ Municipio',
							id:					'deleg_mun_mod1',
							name:					'deleg_mun_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							allowBlank:			false,
							store:				catalogoDelegMun
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'domicilio4',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'Ciudad',
							id:					'ciudad_mod1',
							name:					'ciudad_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoCiudad
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Tel�fono',
							id:					'telefono_mod1',
							name:					'telefono_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			30,
							allowBlank:			false,						
							regexText:			'Solo debe contener n�meros'
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'domicilio5', 
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype: 				'textfield',
							fieldLabel:			'Fax',
							id:					'fax_mod1',
							name:					'fax_mod',
							msgTarget:			'side',
							anchor:				'95%'
						}]					
					}]						
				}]							
			},{								
				xtype:							'fieldset',
				title:							'&nbsp;',
				id: 								'representante_legal',
				collapsible:					false,
				autoHeight:						true,
				labelWidth:						120,
				items: [{					
					layout:						'column',
					id:							'representante_legal1',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'Apellido paterno',
							id:					'ap_paterno_rep_mod1',
							name:					'ap_paterno_rep_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			60
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'Apellido materno',
							id:					'ap_materno_rep_mod1',
							name:					'ap_materno_rep_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			60
						}]					
					}]						
				},{							
					layout:						'column',
					id: 							'representante_legal2',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'Nombre(s)',
							id:					'nombre_rep_mod1',
							name:					'nombre_rep_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			60
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'Tel�fono',
							id:					'telefono_rep_mod1',
							name:					'telefono_rep_mod',
							msgTarget:			'side',
							anchor:				'95%'
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'representante_legal3',	//Se muestra cuando es persona moral
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'Fax',
							id:					'fax_rep_mod1',
							name:					'fax_rep_mod',
							msgTarget:			'side',
							anchor:				'95%'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype: 				'textfield',
							fieldLabel:			'e-mail',
							id:					'email_rep_mod1',
							name:					'email_rep_mod',
							msgTarget:			'side',
							anchor:				'95%'
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'representante_legal4',	//Se muestra cuando es persona moral
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'RFC',
							id:					'rfc_rep_mod1',
							name:					'rfc_rep_mod',
							msgTarget:			'side',
							anchor:				'95%'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*No. de identificaci�n',
							id:					'no_identificacion_rep_mod1',
							name:					'no_identificacion_rep_mod',
							msgTarget:			'side',
							anchor:				'95%'
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'representante_legal5',	//Se muestra cuando es persona moral
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'*Identificaci�n',
							id:					'identificacion_rep_mod1',
							name:					'identificacion_rep_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoIdentificacionRL
						}]					
					}]						
				}]							
			},{								
				xtype:							'fieldset',
				title:							'Datos del contacto',
				id:								'datos_contacto',
				collapsible:					false,
				autoHeight:						true,
				labelWidth:						120,
				items: [{					
					layout:						'column',
					id:							'datos_contacto1',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Apellido paterno',
							id:					'ap_paterno_cont_mod1',	
							name:					'ap_paterno_cont_mod',
							msgTarget:			'side',
							anchor:				'95%',
							allowBlank:			false       
						}]						
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Apellido materno',
							id:					'ap_materno_cont_mod1',
							name:					'ap_materno_cont_mod',
							msgTarget:			'side',
							anchor:				'95%',
							allowBlank:			false
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'datos_contacto2',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Nombre(s)',
							id:					'nombre_cont_mod1',
							name:					'nombre_cont_mod',
							msgTarget:			'side',
							anchor:				'95%',
							allowBlank:			false
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Tel�fono',
							id:					'telefono_cont_mod1',
							name:					'telefono_cont_mod',
							msgTarget:			'side',
							anchor:				'95%',
							allowBlank:			false
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'datos_contacto3',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'Fax',
							id:					'fax_cont_mod1',
							name:					'fax_cont_mod',
							msgTarget:			'side',
							anchor:				'95%'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*e-mail',
							id:					'email_cont_mod1',
							name:					'email_cont_mod',
							msgTarget:			'side',
							anchor:				'95%',
							allowBlank:			false
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'datos_contacto4',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Celular:&nbsp; 044',
							id:					'celular_cont_mod1',
							name:					'celular_cont_mod',
							msgTarget:			'side',
							anchor:				'95%',
							regex:				/^[0-9]*$/,
							allowBlank:			false
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						id:					'num_proveedor_cont_mod1_1',
						items: [
							{			
							xtype:				'compositefield',
							msgTarget:			'side',
							combineErrors:		false,
							items: [{			
								width:				118,
								xtype:				'textfield',
								fieldLabel:			'*No. de proveedor',
								id:					'num_proveedor_cont_mod1',
								name:					'num_proveedor_cont_mod',
								msgTarget:			'side',
								anchor:				'95%' ,
								listeners:{
								blur: function(field){ 
									var valor=  Ext.getCmp('num_proveedor_cont_mod1').getValue();
									Ext.getCmp('num_proveedor_diversos_mod1').setValue(valor);
								}
							}	
							},{					
								width:				12,
								frame:				false,
								border:				false
							},{					
								width:				120,
								xtype:				'checkbox',
								boxLabel:			'Sin no. proveedor',
								id:					'Sin_Num_Prov_cont_mod1',
								name:					'Sin_Num_Prov_cont_mod',
								msgTarget:			'side',
								anchor:				'95%',
														listeners:		{
										check: function(object) {
																					if(object.checked){
																						Ext.getCmp('num_proveedor_cont_mod1').setValue("");
																					}
											Ext.getCmp('num_proveedor_cont_mod1').setDisabled(object.checked);
										}
																			
									}
													}]					
						}]						
					}]							
				},{							
					layout:						'column',
					id:							'datos_contacto5',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'*Tipo de categor�a',
							id:					'tipo_categoria_mod1',
							name:					'tipo_categoria_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoTipoCategoria    
						}]					
					},{						
						columnWidth: 			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Ejecutivo de cuenta',
							id:					'ejecut_cuenta_mod1',
							name:					'ejecut_cuenta_mod',
							msgTarget:			'side',
							anchor:				'95%'
						}]					
					}]						
				}]							
			},{								
				xtype:							'fieldset',
				title:							'Diversos',
				id:								'diversos',
				collapsible:					false,
				autoHeight:						true,
				labelWidth:						120,
				items: [{					
					layout:						'column',
					id:							'diversos1',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*No. de escritura',
							id:					'no_escritura_mod1',
							name:					'no_escritura_mod',
							msgTarget:			'side',
							anchor:				'95%',
							regex: 				/^([0-9])*[.]?[0-9]*$/,
							regexText:			'Solo debe contener n�meros'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*No. de notar�a',
							id:					'no_notaria_mod1',
							name:					'no_notaria_mod',
							msgTarget:			'side',
							anchor:				'95%',
							regex:				/^[0-9]*$/,
							regexText:			'Solo debe contener n�meros'
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'diversos2',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Empleos a generar',
							id:					'empleos_gen_mod1',
							name:					'empleos_gen_mod',
							msgTarget:			'side',
							anchor:				'95%',
							regex:				/^[0-9]*$/,
							regexText:			'Solo debe contener n�meros'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Activo total',
							id:					'activo_total_mod1',
							name:					'activo_total_mod',
							msgTarget:			'side',
							anchor:				'95%',
							regex:				/^([0-9])*[.]?[0-9]*$/,
							regexText:			'Solo debe contener n�meros'
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'diversos3',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Capital contable',
							id:					'cap_contable_mod1',
							name:					'cap_contable_mod',
							msgTarget:			'side',
							anchor:				'95%',
							regex:				/^([0-9])*[.]?[0-9]*$/,
							regexText:			'Solo debe contener n�meros'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Ventas netas exportaci�n',
							id:					'ventas_exp_mod1',
							name:					'ventas_exp_mod',
							msgTarget:			'side',
							anchor:				'95%',
							regex:				/^([0-9])*[.]?[0-9]*$/,
							regexText:			'Solo debe contener n�meros'
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'diversos4',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'*Ventas netas totales',
							id:					'ventas_tot_mod1',
							name:					'ventas_tot_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			21,
							regex:				/^([0-9])*[.]?[0-9]*$/,
							regexText:			'Solo debe contener n�meros'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'datefield',
							fieldLabel:			'*Fecha de constituci�n',
							id:					'fecha_const_mod1',	
							name:					'fecha_const_mod',
							msgTarget:			'side',
							anchor:				'95%',
							emptyText:			'dd/MM/aaaa ',
							minValue:			'01/01/1901',
							startDay:			1
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'diversos5',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{				
							xtype:				'textfield',
							fieldLabel:			'*No. de empleados',
							id:					'no_empleados_mod1',
							name:					'no_empleados_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength:			6,
							regex:				/^[0-9]*$/,
							regexText:			'Solo debe contener n�meros'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						id:						'num_proveedor_diversos_mod1_1',
						items: [
							{			
								xtype:				'compositefield',
								msgTarget:			'side',
								combineErrors:		false,
								items: [
									{			
										width:				118,
										xtype:				'textfield',
										fieldLabel:			'*No. de proveedor',
										id:					'num_proveedor_diversos_mod1',
										name:					'num_proveedor_diversos_mod',
										msgTarget:			'side',
										anchor:				'95%',
										listeners:{
											blur: function(field){ 
												var valor=  Ext.getCmp('num_proveedor_diversos_mod1').getValue();
												Ext.getCmp('num_proveedor_cont_mod1').setValue(valor);
											}
										}						
									},{					
										width:				12,
										frame:				false,
										border:				false
									},{					
										width:				120,
										xtype:				'checkbox',
										boxLabel:			'Sin no. proveedor',
										id:					'Sin_Num_Prov_diversos_mod1',
										name:					'Sin_Num_Prov_diversos_mod',
										msgTarget:			'side',
										anchor:				'95%',
									listeners:		{
										check: function(object) {
																					if(object.checked){
																						Ext.getCmp('num_proveedor_diversos_mod1').setValue("");
																					}
											Ext.getCmp('num_proveedor_diversos_mod1').setDisabled(object.checked);
										}
																			
									}		
																	
																	}
								]
							}
						]
					}]						
				},{							
					layout:						'column',
					id:							'diversos10',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'*Tipo de empresa',
							id:					'tipo_empresa_mod1',
							name:					'tipo_empresa_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode: 				'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoTipoEmpresa
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'combo',
							fieldLabel:			'*Domicilio de correspondencia',
							id:					'domicilio_corr_mod1',
							name:					'domicilio_corr_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoDomicilioCorrespondencia
						}]					
					}]						
				},{							
					width:						620,	//id:           'diversos6',
					xtype:						'combo',
					fieldLabel:					'*Sector econ�mico',
					id:							'sector_mod1',
					name:							'sector_mod',
					msgTarget:					'side',
					triggerAction:				'all',
					mode:							'local',
					valueField:					'clave',
					displayField:				'descripcion',
					forceSelection:			true,
					store:						catalogoSector,
					listeners:					{
						select:					function(combo, record, index) {
							Ext.getCmp('rama_mod1').reset();
							Ext.getCmp('clase_mod1').reset();
							catalogoSubSector.load({
								params: Ext.apply({
									sector_econ: combo.getValue()
								})			
							});				
						}					
					}						
				},{							
					width:						620,	//id:           'diversos7',
					xtype:						'combo',
					fieldLabel:					'*Subsector',
					id:							'subsector_mod1',
					name:							'subsector_mod',
					msgTarget:					'side',
					triggerAction:				'all',
					mode:							'local',
					valueField:					'clave',
					displayField:				'descripcion',
					forceSelection:			true,
					store:						catalogoSubSector,
					listeners:					{
						select:					function(combo, record, index) {
							Ext.getCmp('clase_mod1').reset();
							catalogoRama.load({
								params: Ext.apply({
									sector_econ :  Ext.getCmp('sector_mod1').getValue(), 
									subsector: combo.getValue()
								})			
							});				
						}					
					}						
				},{							
					width:						620,	//id: 'diversos8',
					xtype:						'combo',
					fieldLabel:					'*Rama',
					id:							'rama_mod1',
					name:							'rama_mod',
					msgTarget:					'side',
					triggerAction:				'all',
					mode:							'local',
					valueField:					'clave',
					displayField:				'descripcion',
					forceSelection:			true,
					store:						catalogoRama,
					listeners:					{
						select:					function(combo, record, index) {
							catalogoClase.load({
								params: Ext.apply({
									sector_econ: Ext.getCmp('sector_mod1').getValue(),
									subsector: Ext.getCmp('subsector_mod1').getValue(),
									rama: combo.getValue()
								})			
							});				
						}					
					}						
				},{							
					width:						620,	//id: 'diversos9',
					xtype:						'combo',
					fieldLabel:					'*Clase',
					id:							'clase_mod1',
					name:							'clase_mod',
					msgTarget:					'side',
					triggerAction:				'all',
					mode:							'local',
					valueField:					'clave',
					displayField:				'descripcion',
					forceSelection:			true,
					store:						catalogoClase
				},{							
					width:						620,	//id: 'diversos11',
					xtype: 						'textfield',
					fieldLabel:					'*Principales productos',
					id:							'princip_productos_mod1',
					name:							'princip_productos_mod',
					msgTarget: 					'side',
									maxLength:			60
				}]							
			},{								
				xtype:							'fieldset',
				title:							'Otros datos',
				id:								'otros_datos',
				collapsible:					false,
				autoHeight:						true,
				labelWidth:						120,
				items: [{					
					layout:						'column',
					id:							'otros_datos1',
					items:[{				
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'N�mero SIRAC',
							id:					'num_sirac_mod1',
							name:					'num_sirac_mod',
							msgTarget:			'side',
							anchor:				'95%',
							maxLength: 			12,
							regex:				/^[0-9]*$/,
							regexText:			'Solo debe contener n�meros'
						}]					
					},{						
						columnWidth:			.5,
						layout:					'form',
						items: [{			
							width:				120,
							xtype:				'textfield',
							fieldLabel:			'No. de cliente TROYA',
							id:					'num_troya_mod1',
							name:					'num_troya_mod',
							msgTarget:			'side',
							anchor:				'95%',
							hidden:				false
						},{					
							xtype: 				'checkbox',
							id:					'alta_cliente_troya_mod1',//id: 'otros_datos2'
							name:					'alta_cliente_troya_mod',
							boxLabel:			'Alta del cliente a TROYA',
							hidden:				false
						}]					
					}]						
				},{							
					xtype:						'radiogroup',
					fieldLabel:					'Autorizo a NAFIN mandar notificaciones v�a email y/o SMS',
					id:							'autoriza_mod1', //id: otros_datos3
					name:							'autoriza_mod',
					msgTarget:					'side',
					anchor:						'40%',
					align:						'center',
					cls:							'x-check-group-alt',
					style:						{ width: '25%'},
					items:						[
						{	boxLabel	: 'Si', name : 'autoriza_mod', inputValue: 'S' },
						{	boxLabel	: 'No', name : 'autoriza_mod', inputValue: 'N' }
					]						
				}]							
			},{								
				xtype:							'fieldset',
				title:							'Productos',
				id:								'productos',
				collapsible:					false,
				autoHeight:						true,
				items: [{							
					layout:						'column',
					id:							'productos1',
					items:[{						
						columnWidth:			.25,
						layout:					'form',
						labelWidth:				1,
						items: [{				
							xtype:				'checkbox',
							id:					'susceptible_desconstar_mod1',
							name:					'susceptible_desconstar_mod',
							boxLabel:			'Susceptible a descontar'		
						}]					
					},{					
						columnWidth:			.40,
						layout:					'form',
						labelWidth:				110,
						items: [{			
							xtype:				'combo',
							fieldLabel:			'Oficina tramitadora',
							id:					'ic_oficina_tramitadora_descuento1',
							name:					'ic_oficina_tramitadora_descuento',
							msgTarget:			'side',
							anchor:				'100%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoAgenciaSiracDescuento
						}]					
					},{					
						columnWidth:			.35,
						layout:					'form',
						labelWidth:				80,
						items: [{			
							xtype:				'combo',
							fieldLabel:			'Versi�n',
							id:					'version_convenio_mod1',
							name:					'version_convenio_mod',
							msgTarget:			'side',
							anchor:				'95%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoVersionConvenio,
							tpl:'<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>',
							setNewEmptyText: function(emptyTextMsg){
								this.emptyText = emptyTextMsg;
								this.setValue('');
								this.applyEmptyText();
								this.clearInvalid();						
							} 
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'productos2',
					items:[{				
						columnWidth:			.25,
						layout:					'form',
						labelWidth:				1,
						items: [{				
							xtype:				'checkbox',
							id:					'anticipo_mod1',
							name:					'anticipo_mod',
							boxLabel:			'Susceptible a anticipo'		
						}]					
					},{					
						columnWidth:			.40,
						layout:					'form',
						labelWidth:				110,
						items: [{			
							xtype:				'combo',
							fieldLabel:			'Oficina tramitadora',
							id:					'ic_oficina_tramitadora_anticipo1',
							name:					'ic_oficina_tramitadora_anticipo',
							msgTarget:			'side',
							anchor:				'100%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoAgenciaSiracAnticipo
						}]					
					},{					
						columnWidth:			.35,
						layout:					'form',
						labelWidth:				80,
						items: [{			
							xtype:				'textfield',
							fieldLabel:			'Fecha de consulta',
							id:					'fecha_consulta_convenio_mod1',
							name:					'fecha_consulta_convenio_mod',
							msgTarget:			'side',
							anchor:				'95%'
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'productos3',
					items:[{				
						columnWidth:			.25,
						layout:					'form',
						labelWidth:				1,
						items: [{				
							xtype:				'checkbox',
							id:					'credicadenas_mod1',
							name:					'credicadenas_mod',
							boxLabel:			'Susceptible a credicadenas'		
						}]					
					},{					
						columnWidth:			.40,
						layout:					'form',
						labelWidth:				110,
						items: [{			
							xtype:				'combo',
							fieldLabel:			'Oficina tramitadora',
							id:					'ic_oficina_tramitadora_inventario1',
							name:					'ic_oficina_tramitadora_inventario',
							msgTarget:			'side',
							anchor:				'100%',
							triggerAction:		'all',
							mode:					'local',
							valueField:			'clave',
							displayField:		'descripcion',
							forceSelection:	true,
							store:				catalogoAgenciaSiracInventario
						}]					
					},{				
						columnWidth:			.35,
						layout:					'form',
						labelWidth:				1,//25
						items: [{				
							xtype:				'checkbox',
							id:					'convenio_unico_prod_mod1',
							name:					'convenio_unico_cont_mod',
							boxLabel:			'Opera Convenio �nico'		
						}]					
					}]						
				},{							
					layout:						'column',
					id:							'productos4',
					items:[{				
						columnWidth:			.40,
						layout:					'form',
						labelWidth:				1,
						items: [{				
							xtype:				'checkbox',
							id:					'cesion_der_prod_mod1',
							name:					'cesion_derechos_cont_mod',
							boxLabel:			'Susceptible a cesi�n de derechos'		
						}]					
					},{				
						columnWidth:			.5,
						layout:					'form',
						labelWidth:				1,
						items: [
						{				
							xtype:				'checkbox',
							id:					'fideicomiso_desarrollo_prod_mod1',
							name:					'fideicomiso_desarrollo_prod_mod',
							boxLabel:			'Opera Fideicomiso para Desarrollo de Proveedores'		
						},
						{				
							xtype:				'checkbox',
							id:					'chkEntidadGobierno1',
							name:					'chkEntidadGobierno',
							boxLabel:			'Entidad de Gobierno'		
						},
						{				
							xtype:				'checkbox',
							id:					'chkFactDistribuido1',
							name:					'chkFactDistribuido',
							boxLabel:			'Opera Factoraje Distribuido'		
						},
											{				
							xtype:	'checkbox',
							id:'chkProvExtranjero',
							name:	'chkProvExtranjero',
							boxLabel:'Proveedor Extranjero'	 	
						},
						{
							xtype: 'compositefield',
							combineErrors: false,
							forceSelection: false,
							items:[
							{
								xtype: 'checkbox',
								id:'chkOperaDescAutoEPO',
								name:	'chkOperaDescAutoEPO'
							},
							{
								xtype: 'displayfield',
								id:'displayF',
								name: 'displayF'
							}
							]
						}
						]					
					}]						
				},{							
					layout:						'column',
					id:							'productos5',
					items:[
					{				
						columnWidth:			.5,
						layout:					'form',
						labelWidth:				200,
						items: [{					
							xtype:				'datefield',
							fieldLabel:			'*Fecha de firma de convenio �nico',
							id:					'fecha_convenio_prod_mod1',
							name:					'fecha_convenio_prod_mod',
							msgTarget:			'side',
							anchor:				'80%',
							emptyText:			'dd/MM/aaaa ',
							minValue:			'01/01/1901',
							startDay:			1
						}]					
					}				
					]						
				},{							
					layout:						'column',
					id:							'productos6',
					items:[{				
						columnWidth:			.33,
						layout:					'form',
						labelWidth:				1,
						items: [{				
							xtype:				'checkbox',
							id:					'fianza_electronica_mod1',
							name:					'fianza_elec_cont_mod',
							boxLabel:			'Susceptible a fianza electr�nca'		
						}]					
					}]						
				}]							
			},{			
				xtype:				'panel',  
				id:					'datos_ocultos',
				layout:				'table', 
				hidden:				true,
				border:				false,
				width:				800,
				layoutConfig:		{ columns: 8 },
				items: [			
					{				
						width:		90,
						xtype:		'textfield',
						name:			'CS_ACEPTACION',
						id:			'CS_ACEPTACION1',
						emptyText:	'CS Aceptacion'
					},{				
						width:		90,
						xtype:		'textfield',
						name:			'tipo_persona',
						id:			'tipo_persona1',
						emptyText:	'Tipo de persona'
					},{				
						width:		90,
						xtype:		'textfield',
						name:			'ic_pyme',
						id:			'ic_pyme1',
						emptyText:	'ic pyme'
					},{				
						width:		90,
						xtype:		'textfield',
						name:			'ic_epo_mod',
						id:			'ic_epo_mod1',
						emptyText:	'ic pyme'
					},{				
						width:		90,
						xtype:		'textfield',
						name:			'actualiza_sirac',
						id:			'actualiza_sirac1',
						emptyText:	'Actualiza Sirac'
					},{				
						width:		100,
						xtype:		'textfield',
						name:			'IC_DOMICILIO_EPO',
						id:			'ic_domicilio1',
						emptyText:	'IC_DOMICILIO_EPO'
					},{				
						width:		100,
						xtype:		'textfield',
						name:			'IC_CONTACTO',
						id:			'ic_contacto1',
						emptyText:	'IC_CONTACTO'
					},{				
						width:		100,
						xtype:		'textfield',
						name:			'Numero_Exterior',
						id:			'Numero_Exterior1',
						emptyText:	'Numero_Exterior'
					}				
				]					
			}],
			buttons: [						
				{							
					text:				'Actualizar N@E',
					iconCls:			'icoAceptar',
					id:				'BTNACTUALIZARNE',
					hidden:			true,
					handler:			function(boton, evento) {
						Ext.getCmp('actualiza_sirac1').setValue('N');
						//actualizaNafin();	
						validaPermisos('BTNACTUALIZARNE');
						
					}						
				},{							
					text:				'Actualizar N@E Sirac',
					iconCls:			'icoAceptar',
					id:				'BTNACTUALIZARNESIRAC',
					hidden:			true,
					handler:			function(boton, evento) {						
						if( actualizaSirac() ){
							Ext.getCmp('actualiza_sirac1').setValue('S');
							//actualizaNafin();							
							validaPermisos('BTNACTUALIZARNESIRAC');
						}
					}						
				},{							
					text:				'Cancelar',
					iconCls:			'icoCancelar',
					id:				'BTNCANCELARM',
					hidden:			true,
					handler:			function(boton, evento) {
			if(ext_pantalla=='pendSIRAC'){
				document.location.href = "15proveedorPendSIRAC_ext.jsp?idMenu="+idMenu;
			}else{
				//Ext.getCmp('formaModificar').getForm().reset();
				cancelar_regresar('Cancelar');
			}
					}						
				},{							
					text:				'Regresar',
					iconCls:			'icoRegresar',
					id:				'btnRegesarM',
					handler: 		function(boton, evento) {
			if(ext_pantalla=='pendSIRAC'){
				document.location.href = "15proveedorPendSIRAC_ext.jsp?idMenu="+idMenu;
			}else{
				//Ext.getCmp('formaModificar').getForm().reset();
				cancelar_regresar('Regresar');
			}
					}						
				}							
			]								
		});

	/******************************************************************************
	 * C�digo del form Reafiliar
	 ******************************************************************************/
	/********** Consulta para obtener los datos de reafiliaci�n. Obtengo los datos y los catalogos **********/
		function consultaReafiliacion(rec){
			gridConsulta.el.mask('Buscando...', 'x-mask-loading');
			//Obtengo los cat�logos
			catalogoGrupoEpo.load();
			catalogoEpoReafiliacion.load({
				params: Ext.apply({
					ic_pyme: rec.get('CLIENTE')
				})
			});
			// Indico que tipo de reafiliacion es: EPO / GRUPO
			reafiliarPor = 'EPO';
			// Guardo el ic_pyme para futuras consultas
			Ext.getCmp('no_pyme_reafiliar1').setValue(rec.get('CLIENTE'));
			Ext.getCmp('ic_epo_reafiliar1').setValue(rec.get('IC_EPO'));
			// Realizo la consulta para obtener los datos
			Ext.Ajax.request({
				url: '15forma05ProveedoresExt.data.jsp',
				params: Ext.apply({
					informacion: 'consultaReafiliacion', 
					ic_pyme: rec.get('CLIENTE'),
					ic_epo: rec.get('IC_EPO')
				}),
				callback: procesarReafiliacion
			});
		}

	/********** Procesos para mostrar el form de reafiliaci�n **********/  
		var procesarReafiliacion = function(opts, success, response){
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
				gridConsulta.el.unmask();
				Ext.getCmp('nombre_pyme_reafiliar1').setValue(Ext.util.JSON.decode(response.responseText).registro.CG_RAZON_SOCIAL);
				Ext.getCmp('rfc_pyme_reafiliar1').setValue(Ext.util.JSON.decode(response.responseText).registro.CG_RFC);
				Ext.getCmp('forma').hide();
				Ext.getCmp('btnReafiliaGrupo').setText('Reafiliar por Grupo');
				Ext.getCmp('btnReafiliaEPO').setText('<i><B>Reafiliar por EPO</i></B>');
				Ext.getCmp('formaReafiliar').setTitle('Reafiliar por EPO');
				Ext.getCmp('nombre_epo_reafiliar1').show();
				Ext.getCmp('grupo_epo1').hide();
				Ext.getCmp('no_proveedor_reafiliar1').setValue('');
				Ext.getCmp('no_proveedor_reafiliar1').hide();
				Ext.getCmp('forma').hide();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('toolbarReafiliar').show();
				Ext.getCmp('formaReafiliar').show();
			}
		}

	/********** Obtiene los datos del grid y los convierte en array **********/
		function getJsonOfStore(store){
			var datar = new Array();
			var jsonDataEncode = '';
			var records = store.getRange();
			for (var i = 0; i < records.length; i++){
				datar.push(records[i].data);
			}
			jsonDataEncode = Ext.util.JSON.encode(datar);
			Ext.Ajax.request({
				url: '15forma05ProveedoresExt.data.jsp',
				params: Ext.apply({
					informacion: 'validaGridReafiliacion',
					datos: jsonDataEncode
				}),
				callback: procesaRespuestaValidacion
			});
		}

	/********** Proceso para llenar el cat�logo Grupo EPO **********/
		var procesaRespuestaValidacion = function(opts, success, response){
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true){

				if(Ext.util.JSON.decode(response.responseText).total == '0'){

					var datar = new Array();
					var jsonDataEncode = '';
					var records = consultaGridReafiliacion.getRange();
					for (var i = 0; i < records.length; i++){
						datar.push(records[i].data);
					}
					jsonDataEncode = Ext.util.JSON.encode(datar);

					consultaAceptaReafiliacion.load({
						params: Ext.apply({
							informacion: 'consultaAceptaReafiliacion',
							datos: jsonDataEncode,
							ic_pyme: Ext.getCmp('no_pyme_reafiliar1').getValue(),
							ic_epo: Ext.getCmp('ic_epo_reafiliar1').getValue()
						})
					});

				} else{
					Ext.Msg.alert('Mensaje...', 'El campo No. proveedor es obligatorio. Por favor capt�relo.');
				}
			}
		}

	/********** Proceso para llenar el cat�logo Grupo EPO **********/
		var procesarCatalogoGrupoEpo = function(store, records, response){
			if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
				var jsonData = Ext.util.JSON.decode(response.responseText);
				if (jsonData != null){
					var epo = Ext.getCmp('grupo_epo1');
					Ext.getCmp('grupo_epo1').setValue('');
					if(epo.getValue()==''){
						var newRecord = new recordType({
							clave:'',
							descripcion:'Seleccione...'
						});
						store.insert(0,newRecord);
						store.commitChanges();
						epo.setValue('');
					}
				}
			}
		};

	/********** Proceso para llenar el cat�logo EPO para reafiliaci�n **********/
		var procesarCatalogoEpoReafiliacion = function(store, records, response){
			if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
				var jsonData = Ext.util.JSON.decode(response.responseText);
				if (jsonData != null){
					var epo = Ext.getCmp('nombre_epo_reafiliar1');
					Ext.getCmp('nombre_epo_reafiliar1').setValue('');
					if(epo.getValue()==''){
						var newRecord = new recordType({
							clave:'',
							descripcion:'Seleccione...'
						});
						store.insert(0,newRecord);
						store.commitChanges();
						epo.setValue('');
					}
				}
			}
		};

	/********** Proceso para generar el Grid de Reafiliacion **********/
		var procesarConsultaGridReafiliacion = function(store, arrRegistros, opts){
			var fr = Ext.getCmp('formaReafiliar');
			fr.el.unmask();
			var gridReafiliacion = Ext.getCmp('gridReafiliacion');
			var el = gridReafiliacion.getGridEl();
			if (arrRegistros != null){
				var jsonData = store.reader.jsonData;
				el.unmask();
				Ext.getCmp('gridReafiliacion').show();
				if(store.getTotalCount() < 1){
					Ext.getCmp('gridReafiliacion').hide();
					if(reafiliarPor == 'EPO'){
						Ext.Msg.alert('Mensaje', 'No se encontr� ning�n registro, por favor intente con otra EPO');
					} else if(reafiliarPor == 'GRUPO'){
						Ext.Msg.alert('Mensaje', 'No hay EPOS para reafiliar, por favor intente con otro grupo');
					}
				}else{
					el.unmask();
				}
			}
		}

	/********** Proceso para generar el Grid de errores en Reafiliacion **********/
		var procesarConsultaAceptaReafiliacion = function(store, arrRegistros, opts){
			var gridAceptaReafiliacion = Ext.getCmp('gridAceptaReafiliacion');
			var el = gridAceptaReafiliacion.getGridEl();
			if (arrRegistros != null){
				var jsonData = store.reader.jsonData;
				Ext.getCmp('formaReafiliar').hide();
				Ext.getCmp('toolbarReafiliar').hide();
				Ext.getCmp('gridReafiliacion').hide();
				if(store.getTotalCount() < 1){
					Ext.MessageBox.alert('Mensaje...', 'La actualizaci�n se llev� a cabo con �xito');
					cancelar_regresar('Reafiliacion_exitosa')
				} else{
					el.unmask();
					Ext.getCmp('gridAceptaReafiliacion').show();
				}
			}
		}

	/********** Se crea el store para el combo GrupoEPO reafiliacion **********/
		var catalogoGrupoEpo = new Ext.data.JsonStore({
			id: 'catalogoGrupoEpo',
			root: 'registros',
			fields: ['clave', 'descripcion', 'loadMsg'],
			url: '15forma05ProveedoresExt.data.jsp',
			baseParams: {
				informacion: 'catalogoGrupoEpo'
			},
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
				load: procesarCatalogoGrupoEpo,
				exception: NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
		});

	/********** Se crea el store para el combo EPO reafiliacion **********/
		var catalogoEpoReafiliacion = new Ext.data.JsonStore({
			id: 'catalogoEpoReafiliacion',
			root: 'registros',
			fields: ['clave', 'descripcion', 'loadMsg'],
			url: '15forma05ProveedoresExt.data.jsp',
			baseParams: {
				informacion: 'catalogoEpoReafiliacion'
			},
			totalProperty : 'total',
			autoLoad: false,
			listeners: {
				load: procesarCatalogoEpoReafiliacion,
				exception: NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
		});

	/********** Se crea el store del grid para agregar reafiliaci�n **********/
		var consultaGridReafiliacion = new Ext.data.JsonStore({
			root: 'registros',
			url: '15forma05ProveedoresExt.data.jsp',
			baseParams: {
				informacion: 'consultaGridReafiliacion'
			},
			fields: [
				{name: 'IC_PYME'},
				{name: 'IC_EPO'},
				{name: 'CADENA_PRODUCTIVA'},
				{name: 'NAFIN_ELECTRONICO'},
				{name: 'RAZON_SOCIAL'},
				{name: 'RFC'},
				{name: 'NO_PROVEEDOR'},
				{name: 'PROV_SIN_NUMERO', type: 'bool'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaGridReafiliacion,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args){
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaGridReafiliacion(null, null, null);
					}
				}
			}
		});

	/********** Se crea el store del grid de errores en reafiliaci�n **********/
		var consultaAceptaReafiliacion = new Ext.data.JsonStore({
			root: 'registros',
			url: '15forma05ProveedoresExt.data.jsp',
			baseParams: {
				informacion: 'consultaAceptaReafiliacion'
			},
			fields: [
				{name: 'IC_PYME'},
				{name: 'IC_EPO'},
				{name: 'ACREDITADA'},
				{name: 'NAFIN_ELECTRONICO'},
				{name: 'CG_RAZON_SOCIAL'},
				{name: 'CG_RFC'},
				{name: 'NO_PROVEEDOR'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaAceptaReafiliacion,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args){
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaAceptaReafiliacion(null, null, null);
					}
				}
			}
		});

	/********** Se crea el grid  para agregar reafiliaci�n **********/
		var gridReafiliacion = new Ext.grid.EditorGridPanel({
			width:       850,
			height:      200,
			id:          'gridReafiliacion',
			title:       'Reafiliaci�n',
			margins:     '20 0 0 0',
			style:       'margin:0 auto;',
			align:       'center',
			hidden:      true,
			displayInfo: true,
			loadMask:    true,
			stripeRows:  true,
			frame:       false,
			border:      true,
			clicksToEdit:1,
			store:       consultaGridReafiliacion,
			columns: [
				{ header: 'Cadena Productiva',     dataIndex: 'CADENA_PRODUCTIVA', align: 'left',   width: 200  },
				{ header: 'No. Nafin Electr�nico', dataIndex: 'NAFIN_ELECTRONICO', align: 'center', width: 130  },
				{ header: 'Nombre o Raz�n Social', dataIndex: 'RAZON_SOCIAL',      align: 'left',   width: 130  },
				{ header: 'RFC',                   dataIndex: 'RFC',               align: 'center', width: 130  },
				{ header: 'No. Proveedor',         dataIndex: 'NO_PROVEEDOR',  	 align: 'center', width: 130, editor: new fc.TextField({ allowBlank: true }) },
				{ header:    'Proveedor sin n�mero',
				dataIndex: 'PROV_SIN_NUMERO',
				align:     'center',
				width:     130,
				xtype:     'checkcolumn',
				tooltip:   'El numero de proveedor no es requerido',
				editor: {
					xtype: 'checkbox',
					cls:   'x-grid-checkheader-editor'
				}
				}
			],
			bbar: {
				items: ['->','-',	{
					xtype: 'button',
					text: 'Aceptar',
					tooltip: 'Aceptar reafiliaci�n',
					iconCls: 'icoAceptar',
					id: 'btnAceptarReafiliacion',
					handler: function(boton, evento){
						getJsonOfStore(consultaGridReafiliacion);
					}
				},	'-',{
					xtype: 'button',
					text: 'Cancelar',
					tooltip: 'Cancelar reafiliaci�n',
					iconCls: 'icoCancelar',
					id: 'btnCancelarGridReafiliacion',
					handler: function(boton, evento){
						cancelar_regresar('Cancelar_reafiliacion');
					}
				}]
			}
		});

	/********** Se crea el grid  para mostrar los errores de  reafiliaci�n **********/
		var gridAceptaReafiliacion = new Ext.grid.EditorGridPanel({
			width:       850,
			id:          'gridAceptaReafiliacion',
			title:       'Los siguientes numeros de proveedor ya existen por lo cual no pueden ser asignados',
			align:       'center',
			margins:     '20 0 0 0',
			style:       'margin:0 auto;',
			hidden:      true,
			displayInfo: true,
			loadMask:    true,
			stripeRows:  true,
			autoHeight:  true,
			frame:       false,
			border:      true,
			store:       consultaAceptaReafiliacion,
			columns: [
				{ header: 'Cadena Productiva',     dataIndex: 'ACREDITADA',        align: 'left',   width: 250, felx: 1  },
				{ header: 'No. Nafin Electr�nico', dataIndex: 'NAFIN_ELECTRONICO', align: 'center', width: 130  },
				{ header: 'Nombre o Raz�n Social', dataIndex: 'CG_RAZON_SOCIAL',   align: 'left',   width: 235, flex: 1  },
				{ header: 'RFC',                   dataIndex: 'CG_RFC',            align: 'center', width: 120  },
				{ header: 'No. Proveedor',         dataIndex: 'NO_PROVEEDOR',  	 align: 'center', width: 130  }
			],
			bbar: {
				items: ['->','-',{
					xtype: 'button',
					text: 'Corregir',
					//tooltip: 'Aceptar reafiliaci�n ',
					iconCls: 'icoRegresar',
					id: 'btnCorregirReafiliacion',
					handler: function(boton, evento){
						Ext.getCmp('formaReafiliar').show();
						Ext.getCmp('toolbarReafiliar').show();
						Ext.getCmp('gridReafiliacion').show(); 
						Ext.getCmp('gridAceptaReafiliacion').hide();
					}
				},'-',{
					xtype: 'button',
					text: 'Cancelar',
					tooltip: 'Cancelar reafiliaci�n',
					iconCls: 'icoCancelar',
					id: 'btnCancelarErrorReafiliacion',
					handler: function(boton, evento){
						cancelar_regresar('Cancelar')
					}
				}]
			}
		});

	/********** MenuToolbar de la forma Reafiliar **********/
		var menuToolbar = new Ext.Toolbar({
			width:         850,
			id:     'toolbarReafiliar',
			style:  'margin:0 auto;',
			hidden: true,
			defaults: {
				msgTarget: 'side',
				anchor:    '-20'
			},
			items: [{
				width:     423,
				xtype:   'tbbutton',
				id:      'btnReafiliaEPO',
				text:    'Reafiliar por EPO',
				handler: function(){
					reafiliarPor = 'EPO';
					Ext.getCmp('btnReafiliaGrupo').setText('Reafiliar por Grupo');
					Ext.getCmp('btnReafiliaEPO').setText('<i><B>Reafiliar por EPO</i></B>');
					Ext.getCmp('formaReafiliar').setTitle('Reafiliar por EPO');
					Ext.getCmp('filtro_epo1').show();
					Ext.getCmp('nombre_epo_reafiliar1').show();
					Ext.getCmp('grupo_epo1').hide();
					Ext.getCmp('no_proveedor_reafiliar1').hide();
					Ext.getCmp('grupo_epo1').setValue('');
					Ext.getCmp('no_proveedor_reafiliar1').setValue('');
				}
			},'-',{
				width:     423,
				xtype:   'tbbutton',
				text:    'Reafiliar por Grupo',
				id:      'btnReafiliaGrupo',
				handler: function(){
					reafiliarPor = 'GRUPO';
					Ext.getCmp('btnReafiliaEPO').setText('Reafiliar por EPO');
					Ext.getCmp('btnReafiliaGrupo').setText('<i><B>Reafiliar por Grupo</i></B>');
					Ext.getCmp('formaReafiliar').setTitle('Reafiliar por Grupo');
					Ext.getCmp('grupo_epo1').show();
					Ext.getCmp('no_proveedor_reafiliar1').show();
					Ext.getCmp('filtro_epo1').hide();
					Ext.getCmp('nombre_epo_reafiliar1').hide();
					Ext.getCmp('filtro_epo1').setValue('');
					Ext.getCmp('nombre_epo_reafiliar1').setValue('');
				}
			}]
		});

	/********** Elementos de la forma Reafiliar **********/
		var elementosFormaReafiliar = [{
			width:           600,
				xtype: 'textfield',
				id: 'ic_epo_reafiliar1',
				name: 'ic_epo_reafiliar',
				fieldLabel: '&nbsp;&nbsp; IC_EPO',
				hidden: true
			},{
			width:           600,
				xtype: 'textfield',
				id: 'no_pyme_reafiliar1',
				name: 'no_pyme_reafiliar',
				fieldLabel: '&nbsp;&nbsp; No. PyME',
				hidden: true
			},{
			width:           600,
				xtype: 'textfield',
				id: 'nombre_pyme_reafiliar1',
				name: 'nombre_pyme_reafiliar',
				fieldLabel: '&nbsp;&nbsp; Nombre de la PyME',
				msgTarget: 'side',
				disabled: true
			},{
			width:           600,
				xtype: 'textfield',
				id: 'rfc_pyme_reafiliar1',
				name: 'rfc_pyme_reafiliar',
				fieldLabel: '&nbsp;&nbsp; RFC',
				msgTarget: 'side',
				disabled: true
			},{
			width:           600,
			xtype:           'textfield',
			id:              'filtro_epo1',
			name:            'filtro_epo',
				fieldLabel: '&nbsp; Nombre de la EPO',
			emptyText:       'Capture un criterio de b�squeda y presione ENTER',
			enableKeyEvents:  true,
			listeners:       {
				specialkey:   function(f,e){
					if (e.getKey() == e.ENTER) {
						catalogoEpoReafiliacion.load({
							params:        Ext.apply({
								ic_pyme:    Ext.getCmp('no_pyme_reafiliar1').getValue(),
								filtro_epo: Ext.getCmp('filtro_epo1').getValue()
							})
						});	  
					}
				}
			}
		},{
			xtype:           'itemselector',
				name: 'nombre_epo_reafiliar',
				id: 'nombre_epo_reafiliar1',
			imagePath:       '/nafin/00utils/extjs/ux/images/',
			drawUpIcon:      false, 
			drawDownIcon:    false, 
			drawTopIcon:     false, 
			drawBotIcon:     false,
			border:          false,
			allowBlank:      false,
			multiselects:    [{
				width:        335,
				height:       150,
				legend:       '',
				displayField: 'descripcion',
				valueField:   'clave',
				store:        catalogoEpoReafiliacion
			},{
				width:        335,
				height:       150,
				legend:       'EPOs a reafiliar',
				displayField: 'descripcion',
				valueField: 'clave',
				msgTarget:    'side',
				store:        new Ext.data.JsonStore({
					id:        'storeTempData',
					root:      'registros',
					fields:    ['clave', 'descripcion', 'loadMsg'],
					url:       '15forma05ProveedoresExt.data.jsp',
					baseParams:{ informacion: 'CatalogoVirtual'},
					totalProperty:	'total',
					autoLoad:  false,
					listeners: {
						exception:  NE.util.mostrarDataProxyError,
						beforeload: NE.util.initMensajeCargaCombo
					}
				})
			}]
			},{
			width:           600,
				xtype: 'combo',
				name: 'grupo_epo',
				id: 'grupo_epo1',
				fieldLabel: '&nbsp;&nbsp;Grupo EPO',
				mode: 'local',
				triggerAction: 'all',
				msgTarget: 'side',
				displayField: 'descripcion',
				valueField: 'clave',
				typeAhead: true,
				editable: false,
				hidden: true,
				resizable: true,
				store: catalogoGrupoEpo
			},{
			width:           600,
				xtype: 'textfield',
				id: 'no_proveedor_reafiliar1',
				name: 'no_proveedor_reafiliar',
				fieldLabel: '&nbsp;&nbsp; No. Proveedor',
				msgTarget: 'side',
				hidden: true
		}];

	/********** Form Panel Reafiliar **********/
		var fr = new Ext.form.FormPanel({
			width:         850,
			labelWidth:    120,
			id: 'formaReafiliar',
			title: 'Reafiliar por EPO',
			layout: 'form',
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			frame:         true,
			autoHeight:    true,
			hidden:        true,
			monitorValid: true,
			items: [
				elementosFormaReafiliar
			],
			buttons: [{
					text: 'Agregar',
					iconCls: 'icoAceptar',
					id: 'btnAgregarReafiliacion',
					handler: function(boton, evento){
						if(reafiliarPor == 'EPO'){
							if(Ext.getCmp('nombre_epo_reafiliar1').getValue() != ''){
								consultaGridReafiliacion.load({
									params: Ext.apply({
										reafiliar_por: reafiliarPor,
										ic_pyme: Ext.getCmp('no_pyme_reafiliar1').getValue(),
										nombre_epo_reafiliar: Ext.getCmp('nombre_epo_reafiliar1').getValue(),
										grupo_epo: Ext.getCmp('grupo_epo1').getValue()
									})
								});
							} else {
							//Ext.getCmp('nombre_epo_reafiliar1').markInvalid('El campo es obligatorio');
							Ext.Msg.alert('Mensaje', 'Debe seleccionar al menos una EPO');
							return;
							}
						} else if(reafiliarPor == 'GRUPO'){
							if(Ext.getCmp('grupo_epo1').getValue() != '' ){
								consultaGridReafiliacion.load({
									params: Ext.apply({
										reafiliar_por: reafiliarPor,
										ic_pyme: Ext.getCmp('no_pyme_reafiliar1').getValue(),
										grupo_epo: Ext.getCmp('grupo_epo1').getValue(),
										numero_proveedor: Ext.getCmp('no_proveedor_reafiliar1').getValue()
									})
								});
							} else if(Ext.getCmp('grupo_epo1').getValue() == '' ){
								Ext.getCmp('grupo_epo1').markInvalid('El campo es obligatorio');
							}
						}
					}
				},{
					text: 'Cancelar',
					tooltip: 'Cancelar la reafiliaci�n',
					iconCls: 'icoCancelar',
					id: 'btnCancelarReafiliacion',
					handler: function(boton, evento){
						cancelar_regresar('Cancelar');
					}
			}]
		});

	/******************************************************************************
	 * C�digo del form Cuentas de usuario por afiliado
	 ******************************************************************************/
	/********** Procesos para ver las cuentas de usuario por afiliado **********/
		function verCuentasUsuario(rec){
			Ext.getCmp('ic_pyme_hidden1').setValue(rec.get('CLIENTE'));
			fp.el.mask('Enviando...', 'x-mask-loading');
			consultaVerCtasUsuario.load({
				params: Ext.apply({
					informacion: 'verCuentasPorAfiliado',
					ic_pyme: rec.get('CLIENTE'),
					tipo_afiliado: 'P'
				})
			});
		}

	/********** Modifica el perfil **********/
		function modificaPerfil(){
			Ext.getCmp('btnCambioPerfil').setIconClass('loading-indicator');
			Ext.Ajax.request({
				url: '15forma05ProveedoresExt.data.jsp',
				params: Ext.apply({
					informacion:          'ModificaPerfil',
					pfl_afiliados_id:     Ext.getCmp('pfl_afiliados_id').getValue(),
					pfl_clave_usr_id:     Ext.getCmp('pfl_clave_usr_id').getValue(),
					pfl_modificar_id:     Ext.getCmp('pfl_modificar_id').getValue(),
					pfl_actual_id:        Ext.getCmp('pfl_actual_id').getValue(),
					pfl_nafin_elec_id:    Ext.getCmp('pfl_nafin_elec_id').getValue(),
					pfl_tipo_afiliado_id: Ext.getCmp('pfl_tipo_afiliado_id').getValue(),
					pfl_i_no_elec_id:     Ext.getCmp('pfl_i_no_elec_id').getValue(),
					pfl_internacional_id: Ext.getCmp('pfl_internacional_id').getValue()
				}),
				callback: resultadoCambioPerfil
			});
		}

	/********** Muestra el resultado de la acci�n modificar_perfil  **********/
		function resultadoCambioPerfil( opts, success, response ){
			if( Ext.util.JSON.decode(response.responseText).success == true ){
				Ext.getCmp('btnCambioPerfil').setIconClass('modificar');
				Ext.Msg.alert('Mensaje...', 'El cambio de perfil se realiz� exitosamente.');
				Ext.getCmp('winCambioPerfil').hide();
			} else{
				Ext.getCmp('btnCambioPerfil').setIconClass('modificar');
				Ext.Msg.alert('Error...', Ext.util.JSON.decode(response.responseText).mensaje);
			}
		}

	/********** Proceso para generar el Grid de cuentas de usuario por afiliado **********/
		var procesarVerCtasUsuario = function(store, arrRegistros, opts){
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			var gridVerCtasUsuario = Ext.getCmp('gridVerCtasUsuario');
			var el = gridVerCtasUsuario.getGridEl();
			if (arrRegistros != null){
				var jsonData = store.reader.jsonData;
				el.unmask();
				Ext.getCmp('forma').hide();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridVerCtasUsuario').setTitle(jsonData.datos_afiliado);
				Ext.getCmp('formaCtasUsuario').show();
				gridVerCtasUsuario.show();
					
				if(store.getTotalCount() < 1){
					Ext.getCmp('btnImprimirCtas').disable();
				} else{
					Ext.getCmp('btnImprimirCtas').enable();
				}
			}
		}

	/********** Realiza la consulta de cuentas de usuario por afiliado **********/
		function mostrarDatosCuentasUsuario(rec, accion){
			var cuenta = rec.get('CUENTA');
			fv.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15forma05ProveedoresExt.data.jsp',
				params: Ext.apply({
					informacion:   'DatosCambioPerfil',
					cuenta:        rec.get('CUENTA'),
					busca_edita:   accion,
					tipo_afiliado: 'P'
				}),
				callback: procesoMostrarDatosCuentasUsuario
			});
			ventanaCambioPerfil();
		}

	/********** Procesos para ver las cuentas de usuario por afiliado **********/
		function procesoMostrarDatosCuentasUsuario( opts, success, response){
			var fv = Ext.getCmp('formaCtasUsuario');
			fv.el.unmask();
			if( Ext.util.JSON.decode(response.responseText).success == true ){
				valorFormulario = Ext.util.JSON.decode(response.responseText).registro;
				fcp.getForm().setValues(valorFormulario);
				if(Ext.util.JSON.decode(response.responseText).accion == 'EDITAR'){
					catalogoPerfilModificar.load({
						params: Ext.apply({
							informacion:  'catalogoPerfilModificar',
							strTipoPerfil: Ext.util.JSON.decode(response.responseText).strTipoPerfil
						})
					});
					Ext.getCmp('formaCambioPerfil').setTitle('Cambio de Perfil');
					Ext.getCmp('pfl_modificar_id').show();
					Ext.getCmp('btnCambioPerfil').show();
					Ext.getCmp('pfl_modificar_id').setValue(Ext.getCmp('pfl_actual_id').getValue());

				} else if(Ext.util.JSON.decode(response.responseText).accion == 'BUSCAR'){
					Ext.getCmp('formaCambioPerfil').setTitle('Datos del usuario');
					Ext.getCmp('pfl_modificar_id').hide();
					Ext.getCmp('btnCambioPerfil').hide();
				}

			} else{
				Ext.Msg.alert('Mensaje', Ext.util.JSON.decode(response.responseText).mensaje );
			}
		}

	/********** Se crea la ventana para editar o ver los datos del usuario por afiliaci�n **********/
		var ventanaCambioPerfil = function(){
			var winCambioPerfil = Ext.getCmp('winCambioPerfil');
			if(winCambioPerfil){
				winCambioPerfil.show();
			} else{
				new Ext.Window({
					width:       600,
					id:          'winCambioPerfil',
					layout:      'fit',
					closeAction: 'hide',
					modal:       true,
					resizable:   false,
					closable:    true,
					autoHeight:  true,
					items:       fcp
				}).show().setTitle('');
			}
		}

	/********** Se crea el store para el combo Perfil a Modificar **********/
		var catalogoPerfilModificar = new Ext.data.JsonStore({
			id:         'catalogoPerfilModificar',
			root:       'registros',
			url:        '15forma05ProveedoresExt.data.jsp',
			fields:     ['clave', 'descripcion', 'loadMsg'],
			baseParams: {
				informacion: 'catalogoPerfilModificar'
			},
			listeners:   {
				load: function(){
					if(Ext.getCmp('pfl_modificar_id').getValue()!=''){
						Ext.getCmp('pfl_modificar_id').setValue(Ext.getCmp('pfl_actual_id').getValue());
					}
				},
				exception:  NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
		});

	/********** Se crea el store del grid para ver ctas de usuarios por afiliado **********/
		var consultaVerCtasUsuario = new Ext.data.JsonStore({
			root: 'registros',
			url: '15forma05ProveedoresExt.data.jsp',
			baseParams: {
				informacion: 'verCuentasPorAfiliado'
			},
			fields: [
			{name: 'CUENTA'},
			{name: 'CAMBIO_PERFIL'}
			],
			
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarVerCtasUsuario,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args){
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarVerCtasUsuario(null, null, null);
					}
				}
			}
		});

	/********** Se crea el grid  para ver ctas de usuarios por afiliado **********/
		var gridVerCtasUsuario = new Ext.grid.EditorGridPanel({
			width:         410,
			id:            'gridVerCtasUsuario',
			title:         '&nbsp;',
			align:         'center',
			margins:       '20 0 0 0',
			style:         'margin:0 auto;',
			frame:         true,
			border:        false,
			hidden:        false,
			loadMask:      true,
			stripeRows:    true,
			autoHeight:    true,
			displayInfo:   true,
			store:         consultaVerCtasUsuario,
			columns: [{
				width:      199,
				header:     'Login del Usuario',
				dataIndex:  'CUENTA',
				align:      'center',
				sortable:   false,
				resizable:  false
			},{
				width:      99,
				xtype:      'actioncolumn',
				header:     'Consultar',
				align:      'center',
				sortable:   false,
				resizable:  false,
				items: [{
					iconCls: 'icoBuscar',
					handler: function(gridVerCtasUsuario, rowIndex, colIndex) {
						var rec = consultaVerCtasUsuario.getAt(rowIndex); 
						mostrarDatosCuentasUsuario(rec, 'BUSCAR');
					}
				}]
			},{
				width:      95,
				xtype:      'actioncolumn',
				header:     'Cambio de perfil',
				align:      'center',
				dataIndex: 'CAMBIO_PERFIL',
				sortable:   false,			
				resizable:  false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							for(i=0;i<permisosGlobales.length;i++){ 
								boton=  permisosGlobales[i];										
								if(boton=="CAMBIO_PERFIL") {
									this.items[0].tooltip = 'Modificar';
									return 'modificar';							
								}
							}
						},
						handler: function(gridVerCtasUsuario, rowIndex, colIndex){
							var rec = consultaVerCtasUsuario.getAt(rowIndex);
							mostrarDatosCuentasUsuario(rec, 'EDITAR');
						}
					}
				]
			}]
		});

	/********** Form Panel Cambio de perfil del usuario por afiliado **********/  
		var fcp = new Ext.form.FormPanel({
			width:        500,
			labelWidth:   110,
			id:           'formaCambioPerfil',
			title:        'Cambio de perfil',
			layout:       'form',
			style:        'margin:0 auto;',
			frame:        true,
			autoHeight:   true,
			hidden:       false,
			monitorValid: false,
			items: [{
				width:          300,
				xtype:          'displayfield',
				name:           'AFILIADOS',
				id:             'pfl_afiliados_id',
				fieldLabel:     '&nbsp; Afiliados',
				value:          '&nbsp;',
				hidden:         true
			},{
				width:          300,
				xtype:          'displayfield',
				name:           'INTERNACIONAL',
				id:             'pfl_internacional_id',
				fieldLabel:     '&nbsp; Internacional',
				value:          '&nbsp;',
				hidden:         true
			},{
				width:          300,
				xtype:          'displayfield',
				name:           'S_TIPO_AFILIADO',
				id:             'pfl_tipo_afiliado_id',
				fieldLabel:     '&nbsp; Empresa H',
				value:          '&nbsp;',
				hidden:         true
			},{
				width:          300,
				xtype:          'displayfield',
				name:           'I_NO_ELECTRONICO',
				id:             'pfl_i_no_elec_id',
				fieldLabel:     '&nbsp; No. electr�nico H',
				value:          '&nbsp;',
				hidden:         true
			},{
				width:          300,
				xtype:          'displayfield',
				name:           'CLAVE_USUARIO',
				id:             'pfl_clave_usr_id',
				fieldLabel:     '&nbsp; Clave de usuario',
				value:          '&nbsp;',
				hidden:         false
			},{
				xtype:          'compositefield',
				msgTarget:      'side',
				fieldLabel:     '&nbsp; Empresa',
				combineErrors:  false,
				items: [{
					width:       300,
					xtype:       'displayfield',
					name:        'S_NOMBRE_EMPRESA',
					id:          'pfl_empresa_id',
					allowBlank:  false,
					value:       ''
				},{
					xtype:       'displayfield',
					id:          'muestraCol',
					value:       'Nafin Electr�nico:',
					hidden:      false
				},{
					width:       200,
					xtype:       'displayfield',
					name:        'IC_NAFIN_ELECTRONICO',
					id:          'pfl_nafin_elec_id',
					anchor:      '70%',
					allowBlank:  false,
					value:       ''
				}]
			},{
				width:          300,
				xtype:          'displayfield',
				name:           'NOMBRE',
				id:             'pfl_nombre_id',
				fieldLabel:     '&nbsp; Nombre',
				value:          '&nbsp;',
				hidden:         false
			},{
				width:          300,
				xtype:          'displayfield',
				name:           'APELLIDO_PATERNO',
				id:             'pfl_paterno_id',
				fieldLabel:     '&nbsp; Apellido Paterno',
				value:          '&nbsp;',
				hidden:         false
			},{
				width:          300,
				xtype:          'displayfield',
				name:           'APELLIDO_MATERNO',
				id:             'pfl_materno_id',
				fieldLabel:     '&nbsp; Apellido Materno',
				value:          '&nbsp;',
				hidden:         false
			},{
				width:          300,
				xtype:          'displayfield',
				name:           'EMAIL',
				id:             'pfl_email_id',
				fieldLabel:     '&nbsp; Email',
				value:          '&nbsp;',
				hidden:         false
			},{
				width:          300,
				xtype:          'displayfield',
				name:           'PERFIL_ACTUAL',
				id:             'pfl_actual_id',
				fieldLabel:     '&nbsp; Perfil actual',
				value:          '&nbsp;',
				hidden:         false
			},{
				width:          300,
				xtype:          'combo',
				fieldLabel:     '&nbsp; Perfil a modificar',
				name:           'PERFIL_MODIFICAR',
				id:             'pfl_modificar_id',
				msgTarget:      'side',
				triggerAction:  'all',
				mode:           'local',
				valueField:     'clave',
				displayField:   'descripcion',
				forceSelection: true,
				hidden:         true,
				store:          catalogoPerfilModificar
			}],
			buttons: [{
				text:     'Modificar',
				iconCls:  'modificar',
				id:       'btnCambioPerfil',
				handler:  modificaPerfil
			},{
				text:    'Salir',
				iconCls: 'icoRegresar',
				id:      'btnCerraCambioPerfil',
				handler: function(){
					Ext.getCmp('winCambioPerfil').hide();
				}
			}]
		});

	/********** Form Panel Ver cuentas de usuario por afiliado **********/
		var fv = new Ext.form.FormPanel({
			width:        450,
			labelWidth:   0,
			id:           'formaCtasUsuario',
			title:        'Cuentas de usuarios por afiliado',
			layout:       'form',
			style:        'margin:0 auto;',
			frame:        true,
			autoHeight:   true,
			hidden:       true,
			monitorValid: false,
			items: [{
				width:  	  385,
				xtype:     'displayfield',
				id:        'ic_pyme_hidden1',
				name:      'ic_pyme_hidden',
				hidden:    true
			},
			gridVerCtasUsuario
			],
			buttons: [{
				text:      'Imprimir',
				id:        'btnImprimirCtas',
				tooltip:   'Imprimir archivo PDF ',
				iconCls:   'icoPdf',
				handler: function(boton, evento){
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
					url: '15forma05ProveedoresExt.data.jsp',
						params: Ext.apply({
							informacion: 'imprimirCuentasPorAfiliado',
							ic_pyme: Ext.getCmp('ic_pyme_hidden1').getValue(),
							tipo_afiliado: 'P'
						}),
						callback: descargaArchivo
					});
				}
			},{
				text:      'Regresar',
				id:        'btnImRegesar',
				tooltip:   'Regresar a la tabla principal',
				iconCls:   'icoRegresar',
				handler: function(boton, evento){
					Ext.getCmp('forma').show();
					Ext.getCmp('gridConsulta').show();
					Ext.getCmp('formaCtasUsuario').hide();
				}
			}]
		});

	/******************************************************************************
	 * C�digo del form principal
	 ******************************************************************************/
	/********** Realiza la consulta del form principal para llenar el grid **********/ 
		function consultaDatosGrid(){
	/*
			if(Ext.getCmp('ic_epo1').getValue() == '' && Ext.getCmp('nafin_electronico1').getValue() == '' && Ext.getCmp('nombre1').getValue() == '' &&
				Ext.getCmp('rfc1').getValue() == '' && Ext.getCmp('numero_proveedor1').getValue() == '' && Ext.getCmp('numero_sirac1').getValue() == ''){
				Ext.Msg.alert('Error...','Debe de seleccionar al menos un Crit�rio de Busqueda.');
				return;
			}
	*/
			if(Ext.getCmp('forma').getForm().isValid()){
				fp.el.mask('Enviando...', 'x-mask-loading');
				consultaData.load({
					params: Ext.apply({
						informacion: 'consultar',
						operacion: 'generar',
						start:0,
						limit:15,
						nafin_electronico: Ext.getCmp('nafin_electronico1').getValue(),
						nombre: Ext.getCmp('nombre1').getValue(),
						estado: Ext.getCmp('estado1').getValue(),
						rfc: Ext.getCmp('rfc1').getValue(),
						banco_fondeo: Ext.getCmp('banco_fondeo1').getValue(),
						ic_epo: Ext.getCmp('ic_epo1').getValue(),
						numero_proveedor: Ext.getCmp('numero_proveedor1').getValue(),
						numero_sirac: Ext.getCmp('numero_sirac1').getValue(),
						modifPropietario: Ext.getCmp('modif_propietario1').getValue(),
						pymeSinProveedor: Ext.getCmp('pyme_sin_proveedor1').getValue(),
						convenioUnico: Ext.getCmp('convenio_unico1').getValue(),
						operaFideicomiso: Ext.getCmp('opera_fideicomiso1').getValue(),
						suceptibleFloating: Ext.getCmp('suceptibleFloating1').getValue(),
						chkFactDistribuidoCons:Ext.getCmp('chkFactDistribuidoCons1').getValue(),
						chkProvExtranjeroCons:Ext.getCmp('chkProvExtranjeroCons1').getValue(),
						chkOperaDescAutoEPOCons:Ext.getCmp('chkOperaDescAutoEPOCons1').getValue()
					})
				});
			}
		}

	/********** Elimina una pyme del grid **********/	
		var eliminaPyme = function(grid, rowIndex, colIndex, item, event) {
			var rec = grid.getStore().getAt(rowIndex);
			
			Ext.Msg.confirm('Mensaje...', '�Esta seguro de querer eliminar el registro seleccionado?', function(botonConf){
				if (botonConf == 'ok' || botonConf == 'yes'){
					gridConsulta.el.mask('Eliminando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '15forma05ProveedoresExt.data.jsp',
						params: Ext.apply({
							informacion: 'eliminarRegistro',
							ic_pyme:     rec.get('CLIENTE'),
							ic_epo:      rec.get('IC_EPO')
						}),
						callback: procesarEliminar
					});
				}
			});
		}

	/********** Proceso posterior a la eliminaci�n de una pyme del grid **********/
		var procesarEliminar = function(store, records, response){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData.success == true){
				Ext.Msg.alert('Mensaje...', ''+ jsonData.msg);
				consultaData.load({
					params: Ext.apply({
						informacion:       'consultar',
						operacion:         'generar',
						nafin_electronico: Ext.getCmp('nafin_electronico1').getValue(),
						nombre:            Ext.getCmp('nombre1').getValue(),
						estado:            Ext.getCmp('estado1').getValue(),
						rfc:               Ext.getCmp('rfc1').getValue(),
						ic_epo:            Ext.getCmp('ic_epo1').getValue(),
						numero_proveedor:  Ext.getCmp('numero_proveedor1').getValue(),
						numero_sirac:      Ext.getCmp('numero_sirac1').getValue(),
						start:             0,
						limit:             15
					})
				});
				gridConsulta.el.unmask();
			} else{
				Ext.Msg.alert('Error...', ''+ jsonData.msg);
			}
		}

	/********** Renderers del grid  de consulta **********/ 
		function convenioUnico(value, metaData, record, rowIndex, colIndex, store){
			if ( record.data.CONVENIO_UNICO == 'S' )
				return 'Si';
			else 
				return 'No';
		}

		function operaFideicomiso(value, metaData, record, rowIndex, colIndex, store){
			if ( record.data.CS_OPERA_FIDEICOMISO == 'S' )
				return 'Si';
			else 
				return 'No';
		}

	/********** Proceso para llenar el cat�logo Estado **********/  
		var procesarCatalogoEstado = function(store, records, response){
			if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
				var jsonData = Ext.util.JSON.decode(response.responseText);
				if (jsonData != null){
					var estado;
					estado = Ext.getCmp('estado1');
					Ext.getCmp('estado1').setValue('');
					if(estado.getValue()==''){
						var newRecord = new recordType({
							clave:'',
							descripcion:'Seleccione...'
						});
						store.insert(0,newRecord);
						store.commitChanges();
						estado.setValue('');
					}
				}
			}
		};

	/********** Proceso para llenar el cat�logo Cadena productiva **********/
		var procesarCatalogoEPO = function(store, records, response){
			if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
				var jsonData = Ext.util.JSON.decode(response.responseText);
				if (jsonData != null){
					var ic_epo = Ext.getCmp('ic_epo1');
					Ext.getCmp('ic_epo1').setValue('');
					if(ic_epo.getValue()==''){
						var newRecord = new recordType({
							clave:'',
							descripcion:'Seleccione...'
						});
						store.insert(0,newRecord);
						store.commitChanges();
						ic_epo.setValue('');
					}
				}
			}
		};

	/********** Proceso para generar el Grid de consuta **********/  
		var procesarConsultaData = function(store, arrRegistros, opts){
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			var gridConsulta = Ext.getCmp('gridConsulta');
			var el = gridConsulta.getGridEl();
			if (arrRegistros != null){
				var jsonData = store.reader.jsonData;
				
				var cm = gridConsulta.getColumnModel();
				
				
				if(store.getTotalCount() > 0){
					gridConsulta.show();
					el.unmask();
					Ext.getCmp('btnImprimir').enable();
					Ext.getCmp('btnGenerarCSV').enable();
					
					for(i=0;i<permisosGlobales.length;i++){ 
						boton=  permisosGlobales[i];										
						if(boton=="ACCI_REAFILIA_P") {	
							gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('ACCI_REAFILIA_P'), false);	
						}
					}
					
					if(strPerfil==='PROMO NAFIN' || strPerfil==='ADMIN OPERC ' ){							
							gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SELECCIONAR'), true);	
							gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('ACCI_REAFILIA_P'), true);	
					}
					
				} else {
					gridConsulta.show();
					Ext.getCmp('btnImprimir').disable();
					Ext.getCmp('btnGenerarCSV').disable();
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		};

	/********** Se crea el store para el combo Estado **********/
		var catalogoEstado = new Ext.data.JsonStore({
			id: 'catalogoEstado',
			root: 'registros',
			fields: ['clave', 'descripcion', 'loadMsg'],
			url: '15forma05ProveedoresExt.data.jsp',
			baseParams: {
				informacion: 'catalogoEstado'
			},
			totalProperty: 'total',
			autoLoad: true,
			listeners: {
				load: procesarCatalogoEstado,
				exception: NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
		});

	/********** Se crea el store para el combo Cadena Productiva **********/
		var catalogoEPO = new Ext.data.JsonStore({
			id: 'catalogoEPO',
			root: 'registros',
			fields: ['clave', 'descripcion', 'loadMsg'],
			url: '15forma05ProveedoresExt.data.jsp',
			baseParams: {
				informacion: 'catalogoEPO',
				bancoFondeo: '2'
			},
			totalProperty: 'total',
			autoLoad: true,
			listeners: {
				load: procesarCatalogoEPO,
				exception: NE.util.mostrarDataProxyError,
				beforeload: NE.util.initMensajeCargaCombo
			}
		});

	/********** Se crea el store del grid **********/
		var consultaData = new Ext.data.JsonStore({
			root : 'registros',
			url : '15forma05ProveedoresExt.data.jsp',
			baseParams: {
				informacion: 'consultar'
			},
			fields: [
				{ name: 'CLIENTE'             },
				{ name: 'CADENA_PRODUCTIVA'   },
				{ name: 'NUMERO_PROVEEDOR'    },
				{ name: 'NAFIN_ELECTRONICO'   },
				{ name: 'NUMERO_SIRAC'        },
				{ name: 'RAZON_SOCIAL'        },
				{ name: 'RFC'                 },
				{ name: 'DIRECCION'           },
				{ name: 'ESTADO'              },
				{ name: 'TELEFONO'            },
				{ name: 'VERSION_CONVENIO'    },
				{ name: 'FECHA_CONVENIO'      },
				{ name: 'CONVENIO_UNICO'      },
				{ name: 'CS_OPERA_FIDEICOMISO'},
				{ name: 'TIPO_PERSONA'        },
				{ name: 'IC_EPO'              },
				{ name: 'ACCI_REAFILIA_P'     },
				{ name: 'TASA_FLOATING'       },
				{ name: 'FECHA_PARAM_FLOATING' },
				{ name: 'SELECCIONAR' },
				{ name: 'PROVEEDOR_EXTRANJERO' },
				{ name: 'DESC_AUTO_EPO' }                        
							
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				beforeLoad: {fn: function(store, options){
					Ext.apply(options.params,{
						nafin_electronico: Ext.getCmp('nafin_electronico1').getValue(),
						nombre: Ext.getCmp('nombre1').getValue(),
						estado: Ext.getCmp('estado1').getValue(),
						rfc: Ext.getCmp('rfc1').getValue(),
						banco_fondeo: Ext.getCmp('banco_fondeo1').getValue(),
						ic_epo: Ext.getCmp('ic_epo1').getValue(),
						numero_proveedor: Ext.getCmp('numero_proveedor1').getValue(),
						numero_sirac: Ext.getCmp('numero_sirac1').getValue(),
						modif_propietario: Ext.getCmp('modif_propietario1').getValue(),
						pyme_sin_proveedor: Ext.getCmp('pyme_sin_proveedor1').getValue(),
						convenio_unico: Ext.getCmp('convenio_unico1').getValue()
					});
				}},
				load: procesarConsultaData,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args){
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);
					}
				}
			}
		});

	/********** Se crea el grid **********/
		var gridConsulta = new Ext.grid.EditorGridPanel({
			height:            450,
			width:             900,
			id:                'gridConsulta',
			title: 'Consulta- Proveedores',	
			align:             'center',
			margins:           '20 0 0 0',
			style:             'margin:0 auto;',
			emptyMsg:          'No hay registros.',
			hidden:            true,
			displayInfo:       true,
			loadMask:          true,		
			frame:             false,
			clicksToEdit:      1,
			store:             consultaData,
			columns: [
				/*{
					header:    'IC_PYME',
					dataIndex: 'CLIENTE'
				},{
					header:    'IC_EPO',
					dataIndex: 'IC_EPO'
				},{
					header:    'TIPO_PERSONA',
					dataIndex: 'TIPO_PERSONA'
				},*/{
					header:    'Cadena productiva',
					dataIndex: 'CADENA_PRODUCTIVA',
					sortable:  true,
					resizable: true,
					align:     'left',
					width:     200
				},{
					header:    'No. proveedor',
					tooltip:   'N�mero de proveedor',
					dataIndex: 'NUMERO_PROVEEDOR',
					sortable:  true,
					resizable: true,
					align:     'center',
					width:     100
				},{
					header:    'No. Nafin Electr�nico',
					tooltip:   'N�mero de Nafin Electr�nico',
					dataIndex: 'NAFIN_ELECTRONICO',
					sortable:  true,
					resizable: true,
					align:     'center',
					width:     100
				},{
					header:    'No. SIRAC',
					tooltip:   'N�mero de SIRAC',
					dataIndex: 'NUMERO_SIRAC',
					sortable:  true,
					resizable: true,
					align:     'center',
					width:     80
				},{
					header:    'Nombre o raz�n social',
					dataIndex: 'RAZON_SOCIAL',
					sortable:  true,
					resizable: true,
					align:     'left',
					width:     200
				},{
					header:    'RFC',
					dataIndex: 'RFC',
					sortable:  true,
					resizable: true,
					align:     'left',
					width:     120
				},{
					header:    'Domicilio',
					dataIndex: 'DIRECCION',
					sortable:  true,
					resizable: true,
					align:     'left',
					width:     200
				},{
					header:    'Estado',
					dataIndex: 'ESTADO',
					sortable:  true,
					resizable: true,
					align:     'left',
					width:     100
				},{
					header:    'Tel�fono',
					dataIndex: 'TELEFONO',
					sortable:  true,
					resizable: true,
					align:     'center',
					width:     110
				},{
					header:    'Versi�n convenio',
					dataIndex: 'VERSION_CONVENIO',
					sortable:  true,
					resizable: true,
					align:     'left',
					width:     190
				},{
					header:    'Fecha consulta convenio',
					tooltip:   'Fecha de consulta de convenio',
					dataIndex: 'FECHA_CONVENIO',
					sortable:  true,
					resizable: true,
					align:     'center',
					width:     100
				},{
					header:    'Convenio �nico',
					dataIndex: 'CONVENIO_UNICO',
					sortable:  true,
					resizable: true,
					align:     'center',
					renderer:  convenioUnico,
					width:     100
				},{
					header:    'Opera Fideicomiso para Desarrollo de Proveedores',
					dataIndex: 'CS_OPERA_FIDEICOMISO',
					sortable:  true,
					resizable: true,
					align:     'center',
					renderer:  operaFideicomiso,
					width:     100
				},
				{
					header:    'Proveedores <br> susceptibles a Floating ',
					dataIndex: 'TASA_FLOATING',
					sortable:  true,
					resizable: true,
					align:     'center',				
					width:     150
				},		
				{
					header:    'Fecha Parametrizaci�n <br>Floating',
					dataIndex: 'FECHA_PARAM_FLOATING',
					sortable:  true,
					resizable: true,
					align:     'center',				
					width:     150,
					renderer:function(value,metadata,registro){
						if(registro.data['TASA_FLOATING']==='Si') {
							return  value;
						}
					}
				},
							{
					header:    'Proveedor <br>Extranjero ',
					dataIndex: 'PROVEEDOR_EXTRANJERO',
					sortable:  true,
					resizable: true,
					align:     'center',				
					width:     100
				},
							{
					header:    'Descuento <br>Autom�tico EPO',
					dataIndex: 'DESC_AUTO_EPO',
					sortable:  true,
					resizable: true,
					align:     'center',				
					width:     100
				},                        
				{
					xtype		: 'actioncolumn',
					dataIndex: 'SELECCIONAR',
					header: 'Seleccionar',
					tooltip: 'Seleccionar ',				
					sortable: true,
					width: 150,			
					resizable: true,				
					align: 'center',
					items		: [	
						{
							getClass: function(value,metadata,record,rowIndex,colIndex,store){		
								this.items[0].tooltip = 'Modificar';								
								return 'modificar';
							}
							,handler: consultaModificarPyme
						},
						{
							getClass: function(value,metadata,record,rowIndex,colIndex,store){		
								for(i=0;i<permisosGlobales.length;i++){ 
									boton=  permisosGlobales[i];										
									if(boton=="ACCI_ELI_P") {	
										this.items[1].tooltip = 'Eliminar';
										return 'borrar';
									}
								}
							}
							,handler: eliminaPyme
						}
					]
				}, 
				
				{
					xtype:     'actioncolumn',
					header:    'Reafiliaci�n',
					dataIndex: 'ACCI_REAFILIA_P',
					sortable:  false,
					resizable: false,
					align:     'center',
					hidden:   true,
					width:     70,
					items: [{
						iconCls: 'modificar',
						tooltip: 'Reafiliar',
						handler: function(gridConsulta, rowIndex, colIndex){
							var rec = consultaData.getAt(rowIndex); 
							consultaReafiliacion(rec);
						}
					}]
				},{
					xtype:     'actioncolumn',
					header:    'Cuentas de usuario',
					sortable:  false,
					resizable: false,
					align:     'center',
					width:     120,
					items: [{
						iconCls: 'icoBuscar',
						tooltip: 'Ver cuentas de usuario por afiliado',
						handler: function(gridConsulta, rowIndex, colIndex){
							var rec = consultaData.getAt(rowIndex);
							verCuentasUsuario(rec);
						}
					}]
				}
			],
			bbar: {
				xtype:       'paging',
				id:          'barraPaginacion',
				displayMsg:  '{0} - {1} de {2}',
				emptyMsg:    'No hay registros.',
				buttonAlign: 'left',
				displayInfo: true,
				pageSize:    15,
				store:       consultaData,
				items: [
					'->','-',
					{
						xtype:   'button',
						text:    'Generar Archivo',
						id:      'btnGenerarCSV',
						tooltip: 'Generar Archivo csv ',
						iconCls: 'icoXls',
						handler: function(boton, evento){
							boton.setIconClass('loading-indicator');
							var barraPaginacionA = Ext.getCmp("barraPaginacion");
							Ext.Ajax.request({
								url: '15forma05ProveedoresExt.data.jsp',
								params: Ext.apply({
									informacion: 'generarArchivo',
									nafin_electronico: Ext.getCmp('nafin_electronico1').getValue(),
									nombre: Ext.getCmp('nombre1').getValue(),
									estado: Ext.getCmp('estado1').getValue(),
									rfc: Ext.getCmp('rfc1').getValue(),
									banco_fondeo: Ext.getCmp('banco_fondeo1').getValue(),
									ic_epo: Ext.getCmp('ic_epo1').getValue(),
									numero_proveedor: Ext.getCmp('numero_proveedor1').getValue(),
									numero_sirac: Ext.getCmp('numero_sirac1').getValue(),
									modif_propietario: Ext.getCmp('modif_propietario1').getValue(),
									pyme_sin_proveedor: Ext.getCmp('pyme_sin_proveedor1').getValue(),
									convenio_unico: Ext.getCmp('convenio_unico1').getValue(),
									suceptibleFloating: Ext.getCmp('suceptibleFloating1').getValue(),
									chkFactDistribuidoCons:Ext.getCmp('chkFactDistribuidoCons1').getValue(),
																	chkProvExtranjeroCons:Ext.getCmp('chkProvExtranjeroCons1').getValue(),
																	chkOperaDescAutoEPOCons:Ext.getCmp('chkOperaDescAutoEPOCons1').getValue()
								}),
								callback: descargaArchivo
							});
						}
					},
					'-', 
					{
						xtype:   'button',
						text:    'Imprimir',
						id:      'btnImprimir',
						tooltip: 'Imprimir archivo PDF ',
						iconCls: 'icoPdf',
						handler: function(boton, evento){
							var barraPaginacionA = Ext.getCmp("barraPaginacion");
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url: '15forma05ProveedoresExt.data.jsp',
								params: Ext.apply({
									informacion: 'imprimir',
									start: barraPaginacionA.cursor,
									limit: barraPaginacionA.pageSize,
									nafin_electronico: Ext.getCmp('nafin_electronico1').getValue(),
									nombre: Ext.getCmp('nombre1').getValue(),
									estado: Ext.getCmp('estado1').getValue(),
									rfc: Ext.getCmp('rfc1').getValue(),
									banco_fondeo: Ext.getCmp('banco_fondeo1').getValue(),
									ic_epo: Ext.getCmp('ic_epo1').getValue(),
									numero_proveedor: Ext.getCmp('numero_proveedor1').getValue(),
									numero_sirac: Ext.getCmp('numero_sirac1').getValue(),
									modif_propietario: Ext.getCmp('modif_propietario1').getValue(),
									pyme_sin_proveedor: Ext.getCmp('pyme_sin_proveedor1').getValue(),
									convenio_unico: Ext.getCmp('convenio_unico1').getValue(),
									suceptibleFloating: Ext.getCmp('suceptibleFloating1').getValue(),
									chkFactDistribuidoCons:Ext.getCmp('chkFactDistribuidoCons1').getValue(),
																	chkProvExtranjeroCons:Ext.getCmp('chkProvExtranjeroCons1').getValue(),
																	chkOperaDescAutoEPOCons:Ext.getCmp('chkOperaDescAutoEPOCons1').getValue()
								}),
								callback: descargaArchivo
							});
						}
					}
				]
			}
		});

	/********** Form Panel principal **********/
		var fp = new Ext.form.FormPanel({
			width:             650,
			labelWidth:        150,
			id:                'forma',
			title:             'Proveedores',
			layout:            'form',
			style:             'margin:0 auto;',
			frame:             true,
			autoHeight:        true,
			monitorValid:      true,
			defaults: {
				xtype:          'textfield',
				msgTarget:      'side'
			},
			items: [
			{
				width:          455,
				xtype:          'combo',
				id:             'cmbTipoAfiliado',
				name:           'status',
				hiddenName:     'status',
				fieldLabel:     '&nbsp; Tipo de Afiliado', 
				mode:           'local',
				displayField:   'descripcion',
				valueField:     'clave',
				emptyText:      'Seleccionar...',
				triggerAction:  'all',
				forceSelection: true,
				typeAhead:      true,
				minChars:       1,
				value:          '2',	
				store:          dataStatus,
				listeners:{
					select:{
						fn: function(combo){
							var valor = combo.getValue();
							creditoElec(valor);
						}
					}
				}
			},{
				width:          455,
				id:             'nafin_electronico1',
				name:           'nafin_electronico',
				fieldLabel:     '&nbsp; N�mero Nafin electr�nico',
				regex:          /^[0-9]*$/,
				maxLength:      30
			},{
				width:          455,
				id:             'nombre1',
				name:           'nombre',
				fieldLabel:     '&nbsp; Nombre',
				maxLength:      100
			},{
				width:          455,
				xtype:          'combo',
				id:             'estado1',
				name:           'estado',
				fieldLabel:     '&nbsp; Estado',
				mode:           'local',
				displayField:   'descripcion',
				valueField:     'clave',
				emptyText:      'Seleccione ... ',
				triggerAction:  'all',
				typeAhead:      true,
				forceSelection: true,
				minChars:       1,
				store:          catalogoEstado
			},{
				width:          455,
				id:             'rfc1',
				name:           'rfc',
				fieldLabel:     '&nbsp; RFC',
				vtype:          'validaRFC',
				maxLength:      20
				/*regex:          /^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
				regexText:      'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
								'en el formato NNN-AAMMDD-XXX donde:<br>'+
								'NNN:son las iniciales del nombre de la empresa<br>'+
								'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
								'XXX:es la homoclave'*/
			},{
				width:          455,
				xtype:          'combo',
				id:             'banco_fondeo1',
				name:           'banco_fondeo',
				fieldLabel:     '&nbsp; Banco de Fondeo',
				mode:           'local',
				emptyText:      'Seleccione ... ',
				triggerAction:  'all',
				displayField:   'descripcion',
				valueField:     'clave',
				typeAhead:      true,
				hidden:         true,
				minChars:       1/*,
				store:          catalogoBancoFondeo*/
			},{
				width:          455,
				xtype:          'combo',
				id:             'ic_epo1',
				name:           'ic_epo',
				fieldLabel:     '&nbsp; Cadena productiva',
				mode:           'local',
				emptyText:      'Seleccione ... ',
				triggerAction:  'all',
				displayField:   'descripcion',
				valueField:     'clave',
				typeAhead:      true,
				forceSelection: true,
				minChars:       1,
				store:          catalogoEPO
			},{
				width:          455,
				id:             'numero_proveedor1',
				name:           'numero_proveedor',
				fieldLabel:     '&nbsp; N�mero de Proveedor',
				maxLength:      25
			},{
				width:          455,
				id:             'numero_sirac1',
				name:           'numero_sirac',
				fieldLabel:     '&nbsp; N�mero de SIRAC',
				maxLength:      12
			},{
				width:          450,
				xtype:          'checkbox',
				id:             'modif_propietario1',
				name:           'modif_propietario',
				boxLabel:       'Modificado por propietario',
				hidden:true
			},{
				width:          300,
				xtype:          'checkbox',
				id:             'pyme_sin_proveedor1',
				name:           'pyme_sin_proveedor',
				boxLabel:       'Pymes sin n�mero de proveedor',
				hidden:true
			},{
				width:          300,
				xtype:          'checkbox',
				id:             'convenio_unico1',
				name:           'convenio_unico',
				boxLabel:       'Opera convenio �nico',
				hidden:true
			},{
				width:          300,
				xtype:          'checkbox',
				id:             'opera_fideicomiso1',
				name:           'opera_fideicomiso',
				boxLabel:       'Opera Fideicomiso para Desarrollo de Proveedores',
				hidden:true
			},
			{
				width:          300,
				xtype:          'checkbox',
				id:             'chkFactDistribuidoCons1',
				name:           'chkFactDistribuidoCons',
				boxLabel:       'Opera Factoraje Distribuido'
			},
			{
				width:          300,
				xtype:          'checkbox',
				id:             'suceptibleFloating1',
				name:           'suceptibleFloating',
				boxLabel:       'Proveedores susceptibles a Floating'
			},
					
					{
				width:          300,
				xtype:          'checkbox',
				id:             'chkProvExtranjeroCons1',
				name:           'chkProvExtranjeroCons',
				boxLabel:       'Proveedores Extranjero'
			},
					{
				width:          300,
				xtype:          'checkbox',
				id:             'chkOperaDescAutoEPOCons1',
				name:           'chkOperaDescAutoEPOCons',
				boxLabel:       'Descuento Autom�tico EPO'
			}
			],
			buttons: [{
				text:           'Buscar',
				id:             'btnBuscar',
				iconCls:        'icoBuscar',
				handler:        consultaDatosGrid
			}]
		});

	/********** Contenedor Principal **********/
		var pnl = new Ext.Container({
			width:   949,
			id:      'contenedorPrincipal',
			applyTo: 'areaContenido',
			style:   'margin:0 auto;',
			items: [
				NE.util.getEspaciador(20),
				fp,
				menuToolbar,
				fr,
				NE.util.getEspaciador(10),
				gridConsulta,
				gridReafiliacion,
				gridAceptaReafiliacion,
				fm,
				fv
			]
		});

	/**********  Modificacion Prov SIARC Pend   *******/
		if(ext_pantalla=='pendSIRAC'){
			var ext_ic_pyme = Ext.getDom('hic_pyme').value;
			var ext_txtaction = Ext.getDom('htxtaction').value;
			var ext_txtTipoPersona = Ext.getDom('htxtTipoPersona').value;
			var ext_tipoPYME = Ext.getDom('hTipoPYME').value;
			var ext_ic_epo = Ext.getDom('hic_epo').value;
			
			

			//var ext_pantalla = Ext.getDom('hpantalla').value;
			var ext_url_origen = Ext.getDom('hurl_origen').value;
			var ext_csTroya = Ext.getDom('hcsTroya').value;

			var ic_pyme  = ext_ic_pyme;
			var tipo_per = ext_txtTipoPersona;
			var ic_epo   = ext_ic_epo;
			Ext.getCmp('tipo_persona1').setValue(tipo_per); 
			Ext.getCmp('ic_pyme1').setValue(ic_pyme);
			Ext.getCmp('ic_epo_mod1').setValue(ic_epo);
			pnl.el.mask('Buscando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15forma05ProveedoresExt.data.jsp',
				params: Ext.apply({
					informacion: 'consultaModificar', 
					ic_pyme: ic_pyme,
					ic_epo: ic_epo,
					idMenuP:idMenu
				}),
				callback: datosAModificar
			});
		}
		
		Ext.Ajax.request({
			url: '15forma05ProveedoresExt.data.jsp',
			params: {
				informacion: "valoresIniciales",
				idMenuP:idMenu
			},
			callback: procesaValoresIniciales
		});	
		
	});