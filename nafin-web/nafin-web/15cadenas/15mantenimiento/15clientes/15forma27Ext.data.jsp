<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	JSONObject resultado = new JSONObject();

 if(informacion.equals("catalogoGrupo")) {		

		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("cecat_grupo");
		cat.setCampoClave("ic_grupo");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setOrden("ic_grupo");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("catalogoPais")) {		
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionNotIn("52", Integer.class);
		infoRegresar = cat.getJSONElementos();	
	}
 else if(informacion.equals("catalogoEstado")){
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setClavePais(Pais);//Mexico clave 24
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();	
 }
 else if(informacion.equals("catalogoMunicipio"))	{
		
		String Edo= (request.getParameter("estado")==null)?"":request.getParameter("estado");
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoMunicipio cat=new CatalogoMunicipio();
		cat.setClave("ic_municipio");
		cat.setDescripcion("cd_nombre");
		cat.setPais(Pais);
		cat.setEstado(Edo);
		cat.setOrden("cd_nombre");
		List elementos=cat.getListaElementos();
		
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
 }
 else if(informacion.equals("catalogoIdentificacion")) {
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_identificacion");
		cat.setCampoClave("ic_identificacion");
		cat.setCampoDescripcion("cd_nombre");
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
 }
 

 else if(informacion.equals("AltaUniversidad"))	{
	try	
		{

		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		

		String   Grupo    					= (request.getParameter("grupo") == null) ? "" : request.getParameter("grupo"),
					Universidad  				= (request.getParameter("universidad") == null) ? "" : request.getParameter("universidad"),
					R_F_C         				= (request.getParameter("rfc")    == null) ? "" : request.getParameter("rfc"),
					Campus					   = (request.getParameter("campus") == null) ? "" : request.getParameter("campus");
					
					//Domicilio							
		String   Calle        				= (request.getParameter("calle")    == null) ? "" : request.getParameter("calle"),
					Colonia       				= (request.getParameter("colonia")  == null) ? "" : request.getParameter("colonia"),
					Codigo_postal 				= (request.getParameter("code") == null) ? "" : request.getParameter("code"),
					Pais          				= (request.getParameter("cmb_pais")     == null) ? "" : request.getParameter("cmb_pais"),
					Estado        				= (request.getParameter("cmb_estado")   == null) ? "" : request.getParameter("cmb_estado"),
					Delegacion_o_municipio 	= (request.getParameter("cmb_delegacion") == null) ? "" : request.getParameter("cmb_delegacion"),
					Telefono      				= (request.getParameter("telefono") == null) ? "" : request.getParameter("telefono"),
					Email         				= (request.getParameter("email")    == null) ? "" : request.getParameter("email"),
					Fax           				= (request.getParameter("fax")      == null) ? "" : request.getParameter("fax");
					
					//Datos del Representate Legal
		String 	Apellido_paterno_L       = (request.getParameter("paterno")       == null) ? "" : request.getParameter("paterno"),
					Apellido_materno_L       = (request.getParameter("materno")       == null) ? "" : request.getParameter("materno"),
					Nombre_L                 = (request.getParameter("nombre")        == null) ? "" : request.getParameter("nombre"),
					Identificacion				 = (request.getParameter("cmb_identificacion") == null) ? "" : request.getParameter("cmb_identificacion"),
					No_Identificacion			 = (request.getParameter("num_iden") == null) ? "" : request.getParameter("num_iden"),
					Numero_de_escritura      = (request.getParameter("poderes")      == null) ? "" : request.getParameter("poderes"),
					Fecha_del_poder_notarial = (request.getParameter("fch_poder") == null) ? "" : request.getParameter("fch_poder");
					
					
		if( !Delegacion_o_municipio.equals("") && Delegacion_o_municipio != null)
		{
			CatalogoMunicipio cat=new CatalogoMunicipio();
			cat.setClave("ic_municipio");			cat.setDescripcion("cd_nombre");			cat.setPais(Pais);			cat.setEstado(Estado);			cat.setSeleccion(Delegacion_o_municipio);			cat.setOrden("cd_nombre");
			List elementos=cat.getListaElementos();
		
			Iterator it = elementos.iterator();
			while(it.hasNext()) {
				ElementoCatalogo obj = (ElementoCatalogo)it.next();
				String iDelMun = obj.getClave();		String strDelMun = obj.getDescripcion();
				Delegacion_o_municipio = iDelMun + "|" + strDelMun;
			}
			System.out.println("Delegacion_o_municipio " +  Delegacion_o_municipio ) ;
		}
		
		String sDatosUniver = "Universidad-" + Universidad+"|"+"R_F_C-" + R_F_C+"|"+"Campus-" + Campus+"|"+		
							"Calle-" + Calle+"|"+ "Colonia-"+Colonia +"|" +"Codigo_postal-" + Codigo_postal+"|"+"Pais-" + Pais+"|"+"Estado-" + Estado+"|"+ 
							"Delegacion_o_municipio-" + Delegacion_o_municipio+"|"+ "Telefono-" + Telefono+"|"+"Email-" + Email+"|"+ "Fax-" + Fax+"|"+
							"Apellido_paterno_L-" + Apellido_paterno_L+"|"+"Apellido_materno_L-" + Apellido_materno_L+"|"+"Nombre_L-" + Nombre_L+"|"+
							"Identificacion-" + Identificacion+"|"+"No_Identificacion-" + No_Identificacion+"|"+"Numero_de_escritura-" + Numero_de_escritura+"|"+
							"Fecha_del_poder_notarial-" + Fecha_del_poder_notarial+"\n";
			System.out.println(sDatosUniver);					
		   
		
		String lsNoNafinElectronico = BeanAfiliacion.afiliaUniversidad(iNoUsuario, Grupo,Universidad.toUpperCase(), R_F_C.toUpperCase(), Campus.toUpperCase(),
							Calle.toUpperCase(), Colonia.toUpperCase(), Codigo_postal, Pais, Estado, Delegacion_o_municipio, Telefono, Email.toLowerCase(),Fax,
							Apellido_paterno_L, Apellido_materno_L, Nombre_L,  Identificacion, No_Identificacion,  Numero_de_escritura, Fecha_del_poder_notarial);
		
				if( lsNoNafinElectronico != null  &&  !lsNoNafinElectronico.equals("") )
				{
					resultado.put("success", new Boolean(true));
					resultado.put("lsNoNafinElectronico",lsNoNafinElectronico);
					infoRegresar = resultado.toString();	
				}
			}
			catch(Exception e) { 
				throw new AppException("Error en la afiliacion de Universidad", e);
			}
 }

//System.out.println("infoRegresar============"+infoRegresar); 


%>
<%=infoRegresar%>



