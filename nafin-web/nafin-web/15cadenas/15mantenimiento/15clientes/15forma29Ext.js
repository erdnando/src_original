Ext.onReady(function(){ 
	
	var ObjCatalogo = {
		cat1:false,
		cat2:false,
		cat3:false,
		cat4:false
	}
	
	var ic_universidad = window.location.search 
                 ? Ext.urlDecode(window.location.search.substring(1)).ic_universidad : ''; //: ''; cambiar!!!
   
	
	var pais_s, edo, municipio ;
	var primera_vez = true;
	var iEstado = 0;
	var iMunicipio = 0;
/*--------------------------------- Handler's -------------------------------*/

	var procesarSuccessFailureModificaUniversidad = function(opts, success, response) 
   {
		var cmpForma = Ext.getCmp('formaAfiliacion');
		cmpForma.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('',Ext.util.JSON.decode(response.responseText).msg, function() {
				window.location = '15forma28Ext.jsp';
			}, this);	
			
		}
		else {
			Ext.Msg.alert('','Ocurrio un error durante la actualizacion de datos.');	
		}
	}
	
	var procesarSuccessFailureConsultaUniversidad = function(opts, success, response) 
   {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			var reg = Ext.util.JSON.decode(response.responseText).registros[0];
			if (reg != null) 
			{	
				Ext.getCmp('id_grupo').setValue(reg.Grupo);
				Ext.getCmp('universidad').setValue(reg.Razon_Social);
				Ext.getCmp('rfc').setValue(reg.R_F_C);
				Ext.getCmp('campus').setValue(reg.Campus);
				Ext.getCmp('id_cmb_estatus').setValue(reg.Estatus);
				
				Ext.getCmp('calle').setValue(reg.Calle);
				Ext.getCmp('colonia').setValue(reg.Colonia);
				Ext.getCmp('code').setValue(reg.Codigo_postal);
				Ext.getCmp('id_cmb_pais').setValue(reg.Pais);
				Ext.getCmp('id_cmb_estado').setValue(reg.Estado);
				Ext.getCmp('id_cmb_delegacion').setValue(reg.Delegacion_o_municipio);
				Ext.getCmp('telefono').setValue(reg.Telefono);
				Ext.getCmp('email').setValue(reg.Email);
				Ext.getCmp('fax').setValue(reg.Fax);
				
				Ext.getCmp('paterno').setValue(reg.Apellido_paterno_L);
				Ext.getCmp('materno').setValue(reg.Apellido_materno_L);
				Ext.getCmp('nombre').setValue(reg.Nombre_L);
				Ext.getCmp('id_cmb_identificacion').setValue(reg.Identificacion);
				Ext.getCmp('num_iden').setValue(reg.No_Identificacion);
				Ext.getCmp('poderes').setValue(reg.Numero_de_escritura);
				Ext.getCmp('fch_poder').setValue(reg.Fecha_del_poder_notarial);
				
				total = reg.Total;			
				
				/* Guardo la informacion para llenarla en los combos */
				pais_s = reg.Pais;
				edo 	 = reg.Estado;
				municipio = reg.Delegacion_o_municipio;
				
				/* Carga de combos */
				catalogoEstado.load({
								params : Ext.apply({
									pais : pais_s
								})
							});
				catalogoMunicipio.load({
								params : Ext.apply({
									pais : pais_s,
									estado : edo
								})
							});
			}
		}
	}
	

/*------------------------------- End Handler's -----------------------------*/
/*****************************************************************************/
 
	var validaCatalogos = function(){
		if(NE.util.allTrue(ObjCatalogo) && primera_vez == true){
			//ajax
				Ext.Ajax.request({
						url: '15forma29Ext.data.jsp',
						params: Ext.apply({
							informacion: 'ConsultaUniversidad',
							ic_universidad : ic_universidad
							}),				
						callback: procesarSuccessFailureConsultaUniversidad
				});
				primera_vez = false;
		}
	}
	
	var procesarCatalogoGrupo = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat1=true;
			validaCatalogos();
		}
	}
	var procesarCatalogoPais = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat2=true;
			validaCatalogos();
			
		}
	}
	var procesarCatalogoEstado = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			iEstado++;
			if(primera_vez == false && iEstado < 3) {
				Ext.getCmp('id_cmb_estado').setValue(edo);
			}
			else {
				ObjCatalogo.cat3=true;
				validaCatalogos();
			}
		}
	}
	var procesarCatalogoMunicipio = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			iMunicipio++;
			if(primera_vez == false && iMunicipio < 3) {
				Ext.getCmp('id_cmb_delegacion').setValue(municipio);
			}

		}
	}
	
	var procesarCatalogoIdentificacion = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			ObjCatalogo.cat4=true;
			validaCatalogos();
		}
	}
/*---------------------------------- Store's --------------------------------*/
	// catalogo Grupo
	var catalogoGrupo = new Ext.data.JsonStore({
		id				: 'catGrupo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma29Ext.data.jsp',
		baseParams	: { informacion: 'catalogoGrupo'	},
		autoLoad		: false,
		listeners	:
		{
		 load:procesarCatalogoGrupo,
		 exception: NE.util.mostrarDataProxyError
		}
	});		
	
	// catalogo Estatus
	var catalogoEstatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['A','Activo'],
			['I','Inactivo']
		 ]
	}) ;
	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma29Ext.data.jsp',
		baseParams	: { informacion: 'catalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoPais,
			exception: NE.util.mostrarDataProxyError
		}
  });
  
  // catalogo Estado
	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma29Ext.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 load:procesarCatalogoEstado,
		 exception: NE.util.mostrarDataProxyError
		}
	});
	
	//catalogo Municipio
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma29Ext.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 load:procesarCatalogoMunicipio,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		
	//catalogo Identificacion
	var catalogoIdentificacion= new Ext.data.JsonStore
	({
		id				: 'catIdentificacion',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma29Ext.data.jsp',
		baseParams	: { informacion: 'catalogoIdentificacion'	},
		autoLoad		: false,
		listeners	:
		{
		 load:procesarCatalogoIdentificacion,
		 exception: NE.util.mostrarDataProxyError
		}
	});
  

	 /****** Store�s Grid�s *******/	
  var consultaData = new Ext.data.JsonStore
	({		
		root:'registros',	
		url:'15forma08Ext.data.jsp',	
		totalProperty: 'total',		
			fields: [	
					{name: 'nombre_producto'}, 
					{name: 'ic_producto'}, 					
					{name: 'dispersion'},
					{name: 'dispNoNeg'},
					{name: 'producto'}
					],
		messageProperty: 'msg',
		autoLoad: false,
		listeners: { exception: NE.util.mostrarDataProxyError	}		
	});



/*--------------------------------- End Store's -----------------------------*/
/*****************************************************************************/
/*-------------------------------- Componentes ------------------------------*/


	var afiliacionPanel = {
		xtype		: 'panel',
		id 		: 'afiliacionPanel',
		items		: [
			{
				layout		: 'form',
				labelWidth	: 160,
				bodyStyle: 	'padding: 6px; padding-right:0px;padding-left:2px;',
				items		:	
				[
					
					{
						xtype				: 'combo',
						id          	: 'id_grupo',
						name				: 'grupo',
						hiddenName 		: 'grupo',
						fieldLabel  	: '* Grupo',
						forceSelection	: true,
						triggerAction	: 'all',
						mode				: 'local',
						typeAhead		: true,
						emptyText		:'Seleccione...',
						valueField		: 'clave',
						displayField	: 'descripcion',
						width				: 400,
						allowBlank		: false,
						margins			: '0 20 0 0',
						tabIndex			: 1,
						store				: catalogoGrupo
					},
					{
						xtype			: 'textfield',
						name			: 'universidad',
						id				: 'universidad',
						fieldLabel	: '* Nombre Universidad',
						maxLength	: 100,
						width			: 350,
						allowBlank	: false,
						margins		: '0 20 0 0',  //necesario para mostrar el icono de error
						tabIndex		:	2
					},
					{
						xtype 		: 'textfield',
						name  		: 'rfc',
						id    		: 'rfc',
						fieldLabel	: '* R.F.C.',
						maxLength	: 14,
						width			: 150,
						allowBlank 	: false,
						margins		: '0 20 0 0', 
						hidden		: false,
						regex			:/^([A-Z|a-z|&amp;]{3})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
						regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
						'en el formato NNN-AAMMDD-XXX donde:<br>'+
						'NNN:son las iniciales del nombre de la empresa<br>'+
						'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
						'XXX:es la homoclave',
						tabIndex		:	3
					},
					
					{
						xtype			: 'textfield',
						name			: 'campus',
						id				: 'campus',
						fieldLabel	: '* Nombre Campus',
						maxLength	: 100,
						width			: 350,
						allowBlank	: false,
						margins		: '0 20 0 0',
						tabIndex		:	4
					},
					{
						xtype				: 'combo',
						name				: 'cmb_estatus',
						id					: 'id_cmb_estatus',
						fieldLabel		: '* Estatus',
						width				: 100,
						forceSelection: true,
						valueField		: 'clave',
						displayField	: 'descripcion',
						triggerAction	: 'all',
						typeAhead		: true,
						minChars			: 1,
						mode				: 'local',				
						allowBlank		: false,
						margins			: '0 20 0 0',  //necesario para mostrar el icono de error
						tabIndex			:	5,
						store				: catalogoEstatus
					},
					{ 	xtype: 'displayfield',  value: '' }	
			]}
	]};
//---------------------------------------------------------------------------//

	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		labelWidth	: 150,
		bodyStyle: 	'padding: 6px; padding-right:0px;padding-left:2px;',
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calle',
				name			: 'calle',
				fieldLabel  : '* Calle, No. Exterior y No. Interior',
				maxLength	: 100,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	6

			},
			//codigo postal
			{
				xtype			: 'textfield',
				id          : 'code',
				name			: 'code',
				fieldLabel  : '* C�digo Postal',
				allowBlank	: false,
				hidden		: true,
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 90,
				tabIndex		: 8
			},
			//combo estado
			{
				xtype				: 'combo',
				id          	: 'id_cmb_estado',
				name				: 'cmb_estado',
				hiddenName 		: 'cmb_estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				allowBlank		: false,
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				tabIndex			: 10,
				store				: catalogoEstado,
				listeners: {
					select: function(combo, record, index) {						 
						 Ext.getCmp('id_cmb_delegacion').reset();
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais')).getValue()
							})
						});
					}
				}
				
			},
			{
				xtype			: 'textfield',
				id          : 'telefono',
				name			: 'telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30,
				tabIndex		: 12
				
			},
			{
				xtype			: 'textfield',
				id          : 'fax',
				name			: 'fax',
				fieldLabel  : 'Fax',
				width			: 230,
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		:	14
			}
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		labelWidth	: 150,
		bodyStyle: 	'padding: 6px; padding-right:0px;padding-left:2px;',	
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'colonia',
				name			: 'colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 100,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 7
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais',
				name				: 'cmb_pais',
				hiddenName 		: 'cmb_pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 150,
				tabIndex			: 9,
				store				: catalogoPais,
				listeners: {
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado').reset();
						Ext.getCmp('id_cmb_delegacion').reset();
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
							
							
					}
				}				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacion',
				name				: 'cmb_delegacion',
				hiddenName 		: 'cmb_delegacion',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				tabIndex			: 11,
				store				: catalogoMunicipio
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'email',
				name			: 'email',
				fieldLabel: ' E-mail',
				allowBlank	: false,
				maxLength	: 100,
				width			: 230,
				tabIndex		: 13,
				margins		: '0 20 0 0',
				vtype: 'email'
			},
			{ 	xtype: 'displayfield',  value: '' }	
		]
	};
	
	/*--____Fin Domicilio ____--*/
//---------------------------------------------------------------------------//

		/*--____ Datos del Representante Legal ____--*/
	
	var datosRepresentante_izq = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_izq',
		border	: false,
		labelWidth	: 150,
		bodyStyle: 	'padding: 6px; padding-right:0px;padding-left:2px;',	
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'paterno',
				name			: 'paterno',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 15
			},
			{
				xtype			: 'textfield',
				id          : 'nombre',
				name			: 'nombre',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 17
			},
			{
				xtype			: 'textfield',
				id          : 'num_iden',
				name			: 'num_iden',
				fieldLabel  : 'No. de Identificaci�n',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		: 19
			},
			{
				xtype			: 'textfield',
				id          : 'poderes',
				name			: 'poderes',
				fieldLabel  : 'Poderes y Facultades No. de Escritura',
				maxLength	: 40,
				width			: 230,
				allowBlank	: true,
				tabIndex		: 20
			}
		]
	};
		
	var datosRepresentante_der = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_der',
		border	: false,
		labelWidth	: 150,
		bodyStyle: 	'padding: 6px; padding-right:0px;padding-left:2px;',	
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'materno',
				name			: 'materno',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 16
			},			
			{
				xtype				: 'combo',
				id          	: 'id_cmb_identificacion',
				name				: 'cmb_identificacion',
				hiddenName 		: 'cmb_identificacion',
				fieldLabel  	: 'Identificaci�n ',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				tabIndex			: 18,
				store				: catalogoIdentificacion
			},
			
			{ 	xtype: 'displayfield',  value: '',height:20 }	,
			{
				xtype			: 'datefield',
				id          : 'fch_poder',
				name			: 'fch_poder',
				allowBlank	: false,
				width			: 130,
				fieldLabel  : '* Fecha del poder notarial',
				margins		: '0 20 0 0',
				tabIndex		: 21
			}
		]
	};
	
	/*--____Fin Datos del Representante Legal ____--*/

//---------------------------------------------------------------------------//	
	
	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			
			{
				layout	: 'hbox',
				title		: 'Nombre Completo',
				width		: 940,
				items		: [afiliacionPanel ]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 940,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			},
			{
				layout	: 'hbox',
				title		: 'Datos del Representante Legal',
				width		: 940,
				items		: [datosRepresentante_izq, datosRepresentante_der ]
			}
		],
		monitorValid	: true,
		buttons			: [
			{
				text: 'Actualizar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				formBind: true,				
				handler: function(boton, evento) 
				{
					/********************** Validaciones ********************/
					
					
					// --N�mericas--
					var variable = Ext.getCmp('code');
					if (!esVacio(variable.getValue())){
						if (!isdigit(variable.getValue())){
							variable.markInvalid('Verifique el formato del campo C�digo Postal');							
							variable.focus();
							return;
					}}		
					
					variable = Ext.getCmp('telefono');
					if (!esVacio(variable.getValue())){
						if (!isdigit(variable.getValue())){
							variable.markInvalid('Verifique el formato del campo Tel�fono');							
							variable.focus();
							return;
					}}	
					
					variable = Ext.getCmp('fax');
					if (!esVacio(variable.getValue())){
						if (!isdigit(variable.getValue())){
							variable.markInvalid('Verifique el formato del campo Fax');							
							variable.focus();
							return;
					}}		
					
					variable = Ext.getCmp('poderes');
					if (!esVacio(variable.getValue())){
						if (!isdigit(variable.getValue())){
							variable.markInvalid('Verifique el formato del campo N�mero de Escritura');							
							variable.focus();
							return;
					}}		
					
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							var cmpForma = Ext.getCmp('formaAfiliacion');
							var params = (cmpForma)?cmpForma.getForm().getValues():{};				
							cmpForma.el.mask("Procesando", 'x-mask-loading');
																	
							Ext.Ajax.request({
									url: '15forma29Ext.data.jsp',
									params: Ext.apply(params, {										
											informacion: 'ModificaUniversidad',
											ic_universidad: ic_universidad
									}),
									callback: procesarSuccessFailureModificaUniversidad
							});
						}
					});		
				}
			
			}, //fin boton
			{
				text:'Regresar',
				handler: function() {
					window.location = '15forma28Ext.jsp';
				}
			},
			{
				text:'Cancelar',
				handler: function() {
					Ext.Ajax.request({
							url: '15forma29Ext.data.jsp',
							params: Ext.apply({
								informacion: 'ConsultaUniversidad',
								ic_universidad : ic_universidad
								}),				
							callback: procesarSuccessFailureConsultaUniversidad
					});
				}
			}
		]
	});			
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items: //fp
		[
			fpDatos,
			NE.util.getEspaciador(10)
		]
	});


	
	catalogoGrupo.load();
	catalogoPais.load();
	catalogoEstado.load({ params : Ext.apply({
									pais : '24'	})
							});
	catalogoIdentificacion.load();
	
});