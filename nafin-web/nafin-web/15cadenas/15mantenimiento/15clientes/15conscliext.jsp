<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>


<html>
  <head>
    <title>Nafi@net - Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <%@ include file="/extjs.jspf" %>
	 <%@ include file="/01principal/menu.jspf"%>
	 <script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
    <script type="text/javascript" src="15conscliext.js"></script>
  </head>
  
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
	<form id='formParametros' name="formParametros">		
		<input type="hidden" id="strPerfil" name=" " value="<%=strPerfil%>"/>			
		</form>
	<form id='formAux' name="formAux" target='_new'></form>
</body>

</html>
