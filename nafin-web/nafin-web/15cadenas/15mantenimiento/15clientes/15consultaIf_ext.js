Ext.onReady(function() {
	
	var formDato = {
		numeroNafin : null,
		ic_if: null,
		estado:null
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var muestraDetalle = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var numNafinElec = registro.get('NUMNAFINELECT');
		var ventana = Ext.getCmp('winDetalle');
		if (ventana) {
			ventana.destroy();
		}
		Ext.Ajax.request({url: '15consultaIfext.data.jsp',	params: Ext.apply({ informacion: 'obtenDetalle',	numNafinElec: numNafinElec }),callback: procesaMuestraDetalle	});
	}

	var procesaMuestraDetalle = function (opts, success, response) {
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			
			if (!Ext.isEmpty(infoR.consultaDetIf)) {
				if (infoR.consultaDetIf.email_contacto == undefined ) {var email_contacto = "";}
					new Ext.Window({
						modal: true,
						resizable: false,
						layout: 'form',
						width: 640,
						autoHeight:'auto',
						id: 'winDetalle',
						closeAction: 'hide',
						title: 'Detalle Consulta de Intermediarios Financieros',
						bbar: {
							xtype: 'toolbar',
							buttons: ['->','-',{xtype: 'button',text: 'Cerrar',id: 'btnClose',iconCls: 'icoLimpiar',handler: function() {	Ext.getCmp('winDetalle').hide();	}}]
						},
						html:	'	<table width="640" border="0" align="center"> '+
									'<td align="middle"><br>'+
										'<img alt="" border="0" src="../../../00archivos/15cadenas/15archcadenas/logos/nafin.gif"> '+
										'<hr>'+
									'</td>'+
										'<table width="640" align="center">'+
												'<table width="600" align="center">'+
													'<tr>'+
														'<td><span class="formas">Razon Social:</span></td><td><span class="formas">'+infoR.consultaDetIf.razonsocial+'</span></td>'+
														'<td><span class="formas">No. N@E:</span></td><td><span class="formas">'+infoR.consultaDetIf.numnafinelect+'</span></td>'+
													'</tr>'+
													'<tr>'+
														'<td colspan="4">&nbsp;</td>'+
													'</tr>'+
													'<tr>'+
														'<td><span class="formas">Nombre Comercial:</span></td><td><span class="formas">'+infoR.consultaDetIf.nombre_comercial+'</span></td>'+
														'<td><span class="formas">R.F.C.:</span></td><td><span class="formas">'+infoR.consultaDetIf.rfc+'</span></td>'+
													'</tr>'+
												'</table>'+
										 '</table><br>'+
										//<!-- ................................ -->
										'<table width="640" align="center">'+
											'<tr><td colspan="4" bgcolor="silver"><span class="titulos"><font color="#276588">Domicilio</font></span></td></tr>'+
											'<table width="600" align="center">'+
												'<tr>'+
													'<td><span class="formas">Calle, No. Exterior y No. Interior:</span></td><td><span class="formas">'+infoR.consultaDetIf.calle+'</span></td>'+
													'<td><span class="formas">Colonia:</span></td><td><span class="formas">'+infoR.consultaDetIf.colonia+'</span></td>'+
												'</tr>'+
												'<tr>'+
													'<td colspan="4">&nbsp;</td>'+
												'</tr>'+
												'<tr>'+
													'<td><span class="formas">Estado:</span></td><td><span class="formas">'+infoR.consultaDetIf.estado+'</span></td>'+
													'<td><span class="formas">Delegaci&oacute;n o Municipio:</span></td><td><span class="formas">'+infoR.consultaDetIf.municipio+'</span></td>'+
												'</tr>'+
												'<tr>'+
													'<td colspan="4">&nbsp;</td>'+
												'</tr>'+
												'<tr>'+
													'<td><span class="formas">C&oacute;digo Postal:</span></td><td><span class="formas">'+infoR.consultaDetIf.codigo_postal+'</span></td>'+
													'<td><span class="formas">Pa&iacute;s:</span></td><td><span class="formas">'+infoR.consultaDetIf.pais+'</span></td>'+
												'</tr>'+
												'<tr>'+
													'<td colspan="4">&nbsp;</td>'+
												'</tr>'+
												'<tr>'+
													'<td><span class="formas">Tel&eacute;fono:</span></td><td><span class="formas">'+infoR.consultaDetIf.telefono+'</span></td>'+
													'<td><span class="formas">E-mail:</span></td><td><span class="formas">'+email_contacto+'</span></td>'+
												'</tr>'+
												'<tr>'+
													'<td colspan="4">&nbsp;</td>'+
												'</tr>'+
												'<tr>'+
													'<td><span class="formas">Fax:</span></td><td><span class="formas">'+infoR.consultaDetIf.fax+'</span></td>'+
													'<td>&nbsp;</td>'+
												'</tr>'+
											'</table>'+
										'</table><br>'+
									'</table>'
					}).show();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			Ext.getCmp('btnBajarArchivo').hide();
			Ext.getCmp('btnBajarPDF').hide();
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnGenerarPDF').enable();
				el.unmask();
			}else {
				Ext.getCmp('btnGenerarArchivo').disable();
				Ext.getCmp('btnGenerarPDF').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15consultaIfext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NUMNAFINELECT'},
			{name: 'RAZONSOCIAL'},
			{name: 'CALLE'},
			{name: 'ESTADO'},
			{name: 'TELEFONO'},
			{name: 'CS_TIPO'},
			{name: 'CG_BANCO'},
			{name: 'CG_NUM_CUENTA'},
			{name: 'PLAZA'},
			{name: 'CS_ACEPTACION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){Ext.apply(options.params, {numNafinElec:formDato.numeroNafin, ic_if:formDato.ic_if, estado:formDato.estado});}	},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var catalogoEstadosData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consultaIfext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstados'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consultaIfext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIfDist'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	/*var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Intermediarios Financieros', colspan: 6, align: 'center'},
				{header: 'IFs Relacionados', colspan: 5, align: 'center'}
			]
		]
	});*/

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header : 'N�mero de Naf�n Electr�nico', tooltip: 'Numero de Naf�n Electr�nico', dataIndex : 'NUMNAFINELECT',
				sortable: true,	align: 'center',	width: 120,	resizable: true, hidden: false, hideable:true
			},{
				header : 'Nombre o Raz�n Social', tooltip: 'Nombre o Raz�n Social', dataIndex : 'RAZONSOCIAL',
				sortable: true,	align: 'center',	width: 250,	resizable: true, hidden: false, hideable:true
			},{
				header : 'Domicilio', tooltip: 'Domicilio', dataIndex : 'CALLE',
				sortable: true,	align: 'center',	width: 250,	resizable: true, hidden: false, hideable:true
			},{
				header : 'Estado', tooltip: 'Estado', dataIndex : 'ESTADO',
				sortable: true,	width: 100,	resizable: true, hidden: false, hideable:true
			},{
				header : 'Tel�fono', tooltip: 'Tel�fono', dataIndex : 'TELEFONO',
				sortable: true,	align: 'center',	width: 100,	resizable: true, hidden: false, hideable:true
			},{
				xtype:	'actioncolumn',
				header : 'Detalle', tooltip: 'Detalle',	dataIndex : '',	width:	100,	align: 'center',	hideable:true,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('NUMNAFINELECT')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraDetalle
					}
				]
			},{
				header : 'Tipo IF', tooltip: 'Tipo IF', dataIndex : 'CS_TIPO',
				sortable: true,	align: 'center',	width: 100,	resizable: true, hidden: false, hideable:true
			},{
				header : 'Banco', tooltip: 'Banco', dataIndex : 'CG_BANCO',
				sortable: true,	align: 'center',	width: 120,	resizable: true, hidden: false, hideable:true
			},{
				header : 'Datos de Cuenta', tooltip: 'Datos de Cuenta', dataIndex : 'CG_NUM_CUENTA',
				sortable: true,	align: 'center',	width: 150,	resizable: true, hidden: false, hideable:true
			},{
				header : 'Plaza', tooltip: 'Plaza', dataIndex : 'PLAZA',
				sortable: true,	align: 'center',	width: 200,	resizable: true, hidden: false, hideable:true
			},{
				header : 'Autorizaci�n', tooltip: 'Autorizaci�n', dataIndex : 'CS_ACEPTACION',
				sortable: true,	align: 'center',	width: 100,	resizable: true, hidden: false, hideable:true
			}
		],
		//plugins: grupos,
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		colunmWidth: true,
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15consultaIfext.data.jsp',
							params: {
								numNafinElec:formDato.numeroNafin, 
								ic_if:formDato.ic_if, 
								estado:formDato.estado,
								informacion: 'ArchivoCSV',
								tipo: 'CSV'
								},
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar Archivo',
					//iconCls: 'icoXls',
					id: 'btnBajarArchivo',
					hidden: true
				},'-',{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '15consultaIfext.data.jsp',
							params: {
								numNafinElec:formDato.numeroNafin, 
								ic_if:formDato.ic_if, 
								estado:formDato.estado,
								informacion: 'ArchivoPDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize,
								tipo: 'PDF'
								},
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar PDF',
					//iconCls: 'icoPdf',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
	});
	var elementosForma = [
		{
			xtype:		'numberfield',
			id:			'txtNumero',
			name:			'numNafinElec',
			maxLength:	38,
			anchor:		'70%',
			fieldLabel:	'N�mero Naf�n Electr�nico'
		},{
			xtype: 'combo',
			name: 'ic_if',
			id:	'cboIf',
			fieldLabel: 'Intermediario Financiero',
			emptyText: 'Seleccione un Intermediario Financiero valido. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_if',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIfData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype:		'combo',
			name:			'estado',
			id:			'cboEstado',
			hiddenName:	'estado',
			fieldLabel:	'Estado',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEstadosData,
			tpl : NE.util.templateMensajeCargaCombo
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		title:	'Consulta de Intermediarios Financieros',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		labelWidth: 160,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
				formDato = {numeroNafin : null,ic_if: null,estado:null}
					grid.hide();
					fp.el.mask('Enviando...','x-mask-loading');
					formDato.numeroNafin =	Ext.getCmp('txtNumero').getValue();
					formDato.ic_if			=	Ext.getCmp('cboIf').getValue();
					formDato.estado		=	Ext.getCmp('cboEstado').getValue();
					consultaData.load({
							params: Ext.apply({//fp.getForm().getValues(),{
							operacion: 'Generar', //Generar datos para la consulta
							start: 0,
							limit: 15
							})
					});
				} //fin-handler
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});

	catalogoEstadosData.load();
	catalogoIfData.load();

});