Ext.onReady(function() {

    var cambiosRenderer = function( value, metadata, record, rowIndex, colIndex, store){
        metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
	metadata.attr += '" ';
	return value;
    };	
    
    var VerDatosNotificacion = function(grid, rowIndex, colIndex, item, event) {
    
        var registro = grid.getStore().getAt(rowIndex);
        var icBitAfiliados = registro.get('IC_BIT_AFILIADOS');   
        var nombrePyme = registro.get('RAZON_SOCIAL');   
        consultaDataE.load({
            params: Ext.apply(fp.getForm().getValues(),{
                icBitAfiliados:icBitAfiliados
            })
	});
        var ventana = Ext.getCmp('ventanaE');
        if(ventana){
            ventana.setTitle(nombrePyme);
            ventana.show();
        }else{
            new Ext.Window({
                layout: 'fit',
                resizable: false,
                width: 820,
		height: 250,
		id: 'ventanaE',		
                closeAction: 'hide',
                title:nombrePyme,
                items: [
                    gridE
                ]		
            }).show().setTitle(nombrePyme);
	} 
    };
    
    var procesarConsultaDataE = function(store,arrRegistros,opts){
        if(arrRegistros != null){
            var gridE = Ext.getCmp('gridE');
            var el = gridE.getGridEl();
            if(store.getTotalCount()>0){
                el.unmask();
            }else{
                el.mask('No se encontr� ning�n registro','x-mask');
            }
	}
    };
    
    var consultaDataE = new Ext.data.JsonStore({
        root: 'registros',
	url: '15BitaActualizacionDatos.data.jsp',
	baseParams: {
            informacion: 'Cons_EnvioEmail'
	},
        fields: [
            {name: 'NOMBRE_INTERMEDIARIO'},
            {name: 'FECHA_NOTIFICACION'},
            {name: 'OBSERVACIONES'}                     
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
            load: procesarConsultaDataE,
            exception: {
                fn: function(proxy,type,action,optionRequest,response,args){
                    NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
                    procesarConsultaDataE(null,null,null);
		}
            }
	}
    });
    
    var gridE = {
        xtype: 'grid',
	store: consultaDataE,
	id: 'gridE',
	width: 780,
	height: 200,
	columns: [
            {
                header: 'Nombre Intermediario',
                tooltip: 'Nombre Intermediario',
		dataIndex: 'NOMBRE_INTERMEDIARIO',
		align: 'left',
		width: 200
            },   
            {
                header: 'Fecha y Hora de Notificaci�n',
                tooltip: 'Fecha y Hora de Notificaci�n',
		dataIndex: 'FECHA_NOTIFICACION',
		align: 'center',
		width: 170
            },
             {
                header: 'Observaciones ',
                tooltip: 'Observaciones',
		dataIndex: 'OBSERVACIONES',
		align: 'left',
		width: 400,
                renderer:cambiosRenderer
            }
        ],
	stripeRows: true,
	loadMask: true,
	title: '',
	frame: true
    };
    
    
    
    var VerDatosEnterado = function(grid, rowIndex, colIndex, item, event) {
        var registro = grid.getStore().getAt(rowIndex);
        var icBitAfiliados = registro.get('IC_BIT_AFILIADOS');   
        var nombrePyme = registro.get('RAZON_SOCIAL');  
        
        consultaDataB.load({           
            params: Ext.apply(fp.getForm().getValues(),{
                icBitAfiliados:icBitAfiliados
            })
	});
                        
        var ventana = Ext.getCmp('ventanaB');
        if(ventana){
            ventana.setTitle(nombrePyme);
            ventana.show();
        }else{
            new Ext.Window({
                layout: 'fit',   
                resizable: false,
		width: 780,
		height: 200,
		id: 'ventanaB',		
                closeAction: 'hide',                
		items: [
                    gridB
                ]		
            }).show().setTitle(nombrePyme);
	}            
    };
    
    var procesarConsultaDataB = function(store,arrRegistros,opts){
        if(arrRegistros != null){
            var gridB = Ext.getCmp('gridB');
            var el = gridB.getGridEl();
            if(store.getTotalCount()>0){
                el.unmask();
            }else{
                el.mask('No se encontr� ning�n registro','x-mask');
            }
	}
    };
    var consultaDataB = new Ext.data.JsonStore({
        root: 'registros',
	url: '15BitaActualizacionDatos.data.jsp',
	baseParams: {
            informacion: 'Cons_Enterado'
	},
        fields: [
            {name: 'NOMBRE_INTERMEDIARIO'},
            {name: 'USUARIO'},
            {name: 'NOMBRE_USUARIO'},
            {name: 'FECHA_ENTERADO'} ,
            {name: 'HORA_ENTERADO'}             
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
            load: procesarConsultaDataB,
            exception: {
                fn: function(proxy,type,action,optionRequest,response,args){
                    NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
                    procesarConsultaDataB(null,null,null);
		}
            }
	}
    });
    
    var gridB = {
        xtype: 'grid',
	store: consultaDataB,
	id: 'gridB',
	width: 700,
	height: 200,
	columns: [
            {
                header: 'Nombre Intermediario',
                tooltip: 'Nombre Intermediario',
		dataIndex: 'NOMBRE_INTERMEDIARIO',
		align: 'left',
		width: 200
            },   
            {
                header: 'Usuario',
                tooltip: 'Usuario',
		dataIndex: 'USUARIO',
		align: 'center',
		width: 100
            },            
            {
                header: 'Nombre Usuario',
                tooltip: 'Nombre Usuario',
		dataIndex: 'NOMBRE_USUARIO',
		align: 'left',
		width: 200
            },
            {
                header: 'Fecha de Enterado',
                tooltip: 'Fecha de Enterado',
		dataIndex: 'FECHA_ENTERADO',
		align: 'center',
		width: 120
            } ,
            {
                header: 'Hora',
                tooltip: 'Hora',
		dataIndex: 'HORA_ENTERADO',
		align: 'center',
		width: 120                
            }             
        ],
	stripeRows: true,
	loadMask: true,
	title: '',
	frame: true
    };
        

    function procesarDescargaArchivos(opts, success, response) {
        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
            var infoR = Ext.util.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
            fp.getForm().getEl().dom.submit();
	}else {
            NE.util.mostrarConnError(response,opts);
	}
    };
        
    var procesarConsultaData = function(store, arrRegistros, opts) 	{
        var fp = Ext.getCmp('forma');
        fp.el.unmask();
                
        var gridConsulta = Ext.getCmp('gridConsulta');	
        var el = gridConsulta.getGridEl();	
	if (arrRegistros != null) {
            if (!gridConsulta.isVisible()) {
                gridConsulta.show();
            }	
		
            if(store.getTotalCount() > 0) {	
                Ext.getCmp('btnDescargar').enable();
                el.unmask();
            } else {	
                Ext.getCmp('btnDescargar').disable();
		el.mask('No existe registros ', 'x-mask');				
            }
	}
    };
        
    var consultaData = new Ext.data.JsonStore({
        root: 'registros',
	url: '15BitaActualizacionDatos.data.jsp',
	baseParams: {
            informacion: 'Consulta'
	},
	fields: [      
            {name: 'IC_BIT_AFILIADOS'},
            {name: 'NA_ELECTRONICO'},
            {name: 'RAZON_SOCIAL'},
            {name: 'RFC'},
            {name: 'TRAMITE'},
            {name: 'DF_MODIFICACION'}           
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
            load: procesarConsultaData,
            exception: {
                fn: function(proxy,type,action,optionsRequest,response,args){
                    NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
                    procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
		}
            }
	}
    });


    var gridConsulta = new Ext.grid.GridPanel({
        id: 'gridConsulta',
        hidden: true,
	header:true,
	title: ' Bit�cora de Actualizaci�n de Datos ',
	store: consultaData,
	style: 'margin:0 auto;',
	columns:[
            {
                header:'<center>NE </center>',
		tooltip: 'Nafin Electr�nico ',
		sortable: true,
		dataIndex: 'NA_ELECTRONICO',
		width: 80,
		align: 'center'
            },
            {
                header:'<center>RFC</center>',
		tooltip: 'RFC',
		sortable: true,
		dataIndex: 'RFC',
		width: 110,
		align: 'left'
            },
            {
                header:'<center>Raz�n Social</center>',
		tooltip: 'Raz�n Social',
		sortable: true,
		dataIndex: 'RAZON_SOCIAL',
		width: 260,
		align: 'left'
            },
            {
                header:'<center>Tr�mite</center>',
		tooltip: 'Tr�mite',
		sortable: true,
		dataIndex: 'TRAMITE',
		width: 250,
		align: 'left',
                renderer:cambiosRenderer
            },
            {
                header:'<center>Fecha Modificaci�n</center>',
		tooltip: 'Fecha Modificaci�n',
		sortable: true,
		dataIndex: 'DF_MODIFICACION',
		width: 140,
		align: 'left'
            } ,
            {
                xtype: 'actioncolumn',
		header: 'Enterado',
		tooltip: 'Enterado',
		dataIndex: 'IC_BIT_AFILIADOS',
		width: 80,
		align: 'center',
		items: [
                    {
                        getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
                            this.items[0].tooltip = 'Buscar';
                            return 'icoBuscar';		
                        },handler: VerDatosEnterado
                    }
                ]				
            },
            {
                xtype: 'actioncolumn',
		header: 'Notificaci�n',
		tooltip: 'Notificaci�n',
		dataIndex: 'IC_BIT_AFILIADOS',
		width: 80,
		align: 'center',
		items: [
                    {
                        getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
                            this.items[0].tooltip = 'Buscar';
                            return 'icoBuscar';		
                        },handler: VerDatosNotificacion
                    }
                ]				
            }  
        ],
        stripeRows: true,
        loadMask: true,
	height: 300,
	width: 900,	
	frame: true,
        bbar: {
            items: [
                '-','->',
                {
                    xtype: 'button',
                    text: 'Descargar ',
                    id: 'btnDescargar',                    
                    iconCls: 'icoXls',
                    handler: function(boton, evento) {
                        Ext.Ajax.request({
                            url: '15BitaActualizacionDatos.data.jsp',
                            params: Ext.apply( fp.getForm().getValues(), {
                                informacion: 'GenerarArchivo'
                            }),
                            callback: procesarDescargaArchivos
			});
                    }
		}
            ]
        }
    });
    
    
    function procesarSuccessFailureNombreNae(opts, success, response) {
        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
            var infoR = Ext.util.JSON.decode(response.responseText);
            
            if(infoR.existe==='S'){
                var nom_Nae = infoR.nom_Nae;
                Ext.getCmp('_txt_nombre').setValue(nom_Nae);
            
            }else  if(infoR.existe==='N'){
                Ext.Msg.alert('Aviso','El No. Nafin Electr�nico no corresponde a una PyME afiliada o no existe.');
            }
        }
    };
    
    var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
        var jsonData = store.reader.jsonData;
        if (jsonData.success){
            if (jsonData.total === "excede"){
                Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
		Ext.getCmp('fpWinBusca').getForm().reset();
		Ext.getCmp('fpWinBuscaB').getForm().reset();
		return;
            }else if (jsonData.total === "0"){
                Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
		Ext.getCmp('fpWinBusca').getForm().reset();
		Ext.getCmp('fpWinBuscaB').getForm().reset();
		return;
            }
            Ext.getCmp('cmb_num_ne').focus();
        }
    } ;   
        
    var catalogoNombreProvData = new Ext.data.JsonStore({
        id: 'catalogoNombreStore',
        root : 'registros',
	fields : ['clave', 'descripcion', 'loadMsg'],
	url : '15BitaActualizacionDatos.data.jsp',
	baseParams: {
            informacion: 'CatalogoBuscaAvanzada'
	},
	totalProperty : 'total',
	autoLoad: false,
	listeners: {
            load: procesarCatalogoNombreProvData,
            exception: NE.util.mostrarDataProxyError,
            beforeload: NE.util.initMensajeCargaCombo
	}
    });
        
    var catTramite= new Ext.data.JsonStore({
        id: 'catTramite',
	root : 'registros',
	fields : ['clave', 'descripcion', 'loadMsg'],
	url : '15BitaActualizacionDatos.data.jsp',
	baseParams: {
            informacion: 'catTramite'
	},
	totalProperty : 'total',
	autoLoad: false,		
	listeners: {			
            exception: NE.util.mostrarDataProxyError,
            beforeload: NE.util.initMensajeCargaCombo
	}		
    });
      
        
    var  elementosForma = [ 
        {
            xtype: 'compositefield',
            id:'_nafelec',
            combineErrors: false,
            msgTarget: 'side',
            fieldLabel: 'No. Nafin Electr�nico',
            items: [
                {
                    xtype: 'textfield',
                    name: 'txt_ne',
                    id:	'_txt_ne',                    
                    hiddenName:	'txt_ne',
                    maskRe:/[0-9]/,
                    allowBlank:	true,
                    maxLength:	15,		
                    listeners:{
                        'change':function(field){
                           
                            var num_naf = field.getValue();
                            Ext.getCmp('_txt_nombre').setValue('');
                            Ext.Ajax.request({
                                url: '15BitaActualizacionDatos.data.jsp',
                                params: {
                                    informacion: 'CargaNom_Nae',                                    
                                    num_Nae: num_naf
                                },
                                callback: procesarSuccessFailureNombreNae
                            });                            
                        }
                    }
                },                
                {
                    xtype: 'textfield',
                    name: 'txt_nombre',
                    id:	'_txt_nombre',                    
                    width:260,
                    disabledClass:"x2",	//Para cambiar el estilo de la clase deshabilitada
                    disabled:	true,
                    allowBlank:	true,
                    maxLength:	100
                }
            ]
        },
        {
            xtype: 'compositefield',
            combineErrors: false,
            id: 'busqava',
            msgTarget: 'side',
            items: [
                {
                    xtype:'displayfield',
                    id:'disEspacio',
                    text:''
		},
		{
                    xtype:	'button',
                    id:		'btnBuscaA',
                    iconCls:	'icoBuscar',
                    text: 'B�squeda Avanzada',
                    handler: function(boton, evento) {                        
                       		
                        Ext.getCmp('_txt_ne').reset();
                        Ext.getCmp('_txt_nombre').reset();
                        var winVen = Ext.getCmp('winBuscaA');
                        if (winVen){
                            Ext.getCmp('fpWinBusca').getForm().reset();
                            Ext.getCmp('fpWinBuscaB').getForm().reset();
                            Ext.getCmp('cmb_num_ne').setValue();
                            Ext.getCmp('cmb_num_ne').store.removeAll();
                            Ext.getCmp('cmb_num_ne').reset();
                            winVen.show();
                        }else{
                            new Ext.Window ({
                                id:'winBuscaA',
                                height: 364,
                                x: 300,
                                y: 100,
                                width: 550,
                                modal: true,
                                closeAction: 'hide',
                                title: 'B�squeda Avanzada',
                                items:[
                                    {
                                        xtype:'form',
                                        id:'fpWinBusca',
                                        frame: true,
                                        border: false,
                                        style: 'margin: 0 auto',
                                        bodyStyle:'padding:10px',
                                        defaults: {
                                            msgTarget: 'side',
                                            anchor: '-20'
                                        },
                                        labelWidth: 130,
                                        items:[                                            
                                            {
                                                xtype:'displayfield',
                                                frame:true,
                                                border: false,
                                                value:'Utilice el * para b�squeda gen�rica'
                                            },
                                            {
                                                xtype:'displayfield',
                                                frame:true,
                                                border: false,
                                                value:''
                                            },
                                            {
                                                xtype:'hidden',
                                                id:	'hid_ic_pyme',
                                                value:''
                                            },
                                            {
                                                xtype:'hidden',
                                                id:	'hid_ic_banco',
                                                value:''
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'nombre_pyme',
                                                id:	'txtNombre',
                                                fieldLabel:'Nombre',
                                                maxLength:	100
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'rfc_prov',
                                                id:	'_rfc_prov',
                                                fieldLabel:'RFC',
                                                maxLength:	20
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'num_pyme',
                                                id:	'txtNe',
                                                fieldLabel:'N�mero de Nafin Electr�nico',
                                                maxLength:	25
                                            }
                                        ],
                                        buttonAlign:'center',
                                        buttons:[
                                            {
                                                text:'Buscar',
                                                iconCls:'icoBuscar',
                                                handler: function(boton) {                                                   
                                                    catalogoNombreProvData.load({ 
                                                        params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) 
                                                    });
                                                }
                                            },
                                            {
                                                text:'Cancelar',
                                                iconCls: 'icoRechazar',
                                                handler: function() {
                                                    Ext.getCmp('winBuscaA').hide();
                                                    Ext.getCmp('winBuscaA').destroy();
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype:'form',
                                        frame: true,
                                        id:'fpWinBuscaB',
                                        style: 'margin: 0 auto',
                                        bodyStyle:'padding:10px',
                                        monitorValid: true,
                                        defaults: {
                                            msgTarget: 'side',
                                            anchor: '-20'
                                        },
                                        labelWidth: 80,
                                        items:[
                                            {
                                                xtype: 'combo',
                                                id:	'cmb_num_ne',
                                                name: 'ic_pyme',
                                                hiddenName : 'ic_pyme',
                                                fieldLabel: 'Nombre',
                                                emptyText: 'Seleccione. . .',
                                                displayField: 'descripcion',
                                                valueField: 'clave',
                                                triggerAction : 'all',
                                                forceSelection:true,
                                                allowBlank: false,
                                                typeAhead: true,
                                                mode: 'local',
                                                minChars : 1,
                                                store: catalogoNombreProvData,
                                                tpl : NE.util.templateMensajeCargaCombo
                                            }
                                        ],
                                        buttonAlign:'center',
                                        buttons:[
                                            {
                                                text:'Aceptar',
                                                iconCls:'aceptar',
                                                formBind:true,
                                                handler: function() {
                                                    if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
                                                        var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
                                                        var cveP = disp.substr(0,disp.indexOf(" "));
                                                        var desc = disp.slice(disp.indexOf(" ")+1);
                                                        Ext.getCmp('_txt_ne').setValue(cveP);
                                                        Ext.getCmp('_txt_nombre').setValue(desc);
                                                        Ext.getCmp('winBuscaA').hide();
                                                    }
                                                }
                                            },
                                            {
                                                text:'Cancelar',
                                                iconCls: 'icoRechazar',
                                                handler: function() {	
                                                    Ext.getCmp('winBuscaA').hide();	
                                                    Ext.getCmp('winBuscaA').destroy();
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }).show();
                        }                        
                    }
                }
            ]
        },
        {
            xtype: 'compositefield',
            fieldLabel: 'RFC',
            combineErrors: false,
            msgTarget: 'side',
            items: [
                {
                    xtype: 'textfield',
                    name: 'rfc',
                    id: 'rfc',
                    allowBlank: true,
                    startDay: 0,
                    width: 150,
                    msgTarget: 'side',
                    margins: '0 20 0 0'  
                }
            ]
        },
        {
            xtype: 'compositefield',
            fieldLabel: 'Fecha Modificaci�n',
            combineErrors: false,
            msgTarget: 'side',
            items: [
                {
                    xtype: 'datefield',
                    name: 'fechaModifDe',
                    id: 'fechaModifDe',
                    allowBlank: true,
                    startDay: 0,
                    width: 100,
                    msgTarget: 'side',
                    minValue: '01/01/1901',
                    vtype: 'rangofecha',
                    campoFinFecha: 'fechaModifA',
                    margins: '0 20 0 0'  
                },
                {
                    xtype: 'displayfield',
                    value: 'a',
                    width: 25
		},
                {
                    xtype: 'datefield',
                    name: 'fechaModifA',
                    id: 'fechaModifA',
                    allowBlank: true,
                    startDay: 1,
                    width: 100,
                    msgTarget: 'side',
                    minValue: '01/01/1901',
                    vtype: 'rangofecha',
                    campoInicioFecha: 'fechaModifDe',
                    margins: '0 20 0 0'  
                }
            ]
        },
        {
            xtype: 'compositefield',
            fieldLabel: 'Fecha Notificaci�n',
            combineErrors: false,
            msgTarget: 'side',
            items: [
                {
                    xtype: 'datefield',
                    name: 'fechaNotifDe',
                    id: 'fechaNotifDe',
                    allowBlank: true,
                    startDay: 0,
                    width: 100,
                    msgTarget: 'side',
                    minValue: '01/01/1901',
                    vtype: 'rangofecha',
                    campoFinFecha: 'fechaNotifA',
                    margins: '0 20 0 0'  
                },
                {
                    xtype: 'displayfield',
                    value: 'a',
                    width: 25
		},
                {
                    xtype: 'datefield',
                    name: 'fechaNotifA',
                    id: 'fechaNotifA',
                    allowBlank: true,
                    startDay: 1,
                    width: 100,
                    msgTarget: 'side',
                    minValue: '01/01/1901',
                    vtype: 'rangofecha',
                    campoInicioFecha: 'fechaNotifDe',
                    margins: '0 20 0 0'  
                }
            ]
        },
        {
            xtype: 'compositefield',
            fieldLabel: 'Fecha Enterado',
            combineErrors: false,
            msgTarget: 'side',
            items: [
                {
                    xtype: 'datefield',
                    name: 'fechaEnteradoIni',
                    id: 'fechaEnteradoIni',
                    allowBlank: true,
                    startDay: 0,
                    width: 100,
                    msgTarget: 'side',  
                    minValue: '01/01/1901',
                    vtype: 'rangofecha',
                    campoFinFecha: 'fechaEnteradoFinal',
                    margins: '0 20 0 0'  
                },
                {
                    xtype: 'displayfield',
                    value: 'a',
                    width: 25
		},
                {
                    xtype: 'datefield',
                    name: 'fechaEnteradoFinal',
                    id: 'fechaEnteradoFinal',
                    allowBlank: true,
                    startDay: 1,
                    width: 100,
                    msgTarget: 'side',  
                    minValue: '01/01/1901',
                    vtype: 'rangofecha',
                    campoInicioFecha: 'fechaEnteradoIni', 
                    margins: '0 20 0 0'  
                }               
            ]
        },
        {
            xtype: 'combo',	
            name: 'claveTramite',	
            id: 'claveTramite',
            hiddenName : 'claveTramite',
            fieldLabel:'Tramite',
             mode: 'local', 
            autoLoad: false,	
            displayField : 'descripcion',	
            valueField : 'clave',
            emptyText: 'Seleccionar...',
            forceSelection : true,	
            triggerAction : 'all',	
            typeAhead: true,
            allowBlank: true,	
            minChars : 1,	
            store : catTramite,	
            tpl : NE.util.templateMensajeCargaCombo            
        }
    ];
       
                
    var fp = new Ext.form.FormPanel({
        id: 'forma',
	width: 650,
	monitorValid: true,
	title: 'Criterios de b�squeda ',
	frame: true,		
	titleCollapse: false,
	style: 'margin:0 auto;',
	bodyStyle: 'padding: 6px',
	labelWidth: 150,	
	defaultType: 'textfield',
	defaults: {
            msgTarget: 'side',
            anchor: '-20'
	},
	items: elementosForma,		
	buttons: [
            {
                text: 'Limpiar',
		iconCls: 'icoLimpiar',
		handler: function() {
                    Ext.getCmp('forma').getForm().reset();
                }
            },	 	
             {
                text: 'Consultar',
		id: 'Consultar',
		iconCls: 'icoBuscar',
		formBind: true,				
		handler: function(boton, evento) {
                
                    var fechaModifDe = Ext.getCmp('fechaModifDe');
                    var fechaModifA = Ext.getCmp('fechaModifA');
                    if (!Ext.isEmpty(fechaModifDe.getValue()) || !Ext.isEmpty(fechaModifA.getValue()) ) {
                        if(Ext.isEmpty(fechaModifDe.getValue()))	{
                            fechaModifDe.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
                            fechaModifDe.focus();
                            return;
			}else if (Ext.isEmpty(fechaModifA.getValue())){
                            fechaModifA.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
                            fechaModifA.focus();
                            return;
			}
                    }						
                 
                    var fechaNotifDe = Ext.getCmp('fechaNotifDe');
                    var fechaNotifA = Ext.getCmp('fechaNotifA');
                    if (!Ext.isEmpty(fechaNotifDe.getValue()) || !Ext.isEmpty(fechaNotifA.getValue()) ) {
                        if(Ext.isEmpty(fechaNotifDe.getValue()))	{
                            fechaNotifDe.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
                            fechaNotifDe.focus();
                            return;
			}else if (Ext.isEmpty(fechaNotifA.getValue())){
                            fechaNotifA.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
                            fechaNotifA.focus();
                            return;
			}
                    }	
                    
                    var fechaEnteradoIni = Ext.getCmp('fechaEnteradoIni');
                    var fechaEnteradoFinal = Ext.getCmp('fechaEnteradoFinal');
                    if (!Ext.isEmpty(fechaEnteradoIni.getValue()) || !Ext.isEmpty(fechaEnteradoFinal.getValue()) ) {
                        if(Ext.isEmpty(fechaEnteradoIni.getValue()))	{
                            fechaEnteradoIni.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
                            fechaEnteradoIni.focus();
                            return;
			}else if (Ext.isEmpty(fechaEnteradoFinal.getValue())){
                            fechaEnteradoFinal.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
                            fechaEnteradoFinal.focus();
                            return;
			}
                    }	
                    
                    
                
                    fp.el.mask('Enviando...', 'x-mask-loading');
                    consultaData.load({
                        params: Ext.apply(fp.getForm().getValues(),{
                            num_Nae: Ext.getCmp('_txt_ne').getValue()                           
                        })
                    });	
                }
            }
		
	]
    });
        
    new Ext.Container({
        id: 'contenedorPrincipal',
	applyTo: 'areaContenido',
	width: 949,		
	style: 'margin:0 auto;',
	items: [ 
            fp,
            NE.util.getEspaciador(20),
            gridConsulta 
        ]
    });  
        
    catTramite.load();
                                        
        
});