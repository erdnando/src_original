<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,		
		com.netro.exception.*, 
		com.netro.descuento.*,
		com.netro.cadenas.*,
		com.netro.seguridad.*,		
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  			
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String n_electronico = (request.getParameter("n_electronico")!=null)?request.getParameter("n_electronico"):""; 
	String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
	String num_electronico = request.getParameter("num_electronico")==null?"":request.getParameter("num_electronico");
	String ic_pyme = request.getParameter("ic_pyme")==null?"":request.getParameter("ic_pyme");	
	String txtNombre = request.getParameter("txtNombre")==null?"":request.getParameter("txtNombre");
		
	String infoRegresar ="", consulta ="";
	JSONObject jsonObj = new JSONObject();
	
	ParametrosDescuento parametrosDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

	// Pagiandor 
	ContactosEntidad paginador = new ContactosEntidad (); 
	paginador.setIc_pyme(ic_pyme);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	
if (informacion.equals("busquedaAvanzada")) {

	CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");	
	
	cat.setRfc_pyme(rfcPyme);	
	cat.setNombre_pyme(nombrePyme);
	cat.setNafin_electronico(n_electronico);
	cat.setOrden("p.cg_razon_social");		
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("pymeNombre")) {

	List datosPymes =  parametrosDescuento.datosPymes(num_electronico, "" ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}
	
	String validaEntidad = paginador.getValidaPymeEntidad(ic_pyme  );  //valida si la pyme opera  Entidad de Gobierno	
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);
	jsonObj.put("ic_pyme", ic_pyme);
	jsonObj.put("validaEntidad", validaEntidad);	
	infoRegresar = jsonObj.toString();	

}else  if (informacion.equals("Consultar")) {
	
	HashMap mapa=new HashMap();
	JSONArray registros = new JSONArray();	
	Registros reg	=	queryHelper.doSearch();
	String regprimer ="N";
	
	if(reg.getNumeroRegistros()>0)  {
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
	}else  {
	
		mapa=new HashMap();
		mapa.put("IC_CONTACTO","");
		mapa.put("APELLIDO_PATERNO","");
		mapa.put("APELLIDO_MATERNO","");
		mapa.put("NOMBRES","");
		mapa.put("TELEFONO","");
		mapa.put("FAX","");
		mapa.put("CORREO_ELECTRONICO","");	  	 			
		registros.add(mapa);
		regprimer="S";
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
	}
	jsonObj.put("nombrePyme", txtNombre);	
	jsonObj.put("regprimer", regprimer);
	infoRegresar = jsonObj.toString();  		

}else  if (informacion.equals("GuardarContactos")) {

	String ic_contacto[] = request.getParameterValues("ic_contacto");
	String ap_paterno[] = request.getParameterValues("ap_paterno");
	String ap_materno[] = request.getParameterValues("ap_materno");
	String nombre[] = request.getParameterValues("nombre");
	String telefono[] = request.getParameterValues("telefono");
	String fax[] = request.getParameterValues("fax");
	String email[] = request.getParameterValues("email");
	
	try{
		paginador.guardaContactos( ic_contacto,  ap_paterno,  ap_materno,  nombre,  telefono,  fax,  email, ic_pyme );
		jsonObj.put("success", new Boolean(true));	
	}catch(Exception e){
		jsonObj.put("success", new Boolean(false));		
	}	
	
	infoRegresar = jsonObj.toString();	
		
}else  if (informacion.equals("Inhabilitar")) {
	
	String ic_contacto = request.getParameter("ic_contacto")==null?"":request.getParameter("ic_contacto");
	
	try{
	
		paginador.inhabilitarContacto( ic_contacto );
		
		jsonObj.put("success", new Boolean(true));	
	}catch(Exception e){
		jsonObj.put("success", new Boolean(false));		
	}	
	
	infoRegresar = jsonObj.toString();	
	
}		
%>


<%=infoRegresar%>

