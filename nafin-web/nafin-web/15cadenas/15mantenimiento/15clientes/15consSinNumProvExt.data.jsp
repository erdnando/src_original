<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*,
		java.text.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  			
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	String fechaCapturaFin = (request.getParameter("fechaCapturaFin")!=null)?request.getParameter("fechaCapturaFin"):""; 
	String fechaCapturaIni = (request.getParameter("fechaCapturaIni")!=null)?request.getParameter("fechaCapturaIni"):""; 
	String txtNumElectronico = (request.getParameter("Numero")!=null)?request.getParameter("Numero"):""; 
	String txtNombre = (request.getParameter("txtNombre")!=null)?request.getParameter("txtNombre"):""; 
	String convenio_unico = (request.getParameter("convenioUnico")!=null)?request.getParameter("convenioUnico"):""; 
	String epo = (request.getParameter("epo")!=null)?request.getParameter("epo"):""; 
	String txtLogin = (request.getParameter("txtLogin")!=null)?request.getParameter("txtLogin"):""; 
	String tipoAfiliado = (request.getParameter("cmbTipoAfiliado")!=null)?request.getParameter("cmbTipoAfiliado"):"";	
	String claveEPO = request.getParameter("epo") == null?"":(String)request.getParameter("epo");		
	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion BeanAfiliacion = afiliacionHome.create();
	Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	
	String ic_epo = request.getParameter("claveEPO") == null?"":(String)request.getParameter("claveEPO");		
	ConsSinNumProv paginador = new ConsSinNumProv();
	String msg_error = "";
	String infoRegresar="", consulta="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	CambioPerfil clase = new  CambioPerfil();
	
	if(convenio_unico.equals("on")){  convenio_unico  ="S"; }
	if (informacion.equals("catalogoEstado")) {
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("COMCAT_ESTADO");
		cat.setCampoClave("IC_ESTADO");
		cat.setCampoDescripcion("CD_NOMBRE"); 
		cat.setOrden("CD_NOMBRE");
		infoRegresar = cat.getJSONElementos();

	} else  if(informacion.equals("catalogoPerfil") ){  
		
		HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  tipoAfiliado  );	
		List perfiles = (List)catPerfil.get("PERFILES");	
		Iterator it = perfiles.iterator();	
		HashMap datos = new HashMap();
		List registros  = new ArrayList();	
		while(it.hasNext()) {
			List campos = (List) it.next();
			String clave = (String) campos.get(0);
			datos = new HashMap();
			datos.put("clave", clave );
			datos.put("descripcion", clave );
			registros.add(datos);
		}
		jsonObj = new JSONObject();
		jsonObj.put("success",  new Boolean(true)); 	
		jsonObj.put("registros", registros);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("catalogoBancoFodeo")){
	
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_banco_fondeo");
		catalogo.setCampoDescripcion("cd_descripcion");
		catalogo.setTabla("comcat_banco_fondeo");
		catalogo.setOrden("1");
		infoRegresar = catalogo.getJSONElementos();
		System.out.println("infoRegresar "+infoRegresar);
	}else if(informacion.equals("catalogoEpo")){
	
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setBancofondeo("1");
		catalogo.setSeleccion(claveEPO);
		infoRegresar = catalogo.getJSONElementos();
	}else if(informacion.equals("Consultar")){
	
		paginador.setConvenio_unico(convenio_unico);
		paginador.setFec_fin(fechaCapturaFin);
		paginador.setFec_ini(fechaCapturaIni);
		paginador.setNum_electronico(txtNumElectronico);
		paginador.setTxtCadProductiva(epo);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
		try{
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);	
			infoRegresar = jsonObj.toString(); 
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}	
	}else if(informacion.equals("Captura")){
		
	   String numRegistros  =(request.getParameter("numRegistros")!=null) ? request.getParameter("numRegistros"):"";
		int numRegistros2 = Integer.parseInt(numRegistros);
		Calendar fecActual = Calendar.getInstance();
		String dia = Integer.toString(fecActual.get(Calendar.DATE));
		String mes = Integer.toString(fecActual.get(Calendar.MONTH)+1);
		String anio = Integer.toString(fecActual.get(Calendar.YEAR));
		String fecha = dia+"/"+mes+"/"+anio;
		List lstDatosTabla = new ArrayList();
		String flag = (request.getParameter("flag")!=null) ? request.getParameter("flag"):"";
		String flag2 = (request.getParameter("flag2")!=null) ? request.getParameter("flag2"):"";
		int i_numReg = Integer.parseInt(numRegistros);
		String numPyme[] = request.getParameterValues("numPyme");	
		String aux_eliminaRel = "";
		for(int i = 0; i< i_numReg; i++){
			String chkELIMINA[] = request.getParameterValues("chkELIMINA");
			String numero[] = request.getParameterValues("numero");
			String causa[] = request.getParameterValues("causa");
			if((chkELIMINA[i].equals("true")&&!causa[i].equals("")&&numero[i].equals(""))){
				aux_eliminaRel = "S";
			}else if(chkELIMINA[i].equals("true")&&!numero[i].equals("")&&!causa[i].equals("")){
				chkELIMINA[i]="";
				aux_eliminaRel = "N";
				causa[i]="";
			}else if(chkELIMINA[i].equals("")&&numero[i].equals("")&&!causa[i].equals("")){
				chkELIMINA[i]="true";
				aux_eliminaRel = "S";
			}else if((chkELIMINA[i].equals("")&&!causa[i].equals(""))){
				chkELIMINA[i]="true";
				aux_eliminaRel = "S";
			}else {
				aux_eliminaRel = "N";
				chkELIMINA[i]="false";
			}
				List lstRegNumProv = new ArrayList();
				lstRegNumProv.add(claveEPO);
				lstRegNumProv.add(fecha);
				lstRegNumProv.add(numPyme[i]);
				lstRegNumProv.add(numero[i]);
				lstRegNumProv.add(aux_eliminaRel);
				lstRegNumProv.add(causa[i]);
				lstDatosTabla.add(lstRegNumProv);
		}
		msg_error = BeanAfiliacion.actualizaProvSinNumero(lstDatosTabla,iNoUsuario);
		JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("success", new Boolean(true));
		infoRegresar = jsonObj1.toString();
		
	}else if(informacion.equals("CatalogoBuscaAvanzada")){
		String cmb_afiliado			=	(request.getParameter("hid_ic_pyme")==null)?"":request.getParameter("hid_ic_pyme");
		String cmb_banco_fondeo				=	(request.getParameter("hid_ic_banco") == null) ? "":request.getParameter("hid_ic_banco");
		String rfc_prov = request.getParameter("rfc_prov")==null?"":request.getParameter("rfc_prov");
		String txt_ne			=	(request.getParameter("num_pyme") == null) ? "": request.getParameter("num_pyme");
		String txt_nombre			=	(request.getParameter("nombre_pyme") == null) ? "": request.getParameter("nombre_pyme");
		
		System.out.println(ic_epo+"_**_"+cmb_banco_fondeo+"_**_"+rfc_prov+"_**_"+txt_ne+"_**_"+txt_nombre);
		Registros regi = BeanAfiliacion.getProveedores(ic_epo, txt_ne, rfc_prov,txt_nombre);
		List coleccionElementos = new ArrayList();
		JSONArray jsonArr = new JSONArray();
		while (regi.next()){
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(regi.getString("IC_NAFIN_ELECTRONICO"));
			elementoCatalogo.setDescripcion(regi.getString("IC_NAFIN_ELECTRONICO")+" "+regi.getString("CG_RAZON_SOCIAL"));
			coleccionElementos.add(elementoCatalogo);
		}
		Iterator it = coleccionElementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("success", new Boolean(true));
		if (jsonArr.size() == 0){
			jsonObj1.put("total", "0");
			jsonObj1.put("registros", "" );
		}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
			jsonObj1.put("total",  Integer.toString(jsonArr.size()));
			jsonObj1.put("registros", jsonArr.toString() );
		}else if (jsonArr.size() > 1000 ){
			jsonObj1.put("total", "excede" );
			jsonObj1.put("registros", "" );
		}
		infoRegresar = jsonObj1.toString();
	}
%>


<%=infoRegresar%>
