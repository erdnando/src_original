Ext.onReady(function() {
	
	var formDato = {
		numeroNafin : null,
		nombreIf: null,
		estado:null,
		rfc:null
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var muestraDetalle = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var numNafinElec = registro.get('NUMNAFINELECT');
		var ventana = Ext.getCmp('winDetalle');
		if (ventana) {
			ventana.destroy();
		}
		Ext.Ajax.request({url: '15consultaMandantes.data.jsp',	params: Ext.apply({ informacion: 'obtenDetalle',	numNafinElec: numNafinElec }),callback: procesaMuestraDetalle	});
	}

	var procesaMuestraDetalle = function (opts, success, response) {
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			
			if (!Ext.isEmpty(infoR.consultaDet)) {
					new Ext.Window({
						modal: true,
						resizable: false,
						layout: 'form',
						frame: true,
						y: 100,
						width: 640,
						autoHeight:'auto',
						//height: 510,
						id: 'winDetalle',
						closeAction: 'hide',
						title: 'Detalle Consulta de Mandantes',
						bbar: {
							xtype: 'toolbar',
							buttons: ['->','-',{xtype: 'button',text: 'Cerrar',id: 'btnClose',iconCls: 'icoLimpiar',handler: function() {	Ext.getCmp('winDetalle').hide();	}}]
						},
						html:	'	<table width="640" border="0" align="center"> '+
									'<td align="middle"><br>'+
										'<img alt="" border="0" src="../../../00archivos/15cadenas/15archcadenas/logos/nafin.gif"> '+
										'<hr>'+
									'</td>'+
										'<table width="640" align="center">'+
										  '<tr><td colspan="4" bgcolor="silver"><span class="titulos"><font color="#276588">Nombre del Mandante (Persona Moral)</font></span></td></tr>'+
												'<table width="600" align="center">'+
													'<tr>'+
														'<td><span class="formas">Razon Social:</span></td><td><span class="formas">'+infoR.consultaDet.razonsocial+'</span></td>'+
														'<td><span class="formas">No. N@E:</span></td><td><span class="formas">'+infoR.consultaDet.numnafinelect+'</span></td>'+
													'</tr>'+
													'<tr>'+
														'<td colspan="4">&nbsp;</td>'+
													'</tr>'+
													'<tr>'+
														'<td><span class="formas">Nombre Comercial:</span></td><td><span class="formas">'+infoR.consultaDet.nombre_comercial+'</span></td>'+
														'<td><span class="formas">R.F.C.:</span></td><td><span class="formas">'+infoR.consultaDet.rfc+'</span></td>'+
													'</tr>'+
												'</table>'+
										 '</table><br>'+
										//<!-- ................................ -->
										'<table width="640" align="center">'+
											'<tr><td colspan="4" bgcolor="silver"><span class="titulos"><font color="#276588">Domicilio</font></span></td></tr>'+
											'<table width="600" align="center">'+
												'<tr>'+
													'<td><span class="formas">Calle, No. Exterior y No. Interior:</span></td><td><span class="formas">'+infoR.consultaDet.calle+'</span></td>'+
													'<td><span class="formas">Colonia:</span></td><td><span class="formas">'+infoR.consultaDet.colonia+'</span></td>'+
												'</tr>'+
												'<tr>'+
													'<td colspan="4">&nbsp;</td>'+
												'</tr>'+
												'<tr>'+
													'<td><span class="formas">Estado:</span></td><td><span class="formas">'+infoR.consultaDet.estado+'</span></td>'+
													'<td><span class="formas">Delegaci&oacute;n o Municipio:</span></td><td><span class="formas">'+infoR.consultaDet.municipio+'</span></td>'+
												'</tr>'+
												'<tr>'+
													'<td colspan="4">&nbsp;</td>'+
												'</tr>'+
												'<tr>'+
													'<td><span class="formas">C&oacute;digo Postal:</span></td><td><span class="formas">'+infoR.consultaDet.codigo_postal+'</span></td>'+
													'<td><span class="formas">Pa&iacute;s:</span></td><td><span class="formas">'+infoR.consultaDet.pais+'</span></td>'+
												'</tr>'+
												'<tr>'+
													'<td colspan="4">&nbsp;</td>'+
												'</tr>'+
												'<tr>'+
													'<td><span class="formas">Tel&eacute;fono:</span></td><td><span class="formas">'+infoR.consultaDet.telefono+'</span></td>'+
													'<td><span class="formas">Fax:</span></td><td><span class="formas">'+infoR.consultaDet.fax+'</span></td>'+
											'</table>'+
										'</table><br>'+
										//<!-- ................................ -->
										'<table width="640" align="center">'+
											'<tr><td colspan="4" bgcolor="silver"><span class="titulos"><font color="#276588">Datos del contacto</font></span></td></tr>'+
											'<table width="600" align="center">'+
												'<tr>'+
													'<td><span class="formas">Apellido Paterno:</span></td><td><span class="formas">'+infoR.consultaDet.ap_paterno+'</span></td>'+
													'<td><span class="formas">Apellido Materno:</span></td><td><span class="formas">'+infoR.consultaDet.ap_materno+'</span></td>'+
												'</tr>'+
												'<tr>'+
													'<td colspan="4">&nbsp;</td>'+
												'</tr>'+
												'<tr>'+
													'<td><span class="formas">Nombre(s):</span></td><td><span class="formas">'+infoR.consultaDet.nombre_contacto+'</span></td>'+
													'<td><span class="formas">Tel&eacute;fono:</span></td><td><span class="formas">'+infoR.consultaDet.tel_contacto+'</span></td>'+
												'</tr>'+
												'<tr>'+
													'<td colspan="4">&nbsp;</td>'+
												'</tr>'+
												'<tr>'+
													'<td><span class="formas">Fax:</span></td><td><span class="formas">'+infoR.consultaDet.fax_contacto+'</span></td>'+
													'<td><span class="formas">E-mail:</span></td><td><span class="formas">'+infoR.consultaDet.email_contacto+'</span></td>'+
												'</tr>'+
												'<tr>'+
													'<td colspan="4">&nbsp;</td>'+
												'</tr>'+
												'<tr>'+
													'<td><span class="formas">Celular (Clave Lada):</span></td><td><span class="formas">'+infoR.consultaDet.cel_contacto+'</span></td>'+
													'<td><span class="formas">&nbsp;</span></td><td><span class="formas">&nbsp;</span></td>'+
												'</tr>'+
											'</table>'+
									 '</table>'+
									'</table>'
					}).show();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			Ext.getCmp('btnBajarArchivo').hide();
			Ext.getCmp('btnBajarPDF').hide();
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnGenerarPDF').enable();
				el.unmask();
			}else {
				Ext.getCmp('btnGenerarArchivo').disable();
				Ext.getCmp('btnGenerarPDF').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15consultaMandantes.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NUMNAFINELECT'},
			{name: 'RAZONSOCIAL'},
			{name: 'RFC'},
			{name: 'CALLE'},
			{name: 'ESTADO'},
			{name: 'TELEFONO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){Ext.apply(options.params, {numNafinElec:formDato.numeroNafin, nombreMandante:formDato.nombreIf, estado:formDato.estado, rfc:formDato.rfc });} },
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var catalogoEstadosData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consultaMandantes.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstados'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header : 'Numero de Nafin Electr�nico', tooltip: 'Numero de Nafin Electr�nico', dataIndex : 'NUMNAFINELECT',
				sortable: true,	align: 'center',	width: 120,	resizable: true, hidden: false, hideable:false
			},{
				header : 'Nombre o Raz�n Social', tooltip: 'Nombre o Raz�n Social', dataIndex : 'RAZONSOCIAL',
				sortable: true,	align: 'center',	width: 250,	resizable: true, hidden: false, hideable:false
			},{
				header : 'RFC', tooltip: 'RFC', dataIndex : 'RFC',
				sortable: true,	align: 'center',	width: 100,	resizable: true, hidden: false, hideable:false
			},{
				header : 'Domicilio', tooltip: 'Domicilio', dataIndex : 'CALLE',
				sortable: true,	align: 'center',	width: 200,	resizable: true, hidden: false, hideable:false
			},{
				header : 'Estado', tooltip: 'Estado', dataIndex : 'ESTADO',
				sortable: true,	width: 100,	resizable: true, hidden: false, hideable:false
			},{
				header : 'Tel�fono', tooltip: 'Tel�fono', dataIndex : 'TELEFONO',
				sortable: true,	align: 'center',	width: 100,	resizable: true, hidden: false, hideable:false
			},{
				xtype:	'actioncolumn',
				header : 'Detalle', tooltip: 'Detalle',	dataIndex : '',	width:	80,	align: 'center',	hideable:false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('NUMNAFINELECT')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraDetalle
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		colunmWidth: true,
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15consultaMandantes.data.jsp',
							params: {
								numNafinElec:formDato.numeroNafin, 
								nombreMandante:formDato.nombreIf, 
								estado:formDato.estado, 
								rfc:formDato.rfc,
								informacion: 'ArchivoCSV',
								tipo: 'CSV'
								},
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar Archivo',
					//iconCls: 'icoXls',
					id: 'btnBajarArchivo',
					hidden: true
				},'-',{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '15consultaMandantes.data.jsp',
							params: {
								numNafinElec:formDato.numeroNafin, 
								nombreMandante:formDato.nombreIf, 
								estado:formDato.estado, 
								rfc:formDato.rfc,
								informacion: 'ArchivoPDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize,
								tipo: 'PDF'
								},
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar PDF',
					//iconCls: 'icoPdf',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
	});
	var elementosForma = [
		{
			xtype:		'textfield',
			id:			'txtNumero',
			name:			'numNafinElec',
			maxLength:	38,
			anchor:		'70%',
			fieldLabel:	'Numero Naf�n Electr�nico'
		},{
			xtype:		'textfield',
			id:			'txtNombre',
			name:			'nombreMandante',
			maxLength:	100,
			fieldLabel:	'Nombre'
		},{
			xtype:		'combo',
			name:			'estado',
			id:			'cboEstado',
			hiddenName:	'estado',
			fieldLabel:	'Estado',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEstadosData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype:		'textfield',
			id:			'txtRfc',
			name:			'rfc',
			maxLength:	20,
			anchor:		'55%',
			fieldLabel:	'RFC'
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		title:	'Consulta de Mandantes',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		labelWidth: 160,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
				formDato = {numeroNafin : null,nombreIf: null,estado:null}
				//grid.hide();
					fp.el.mask('Enviando...','x-mask-loading');
					formDato.numeroNafin =	Ext.getCmp('txtNumero').getValue();
					formDato.nombreIf		=	Ext.getCmp('txtNombre').getValue();
					formDato.estado		=	Ext.getCmp('cboEstado').getValue();
					formDato.rfc		=	Ext.getCmp('txtRfc').getValue();
					consultaData.load({
							params: Ext.apply({//fp.getForm().getValues(),{
							operacion: 'Generar', //Generar datos para la consulta
							start: 0,
							limit: 15
							})
					});
				} //fin-handler
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});

	catalogoEstadosData.load();

});