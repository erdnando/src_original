<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  			
		com.netro.fianza.*,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	
	
	String numFiado = (request.getParameter("numFiado")!=null)?request.getParameter("numFiado"):""; 
	String nombre = (request.getParameter("nombre")!=null)?request.getParameter("nombre"):""; 
	String rfc = (request.getParameter("claveRFC")!=null)?request.getParameter("claveRFC"):""; 
	String estado = (request.getParameter("estado")!=null)?request.getParameter("estado"):""; 
	String terminosCodiciones = (request.getParameter("terminosCondiciones")!=null)?request.getParameter("terminosCondiciones"):"";

	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	
	//FianzaElectronicaHome fianzaElectronicaHome = (FianzaElectronicaHome)ServiceLocator.getInstance().getEJBHome("FianzaElectronicaEJB", FianzaElectronicaHome.class);
	//FianzaElectronica fianzaElectronicaBean = fianzaElectronicaHome.create();
	FianzaElectronica fianzaElectronicaBean = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB",FianzaElectronica.class);

	ConsFiados paginador = new ConsFiados();
	Vector vdDatos = new Vector();
	Vector vdResult = new Vector();
	vdDatos = null;
	HashMap datos = new HashMap();
if(informacion.equals("valoresIniciales")){
	
	paginador.setNumFiado(numFiado);
	Registros reg = paginador.getInfoModificarFiados();
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta );
	infoRegresar=jsonObj.toString();
}else if(informacion.equals("catalogoSectorEconomico")){
	List catSectorEcon= paginador.getDatosCatalogoSectorEconomicoList();
	jsonObj.put("registros", catSectorEcon);	
   infoRegresar = jsonObj.toString();	
	
 }


else if (informacion.equals("Actualizar")){
	try{
			String nombreFiado = request.getParameter("nombreRazonSocialMod")==null?"":request.getParameter("nombreRazonSocialMod");
			String rfcFiado = request.getParameter("rfcMod")==null?"":request.getParameter("rfcMod");
			String calleFiado = request.getParameter("calleMod")==null?"":request.getParameter("calleMod");
			String noExteriorFiado = request.getParameter("numExteriorMod")==null?"":request.getParameter("numExteriorMod");
			String noIntFiado = request.getParameter("numInteriorMod")==null?"":request.getParameter("numInteriorMod");
			String colFiado = request.getParameter("coloniaMod")==null?"":request.getParameter("coloniaMod");
			String locFiado = request.getParameter("localidadMod")==null?"":request.getParameter("localidadMod");
			String refFiado = request.getParameter("referenciaMod")==null?"":request.getParameter("referenciaMod");
			String municFiado = request.getParameter("municipioMod")==null?"":request.getParameter("municipioMod");
			String estadoFiado = request.getParameter("estadoMod")==null?"":request.getParameter("estadoMod");
			String paisFiado = request.getParameter("paisMod")==null?"":request.getParameter("paisMod");
			String cpFiado = request.getParameter("codigoPostalMod")==null?"":request.getParameter("codigoPostalMod");
			String nombreRL = request.getParameter("nombreRepLegalMod")==null?"":request.getParameter("nombreRepLegalMod");
			String emailRL = request.getParameter("emailRepLegalMod")==null?"":request.getParameter("emailRepLegalMod");
			String telefonoRL = request.getParameter("telefonoRepLegalMMod")==null?"":request.getParameter("telefonoRepLegalMMod");
			String ventFiado = request.getParameter("ventasAnualesMod")==null?"":request.getParameter("ventasAnualesMod");
			String numEmpFiado = request.getParameter("numEmpleadosMod")==null?"":request.getParameter("numEmpleadosMod");
			String sectorEcFiado = request.getParameter("catalogoSectorMod")==null?"":request.getParameter("catalogoSectorMod");
			String estratoFiado = request.getParameter("estratoFiado")==null?"":request.getParameter("estratoFiado");
			String cveEstratoFiado = request.getParameter("cveEstratoFiado")==null?"":request.getParameter("cveEstratoFiado");

	if("Actualizar".equals(informacion)){
		HashMap map = new HashMap();
		map.put("nombreFiado",nombreFiado);
		map.put("rfcFiado",rfcFiado);
		map.put("calleFiado",calleFiado);
		map.put("noExteriorFiado",noExteriorFiado);
		map.put("noIntFiado",noIntFiado);
		map.put("colFiado",colFiado);
		map.put("locFiado",locFiado);
		map.put("refFiado",refFiado);
		map.put("municFiado",municFiado);
		map.put("estadoFiado",estadoFiado);
		map.put("paisFiado",paisFiado);
		map.put("cpFiado",cpFiado);
		map.put("nombreRL",nombreRL);
		map.put("emailRL",emailRL);
		map.put("telefonoRL",telefonoRL);
		map.put("ventFiado",ventFiado);
		map.put("numEmpFiado",numEmpFiado);
		map.put("sectorEcFiado",sectorEcFiado);
		map.put("estratoFiado",cveEstratoFiado);
		
		fianzaElectronicaBean.actuzalizaFiado(numFiado, iNoUsuario, map);
		mensaje="Actualización exitosa";
	}
	
					
		
	} catch(Exception e) { 
		mensaje= e.toString();
		throw new AppException("Error en la afiliacion", e);
		
	}
	
				JSONObject resultado = new JSONObject();
				resultado.put("success", new Boolean(true));
				resultado.put("msg", mensaje);
				infoRegresar = resultado.toString();
} 
 
%>

<%=infoRegresar%>

