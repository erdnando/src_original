<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*, 
				com.netro.procesos.*, 
				netropology.utilerias.*, 
				com.netro.cadenas.*,
				com.netro.afiliacion.*,
				com.netro.model.catalogos.*,   
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%  
String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String descripcion = (request.getParameter("txtDescripcion") !=null)?request.getParameter("txtDescripcion"):"";
String claveGrupo = (request.getParameter("claveGrupo") !=null)?request.getParameter("claveGrupo"):"";
String eposSeleccionadas = (request.getParameter("eposSelect") !=null)?request.getParameter("eposSelect"):"";
String infoRegresar = "";

Afiliacion bean = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

if (informacion.equals("consultaInicial")) {
	List lista = bean.getGrupoEpo();
	JSONArray jsonA= new JSONArray();
	jsonA = JSONArray.fromObject(lista);
	infoRegresar = "{\"success\": true, \"total\": \""	+	jsonA.size() + "\", \"registros\": " + jsonA.toString()+ "}";
	
}else if(informacion.equals("insertarGrupo")){
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if(bean.buscaGrupoRepetido(descripcion) == false){
		if(bean.insertarGrupo(descripcion, iNoUsuario) == true){
			jsonObj.put("textoMensaje","operacion_exitosa");
		}else{
			jsonObj.put("textoMensaje","Error al insertar");
		}
	}else{
		jsonObj.put("textoMensaje","existe");
	}
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("modificarGrupo")){
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if(bean.modificarGrupo(claveGrupo,descripcion, iNoUsuario) == true){
		jsonObj.put("textoMensaje","operacion_exitosa");
	}else{
		jsonObj.put("textoMensaje","Error al modificar");
	}	
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("eliminarGrupo")){
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if(bean.eliminarGrupo(claveGrupo, iNoUsuario) == true){
		jsonObj.put("textoMensaje","operacion_exitosa");
	}else{
		jsonObj.put("textoMensaje","Error al modificar");
	}	
	infoRegresar = jsonObj.toString();	

}else if(informacion.equals("consultaGrupoEPO")){
	CatalogoGrupo cat = new CatalogoGrupo();
	cat.setClave("ic_grupo_epo");
	cat.setDescripcion("cg_descripcion");
	cat.setOrder("cg_descripcion");
	List lista = cat.getListaElementos();
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lista);
	infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";

}else if(informacion.equals("consultaEPO")){
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	if(!claveGrupo.equals("")){
		List listaClave = bean.getEposDeGrupo(claveGrupo);
		cat.setValoresCondicionNotIn(listaClave);
	}
	List lista = cat.getListaElementos();
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lista);
	infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";

}else if(informacion.equals("catalogoEpoGrupo")){

	List listaClave = bean.getEposDeGrupo(claveGrupo);
	List lista= new ArrayList();
	if(listaClave.size()>0)  {
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");	
		cat.setValoresCondicionIn(listaClave);	
		lista = cat.getListaElementos();	
	}
	
	JSONArray registros = new JSONArray();
	registros = registros.fromObject(lista);
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

}else if(informacion.equals("confirmarEpos")){
	String listaEpos[] = eposSeleccionadas.split(",");	
	List listaTemp = bean.insertarRelacionGrupoEpo(listaEpos, claveGrupo, "", iNoUsuario);
	StringBuffer valores = (StringBuffer)listaTemp.get(0);
	
	if(valores.length()>0){
		String listaTemporal[] = valores.toString().split("\n");
		List lista = new ArrayList();
		for(int i = 0; i < listaTemporal.length; i++){
			lista.add(listaTemporal[i]);
		}
		Collections.sort(lista);
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(lista);
		infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"usuario\": \""+ strNombreUsuario + "\", \"grupo\": \""+ listaTemp.get(1) + "\", \"registros\": " + jsObjArray.toString()+ "}";
		System.out.println("RESULTADO:\n" + infoRegresar);
	}
}


%>
<%=  infoRegresar%>