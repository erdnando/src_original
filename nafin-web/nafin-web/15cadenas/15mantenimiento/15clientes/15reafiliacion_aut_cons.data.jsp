<%@ page contentType="application/json;charset=UTF-8" import="   
   java.util.*, java.sql.*,   
	netropology.utilerias.*,   
	com.netro.exception.*,  
	javax.naming.*,   
	com.netro.cadenas.*, 
	java.text.SimpleDateFormat,
	com.netro.model.catalogos.*,  
	net.sf.json.JSONArray,   
	net.sf.json.JSONObject" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<% 
String informacion  =(request.getParameter("informacion")!=null) ? request.getParameter("informacion"):"";
String ic_epo  =(request.getParameter("ic_epo")!=null) ? request.getParameter("ic_epo"):"";
String ic_if  =(request.getParameter("ic_if")!=null) ? request.getParameter("ic_if"):"";
String fechaIni  =(request.getParameter("fechaIni")!=null) ? request.getParameter("fechaIni"):"";
String fechaFin  =(request.getParameter("fechaFin")!=null) ? request.getParameter("fechaFin"):"";


SimpleDateFormat fecha_hoy = new SimpleDateFormat("dd/MM/yyyy");
String fecha_h = fecha_hoy.format(new java.util.Date());

String infoRegresar ="";
JSONObject   jsonObj = new JSONObject(); 

ConsReafiliacion paginador=  new ConsReafiliacion();
paginador.setIc_epo(ic_epo);
paginador.setIc_if(ic_if);
paginador.setFechaIni(fechaIni);
paginador.setFechaFin(fechaFin);
paginador.setPantalla("Consulta");

CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);	
	

if (informacion.equals("valoresIniciales")  ) {	

	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("fecha_hoy", fecha_h);	
	infoRegresar = jsonObj.toString();		
	
}else  if(informacion.equals("catalogoEPO")){

	CatalogoEPO catalogo = new CatalogoEPO();	
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");	
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("catalogoIF")){

	CatalogoIFEpo catalogo = new CatalogoIFEpo();	
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");	
	catalogo.setClaveEpo(ic_epo);	
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("Consultar")){
	
	try {
		Registros reg	=	queryHelper.doSearch();
			
		String 	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj.put("success", new Boolean(true));
		jsonObj = JSONObject.fromObject(consulta);
	}	catch(Exception e) {
		throw new AppException("Error en la paginación", e);
	}
	
	jsonObj.put("fecha_hoy",  fecha_h);
   infoRegresar = jsonObj.toString();

} else if(informacion.equals("GenerarCSV")){
	try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	infoRegresar = jsonObj.toString();
} else if(informacion.equals("GenerarPDF")){

	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}
	infoRegresar = jsonObj.toString();

}

%>

<%=infoRegresar%>