var showPanelLayoutDeCarga;
Ext.onReady(function(){
	 
//----------------------Handlers-----------------------
 var processID=0;
 var sistema ;
 var hayLineasConError;
 var iTotalPymes;
 var iTotalPymesSinError;
 var iTotalPymesConError;
 var iTotalPymesPrevReg;
 var hayLineasConErrorEcon;
 
 var iTotalPymesSinErrorEcon;
 var iTotalPymesConErrorEcon;
 
 var realizarCarga;
 var realizarCargaEcon;
 
 
 var urlArchivo;
 var detalleAMostrar = '';
 
 
 	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
			
				Ext.getCmp('fpResultadosProc').hide(); 
				catalogoProducto.load();
				catalogoPromotoria.load(); 
				catalogoEpo.load({params: {
					cargaEContract: true
				}});
				
		
		
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
 var barraProgreso = new Ext.ProgressBar({
	text:'Iniciando Validaci�n...',
   id:'pbar2',
	cls:'left-align',
	width: 690	
 });
  
	var procesarConsultaRegProc = function(store, arrRegistros, opts) {

		gridResultadoProcNE.setAutoScroll(true);
		var gridColumnMod = gridResultadoProcNE.getColumnModel();		
		if(detalleAMostrar=='N'){
			gridColumnMod.setHidden(7,true);
			gridColumnMod.setHidden(8,true);
		}else if(detalleAMostrar=='E'){
			gridColumnMod.setHidden(5,true);
			gridColumnMod.setHidden(6,true);
		}

	}
	
	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}   
	
	
	function descargarPDF() {		
		var archivo = urlArchivo;				
		archivo = archivo.replace('/nafin','');
		var params = {nombreArchivo: archivo};				
		fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
		fp.getForm().getEl().dom.submit();		
	}
	
	var descargaExcel = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		if(registro.get('ID_RESULTADO')==0){
			Ext.MessageBox.alert('Error','No existen registros que mostrar');
		}else{
			Ext.Ajax.request({
				url: '15admclimasdetallesExt.data.jsp',
				params: Ext.apply({
					informacion:'ConsultarArchivo',
					tipo: registro.get('ID_DESCARGA'),
					procesoID:processID,
					sistema:'econ' 
				}),
				callback: procesarArchivoSuccess
			});
		}
	}
	
	var descargaExcelNE = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		if(registro.get('ID_RESULTADO')==0){
			Ext.MessageBox.alert('Error','No existen registros que mostrar');
		}else{
			Ext.Ajax.request({
				url: '15admclimasdetallesExt.data.jsp',
				params: Ext.apply({
					informacion:'ConsultarArchivo',
					tipo: registro.get('ID_DESCARGA'),
					procesoID:processID,
					sistema:'nae' 
				}),
				callback: procesarArchivoSuccess
			});
		}
	}
	
	function validaFormulario(forma){
		var bandera = true;
      forma.getForm().items.each(function(f){
			if(f.xtype != 'label'&&f.xtype != undefined&&bandera){
				if(f.isVisible()){
					if(!f.isValid()){
						f.focus();
						f.markInvalid('Este campo es Obligatorio');
						bandera= false;
					}
				}
			}
		});
      return bandera;
   }
	  
	function validaThread(){
		Ext.Ajax.request({
			url: '15forma01MasExt.data.jsp',
			params: Ext.apply(fpDatos.getForm().getValues(),{
				informacion: 'validaThread'
			}),
			callback: procesarAlta
		});	
		fpDatos.setVisible(false);	
		fp.setVisible(true);
		fp.add(barraProgreso);
		fp.doLayout();
	}	 

	function procesarResultados(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			respuesta=Ext.util.JSON.decode(response.responseText);
				
				fp.setVisible(false);
				fpResultados.setVisible(true);
				fpResultados.setTitle('Resumen de validaci�n de carga masiva de proveedores<br/>');
				Ext.getCmp('panelResultados').setTitle('N&uacute;mero de registros del archivo: '+respuesta.iTotalPymes);
				
				storeResultadoNE.loadData(respuesta.regsNE);
				storeResultadoEco.loadData(respuesta.regsECO);
				hayLineasConError=respuesta.hayLineasConError;
				iTotalPymes=respuesta.iTotalPymes;
				iTotalPymesSinError=respuesta.iTotalPymesSinError;
				iTotalPymesConError=respuesta.iTotalPymesConError;
				iTotalPymesPrevReg=respuesta.iTotalPymesPrevReg;
				hayLineasConErrorEcon=respuesta.hayLineasConErrorEcon;
				iTotalPymesSinErrorEcon=respuesta.iTotalPymesSinErrorEcon;
				iTotalPymesConErrorEcon=respuesta.iTotalPymesConErrorEcon;
				realizarCargaEcon=respuesta.realizarCargaEcon;
				realizarCarga=respuesta.realizarCarga;
				
				fpResultados.setVisible(true);
				
				if (!Ext.getCmp('id_cargaEContract').getValue()){
					gridResultadoEco.setVisible(false);
				}
				if (!Ext.getCmp('id_cargaNE').getValue()){
					gridResultadoNE.setVisible(false);
				}
				
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	function realizarCargaRespuesta(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			respuesta=Ext.util.JSON.decode(response.responseText);
	
			detalleAMostrar = respuesta.regAMostrar;
			storeResultadoEcoProc.loadData(respuesta.listaEcon);
			storeResultadoNEProc.loadData(respuesta.listaNaf);
			
				
			Ext.getCmp('registrosArchivo').update(respuesta.iTotalPymes);
			Ext.getCmp('registrosProcesadosEco').update(respuesta.RegsEContract);
			Ext.getCmp('registrosArchivoNE').update(respuesta.iTotalPymes);
			Ext.getCmp('registrosProcesadosNE').update(respuesta.iTotalPymesSinError);
			Ext.getCmp('registrosErrNE').update(respuesta.RegsError);
			urlArchivo = respuesta.urlArchivo;
		
			fpResultados.setVisible(false);
			fpResultadosProc.setVisible(true); 
			
			Ext.getCmp('menuToolbar').hide();
			
			if (Ext.getCmp('id_cargaEContract').getValue()){				
				Ext.getCmp('divResultadosProcECO').show();
				//Ext.getCmp('divResultadosProcECO1').show();
			}
			if (Ext.getCmp('id_cargaNE').getValue()){				
				Ext.getCmp('divResultadosProcNE').show();
			}
			Ext.getCmp('gridResultadoProcNE').show();
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarAlta(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			respuesta=Ext.util.JSON.decode(response.responseText);
			var porcentaje = 0;
			porcentaje = parseFloat(respuesta.numeroRegsProc)/parseFloat(respuesta.numeroRegs);
			porcentaje = Math.round(porcentaje * Math.pow(10, 1)) / Math.pow(10, 1);
			if(!respuesta.isCompleted&&!respuesta.hasError){
				Ext.getCmp('registrosProcesados').setValue(respuesta.numeroRegsProc);
				Ext.getCmp('registrosTotal').setValue(respuesta.numeroRegs);
				barraProgreso.updateProgress(porcentaje,'Procesando...');
				Ext.Ajax.request({
					url: '15forma01MasExt.data.jsp',
					params: Ext.apply(fpDatos.getForm().getValues(),{
						informacion: 'validaThread'
					}),
					callback: procesarAlta
				});
			}else{
				Ext.getCmp('registrosProcesados').setValue(respuesta.numeroRegsProc);
				Ext.getCmp('registrosTotal').setValue(respuesta.numeroRegs);
				barraProgreso.updateProgress(porcentaje,'Terminado');
				processID=respuesta.processID;
				Ext.Ajax.request({
					url: '15forma01MasExt.data.jsp',
					params: Ext.apply({
						informacion: 'Resultados'
					}),
					callback: procesarResultados
				});					
			}			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	function mostrarArchivoTXT(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fpDatos.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpDatos.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesaCargaMasiva = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaMasiva(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaMasiva(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	
	var cargaMasiva = function(estado, respuesta ){
		
		if(			estado == "SUBIR_ARCHIVO"					){
			
			if (Ext.getCmp('idAsociar').getValue()==''){					
						Ext.Msg.confirm('', '�Esta seguro que desea llevar a cabo la carga sin asociarle un Folio?',	function(botonConf) {
							if (botonConf == 'ok' || botonConf == 'yes') {
								var params = (fpDatos)?fpDatos.getForm().getValues():{};
								fpDatos.getForm().submit({
									clientValidation: 	true,
									url: 						'15forma01MasExt.data.jsp?informacion=AfiliacionSave',								
									success: function(form, action) {
										 procesaCargaMasiva(null,  true,  action.response );
									}								
								});							
							}
						});	
					}else{
						var params = (fpDatos)?fpDatos.getForm().getValues():{};				
								
						fpDatos.getForm().submit({
							clientValidation: 	true,
							url: 						'15forma01MasExt.data.jsp?informacion=AfiliacionSave',								
							success: function(form, action) {
								 procesaCargaMasiva(null,  true,  action.response );
							},
							failure: function(form, action) {								
								Ext.MessageBox.alert('Error',action.result.mensaje!=undefined?action.result.mensaje:action.result.msg);
							}
						});							
					}
			
		}else if(	estado == "VALIDA_CARACTERES_CONTROL"				){
	     
			var nombreArchivo	= respuesta.nombreArchivo;
			var cboProducto	= respuesta.cboProducto;
			var cboPromotoria	= respuesta.cboPromotoria;
			var folioArchivo	= respuesta.folioArchivo;
			var Epo1	= respuesta.Epo;
			var cboTipoAfiliacion	= respuesta.cboTipoAfiliacion;
			
			Ext.getCmp('nombreArchivo').setValue(nombreArchivo);
			Ext.getCmp('cboProducto').setValue(cboProducto);
			Ext.getCmp('cboPromotoria').setValue(cboPromotoria);
		
			Ext.getCmp('folioArchivo').setValue(folioArchivo);
			Ext.getCmp('Epo1').setValue(Epo1);
			Ext.getCmp('cboTipoAfiliacion').setValue(cboTipoAfiliacion);
			Ext.Ajax.request({
				url: 		'15forma01MasExt.data.jsp',
				params: 	{
					informacion:				'CargaMasiva.validaCaracteresControl',
					nombreArchivo: 			nombreArchivo
				},
				callback: 				procesaCargaMasiva
			});
		
		}else if(	estado == "MOSTRAR_AVANCE_THREAD"				){
			Ext.Ajax.request({
				url: 		'15forma01MasExt.data.jsp',
				params: 	{
					informacion:				'CargaMasiva.avanceThreadCaracteres'
				},
				callback: 				procesaCargaMasiva
			});
		
		}else if(	estado == "HAY_CARACTERES_CONTROL"				){
				var mensaje	= respuesta.mns;
				var nombreArchivoCaractEsp	= respuesta.nombreArchivoCaractEsp;
				Ext.getCmp('nombArchivoDetalle').setValue(nombreArchivoCaractEsp);
				fpDatos.hide();
				menuToolbar.hide();
				caracteresEspeciales.show();
				caracteresEspeciales.setMensaje(mensaje);
				 
		}else if(	estado == "ERROR_THREAD_CARACTERES"				){
				var mensaje	= respuesta.mns;
				Ext.Msg.alert("Error", mensaje );
				return;
		}else if(	estado == "DESCARGAR_ARCHIVO"				){
		
			var archivo = Ext.getCmp('nombArchivoDetalle').getValue();
			Ext.Ajax.request({
				url: 		'15forma01MasExt.data.jsp',
				params: 	{
					informacion:				'descargaArchivoDetalle',
					archivo : archivo
				},
				callback: 				mostrarArchivoTXT
			});
		} else if(	estado == "REVISAR_VALIDACION_DOCTO"				){
			
			Ext.Ajax.request({
				url: 		'15forma01MasExt.data.jsp',
				params: 	{
					informacion:				'CargaMasiva.revisaValidacionDocto',
					nombreArchivo:				Ext.getCmp('nombreArchivo').getValue(),
					cboProducto:				Ext.getCmp('cboProducto').getValue(),
					cboPromotoria:				Ext.getCmp('cboPromotoria').getValue(),
					valCargaNaf:				Ext.getCmp('id_cargaNE').getValue(),
					valCargaEco:				Ext.getCmp('id_cargaEContract').getValue(),
					valCruce:				Ext.getCmp('id_cruceDB').getValue(),
					folioArchivo:				Ext.getCmp('folioArchivo').getValue(),
					
					Epo1:				Ext.getCmp('Epo1').getValue(),
					cboTipoAfiliacion:				Ext.getCmp('cboTipoAfiliacion').getValue()
					
				},
				callback: 				procesaCargaMasiva
			});
		
		} else if(	estado == "VALIDA_THREAD"				){
			 
			validaThread();
		
		} 
		
		return;
		
	}

//----------------------Stores--------------------

	
	var catalogoPersona = new Ext.data.ArrayStore({
		fields: ['clave',  'descripcion' ],
        data: [
			[1, 'F�sica'],
			[5, 'Moral']
		  ]
    });
	 
	 var catalogoEpo = new Ext.data.JsonStore  ({
		id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15forma01MasExt.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		autoLoad: false,
		listeners:{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
	var catalogoFolio = new Ext.data.JsonStore   ({
	   id: 'catalogoFolio',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15forma01MasExt.data.jsp',
		baseParams:	{
		 informacion: 'catalogoFolio'
		},
		autoLoad: false,
		listeners:		{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoProducto = new Ext.data.JsonStore   ({
	   id: 'catalogoProducto',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15forma01MasExt.data.jsp',
		baseParams: 	{
		 informacion: 'catalogoProductos'
		},
		autoLoad: false,
		listeners:		{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function (){
				var recordSelected =catalogoProducto.getAt(0);    				
				Ext.getCmp('idProducto').setValue(recordSelected.get('clave'));
			}
		}
  });

	var catalogoPromotoria = new Ext.data.JsonStore   ({
	   id: 'catalogoPromotoria',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15forma01MasExt.data.jsp',
		baseParams: {
		 informacion: 'catalogoPromotoria'
		},
		autoLoad: false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function (){
				if(catalogoPromotoria.getCount()>0){
					var recordSelected =catalogoPromotoria.getAt(0);                     
					Ext.getCmp('idPromotoria').setValue(recordSelected.get('clave'));
				}
			}
		}
  });
	
	var storeResultadoNE = new Ext.data.JsonStore({
		fields: [
			{name:'ID_RESULTADO'},
			{name:'CG_DESCRIPCION'},
			{name:'ID_DESCARGA'}
		],
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var storeResultadoEco = new Ext.data.JsonStore({
		fields: [
			{name:'ID_RESULTADO'},
			{name:'CG_DESCRIPCION'},
			{name:'ID_DESCARGA'}
			],
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeResultadoNEProc = new Ext.data.JsonStore({
		fields: [
			{name:'PROVEEDOR'},
			{name:'NAF_ELEF'},
			{name:'RAZON_SOCIAL'},
			{name:'RFC'},
			{name:'OPERACIONES'},
			{name:'CS_ERROR_NE'},
			{name:'DET_ERROR_NE'},
			{name:'CS_ERROR_EC'},
			{name:'DET_ERROR_EC'}
			],
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			load: procesarConsultaRegProc
		}
	});

	var storeResultadoEcoProc = new Ext.data.JsonStore({
		fields: [
			{name:'PROCESO'},
			{name:'NOMBRE_ARCHIVO'},
			{name:'FECHA_CARGA'},
			{name:'FOLIO'}			
			],
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	//Fin Stores
	
	var elementosProcesando = [
		{
			xtype: 'displayfield',
			value: '',
			id: 'registrosTotal',
			fieldLabel: 'N�mero total de registros',
			width: 180
		},
		{
			xtype: 'displayfield',
			value: '',
			id: 'registrosProcesados',
			fieldLabel: 'Registros Procesados',
			width: 180
		}
	];

/*
	var elementosProcesadoEco = {
		xtype			: 'fieldset',
		id 			: 'elementosProcesadoEco',
		border		: false,
		labelWidth	: 300,
		heigth:	800,
		items			:  [
			{
				xtype: 'panel',
				id: 'tituloEco',
				title: 'Resumen Proceso de Carga Masiva Proveedores EContract',
				width		: 700
			},
			{
				xtype: 'displayfield',				
				id: 'registrosArchivo',
				fieldLabel: 'N�mero de registros del archivo',
				style: 'text-align:left;',				
				text: '-',
				width: 300
			},
			{
				xtype: 'displayfield',
				value: '',
				id: 'registrosProcesadosEco',
				fieldLabel: 'N�mero de registros a ser procesados por Econtract',
				width: 300
			}
		]
	};*/

	/*var elementosProcesadoNE = {
		xtype			: 'fieldset',
		id 			: 'elementosProcesadoNE',
		border		: false,
		labelWidth	: 300,
		heigth:	800,
		items			:  [
			{
				xtype: 'panel',
				title: 'Resumen Proceso de Carga Masiva Proveedores N@E',
				width		: 700
			},
			{
				xtype: 'displayfield',
				value: '',
				id: 'registrosArchivoNE',
				fieldLabel: 'N�mero de registros del archivo',
				width: 300
			},
			{
				xtype: 'displayfield',
				value: '',
				id: 'registrosProcesadosNE',
				fieldLabel: 'N�mero de registros a ser procesados',
				width: 300
			},
			{
				xtype: 'displayfield',
				value: '',
				id: 'registrosErrNE',
				fieldLabel: 'N�mero de registros para el archivo de errores',
				width: 300
			}
		]
	};*/
	
	var elementosForma = [
		{
			xtype		: 'panel',
			id 		: 'afiliacion',
			layout	: 'hbox',
			title		: 'Seleccione Tipo(s) de Carga: 	',
			border	: false,		
			width		: 900,		
			bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:110px;',
			items		: 	[
				{
					layout		: 'form',
					width			:600,
					labelWidth	: 200,
					border		: false,
					items			:[
						{
							xtype			: 'checkbox',
							id				: 'id_cargaNE',
							name			: 'valCargaNaf',
							hiddenName	: 'valCargaNaf',
							fieldLabel	: 'CARGA A N@E',
							checked		: true,
							enable 		: true,
							listeners: {
								check: function(check,isCheck){
									if (!Ext.getCmp('id_cruceDB').getValue()){
										check.setValue(true);
									}
									
								}
							}
						},
						{
							xtype			: 'checkbox',
							id				: 'id_cruceDB',
							name			: 'valCruce',
							hiddenName	: 'valCruce',
							fieldLabel	: 'CRUCE DE BD',
							checked		: true,
							enable 		: true,
							listeners: {
								check: function(check,isCheck){
									if (Ext.getCmp('id_cargaNE').getValue()){
									
									}else{
										if(!isCheck){
											Ext.getCmp('id_cargaNE').setValue(true);
										}
									}
									if(isCheck){
										Ext.getCmp('panelDist').setVisible(true);
										Ext.getCmp('idProducto').setVisible(true);								
									}else{
										if(!Ext.getCmp('id_cargaEContract').getValue()){
											Ext.getCmp('panelDist').setVisible(false);
											Ext.getCmp('idProducto').setVisible(false);
										}
									}
								}
							}
						},						
						{
							xtype			: 'checkbox',
							id				: 'id_cargaEContract',
							name			: 'valCargaEco',
							hiddenName	: 'valCargaEco',
							fieldLabel	: 'CARGA A ECONTRACT',
							hidden		: false,
							checked		: true,
							enable 		: true,
							listeners: {
								check: function(check,isCheck){
									catalogoEpo.load({params: {
										cargaEContract: isCheck
									}});									
									if(isCheck){
										Ext.getCmp('panelDist').setVisible(true);
										Ext.getCmp('idProducto').setVisible(true);
										Ext.getCmp('idPromotoria').setVisible(true);
										Ext.getCmp('opciones').setVisible(true);
										Ext.getCmp('id_Actualizacion').setVisible(true);
										//Ext.getCmp('valDirectProm_1').setVisible(true);
										Ext.getCmp('valDirectProm_12').setVisible(true);
                                                                                Ext.getCmp('valDirectProm_12').setValue("");
                                                                                Ext.getCmp('valDirectProm_1').setValue("");
                                                                                //Ext.getCmp('valCruzada_1').setVisible(true);
                                                                                Ext.getCmp('valCruzada_12').setVisible(true);
                                                                                Ext.getCmp('valCruzada_12').setValue(true);
                                                                                Ext.getCmp('valCruzada_1').setValue("on");
										Ext.getCmp('idPromotoria').allowBlank=false;										
									}else{
										if (!Ext.getCmp('id_cruceDB').getValue()){
											Ext.getCmp('panelDist').setVisible(false);
											Ext.getCmp('idProducto').setVisible(false);
										}
										Ext.getCmp('idPromotoria').setVisible(false);
										Ext.getCmp('opciones').setVisible(false);
										Ext.getCmp('id_Actualizacion').setVisible(false);
										Ext.getCmp('idPromotoria').allowBlank=true;
										//Ext.getCmp('valDirectProm_1').setVisible(false);
										Ext.getCmp('valDirectProm_12').setVisible(false);
                                                                                Ext.getCmp('valDirectProm_12').setValue(false);
                                                                                Ext.getCmp('valDirectProm_1').setValue("");
                                                                                //Ext.getCmp('valCruzada_1').setVisible(false);										
										Ext.getCmp('valCruzada_12').setVisible(false);
                                                                                Ext.getCmp('valCruzada_12').setValue(false);
                                                                                //Ext.getCmp('valCruzada_1').setValue(false);
                                                                                Ext.getCmp('valCruzada_1').setValue("");
									
									}										
								}
							}
					},
					{  
					  xtype:	'hidden',  
					  name:	'nombreArchivo',
					  id: 	'nombreArchivo'
					},
					{  
					  xtype:	'hidden',  
					  name:	'cboProducto',
					  id: 	'cboProducto'
					},
					{  
					  xtype:	'hidden',  
					  name:	'cboPromotoria',
					  id: 	'cboPromotoria'
					},					
					{  
					  xtype:	'hidden',  
					  name:	'folioArchivo',
					  id: 	'folioArchivo'
					},
					{  
					  xtype:	'hidden',  
					  name:	'nombArchivoDetalle',
					  id: 	'nombArchivoDetalle'
					},
					{  
					  xtype:	'hidden',  
					  name:	'Epo1',
					  id: 	'Epo1'
					},
					{  
					  xtype:	'hidden',  
					  name:	'cboTipoAfiliacion',
					  id: 	'cboTipoAfiliacion'
					}
				]
			}
		]
	},
	{
		xtype: 'combo',
		fieldLabel: '* Cadenas Productivas',
		emptyText: 'Seleccionar una EPO',
		displayField: 'descripcion',
		allowBlank: false,
		anchor: '70%',
		valueField: 'clave',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		store: catalogoEpo,
		tpl: NE.util.templateMensajeCargaCombo,
		name:'Epo',
		id: 'icEpo',
		mode: 'local',
		hiddenName: 'Epo',
		forceSelection: true,
		listeners: {
			select: function(combo, record, index) {
				Ext.getCmp('idAsociar').reset();
				catalogoFolio.load({
				params : Ext.apply({
					epo : record.json.clave
				})
				});
				if (!Ext.isEmpty(Ext.getCmp('idProducto').getValue() ) && !Ext.isEmpty(Ext.getCmp('icEpo').getValue() ) ){	
					catalogoPromotoria.load({
						params : Ext.apply({
							epo : record.json.clave,
							producto: Ext.getCmp('idProducto').getValue()
						})
					});
				}
			}
		}
	},
	{
		xtype: 'combo',
		fieldLabel: 'Asociar a Folio',
		emptyText: 'Seleccionar ...',
		displayField: 'descripcion',
		anchor: '40%',
		valueField: 'clave',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		store: catalogoFolio,
		tpl: NE.util.templateMensajeCargaCombo,
		name:'folioArchivo',
		id: 'idAsociar',
		mode: 'local',
		hiddenName: 'folioArchivo',
		forceSelection: true
	},
	{
		xtype		: 'panel',
		title		: 'Distribuci�n de Solicitudes:',
		id: 'panelDist'
	},
	{
		xtype: 'combo',
		fieldLabel: '* Producto',
		emptyText: 'Seleccionar ...',
		displayField: 'descripcion',
		allowBlank: false,
		anchor: '60%',
		valueField: 'clave',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		store: catalogoProducto,
		tpl: NE.util.templateMensajeCargaCombo,
		name:'cboProducto',
		id: 'idProducto',
		mode: 'local',
		hiddenName: 'cboProducto',
		forceSelection: true,
		listeners: {
			select: function(combo, record, index) {
				if (!Ext.isEmpty(Ext.getCmp('idProducto').getValue() ) && !Ext.isEmpty(Ext.getCmp('icEpo').getValue() ) ){	
					catalogoPromotoria.load({
						params : Ext.apply({
							epo : Ext.getCmp('icEpo').getValue(),
							producto: Ext.getCmp('idProducto').getValue()
						})
					});
				}	
			}
		}
	},
	{
		xtype: 'combo',
		fieldLabel: '* Promotorias',
		emptyText: 'Seleccionar ...',
		displayField: 'descripcion',
		allowBlank: false,
		anchor: '60%',
		valueField: 'clave',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		store: catalogoPromotoria,
		tpl: NE.util.templateMensajeCargaCombo,
		name:'cboPromotoria',
		id: 'idPromotoria',
		mode: 'local',
		hiddenName: 'cboPromotoria',
		forceSelection: true
	},
	{
		xtype		: 'panel',
		title		: 'Opciones: ',
		id: 'opciones'
	},
	{
		xtype			: 'checkbox',
		id				: 'id_Actualizacion',
		name			: 'valActualizaDat',
		hiddenName	: 'valActualizaDat',
		fieldLabel	: 'Actualizaci�n de Datos',
		checked		: false,
		enable 		: true
	},
	{                                                                                              
                        xtype: 'textfield',
                        hidden: true,
                        id				: 'valDirectProm_1',
                        name			: 'valDirectProm'
         },
         {                                                                                              
                        xtype: 'textfield',
                        hidden: true,
                        id				: 'valCruzada_1',
                        name			: 'valCruzada',
                        value                   : 'on'
         },
        /*{
		xtype			: 'checkbox',
		id				: 'valDirectProm_1',
		name			: 'valDirectProm',
		hiddenName	: 'valDirectProm',
		fieldLabel	: 'Directo a Promotor�a',
		checked		: false,
                hidden          : false,
		enable 		: true,
		listeners: {
			check: function() {			
				Ext.getCmp('valCruzada_1').setValue(false);	
			}
		}
	},
	{
		xtype			: 'checkbox',
		id				: 'valCruzada_1',
		name			: 'valCruzada',
		hiddenName	: 'valCruzada',
		fieldLabel	: 'Venta Cruzada',
	 	hidden		: false,
		checked		: true,
		enable 		: true,
		listeners: {
			check: function() {			
				Ext.getCmp('valDirectProm_1').setValue(false);	
			}
		}
	},*/
        {
                    xtype: 'radio',
                    id: 'valDirectProm_12',
                    hiddenName	: 'valDirectProm2',
                    fieldLabel	: 'Directo a Promotor�a',
                    name: 'rbGroup',
                    checked: false,
                    hideLabel: false,
                    value    : 1,
                    listeners: {
			check: function(object) {			
				if(object.checked){
                                    Ext.getCmp('valDirectProm_1').setValue("on");
                                    Ext.getCmp('valCruzada_1').setValue("");                                    
                                }
			}
		}
                }, 
            {
                    xtype: 'radio',
                    id: 'valCruzada_12',
                    fieldLabel	: 'Venta Cruzada',
                    hiddenName	: 'valCruzada2',
                    name: 'rbGroup',
                    checked: true,
                    hideLabel: false,
                    value  : 2,
                    listeners: {
			check: function(object) {
                                if(object.checked){
                                   Ext.getCmp('valCruzada_1').setValue("on");	
                                   Ext.getCmp('valDirectProm_1').setValue("");
                                }
			}
		}
                },        
	{
		xtype: 		'fileuploadfield',
		id: 			'archivo',
		name: 			'archivo',
		emptyText: 	'Seleccione...',
		fieldLabel: 	"Carga Archivo de", 
		buttonText: 	null,
		buttonCfg: {
			iconCls: 	'upload-icon'
		},
		anchor: 		'-20'					  
	}
];

	//---------------------------- 3. PANEL LAYOUT DE CARGA ------------------------------
	
	showPanelLayoutDeCarga = function(){
		Ext.getCmp('panelLayoutDeCarga').show();
		Ext.getCmp('espaciadorPanelLayoutDeCarga').show();
	}
	
	var hidePanelLayoutDeCarga = function(){
		Ext.getCmp('panelLayoutDeCarga').hide();
		Ext.getCmp('espaciadorPanelLayoutDeCarga').hide();
	}
	
	var elementosLayoutDeCarga = [
		{
			xtype: 	'label',
			id:		'labelLayoutCarga',
			name:		'labelLayoutCarga',
			html: 	"<div align='left' class='formas' style='font-weight:bold; font-size:12pt; color:#000088;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Carga Masiva</div><BR>" +
					"&nbsp;<BR>"+
					"<table><tr><td align='center'><table cellpadding='2' cellspacing='0' border='1' bordercolor='#A5B8BF' style='background:#FFFFFF; aling ='center'>" +
					"<tr><td class='celda01' align='left'><strong>N�mero de<BR>Campo</strong></td><td class='celda01' align='center'><strong>Descripci�n</strong></td>" +
					"<td class='celda01' align='left'><strong>Tipo de<BR>Dato</strong></td><td class='celda01' align='center'><strong>Longitud</strong></td>" +
					"<td class='celda01' align='center'><strong>Obligatorios N@E</strong></td> <td class='celda01' align='center'><strong>Obligatorios Econtract</strong></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>1</font></td><td class='formas'> N�mero de proveedor (poner NA si no se tiene)</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>25</td>"+
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>2</font></td><td class='formas'> RFC</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>20</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>3</font></td><td class='formas'>Apellido Paterno (S�lo para personas f�sicas)</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>30</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>4</font></td><td class='formas'>Apellido Materno (S�lo para personas f�sicas)</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>25</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>5</font></td><td class='formas'> Nombre (S�lo para personas f�sicas)</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>40</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>6</font></td><td class='formas'> Raz�n Social (S�lo para personas morales)</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>100</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>7</font></td><td class='formas'> Calle y n�mero que corresponde al domicilio fiscal</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>100</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>8</font></td><td class='formas'> Colonia que corresponde al domicilio fiscal</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>100</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>9</font></td><td class='formas'> Estado que corresponde al domicilio fiscal</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>100</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>10</font></td><td class='formas'> Pa�s que corresponde al domicilio fiscal</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>100</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>11</font></td><td class='formas'> Municipio que corresponde al domicilio fiscal</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>100</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>12</font></td><td class='formas'> C�digo Postal que corresponde al domicilio fiscal</td>" +
					"<td class='formas' align='left'>Num�rico</td><td class='formas' align='center'>5</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>13</font></td><td class='formas'> Tel�fono que corresponde al domicilio fiscal</td>" +
					"<td class='formas' align='left'>Num�rico</td><td class='formas' align='center'>30</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>14</font></td><td class='formas'>Fax que corresponde al domicilio fiscal</td>" +
					"<td class='formas' align='left'>Num�rico</td><td class='formas' align='center'>30</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>15</font></td><td class='formas'>E-mail que corresponde al domicilio fiscal</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>100</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>16</font></td><td class='formas'> Apellido Paterno del contacto</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>25</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>17</font></td><td class='formas'> Apellido Materno del contacto</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>25</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>18</font></td><td class='formas'> Nombre del contacto</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>80</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>19</font></td><td class='formas'> Tel�fono del contacto</td>" +
					"<td class='formas' align='left'>Num�rico</td><td class='formas' align='center'>30</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>20</font></td><td class='formas'>Fax del contacto</td>" +
					"<td class='formas' align='left'>Num�rico</td><td class='formas' align='center'>30</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>21</font></td><td class='formas'> E-mail del contacto</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>100</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>22</font></td><td class='formas'>Tipo de cliente</td>" +
					"<td class='formas' align='left'>Num�rico</td><td class='formas' align='center'>1</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>23</font></td><td class='formas'> Indicador de susceptibilidad a descuento</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>1</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>24</font></td><td class='formas'> Generar clave de consulta</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>1</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/correcto.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"<tr><td class='formas'><font color='#0000ff'>25</font></td><td class='formas'> * Operaci&oacute;n (B=Baja)</td>" +
					"<td class='formas' align='center'>Alfanum�rico</td><td class='formas' align='center'>15</td>" +
					"<td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td><td class='formas' align='center'><img src='/nafin/00utils/gif/tache.gif' border='0'></td></tr>" +

					"</table></td></tr>" +
					"<td class='formas'>* Aplica &uacute;nicamente para dar de baja pymes</td>",
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginBottom: 	'10px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		}
	];
	
	var panelLayoutDeCarga = {
		xtype:			'panel',
		id: 				'panelLayoutDeCarga',
		hidden:			true,
		width: 			700,
		title: 			'Descripci�n del Layout de Carga',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosLayoutDeCarga,
		tools: [
			{
				id:			'close',
				handler: function(event, toolEl, panel){
					hidePanelLayoutDeCarga();
				}
    		}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      )
	};
		
	var espaciadorPanelLayoutDeCarga = {
		xtype: 	'box',
		id:		'espaciadorPanelLayoutDeCarga',
		hidden:	true,
		height: 	10,
		width: 	800
	};
	
	
	//Grids de resultados de Validacion 
	
	
	var gridResultadoNE = new Ext.grid.GridPanel({
		id: 'gridNE',
		hidden: false,
		header: true,
		title: '<center>Resumen N@E</center>',
		store: storeResultadoNE,				
		height: 135,
		width: 440,				
		frame: false,
		columns:[
		//CAMPOS DEL GRID
			{
				dataIndex: 'CG_DESCRIPCION',
				width: 220,
				align: 'center'						
			},
			{
				dataIndex: 'ID_RESULTADO',
				width: 90,
				align: 'center'
			},
			{
				xtype: 'actioncolumn',						
				width: 90,
				align: 'center',
				items: [
					{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						this.items[0].tooltip = 'Descargar';	
							sistema = 'nae';
							return 'icoXls';	
						}
					,handler: descargaExcelNE
					}
				]	
			}
		]	
	});
		
	var gridResultadoEco = new Ext.grid.GridPanel({
		id: 'gridEco',
		hidden: false,
		header: true,
		title: '<center>Resumen EContract</center>',
		store: storeResultadoEco,				
		height: 135,
		width: 430,				
		frame: false,
		columns:[
			//CAMPOS DEL GRID						
			{
				dataIndex: 'CG_DESCRIPCION',
				width: 220,
				align: 'center'
			},
			{
				dataIndex: 'ID_RESULTADO',
				width: 90,
				align: 'center'						
			},
			{
				xtype: 'actioncolumn',						
				width: 90,
				align: 'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Descargar';		
							sistema = 'econ';
							return 'icoXls';	
						}
						,handler: descargaExcel
					}
				]
			}
		]				
	});
		
	var gridResultadoProcEco = new Ext.grid.GridPanel({
		id: 'gridResultadoProcEco',
		hidden: false,
		store: storeResultadoEcoProc,					
		width: 680,			
		frame: false,
		title: '',
		height: 110,
		columns:[
			//CAMPOS DEL GRID
			{
				dataIndex: 'PROCESO',
				width: 130,
				header: 		'N�mero Proceso',
				tooltip: 	'N�mero Proceso',
				align: 'center'						
			},
			{
				dataIndex: 'NOMBRE_ARCHIVO',
				width: 230,
				header: 		'Nombre Archivo',
				tooltip: 	'Nombre Archivo',
				align: 'center'						
			},
			{
				dataIndex: 'FECHA_CARGA',
				width: 150,
				header: 		'Fecha Carga de Archivo',
				tooltip: 	'Fecha Carga de Archivo',
				align: 'center'						
			},
			{
				dataIndex: 'FOLIO',
				width: 120,
				header: 		'N�mero Folio',
				tooltip: 	'N�mero Folio',
				align: 'center'						
			}				
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		align: 'center'
	});
		
	var gridResultadoProcNE = new Ext.grid.GridPanel({
		id: 'gridResultadoProcNE',
		hidden: false,				
		title: '<center>Detalle de carga masiva afiliaci�n Pyme</center>',
		store: storeResultadoNEProc,		
		width: 680,				
		frame: false,
		height: 150,
		columns:[
			//CAMPOS DEL GRID
			{
				dataIndex: 'PROVEEDOR',
				width: 120,
				header: 		'N�mero de proveedor',
				tooltip: 	'N�mero de proveedor',
				align: 'center'
			},
			{
				dataIndex: 'NAF_ELEF',
				width: 120,
				header: 		'N�mero Nafin Electr�nico',
				tooltip: 	'N�mero Nafin Electr�nico',
				align: 'center'
			},
			{
				dataIndex: 'RAZON_SOCIAL',
				width: 200,
				header: 		'Nombre completo o Raz�n social',
				tooltip: 	'Nombre completo o Raz�n social',
				align: 'center'
			},
			{
				dataIndex: 'RFC',
				width: 120,
				header: 		'RFC',
				tooltip: 	'RFC',
				align: 'center'
			},
			{						
				dataIndex: 'OPERACIONES',
				width: 120,
				header: 		'Operaci�n',
				tooltip: 	'Operaci�n',
				align: 'center'
			},
			{
				dataIndex: 'CS_ERROR_NE',
				width: 120,
				header: 		'Con error N@E',
				tooltip: 	'Con error N@E',
				align: 'center'
			},
			{
				dataIndex: 'DET_ERROR_NE',
				width: 200,
				header: 		'Detalle error N@E',
				tooltip: 	'Detalle error N@E',
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa.replace(/\"/gi,"").replace(/\,/gi," ")) + '"';
					return causa;
				}
			},
			{
				dataIndex: 'CS_ERROR_EC',
				width: 120,
				header: 		'Con error EContract',
				tooltip: 	'Con error EContract',
				align: 'center'						
			},
			{
				dataIndex: 'DET_ERROR_EC',
				width: 200,
				header: 		'Detalle error EContract',
				tooltip: 	'Detalle error EContract',
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa.replace(/\"/gi,"").replace(/\,/gi," ")) + '"';
					return causa;
				}
			}
		],
		//displayInfo: true,		
		//emptyMsg: "No hay registros.",
		loadMask: true,
		stripeRows: true,
		align: 'center'
	});
//-----------------Formularios-----------------------
	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		hidden			: false,
		collapsible		: false,
		fileUpload: 	true,
		titleCollapse	: false	,
		labelWidth	: 160,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[ elementosForma	],
		layoutConfig: {
			fieldTpl: new Ext.XTemplate(
				'<tpl if="id==\'archivo\'">',
           	'<div class="x-form-item {itemCls}" tabIndex="-1">',
				'<div class="x-form-item {itemCls}" tabIndex="-1">',
				'<label for="{id}" style="{labelStyle}" class="x-form-item-label">',
				'<a class="x-tool x-tool-ayudaLayout" onfocus="blur()" style="float:left;" onclick="javascript:showPanelLayoutDeCarga();"></a>&nbsp;',
				'{label}{labelSeparator}</label>',
				'<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
				'</div><div class="{clearCls}"></div>',
				'</div>',
				'</div>',
				'</tpl>',
				'<tpl if="id!=\'archivo\'">',
				'<div class="x-form-item {itemCls}" tabIndex="-1">',
				'<label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label>',
				'<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
				'</div><div class="{clearCls}"></div>',
				'</div>',
				'</tpl>'
		   )
		},		
		buttons			: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				handler: function(boton, evento) 	{
					/*****************Validaciones***************/
					if(!validaFormulario(fpDatos)){
						return;
					}
					
					var archivo = Ext.getCmp('archivo');
					if(Ext.isEmpty(archivo.getValue())){
						archivo.markInvalid('Favor de cargar un archivo');
						archivo.focus();
						return;
					}
		
					
					cargaMasiva("SUBIR_ARCHIVO", null);			
				}
			},
			{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					location.reload();
				}
			}
		]
	});	
	
	var fp = new Ext.form.FormPanel   ({
		hidden: true,
		height: 'auto',
		width: 600,		
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},		
		items: [elementosProcesando]		
	});
	
	var fpResultados = new Ext.form.FormPanel   ({
		hidden: true,
		height: 'auto',
		width: 465,
		title: ' ',
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'formaResultados',
		defaults: 	{
			msgTarget: 'side',
			anchor: '-20'
		},		
		items: [
			{
				xtype		: 'panel',
				title		: ' ',
				id: 'panelResultados', 
				width: 465
		},
		gridResultadoNE,
		gridResultadoEco
		],		
		buttons	: [
			{
				text: 'Procesar',
				id: 'btnProcesar',				
				handler: function(boton, evento) 	{
          boton.disable();
					Ext.Ajax.request({
						url: '15forma01MasExt.data.jsp',
						params: Ext.apply(fpDatos.getForm().getValues(),{
							procesoID:processID,
							informacion:'Procesar',
							hayLineasConError:hayLineasConError,
							iTotalPymes:iTotalPymes,
							iTotalPymesSinError:iTotalPymesSinError,
							iTotalPymesConError:iTotalPymesConError,
							iTotalPymesPrevReg:iTotalPymesPrevReg,
							hayLineasConErrorEcon:hayLineasConErrorEcon,
							iTotalPymesSinErrorEcon:iTotalPymesSinErrorEcon,
							iTotalPymesConErrorEcon:iTotalPymesConErrorEcon,
							realizarCargaEcon:realizarCargaEcon,
							realizarCarga:realizarCarga}),
						callback: realizarCargaRespuesta
					});				
				}			
			},
			{
				text:'Generar Archivo con Errores',
				iconCls: '',
				handler: function() {
					Ext.Ajax.request({
						url: '15admclimaserrorestxtExt.data.jsp',
						params: Ext.apply({procesoID:processID }),
						callback: procesarArchivoSuccess
					});
				}
			},
			{
				text:'Cancelar',				
				handler: function() {
					location.reload();
				}
			}
		]		
	});
	
	var fpResultadosProc = new Ext.form.FormPanel   ({
		id					: 'fpResultadosProc',
		layout			: 'form',
		width				: 700,
		style				: ' margin:0 auto;',
		frame				: true,		
		collapsible		: false,
		titleCollapse	: false	,		
		bodyStyle		: 'padding: 8px',
		labelWidth			: 300,
		monitorValid: true,
		defaults: { 	msgTarget: 'side', 	anchor: '-20'
		},		
		items: [
			{
				xtype: 'fieldset',
				title: '<CENTER>Resumen Proceso de Carga Masiva Proveedores EContract</CENTER>',
				width: 690,
				id			: 'divResultadosProcECO',
				hidden: true,
				items:[
					/*{
						layout	: 'hbox',
						title		: '',
						id			: 'divResultadosProcECO',
						hidden	  : true,
						width		: 680,
						items		: [elementosProcesadoEco  ]
					},*/
					{
						xtype: 'displayfield',				
						id: 'registrosArchivo',
						fieldLabel: 'N�mero de registros del archivo',
						style: 'text-align:left;',				
						text: '-',
						width: 300
					},
					{
						xtype: 'displayfield',
						value: '',
						id: 'registrosProcesadosEco',
						fieldLabel: 'N�mero de registros a ser procesados por Econtract',
						width: 300
					},
					gridResultadoProcEco
					/*{
						layout	: 'hbox',
						title		: '',
						id			: 'divResultadosProcECO1',
						hidden	  : true,
						width		: 680,				
						items		: [ gridResultadoProcEco ]
					}*/
				]
			},
			//------------	
			{
				xtype: 'fieldset',
				title: 'Resumen Proceso de Carga Masiva Proveedores N@E',
				width: 690,
				layout: 'form',
				hidden: true,
				id			: 'divResultadosProcNE',
				items:[
					/*{
						layout	: 'hbox',
						title		: '',
						id			: 'divResultadosProcNE',
						hidden	  : true,
						width		: 800,
						items		: [ elementosProcesadoNE    ]
					}*/
					{
						xtype: 'displayfield',
						value: '',
						id: 'registrosArchivoNE',
						fieldLabel: 'N�mero de registros del archivo',
						width: 300
					},
					{
						xtype: 'displayfield',
						value: '',
						id: 'registrosProcesadosNE',
						fieldLabel: 'N�mero de registros a ser procesados',
						width: 300
					},
					{
						xtype: 'displayfield',
						value: '',
						id: 'registrosErrNE',
						fieldLabel: 'N�mero de registros para el archivo de errores',
						width: 300
					}
					/*, {
						layout	: 'hbox',
						title		: '',
						id			: 'divResultadosProcNE1',
						hidden	  : true,
						width		: 800,				
						items		: [ gridResultadoProcNE   ]
					}*/
				]
			},
			gridResultadoProcNE
		],
		buttons	: [
			{
				text: 'Imprimir',				
				handler: function(boton, evento) 		{
					descargarPDF();
				}			
			},
			{
				text:'Generar Archivo con Errores',
				iconCls: '',
				handler: function() {
					Ext.Ajax.request({
						url: '15admclimaserrorestxtExt.data.jsp',
						params: Ext.apply({
							procesoID:processID 
						}),
						callback: procesarArchivoSuccess
					});
				}
			},
			{
				text:'Salir',				
				handler: function() {
					location.reload();
				}
			}
		]		
	});

	var menuToolbar =  new Ext.Toolbar({
		width: 410,
		style: 'margin:0 auto;',
		id:'menuToolbar',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},		
		items: [
			{
				xtype: 'tbbutton',
				id: 'btnIndividual',
				text: 'Individual',
				width: 200,
				handler: function(){				
					window.location ='15forma01Ext.jsp';			
				}
			},
			'-',
			{
				xtype: 'tbbutton',
				text:'<b>Masiva</b>',
				width: 200,
				id: 'btnMasiva',
				handler: function(){
					window.location ='15forma01MasExt.jsp';	
				}
			}
		]
	});
	var caracteresEspeciales = new NE.cespcial.CaracteresEspeciales(
		{
		hidden: true
		}
	);
	
	caracteresEspeciales.setHandlerAceptar(function(){
		caracteresEspeciales.hide(),
		fpDatos.show();
		menuToolbar.show();
	});
	
	caracteresEspeciales.setHandlerDescargarArchivo(function(){
		 cargaMasiva("DESCARGAR_ARCHIVO", null);
	});
	var pnl = new Ext.Container   ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',	  
	  width: 940,
	  items:
	   [
			menuToolbar,
			fpDatos,
			espaciadorPanelLayoutDeCarga,
			panelLayoutDeCarga,
			fp,
			fpResultados,
			fpResultadosProc,
			caracteresEspeciales 
		]
  });
	
	
	fp.el.mask('Cargando...', 'x-mask-loading');			
	Ext.Ajax.request({
		url: '15forma01MasExt.data.jsp',
		params: {
			informacion: "valoresIniciales"			
		},
		callback: procesaValoresIniciales
	});
	
});