Ext.onReady(function(){
	/**
     * Funcion que recoje los valores pasados por get de una url.
     * Tiene que recibir el nombre de la variable a devolver su valor.
     * Ejemplo: Si la url es del tipo:
     *  http://lawebdelprogramador.com/index.php?nombre=valor
     * Podemos llamar a la funcion de esta manera:
     *  getURLParameter("nombre")
     */
	function getURLParameter(name){
        return decodeURI(
            (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
        );
    }
	function calculaEstrato(){////////////////////////////////
		
		var puntaje = 0.0;
		var cveEstratoFiado='';
		
		var ventasAnuales = Ext.getCmp('txtVentasAnualesMod').getValue();
		var numEmp = Ext.getCmp('txtNumEmpleadosMod').getValue();
		var sectorEcon = Ext.getCmp('cboCatalogoSectorMod').getValue();
		if(!Ext.isEmpty(ventasAnuales) && !Ext.isEmpty(numEmp) && sectorEcon!='' ){
			puntaje = (((parseFloat(numEmp) * 10)/100) + ((parseFloat(ventasAnuales)/1000000 * 90)/100));
			if(sectorEcon==1){
				cveEstratoFiado = (puntaje<=4.6)?"1":(puntaje>4.6 && puntaje<=95)?"2":(puntaje>95 && puntaje<=250)?"3":puntaje>250?"4":"";
			}
			if(sectorEcon==2){
				cveEstratoFiado = (puntaje<=4.6)?"1":(puntaje>4.6 && puntaje<=93)?"2":(puntaje>93 && puntaje<=235)?"3":puntaje>235?"4":"";
			}
			if(sectorEcon==3){
				cveEstratoFiado = (puntaje<=4.6)?"1":(puntaje>4.6 && puntaje<=93)?"2":(puntaje>93 && puntaje<=235)?"3":puntaje>235?"4":"";
			}
		}	else{
			cveEstratoFiado = '';
		}
		
		if(cveEstratoFiado=='1'){
			Ext.getCmp('lblEstrato').setValue('MICRO');
			claveEstratoFiado=1;
			estrato='MICRO';
			//document.getElementById('divEstrato').innerHTML = 'MICRO';
		}
		if(cveEstratoFiado=='2'){
			Ext.getCmp('lblEstrato').setValue('PEQUE�A');
			claveEstratoFiado=2;
			estrato='PEQUE�A';
			//document.getElementById('divEstrato').innerHTML = 'PEQUE�A';
		}
		
		if(cveEstratoFiado=='3'){
			Ext.getCmp('lblEstrato').setValue('MEDIANA');		
			claveEstratoFiado=3;
			estrato='MEDIANA';
			//document.getElementById('divEstrato').innerHTML = 'MEDIANA';
		}
		if(cveEstratoFiado=='4'){
			Ext.getCmp('lblEstrato').setValue('GRANDE');		
			claveEstratoFiado=4;
			estrato='GRANDE';
			//document.getElementById('divEstrato').innerHTML = 'GRANDE';
		}
	}
	var claveEstratoFiado='';
	var estrato='';
	var estatus='';
	var numFiado='';
//------------------------------------------------------------------------------
//------------------------------HANDLER's---------------------------------------
//------------------------------------------------------------------------------
 function procesaActualizar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText).msg;
			if (jsonData != null &&jsonData=='Actualizaci�n exitosa'){
				//Ext.Msg.alert('sssss',jsonData);//Muestra el mensaje de Extio al actualizar
				Ext.Msg.alert('',jsonData, function() {
					window.location = '15consfiadosExt.jsp';
				}, this);		
				/*Ext.Ajax.request({
					url: '15consfiadosModExt.data.jsp',
					params: {
						informacion: "valoresIniciales",
						numFiado:getURLParameter("cveFiado")
					},
					callback: procesaValoresIniciales
				});*/
			}else{
				Ext.Msg.alert('',jsonData);//Muestra el mensaje de cuando no se realizo la actualizacion
			}
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
 }	
 function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText).registros[0];
			if (jsonData != null){
				Ext.getCmp('txtNombreRazonSocialMod').setValue(jsonData.NOMBREFIADO);//*
				Ext.getCmp('txtrfcMod').setValue(jsonData.RFCFIADO);//*
				
				Ext.getCmp('txtCalleMod').setValue(jsonData.CALLEFIADO);//*
				Ext.getCmp('txtNumExteriorMod').setValue(jsonData.NOEXTERIORFIADO);
				Ext.getCmp('txtNumInteriorMod').setValue(jsonData.NOINTFIADO);
				Ext.getCmp('txtColoniaMod').setValue(jsonData.COLFIADO);
				Ext.getCmp('txtLocalidadMod').setValue(jsonData.LOCFIADO);
				
				Ext.getCmp('txtReferenciaMod').setValue(jsonData.REFFIADO);
				Ext.getCmp('txtMunicipioMod').setValue(jsonData.MUNICFIADO);//*
				Ext.getCmp('txtEstadoMod').setValue(jsonData.ESTADOFIADO);//*
				Ext.getCmp('txtPaisMod').setValue(jsonData.PAISFIADO);//*
				Ext.getCmp('txtCodigoPostalMOd').setValue(jsonData.CPFIADO);//*
				
				Ext.getCmp('txtNombreRepLegalMod').setValue(jsonData.NOMBRERL);//*
				Ext.getCmp('txtemailRepLegalMod').setValue(jsonData.EMAILRL);//*
				Ext.getCmp('txtTelefonoRepLegalMod').setValue(jsonData.TELEFONORL);//*
				
				Ext.getCmp('txtVentasAnualesMod').setValue(jsonData.VENTFIADO);
				Ext.getCmp('txtNumEmpleadosMod').setValue(jsonData.NUMEMPFIADO);
				Ext.getCmp('cboCatalogoSectorMod').setValue(jsonData.SECTORECFIADO);
				Ext.getCmp('lblEstrato').setValue(jsonData.ESTRATOFIADO);
				
				numFiado=jsonData.IC_FIADO;
				estrato=jsonData.ESTRATOFIADO;
				claveEstratoFiado=jsonData.CVEESTRATOFIADO;
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//------------------------------------------------------------------------------
//-----------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------

	//CATALOGO SECTOR ECONOMICO
	var catalogoSectorEconomico = new Ext.data.JsonStore({
		id				: 'catalogoSectorEconomico',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15consfiadosModExt.data.jsp',
		baseParams	: { informacion: 'catalogoSectorEconomico'	},
		totalProperty : 'total',
		autoLoad		: false,
				 exception	: NE.util.mostrarDataProxyError,
		 beforeload	: NE.util.initMensajeCargaCombo//,
		 //load			: procesarCatalogoSectorEconomico
		
	});
	
//------------------------------------------------------------------------------
//---------------------------COMPONENTES----------------------------------------
//------------------------------------------------------------------------------
	var datosFiadoPanelIzq={
		xtype			:'fieldset',
		id				: 'datosFiadoPanelIzq',
		border		:false,
		width			: 400,
		labelWidth	: 130,
		defaults			: { msgTarget: 'side' },
		items			: [
			{
				xtype			: 'textfield',
				id          : 'txtNombreRazonSocialMod',
				name			: 'nombreRazonSocialMod',
				fieldLabel  : '* Nombre ',
				width			: 200,
				maxLength	: 50,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side',
				tabIndex		:	1
			}
		]
	}
	var datosFiadoPanelDer={
		xtype			:'fieldset',
		id				: 'datosFiadoPanelDer',
		border		:false,
		width			: 400,
		labelWidth	: 130,
		items			: [
			{
				width: 200,	
				xtype: 'textfield',
				maxLength: 20,
				msgTarget: 'side',
				fieldLabel	:'* RFC',
				margins: '0 10 0 0',
				tabIndex	:2,
				allowBlank	: false,
				msgTarget: 'side',
				name: 'rfcMod',
				id: 'txtrfcMod',
				regex	:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
				regexText:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
							  'en el formato NNN-AAMMDD-XXX donde:<br>'+
							  'NNN:son las iniciales del nombre de la empresa<br>'+
							  'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
							  'XXX:es la homoclave'
			}
		]
	}
	var datosDomicilioPanelIzq={
		xtype			:'fieldset',
		id				: 'datosDomicilioPanelIzq',
		border		:false,
		width			: 400,
		labelWidth	: 130,
		items			: [
			{
				xtype			: 'textfield',
				id          : 'txtCalleMod',
				name			: 'calleMod',
				fieldLabel  : '* Calle',
				msgTarget: 'side',
				maxLength	: 100,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:3
			},
			{
				xtype			: 'textfield',
				id          : 'txtNumExteriorMod',
				name			: 'numExteriorMod',
				fieldLabel  : 'No. Exterior',
				msgTarget: 'side',
				maxLength	: 50,
				width			: 100,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		:4
			},
			{
				xtype			: 'textfield',
				id          : 'txtNumInteriorMod',
				name			: 'numInteriorMod',
				fieldLabel  : 'No. INterior',
				msgTarget: 'side',
				maxLength	: 50,
				width			: 100,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		:5
			},
			{
				xtype			: 'textfield',
				id          : 'txtColoniaMod',
				name			: 'coloniaMod',
				fieldLabel  : 'Colonia',
				msgTarget: 'side',
				maxLength	: 50,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		:6
			},
			{
				xtype			: 'textfield',
				id          : 'txtLocalidadMod',
				name			: 'localidadMod',
				fieldLabel  : 'Localidad',
				msgTarget: 'side',
				maxLength	: 50,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		:7
			}
		]	
	}
	var datosDomicilioPanelDer={
		xtype			:'fieldset',
		id				: 'datosDomicilioPanelDer',
		border		:false,
		width			: 400,
		labelWidth	: 130,
		items			: [
			{
				xtype			: 'textfield',
				id          : 'txtReferenciaMod',
				name			: 'referenciaMod',
				fieldLabel  : 'Referencia',
				msgTarget: 'side',
				maxLength	: 50,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: true,
				tabIndex		:8
			},
			{
				xtype			: 'textfield',
				id          : 'txtMunicipioMod',
				name			: 'municipioMod',
				fieldLabel  : '* Municipio',
				msgTarget: 'side',
				maxLength	: 50,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:9
			},
			{
				xtype			: 'textfield',
				id          : 'txtEstadoMod',
				name			: 'estadoMod',
				fieldLabel  : '* Estado',
				msgTarget: 'side',
				maxLength	: 50,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 10
			},
			{
				xtype			: 'textfield',
				id          : 'txtPaisMod',
				name			: 'paisMod',
				fieldLabel  : '* Pa�s',
				msgTarget: 'side',
				maxLength	: 50,
				width			: 200,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		: 11
			},
			{
				xtype			:'textfield',
				name			: 'codigoPostalMod',
				id				: 'txtCodigoPostalMOd',
				fieldLabel	: '* C�digo Postal: ',
				msgTarget: 'side',
				maxLength	: 5,
				width			: 100,
				allowBlank	: false,
				tabIndex		: 12
			}
		]
	}
	var datosRepresentanteLegalIzq={
		xtype			:'fieldset',
		id				: 'datosRepresentanteLegalIzq',
		border		:false,
		width			:400,
		labelWidth	: 130,
		items			: [
			{
				xtype			: 'textfield',
				id          : 'txtNombreRepLegalMod',
				name			: 'nombreRepLegalMod',
				fieldLabel  : '* Nombre ',
				msgTarget: 'side',
				width			: 200,
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: false,
				tabIndex		:	13
			},
			{
				xtype			: 'textfield',
				id				: 'txtemailRepLegalMod',
				name			: 'emailRepLegalMod',
				fieldLabel	: '* E-mail',
				allowBlank	: false,
				maxLength	: 100,
				width			: 200,
				tabIndex		: 14,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				vtype: 'email'
			}
			
		]
	}
	var datosRepresentanteLegalDer={
		xtype			:'fieldset',
		id				: 'datosRepresentanteLegalDer',
		border		:false,
		width			: 400,
		labelWidth	: 130,
		items			: [
			{xtype: 'displayfield', value: '', width: 200},//{xtype: 'displayfield', value: '', width: 237},
			{
				xtype			: 'numberfield',
				id          : 'txtTelefonoRepLegalMod',
				name			: 'telefonoRepLegalMMod',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				msgTarget: 'side',
				width			: 180,
				margins		: '0 20 0 0',
				maxLength	: 30,
				tabIndex		: 15
			}
		]
	}
	var datosDiversosPaneIzq ={
		xtype			:'fieldset',
		id				: 'datosDiversosPaneIzq',
		border		:false,
		width			: 400,
		labelWidth	: 130,
		items			: [
			{
				xtype			:'numberfield',
				name			: 'ventasAnualesMod',
				id				: 'txtVentasAnualesMod',
				fieldLabel	: 'Venta Anuales',
				msgTarget	: 'side',
				maxLength	: 13,
				width			: 100,
				allowBlank	: true,
				listeners: {
					change	: function(){calculaEstrato() ;}
            },
				tabIndex		:	16
			},
			{
				xtype			:'numberfield',
				name			: 'numEmpleadosMod',
				id				: 'txtNumEmpleadosMod',
				fieldLabel	: 'N�mero de empleados ',
				msgTarget	: 'side',
				maxLength	: 8,
				width			: 100,
				allowBlank	: true,
				listeners: {
					change	: function(){calculaEstrato() ;}
            },
				tabIndex		:	17
			}
		]
	}
	var datosDiversosPaneDer ={
		xtype			:'fieldset',
		id				: 'datosDiversosPaneDer',
		border		:false,
		width			: 400,
		labelWidth	: 130,
		items			: [
			{
				xtype				: 'combo',
				id          	: 'cboCatalogoSectorMod',
				name				: 'catalogoSectorMod',
				hiddenName 		: 'catalogoSectorMod',
				fieldLabel  	: 'Sector Econ�mico',
				msgTarget		: 'side',
				forceSelection	: true,
				allowBlank		: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccione ...',
				width				: 200,
				tabIndex			: 18,
				listeners: {
				select: function (combo, value) {
						  calculaEstrato();
					 }
				},
				store				: catalogoSectorEconomico
			},
			{	xtype				: 'displayfield', 
				id					:'lblEstrato',
				fieldLabel		:'Estrato',
				value				: '',
				width				: 200
			}
		]
	}
	
	var fpDatosModificar = new Ext.form.FormPanel({
		id					: 'formaDatosModificar',
		layout			: 'form',
		width				: '98%',
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			
			{
				layout	: 'hbox',
				title 	: 'Datos del Fiado',
				cllapsible	:true,
				id			: 'datosFiado',
				width		: 940,
				items		: [datosFiadoPanelIzq	, datosFiadoPanelDer	]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				cllapsible	:true,
				id			: 'domicilio',
				width		: 940,
				items		: [datosDomicilioPanelIzq	, datosDomicilioPanelDer	]
			},
			{
				layout	: 'hbox',
				title 	: 'Datos del Representante Legal',
				cllapsible	:true,
				id			: 'datosRepLegal',
				width		: 940,
				items		: [datosRepresentanteLegalIzq	, datosRepresentanteLegalDer	]
			},
			{
				layout	: 'hbox',
				title 	: 'Diversos',
				cllapsible	:true,
				id			: 'diversos',
				width		: 940,
				items		: [datosDiversosPaneIzq	, datosDiversosPaneDer	]
			}
		],
		buttons			: [
		{
			text: 'Actualizar N@E ',
			id: 'btnActualizaNE',
			iconCls:'aceptar',
			formBind: true,
			handler: function(boton, evento) {
				var todoCorrecto=false;
				/////Validaciones
				if(Ext.getCmp('txtNombreRazonSocialMod').getValue()==''){
					Ext.getCmp('txtNombreRazonSocialMod').markInvalid();
					return;
				}
				if(Ext.getCmp('txtrfcMod').getValue()==''){
					Ext.getCmp('txtrfcMod').markInvalid();
					return;
				}
				if(Ext.getCmp('txtCalleMod').getValue()==''){
					Ext.getCmp('txtCalleMod').markInvalid();
					return;
				}
				if(Ext.getCmp('txtMunicipioMod').getValue()==''){
					Ext.getCmp('txtMunicipioMod').markInvalid();
					return;
				}
				if(Ext.getCmp('txtEstadoMod').getValue()==''){
					Ext.getCmp('txtEstadoMod').markInvalid();
					return;
				}else if(Ext.getCmp('txtPaisMod').getValue()==''){
					Ext.getCmp('txtPaisMod').markInvalid();
					return;
				}
				if(Ext.getCmp('txtCodigoPostalMOd').getValue()==''){
					Ext.getCmp('txtCodigoPostalMOd').markInvalid();
					return;
				}
				if(Ext.getCmp('txtNombreRepLegalMod').getValue()==''){
					Ext.getCmp('txtNombreRepLegalMod').markInvalid();
					return;
				} 
				
				if (!Ext.isEmpty(Ext.getCmp('txtNombreRepLegalMod').getValue()) ){							
					if (Ext.getCmp('txtNombreRepLegalMod').getValue().length>30 ){
						Ext.getCmp('txtNombreRepLegalMod').markInvalid('El tama�o m�ximo de este campo es de 30');
						return;
					}
				}
				
				if(Ext.getCmp('txtemailRepLegalMod').getValue()==''){
					Ext.getCmp('txtemailRepLegalMod').markInvalid();
					return;
				}
				if(Ext.isEmpty(Ext.getCmp('txtTelefonoRepLegalMod').getValue()) || Ext.getCmp('txtTelefonoRepLegalMod').getValue()<0){
					Ext.getCmp('txtTelefonoRepLegalMod').markInvalid();
					return;
				}
				if(todoCorrecto){
					Ext.Ajax.request({
						url: '15consfiadosModExt.data.jsp',
						params: Ext.apply(fpDatosModificar.getForm().getValues(),{
							informacion		: 'Actualizar',
							numFiado			: numFiado,
							estratoFiado	:estrato,
							cveEstratoFiado:claveEstratoFiado					
						}),
						callback:procesaActualizar
					});
					
				}
			}
			},
			{
				text:'Cancelar',
				handler:  function(boton, evento) {	
					Ext.Ajax.request({
						url: '15consfiadosModExt.data.jsp',
						params: {
							informacion: "valoresIniciales",
							numFiado:getURLParameter("cveFiado")
						},
						callback: procesaValoresIniciales
					});	
					
				}
			},
			{
				text:'Regresar',
				handler: function() {
					window.location = '15consfiadosExt.jsp';
				}
			}
		]
	});
	
		var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fpDatosModificar,
			NE.util.getEspaciador(20),
				
			NE.util.getEspaciador(20)
		]
	});
	
	
	catalogoSectorEconomico.load();
	//catalogoSubSector.load();
	Ext.Ajax.request({
		url: '15consfiadosModExt.data.jsp',
		params: {
			informacion: "valoresIniciales",
			numFiado:getURLParameter("cveFiado")
		
	
		},
		callback: procesaValoresIniciales
	});	
	
	

});//fin de js
		