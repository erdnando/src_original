Ext.onReady(function(){
	
	var sTipoUsuario =  Ext.getDom('sTipoUsuario').value;	
	
	var cboTipoAfiliacion = window.location.search 
                 ? Ext.urlDecode(window.location.search.substring(1)).cboTipoAfiliacion 
                 : '1';				
	var nombreTabla = true;
/*--------------------------------- Handler's -------------------------------*/

	var procesarAltaIF =  function(opts, success, response) {
	
		var cmpForma = Ext.getCmp('formaAfiliacion');
		cmpForma.el.unmask();
		cmpForma.getForm().reset();
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var numero_Nafin = Ext.util.JSON.decode(response.responseText).lsNoNafinElectronico;
			if(numero_Nafin != null && numero_Nafin != ""){
				Ext.Msg.alert('Afiliacion EPO','Su n�mero de Nafin Electr�nico: ' + numero_Nafin,function() {
					window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';	
				},this);
			}
		}
		else { 
			NE.util.mostrarConnError(response,opts);			
		}
	}
	
/*------------------------------- End Handler's -----------------------------*/
	function leeRespuesta(){
		window.location = '15forma03Ext.jsp';
	}
	function Guardar () {
		var cargaArchivo = Ext.getCmp('archivo');
		
		if(Ext.isEmpty(cargaArchivo.getValue())){
			cargaArchivo.markInvalid();
			cargaArchivo.focus();
			return;
		}
		Ext.getCmp('winCargaArchivo').hide();						
		fpcargaArchivo.getForm().submit({
			url: '15forma03_file.data.jsp',
			waitMsg: 'Enviando datos...',
			success: function(form, action) {
				var resp = action.result;
				
				Ext.getCmp('strArchivo').setValue(resp.archivo);
				Ext.getCmp('path_destino').setValue(resp.path_destino);
				Ext.MessageBox.alert('Mensaje de p�gina web',resp.msgError );
			},
			failure: NE.util.mostrarSubmitError
			
		})	
	}
	
	function cargarImagen(){
		var ventana = Ext.getCmp('winCargaArchivo');
		if (ventana) {
			fpcargaArchivo.getForm().reset();
			ventana.show(); 
		} 
		else 
		{
			new Ext.Window({
					title			: 'Nafin@net Documentos',
					id				: 'winCargaArchivo',
					layout		: 'fit',
					heigth		:	100,										
					width			:	500,
					minWidth		:	400,
					minHeight	: 	100,
					//buttonAlign	: 'center',
					modal			: true,
					closeAction	: 'hide',
					items			: [fpcargaArchivo]
				}).show();   				
		}
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var formaCarga = Ext.getCmp('fcargaArchivo');
		contenedorPrincipalCmp.add(fpcargaArchivo);
	
	}
	
	var procesarSuccessRFCexistente =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			//rfc_invalido = 'N';
			var cmpForma = Ext.getCmp('formaAfiliacion');
				var params = (cmpForma)?cmpForma.getForm().getValues():{};				
			cmpForma.el.mask("Procesando", 'x-mask-loading');
			//peticion de Registro de Afiliaci�n
			Ext.Ajax.request({
					url: '15forma03Ext.data.jsp',
					params: Ext.apply(params,{ informacion: 'AltaIF'}),
					callback: procesarAltaIF
			});
		}
		else {
			Ext.MessageBox.alert('Mensaje de p�gina web','El RFC ya existe, favor de verificaro' );
			return;
		}
	}
	
	/*****************Validaciones***************/
	function enviar()
	{	
		var descontante1 = Ext.getCmp('id_descontanteIf1');
		var descontante2 = Ext.getCmp('id_descontanteIf2');
		var tipoEpoInst  = Ext.getCmp('id_tipoEpoInstruccion');
		var mandato = (Ext.getCmp('id_mandato_documento')).getValue();
	
		if (mandato) {
			if(!Ext.isEmpty(descontante1.getValue()) && !Ext.isEmpty(descontante2.getValue()) && descontante1.getValue() == descontante2.getValue()) {
				Ext.MessageBox.alert('Mensaje de p�gina web','Los descontantes seleccionados no pueden ser iguales.' );
				return;
			}
			if (Ext.isEmpty(tipoEpoInst.getValue())) {
				Ext.MessageBox.alert('Seleccione un Tipo de Cadena.' );
				tipoEpoInst.markInvalid('Por favor seleccione el archivo para subir');
				return;
			}
		}
		if (!nombreTabla) {
			Ext.MessageBox.alert('Mensaje de p�gina web','El valor  de nombre de la tabla no puede ser vacio' );
			return;
		}
		Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
			if (botonConf == 'ok' || botonConf == 'yes') {
				Ext.Ajax.request({
					url: '15forma03Ext.data.jsp',
					params: Ext.apply({	informacion: 'ConsultaRFCexistente',rf: Ext.getCmp('rfc').getValue() }),
					callback: procesarSuccessRFCexistente
				});				
			} // if botonConfig
		});		
	}
/*****************************************************************************/
 /*
	
	var procesarCatalogoPais = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('id_cmb_pais').setValue('24');
		}
	}
	*/
/*---------------------------------- Store's --------------------------------*/

	function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		}else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} 	else if (pagina == 2) { 	// Distribuidor						
			window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 		 
		} else if (pagina == 3) { 	//Proveedor
			window.location = '15forma01Ext.jsp'; 
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} else if (pagina == 6) { 	//Contragarante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 	
		} else if (pagina == 9) { 	// Mandante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
		} else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		}	else if (pagina == 11) { 	//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 			
		} 	else if (pagina == 12) { 						//Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliaClienteExternoExt.jsp"; 
		}
	}
	var procesarRfcAyuda =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var rfc_sirac = resp.rfc;
			var win =new Ext.Window({
				modal			: false,
				resizable	: false,
				frame       : false,
				layout		: 'form',
				x				: 670,
				y				:550,
				width			: 250,
				id				: 'winAyuRFC',
				items			: [{
					xtype: 'panel',
					items:[{
							xtype: 'label',
							style:'margin:5px',
							html: 'El RFC correspondiente al n�mero de cliente SIRAC ingresado es <br> ( '+rfc_sirac+' )',
							listeners: {
								render: function(c){
									c.getEl().on({
										click: function(el){
										win.close();
										 if(rfc_sirac==''){
											Ext.getCmp('idnumCliSirac').setValue('');
											}
										},
										scope: c
									});
								 }
							}
						}
					]
				}]
			}).show();
		}
   }
	function muestraAyuda(btn){
		Ext.Ajax.request({
				url: '15forma03Ext.data.jsp',
				params:	Ext.apply(
							{
								informacion: 'rfc_Sirac',
								numCliSirac: Ext.getCmp('idnumCliSirac').getValue()
							}
				),
				callback: procesarRfcAyuda
		});
	
   }
	
	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [[/*'EPO'*/  '0','Cadena Productiva']
				,[/*'IF'*/	'1','Intermediario Financiero']
				,[/*'2'*/	'2','Distribuidor']
				,[/*'1'*/	'3','Proveedor']	
				,[/*'ACE'*/	'4','Afiliados Credito Electronico']
				,[/*'EPOI'*/'5','Cadena Productiva Internacional']
				,[/*'CONTRA'*/'6','Contragarante']				
				,[/*'M'*/	'9','Mandante']	
				,[/*'AF'*/	'10','Afianzadora']
				,[/*'UNI'*/ '11','Universidad-Programa Cr�dito Educativo']
				,['12','Cliente Externo']
					
				]
	});
				

	/// ***** ***** ***** ***** CATALOGOS ***** ***** ***** ***** ///
	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			//load:procesarCatalogoPais,
			exception: NE.util.mostrarDataProxyError
		}
  });
  
  // catalogo Estado
	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Municipio
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	//catalogo Domicilio correspondencia
	var catalogoDomicilio = new Ext.data.JsonStore({
	   id				: 'catDomicilio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoDomicilio'},
		autoLoad		: false,
		listeners	:
		{
			exception: NE.util.mostrarDataProxyError
		}
	});	
	
	//catalogo Ejecutivo de cuenta
	var catalogoEjecutivo = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['HVARGAS','HVARGAS'],
			['MCASTANE','MCASTANE'],
			['PMACEDO','PMACEDO']
		 ]
	}) ;
	
	//catalogo Numero IF
	var catalogoNumeroIF= new Ext.data.JsonStore
	({
		id				: 'catNumeroIF',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoNumeroIF'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Avales
	var catalogoAvales= new Ext.data.JsonStore
	({
		id				: 'catAvales',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoAvales'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo TipoRiesgo
	var catalogoTipoRiesgo= new Ext.data.JsonStore
	({
		id				: 'catTipoRiesgo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoTipoRiesgo'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Viabilidad
	var catalogoViabilidad= new Ext.data.JsonStore
	({
		id				: 'catViabilidad',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoViabilidad'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	// catalogoOficina_controladora
	var catalogoOficina_controladora= new Ext.data.JsonStore
	({
		id				: 'catOficina_controladora',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoOficina_controladora'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	// catalogo ClaveIfSIAG
	var catalogoClaveIfSIAG= new Ext.data.JsonStore
	({
		id				: 'catClaveIfSIAG',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoClaveIfSIAG'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});	
	// catalogo ClaveSUCRE
	var catalogoClaveSUCRE= new Ext.data.JsonStore
	({
		id				: 'catClaveSUCRE',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoClaveSUCRE'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Descontante 
	var catalogoDescontante= new Ext.data.JsonStore
	({
		id				: 'catDescontante',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma03Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoDescontante'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	//catalogo Tipo de Cadena 
	var catalogoTipoCadena = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','PUBLICA'],
			['2','PRIVADA'],
			['3','AMBAS']
		 ]
	}) ;
	
/*--------------------------------- End Store's -----------------------------*/
/*****************************************************************************/

	// *** Pop up *** //
	var elementoscargaArchivo = [
		{
			xtype: 		'panel',
			defaults: {	msgTarget: 'side',	anchor: '-20'	},
			bodyStyle: 	'padding: 12px; padding-right:6px;padding-left:6px;',
			layout: 'hbox',
			items: [				
				{	xtype: 'displayfield',	value: 'Carga archivo de Imagen IF:', width: 150	},				
				{
					xtype: 'fileuploadfield',
					id: 'archivo',
					width: 280,	  
					name: 'archivoCesion',
					buttonText: 'Examinar...',
					//buttonCfg: {  iconCls: 'upload-icon' },
				  anchor: '60%'
				  //vtype: 'archivopdf' ---> para imagenes... Deysi �???
				}
			],
			buttons: [
				{	
					text: 'Subir Archivo',
					iconCls: 'icoContinuar',
					formBind: true,
					handler: Guardar	
				},
				{
					text:'Cancelar', iconCls: 'icoLimpiar',	
					handler: function() {					
							var ventana = Ext.getCmp('winCargaArchivo');
							fpcargaArchivo.getForm().reset();
							ventana.hide();
					}	
				}
			]
		}	
	];
	var fpcargaArchivo = new Ext.form.FormPanel({
		id				: 'fcargaArchivo',
      frame			: true,
		width			: 480,
		height		: 100,
		//autoHeight	: true,
		layout		: 'form',
		fileUpload: true,
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		items			: elementoscargaArchivo,
		monitorValid: true		
	});
/*-------------------------------- Componentes ------------------------------*/
	
	var afiliacion = {
		xtype		: 'panel',
		id 		: 'afiliacion',
		layout	: 'hbox',
		border	: false,		
		width		: 900,		
		bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:192px;',
		items		: 
		[{
			layout		: 'form',
			width			:400,
			labelWidth	: 60,
			border		: false,
			items			:[
				{
					xtype: 'hidden',
					id	  : 'path_destino',
					name : 'path_destino',
					value: ''
				},
				{
					xtype: 'hidden',
					id   : 'strArchivo',
					name : 'strArchivo',
					value: ''
				},
				{
					xtype				: 'combo',
					id					: 'id_afiliacion',
					name				: 'afiliacion',
					hiddenName 		:'afiliacion', 
					fieldLabel		: 'Afiliaci�n',
					width				: 250,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',
					value				: '1',
					store				: storeAfiliacion,
					listeners: {
						select: {
							fn: function(combo) {
								var valor  = combo.getValue();
								creditoElec(valor);		
							}
						}
					}					
				}]
			}
	]};
	

	var afiliacionIFPanel = {
		xtype		: 'panel',
		id 		: 'afiliacionIFPanel',
		width		: 600,
		items		: [
			{
				layout		: 'form',
				labelWidth	: 150,
				bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:2px;',
				items		:	
				[
					{
						xtype			: 'textfield',
						name			: 'Razon_Social',
						id				: '* Razon_Social',
						fieldLabel	: '* Raz�n Social',
						margins		: '0 20 0 0',
						msgTarget	: 'side',
						maxLength	: 100,
						width			: 350,
						allowBlank	: false,
						tabIndex		:	1
					},
					{
						xtype			: 'textfield',
						name			: 'nombre_comercial',
						id				: 'nombre_comercial',
						fieldLabel	: '* Nombre Comercial',
						margins		: '0 20 0 0',
						msgTarget	: 'side',
						maxLength	: 40,
						width			: 250,
						allowBlank	: false,
						tabIndex		:	2
					},
					{
						xtype 		: 'textfield',
						name  		: 'rfc',
						id    		: 'rfc',
						fieldLabel	: '* R.F.C.',
						maxLength	: 20,
						width			: 150,
						allowBlank 	: false,
						hidden		: false,
						margins		: '0 20 0 0',
						msgTarget	: 'side',
						regex			:/^([A-Z|&amp;]{3})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
						regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
						'en el formato NNN-AAMMDD-XXX donde:<br>'+
						'NNN:son las iniciales del nombre de la empresa<br>'+
						'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
						'XXX:es la homoclave',
						tabIndex		:	3
					}
			]}
	]};
//---------------------------------------------------------------------------//

	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		layout   : 'form',
		width  	: 450,
		labelWidth	: 170,
		bodyStyle: 	'padding: 4px; padding-right:0px;padding-left:2px;',
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calle',
				name			: 'calle',
				fieldLabel  : '* Calle No. Exterior y No. Interior',
				maxLength	: 100,
				width			: 250,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				allowBlank	: false,
				tabIndex		:	4

			},
			{
				xtype			: 'textfield',
				id          : 'code',
				name			: 'code',
				fieldLabel  : '* C�digo Postal',
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				maxLength	: 5,
				minLength	: 5,
				width			: 90,
				allowBlank	: false,
				//regex			:/^(([0-9]{5})$)/,
				regex			: /^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/,
				regexText	:'S�lo debe de introducir n�meros',
				tabIndex		: 6
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_estado',
				name				: 'cmb_estado',
				hiddenName 		: 'cmb_estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins		   : '0 20 0 0',
				msgTarget	   : 'side',
				tabIndex			: 8,
				store				: catalogoEstado,
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_delegacion').reset();
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais')).getValue()
							})
						});
					}
				}				
			},
			{
				xtype			: 'textfield',
				id          : 'telefono',
				name			: 'telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				maxLength	: 30,
				regex			:/^(([0-9\-]*)$)/,	
				regexText	:'El campo solo puede contener n�meros',
				tabIndex		: 10	//validacion en evento on blur de si isTelFax
				
			},
			{
				xtype			: 'textfield',
				id          : 'fax',
				name			: 'fax',
				fieldLabel  : ' Fax',
				width			: 230,
				maxLength	: 30,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				allowBlank	: true,
				regex			:/^(([0-9\-]*)$)/,	
				regexText	:'El campo solo puede contener n�meros',
				tabIndex		:	12 //validacion en evento on blur de si isTelFax
			},
			{
				xtype				: 'combo',
				id          	: 'id_Domicilio_correspondencia',
				name				: 'Domicilio_correspondencia',
				hiddenName 		: 'Domicilio_correspondencia',
				fieldLabel  	: '* Domicilio correspondencia',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins			: '0 20 0 0',
				msgTarget		: 'side',
				tabIndex			: 13,
				store				: catalogoDomicilio
			},
			{
				xtype				: 'combo',
				id          	: 'id_Numero_de_IF',
				name				: 'Numero_de_IF',
				hiddenName 		: 'Numero_de_IF',
				fieldLabel  	: '* N�mero de IF',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins			: '0 20 0 0',
				msgTarget		: 'side',
				tabIndex			: 15,
				store				: catalogoNumeroIF
			},
			//{ 	xtype: 'displayfield',  value: '' },
			{
				xtype: 'compositefield',
				combineErrors: false,
				msgTarget: 'side',
				items: [				
					{
						xtype: 'textfield',
						name: 'numCliSirac',
						id: 'idnumCliSirac',
						fieldLabel: 'N�mero Cliente SIRAC',
						allowBlank: true,
						maxLength: 25,	//ver el tama�o maximo del numero en BD para colocar este igual
						width: 180,
						msgTarget: 'side',
						margins: '0 20 0 0', //n
						maskRe:/[0-9]/
					},
					{
						xtype:'button',
						id:'ayudaRFC',
						iconCls:'icoAyuda',
						handler:muestraAyuda
					}
				]	
			},
			{
				xtype				: 'combo',
				id          	: 'id_Avales',
				name				: 'Avales',
				hiddenName 		: 'Avales',
				fieldLabel  	: '* Avales',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins			: '0 20 0 0',
				msgTarget		: 'side',
				tabIndex			: 18,
				store				: catalogoAvales
			},
			{ 
				xtype: 			'bigdecimal',
				name: 			'Facultades',
				id: 				'Facultades',
				allowDecimals: true,
				allowNegative: false,
				fieldLabel: 	'* Facultades',
				blankText:		'El campo tiene un n�mero mayor de enteros y/o decimales de lo permitido.\nPor favor escriba un n�mero menor o igual a 19 enteros y 2 decimales.',
				allowBlank: 	false,
				hidden: 			false,				
				msgTarget: 		'side',
				anchor:			'-20',				
				maxValue: 		'9,999,999,999,999,999,999.99',
				format:			'0,000.00',
				width			: 90				
			},				
			{
				xtype			: 'textfield',
				id          : 'Ente_contable',
				name			: 'Ente_contable',
				fieldLabel  : '* Ente contable',
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				maxLength	: 6,
				width			: 90,
				allowBlank	: false,
				regex			:/^(([0-9]+)$)/,
				regexText	:'S�lo debe de introducir n�meros',
				tabIndex		: 22 //validacion en evento on blur de si soloNumeros
			},
			{
				xtype				: 'combo',
				id          	: 'id_Viabilidad',
				name				: 'Viabilidad',
				hiddenName 		: 'Viabilidad',
				fieldLabel  	: '* Viabilidad',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				allowBlank		: false,
				margins			: '0 20 0 0',
				msgTarget		: 'side',
				tabIndex			: 24,
				store				: catalogoViabilidad
			},
			{
				xtype			: 'checkbox',
				id				: 'id_autorizauto_opersf',
				name			: 'autorizauto_opersf',
				hiddenName	: 'autorizauto_opersf',
				fieldLabel	: 'Autorizaci�n Autom�tica de Operaciones sin fondeo',
				tabIndex		: 26,
				enable 		: true
			}
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		layout   : 'form',
		width  	: 450,
		labelWidth	: 170,
		bodyStyle: 	'padding: 4px; padding-right:0px;padding-left:2px;',	
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'colonia',
				name			: 'colonia',
				fieldLabel  : 'Colonia ',
				maxLength	: 100,
				width			: 230,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				allowBlank	: true,
				tabIndex		: 5
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais',
				name				: 'cmb_pais',
				hiddenName 		: 'cmb_pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: false,
				margins		   : '0 20 0 0',
				msgTarget	   : 'side',
				width				: 150,
				tabIndex			: 7,
				store				: catalogoPais,
				listeners: {
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado').reset();
						Ext.getCmp('id_cmb_delegacion').reset();
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
							
							
					}
				}				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacion',
				name				: 'cmb_delegacion',
				hiddenName 		: 'cmb_delegacion',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				triggerAction	: 'all',
				emptyText		:'Seleccionar...',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: false,
				margins			: '0 20 0 0',
				width				: 230,
				tabIndex			: 9,
				store				: catalogoMunicipio				
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_email',
				name			: 'email',
				fieldLabel	: ' E-mail',
				allowBlank	: true,
				maxLength	: 100,
				width			: 230,				
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				tabIndex		: 11,
				vtype: 'email' //validacion en evento on blur de si verEmail
			},
			{ 	xtype: 'displayfield',  value: '' },
			///////
			{
				xtype				: 'combo',
				id          	: 'id_Ejecutivo_de_cuenta',
				name				: 'Ejecutivo_de_cuenta',
				hiddenName 		: 'Ejecutivo_de_cuenta',
				fieldLabel  	: '* Ejecutivo de cuenta',
				forceSelection	: true,
				triggerAction	: 'all',
				emptyText		:'Seleccionar...',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: false,
				margins			: '0 20 0 0',
				width				: 230,
				tabIndex			: 14,
				store				: catalogoEjecutivo				
			},
			{
				xtype			: 'radiogroup',
				msgTarget	: 'side',
				id				: 'id_Tipo_de_IF',
				name			: 'Tipo_de_IF',
				hiddenName	: 'Tipo_de_IF',
				fieldLabel	: '* Tipo de IF',
				cls			: 'x-check-group-alt',
				columns		: [50, 50],
				valueField	:'base',
				tabIndex		: 16,
				items			: [
					{	boxLabel	: 'IFNB',	name : 'Tipo_de_IF', inputValue: 'NB',	checked : true },
					{	boxLabel	: 'IFB',	name : 'Tipo_de_IF', inputValue: 'B' }
			]},
			{
				xtype			: 'radiogroup',
				msgTarget	: 'side',
				id				: 'id_Tipo_de_IF_piso',
				name			: 'Tipo_de_IF_piso',
				hiddenName	: 'Tipo_de_IF_piso',
				fieldLabel	: '',
				cls			: 'x-check-group-alt',
				columns		: [100, 100],
				valueField	:'base',
				width			: 230,
				tabIndex		: 17,
				items			: [
					{	boxLabel	: 'Primer Piso',	name : 'Tipo_de_IF_piso', inputValue: '1'	},
					{	boxLabel	: 'Segundo P�so',	name : 'Tipo_de_IF_piso', inputValue: '2',	checked : true }
			]},
			{
				xtype				: 'combo',
				id          	: 'id_tipo_riesgo',
				name				: 'tipo_riesgo',
				hiddenName 		: 'tipo_riesgo',
				fieldLabel  	: 'Tipo de Riesgo',
				forceSelection	: true,
				triggerAction	: 'all',
				emptyText		:'Seleccionar...',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: true,
				margins			: '0 20 0 0',
				width				: 230,
				tabIndex			: 19,
				store				: catalogoTipoRiesgo				
			},
			{ 
				xtype: 			'bigdecimal',
				name: 			'Limite_maximo_endeudamiento',
				id: 				'Limite_maximo_endeudamiento',
				allowDecimals: true,
				allowNegative: false,
				fieldLabel: 	'* L�mite m�ximo endeudamieto',
				blankText:		'El campo tiene un n�mero mayor de enteros y/o decimales de lo permitido.\nPor favor escriba un n�mero menor o igual a 19 enteros y 2 decimales.',
				allowBlank: 	false,
				hidden: 			false,				
				msgTarget: 		'side',
				anchor:			'-20',				
				maxValue: 		'9,999,999,999,999,999,999.99',
				format:			'0,000.00',
				width			: 90				
			},					
			{ 
				xtype: 			'bigdecimal',
				name: 			'Porcentaje_capital_contable',
				id: 				'Porcentaje_capital_contable',
				allowDecimals: true,
				allowNegative: false,
				fieldLabel: 	'* Porcentaje capital contable',
				blankText:		'El campo tiene un n�mero mayor de enteros y/o decimales de lo permitido.\nPor favor escriba un n�mero menor o igual a 7 enteros y 2 decimales.',
				allowBlank: 	false,
				hidden: 			false,				
				msgTarget: 		'side',
				anchor:			'-20',				
				maxValue: 		'9,999,999.99',
				format:			'0,000.00',
				width			: 90				
			},				
			{
				xtype				: 'combo',
				id          	: 'id_Oficina_controladora',
				name				: 'Oficina_controladora',
				hiddenName 		: 'Oficina_controladora',
				fieldLabel  	: '* Oficina controladora',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				emptyText		:'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				allowBlank		: false,
				margins			: '0 20 0 0',
				width				: 230,
				tabIndex			: 25,
				store				: catalogoOficina_controladora				
			},
			{
				xtype			: 'checkbox',
				id				: 'id_chkFinanciamiento',
				name			: 'chkFinanciamiento',
				hiddenName	: 'chkFinanciamiento',
				fieldLabel	: 'Financiamiento a Distribuidores',
				enable 		: true,
				tabIndex		: 27
			}
		]
	};
	
	var domicilioPanel = {
		xtype			: 'panel',
		id 			: 'domicilioPanel',
		layout		: 'hbox',
		border		: false,		
		width			: 900,		
		labelWidth	: 160,
		bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:13px;',
		items		: 
		[{
				layout	: 'form',
				width		:900,
				border	: false,
				items		:[
					
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype				: 'combo',
								id          	: 'id_if_siag',
								name				: 'if_siag',
								hiddenName 		: 'if_siag',
								fieldLabel  	: 'Clave del Intermediario relacionado en SIAG',
								forceSelection	: true,
								triggerAction	: 'all',
								mode				: 'local',
								typeAhead		: true,
								emptyText		:'Seleccionar...',
								valueField		: 'clave',
								displayField	: 'descripcion',
								width				: 230,								
								margins			: '0 20 0 0',
								tabIndex			: 28,
								store				: catalogoClaveIfSIAG
							},
							{	xtype: 'displayfield',	value: '', width: 38	},
							{
								xtype			: 'checkbox',
								id				: 'id_enlace_automatico',
								name			: 'enlace_automatico',
								hiddenName	: 'enlace_automatico',
								//fieldLabel	: 'Enlace Autom�tico en la Operaci�n del FISO',
								enable 		: true,
								tabIndex		: 29,
								listeners	: {
									blur: function() {
										var enlace = (Ext.getCmp('id_enlace_automatico')).getValue();
										var tabla  = (Ext.getCmp('nombre_tabla'));
										if (enlace == true && Ext.isEmpty(tabla.getValue())) {
											//Ext.Msg.alert('Mensaje de P�gina Web','Debe capturar el nombre de la tabla');
											tabla.markInvalid('Debe capturar el nombre de la tabla');
											tabla.focus();
											nombreTabla = false;
										}
										else if(enlace == false) {
											tabla.setValue('');
											nombreTabla = true;
										}
									}								
								}
							},
							{	xtype: 'displayfield',	value: 'Enlace Autom�tico en la Operaci�n del FISO', width: 120	},
							{	xtype: 'displayfield',	value: '', width: 18	},
							{	xtype: 'displayfield',	value: 'Nombre de la tabla', width: 110	},
							{
								xtype			: 'textfield',
								id          : 'nombre_tabla',
								name			: 'nombre_tabla',
								margins		: '0 20 0 0',
								msgTarget	: 'side',
								//maxLength	: 6,
								width			: 90,
								allowBlank	: true,
								tabIndex		: 30, //validacion en evento on blur "JavaScript:validaTabla();"
								listeners	: {
									blur: function() {
										var enlace = (Ext.getCmp('id_enlace_automatico')).getValue();
										var tabla  = (Ext.getCmp('nombre_tabla'));
										
										if (Ext.isEmpty(tabla.getValue()) && enlace == true) {
											tabla.markInvalid('Debe capturar el nombre de la tabla');
											tabla.focus();
										}
										if (!Ext.isEmpty(tabla.getValue()) && enlace == false) {
											(Ext.getCmp('id_enlace_automatico')).setValue(true);
										}
										if (!Ext.isEmpty(tabla.getValue()) && enlace == true) 
											nombreTabla = true;
									}								
								}
							}
						]
					},	//compo
					{
						xtype: 'compositefield',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype				: 'combo',
								id          	: 'id_if_sucre',
								name				: 'if_sucre',
								hiddenName 		: 'if_sucre',
								fieldLabel  	: 'Clave del Intermediario en el SUCRE',
								forceSelection	: true,
								triggerAction	: 'all',
								mode				: 'local',
								typeAhead		: true,
								emptyText		:'Seleccionar...',
								valueField		: 'clave',
								displayField	: 'descripcion',
								width				: 230,								
								margins			: '0 20 0 0',
								tabIndex			: 31,
								store				: catalogoClaveSUCRE,
								tpl:'<tpl for=".">' +
								'<tpl if="!Ext.isEmpty(loadMsg)">'+
								'<div class="loading-indicator">{loadMsg}</div>'+
								'</tpl>'+
								'<tpl if="Ext.isEmpty(loadMsg)">'+
								'<div class="x-combo-list-item">' +
								'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
								'</div></tpl></tpl>',
								setNewEmptyText: function(emptyTextMsg){
									this.emptyText = emptyTextMsg;
									this.setValue('');
									this.applyEmptyText();
									this.clearInvalid();								
								} 

							},
							{	xtype: 'displayfield',	value: '',	width: 38	},		
							{
								xtype			: 'checkbox',
								id				: 'id_convenio_unico',
								name			: 'convenio_unico',
								hiddenName	: 'convenio_unico',
								//fieldLabel	: 'Opera Convenio �nico',
								tabIndex		: 32,
								enable 		: true
							},
							{	xtype: 'displayfield',	value: 'Opera Convenio �nico',	width: 150	}
						]
                    },
                    {
                        xtype			: 'radiogroup',
                        msgTarget	: 'side',
                        id				: 'id_OperaMontosMenores',
                        name			: 'id_OperaMontosMenores',
                        hiddenName	: 'id_OperaMontosMenores',
                        fieldLabel	: 'Opera Montos Menores',
                        cls			: 'x-check-group-alt',
                        columns		: [50, 50],
                        valueField	:'base',
                        tabIndex		: 16,
                        items			: [
                            {	boxLabel	: 'SI',	name : 'id_OperaMontosMenores', inputValue: 'S'},
                            {	boxLabel	: 'NO',	name : 'id_OperaMontosMenores', inputValue: 'N', checked : true }
                    ]},						
					{
						xtype: 'compositefield',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{	xtype: 'displayfield',	value: '',	width: 289	},		
							{
								xtype: 'checkbox',
								fieldLabel: '',
								name: 'chkFactDistribuido',
								id: 'chkFactDistribuido1',
								hiddenName:'chkFactDistribuido'						
							},
							{	xtype: 'displayfield',	value: 'Opera Factoraje Distribuido',	width: 300	}		
						]
					},				
					
					
					
					
					{
						xtype: 'compositefield',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{	xtype: 'displayfield',	value: '',	width: 289	},		
							{
								xtype			: 'checkbox',
								id				: 'id_fideicomiso',
								name			: 'fideicomiso',
								hiddenName	: 'fideicomiso',								
								tabIndex		: 32,
								enable 		: true
							},
							{	xtype: 'displayfield',	value: 'Opera Fideicomiso para Desarrollo de Proveedores',	width: 300	}		
						]
					},						
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{	xtype: 'displayfield',	value: '',	width: 288	},		
							{
								xtype			: 'checkbox',
								id				: 'id_mandato_documento',
								name			: 'mandato_documento',
								hiddenName	: 'mandato_documento',
								//fieldLabel	: 'Opera Instrucci�n Irrevocable',
								tabIndex		: 33,
								enable 		: true, //evento onclick="javascript:modificaDescontantes();
								listeners	: {
									check: function() {
										var mandato = (Ext.getCmp('id_mandato_documento')).getValue();
										if (mandato) {
											Ext.getCmp('id_descontanteIf1').setDisabled(false);
											Ext.getCmp('id_descontanteIf2').setDisabled(false);
											Ext.getCmp('id_tipoEpoInstruccion').setDisabled(false);
										}
										else {
											Ext.getCmp('id_descontanteIf1').setDisabled(true);
											Ext.getCmp('id_descontanteIf2').setDisabled(true);
											Ext.getCmp('id_tipoEpoInstruccion').setDisabled(true);
										}
									}								
								}
							},
							{	xtype: 'displayfield',	value: 'Opera Instrucci�n Irrevocable', width: 180	}
						]
					},
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
						{	xtype: 'displayfield',	value: '',	width: 287	},		
						{	xtype: 'displayfield',	value: 'Descontante 1:',	width: 170	},		
						{
							xtype				: 'combo',
							id          	: 'id_descontanteIf1',
							name				: 'descontanteIf1',
							hiddenName 		: 'descontanteIf1',
							//fieldLabel  	: 'Descontante 1',
							forceSelection	: true,
							triggerAction	: 'all',
							emptyText		:'Sin Descontante',
							mode				: 'local',
							valueField		: 'clave',
							displayField	: 'descripcion',
							disabled			: true,
							margins			: '0 20 0 0',
							width				: 230,
							tabIndex			: 34,
							store				: catalogoDescontante				
						}
						]
					},//composi
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{	xtype: 'displayfield',	value: '',	width: 287	},		
							{	xtype: 'displayfield',	value: 'Descontante 2:',	width: 170	},		
							{
								xtype				: 'combo',
								id          	: 'id_descontanteIf2',
								name				: 'descontanteIf2',
								hiddenName 		: 'descontanteIf2',
								forceSelection	: true,
								triggerAction	: 'all',
								mode				: 'local',
								emptyText		:'Sin Descontante',
								valueField		: 'clave',
								displayField	: 'descripcion',
								disabled			: true,
								margins			: '0 20 0 0',
								width				: 230,
								tabIndex			: 35,
								store				: catalogoDescontante				
							}
						]
					},//composit
					{
						xtype: 'compositefield',
						fieldLabel: '',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{	xtype: 'displayfield',	value: '',	width: 287	},		
							{	xtype: 'displayfield',	value: 'Tipo de Cadena:',	width: 170	},		
							{
								xtype				: 'combo',
								id          	: 'id_tipoEpoInstruccion',
								name				: 'tipoEpoInstruccion',
								hiddenName 		: 'tipoEpoInstruccion',
								forceSelection	: true,
								triggerAction	: 'all',
								mode				: 'local',
								emptyText		:'Seleccionar...',
								valueField		: 'clave',
								displayField	: 'descripcion',
								allowBlank		: false,
								disabled			: true,
								margins			: '0 20 0 0',
								width				: 230,
								tabIndex			: 36,
								store				: catalogoTipoCadena				
							}
						]
					}//composit
				]
		}]
	};
	
	/*--____Fin Domicilio ____--*/
			
//---------------------------------------------------------------------------//	

	
	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',  
		width				: 940,   
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		monitorValid	: true,
		items				: 
		[
			{
				layout	: 'hbox',
				title		: '',
				width		: 940,
				items		: [afiliacion ]
			},
			{
				layout	: 'hbox',
				title		: '',
				//height	: 500,
				width		: 940,
				items		: [afiliacionIFPanel ]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 940,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			},
			{
				layout	: 'hbox',
				title		: '',
				width		: 940,
				items		: [domicilioPanel ]
			}
		],
		buttons: [ 
			{
				text: 'Cargar Imagen',
				id: 'btnCargarImagen',
				handler: function() {
					cargarImagen();
				}
			},
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				formBind: true,
				handler: function() {
					enviar();
				}
			},
			{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					leeRespuesta();
				}
			}
	  ]
	});			
	       
	
	var pnl = new Ext.Container({
		id  		: 'contenedorPrincipal',
		applyTo	: 'areaContenido',
		width		: 'auto',
		height	: 'auto',
		items		: [fpDatos]
	});

		
	
	Ext.getCmp('id_afiliacion').setValue(cboTipoAfiliacion);
	if(cboTipoAfiliacion == null){
		Ext.getCmp('id_afiliacion').setValue('1');
	}
	
	catalogoPais.load();
	catalogoDomicilio.load();
	catalogoNumeroIF.load();
	catalogoAvales.load();
	catalogoTipoRiesgo.load();
	catalogoViabilidad.load();
	catalogoOficina_controladora.load();
	catalogoClaveIfSIAG.load();
	catalogoClaveSUCRE.load();
	catalogoDescontante.load();
	
});