Ext.onReady(function(){
	var widthLabel = 120;

/**	
	var procesarCatalogoInter = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('id_intermediario').setValue('');
		}
	}
*/
/** 01(INI). Modificado: ABR. Fecha: 07/03/2014. */	
	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);
	
  	var procesarCatalogoInter = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var id_intermediario = Ext.getCmp('id_intermediario');
				Ext.getCmp('id_intermediario').setValue("");
				if(id_intermediario.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_intermediario.setValue("");			
				}	
			}
		}
  };
  
  var procesarCatalogoPaisOrig = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var id_paisOrig_m = Ext.getCmp('id_paisOrig_m');
				var id_paisOrig = Ext.getCmp('id_paisOrig');
				Ext.getCmp('id_paisOrig_m').setValue("");
				Ext.getCmp('id_paisOrig').setValue("");
				if(id_paisOrig_m.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_paisOrig_m.setValue("");			
				}
				if(id_paisOrig.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_paisOrig.setValue("");			
				}
			}
		}
  };
  
  	var procesarCatalogoPaisDom = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				Ext.getCmp('id_pais').setValue('24');	
			}
		}
  };  
 
 	var procesarCatalogoEstado = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var id_estado = Ext.getCmp('id_estado');
				Ext.getCmp('id_estado').setValue("");
				if(id_estado.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_estado.setValue("");			
				}	
			}
		}
  }; 
  
	var procesarCatalogoMunicipio  = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var id_municipio = Ext.getCmp('id_municipio');
				Ext.getCmp('id_municipio').setValue("");
				if(id_municipio.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_municipio.setValue("");			
				}	
			}
		}
  }; 
  
  	var procesarCatalogoCiudad  = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var id_ciudad = Ext.getCmp('id_ciudad');
				Ext.getCmp('id_ciudad').setValue("");
				if(id_ciudad.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_ciudad.setValue("");			
				}	
			}
		}
  }; 
    
  	var procesarCatalogoSector = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var id_sector = Ext.getCmp('id_sector');
				Ext.getCmp('id_sector').setValue("");
				if(id_sector.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_sector.setValue("");			
				}	
			}
		}
  }; 
      
  	var procesarCatalogoSubSector = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var id_subsector = Ext.getCmp('id_subsector');
				Ext.getCmp('id_subsector').setValue("");
				if(id_subsector.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_subsector.setValue("");			
				}
								Ext.getCmp('id_subsector').reset();
								Ext.getCmp('id_rama').reset();
								Ext.getCmp('id_clase').reset();
			}
		}
  }; 
        
  	var procesarCatalogoRama = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var id_rama = Ext.getCmp('id_rama');
				Ext.getCmp('id_rama').setValue("");
				if(id_rama.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_rama.setValue("");			
				}	
			}
		}
  }; 
         
  	var procesarCatalogoClase = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var id_clase = Ext.getCmp('id_clase');
				Ext.getCmp('id_clase').setValue("");
				if(id_clase.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					id_clase.setValue("");			
				}	
			}
		}
  }; 
/** 01(FIN) */

	var procesarAltaEPO =  function(opts, success, response) {
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			if(resp.errorMsg==''){
				var dataCargaAfil = [
					[resp.sirac, resp.nafelec,resp.troya ]
				];
				
				storeDataCargaAfil.loadData(dataCargaAfil);
				fpDatos.hide();
				contenedorPrincipalCmp.add(gridAfiliado);
				contenedorPrincipalCmp.doLayout();
				
				Ext.getCmp('btnArchivo').setHandler(function(btn)
				{
					var archivo = resp.urlArchivo;				
					archivo = archivo.replace('/nafin','');
					var params = {nombreArchivo: archivo};

					fpDatos.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					fpDatos.getForm().getEl().dom.submit();
				});
			} else {
				Ext.Msg.show({
				title: 'Respuesta',
				msg: resp.errorMsg,
				modal: true,
				icon: Ext.Msg.ERROR,
				buttons: Ext.Msg.OK,
				fn: function (){
					Ext.getCmp('id_intermediario').setValue('');
					}
				});
			}
			
			
		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	
//------------------------------------------------------------------------------
	
	function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		}else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} 	else if (pagina == 2) { 	// Distribuidor						
			window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 		 
		} else if (pagina == 3) { 	//Proveedor
			window.location = '15forma01Ext.jsp'; 
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} else if (pagina == 6) { 	//Contragarante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 	
		} else if (pagina == 9) { 	// Mandante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
		} else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		}	else if (pagina == 11) { 	//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 	
		}else if (pagina == 12) { 	//Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliaClienteExternoExt.jsp"; 
		}
	}
	
	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [[/*'EPO'*/  '0','Cadena Productiva']
				,[/*'IF'*/	'1','Intermediario Financiero']
				,[ 			'12','Distribuidor de fondos']
				,[/*'2'*/	'2','Distribuidor']
				,[/*'1'*/	'3','Proveedor']	
				,[/*'ACE'*/	'4','Afiliados Cr�dito Electr�nico']
				,[/*'EPOI'*/'5','Cadena Productiva Internacional']
				,[/*'CONTRA'*/'6','Contragarante']				
				,[/*'M'*/	'9','Mandante']	
				,[/*'AF'*/	'10','Afianzadora']
				,[/*'UNI'*/ '11','Universidad-Programa Cr�dito Educativo']
				, ['12','Cliente Externo']
					
				]
	});
	
	
	var storeCatIntermData = new Ext.data.JsonStore({
	   id				: 'storeCatIntermData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15ext.data.jsp',
		baseParams	: { informacion: 'catalogoIntermediario'},
		autoLoad		: true,
		listeners	:
		{
			load:procesarCatalogoInter,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeTipoPersona = new Ext.data.SimpleStore({
    fields: ['clave', 'descripcion'],
	 autoLoad		: true,
    data : [
				['F','F�sica'],
				['M','Moral']
			  ]
	});
	
	var storeCatPaisOrigData = new Ext.data.JsonStore({
	   id				: 'storeCatPaisData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15ext.data.jsp',
		baseParams	: { informacion: 'catalogoPaisOrig'},
		autoLoad		: true,
		totalProperty : 'total',
		listeners	:
		{
			load:procesarCatalogoPaisOrig,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeCatPaisDomData = new Ext.data.JsonStore({
	   id				: 'storeCatPaisDomData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15ext.data.jsp',
		baseParams	: { informacion: 'catalogoPais'},
		autoLoad		: true,
		listeners	:
		{
			load:procesarCatalogoPaisDom,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeCatEstadoData = new Ext.data.JsonStore({
	   id				: 'storeCatEstadoData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15ext.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoEstado,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeCatMunicipioData = new Ext.data.JsonStore({
	   id				: 'storeCatMunicipioData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15ext.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoMunicipio,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeCatCiudadData = new Ext.data.JsonStore({
	   id				: 'storeCatCiudadData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15ext.data.jsp',
		baseParams	: { informacion: 'catalogoCiudad'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoCiudad,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeCatSectorData = new Ext.data.JsonStore({
	   id				: 'storeCatSectorData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15ext.data.jsp',
		baseParams	: { informacion: 'catalogoSector'},
		autoLoad		: true,
		listeners	:
		{
			load:procesarCatalogoSector,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeCatSubSectorData = new Ext.data.JsonStore({
	   id				: 'storeCatSubSectorData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15ext.data.jsp',
		baseParams	: { informacion: 'catalogoSubSector'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoSubSector,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeCatRamaData = new Ext.data.JsonStore({
	   id				: 'storeCatRamaData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15ext.data.jsp',
		baseParams	: { informacion: 'catalogoRama'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoRama,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeCatClaseData = new Ext.data.JsonStore({
	   id				: 'storeCatClaseData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma15ext.data.jsp',
		baseParams	: { informacion: 'catalogoClase'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoClase,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeDataCargaAfil = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'numSirac'},
			  {name: 'numNafin'},
			  {name: 'msgTroya'}
		  ]
	 });
//------------------------------------------------------------------------------
  

//------------------------------------------------------------------------------
	
	var afiliacion = {
		xtype		: 'panel',
		id 		: 'afiliacion',
		layout	: 'form',
		border	: false,		
		frame: true,
		width		: 865,		
		bodyStyle: 	'padding: 2px; padding-right:0px;',
		items		: 
		[{
			xtype: 'compositefield',
			msgTarget: 'side',
			combineErrors: false,
			items: [
				{
					xtype: 'displayfield',
					width: 250
				},{
					xtype			: 'radiogroup',
					msgTarget	: 'side',
					id				: 'id_fp_dir',
					name			: 'fp_dir',
					hiddenName	: 'fp_sdir',
					cls			: 'x-check-group-alt',
					columns		: [100, 100],
					valueField	:'I',
					items			: [
						{boxLabel	: 'Individual',	name : 'fp_dir', inputValue: 'I',	checked : true},
						{boxLabel	: 'Masiva',	name : 'fp_dir', inputValue: 'M', handler: function() { window.location  = '15forma15aext.jsp';} }
					]
				}	
			]
		},{
			layout		: 'form',
			width			:600,
			//labelWidth	: 60,
			border		: false,
			items			:[{
					xtype				: 'combo',
					id					: 'id_afiliacion',
					name				: 'cmbAfiliacion',
					hiddenName 		:'cmbAfiliacion', 
					fieldLabel		: 'Afiliaci�n',
					width				: 250,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',
					value				: '4',
					store				: storeAfiliacion,
					listeners: {
						select: {
							fn: function(combo) {
								var valor  = combo.getValue();
								creditoElec(valor);		
							}
						}
					}
					
				}]
			},
			{
				layout	: 'form',
				width		:600,
				border	: false,
				items		:[{
					xtype				: 'combo',
					id					: 'id_intermediario',
					name				: 'cmbIntermediario',
					hiddenName 		: 'cmbIntermediario',
					fieldLabel		: 'Intermediario',
					width				: 400,
					emptyText: 'Seleccione ... ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					store				: storeCatIntermData
				}]
			},
			{
				layout	: 'form',
				width		:600,
				border	: false,
				items		:[{
					xtype				: 'combo',
					id					: 'id_tipoPersona',
					name				: 'cmbTipoPersona',
					hiddenName 		: 'cmbTipoPersona',
					fieldLabel		: 'Tipo de Persona',
					width				: 200,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					value				: 'F',
					displayField	: 'descripcion',
					store				: storeTipoPersona,
					listeners		:
					{
						change:function(obj, nVal, oVal){
							if(nVal != oVal){
								if(nVal=='F'){
									
									Ext.getCmp('fp_apPat').allowBlank = false;
									Ext.getCmp('fp_apPat').show();
									Ext.getCmp('fp_apMat').allowBlank = false;
									Ext.getCmp('fp_apMat').show();
									Ext.getCmp('fp_nombreAfil').allowBlank = false;
									Ext.getCmp('fp_nombreAfil').show();
									Ext.getCmp('id_fp_sexo').allowBlank = false;
									Ext.getCmp('id_fp_sexo').show();
									Ext.getCmp('fp_fecNacimiento').allowBlank = false;
									Ext.getCmp('fp_fecNacimiento').show();
									//Ext.getCmp('fp_curp').allowBlank = false;
									Ext.getCmp('fp_curp').show();
									Ext.getCmp('fp_rfcAfil').allowBlank = false;
									Ext.getCmp('fp_rfcAfil').show();
									Ext.getCmp('fp_fiel').show();
									Ext.getCmp('id_paisOrig').show();
									
									Ext.getCmp('fp_razonSocial').allowBlank = true;
									Ext.getCmp('fp_razonSocial').hide();
									Ext.getCmp('fp_razonSocial').reset();
									Ext.getCmp('fp_rfcAfil_m').allowBlank = true;
									Ext.getCmp('fp_rfcAfil_m').hide();
									Ext.getCmp('fp_rfcAfil_m').reset();
									Ext.getCmp('fp_fiel_m').allowBlank = true;
									Ext.getCmp('fp_fiel_m').hide();
									Ext.getCmp('fp_fiel_m').reset();
									Ext.getCmp('id_paisOrig_m').allowBlank = true;
									Ext.getCmp('id_paisOrig_m').hide();
									Ext.getCmp('id_paisOrig_m').reset();
								}else if(nVal=='M'){
									
									Ext.getCmp('fp_apPat').allowBlank = true;
									Ext.getCmp('fp_apPat').hide();
									Ext.getCmp('fp_apPat').reset();
									Ext.getCmp('fp_apMat').allowBlank = true;
									Ext.getCmp('fp_apMat').hide();
									Ext.getCmp('fp_apMat').reset();
									Ext.getCmp('fp_nombreAfil').allowBlank = true;
									Ext.getCmp('fp_nombreAfil').hide();
									Ext.getCmp('fp_nombreAfil').reset();
									Ext.getCmp('id_fp_sexo').allowBlank = true;
									Ext.getCmp('id_fp_sexo').hide();
									Ext.getCmp('id_fp_sexo').reset();
									Ext.getCmp('fp_fecNacimiento').allowBlank = true;
									Ext.getCmp('fp_fecNacimiento').hide();
									Ext.getCmp('fp_fecNacimiento').reset();
									Ext.getCmp('fp_curp').allowBlank = true;
									Ext.getCmp('fp_curp').hide();
									Ext.getCmp('fp_curp').reset();
									Ext.getCmp('fp_rfcAfil').allowBlank = true;
									Ext.getCmp('fp_rfcAfil').hide();
									Ext.getCmp('fp_rfcAfil').reset();
									Ext.getCmp('fp_fiel').hide();
									Ext.getCmp('fp_fiel').reset();
									Ext.getCmp('id_paisOrig').hide();
									Ext.getCmp('id_paisOrig').reset();
									
									Ext.getCmp('fp_razonSocial').allowBlank = false;
									Ext.getCmp('fp_razonSocial').show();
									Ext.getCmp('fp_rfcAfil_m').allowBlank = false;
									Ext.getCmp('fp_rfcAfil_m').show();
									Ext.getCmp('fp_fiel_m').allowBlank = true;
									Ext.getCmp('fp_fiel_m').show();
									Ext.getCmp('id_paisOrig_m').allowBlank = true;
									Ext.getCmp('id_paisOrig_m').show();
								}
							}
						}
					}
					
				}]
			}
	]};
	
	
	var datosTipoPersona = {
		xtype		: 'panel',
		id 		: 'datosTipoPersona',
		border	: false,
		layout: {
			 type: 'table',
			 columns: 2
		},
		frame: true,
		width		: 865,		
		bodyStyle: 	'padding: 2px; padding-right:0px;',
		items		: 
		[	{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'textfield',
					id          : 'fp_apPat',
					name			: 'fp_apPat',
					fieldLabel  : '* Apellido paterno',
					maxLength	: 30,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: false
				},
				{
					xtype			: 'textfield',
					id          : 'fp_razonSocial',
					name			: 'fp_razonSocial',
					fieldLabel  : '* Raz�n Social',
					maxLength	: 30,
					width			: 230,
					margins		: '0 20 0 0',
					hidden		: true,
					allowBlank	: true
				}
				]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'textfield',
					id          : 'fp_apMat',
					name			: 'fp_apMat',
					fieldLabel  : '* Apellido materno',
					maxLength	: 100,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: false
				},
				{
					xtype			: 'textfield',
					id          : 'fp_rfcAfil_m',
					name			: 'fp_rfcAfil_m',
					fieldLabel  : '* R.F.C.',
					maxLength	: 15,
					width			: 230,
					margins		: '0 20 0 0',
					regex			:/^([A-Z|a-z|&amp;]{3})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
					regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
					'en el formato NNN-AAMMDD-XXX',
					allowBlank	: true,
					hidden: true
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'textfield',
					id          : 'fp_nombreAfil',
					name			: 'fp_nombreAfil',
					fieldLabel  : '* Nombre(s)',
					maxLength	: 40,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: false
				},
				{
					xtype			: 'textfield',
					id          : 'fp_fiel_m',
					name			: 'fp_fiel_m',
					fieldLabel  : '  FIEL',
					maxLength	: 25,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: true,
					hidden		: true
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'textfield',
					id          : 'fp_rfcAfil',
					name			: 'fp_rfcAfil',
					fieldLabel  : '* R.F.C.',
					maxLength	: 15,
					width			: 230,
					msgTarget: 'side',
					margins		: '0 20 0 0',
					regex			:/^([A-Z|&amp;]{4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
													regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
													'en el formato NNNN-AAMMDD-XXX donde:<br>'+
													'NNNN:son las iniciales del nombre de la empresa<br>'+
													'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
													'XXX:es la homoclave',
					allowBlank	: false
				},	
				{
					xtype				: 'combo',
					id					: 'id_paisOrig_m',
					name				: 'cmbPaisOrig_m',
					hiddenName 		: 'cmbPaisOrig_m',
					fieldLabel		: 'Pa�s de origen',
					width				: 230,
					emptyText      : 'Seleccione ... ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					displayField   : 'descripcion',	
					valueField     : 'clave',
					store				: storeCatPaisOrigData,
					allowBlank	: true,
					hidden			: true
				},{xtype: 'displayfield', value: '', width: 237}
				]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'radiogroup',
					msgTarget	: 'side',
					id				: 'id_fp_sexo',
					name			: 'fp_sexo',
					hiddenName	: 'fp_sexo',
					fieldLabel	: '* Sexo',
					cls			: 'x-check-group-alt',
					columns		: [50, 50],
					valueField	:'M',
					//tabIndex		: 33,
					items			: [
						{boxLabel	: 'F',	name : 'fp_sexo', inputValue: 'F'},
						{boxLabel	: 'M',	name : 'fp_sexo', inputValue: 'M',	checked : true }
					]
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				//hidden:true,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'datefield',
					id				: 'fp_fecNacimiento',
					name			: 'fp_fecNacimiento',
					fieldLabel	: '* Fecha de nacimiento (dd/mm/aaaa)',
					allowBlank 	: false,
					width			: 120,
					msgTarget	: 'side',
					//tabIndex		:	7,
					margins		: '0 20 0 0',
					listeners	:{
						blur:function(obj){
							//var Frm = document.formCE;
							var fechaHoy = new Date();
							var fechaRFC = Ext.getCmp('fp_rfcAfil').getValue();
							if(fechaRFC != '') {
								if(navigator.appName =='Microsoft Internet Explorer')
									var anioHoy = fechaHoy.getYear();
								else
									var anioHoy = fechaHoy.getYear() + 1900;
								var finAnoHoy = parseInt(anioHoy.toString().substr(2,4));
									
								var guion = fechaRFC.indexOf("-") + 1;
								var fechaRFC_AAMMDD = fechaRFC.substr(guion,6);
								var anoDado = parseInt(fechaRFC_AAMMDD.substr(0,2));
								if(anoDado > finAnoHoy && anoDado < 99)
									var strAnio = "19"+fechaRFC_AAMMDD.substr(0,2);
								else
									var strAnio = "20"+fechaRFC_AAMMDD.substr(0,2);
								var strMes = fechaRFC_AAMMDD.substr(2,2);
								var strDia = fechaRFC_AAMMDD.substr(4,2);
								var strFechaNacim = strDia+"/"+strMes+"/"+strAnio
							
								if(obj.getValue() != "" || obj.getValue() == ""){
									obj.setValue(strFechaNacim);
								}
							}
						}
					}
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'textfield',
					id          : 'fp_curp',
					name			: 'fp_curp',
					fieldLabel  : '  CURP',
					maxLength	: 18,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: true
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'textfield',
					id          : 'fp_fiel',
					name			: 'fp_fiel',
					fieldLabel  : '  FIEL',
					maxLength	: 25,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: true
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype				: 'combo',
					id					: 'id_paisOrig',
					name				: 'cmbPaisOrig',
					hiddenName 		: 'cmbPaisOrig',
					fieldLabel		: 'Pa�s de origen',
					width				: 230,
					emptyText      : 'Seleccione ... ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					displayField   : 'descripcion',	
					valueField     : 'clave',
					store				: storeCatPaisOrigData,
					allowBlank	: true
				}]
			}
	]};
	
	var datosDomicilio = {
		xtype		: 'panel',
		id 		: 'datosDomicilio',
		border	: false,
		layout: {
			 type: 'table',
			 columns: 2
		},
		frame: true,
		width		: 865,		
		bodyStyle: 	'padding: 2px; padding-right:0px;',
		items		: 
		[	{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'textfield',
					id          : 'fp_calle',
					name			: 'fp_calle',
					fieldLabel  : '* Calle, No.Exterior y No.Interior',
					maxLength	: 100,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: false
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'textfield',
					id          : 'fp_colonia',
					name			: 'fp_colonia',
					fieldLabel  : '* Colonia',
					maxLength	: 100,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: false
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[
                                
                                /*{
					xtype			: 'numberfield',
					id          : 'fp_codPost',
					name			: 'fp_codPost',
					fieldLabel  : '* C�digo postal',
					maxLength	: 5,
					width			: 230,
					margins		: '0 20 0 0',
					allowDecimals: false,
					allowBlank	: false
				}*/
                                {
					xtype			: 'textfield',
					id          : 'fp_codPost',
					name			: 'fp_codPost',
					fieldLabel  : '* C�digo postal',
					maxLength	: 5,
                                        minLength:		5,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: false,
                                        regex       : /^[0-9]*$/,
                                        regexText   : 'Solo de contener n�meros'
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype				: 'combo',
					id					: 'id_pais',
					name				: 'cmbPais',
					hiddenName 		: 'cmbPais',
					fieldLabel		: '* Pa�s',
					width				: 230,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					allowBlank		: false,
					store				: storeCatPaisDomData,
					listeners		:{
						select: function(combo, record, index) {
							storeCatEstadoData.load({
								params:{
									cmbPais: combo.getValue()
								}
							});	
						}
					}
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype				: 'combo',
					id					: 'id_estado',
					name				: 'cmbEstado',
					hiddenName 		: 'cmbEstado',
					fieldLabel		: '* Estado',
					width				: 230,
					emptyText: 'Seleccione ... ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					allowBlank		: false,
					store				: storeCatEstadoData,
					listeners		:{
						change: function(obj, newVal, oldVal){
							if(oldVal!=newVal){
								storeCatMunicipioData.load({
									params:{
										cmbPais: Ext.getCmp('id_pais').getValue(),
										cmbEstado: newVal
									}
								});
								
								storeCatCiudadData.load({
									params:{
										cmbPais: Ext.getCmp('id_pais').getValue(),
										cmbEstado: newVal
									}
								});
								
							}
						}
					}
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype				: 'combo',
					id					: 'id_municipio',
					name				: 'cmbMunicipio',
					hiddenName 		: 'cmbMunicipio',
					fieldLabel		: '* Delegaci�n o municipio',
					width				: 230,
					emptyText: 'Seleccione ... ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					allowBlank		: false,
					store				: storeCatMunicipioData
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype				: 'combo',
					id					: 'id_ciudad',
					name				: 'cmbCiudad',
					hiddenName 		: 'cmbCiudad',
					fieldLabel		: 'Ciudad',
					width				: 230,
					emptyText: 'Seleccione ... ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					allowBlank		: true,
					store				: storeCatCiudadData
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'numberfield',
					id          : 'fp_telefono',
					name			: 'fp_telefono',
					fieldLabel  : '* Tel�fono',
					maxLength	: 30,
					width			: 230,
					margins		: '0 20 0 0',
					allowBlank	: false
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'textfield',
					id          : 'fp_mail',
					name			: 'fp_mail',
					fieldLabel  : '* E-mail',
					maxLength	: 50,
					width			: 230,
					margins		: '0 20 0 0',
					vtype			: 'email',
					allowBlank	: false
				}]
			}
			
	]};
	
	
	var datosDiversos = {
		xtype		: 'panel',
		id 		: 'datosDiversos',
		border	: false,
		layout: {
			 type: 'table',
			 columns: 1
		},
		frame: true,
		width		: 865,		
		bodyStyle: 	'padding: 2px; padding-right:0px;',
		items		: 
		[	{
					xtype: 'compositefield',
					msgTarget: 'side',
					combineErrors: false,
					items: [
						{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'numberfield',
					id          : 'fp_ventas',
					name			: 'fp_ventas',
					fieldLabel  : '* Ventas netas totales',
					maxLength	: 19,
					width			: 230,
					margins		: '0 20 0 0',
					renderer: Ext.util.Format.numberRenderer('0.00'),
					allowDecimals: true,
					allowBlank	: false,
					listeners:{
						blur: function(obj){
							if(obj.getValue()!="" && parseFloat(obj.getValue())<5000) {
								Ext.Msg.alert("Mensaje Web", "El parametro de Ventas Netas Totales debe ser mayor o igual a $5,000.00", function(){
									obj.setValue('');
								});
							}
						}
					}
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'numberfield',
					id          : 'fp_numEmp',
					name			: 'fp_numEmp',
					fieldLabel  : '* N�mero de empleados',
					maxLength	: 6,
					width			: 230,
					margins		: '0 20 0 0',
					allowDecimals: false,
					allowBlank	: false
				}]
			}
					]
				},
			{
				layout	: 'form',
				width		:860,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype				: 'combo',
					id					: 'id_sector',
					name				: 'cmbSector',
					hiddenName 		: 'cmbSector',
					fieldLabel		: '* Sector econ�mico',
					width				: 685,
					emptyText: 'Seleccione ... ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					allowBlank		: false,
					store				: storeCatSectorData,
					listeners		:{
						change: function(obj, newVal, oldVal){
							
							if(oldVal!=newVal && newVal !=""){
								storeCatSubSectorData.load({
									params:{
										cmbSector: newVal
									}
								});
							} else if(newVal==""){
								Ext.getCmp('id_subsector').reset();
								Ext.getCmp('id_rama').reset();
								Ext.getCmp('id_clase').reset();								
							}
						}
					}
				}]
			},
			{
				layout	: 'form',
				width		:860,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype				: 'combo',
					id					: 'id_subsector',
					name				: 'cmbSubSector',
					hiddenName 		: 'cmbSubSector',
					fieldLabel		: '* Subsector',
					width				: 685,
					emptyText: 'Seleccione ... ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					allowBlank		: false,
					store				: storeCatSubSectorData,
					listeners		:{
						change: function(obj, newVal, oldVal){
							
							if(oldVal!=newVal && newVal !="" && Ext.getCmp('id_sector').getValue() !="" ){
								storeCatRamaData.load({
									params:{
										cmbSector: Ext.getCmp('id_sector').getValue(),
										cmbSubSector: newVal
									}
								});
							} else if(newVal=="") {
								Ext.getCmp('id_subsector').focus();
								Ext.getCmp('id_rama').reset();
								Ext.getCmp('id_clase').reset();
							} else if (Ext.getCmp('id_sector').getValue() ==""){
								Ext.getCmp('id_sector').focus();
								Ext.getCmp('id_subsector').reset();
								Ext.getCmp('id_rama').reset();
								Ext.getCmp('id_clase').reset();
							}
						}
					}
				}]
			},
			{
				layout	: 'form',
				width		:860,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype				: 'combo',
					id					: 'id_rama',
					name				: 'cmbRama',
					hiddenName 		: 'cmbRama',
					fieldLabel		: '* Rama',
					width				: 685,
					emptyText: 'Seleccione ... ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					allowBlank		: false,
					store				: storeCatRamaData,
					listeners		:{
						change: function(obj, newVal, oldVal){						
					
							if(oldVal!=newVal && newVal !="" && Ext.getCmp('id_sector').getValue() !="" && Ext.getCmp('id_subsector').getValue() !="" ){
								storeCatClaseData.load({
									params:{
										cmbSector: Ext.getCmp('id_sector').getValue(),
										cmbSubSector: Ext.getCmp('id_subsector').getValue(),
										cmbRama: newVal
									}
								});
							} else if( Ext.getCmp('id_sector').getValue() !="" && Ext.getCmp('id_subsector').getValue() =="") {
								Ext.getCmp('id_rama').reset();
								Ext.getCmp('id_clase').reset();
								Ext.getCmp('id_subsector').focus();
							} else if( Ext.getCmp('id_sector').getValue() =="" ){
								Ext.getCmp('id_subsector').reset();
								Ext.getCmp('id_rama').reset();
								Ext.getCmp('id_clase').reset();
								Ext.getCmp('id_sector').focus();
							}
						}
					}
				}]
			},
			{
				layout	: 'form',
				width		:860,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype				: 'combo',
					id					: 'id_clase',
					name				: 'cmbClase',
					hiddenName 		: 'cmbClase',
					fieldLabel		: '* Clase',
					width				: 685,
					emptyText: 'Seleccione ... ',
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					allowBlank		: false,
					store				: storeCatClaseData
				}]
			},
			{
				layout	: 'form',
				width		:450,
				border	: false,
				labelWidth	: widthLabel,
				items		:[{
					xtype			: 'checkbox',
					id          : 'fp_troya',
					name			: 'fp_troya',
					checked     : true,
					fieldLabel  : 'Alta del cliente a TROYA',
					value			: 'S',
					width			: 100,
					margins		: '0 20 0 0'
					//allowBlank	: false
				}]
			}
			
	]};

	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 930,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side', anchor: '-20' },
		items				: 
		[
			{
				layout	: 'form',
				title		: '',
				width		: 940,
				items		: [afiliacion ]
			},
			{
				layout	: 'form',
				title		: 'Nombre Completo',
				width		: 940,
				items		: [datosTipoPersona ]
			},
			{
				layout	: 'form',
				title		: 'Domicilio',
				width		: 940,
				items		: [datosDomicilio ]
			},
			{
				layout	: 'form',
				title		: 'Diversos',
				width		: 940,
				items		: [datosDiversos ]
			}
		],
		monitorValid	: true,
		buttons			: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				formBind: true,
				handler:function(){
					
					if (Ext.getCmp('id_intermediario').getValue() ==""){
							Ext.getCmp('id_intermediario').focus();
							Ext.getCmp('id_intermediario').markInvalid('Seleccione un Intermediario.');
							return;
					} else if(Ext.getCmp('id_municipio').getValue() ==""){
							Ext.getCmp('id_municipio').focus();
							Ext.getCmp('id_municipio').markInvalid('Seleccione un Municipio.');
					} else if (Ext.getCmp('id_sector').getValue() ==""){
							Ext.getCmp('id_sector').focus();
							Ext.getCmp('id_sector').markInvalid('Seleccione el subsector.');				
							return;
					} else if (Ext.getCmp('id_subsector').getValue() ==""){
							Ext.getCmp('id_subsector').focus();
							Ext.getCmp('id_subsector').markInvalid('Seleccione el subsector.');				
							return;
					}  else if(Ext.getCmp('id_rama').getValue() ==""){
							Ext.getCmp('id_rama').focus();
							Ext.getCmp('id_rama').markInvalid('Seleccione la rama.');
							return;
					} else if (Ext.getCmp('id_clase').getValue() ==""){
							Ext.getCmp('id_clase').focus();
							Ext.getCmp('id_clase').markInvalid('Seleccione la clase.');
							return;
					}   else {
							var cmpForma = Ext.getCmp('formaAfiliacion');
							var params = (cmpForma)?cmpForma.getForm().getValues():{};
							Ext.Ajax.request({
									url: '15forma15ext.data.jsp',
									params: Ext.apply(params,{
											informacion: 'enviarInfo'
									}),
									callback: procesarAltaEPO
							});
						}
					}
			}/**,
			{
				text:'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15forma0Ext.jsp';
				}
			}*/
		]
	});
	
	
	var gridAfiliado = new Ext.grid.GridPanel({
		id: 'gridAfiliado',
		store: storeDataCargaAfil,
		margins: '20 0 0 0',
		//hideHeaders : true,
		columns: [
			{
				header : 'N�mero SIRAC',
				dataIndex : 'numSirac',
				width : 150,
				sortable : true
			},
			{
				header : 'Nafin Electr�nico',
				tooltip: 'Nafin Electr�nico',
				dataIndex : 'numNafin',
				width : 150,
				sortable : true
			},
			{
				header : 'N�mero Cliente TROYA',
				dataIndex : 'msgTroya',
				width : 150,
				sortable : true
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 453,
		style: 'margin:0 auto;',
		autoHeight : true,
		title: '<p align="center">Resumen de Carga<br>La siguiente PYME quedo afiliada a Cr�dito Electr�nico</p>',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Generar Archivo',
					id: 'btnArchivo'
				},
				'-',
				{
				text: 'Terminar',
				id: 'btnTerminar',
				handler: function(){ window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; }
				}
			]
		}
	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//width: 'auto',
		width: 940,
		height: 'auto',
		items: //fp
		[ fpDatos ]
	});

storeCatEstadoData.load({
	params:{
		cmbPais: 24
	}
});

});