<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,
		com.netro.seguridadbean.*,  
		net.sf.json.JSONObject,  			
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	String txtNumElectronico = (request.getParameter("txtNumElectronico")!=null)?request.getParameter("txtNumElectronico"):""; 
	String txtNombre = (request.getParameter("txtNombre")!=null)?request.getParameter("txtNombre"):""; 
	String sel_edo = (request.getParameter("sel_edo")!=null)?request.getParameter("sel_edo"):""; 
	String convenio_unico = (request.getParameter("convenio_unico")!=null)?request.getParameter("convenio_unico"):""; 
	String ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):""; 
	String txtLogin = (request.getParameter("txtLogin")!=null)?request.getParameter("txtLogin"):""; 
	String tipoAfiliado = (request.getParameter("tipoAfiliado")!=null)?request.getParameter("tipoAfiliado"):""; 
	String modificar = (request.getParameter("modificar")!=null)?request.getParameter("modificar"):""; 
	String tituloF = (request.getParameter("tituloF")!=null)?request.getParameter("tituloF"):"";
	String tituloC = (request.getParameter("tituloC")!=null)?request.getParameter("tituloC"):"";
	String interFinan = (request.getParameter("interFinan")!=null)?request.getParameter("interFinan"):"";	
	String chkFactDistribuidoCons = (request.getParameter("chkFactDistribuidoCons")   != null && request.getParameter("chkFactDistribuidoCons").equals("on"))  ?"S":"";
	 
	Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	
	ConIntermediarios paginador = new ConIntermediarios();	
	CambioPerfil clase = new  CambioPerfil();
	
	if(convenio_unico.equals("on")){  convenio_unico  ="S"; }
	
	int  start= 0, limit =0;
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();

	
if (informacion.equals("valoresIniciales")) {   

	jsonObj = new JSONObject();
	//** Fodea 021-2015 (E);
	com.netro.seguridadbean.Seguridad seguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
		
	String idMenuP   = request.getParameter("idMenuP")==null?"15FORMA5":request.getParameter("idMenuP");
	String  [] permisosSolicitados = { "ACCI_ELI_IFN", "CAMBIO_PERFIL" };       
		
	List permisos =  seguridadBean.getPermisosPorMenu( iTipoPerfil, strPerfil, idMenuP, permisosSolicitados );     
	//** Fodea 021-2015 (S);   
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("permisos",  permisos);   
	infoRegresar = jsonObj.toString();	


}else  if (informacion.equals("catalogoEstado")) {
	
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("COMCAT_ESTADO");
   cat.setCampoClave("IC_ESTADO");
   cat.setCampoDescripcion("CD_NOMBRE"); 
	cat.setOrden("CD_NOMBRE");
   infoRegresar = cat.getJSONElementos();

} else  if(informacion.equals("catalogoPerfil") ){  

	HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  tipoAfiliado  );	
	List perfiles = (List)catPerfil.get("PERFILES");	
	Iterator it = perfiles.iterator();	
	HashMap datos = new HashMap();
	List registros  = new ArrayList();	
	while(it.hasNext()) {
		List campos = (List) it.next();
		String clave = (String) campos.get(0);
		datos = new HashMap();
		datos.put("clave", clave );
		datos.put("descripcion", clave );
		registros.add(datos);
	}
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();


}else  if (informacion.equals("Consultar") ||  informacion.equals("GeneraArchivoPDF")   || informacion.equals("GeneraArchivoCSV")  ) {

	
	paginador.setTxtNumElectronico(txtNumElectronico);
	paginador.setTxtNombre(txtNombre);
	paginador.setSel_edo(sel_edo);
	paginador.setConvenio_unico(convenio_unico);
	paginador.setInterFinan(interFinan);
	paginador.setOperaFactDistribuido(chkFactDistribuidoCons);
	
		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if(informacion.equals("Consultar") ||  informacion.equals("GeneraArchivoPDF") ) {
		
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
					
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	
		if(informacion.equals("Consultar")) {
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);										
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
				
			jsonObj = JSONObject.fromObject(consulta);				
	
		}else  if ( informacion.equals("GeneraArchivoPDF") ) {
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
			
	}else  if ( informacion.equals("GeneraArchivoCSV") ) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}			
	}
	
	infoRegresar = jsonObj.toString();	

}else  if (informacion.equals("Borrar") ) {
        System.out.println("BORRAR=======");

	try {
		msg = BeanAfiliacion.borraIf(iNoUsuario, ic_if);		
	} catch(NafinException lexError) {
		msg = lexError.getMsgError();		
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("msg", msg);	
	infoRegresar = jsonObj.toString();


}else  if (informacion.equals("Inhabilitar") ) {
	
	String naE = (request.getParameter("naE")!=null)?request.getParameter("naE"):"";
	String nombreIF = (request.getParameter("nombreIF")!=null)?request.getParameter("nombreIF"):"";

	try {
		msg = BeanAfiliacion.inhabilitarEpo(ic_if, iNoUsuario);
	} catch(NafinException lexError) {
		msg = lexError.getMsgError();		
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("msg", msg);	
	jsonObj.put("tituloC", tituloC);	
	jsonObj.put("tituloF", tituloF);		
	jsonObj.put("naE", naE);	
	jsonObj.put("nombreIF", nombreIF);	
	jsonObj.put("ic_epo", ic_if);	
	infoRegresar = jsonObj.toString();
	
}else  if (informacion.equals("ConsCuentas") ||   informacion.equals("ImprimirConsCuentas")  ) {
	
	UtilUsr utilUsr = new UtilUsr();
	List cuentas = utilUsr.getUsuariosxAfiliado(ic_if, tipoAfiliado);
	Iterator itCuentas = cuentas.iterator();	
	
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	
	if (informacion.equals("ConsCuentas"))  {
		while (itCuentas.hasNext()) {
			String cuenta = (String) itCuentas.next();		
			datos = new HashMap();		
			datos.put("CUENTA", cuenta );				
			registros.add(datos);
		}	
			
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		
	}else  if( informacion.equals("ImprimirConsCuentas") )  {  
	
		String nombreArchivo = paginador.imprimirCuentas(request, cuentas, tituloF, tituloC,strDirectorioTemp );		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	}
	
	jsonObj.put("tituloC", tituloC);	
	jsonObj.put("tituloF", tituloF);	
	infoRegresar = jsonObj.toString();


}else  if( informacion.equals("informacionUsuario") ){  
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true));
	
	HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  "S"  );	
	
	if(catPerfil.size()>0){
	
		List perfiles = (List)catPerfil.get("PERFILES");
	
		String lblNombreEmpresa = (String)catPerfil.get("EMPRESA");
		String lblNafinElec = (String)catPerfil.get("NAELECTRONICO");
		String lblNombre = (String)catPerfil.get("NOMBRE_USUARIO");
		String lblApellidoPaterno = (String)catPerfil.get("APELLIDO_PATERNO");
		String lblApellidoMaterno = (String)catPerfil.get("APELLIDO_MATERNO");
		String lblEmail = (String)catPerfil.get("EMAIL");
		String lblPerfilActual = (String)catPerfil.get("PERFIL_ACTUAL");
		String internacional = (String)catPerfil.get("INTERNACIONAL");
		String sTipoAfiliado = (String)catPerfil.get("TIPOAFILIADO");		
			
		jsonObj.put("lblNombreEmpresa", lblNombreEmpresa);
		jsonObj.put("lblNafinElec", lblNafinElec);
		jsonObj.put("lblNombre", lblNombre);
		jsonObj.put("lblApellidoPaterno", lblApellidoPaterno);
		jsonObj.put("lblApellidoMaterno", lblApellidoMaterno);
		jsonObj.put("lblEmail", lblEmail);
		jsonObj.put("lblPerfilActual", lblPerfilActual);
		jsonObj.put("txtLogin", txtLogin);
		jsonObj.put("mensaje", mensaje);	
		
		jsonObj.put("internacional", internacional);
		jsonObj.put("sTipoAfiliado", sTipoAfiliado);	
		jsonObj.put("txtNafinElectronico", lblNafinElec);	
		jsonObj.put("txtPerfilAnt", lblPerfilActual);	
		jsonObj.put("modificar", modificar);
				
	}else  {
		mensaje = "No se encontró el usuario con la clave especificada, por favor vuelva a intentarlo";
		jsonObj.put("mensaje", mensaje);
	}
	
	infoRegresar = jsonObj.toString();
	
}else  if( informacion.equals("ModifiarPerfil") ){ 

	String txtPerfil = (request.getParameter("txtPerfil")==null)?"":request.getParameter("txtPerfil");
	String txtNafinElectronico = (request.getParameter("txtNafinElectronico")==null)?"":request.getParameter("txtNafinElectronico");
	String txtPerfilAnt = (request.getParameter("txtPerfilAnt")==null)?"":request.getParameter("txtPerfilAnt");
	String internacional = (request.getParameter("internacional")==null)?"":request.getParameter("internacional");
	String txtTipoAfiliado = (request.getParameter("sTipoAfiliado")==null)?"":request.getParameter("sTipoAfiliado");
	String clave_usuario = (String)session.getAttribute("Clave_usuario");
	String txtLoginC = (request.getParameter("txtLoginC") != null) ? request.getParameter("txtLoginC") : "";
	
	mensaje =  clase.getConsultaUsuario( txtLoginC,  txtPerfil, txtPerfilAnt,  txtNafinElectronico ,  internacional,  txtTipoAfiliado, 	 clave_usuario ) ;

	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("mensaje", mensaje);
	infoRegresar = jsonObj.toString();
}	

	//System.out.println("infoRegresar  "+infoRegresar);
%>


<%=infoRegresar%>

