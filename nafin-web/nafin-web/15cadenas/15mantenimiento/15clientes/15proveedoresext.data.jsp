<%@ page contentType="application/json;charset=UTF-8" import="   
   java.util.*, java.sql.*,   
	netropology.utilerias.*,   
	com.netro.exception.*,  
	javax.naming.*,   
	com.netro.model.catalogos.CatalogoSimple,   
	com.netro.model.catalogos.CatalogoEstado,  
	net.sf.json.JSONArray,   
	net.sf.json.JSONObject" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<% 

String strTipo                = "PYMEP";
String busca                  = request.getParameter("btnBuscar");            if (busca                  == null) { busca                 = ""; }
String sector                 = request.getParameter("cSector");              if (sector                 == null) { sector                = ""; }
String nombre                 = request.getParameter("txtNombre");            if (nombre                 == null) { nombre                = ""; }
//String cboEPO                 = request.getParameter("cboEPO");               if (cboEPO                 == null) { cboEPO                = ""; } else { cboEPO = cboEPO.trim(); }
String nafinElectronico 		= request.getParameter("txtNafinElectronico");  if (nafinElectronico 		== null) { nafinElectronico 		= ""; }
String estado 						= request.getParameter("cEstado"); 					if (estado 						== null) { estado 					= ""; }
String rfc 							= request.getParameter("txtRFC"); 					if (rfc 							== null) { rfc 						= ""; }
String numeroProveedor 			= request.getParameter("txtNumeroProveedor"); 	if (numeroProveedor 			== null) { numeroProveedor 		= ""; }
String pymesSinNumProv     	= request.getParameter("cbPymesSinNumProv");    if (pymesSinNumProv        == null) { pymesSinNumProv 	   = ""; }
int start                     = 0;
int limit                     = 0;
JSONObject resultado 	      = new JSONObject();

String informacion            =(request.getParameter("informacion")!=null) ? request.getParameter("informacion"):"";
String operacion              =(request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String infoRegresar           = "";
	
boolean esProveedor = strTipo.equals("PYMEP")?true:false;

boolean filtrarxEpo = false;
if (strTipoUsuario.equals("EPO")) {	filtrarxEpo = true; }


if (informacion.equals("catologoSectorDist")) {
 CatalogoSimple cat = new CatalogoSimple();
 cat.setCampoClave("ic_sector_econ");
 cat.setCampoDescripcion("cd_nombre");
 cat.setTabla("comcat_sector_econ");
 cat.setOrden("cd_nombre");
 infoRegresar = cat.getJSONElementos();
}
else if (informacion.equals("catologoEstadoDist")) {
 CatalogoEstado cat = new CatalogoEstado();
 cat.setCampoClave("ic_estado");
 cat.setCampoDescripcion("initcap(CD_NOMBRE)"); 
 cat.setClavePais("24");
 cat.setOrden("cd_nombre");
 infoRegresar = cat.getJSONElementos();
}
else if (informacion.equals("Consulta") || informacion.equals("ConsultaTotales") || informacion.equals("ArchivoCSV")) {
   com.netro.cadenas.ConsProveedores paginador = new com.netro.cadenas.ConsProveedores();
	paginador.setEsProveedor(esProveedor);
	paginador.setFiltrarxEpo(filtrarxEpo);
	paginador.setINoCliente(iNoCliente);
	paginador.setSector(sector);
	paginador.setNafinElectronico(nafinElectronico);
   paginador.setNombre(nombre);
	paginador.setEstado(estado);
	paginador.setRfc(rfc);
	paginador.setPymesSinNumProv(pymesSinNumProv);
	paginador.setNumeroProveedor(numeroProveedor);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";
	
	if(informacion.equals("Consulta")) {
	 /*try {
	  Registros reg = queryHelper.doSearch();
	  infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
	 } 
	 catch(Exception e) {
	  throw new AppException("Error en la paginacion", e);
	 }*/
	 try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);															   		
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		
				resultado = JSONObject.fromObject(consulta);
				infoRegresar = resultado.toString();
	}
	else if (informacion.equals("ConsultaTotales") ) {
		
		queryHelper = new CQueryHelperRegExtJS(paginador); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion
		
	}
	else if (informacion.equals("ArchivoCSV")) {
	 try {
	  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
	  JSONObject jsonObj = new JSONObject();
	  jsonObj.put("success", new Boolean(true));
	  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	  infoRegresar = jsonObj.toString();
	  } 
	  catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
	  }
	} 
}

%>
<%=infoRegresar%>
