<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		com.netro.seguridadbean.*, 
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	
	String clavePyme =(request.getParameter("clavePyme")!=null)?request.getParameter("clavePyme"):"";		
	String claveEpo = (request.getParameter("claveEpo")!=null)?request.getParameter("claveEpo"):""; 
	String tipoPersona =	(request.getParameter("tipoPersona")!=null)?request.getParameter("tipoPersona"):""; 
	
	String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");

	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	
	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	
	/*
	*	no se deberia hacer una instancia de AfiliacionBean puesto que es u ejb
	*	se hizo para poder para como  parametro un objeto de tipo AccesoDB, esta igual que en la version original
	*/
	AfiliacionBean beanAfiliacion = new AfiliacionBean();//afiliacionHome.create(); 
	ConDistribuidores paginador = new ConDistribuidores();
	Vector vdDatos = new Vector();
	Vector vdResult = new Vector();
	vdDatos = null;
	HashMap datos = new HashMap();
	
	com.netro.seguridadbean.Seguridad seguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
		
	String idMenuP   = request.getParameter("idMenuP")==null?"15FORMA5":request.getParameter("idMenuP");
	String ccPermiso = (request.getParameter("ccPermiso")!=null)?request.getParameter("ccPermiso"):"";

if(informacion.equals("valoresIniciales")){
	
	//** Fodea 021-2015 (E);
		String  [] permisosSolicitados = {"BTNACTUASIRAC_DM", "BTNACTUALIZANE_DM"};      
		
	List permisos =  seguridadBean.getPermisosPorMenu(  iTipoPerfil, strPerfil, idMenuP, permisosSolicitados );     
	//** Fodea 021-2015 (S);   

	paginador.setClavePyme(clavePyme);
	paginador.setClaveEpo(claveEpo);
	paginador.setTipoPersona(tipoPersona);
	Registros reg = paginador.getInfoModificar();
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta );
	jsonObj.put("permisos",  permisos); 
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("catalogoPaisOrigen")){
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("COMCAT_PAIS");
   cat.setCampoClave("IC_PAIS");
   cat.setCampoDescripcion("CD_DESCRIPCION"); 
   infoRegresar = cat.getJSONElementos();
}else if(informacion.equals("catalogoPais")){
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("COMCAT_PAIS");
   cat.setCampoClave("IC_PAIS");
   cat.setCampoDescripcion("CD_DESCRIPCION"); 
	cat.setValoresCondicionNotIn("52", String.class);
	cat.setOrden("IC_PAIS");
   infoRegresar = cat.getJSONElementos();
	
} else if(informacion.equals("catalogoEstado")){

	String pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
	CatalogoEstado cat = new CatalogoEstado();
	cat.setCampoClave("ic_estado");
	cat.setCampoDescripcion("cd_nombre"); 
	cat.setClavePais(pais);//Mexico clave 24
	cat.setOrden("cd_nombre");
	infoRegresar = cat.getJSONElementos();	
	
		
 }else if (informacion.equals("catalogoMunicipio")){
	String Edo= (request.getParameter("estado")==null)?"":request.getParameter("estado");
	String pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
	CatalogoMunicipio cat=new CatalogoMunicipio();
		cat.setClave("ic_municipio"+"||"+"\'|\'" +"||cd_nombre");
		cat.setDescripcion("cd_nombre");
		cat.setPais(pais);
		cat.setEstado(Edo);
		cat.setOrden("cd_nombre");
	List elementos=cat.getListaElementos();
	JSONArray jsonArr = new JSONArray();
	Iterator it = elementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar= "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
}else if (informacion.equals("catalogoCiudad")){
	String pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
	String edo= (request.getParameter("estado")==null)?"":request.getParameter("estado");
	paginador.setClavePais(pais);
	paginador.setClaveEstado(edo);
	List catCiudad= paginador.getDatosCatalogoCiudadList();
	jsonObj.put("registros", catCiudad);	
   infoRegresar = jsonObj.toString();		
 
 }else  if(informacion.equals("catalogoIdentificacion")){
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("comcat_identificacion");
   cat.setCampoClave("IC_IDENTIFICACION");
   cat.setCampoDescripcion("CD_NOMBRE"); 
	infoRegresar = cat.getJSONElementos();

 }else if (informacion.equals("catalogoTipoCategoria")){
 //select IC_TIPO_CATEGORIA,CD_NOMBRE from comcat_tipo_categoria
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("comcat_tipo_categoria");
   cat.setCampoClave("IC_TIPO_CATEGORIA");
   cat.setCampoDescripcion("CD_NOMBRE"); 
	infoRegresar = cat.getJSONElementos();
			
 }else if(informacion.equals("catalogoSectorEconomico")){
	List catSectorEcon= paginador.getDatosCatalogoSectorEconomicoList();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", catSectorEcon);	
   infoRegresar = jsonObj.toString();	
	
 }else if(informacion.equals("catalogoSubSector")){
	String claveSector=(request.getParameter("claveSector")==null)?"":request.getParameter("claveSector");
	paginador.setClaveSector(claveSector);
	List catSubSectorEcon= paginador.getDatosCatalogoSubSectorEconomicoList();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", catSubSectorEcon);	
   infoRegresar = jsonObj.toString();	
	
}else if(informacion.equals("catalogoRama")){
	String claveSector= (request.getParameter("claveSector")==null)?"":request.getParameter("claveSector");
	String claveSubSector= (request.getParameter("claveSubSector")==null)?"":request.getParameter("claveSubSector");
	paginador.setClaveSector(claveSector);
	paginador.setClaveSubSector(claveSubSector);
	List catRama= paginador.getDatosCatalogoRamaList();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", catRama);	
   infoRegresar = jsonObj.toString();
 
 }else if(informacion.equals("catalogoClase")){
	String claveSector= (request.getParameter("claveSector")==null)?"":request.getParameter("claveSector");
	String claveSubSector= (request.getParameter("claveSubSector")==null)?"":request.getParameter("claveSubSector");
	String claveRama =(request.getParameter("claveRama")==null)?"":request.getParameter("claveRama");
	paginador.setClaveSector(claveSector);
	paginador.setClaveSubSector(claveSubSector);
	paginador.setClaveRama(claveRama);
	List catClase= paginador.getDatosCatalogoClaseList();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", catClase);	
   infoRegresar = jsonObj.toString();
 
 
 }else if(informacion.equals("catlogoEstadoCivil")){
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("COMCAT_ESTADO_CIVIL");
   cat.setCampoClave("IC_ESTADO_CIVIL");
   cat.setCampoDescripcion("CD_DESCRIPCION"); 
	infoRegresar = cat.getJSONElementos();
 }else if(informacion.equals("catalogoGradoEscolar")){
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("COMCAT_GRADO_ESC");
   cat.setCampoClave("IC_GRADO_ESC");
   cat.setCampoDescripcion("CD_NOMBRE"); 
	infoRegresar = cat.getJSONElementos();
	
 }
 else if(informacion.equals("catalogoRegimenMatrimonial")){
 
	JSONArray registros = new JSONArray();
	datos = new HashMap(); 	
		datos.put("clave", "s"); 
		datos.put("descripcion", "Separación de Bienes"); 	
		registros.add(datos);
	datos = new HashMap(); 	
		datos.put("clave", "m"); 
		datos.put("descripcion", "Bienes Mancomunados"); 	
		registros.add(datos);
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);		
	infoRegresar = jsonObj.toString();
 
 }else if(informacion.equals("catalogoTipoEmpresa")){
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("COMCAT_TIPO_EMPRESA");
   cat.setCampoClave("IC_TIPO_EMPRESA");
   cat.setCampoDescripcion("CD_NOMBRE"); 
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("catalogoTipoCredito")){
	
	paginador.setClaveEpo(claveEpo);		
	String tipoCredito= paginador.getTipoCredito();
	
	JSONArray registros = new JSONArray();
	if(tipoCredito.equals("A")){
		datos = new HashMap(); 	
		datos.put("clave", "C"); 
		datos.put("descripcion", "Modalidad 2 (Riesgo Distribuidor)"); 
		registros.add(datos);
		
		datos = new HashMap(); 	
		datos.put("clave", "D"); 
		datos.put("descripcion", "Modalidad 1 (Riesgo Empresa de Primer Orden"); 
		registros.add(datos);
		
	}else if(tipoCredito.equals("D")){
		datos = new HashMap(); 	
		datos.put("clave", "D"); 
		datos.put("descripcion", "Modalidad 1 (Riesgo Empresa de Primer Orden )"); 	
		registros.add(datos);
		
	}else if(tipoCredito.equals("C")){
		datos = new HashMap(); 	
		datos.put("clave", "C");
		datos.put("descripcion", "Modalidad 2 (Riesgo Distribuidor)"); 	
		registros.add(datos);
		
	}else if(tipoCredito.equals("F")){
		datos = new HashMap(); 	
		datos.put("clave", "F"); 
		datos.put("descripcion", "Factoraje con Recurso)"); 	
		registros.add(datos);
	} 
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";	
		
	
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	
	
	

}else if(informacion.equals("catalogoOficinaTramitadora")){
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("comcat_agencia_sirac");
   cat.setCampoClave("ic_estado");
   cat.setCampoDescripcion("cd_descripcion"); 
	infoRegresar = cat.getJSONElementos();
	
}


else if (informacion.equals("actualizarNAE")||informacion.equals("actualizarSirac")){
			AccesoDB con = new AccesoDB();
			
	try{
			con.conexionDB();
			String ic_epo=(request.getParameter("claveEpo")!=null)?request.getParameter("claveEpo"):""; 
			String ic_pyme=(request.getParameter("clavePyme")!=null)?request.getParameter("clavePyme"):"";		
			String Razon_Social=(request.getParameter("razonSocial")!=null)?request.getParameter("razonSocial"):""; 
			String Nombre_Comercial="";
			
			String R_F_C="";String Colonia="";	String Estado="";	String Pais_de_origen="";
			String Sector_economico="";String Numero_de_empleados="";String Telefono="";
			String Fax="";String No_identificacion="";String Calle="";String Codigo_postal="";
			String Delegacion_o_municipio=""; Pais="";String Ventas_netas_totales="";
			String Subsector="";	String Rama="";String Clase="";String Tipo_empresa="";
			String Principales_productos="";String Apellido_paterno_C="";String Apellido_materno_C="";
			String Nombre_C="";String Telefono_C="";String Email_C="";String Fax_C="";
			String Numero_de_distribuidor="";String TipoCredito="";String strOficinaTramitadora="";
			String fiel="";String ciudad="";String Identificacion="";String EMail="";
			if(tipoPersona.equals("F")){
				R_F_C=(request.getParameter("rfcFisica")!=null)?request.getParameter("rfcFisica"):"";
				Colonia =(request.getParameter("coloniaFisica")!=null)?request.getParameter("coloniaFisica"):"";
				Estado =(request.getParameter("catalogoEstadoFisica")!=null)?request.getParameter("catalogoEstadoFisica"):"";
				Pais_de_origen=(request.getParameter("catalogoPaisOrigenFisica")!=null)?request.getParameter("catalogoPaisOrigenFisica"):"";
				Sector_economico=(request.getParameter("catalogoSectorEconomicoFisica")!=null)?request.getParameter("catalogoSectorEconomicoFisica"):"";
				Numero_de_empleados=(request.getParameter("numEmpleadosFisica")!=null)?request.getParameter("numEmpleadosFisica"):"";
				Telefono=(request.getParameter("telefonoFisica")!=null)?request.getParameter("telefonoFisica"):"";
				Fax =(request.getParameter("faxContactoDomicilioFisica")!=null)?request.getParameter("faxContactoDomicilioFisica"):"";
				Calle=(request.getParameter("calleFisica")!=null)?request.getParameter("calleFisica"):"";
				Codigo_postal=(request.getParameter("codigoPostalFisica")!=null)?request.getParameter("codigoPostalFisica"):"";
				Delegacion_o_municipio=(request.getParameter("catalogoDelegacionFisica")!=null)?request.getParameter("catalogoDelegacionFisica"):"";
				Pais=(request.getParameter("catalogoPaisFisica")!=null)?request.getParameter("catalogoPaisFisica"):"";
				Ventas_netas_totales=(request.getParameter("ventasNetasFisica")!=null)?request.getParameter("ventasNetasFisica"):"";
				Subsector=(request.getParameter("catalogoSubSectorFisica")!=null)?request.getParameter("catalogoSubSectorFisica"):"";
				Rama=(request.getParameter("catalogoRamaFisica")!=null)?request.getParameter("catalogoRamaFisica"):"";
				Clase	=(request.getParameter("catalogoClaseFisica")!=null)?request.getParameter("catalogoClaseFisica"):"";
				Tipo_empresa	=(request.getParameter("catalogoTipoEmpresaFisica")!=null)?request.getParameter("catalogoTipoEmpresaFisica"):"";
				Principales_productos=(request.getParameter("productosPrincipalesFisica")!=null)?request.getParameter("productosPrincipalesFisica"):"";
				Apellido_paterno_C=(request.getParameter("apPaternoContactoFisica")!=null)?request.getParameter("apPaternoContactoFisica"):"";
				Nombre_C		=(request.getParameter("nombreContactoFisica")!=null)?request.getParameter("nombreContactoFisica"):"";
				Fax_C			=(request.getParameter("faxContactoFisica")!=null)?request.getParameter("faxContactoFisica"):"";
				Apellido_materno_C	=(request.getParameter("apMaternoContactoFisica")!=null)?request.getParameter("apMaternoContactoFisica"):"";
				Telefono_C		=(request.getParameter("telefonoContactoFisica")!=null)?request.getParameter("telefonoContactoFisica"):"";
				Email_C			=(request.getParameter("emailContactoFisica")!=null)?request.getParameter("emailContactoFisica"):"";
				EMail=(request.getParameter("emailContactoFisica")!=null)?request.getParameter("emailContactoFisica"):"";//EMAIL emailContactoFisica//???????????????????????????????????
				Numero_de_distribuidor=(request.getParameter("numDistribuidorFisica")!=null)?request.getParameter("numDistribuidorFisica"):"";
				TipoCredito=(request.getParameter("catalogoTipoCreditoFisica")!=null)?request.getParameter("catalogoTipoCreditoFisica"):"";
				strOficinaTramitadora=(request.getParameter("catalogoOficinaTramitadoraFisica")!=null)?request.getParameter("catalogoOficinaTramitadoraFisica"):"";
				fiel=(request.getParameter("fielFisica")!=null)?request.getParameter("fielFisica"):""; //Fodea campos PLD 2010
				ciudad=(request.getParameter("catalogoCiudadFisica")!=null)?request.getParameter("catalogoCiudadFisica"):"";
				Identificacion=(request.getParameter("catalogoIdentificacionFisica")!=null)?request.getParameter("catalogoIdentificacionFisica"):"";
				No_identificacion=(request.getParameter("numIdentificacionFisica")!=null)?request.getParameter("numIdentificacionFisica"):"";//
			}else{
			
				R_F_C=(request.getParameter("rfc")!=null)?request.getParameter("rfc"):"";
				Colonia =(request.getParameter("colonia")!=null)?request.getParameter("colonia"):"";
				Estado =(request.getParameter("catalogoEstado")!=null)?request.getParameter("catalogoEstado"):"";
				Pais_de_origen=(request.getParameter("catalogoPaisOrigen")!=null)?request.getParameter("catalogoPaisOrigen"):"";
				Sector_economico=(request.getParameter("catalogoSectorEconomico")!=null)?request.getParameter("catalogoSectorEconomico"):"";
				Numero_de_empleados=(request.getParameter("numEmpleados")!=null)?request.getParameter("numEmpleados"):"";
				Telefono=(request.getParameter("telefono")!=null)?request.getParameter("telefono"):"";
				Fax =(request.getParameter("fax")!=null)?request.getParameter("fax"):"";
				Calle=(request.getParameter("calle")!=null)?request.getParameter("calle"):"";
				Codigo_postal=(request.getParameter("codigoPOstal")!=null)?request.getParameter("codigoPOstal"):"";
				Delegacion_o_municipio=(request.getParameter("catalogoDelegacion")!=null)?request.getParameter("catalogoDelegacion"):"";
				Pais=(request.getParameter("catalogoPais")!=null)?request.getParameter("catalogoPais"):"";
				Ventas_netas_totales=(request.getParameter("ventasNetas")!=null)?request.getParameter("ventasNetas"):"";
				Subsector=(request.getParameter("catalogoSubSector")!=null)?request.getParameter("catalogoSubSector"):"";
				Rama=(request.getParameter("catalogoRama")!=null)?request.getParameter("catalogoRama"):"";
				Clase	=(request.getParameter("catalogoClase")!=null)?request.getParameter("catalogoClase"):"";
				Tipo_empresa	=(request.getParameter("catalogoTipoEmpresa")!=null)?request.getParameter("catalogoTipoEmpresa"):"";
				Principales_productos=(request.getParameter("productosPrincipales")!=null)?request.getParameter("productosPrincipales"):"";
				Apellido_paterno_C=(request.getParameter("apPaternoContacto")!=null)?request.getParameter("apPaternoContacto"):"";
				Nombre_C		=(request.getParameter("nombreContacto")!=null)?request.getParameter("nombreContacto"):"";
				Fax_C			=(request.getParameter("faxContacto")!=null)?request.getParameter("faxContacto"):"";
				Apellido_materno_C	=(request.getParameter("apMaternoContacto")!=null)?request.getParameter("apMaternoContacto"):"";
				Telefono_C		=(request.getParameter("telefonoContacto")!=null)?request.getParameter("telefonoContacto"):"";
				Email_C			=(request.getParameter("emailContacto")!=null)?request.getParameter("emailContacto"):"";
				EMail=(request.getParameter("emailRep")!=null)?request.getParameter("emailRep"):"";//EMAIL Rep//???????????????????????????????????
				Numero_de_distribuidor=(request.getParameter("numDistribuidor")!=null)?request.getParameter("numDistribuidor"):"";
				TipoCredito=(request.getParameter("catalogoTipoCredito")!=null)?request.getParameter("catalogoTipoCredito"):"";
				strOficinaTramitadora=(request.getParameter("catalogoOficinaTramitadora")!=null)?request.getParameter("catalogoOficinaTramitadora"):"";
				fiel=(request.getParameter("fiel")!=null)?request.getParameter("fiel"):""; //Fodea campos PLD 2010
				ciudad=(request.getParameter("catalogoCiudad")!=null)?request.getParameter("catalogoCiudad"):"";
				Identificacion=(request.getParameter("catalogoIdentificacion")!=null)?request.getParameter("catalogoIdentificacion"):"";
				No_identificacion=(request.getParameter("numIdentificacion")!=null)?request.getParameter("numIdentificacion"):"";//
			}
			String chkDescuento="";		//Activa el producto de descuento-usar nulo
			String Num_Sirac=(request.getParameter("numSirac")!=null)?request.getParameter("numSirac"):"";
			String Numero_Exterior="";
			String Fecha_de_nacimiento=(request.getParameter("fechaNacioFisica")!=null)?request.getParameter("fechaNacioFisica"):"";
			String chkAnticipo="";			//Activa anticipo
			String Estado_civil=(request.getParameter("catlogoEstadoCivil")!=null)?request.getParameter("catlogoEstadoCivil"):"";
			String TAfilia="R";				//Tipo de afiliacion:H,R,N,S
			String Apellido_paterno=(request.getParameter("apellidoPaternoFisica")!=null)?request.getParameter("apellidoPaternoFisica"):"";
			String Apellido_materno=(request.getParameter("apMaternoFisica")!=null)?request.getParameter("apMaternoFisica"):"";
			String Nombre=(request.getParameter("nombreFisica")!=null)?request.getParameter("nombreFisica"):"";
			String Sexo=(request.getParameter("Sexo")!=null)?request.getParameter("Sexo"):"";
			String r_matrimonial=(request.getParameter("catlogoRegimenMatrimonial")!=null)?request.getParameter("catlogoRegimenMatrimonial"):"";
			String Apellido_casada="";
			String txtTipoPersona=	(request.getParameter("tipoPersona")!=null)?request.getParameter("tipoPersona"):""; 
			String Grado_escolaridad=(request.getParameter("catalogoGradoEscolar")!=null)?request.getParameter("catalogoGradoEscolar"):"";
			String No_de_notaria="";
			String Apellido_paterno_L=(request.getParameter("apPaterno")!=null)?request.getParameter("apPaterno"):"";
			String Apellido_materno_L=(request.getParameter("apMaterno")!=null)?request.getParameter("apMaterno"):"";
			String Nombre_L=(request.getParameter("nombre")!=null)?request.getParameter("nombre"):"";
			String Tel_Rep_Legal=(request.getParameter("telefonoRep")!=null)?request.getParameter("telefonoRep"):"";
			String Fax_Rep_Legal=(request.getParameter("faxRep")!=null)?request.getParameter("faxRep"):"";
			String Email_Rep_Legal=(request.getParameter("emailRep")!=null)?request.getParameter("emailRep"):"";
			String RFC_legal=(request.getParameter("rfcRep")!=null)?request.getParameter("rfcRep"):"";
			String Tipo_categoria=(request.getParameter("catalogoTipoCategoria")!=null)?request.getParameter("catalogoTipoCategoria"):"";
			String Ejecutivo_de_cuenta="";
			String Empleos_a_generar="";
			String Activo_total="";
			String Capital_contable="";
			String Ventas_netas_exportacion="";
			String Fecha_de_Constitucion=(request.getParameter("fechaConstitucion")!=null)?request.getParameter("fechaConstitucion"):"";
			String Domicilio_Correspondencia="";
			String ic_domicilio=(request.getParameter("ic_domicilio")!=null)?request.getParameter("ic_domicilio"):"";
			String No_de_escritura=(request.getParameter("numEscritura")!=null)?request.getParameter("numEscritura"):"";
			String ic_contacto=(request.getParameter("claveContacto")!=null)?request.getParameter("claveContacto"):"";
			String Numero_de_cliente="";		//Numero de cliente interno
			String chkFinanciamiento="S";
			String chkInventario="";			//Activa inventarios
			String TipoAfiliacion= (request.getParameter("tipoAfiliacion")!=null)?request.getParameter("tipoAfiliacion"):"";			//B Basica, C Complementaria
			
			String Escritura_RL=(request.getParameter("numEscrituraRep")!=null)?request.getParameter("numEscrituraRep"):"";
			String FechaPoderNotarial_RL=(request.getParameter("fechaPoderNotarial")!=null)?request.getParameter("fechaPoderNotarial"):"";
			String  invalidar=(request.getParameter("invalidar")!=null)?request.getParameter("invalidar"):"";
			boolean invaldarAux=false;
				boolean actualizaSiracAux=false;
				if(invalidar.equals("on")){  invaldarAux  =true; }
			boolean actualizaSirac=(request.getParameter("actualizaSirac") != null &&
			request.getParameter("actualizaSirac").equals("S") )?true:false;
			String curp=(request.getParameter("curpFisica")!=null)?request.getParameter("curpFisica"):""; //Fodea campos PLD 2010
/*System.out.println(iNoUsuario+"\n"+ic_epo+"\n"+ic_pyme+"\n"+
Razon_Social+"\n"+
Nombre_Comercial+"\n"+	
R_F_C+"\n"+
chkDescuento+"\n"+
Num_Sirac+"\n"+
Numero_Exterior+"\n"+
Colonia+"\n"+
Estado+"\n"+
Fecha_de_nacimiento+"\n"+
chkAnticipo+"\n"+
Estado_civil+"\n"+
Pais_de_origen+"\n"+
TAfilia+"\n"+
Sector_economico+"\n"+
Numero_de_empleados+"\n"+
Apellido_paterno+"\n"+
Apellido_materno+"\n"+
Nombre+"\n"+
Telefono+"\n"+
Fax+"\n"+
Sexo+"\n"+
r_matrimonial+"\n"+
Apellido_casada+"\n"+
No_identificacion+"\n"+
txtTipoPersona+"\n"+
Calle+"\n"+
Codigo_postal+"\n"+
Delegacion_o_municipio+"\n"+
Pais+"\n"+
Grado_escolaridad+"\n"+
No_de_notaria+"\n"+
Apellido_paterno_L+"\n"+
Apellido_materno_L+"\n"+
Nombre_L+"\n"+
Tel_Rep_Legal+"\n"+
Fax_Rep_Legal+"\n"+
Email_Rep_Legal+"\n"+
RFC_legal+"\n"+
Identificacion+"\n"+
Tipo_categoria+"\n"+
Ejecutivo_de_cuenta+"\n"+
Empleos_a_generar+"\n"+
Activo_total+"\n"+
Capital_contable+"\n"+
Ventas_netas_totales+"\n"+
Ventas_netas_exportacion+"\n"+
Fecha_de_Constitucion+"\n"+
Subsector+"\n"+
Rama+"\n"+
Clase+"\n"+
Tipo_empresa+"\n"+
Domicilio_Correspondencia+"\n"+
Principales_productos+"\n"+
ic_domicilio+"\n"+
No_de_escritura+"\n"+
ic_contacto+"\n"+
Apellido_paterno_C+"\n"+
Apellido_materno_C+"\n"+
Nombre_C+"\n"+
Telefono_C+"\n"+
Email_C+"\n"+
Fax_C+"\n"+
Numero_de_cliente+"\n"+
Numero_de_distribuidor+"\n"+
chkFinanciamiento+"\n"+
chkInventario+"\n"+	
TipoAfiliacion+"\n"+
EMail+"\n"+
Escritura_RL+"\n"+
FechaPoderNotarial_RL+"\n"+
TipoCredito+"\n"+
strOficinaTramitadora+"\n"+
invaldarAux+"\n"+
actualizaSirac+"\n"+ 
curp+"\n"+ 
fiel+"\n"+ 
ciudad+"\n"+
con);*/
		mensaje=beanAfiliacion.actualizaPyme(iNoUsuario,ic_epo,ic_pyme,Razon_Social,Nombre_Comercial,
														R_F_C,chkDescuento,Num_Sirac,Numero_Exterior,Colonia,
														Estado,Fecha_de_nacimiento,chkAnticipo,Estado_civil,
														Pais_de_origen,TAfilia,Sector_economico,Numero_de_empleados,
														Apellido_paterno,Apellido_materno,Nombre,Telefono,Fax,Sexo,
														r_matrimonial,Apellido_casada,No_identificacion,txtTipoPersona,
														Calle,Codigo_postal,Delegacion_o_municipio,Pais,Grado_escolaridad,
														No_de_notaria,Apellido_paterno_L,Apellido_materno_L,Nombre_L,Tel_Rep_Legal,
														Fax_Rep_Legal,Email_Rep_Legal,RFC_legal,Identificacion,Tipo_categoria,
														Ejecutivo_de_cuenta,Empleos_a_generar,Activo_total,Capital_contable,
														Ventas_netas_totales,Ventas_netas_exportacion,Fecha_de_Constitucion,
														Subsector,Rama,Clase,Tipo_empresa,Domicilio_Correspondencia,
														Principales_productos,ic_domicilio,No_de_escritura,ic_contacto,
														Apellido_paterno_C,Apellido_materno_C,Nombre_C,Telefono_C,Email_C,
														Fax_C,Numero_de_cliente,Numero_de_distribuidor,chkFinanciamiento,
														chkInventario,	TipoAfiliacion,EMail,Escritura_RL,FechaPoderNotarial_RL,
														TipoCredito,strOficinaTramitadora,invaldarAux,actualizaSirac, curp, 
														fiel, ciudad,con);
				
				JSONObject resultado = new JSONObject();
				resultado.put("success", new Boolean(true));
				resultado.put("msg", mensaje);
				infoRegresar = resultado.toString();		
		
	} catch(Exception e) { 
		throw new AppException("Error en la afiliacion", e);
	}finally{
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}

}else if (informacion.equals("validaPermiso")) {
	
	jsonObj = new JSONObject();
	
	seguridadBean.validaPermiso(  iTipoPerfil,  strPerfil,  idMenuP,  ccPermiso,  strLogin );  
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 
	jsonObj.put("ccPermiso",  ccPermiso); 	
	infoRegresar = jsonObj.toString();	
	

}

%>

<%=infoRegresar%>

