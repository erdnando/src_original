<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.text.*,
	java.util.*,
	java.io.File,
	org.apache.commons.logging.Log,
	com.netro.pdf.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.*,
	com.netro.cadenas.*,
	com.netro.afiliacion.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion")  == null?"":(String)request.getParameter("informacion");
String icPyme      = request.getParameter("ic_pyme")      == null?"":(String)request.getParameter("ic_pyme");
String icEpo       = request.getParameter("ic_epo")       == null?"":(String)request.getParameter("ic_epo");
String filtroEpo   = request.getParameter("filtro_epo")   == null?"":(String)request.getParameter("filtro_epo");
String rfc         = request.getParameter("rfc")          == null?"":(String)request.getParameter("rfc");
String tipoPersona = request.getParameter("tipo_persona") == null?"":(String)request.getParameter("tipo_persona");
String afiliacion  = request.getParameter("afiliacion")   == null?"":(String)request.getParameter("afiliacion");
String datosGrid   = request.getParameter("datos")        == null?"":(String)request.getParameter("datos");
String pymeEpoInt  = request.getParameter("pyme_epo_int") == null?"":(String)request.getParameter("pyme_epo_int");
String csAceptacion= request.getParameter("cs_aceptacion")== null?"":(String)request.getParameter("cs_aceptacion");

String consulta      = "";
String mensaje       = "";
String infoRegresar  = "";
JSONArray jsonArr    = new JSONArray();
JSONObject resultado = new JSONObject();
boolean success      = true;

log.debug("<<<informacion: " + informacion + ">>>>");
log.debug("<<<icPyme: "      + icPyme      + ">>>>");
log.debug("<<<icEpo: "       + icEpo       + ">>>>");
log.debug("<<<filtroEpo: "   + filtroEpo   + ">>>>");

if(informacion.equals("CATALOGO_EPO")){

	CatalogoEpoXProductoReafiliacion cat = new CatalogoEpoXProductoReafiliacion();
	cat.setClave("IC_EPO");
	cat.setDescripcion("CG_RAZON_SOCIAL");
	cat.setIcPyme(Integer.parseInt(icPyme));
	cat.setFiltroEpo(filtroEpo);
	List elementos = cat.getListaElementos();

	HashMap  datosCombo     = new HashMap();
	Iterator it             = elementos.iterator();
	String   descripcion    = "";
	String   clave          = "";
	String   textoBuscado   = " ";
	String   descripcionTmp = "";
	int      contador       = 0;

	while(it.hasNext()){
		Object obj = it.next();
		descripcionTmp = "";
		contador = 0;
		if (obj instanceof netropology.utilerias.ElementoCatalogo){
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			clave = ec.getClave();
			descripcion = ec.getDescripcion();
			if(descripcion.length() > 45){
				descripcionTmp = descripcion.substring(0,45);
				contador = descripcionTmp.lastIndexOf(textoBuscado);
				descripcion = descripcion.substring(0,contador) + "<br>" + descripcion.substring(contador,descripcion.length());
			}
			datosCombo = new HashMap();
			datosCombo.put("clave", clave);
			datosCombo.put("descripcion", descripcion);
			jsonArr.add(datosCombo);
		}
	}

	infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

} else if(informacion.equals("CATALOGO_TIPO_CREDIT0")){

	String tipoCredito = "";
	HashMap  datosCombo = new HashMap();

	if(!icEpo.equals("")){
		ConsultaDistribuidoresReafiliacion reafilia = new ConsultaDistribuidoresReafiliacion();
		reafilia.setIcEpo(icEpo);
		tipoCredito = reafilia.getTipoCredito();
	} else{
		tipoCredito = "";
	}

	if(tipoCredito.equals("D")){
		datosCombo = new HashMap();
		datosCombo.put("clave", "D");
		datosCombo.put("descripcion", "Modalidad 1 (Riesgo Empresa de Primer Orden)");
		jsonArr.add(datosCombo);
	} else if(tipoCredito.equals("C")){
		datosCombo.put("clave", "C");
		datosCombo.put("descripcion", "Modalidad 2 (Riesgo Distribuidor)");
		jsonArr.add(datosCombo);
	} else if(tipoCredito.equals("A")){
		datosCombo = new HashMap();
		datosCombo.put("clave", "A");
		datosCombo.put("descripcion", "Ambos");
		jsonArr.add(datosCombo);
		datosCombo = new HashMap();
		datosCombo.put("clave", "D");
		datosCombo.put("descripcion", "Modalidad 1 (Riesgo Empresa de Primer Orden)");
		jsonArr.add(datosCombo);
		datosCombo.put("clave", "C");
		datosCombo.put("descripcion", "Modalidad 2 (Riesgo Distribuidor)");
		jsonArr.add(datosCombo);
	} else if(tipoCredito.equals("F")){
		datosCombo.put("clave", "F");
		datosCombo.put("descripcion", "Factoraje con recurso Propio");
		jsonArr.add(datosCombo);
	} else{
		datosCombo = new HashMap();
		datosCombo.put("clave", "A");
		datosCombo.put("descripcion", "Ambos");
		jsonArr.add(datosCombo);
		datosCombo = new HashMap();
		datosCombo.put("clave", "D");
		datosCombo.put("descripcion", "Modalidad 1 (Riesgo Empresa de Primer Orden)");
		jsonArr.add(datosCombo);
		datosCombo.put("clave", "C");
		datosCombo.put("descripcion", "Modalidad 2 (Riesgo Distribuidor)");
		jsonArr.add(datosCombo);
		datosCombo = new HashMap();
		datosCombo.put("clave", "F");
		datosCombo.put("descripcion", "Factoraje con recurso Propio");
		jsonArr.add(datosCombo);
	}

	infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

} else if(informacion.equals("CONSULTA_DATA")){

	ConsultaDistribuidoresReafiliacion reafilia = new ConsultaDistribuidoresReafiliacion();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(reafilia);
	reafilia.setIcPyme(icPyme);
	reafilia.setIcEpo(icEpo);
	Registros listaRegistros = new Registros();
	listaRegistros = queryHelper.doSearch();
	infoRegresar =  "{\"success\": true, \"total\": \"" + listaRegistros.getNumeroRegistros()+ "\", \"registros\": " + listaRegistros.getJSONData()+"}";

} else if(informacion.equals("REAFILIA_EPO")){

	Afiliacion beanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

	JSONArray arrayReg       = JSONArray.fromObject(datosGrid);
	HashMap mapaReafiliacion = new HashMap();
	List listaReafiliacion   = new ArrayList();

	if(arrayReg.size() > 0){
		for(int i=0; i<arrayReg.size(); i++){
			JSONObject auxiliar = arrayReg.getJSONObject(i);
			mapaReafiliacion = new HashMap();
			mapaReafiliacion.put("IC_PYME",             icPyme);
			mapaReafiliacion.put("PYME_EPO_INTERNO",    pymeEpoInt);
			mapaReafiliacion.put("CS_ACEPTACION",       csAceptacion);
			mapaReafiliacion.put("IC_EPO",              auxiliar.getString("IC_EPO"));
			mapaReafiliacion.put("CADENA_PRODUCTIVA",   auxiliar.getString("CADENA_PRODUCTIVA"));
			mapaReafiliacion.put("NAFIN_ELECTRONICO",   auxiliar.getString("NAFIN_ELECTRONICO"));
			mapaReafiliacion.put("RAZON_SOCIAL",        auxiliar.getString("RAZON_SOCIAL"));
			mapaReafiliacion.put("RFC",                 auxiliar.getString("RFC"));
			mapaReafiliacion.put("NUMERO_DISTRIBUIDOR", auxiliar.getString("NUMERO_DISTRIBUIDOR"));
			if(auxiliar.getString("TIPO_CREDITO").equals("Modalidad 2 (Riesgo Distribuidor)")){
				mapaReafiliacion.put("TIPO_CREDITO",     "C");
			} else if(auxiliar.getString("TIPO_CREDITO").equals("Modalidad 1 (Riesgo Empresa de Primer Orden)")){
				mapaReafiliacion.put("TIPO_CREDITO",     "D");
			} else if(auxiliar.getString("TIPO_CREDITO").equals("Factoraje con recurso Propio")){
				mapaReafiliacion.put("TIPO_CREDITO",     "F");
			} else if(auxiliar.getString("TIPO_CREDITO").equals("Ambos")){
				mapaReafiliacion.put("TIPO_CREDITO",     "A");
			} else{
				mapaReafiliacion.put("TIPO_CREDITO",     "N");
			}
			listaReafiliacion.add(mapaReafiliacion);
		}
	}

	if(!listaReafiliacion.isEmpty()){
		mensaje = beanAfiliacion.afiliaPyme(iNoUsuario, listaReafiliacion);
	} else{
		mensaje = "No se encontraron registros para afiliar.";
	}

	resultado.put("success", new Boolean(true));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();

}
%>
<%=infoRegresar%>