Ext.onReady(function(){


//--------------------------------HANDLERS-----------------------------------
	var seleccionCombo = function(combo){
		var cmbEPO = Ext.getCmp('cmbNombreEPO').getValue();
		if(combo.id == 'cmbNombreEPO'){
			catalogoIF.load({
				params:{
					informacion: 'consultaIF',
					claveEPO: cmbEPO
				}
			});
		}
		if(combo.id == 'cmbNumNE'){
			var boton = Ext.getCmp('btnAceptar');
			if(combo.getValue() == '')
				boton.hide();
			else
				boton.show();
		}
	}
	
	var procesarBusquedaAvanzada = function(store, arrRegistros, opts){
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
		}
	}
	
	var consultarNombrePyme = function(){
		var numPyme = Ext.getCmp('txtNumPYME');
		var epo = Ext.getCmp('cmbNombreEPO');
		Ext.Ajax.request({
			url: '15ConsAfiliaElecNAFIN01ext.data.jsp',
			params:{
				informacion: 'obtenerNombrePyme',
				claveEPO: epo.getValue(),
				numPyme: numPyme.getValue()
			},
			callback: procesaConsultaNombrePyme
		});
	}
	
	var procesaConsultaNombrePyme = function(opts, success, response){
		var cadenaPyme = Ext.getCmp('txtCadenaPYME');
		var numPyme = Ext.getCmp('txtNumPYME');
		var idPyme = Ext.getCmp('txtPYME');
		if (success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			if (resp.muestraMensaje != undefined && resp.muestraMensaje){
				Ext.Msg.alert("Mensaje de p�gina web.",	resp.textoMensaje);
				numPyme.reset();
				cadenaPyme.reset();
			}else{
				cadenaPyme.setValue(resp.nombrePyme);
				idPyme.setValue(resp.idPyme);
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var realizarConsultaGeneral = function(){
		var claveEpo = Ext.getCmp('cmbNombreEPO');
		var claveIF = Ext.getCmp('cmbNombreIF');
		var clavePyme = Ext.getCmp('txtPYME');		
		var claveEstatus = Ext.getCmp('cmbEstatus');
		var fechaInicial = Ext.getCmp('fechaInicio');
		var fechaFinal = Ext.getCmp('fechaFin');
		var banderaAux = 0;
		if(Ext.isEmpty(fechaInicial.getValue()) && Ext.isEmpty(fechaFinal.getValue())){
			banderaAux = 1;
		}else if(!Ext.isEmpty(fechaInicial.getValue()) || !Ext.isEmpty(fechaFinal.getValue())){			
			if(Ext.isEmpty(fechaInicial.getValue())){
				fechaInicial.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
				fechaInicial.focus();
				return;
			}else	if(fechaInicial.isValid(false)){
				if(!isdate(fechaInicial.getRawValue())){
					fechaInicial.markInvalid("La fecha es incorrecta. Verifique que el formato sea dd/mm/aaaa");
					return;
				}
			}
			
			if(Ext.isEmpty(fechaFinal.getValue())){
				fechaFinal.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
				fechaFinal.focus();
				return;
			}else	if(fechaFinal.isValid(false)){
				if(!isdate(fechaFinal.getRawValue())){
					fechaFinal.markInvalid("La fecha es incorrecta. Verifique que el formato sea dd/mm/aaaa");
					return
				}
			}
			
			
			if(!Ext.isEmpty(fechaInicial.getValue()) && !Ext.isEmpty(fechaFinal.getValue())){
				if(fechaInicial.getValue() > fechaFinal.getValue()){
					fechaInicial.markInvalid('Las fechas de ejecuci�n deben ir de menor a mayor');
				return;
				}else{
					banderaAux = 1;
				}
			}
		}
		if(banderaAux == 1){
			consultaData.load({
				url: '15ConsAfiliaElecNAFIN01ext.data.jsp',
				params:{
					informacion: 'consultaGeneral',
					claveEPO: claveEpo.getValue(),
					numIF: claveIF.getValue(),
					clavePyme: clavePyme.getValue(),
					claveEstatus: claveEstatus.getValue(),
					fechaInicio: fechaInicial.getRawValue(),
					fechaFin: fechaFinal.getRawValue()
				}
			//	callback: procesaConsultaNombrePyme
			});
		}
	}
	
	var generarArchivo = function(){
		var claveEpo = Ext.getCmp('cmbNombreEPO');
		var claveIF = Ext.getCmp('cmbNombreIF');
		var clavePyme = Ext.getCmp('txtPYME');		
		var claveEstatus = Ext.getCmp('cmbEstatus');
		var fechaInicial = Ext.getCmp('fechaInicio');
		var fechaFinal = Ext.getCmp('fechaFin');
		
		Ext.Ajax.request({
			url: '15ConsAfiliaElecNAFIN01ext.data.jsp',	
			params: {
				informacion: "ArchivoCSV",	
				claveEPO: claveEpo.getValue(),
				numIF: claveIF.getValue(),
				clavePyme: clavePyme.getValue(),
				claveEstatus: claveEstatus.getValue(),
				fechaInicio: fechaInicial.getRawValue(),
				fechaFin: fechaFinal.getRawValue()
			},
			callback: procesarDescargaArchivo
		});
	}
	
	var procesarDescargaArchivo = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			Ext.getCmp('btnArchivo').enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	
	var verExpediente = function(grid, rowIndex, colIndex, item, event){
		var registro = gridGeneral.getStore().getAt(rowIndex);
		var rfc = registro.get('RFC');		
		fp.getForm().getEl().dom.action = '/nafin/15cadenas/15mantenimiento/15clientes/popupexpediente.jsp?rfc='+escape(rfc)+'&version=ext';
		fp.getForm().getEl().dom.submit();
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts){
		var el = gridGeneral.getGridEl();
		if(consultaData.getTotalCount() > 0){
			el.unmask();
			Ext.getCmp('btnArchivo').enable();
		//	Ext.getCmp('btnActualizar').enable();
		}else{
			Ext.getCmp('btnArchivo').disable();
		//	Ext.getCmp('btnActualizar').disable();
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
		gridGeneral.show();
	}
//---------------------------------STORES------------------------------------

	//STORE PARA COMBO NOMBRE EPO
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15ConsAfiliaElecNAFIN01ext.data.jsp',
		baseParams: {
			informacion: 'consultaEPO'
		},
		totalProperty: 'Total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	//STORE PARA COMBO NOMBRE IF
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15ConsAfiliaElecNAFIN01ext.data.jsp',
		baseParams: {
			informacion: 'consultaIF'
		},
		totalProperty: 'Total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	//STORE PARA COMBO ESTATUS
	var storeComboEstatus = new Ext.data.SimpleStore({
		fields: ['clave', 'estatus'],
		data:[
			['E','Enviado'],
			['A','Autorizado']
		]
	});
	
	//STORE PARA COMBO DE BUSQUEDA AVANZADA
	var catalogoBusquedaAvanzada = new Ext.data.JsonStore({
		id: 'catalogoBusquedaAvanzada',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ConsAfiliaElecNAFIN01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoBusquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarBusquedaAvanzada,
			beforeload: NE.util.initMensajeCargaCombo,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15ConsAfiliaElecNAFIN01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGeneral'
		},
		fields: [
			{name: 'BANCO_FONDEO'},
			{name: 'NOMBRE_IF'},					
			{name: 'NOMBRE_EPO'},
			{name: 'NAFIN_E'}, 
			{name: 'RSOCIAL'},					
			{name: 'RFC'},
			{name: 'DOMICILIO'},
			{name: 'FECHA_SOLIC'},
			{name: 'MONEDA'},
			{name: 'CTA_BANCARIA'},
			{name: 'BANCO_SERVICIO'},
			{name: 'SUCURSAL'},
			{name: 'PLAZA'},
			{name: 'ESTATUS'},
			{name: 'EXPEDIENTE'},
			{name: 'CAUSA_RECHAZO'},
			{name: 'NOTAS'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			//beforesave :{fn: function(){alert();}},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});
	
//------------------------------COMPONENTES----------------------------------
	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		id: 'grupoHeaders',
		rows: [
			[
				{header: '', colspan: 2, align: 'center'},
				{header: 'Datos de la PyME', colspan: 5, align: 'center'},
				{header: 'Datos de la Cuenta Bancaria', colspan: 6, align: 'center'},
				{header: '', colspan: 4, align: 'center'}
			]
		]
	});
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: '_cmbNombreEPO',
			id: 'cmbNombreEPO',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local',
			width: '100%',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cmb_nombre_epo',
			emptyText: 'Seleccionar EPO',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select: seleccionCombo
			}
		},{
			xtype: 'combo',
			name: '_cmbNombreIF',
			id: 'cmbNombreIF',
			fieldLabel: 'Nombre del IF',
			mode: 'local',
			anchor: '70%',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cmb_nombre_if',
			emptyText: 'Seleccionar IF',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoIF,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'compositefield',
			id:'nombrePyme',
			combineErrors: false,
			anchor: '100%',
			msgTarget: 'side',
			fieldLabel: 'Nombre de la PYME',
			items: [
				{
					xtype:		'textfield',
					name:			'_txtNumPYME',
					id:			'txtNumPYME',
					hiddenName:		'txt_num_pyme',
					maskRe:		/[0-9]/,
					allowBlank:	true,
					maxLength:	10,
					margins: '0 20 0 0',
					listeners:{
						'change': consultarNombrePyme
					}
				}, {
					xtype:			'textfield',
					name:				'_txtCadenaPYME',
					id:				'txtCadenaPYME',
					hiddenName:		'txt_cadena_pyme',
					width:			350,
					disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
					disabled:		true,
					allowBlank:		true,
					maxLength:		100
				}
			]
		},{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'busAvanzada',
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					id:'disEspacio',
					text:''
				},
				{
					xtype: 'button',
					text: 'B�squeda Avanzada...',
					name: '_btnBusquedaAvanzada',
					id: 'btnBusquedaAvanzada',
					hiddenName: 'btn_busqueda_avanzada',
					iconCls: 'icoBuscar',
					handler: function(boton, evento){
						var epo = Ext.getCmp("txtEPO");
						var nomEpo = Ext.getCmp('cmbNombreEPO');
						epo.setValue(nomEpo.getValue());
						if(nomEpo.getValue() == '0' || nomEpo.getValue() == ''){
							Ext.getCmp('txtNumProveedor').hide();
						}else{
							Ext.getCmp('txtNumProveedor').show();
						}
						Ext.getCmp('winBusqueda').show();
					}
				}
			]
		},{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '100%',
			items:[			
				{
					xtype: 'datefield',
					fieldLabel: 'Fecha de Solicitud de',
					name: '_fechaInicio',
					id: 'fechaInicio',
					hiddenName: 'fecha_inicio',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 10
				},
				{
					xtype: 'datefield',
					name: '_fechaFin',
					id: 'fechaFin',
					hiddenName: 'fecha_fin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		},{
			xtype: 'combo',
			name: '_cmbEstatus',
			id: 'cmbEstatus',
			fieldLabel: 'Estatus',
			anchor: '40%',
			mode: 'local', 
			displayField : 'estatus',
			valueField : 'clave',
			hiddenName : 'cmb_Estatus',
			emptyText: 'Seleccionar...',
			forceSelection : true,
			emptyText: 'Seleccionar...',
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeComboEstatus	
		},
		{
			xtype:		'textfield',
			fieldLabel: 'IC PYME',
			name:			'txt_pyme',
			id:			'txtPYME',
			hiddenName:		'txt_PYME',
			hidden: true,
			allowBlank:	true,
			anchor: '40%'
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 140,
		labelAlign : 'right',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid: true,
		buttons: [		
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento){
					gridGeneral.show();
					realizarConsultaGeneral();
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				formBind: false,
				handler: function(boton, evento){
					Ext.getCmp('forma').getForm().reset();
					Ext.getCmp('gridGeneral').hide();
				}
			}
		]
	});
	
	
	var elementosBusquedaAvanzada = [		
		{
			xtype:'form',
			id:'fpWinBusca',
			frame: true,
			labelAlign : 'right',
			border: false,
			style: 'margin: 0 auto',
			bodyStyle:'padding:10px',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			labelWidth: 130,
			items:[
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					value:'Utilice el * para b�squeda gen�rica'
				},{
					xtype:		'textfield',
					fieldLabel: 'EPO',
					name:			'cmb_nombre_epo',
					id:			'txtEPO',
					hiddenName:		'txt_epo',
					hidden: true,
					allowBlank:	true
				},{
					xtype:		'textfield',
					fieldLabel: 'Nombre',
					name:			'_txtNombre',
					id:			'txtNombre',
					hiddenName:		'txt_nombre',
					allowBlank:	true
				},
				{
					xtype:		'textfield',
					fieldLabel: 'RFC',
					name:			'_txtRFC',
					id:			'txtRFC',
					hiddenName:		'txt_RFC',
					maxLength:	20,
					allowBlank:	true
				},
				{
					xtype:		'textfield',
					fieldLabel: 'N�mero de Proveedor',
					name:			'_txtNumProveedor',
					id:			'txtNumProveedor',
					hiddenName:		'txt_num_proveedor',
					maxLength:	20,
					allowBlank:	true
				}
			],
			buttonAlign:'center',
			buttons:[
				{
					text:'Buscar',
					iconCls:'icoBuscar',
					handler: function(boton) {
						var epo = Ext.getCmp("txtEPO");
						var nombre = Ext.getCmp("txtNombre");
						var rfc = Ext.getCmp("txtRFC");
						var proveedor = Ext.getCmp("txtNumProveedor");
						catalogoBusquedaAvanzada.load({
							params: {
								claveEPO: epo.getValue(),
								txtNombre: nombre.getValue(),
								txtRFC: rfc.getValue(),
								txtNumProveedor: proveedor.getValue()
							}
						});
					}
				},{
					text:'Cancelar',
					iconCls: 'icoCancelar',
					handler: function() {
						Ext.getCmp('fpWinBusca').getForm().reset();
						Ext.getCmp('fpWinBuscaB').getForm().reset();
						Ext.getCmp('winBusqueda').hide();
					}
				}
			]
		},{
			xtype:'form',
			id:'fpWinBuscaB',
			frame: true,
			border: false,
			labelAlign : 'right',
			style: 'margin: 0 auto',
			bodyStyle:'padding:10px',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			labelWidth: 130,
			items:[
				{
					xtype: 'combo',
					id:	'cmbNumNE',
					name: 'ic_pyme',
					hiddenName : 'ic_pyme',
					fieldLabel: 'Nombre',
					emptyText: 'Seleccione. . .',
					displayField: 'descripcion',
					valueField: 'clave',
					triggerAction : 'all',
					forceSelection:true,
					allowBlank: false,
					typeAhead: true,
					mode: 'local',
					minChars : 1,
					store: catalogoBusquedaAvanzada,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners:{
						select: seleccionCombo
					}
				}
			],
			buttonAlign:'center',
			buttons:[
				{
					text:'Aceptar',
					iconCls:'aceptar',
					id: 'btnAceptar',
					hidden: true,
					handler: function() {
						var claveProd = Ext.getCmp('cmbNumNE');
						var txtNumPyme = Ext.getCmp('txtNumPYME');
						if (!Ext.isEmpty(claveProd.getValue())){
							txtNumPyme.setValue(claveProd.getValue());
							Ext.getCmp('fpWinBusca').getForm().reset();
							Ext.getCmp('fpWinBuscaB').getForm().reset();
							Ext.getCmp('winBusqueda').hide();
							txtNumPyme.focus();						
						}
						consultarNombrePyme();
					}
				},{
					text:'Cancelar',
					iconCls: 'icoCancelar',
					handler: function() {
						Ext.getCmp('fpWinBusca').getForm().reset();
						Ext.getCmp('fpWinBuscaB').getForm().reset();
						Ext.getCmp('winBusqueda').hide();	
					}
				}
			]
		}
	];
	
	var ventanaBusqueda = new Ext.Window({
		id: 'winBusqueda',
		width: 550,
		height: 340,
		modal: true,
		labelAlign : 'right',
		closeAction: 'hide',
		title: '.:: Nafinet ::.',
		items:[
			elementosBusquedaAvanzada
		]
	});
	
	var gridGeneral = new Ext.grid.GridPanel({
		store: consultaData,
		style: 'margin: 0 auto;',
		hidden: true,
		plugins:	grupos,
      id: 'gridGeneral',
		columns: [		
			{
				header: 'Banco de Fondeo',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'BANCO_FONDEO'
			},{
				header: 'Nombre del IF',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'NOMBRE_IF'
			},{
				header: 'Nombre EPO',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'NOMBRE_EPO'
			},{
				header: 'Nafin Electr�nico',
				sortable: true,
				resizable: true,
				align: 'center',
				dataIndex: 'NAFIN_E'
			},{
				header: 'Nombre o Raz�n Social',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'RSOCIAL'
			},{
				header: 'RFC',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'RFC'
			},{
				header: 'Domicilio',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'DOMICILIO'
			},{
				header: 'Fecha de Solicitud',
				sortable: true,
				resizable: true,
				align: 'center',
				dataIndex: 'FECHA_SOLIC'
			},{
				header: 'Moneda',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'MONEDA'
			},{
				header: 'Cuenta Bancaria',
				sortable: true,
				resizable: true,
				align: 'center',
				dataIndex: 'CTA_BANCARIA'
			},{
				header: 'Banco de Servicio',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'BANCO_SERVICIO'
			},{
				header: 'Sucursal',
				sortable: true,
				resizable: true,
				align: 'center',
				dataIndex: 'SUCURSAL'
			},{
				header: 'Plaza',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'PLAZA'
			},{
				header: 'Estatus',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'ESTATUS',
				renderer:  function (value,metaData,registro,rowIndex,colIndex,store){
					if(value == 'E')
						return 'Enviado';
					if(value == 'A')
						return 'Autorizado';			
				}
			},{
				xtype: 'actioncolumn',
				header: 'Expediente',
				sortable: true,
				resizable: true,
				align: 'center',
				dataIndex: 'EXPEDIENTE',
				renderer:  function (value,metaData,registro,rowIndex,colIndex,store){
					if(value == 'N/A')
						return value;
					else
						return 'Buscar ';
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver Detalle';
							if(valor != 'N/A')
								return 'iconoLupa';										
						},
						handler: verExpediente
					}
				]	
			},{
				header: 'Causas de Rechazo',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'CAUSA_RECHAZO'
			},{
				header: 'Notas',
				sortable: true,
				resizable: true,
				align: 'left',
				dataIndex: 'NOTAS'
			}
		],
		stripeRows: true,
	   loadMask: true,
		frame: true,
		autoWidth: true,
		height: 400,
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnArchivo',
					iconCls: 'icoXls',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						generarArchivo();
					}
				}
			]
		}
		
	});
	
//-------------------------COMPONENTE PRINCIPAL------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 950,
		height: 'auto',
		items: [
			fp,
			ventanaBusqueda,
			NE.util.getEspaciador(10),
			gridGeneral
		]
	});
	
	catalogoEPO.load();
	catalogoIF.load();
});