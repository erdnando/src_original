Ext.onReady(function() {

	function procesarArchivoPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	function procesarArchivoCSV(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');		
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;
					
			if(store.getTotalCount() > 0) {		
				el.unmask();					
				Ext.getCmp('btnGenerarCSV').enable();
			Ext.getCmp('btnGenerarPDF').enable();
			} else {	
				Ext.getCmp('btnGenerarCSV').disable();
				Ext.getCmp('btnGenerarPDF').disable();
				el.mask('No se encontró ningún registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15reafiliacion_aut_cons.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_PYME'},
			{name: 'NOMBRE_IF'},
			{name: 'MONEDA'},
			{name: 'NO_CUENTA'},
			{name: 'FECHA_REAFILIACION'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		//clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},	
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},		
					
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. Cuenta',
				tooltip: 'No. Cuenta',
				dataIndex: 'NO_CUENTA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Fecha Reafiliación',
				tooltip: 'Fecha Reafiliación',
				dataIndex: 'FECHA_REAFILIACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,				
		bbar: {
			items: [
				'->','-'	,
						{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15reafiliacion_aut_cons.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'GenerarPDF'							
							}),
							callback: procesarArchivoPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
					
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15reafiliacion_aut_cons.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'GenerarCSV'							
							}),
							callback: procesarArchivoCSV
						});
					}
				}		
			]
		}
	});
	

	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				
				Ext.getCmp("fechaIni").setValue(jsonData.fecha_hoy);
				Ext.getCmp("fechaFin").setValue(jsonData.fecha_hoy);
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	/**********CRITERIOS DE BUSQUEDA ***************/		

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: 'Captura',			
				id: 'btnCaptura',					
				handler: function() {
					window.location = '15reafiliacion_autExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<b>Consulta</b>',			
				id: 'btnConsulta',					
				handler: function() {
					window.location = '15reafiliacion_aut_consExt.jsp';
				}
			}	
		]
	});
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15reafiliacion_aut_cons.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15reafiliacion_aut_cons.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementosForma=[
		{
			xtype: 'combo',
			fieldLabel: 'EPO',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar EPO...',			
			valueField: 'clave',	
			hiddenName : 'ic_epo',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						
						var cmbIF = Ext.getCmp('ic_if1');
						cmbIF.setValue('');
						cmbIF.store.load({
							params: {
								ic_epo:combo.getValue()
							}
						});				
					}
				}
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'IF',
			name: 'ic_if',
			id: 'ic_if1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar IF ...',			
			valueField: 'clave',	
			hiddenName : 'ic_if',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIF,
			tpl : NE.util.templateMensajeCargaCombo		
		},
			{
			xtype: 'compositefield',
			fieldLabel: 'Fecha',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaIni',
					id: 'fechaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaFin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaFin',
					id: 'fechaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fechaIni',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Consulta Reafiliaciones Automaticas',
		frame: true,
		//collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar ',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function() {	
				
					var fechaIni = Ext.getCmp('fechaIni');
					var fechaFin = Ext.getCmp('fechaFin');
				
					if(!Ext.isEmpty(fechaIni.getValue()) || !Ext.isEmpty(fechaFin.getValue())){					
						if(Ext.isEmpty(fechaIni.getValue()) ){					
							fechaIni.markInvalid('Debe capturar ambas fechas ');
							fechaIni.focus();		
							return;	
						}
						if(Ext.isEmpty(fechaFin.getValue()) ){	
							fechaFin.markInvalid('Debe capturar ambas fechas ');
							fechaFin.focus();
							return;						
						}						
					}
					
					if(Ext.isEmpty(fechaIni.getValue()) || Ext.isEmpty(fechaFin.getValue())){	
						if(Ext.isEmpty(fechaIni.getValue()) ){					
							fechaIni.markInvalid('Debe capturar ambas fechas ');
							fechaIni.focus();		
							return;	
						}
						if(Ext.isEmpty(fechaFin.getValue()) ){	
							fechaFin.markInvalid('Debe capturar ambas fechas ');
							fechaFin.focus();
							return;						
						}	
					}
					
					var fechaIni_ = Ext.util.Format.date(fechaIni.getValue(),'d/m/Y');
					var fechaFin_ = Ext.util.Format.date(fechaFin.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(fechaIni.getValue())){
						if(!isdate(fechaIni_)) { 
							fechaIni.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fechaIni.focus();
						return;
						}
					}
					
					if( !Ext.isEmpty(fechaFin.getValue())){
						if(!isdate(fechaFin_)) { 
						fechaFin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
						fechaFin.focus();
						return;
						}
					}

					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'						
						})
					});					
				}		
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
				window.location = '15reafiliacion_aut_consExt.jsp';								
				}		
			}
		]			
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			fpBotones,
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),	
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	
		//Peticion para obtener valores iniciales y la parametrización
	Ext.Ajax.request({
		url: '15reafiliacion_aut_cons.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});

	catalogoEPO.load();
	catalogoIF.load();


});
