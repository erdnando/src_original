<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		com.netro.model.catalogos.CatalogoEstado,
		com.netro.model.catalogos.CatalogoMunicipioB,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	JSONObject resultado = new JSONObject();


	if(informacion.equals("catalogoIntermediario"))	{
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_if");
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("catalogoPaisOrig"))	{
		ArrayList lstIn = new ArrayList();
		lstIn.add("52");
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setCondicionNotIn(lstIn);
		cat.setOrden("cd_descripcion");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("catalogoPais"))	{
		ArrayList lstIn = new ArrayList();
		lstIn.add("24");
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setCondicionIn(lstIn);
		cat.setOrden("cd_descripcion");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("catalogoEstado"))	{
		String pais = (request.getParameter("cmbPais")!=null)?request.getParameter("cmbPais"):"";
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre");
		cat.setPais(pais);
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("catalogoMunicipio"))	{
		String pais = (request.getParameter("cmbPais")!=null)?request.getParameter("cmbPais"):"";
		String estado = (request.getParameter("cmbEstado")!=null)?request.getParameter("cmbEstado"):"";
		CatalogoMunicipioB cat = new CatalogoMunicipioB();
		cat.setCampoClave("ic_municipio");
		cat.setCampoDescripcion("cd_nombre");
		cat.setClavePais(pais);
		cat.setClaveEstado(estado);
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("catalogoCiudad"))	{
		String pais = (request.getParameter("cmbPais")!=null)?request.getParameter("cmbPais"):"";
		String estado = (request.getParameter("cmbEstado")!=null)?request.getParameter("cmbEstado"):"";
		String municipio = (request.getParameter("cmbMunicipio")!=null)?request.getParameter("cmbMunicipio"):"";
		
		CatalogoCiudad cat = new CatalogoCiudad();
		cat.setTabla("comcat_ciudad");
		cat.setCampoClave("codigo_ciudad");
		cat.setCampoDescripcion("nombre");
		cat.setPais(pais);
		cat.setEstado(estado);
		cat.setOrden("nombre");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("catalogoSector"))	{
		ArrayList lstIn = new ArrayList();
		lstIn.add("1");
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_sector_econ");
		cat.setCampoClave("IC_SECTOR_ECON");
		cat.setCampoDescripcion("IC_SECTOR_ECON || ' ' || CD_NOMBRE");
		cat.setCondicionNotIn(lstIn);
		cat.setOrden("IC_SECTOR_ECON");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("catalogoSubSector"))	{
		String sector = (request.getParameter("cmbSector")!=null)?request.getParameter("cmbSector"):"";
		CatalogoSubSector cat = new CatalogoSubSector();
		cat.setTabla("comcat_subsector");
		cat.setCampoClave("ic_subsector");
		cat.setCampoDescripcion("IC_SUBSECTOR || ' ' || CD_NOMBRE");
		cat.setSector(sector);
		cat.setOrden("ic_subsector");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("catalogoRama"))	{
		String sector = (request.getParameter("cmbSector")!=null)?request.getParameter("cmbSector"):"";
		String subSector = (request.getParameter("cmbSubSector")!=null)?request.getParameter("cmbSubSector"):"";
		CatalogoRama cat = new CatalogoRama();
		cat.setTabla("comcat_rama");
		cat.setCampoClave("ic_rama");
		cat.setCampoDescripcion("ic_rama || ' ' || cd_nombre");
		cat.setSector(sector);
		cat.setSubSector(subSector);
		cat.setOrden("ic_rama");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("catalogoClase"))	{
		String sector = (request.getParameter("cmbSector")!=null)?request.getParameter("cmbSector"):"";
		String subSector = (request.getParameter("cmbSubSector")!=null)?request.getParameter("cmbSubSector"):"";
		String rama = (request.getParameter("cmbRama")!=null)?request.getParameter("cmbRama"):"";
		
		CatalogoClase cat = new CatalogoClase();
		cat.setTabla("comcat_clase");
		cat.setCampoClave("ic_clase");
		cat.setCampoDescripcion("ic_clase || ' ' || cd_nombre");
		cat.setSector(sector);
		cat.setSubSector(subSector);
		cat.setRama(rama);
		cat.setOrden("ic_clase");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("enviarInfo")){
	
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		String TipoPersona = (request.getParameter("cmbTipoPersona")==null)?"":request.getParameter("cmbTipoPersona"),
		NoIf = (request.getParameter("cmbIntermediario")==null)?"":request.getParameter("cmbIntermediario"),
		
		// Persona Física.
		Apellido_paterno = (request.getParameter("fp_apPat")==null)?"":request.getParameter("fp_apPat").trim(),
		Apellido_materno = (request.getParameter("fp_apMat")==null)?"":request.getParameter("fp_apMat").trim(),
		Nombre = (request.getParameter("fp_nombreAfil")==null)?"":request.getParameter("fp_nombreAfil").trim(),
		Sexo = (request.getParameter("fp_sexo")==null)?"":request.getParameter("fp_sexo"),
		Fecha_de_nacimiento = (request.getParameter("fp_fecNacimiento")==null)?"":request.getParameter("fp_fecNacimiento").trim(),
		
		// Persona Moral.
		Razon_Social = (request.getParameter("fp_razonSocial")==null)?"":request.getParameter("fp_razonSocial").trim(),
		
		// Datos Generales para ambos Tipo de Persona.
		R_F_C = (request.getParameter("fp_rfcAfil")==null)?"":request.getParameter("fp_rfcAfil").trim(),
		Calle = (request.getParameter("fp_calle")==null)?"":request.getParameter("fp_calle").trim(),
		Colonia = (request.getParameter("fp_colonia")==null)?"":request.getParameter("fp_colonia").trim(),
		Estado = (request.getParameter("cmbEstado")==null)?"":request.getParameter("cmbEstado"),
		Delegacion_o_municipio = (request.getParameter("cmbMunicipio")==null)?"":request.getParameter("cmbMunicipio"),
		Codigo_postal = (request.getParameter("fp_codPost")==null)?"":request.getParameter("fp_codPost").trim(),
		Pais = (request.getParameter("cmbPais")==null)?"":request.getParameter("cmbPais"),
		Ventas_netas_totales = (request.getParameter("fp_ventas")==null)?"":request.getParameter("fp_ventas").trim(),
		Numero_de_empleados = (request.getParameter("fp_numEmp")==null)?"":request.getParameter("fp_numEmp").trim(),
		Sector_economico = (request.getParameter("cmbSector")==null)?"":request.getParameter("cmbSector"),
		Subsector = (request.getParameter("cmbSubSector")==null)?"":request.getParameter("cmbSubSector"),
		Rama = (request.getParameter("cmbRama")==null)?"":request.getParameter("cmbRama"),
		Clase = (request.getParameter("cmbClase")==null)?"":request.getParameter("cmbClase"),
		Alta_Troya = (request.getParameter("fp_troya")==null)?"N":request.getParameter("fp_troya"),
		Telefono 		 = (request.getParameter("fp_telefono") == null) ? "" : request.getParameter("fp_telefono"),
		Email 		 = (request.getParameter("fp_mail") == null) ? "" : request.getParameter("fp_mail");
		
		String curp = (request.getParameter("fp_curp")==null)?"":request.getParameter("fp_curp");//Fodea 00-campos PLD 2010
		String fiel = (request.getParameter("fp_fiel")==null)?"":request.getParameter("fp_fiel");//Fodea 00-campos PLD 2010
		String ciudad = (request.getParameter("cmbCiudad")==null)?"":request.getParameter("cmbCiudad");//Fodea 00-campos PLD 2010
		String Pais_de_Origen = (request.getParameter("cmbPaisOrig")==null)?"":request.getParameter("cmbPaisOrig");//Fodea 00-campos PLD 2010
		
		if("M".equals(TipoPersona)){
			fiel = (request.getParameter("fp_fiel_m")==null)?"":request.getParameter("fp_fiel_m");//Fodea 00-campos PLD 2010
			R_F_C = (request.getParameter("fp_rfcAfil_m")==null)?"":request.getParameter("fp_rfcAfil_m").trim();
			Pais_de_Origen = (request.getParameter("cmbPaisOrig_m")==null)?"":request.getParameter("cmbPaisOrig_m");//Fodea 00-campos PLD 2010
		}
		
		System.out.println("TipoPersona = "+TipoPersona);
		System.out.println("NoIf = "+NoIf);
		
		// Persona Física.
		System.out.println("Apellido_paterno = "+Apellido_paterno);
		System.out.println("Apellido_materno = "+Apellido_materno);
		System.out.println("Nombre = "+Nombre);
		System.out.println("Sexo = "+Sexo);
		System.out.println("Fecha_de_nacimiento = "+Fecha_de_nacimiento);
		
		// Persona Moral.
		System.out.println("Razon_Social = "+Razon_Social);
		
		// Datos Generales para ambos Tipo de Persona.
		System.out.println("R_F_C = "+R_F_C );
		System.out.println("Calle = "+Calle );
		System.out.println("Colonia = "+Colonia );
		System.out.println("Estado = "+Estado );
		System.out.println("Delegacion_o_municipio = "+Delegacion_o_municipio );
		System.out.println("Codigo_postal = "+Codigo_postal );
		System.out.println("Pais = "+Pais );
		System.out.println("Ventas_netas_totales = "+Ventas_netas_totales );
		System.out.println("Numero_de_empleados = "+Numero_de_empleados );
		System.out.println("Sector_economico = "+Sector_economico );
		System.out.println("Subsector = "+Subsector );
		System.out.println("Rama = "+Rama );
		System.out.println("Clase = "+Clase );
		System.out.println("Alta_Troya = "+Alta_Troya );
		System.out.println("Telefono 		 = "+Telefono 		 );
		System.out.println("Email 		 = "+Email 		 );
		System.out.println("curp = "+curp );
		System.out.println("fiel = "+fiel );
		System.out.println("ciudad = "+ciudad );
		System.out.println("Pais_de_Origen = "+Pais_de_Origen );
		
		resultado.put("success",new Boolean(true));  //El RFC es correcto
		infoRegresar = resultado.toString();
		
		
		String sDatosPyme =	R_F_C+"|"+Nombre+"|"+Apellido_paterno+"|"+Apellido_materno+"|"+Sexo+"|"+Razon_Social+"|"+Numero_de_empleados+"|"+
									Ventas_netas_totales+"|"+Sector_economico+"|"+Subsector+"|"+Rama+"|"+Clase+"|"+Estado+"|"+Delegacion_o_municipio+"|"+
									Calle+"|"+Colonia+"|"+Codigo_postal+"|"+Alta_Troya+"|"+Email+"|"+Telefono+"|"+curp+"|"+fiel+"|"+Pais_de_Origen+"|"+Pais+"|"+ciudad+"\n";
		
		
		Hashtable hAfiliaCE = BeanAfiliacion.afiliaPymeCE(sDatosPyme, TipoPersona, Fecha_de_nacimiento, "",NoIf, iNoUsuario, strTipoUsuario);
		boolean bTodoOK = new Boolean(hAfiliaCE.get("bTodoOK").toString()).booleanValue();
		String sErrorTroya = hAfiliaCE.get("sErrorTroya").toString();
		
		try {
		String sPymesDadasAlta = hAfiliaCE.get("sNoPymesAlta").toString();
		Vector vDatosPyme = BeanAfiliacion.getPymeAfiliadasCE(sPymesDadasAlta);
		String sConError = hAfiliaCE.get("sConError").toString();
		
		
		if(bTodoOK) {
			StringBuffer contenidoArchivo = new StringBuffer("Resumen de Carga:\nLa siguiente PYME quedo afiliada a Crédito Electrónico con:\nNúmero de SIRAC:,Número de Folio:"+(strTipoUsuario.equals("NAFIN")?",Número Cliente TROYA":"")+"\n");
			for(int i=0; i<vDatosPyme.size(); i++) {
				Vector vPyme = (Vector)vDatosPyme.get(i);
				resultado.put("sirac", vPyme.get(0));
				resultado.put("nafelec", vPyme.get(12));
				resultado.put("troya", vPyme.get(13));
				
				contenidoArchivo.append(vPyme.get(0).toString()+","+vPyme.get(12).toString()+","+(strTipoUsuario.equals("NAFIN")?(sErrorTroya.equals("")?vPyme.get(13).toString():sErrorTroya):"")+"\n");
			}
			
			String ruta = "";
			String nombreArchivo = "";
			CreaArchivo archivo = new CreaArchivo();
			if (archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
				nombreArchivo = archivo.nombre;
				ruta = strDirecVirtualTemp + nombreArchivo ;
				resultado.put("urlArchivo", ruta);
			}
		
		} else {	
			sConError = hAfiliaCE.get("sConError").toString();
			sConError = Comunes.strtr(sConError,"\n","\\n");
			
		}
		
		resultado.put("errorMsg", sConError);
		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();
	} catch (Exception e){
		resultado.put("errorMsg", "Errores en el Registro No. 1 al afiliar la PYME.<br> Error en procedimientos para determinar Estrato.");
		resultado.put("success", new Boolean(true));
		resultado.put("error", new Boolean(true));
		infoRegresar = resultado.toString();
	}
	infoRegresar = resultado.toString();
	}

System.out.println("infoRegresarinfoRegresar=="+infoRegresar);
%>
<%=infoRegresar%>