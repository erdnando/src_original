Ext.onReady(function(){

//----------------------Handlers-------------------
var tablaMensaje='<table width="585" cellpadding="3" cellspacing="0" border="0" class="formas">'+
'							<tr>'+
'								<td class="formas">Verifica la existencia de la siguiente documentaci&oacute;n:<br><br>'+

'Persona moral<br/><br/>'+
'<ul>'+
'<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226; Copia simple del Acta Constitutiva con datos de inscripci&oacute;n en el Registro P&uacute;blico de la'+
' Propiedad<br/>'+
'<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226; Copia del Instrumento P&uacute;blico que contenga los Estatutos Sociales Vigentes o copia simple de la Escritura<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P&uacute;blica en la que conste las modificaciones a los Estatutos Sociales debidamente inscritos en el Registro <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P&uacute;blico correspondiente<br/>'+
'<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226; Copia simple de la Escritura P&uacute;blica que contenga el poder otorgado a favor de las personas que suscribir�n el &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Convenio  <br/>'+
'<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226; Copia del comprobante de domicilio de la Sociedad<br/>'+
'</ul><br/><br/>'+

'Persona f&iacute;sica <br/><br/>'+
'<ul>'+
'<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226; Copia simple del Comprobante de Domicilio<br/>'+
'<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226; Copia simple de Identificaci&oacute;n Oficial vigente<br/>'+
'<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226; Copia simple del Registro Federal de Contribuyentes</ul></td>'+
							'</tr>'+
						'</table>';
						
var icEpo;
var icPyme;
var txtTipoPersona2;
function camposFisica(txtTipoPersona){
txtTipoPersona2=txtTipoPersona;
	if(txtTipoPersona=="M"){
		Ext.getCmp('divNombreInter').setVisible(false);
		Ext.getCmp('divNombreInter').destroy();
		Ext.getCmp('divNombre').setVisible(false);
		Ext.getCmp('divNombre').destroy();
		Ext.getCmp('divRepresentanteInter').setTitle('Nombre del representante legal');
		Ext.getCmp('divRepresentante').setTitle('Nombre del representante legal');
		
		/*
		si es moral mostrar
		rfclegal
		faxlegal
		email legal
		no de escritura
		no de notaria
		fecha constitucion
		tipo de empresa
		*/
	
	} else{
		Ext.getCmp('divEmpresa').setVisible(false);
		Ext.getCmp('divEmpresa').destroy();
		Ext.getCmp('divEmpresaInter').setVisible(false);
		Ext.getCmp('divEmpresaInter').destroy();
		Ext.getCmp('divRepresentanteInter').setTitle('Persona Autorizada');
		Ext.getCmp('divRepresentante').setTitle('Persona Autorizada');
		
		Ext.getCmp('id_email_legal').setVisible(false);
		Ext.getCmp('rfc_legal').setVisible(false);
		Ext.getCmp('fax_legal').setVisible(false);
		Ext.getCmp('num_escritura_diversos').setVisible(false);
		Ext.getCmp('id_cmb_empresa').setVisible(false);
		Ext.getCmp('num_notaria_diversos').setVisible(false);
		Ext.getCmp('fch_constitucion').setVisible(false);
		Ext.getCmp('num_iden_legal').setVisible(false);
		Ext.getCmp('id_cmb_identificacion_legal').setVisible(false);
		
		
		
		Ext.getCmp('id_email_legalInter').setVisible(false);
		Ext.getCmp('rfc_legalInter').setVisible(false);
		Ext.getCmp('fax_legalInter').setVisible(false);
		Ext.getCmp('num_escritura_diversosInter').setVisible(false);
		Ext.getCmp('id_cmb_empresaInter').setVisible(false);
		Ext.getCmp('num_notaria_diversosInter').setVisible(false);
		Ext.getCmp('fch_constitucionInter').setVisible(false);
	}

}



function procesarAlta(opts, success, response) {
		fpDatosInter.el.unmask();
		fpDatos.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			respuesta=Ext.util.JSON.decode(response.responseText);
			if(respuesta.valida){
				
				Ext.Msg.show({
									title:	"Mensaje",
									msg:		respuesta.resultado,
									buttons:	Ext.Msg.OK,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											location.reload();
										}
									},
									closable:false
								});
			} else {
				Ext.Msg.show({
									title:	"Mensaje",
									msg:		respuesta.resultado,
									buttons:	Ext.Msg.OK,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
										}
									},
									closable:false
								});
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
}
function procesarConsulta(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			respuesta=Ext.util.JSON.decode(response.responseText);
			if(respuesta.valida){
				Ext.getCmp('nota').setVisible(false);
				if(respuesta.registro.MENSAJE==""){
					icPyme=respuesta.VALOR_PYME;
					icEpo=respuesta.VALOR_EPO;
					
					fp.setVisible(false);
					if(respuesta.ES_PROVEEDOR_INTERNACIONAL=='S'){
						fpDatosInter.setVisible(true);
						fpDatosInter.getForm().setValues(respuesta.registro);
						Ext.getCmp('nombre_comercial2Inter').setValue(Ext.getCmp('razon_SocialInter').getValue());
						Ext.getCmp('rfc_eInter').setValue(Ext.getCmp('rfcInter').getValue());
						//Ext.getCmp('id_cmb_pais_eInter').setValue(Ext.getCmp('id_cmb_pais_domicilioInter').getValue());
						catalogoEstado.load({
								params : Ext.apply({
									pais : Ext.getCmp('id_cmb_pais_domicilioInter').getValue()
								})
							});
						Ext.getCmp('numero_empleadosInter').setValue(respuesta.registro.IN_NUMERO_EMP)
						Ext.getCmp('ventas_totalesInter').setValue(respuesta.registro.FN_VENTAS_NET_TOT)
						Ext.getCmp('ventas_netasInter').setValue(respuesta.registro.FN_VENTAS_NET_EXP)
						Ext.getCmp('num_activoInter').setValue(respuesta.registro.FN_ACTIVO_TOTAL)
						Ext.getCmp('capital_contableInter').setValue(respuesta.registro.FN_CAPITAL_CONTABLE)
						Ext.getCmp('empleos_generarInter').setValue(respuesta.registro.IN_EMPLEOS_GENERAR);		
						
						
					}else{
						fpDatos.setVisible(true);
						camposFisica(respuesta.registro.txtTipoPersona); 
						fpDatos.getForm().setValues(respuesta.registro);
					//	Ext.getCmp('nombre_comercial2').setValue(Ext.getCmp('razon_Social').getValue()); 
						//Ext.getCmp('rfc_e').setValue(Ext.getCmp('rfc').getValue());
					//	Ext.getCmp('id_cmb_pais_e').setValue(Ext.getCmp('id_cmb_pais_domicilio').getValue());
						
						
						//alert(Ext.getCmp('id_cmb_estado_domicilio').getValue());
						catalogoEstado.load({
								params : Ext.apply({
									pais : Ext.getCmp('id_cmb_pais_domicilio').getValue()
								})
							});
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : Ext.getCmp('id_cmb_estado_domicilio').getValue(),
								pais : (Ext.getCmp('id_cmb_pais_domicilio')).getValue()
							})
						});
						if(Ext.getCmp('id_cmb_pais_domicilio').getValue()==24){
							catalogoCiudad.load({
							params: Ext.apply({
								estado : Ext.getCmp('id_cmb_estado_domicilio').getValue(),
								pais : (Ext.getCmp('id_cmb_pais_domicilio')).getValue()
							})
						});
						}
						
					Ext.getCmp('empleos_generar').setValue(respuesta.registro.IN_EMPLEOS_GENERAR);					
					Ext.getCmp('numero_empleados').setValue(respuesta.registro.IN_NUMERO_EMP);
					Ext.getCmp('ventas_totales').setValue(respuesta.registro.FN_VENTAS_NET_TOT);
					Ext.getCmp('Ventas_netas_exportacion1').setValue(respuesta.registro.FN_VENTAS_NET_EXP);
					Ext.getCmp('num_activo').setValue(respuesta.registro.FN_ACTIVO_TOTAL);
					Ext.getCmp('capital_contable').setValue(respuesta.registro.FN_CAPITAL_CONTABLE);
					Ext.getCmp('fch__firma_convenio').setValue(respuesta.registro.fecha_convenio_unico);
					
					}
					
					Ext.getCmp('nota').setVisible(true);
					Ext.getCmp('nota').body.update(tablaMensaje);
				}else{
					fp.setVisible(false);
					Ext.getCmp('nota').setVisible(true);
					Ext.getCmp('nota').body.update('<center>'+respuesta.registro.MENSAJE+'</center>');
				}
				
			} else {
				Ext.getCmp('nota').setVisible(true);
				Ext.getCmp('nota').body.update('<center>'+respuesta.MENSAJE+'</center>');
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
}
 function validaFormulario(forma){
	var bandera = true;
         forma.getForm().items.each(function(f){			
				if(f.xtype != 'label'&&f.xtype != undefined&&bandera){
				//alert(f.xtype);
				  if(f.isVisible()){
						if(!f.isValid()){
							f.focus();
							f.markInvalid('Este campo es Obligatorio');
							
							bandera= false;
							
						}
				  }
				}
			});
			 	
			if(Ext.getCmp('oper_convenio').getValue()==true && Ext.isEmpty(Ext.getCmp('fch__firma_convenio').getValue())   ){
				Ext.getCmp('fch__firma_convenio').markInvalid('Este campo es Obligatorio');
				Ext.getCmp('fch__firma_convenio').focus();
				bandera= false;
			}
			
			if(Ext.getCmp('oper_convenioInter').getValue()==true && Ext.isEmpty(Ext.getCmp('fch__firma_convenioInter').getValue())   ){
				Ext.getCmp('fch__firma_convenioInter').markInvalid('Este campo es Obligatorio');
				Ext.getCmp('fch__firma_convenioInter').focus();
				bandera= false;
			}
		
		
		   return bandera;
    }
	var procesarCatalogoPais = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			//Ext.getCmp('id_cmb_pais').setValue('24');
		}
	}
//----------------------Stores-------------------

 var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15forma11Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo		
		}
  });
 var catalogoIdentificacion= new Ext.data.JsonStore
	({
		id				: 'catIdentificacion',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoIdentificacion'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
 	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoPais,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
  });
  	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 load: function(){
			if(Ext.getCmp('id_cmb_estado_domicilio').getValue()!=''){
				Ext.getCmp('id_cmb_estado_domicilio').setValue(Ext.getCmp('id_cmb_estado_domicilio').getValue());
			}
		 
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 load: function(){
			if(Ext.getCmp('id_cmb_delegacion_domicilio').getValue()!=''){
				Ext.getCmp('id_cmb_delegacion_domicilio').setValue(Ext.getCmp('id_cmb_delegacion_domicilio').getValue());
			}
		 
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoCiudad= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoCiudad'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	//catalogo Identificacion
	var catalogoIdentificacion= new Ext.data.JsonStore
	({
		id				: 'catIdentificacion',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoIdentificacion'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	//catalogo SubSector ECON�MICO
	var catalogoSubSectorECO= new Ext.data.JsonStore
	({
		id				: 'catSubSectorECO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoSubSector'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Rama ECON�MICA catalogoClase catalogoRamaECO catalogoSubSectorECO
	var catalogoRamaECO= new Ext.data.JsonStore
	({
		id				: 'catRamaECO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoRama'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});	
	
	
	var catalogoClase= new Ext.data.JsonStore
	({
		id				: 'catClase',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoClase'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});	
	//catalogo SectorEPO

	
	var catalogoSectorECO= new Ext.data.JsonStore
	({
		id				: 'catSectorECO',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoSector'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoDomicilioCorres= new Ext.data.JsonStore
	({
		id				: 'catSubdireccion',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoDomicilioCorres'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoEmpresa= new Ext.data.JsonStore
	({
		id				: 'catPromotor',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoEmpresa'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});	 
	
		var catalogoEstadoCivil= new Ext.data.JsonStore
	({
		id				: 'catPromotor',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoEdoCivil'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});	 
	var catalogoGrado = new Ext.data.JsonStore
	({
		id				: 'catPromotor',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoGrado'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});	 
	var catalogoCategoria = new Ext.data.JsonStore
	({
		id				: 'catPromotor',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoCategoria'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});	 
	
	var catalogoVersionConvenio = new Ext.data.JsonStore
	({
		id				: 'catPromotor',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma11Ext.data.jsp',
		baseParams	: { informacion: 'catalogoVersionConvenio'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});	
	var catalogoRegimenMatrimonio = new Ext.data.ArrayStore({
        fields: [
            'clave',
            'descripcion'
        ],
        data: [['S', 'Separaci�n de Bienes'], ['M', 'Bienes Mancomunados']]
    });
//----------------------Elementos-------------------
var elementosForma = [
	{
				xtype: 'combo',
				fieldLabel: 'Cadena Productiva',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				forceSelection: true,
				//anchor: '70%',
				name:'HicEpo',
				store: catalogoEpo,
				id: 'icEpo',
				mode: 'local',
				hiddenName: 'HicEpo',
				hidden: false,				
				emptyText: 'Seleccionar',
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
				setNewEmptyText: function(emptyTextMsg){
					this.emptyText = emptyTextMsg;
					this.setValue('');
					this.applyEmptyText();
					this.clearInvalid();
				}
				},
				{
					xtype: 'textfield',
					name: 'numProv',
					fieldLabel: 'N�mero de Proveedor',
					id: 'numProv',
					maxLength: 20,
					width: 110,
					msgTarget: 'side'
				},
				{
					xtype: 'numberfield',
					name: 'numElectronico',
					fieldLabel: 'N�mero Electr�nico',
					id: 'numElectronico',
					maxLength: 9,
					width: 110,
					msgTarget: 'side'
				}

]
//Formas datos afiliado nacional e internacional


/*--____ Datos del Contacto NACIONAL  ____--*/
	
	var datosPersona_izq = {
		xtype			: 'fieldset',
		id 			: 'datosPersona_izq',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_persona',
				name			: 'Apellido_paterno',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				allowBlank	: false,
				msgTarget	: 'side',
				width			: 230
			},
			{
				xtype			: 'textfield',
				id          : 'id_nombre_persona2',
				name			: 'Nombre',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				msgTarget	: 'side',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0'
			},
			{
				xtype			: 'textfield',
				id          : 'id_curp_persona',
				name			: 'curp',
				fieldLabel  : 'CURP',
				maxLength	: 30,
				msgTarget	: 'side',
				width			: 230,
				margins		: '0 20 0 0'
			},
			{
				xtype			: 'textfield',
				id          : 'id_apellido_casada',
				name			: 'Apellido_casada',
				fieldLabel  : 'Apellido de Casada',
				maxLength	: 25,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				width			: 230
			},
			{
				xtype:'radiogroup',
				id:	'radioGp',
				columns: 2,
				allowBlank	: false,
				width			: 240,
				fieldLabel  : '* Sexo',
				hidden: false,
				items:[
					{boxLabel: 'F',id:'id_femenino', name: 'Sexo',inputValue:'F',  checked: false,
						listeners:{
							check:	function(radio){
											
										}
						}
					},
					{boxLabel: 'M',id:'id_masculino', name: 'Sexo',inputValue:'M',checked: false
						
					}
				]
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_civil',
				name				: 'Estado_civil',
				hiddenName 		: 'Estado_civil',
				fieldLabel  	: '* Estado Civil ',
				forceSelection	: true,
				triggerAction	: 'all',
				msgTarget	: 'side',
				allowBlank	: false,
				mode				: 'local',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoEstadoCivil,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			
			},
			{
				xtype			: 'datefield',
				id          : 'fch_nac',
				name			: 'Fecha_de_nacimiento',
				allowBlank	: false,
				width			: 130,
				minValue: '01/01/1901',
				fieldLabel  : '* Fecha de Nacimiento',
				margins		: '0 20 0 0'
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_escolaridad',
				name				: 'Grado_escolaridad',
				hiddenName 		: 'Grado_escolaridad',
				fieldLabel  	: '* Grado Escolaridad',
				forceSelection	: true,
				allowBlank	: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoGrado,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
			
		]
	};
		
	var datosPersona_der = {
		xtype			: 'fieldset',
		id 			: 'datosPersona_der',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_persona',
				name			: 'Apellido_materno',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				msgTarget	: 'side',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0'
			},{
				xtype 		: 'textfield',
				name  		: 'R_F_C',
				id    		: 'rfc',
				fieldLabel	: '* R.F.C.',
				maxLength	: 20,
				width			: 150,
				allowBlank 	: false,
				hidden		: false,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				vtype: 'validaRFC',
				tipoPersona: 'fisica'
			},
			{
				xtype			: 'textfield',
				id          : 'fiel_persona',
				name			: 'fiel',
				fieldLabel  : 'FIEL',
				msgTarget	: 'side',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0'
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_regimenM',
				name				: 'r_matrimonial',
				hiddenName 		: 'r_matrimonial',
				fieldLabel  	: 'R�gimen Matrimonial ',
				forceSelection	: true,
				msgTarget	: 'side',
				triggerAction	: 'all',
				mode				: 'local',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoRegimenMatrimonio				
			
			},{ 	xtype: 'displayfield', width				: 260, value: '',height:20 }	
			,
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais',
				name				: 'Pais_de_origen',
				hiddenName 		: 'Pais_de_origen',
				fieldLabel  	: 'Pa�s de Origen',
				forceSelection	: true,
				msgTarget	: 'side',
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 150,
				store				: catalogoPais,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_identificacion_persona',
				name				: 'Identificacion',
				hiddenName 		: 'Identificacion',
				fieldLabel  	: '* Identificaci�n ',
				forceSelection	: true,
				allowBlank	: false,
				triggerAction	: 'all',
				msgTarget	: 'side',
				mode				: 'local',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoIdentificacion,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
		]
	};

/*--____ Datos del Contacto INTERNACIONAL  ____--*/
	
		var datosPersona_izqInter = {
		xtype			: 'fieldset',
		id 			: 'datosPersona_izqInter',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_personaInter',
				name			: 'Apellido_paterno',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				allowBlank	: false,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				width			: 230
			},
			{
				xtype			: 'textfield',
				id          : 'id_nombre_personaInter',
				name			: 'Nombre',
				fieldLabel  : '* Nombre(s)',
				allowBlank	: false,
				maxLength	: 30,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0'
			},
			{
				xtype			: 'textfield',
				id          : 'id_apellido_casadaInter',
				name			: 'Apellido_casada',
				fieldLabel  : 'Apellido Casada',
				maxLength	: 25,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				width			: 230
			},
			{
				xtype:'radiogroup',
				id:	'radioGpInter',
				columns: 2,
				msgTarget	: 'side',
				width			: 230,
				fieldLabel  : '* Sexo',
				allowBlank	: false,
				hidden: false,
				items:[
					{boxLabel: 'F',id:'id_femeninoInter', name: 'Sexo',inputValue:'F',  checked: false,
						listeners:{
							check:	function(radio){
											
										}
						}
					},
					{boxLabel: 'M',id:'id_masculinoInter', name: 'Sexo',inputValue:'M',checked: false
						
					}
				]
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_paisInter',
				name				: 'Pais_de_origen',
				hiddenName 		: 'Pais_de_origen',
				fieldLabel  	: '* Pa�s de Origen',
				forceSelection	: true,
				msgTarget	: 'side',
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 150,
				store				: catalogoPais,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_escolaridadInter',
				name				: 'Grado_escolaridad',
				hiddenName 		: 'Grado_escolaridad',
				fieldLabel  	: '* Grado Escolaridad',
				forceSelection	: true,
				allowBlank	: false,
				triggerAction	: 'all',
				msgTarget	: 'side',
				mode				: 'local',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoGrado,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }

		]
	};
		
	var datosPersona_derInter = {
		xtype			: 'fieldset',
		id 			: 'datosPersona_derInter',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_personaInter',
				name			: 'Apellido_materno',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				allowBlank	: false,
				msgTarget	: 'side',
				width			: 230,
				margins		: '0 20 0 0'
			},{
				xtype 		: 'textfield',
				name  		: 'R_F_C',
				id    		: 'rfcInter',
				fieldLabel	: '* R.F.C.',
				maxLength	: 20,
				msgTarget	: 'side',
				width			: 150,
				allowBlank 	: false,
				hidden		: false,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				vtype: 'validaRFC',
				tipoPersona: 'fisica'
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_regimenMInter',
				name				: 'r_matrimonial',
				hiddenName 		: 'r_matrimonial',
				fieldLabel  	: 'R�gimen Matrimonial ',
				forceSelection	: true,
				triggerAction	: 'all',
				msgTarget	: 'side',
				mode				: 'local',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoRegimenMatrimonio				
			
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_civilInter',
				name				: 'Estado_civil',
				hiddenName 		: 'Estado_civil',
				fieldLabel  	: '* Estado Civil ',
				forceSelection	: true,
				msgTarget	: 'side',
				allowBlank	: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoEstadoCivil,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			
			},
			{
				xtype			: 'datefield',
				id          : 'fch_nacInter',
				name			: 'Fecha_de_nacimiento',
				allowBlank	: false,
				minValue: '01/01/1901',
				width			: 130,
				msgTarget	: 'side',
				fieldLabel  : '* Fecha de Nacimiento',
				margins		: '0 20 0 0'
			},{ 	xtype: 'displayfield',  value: '',height:20 }	,
			{
				xtype				: 'combo',
				id          	: 'id_cmb_identificacion',
				name				: 'Identificacion',
				hiddenName 		: 'Identificacion',
				fieldLabel  	: '* Identificaci�n ',
				forceSelection	: true,
				allowBlank	: false,
				triggerAction	: 'all',
				mode				: 'local',
				msgTarget	: 'side',
				width				: 230,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoIdentificacion,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
		]
	};
	
	
//--------------Datos Empresa Nacional------------
	var datosEmpresa_izq = {
		xtype			: 'fieldset',
		id 			: 'datosEmpresa_izq',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				name			: 'Nombre_Comercial',
				id				: 'razon_Social',
				fieldLabel	: '* Raz�n Social',
				maxLength	: 150,
				width			: 300,
				msgTarget	: 'side',
				allowBlank	: false
			},
			{
				xtype			: 'textfield',
				name			: 'Nombre_Comercial',
				id				: 'nombre_comercial2',
				fieldLabel	: '* Nombre Comercial',
				maxLength	: 150,
				width			: 250,
				msgTarget	: 'side',
				allowBlank	: false
			},{
				xtype			: 'textfield',
				id          : 'fiel',
				name			: 'fiel',
				fieldLabel  : 'FIEL',
				maxLength	: 30,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0'
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
			]
		
		};
	var datosEmpresa_der = {
		xtype			: 'fieldset',
		id 			: 'datosEmpresa_der',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{ 	xtype: 'displayfield',  value: '',height:20 },
			{
				xtype 		: 'textfield',
				name  		: 'R_F_C',
				id    		: 'rfc_e',
				fieldLabel	: '* R.F.C.',
				maxLength	: 20,
				width			: 150,
				allowBlank 	: false,
				hidden		: false,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				vtype: 'validaRFC',
				tipoPersona: 'moral'
				},{
				xtype				: 'combo',
				id          	: 'id_cmb_pais_e',
				name				: 'Pais_de_origen',
				hiddenName 		: 'Pais_de_origen',
				fieldLabel  	: 'Pa�s de Origen',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 150,
				store				: catalogoPais,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado').reset();
						Ext.getCmp('id_cmb_delegacion').reset();
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
						}
					}
				},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
			
		]
		};
		
//--------------Datos Empresa Internacional-----------------
	var datosEmpresa_izqInter = {
		xtype			: 'fieldset',
		id 			: 'datosEmpresa_izqInter',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				name			: 'razonSocial',
				id				: 'Razon_SocialInter',
				fieldLabel	: '* Raz�n Social',
				maxLength	: 100,
				width			: 350,
				msgTarget	: 'side',
				allowBlank	: false
			},
			{
				xtype			: 'textfield',
				name			: 'Nombre_Comercial',
				id				: 'nombre_comercial2Inter',
				fieldLabel	: '* Nombre Comercial',
				maxLength	: 150,
				width			: 250,
				msgTarget	: 'side',
				allowBlank	: false
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
			]
		
		};
	var datosEmpresa_derInter = {
		xtype			: 'fieldset',
		id 			: 'datosEmpresa_derInter',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{ 	xtype: 'displayfield',  value: '',height:20 },
			{
				xtype 		: 'textfield',
				name  		: 'R_F_C',
				id    		: 'rfc_eInter',
				fieldLabel	: '* R.F.C.',
				maxLength	: 20,
				width			: 150,
				allowBlank 	: false,
				hidden		: false,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				vtype: 'validaRFC',
				tipoPersona: 'moral'
				},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
			]
		};
		
//--------------------Domicilio--------------------
	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calle_domicilio',
				name			: 'Calle',
				fieldLabel  : '* Calle, No Exterior y No Interior',
				maxLength	: 100,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false

			},{
				xtype			: 'textfield',
				id          : 'Codigo_postal',
				name			: 'Codigo_postal',
				fieldLabel  : '* C�digo Postal',
				allowBlank	: false,
				hidden		: false,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 220,
				regex       : /^[0-9]*$/,
				regexText   : 'Solo de contener n�meros'
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_estado_domicilio',
				name				: 'Estado',
				hiddenName 		: 'Estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				msgTarget	: 'side',
				allowBlank		: false,
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 220,
				store				: catalogoEstado,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					
					select: function(combo, record, index) {
						 
						 
						 Ext.getCmp('id_cmb_delegacion_domicilio').reset();
						 catalogoCiudad.removeAll();
						 Ext.getCmp('id_cmb_ciudad_domicilio').reset();
						 
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais_domicilio')).getValue()
							})
						});
						if(Ext.getCmp('id_cmb_pais_domicilio').getValue()==24){
							catalogoCiudad.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais_domicilio')).getValue()
							})
						});
						}
					}
				}
				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_ciudad_domicilio',
				name				: 'ciudad',
				hiddenName 		: 'ciudad',
				fieldLabel  	: 'Ciudad',
				forceSelection	: true,
				//allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 220,
				store				: catalogoCiudad,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
				
			},
			{
				xtype			: 'textfield',
				id          : 'fax_domicilio',
				name			: 'Fax',
				fieldLabel  : '* Fax',
				width			: 220,
				msgTarget	: 'side',
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 },
			//elementos hidden
			{
				xtype			: 'textfield',
				id          : 'txtIC_Domicilio',
				name			: 'txtIC_Domicilio',
				hidden: true
			},{
				xtype			: 'textfield',
				id          : 'txtIC_Contacto',
				name			: 'txtIC_Contacto',
				hidden: true
			},{
				xtype			: 'textfield',
				id          : 'txtLogin',
				name			: 'txtLogin',
				hidden: true
			},{
				xtype			: 'textfield',
				id          : 'iNoEPO2',
				name			: 'iNoEPO2',
				hidden: true
			},{
				xtype			: 'textfield',
				id          : 'iNoCliente2',
				name			: 'iNoCliente2',
				hidden: true
			}
			
			
			
		
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			
			{
				xtype			: 'textfield',
				id          : 'colonia_domicilio',
				name			: 'Colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 100,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais_domicilio',
				name				: 'Pais',
				hiddenName 		: 'Pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				msgTarget	: 'side',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 220,
				store				: catalogoPais,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
				
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado_domicilio').reset();
						Ext.getCmp('id_cmb_delegacion_domicilio').reset();
						catalogoCiudad.removeAll();
						 Ext.getCmp('id_cmb_ciudad_domicilio').reset();
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
							
							
					}
				}
				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacion_domicilio',
				name				: 'Delegacion_o_municipio',
				hiddenName 		: 'Delegacion_o_municipio',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 220,
				store				: catalogoMunicipio,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
				
			},
			{
				xtype			: 'numberfield',
				id          : 'telefono_domicilio',
				name			: 'Telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				maxLength	: 30
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }	
			
		]
	};
	
	
	/*--____Domicilio_Internacional____--*/
	
	var domicilioPanel_izqInter = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izqInter',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calleInter',
				name			: 'Calle',
				fieldLabel  : '* Calle, No Exterior y No Interior',
				maxLength	: 100,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false

			},{
				xtype			: 'textfield',
				id          : 'id_codeInter',
				name			: 'Codigo_postal',
				fieldLabel  : '* C�digo Postal',
				allowBlank	: false,
				hidden		: true,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 220
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_estadoInter',
				name				: 'Estado',
				hiddenName 		: 'Estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				allowBlank		: false,
				msgTarget	: 'side',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 220,
				store				: catalogoEstado,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_delegacion_domicilioInter').reset();
						 catalogoCiudad.removeAll();
						 Ext.getCmp('id_cmb_ciudad_domicilioInter').reset();
						 
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais_domicilioInter')).getValue()
							})
						});
						if(Ext.getCmp('id_cmb_pais_domicilioInter').getValue()==24){
							catalogoCiudad.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais_domicilioInter')).getValue()
							})
						});
						}
					}
				}
				
			},
			{
				xtype			: 'numberfield',
				id          : 'telefono_domicilioInter',
				name			: 'Telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				maxLength	: 30
				
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 },
			
			{
				xtype			: 'textfield',
				id          : 'txtIC_DomicilioInt',
				name			: 'txtIC_Domicilio',
				hidden: true
			},{
				xtype			: 'textfield',
				id          : 'txtIC_ContactoInt',
				name			: 'txtIC_Contacto',
				hidden: true
			},{
				xtype			: 'textfield',
				id          : 'txtLoginInt',
				name			: 'txtLogin',
				hidden: true
			},{
				xtype			: 'textfield',
				id          : 'iNoEPO2Int',
				name			: 'iNoEPO2',
				hidden: true
			},{
				xtype			: 'textfield',
				id          : 'iNoCliente2Int',
				name			: 'iNoCliente2',
				hidden: true
			}
		
		]
	};
		
	var domicilioPanel_derInter = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_derInter',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			
			{
				xtype			: 'textfield',
				id          : 'coloniaInter',
				name			: 'Colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 100,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais_domicilioInter',
				name				: 'Pais',
				hiddenName 		: 'Pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 220,
				store				: catalogoPais,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado_domicilioInter').reset();
						Ext.getCmp('id_cmb_delegacion_domicilioInter').reset();
						catalogoCiudad.removeAll();
						 Ext.getCmp('id_cmb_ciudad_domicilioInter').reset();
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
							
							
					}
				}
				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacionInter',
				name				: 'Delegacion_o_municipio',
				hiddenName 		: 'Delegacion_o_municipio',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 220,
				store				: catalogoMunicipio,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
				
			},
			{
				xtype			: 'textfield',
				id          : 'faxInter',
				name			: 'Fax',
				fieldLabel  : 'Fax',
				width			: 220,
				maxLength	: 30,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
			
		]
	};
	
	/*--____Fin Domicilio ____--*/
	
	
		/*--____ Datos del Representante Legal ____--*/
	
	var datosRepresentante_izq = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_izq',
		//title 	: 'Domicilio',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_legal',
				name			: 'Apellido_paterno_L',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'textfield',
				id          : 'nombre_legal',
				name			: 'Nombre_L',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'telefono_legal',
				name			: 'Tel_Rep_Legal',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				maxLength	: 30
				
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_email_legal',
				name			: 'Email_Rep_Legal',
				fieldLabel  : ' E-mail',
				maxLength	: 50,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				//vtype		: 'email',
				regex       : /^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z])/,
				regexText   : 'Este campo debe ser una direcci�n de correo electr�nico',
				allowBlank	: true
			},
			{
				xtype			: 'textfield',
				id          : 'num_iden_legal',
				name			: 'No_Identificacion',
				fieldLabel  : '* No. de Identificaci�n',
				maxLength	: 30,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }	
		]
	};
		
	var datosRepresentante_der = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_der',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_legal',
				name			: 'Apellido_materno_L',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},			
			{
				xtype 		: 'textfield',
				name  		: 'Rfc_Rep_Legal',
				id    		: 'rfc_legal',
				fieldLabel	: '* R.F.C.',
				maxLength	: 20,
				width			: 120,
				allowBlank 	: false,
				hidden		: false,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				vtype: 'validaRFC',
				tipoPersona: 'fisica'
				},
				{
				xtype			: 'textfield',
				id          : 'fax_legal',
				name			: 'Fax_Rep_Legal',
				fieldLabel  : 'Fax',
				width			: 220,
				msgTarget	: 'side',
				maxLength	: 30,
				margins		: '0 20 0 0'
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_identificacion_legal',
				name				: 'Identificacion',
				hiddenName 		: 'Identificacion',
				fieldLabel  	: '* Identificaci�n ',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				msgTarget	: 'side',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoIdentificacion,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }	
			
			
		]
	};
	

/*--____ Datos del Representante Legal Internacional____--*/
	
	var datosRepresentante_izqInter = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_izqInter',
		//title 	: 'Domicilio',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_legalInter',
				name			: 'Apellido_paterno_L',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'textfield',
				id          : 'nombre_legalInter',
				name			: 'Nombre_L',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'telefono_legalInter',
				name			: 'Tel_Rep_Legal',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				maxLength	: 30
				
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_email_legalInter',
				name			: 'Email_Rep_Legal',
				fieldLabel: ' E-mail',
				maxLength	: 50,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				//vtype		: 'email',
				regex       : /^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z])/,
				regexText   : 'Este campo debe ser una direcci�n de correo electr�nico',
				allowBlank	: true
			},
			{
				xtype			: 'textfield',
				id          : 'num_iden_legalInter',
				name			: 'No_Identificacion',
				fieldLabel  : '* No. de Identificaci�n',
				maxLength	: 30,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }	
		]
	};
		
	var datosRepresentante_derInter = {
		xtype		: 'fieldset',
		id 		: 'datosRepresentante_derInter',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_legalInter',
				name			: 'Apellido_materno_L',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},			
			
			{
				xtype 		: 'textfield',
				name  		: 'Rfc_Rep_Legal',
				id    		: 'rfc_legalInter',
				fieldLabel	: '* R.F.C.',
				maxLength	: 20,
				width			: 150,
				allowBlank 	: false,
				hidden		: false,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				vtype: 'validaRFC',
				tipoPersona: 'fisica'
				},
				{
				xtype			: 'textfield',
				id          : 'fax_legalInter',
				name			: 'Fax_Rep_Legal',
				fieldLabel  : '* Fax',
				width			: 220,
				msgTarget	: 'side',
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_identificacion_legalInter',
				name				: 'Identificacion',
				hiddenName 		: 'Identificacion',
				fieldLabel  	: '* Identificaci�n ',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				msgTarget	: 'side',
				width				: 220,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoIdentificacion,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }	
			
		]
	};
	
	/*--____Fin Datos del Representante Legal ____--*/
	
	/*--____ Datos del Contacto  ____--*/
	
	var datosContacto_izq = {
		xtype			: 'fieldset',
		id 			: 'datosContacto_izq',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_contacto',
				name			: 'Apellido_paterno_C',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				width			: 220,
				allowBlank	: false
			},
			{
				xtype			: 'textfield',
				id          : 'id_nombre_contacto',
				name			: 'Nombre_C',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			
			{
				xtype			: 'textfield',
				id          : 'faxc',
				name			: 'Fax_C',
				fieldLabel  : 'Fax',
				maxLength	: 30,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: true
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_categoria',
				name				: 'Tipo_categoria',
				hiddenName 		: 'Tipo_categoria',
				fieldLabel  	: '* Tipo categoria',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				msgTarget	: 'side',
				width				: 220,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoCategoria,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }	
		]
	};
		
	var datosContacto_der = {
		xtype			: 'fieldset',
		id 			: 'datosContacto_der',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_contacto',
				name			: 'Apellido_materno_C',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'telc',
				name			: 'Telefono_C',
				msgTarget	: 'side',
				fieldLabel  : '* Tel�fono',
				maxLength	: 30,
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				// E-mail
				xtype       : 'textfield',
				id          : 'id_emailc',
				name        : 'Email_C',
				fieldLabel  : '* E-mail',
				maxLength   : 50,
				width       : 220,
				msgTarget   : 'side',
				margins     : '0 20 0 0',
				//vtype     : 'email',
				regex       : /^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z])/,
				regexText   : 'Este campo debe ser una direcci�n de correo electr�nico',
				allowBlank  : false
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }	
		]
	};
	

	/*--____ Datos del Contacto Internacional ____--*/
	
	var datosContacto_izqInter = {
		xtype			: 'fieldset',
		id 			: 'datosContacto_izqInter',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_contactoInter',
				name			: 'Apellido_paterno_C',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				width			: 220,
				allowBlank	: false
			},
			{
				xtype			: 'textfield',
				id          : 'id_nombre_contactoInter',
				name			: 'Nombre_C',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			
			{
				xtype			: 'textfield',
				id          : 'faxcInter',
				name			: 'Fax_C',
				fieldLabel  : 'Fax',
				msgTarget	: 'side',
				maxLength	: 30,
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: true
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_categoriaInter',
				name				: 'Tipo_categoria',
				hiddenName 		: 'Tipo_categoria',
				fieldLabel  	: '* Tipo categoria',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				msgTarget	: 'side',
				width				: 220,
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoCategoria,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
		]
	};
		
	var datosContacto_derInter = {
		xtype			: 'fieldset',
		id 			: 'datosContacto_derInter',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_contactoInter',
				name			: 'Apellido_materno_C',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'telcInter',
				name			: 'Telefono_C',
				fieldLabel  : '* Tel�fono',
				maxLength	: 30,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_emailcInter',
				name			: 'Email_C',
				fieldLabel	: '* E-mail',
				maxLength	: 50,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				//vtype     : 'email',
				regex       : /^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z])/,
				regexText   : 'Este campo debe ser una direcci�n de correo electr�nico',
				allowBlank	: false
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
		]
	};
	
	/*--____Fin Datos del Contacto  ____--*/
	
	
	
	
	
	/*--____ Otros Datos  Diversos____--*/
	
	var otrosDatos_izq = {
		xtype		: 'fieldset',
		id 		: 'otrosDatos_izq',
		border	: false,
		labelWidth	: 150,
		items		: 
		[		
			{
				xtype			: 'textfield',
				id          : 'num_escritura_diversos',
				name			: 'No_de_escritura',
				fieldLabel  : '* No. de Escritura',
				maxLength	: 12,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'empleos_generar',
				name			: 'Empleos_a_generar',
				fieldLabel  : '* Empleos a Generar',
				maxLength	: 100,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			
			{
				xtype			: 'numberfield',
				id          : 'capital_contable',
				name			: 'Capital_contable',
				fieldLabel  : '* Capital Contable',
				maxLength	: 25,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'ventas_totales',
				name			: 'Ventas_netas_totales',
				fieldLabel  : '* Ventas netas Totales',
				maxLength	: 25,
				minValue		: 5000,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'numero_empleados',
				name			: 'Numero_de_empleados',
				fieldLabel  : '* N�mero de Empleados',
				maxLength	: 6,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_subsectoreco',
				name				: 'Subsector',
				hiddenName 		: 'Subsector',
				fieldLabel  	: '* Subsector',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				msgTarget	: 'side',
				width				: 220,
				emptyText		: 'Seleccione Subsector Econ�mico',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoSubSectorECO,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_ramaECO').reset();
						 Ext.getCmp('id_cmb_clase').reset();
						 catalogoClase.removeAll();
						 catalogoRamaECO.load({
							params: Ext.apply({
								sector	: (Ext.getCmp('id_cmb_sector')).getValue(),
								subsector :record.json.clave							
							})
						});
					}
				}
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_clase',
				name				: 'Clase',
				hiddenName 		: 'Clase',
				fieldLabel  	: '* Clase',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				msgTarget	: 'side',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccionar',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoClase,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
					}
				}
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_empresa',
				name				: 'cmb_empresa',
				hiddenName 		: 'cmb_empresa',
				fieldLabel  	: '* Tipo de Empresa',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				msgTarget	: 'side',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccionar',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoEmpresa,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
					}
				}
			},
			{
				xtype			: 'checkbox',
				id				: 'proveedores',
				name			: 'd_proveedores',
				msgTarget	: 'side',
				hiddenName	: 'proveedores',
				fieldLabel	: 'Desarrollo Proveedores ',
				enable 		: true
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_version_convenio',
				name				: 'Version_Convenio',
				hiddenName 		: 'Version_Convenio',
				fieldLabel  	: '* Versi�n del Convenio',
				forceSelection	: true,
				msgTarget	: 'side',
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccionar',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoVersionConvenio,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			},
			{
				xtype			: 'checkbox',
				id				: 'alta_clientes',
				name			: 'alta_troya',
				msgTarget	: 'side',
				checked     : true,
				hiddenName	: 'alta_clientes',
				fieldLabel	: 'Alta del Cliente a TROYA',
				enable 		: true
			},
			{
				xtype			: 'checkbox',
				id				: 'oper_convenio',
				name			: 'convenio_unico',
				hiddenName	: 'oper_convenio',
				msgTarget	: 'side',
				fieldLabel	: 'Opera Convenio �nico',
				enable 		: true
			},
			{
				xtype			: 'datefield',
				id          : 'fch__firma_convenio',
				name			: 'fecha_convenio_unico',				
				width			: 220,
				minValue: '01/01/1901',
				msgTarget	: 'side',
				fieldLabel  : 'Fecha de Firma del Convenio �nico',
				margins		: '0 20 0 0'
			},
			
			{ 	xtype: 'displayfield',  value: '',width			: 238 }	
		]
	};
		
	var otrosDatos_der = {
		xtype		: 'fieldset',
		id 		: 'otrosDatos_der',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'numberfield',
				id          : 'num_notaria_diversos',
				name			: 'No_de_notaria',
				fieldLabel  : '* No. de notaria',
				maxLength	: 12,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'num_activo',
				name			: 'Activo_total',
				fieldLabel  : '* Activo Total',
				maxLength	: 12,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'Ventas_netas_exportacion1',
				name			: 'Ventas_netas_exportacion',
				fieldLabel  : '* Ventas Netas Exportaci�n',
				maxLength	: 12,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'datefield',
				id          : 'fch_constitucion',
				name			: 'Fecha_de_Constitucion',
				allowBlank	: false,
				msgTarget	: 'side',
				width			: 130,
				minValue: '01/01/1901',
				fieldLabel  : '* Fecha de Constituci�n',
				margins		: '0 20 0 0'
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_sector',
				name				: 'Sector_economico',
				hiddenName 		: 'Sector_economico',
				fieldLabel  	: '* Sector econ�mico',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccione Sector',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoSectorECO,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_subsectoreco').reset();
						 Ext.getCmp('id_cmb_ramaECO').reset();
						 Ext.getCmp('id_cmb_clase').reset();
						 catalogoClase.removeAll(); catalogoRamaECO.removeAll();
						 catalogoSubSectorECO.load({
							params: Ext.apply({
								sector :record.json.clave							
							})
						});
					}
				}
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_ramaECO',
				name				: 'Rama',
				hiddenName 		: 'Rama',
				fieldLabel  	: '* Rama',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccione Rama Econ�mica',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoRamaECO,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
						 
						 catalogoClase.load({
							params: Ext.apply({
								sector :(Ext.getCmp('id_cmb_sector')).getValue(),
								subsector:(Ext.getCmp('id_cmb_subsectoreco')).getValue(),
								rama: record.json.clave
							})
						});
					}
				}
			},
			{
				xtype			: 'textfield',
				id          : 'principales_productos',
				name			: 'Principales_productos',
				fieldLabel  : '* Principales Productos',
				maxLength	: 60,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_domicilio',
				name				: 'Domicilio_Correspondencia',
				hiddenName 		: 'Domicilio_Correspondencia',
				fieldLabel  	: '* Domicilio<br/>Correspondencia',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccionar',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				msgTarget	: 'side',
				store				: catalogoDomicilioCorres,
					tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			},
			{
				xtype:'radiogroup',
				id:	'radioGpDescuento',
				columns: 2,
				width			: 220,
				fieldLabel  : 'Operar Descuento Autom�tico',
				hidden: false,
				items:[
					{boxLabel: 'Si Autorizo',id:'autoriza', name: 'desc_automatico', inputValue:'S', checked: false,
						listeners:{
							check:	function(radio){
											
										}
						}
					},
					{boxLabel: 'No Autorizo',id:'no_autoriza', name: 'desc_automatico', inputValue:'N',checked: true
						
					}
				]
			},
			{
				xtype:'radiogroup',
				id:	'radioGpAutorizo',
				columns: 2,
				width			: 220,
				fieldLabel  : '* Autorizo a NAFIN mandar notificaciones, v�a email y/o SMS:',
				hidden: false,
				items:[
					{boxLabel: 'Si',id:'autFondeoInter', name: 'autMensaje', inputValue:'S', checked: false,
						listeners:{
							check:	function(radio){
											
										}
						}
					},
					{boxLabel: 'No',id:'noAutInter', name: 'autMensaje', inputValue:'N',checked: true
						
					}
				]
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }	
	
			
		]
	};
	
	
	
	/*--____ Otros Datos  Diversos Internaional____--*/
	
	var otrosDatos_izqInter = {
		xtype		: 'fieldset',
		id 		: 'otrosDatos_izqInter',
		border	: false,
		labelWidth	: 150,
		items		: 
		[		
			{
				xtype			: 'textfield',
				id          : 'num_escritura_diversosInter',
				name			: 'No_de_escritura',
				fieldLabel  : '* No. de Escritura',
				maxLength	: 12,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'empleos_generarInter',
				name			: 'Empleos_a_generar',
				fieldLabel  : '* Empleos a Generar',
				maxLength	: 10,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			
			{
				xtype			: 'numberfield',
				id          : 'capital_contableInter',
				name			: 'Capital_contable',
				fieldLabel  : '* Capital Contable',
				maxLength	: 25,
				width			: 220,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'ventas_totalesInter',
				name			: 'Ventas_netas_totales',
				fieldLabel  : '* Ventas netas Totales',
				maxLength	: 25,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'numero_empleadosInter',
				name			: 'Numero_de_empleados',
				fieldLabel  : '* N�mero de Empleados',
				maxLength	: 6,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_subsectorecoInter',
				name				: 'Subsector',
				hiddenName 		: 'Subsector',
				fieldLabel  	: '* Subsector',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 220,
				msgTarget      : 'side',
				emptyText		: 'Seleccione Subsector Econ�mico',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoSubSectorECO,
					tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_ramaECOInter').reset();
						 Ext.getCmp('id_cmb_claseInter').reset();
						 catalogoClase.removeAll();
						 catalogoRamaECO.load({
							params: Ext.apply({
								sector	: (Ext.getCmp('id_cmb_sectorInter')).getValue(),
								subsector :record.json.clave							
							})
						});
					}
				}
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_claseInter',
				name				: 'Clase',
				hiddenName 		: 'Clase',
				fieldLabel  	: '* Clase',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 220,
				msgTarget      : 'side',
				emptyText		: 'Seleccionar',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoClase,
					tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
					}
				}
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_empresaInter',
				name				: 'cmb_empresa',
				hiddenName 		: 'cmb_empresa',
				fieldLabel  	: '* Tipo de Empresa',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				msgTarget      : 'side',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccionar',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoEmpresa,
					tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
					}
				}
			},
			{
				xtype			: 'checkbox',
				id				: 'proveedoresInter',
				name			: 'd_proveedores',
				hiddenName	: 'd_proveedores',
				msgTarget	: 'side',
				fieldLabel	: 'Desarrollo Proveedores ',
				enable 		: true
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_version_convenioInter',
				name				: 'Version_Convenio',
				hiddenName 		: 'Version_Convenio',
				fieldLabel  	: '* Versi�n del Convenio',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 220,
				msgTarget	: 'side',
				emptyText		: 'Seleccionar',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoVersionConvenio,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			},
		
			{
				xtype			: 'checkbox',
				id				: 'oper_convenioInter',
				name			: 'convenio_unico',
				hiddenName	: 'convenio_unico',
				msgTarget	: 'side',
				fieldLabel	: 'Opera Convenio �nico',
				enable 		: true
			},
			{
				xtype			: 'datefield',
				id          : 'fch__firma_convenioInter',
				name			: 'fecha_convenio_unico',
				
				width			: 220,
				msgTarget	: 'side',
				minValue: '01/01/1901',
				fieldLabel  : '* Fecha de Firma del Convenio �nico',
				margins		: '0 20 0 0'
			},
			
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
		]
	};
		
	var otrosDatos_derInter = {
		xtype		: 'fieldset',
		id 		: 'otrosDatos_derInter',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'numberfield',
				id          : 'num_notaria_diversosInter',
				name			: 'No_de_notaria',
				fieldLabel  : '* No. de notaria',
				maxLength	: 12,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'num_activoInter',
				name			: 'Activo_total',
				fieldLabel  : '* Activo Total',
				maxLength	: 12,
				width			: 220,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'numberfield',
				id          : 'ventas_netasInter',
				name			: 'Ventas_netas_exportacion',
				fieldLabel  : '* Ventas Netas Exportaci�n',
				maxLength	: 12,
				msgTarget	: 'side',
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'datefield',
				id          : 'fch_constitucionInter',
				name			: 'Fecha_de_Constitucion',
				allowBlank	: false,
				msgTarget	: 'side',
				width			: 220,
				minValue    : '01/01/1901',
				fieldLabel  : '* Fecha de Constituci�n',
				margins		: '0 20 0 0'
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_sectorInter',
				name				: 'Sector_economico',
				hiddenName 		: 'Sector_economico',
				fieldLabel  	: '* Sector Econ�mico',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	   : 'side',
				triggerAction	: 'all',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccione Sector',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoSectorECO,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('id_cmb_subsectorecoInter').reset();
						 Ext.getCmp('id_cmb_ramaECOInter').reset();
						 Ext.getCmp('id_cmb_claseInter').reset();
						 catalogoClase.removeAll(); catalogoRamaECO.removeAll();
						 catalogoSubSectorECO.load({
							params: Ext.apply({
								sector :record.json.clave							
							})
						});
					}
				}
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_ramaECOInter',
				name				: 'Rama',
				hiddenName 		: 'Rama',
				fieldLabel  	: '* Rama',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget      : 'side',
				triggerAction	: 'all',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccione Rama Econ�mica',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoRamaECO,
					tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
				listeners: {
					select: function(combo, record, index) {
						 
						 
						 catalogoClase.load({
							params: Ext.apply({
								sector :(Ext.getCmp('id_cmb_sectorInter')).getValue(),
								subsector:(Ext.getCmp('id_cmb_subsectorecoInter')).getValue(),
								rama: record.json.clave
							})
						});
					}
				}
			},
			{
				xtype			: 'textfield',
				id          : 'principales_productosInter',
				name			: 'Principales_productos',
				fieldLabel  : '* Principales Productos',
				maxLength	: 60,
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_domicilioInter',
				name				: 'Domicilio_Correspondencia',
				hiddenName 		: 'Domicilio_Correspondencia',
				fieldLabel  	: '* Domicilio<br/>Correspondencia',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				mode				: 'local',
				width				: 220,
				emptyText		: 'Seleccionar',
				valueField		: 'clave',
				displayField	: 'descripcion',
				autoWidth		: true,
				store				: catalogoDomicilioCorres,
				tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
			},
			{
				xtype:'radiogroup',
				id:	'radioGpDescuentoInter',
				columns: 2,
				width			: 220,
				fieldLabel  : 'Operar Descuento Autom�tico',
				hidden: false,
				items:[
					{boxLabel: 'Si Autorizo',id:'autorizaInter', name: 'desc_automatico',  inputValue:'S',  checked: false,
						listeners:{
							check:	function(radio){
											
										}
						}
					},
					{boxLabel: 'No Autorizo',id:'no_autorizaInter', name: 'desc_automatico', inputValue:'N',checked: false
						
					}
				]
			},
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
			
			
			
		]
	};


//-----------------Formularios-----------------------
var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: 'margin:0 auto;',
		frame				: true,
		hidden			: false,
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			{
				layout	: 'hbox',
				title		: 'Nombre completo',
				id			: 'divNombre',
				width		: 940,
				items		: [datosPersona_izq,datosPersona_der ]
			},
			{
				layout	: 'hbox',
				title		: 'Nombre de la empresa',
				id			: 'divEmpresa',
				//height	: 280,
				width		: 940,
				items		: [datosEmpresa_izq,datosEmpresa_der ]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 940,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			},
			{
				layout	: 'hbox',
				id			: 'divRepresentante',
				title		: 'Datos del Representante Legal',
				width		: 940,
				items		: [datosRepresentante_izq, datosRepresentante_der ]
			},
			{
				layout	: 'hbox',
				title 	: 'Datos del Contacto',
				width		: 940,
				items		: [datosContacto_izq, datosContacto_der ]
			},
			{
				layout	: 'hbox',
				title		: 'Diversos',
				width		: 940,
				items		: [otrosDatos_izq, otrosDatos_der ]
			}

		],
		buttons			: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				handler: function(boton, evento) 
				{
					/*****************Validaciones***************/

				
					if(!validaFormulario(fpDatos)){
					
						return;
					}
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							var params = (fpDatos)?fpDatos.getForm().getValues():{};				
							fpDatos.el.mask("Procesando", 'x-mask-loading');
							//peticion de Registro de Afiliaci�n
							Ext.Ajax.request({
									url: '15forma11Ext.data.jsp',
									params: Ext.apply(params,{
											txtTipoPersona:txtTipoPersona2,
											iNoEPO2:icEpo,
											iNoCliente2:icPyme,
											informacion: 'guardaAfiliado',
											esProveedorInternacional: 'N'
									}),
									callback: procesarAlta
							});
						}
					});		
				}
			
			},
			{
				text:'Regresar',
				iconCls: 'icoLimpiar',
				handler: function() {
					location.reload();
				}
			}
		]
	});	
	
var fpDatosInter = new Ext.form.FormPanel({
		id					: 'formaAfiliacionInter',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		hidden			: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			{
				layout	: 'hbox',
				title		: 'Nombre completo',
				id			: 'divNombreInter',
				width		: 940,
				items		: [datosPersona_izqInter,datosPersona_derInter ]
			},
			{
				layout	: 'hbox',
				id			: 'divEmpresaInter',
				title		: 'Nombre de la empresa',
				//height	: 280,
				width		: 940,
				items		: [datosEmpresa_izqInter,datosEmpresa_derInter ]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 940,
				items		: [domicilioPanel_izqInter	, domicilioPanel_derInter	]
			},
			{
				layout	: 'hbox',
				id			: 'divRepresentanteInter',
				title		: 'Datos del Representante Legal',
				width		: 940,
				items		: [datosRepresentante_izqInter, datosRepresentante_derInter ]
			},
			{
				layout	: 'hbox',
				title 	: 'Datos del Contacto',
				width		: 940,
				items		: [datosContacto_izqInter, datosContacto_derInter ]
			},
			 
			{
				layout	: 'hbox',
				title		: 'Diversos',
				width		: 940,
				items		: [otrosDatos_izqInter, otrosDatos_derInter ]
			}
			
		],
		buttons			: [
			{
				text: 'Aceptar',
				id: 'btnAceptarInter',
				iconCls:'aceptar',
				handler: function(boton, evento) 
				{
					/*****************Validaciones***************/

				
					
					if(validaFormulario(fpDatosInter)){
						return;
					}
					
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							var params = (fpDatosInter)?fpDatosInter.getForm().getValues():{};				
							fpDatosInter.el.mask("Procesando", 'x-mask-loading');
							//peticion de Registro de Afiliaci�n
							Ext.Ajax.request({
									url: '15forma11Ext.data.jsp',
									params: Ext.apply(params,{
											txtTipoPersona:txtTipoPersona2,
											iNoEPO2:icEpo,
											iNoCliente2:icPyme,
											informacion: 'guardaAfiliado',
											esProveedorInternacional: 'S'
									}),
									callback: procesarAlta
							});
						}
					});		
				}
			
			},
			{
				text:'Regresar',
				iconCls: 'icoLimpiar',
				handler: function() {
					location.reload();
				}
			}
		]
	});
//---------------------Principal---------------------
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Consultar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						
						var icEpo=Ext.getCmp('icEpo').getValue();
						var numProv= Ext.getCmp('numProv').getValue();
						var numElec = Ext.getCmp('numElectronico').getValue();
						if(icEpo==''&&numProv==''&&numElec==''){
							Ext.getCmp('icEpo').markInvalid('Debe de capturar alg�n criterio de b�squeda');
							return;
						
						}else{
								if (numProv!=""){
									if(icEpo=='')
								{		
												Ext.getCmp('icEpo').focus();
												Ext.getCmp('icEpo').markInvalid("Debe seleccionar una Cadena");			
												return;
											}
										}
							}
									if(icEpo!=''){
										if (numElec=="" && numProv==""){
											Ext.getCmp('icEpo').focus();
											Ext.getCmp('icEpo').markInvalid("Debe capturar otro criterio de b�squeda");
											return;
										}
									}
						//boton.disable();
						//boton.setIconClass('loading-indicator');
						fp.el.mask("Procesando", 'x-mask-loading');
						Ext.Ajax.request({
							url: '15forma11Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'consulta'}),
							callback: procesarConsulta
						});

					}
			 }
			 
		]
	});
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
	  fp,fpDatos,fpDatosInter,NE.util.getEspaciador(10),{width:600,style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',id:'nota',html: '',hidden:true	}
		]
  });
  fpDatos.setVisible(false);
  fpDatosInter.setVisible(false);
	catalogoEpo.load();
	catalogoIdentificacion.load();
	catalogoPais.load();
	catalogoEstadoCivil.load();
	catalogoGrado.load();
	catalogoCategoria.load();
	catalogoSectorECO.load();
	catalogoEmpresa.load();
	catalogoDomicilioCorres.load();
	catalogoVersionConvenio.load();
});