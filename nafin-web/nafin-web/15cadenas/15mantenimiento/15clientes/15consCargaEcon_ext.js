Ext.onReady(function() {

	var strTipoUsuario = Ext.getDom('hidStrUsuario').value;
	var FlagCargaEcon = Ext.getDom('FlagCargaEcon').value;
	var gbl_icResumen = '';
	var gbl_cAfil = '';
	var gbl_cReafil = '';

//----------------------------------------------------------HANDLERS OR FUNCTION
	
	var procesarCatEstatusData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catTipoAfiliado = Ext.getCmp('tipoAfiliado1');
			if(catTipoAfiliado.getValue()==''){
				catTipoAfiliado.setValue('10');
			}
		}
  } 
	
	var procesarCatalogoEpoData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_epo = Ext.getCmp('ic_epo1');
			if(ic_epo.getValue()==''){
				//ic_epo.setValue(records[0].data['clave']);
			}
		}
  }
  
	var procesarConsultaVencData = function(store, arrRegistros, opts) {
		fpConsultaCargaMas.el.unmask();
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		
		gridCargaMasiva.show();
		if (arrRegistros != null) {
		
			if (!gridCargaMasiva.isVisible()) {
				contenedorPrincipalCmp.add(gridCargaMasiva);
				contenedorPrincipalCmp.doLayout();
			}
			

			var el = gridCargaMasiva.getGridEl();
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenArchivo').urlArchivo='';
				Ext.getCmp('btnGenArchivo').setText('Generar Archivo');
				Ext.getCmp('btnGenArchivo').enable();
				//Ext.getCmp('barraPaginacion1').show();
				el.unmask();
			}else {
				Ext.getCmp('btnGenArchivo').disable();
				//Ext.getCmp('barraPaginacion1').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};
	
	
	var processSuccessFailureArchivo = function(opts, success, response) {
		var btnGenArchivo = Ext.getCmp('btnGenArchivo');
		btnGenArchivo.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			btnGenArchivo.setIconClass('icoXls');
			btnGenArchivo.setText('Abrir Excel');
			
			var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};

			
			btnGenArchivo.urlArchivo =  NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			//btnGenArchivo.urlArchivo =  archivo;
			btnGenArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			
		
		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						btnGenArchivo.setIconClass('icoXls');
						btnGenArchivo.setText('Abrir Excel');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				btnGenArchivo.setIconClass('icoXls');
				btnGenArchivo.setText('Abrir Excel');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
	};
	
	var verDetalle = function(icResumen, tipoCarga, cAfil, cReafil){
		
		gbl_icResumen = icResumen
		gbl_cAfil = cAfil;
		gbl_cReafil = cReafil;
		
		var ventana = Ext.getCmp('winDescZIP');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				modal: true,
				resizable: false,
				layout: 'form',
				x: 100,
				width: 200,
				id: 'winDescZIP',
				closable: true,
				closeAction: 'hide',
				items: [ pnlGeneraArchivoDet ],
				title: 'Descarga Detalle'
			}).show();
		}
		
		
		if(Ext.getCmp('btnGenArchivoDet')){
			var boton = Ext.getCmp('btnGenArchivoDet');
			var msgExcelVacio = Ext.getCmp('msgExcelVacio');
			
			msgExcelVacio.hide();
			boton.show();
			boton.urlArchivo='';
			boton.setIconClass('icoXls');
			boton.setText('Descargar Excel');
			
			
			if(Ext.isEmpty(boton.urlArchivo)){
				boton.disable();
				boton.setIconClass('loading-indicator');
				boton.setText('Generando Archivo Xls...');
				
				Ext.Ajax.request({
					url : '15consCargaEcon_ext.data.jsp',
					params: {
						informacion: 'generarArchivoDetalle',
						numResumen: gbl_icResumen,
						contAfil: gbl_cAfil,
						contReafil: gbl_cReafil
					},
					callback: processSuccessFailureArchivoDetalle
				});
			}
			
		}
	}
	
	
	
	var processSuccessFailureArchivoDetalle = function(opts, success, response) {
		var btnGenArchivo = Ext.getCmp('btnGenArchivoDet');
		var msgExcelVacio = Ext.getCmp('msgExcelVacio');
		btnGenArchivo.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			if(resp.archVacio){
				btnGenArchivo.hide();
				msgExcelVacio.show();
			}else{
				btnGenArchivo.setIconClass('icoXls');
				btnGenArchivo.setText('Abrir Excel');
				
				var archivo = resp.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
	
				btnGenArchivo.urlArchivo =  NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				//btnGenArchivo.urlArchivo =  archivo;
				btnGenArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			}

		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						btnGenArchivo.setIconClass('icoXls');
						btnGenArchivo.setText('Abrir Excel');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				btnGenArchivo.setIconClass('icoXls');
				btnGenArchivo.setText('Abrir Excel');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
	};
	
	

//--------------------------------------------------------------STORE

	var storeCatEpoData = new Ext.data.JsonStore({
		id: 'storeCatEpoData',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15consCargaEcon_ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoEpoData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	

	var storeVencData = new Ext.data.JsonStore({
		url : '15consCargaEcon_ext.data.jsp',
		id: 'storeVencData1',
		baseParams:{
			informacion: 'consultaCargasMasivasEcon'
		},
		root : 'registros',
		fields: [
			{name: 'IC_RESUMEN'},
			{name: 'IC_EPO'},
			{name: 'VRAZON_SOCIAL'},
			{name: 'CG_NOMBRE_ARCHIVO'},
			{name: 'IG_REG_VALIDOS'},
			{name: 'IG_REG_ERROR'},
			{name: 'IG_SUSCEPTIBLES'},
			{name: 'IG_DUPLICADOS'},
			{name: 'IG_AFILIADOS'},
			{name: 'IG_REAFILIA_NUEVAS'},
			{name: 'IG_REAFI_AUT'},
			{name: 'IG_X_AFILIAR'},
			{name: 'IG_ECON_CARGADOS'},
			{name: 'IG_ECON_X_CARGAR'},
			{name: 'CS_PROCESADO_X_ECON'},
			{name: 'DF_CARGA'},
			{name: 'CG_TIPO_CARGA'},
			{name: 'CG_CARGA_ECON'},
			{name: 'CG_CRUCE_DB'},
			{name: 'IC_PRODUCTO'},
			{name: 'CS_DIRECTO_PROMO'},
			{name: 'CS_VENTA_CRUZADA'},
			{name: 'IG_CONV_UNICO'},
			{name: 'IG_REG_TOTALES'},
			{name: 'IG_REG_SINERROR_NE'},
			{name: 'IG_REG_CONERROR_NE'},
			{name: 'IG_REG_PROV_YA_EXISTENTES_NE'},
			{name: 'C_AFIL'},
			{name: 'C_REAFIL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaVencData,
			beforeload:{
				fn: function(store, Obj) {
					
					store.baseParams.fec_ini = Ext.getCmp('fec_ini')?Ext.getCmp('fec_ini').getValue():'';
					store.baseParams.fec_ini = Ext.getCmp('fec_fin')?Ext.getCmp('fec_fin').getValue():'';
					store.baseParams.ic_epo = Ext.getCmp('ic_epo')?Ext.getCmp('ic_epo').getValue():'';
					store.baseParams.fec_venc_fin = Ext.getCmp('numResumen')?Ext.getCmp('numResumen').getValue():'';
					store.baseParams.fec_venc_fin = Ext.getCmp('valProcesado1')?Ext.getCmp('valProcesado1').getValue():'';
				}		
			},
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaVencData(null, null, null);
				}
			}
		}
	});
	
	var storeCatEstatusData = new Ext.data.ArrayStore({
	  //id: 0,
	  fields: ['clave','descripcion'  ],
	  //data: [['N', 'Por validar'], ['V', 'Validado'],['B','Borrado'],['NPR','NP Rechazados']],
	  autoLoad: false,
	  listeners: {
			load: procesarCatEstatusData
	  }
	});

//--------------------------------------------------------------------ELEMENTOS

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan:2 , align: 'center'},
				//{header: '', colspan:1 , align: 'center'},
				{header: 'Carga N@E', colspan:4 , align: 'center'},
				{header: 'Limpieza', colspan:3 , align: 'center'},
				{header: 'Dispersion de Registros', colspan:5 , align: 'center'},
				{header: 'Econtract', colspan:2 , align: 'center'},
				{header: '', colspan:5 , align: 'center'}
			]
		]
	});
	
	
	var elementosForm =
	[
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_epo',
			emptyText: 'Seleccione...',
			//width: 400,
			anchor: '100%',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			hidden: false,
			store : storeCatEpoData
		},
		{
			xtype: 'numberfield',
			name: 'numResumen',
			id: 'numResumen1',
			fieldLabel: 'N�mero Proceso',
			allowBlank: true,
			maxLength: 8,	//ver el tama�o maximo del numero en BD para colocar este igual
			anchor: '45%',
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'radiogroup',
			fieldLabel: 'Archivos Procesados',
			id:'radioProc',
			items: [
				 {boxLabel: 'Ambos', name: 'valProcesado1', inputValue:'A', checked: true},
				 {boxLabel: 'Si', name: 'valProcesado1', inputValue:'S'},
				 {boxLabel: 'No', name: 'valProcesado1', inputValue:'N'}
			]
      },
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Carga de Archivo',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fec_ini',
					id: 'fec_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fec_fin',
					margins: '0 20 0 0',  //necesario para mostrar el icono de error
					regexText:"La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
					regex: /.*[1-9][0-9]{3}$/
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fec_fin',
					id: 'fec_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fec_ini',
					margins: '0 20 0 0',  //necesario para mostrar el icono de error
					regexText:"La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
					regex: /.*[1-9][0-9]{3}$/
				}
			]
		}
	];

var fpConsultaCargaMas = new Ext.form.FormPanel({
		id: 'fpConsultaCargaMas1',
		width: 600,
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForm,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					
					var objFecIni = Ext.getCmp('fec_ini');
					var objFecFin = Ext.getCmp('fec_fin');
					
					if(!(objFecIni.getValue()!='' && objFecFin.getValue()!='') && (objFecIni.getValue()!='' || objFecFin.getValue()!='')){
						if(objFecIni.getValue()==''){
							objFecIni.markInvalid('El valor de la fecha inicial es requerido');
							return;
						}
						if(objFecFin.getValue()==''){
							objFecFin.markInvalid('El valor de la fecha final es requerido');
							return;
						}
					}
						
					fpConsultaCargaMas.el.mask('Enviando...', 'x-mask-loading');		
					storeVencData.load({
							params: Ext.apply(fpConsultaCargaMas.getForm().getValues(),{
								operacion:'Generar',
								start: 0,
								limit: 50
							})
						});
					
					
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href='15consCargaEcon_ext.jsp';
				}
				
			}
		]
	});
	

var gridCargaMasiva = new Ext.grid.EditorGridPanel({
	id: 'gridCargaMasiva1',
	store: storeVencData,
	style:	'margin:0 auto;',
	margins: '20 0 0 0',
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	plugins: grupos,
	columns: [
		{//1
			header: 'N�mero de Proceso',
			tooltip: 'N�mero de Proceso',
			dataIndex: 'IC_RESUMEN',
			sortable: true,
			width: 150,
			resizable: true,
			align: 'center',
			hidden: false
		},
		{//2
			header: 'EPO',
			tooltip: 'EPO',
			dataIndex: 'VRAZON_SOCIAL',
			sortable: true,
			hideable: false,
			width: 130,
			align: 'left',
			hidden: false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{
			header: 'Registros Totales',
			tooltip: 'Registros Totales',
			dataIndex: 'IG_REG_TOTALES',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Registros sin Error',
			tooltip: 'Registros sin Error',
			dataIndex: 'IG_REG_SINERROR_NE',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Registros con Error',
			tooltip: 'Registros con Error',
			dataIndex: 'IG_REG_CONERROR_NE',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Ya Existentes',
			tooltip: 'Ya Existentes',
			dataIndex: 'IG_REG_PROV_YA_EXISTENTES_NE',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Registros Totales',
			tooltip: 'Registros Totales',
			//dataIndex: 'MFOLIO',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false,
			renderer: function(valor, metadata, registro, rowindex, colindex, store) {
				valor = Number(registro.data['IG_REG_VALIDOS']) + Number(registro.data['IG_DUPLICADOS']);
				return valor;
			}
		},
		{
			header: 'Duplicados',
			tooltip: 'Duplicados',
			dataIndex: 'IG_DUPLICADOS',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Unicos',
			tooltip: 'Unicos',
			dataIndex: 'IG_REG_VALIDOS',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Ya Afiliados',
			tooltip: 'Ya Afiliados',
			dataIndex: 'IG_AFILIADOS',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Reafiliaciones',
			tooltip: 'Reafiliaciones',
			dataIndex: 'IG_REAFILIA_NUEVAS',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Reafiliaciones Auto.',
			tooltip: 'Reafiliaciones Auto.',
			dataIndex: 'IG_REAFI_AUT',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Con Contrato �nico',
			tooltip: 'Con Contrato �nico',
			dataIndex: 'IG_CONV_UNICO',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Por Afiliar',
			tooltip: 'Por Afiliar',
			dataIndex: 'IG_X_AFILIAR',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: '* Ya Cargados',
			tooltip: 'Ya Cargados',
			dataIndex: 'IG_ECON_CARGADOS',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: '* Por Cargar',
			tooltip: 'Por Cargar',
			dataIndex: 'IG_ECON_X_CARGAR',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			header: 'Nombre de Archivo',
			tooltip: 'Nombre de Archivo',
			dataIndex: 'CG_NOMBRE_ARCHIVO',
			sortable: true,
			width: 150,
			align: 'left',
			hidden: false
		},
		{
			header: 'Fecha de Carga',
			tooltip: 'Fecha de Carga',
			dataIndex: 'DF_CARGA',
			sortable: true,
			width: 100,
			align: 'left',
			hidden: false
		},
		{
			header: 'Datos de Carga',
			tooltip: 'Datos de Carga',
			dataIndex: 'MFOLIO',
			sortable: true,
			width: 100,
			align: 'left',
			hidden: false,
			renderer: function(valor, metadata, registro, rowindex, colindex, store) {
				var datosDeCarga = '';
				var datosDeCargaFile = '';
				if(registro.data['CG_CARGA_ECON']!=''){
					datosDeCarga += "*Carga";
					datosDeCargaFile += "*Carga";
				}
				
				if(registro.data['CG_CRUCE_DB']!=''){
					datosDeCarga += datosDeCarga!=''?" y Cruce":"*Cruce";
					datosDeCargaFile += datosDeCargaFile!=''?" y Cruce":"*Cruce";
				}
				
				if(registro.data['IC_PRODUCTO']!='' && registro.data['IC_PRODUCTO']=='1'){
					datosDeCarga +="<br>*Factoraje";
					datosDeCargaFile +="- Factoraje";
				}
				
				//if(c_ic_producto!=null && c_ic_producto.equals("11")){
				if(registro.data['IC_PRODUCTO']!='' && registro.data['IC_PRODUCTO']=='11'){
					datosDeCarga +="<br>*Reafiliacion IF";
					datosDeCargaFile +="- Reafiliacion IF";
				}
				
				if(registro.data['CS_DIRECTO_PROMO']!=''){
					datosDeCarga +="<br>*Directo a Promotoria";
					datosDeCargaFile +="- Directo a Promotoria";
				}
				
				if(registro.data['CS_VENTA_CRUZADA']!=''){
					datosDeCarga +="<br>*Venta Cruzada";
					datosDeCargaFile +="- Venta Cruzada";
				}
				
				valor = datosDeCargaFile;
				return datosDeCarga;
			}
		},
		{
			header: 'Procesado',
			tooltip: 'Procesado',
			dataIndex: 'CS_PROCESADO_X_ECON',
			sortable: true,
			width: 80,
			align: 'center',
			hidden: false
		},
		{
			xtype: 		'actioncolumn',
			header: 		'Resumen',
			tooltip: 	'Resumen',
			width: 		120,
			align:		'center',
			hidden: 		false,
			renderer: function(valor, metadata, registro, rowindex, colindex, store) {
				valor='Ver Detalle';
				return valor;
			},
			items: [
				{
					getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						this.items[0].tooltip = 'Ver detalle';
						return 'iconoLupa';
					},
					handler: function(grid, rowIndex, colIndex) {
						var registro = Ext.StoreMgr.key('storeVencData1').getAt(rowIndex);
						verDetalle(registro.data['IC_RESUMEN'], registro.data['CG_TIPO_CARGA'], registro.data['C_AFIL'], registro.data['C_REAFIL']);
					}
				}
			]
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 930,
	style: 'margin:0 auto;',
	title: '',
	frame: true,
	bbar: {
			xtype:		'paging',
			autoScroll:	true,
			height:		30,
			pageSize:	50,
			buttonAlign:'left',
			id:			'barraPaginacion1',
			displayInfo:true,
			store:		storeVencData,
			displayMsg:	'{0} - {1} de {2}',
			emptyMsg:	"No hay registros.",
			items:		[
				'->','-',
				{
						xtype:	'button',
						text:		'Generar Archivo',
						id:		'btnGenArchivo',
						iconCls: 'icoXls',
						hidden:	false,
						urlArchivo: '',
						handler:	function(boton, evento) {
							if(Ext.isEmpty(boton.urlArchivo)){
								boton.disable();
								boton.setIconClass('loading-indicator');
								
								Ext.Ajax.request({
									url : '15consCargaEcon_ext.data.jsp',
									params:Ext.apply(fpConsultaCargaMas.getForm().getValues(),{
										informacion: 'generarArchivoConsulta'
									}),
									callback: processSuccessFailureArchivo
								});
							}else{
								
								var fp = Ext.getCmp('fpConsultaCargaMas1');
								fp.getForm().getEl().dom.action = boton.urlArchivo;
								fp.getForm().getEl().dom.submit();
								
							}
						}
				}
			]
		}
	});


	
	var pnlGeneraArchivoDet = new Ext.Panel({
		name: 'pnlGeneraArchivoDet',
		id: 'pnlGeneraArchivoDet1',
		width: 200,
		style: 'margin:0 auto;',
		frame: true,
		items:[
			{
				xtype: 'displayfield',
				id: 'msgExcelVacio',
				value: '<p align="center">No se encontraron Registros</p>',
				width: 'auto',
				hidden: true
			},
			{
				xtype: 'button',
				id: 'btnGenArchivoDet',
				iconCls: 'icoXls',
				style: 'margin:0 auto;',
				text:'Descargar Excel',
				handler: function(boton){
					if(Ext.isEmpty(boton.urlArchivo)){
						boton.disable();
						boton.setIconClass('loading-indicator');
						boton.setText('Generando Archivo Xls...');
						
						Ext.Ajax.request({
							url : '15consCargaEcon_ext.data.jsp',
							params: {
								informacion: 'generarArchivoDetalle',
								numResumen: gbl_icResumen,
								contAfil: gbl_cAfil,
								contReafil: gbl_cReafil
							},
							callback: processSuccessFailureArchivoDetalle
						});
					}else{
						var fp = Ext.getCmp('fpConsultaCargaMas1');
						fp.getForm().getEl().dom.action = boton.urlArchivo;
						fp.getForm().getEl().dom.submit();
						
					}
				}
			}
		]
	});
	
	var pnlTipoAfil = new Ext.Panel({
		name: 'pnlTipoAfil',
		id: 'pnlTipoAfil1',
		width: 600,
		style: 'margin:0 auto;',
		frame: true,
		layout: 'form',
		items:[
			{
				xtype: 'combo',
				name: 'tipoAfiliado',
				id: 'tipoAfiliado1',
				fieldLabel: 'Tipo de Afiliado',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'tipoAfiliado',
				emptyText: 'Seleccione...',
				autoSelect :true,
				//width: 400,
				anchor: '100%',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				hidden: false,
				store : storeCatEstatusData,
				listeners:{
					select:{ 
						fn:function (combo) {
							var opcion = combo.getValue();
							var pagina = combo.getValue();
							if (pagina == 0) { 
								window.location  = "15forma05AfianzadoraExt.jsp"; 	
							}	else if (pagina == 1 ) { 
								window.location ="15consEPOext.jsp";
							}	else if (pagina == 2) { //Proveedores
								window.location = "./15forma05ProveedoresExt.jsp";
							}	else if (pagina == 3) { //Proveedor internacional 
							
							}	else if (pagina == 4) {  //Proveedor sin Numero								
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp"; 	
							}	else if (pagina == 5) {
								window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp";		
							}	else if (pagina == 6) { 
								window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp";		
							}	else if (pagina == 7) { 
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B"; 	
							} else if (pagina == 8) { 
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB"; 	 	
							} else if (pagina == 9) { 
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB"; 		 	
							} else if (pagina == 10) { // proveedor carga masiva EContract
								window.location = "./15consCargaEcon_ext.jsp";
							}	else if (pagina == 11) { // Afiliados a Credito Electronico								
								window.location = "15forma05ext_AfCredElectronico.jsp";
							}	else if (pagina == 12) { // Cadena Productiva Internacional
							
							}	else if (pagina == 13) { // Contragarante
							
							}	else if (pagina == 14) { 
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp"; 	
							}	else if (pagina == 15) { 
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp"; 	
							}	else if (pagina == 16) { // Universidad Programa Credito Educativo
								window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp"; 		
							}							
						}	
					}
				}
			}
		]
	});

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		renderHidden: true,
		layoutConfig: {
			align:'center'
		},
		items: [
			NE.util.getEspaciador(20),
			pnlTipoAfil,
			NE.util.getEspaciador(10),
			fpConsultaCargaMas,
			NE.util.getEspaciador(20)
		]
	});
	
	storeCatEpoData.load();
	
	if (strTipoUsuario == 'NAFIN' && FlagCargaEcon =='N'){
		storeCatEstatusData.loadData(	[
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],			
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],				
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo'],
			['16','Cliente Externo']
		]);
	}else  {
		storeCatEstatusData.loadData(	[
			['10','Proveedor Carga Masiva EContract']
		]);	
	}

});