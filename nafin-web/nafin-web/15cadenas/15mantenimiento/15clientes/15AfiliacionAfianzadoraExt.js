Ext.onReady(function(){
	var sTipoUsuario =  Ext.getDom('sTipoUsuario').value;	
	
//----------------------Handlers-----------------------
 function validaFormulario(forma){
	var bandera = true;
         forma.getForm().items.each(function(f){
			if(f.xtype != 'label'&&f.xtype != undefined&&bandera){
           if(f.isVisible()){
				   if(!f.isValid()){
						f.focus();
						f.markInvalid('Este campo es Obligatorio');
						bandera= false;
					}
           }
			}
        });
        return bandera;
    }

function procesarAlta(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			respuesta=Ext.util.JSON.decode(response.responseText);
		
				Ext.Msg.show({
									title:	"Mensaje",
									msg:		"Su n�mero de Nafin Electr�nico: "+respuesta.numeroNafinElectronico,
									buttons:	Ext.Msg.OK,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											location.reload();
										}
									},
									closable:false
								});
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
}

//----------------------Stores--------------------

	function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		}else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} 	else if (pagina == 2) { 	// Distribuidor						
			window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 		 
		} else if (pagina == 3) { 	//Proveedor
			window.location = '15forma01Ext.jsp'; 
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} else if (pagina == 6) { 	//Contragarante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 	
		} else if (pagina == 9) { 	// Mandante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
		} else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		}	else if (pagina == 11) { 	//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 	
		}	else if (pagina == 12) { 	//Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliaClienteExternoExt.jsp"; 
		}
	}
	
	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [[/*'EPO'*/  '0','Cadena Productiva']
				,[/*'IF'*/	'1','Intermediario Financiero']
				,[/*'2'*/	'2','Distribuidor']
				,[/*'1'*/	'3','Proveedor']	
				,[/*'ACE'*/	'4','Afiliados Credito Electronico']
				,[/*'EPOI'*/'5','Cadena Productiva Internacional']
				,[/*'CONTRA'*/'6','Contragarante']				
				,[/*'M'*/	'9','Mandante']	
				,[/*'AF'*/	'10','Afianzadora']
				,[/*'UNI'*/ '11','Universidad-Programa Cr�dito Educativo']
				,['12','Cliente Externo']
					
				]
	});
 	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15AfiliacionAfianzadoraExt.data.jsp',
		baseParams	: { informacion: 'catalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			exception: NE.util.mostrarDataProxyError
		}
  });
  	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15AfiliacionAfianzadoraExt.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 load: function(){
			
		 
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15AfiliacionAfianzadoraExt.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 load: function(){
		
		 
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	



//--------------------Domicilio--------------------
	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calle_domicilio',
				name			: 'Calle',
				fieldLabel  : '* Calle, No Exterior y No Interior',
				maxLength	: 100,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false

			},{
				xtype			: 'textfield',
				id          : 'Codigo_postal',
				name			: 'code',
				fieldLabel  : '* C�digo Postal',
				allowBlank	: false,
				hidden		: false,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 90
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_estado_domicilio',
				name				: 'Estado',
				hiddenName 		: 'Estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				msgTarget	: 'side',
				allowBlank		: false,
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				store				: catalogoEstado,
				listeners: {
					
					select: function(combo, record, index) {
						 
						 
						 Ext.getCmp('id_cmb_delegacion_domicilio').reset();
						 
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais_domicilio')).getValue()
							})
						});
						
					}
				}
				
			},
			{
				xtype			: 'textfield',
				id          : 'telefono',
				regex			:/^[0-9]*$/,
				regexText	: 'El Tel�fono solo de contener n�meros',
				name			: 'telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30
				
			},
			{
				xtype			: 'textfield',
				id          : 'fax_domicilio',
				name			: 'Fax',
				fieldLabel  : 'Fax',
				width			: 230,
				msgTarget	: 'side',
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: true
			},
			{
				xtype			: 'textfield',
				id          : 'URL',
				name			: 'URL',
				fieldLabel  : '* URL para la Validaci�n de fianza',
				maxLength	: 100,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false

			},
			{ 	xtype: 'displayfield',  value: '',width			: 250 }
			
			
			
		
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			
			{
				xtype			: 'textfield',
				id          : 'colonia_domicilio',
				name			: 'Colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 100,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_pais_domicilio',
				name				: 'Pais',
				hiddenName 		: 'Pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				msgTarget	: 'side',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 230,
				store				: catalogoPais,
				listeners: {
				
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado_domicilio').reset();
						Ext.getCmp('id_cmb_delegacion_domicilio').reset();
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
							
							
					}
				}
				
			},
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacion_domicilio',
				name				: 'Delegacion_o_municipio',
				hiddenName 		: 'Delegacion_o_municipio',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				store				: catalogoMunicipio
				
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_email_legal',
				name			: 'Email_Rep_Legal',
				fieldLabel: '* E-mail Notificaciones',
				maxLength	: 100,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				vtype			: 'email',
				allowBlank	: false
			}			
			,{ 	xtype: 'displayfield',  value: '',width			: 250 }
		]
	};
var elementosForma = [
				{
		xtype		: 'panel',
		id 		: 'afiliacion',
		layout	: 'hbox',
		
		border	: false,		
		width		: 900,		
		bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:110px;',
		items		: 
		[{
			layout		: 'form',
			width			:400,
			labelWidth	: 60,
			border		: false,
			items			:[{
					xtype				: 'combo',
					id					: 'id_afiliacion',
					name				: 'afiliacion',
					hiddenName 		:'afiliacion', 
					fieldLabel		: 'Afiliaci�n',
					width				: 250,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',
					value				: '10',
					store				: storeAfiliacion,
					listeners: {
						select: {
							fn: function(combo) {
								var valor  = combo.getValue();
								creditoElec(valor);		
							}
						}
					}
					
				}]
			}
	]},
		{
				xtype			: 'textfield',
				name			: 'Razon_Social',
				id				: 'razon_Social',
				fieldLabel	: '* Raz�n Social',
				maxLength	: 100,
				anchor			: '60%' ,
				
				msgTarget	: 'side',
				allowBlank	: false
			},
			{
				xtype			: 'textfield',
				name			: 'Nombre_Comercial',
				id				: 'nombre_comercial',
				fieldLabel	: '* Nombre Comercial',
				maxLength	: 100,
				anchor			: '60%' ,
				
				msgTarget	: 'side',
				allowBlank	: false
			},{
				layout	: 'hbox',
				width		:940,
				items		: [
							{
							xtype		: 'fieldset',
							id 		: 'paneliz',
							border	: false,
							labelWidth	: 150,
							items		: 
							[
									{
													xtype 		: 'textfield',
													name  	: 'R_F_C',
													id    		: 'rfc_e',
													fieldLabel	: '* R.F.C.',
													maxLength	: 20,
													width			: 230,
													allowBlank 	: false,
													hidden		: false,
													margins		: '0 20 0 0',
													msgTarget	: 'side',
													regex			:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
													regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
													'en el formato NNN-AAMMDD-XXX donde:<br>'+
													'NNN:son las iniciales del nombre de la empresa<br>'+
													'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
													'XXX:es la homoclave'
												},
												{ 	xtype: 'displayfield',  value: '',width			: 250 }
								]
								}, 
								{
							xtype		: 'fieldset',
							id 		: 'panelder',
							border	: false,
							labelWidth	: 150,
							items		: 
							[
							{
								xtype			: 'textfield',
								id          : 'CNSF',
								name			: 'CNSF',
								fieldLabel  : '* N�mero Asignado por la CNSF',
								regex			:/^[0-9]*$/,
								regexText	: 'El campo solo de contener n�meros',
								maxLength	: 20,
								allowBlank 	: false,
								width			: 230,
								msgTarget	: 'side',
								margins		: '0 20 0 0'
								},
								{ 	xtype: 'displayfield',  value: '',width			: 250 }
								]
							}
						]
			}
]

//-----------------Formularios-----------------------
var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		hidden			: false,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 160,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			elementosForma,
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 930,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			}
		],
		buttons			: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				handler: function(boton, evento) 
				{
					/*****************Validaciones***************/

				
					if(!validaFormulario(fpDatos)){
					
						return;
					}
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							var params = (fpDatos)?fpDatos.getForm().getValues():{};				
							//fpDatosInter.el.mask("Procesando", 'x-mask-loading');
							//peticion de Registro de Afiliaci�n
							Ext.Ajax.request({
									url: '15AfiliacionAfianzadoraExt.data.jsp',
									params: Ext.apply(params,{
											informacion: 'AfiliacionSave'
									}),
									callback: procesarAlta
							});
						}
					});		
				}
			
			},
			{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					location.reload();
				}
			}
		]
	});	
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
	  fpDatos,NE.util.getEspaciador(10)
		]
  });
  catalogoPais.load();
});