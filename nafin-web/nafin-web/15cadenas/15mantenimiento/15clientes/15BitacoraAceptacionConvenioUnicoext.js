Ext.onReady(function() {
	var cvePerf = "";

	var ic_if =  Ext.getDom('ic_if').value;
	var busqAvanzadaSelect=0;
   var rfc='';
   
	Todas = Ext.data.Record.create([ 
		{name: "clave",         type: "string"}, 
		{name: "descripcion",   type: "string"}, 
		{name: "loadMsg",       type: "string"}
	]);
	//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
	var procesarCatalogoBancoFondeo = function(store, arrRegistros, opts) {
		Ext.getCmp("id_noBancoFondeo").setValue((store.getRange()[0].data).clave);	
	}
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton = Ext.getCmp('btnGenerarArchivo');
			boton.enable();
			var boton2 = Ext.getCmp('btnGenerarPDF');
			boton2.enable();		
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
			
	var procesarCatalogoNombreEPO = function(store, arrRegistros, opts) {
		//Ext.getCmp("noIc_epo").setValue((ic_epo));		
	}
   
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
	
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnBajarPDF').hide();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var returnInit= function(opts, success, response) { 

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			if(Ext.util.JSON.decode(response.responseText).cvePerf!='')
			{
				var jsonObj = Ext.util.JSON.decode(response.responseText);
				var cveAux=jsonObj.cvePerf;
				cvePerf = cveAux;
	
				if(cvePerf == '4'){
					catalogoIF.load();
					Ext.getCmp('id_noIc_if').show();
				}
			}		
		} else {
			//NE.util.mostrarConnError(response,opts);
		}
}
	
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);			
		}
	}
 
   function procesarBusquedaExpediente(opts, success, response) {
		var grid = Ext.getCmp('grid');
      var el = grid.getGridEl();
      el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;	
			if (Ext.util.JSON.decode(response.responseText).existearchivo != 'false') {
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			}else{
            Ext.Msg.show({
               title: 'Informaci�n',
               msg:  'No se encontr� ning�n registro',
               buttons: Ext.Msg.OK,
               icon:    Ext.Msg.INFO
            });
         }
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var bajarContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();			
		} else {
			alert("error");
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('id_cmb_num_ne').focus();
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15BitacoraAceptacionConvenioUnicoext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'EXTENSION'},
			{name: 'IC_CONSECUTIVO'},
			{name: 'IC_IF'},
			{name: 'IC_PYME'},
			{name: 'NOMBRE_IF'},
			{name: 'FECHA_AFIL_IF'},
			{name: 'FECHA_AUT_IF'},
			{name: 'NUMERO_NAFELE'},
			{name: 'NUMERO_SIRAC'},
			{name: 'RFC'},
			{name: 'NOMBRE_PYME'},
			{name: 'FECHA_ACEP_PYME'},
			{name: 'FECHA_FIRMA_PYME'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	var catalogoBancoFondeo = new Ext.data.JsonStore({
		id: 'catalogoBancoFondeo',
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15BitacoraAceptacionConvenioUnicoext.data.jsp',
		listeners: {
			load: procesarCatalogoBancoFondeo,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoBancoFondeo'
		},
		totalProperty : 'total',
		autoLoad: false
	});
	
		var catalogoIF = new Ext.data.JsonStore({
		id: 'idCatalogoIF',
		//xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15BitacoraAceptacionConvenioUnicoext.data.jsp',
		listeners: {
			//load: procesarCatalogoIF,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false
	});
	
	var catalogoNombreEPO = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '15BitacoraAceptacionConvenioUnicoext.data.jsp',
		listeners: {		
			load: procesarCatalogoNombreEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoNombreEPO'			
		},
		totalProperty : 'total',
		autoLoad: true
	});
	
	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion','ic_pyme','loadMsg'],
		url : '15BitacoraAceptacionConvenioUnicoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
   
   var gruposHeader = new Ext.ux.grid.ColumnHeaderGroup({
      rows: [
         [
            {header: '&nbsp;', colspan: 2, align: 'center'},
            {header: 'Datos del Proveedor', colspan: 9, align: 'center'}					
         ]
      ]
	});

	var grid = new Ext.grid.GridPanel({ 
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
      plugins: gruposHeader,
      columnLines: true,
		hidden: true,
		columns: [{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 200, resizable: true,
				align: 'left'
			},{
				header: 'Fecha de Afiliaci�n <br>del IF', tooltip: 'Fecha de Afiliaci�n del IF',
				dataIndex: 'FECHA_AFIL_IF',
				sortable: true,	align: 'center',
				width: 130, resizable: true,
				align: 'left'
			},{
               header: 'Fecha de Aceptaci�n IF<br>de Cuenta Bancaria / Usuario', tooltip: 'Fecha de Aceptaci�n IF de Cuenta Bancaria / Usuario',
               dataIndex: 'FECHA_AUT_IF',
               sortable: true,	align: 'center',
               width: 170, resizable: true,
               align: 'left'
            },{
               header: 'N�m<br>Electr�nico', tooltip: 'N�mero Electronico',
               dataIndex: 'NUMERO_NAFELE',
               sortable: true,	align: 'center',
               width: 70, resizable: true,
               align: 'left'
            },{
               header: 'Clave SIRAC', tooltip: 'Clave SIRAC',
               dataIndex: 'NUMERO_SIRAC',
               sortable: true,	align: 'center',
               width: 80, resizable: true,
               align: 'left'
            },{
               header: 'RFC', tooltip: 'RFC',
               dataIndex: 'RFC',
               sortable: true,	align: 'center',
               width: 110, resizable: true,
               align: 'left'
            },{
               header: 'Nombre PyME', tooltip: 'Nombre PyME',
               dataIndex: 'NOMBRE_PYME',
               sortable: true,	align: 'center',
               width: 150, resizable: true,
               align: 'left'
            },{
               header: 'Fecha de Aceptaci�n <br>del Convenio �nico <br>Electr�nico', tooltip: 'Fecha de Aceptaci�n del Convenio �nico Electr�nico',
               dataIndex: 'FECHA_ACEP_PYME',
               sortable: true,	align: 'center',
               width: 130, resizable: true,
               align: 'left'
            },{
               header: 'Fecha de Firma <br>Aut�grafa de C.U', tooltip: 'Fecha de Firma Aut�grafa de C.U',
               dataIndex: 'FECHA_FIRMA_PYME',
               sortable: true,	align: 'center',
               width: 130, resizable: true,
               align: 'left'
            },{
               xtype: 'actioncolumn',
               header: 'Expediente PyME', tooltip: 'Expediente PyME',
               dataIndex: '',
               width: 100, resizable: true,
               align: 'center', 
               renderer: function(){ return 'Buscar  '; },
               items: [
                  { //para cambiar icono segun extension PDF/DOC 
                     getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
                        var icono = 'icoBuscar';
                        return icono;
                     },
                     tooltip: 'Buscar Expediente',
                     handler: function(grid, rowIdx, colIds){
                     
                     var reg = grid.getStore().getAt(rowIdx);
                     rfc = reg.get('RFC');                 
                     var grid = Ext.getCmp('grid');
                     var el = grid.getGridEl();
                     el.mask('Procesando expediente...', 'x-mask');
                     
                     Ext.Ajax.request({
                        url: '15BitacoraAceptacionConvenioUnicoext.data.jsp',
                        params: Ext.apply(paramSubmit,{
                           informacion: 'BusquedaExpediente',
                           rfc: rfc,
                           version: 'ext'
                        }),
                        callback: procesarBusquedaExpediente
                     }); 	
                     
                  }				
               }
            ]
            },{
               xtype: 'actioncolumn',
               header: 'Clausulado', tooltip: 'Clausulado',
               dataIndex: '',
               width: 70, resizable: true,
               align: 'center', 
               renderer: function(){ return ''; },
               items: [
                  { //para cambiar icono segun extension PDF/DOC 
                     getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
                        var icono = 'icoPdf';
                        return icono;
                     },
                     tooltip: 'Descargar Clausulado',
                     handler: function(grid, rowIdx, colIds){		
                        //boton.setIconClass('loading-indicator');
                        var reg = grid.getStore().getAt(rowIdx);
                        var forma = Ext.getDom('formAux');
                        document.forms[0].action = "/nafin/15cadenas/15mantenimiento/15clientes/15ClausuladoConvenioUnico.jsp?clave_if="+reg.get('IC_IF')+"&clave_pyme="+reg.get('IC_PYME');
                        document.forms[0].submit();
                  }				
               }
            ]
			//}]
      }],
		viewConfig: {
         stripeRows: true
      },
		loadMask: true,
		deferRowRender: false,
		height: 513,
		width: 916,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		bbar: {
		xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						//boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '15BitacoraAceptacionConvenioUnicoext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'XLS',
								informacion: 'Consulta'
							}),
							//callback: procesarSuccessFailureGenerarArchivo
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						//boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '15BitacoraAceptacionConvenioUnicoext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'PDF',
								informacion: 'Consulta'
							}),
							//callback: procesarSuccessFailureGenerarPDF
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
	});
	var elementosFecha = [{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Aceptaci�n de Convenio �nico',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'txt_fecha_acep_de',
					id: 'df_consultaMin',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_consultaMax',
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					formBind: true
					//value:ahora.getDate()+'/'+mes.toString()+'/'+ahora.getFullYear()
					},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txt_fecha_acep_a',
					id: 'df_consultaMax',
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_consultaMin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}
	];
	var PanelVerPoderesPyme2 = {
		xtype: 'panel',
		id: 'PanelExpediente',
		width: 700,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	};
	var comboFondeo=[
	/*{
			xtype: 'combo',
			name: 'BancoFondeo',
			id: 'id_noBancoFondeo',
			hiddenName : 'noBancoFondeo',
			fieldLabel: 'Banco de Fondeo',
			emptyText: 'Seleccione Banco de Fondeo',
			store: catalogoBancoFondeo,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			allowBlank : false,
			minChars : 1,
			width:300
	},*/
	{
			xtype: 'combo',
			name: 'ic_if',
			id: 'id_noIc_if',
			hiddenName : 'noIc_if',
			fieldLabel: 'IF',
			emptyText: 'Seleccione IF',
			store: catalogoIF,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			forceSelection:true,
			width:300,
			listeners: {
						select: {
							fn: function(combo) {	
								//if(combo.getValue()!=ic_epo) {
								//	Ext.getCmp("id_noIc_epo").setValue(ic_epo); 
								//}							
							}
						}
					}
		}];
	var elementosForma = [
		{
			xtype: 'hidden',
			name: 'ic_pyme',
			id: 'ic_pyme'
		},
		{
			xtype: 'compositefield',
			width: 800,
			fieldLabel: 'N�m. Elect. PyME',
			labelWidth: 200,
			items:[{
				xtype: 'numberfield',
				name: 'txt_nafelec',
				id:'txt_nafelec',
				width:50,
				listeners: {
					'blur': function(){
						// Petici�n b�sica  
						Ext.Ajax.request({  
							url: '15BitacoraAceptacionConvenioUnicoext.data.jsp',  
							method: 'POST',  
							callback: successAjaxFn,  
							params: {  
								 informacion: 'pymeNombre' ,
								 comboEpo: Ext.getCmp('id_noIc_epo').getValue(),
								 txt_nafelec: Ext.getCmp('txt_nafelec').getValue()
							}  
						});  
					}
				}
			},
			{
				xtype: 'textfield',
				readOnly: true,
				id:'hid_nombre',
				name:'hid_nombre',
				disabled:true,
				hiddenName : 'hid_nombre',
				mode: 'local',
				resizable: true,
				triggerAction : 'all',
				width: 260
			},{
				xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [{
						xtype:'displayfield',
						id:'disEspacio',
						text:''
					},{
						xtype: 'button',
						text: 'Busqueda Avanzada',
						id: 'btnAvanzada',
						iconCls:	'icoBuscar',
						handler: function(boton, evento) {
							var winVen = Ext.getCmp('winBuscaA');
								if (winVen){
									Ext.getCmp('fpWinBusca').getForm().reset();
									Ext.getCmp('fpWinBuscaB').getForm().reset();
									Ext.getCmp('id_cmb_num_ne').store.removeAll();
									Ext.getCmp('id_cmb_num_ne').reset();
									winVen.show();
								}else{
									var winBuscaA = new Ext.Window ({
										id:'winBuscaA',
										height: 300,
										x: 300,
										y: 100,
										width: 600,
										heigth: 100,
										modal: true,
										closeAction: 'hide',
										title: 'Tipo de Afiliado',
										items:[{
											xtype:'form',
											id:'fpWinBusca',
											frame: true,
											border: false,
											style: 'margin: 0 auto',
											bodyStyle:'padding:10px',
											defaults: {
												msgTarget: 'side',
												anchor: '-20'
											},
											labelWidth: 140,
											items:[{
												xtype:'displayfield',
												frame:true,
												border: false,
												value:'Utilice el * para b�squeda gen�rica'
											},{
												xtype:'displayfield',
												frame:true,
												border: false,
												value:''
											},{
												xtype: 'textfield',
												name: 'nombre_pyme',
												id:	'txtNombre',
												fieldLabel:'Nombre',
												maxLength:	100
											},{
												xtype: 'textfield',
												name: 'rfc_pyme',
												id:	'txtRfc',
												fieldLabel:'RFC',
												maxLength:	20
											}],
											buttons:[{
												text:'Buscar',
												iconCls:'icoBuscar',
												handler: function(boton) {
													/*if ( Ext.isEmpty(Ext.getCmp('txtNombre').getValue()) && Ext.isEmpty(Ext.getCmp('txtRfc').getValue()) && Ext.isEmpty(Ext.getCmp('txtNe').getValue()) ){
														Ext.Msg.alert(boton.text,'No existe informaci�n con los criterios determinados');
														return;
													}	*/
													catalogoNombreData.load({ 
                                          params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues(),{envia: 'CONSULTAR'}) 
                                       });
												}
											},{
												text:'Cancelar',
												iconCls: 'icoLimpiar',
												handler: function() {
													Ext.getCmp('winBuscaA').hide();
												}
											}]
										},{
											xtype:'form',
											frame: true,
											id:'fpWinBuscaB',
											style: 'margin: 0 auto',
											bodyStyle:'padding:10px',
											monitorValid: true,
											defaults: {
												msgTarget: 'side',
												anchor: '-20'
											},
											items:[{
												xtype: 'combo',
												id:	'id_cmb_num_ne',
												name: 'cmb_num_ne',
												hiddenName : 'cmb_num_ne',
												fieldLabel: 'Nombre',
												emptyText: 'Seleccione . . .',
												displayField: 'descripcion',
												valueField: 'clave',
												triggerAction : 'all',
												forceSelection:true,
												allowBlank: false,
												typeAhead: true,
												mode: 'local',
												minChars : 1,
												store: catalogoNombreData,
												tpl : NE.util.templateMensajeCargaCombo,
												listeners: {
													select: function(combo, record, index) {
														busqAvanzadaSelect=index;
													}
												}
											}],
											buttons:[{
												text:'Aceptar',
												iconCls:'aceptar',
												formBind:true,
												handler: function() {
													if (!Ext.isEmpty(Ext.getCmp('id_cmb_num_ne').getValue())){
														var reg = Ext.getCmp('id_cmb_num_ne').getStore().getAt(busqAvanzadaSelect).get('ic_pyme');
														Ext.getCmp('ic_pyme').setValue(reg);
														var disp = Ext.getCmp('id_cmb_num_ne').lastSelectionText;
														var desc = disp.slice(disp.indexOf(" ")+1);
														Ext.getCmp('txt_nafelec').setValue(Ext.getCmp('id_cmb_num_ne').getValue());
														Ext.getCmp('hid_nombre').setValue(desc);
														Ext.getCmp('winBuscaA').hide();
													}
												}
											},{
												text:'Cancelar',
												iconCls: 'icoLimpiar',
												handler: function() {	Ext.getCmp('winBuscaA').hide();	}
											}]
										}]
									}).show();
								}
							}
						}]
				}]
		},{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'id_noIc_epo',
			hiddenName : 'noIc_epo',
			fieldLabel: 'EPO',
			emptyText: 'Seleccione EPO',
			store: catalogoNombreEPO,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			forceSelection:true,
			width:300,
			listeners: {
						select: {
							fn: function(combo) {	
							//	if(combo.getValue()!=ic_epo) {
							//		Ext.getCmp("id_noIc_epo").setValue(ic_epo); 
							//	}							
							}
						}
					}
		}];
	
	//Forma para hacer la busqueda filtrada
	var fp = new Ext.form.FormPanel({
		//xtype: 'form',
		id: 'forma',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 600,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[comboFondeo,elementosForma,elementosFecha],
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var df_consultaNotMin = Ext.getCmp("df_consultaMin");
					var df_consultaNotMax = Ext.getCmp("df_consultaMax");		
               var grid              = Ext.getCmp('grid');
					
					var Desde =  Ext.util.Format.date(df_consultaNotMin.getValue(),'d/m/Y');   
					var Hasta =  Ext.util.Format.date(df_consultaNotMax.getValue(),'d/m/Y'); 
					

					if (!Ext.isEmpty(df_consultaNotMin.getValue()) && Ext.isEmpty(df_consultaNotMax.getValue()) || Ext.isEmpty(df_consultaNotMin.getValue()) && !Ext.isEmpty(df_consultaNotMax.getValue())) {
						if(!Ext.isEmpty(df_consultaNotMin.getValue()) && Ext.isEmpty(df_consultaNotMax.getValue())){
                     df_consultaNotMax.markInvalid('Debe capturar la fecha de Notificaci�n final');	 
                  }
                  if(Ext.isEmpty(df_consultaNotMin.getValue()) && !Ext.isEmpty(df_consultaNotMax.getValue())){
                     df_consultaNotMin.markInvalid('Debe capturar ambas fechas de  Notificaci�n  o dejarlas en blanco');
                  }
                  consultaDataGrid.removeAll();
                  grid.hide();
						return;
					}
				
					if (!Ext.isEmpty(df_consultaNotMin.getValue()) &&  !Ext.isEmpty(df_consultaNotMax.getValue())){
						if(!isdate(Desde)){
							df_consultaNotMin.markInvalid("Favor de teclear la fecha con el formato  (dd/mm/aaaa)");				
							Ext.getCmp('grid').hide();
							df_consultaNotMin.focus();		
							return;
						}
						 if(!isdate(Hasta)){
							df_consultaNotMax.markInvalid("Favor de teclear la fecha con el formato  (dd/mm/aaaa)");				
							Ext.getCmp('grid').hide();
							df_consultaNotMax.focus();		
							return;
						}
				}
				/********   *********/
               grid.show();
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaDataGrid.load({
						params: Ext.apply(paramSubmit,{
						operacion: 'Generar',
						start:0,
						limit:15							
					})
				});
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					if(cvePerf == '4'){
						window.location = '15BitacoraAceptacionConvenioUnicoNaExt.jsp';
					}else{
						window.location = '15BitacoraAceptacionConvenioUnicoext.jsp';
					}
				}
			}
		]
	});//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});


	Ext.Ajax.request({  
            url: '15BitacoraAceptacionConvenioUnicoext.data.jsp',  
           // method: 'POST',  
            callback: returnInit,  
            params: {  
                informacion: 'initPage'
				}  
        });

});

