Ext.onReady(function(){


	var ic_contacto = [];	
	var ap_paterno = [];	
	var ap_materno = [];	
	var nombre = [];	
	var telefono = [];	
	var fax = [];
	var email = [];
	
	var cancelar =  function() {  
		ic_contacto = [];	
		ap_paterno = [];	
		ap_materno = [];	
		nombre = [];	
		telefono = [];	
		fax = [];
		email = [];
	}
		
	var  procesarSuccessFailureInhabilitar =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			
				Ext.MessageBox.alert('Mensaje',	'El registro fue eliminado',
					function(){
						fp.el.mask('Enviando...', 'x-mask-loading');			
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{  
								informacion: 'Consultar',
								txtNombre: Ext.getCmp('txtNombre1').getValue()							
							})
						});		
					}
				);
				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarInhabilitar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_contacto = registro.get('IC_CONTACTO');	
		
		
			Ext.Msg.confirm("Confirmaci�n", "El registro ser� eliminado. �Desea continuar ? ",
				function(boton) {					
					if (boton == 'yes') {						
						Ext.Ajax.request({
							url : '15forma6ext.data.jsp',
							params : {
								informacion: 'Inhabilitar',	
								ic_contacto : ic_contacto							 
							},
							 callback: procesarSuccessFailureInhabilitar
						});					
					} 
				}
			);
									
									
		
	
	
	
	}
	
	//***********************+Guardar*************************+
	var  procesarSuccessFailureGuardar =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
				Ext.MessageBox.alert('Mensaje',	'La operaci�n se realiz� con �xito',
					function(){
						 fp.el.mask('Enviando...', 'x-mask-loading');			
							consultaData.load({
								params: Ext.apply(fp.getForm().getValues(),{  
									informacion: 'Consultar',
									txtNombre: Ext.getCmp('txtNombre1').getValue()							
								})
							});		
					}
				);
				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGuardar = function() 	{
	
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		var modificados = store.getModifiedRecords();
		var columnModelGrid = gridConsulta.getColumnModel();
		var numRegistro =0 ;
		var totoalError =0;
		
		cancelar(); //limpia variables
		
		store.each(function(record) {
			numRegistro = store.indexOf(record);
			
			if(record.data['INHABILITADO']=='N'){
			
				if( Ext.isEmpty(record.data['APELLIDO_PATERNO']) ) {
					totoalError++;
					Ext.MessageBox.alert('Mensaje',	'El Apellido Paterno es obligatorio',
						function(){
							gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('APELLIDO_PATERNO'));
						}
					);
					return false;	
				}else {
					if ( record.data['APELLIDO_PATERNO'].length >= 25){
						totoalError++;
						Ext.MessageBox.alert('Mensaje',	'El Apellido Paterno no debe de sobrepasar los 25 caracteres.',
							function(){
								gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('APELLIDO_PATERNO'));
							}
						);
						return false;	
					}
				}
				
				if( Ext.isEmpty(record.data['APELLIDO_MATERNO']) ) {
					totoalError++;
					Ext.MessageBox.alert('Mensaje',	'El Apellido Materno es obligatorio',
						function(){
							gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('APELLIDO_MATERNO'));
						}
					);
					return false;	
				}else {
					if ( record.data['APELLIDO_MATERNO'].length >= 25){
						totoalError++;
						Ext.MessageBox.alert('Mensaje',	'El Apellido Materno no debe de sobrepasar los 25 caracteres.',
							function(){
								gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('APELLIDO_MATERNO'));
							}
						);
						return false;	
					}
				}
				
				if( Ext.isEmpty(record.data['NOMBRES']) ) {
					totoalError++;
					Ext.MessageBox.alert('Mensaje',	'El Nombre(s) es obligatorio',
						function(){
							gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('NOMBRES'));
						}
					);
					return false;	
				}else {
					if ( record.data['NOMBRES'].length >= 80){
						totoalError++;
						Ext.MessageBox.alert('Mensaje',	'El Nombre(s) no debe de sobrepasar los 80 caracteres.',
							function(){
								gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('NOMBRES'));
							}
						);
						return false;	
					}
				}
				
				if( Ext.isEmpty(record.data['TELEFONO']) ) {
					totoalError++;
					Ext.MessageBox.alert('Mensaje',	'El Tel�fono es obligatorio',
						function(){
							gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('TELEFONO'));
						}
					);
					return false;
					
				}else {
					if ( record.data['TELEFONO'].length >= 30){
						totoalError++;
						Ext.MessageBox.alert('Mensaje',	'El Tel�fono no debe de sobrepasar los 30 caracteres.',
							function(){
								gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('TELEFONO'));
							}
						);
						return false;	
					}
				}
				
				if(! Ext.isEmpty(record.data['FAX']) ) {
					if ( record.data['FAX'].length >= 30){
						totoalError++;
						Ext.MessageBox.alert('Mensaje',	'El Fax no debe de sobrepasar los 30 caracteres.',
							function(){
								gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('TELEFONO'));
							}
						);
						return false;	
					}
				}
				
				if( Ext.isEmpty(record.data['CORREO_ELECTRONICO']) ) {
					totoalError++;
					Ext.MessageBox.alert('Mensaje',	'El	Correo Electr�nico es obligatorio',
						function(){
							gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('CORREO_ELECTRONICO'));
						}
					);
					return false;	
				}else {
					if ( record.data['CORREO_ELECTRONICO'].length >= 100){
						totoalError++;
						Ext.MessageBox.alert('Mensaje',	'El Correo Electr�nico no debe de sobrepasar los 100 caracteres.',
							function(){
								gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('TELEFONO'));
							}
						);
						return false;	
					}
				}
				
			
				if(totoalError==0) {
					ic_contacto.push(record.data['IC_CONTACTO']);
					ap_paterno.push(record.data['APELLIDO_PATERNO']);
					ap_materno.push(record.data['APELLIDO_MATERNO']);
					nombre.push(record.data['NOMBRES']);
					telefono.push(record.data['TELEFONO']);
					fax.push(record.data['FAX']);
					email.push(record.data['CORREO_ELECTRONICO']);
				}	
			
			}
		});
		
		if(totoalError==0) {
			Ext.Ajax.request({
				url : '15forma6ext.data.jsp',
				params : {
					informacion: 'GuardarContactos',	
					ic_contacto : ic_contacto,
					ap_paterno : ap_paterno,
					ap_materno : ap_materno,
					nombre : nombre,	
					telefono : telefono,	
					fax : fax,
					email : email,
					ic_pyme: Ext.getCmp('ic_pyme1').getValue()
				},
				 callback: procesarSuccessFailureGuardar
			});
		}
			 
	
	}
	
	
	var agregar = function(){
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		var  totalReg= store.getCount();
		
		 Todas = Ext.data.Record.create([ 
			{ name: 'IC_CONTACTO'},
			{ name: 'APELLIDO_PATERNO'},
			{ name: 'APELLIDO_MATERNO'},
			{ name: 'NOMBRES'},
			{ name: 'TELEFONO'},
			{ name: 'FAX'},
			{ name: 'CORREO_ELECTRONICO'}	,
			{ name: 'INHABILITADO'}		
		]);
														
		store.insert(totalReg,new Todas({ 
			IC_CONTACTO:'',
			APELLIDO_PATERNO:'',
			APELLIDO_MATERNO:'',
			NOMBRES:'', 
			TELEFONO:'',
			FAX:'',
			C0RREO_ELECTRONICO:'',
			INHABILITADO:'N'
		})); 
		store.commitChanges(); 	
		
		Ext.getCmp('btnGuardar').enable();
		
	}
	
	
	// ------------GENERACION DE LA CONSULTA ------------------
	 
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
					
			gridConsulta.setTitle(jsonData.nombrePyme);
			
			
			if(store.getTotalCount() > 0) {
				if(jsonData.regprimer=='S') {
					Ext.getCmp('btnGuardar').enable();
				}else {
					Ext.getCmp('btnGuardar').disable();
				}
				el.unmask();					
			} else {					
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15forma6ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},			
		fields: [	
			{ name: 'IC_CONTACTO'},
			{ name: 'APELLIDO_PATERNO'},
			{ name: 'APELLIDO_MATERNO'},
			{ name: 'NOMBRES'},
			{ name: 'TELEFONO'},
			{ name: 'FAX'},
			{ name: 'CORREO_ELECTRONICO'},
			{ name: 'INHABILITADO'}			
		],			
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});


	

	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		clicksToEdit: 1, 
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		hidden:true,
		title: 'Base de Operaci�n de Cr�dito Educativo',
		tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
				{
					text: 'Agregar Contacto',
					iconCls: 'icon-register-add',            
					scope: this,
					handler: agregar					
				}				
			] 
		},							
		columns: [	
			{
				header: '<div><div align=center >Apellido Paterno</div>',
				tooltip: 'Apellido Paterno',
				dataIndex: 'APELLIDO_PATERNO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				editor:{
					xtype:	'textfield',					
					maxLength: 25,
					enableKeyEvents: true,
					allowBlank: false,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 25){
								field.setValue((field.getValue()).substring(0,24));
								Ext.Msg.alert('Observaciones','El Apellido Paterno no debe de sobrepasar los 25 caracteres.');
								field.focus();
							}
						}
					}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					if(registro.get('INHABILITADO')== 'N')  {
						return NE.util.colorCampoEdit(value,metadata,registro);
					}else{
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						var color = registro.data['COLOR'];	
						return '<span style="color:red;">' + value + '</span>';
					}					
				}
			},	
			{
				header: '<div><div align=center >Apellido Materno</div>',
				tooltip: 'Apellido Materno',
				dataIndex: 'APELLIDO_MATERNO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				editor:{
					xtype:	'textfield',				
					maxLength: 25,
					enableKeyEvents: true,
					allowBlank: false,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 25){
								field.setValue((field.getValue()).substring(0,24));
								Ext.Msg.alert('Observaciones','El Apellido Materno no debe de sobrepasar los 25 caracteres.');
								field.focus();
							}
						}
					}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					if(registro.get('INHABILITADO')== 'N')  {
						return NE.util.colorCampoEdit(value,metadata,registro);
					}else{
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						var color = registro.data['COLOR'];	
						return '<span style="color:red;">' + value + '</span>';
					}					
				}
			},	
			{
				header: '<div><div align=center >Nombre(s)</div>',
				tooltip: 'Nombre(s)',
				dataIndex: 'NOMBRES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				editor:{
					xtype:	'textfield',					
					maxLength: 80,
					enableKeyEvents: true,
					allowBlank: false,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 80){
								field.setValue((field.getValue()).substring(0,79));
								Ext.Msg.alert('Observaciones','El nombre no debe de sobrepasar los 80 caracteres.');
								field.focus();
							}
						}
					}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					if(registro.get('INHABILITADO')== 'N')  {
						return NE.util.colorCampoEdit(value,metadata,registro);
					}else{
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						var color = registro.data['COLOR'];	
						return '<span style="color:red;">' + value + '</span>';
					}					
				}				
			},
			{
				header: '<div><div align=center >Tel�fono</div>',
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				editor: { 
					xtype:'numberfield',
					maxLength: 30,						
					allowBlank: false
				},			
            renderer: function(value, metadata, registro, rowIndex, colIndex, store){  				
					if(registro.get('INHABILITADO')== 'N')  {
						return NE.util.colorCampoEdit(value,metadata,registro);
					}else{
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						var color = registro.data['COLOR'];	
						return '<span style="color:red;">' + value + '</span>';
					}			
            }
			},		
			{
				header: '<div><div align=center >Fax</div>',				
				tooltip: 'Fax',
				dataIndex: 'FAX',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				editor: { 
					xtype:'numberfield',
					maxLength: 30,
					allowBlank: false,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				},
            renderer: function(value, metadata, registro, rowIndex, colIndex, store){            	
					if(registro.get('INHABILITADO')== 'N')  {
						return NE.util.colorCampoEdit(value,metadata,registro);
					}else{
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						var color = registro.data['COLOR'];	
						return '<span style="color:red;">' + value + '</span>';
					}			
            }
			},
			{
				header: '<div><div align=center >Correo Electr�nico</div>',	
				tooltip: 'Correo Electr�nico',
				dataIndex: 'CORREO_ELECTRONICO',
				sortable: true,
				width: 200,			
				resizable: true,				
				align: 'left',
				editor:{
					xtype:	'textfield',
					vtype: 'email'	,				
					maxLength: 100,
					enableKeyEvents: true,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 100){
								field.setValue((field.getValue()).substring(0,99));
								Ext.Msg.alert('Observaciones','El Correo Electr�nico no debe de sobrepasar los 100 caracteres.');
								field.focus();
							}
						}
					}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					if(registro.get('INHABILITADO')== 'N')  {
						return NE.util.colorCampoEdit(value,metadata,registro);
					}else{
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						var color = registro.data['COLOR'];	
						return '<span style="color:red;">' + value + '</span>';
					}
				}
			},
			{
	       xtype: 'actioncolumn',
				 header: 'Eliminar',
				 tooltip: 'Eliminar',				
				 width: 130,
				 align: 'center',
				 items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {		
							if(registro.get('IC_CONTACTO') !='' &&  registro.get('INHABILITADO')== 'N')  {
								this.items[0].tooltip = 'Ver';
								return 'borrar';	
							}
						}
						,handler: procesarInhabilitar
					}
				]	
			}	
		],			
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		listeners: {	
			beforeedit: function(e){		
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
				if(campo == 'APELLIDO_PATERNO' || campo == 'APELLIDO_MATERNO' || campo == 'NOMBRES'  || campo == 'TELEFONO' ||  campo == 'FAX'   || campo == 'CORREO_ELECTRONICO' ){
					Ext.getCmp('btnGuardar').enable();
				}										
				if(campo == 'APELLIDO_PATERNO' || campo == 'APELLIDO_MATERNO' || campo == 'NOMBRES'  || campo == 'TELEFONO' ||  campo == 'FAX'   || campo == 'CORREO_ELECTRONICO' ){				
					if(record.data['INHABILITADO']=='N'){//retorno
						return true;
					}else {
						return false;
					}			
				}
			}
		},		
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',
				{
					xtype: 'button',
					id: 'btnGuardar',
					iconCls: 'icoGuardar',
					text: 'Guardar',
					disabled:true,
					handler: procesarGuardar					
				}	
			]
		}
	});
	






//*************************Criterios de Busqueda ******************************

	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			
			if(jsonObj.validaEntidad=='N') {			
				Ext.getCmp('num_electronico1').setValue('');	
				Ext.getCmp('txtNombre1').setValue('');	
				Ext.getCmp('ic_pyme1').setValue('');					
				Ext.MessageBox.alert("Mensaje","La PYME no opera como Entidad de Gobierno ");	
				Ext.getCmp('gridConsulta').hide();
			}else {
				Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);	
				Ext.getCmp('ic_pyme1').setValue(jsonObj.ic_pyme);	
				
				if(jsonObj.txtNombre==''){
					Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
					Ext.getCmp('num_electronico1').setValue('');	
					Ext.getCmp('gridConsulta').hide();
				}
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15forma6ext.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},			
		{
			xtype: 'numberfield',
			name: 'n_electronico',
			id: 'n_electronico1',
			fieldLabel: 'N�mero de Nafin Electr�nico',
			allowBlank: true,
			maxLength: 38,	
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'			
		},	
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 		'panel',
			bodyStyle: 	'padding: 10px',
			layout: {
				type: 	'hbox',
				pack: 	'center',
				align: 	'middle'
			},
			items: [
				{
					xtype: 'button',
					text: 'Buscar',
					id: 'btnBuscar',
					iconCls: 'icoBuscar',
					width: 	75,
					handler: function(boton, evento) {
						var pymeComboCmp = Ext.getCmp('cbPyme1');
						pymeComboCmp.setValue('');
						pymeComboCmp.setDisabled(false);								
						storeBusqAvanzPyme.load({
							params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
								informacion: 'busquedaAvanzada'					
							})																	
						});				
					},
					style: { 
						marginBottom:  '10px' 
					} 
				}
			]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		}	
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);		
					// Petici�n b�sica  
						Ext.Ajax.request({  
							url: '15forma6ext.data.jsp',  
							method: 'POST',  
							callback: successAjaxFn,  
							params: {  
								informacion: 'pymeNombre' ,
								num_electronico: Ext.getCmp('num_electronico1').getValue()
							}  
						});
							
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});
	
	var  elementosForma =  [	
		{
			xtype: 'compositefield',
			fieldLabel: 'No. de Nafin Electr�nico ',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'No. de Nafin Electr�nico',
					allowBlank: true,
					maxLength: 38,	
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){
						 // Petici�n b�sica  
						 var num_electronico= 	Ext.getCmp('num_electronico1');
					
							if(!Ext.isEmpty(num_electronico.getValue())){
								Ext.Ajax.request({  
									url: '15forma6ext.data.jsp',  
									method: 'POST',  
									callback: successAjaxFn,  
									params: {  
										 informacion: 'pymeNombre' ,
										 num_electronico: Ext.getCmp('num_electronico1').getValue()
									}  
								}); 
							}
						}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					disabled:true,
					maxLength: 100,
					width: 300,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				}			
			]
		},
		{ xtype: 'textfield', 	name: 'ic_pyme', 	id: 'ic_pyme1', 	 hidden: true 	},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'button',
					text: 'B�squeda Avanzada...',
					iconCls: 'icoBuscar',
					id: 'btnBusqAv',
					handler: function(boton, evento) {
						var pymeComboCmp = Ext.getCmp('cbPyme1');
						pymeComboCmp.setValue('');
						pymeComboCmp.setDisabled(true);
						var ventana = Ext.getCmp('winBusqAvan');
						if (ventana) {
							ventana.show();
						} else {
							new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
						}
					}
				}
			]
		}		
	];		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',				
				formBind: true,				
				handler: function(boton, evento) {						
					
					var num_electronico= 	Ext.getCmp('num_electronico1');
					
					if(Ext.isEmpty(num_electronico.getValue())){
						num_electronico.markInvalid('El No. de Nafin Electr�nico  es obligatorio ');
						num_electronico.focus();
						return;
					}
						
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{  
							informacion: 'Consultar',
							txtNombre: Ext.getCmp('txtNombre1').getValue()							
						})
					});
					
				}
			}			
		]	
		
	});
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,			
			NE.util.getEspaciador(20)
		]
	});
	
});