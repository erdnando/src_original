
<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
   java.sql.*,
	com.netro.seguridadbean.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 

/*** OBJETOS ***/
com.netro.cadenas.ClausuladoNafinIF paginador;
CQueryHelperRegExtJS queryHelper;
CQueryHelper queryHelper2;
CatalogoSimple cat;
JSONObject jsonObj;
JSONObject jsonObjP;
Afiliacion afiliacion;
Registros registros;
JSONArray jsonArr;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String ic_consecutivo 	= (request.getParameter("ic_consecutivo") == null)?"":request.getParameter("ic_consecutivo");
String ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String txt_nombre			= (request.getParameter("hid_nombre")==null)?"":request.getParameter("hid_nombre");
String txt_nafelec		= (request.getParameter("txt_nafelec")==null)?"":request.getParameter("txt_nafelec");
String txt_usuario		= (request.getParameter("txt_usuario")==null)?"":request.getParameter("txt_usuario");
String txt_fecha_acep_de= (request.getParameter("txt_fecha_acep_de")==null)?"":request.getParameter("txt_fecha_acep_de");
String txt_fecha_acep_a	= (request.getParameter("txt_fecha_acep_a")==null)?"":request.getParameter("txt_fecha_acep_a");
String ic_if				= (request.getParameter("noIc_if")==null)?"":request.getParameter("noIc_if");
String noBancoFondeo		= (request.getParameter("noBancoFondeo")==null)?"":request.getParameter("noBancoFondeo");
String hidAction			= (request.getParameter("hidAction")==null)?"":request.getParameter("hidAction");
String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String ic_epo				= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String cvePerf          = "";
try{   
   //com.netro.seguridadbean.SeguridadHome seguridadHome = (com.netro.seguridadbean.SeguridadHome)netropology.utilerias.ServiceLocator.getInstance().getEJBHome("SeguridadEJB", com.netro.seguridadbean.SeguridadHome.class);
   //com.netro.seguridadbean.Seguridad	seguridadBean = seguridadHome.create();
   com.netro.seguridadbean.Seguridad	seguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
   cvePerf = seguridadBean.getTipoAfiliadoXPerfil(strPerfil);
}catch(SeguException seg){}

String resultado			= "";
String epoDefault 		= "";
String qrySentencia		= "";
String condicion 			= "";
int start					= 0;
int limit 					= 0;
AccesoDB con 				= null;
PreparedStatement ps 	= null;
ResultSet rs 				= null;
/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/

if(strTipoUsuario.equals("IF")){
  ic_if = iNoCliente;
}

if(   informacion.equals("IniciaPrograma") ){
   jsonObj = new JSONObject();
   jsonObj.put("success", new Boolean(true));
   jsonObj.put("cvePerf", cvePerf);
   resultado = jsonObj.toString();
}


/*** INICIO CATALOGO DE BANCO FONDEO ***/
else if(informacion.equals("catalogoBancoFondeo")){
	cat = new CatalogoSimple();
	
	cat.setTabla("comcat_banco_fondeo");
	cat.setCampoClave("ic_banco_fondeo");
	cat.setCampoDescripcion("cd_descripcion");
	
   cvePerf = cvePerf.equals("")?"1":cvePerf;
   
	switch(Integer.parseInt(cvePerf.trim())){
		case 8 : cat.setCondicionIn("2", "ic_banco_fondeo"); break;
		case 4 : cat.setCondicionIn("1", "ic_banco_fondeo"); break;
		default  : cat.setOrden("1");
	}
	resultado = cat.getJSONElementos();
} /*** FIN DEL CATALOGO DE BANCO FONDEO***/

/*** INICIO CATALOGO NOMBRE IF ***/
else if(informacion.equals("catalogoNombreIF")){
   con = new AccesoDB();
		try {
			StringBuffer tablas = new StringBuffer();
			StringBuffer condiciones = new StringBuffer();

			if (!ic_epo.equals("")) {

				tablas.append(
					" comcat_if I, comrel_if_epo IE ");
				condiciones.append(
					" WHERE IE.ic_if=I.ic_if "+
					" AND I.cs_habilitado='S' "+
					" AND IE.ic_epo="+ic_epo+
					" AND IE.cs_vobo_nafin = 'S'");
				//condicion.append( (!"".equals(vobonafin))?vobonafin:" 'S' ");

			} else {
				tablas.append( 
					" comcat_if I ");
				condiciones.append(
					" WHERE I.cs_habilitado='S' ");
			}

			if(!"".equals(noBancoFondeo)){			
				condiciones.append( 
					" AND EXISTS( " +
					" 		SELECT rie.ic_if " +
					" 		FROM comrel_if_epo_x_producto rie, "+
					" 			comcat_epo epo " +
					" 		WHERE " +
					" 			rie.ic_if = I.ic_if " +
					" 			AND rie.ic_epo = epo.ic_epo " +
					" 			AND epo.ic_banco_fondeo = " + noBancoFondeo + ")");
			}
			
			condiciones.append(" ORDER BY 2 ");

			qrySentencia = "SELECT ic_if,cg_razon_social "+
					" FROM "+tablas+
					condiciones;
      
			//rs;
			String cadena = "";

			con.conexionDB();
			System.out.println(">>>>>>>>> # " + qrySentencia + " # >>>>>>>>>>>");
			rs = con.queryDB(qrySentencia);

			HashMap datos;
         List reg = new ArrayList();
			while(rs.next()) {            
            datos = new HashMap();
            datos.put("clave",rs.getString(1));
            datos.put("descripcion",rs.getString(2));
            reg.add(datos);
			}
			rs.close();
			con.cierraStatement();
			
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("registros", reg);
			resultado = jsonObj.toString();
			rs.close();
			con.cierraStatement();
		} catch (Exception e) {
			System.err.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
   
} /*** FIN CATALOGO NOMBRE IF ***/

/*** INICIO PYME NOMBRE ***/
else if(informacion.equals("pymeNombre")) {
	try{
		con = new AccesoDB();
		con.conexionDB();
		ic_epo=iNoCliente;
		queryHelper2 = new CQueryHelper(new ClausuladoNafinCA());
		queryHelper2.cleanSession(request);
      String mensaje = "";
	
		if(!"".equals(txt_nafelec) && "".equals(ic_pyme)) {
			if(!"".equals(ic_if)) { 
				condicion = " AND con.ic_if = ?"  ;
			}
			qrySentencia = 
                  " SELECT pym.ic_pyme, pym.cg_razon_social"+
                  "   FROM com_aceptacion_contrato_if con,"+
                  "        comcat_pyme pym, "+
                  "        comrel_nafin crn"+
                  "  WHERE con.ic_pyme = pym.ic_pyme"+
                  "    AND con.ic_pyme = crn.ic_epo_pyme_if"+
                  "    AND crn.ic_nafin_electronico = ?"+
                  condicion;
				
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(txt_nafelec));
								
			if(!"".equals(ic_if)){
				ps.setInt(2,Integer.parseInt(ic_if));
			}
			
			rs = ps.executeQuery(); 
			if(rs.next()){
				ic_pyme = rs.getString(1)==null?"":rs.getString(1);
				txt_nombre = rs.getString(2)==null?"":rs.getString(2);  
			} else {
            if(!"".equals(ic_if) && "".equals(txt_nombre)){
               mensaje = "El nafin electronico no corresponde a una PyME afiliada a la IF o no existe.";
             }else{ 
               mensaje = "El nafin electronico no corresponde a una PyME o no existe.";
             }
				ic_pyme = "";
				txt_nombre="";
			}
			
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("pyme", txt_nombre);
			jsonObj.put("ic_pyme",ic_pyme);
			jsonObj.put("mensaje",mensaje);
			resultado = jsonObj.toString();
			
			rs.close();
			ps.close();
		}
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}		
} /*** FIN PYME NOMBRE ***/

/*** INICIO CONSULTA ***/
else if(informacion.equals("Consulta")) { 
	paginador = new com.netro.cadenas.ClausuladoNafinIF();
	String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
	/* SE PASAN LOS VALORES AL OBJETO PAGINADOR PARA REALIZAR LA CONSULTA */
	paginador.setIc_consecutivo(ic_consecutivo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setTxt_usuario(txt_usuario);
	paginador.setTxt_fecha_acep_de(txt_fecha_acep_de);
	paginador.setTxt_fecha_acep_a(txt_fecha_acep_a);
	paginador.setIc_if(ic_if);
	paginador.setNoBancoFondeo(noBancoFondeo);
	queryHelper	= new CQueryHelperRegExtJS( paginador ); 
	
	if((informacion.equals("Consulta") && operacion.equals("Generar")) || (informacion.equals("Consulta") && operacion.equals(""))){ 
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));				
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if(operacion.equals("Generar")){
				queryHelper.executePKQuery(request);
			}
			Registros reg = queryHelper.getPageResultSet(request,start,limit);
			resultado	= "{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if(operacion.equals("XLS")||operacion.equals("PDF")){
		jsonObj = new JSONObject();
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,operacion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
	}	
} /*** FIN DE CONSULTA ***/

/*** INICIO DE CONTRATO ***/
else if(informacion.equals("Contrato")){
	String ClaveIF				= (request.getParameter("clave_if")==null)?"":request.getParameter("clave_if");
	jsonObj = new JSONObject();
	ContratoIFArchivo contratoArchivo = new ContratoIFArchivo();
	if (cvePerf.equals("4") ){
		contratoArchivo.setClaveIF(ClaveIF);
	}else{
		contratoArchivo.setClaveIF(iNoCliente);
	}
	contratoArchivo.setConsecutivo(ic_consecutivo);
	//extension = contratoArchivo.getExtension();
		String nombreArchivo = contratoArchivo.getArchivoContratoIF(strDirectorioTemp);
		String extension = contratoArchivo.getExtension();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("EXTENSION", extension);
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		
		
		resultado = jsonObj.toString();
} /*** FIN DE CONTRATO ***/

/*** INICIA CATALOGO DE BUSQUEDA AVANZADA ***/
else if (informacion.equals("CatalogoBuscaAvanzada")) {
	String num_pyme				= (request.getParameter("num_pyme")==null || request.getParameter("num_pyme").equals(""))?"":request.getParameter("num_pyme");
	String rfc_pyme				= (request.getParameter("rfc_pyme")==null || request.getParameter("rfc_pyme").equals(""))?"":request.getParameter("rfc_pyme");
	String nombre_pyme			= (request.getParameter("nombre_pyme")==null || request.getParameter("nombre_pyme").equals(""))?"":request.getParameter("nombre_pyme");	
	String ic_producto_nafin 	= (request.getParameter("ic_producto_nafin")==null)?"":request.getParameter("ic_producto_nafin");
	String pantalla 			   = "InformacionDocumentos";
   ic_if				            = (request.getParameter("noIc_if")==null)?"":request.getParameter("noIc_if");
	
	List coleccionElementos = new ArrayList();
	jsonArr						= new JSONArray();
	jsonObj 						= new JSONObject();
	con				= null;
	condicion		= "";
	String				condicionDoc	= "";
	String				tablaDoc		= "";
	qrySentencia	= "";
	ps				= null;
	rs				= null;
	Vector vecRows = new Vector();
	Vector vecCols = null;
	String campoProveedor = "0"; //Geag
	
	try {
		con = new AccesoDB();
		con.conexionDB();
		if(!"".equals(ic_pyme)) {
			condicion = "";

			if(!"".equals(ic_epo)) {
				condicion += "    AND r.ic_epo = ?";
				campoProveedor = "r.cg_pyme_epo_interno"; //Geag
			} else {
				//Si la EPO es nula, entonces el numero de proveedor no tiene sentido
				//por lo cual se pone un valor fijo para poder hacer un distinct
				campoProveedor = "0"; //Geag
			}
			
			qrySentencia = 
				" SELECT /*+index(crn CP_COMREL_NAFIN_PK) use_nl(r p)*/"   +
//				"        p.ic_pyme, r.cg_pyme_epo_interno num_prov, p.cg_razon_social,"   +
				"        distinct p.ic_pyme, " + campoProveedor + " as num_prov, p.cg_razon_social,"   +	//Geag
				"        crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega, "   +
				"        crn.ic_nafin_electronico"   +
				"   FROM comrel_nafin crn, comrel_pyme_epo r, comcat_pyme p"   +
				"  WHERE crn.ic_epo_pyme_if = p.ic_pyme"   +
				"    AND p.ic_pyme = r.ic_pyme"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND p.ic_pyme = ?"   +
				"    AND r.ic_pyme = ?"+condicion;
//			System.out.println("\n qrySentencia: "+qrySentencia);
			if(!ic_if.equals("")){
				qrySentencia +=  
					" AND p.ic_pyme in ( select "  +
					"	      distinct "  +
					"		      cuenta.ic_pyme "  + 
					"     from  "  +
					"	      comrel_pyme_if rel, "  +
					"	      comrel_cuenta_bancaria cuenta "  +
					"     where "  +
					"	      rel.cs_vobo_if = 'S' and "  + 
					"	      rel.cs_borrado = 'N' and "  +
					"	      rel.ic_if      =  ?  and "  +
					"	      rel.ic_cuenta_bancaria = cuenta.ic_cuenta_bancaria and "  +
					"	cuenta.cs_borrado = 'N' ) ";
			}

			System.out.println(qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_pyme));
			ps.setInt(2,Integer.parseInt(ic_pyme));
			int cont = 2;
			if(!"".equals(ic_epo)){
				cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
			}
			if(!"".equals(ic_if)){ 
				cont++;ps.setString(cont,ic_if);
			}
		} else {
			condicion = "";

			if(!"".equals(ic_epo)) {
				condicion += "    AND pe.ic_epo = ?";
				campoProveedor = "pe.cg_pyme_epo_interno"; //Geag
			} else {
				//Si la EPO es nula, entonces el numero de proveedor no tiene sentido
				//por lo cual se pone un valor fijo para poder hacer un distinct
				campoProveedor = "0"; //Geag
			}
			if(!"".equals(num_pyme))
				condicion += "    AND cg_pyme_epo_interno LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
			if(!"".equals(rfc_pyme))
				condicion += "    AND cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
			if(!"".equals(nombre_pyme)){
				condicion += "    AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
			}
			if(!("2".equals(ic_producto_nafin)||"5".equals(ic_producto_nafin)) && !pantalla.equals("listReq") && !pantalla.equals("consSinNumProv")) {
				condicionDoc = 
					"    AND d.ic_pyme = pe.ic_pyme"   +
					"    AND d.ic_epo = pe.ic_epo";
				tablaDoc = "com_documento d,";
			}
			
			if( pantalla.equals("MantenimientoDoctos") || pantalla.equals("InformacionDocumentos") || pantalla.equals("CambioEstatus") ) {
				condicionDoc = 
					"		AND PE.cs_habilitado 			= 	'S' " +
					"		AND P.cs_habilitado 				= 	'S' " +
					"		AND P.cs_invalido 				!= 'S' " +
					"		AND d.ic_pyme 	= pe.ic_pyme			 " +
					"		AND pe.cs_num_proveedor 		= 'N'  " +  // Tiene CG_PYME_EPO_INTERNO
					"		AND d.ic_epo 	= pe.ic_epo";
				tablaDoc = "com_documento d,";
			}
			
			qrySentencia = 
				" SELECT /*+index(pe CP_COMREL_PYME_EPO_PK) index(p CP_COMCAT_PYME_PK) use_nl(pe d p)*/ " +
//				"        p.ic_pyme, pe.cg_pyme_epo_interno, p.cg_razon_social," +
				"        distinct p.ic_pyme, " + campoProveedor + " as cg_pyme_epo_interno, p.cg_razon_social," + //GEAG
				"        crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega," +
				"        crn.ic_nafin_electronico" +
				"   FROM comrel_pyme_epo pe, "+tablaDoc+"comrel_nafin crn, comcat_pyme p" +
				"  WHERE p.ic_pyme = pe.ic_pyme" +
				"    AND p.ic_pyme = crn.ic_epo_pyme_if" +
				condicionDoc+
				"    AND crn.cg_tipo = 'P' "+condicion;
				if(!pantalla.equals("listReq") && !pantalla.equals("consSinNumProv") && !pantalla.equals("MantenimientoDoctos") && !pantalla.equals("InformacionDocumentos") && !pantalla.equals("CambioEstatus") ){
				     qrySentencia += "    AND pe.cs_habilitado = 'S'"+
				     		"    AND p.cs_habilitado = 'S'"+
				     		"    AND pe.cg_pyme_epo_interno IS NOT NULL ";
				}
				if(pantalla.equals("consSinNumProv")){
					qrySentencia += 
				     		" AND pe.cg_pyme_epo_interno IS NULL "+
				     		" AND pe.cs_num_proveedor = 'S'";
				}
				
				if(!ic_if.equals("")){
				qrySentencia +=  
					" AND p.ic_pyme in ( select "  +
					"	      distinct "  +
					"		      cuenta.ic_pyme "  + 
					"     from  "  +
					"	      comrel_pyme_if rel, "  +
					"	      comrel_cuenta_bancaria cuenta "  +
					"     where "  +
					"	      rel.cs_vobo_if = 'S' and "  + 
					"	      rel.cs_borrado = 'N' and "  +
					"	      rel.ic_if      =  ?  and "  +
					"	      rel.ic_cuenta_bancaria = cuenta.ic_cuenta_bancaria and "  +
					"	cuenta.cs_borrado = 'N' ) ";
				}
				qrySentencia += 
				
				"  GROUP BY p.ic_pyme," +
				"           pe.cg_pyme_epo_interno," +
				"           p.cg_razon_social," +
				"           RPAD (pe.cg_pyme_epo_interno, 10, ' ') || p.cg_razon_social," +
				"           crn.ic_nafin_electronico" +
				"  ORDER BY p.cg_razon_social"  ;
			//System.err.println("\n qrySentencia = < "+qrySentencia + ">"); // Debug info
			System.out.println(qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			int cont = 0;
			if(!"".equals(ic_epo)) {
				cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
			}
			if(!"".equals(num_pyme)) {
				cont++;ps.setString(cont,num_pyme);
			}
			if(!"".equals(rfc_pyme)) {
				cont++;ps.setString(cont,rfc_pyme);
			}
			if(!"".equals(nombre_pyme)) {
				cont++;ps.setString(cont,nombre_pyme);
			}
			if(!"".equals(ic_if)) {
				cont++;ps.setString(cont,ic_if);
			} 
		} // fin else
		rs = ps.executeQuery();
		HashMap jsonHM;
		int cont =0;
		
		while(rs.next()) {
			jsonHM = new HashMap();		
			jsonHM.put("clave", rs.getString("IC_NAFIN_ELECTRONICO"));
			jsonHM.put("descripcion", rs.getString("IC_NAFIN_ELECTRONICO")+" "+rs.getString("CG_RAZON_SOCIAL"));
			jsonHM.put("ic_pyme",rs.getString("IC_PYME"));
			jsonArr.add(jsonHM);
			cont++;
		}
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("total", "0");
		jsonObj.put("registros", jsonArr);
		resultado = "{\"success\": true, \"total\": \"" + cont + "\", \"registros\": " + jsonArr.toString()+"}";;
		rs.close();
		if(ps!=null) ps.close();
	} catch(Exception e){
		System.out.println("getProveedores:: Exception "+e);
		e.printStackTrace();
	} finally{
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
		System.out.println("\n getProveedores::(S) ");
	}
	
	
	
	
	resultado = jsonObj.toString();
	
} /*** FIN CATALOGO BUSQUEDA AVANZADA ***/
%>
<%= resultado%>
