<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoEstado,com.netro.afiliacion.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
//Afiliacion afilia = afiliacionHome.create();
Afiliacion afilia = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);


if ( informacion.equals("Consulta") ){

	JSONObject jsonObj = new JSONObject();
	String txtRFC = (request.getParameter("txtRFC")!=null)?request.getParameter("txtRFC"):"";
	Registros reg = afilia.getPymeSinNumProv(iNoCliente,txtRFC);
	if (reg != null){
		jsonObj.put("registros", reg.getJSONData());
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if ( informacion.equals("Guarda") ){
	
	JSONObject jsonObj = new JSONObject();
	
	String cadList = (request.getParameter("lista")!=null)?request.getParameter("lista"):"";
	JSONArray listaH = JSONArray.fromObject(cadList);

	afilia.restablecePymeSinNumProv(listaH,iNoUsuario);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>