<%--
Propiedad de Nacional Financiera, S.N.C., se prohibe su reproduccion total o parcial
--%>
<%@ page import="java.io.*, java.util.*,  netropology.utilerias.*, com.netro.exception.*, javax.naming.*, com.netro.descuento.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="../../015secsession.jspf"%>
<%
	// Nota: corregir este JSP, para que vaya leyendo archivos de bloque en bloque y guardando archivos
	// de bloque en bloque

	String procesoID = (request.getParameter("procesoID") == null) ? "" : request.getParameter("procesoID");

	CreaArchivo 	archivo 				= new CreaArchivo();
	StringBuffer 	contenidoArchivo 	= new StringBuffer();
	String 			nombreArchivo 		= null;

	// Leer lista de lineas con errores
	AccesoDB 		con 						= new AccesoDB();
	List 				lvarbind 				= new ArrayList();
	Registros 		registrosConError 	= null;
	Registros 		registrosConErrorEcon 	= null;
	String 			query 					= "";
	int x =0, y=0;
	try{
		con.conexionDB();
		query =
			"SELECT " +
				"CG_NUMERO_PROVEEDOR, CG_RFC, CG_APPAT, CG_APMAT, CG_NOMBRE, CG_RAZON_SOCIAL, CG_CALLE, CG_COLONIA, CG_ESTADO, CG_PAIS,"+
				"CG_MUNICIPIO, CG_CP, CG_TELEFONO, CG_FAX, CG_EMAIL, CG_APPAT_C, CG_APMAT_C, CG_NOMBRE_C, CG_TELEFONO_C, CG_FAX_C,"+
				"CG_EMAIL_C, CG_TIPO_CLIENTE, CG_SUSCEPTIBLE_DESCONTAR " +
			"FROM COMTMP_AFILIA_PYME_MASIVA " +
			"WHERE " +
				"IC_NUMERO_PROCESO = ? AND "+
				"CG_MENSAJES_ERROR IS NOT NULL " +
			"ORDER BY IC_NUMERO_LINEA ASC ";
		lvarbind.add(procesoID);
		registrosConError = con.consultarDB(query,lvarbind,false);

		/*****/
		query =
			"SELECT " +
				"CG_NUMERO_PROVEEDOR, CG_RFC, CG_APPAT, CG_APMAT, CG_NOMBRE, CG_RAZON_SOCIAL, CG_CALLE, CG_COLONIA, CG_ESTADO, CG_PAIS,"+
				"CG_MUNICIPIO, CG_CP, CG_TELEFONO, CG_FAX, CG_EMAIL, CG_APPAT_C, CG_APMAT_C, CG_NOMBRE_C, CG_TELEFONO_C, CG_FAX_C,"+
				"CG_EMAIL_C, CG_TIPO_CLIENTE, CG_SUSCEPTIBLE_DESCONTAR " +
			"FROM ECON_AFILIA_PYME_MASIVA " +
			"WHERE " +
				"IC_NUMERO_PROCESO = ? AND "+
				"CG_MENSAJES_ERROR IS NOT NULL " +
			"ORDER BY IC_NUMERO_LINEA ASC ";
		//lvarbind.add(procesoID);
		registrosConErrorEcon = con.consultarDB(query,lvarbind,false);
	}catch (Exception e){
		e.printStackTrace();
		throw new NafinException("SIST0001");
	}finally{
		if(con.hayConexionAbierta()) con.cierraConexionDB();
	}

	// Formatear lineas con errores
		while(registrosConError != null && registrosConError.next()) {
			x++;
			if(x==1){
				contenidoArchivo.append("Nafin Electronico");
				contenidoArchivo.append("\n");
				contenidoArchivo.append("\n");
			}

			// Extraer registros

			contenidoArchivo.append(registrosConError.getString("CG_NUMERO_PROVEEDOR")); contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_RFC"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_APPAT"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_APMAT"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_NOMBRE"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_RAZON_SOCIAL"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_CALLE"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_COLONIA"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_ESTADO"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_PAIS"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_MUNICIPIO"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_CP"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_TELEFONO"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_FAX"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_EMAIL"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_APPAT_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_APMAT_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_NOMBRE_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_TELEFONO_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_FAX_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_EMAIL_C"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_TIPO_CLIENTE"));contenidoArchivo.append("|");
			contenidoArchivo.append(registrosConError.getString("CG_SUSCEPTIBLE_DESCONTAR"));contenidoArchivo.append("|");

			// Agregar registro a archivo con errores
			contenidoArchivo.append("\n");

		}

		// Formatear lineas con errores
				while(registrosConErrorEcon != null && registrosConErrorEcon.next()) {

					y++;
					if(y==1){
						contenidoArchivo.append("Econtract");
						contenidoArchivo.append("\n");
						contenidoArchivo.append("\n");
					}
					// Extraer registros
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_NUMERO_PROVEEDOR")); contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_RFC"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_APPAT"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_APMAT"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_NOMBRE"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_RAZON_SOCIAL"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_CALLE"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_COLONIA"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_ESTADO"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_PAIS"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_MUNICIPIO"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_CP"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_TELEFONO"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_FAX"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_EMAIL"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_APPAT_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_APMAT_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_NOMBRE_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_TELEFONO_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_FAX_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_EMAIL_C"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_TIPO_CLIENTE"));contenidoArchivo.append("|");
					contenidoArchivo.append(registrosConErrorEcon.getString("CG_SUSCEPTIBLE_DESCONTAR"));contenidoArchivo.append("|");

					// Agregar registro a archivo con errores
					contenidoArchivo.append("\n");

		}

%>
<html>
<head>
	<title>Nafinet</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="<%=strDirecVirtualCSS%>/css/<%=strClase%>">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="300" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td valign="top" align="center">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="formas" align="center"><br>
<%
	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt")){
		out.print("<--!Error al generar el archivo-->");
	}else{
		nombreArchivo = archivo.nombre;
	}
%>
					</td>
				</tr>
				<tr>
					<td class="formas" align="right"><br>
						<table cellpadding="3" cellspacing="1">
							<tr>
								<%--td>
									<iframe height="0" width="0" src="<%=strDirecVirtualTemp+nombreArchivo%>"></iframe>
								</td--%>
								<td class="celda02" width="100" align="center" height="30">
									<a href="<%=strDirecVirtualTemp+nombreArchivo%>">Descargar</a>
								</td>
								<td class="celda02" width="100" align="center">
									<a href="javascript:close();">Cerrar</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
</table>
</div>
</body>
</html>
