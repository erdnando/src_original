<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject, 
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,	
		com.netro.cadenas.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String nombre = (request.getParameter("nombre") != null)?request.getParameter("nombre"):"";
String estado = (request.getParameter("Estado") != null)?request.getParameter("Estado"):"";
String nafinEle = (request.getParameter("numElectronico") != null)?request.getParameter("numElectronico"):"";
//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
//Afiliacion afiliacion = afiliacionHome.create();
Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
String infoRegresar ="";
int start=0,limit=0;
JSONObject jsonObj 	      = new JSONObject();

 if(informacion.equals("catalogoEpo")){
CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
}

else if(informacion.equals("AfiliacionSave")){
		

		String tp          = (request.getParameter("tp")     == null) ? "" : request.getParameter("tp");
		String 	iCveEpo     = (request.getParameter("iCveEpo")            == null) ? ""  : request.getParameter("iCveEpo");
		String cboEPO      = (request.getParameter("cboEPO") == null) ? "" : request.getParameter("cboEPO");
		String TipoPYME    = (request.getParameter("TipoPYME") == null) ? "" : request.getParameter("TipoPYME");
		String Apellido_paterno  = (request.getParameter("Apellido_paterno")  == null) ? "" : request.getParameter("Apellido_paterno");
		String	Apellido_materno  = (request.getParameter("Apellido_materno")  == null) ? "" : request.getParameter("Apellido_materno");
		String 	cboTipoAfiliacion = "1";
		String lsNoNafinElectronico = new String();
		String lsNoNafinElectronicoAux = new String();
		String 	Apellido_paterno_C = (request.getParameter("Apellido_paterno_C") == null) ? "" : request.getParameter("Apellido_paterno_C"),
				Apellido_materno_C = (request.getParameter("Apellido_materno_C") == null) ? "" : request.getParameter("Apellido_materno_C"),
				Nombre_C           = (request.getParameter("Nombre_C") 			 == null) ? "" : request.getParameter("Nombre_C"),
				Telefono_C         = (request.getParameter("Telefono_C") 		 == null) ? "" : request.getParameter("Telefono_C"),
				Fax_C              = (request.getParameter("Fax_C") 			 == null) ? "" : request.getParameter("Fax_C"),
				Email_C            = (request.getParameter("Email_C") 			 == null) ? "" : request.getParameter("Email_C");
		String Num_Sirac   = (request.getParameter("Num_Sirac")          == null) ? ""  : request.getParameter("Num_Sirac");
		String Numero_de_cliente = (request.getParameter("Numero_de_cliente") == null) ? "" : request.getParameter("Numero_de_cliente");
		String 	Razon_Social  = (request.getParameter("Razon_Social") == null) ? "" : request.getParameter("Razon_Social");
		String 	Nombre_comercial = (request.getParameter("Nombre_comercial") == null) ? "" : request.getParameter("Nombre_comercial");
		String chkDescuento = (request.getParameter("chkDescuento") == null) ? "N" : request.getParameter("chkDescuento");
		String R_F_C         = (request.getParameter("R_F_C")    == null) ? "" : request.getParameter("R_F_C");
		String Calle         = (request.getParameter("Calle")    == null) ? "" : request.getParameter("Calle");
		String Colonia       = (request.getParameter("Colonia")  == null) ? "" : request.getParameter("Colonia");
		String Estado        = (request.getParameter("Estado")   == null) ? "" : request.getParameter("Estado");
		String Delegacion_o_municipio = (request.getParameter("Delegacion_o_municipio") == null) ? "" : request.getParameter("Delegacion_o_municipio");
		String Codigo_postal = (request.getParameter("Codigo_postal") == null) ? "" : request.getParameter("Codigo_postal");
		String Pais          = (request.getParameter("Pais")     == null) ? "" : request.getParameter("Pais");
		String Telefono      = (request.getParameter("Telefono") == null) ? "" : request.getParameter("Telefono");
		String Email         = (request.getParameter("Email")    == null) ? "" : request.getParameter("Email");
		String Fax           = (request.getParameter("Fax")      == null) ? "" : request.getParameter("Fax");
		String Nombre      = (request.getParameter("Nombre")  == null) ? "" : request.getParameter("Nombre");
		String Numero_Escritura	= (request.getParameter("Numero_Escritura") == null) ? "" : request.getParameter("Numero_Escritura");
		String Fecha_Constitucion	= (request.getParameter("Fecha_Constitucion") == null) ? "" : request.getParameter("Fecha_Constitucion");
		String genera_cuenta = (request.getParameter("genera_cuenta") == null)?"":request.getParameter("genera_cuenta");		
		String cuenta_usuario_asignada = (request.getParameter("cuenta_usuario_asignada") == null)?"":request.getParameter("cuenta_usuario_asignada");
		String numeroCelular = (request.getParameter("numeroCelular") == null?"":request.getParameter("numeroCelular"));
		String chkOperaFideicomiso = (request.getParameter("chkOperaFideicomiso") == null?"":request.getParameter("chkOperaFideicomiso"));
		
		String cesion_derechos = (request.getParameter("derechos") == null?"":request.getParameter("derechos"));
		String fianza_electronica = (request.getParameter("fianza") == null?"":request.getParameter("fianza"));
		String Sin_Num_Prov = (request.getParameter("sinProv") == null?"":request.getParameter("sinProv"));
		String chkEntidadGobierno = (request.getParameter("chkEntidadGobierno") == null?"":request.getParameter("chkEntidadGobierno"));
		String chkFactDistribuido = (request.getParameter("chkFactDistribuido") == null?"":request.getParameter("chkFactDistribuido"));
		
		String chkProvExtranjero = (request.getParameter("chkProvExtranjero") == null?"N":request.getParameter("chkProvExtranjero"));
		String chkOperaDescAutoEPO = (request.getParameter("chkOperaDescAutoEPO") == null?"N":request.getParameter("chkOperaDescAutoEPO"));
                 
                
		//Proveedor Fisica o Moral
		if ("F".equals(tp) || "M".equals(tp)){
		
			if(strTipoUsuario.equals("NAFIN")) {
				iCveEpo = cboEPO;
			} else if(strTipoUsuario.equals("EPO")) {
				iCveEpo = iNoEPO;
			}

			try	{
                       
				lsNoNafinElectronico = 
						afiliacion.afiliaPyme(iNoUsuario, cboTipoAfiliacion, 
								iCveEpo, tp, Num_Sirac, Numero_de_cliente, Nombre_comercial,
								Razon_Social, chkDescuento.equals("on")?"S":"N", R_F_C, Calle,
								Colonia, Estado,  Delegacion_o_municipio,
								Codigo_postal, Pais, Telefono, Email,
								Fax, Apellido_paterno, Apellido_materno,
								Nombre, Apellido_paterno_C, Apellido_materno_C,
								Nombre_C, Telefono_C, Fax_C, Email_C,
								Numero_Escritura, 
								Fecha_Constitucion, genera_cuenta.equals("on")?"S":"N", Sin_Num_Prov.equals("on")?"S":"N", 
                                                                cuenta_usuario_asignada,cesion_derechos.equals("on")?"S":"N",
                                                                fianza_electronica.equals("on")?"S":"N",
                                                                chkOperaFideicomiso.equals("on")?"S":"N",
                                                                chkEntidadGobierno.equals("on")?"S":"N",
                                                                chkFactDistribuido.equals("on")?"S":"N", 
								chkProvExtranjero.equals("on")?"S":"N", 
                                                                chkOperaDescAutoEPO.equals("on")?"S":"N"   );    
				
//****************************************************************************** FODEA 032 - 2008 PROMOCION A PYMES	- ACF
				//BeanAfiliacion.actualizaCelular(lsNoNafinElectronico, numeroCelular);
				//BeanAfiliacion.establecePromocionPyme(lsNoNafinElectronico, strLogin, strNombreUsuario, promocionCorreo, promocionCelular);
//****************************************************************************** FODEA 048 - 2008 FACTORAJE MOVIL	- ACF
			System.out.println(" lsNoNafinElectronico "+lsNoNafinElectronico);
			VectorTokenizer vt=null; Vector vecdat=null;
			vt = new VectorTokenizer(lsNoNafinElectronico,":");
			vecdat = vt.getValuesVector();
			boolean ban = true;
			if(vecdat.size()==2){
				lsNoNafinElectronico =(String)vecdat.elementAt(1); 
				lsNoNafinElectronicoAux  = (String)vecdat.elementAt(0);
				ban = false;
			}
			if(ban==true){// solo 
				afiliacion.agregaPymeFactorajeMovil(lsNoNafinElectronico, strLogin, strNombreUsuario, numeroCelular);
			}
			if(ban==false){
				lsNoNafinElectronico = lsNoNafinElectronicoAux;
			}
//****************************************************************************** FODEA 048 - 2008 FACTORAJE MOVIL	- ACF
				//out.print("<script language=\"JavaScript\">alert(\"Su número de Nafin Electrónico es: "+lsNoNafinElectronico+" \");");
				//out.print("<script language=\"JavaScript\">alert(\" "+lsNoNafinElectronico+" \");");
//****************************************************************************** FODEA 032 - 2008 PROMOCION A PYMES	- ACF
				//out.print(("NAFIN".equals(strTipoUsuario))?"location.href=\"15forma0.jsp\";</script>":"location.href=\"15forma1a.jsp\";</script>");
				jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("lsNoNafinElectronico",lsNoNafinElectronico);
				infoRegresar = jsonObj.toString();
			}
			catch(Exception e){
				//System.out.println(e);
				throw new AppException("Error verificar que los datos sean correctos ",e);
				//out.print("<script language=\"JavaScript\">alert(\"" + lexError.getMsgError() + "\"); history.back(-1);</script>");
			}
		}
		
}else if(informacion.equals("catalogoPais")) {		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionIn("24", Integer.class);
		infoRegresar = cat.getJSONElementos();	
	} else if(informacion.equals("catalogoEstado"))	{
		String pais = (request.getParameter("pais") != null)?request.getParameter("pais"):"";
		CatalogoEstado cat = new CatalogoEstado();
		//cat.setTabla("comcat_version_convenio");
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre");
		cat.setClavePais(pais);
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
 } else if(informacion.equals("catalogoMunicipio"))	{
		String pais = (request.getParameter("pais") != null)?request.getParameter("pais"):"";
		String estados = (request.getParameter("estado") != null)?request.getParameter("estado"):"";
		CatalogoMunicipio cat = new CatalogoMunicipio();
		cat.setPais(pais);
		cat.setClave("IC_MUNICIPIO||'|'||CD_NOMBRE");
		cat.setDescripcion("upper(CD_NOMBRE)");
		cat.setEstado(estados);
		List elementos = cat.getListaElementos();
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar =  "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

		 
 }


%>
<%=infoRegresar%>