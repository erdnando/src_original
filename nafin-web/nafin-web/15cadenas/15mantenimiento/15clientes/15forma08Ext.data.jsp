<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.ResultSet,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		com.netro.seguridadbean.*, 
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	

 if(informacion.equals("catalogoBanco"))	{
		//String banco = (request.getParameter("cmb_Banco")==null)?"":request.getParameter("cmb_Banco");
	
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_banco_fondeo");
		cat.setCampoClave("ic_banco_fondeo");
		cat.setCampoDescripcion("cd_descripcion");
		//cat.setCondicionIn(banco, "String");
		cat.setOrden("ic_banco_fondeo");
		infoRegresar = cat.getJSONElementos();		
 } 
 else if(informacion.equals("catalogoPais")) {		
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("ic_pais");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setValoresCondicionNotIn("52", Integer.class);
		infoRegresar = cat.getJSONElementos();	
	}
 else if(informacion.equals("catalogoEstado")){
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setClavePais(Pais);//Mexico clave 24
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();	
 }
 else if(informacion.equals("catalogoMunicipio"))	{
		
		String Edo= (request.getParameter("estado")==null)?"":request.getParameter("estado");
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoMunicipio cat=new CatalogoMunicipio();
		cat.setClave("ic_municipio");
		cat.setDescripcion("cd_nombre");
		cat.setPais(Pais);
		cat.setEstado(Edo);
		cat.setOrden("cd_nombre");
		List elementos=cat.getListaElementos();
		
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
 }
 
 else if(informacion.equals("catalogoRamoSIAFF"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_siaff_ramos");
		cat.setCampoClave("cc_ramo_siaff");
		cat.setCampoDescripcion("cc_ramo_siaff || '- ' || cg_descripcion");
		cat.setOrden("cc_ramo_siaff");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("catalogoUnidadSIAFF"))	{
		String ramo= (request.getParameter("ramo")==null)?"":request.getParameter("ramo");
	
		CatalogoUnidadSIAFF cat = new CatalogoUnidadSIAFF();
		cat.setClave("cc_unidad_siaff");
		cat.setDescripcion("cc_unidad_siaff || ' - ' || cg_descripcion");		
		cat.setSeleccion(ramo);
		cat.setOrden("cc_unidad_siaff");
		infoRegresar = cat.getJSONElementos();		
 }
 ////////////////FODEA2012//////////////////
 else if(informacion.equals("catalogoSectorECO"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_sector_econ_epo");
		cat.setCampoClave("ic_sector_econ_epo");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("catalogoSubSectorECO"))	{
		String sectorECO= (request.getParameter("sectorECO")==null)?"":request.getParameter("sectorECO");
	
		CatalogoSubsectorEconomico cat = new CatalogoSubsectorEconomico();
		cat.setClave("ic_subsector_epo");
		cat.setDescripcion("cg_descripcion");
		cat.setSeleccion(sectorECO);
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 
 else if(informacion.equals("catalogoRamaECO"))	{
		String sectorECO= (request.getParameter("sectorECO")==null)?"":request.getParameter("sectorECO");
		String subsectorECO= (request.getParameter("subsectorECO")==null)?"":request.getParameter("subsectorECO");
	
		CatalogoRamaEconomica cat = new CatalogoRamaEconomica();
		cat.setClave("ic_rama_econ_epo");
		cat.setDescripcion("cg_descripcion");
		cat.setSectorEcon(sectorECO);
		cat.setSubSectorEcon(subsectorECO);
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 ////////////////FODEA2012//////////////////
 
  else if(informacion.equals("catalogoSectorEPO"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_sector_epo");
		cat.setCampoClave("ic_sector_epo");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 else if(informacion.equals("catalogoSubdireccion"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_subdireccion");
		cat.setCampoClave("ic_subdireccion");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }
 
 else if(informacion.equals("catalogoPromotor"))	{
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_lider_promotor");
		cat.setCampoClave("ic_lider_promotor");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setOrden("cg_descripcion");
		infoRegresar = cat.getJSONElementos();		
 }

 else if(informacion.equals("ConsultaEPO"))	{
	try	
		{
			
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			String   ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
			
			Registros reg ;		
			reg = BeanAfiliacion.getDatosEPO(ic_epo);
			//reg = BeanAfiliacion.getDatosEPO("100");
			
			HashMap resultado = new HashMap();
			ArrayList lista_res = new ArrayList();
			/*Barro  la informacion del Query */
			while(reg.next()) {
				String Razon_Social      			= (reg.getString(2) == null) ? "":reg.getString(2).trim();   // out.println("<b>Razon_Social</b>: "+Razon_Social+"<br>");
				String chkFinanciamiento 		   = (reg.getString("chkFinan") == null) ? "N" : reg.getString("chkFinan").trim();	 
				String Nombre_comercial  			= (reg.getString(3) == null) ? "" : reg.getString(3).trim();
				String R_F_C 			  				= (reg.getString(4) == null) ? "" : reg.getString(4).trim();
				String Calle 			  				= (reg.getString(5) == null) ? "" : reg.getString(5).trim();   
				String Numero_Exterior   			= (reg.getString(6) == null) ? "" : reg.getString(6).trim();
				String Numero_interior   			= (reg.getString(7) == null) ? "" : reg.getString(7).trim();
				String Colonia 		  				= (reg.getString(8) == null) ? "" : reg.getString(8).trim();
				String Delegacion_o_municipio 	= (reg.getString(9) == null) ? "" : reg.getString(9).trim()+"|"+reg.getString(10).trim();
				String Codigo_postal 	  			= (reg.getString(11) == null) ? "": reg.getString(11).trim();
				String Estado 			  				= (reg.getString(12) == null) ? "": reg.getString(12).trim();
				String Pais 			  				= (reg.getString(13) == null) ? "": reg.getString(13).trim();
				String Telefono 		  				= (reg.getString(14) == null) ? "": reg.getString(14).trim();
				String Email 			  				= (reg.getString(15) == null) ? "": reg.getString(15).trim();
				String Fax 	 		  					= (reg.getString(16) == null) ? "": reg.getString(16).trim();
				String Fecha_del_poder_notarial 	= (reg.getString(17) == null) ? "":reg.getString(17).trim();
				String Numero_de_escritura 		= (reg.getString(18) == null) ? "" : reg.getString(18).trim();
				String Apellido_paterno_L 			= (reg.getString(19) == null) ? "" : reg.getString(19).trim();
				String Apellido_materno_L 			= (reg.getString(20) == null) ? "" : reg.getString(20).trim();
				String Nombre_L 		  				= (reg.getString(21) == null) ? "" : reg.getString(21).trim();
				String ic_domicilio 	  				= (reg.getString(28) == null) ? "" : reg.getString(28).trim();
				String ic_contacto 	  				= (reg.getString(29) == null) ? "" : reg.getString(29).trim();
				String c_proveedores 	  			= (reg.getString(30) == null) ? "" :reg.getString(30).trim();
				String cliente_primer_piso 		= (reg.getString(31) == null) ? "" : reg.getString(31).trim();
				String no_sirac 		 				= (reg.getString(32) == null) ? "" : reg.getString(32).trim();
				String cg_banco_retiro 				= (reg.getString("cg_banco_retiro")  == null) ? "" : reg.getString("cg_banco_retiro").trim();
				String cg_cta_retiro 				= (reg.getString("cg_cta_retiro")    == null) ? "" : reg.getString("cg_cta_retiro").trim();
				String SSId_Number 					= (reg.getString(36) == null) ? "" : reg.getString(36).trim();
				String Dab_Number 		 			= (reg.getString(37) == null) ? "" : reg.getString(37).trim();
				String noBancoFondeo					= "1";
				//MODIFICACION FODEA 049-2008
				//numeroSIAFF				= (rsTraeEPO.getString(39)    == null) ? "" : rsTraeEPO.getString(39).trim();//FODEA 034 - 2009 ACF
				String ramo_siaff						= (reg.getString("cc_ramo_siaff") == null) ? "" : reg.getString("cc_ramo_siaff").trim();//FODEA 034 - 2009 ACF
				String unidad_siaff					= (reg.getString("cc_unidad_siaff") == null) ? "" : reg.getString("cc_unidad_siaff").trim();//FODEA 034 - 2009 ACF
				String convenio_unico				= (reg.getString("cs_convenio_unico")    == null) ? "" : reg.getString("cs_convenio_unico").trim();//FODEA 040 - 2009 FVR
//				chkNotasCred	 = (rsTraeEPO.getString("chkNotasCred")    == null) ? "" : rsTraeEPO.getString("chkNotasCred").trim();
				
				//FODEA 041-2009
				String mandato_documento			= (reg.getString("CS_MANDATO_DOCUMENTO")    == null) ? "" : reg.getString("CS_MANDATO_DOCUMENTO").trim();
				//Fodea 2012
				String Sector_Econ					= (reg.getString("IC_SECTOR_ECON_EPO")    == null) ? "" : reg.getString("IC_SECTOR_ECON_EPO").trim();
				String Subsector_econ				= (reg.getString("IC_SUBSECTOR_EPO")  == null) ? "" : reg.getString("IC_SUBSECTOR_EPO").trim();
				String rama_eco						= (reg.getString("IC_RAMA_ECON_EPO")== null) ? "" : reg.getString("IC_RAMA_ECON_EPO").trim();
				
				String Sector_Epo						= (reg.getString("IC_SECTOR_EPO")    == null) ? "" : reg.getString("IC_SECTOR_EPO").trim();
				String Subdireccion					= (reg.getString("IC_SUBDIRECCION")  == null) ? "" : reg.getString("IC_SUBDIRECCION").trim();
				String Lider_Promotor				= (reg.getString("IC_LIDER_PROMOTOR")== null) ? "" : reg.getString("IC_LIDER_PROMOTOR").trim();
				 
			StringTokenizer vTokFelMun = new StringTokenizer(Delegacion_o_municipio,"|");
			String iDelMun = "", strDelMun = "";
			while(vTokFelMun.hasMoreTokens()){
				iDelMun = vTokFelMun.nextToken();
				strDelMun = vTokFelMun.nextToken();
			}	 
			
			resultado.put("ic_domicilio",ic_domicilio);
			resultado.put("ic_contacto",ic_contacto);
			resultado.put("Razon_Social",Razon_Social);
			resultado.put("chkFinanciamiento",chkFinanciamiento);
			resultado.put("Nombre_comercial",Nombre_comercial);
			resultado.put("R_F_C",R_F_C);
			resultado.put("Calle",Calle);
			resultado.put("Numero_Exterior",Numero_Exterior);
			resultado.put("Numero_interior",Numero_interior);
			resultado.put("Colonia",Colonia);
			resultado.put("Delegacion_o_municipio",iDelMun);
			resultado.put("Codigo_postal",Codigo_postal);
			resultado.put("Estado",Estado);
			resultado.put("Pais",Pais);
			resultado.put("Telefono",Telefono);
			resultado.put("Email",Email);
			resultado.put("Fax",Fax);
			resultado.put("Fecha_del_poder_notarial",Fecha_del_poder_notarial);
			resultado.put("Numero_de_escritura",Numero_de_escritura);
			resultado.put("Apellido_paterno_L",Apellido_paterno_L);
			resultado.put("Apellido_materno_L",Apellido_materno_L);
			resultado.put("Nombre_L",Nombre_L);
			resultado.put("ic_domicilio",ic_domicilio);
			resultado.put("ic_contacto",ic_contacto);
			resultado.put("c_proveedores",c_proveedores);
			resultado.put("cliente_primer_piso",cliente_primer_piso);
			resultado.put("no_sirac",no_sirac);
			resultado.put("cg_banco_retiro",cg_banco_retiro);
			resultado.put("cg_cta_retiro",cg_cta_retiro);
			resultado.put("SSId_Number",SSId_Number);
			resultado.put("Dab_Number",Dab_Number);
			resultado.put("noBancoFondeo",noBancoFondeo);
			resultado.put("ramo_siaff",ramo_siaff);
			resultado.put("unidad_siaff",unidad_siaff);
			resultado.put("convenio_unico",convenio_unico);
			resultado.put("mandato_documento",mandato_documento);
			resultado.put("Sector_Econ",Sector_Econ);
			resultado.put("Subsector_econ",Subsector_econ);
			resultado.put("rama_eco",rama_eco);
			resultado.put("Sector_Epo",Sector_Epo);
			resultado.put("Subdireccion",Subdireccion);
			resultado.put("Lider_Promotor",Lider_Promotor);

			}
			
			String Total = BeanAfiliacion.getTotalSolicitudes(ic_epo, "ic_epo");
			resultado.put("Total",Total);
			
			/*Recupero la informacion en un hashmap para enviarla del lado del js */
			
			lista_res.add(resultado);
			JSONArray jsObjArray = JSONArray.fromObject(lista_res);
			infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
				
		} catch(Exception e) { //catch(NafinException lexError){
				//out.print("<script language=\"JavaScript\">alert(\"" + lexError.getMsgError() + "\"); history.back(-1);</script>");
				throw new AppException("Error en la recuperación de datos", e);
			}
 }//fin ConsultaEpo
 else if(informacion.equals("ModificaEPO"))	{
	try	
		{
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			String   ic_epo 						 = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
			String   ic_domicilio				 = (request.getParameter("ic_domicilio") == null) ? "" : request.getParameter("ic_domicilio");
			String   ic_contacto					 = (request.getParameter("ic_contacto") == null) ? "" : request.getParameter("ic_contacto");
			String 	Razon_Social  				 = (request.getParameter("Razon_Social") == null) ? "" : request.getParameter("Razon_Social"),
						Nombre_comercial 			 = (request.getParameter("nombre_comercial") == null) ? "" : request.getParameter("nombre_comercial"),
						R_F_C         				 = (request.getParameter("rfc")    == null) ? "" : request.getParameter("rfc"),
						noBancoFondeo     			 = "1",
						Numero_de_escritura      = (request.getParameter("num_escritura") == null) ? "" : request.getParameter("num_escritura"),
						Fecha_del_poder_notarial= (request.getParameter("fch_poder") == null) ? "" : request.getParameter("fch_poder");
			//Datos del Representate Legal
			String 	Apellido_paterno_L       = (request.getParameter("paterno")       == null) ? "" : request.getParameter("paterno"),
						Apellido_materno_L       = (request.getParameter("materno")       == null) ? "" : request.getParameter("materno"),
						Nombre_L                 = (request.getParameter("nombre")        == null) ? "" : request.getParameter("nombre"),
						c_proveedores   			 = (request.getParameter("c_proveedores")   == null) ? "" : request.getParameter("c_proveedores"),
						cliente_primer_piso 		 = (request.getParameter("cliente_primer_piso") == null) ? "" : request.getParameter("cliente_primer_piso"),
						no_sirac        			 = (request.getParameter("num_sirac")  == null) ? "" : request.getParameter("num_sirac");
			//Datos del Contacto
			String	Apellido_paterno_C 		 = (request.getParameter("paterno_contacto") == null) ? "" : request.getParameter("paterno_contacto"),
						Apellido_materno_C 		 = (request.getParameter("materno_contacto") == null) ? "" : request.getParameter("materno_contacto"),
						Nombre_C           		 = (request.getParameter("nombre_contacto") 			 == null) ? "" : request.getParameter("nombre_contacto"),
						Telefono_C         		 = (request.getParameter("telc") 		 == null) ? "" : request.getParameter("telc"),
						Fax_C              		 = (request.getParameter("faxc") 			 == null) ? "" : request.getParameter("faxc"),
						Email_C            		 = (request.getParameter("emailc") 			 == null) ? "" : request.getParameter("emailc");			
			//Domicilio							
			String   Calle        				 = (request.getParameter("calle")    == null) ? "" : request.getParameter("calle"),
						Numero_Exterior 			 = (request.getParameter("num_ext") == null) ? "" : request.getParameter("num_ext"),
						Numero_interior 			 = (request.getParameter("num_int") == null) ? "" : request.getParameter("num_int"),
						Colonia       				 = (request.getParameter("colonia")  == null) ? "" : request.getParameter("colonia"),
						Pais          				 = (request.getParameter("cmb_pais")     == null) ? "" : request.getParameter("cmb_pais"),
						Estado        				 = (request.getParameter("cmb_estado")   == null) ? "" : request.getParameter("cmb_estado"),
						Delegacion_o_municipio 	 = (request.getParameter("cmb_delegacion") == null) ? "" : request.getParameter("cmb_delegacion"),
						Codigo_postal 				 = (request.getParameter("code") == null) ? "" : request.getParameter("code"),
						Telefono      				 = (request.getParameter("telefono") == null) ? "" : request.getParameter("telefono"),
						Email         				 = (request.getParameter("email")    == null) ? "" : request.getParameter("email"),
						Fax           				 = (request.getParameter("fax")      == null) ? "" : request.getParameter("fax");
						
			String 	cg_banco_retiro 			 = (request.getParameter("bco_retiro") == null) ? "" : request.getParameter("bco_retiro"),
						cg_cta_retiro 				 = (request.getParameter("cuenta")    == null) ? "" : request.getParameter("cuenta").trim(),
						internacional 				 = (request.getParameter("internacional") == null) ? "" : request.getParameter("internacional"),
						SSId_Number 				 = (request.getParameter("SSId_Number") == null) ? "" : request.getParameter("SSId_Number"),
						Dab_Number 					 = (request.getParameter("Dab_Number") == null) ? "" : request.getParameter("Dab_Number"),
						ramo_siaff 					 = (request.getParameter("cmb_ramo") == null) ? "" : request.getParameter("cmb_ramo"),//FODEA 034 - 2009 ACF
						unidad_siaff 			    = (request.getParameter("cmb_unidad") == null) ? "" : request.getParameter("cmb_unidad"),//FODEA 034 - 2009 ACF					
						msChkFinan  				 = (request.getParameter("ditribuidores")  == null) ? "N" : request.getParameter("ditribuidores"),
						convenio_unico 			 = (request.getParameter("convenio") == null) ? "" : request.getParameter("convenio"),
						mandato_documento 		 = (request.getParameter("irrevocable")==null)?"":request.getParameter("irrevocable");//FODEA 041 - 2009						
						
			/**Fodea2012**/
			String 	cmb_sector_eco   			 = (request.getParameter("cmb_economico")   == null) ? "" : request.getParameter("cmb_economico"),
						cmb_subsector_eco			 = (request.getParameter("cmb_subsectoreco") == null) ? "" : request.getParameter("cmb_subsectoreco"),
						cmb_rama_eco			    = (request.getParameter("cmb_ramaECO") == null) ? "" : request.getParameter("cmb_ramaECO");
						
			String	cmb_sector_epo 			 = (request.getParameter("cmb_sector")== null) ? "" : request.getParameter("cmb_sector"),//FODEA 057-2010 FVR
						cmb_subdireccion 			 = (request.getParameter("cmb_subdirec")== null) ? "" : request.getParameter("cmb_subdirec"),//FODEA 057-2010 FVR
						cmb_lider_promotor 		 = (request.getParameter("cmb_promotor")== null) ? "" : request.getParameter("cmb_promotor");//FODEA 057-2010 FVR
				
			///////////que bit con esto??
			//String Regresa_pag=Regresa_A;
			//String redirecciona = "";
			
			String msg = "";
			boolean okPymeEpoIF  = true,
					okDom        = true,
					okCont       = true,
					okUsuario    = true,
					okRelPymeEpo = true,
					okNafinElect = true;
					
			/*Validaciones antes de enviar*/
			if(!msChkFinan.equals(""))
		{
			if ( msChkFinan.equals("on") )
				msChkFinan = "S";
			else
				msChkFinan = "N";
		}
		
		if ( convenio_unico.equals("on") ){
			convenio_unico = "S";
		}	else{
			convenio_unico = "N";
		}
			if(!mandato_documento.equals(""))
		{
			if ( mandato_documento.equals("on") )
				mandato_documento = "S";
			else
				mandato_documento = "N";
		}
			if (internacional.equals("")){
				internacional = "N";
			}
			
			if( !Delegacion_o_municipio.equals("") && Delegacion_o_municipio != null)
			{
				CatalogoMunicipio cat=new CatalogoMunicipio();
				cat.setClave("ic_municipio");
				cat.setDescripcion("cd_nombre");
				cat.setPais(Pais);
				cat.setEstado(Estado);
				cat.setSeleccion(Delegacion_o_municipio);
				cat.setOrden("cd_nombre");
				List elementos=cat.getListaElementos();
			
				Iterator it = elementos.iterator();
			while(it.hasNext()) {
				ElementoCatalogo obj = (ElementoCatalogo)it.next();
				String iDelMun = obj.getClave();				String strDelMun = obj.getDescripcion();
				Delegacion_o_municipio = iDelMun + "|" + strDelMun;
			}
				System.out.println("Delegacion_o_municipio " +  Delegacion_o_municipio ) ;
			}
			
			String sDatosEpo = "ic_contacto-" + ic_contacto +"|" +"ic_epo-" + ic_epo +"|"+ "ic_domicilio-" + ic_domicilio +"|"+"Razon_Social-" + 
							Razon_Social+"|"+"msChkFinan-" + msChkFinan+"|"+"Nombre_comercial-" + Nombre_comercial+"|"+"R_F_C-" + R_F_C+"|"+ 
							"noBancoFondeo-"+	noBancoFondeo +"|" +"Numero_de_escritura-" + Numero_de_escritura+"|"+"Fecha_del_poder_notarial-" + Fecha_del_poder_notarial+"|"+
							"Apellido_paterno_L-" + Apellido_paterno_L+"|"+ "Apellido_materno_L-" + Apellido_materno_L+"|"+ "Nombre_L-" + Nombre_L+"|"+
							"c_proveedores-" + c_proveedores+"|"+ "cliente_primer_piso-" + cliente_primer_piso+"|"+ "no_sirac-" + no_sirac+"|"+
							"Apellido_paterno_C-" + Apellido_paterno_C+"|"+"Apellido_materno_C-" + Apellido_materno_C+"|"+"Nombre_C-" + Nombre_C+"|"+
							"Telefono_C-" + Telefono_C+"|"+"Fax_C-" + Fax_C+"|"+"Email_C-" + Email_C+"|"+
							"Calle-" + Calle+"|"+"Numero_Exterior-" + Numero_Exterior+"|"+"Numero_interior-" + Numero_interior+"|"+
							"Colonia-" + Colonia+"|"+"Pais-" + Pais+"|"+"Estado-" + Estado+"|"+"Delegacion_o_municipio-" + Delegacion_o_municipio+"|"+
							"Codigo_postal-" + Codigo_postal+"|"+"Telefono-" + Telefono+"|"+ "Email-" + Email+"|"+"Fax-" + Fax+"|"+
							"cg_banco_retiro-" + cg_banco_retiro+"|"+"cg_cta_retiro-" + cg_cta_retiro+"|"+"internacional-" + internacional+"|"+"SSId_Number-" + SSId_Number+"|"+"Dab_Number-" + Dab_Number+"|"+
							"ramo_siaff-" + ramo_siaff+"|"+"unidad_siaff-" + unidad_siaff+"|"+"convenio_unico" + convenio_unico+"|"+"mandato_documento" + mandato_documento+"|"+
							"cmb_sector_epo-" + cmb_sector_epo+"|"+"cmb_subdireccion-" + cmb_subdireccion+"|"+"cmb_lider_promotor-" + cmb_lider_promotor+"|"+
							"cmb_sector_eco-" + cmb_sector_eco+"|"+"cmb_subsector_eco-" + cmb_subsector_eco+"|"+"cmb_rama_eco-" + cmb_rama_eco+"\n";
				
			BeanAfiliacion.actualizaEpo(iNoUsuario,ic_epo,  Razon_Social, Nombre_comercial, R_F_C,  Numero_de_escritura, Fecha_del_poder_notarial,
					Apellido_paterno_L, Apellido_materno_L, Nombre_L, c_proveedores, /*ic_contacto, Apellido_paterno_C, Apellido_materno_C, 
					Nombre_C, Telefono_C, Fax_C, Email_C, */ic_domicilio, Calle, Numero_Exterior, Numero_interior, Colonia, Estado, Pais, 
					Delegacion_o_municipio, Codigo_postal, Telefono, Email,Fax,cliente_primer_piso,no_sirac, msChkFinan, 
					cg_banco_retiro, cg_cta_retiro, SSId_Number, Dab_Number, noBancoFondeo, ramo_siaff, unidad_siaff, convenio_unico, mandato_documento,
					cmb_sector_epo,cmb_subdireccion,cmb_lider_promotor,cmb_sector_eco,cmb_subsector_eco,cmb_rama_eco);
			
			msg = "Los datos han sido actualizados";
				
			JSONObject resultado = new JSONObject();
			resultado.put("success", new Boolean(true));
			resultado.put("msg", msg);
			infoRegresar = resultado.toString();	
		
		} catch(Exception e) { //catch(NafinException lexError){
				//out.print("<script language=\"JavaScript\">alert(\"" + lexError.getMsgError() + "\"); history.back(-1);</script>");
				throw new AppException("Error en la afiliacion", e);
			}



}else if (informacion.equals("validaPermiso")) {
	
	JSONObject jsonObj = new JSONObject();
	
	//** Fodea 021-2015 (E);
	com.netro.seguridadbean.Seguridad seguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			
	String idMenuP   = request.getParameter("idMenuP")==null?"15FORMA5":request.getParameter("idMenuP");
	String ccPermiso = (request.getParameter("ccPermiso")!=null)?request.getParameter("ccPermiso"):"";
	String  [] permisosSolicitados = {"BTNCANCELAR_CM", "BTNACEPTAR_CM"}; 
	List permisos =  new ArrayList();
	
	if(ccPermiso.equals("")) {
		permisos =  seguridadBean.getPermisosPorMenu( iTipoPerfil, strPerfil, idMenuP, permisosSolicitados ); 
	}else   if(!ccPermiso.equals("")) {
		seguridadBean.validaPermiso(  iTipoPerfil,  strPerfil,  idMenuP,  ccPermiso,  strLogin );  
	}
		//** Fodea 021-2015 (S);  
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("permisos",  permisos);   
	infoRegresar = jsonObj.toString();	
	

 }


//System.out.println("infoRegresar============"+infoRegresar); 


%>
<%=infoRegresar%>



