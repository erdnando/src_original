<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.afiliacion.*,
		java.io.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoPantallaBitacora,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
//Afiliacion afiliacion = afiliacionHome.create();
Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if (informacion.equals("valoresIniciales"))	{

	String cmb_grupo  		= (request.getParameter("cmb_grupo")==null)?"":request.getParameter("cmb_grupo");
	String noBancoFondeo		= (request.getParameter("noBancoFondeo")==null)?"":request.getParameter("noBancoFondeo");

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("strTipoUsuario", strTipoUsuario);
	jsonObj.put("strPerfil", strPerfil);
	jsonObj.put("iNoCliente", iNoCliente);
	jsonObj.put("cmb_grupo", cmb_grupo);
	jsonObj.put("noBancoFondeo", noBancoFondeo);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoEPO")){

	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	//catalogo.setOrden("ic_epo");
	infoRegresar = catalogo.getJSONElementos();

}else if (informacion.equals("CatalogoPantalla")){

	CatalogoPantallaBitacora catalogo = new CatalogoPantallaBitacora();
	catalogo.setCampoClave("cc_pantalla_bitacora");
	catalogo.setCampoDescripcion("cg_descripcion");
	catalogo.setClaveAfiliacion("P");
	catalogo.setValoresCondicionIn("REGAFILMAS,REGAFIL,CONSAFIL",String.class);
	catalogo.setOrden("cg_descripcion");
	infoRegresar = catalogo.getJSONElementos();

}else if (informacion.equals("ConsultaCarga") ){

	String fecini = request.getParameter("fecini")==null?Fecha.getFechaActual():request.getParameter("fecini");
	String fecfin = request.getParameter("fecfin")==null?Fecha.getFechaActual():request.getParameter("fecfin");
	String cveEpo = request.getParameter("cveEpo")==null?"":request.getParameter("cveEpo");
	
	List lstConsultaBitCarga = new ArrayList();
	List reg = new ArrayList();
	
	JSONObject jsonObj = new JSONObject();

	if("EPO".equals(strTipoUsuario)){
		cveEpo = iNoCliente;
	}

	///**Linea para realizar pruebas**/lstConsultaBitCarga = afiliacion.getBitacoraCargaMasiva("225", "01/09/2011", "22/11/2011");
	lstConsultaBitCarga = afiliacion.getBitacoraCargaMasiva(cveEpo, fecini, fecfin);
	if(lstConsultaBitCarga!=null && lstConsultaBitCarga.size()>0){
		for(int x=0; x<lstConsultaBitCarga.size();x++){
			HashMap	datos = new HashMap();
			List lstRegConsulta = (List)lstConsultaBitCarga.get(x);

			datos.put("IC_BIT_CARGA",(String)lstRegConsulta.get(0));
			datos.put("IC_EPO",(String)lstRegConsulta.get(1));
			datos.put("CG_NOMBRE_ARCH",(String)lstRegConsulta.get(2));
			datos.put("IG_TOTAL_REG",(String)lstRegConsulta.get(3));
			datos.put("IG_REG_PROC",(String)lstRegConsulta.get(4));
			datos.put("IG_REG_ERROR",(String)lstRegConsulta.get(5));
			datos.put("DF_CARGA",(String)lstRegConsulta.get(6));
			datos.put("IC_USUARIO",(String)lstRegConsulta.get(7));
			datos.put("CG_NOMBRE_USUARIO",(String)lstRegConsulta.get(8));
			datos.put("NOMBRE_EPO",(String)lstRegConsulta.get(9));
			reg.add(datos);
		}
		jsonObj.put("registros", reg);
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenDetalles")) {

	String icBitacora = request.getParameter("icBitacora")==null?"":request.getParameter("icBitacora");
	String tipoRegistro = request.getParameter("tipoRegistro")==null?"":request.getParameter("tipoRegistro");		
	String rutaNombreArchivo ="";
	CreaArchivo archivo = new CreaArchivo();
	StringBuffer 	contenidoArchivo 	= new StringBuffer();
	String nombreArchivo = null;
	String	linea = "";
	BufferedReader	brt =  null;
	JSONObject jsonObj = new JSONObject();
	
	rutaNombreArchivo = afiliacion.getDetalleBitCargaPymeMasiva(icBitacora,strDirectorioTemp,tipoRegistro);
	java.io.File ft = new java.io.File(rutaNombreArchivo);
	brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
	
	while((linea=brt.readLine())!=null){
		System.out.println("linea::"+linea);
		VectorTokenizer vtd		= new VectorTokenizer(linea,"|");
		Vector vecdet	= vtd.getValuesVector();
		System.out.println("(String)vecdet.get(vecdet.size()-1)::::::"+(String)vecdet.get(vecdet.size()-1));
		if("T".equals(tipoRegistro) )
			contenidoArchivo.append(linea+"\n");
		if("C".equals(tipoRegistro) && ((String)vecdet.get(vecdet.size()-1)==null || "".equals((String)vecdet.get(vecdet.size()-1))) )
			contenidoArchivo.append(linea+"\n");
		if("E".equals(tipoRegistro) && (String)vecdet.get(vecdet.size()-1)!=null && !"".equals((String)vecdet.get(vecdet.size()-1)))
			contenidoArchivo.append(linea+"\n");
	}
	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt")){
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivo = archivo.nombre;
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ConsultaBitacora") || informacion.equals("ArchivoCSVBit") || informacion.equals("ArchivoPDFBit") ) {

	String txt_ne  			= (request.getParameter("txt_ne")==null)?"":request.getParameter("txt_ne");
	String cmb_pantalla  	= (request.getParameter("cmb_pantalla")==null)?"":request.getParameter("cmb_pantalla");
	String txt_usuario  		= (request.getParameter("txt_usuario")==null)?"":request.getParameter("txt_usuario");
	String txt_fecha_act_de = (request.getParameter("txt_fecha_act_de")==null)?"":request.getParameter("txt_fecha_act_de");
	String txt_fecha_act_a  = (request.getParameter("txt_fecha_act_a")==null)?"":request.getParameter("txt_fecha_act_a");
	String pantalla  			= (request.getParameter("pantalla")==null)?"":request.getParameter("pantalla");
	String rfc_prov  			= (request.getParameter("rfc_prov")==null)?"":request.getParameter("rfc_prov");
	String noProv  			= (request.getParameter("numeroProveedor")==null)?"":request.getParameter("numeroProveedor");
	String cmb_afiliado  	= (request.getParameter("cmb_afiliado")==null)?"":request.getParameter("cmb_afiliado");
	String cmb_grupo  		= (request.getParameter("cmb_grupo")==null)?"":request.getParameter("cmb_grupo");
	String noBancoFondeo		= (request.getParameter("noBancoFondeo")==null)?"":request.getParameter("noBancoFondeo");
	String cveEpo				= (request.getParameter("cveEpo")==null)?"":request.getParameter("cveEpo");

	if("EPO".equals(strTipoUsuario)){
		cveEpo = iNoCliente;
	}

	int start = 0;
	int limit = 0;
	com.netro.cadenas.BitCambiosNafinCA2 pag = new com.netro.cadenas.BitCambiosNafinCA2();
	pag.setNoNafin(txt_ne);
	pag.setCmbPantalla(cmb_pantalla);
	pag.setPantalla("B");
	pag.setUsuario(txt_usuario);
	pag.setFechaInicial(txt_fecha_act_de);
	pag.setFechaFinal(txt_fecha_act_a);
	pag.setRfc(rfc_prov);
	pag.setNoProveedor(noProv);
	pag.setClaveEpo(cveEpo);
	
	pag.setClaveAfiliado(cmb_afiliado);
	pag.setGrupo(cmb_grupo);
	pag.setBancoFondeo(noBancoFondeo);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pag);

	if (informacion.equals("ConsultaBitacora")) {		//Datos para la Consulta con Paginacion
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}

			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("ArchivoCSVBit")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ArchivoPDFBit")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
}else if (informacion.equals("CatalogoBuscaAvanzada")) {
	
	String num_ne	= (request.getParameter("num_ne")==null)?"":request.getParameter("num_ne");
	String rfc		= (request.getParameter("rfc")==null)?"":request.getParameter("rfc");
	String nombre	= (request.getParameter("nombre")==null)?"":request.getParameter("nombre");
	String cmb_afiliado = (request.getParameter("cmb_afiliado")==null)?"":request.getParameter("cmb_afiliado");
	String noBancoFondeo = (request.getParameter("noBancoFondeo")==null)?"":request.getParameter("noBancoFondeo");

	Registros registros = afiliacion.getNumeros(cmb_afiliado,nombre,rfc,num_ne,noBancoFondeo);
	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (registros.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(registros.getString("CLAVE"));
		elementoCatalogo.setDescripcion(registros.getString("CLAVE")+" "+registros.getString("DESCRIPCION"));
		coleccionElementos.add(elementoCatalogo);
	}	
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>