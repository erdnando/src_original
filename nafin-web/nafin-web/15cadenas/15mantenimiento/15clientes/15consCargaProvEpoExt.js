var proceso = "";
Ext.onReady(function() {

	var formDato = {
		icEpo:		null,
		icFolio:	null,
		icEstatus:	null,
		fec_ini:		null,
		fec_fin:		null
	}

	function procesaCambiarEstatus(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('contenedorPrincipal').el.unmask();
			if (infoR.result != undefined && infoR.result == 'ok'){
				Ext.getCmp('barraPaginacion').doRefresh();
				Ext.Msg.alert('Cambiar Estatus','Su informaci�n fue actualizada con �xito.');
				Ext.getCmp('winCambia').hide();
			}else{
				Ext.Msg.alert('Cambiar Estatus','Su informaci�n no pudo ser actualizada.');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaEliminarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('contenedorPrincipal').el.unmask();
			if (infoR.result != undefined && infoR.result == 'ok'){
				Ext.Msg.alert('Eliminar Archivos','Su informaci�n fue actualizada con �xito.');
				consultaData.reload();
				//grid.getSelectionModel().clearSelections();
			}else{
				Ext.Msg.alert('Eliminar Archivos','Su informaci�n no pudo ser actualizada.');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaDescargaZIP(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('contenedorPrincipal').el.unmask();
			if (infoR.nombreZip != undefined){
				var winZip = new Ext.Window ({
				  height: 100,
				  x: 400,
				  y: 100,
				  width: 200,
				  modal: true,
				  closeAction: 'hide',
				  title: 'Descarga ZIP',
				  html: '<iframe style="width:100%;height:100%;border:0" src="'+infoR.strDirecVirtualTemp+infoR.nombreZip+'"></iframe>'
				}).show();
				if (winZip){
					winZip.hide();
				}
			}else{
				Ext.Msg.alert('Descargar ZIP','No existe archivo a descargar, los archivos ya fueron eliminados.');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var descargaExcel = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var numProceso = registro.data['NUMPROC'];
		Ext.Ajax.request({
			url: '15consCargaProvEpoExt.data.jsp',
			params: {
					informacion:'GeneraResumenExcel',
					numProc: numProceso
					},
			callback: procesarSuccessGeneraResumenExcel
		});
		
		
	}
	
	var muestraProceso = function(grid, rowIndex, colIndex, item, event) {
		
		var registro = grid.getStore().getAt(rowIndex);
		var numProceso = registro.data['NOMBRE_PROCESO'];

		resCargaEconData.load({
			params:{
				numProc : numProceso
			}
		});
		
		
	}
	
	var procesarConsultaCargaEcon = function(store, arrRegistros, opts) {
		if(arrRegistros!= null) {

			if(store.getTotalCount() > 0) {

				if(!gridCargaEcon.isVisible()) {
					gridCargaEcon.show();
				}
				
				var btnGenerarArch = Ext.getCmp('btnArchivo');
				var btnAbrirCSV = Ext.getCmp('btnAbrirCSV');
				btnGenerarArch.enable();
				btnAbrirCSV.hide();
				fpConsulta.hide();
				grid.hide();
				Ext.getCmp('leyenda').hide();
			}
		}
	}

	var procesarConsultaData = function (store, arrRegistros, opts) {
		
		var fpCons = Ext.getCmp('pnlConsulta');
		fpCons.el.unmask();
		if(arrRegistros!= null) {
			if(!grid.isVisible()) {
				grid.show();
				Ext.getCmp('leyenda').show();				
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnDescargarZIP = Ext.getCmp('btnDescargarZIP');
			
			var btnEliminarArchivo = Ext.getCmp('btnEliminarArchivo');
			var btnCambiarEstatus = Ext.getCmp('btnCambiarEstatus');
			
			var el = grid.getGridEl();  
			    
			if(store.getTotalCount() > 0) {
				btnGenerarArchivo.enable();
				btnBajarArchivo.hide();
				btnDescargarZIP.enable();
				btnEliminarArchivo.enable();
				btnCambiarEstatus.enable();				
				el.unmask();
			}else {
				btnGenerarArchivo.disable();
				btnDescargarZIP.disable();
				btnEliminarArchivo.disable();
				btnCambiarEstatus.disable();
				btnBajarArchivo.hide();			
				
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessGeneraResumenExcel = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var forma = Ext.getDom('formAux');
			if(resp.nombreArchivo=='Vacio') {
				Ext.MessageBox.alert('Mensaje','No se encontraron Registros');
			}else{
				forma.action = resp.urlArchivo;
				forma.submit();
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarSuccessGeneraCSV = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var btnGenerarArch = Ext.getCmp('btnArchivo');
			
			btnGenerarArch.setIconClass('');
			var btnAbrirCSV = Ext.getCmp('btnAbrirCSV');
			btnAbrirCSV.show();
			btnAbrirCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnAbrirCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = resp.urlArchivo;
				forma.submit();
			});
			btnGenerarArch.disable();
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consCargaProvEpoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPOCarga'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consCargaProvEpoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusConsulta'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatusBData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consCargaProvEpoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusCambia'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMotivoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consCargaProvEpoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoMotivo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});


	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15consCargaProvEpoExt.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
					{name: 'IC_FOLIO'},
					{name: 'NOMBRE_EPO'},
					{name: 'CG_NOMBRE_DOCTO'},
					{name: 'DF_CARGA'},
					{name: 'CG_USUARIO'},
					{name: 'CG_NOMBRE_USUARIO'},
					{name: 'IC_ESTATUS_CARGA_PYME'},
					{name: 'ESTATUS'},
					{name: 'CG_OBS_MOTIVO'},
					{name: 'IC_MOTIVO'},
					{name: 'DESCRIPCION'},
					{name: 'IC_FECHA_CAMBIO_ESTATUS'},
					{name: 'NOMBRE_PROCESO'},
					{name: 'CS_BORRADO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, {claveEpo:formDato.icEpo, icFolio:formDato.icFolio, icEstatus:formDato.icEstatus, fecha_carga_ini:formDato.fec_ini,fecha_carga_fin:formDato.fec_fin });
									} 
							},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});
	
	var resCargaEconData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15consCargaProvEpoExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaCargaEcon'
		},
		fields: [
			{name: 'NUMPROC'},
			{name: 'EPO'},
			
			{name: 'REG_TOTALES'},
			{name: 'REG_SINERROR_NE'},
			{name: 'REG_CONERROR_NE'},
			{name: 'REG_PROV_YA_EXISTENTES_NE'},
			  
			{name: 'REGTOTAL'},
			{name: 'REGDUPLI'},
			{name: 'REGUNICO'},
			{name: 'REGYAAFIL'},
			{name: 'REGREAFIL'},
			{name: 'REGREAFILAUT'},
			{name: 'REGCONVUNICO'},
			{name: 'REGPORAFIL'},
			{name: 'REGCARGADOS'},
			{name: 'REGPORCARGAR'},
			{name: 'NOMBREARCH'},
			{name: 'FECHACARGA'},
			{name: 'DATOSCARGA'},
			{name: 'CSPROCESADO'},
			{name: 'TIPOCARGA'},
			{name: 'CONTAFIL'},
			{name: 'CONTREAFIL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaCargaEcon,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCargaEcon(null, null, null);
				}
			}
		}
		
	});

	var selectModel = new Ext.grid.CheckboxSelectionModel({
		width:	25,
		singleSelect: false,
      checkOnly: true,
		listeners: {
			 beforerowselect : function (sm, rowIndex, keep, rec) {
				if ( rec.data.CS_BORRADO == 'S'){
					return false;
				}
			 }
		}
    });

	var Columnas = new Ext.grid.ColumnModel([ 
			selectModel,
			{
				header: 'Folio',tooltip: 'N�mero Folio',	
				dataIndex: 'IC_FOLIO',	
				sortable : true, 
				width : 100, 
				align: 'center',
				resizable: true
			},
			
			{
				header: 'EPO',
				tooltip: 'Nombre de la EPO',	
				dataIndex: 'NOMBRE_EPO',	
				sortable : true, 
				width : 185, 
				align: 'center',
				resizable: true,
				css:"background-color:#FFF;"/*----------------------------------------*/
			},
			
			{
				header: 'Nombre Archivo',
				tooltip: 'Nombre Archivo',	
				dataIndex: 'CG_NOMBRE_DOCTO',	
				sortable : true, 
				width : 185, 
				align: 'center',
				resizable: true
			},
			
			{
				header: 'Fecha y Hora de Carga',
				tooltip: 'Fecha y Hora de Carga',	
				dataIndex: 'DF_CARGA',	
				sortable : true, 
				width : 125, 
				align: 'center',
				resizable: true
			},
			
			{
				header: 'Usuario',
				tooltip: 'Usuario',	
				dataIndex: 'CG_USUARIO',	
				sortable : true, 
				width : 240, 
				align: 'center',
				resizable: true,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value + ' ' +record.get('CG_NOMBRE_USUARIO')) + '"';
								return value + ' ' +record.get('CG_NOMBRE_USUARIO');
				}
			},
			
			{
				header: 'Estatus',
				tooltip: 'Estatus',	
				dataIndex: 'ESTATUS',	
				sortable : true, 
				width : 90, 
				align: 'center',
				resizable: true
			},
			
			{
				header: 'Fecha cambio de estatus',
				tooltip: 'Fecha cambio de estatus',	
				dataIndex: 'IC_FECHA_CAMBIO_ESTATUS',	
				sortable : true, 
				width : 125, 
				align: 'center',
				resizable: true				
			},
			
			{
				header:	'', 
				dataIndex: 'CG_OBS_MOTIVO',
				hidden:true				
			},
			{
				header:	'', 
				dataIndex: 'DESCRIPCION',
				hidden:true				
			},
			
			{
				header: 'Motivo Rechazo',
				tooltip: 'Motivo Rechazo',	
				dataIndex: 'IC_MOTIVO',	
				sortable : true, 
				width : 90, 
				align: 'center',
				resizable: true,
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {	
					if(registro.get('ESTATUS') == ('Rechazada'))
					{
						if ( registro.get('IC_MOTIVO') == ('6') )	
							return value = registro.get('DESCRIPCION') + "- " + registro.get('CG_OBS_MOTIVO'); //;// + "- " + registro.get('CG_OBS_MOTIVO');
						else
							return value = registro.get('DESCRIPCION');
					}
					else 
						return value = 'N/A';				
				}
			},
			
			{
				xtype:	'actioncolumn',
				header : 'Proceso', tooltip: 'Proceso',
				dataIndex : 'NOMBRE_PROCESO',
				width:	80,	
				align: 'center',
				hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('IC_ESTATUS_CARGA_PYME') == '5'){
						return value;
					}else{
						return 'N/A';
					}
				},
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							if (record.get('IC_ESTATUS_CARGA_PYME') == '5'){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else	{
								return "";
							}
						},
						handler:	muestraProceso
					}
				]
			}
	]); 

	var gridView = new Ext.grid.GridView({ 
		//forceFit: true, //Comentar si no. 
		getRowClass : function (row, index) { 
			var cls =''; 
			var data = row.data.CS_BORRADO; //Color es el campo que contiene la simbologia para la leyenda 
			if(data == 'S'){
				return cls = 'redrow'
			}
		} 
	}); //end gridView 


	var grid = new Ext.grid.EditorGridPanel({
		store: consultaData,
		view:gridView, 
		width:300,
		heigth:130,
		hidden: false,
		sm: selectModel,
		cm : Columnas,
		stripeRows: true,
		loadMask: true,
		height: 500,
		width: 900,
		columnLines: true,
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		frame: true,
		hidden: true,
		bbar:{
			xtype: 'paging',
			pageSize: 50,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype:	'button',
					text:		'Eliminar Archivo',
					id:		'btnEliminarArchivo',
					handler: function(boton, evento) {
							var sm = grid.getSelectionModel().getSelections();
							if (!Ext.isEmpty(sm)){
								var folios = [];
								for (i=0; i<=sm.length-1; i++) {
									if (sm[i].get('IC_ESTATUS_CARGA_PYME') == 5){
										folios.push(sm[i].get('IC_FOLIO'));
									}else{
										Ext.Msg.alert(boton.text,'S�lo puede eliminar archivos de remesas con estatus Procesada. <br>Favor de verificar.');
										return;
									}
								}
								
								Ext.Msg.confirm('Eliminar Archivos', '�Desea eliminar el o los archivos? : ',
											function(botonConf) {
												if (botonConf == 'ok' || botonConf == 'yes') {
													Ext.getCmp('contenedorPrincipal').el.mask('Procesando. . . ','x-mask-loading');

													Ext.Ajax.request({
														url: '15consCargaProvEpoExt.data.jsp',
														params: Ext.apply({informacion: "EliminarArchivos",listFolios: Ext.encode(folios)}),
														callback: procesaEliminarArchivos
													});
												}
											}
										);				
								
							}else{
								Ext.Msg.alert(boton.text,'Debe seleccionar al menos un registro para eliminar su archivo original.');
							}
					}
				},'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15consCargaProvEpoExt.data.jsp',
							params: Ext.apply(fpConsulta.getForm().getValues(),{
							 informacion: 'ArchivoCSV',
							 tipo: 'CSV'
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Descargar ZIP',
					id: 'btnDescargarZIP',
					handler: function(boton, evento) {
							var sm = grid.getSelectionModel().getSelections();
							if (!Ext.isEmpty(sm)){
								var folios = [];
								for (i=0; i<=sm.length-1; i++) {
									folios.push(sm[i].get('IC_FOLIO'));
								}
								Ext.getCmp('contenedorPrincipal').el.mask('Procesando. . . ','x-mask-loading');

								Ext.Ajax.request({
									url: '15cargaArchivoExt.data.jsp',
									params: Ext.apply({informacion: "DescargaZIP",listFolios: Ext.encode(folios)}),
									callback: procesaDescargaZIP
								});
							}else{
								Ext.Msg.alert(boton.text,'Debe seleccionar al menos un registro para generar su archivo original.');
							}
					}
				},'-',{
					xtype:	'button',
					text:		'Cambiar Estatus',
					id:		'btnCambiarEstatus',
					handler: function(boton, evento) {
							var sm = grid.getSelectionModel().getSelections();
							if (!Ext.isEmpty(sm)){
								fpCambia.remove(Ext.getCmp('hidFolios'));
								fpCambia.remove(Ext.getCmp('hidUsua'));
								var foliosC = [];
								var usua = [];
								for (i=0; i<=sm.length-1; i++) {
									if (sm[i].get('IC_ESTATUS_CARGA_PYME') != 5){
										foliosC.push(sm[i].get('IC_FOLIO'));
										usua.push(sm[i].get('CG_USUARIO'));
									}else{
										Ext.Msg.alert(boton.text,'No es posible modificar el estatus Procesada. Favor de verificar.');
										return;
									}
								}
								var config = {	xtype:	'hidden', name:	'hidFolios', id:	'hidFolios', value:	Ext.encode(foliosC) }
								var configB = {xtype:	'hidden', name:	'hidUsua', id:	'hidUsua', value:	Ext.encode(usua) }
								fpCambia.add(config);
								fpCambia.add(configB);
								fpCambia.doLayout();
								var winCambia = Ext.getCmp('winCambia');
								if (winCambia){
									winCambia.show();
								}else{
									var winCambia = new Ext.Window ({
										id:	'winCambia',
										width: 710,	
										modal: true,
										autoHeight: true,
										resizable:false,
										closeAction: 'hide',
										closable:false,
										autoDestroy:true,			
										title: 'Cambio de Estatus',
										items:[fpCambia],
										bbar: {
											xtype: 'toolbar',	buttonAlign:'center',	
											buttons: ['->',
												{	xtype: 'button',	text: 'Salir',	iconCls: 'icoLimpiar', 	id: 'btnCerraP', handler: function(){Ext.getCmp('winCambia').hide();} },
												'-'							
											]
										}
									}).show();
								}
								fpCambia.getForm().reset();
							}else{
								Ext.Msg.alert(boton.text,'Debe seleccionar al menos un registro para cambiar su Estatus.');
							}
					}
				}
      ]
		}
	}); 
	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 2, align: 'center'},
				{header: 'Carga N@E', colspan: 4, align: 'center'},
				{header: 'Limpieza', colspan: 3, align: 'center'},
				{header: 'Dispersi�n de Registros', colspan: 5, align: 'center'},
				{header: 'Econtract', colspan: 2, align: 'center'},
				{header: '', colspan: 5, align: 'center'}
			]
		]
	});
	
	
	var gridCargaEcon = new Ext.grid.EditorGridPanel({
		id: 'gridCargaEcon1',
		store: resCargaEconData,
		margins: '20 0 0 0',
		clicksToEdit: 1,
		columns: [
			{
				header: 'N�mero de Proceso',
				tooltip: 'N�mero de Proceso',
				dataIndex: 'NUMPROC',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'EPO',
				sortable: true,
				width: 200,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			/*-------------------------------CARGA N@E---------------------------*/
			{
				header: 'Registros Totales',
				tooltip: 'Registros Totales',
				dataIndex: 'REG_TOTALES ',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {	
					if( registro.get('REG_TOTALES') == ('0') ){
						value = '0';
					} else {
						value = registro.get('REG_TOTALES');
					}
					return value;
				}
			},
			{
				header: 'Registros sin Error',
				tooltip: 'Registros sin Error',
				dataIndex: 'REG_SINERROR_NE ',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {	
					if( registro.get('REG_SINERROR_NE') == ('0') ) {
						value = '0';
					} else {
						value	= registro.get('REG_SINERROR_NE');
					}
					return value;
				}
			},
			{
				header: 'Registros con Error',
				tooltip: 'Registros con Error',
				dataIndex: 'REG_CONERROR_NE ',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {	
					if( registro.get('REG_CONERROR_NE') == ('0') ){
						value = '0';
					} else {
						value = registro.get('REG_CONERROR_NE');
					}
					return value;
				}
			},
			{
				header: 'Ya Existentes',
				tooltip: 'Ya Existentes',
				dataIndex: 'REG_PROV_YA_EXISTENTES_NE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {	
					if( registro.get('REG_PROV_YA_EXISTENTES_NE') == ('0') ){
						value = '0';
					} else {
						value = registro.get('REG_PROV_YA_EXISTENTES_NE');
					}
					return value;
				}
			},
			/*-------------------------------------------------------------------*/
			{
				header: 'Registros Totales',
				tooltip: 'Registros Totales',
				dataIndex: 'REGTOTAL',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Duplicados',
				tooltip: 'Duplicados',
				dataIndex: 'REGDUPLI',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: '�nicos',
				tooltip: 'Unicos',
				dataIndex: 'REGUNICO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Ya Afiliados',
				tooltip: 'Ya Afiliados',
				dataIndex: 'REGYAAFIL',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Reafiliaciones',
				tooltip: 'Reafiliaciones',
				dataIndex: 'REGREAFIL',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Reafiliaciones Auto.',
				tooltip: 'Reafiliaciones Autom�ticas',
				dataIndex: 'REGREAFILAUT',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Con Contrato �nico',
				tooltip: 'Con Contrato �nico',
				dataIndex: 'REGCONVUNICO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Por Afiliar',
				tooltip: 'Por Afiliar',
				dataIndex: 'REGPORAFIL',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Ya Cargados',
				tooltip: 'Ya Cargados',
				dataIndex: 'REGCARGADOS',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Por Cargar',
				tooltip: 'Por Cargar',
				dataIndex: 'REGPORCARGAR',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Nombre Archivo',
				tooltip: 'Nombre Archivo',
				dataIndex: 'NOMBREARCH',
				sortable: true,
				width: 200,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Fecha de Carga',
				tooltip: 'Fecha de Carga',
				dataIndex: 'FECHACARGA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Datos de Carga',
				tooltip: 'Datos de Carga',
				dataIndex: 'DATOSCARGA',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Procesado',
				tooltip: 'Procesado',
				dataIndex: 'CSPROCESADO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},{
				xtype:	'actioncolumn',
				header : 'Resumen', tooltip: 'Resumen',
				width:	65,	align: 'center', hidden: false,
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver Detalle';
							return 'icoXls';
							
						},
						handler:	descargaExcel
					}
				]
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 300,
		width: 900,
		style: 'margin:0 auto;',
		title: '<div align="center">Consulta de la Carga Masiva de Proveedores</div>',
		frame: true,
		hidden: true,
		plugins: grupos,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Generar Archivo',
					id:'btnArchivo',
					handler: function(btn){
						
						var store = gridCargaEcon.getStore();
						var registrosEnviar = [];
						store.each(function(record) {
							registrosEnviar.push(record.data);
						});//fin 
						
						registrosEnviar = Ext.encode(registrosEnviar);
						
						btn.setIconClass('loading-indicator');
						
						Ext.Ajax.request({
							url : '15consCargaProvEpoExt.data.jsp',
							params :{
								informacion: 'GenerarCSV',
								registros : registrosEnviar
							},
							callback: procesarSuccessGeneraCSV
						});
					}
				},
				{
					text: 'Abrir CSV',
					id:'btnAbrirCSV',
					hidden:true
				},
				'-',
				{
					text: 'Regresar',
					id:'btnRegresar',
					handler: function(btn){
						gridCargaEcon.hide();
						fpConsulta.show();
						grid.show();
						Ext.getCmp('leyenda').show();
					}
				}
			]
			
		}
		
	});

	var elementosFormaCambia = [
		{
			xtype:	'combo',
			id:		'id_cmbCambia',
			name:		'cmbCambia',
			fieldLabel: 'Estatus Nuevo',
			mode: 'local',
			allowBlank:	false,
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cmbCambia',
			emptyText: 'Seleccione. . .',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEstatusBData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:	{
				select:	{
					fn: function(combo){
						
						var motivo = Ext.getCmp('id_cmbMotivo');
						motivo.setValue('');
						motivo.store.removeAll();
						motivo.setDisabled(true);
						var txtArea = Ext.getCmp('txtArea');
						txtArea.setValue('');
						txtArea.setDisabled(true);
						if (combo.getValue() == 4){
							motivo.setDisabled(false);
							motivo.store.reload();
							motivo.reset();
						}
					}
				}
			}
		},{
			xtype:	'combo',
			id:		'id_cmbMotivo',
			name:		'cmbMotivo',
			fieldLabel: 'Motivo de Rechazo',
			mode: 'local',
			disabled: true,
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cmbMotivo',
			emptyText: 'Seleccione. . .',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoMotivoData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:	{
				select:	{
					fn: function(combo){
						var txtArea = Ext.getCmp('txtArea');
						txtArea.setValue('');
						if (combo.getValue() == 6){
							txtArea.setDisabled(false);
							txtArea.focus();
						}else{
							txtArea.setDisabled(true);
						}
					}
				}
			}
		},{
			xtype:	'textarea',
			name:		'txtArea',
			id:		'txtArea',
			fieldLabel: 'Especifique',
			disabled:	true,
			allowBlanck: false,
			height:		100,
			maxLength: 255
		}
	]; 

	var fpCambia = new Ext.form.FormPanel({
		id: 'pnlCambia',
		width: 480,
		style: 'margin: 0 auto',
		bodyStyle:'padding:10px',
		defaults: {
			msgTarget: 'side',
			anchor: '-18'
		},
		labelWidth: 130,
		items: elementosFormaCambia,
		monitorValid: true,
		buttonAlign:	'center',
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'aceptar',
				formBind: true,
				handler: function(boton, evento) {
					var cmbCambia = Ext.getCmp('id_cmbCambia');
					if (cmbCambia.getValue() == 4){
						var cmbMotivo = Ext.getCmp('id_cmbMotivo');
						if (cmbMotivo.getValue() == ''){
							cmbMotivo.markInvalid('Debe especificar el motivo de rechazo');
							return;
						}
						if (cmbMotivo.getValue() == 6){
							var txtArea = Ext.getCmp('txtArea');
							if (Ext.isEmpty(txtArea.getValue())){
								txtArea.markInvalid('Debe especificar el motivo de rechazo');
								return;
							}
						}
					}
					Ext.getCmp('contenedorPrincipal').el.mask('Procesando. . . ','x-mask-loading');

					Ext.Ajax.request({
						url: '15consCargaProvEpoExt.data.jsp',
						params: Ext.apply(fpCambia.getForm().getValues(),{informacion: "CambiarEstatus"}),
						callback: procesaCambiarEstatus
					});
				}
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					fpCambia.getForm().reset();
					var motivo = Ext.getCmp('cmbMotivo');
					motivo.setValue('');
					motivo.store.removeAll();
					motivo.setDisabled(true);
					motivo.reset();
				}
			}
		]
	});

	var elementosFormaConsulta = [
		{
			xtype: 'combo',
			name: 'claveEpo',
			id: 'cmbEpo',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveEpo',
			emptyText: 'Seleccione EPO',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			anchor:	'95%',
			tpl:'<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}</div>'+
				'</tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
				'</div></tpl></tpl>',
				setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();			
			} 
		},{
			xtype:	'textfield',
			id:		'icFolio',
			name:		'icFolio',
			fieldLabel:	'N�mero Folio',
			maxLength:	16,
			anchor:	'95%'
		},
				{
			xtype: 'combo',
			name: 'icEstatus',
			id: 'icEstatus1',
			fieldLabel: 'Estatus',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'icEstatus',
			emptyText: 'Seleccione Estatus',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEstatusData,
			tpl : NE.util.templateMensajeCargaCombo,
			anchor:	'95%'			
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud',
			combineErrors: false,
			msgTarget: 'side',
			//anchor:'100%',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_carga_ini',
					id: 'fecha_carga_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecha: 'fecha_carga_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_carga_fin',
					id: 'fecha_carga_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_carga_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}
	];

	var fpConsulta = new Ext.form.FormPanel({
		id: 'pnlConsulta',
		width: 500,
		title: 'Consulta Carga de Proveedores-Epo�s',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		style: 'margin: 0 auto',
		bodyStyle:'padding:10px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		labelWidth: 130,
		defaultType: 'textfield',
		items: elementosFormaConsulta,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					
					/*Validacion del folio*/
					var folio = Ext.getCmp('icFolio');
					if (!esVacio(folio.getValue())){
						if (!isdigit(folio.getValue())){
							folio.markInvalid('Verifique el formato del campo N�mero Folio');
							folio.focus();
							return;
					}}	
					
					cmbEpo = Ext.getCmp('cmbEpo');
					icFolio = Ext.getCmp('icFolio');
					icEstatus = Ext.getCmp('icEstatus1');
					d_ini = Ext.getCmp('fecha_carga_ini');
					d_fin = Ext.getCmp('fecha_carga_fin');
					formDato.icEpo	= cmbEpo.getValue();
					formDato.icFolio	= icFolio.getValue();
					formDato.icEstatus= icEstatus.getValue();
					if (Ext.isEmpty(d_ini.getValue())){
						formDato.fec_ini = '';
					}else{
						formDato.fec_ini	= (d_ini.getValue()).format('d/m/Y');
					}
					if (Ext.isEmpty(d_fin.getValue())){
						formDato.fec_fin	= '';
					}else{
						formDato.fec_fin	= (d_fin.getValue()).format('d/m/Y');
					}
					
					
					if (Ext.isEmpty(cmbEpo.getValue()) && Ext.isEmpty(icFolio.getValue()) && Ext.isEmpty(icEstatus.getValue()) && Ext.isEmpty(d_ini.getValue())){
						Ext.Msg.alert(boton.text,'Debe seleccionar al menos un criterio para realizar la b�squeda.');
						return;
					}
					
					var fecha_carga_ini = Ext.getCmp('fecha_carga_ini');
					var fecha_carga_fin = Ext.getCmp('fecha_carga_fin');
					if(!Ext.isEmpty(fecha_carga_ini.getValue()) || !Ext.isEmpty(fecha_carga_fin.getValue())){
						if(Ext.isEmpty(fecha_carga_ini.getValue())){
							fecha_carga_ini.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_carga_ini.focus();
							return;
						}
						if(Ext.isEmpty(fecha_carga_fin.getValue())){
							fecha_carga_fin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_carga_fin.focus();
							return;
						}
					}
					
					grid.hide();
					Ext.getCmp('leyenda').hide();					
					fpConsulta.el.mask('Enviando...', 'x-mask-loading');					
					consultaData.load({params: Ext.apply({operacion: 'Generar',start: 0,limit: 50})});
					
				}
			},
			{
				text: 'Cancelar',
				hidden: false,
				iconCls: 'borrar',
				handler: function() {
					fpConsulta.getForm().reset();
					grid.hide();
					Ext.getCmp('leyenda').hide();
				}
				
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			NE.util.getEspaciador(10),
			fpConsulta,
			NE.util.getEspaciador(10),
			grid,
			{
				xtype: 	'label',
				id:	 	'leyenda',
				hidden: 	true,
				cls:		'x-form-item',
				style: 	'font color:white;  background-color: #F00; size:1; text-align:left;margin:10px;',
				html:  	'<b>Nota: Los registros marcados en color "ROJO" indican las Cargas de Proveedores eliminados permanentemente</b>'
			},

			NE.util.getEspaciador(10),
			gridCargaEcon
		]
	});

	catalogoEPOData.load();
	catalogoEstatusData.load();
	catalogoEstatusBData.load();

});