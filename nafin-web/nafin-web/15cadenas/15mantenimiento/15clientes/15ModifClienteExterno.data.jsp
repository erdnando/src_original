<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject,
		java.sql.*,
		com.netro.seguridadbean.SeguException,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,			
		com.netro.seguridad.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	
String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String cmbTipoAfiliado = (request.getParameter("cmbTipoAfiliado") != null)?request.getParameter("cmbTipoAfiliado"):"";
String cboTipoPersona = (request.getParameter("cboTipoPersona") != null)?request.getParameter("cboTipoPersona"):"";
String appPaterno = (request.getParameter("appPaterno") != null)?request.getParameter("appPaterno"):"";
String appMaterno = (request.getParameter("appMaterno") != null)?request.getParameter("appMaterno"):"";
String nombres = (request.getParameter("nombres") != null)?request.getParameter("nombres"):"";
String txtRFC_Fisica = (request.getParameter("txtRFC_Fisica") != null)?request.getParameter("txtRFC_Fisica"):"";
String txtRazonSocial = (request.getParameter("txtRazonSocial") != null)?request.getParameter("txtRazonSocial"):"";
String txtRFC_Moral = (request.getParameter("txtRFC_Moral") != null)?request.getParameter("txtRFC_Moral"):"";
String numClienteSirac = (request.getParameter("numClienteSirac") != null)?request.getParameter("numClienteSirac"):"";
String txtCalle = (request.getParameter("txtCalle") != null)?request.getParameter("txtCalle"):"";
String txtColonia = (request.getParameter("txtColonia") != null)?request.getParameter("txtColonia"):"";
String cboPais = (request.getParameter("cboPais") != null)?request.getParameter("cboPais"):"";
String cboEstado = (request.getParameter("cboEstado") != null)?request.getParameter("cboEstado"):"";
String cboMunicipio = (request.getParameter("cboMunicipio") != null)?request.getParameter("cboMunicipio"):"";
String txtEmail = (request.getParameter("txtEmail") != null)?request.getParameter("txtEmail"):"";
String txtCodigoPostal = (request.getParameter("txtCodigoPostal") != null)?request.getParameter("txtCodigoPostal"):"";
String txtTelefono = (request.getParameter("txtTelefono") != null)?request.getParameter("txtTelefono"):"";
String txtNumeroCel = (request.getParameter("txtNumeroCel") != null)?request.getParameter("txtNumeroCel"):"";
String num_electronico = (request.getParameter("num_electronico") != null)?request.getParameter("num_electronico"):"";



String txtRFC ="",   infoRegresar = "",  mensaje =""; 

JSONObject jsonObj = new JSONObject();

if(cboTipoPersona.equals("F")) {	  txtRFC   =  txtRFC_Fisica;  }
if(cboTipoPersona.equals("M")) {	  txtRFC   =  txtRFC_Moral;  } 

Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);		

if(informacion.equals("catalogoPais")) {		
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_pais");
	cat.setCampoClave("ic_pais");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setValoresCondicionNotIn("52", Integer.class);
	
	infoRegresar = cat.getJSONElementos();	
	
} else if(informacion.equals("catalogoEstado"))	{
	
	CatalogoEstado cat = new CatalogoEstado();
	cat.setCampoClave("ic_estado");
	cat.setCampoDescripcion("cd_nombre");
	cat.setClavePais(cboPais);
	cat.setOrden("cd_nombre");
	
	infoRegresar = cat.getJSONElementos();
	
 } else if(informacion.equals("catalogoMunicipio"))	{
 
	CatalogoMunicipio cat = new CatalogoMunicipio();
	cat.setPais(cboPais);
	cat.setClave("IC_MUNICIPIO");  
	cat.setDescripcion("upper(CD_NOMBRE)");
	cat.setEstado(cboEstado);
	List elementos = cat.getListaElementos();
	JSONArray jsonArr = new JSONArray();
	Iterator it = elementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";


 } else if(informacion.equals("ConsultarDatos"))	{
 
 Registros reg = afiliacion.getDatosClienteExterno( num_electronico);
 	
	while(reg.next()) {
		cboTipoPersona = (reg.getString("PERSONA") == null) ? "" : reg.getString("PERSONA").trim();
		appPaterno = (reg.getString("PATERNO") == null) ? "" : reg.getString("PATERNO").trim();
		appMaterno = (reg.getString("MATERNO") == null) ? "" : reg.getString("MATERNO").trim();
		nombres = (reg.getString("NOMBRE") == null) ? "" : reg.getString("NOMBRE").trim();
		txtRFC = (reg.getString("RFC") == null) ? "" : reg.getString("RFC").trim();
		txtRazonSocial = (reg.getString("RAZON_SOCIAL") == null) ? "" : reg.getString("RAZON_SOCIAL").trim();
		numClienteSirac = (reg.getString("NUM_SIRAC") == null) ? "" : reg.getString("NUM_SIRAC").trim();
		txtCalle = (reg.getString("CALLE") == null) ? "" : reg.getString("CALLE").trim();
		txtColonia = (reg.getString("COLONIA") == null) ? "" : reg.getString("COLONIA").trim();
		cboPais = (reg.getString("CLAVE_PAIS") == null) ? "" : reg.getString("CLAVE_PAIS").trim();
		cboEstado = (reg.getString("CLAVE_ESTADO") == null) ? "" : reg.getString("CLAVE_ESTADO").trim();
		cboMunicipio = (reg.getString("MUNICIPIO") == null) ? "" : reg.getString("MUNICIPIO").trim();
		txtEmail = (reg.getString("EMAIL") == null) ? "" : reg.getString("EMAIL").trim();
		txtCodigoPostal = (reg.getString("CODIGO_POSTAL") == null) ? "" : reg.getString("CODIGO_POSTAL").trim();
		txtTelefono = (reg.getString("TELEFONO") == null) ? "" : reg.getString("TELEFONO").trim();
		txtNumeroCel = (reg.getString("CELULAR") == null) ? "" : reg.getString("CELULAR").trim();
		num_electronico = (reg.getString("NAFIN_ELECTRONICO") == null) ? "" : reg.getString("NAFIN_ELECTRONICO").trim();
			
	};
	
	jsonObj.put("success",  new Boolean(true));		
	jsonObj.put("num_electronico", num_electronico);
	jsonObj.put("cboTipoPersona", cboTipoPersona);
	jsonObj.put("appPaterno", appPaterno);
	jsonObj.put("appMaterno", appMaterno);
	jsonObj.put("nombres", nombres);
	jsonObj.put("txtRFC", txtRFC);
	jsonObj.put("txtRazonSocial", txtRazonSocial);
	jsonObj.put("numClienteSirac", numClienteSirac);
	jsonObj.put("txtCalle", txtCalle);
	jsonObj.put("txtColonia", txtColonia);
	jsonObj.put("cboPais", cboPais);
	jsonObj.put("cboEstado", cboEstado);
	jsonObj.put("cboMunicipio", cboMunicipio);
	jsonObj.put("txtEmail", txtEmail);
	jsonObj.put("txtCodigoPostal", txtCodigoPostal);
	jsonObj.put("txtTelefono", txtTelefono);
	jsonObj.put("txtNumeroCel", txtNumeroCel);
	jsonObj.put("num_electronico", num_electronico);		
	infoRegresar = jsonObj.toString();	

 } else if(informacion.equals("ClienteSIRAC"))	{
 
 	String rfcSirac =  afiliacion.getClienteSirac (numClienteSirac );
	String ValidaSirac =  afiliacion.validaNumClieSurac (numClienteSirac );
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true));	
	jsonObj.put("rfcSirac", rfcSirac);		
	jsonObj.put("ValidaSirac", ValidaSirac);	
	infoRegresar = jsonObj.toString();	
 
 } else if(informacion.equals("Modificar"))	{
		
	List parametros = new ArrayList();
 	parametros.add(cboTipoPersona );
	parametros.add(appPaterno );	
	parametros.add(appMaterno );	
	parametros.add(nombres );
	parametros.add(txtRFC );	
	parametros.add(txtRazonSocial );	
	parametros.add(numClienteSirac );	
	parametros.add(txtCalle );	
	parametros.add(txtColonia );	
	parametros.add(cboPais );	
	parametros.add(cboEstado );	
	parametros.add(cboMunicipio );	
	parametros.add(txtEmail );	
	parametros.add(txtCodigoPostal );	
	parametros.add(txtTelefono );	
	parametros.add(txtNumeroCel );	
	parametros.add(strLogin  );	
	parametros.add(num_electronico );	
	
 
	mensaje =   afiliacion.actualizaClienteExterno( parametros) ;
	
	jsonObj.put("success",  new Boolean(true));	
	jsonObj.put("mensaje","<center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+mensaje+"</center>");
	infoRegresar = jsonObj.toString();	
	
 
 }
%>
<%=infoRegresar%>

<%!

	
	

	%>