<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.afiliacion.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%

String fecini = request.getParameter("fecini")==null?Fecha.getFechaActual():request.getParameter("fecini");
String fecfin = request.getParameter("fecfin")==null?Fecha.getFechaActual():request.getParameter("fecfin");
String cveEpo = request.getParameter("cveEpo")==null?"":request.getParameter("cveEpo");

if("EPO".equals(strTipoUsuario)){
	cveEpo = iNoCliente;
}
JSONObject jsonObj = new JSONObject();

try {
	List lstConsultaBitCarga = new ArrayList();
	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion afiliacion = afiliacionHome.create();
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = Comunes.cadenaAleatoria(16)+".csv";

	//lstConsultaBitCarga = afiliacion.getBitacoraCargaMasiva("225", "01/09/2011", "22/11/2011");	//Para realizar pruebas
	lstConsultaBitCarga = afiliacion.getBitacoraCargaMasiva(cveEpo, fecini, fecfin);
	if (lstConsultaBitCarga!=null && lstConsultaBitCarga.size()>0) {
		for(int x=0; x<lstConsultaBitCarga.size();x++){
			List lstRegConsulta = (List)lstConsultaBitCarga.get(x);
			
			if(x==0){
				if("NAFIN".equals(strTipoUsuario)){
					contenidoArchivo.append("EPO,");
				}	
				contenidoArchivo.append("Fecha de Carga de Archivo,");
				if("NAFIN".equals(strTipoUsuario)){
					contenidoArchivo.append("Nombre Archivo,Usuario responsable,");
				}
				contenidoArchivo.append("Total de Registros,Registros Procesados,Registros Inválidos\n");
			}
			if("NAFIN".equals(strTipoUsuario)){
				contenidoArchivo.append(((String)lstRegConsulta.get(9)).replaceAll(",","")+",");
			}
			contenidoArchivo.append((String)lstRegConsulta.get(6)+",");
			if("NAFIN".equals(strTipoUsuario)){
				contenidoArchivo.append(
					(String)lstRegConsulta.get(2) + "," +
					((String)lstRegConsulta.get(8)).replaceAll(",","") + ","
				);
			}
			contenidoArchivo.append((String)lstRegConsulta.get(3)+","+(String)lstRegConsulta.get(4)+","+(String)lstRegConsulta.get(5)+"\n");
		}
	}

	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {}
%>
<%=jsonObj%>