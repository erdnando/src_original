Ext.onReady(function(){

	function procesarSuccessFailureGuardar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var cve = infoR.num_cve;
			if(cve != null && cve != ""){
				Ext.Msg.alert('Mensaje de p�gina web','Su n�mero de Nafin Electr�nico es: ' + cve,function() {
					window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
				},this);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
/*----------------------------- Maquina de Edos. ----------------------------*/
		var accionConsulta = function(estadoSiguiente, respuesta){

			if(  estadoSiguiente == "CONSULTAR" ){
				var a = Ext.getCmp('id_emailc').getValue();
				var b = Ext.getCmp('id_emailc_conf').getValue();
				//*****************Validaciones***************
				if(Ext.getCmp('_cmb_persona').getValue()==1 && (Ext.getCmp('_cmb_cadenas').getValue()=='' || Ext.getCmp('id_paterno').getValue()=='' || !Ext.getCmp('id_paterno').validateValue() || Ext.getCmp('id_nombre').getValue()=='' || !Ext.getCmp('id_nombre').validateValue()
				|| Ext.getCmp('id_materno').getValue()=='' || !Ext.getCmp('id_materno').validateValue() || Ext.getCmp('_rfc_1').getValue()=='' || !Ext.getCmp('_rfc_1').validateValue() || Ext.getCmp('_calle').getValue()=='' || !Ext.getCmp('_calle').validateValue() || Ext.getCmp('_cmb_pais').getValue()==''
				|| Ext.getCmp('_cmb_edo').getValue()=='' || Ext.getCmp('_cod_post').getValue()=='' || !Ext.getCmp('_cod_post').validateValue() || Ext.getCmp('_telefono').getValue()=='' || !Ext.getCmp('_telefono').validateValue() || Ext.getCmp('_colonia').getValue()=='' || !Ext.getCmp('_colonia').validateValue()
				|| Ext.getCmp('_cmb_del_mun').getValue()=='' || Ext.getCmp('id_paterno_contacto').getValue()=='' || !Ext.getCmp('id_paterno_contacto').validateValue() || Ext.getCmp('id_nombre_contacto').getValue()=='' || !Ext.getCmp('id_nombre_contacto').validateValue()
				|| Ext.getCmp('id_emailc').getValue()=='' || !Ext.getCmp('id_emailc').validateValue() || Ext.getCmp('id_materno_contacto').getValue()=='' || !Ext.getCmp('id_materno_contacto').validateValue() || Ext.getCmp('_telc').getValue()=='' || !Ext.getCmp('_telc').validateValue() || Ext.getCmp('_txt_cel_cont').getValue()=='' || !Ext.getCmp('_txt_cel_cont').validateValue() 
				|| Ext.getCmp('id_emailc_conf').getValue()=='') || !Ext.getCmp('id_emailc_conf').validateValue() || !Ext.getCmp('_fax').validateValue() || !Ext.getCmp('_faxc').validateValue()){
					if(Ext.getCmp('_cmb_cadenas').getValue()=='')
						Ext.getCmp('_cmb_cadenas').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_paterno').getValue()=='')
						Ext.getCmp('id_paterno').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_nombre').getValue()=='')
						Ext.getCmp('id_nombre').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_materno').getValue()=='')
						Ext.getCmp('id_materno').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_rfc_1').getValue()=='')
						Ext.getCmp('_rfc_1').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_calle').getValue()=='')
						Ext.getCmp('_calle').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_cmb_pais').getValue()=='')
						Ext.getCmp('_cmb_pais').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_cmb_edo').getValue()=='')
						Ext.getCmp('_cmb_edo').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_cod_post').getValue()=='')
						Ext.getCmp('_cod_post').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_telefono').getValue()=='')
						Ext.getCmp('_telefono').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_colonia').getValue()=='')
						Ext.getCmp('_colonia').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_cmb_del_mun').getValue()=='')
						Ext.getCmp('_cmb_del_mun').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_paterno_contacto').getValue()=='')
						Ext.getCmp('id_paterno_contacto').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_nombre_contacto').getValue()=='')
						Ext.getCmp('id_nombre_contacto').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_emailc').getValue()=='')
						Ext.getCmp('id_emailc').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_materno_contacto').getValue()=='')
						Ext.getCmp('id_materno_contacto').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_telc').getValue()=='')
						Ext.getCmp('_telc').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_txt_cel_cont').getValue()=='')
						Ext.getCmp('_txt_cel_cont').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_emailc_conf').getValue()=='')
						Ext.getCmp('id_emailc_conf').markInvalid("Campo obligatorio");
						
				}else if(Ext.getCmp('_cmb_persona').getValue()==5 && ( Ext.getCmp('_cmb_cadenas').getValue()=='' || Ext.getCmp('_razon_social_moral').getValue()=='' || !Ext.getCmp('_razon_social_moral').validateValue() || Ext.getCmp('_rfc_moral').getValue()=='' || !Ext.getCmp('_rfc_moral').validateValue()
					|| Ext.getCmp('_calle').getValue()=='' || !Ext.getCmp('_calle').validateValue() || Ext.getCmp('_cmb_pais').getValue()==''
					|| Ext.getCmp('_cmb_edo').getValue()=='' || Ext.getCmp('_cod_post').getValue()=='' || !Ext.getCmp('_cod_post').validateValue() || Ext.getCmp('_telefono').getValue()=='' || !Ext.getCmp('_telefono').validateValue() || Ext.getCmp('_colonia').getValue()=='' || !Ext.getCmp('_colonia').validateValue()
					|| Ext.getCmp('_cmb_del_mun').getValue()=='' || Ext.getCmp('id_paterno_contacto').getValue()=='' || !Ext.getCmp('id_paterno_contacto').validateValue() || Ext.getCmp('id_nombre_contacto').getValue()=='' || !Ext.getCmp('id_nombre_contacto').validateValue()
					|| Ext.getCmp('id_emailc').getValue()=='' || !Ext.getCmp('id_emailc').validateValue() || Ext.getCmp('id_materno_contacto').getValue()=='' || !Ext.getCmp('id_materno_contacto').validateValue() || Ext.getCmp('_telc').getValue()=='' || !Ext.getCmp('_telc').validateValue() || Ext.getCmp('_txt_cel_cont').getValue()=='' || !Ext.getCmp('_txt_cel_cont').validateValue() 
					|| Ext.getCmp('id_emailc_conf').getValue()=='') || !Ext.getCmp('id_emailc_conf').validateValue() || !Ext.getCmp('_fax').validateValue() || !Ext.getCmp('_faxc').validateValue()){
					if(Ext.getCmp('_cmb_cadenas').getValue()=='')
						Ext.getCmp('_cmb_cadenas').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_razon_social_moral').getValue()=='')
						Ext.getCmp('_razon_social_moral').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_rfc_moral').getValue()=='')
						Ext.getCmp('_rfc_moral').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_calle').getValue()=='')
						Ext.getCmp('_calle').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_cmb_pais').getValue()=='')
						Ext.getCmp('_cmb_pais').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_cmb_edo').getValue()=='')
						Ext.getCmp('_cmb_edo').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_cod_post').getValue()=='')
						Ext.getCmp('_cod_post').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_telefono').getValue()=='')
						Ext.getCmp('_telefono').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_colonia').getValue()=='')
						Ext.getCmp('_colonia').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_cmb_del_mun').getValue()=='')
						Ext.getCmp('_cmb_del_mun').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_paterno_contacto').getValue()=='')
						Ext.getCmp('id_paterno_contacto').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_nombre_contacto').getValue()=='')
						Ext.getCmp('id_nombre_contacto').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_emailc').getValue()=='')
						Ext.getCmp('id_emailc').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_materno_contacto').getValue()=='')
						Ext.getCmp('id_materno_contacto').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_telc').getValue()=='')
						Ext.getCmp('_telc').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('_txt_cel_cont').getValue()=='')
						Ext.getCmp('_txt_cel_cont').markInvalid("Campo obligatorio");
					else if(Ext.getCmp('id_emailc_conf').getValue()=='')
						Ext.getCmp('id_emailc_conf').markInvalid("Campo obligatorio");
							
				}else if(a!=b)
					Ext.Msg.alert('Alerta','El mail y confirmaci�n deben ser iguales');
				
				else{
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							//peticion de Registro de Afiliaci�n
							Ext.Ajax.request({
								url: '15forma1_manExt.data.jsp',
								params:	Ext.apply(fpDatos.getForm().getValues(),
										{
											informacion:'guardaDatos'
										}
								),
								callback: procesarSuccessFailureGuardar
							});
						}
					});
				}
				
			}else if(	estadoSiguiente == "LIMPIAR"){
				var p = Ext.getCmp('_cmb_persona').getValue();
				Ext.getCmp('formaAfiliacion').getForm().reset();
				Ext.getCmp('_cmb_persona').setValue(p);
			}
			
		}
	
/*---------------------------------- Store's --------------------------------*/

	function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		}else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} 	else if (pagina == 2) { 	// Distribuidor						
			window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 		 
		} else if (pagina == 3) { 	//Proveedor
			window.location = '15forma01Ext.jsp'; 
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} else if (pagina == 6) { 	//Contragarante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 	
		} else if (pagina == 9) { 	// Mandante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
		} else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		}	else if (pagina == 11) { 	//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 	
		}	else if (pagina == 12) { 	//Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliaClienteExternoExt.jsp"; 
		}
	}
	
	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [[/*'M'*/	'9','Mandante']
				,[/*'EPO'*/  '0','Cadena Productiva']
				,[/*'IF'*/	'1','Intermediario Financiero']
				,[/*'2'*/	'2','Distribuidor']
				,[/*'1'*/	'3','Proveedor']	
				,[/*'ACE'*/	'4','Afiliados Credito Electronico']
				,[/*'EPOI'*/'5','Cadena Productiva Internacional']
				,[/*'CONTRA'*/'6','Contragarante']				
				,[/*'AF'*/	'10','Afianzadora']
				,[/*'UNI'*/ '11','Universidad-Programa Cr�dito Educativo']
				,['12','Cliente Externo']
				]
	});
	
	var storePersona = new Ext.data.SimpleStore({
    fields: ['clave', 'descripcion'],
    data : [ ['1','F�sica']
				 ,['5','Moral']
			  ]
	});
	
	//Catalogo para el combo de seleccion de cadenas productivas
	var catalogoCadenasData = new Ext.data.JsonStore({
		id:				'catalogoCadenasDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15forma1_manExt.data.jsp',
		baseParams:		{	informacion: 'catalogoCadenas'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//Catalogo para el combo de seleccion de Pais
	var catalogoPaisData = new Ext.data.JsonStore({
		id:				'catalogoPaisDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15forma1_manExt.data.jsp',
		baseParams:		{	informacion: 'catalogoPais'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//Catalogo para el combo de seleccion de Estado
	var catalogoEstadoData = new Ext.data.JsonStore({
		id:				'catalogoEstadoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15forma1_manExt.data.jsp',
		baseParams:		{	informacion: 'catalogoEstado'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//catalogo Municipio
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma1_manExt.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
/*-------------------------------- Componentes ------------------------------*/
	
	var afiliacion = {
		xtype		: 'panel',
		id 		: 'afiliacion',
		layout	: 'hbox',
		border	: false,		
		width		: 900,		
		bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:110px;',
		items		: 
		[{
			layout		: 'form',
			width			:400,
			labelWidth	: 60,
			border		: false,
			items			:[{
					xtype				: 'combo',
					id					: 'id_afiliacion',
					name				: 'afiliacion',
					hiddenName 		:'afiliacion', 
					fieldLabel		: 'Afiliaci�n',
					width				: 250,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',
					value				: '9',
					store				: storeAfiliacion,
					listeners: {
						select: {
							fn: function(combo) {
								var valor  = combo.getValue();
								creditoElec(valor);		
							}
						}
					}
					
				}]
			}
	]};
	
	var afiliacionPanel = {
		xtype		: 'panel',
		id 		: 'afiliacionPanel',
		items		: [
			{
				layout		: 'form',
				labelWidth	: 150,
				bodyStyle: 	'padding: 2px; padding-right:20px;padding-left:2px;',
				items		:	
				[
					{
						xtype				: 'combo',
						id          	: '_cmb_persona',
						hiddenName 		: 'cmb_persona',
						fieldLabel  	: 'Persona ',
						allowBlank		: false,
						triggerAction	: 'all',
						mode				: 'local',
						width				: 150,
						emptyText		: 'Seleccionar...',
						valueField		: 'clave',
						displayField	: 'descripcion',
						autoWidth		: true,
						value				: '1',
						store				: storePersona,
						listeners :{
							select: function(combo, record, index) {
								if(combo.getValue()=="5"){
									Ext.getCmp('prueba2').show();
									Ext.getCmp('prueba').hide();
								}else{
									Ext.getCmp('prueba2').hide();
									Ext.getCmp('_razon_social_moral').setValue("nada");
									Ext.getCmp('_rfc_moral').setValue("nada");
									Ext.getCmp('prueba').show();
								}
								accionConsulta("LIMPIAR", null);
							}
						}
					},{
						xtype				: 'combo',
						id          	: '_cmb_cadenas',
						hiddenName 		: 'cmb_cadenas',
						fieldLabel  	: '* Cadenas Productivas ',
						allowBlank		: false,
						msgTarget: 'side',
						triggerAction	: 'all',
						mode				: 'local',
						width				: 420,
						emptyText		: 'Seleccionar...',
						valueField		: 'clave',
						displayField	: 'descripcion',
						autoWidth		: true,
						store				: catalogoCadenasData
					}
			]}
	]};
	
	/*--____ Persona Moral  ____--*/
	
	var afiliacionMoralPanel = {
		xtype		: 'panel',
		id 		: 'afiliacionMoralPanel',
		items		: [
			{
				layout		: 'form',
				labelWidth	: 150,
				bodyStyle: 	'padding: 2px; padding-right:20px;padding-left:2px;',
				items		:	
				[	
					{
						xtype			: 'textfield',
						name			: 'razon_social_moral',
						id				: '_razon_social_moral',
						fieldLabel	: '* Raz�n Social',
						maxLength	: 100,
						width			: 350,
						hidden		: false,
						msgTarget: 'side',
						allowBlank	: false,
						tabIndex		:	1
					},
					{
						xtype			: 'textfield',
						name			: 'rfc',
						id				: '_rfc_moral',
						fieldLabel	: '* RFC',
						maxLength	: 15,
						width			: 250,
						hidden		: false,
						allowBlank	: false,
						msgTarget: 'side',
						regex			:/^([A-Z|a-z|&amp;]{3})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
										regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
										'en el formato NNN-AAMMDD-XXX donde:<br>'+
										'NNN:son las iniciales del nombre de la empresa<br>'+
										'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
										'XXX:es la homoclave',
						listeners:{change:function(field, newValue){  field.setValue(newValue.toUpperCase()); 	}}
					}
			]}
	]};
	
	/*--____ Fin Persona Moral  ____--*/
	
	/*--____ Datos Completos ____--*/
	
	var datos_izq = {
		xtype			: 'fieldset',
		id 			: 'datos_izq',
		border		: false,
		labelWidth	: 150,
		width			: 410,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno',
				name			: 'paterno',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				margins		: '0 20 0 0',
				width			: 220,
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype			: 'textfield',
				id          : 'id_nombre',
				name			: 'nombre',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			}
		]
	};
		
	var datos_der = {
		xtype			: 'fieldset',
		id 			: 'datos_der',
		border		: false,
		width			: 440,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno',
				name			: 'materno',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype			: 'textfield',
				id          : '_rfc_1',
				name			: 'rfc',
				fieldLabel  : '* RFC',
				maxLength	: 15,
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side',
				regex			:/^([A-Z|a-z|&amp;]{4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
										regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
										'en el formato NNNN-AAMMDD-XXX donde:<br>'+
										'NNN:son las iniciales del nombre y apellido<br>'+
										'AAMMDD: es la fecha de nacimiento menor al dia de hoy<br>'+
										'XXX:es la homoclave',
				listeners:{change:function(field, newValue){  field.setValue(newValue.toUpperCase()); 	}}
			}
		]
	};
	
	/*--____Fin Datos Completos ____--*/
	
	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		width			: 410,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : '_calle',
				name			: 'calle',
				fieldLabel  : '* Calle, No. Exterior y No. Interior',
				maxLength	: 40,
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'

			},
			{
				xtype				: 'combo',
				id          	: '_cmb_pais',
				hiddenName 		: 'cmb_pais',
				fieldLabel  	: '* Pa�s',
				allowBlank		: false,
				msgTarget: 'side',
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccionar...',
				width				: 150,
				store				: catalogoPaisData,
				listeners: {
					
				}	
			},
			{
				xtype				: 'combo',
				id          	: '_cmb_edo',
				hiddenName 		: 'cmb_edo',
				fieldLabel  	: '* Estado',
				allowBlank		: false,
				msgTarget: 'side',
				triggerAction	: 'all',
				emptyText		: 'Seleccionar...',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 220,
				store				: catalogoEstadoData,
				listeners: {
					select: function(combo, record, index) {
						 
						 Ext.getCmp('_cmb_del_mun').reset();
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave
							})
						});
					}
				}
			},
			/*{
				xtype			: 'numberfield',
				id          : '_cod_post',
				name			: 'cod_post',
				fieldLabel  : '* C�digo Postal',
				maxLength	: 5,
				width			: 90,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			}*/
                        {
				xtype			: 'textfield',
				id          : '_cod_post',
				name			: 'cod_post',
				fieldLabel  : '* C�digo Postal',
				maxLength	: 5,
                                minLength:		5,
				width			: 90,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side',
                                regex       : /^[0-9]*$/,
                                regexText   : 'Solo de contener n�meros'
			},
			{
				xtype			: 'textfield',
				id          : '_telefono',
				name			: 'telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				msgTarget: 'side',
				width			: 220,
				maskRe:		/[0-9]/,
				margins		: '0 20 0 0',
				maxLength	: 8
				
			},
			{
				xtype			: 'textfield',
				id          : '_fax',
				name			: 'fax',
				fieldLabel  : 'Fax',
				maskRe:		/[0-9]/,
				width			: 220,
				maxLength	: 20,
				msgTarget: 'side',
				margins		: '0 20 0 0',
				allowBlank	: true
			}
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		//title		: 'derecho',
		width			: 440,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : '_colonia',
				name			: 'colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 40,
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype				: 'combo',
				id          	: '_cmb_del_mun',
				hiddenName 		: 'cmb_del_mun',
				fieldLabel  	: '* Delegaci�n o municipio ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				//typeAhead		: true,
				allowBlank		: false,
				msgTarget: 'side',
				emptyText		: 'Seleccionar...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 220,
				store				: catalogoMunicipio
				
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_email',
				name			: 'email',
				fieldLabel: ' E-mail',
				allowBlank	: true,
				maxLength	: 100,
				width			: 220,
				margins		: '0 20 0 0',
				vtype: 'email'
			}
		]
	};
	
	/*--____Fin Domicilio ____--*/
	
/*--____ Datos del Contacto  ____--*/
	
	var datosContacto_izq = {
		xtype			: 'fieldset',
		id 			: 'datosContacto_izq',
		border		: false,
		width			: 410,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_contacto',
				name			: 'paterno_contacto',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				margins		: '0 20 0 0',
				width			: 220,
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype			: 'textfield',
				id          : 'id_nombre_contacto',
				name			: 'nombre_contacto',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},
			
			{
				xtype			: 'textfield',
				id          : '_faxc',
				name			: 'faxc',
				fieldLabel  : 'Fax',
				maxLength	: 20,
				width			: 220,
				maskRe:		/[0-9]/,
				margins		: '0 20 0 0',
				allowBlank	: true,
				msgTarget: 'side'
			},{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_emailc',
				name			: 'emailc',
				fieldLabel: '* E-mail',
				maxLength	: 50,
				width			: 220,
				margins		: '0 20 0 0',
				vtype			: 'email',
				allowBlank	: false,
				msgTarget: 'side'
			}
		]
	};
		
	var datosContacto_der = {
		xtype			: 'fieldset',
		id 			: 'datosContacto_der',
		border		: false,
		width			: 440,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_contacto',
				name			: 'materno_contacto',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				width			: 220,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},
			{
				xtype			: 'textfield',
				id          : '_telc',
				name			: 'telc',
				fieldLabel  : '*Tel�fono',
				maxLength	: 8,
				width			: 220,
				maskRe:		/[0-9]/,
				margins		: '0 20 0 0',
				allowBlank	: false,
				msgTarget: 'side'
			},{
				xtype: 'compositefield',
				id:'_celular_cont',
				combineErrors: false,
				msgTarget: 'side',
				fieldLabel: '* Celular (Clave Lada)',
				items: [
					{
						xtype: 'displayfield',
						value: '044',
						width: 22
					},{
						xtype:			'textfield',
						name:				'txt_cel_cont',
						id:				'_txt_cel_cont',
						width:			193,
						allowBlank:		false,
						msgTarget: 'side',
						maskRe:		/[0-9]/,
						maxLength:		10
					}
				]
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_emailc_conf',
				name			: 'emailc_conf',
				fieldLabel: ' * Confirmaci�n E-mail',
				maxLength	: 50,
				width			: 220,
				margins		: '0 20 0 0',
				vtype			: 'email',
				allowBlank	: false,
				msgTarget: 'side'
			}
		]
	};
	
	/*--____Fin Datos del Contacto  ____--*/
	
	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			{
				layout	: 'hbox',
				title		: '',
				width		: 940,
				items		: [afiliacion ]
			},
			{
				layout	: 'hbox',
				title		: '',
				//height	: 280,
				width		: 940,
				items		: [afiliacionPanel ]
			},
			{
				layout	: 'hbox',
				title		: '',
				id			: 'prueba2',
				hidden	: true,
				width		: 940,
				items		: [afiliacionMoralPanel ]
			},
			{
				layout	: 'hbox',
				id			: 'prueba',
				title 	: 'Nombre completo',
				padding	:	'10 0 0 0',
				width		: 940,
				items		: [datos_izq	, datos_der	]
			},
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				padding	:	'10 0 0 0',
				width		: 940,
				items		: [domicilioPanel_izq, domicilioPanel_der ]
			},
			{
				layout	: 'hbox',
				title 	: 'Datos del contacto',
				padding	:	'10 0 0 0',
				width		: 940,
				items		: [datosContacto_izq, datosContacto_der ]
			}
		],
		monitorValid	: true,
		buttons			: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				//formBind: true,
				handler: function(boton, evento) {
                if(Ext.getCmp('id_email').isValid()){
								accionConsulta("CONSULTAR", null);
                } else {
                 Ext.getCmp('id_email').focus();
                }
				}
			},{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls:'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR", null);
				}
			
			},
			{
				text:'Cancelar',
				iconCls: 'icoCancelar',
				handler: function() {
					window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
				}
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items: //fp
		[ fpDatos ]
	});
	
	Ext.StoreMgr.key('catalogoCadenasDataStore').load();
	Ext.StoreMgr.key('catalogoPaisDataStore').load();
	Ext.StoreMgr.key('catalogoEstadoDataStore').load();
	
});