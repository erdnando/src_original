Ext.onReady(function () {

	var strPerfil = Ext.getDom('strPerfil').value;	
	
	Ext.QuickTips.init();
	
	//------------------------------------HANDLERS-------------------------------------------
	var procesarBusquedaAvanzada = function (store, arrRegistros, opts) {			
		var jsonData = store.reader.jsonData;
        if (jsonData.success){
            if (jsonData.total === "excede"){
                Ext.Msg.alert('Mensaje informativo', 'Demasiados registros encontrados, favor de ser m�s espec�fico en la b�squeda.');				
				return;
            }else if (jsonData.total === "0"){
                Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');				
				return;
            }
            Ext.getCmp('clavePymeSelecionada').focus();
        }		
	};


	var procesarConsultaData = function (store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridGeneral = Ext.getCmp('gridGeneral');
		var el = gridGeneral.getGridEl();
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
				gridGeneral.show();
			}
			var jsonData = store.reader.jsonData;
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarXpaginaPDF = Ext.getCmp('btnBajarXpaginaPDF');
			var btnGenerarXpaginaPDF = Ext.getCmp('btnGenerarXpaginaPDF');

			if (jsonData.mensaje != '') {
				Ext.Msg.alert('Mensaje', jsonData.mensaje);
			}

			if (store.getTotalCount() > 0) {
				btnGenerarXpaginaPDF.enable();
				btnBajarXpaginaPDF.hide();
				btnBajarArchivo.hide();
				if (!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarXpaginaPDF.disable();
				btnBajarXpaginaPDF.hide();
				btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin', '');
			var params = { nombreArchivo: archivo };
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton = Ext.getCmp('btnGenerarArchivo');
			boton.enable();
			var boton2 = Ext.getCmp('btnGenerarXpaginaPDF');
			boton2.enable();
		} else {
			NE.util.mostrarConnError(response, opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', { duration: 5, easing: 'bounceOut' });
			btnBajarArchivo.setHandler(function (boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response, opts);
		}
	}
	var procesarSuccessFailureGenerarXpaginaPDF = function (opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarXpaginaPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarXpaginaPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing: 'bounceOut' });
			btnBajarPDF.setHandler(function (boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response, opts);
		}
	}

	var returnInit = function (opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var fp = Ext.getCmp('forma');
			fp.el.unmask();

			if (Ext.util.JSON.decode(response.responseText).cvePerf != '') {
				var jsonObj = Ext.util.JSON.decode(response.responseText);
				var cveAux = jsonObj.cvePerf;
				cvePerf = cveAux;
				Ext.getCmp('cvePerf').setValue(cvePerf);
				if (cvePerf == '4') {
					catalogoNombreEpo.load();
					Ext.getCmp('NombreEpo').show();
				}
			}
		} else {
			//NE.util.mostrarConnError(response,opts);
		}
	}


	var procesarCatalogoNombreEpo = function (store, arrRegistros, opts) {
		store.insert(0, new Todas({
			clave: "",
			descripcion: "Seleccionar Todos",
			loadMsg: ""
		}));
		store.commitChanges();

		Ext.getCmp('NombreEpo').setValue("");
	}

	function restaFechas(f1, f2){
		var aFEcha1 = f1.split('/');
		var aFEcha2 = f2.split('/');
		var fFecha1 = Date.UTC(aFEcha1[2], aFEcha1[1]-1, aFEcha1[0]);
		var fFecha2 = Date.UTC(aFEcha2[2], aFEcha2[1]-1, aFEcha2[0]);
		var dif = fFecha2 - fFecha1;
		var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
		return dias;
	}
	

	//--------------------------------------STORES----------------------------------

	Todas = Ext.data.Record.create([
		{ name: "clave", type: "string" },
		{ name: "descripcion", type: "string" },
		{ name: "loadMsg", type: "string" }
	]);

	var storeBusquedaAvanzada = new Ext.data.JsonStore({
		id: 'catalogoProveedoresDataStore',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15conscliext.data.jsp',
		baseParams: {
			informacion: 'CatalogoProveedor'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			load: procesarBusquedaAvanzada,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15conscliext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{ name: 'NO_PROVEEDOR' },
			{ name: 'NUMERO_NAFIN_ELECTRONICO' },
			{ name: 'NUMERO_SIRAC' },
			{ name: 'ESTATUS_PYME' },
			{ name: 'RAZON_SOCIAL' },
			{ name: 'DOMICILIO_COLONIA' },
			{ name: 'ESTADO' },
			{ name: 'TELEFONO' },
			{ name: 'CG_EMAIL' },
			{ name: 'NOMBRE_IF' },
			{ name: 'MONEDA' },
			{ name: 'ESTATUS_IF' }
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function (proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);//Llama procesar  consulta, para que desbloquee los componentes.
				}
			}
		}
	});

	var catalogoIFdata = new Ext.data.JsonStore({
		id: 'catalogoIFdataStore',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		autoDestroy: true,
		url: '15conscliext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty: 'total',
		autoLoad: true,
		listeners: {
			exception: {
				fn: function (proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});

	var catalogoNombreEpo = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root: 'registros',
		fields: ['clave', 'descripcion'],
		url: '15conscliext.data.jsp',
		listeners: {
			load: procesarCatalogoNombreEpo,
			exception: {
				fn: function (proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'CatalogoEPO'

		},
		totalProperty: 'total',
		autoLoad: false

	});
	
	    var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
        var jsonData = store.reader.jsonData;
		
		
        if (jsonData.success){
            if (jsonData.total.trim() === "excede"){
				Ext.MessageBox.alert('Error', 'Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');                
				
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
            }else if (jsonData.total === "0"){
                Ext.Msg.alert('Mensaje informativo','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
			return;
            }
            //Ext.getCmp('cmb_num_ne').focus();
        }
    } ;   
	
	var catalogoNombreProvData = new Ext.data.JsonStore({
        id: 'catalogoNombreStore',
        root : 'registros',
	fields : ['clave', 'descripcion', 'loadMsg'],
	url: '15conscliext.data.jsp',
	baseParams: {
            informacion: 'CatalogoProveedor'
	},
	totalProperty : 'total',
	autoLoad: false,
	listeners: {
            load: procesarCatalogoNombreProvData,
            exception: NE.util.mostrarDataProxyError,
            beforeload: NE.util.initMensajeCargaCombo
	}
    });

	//---------------------------------COMPONENTES----------------------------------
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{ header: 'DATOS DEL PROVEEDOR', colspan: 9, align: 'center' },
				{ header: 'DATOS INTERMEDIARIO FINANCIERO', colspan: 3, align: 'center' }
			]
		]
	});
	var elementosBusquedaAvanzada = [
		{
			xtype: 'textfield',
			name: 'nombrePymeIntroducido',
			id: 'txtnombreB',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'//Necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcIntroducido',
			id: 'txtRfc',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			name: 'claveProveedor',
			id: 'txtNoProveedor',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 15,
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'//Necesario para mostrar el icono de error
		},
		{
			xtype: 'panel',
			items: [
						{
							xtype: 'button',
							width: 80,
							height: 10,
							text: 'Buscar',
							iconCls: 'iconoLupa',
							id: 'btnBuscar',
							anchor: '',
							style: 'float:right',
							handler: function(boton,evento){
							
								storeBusquedaAvanzada.load({
										params: Ext.apply(fpBusquedaAvanzada.getForm().getValues())
								});
							}
						}
			]
		},
		NE.util.getEspaciador(20),
		{
			xtype: 'combo',
			name: 'clavePymeSelecionada',
			id: 'cmbProveedor',
			mode: 'local',
			autoLoad: true,
			displayField: 'descripcion',
			emptyText: 'Seleccione proveedor...',
			valueField: 'clave',
			hiddenName: 'proveedor',
			fieldLabel: 'Nombre',
			forceSelection: true,
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: storeBusquedaAvanzada,
			listeners: {
				select: {
					fn: function(combo){
							storeBusquedaAvanzada.load({
								params: Ext.apply(fpBusquedaAvanzada.getForm().getValues(),{
									clavePymeSelecionada: combo.getValue()
								})
							});			
					}
				},
				change: {
					fn: function(combo){
							var btnAceptar = Ext.getCmp('btnAceptar');
							if(combo.getValue()==''){
								btnAceptar.hide();
							}else{
								btnAceptar.show();
							} 
					}
				}
			}
		}
];

	var elementosForma = [
		{ 
			xtype: 'textfield', 
			hidden: true, 
			id: 'cvePerf', 
			value: '' 
		},
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'NombreEpo',
			combineErrors: false,
			hiddenName: 'ic_epo',
			fieldLabel: 'Nombre de la Epo',
			//emptyText: 'Seleccionar EPO',
			store: catalogoNombreEpo,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			typeAhead: true,
			editable: true,
			forceSelection: true,
			//anchor: '96%',
			width: 200,
			triggerAction: 'all',
			margins: '0 20 0 0',
			listeners: {
				select: function (combo) {
					Ext.getCmp('cmbIf').setValue("");
					catalogoIFdata.load({
						params: {
							comboEpo: combo.getValue()
						}
					});
				}
			},
			minChars: 1,
			hidden: true
		},
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'cmbIf',
			allowBlank: true,
			fieldLabel: 'Nombre del IF',
			forceSelection: true,
			editable: true,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_if',
			emptyText: 'Seleccionar IF',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoIFdata,
			tpl: NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero N@E',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					name: 'numeroNae',
					id: 'txtNumeroNae',
					allowBlank: true,
					allowDecimals: false,
					allowNegative: false,
					maxLength: 15,
					msgTarget: 'side',
					margins: '0 20 0 0' //Necesario para mostrar el icono de error
				},
				{
					xtype: 'button',
					id: 'btnBusqueda',
					iconCls:	'icoBuscar',
					text: 'B�squeda avanzada...',					
					handler: function(boton, evento) {                        
                       	
                        var winVen = Ext.getCmp('winBuscaA');
                        if (winVen){
                            Ext.getCmp('fpWinBusca').getForm().reset();
                            Ext.getCmp('fpWinBuscaB').getForm().reset();
                            Ext.getCmp('cmb_num_ne').setValue();
                            Ext.getCmp('cmb_num_ne').store.removeAll();
                            Ext.getCmp('cmb_num_ne').reset();
                            winVen.show();
                        }else{
                            new Ext.Window ({
                                id:'winBuscaA',
                                height: 364,
                                x: 300,
                                y: 100,
                                width: 550,
                                modal: true,
                                closeAction: 'hide',
                                title: 'B�squeda Avanzada',
                                items:[
                                    {
                                        xtype:'form',
                                        id:'fpWinBusca',
                                        frame: true,
                                        border: false,
                                        style: 'margin: 0 auto',
                                        bodyStyle:'padding:10px',
                                        defaults: {
                                            msgTarget: 'side',
                                            anchor: '-20'
                                        },
                                        labelWidth: 130,
                                        items:[                                            
                                            {
                                                xtype:'displayfield',
                                                frame:true,
                                                border: false,
                                                value:'Utilice el * para b�squeda gen�rica'
                                            },
                                            {
                                                xtype:'displayfield',
                                                frame:true,
                                                border: false,
                                                value:''
                                            },
                                            {
                                                xtype:'hidden',
                                                id:	'hid_ic_pyme',
                                                value:''
                                            },
                                            {
                                                xtype:'hidden',
                                                id:	'hid_ic_banco',
                                                value:''
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'nombre_pyme',
                                                id:	'txtNombre',
                                                fieldLabel:'Nombre',
                                                maxLength:	100
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'rfc_prov',
                                                id:	'_rfc_prov',
                                                fieldLabel:'RFC',
                                                maxLength:	20
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'num_pyme',
                                                id:	'txtNe',
                                                fieldLabel:'N�mero de Proveedor',
                                                maxLength:	25
                                            }
                                        ],
                                        buttonAlign:'center',
                                        buttons:[
                                            {
                                                text:'Buscar',
                                                iconCls:'icoBuscar',
                                                handler: function(boton) {            												
                                                    catalogoNombreProvData.load({ 
                                                        params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) 
                                                    });
                                                }
                                            },
                                            {
                                                text:'Cancelar',
                                                iconCls: 'icoRechazar',
                                                handler: function() {
                                                    Ext.getCmp('winBuscaA').hide();
                                                    Ext.getCmp('winBuscaA').destroy();
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype:'form',
                                        frame: true,
                                        id:'fpWinBuscaB',
                                        style: 'margin: 0 auto',
                                        bodyStyle:'padding:10px',
                                        monitorValid: true,
                                        defaults: {
                                            msgTarget: 'side',
                                            anchor: '-20'
                                        },
                                        labelWidth: 80,
                                        items:[
                                            {
                                                xtype: 'combo',
                                                id:	'cmb_num_ne',
                                                name: 'ic_pyme',
                                                hiddenName : 'ic_pyme',
                                                fieldLabel: 'Nombre',
                                                emptyText: 'Seleccione. . .',
                                                displayField: 'descripcion',
                                                valueField: 'clave',
                                                triggerAction : 'all',
                                                forceSelection:true,
                                                allowBlank: false,
                                                typeAhead: true,
                                                mode: 'local',
                                                minChars : 1,
                                                store: catalogoNombreProvData,
                                                tpl : NE.util.templateMensajeCargaCombo
                                            }
                                        ],
                                        buttonAlign:'center',
                                        buttons:[
                                            {
                                                text:'Aceptar',
                                                iconCls:'aceptar',
                                                formBind:true,
                                                handler: function() {
                                                    if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
                                                        var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
                                                        var cveP = disp.substr(0,disp.indexOf(" "));                                                        
                                                        Ext.getCmp('txtNumeroNae').setValue(cveP);                                                        
                                                        Ext.getCmp('winBuscaA').hide();
                                                    }
                                                }
                                            },
                                            {
                                                text:'Cancelar',
                                                iconCls: 'icoRechazar',
                                                handler: function() {	
                                                    Ext.getCmp('winBuscaA').hide();	
                                                    Ext.getCmp('winBuscaA').destroy();
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }).show();
                        }                        
                    }
				}
			]
		},
		{
			xtype: 'textfield',
			fieldLabel: 'No. Proveedor',
			name: 'numProveedor',
			id: 'txtNumProveedor',
			anchor: '%34',
			allowBlank: true,
			msgTarget: 'side'
		},		
		{
			xtype: 'checkbox',
			boxLabel: 'Pymes sin n�mero de Proveedor',
			name: 'pymesSN',
			id: 'chkPymesSN',
			inputValue: 'S',
			msgTarget: 'side'
		},
		{
			xtype: 'checkbox',
			boxLabel: 'Pymes por Habilitar',
			name: 'pymesHab',
			id: 'chkPymesHab',
			inputValue: 'S',
			msgTarget: 'side'
		},
		{
			xtype: 'checkbox',
			boxLabel: 'Pymes Habilitadas',
			name: 'chkPymesH',
			id: 'chkPymesH',
			inputValue: 'S',
			msgTarget: 'side',
			enable: true,
			listeners:{
				check: function(field){
					var check = (Ext.getCmp('chkPymesH')).getValue();					
					if(check){
						Ext.getCmp('fecha_sol_Min').enable();
						Ext.getCmp('fecha_sol_Max').setDisabled(false);
					}else{
						Ext.getCmp('fecha_sol_Min').disable();
						Ext.getCmp('fecha_sol_Max').setDisabled(true);
						Ext.getCmp('fecha_sol_Min').setValue(''); 
						Ext.getCmp('fecha_sol_Max').setValue('');
					}
				}
			}
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de habilitaci�n IF',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_sol_de',
					id: 'fecha_sol_Min',
					allowBlank: true,
					startDay: 0,
					width: 110,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecha: 'fecha_sol_Max',
					
					margins: '0 20 0 0' //Necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 24
				},
				{
					xtype: 'datefield',
					name: 'fecha_sol_a',
					id: 'fecha_sol_Max',
					allowBlank: true,
					startDay: 1,
					width: 110,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_sol_Min',
					
					margins: '0 20 0 0'
				}
			]
		}
	];
	var grid = new Ext.grid.GridPanel({
		id: 'gridGeneral',
		store: consultaData,
		plugins: grupos,
		style: 'margin:0 auto;',
		hidden: true,
		columns: [
			{
				header: 'No. Proveedor',
				tooltip: 'No. Proveedor',
				sortable: true,
				dataIndex: 'NO_PROVEEDOR',
				align: 'center',
				width: 150
			},
			{
				header: 'No. Nafin electr�nico',
				tooltip: 'No. Nafin electr�nico',
				sortable: true,
				align: 'center',
				dataIndex: 'NUMERO_NAFIN_ELECTRONICO',
				width: 150
			},
			{
				header: 'No. SIRAC',
				tooltip: 'No. SIRAC',
				sortable: true,
				align: 'center',
				dataIndex: 'NUMERO_SIRAC',
				width: 150
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				sortable: true,
				align: 'center',
				dataIndex: 'ESTATUS_PYME',
				width: 150
			},
			{
				header: 'Nombre o raz�n social',
				tooltip: 'Nombre o raz�n social',
				sortable: true,
				dataIndex: 'RAZON_SOCIAL',
				width: 250
			},
			{
				header: 'Domicilio',
				tooltip: 'Domicilio',
				sortable: true,
				dataIndex: 'DOMICILIO_COLONIA',
				width: 300
			},
			{
				header: 'Estado',
				tooltip: 'Estado',
				sortable: true,
				dataIndex: 'ESTADO',
				width: 150
			},
			{
				header: 'Tel�fono',
				tooltip: 'Tel�fono',
				sortable: true,
				dataIndex: 'TELEFONO',
				width: 150
			},
			{
				header: 'Correo Electr�nico',
				tooltip: 'Correo Electr�nico',
				sortable: true,
				dataIndex: 'CG_EMAIL',
				width: 250
			},
			{
				header: 'Nombre',
				tooltip: 'Nombre',
				sortable: true,
				dataIndex: 'NOMBRE_IF',
				width: 250
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				sortable: true,
				dataIndex: 'MONEDA',
				width: 200
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				sortable: true,
				dataIndex: 'ESTATUS_IF',
				align: 'center',
				width: 150
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,		
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function (boton, evento) {
						boton.disable();
						//boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15conscliext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(), {
								informacion: 'ArchivoCSV'
							}),
							//callback: procesarSuccessFailureGenerarArchivo
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar x P�gina PDF',
					id: 'btnGenerarXpaginaPDF',
					handler: function (boton, evento) {
						boton.disable();
						//boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '15conscliext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(), {
								informacion: 'ArchivoXpaginaPDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							//callback: procesarSuccessFailureGenerarXpaginaPDF
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar x P�gina PDF',
					id: 'btnBajarXpaginaPDF',
					hidden: true
				},
				'-'
			]
		}
	});
	var busquedaAvanzada = ["Utilice el * para b�squeda gen�rica"];
	//Panel para mostrar el texto
	var fpB = {
		xtype: 'panel',
		id: 'forma1',
		width: 600,
		frame: true,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		defaultType: 'textfield',
		align: 'center',
		html: busquedaAvanzada.join('')
	}
	var fpBusquedaAvanzada = new Ext.form.FormPanel({
		id: 'formBusquedaAvanzada',
		width: 400,
		title: 'Busqueda Avanzada',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			fpB,
			NE.util.getEspaciador(10),
			elementosBusquedaAvanzada
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoBuscar',
				
				align: 'center',
				handler: function (boton, evento) {
					var txtNumeroNae = Ext.getCmp('txtNumeroNae');
					var cmbProveedor = Ext.getCmp('cmbProveedor');					
					var ventana = Ext.getCmp('winBusqAvan');
					if (Ext.isEmpty(cmbProveedor.getValue())) {
						cmbProveedor.markInvalid('Seleccione el Proveedor');
						return;
					}
					var record = cmbProveedor.findRecord(cmbProveedor.valueField, cmbProveedor.getValue());
					record = record ? record.get(cmbProveedor.displayField) : cmbProveedor.valueNotFoundText;
					var a = new Array();
					a = record.split(" ", 1);
					var nafinElectronico = a[0];
					txtNumeroNae.setValue(nafinElectronico);
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function () {
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusquedaAvanzada.getForm().reset();
					ventana.hide();
				}
			}
		]
	});
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 820,
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto',
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfield',
		defaults: {
			//msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function (boton, evento) {		
				
					var fecha_sol_Min = Ext.getCmp('fecha_sol_Min');
					var fecha_sol_Max = Ext.getCmp('fecha_sol_Max');					
					
					var Desde = Ext.util.Format.date(fecha_sol_Min.getValue(), 'd/m/Y');
					var Hasta = Ext.util.Format.date(fecha_sol_Max.getValue(), 'd/m/Y');									
					var dias = restaFechas(Desde, Hasta);	
						
					if(strPerfil!=='ADMIN EPO'){
									
					
					if(Ext.getCmp('txtNumProveedor').getValue() == '' && Ext.getCmp('txtNumeroNae').getValue() =='' && Ext.getCmp('cmbIf').getValue() == '' && !Ext.getCmp('chkPymesSN').getValue() && !Ext.getCmp('chkPymesHab').getValue() && !Ext.getCmp('chkPymesH').getValue() && Ext.getCmp('fecha_sol_Min').getValue() == '' && Ext.getCmp('fecha_sol_Max').getValue() == '' && Ext.getCmp('NombreEpo').getValue() == ''){
						Ext.MessageBox.alert('Error', 'Favor de seleccionar al menos un criterio de b�squeda');
						fp.el.unmask();
						return;
					}					
					}
					if(Ext.getCmp('chkPymesH').getValue()){
						if(Ext.getCmp('fecha_sol_Max').getValue() == '' || Ext.getCmp('fecha_sol_Min').getValue() == '' ){
							Ext.MessageBox.alert('Error', 'Debe seleccionar una fecha, esta debe ser de un rango de un a�o m�ximo');
							fp.el.unmask();
							return;
						}						
					}
					
					if(dias >= 365){
						Ext.MessageBox.alert('Error', 'El rango de fechas no puede ser mayor a 365 d�as, favor de revisar');
						fp.el.unmask();						
						return;
					}
					
					fp.el.mask('Enviando...', 'x-msk-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(), {
							operacion: 'Generar',//Generar datos para consulta
							start: 0,
							limit: 15
						})
					});
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function () {
					if (Ext.getCmp('cvePerf').getValue() == 4) {
						window.location = '15conscliNaExt.jsp';
					} else {
						window.location = '15conscliext.jsp';
					}
				}
			}
		]
	});
	//-----------------------------------PRINCIPAL----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	});


	fp.el.mask('Cargando Pantalla...', 'x-msk-loading');
	Ext.Ajax.request({
		url: '15conscliext.data.jsp',
		// method: 'POST',  
		callback: returnInit,
		params: {
			informacion: 'initPage'
		}
	});

	Ext.getCmp('fecha_sol_Min').disable();
	Ext.getCmp('fecha_sol_Max').disable();
	catalogoIFdata.load();
	
});
