Ext.onReady(function(){


//--------------------------------HANDLERS-----------------------------------
	var seleccionCombo = function(combo){
		catalogoEpoGrupo.load({
			params:{
				informacion: 'catalogoEpoGrupo',
				claveGrupo: combo.getValue()
			}
		});
		catalogoEPO.load({
			params:{
				informacion: 'consultaEPO',
				claveGrupo: combo.getValue()
			}
		});
	}
	
	var procesarSuccessFailureConfirmar = function(opts, success, response){
		var resp = Ext.util.JSON.decode(response.responseText);
		var fechaActual = Ext.getCmp('fecha_actual').getRawValue();
		var lblFecha = Ext.getCmp('lblFecha');
		var lblUsuario = Ext.getCmp('lblUsuario');
		var lblGrupo = Ext.getCmp('lblGrupo');
		var lblTotal = Ext.getCmp('lblTotalEpos');
		
		var txtAreaEpos = Ext.getCmp('areaEposRelacionadas');
		if(success == true){
			lblFecha.setValue(fechaActual);
			lblUsuario.setValue(resp.usuario);
			lblGrupo.setValue(resp.grupo);
			lblTotal.setValue('Total EPO�s: ' + resp.total);
			var eposAux = '';
			var epos = resp.registros;
			for(i = 0; i<epos.length; i++){
				eposAux += epos[i]+'\n';
			}
			txtAreaEpos.setValue(eposAux);
			fp.hide();
			fpBotones.hide();
			fpParametrizacion.show();
		}	
	}
	
	var procesarCatalogoEpo = function(store, arrRegistros, opts){
		var jsonData = store.reader.jsonData;
		if(jsonData.success){
			Ext.getCmp('displayExistentes').setValue('Total EPO�s:  ' + store.getTotalCount());
		}
	}
	
	var procesarCatalogoEpoGrupo = function(store, arrRegistros, opts){
		var jsonData = store.reader.jsonData;
		if(jsonData.success){
			Ext.getCmp('displayAdicionadas').setValue('Total EPO�s:  ' + store.getTotalCount());
		}
	}

//---------------------------------STORES------------------------------------
	//STORE PARA COMBO GRUPO EPO
	var catalogoGrupoEPO = new Ext.data.JsonStore({
		id: 'catalogoGrupoEPO',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15GrupoOperacion01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGrupoEPO'
		},
		totalProperty: 'Total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	//STORE PARA MULTISELECT IZQUIERDO
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15GrupoOperacion01ext.data.jsp',
		baseParams: {
			informacion: 'consultaEPO'
		},
		totalProperty: 'Total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoEpo,
			exception: NE.util.mostrarDataProxyError
		//	beforeload: NE.util.initMensajeCargaCombo
		}
	});
	//STORE PARA MULTISELECT DERECHO
	var catalogoEpoGrupo = new Ext.data.JsonStore({
		id: 'storeGrupo',
		root:	'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15GrupoOperacion01ext.data.jsp',
		baseParams:	{	
			informacion:	'catalogoEpoGrupo'
		},
		totalProperty:	'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoEpoGrupo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
				
				

//------------------------------COMPONENTES----------------------------------
	
	var fpBotones = new Ext.Container ({
		layout: 'table',
		id: 'fpBotones',
		width: '200',
		heigth: 'auto',
		style: 'margin: 0 auto',
		align: 'center',
		defaults:{
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
			{
				xtype: 'button',
				text: 'Captura de Grupos',
				id: 'btnCapturaGrupos',
				handler: function(){
					window.location = '15GrupoOperacion01ext.jsp'
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<b>Parametrizaci�n EPO�s</b>',
				id: 'btnParametrizacion',
				handler: function(){
					window.location = '15GruposAfiliParamOpera01ext.jsp'
				}
			}
		]
	});
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'cmbEPO',
			fieldLabel: 'Grupo EPO',
			mode: 'local',
			anchor: '50%',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cmb_epo',
			emptyText: 'Seleccionar EPO',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoGrupoEPO,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select: seleccionCombo
			}
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'totales',
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					width: 300,
					margins: '0 20 0 0',
					value:'EPO�s Existentes'
				},
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					width: '40%',
					margins: '0 20 0 0',
					value:'EPO�s Adicionadas a Grupo'
				}
			]
		},
		{
			xtype: 'itemselector',
			name: '_epoRelacionGrupo',
			id: 'epoRelacionGrupo',
			fieldLabel: '',
			imagePath: '/nafin/00utils/extjs/ux/images/',
			drawUpIcon:false, 
			drawDownIcon:false, 
			drawTopIcon:false, 
			drawBotIcon:false,
			border: false,
			allowBlank: false,
			multiselects: [{
				width: 300,
				height: 250,
				legend: '',
				id: 'epoExistente',
				store: catalogoEPO,
				//disabled: true,
				displayField: 'descripcion',
				valueField: 'clave'
			},
			{
				width: 300,
				height: 250,
				legend: '',
				id: 'epoGrupo',
				store: catalogoEpoGrupo,
				allowBlank: false,
				displayField: 'descripcion',
				valueField: 'clave'
			}]
		},{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'fieldTotales',
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					id: 'displayExistentes',
					width: 300,
					margins: '0 20 0 0',
					value: 'Total EPO�s: '
				},
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					id: 'displayAdicionadas',
					width: '40%',
					margins: '0 20 0 0',
					value: 'Total EPO�s: 0'
				}
			]
		}
	];
	
	var elementosParametrizacion = [
		{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'fieldFecha',
			msgTarget: 'side',
			fieldLabel: 'Fecha',
			items: [
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					id: 'lblFecha',
					margins: '0 20 0 0',
					value: ''
				}
			]
		},{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'fieldUsuario',
			msgTarget: 'side',
			fieldLabel: 'Usuario',
			items: [
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					id: 'lblUsuario',
					margins: '0 20 0 0',
					value: ''
				}
			]
		},{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'fieldGrupo',
			msgTarget: 'side',
			fieldLabel: 'Grupo',
			items: [
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					id: 'lblGrupo',
					margins: '0 20 0 0',
					value: ''
				}
			]
		},{
			xtype: 'textarea',
			id: 'areaEposRelacionadas',
			width: '98%',
			height: 100
		},{
			xtype: 'datefield',
			id: 'fecha_actual',
			startDay: 0,
			value: new Date(),
			hidden: true
		},{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'fieldTotalEpos',
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					id: 'lblTotalEpos',
					margins: '0 20 0 0',
					value: ''
				}
			]
		}
	];
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 740,
		height: 'auto',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 70,
		labelAlign : 'right',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid: true,
		buttons: [		
			{
				text: 'Continuar',
				id: 'btnContinuar',
				iconCls: 'icoAceptar',
				formBind: false,
				handler: function(boton, evento){
					var grupoEpo = Ext.getCmp('cmbEPO');
					var eposSeleccionadas = Ext.getCmp('epoRelacionGrupo');
					if(grupoEpo.getValue() == ''){
						grupoEpo.markInvalid('El valor de el grupo es requerido.');
					}else if(eposSeleccionadas.getValue() == ''){
						eposSeleccionadas.markInvalid('Debe adicionar al menos una EPO al Grupo para continuar');
					}else{
					
						Ext.Ajax.request({
							url: '15GrupoOperacion01ext.data.jsp',
							params: {
								informacion: 'confirmarEpos',
								claveGrupo: grupoEpo.getValue(),
								eposSelect: eposSeleccionadas.getValue()
							},
							callback: procesarSuccessFailureConfirmar
						});
					}
				}
			}
		]
	});
	
	var fpParametrizacion = new Ext.form.FormPanel({
		id: 'formaParam',
		frame: true,
		title: 'LA SIGUIENTE PARAMETRIZACI�N SE REALIZO CON �XITO',
		width: 600,
		height: 280,
		frame: true,
		hidden: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 80,
		labelAlign : 'left',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosParametrizacion,
		buttons: [{
			text: 'Regresar',
			id: 'btnRegresar',
			iconCls: 'icoRegresar',
			formBind: false,
			handler: function(boton, evento){
				location.href = "15GruposAfiliParamOpera01ext.jsp";
			}
		}]
	});

//-------------------------COMPONENTE PRINCIPAL------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 900,
		height: 'auto',
		items:[
			NE.util.getEspaciador(25),
			fpBotones,
			NE.util.getEspaciador(10),
			fp,
			fpParametrizacion
		]
	});
	
	catalogoGrupoEPO.load();
	catalogoEPO.load();

});