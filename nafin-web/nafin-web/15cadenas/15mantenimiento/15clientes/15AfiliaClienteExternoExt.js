Ext.onReady(function(){
	
	var ban = 'S';
	var procesarGuardar = function(options, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  jsonData = Ext.JSON.decode(response.responseText);
			Ext.MessageBox.alert('Mensaje','Su n�mero de Nafin Electr�nico : '+jsonData.nafinElectronico, 
				function(){ 					
					window.location = '15AfiliaClienteExternoExt.jsp';
			}); 
								
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	function procesaClienteSirac(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  info = Ext.JSON.decode(response.responseText); 
			var  forma =   Ext.ComponentQuery.query('#forma')[0];
			var numClienteSirac =	forma.query('#numClienteSirac_1')[0];	
			if(info.ValidaSirac!=''){
				numClienteSirac.markInvalid('El N�mero de Cliente SIRAC ingresado ya existe favor de verificarlo');
				numClienteSirac.focus();
				return;
			}else{
				var  forma =   Ext.ComponentQuery.query('#forma')[0];
				forma.query('#rfcSirac')[0].setValue(info.rfcSirac);
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesaClienteSirac1(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  info = Ext.JSON.decode(response.responseText); 
			var  forma =   Ext.ComponentQuery.query('#forma')[0];
			var numClienteSirac =	forma.query('#numClienteSirac_1')[0];	
			if(info.ValidaSirac!=''){
				numClienteSirac.markInvalid('El N�mero de Cliente SIRAC ingresado ya existe favor de verificarlo');
				numClienteSirac.focus();
				return;
			}else{
				var  forma =   Ext.ComponentQuery.query('#forma')[0];
	
				var cboTipoPersona =	forma.query('#cboTipoPersona_1')[0];
				var appPaterno =	forma.query('#appPaterno_1')[0];
				var appMaterno =	forma.query('#appMaterno_1')[0];
				var nombres =	forma.query('#nombres_1')[0];
				var txtRFC_Fisica =	forma.query('#txtRFC_Fisica_1')[0];
				var txtRazonSocial =	forma.query('#txtRazonSocial_1')[0];
				var txtRFC_Moral =	forma.query('#txtRFC_Moral_1')[0];
				var numClienteSirac =	forma.query('#numClienteSirac_1')[0];
				var txtCalle =	forma.query('#txtCalle_1')[0];
				var txtColonia =	forma.query('#txtColonia_1')[0];
				var cboPais =	forma.query('#cboPais_1')[0];
				var cboEstado =	forma.query('#cboEstado_1')[0];
				var cboMunicipio =	forma.query('#cboMunicipio_1')[0];
				var txtEmail =	forma.query('#txtEmail_1')[0];
				var txtCodigoPostal =	forma.query('#txtCodigoPostal_1')[0];
				var txtTelefono =	forma.query('#txtTelefono_1')[0];
				var txtNumeroCel =	forma.query('#txtNumeroCel_1')[0];
				
				Ext.MessageBox.confirm('Confirmaci�n','�Est� seguro de querer enviar su informaci�n?', function(resp){
				if(resp =='yes'){	
					Ext.Ajax.request({
						url: '15AfiliaClienteExterno.data.jsp',
						params: {							
							informacion: 'Guardar',
							cboTipoPersona: cboTipoPersona.getValue(),
							appPaterno: appPaterno.getValue(),
							appMaterno: appMaterno.getValue(),
							nombres: nombres.getValue(),
							txtRFC_Fisica: txtRFC_Fisica.getValue(),
							txtRazonSocial: txtRazonSocial.getValue(),
							txtRFC_Moral: txtRFC_Moral.getValue(),
							numClienteSirac: numClienteSirac.getValue(),
							txtCalle: txtCalle.getValue(),
							txtColonia: txtColonia.getValue(),
							cboPais: cboPais.getValue(),
							cboEstado: cboEstado.getValue(),
							cboMunicipio: cboMunicipio.getValue(),
							txtEmail: txtEmail.getValue(),
							txtCodigoPostal: txtCodigoPostal.getValue(),
							txtTelefono: txtTelefono.getValue(),
							txtNumeroCel: txtNumeroCel.getValue()
						},
						callback: procesarGuardar
					});			
				}
			});
				
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function procesoAceptar() {
	
		var  forma =   Ext.ComponentQuery.query('#forma')[0];
	
		var cboTipoPersona =	forma.query('#cboTipoPersona_1')[0];
		var appPaterno =	forma.query('#appPaterno_1')[0];
		var appMaterno =	forma.query('#appMaterno_1')[0];
		var nombres =	forma.query('#nombres_1')[0];
		var txtRFC_Fisica =	forma.query('#txtRFC_Fisica_1')[0];
		var txtRazonSocial =	forma.query('#txtRazonSocial_1')[0];
		var txtRFC_Moral =	forma.query('#txtRFC_Moral_1')[0];
		var numClienteSirac =	forma.query('#numClienteSirac_1')[0];
		var txtCalle =	forma.query('#txtCalle_1')[0];
		var txtColonia =	forma.query('#txtColonia_1')[0];
		var cboPais =	forma.query('#cboPais_1')[0];
		var cboEstado =	forma.query('#cboEstado_1')[0];
		var cboMunicipio =	forma.query('#cboMunicipio_1')[0];
		var txtEmail =	forma.query('#txtEmail_1')[0];
		var txtCodigoPostal =	forma.query('#txtCodigoPostal_1')[0];
		var txtTelefono =	forma.query('#txtTelefono_1')[0];
		var txtNumeroCel =	forma.query('#txtNumeroCel_1')[0];
		
		if(cboTipoPersona.getValue()=='F'){
			
			if(Ext.isEmpty( appPaterno.getValue()  )){
				appPaterno.markInvalid('Este campo es obligatorio');
				appPaterno.focus();
				return;
			}			
			if(Ext.isEmpty( appMaterno.getValue()  )){
				appMaterno.markInvalid('Este campo es obligatorio');
				appMaterno.focus();
				return;
			}
			if(Ext.isEmpty( nombres.getValue()  )){
				nombres.markInvalid('Este campo es obligatorio');
				nombres.focus();
				return;
			}
			if(Ext.isEmpty( txtRFC_Fisica.getValue()  )){
				txtRFC_Fisica.markInvalid('Este campo es obligatorio');
				txtRFC_Fisica.focus();
				return;
			}			
		}else  if(cboTipoPersona.getValue()=='M'){
		
			if(Ext.isEmpty( txtRazonSocial.getValue()  )){
				txtRazonSocial.markInvalid('Este campo es obligatorio');
				txtRazonSocial.focus();
				return;
			}	
			if(Ext.isEmpty( txtRFC_Moral.getValue()  )){
				txtRFC_Moral.markInvalid('Este campo es obligatorio');
				txtRFC_Moral.focus();
				return;
			}	
		}
		
		if(Ext.isEmpty( numClienteSirac.getValue()  )){
			numClienteSirac.markInvalid('Este campo es obligatorio');
			numClienteSirac.focus();
			return;
		}	
		
		
		if(Ext.isEmpty( txtCalle.getValue()  )){
			txtCalle.markInvalid('Este campo es obligatorio');
			txtCalle.focus();
			return;
		}	
		
		if(Ext.isEmpty( txtColonia.getValue()  )){
			txtColonia.markInvalid('Este campo es obligatorio');
			txtColonia.focus();
			return;
		}	
		
		if(Ext.isEmpty( cboPais.getValue()  )){
			cboPais.markInvalid('Este campo es obligatorio');
			cboPais.focus();
			return;
		}
		
		if(Ext.isEmpty( cboEstado.getValue()  )){
			cboEstado.markInvalid('Este campo es obligatorio');
			cboEstado.focus();
			return;
		}
		
		if(Ext.isEmpty( cboMunicipio.getValue()  )){
			cboMunicipio.markInvalid('Este campo es obligatorio');
			cboMunicipio.focus();
			return;
		}
		
		if(Ext.isEmpty( txtEmail.getValue()  )){
			txtEmail.markInvalid('Este campo es obligatorio');
			txtEmail.focus();
			return;
		}
		
		if(Ext.isEmpty( txtCodigoPostal.getValue()  )){
			txtCodigoPostal.markInvalid('Este campo es obligatorio');
			txtCodigoPostal.focus();
			return;
		}
		
		if(Ext.isEmpty( txtTelefono.getValue()  )){
			txtTelefono.markInvalid('Este campo es obligatorio');
			txtTelefono.focus();
			return;
		}
		if(Ext.isEmpty( txtNumeroCel.getValue()  )){
			txtNumeroCel.markInvalid('Este campo es obligatorio');
			txtNumeroCel.focus();
			return;
		}	
		
			Ext.Ajax.request({
				url: '15AfiliaClienteExterno.data.jsp',
				params: {							
					informacion: 'ClienteSIRAC',
					numClienteSirac:forma.query('#numClienteSirac_1')[0].getValue()
				},
					callback: procesaClienteSirac1
			});
		
	
	}
	
	
	
	function mostrarAyuda(){
		descripcion = ['<table width="300" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>El RFC correspondiente al n�mero de cliente SIRAC ingresado es '  +Ext.ComponentQuery.query('#rfcSirac')[0].getValue()+						
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
	 new Ext.Window({								
		id:'ventanaAyuda',
		modal: true,
		width: 340,
		height: 100,				
		modal: true,					
		closeAction: 'destroy',
		resizable: true,
		constrain: true,
		closable:true,
		html: descripcion.join('')		
		}).show().setTitle('');
	}
	
	
	
	function verEmail(nomObj){
	
		var  forma =   Ext.ComponentQuery.query('#forma')[0];
		var txtEmail =	forma.query('#txtEmail_1')[0];
		
		if (nomObj.length>0){
			if ( nomObj.indexOf("@")==-1 ){				
				txtEmail.markInvalid('Este campo debe ser una direcci�n de correo o una lista de estas: usuario1@ejemplo.com, usuario2@ejemplo.com');
				txtEmail.focus();
				return;
			}
			if (nomObj.indexOf(".")==-1 ){
				txtEmail.markInvalid('Este campo debe ser una direcci�n de correo o una lista de estas: usuario1@ejemplo.com, usuario2@ejemplo.com');
				txtEmail.focus();
				return;
			}			
		}
	}
	
	function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		} else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} else if (pagina == 2) { 	// Distribuidor							
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp'; 			 
		} 	else if (pagina == 3) { 	//Proveedor
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma01Ext.jsp?TipoPYME='+1; 
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} 	else if (pagina == 6) { 	//Contragarante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 
		} 	else if (pagina == 9) { 	// Mandante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
		} 	else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		} 	else if (pagina == 11) { 	//Afianzadora							//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 
		} 	else if (pagina == 12) { 	//Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliaClienteExternoExt.jsp"; 
		}
	}	
	
	var storeAfiliacion = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],
		data : [
			['0','Cadena Productiva'],
			['1','Intermediario Financiero'],
			['2','Distribuidor'],
			['3','Proveedor']	,
			['4','Afiliados Credito Electr�nico'],
			['5','Cadena Productiva Internacional'],
			['6','Contragarante'],			
			['9','Mandante'],	
			['10','Afianzadora'],
			['11','Universidad-Programa Cr�dito Educativo'],
			['12','Cliente Externo']
		]
	});
	
	var storesCboTipoPersona = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],
		data : [
			[ 'F','F�sica'],
			['M','Moral']				
		]
	});
	 
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
	
	var catalogoPais = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15AfiliaClienteExterno.data.jsp', 
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoPais'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var catalogoMunicipio = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15AfiliaClienteExterno.data.jsp', 
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoMunicipio'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var catalogoEstado = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15AfiliaClienteExterno.data.jsp', 
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoEstado' 
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
		
	
	var  elementosForma =  [
		{
			xtype: 'combo',
			fieldLabel: 'Afiliaci�n',
			itemId: 'cmbTipoAfiliado_1',
			name: 'cmbTipoAfiliado',
			hiddenName: 'cmbTipoAfiliado',
			forceSelection: true,
			hidden: false, 
			width: 450,
			value	: '12',
			store: storeAfiliacion,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',						
			listeners: {
				select: function(obj, newVal, oldVal){
					
					var  fp =   Ext.ComponentQuery.query('#forma')[0];
						
					var valor = fp.query('#cmbTipoAfiliado_1')[0].getValue();						
					creditoElec(valor); 
				}
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'Persona',
			itemId: 'cboTipoPersona_1',
			name: 'cboTipoPersona',
			hiddenName: 'cboTipoPersona',
			forceSelection: true,
			hidden: false, 
			width: 200,
			value	: 'F',
			store: storesCboTipoPersona,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',						
			listeners: {
				select: function(obj, newVal, oldVal){	
					var  fp =   Ext.ComponentQuery.query('#forma')[0];
					var valor = fp.query('#cboTipoPersona_1')[0].getValue();	
					if(valor=='F')  {
						fp.query('#contenedor3')[0].hide();
						fp.query('#contenedor4')[0].hide();
						fp.query('#contenedor1')[0].show();
						fp.query('#contenedor2')[0].show(); 
					}else  if(valor=='M')  {
						fp.query('#contenedor1')[0].hide();
						fp.query('#contenedor2')[0].hide(); 
						fp.query('#contenedor3')[0].show();
						fp.query('#contenedor4')[0].show();
					}
					
					
				}
			}
		},
		{
			xtype: 'panel',
			title: 'Datos Generales',
			id:'datos_Generales',
			collapsible: false,
			titleCollapse: false,
			hidden:false,
			bodyStyle: {
				background: '#D9E5F3',
				borderColor: '#D9E5F3',
				borderWidth: '1px'
			}			
		},
		{
        xtype: 'fieldcontainer',        
        labelStyle: 'font-weight:bold;padding:0',
        layout: 'hbox',
		  id:'contenedor1',
		  hidden:false,
        defaultType: 'textfield',
        fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 150
		},
        items: [
		  	{
				xtype: 'textfield',
				fieldLabel: '* Apellido Paterno',
				itemId: 'appPaterno_1',
				name: 'appPaterno',									
				maxLength: 30,
				width: 350
			},
			{
				xtype: 'displayfield',
				value: '',
				width: 50
			},	
			{
				xtype: 'textfield',
				fieldLabel: '* Apellido materno',
				itemId: 'appMaterno_1',
				name: 'appMaterno',					
				width: 350	,			
				maxLength: 30				
			}	
		 
       ]
    },
	{
        xtype: 'fieldcontainer',        
        labelStyle: 'font-weight:bold;padding:0',
        layout: 'hbox',
		   id:'contenedor2',
		  hidden:false,
        defaultType: 'textfield',
        fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 150
		},
      items: [
		  	{
				xtype: 'textfield',
				fieldLabel: '* Nombre(s)',
				itemId: 'nombres_1',
				name: 'nombres',									
				maxLength: 40	,
				width: 350
			},
			{
				xtype: 'displayfield',
				value: '',
				width: 50
			},	
			{
				xtype: 'textfield',
				fieldLabel: '* RFC',
				itemId: 'txtRFC_Fisica_1',
				name: 'txtRFC_Fisica',					
				width: 350	,			
				//maxLength: 20,
				regex				:	/^([A-Z,�,&]{4})-([0-9]{2}[0-1][0-9][0-3][0-9])-[0-9A-Za-z]{3}$/,	//Copiada de internet y modificada por HVC
				regexText		:'Por favor escriba correctamente el RFC en may�sculas  y separado por guiones '+
									'en el formato NNNN-AAMMDD-XXX donde:<br>'+
									'NNNN: son las iniciales del nombre y apellido<br>'+
									'AAMMDD: es la fecha de nacimiento menor al d�a  de hoy<br>'+
									'XXX:es la homoclave',
				listeners	:{
					change	:function(field, newValue){
						field.setValue(newValue.toUpperCase());
						}
				}
			}		 
       ]
    },
	{
        xtype: 'fieldcontainer',        
        labelStyle: 'font-weight:bold;padding:0',
        layout: 'hbox',
		   id:'contenedor3',
		  hidden:true,
        defaultType: 'textfield',
        fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 150
		},
      items: [
		  	{
				xtype: 'textfield',
				fieldLabel: '* Raz�n Social',
				itemId: 'txtRazonSocial_1',
				name: 'txtRazonSocial',									
				maxLength: 100	,
				width: 350
			}					 
       ]
    },	 
	 {
        xtype: 'fieldcontainer',        
        labelStyle: 'font-weight:bold;padding:0',
        layout: 'hbox',
		   id:'contenedor4',
		  hidden:true,
        defaultType: 'textfield',
        fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 150
		},
      items: [
		  	{
				xtype: 'textfield',
				fieldLabel: '* RFC',
				itemId: 'txtRFC_Moral_1',
				name: 'txtRFC_Moral',									
				maxLength: 20	,
				width: 350,
				regex				:	/^([A-Z,�,&]{3})-([0-9]{2}[0-1][0-9][0-3][0-9])-[0-9A-Za-z]{3}$/,	//Copiada de internet y modificada por HVC				
				regexText		:'Por favor escriba correctamente el RFC en may�sculas  y separado por guiones '+
									'en el formato NNN-AAMMDD-XXX donde:<br>'+
									'NNN: son las iniciales del nombre y apellido<br>'+
									'AAMMDD: es la fecha de nacimiento menor al d�a  de hoy<br>'+
									'XXX:es la homoclave',										
				listeners	:{
					change	:function(field, newValue){
						field.setValue(newValue.toUpperCase());
					}
				}
			}					 
       ]
    },	
	{
        xtype: 'fieldcontainer',        
        labelStyle: 'font-weight:bold;padding:0',
        layout: 'hbox',
		   id:'contenedor5',
		  hidden:false,
        defaultType: 'textfield',
        fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 150
		},
      items: [
		  	{
				xtype: 'textfield',
				fieldLabel: '* Numero de Cliente SIRAC',
				itemId: 'numClienteSirac_1',
				regex			:/^[0-9]*$/,
				name: 'numClienteSirac',									
				maxLength: 12	,
				width: 350,
				listeners: {
					blur: {
						fn: function(objTxt) {
							var  forma =   Ext.ComponentQuery.query('#forma')[0];							
							Ext.Ajax.request({
								url: '15AfiliaClienteExterno.data.jsp',
								params: {							
									informacion: 'ClienteSIRAC',
									numClienteSirac:forma.query('#numClienteSirac_1')[0].getValue()
								},
								callback: procesaClienteSirac
							});
							
						}
					}
				}
				
			},
			{
				xtype: 'button',
				id: 'btnAyuda',
				columnWidth: .05,
				autoWidth: true,
				autoHeight: true,
				iconCls: 'icoAyuda',
				handler: function(){	 
					mostrarAyuda();		 		
				}				
			}
       ]
    },
	 
	 //*****************++Domicilio*******************+
	 {
			xtype: 'panel',
			title: 'Domicilio',
			id:'datos_Domicilio',
			hidden:false,		  
			collapsible: false,
			titleCollapse: false,
			bodyStyle: {
				background: '#D9E5F3',
				borderColor: '#D9E5F3',
				borderWidth: '1px'
			}			
		},
		{
        xtype: 'fieldcontainer',        
        labelStyle: 'font-weight:bold;padding:0',
        layout: 'hbox',
        defaultType: 'textfield',
		  id:'contenedor6',
		  hidden:false,
        fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 150
		},
      items: [
		  	{
				xtype: 'textfield',
				fieldLabel: '* Calle, No, Exterior y No Interior',
				itemId: 'txtCalle_1',
				name: 'txtCalle',									
				maxLength: 100	,
				width: 350
			},
			{
				xtype: 'displayfield',
				value: '',
				width: 50
			},	
			{
				xtype: 'textfield',
				fieldLabel: '* Colonia',
				itemId: 'txtColonia_1',
				name: 'txtColonia',					
				width: 350	,			
				maxLength: 100				
			}		 
       ]
    },
	 {
		xtype: 'fieldcontainer',        
      labelStyle: 'font-weight:bold;padding:0',
      layout: 'hbox',
      defaultType: 'textfield',
		 id:'contenedor7',
		  hidden:false,
      fieldDefaults: {
			msgTarget: 'side',
			labelWidth: 150
		},
		items: [
			{
				xtype: 'combo',
				fieldLabel: '* Pa�s',
				itemId: 'cboPais_1',
				name: 'cboPais',
				hiddenName: 'cboPais',
				forceSelection: true,
				hidden: false, 
				width: 350,
				store: catalogoPais,
				emptyText: 'Seleccione...',
				queryMode: 'local',
				allowBlank: true,
				displayField: 'descripcion',
				valueField: 'clave',						
				listeners: {
					select: function(obj, newVal, oldVal){	
					
					var  fp =   Ext.ComponentQuery.query('#forma')[0];
					var cboEstado = fp.query('#cboEstado_1')[0];	
					
						cboEstado.store.load({
							params: {
								cboPais:fp.query('#cboPais_1')[0].getValue()							
							}
						}); 
						
					}
				}
			},
			{
				xtype: 'displayfield',
				value: '',
				width: 50
			},	
			{
				xtype: 'combo',
				fieldLabel: '* Delegaci�n � Municipio ',
				itemId: 'cboMunicipio_1',
				name: 'cboMunicipio',
				hiddenName: 'cboMunicipio',
				forceSelection: true,				
				width: 350,
				store: catalogoMunicipio,
				emptyText: 'Seleccione...',
				queryMode: 'local',
				allowBlank: true,
				displayField: 'descripcion',
				valueField: 'clave',						
				listeners: {
					select: function(obj, newVal, oldVal){	
					
						
					}
				}
			}
		]
	},
	{
		xtype: 'fieldcontainer',        
      labelStyle: 'font-weight:bold;padding:0',
      layout: 'hbox',
		 id:'contenedor8',
		  hidden:false,
      defaultType: 'textfield',
      fieldDefaults: {
			msgTarget: 'side',
			labelWidth: 150
		},
		items: [
			{
				xtype: 'combo',
				fieldLabel: '* Estado',
				itemId: 'cboEstado_1',
				name: 'cboEstado',
				hiddenName: 'cboEstado',
				forceSelection: true,
				hidden: false, 
				width: 350,
				store: catalogoEstado,
				emptyText: 'Seleccione...',
				queryMode: 'local',
				allowBlank: true,
				displayField: 'descripcion',
				valueField: 'clave',						
				listeners: {
					select: function(obj, newVal, oldVal){	
					
						var  fp =   Ext.ComponentQuery.query('#forma')[0];
						var cboMunicipio = fp.query('#cboMunicipio_1')[0];	
					
						cboMunicipio.store.load({
							params: {
								cboPais:fp.query('#cboPais_1')[0].getValue(),
								cboEstado:fp.query('#cboEstado_1')[0].getValue()	
							}
						});						
					}
				}
			},
			{
				xtype: 'displayfield',
				value: '',
				width: 50
			},	
			{
				xtype: 'textfield',
				fieldLabel: '* E-mail Representante Legal',
				itemId: 'txtEmail_1',
				name: 'txtEmail',									
				width: 350,			
				maxLength: 50,
				listeners: {
					blur: {
						fn: function(objTxt) {
							verEmail(objTxt.getValue());
						}
					}
				}
			}
		]
	},
	
	{
		xtype: 'fieldcontainer',        
      labelStyle: 'font-weight:bold;padding:0',
      layout: 'hbox',
      defaultType: 'textfield',
		 id:'contenedor9',
		  hidden:false,
      fieldDefaults: {
			msgTarget: 'side',
			labelWidth: 150
		},
		items: [
			{
				xtype: 'textfield',
				fieldLabel: '* Codigo Postal',
				itemId: 'txtCodigoPostal_1',
				name: 'txtCodigoPostal',	
				regex			:/^[0-9]*$/,
				width: 350,				
				maxLength: 5			
			}
			]
	},
	{
		xtype: 'fieldcontainer',        
      labelStyle: 'font-weight:bold;padding:0',
      layout: 'hbox',
      defaultType: 'textfield',
		id:'contenedor10',
		hidden:false,
      fieldDefaults: {
			msgTarget: 'side',
			labelWidth: 150
		},
		items: [
			{
				xtype: 'textfield',
				fieldLabel: '* Tel�fono',
				itemId: 'txtTelefono_1',
				name: 'txtTelefono',	
				regex			:/^[0-9]*$/,
				width: 350,				
				maxLength: 30			
			}
		]
	},
	{
		xtype: 'fieldcontainer',        
      labelStyle: 'font-weight:bold;padding:0',
      layout: 'hbox',
		id:'contenedor11',
		hidden:false,
      defaultType: 'textfield',
      fieldDefaults: {
			msgTarget: 'side',
			labelWidth: 150
		},
		items: [
			{
				xtype: 'textfield',
				fieldLabel: '* N�m Celular',
				itemId: 'txtNumeroCel_1',
				name: 'txtNumeroCel',	
				regex			:/^[0-9]*$/,
				width: 350,				
				maxLength: 30			
			}
		]
	},
	{ 	xtype: 'textfield',  hidden: true, id: 'rfcSirac', 	value: '' }	
	 
	];
	
	var fp = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'forma',
      frame: true,
		collapsible: 	true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: 'Cliente Externo',
      width: 850,		
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 80
		},
		layout: 'anchor',
		items	: elementosForma,
		monitorValid: 	true,
		buttons: [		
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: procesoAceptar				
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				formBind: true,
				handler: function(){	
					window.location = '15AfiliaClienteExternoExt.jsp';
				}
			}	
		]
	});
	
	
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 900,
		style: 'margin:0 auto;',
		items: [
			fp		
		]
	});
	
	catalogoPais.load(); 
});