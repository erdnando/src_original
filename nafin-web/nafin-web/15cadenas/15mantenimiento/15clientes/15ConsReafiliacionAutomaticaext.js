Ext.onReady(function(){


//------------------Handlers------------------------------

 var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) 
 {
	 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	 var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
	 
	 btnGenerarArchivo.setIconClass('');
	 
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
		btnBajarArchivo.show();
		btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivo.setHandler(function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	 } else {
		 btnGenerarArchivo.enable();
		 NE.util.mostrarConnError(response,opts);
   }
  }
 var procesarNE =  function(opts, success, response) 
 {	 
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
		Ext.getCmp('txt_nombre').setValue(Ext.util.JSON.decode(response.responseText).txt_nombre);
	 } 
  }
function verificaFechas(fec,fec2){
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	


var procesarCatalogoBancoData = function(store, records, oprion){
   if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
    var cbBancoFondeo = Ext.getCmp('Banco');
	 if(cbBancoFondeo.getValue()==''){
	  cbBancoFondeo.setValue(records[0].data['clave']);
	 }
   }
  }

var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	

	var procesarConsulta = function(store,arrRegistros,opts){
			if(arrRegistros!=null){
					var el = grid.getGridEl();
					Ext.getCmp('btnBajarArchivo').hide();
					if(store.getTotalCount()>0){
							el.unmask();
							Ext.getCmp('btnGenerarArchivo').enable();
					}else{
						el.mask('No se encontr� ning�n registro', 'x-mask');
						Ext.getCmp('btnGenerarArchivo').disable();
			}
	}
}

/*
SELECT DISTINCT e.ic_epo AS clave, e.cg_razon_social AS descripcion
           FROM comcat_epo e,
                comrel_pyme_epo pe,
                comrel_pyme_epo_x_producto pep
          WHERE pe.ic_epo = e.ic_epo
            AND pep.ic_epo = pe.ic_epo
            AND pep.ic_pyme = pe.ic_pyme
            AND pe.ic_pyme = 852
            AND pe.cs_distribuidores IN ('S', 'R')
            AND e.cs_habilitado = 'S'
            AND pep.ic_producto_nafin = 4
       ORDER BY e.cg_razon_social
*/

//----------------Stores--------------------------------


var consulta = new Ext.data.JsonStore({
		root: 'registros',
		url: '15ConsReafiliacionAutomaticaext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
					{name: 'NOMBREEPO'},
					{name: 'NOPROVEEDOR'},
					{name: 'NAFINELECTRONICO'},
					{name: 'SIRAC'},
					{name: 'ESTATUS'},
					{name: 'RAZONSOCIAL'},
					{name: 'DOMICILIO'},
					{name: 'ESTADO'},
					{name: 'TELEFONO'},
					{name: 'CORREO'},
					{name: 'FECHAHABILITACION'},
					{name: 'FECHAAFILIACION'},
					{name: 'MONEDA'},
					{name: 'CUENTACHEQUES'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsulta,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							
					}
		}
	}
	});



var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg' ],
		url : '15ConsReafiliacionAutomaticaext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});


  var catalogoBanco = new Ext.data.JsonStore
  ({
	   id: 'catologoBancoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ConsReafiliacionAutomaticaext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoBancoDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 load: procesarCatalogoBancoData,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

	var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ConsReafiliacionAutomaticaext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });







//--------------Componentes------------------------------


var elementosForma = [


		{
				//Banco
				xtype: 'combo',
				fieldLabel: '*Banco',
				emptyText: 'Seleccionar',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				allowBlank: false,
				minChars: 1,
				store: catalogoBanco,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'HBanco',
				id: 'Banco',
				mode: 'local',
				hiddenName: 'HBanco',
				forceSelection: true,
				listeners: {
							select: {
								fn: function(combo) {
									var banco = combo.getValue();
									var cDist = Ext.getCmp('icEpo');
									cDist.setValue('');
									cDist.store.removeAll();
	
									cDist.store.load({
											params: {
												HBanco: combo.getValue()
											}
									});
								
								}
							}
					}
    },
		{
				//EPO
				xtype: 'combo',
				fieldLabel: 'Nombre de la EPO',
				emptyText: 'Todas las EPOs',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'HicEpo',
				id: 'icEpo',
				mode: 'local',
				hiddenName: 'HicEpo',
				forceSelection: false
    },{
			xtype: 'compositefield',
			fieldLabel: 'Proveedor',
			id: 'proveedor',
			combineErrors: false,
			forceSelection: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'txt_ne',
					id:	'txt_ne',
					maxLength:	25,
					msgTarget: 'side',
					flex:1,
					margins: '0 5 0 0',
					listeners: {
							blur: {
								fn: function() {
											Ext.Ajax.request({
												url: '15ConsReafiliacionAutomaticaext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'neElectronico'}),
												callback: procesarNE
											});
								
								}
							}
					}
				},{
					xtype: 'textfield',
					name: 'txt_nombre',
					id:	'txt_nombre',
					maxLength:	50,
					disabled:	true,
					msgTarget: 'side',
					flex:2,
					margins: '0 5 0 0'
				}
			]
		},{
			xtype: 'compositefield',
			combineErrors: false,
			forceSelection: false,
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					id:'disEspacio',
					text:''
				},{
					xtype:	'button',
					id:		'btnBuscaA',
					iconCls:	'icoBuscar',
					text:		'B�squeda Avanzada',
					handler: function(boton, evento) {
								var winVen = Ext.getCmp('winBuscaA');
									if (winVen){
										Ext.getCmp('fpWinBusca').getForm().reset();
										Ext.getCmp('fpWinBuscaB').getForm().reset();
										Ext.getCmp('cmb_num_ne').setValue();
										Ext.getCmp('cmb_num_ne').store.removeAll();
										Ext.getCmp('cmb_num_ne').reset();
										winVen.show();
										
									}else{
										var winBuscaA = new Ext.Window ({
											id:'winBuscaA',
											height: 285,
											x: 300,
											y: 100,
											width: 600,
											heigth: 100,
											modal: true,
											closeAction: 'hide',
											title: 'Nafinet',
											items:[
												{
													xtype:'form',
													id:'fpWinBusca',
													frame: true,
													border: false,
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													labelWidth: 140,
													items:[
														{
															xtype:'displayfield',
															frame:true,
															border: false,
															value:'Utilice el * para b�squeda gen�rica'
														},{
															xtype: 'textfield',
															name: 'nombre',
															id:	'txtNombre',
															fieldLabel:'Nombre',
															maxLength:	100
														},{
															xtype: 'textfield',
															name: 'rfc',
															id:	'txtRfc',
															fieldLabel:'RFC',
															maxLength:	20
														},{
															xtype: 'textfield',
															name: 'num_pyme',
															id:	'txtNe',
															fieldLabel:'N�mero de Proveedor',
															maxLength:	25
														}
													],
													buttons:[
														{
															text:'Buscar',
															iconCls:'icoBuscar',
															handler: function(boton) {
																			if ( Ext.isEmpty(Ext.getCmp('txtNombre').getValue()) && Ext.isEmpty(Ext.getCmp('txtRfc').getValue()) && Ext.isEmpty(Ext.getCmp('txtNe').getValue()) ){
																				Ext.Msg.alert(boton.text,'No existe informaci�n con los criterios determinados');
																				return;
																			}
															  catalogoNombreData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoLimpiar',
															handler: function() {
																			Ext.getCmp('winBuscaA').hide();
																		}
														}
													]
												},{
													xtype:'form',
													frame: true,
													id:'fpWinBuscaB',
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													monitorValid: true,
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													items:[
														{
															xtype: 'combo',
															id:	'cmb_num_ne',
															name: 'Hcmb_num_ne',
															hiddenName : 'Hcmb_num_ne',
															fieldLabel: 'Nombre',
															emptyText: 'Seleccione . . .',
															displayField: 'descripcion',
															valueField: 'clave',
															triggerAction : 'all',
															forceSelection:true,
															allowBlank: false,
															typeAhead: true,
															mode: 'local',
															minChars : 1,
															store: catalogoNombreData,
															tpl : NE.util.templateMensajeCargaCombo
														}
													],
													buttons:[
														{
															text:'Aceptar',
															iconCls:'aceptar',
															formBind:true,
															handler: function() {
																			if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																				var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																				Ext.getCmp('txt_ne').setValue(Ext.getCmp('cmb_num_ne').getValue());
																				Ext.getCmp('txt_nombre').setValue(disp);
																				Ext.getCmp('winBuscaA').hide();
																			}
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoLimpiar',
															handler: function() {	Ext.getCmp('winBuscaA').hide();	}
														}
													]
												}
											]
										}).show();
									}
								}
				}
			]
		},{
			xtype: 'textfield',
			name: 'sirac',
			id:	'sirac',
			fieldLabel:'N�mero de SIRAC',
			width: 100,
			maxLength:	25
			},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Habilitaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
            {
			    // Fecha Inicio
			   xtype: 'datefield',
         name: 'hFechaRegIni',
         id: 'hFechaRegIni',
         vtype: 'rangofecha',
         campoFinFecha: 'hFechaRegFin',
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 margins: '0 20 0 0'
         },
			   {
				 xtype: 'displayfield',
				 value: 'a',
				 width: 50
			   },
		     {
			    // Fecha Final
			   xtype: 'datefield',
         name: 'hFechaRegFin',
         id: 'hFechaRegFin',
         vtype: 'rangofecha',
         campoInicioFecha: 'hFechaRegIni',
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 margins: '0 20 0 0'
         },
				 {
				 xtype: 'displayfield',
				 value: '(dd/mm/aaaa)',
				 width: 40
			  }
      ]
	 },{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Afiliaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
            {
			    // Fecha Inicio
			   xtype: 'datefield',
				name: 'aFechaRegIni',
				id: 'aFechaRegIni',
				vtype: 'rangofecha',
				campoFinFecha: 'aFechaRegFin',
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 margins: '0 20 0 0'
         },
			   {
				 xtype: 'displayfield',
				 value: 'a',
				 width: 50
			   },
		     {
			    // Fecha Final
			   xtype: 'datefield',
				name: 'aFechaRegFin',
				id: 'aFechaRegFin',
				vtype: 'rangofecha',
				campoInicioFecha: 'aFechaRegIni',
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
			
				 margins: '0 20 0 0'
         },
				 {
				 xtype: 'displayfield',
				 value: '(dd/mm/aaaa)',
				 width: 40
			  }
      ]
	 },{
			xtype: 'checkbox',
			name: 'pymeSinProv',
			id: 'pymeSinProv',
			hiddenName:'pymeSinProv',
			fieldLabel: 'Tipo de Consulta'
	 },{
			xtype: 'checkbox',
			name: 'pymeHab',
      id: 'pymeHab',
			hiddenName:'pymeHab',
			fieldLabel: 'Pymes por habilitar'
	 }
		
		];


var grid = new Ext.grid.GridPanel({
	id:'grid',
	header:true,
	store: consulta,
	style: 'margin:0 auto;',
	hidden: true,
	stripeRows: true,
	loadMask: true,
	height:400,
	width: 940,
	frame: true,
	columns:[
				//CAMPOS DEL GRID
						{
						header:'EPO',
						tooltip: 'EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'left',
						renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
								}
						},
						{
						align: 'center',
						header: 'No de Proveedor',
						tooltip: 'No de Proveedor',
						sortable: true,
						dataIndex: 'NOPROVEEDOR',
						width: 150
						},
						{
						align: 'center',
						header: 'No Nafin Electr�nico',
						tooltip: 'No Nafin Electr�nico',
						sortable: true,
						dataIndex: 'NAFINELECTRONICO',
						width: 150
						},
						{
						align: 'center',
						header: 'No SIRAC',
						tooltip: 'No SIRAC',
						sortable: true,
						dataIndex: 'SIRAC',
						width: 150
						},
						{
						align: 'center',
						header: 'Estatus',
						tooltip: 'Estatus',
						sortable: true,
						dataIndex: 'ESTATUS',
						width: 150
						},
						{
						align: 'center',
						header: 'Nombre o Raz�n Social',
						tooltip: 'Nombre o Raz�n Social',
						sortable: true,
						dataIndex: 'RAZONSOCIAL',
						width: 150
						},
						{
						align: 'center',
						header: 'Domicilio',
						tooltip: 'Domicilio',
						sortable: true,
						dataIndex: 'DOMICILIO',
						width: 150
						},
						{
						align: 'center',
						header: 'Estado',
						tooltip: 'Estado',
						sortable: true,
						dataIndex: 'ESTADO',
						width: 150
						},
						{
						align: 'center',
						header: 'Tel�fono',
						tooltip: 'Tel�fono',
						sortable: true,
						dataIndex: 'TELEFONO',
						width: 150
						},
						{
						align: 'center',
						header: 'Correo Electr�nico',
						tooltip: 'Correo Electr�nico',
						sortable: true,
						dataIndex: 'CORREO',
						width: 150
						},
						{
						align: 'center',
						header: 'Fecha de Habilitaci�n',
						tooltip: 'Fecha de Habilitaci�n',
						sortable: true,
						dataIndex: 'FECHAHABILITACION',
						width: 150
						},
						{
						align: 'center',
						header: 'Fecha de Afiliaci�n',
						tooltip: 'Fecha de Afiliaci�n',
						sortable: true,
						dataIndex: 'FECHAAFILIACION',
						width: 150
						},
						{
						align: 'center',
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'MONEDA',
						width: 150
						},
						{
						align: 'center',
						header: 'No. de Cuenta de Cheques',
						tooltip: 'No. de Cuenta de Cheques',
						sortable: true,
						dataIndex: 'CUENTACHEQUES',
						width: 150
						}
						],
						bbar: {
							xtype: 'paging',
							pageSize: 15,
							buttonAlign: 'left',
							id: 'barraPaginacion',
							displayInfo: true,
							store: consulta,
							displayMsg: '{0} - {1} de {2}',
							emptyMsg: "No hay registros.",
							items: [
								'->','-',
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
									handler: function(boton, evento) {
										boton.disable();
										boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
												url: '15ConsReafiliacionAutomaticaext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'ArchivoCSV',
													pymeSinProv: Ext.getCmp('pymeSinProv').getValue(),
													pymeHab: Ext.getCmp('pymeHab').getValue()}),
												callback: procesarSuccessFailureGenerarArchivo
											});
										
									}
								},{
									xtype: 'button',
									text: 'Bajar Archivo',
									hidden: true,
									id: 'btnBajarArchivo'
								
								},
								'-']
							
						}
	


});



//Formulario (ventana principal)

var fp = new Ext.form.FormPanel({
		hidden: false,
		height: 'auto',
		width: 600,
		labelWidth: 130,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding 6px',
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults:{
			msgTarget: 'side',
			anchor:'-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		[
			{
				//Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						
						//Mi acci�n
						if(verificaFechas('aFechaRegIni','aFechaRegFin')&&verificaFechas('hFechaRegFin','hFechaRegIni')){
						
								grid.show();
								consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar',
									start:0,
									limit:15,
									pymeSinProv: Ext.getCmp('pymeSinProv').getValue(),
									pymeHab: Ext.getCmp('pymeHab').getValue()
									}
									
									)});
							}
						}				
				},{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						fp.getForm().reset();
						grid.hide();
				}
			 }
			 
		]

});





//----------------Principal------------------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [ 
		  fp,
	    NE.util.getEspaciador(30),
			grid
			 
		 ]
  });





catalogoBanco.load();


});