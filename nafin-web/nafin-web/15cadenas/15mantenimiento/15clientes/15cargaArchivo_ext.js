Ext.onReady(function() {

	function procesaDescargaZIP(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('contenedorPrincipal').el.unmask();
			if (infoR.nombreZip != undefined){
				var winZip = new Ext.Window ({
				  height: 100,
				  x: 400,
				  y: 100,
				  width: 200,
				  modal: true,
				  closeAction: 'hide',
				  title: 'Descarga ZIP',
				  html: '<iframe style="width:100%;height:100%;border:0" src="'+infoR.strDirecVirtualTemp+infoR.nombreZip+'"></iframe>'
				}).show();
				if (winZip){
					winZip.hide();
				}
			}else{
				Ext.Msg.alert('Descargar ZIP','No existe archivo a descargar, los archivos ya fueron eliminados.');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var descargaExcel = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var numProceso = registro.data['NUMPROC'];
		Ext.Ajax.request({
			url: '15cargaArchivoExt.data.jsp',
			params: {
					informacion:'GeneraResumenExcel',
					numProc: numProceso
					},
			callback: procesarSuccessGeneraResumenExcel
		});
	}

	var muestraProceso = function(grid, rowIndex, colIndex, item, event) {
		
		var registro = grid.getStore().getAt(rowIndex);
		var numProceso = registro.data['NOMBRE_PROCESO'];

		resCargaEconData.load({
			params:{
				numProc : numProceso
			}
		});
	}

	var procesarConsultaCargaEcon = function(store, arrRegistros, opts) {
		if(arrRegistros!= null) {

			if(store.getTotalCount() > 0) {

				if(!gridCargaEcon.isVisible()) {
					gridCargaEcon.show();
				}
				
				var btnGenerarArch = Ext.getCmp('btnArchivo');
				var btnAbrirCSV = Ext.getCmp('btnAbrirCSV');
				btnGenerarArch.enable();
				btnAbrirCSV.hide();
				fpConsulta.hide();
				grid.hide();
				Ext.getCmp('leyenda').hide();
			}
		}
	}

	var procesarConsultaData = function (store, arrRegistros, opts) {
		var fpCons = Ext.getCmp('pnlConsulta');
		fpCons.el.unmask();
		if(arrRegistros!= null) {
			if(!grid.isVisible()) {
				grid.show();
				Ext.getCmp('leyenda').show();		
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnDescargarZIP = Ext.getCmp('btnDescargarZIP');
			
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				btnGenerarArchivo.enable();
				btnBajarArchivo.hide();
				btnDescargarZIP.enable();
				el.unmask();
			}else {
				btnGenerarArchivo.disable();
				btnDescargarZIP.disable();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessGeneraResumenExcel = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var forma = Ext.getDom('formAux');
			
			forma.action = resp.urlArchivo;
			forma.submit();
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessGeneraCSV = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var btnGenerarArch = Ext.getCmp('btnArchivo');
			
			btnGenerarArch.setIconClass('');
			var btnAbrirCSV = Ext.getCmp('btnAbrirCargaCSV');
			btnAbrirCSV.show();
			btnAbrirCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnAbrirCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = resp.urlArchivo;
				forma.submit();
			});
			btnGenerarArch.disable();
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var gridAcuseData = new Ext.data.JsonStore({
		fields:	[{name: 'IC_FOLIO'},
					{name: 'CG_NOMBRE_DOCTO'},
					{name: 'DF_CARGA'},
					{name: 'CG_USUARIO'},
					{name: 'CG_NOMBRE_USUARIO'},
					{name: 'IC_ESTATUS_CARGA_PYME'},
					{name: 'CG_DESCRIPCION'}],
		data:		[
					{'IC_FOLIO':'','CG_NOMBRE_DOCTO':'','DF_CARGA':'','CG_USUARIO':'','CG_NOMBRE_USUARIO':'','IC_ESTATUS_CARGA_PYME':'','CG_DESCRIPCION':''}
		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridLayoutData = new Ext.data.JsonStore({
		fields:	[{name: 'NUMERO'},
					{name: 'DESCRIPCION'},
					{name: 'TIPO_DATO'},
					{name: 'LONGITUD'},
					{name: 'OBLIGATORIO'}],
		data:		[
					{'NUMERO':'1','DESCRIPCION':'N�mero de proveedor','TIPO_DATO':'Alfanum�rico','LONGITUD':'25','OBLIGATORIO':'Si'},
					{'NUMERO':'2','DESCRIPCION':'RFC','TIPO_DATO':'Alfanum�rico','LONGITUD':'20','OBLIGATORIO':'Si'},
					{'NUMERO':'3','DESCRIPCION':'Apellido Paterno (S�lo para personas f�sicas)','TIPO_DATO':'Alfanum�rico','LONGITUD':'30','OBLIGATORIO':'No'},
					{'NUMERO':'4','DESCRIPCION':'Apellido Materno (S�lo para personas f�sicas)','TIPO_DATO':'Alfanum�rico','LONGITUD':'25','OBLIGATORIO':'No'},
					{'NUMERO':'5','DESCRIPCION':'Nombre (S�lo para personas f�sicas)','TIPO_DATO':'Alfanum�rico','LONGITUD':'40','OBLIGATORIO':'Si'},
					{'NUMERO':'6','DESCRIPCION':'Raz�n Social (S�lo para personas morales)','TIPO_DATO':'Alfanum�rico','LONGITUD':'100','OBLIGATORIO':'Si'},
					{'NUMERO':'7','DESCRIPCION':'Calle y n�mero que corresponde al domicilio fiscal','TIPO_DATO':'Alfanum�rico','LONGITUD':'100','OBLIGATORIO':'Si'},
					{'NUMERO':'8','DESCRIPCION':'Colonia que corresponde al domicilio fiscal','TIPO_DATO':'Alfanum�rico','LONGITUD':'100','OBLIGATORIO':'Si'},
					{'NUMERO':'9','DESCRIPCION':'Estado que corresponde al domicilio fiscal','TIPO_DATO':'Alfanum�rico','LONGITUD':'100','OBLIGATORIO':'Si'},
					{'NUMERO':'10','DESCRIPCION':'Pa�s que corresponde al domicilio fiscal','TIPO_DATO':'Alfanum�rico','LONGITUD':'100','OBLIGATORIO':'Si'},
					{'NUMERO':'11','DESCRIPCION':'Municipio que corresponde al domicilio fiscal','TIPO_DATO':'Alfanum�rico','LONGITUD':'100','OBLIGATORIO':'Si'},
					{'NUMERO':'12','DESCRIPCION':'C�digo Postal que corresponde al domicilio fiscal','TIPO_DATO':'Num�rico','LONGITUD':'5','OBLIGATORIO':'Si'},
					{'NUMERO':'13','DESCRIPCION':'Tel�fono que corresponde al domicilio fiscal','TIPO_DATO':'Num�rico','LONGITUD':'30','OBLIGATORIO':'Si'},
					{'NUMERO':'14','DESCRIPCION':'Fax que corresponde al domicilio fiscal','TIPO_DATO':'Num�rico','LONGITUD':'30','OBLIGATORIO':'No'},
					{'NUMERO':'15','DESCRIPCION':'E-mail que corresponde al domicilio fiscal','TIPO_DATO':'Alfanum�rico','LONGITUD':'100','OBLIGATORIO':'No'},
					{'NUMERO':'16','DESCRIPCION':'Apellido Paterno del contacto','TIPO_DATO':'Alfanum�rico','LONGITUD':'25','OBLIGATORIO':'Si'},
					{'NUMERO':'17','DESCRIPCION':'Apellido Materno del contacto','TIPO_DATO':'Alfanum�rico','LONGITUD':'25','OBLIGATORIO':'Si'},
					{'NUMERO':'18','DESCRIPCION':'Nombre del contacto','TIPO_DATO':'Alfanum�rico','LONGITUD':'80','OBLIGATORIO':'Si'},
					{'NUMERO':'19','DESCRIPCION':'Tel�fono del contacto','TIPO_DATO':'Num�rico','LONGITUD':'30','OBLIGATORIO':'Si'},
					{'NUMERO':'20','DESCRIPCION':'Fax del contacto','TIPO_DATO':'Num�rico','LONGITUD':'30','OBLIGATORIO':'No'},
					{'NUMERO':'21','DESCRIPCION':'E-mail del contacto','TIPO_DATO':'Alfanum�rico','LONGITUD':'100','OBLIGATORIO':'No'},
					{'NUMERO':'22','DESCRIPCION':'Tipo de cliente (siempre el valor 1)','TIPO_DATO':'Num�rico','LONGITUD':'1','OBLIGATORIO':'Si'},
					{'NUMERO':'23','DESCRIPCION':'Indicador de susceptibilidad a descuento (siempre el valor S)','TIPO_DATO':'Alfanum�rico','LONGITUD':'1','OBLIGATORIO':'Si'},
					{'NUMERO':'24','DESCRIPCION':'Generar clave de consulta (siempre el valor N)','TIPO_DATO':'Alfanum�rico','LONGITUD':'1','OBLIGATORIO':'Si'}

		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15cargaArchivoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusConsulta'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15cargaArchivoExt.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
					{name: 'IC_FOLIO'},
					{name: 'NOMBRE_EPO'},
					{name: 'CG_NOMBRE_DOCTO'},
					{name: 'DF_CARGA'},
					{name: 'CG_USUARIO'},
					{name: 'CG_NOMBRE_USUARIO'},
					{name: 'IC_ESTATUS_CARGA_PYME'},
					{name: 'ESTATUS'},
					{name: 'NOMBRE_PROCESO'},
					
					{name: 'CG_OBS_MOTIVO'},
					{name: 'IC_MOTIVO'},
					{name: 'DESCRIPCION'},
					{name: 'IC_FECHA_CAMBIO_ESTATUS'},
					{name: 'CS_BORRADO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});

	var resCargaEconData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15cargaArchivoExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaCargaEcon'
		},
		fields: [
			{name: 'NUMPROC'},
			{name: 'EPO'},
			
			{name: 'REG_TOTALES'},
			{name: 'REG_SINERROR_NE'},
			{name: 'REG_CONERROR_NE'},
			{name: 'REG_PROV_YA_EXISTENTES_NE'},
			  
			{name: 'REGTOTAL'},
			{name: 'REGDUPLI'},
			{name: 'REGUNICO'},
			{name: 'REGYAAFIL'},
			{name: 'REGREAFIL'},
			{name: 'REGREAFILAUT'},
			{name: 'REGCONVUNICO'},
			{name: 'REGPORAFIL'},
			{name: 'REGCARGADOS'},
			{name: 'REGPORCARGAR'},
			{name: 'NOMBREARCH'},
			{name: 'FECHACARGA'},
			{name: 'DATOSCARGA'},
			{name: 'CSPROCESADO'},
			{name: 'TIPOCARGA'},
			{name: 'CONTAFIL'},
			{name: 'CONTREAFIL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaCargaEcon,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCargaEcon(null, null, null);
				}
			}
		}
	});

	var selectModel = new Ext.grid.CheckboxSelectionModel({
		//kheader: 'Seleccione',
		//header:	'<div class="x-grid3-row x-grid-row-checker {width: 18px;}">&#160;</div>',
		width:	25,
		singleSelect: false,
      checkOnly: true
    });
	 
	var Columnas = new Ext.grid.ColumnModel([ 
		selectModel,
		{
			header: 'Folio',
			tooltip: 'N�mero Folio',	
			dataIndex: 'IC_FOLIO',	
			sortable : true, 
			width : 100, 
			align: 'center',
			resizable: true
		},
		{
			header: 'EPO',
			tooltip: 'Nombre de la EPO',	
			dataIndex: 'NOMBRE_EPO',	
			sortable : true, 
			width : 185, 
			align: 'center',
			resizable: true
		},
		{
			header: 'Nombre Archivo',
			tooltip: 'Nombre Archivo',	
			dataIndex: 'CG_NOMBRE_DOCTO',	
			sortable : true, 
			width : 200,
			align: 'center',
			resizable: true,
			renderer: function(value, metadata, record, rowindex, colindex, store) {
							metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
							return value;
				}
			},
			{
				header: 'Fecha y Hora de Carga',
				tooltip: 'Fecha y Hora de Carga',	
				dataIndex: 'DF_CARGA',	
				sortable : true, 
				width : 125, 
				align: 'center',
				resizable: true
			},
			{
				header: 'Usuario',tooltip: 'Usuario',	dataIndex: 'CG_USUARIO',	sortable : true, width : 240, align: 'center',resizable: true,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value + ' ' +record.get('CG_NOMBRE_USUARIO')) + '"';
								return value + ' ' +record.get('CG_NOMBRE_USUARIO');
				}
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',	
				dataIndex: 'ESTATUS',	
				sortable : true,
				width : 90,
				align: 'center',
				resizable: true
			},
			// Fodeo020 ----------------------------------------------------------------------
			{
				header: 'Fecha cambio de estatus',
				tooltip: 'Fecha cambio de estatus',	
				dataIndex: 'IC_FECHA_CAMBIO_ESTATUS',	
				sortable : true, 
				width : 125, 
				align: 'center',
				resizable: true				
			},
			
			{
				header:	'', 
				dataIndex: 'CG_OBS_MOTIVO',
				hidden:true				
			},
			{
				header:	'', 
				dataIndex: 'DESCRIPCION',
				hidden:true				
			},
			
			{
				header: 'Motivo Rechazo',
				tooltip: 'Motivo Rechazo',	
				dataIndex: 'IC_MOTIVO',	
				sortable : true, 
				width : 90, 
				align: 'center',
				resizable: true,
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {	
					if(registro.get('ESTATUS') == ('Rechazada'))
					{
						if ( registro.get('IC_MOTIVO') == ('6') )	
							return value = registro.get('DESCRIPCION') + "- " + registro.get('CG_OBS_MOTIVO'); //;// + "- " + registro.get('CG_OBS_MOTIVO');
						else
							return value = registro.get('DESCRIPCION');
					}
					else 
						return value = 'N/A';				
				}
			},
			// FIN Fodeo020 ------------------------------------------------------------------
			
			
			{
				xtype:	'actioncolumn',
				header : 'Proceso', tooltip: 'Proceso',
				dataIndex : 'NOMBRE_PROCESO',
				width:	100,	align: 'center', hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if (record.get('IC_ESTATUS_CARGA_PYME') == '5'){
									return value;
								}else{
									return 'N/A';
								}
				},
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							if (record.get('IC_ESTATUS_CARGA_PYME') == '5'){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else	{
								return "";
							}
						},
						handler:	muestraProceso
					}
				]
			}
		]);
			
			
	var gridView = new Ext.grid.GridView({ 
		//forceFit: true, //Comentar si no. 
		getRowClass : function (row, index) { 
			var cls =''; 
			var data = row.data.CS_BORRADO; //Color es el campo que contiene la simbologia para la leyenda 
			if(data == 'S'){
				return cls = 'redrow'
			}
		} 
	}); //end gridView 
	
	var grid = new Ext.grid.EditorGridPanel({
		store: consultaData,
		view:gridView, 
		hidden: false,
		sm: selectModel,
		cm : Columnas,
		stripeRows: true,
		loadMask: true,
		height: 500,
		width: 940,
		columnLines: true,
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		frame: true,
		hidden: true,
		bbar:{
			xtype: 'paging',
			pageSize: 50,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15cargaArchivoExt.data.jsp',
							params: Ext.apply(fpConsulta.getForm().getValues(),{
							 informacion: 'ArchivoCSV',
							 tipo: 'CSV'
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Descargar ZIP',
					id: 'btnDescargarZIP',
					handler: function(boton, evento) {
							var sm = grid.getSelectionModel().getSelections();
							if (!Ext.isEmpty(sm)){
								var folios = [];
								for (i=0; i<=sm.length-1; i++) {
									folios.push(sm[i].get('IC_FOLIO'));
								}
								Ext.getCmp('contenedorPrincipal').el.mask('Procesando. . . ','x-mask-loading');

								Ext.Ajax.request({
									url: '15cargaArchivoExt.data.jsp',
									params: Ext.apply({informacion: "DescargaZIP",listFolios: Ext.encode(folios)}),
									callback: procesaDescargaZIP
								});
							}else{
								Ext.Msg.alert(boton.text,'Debe seleccionar al menos un registro para generar su archivo original.');
							}
					}
				}
      ]
		}
	});

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 2, align: 'center'},
				{header: 'Carga N@E', colspan: 4, align: 'center'},
				{header: 'Limpieza', colspan: 3, align: 'center'},
				{header: 'Dispersi�n de Registros', colspan: 5, align: 'center'},
				{header: 'Econtract', colspan: 2, align: 'center'},
				{header: '', colspan: 5, align: 'center'}
			]
		]
	});
	
	
	var gridCargaEcon = new Ext.grid.EditorGridPanel({
		id: 'gridCargaEcon1',
		store: resCargaEconData,
		margins: '20 0 0 0',
		clicksToEdit: 1,
		columns: [
			{
				header: 'N�mero de Proceso',
				tooltip: 'N�mero de Proceso',
				dataIndex: 'NUMPROC',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'EPO',
				sortable: true,
				width: 200,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			/*-------------------------------CARGA N@E---------------------------*/
			{
				header: 'Registros Totales',
				tooltip: 'Registros Totales',
				dataIndex: 'REG_TOTALES',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Registros sin Error',
				tooltip: 'Registros sin Error',
				dataIndex: 'REG_SINERROR_NE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Registros con Error',
				tooltip: 'Registros con Error',
				dataIndex: 'REG_CONERROR_NE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Ya Existentes',
				tooltip: 'Ya Existentes',
				dataIndex: 'REG_PROV_YA_EXISTENTES_NE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			/*-------------------------------------------------------------------*/
			
			{
				header: 'Registros Totales',
				tooltip: 'Registros Totales',
				dataIndex: 'REGTOTAL',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Duplicados',
				tooltip: 'Duplicados',
				dataIndex: 'REGDUPLI',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: '�nicos',
				tooltip: 'Unicos',
				dataIndex: 'REGUNICO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Ya Afiliados',
				tooltip: 'Ya Afiliados',
				dataIndex: 'REGYAAFIL',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Reafiliaciones',
				tooltip: 'Reafiliaciones',
				dataIndex: 'REGREAFIL',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Reafiliaciones Auto.',
				tooltip: 'Reafiliaciones Autom�ticas',
				dataIndex: 'REGREAFILAUT',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Con Contrato �nico',
				tooltip: 'Con Contrato �nico',
				dataIndex: 'REGCONVUNICO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Por Afiliar',
				tooltip: 'Por Afiliar',
				dataIndex: 'REGPORAFIL',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Ya Cargados',
				tooltip: 'Ya Cargados',
				dataIndex: 'REGCARGADOS',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Por Cargar',
				tooltip: 'Por Cargar',
				dataIndex: 'REGPORCARGAR',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Nombre Archivo',
				tooltip: 'Nombre Archivo',
				dataIndex: 'NOMBREARCH',
				sortable: true,
				width: 200,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Fecha de Carga',
				tooltip: 'Fecha de Carga',
				dataIndex: 'FECHACARGA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Datos de Carga',
				tooltip: 'Datos de Carga',
				dataIndex: 'DATOSCARGA',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Procesado',
				tooltip: 'Procesado',
				dataIndex: 'CSPROCESADO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},{
				xtype:	'actioncolumn',
				header : 'Resumen', tooltip: 'Resumen',
				width:	65,	align: 'center', hidden: false,
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver Detalle';
							return 'icoXls';
							
						},
						handler:	descargaExcel
					}
				]
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 160,
		width: 940,
		style: 'margin:0 auto;',
		title: '<div align="center">Consulta de la Carga Masiva de Proveedores</div>',
		frame: true,
		hidden: true,
		plugins: grupos,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Generar Archivo',
					id:'btnArchivo',
					handler: function(btn){
						
						var store = gridCargaEcon.getStore();
						var registrosEnviar = [];
						store.each(function(record) {
							registrosEnviar.push(record.data);
						});//fin 
						
						registrosEnviar = Ext.encode(registrosEnviar);
						
						btn.setIconClass('loading-indicator');
						
						Ext.Ajax.request({
							url : '15cargaArchivoExt.data.jsp',
							params :{
								informacion: 'GenerarCSV',
								registros : registrosEnviar
							},
							callback: procesarSuccessGeneraCSV
						});
					}
				},
				{
					text: 'Abrir CSV',
					id:'btnAbrirCargaCSV',
					hidden:true
				},
				'-',
				{
					text: 'Regresar',
					id:'btnRegresar',
					handler: function(btn){
						gridCargaEcon.hide();
						fpConsulta.show();
						grid.show();
						Ext.getCmp('leyenda').show();
					}
				}
			]
			
		}
	});

	var elementosFormaConsulta = [
		{
			xtype:	'textfield',
			id:		'icFolio',
			name:		'icFolio',
			fieldLabel:	'N�mero Folio',
			maxLength:	16,
			anchor:	'95%'
		},{
			xtype: 'combo',
			name: 'icEstatus',
			id:	'idEstatus',
			fieldLabel: 'Estatus',
			emptyText: 'Seleccionar . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'icEstatus',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEstatusData,
			tpl : NE.util.templateMensajeCargaCombo,
			anchor:	'95%'
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud',
			combineErrors: false,
			msgTarget: 'side',
			//anchor:'100%',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_carga_ini',
					id: 'fecha_carga_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecha: 'fecha_carga_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_carga_fin',
					id: 'fecha_carga_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_carga_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}
	];

	var fpConsulta = new Ext.form.FormPanel({
		id: 'pnlConsulta',
		width: 435,
		title: 'Consulta Estatus de Carga',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		style: 'margin: 0 auto',
		bodyStyle:'padding:10px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		labelWidth: 130,
		defaultType: 'textfield',
		items: elementosFormaConsulta,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				
				handler: function(boton, evento) {
					/*Validacion del folio*/
					var folio = Ext.getCmp('icFolio');
					if (!esVacio(folio.getValue())){
						if (!isdigit(folio.getValue())){
							folio.markInvalid('Verifique el formato del campo N�mero Folio');
							folio.focus();
							return;
					}}	
					
					grid.hide();
					Ext.getCmp('leyenda').hide();
					fpConsulta.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({params: Ext.apply(fpConsulta.getForm().getValues(),{operacion: 'Generar',start: 0,limit: 50})});
				}
			},
			{
				text: 'Cancelar',
				hidden: false,
				iconCls: 'borrar',
				handler: function() {
					fpConsulta.getForm().reset();
					grid.hide();
					Ext.getCmp('leyenda').hide();
				}
				
			}
		]
	});

	var elementosFormaAcuse = [
		{
			xtype: 'grid',
			id: 'gridAcuse',
			anchor: '100%',
			store: gridAcuseData,
			columns: [
				{
					header: 'N�mero Folio',tooltip: 'N�mero Folio',	dataIndex: 'IC_FOLIO',	sortable : true, width : 120, align: 'center',resizable: true
				},{
					header: 'Nombre Archivo',tooltip: 'Nombre Archivo',	dataIndex: 'CG_NOMBRE_DOCTO',	sortable : true, width : 240, align: 'center',resizable: true,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
									metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
									return value;
					}
				},{
					header: 'Fecha y Hora de Carga',tooltip: 'Fecha y Hora de Carga',	dataIndex: 'DF_CARGA',	sortable : true, width : 150, align: 'center',resizable: true
				},{
					header: 'Usuario',tooltip: 'Usuario',	dataIndex: 'CG_USUARIO',	sortable : true, width : 240, align: 'center',resizable: true,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
									metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value + ' ' +record.get('CG_NOMBRE_USUARIO')) + '"';
									return value + ' ' +record.get('CG_NOMBRE_USUARIO');
					}
				},{
					header: 'Estatus',tooltip: 'Estatus',	dataIndex: 'CG_DESCRIPCION',	sortable : true, width : 130, align: 'center',resizable: true
				}
			],
			bbar:{
				xtype: 'toolbar',
				buttons: ['->','-',
					{
						xtype: 'button',
						text: 'Abrir Archivo',
						id: 'btnAbrirCSV'
					},'-',{
						xtype: 'button',
						text: 'Abrir PDF',
						id: 'btnAbrirPDF'
					},'-',{
						xtype: 'button',
						text: 'Aceptar',
						id: 'btnAceptar',
						handler: function(boton, evento) {
							window.location = '15cargaArchivo_ext.jsp'
						}
					}
				]
			},
			stripeRows: true,
			loadMask: true,
			height: 120,
			//width: 885,
			title: '<div align="center">Acuse de recepci�n de archivo de proveedores para carga</div>',
			frame: true,
			viewConfig: {forceFit: true},
			hidden: false
		}
	];

	var fpAcuse = new Ext.form.FormPanel({
		id: 'pnlAcuse',
		hidden: true	,
		width: 885,
		title: '',
		frame: false,
		border: false,
		collapsible: false,
		titleCollapse: false,
		bodyStyle:'padding:0px',
		style: 'margin: 0 auto',
		labelWidth: 1,
		defaultType: 'textfield',
		items: elementosFormaAcuse
	});

	var elementosFormaCarga = [
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function() {
						if (!Ext.getCmp('gridLayout').isVisible()){
							Ext.getCmp('gridLayout').show();
						}
					}
				},{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 100,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'fileuploadfield',
							id: 'form_file',
							anchor: '95%',
							allowBlank: false,
							blankText:	'Debe seleccionar una ruta de archivo.',
							emptyText: 'Seleccione un archivo',
							fieldLabel: 'Ruta del Archivo',
							name: 'xlsArchivo',
							regex: /^.*\.(xls|XLS|xlsx|XLSX)$/,
							regexText:'El archivo debe tener extensi�n .xls � .xlsx',
							buttonText: 'Examinar...'
						},{
							xtype: 'hidden',
							id:	'hidExtension',
							name:	'hidExtension',
							value:	''
						}
					]
				},{
					xtype:'panel',
					columnWidth: 1,
					id:	'pnlReco',
					style: 'margin: 0 auto',
					defaults: {
						msgTarget: 'side',
						anchor: '-20'
					},
					layout:	'form',
					html: '</br><div class="formas" align="left" ><b><i>Recomendaciones:</i></b><br/>'+
							'<i>-Subir un archivo de Excel con tama�o m�ximo 500 registros (700 Kb en formato .xls � 150 Kb en formato .xlsx)</i></br>'+
							'<i>-Enviar �nicamente una hoja por archivo.</i></br>'+
							'<i>-La hoja enviada No debe tener formato (bordes, colores, negritas, etc.)</i>'+
							'</div>',
					buttons: [
						{
							text: 'Aceptar',
							iconCls: 'correcto',
							//formBind: true,
							handler: function(boton, evento) {
										var cargaArchivo = Ext.getCmp('form_file');
										if (!cargaArchivo.isValid()){
											cargaArchivo.focus();
											return;
										}
										var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
										var extArchivo = Ext.getCmp('hidExtension');
										
										if (/^.*\.(xls)$/.test(ifile)){
											extArchivo.setValue('xls');
										}else if(/^.*\.(xlsx)$/.test(ifile)){
											extArchivo.setValue('xlsx');
										}

										fpCarga.el.mask('Procesando...', 'x-mask-loading');
										fpCarga.getForm().submit({
											url: '15cargaArchivo_ext.data.jsp',
											waitMsg: 'Enviando datos...',
											success: function(form, action) {
												if (action.result.flag){
													gridAcuseData.loadData(action.result.acuse);
													Ext.getCmp('btnAbrirCSV').setHandler( function(boton, evento) {
														var forma = Ext.getDom('formAux');
														forma.action = action.result.archivoCSV.urlArchivo;
														forma.submit();
													});
													Ext.getCmp('btnAbrirPDF').setHandler( function(boton, evento) {
														var forma = Ext.getDom('formAux');
														forma.action = action.result.archivoPDF.urlArchivo;
														forma.submit();
													});
													fpCarga.el.unmask();
													fpCarga.hide();
													fpAcuse.show();
												
												}else{
													fpCarga.el.unmask();
													cargaArchivo.markInvalid('El tama�o m�ximo del archivo es de 500 registros (700 KB para formato .xls y de 150 KB para formato .xlsx)');
													return;
												}
											},
											failure: NE.util.mostrarSubmitError
										})
							}
						},
						{
							text: 'Cancelar',
							hidden: false,
							iconCls: 'borrar',
							handler: function() {
								fpCarga.getForm().reset();
								if (Ext.getCmp('gridLayout').isVisible()){
									Ext.getCmp('gridLayout').hide();tp.el.dom.scrollIntoView();
								}
							}
							
						}
					]
				},{
					xtype: 'grid',
					id: 'gridLayout',
					store: gridLayoutData,
					columns: [
						{header: 'No de Campo',tooltip: 'No. de Campo',	dataIndex: 'NUMERO',	sortable : true, width : 80, align: 'center',resizable: false},
						{header: 'Descripci�n',tooltip: 'Descripci�n',	dataIndex: 'DESCRIPCION',	sortable : true, width : 350, align: 'left',resizable: true},
						{header: 'Tipo de Dato',tooltip: 'Tipo de Dato',	dataIndex: 'TIPO_DATO',	sortable : true, width : 90, align: 'center',resizable: false},
						{header: 'Longitud',tooltip: 'Longitud',	dataIndex: 'LONGITUD',	sortable : true, width : 75, align: 'center',resizable: false},
						{header: 'Obligatorio',tooltip: 'Obligatorio',	dataIndex: 'OBLIGATORIO',	sortable : true, width : 75, align: 'center',resizable: false}
					],
					bbar:{
						xtype: 'toolbar',
						buttons: ['->','-',{
													xtype: 'button',text: 'Descargar ejemplo',	id: 'btnDescarga', 
															handler: function() {
																		var winEjemplo = new Ext.Window ({
																		  height: 200,
																		  x: 400,
																		  y: 100,
																		  width: 300,
																		  modal: true,
																		  closeAction: 'hide',
																		  title: 'Layout ejemplo',
																		  html: '<iframe style="width:100%;height:100%;border:0" src="/nafin/00archivos/15cadenas/ADMON.FED.DE.SERV.EDC.P.D.F.01_07_11.xls"></iframe>'
																		}).show();
																		if (winEjemplo){
																			winEjemplo.hide();
																		}
															}
												},'-',{
													xtype: 'button',text: 'Cerrar',id: 'btnCerrar', handler: function() {if (Ext.getCmp('gridLayout').isVisible()){Ext.getCmp('gridLayout').hide();tp.el.dom.scrollIntoView();}}}
									]
					},
					stripeRows: true,
					loadMask: true,
					height: 600,
					width: 730,
					anchor: '100%',
					title: 'Layout esperado en el archivo de carga para que �ste sea aceptado por el Administrador de cargas Nafin',
					frame: false,
					hidden: true
				}
			]
		}
	];

	var fpCarga = new Ext.form.FormPanel({
		id: 'pnlCarga',
		fileUpload: true,
		width: 750,
		title: 'Carga archivo de Pyme�s.',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 4px',
		style: 'margin: 0 auto',
		labelWidth: 100,
		defaultType: 'textfield',
		items: elementosFormaCarga
	});

	var tp = new Ext.TabPanel({
		activeTab: 0,
		width:940,
		plain:true,
		defaults:{autoHeight: true},
		items:[
			{
				title: 'Carga archivo de Pyme�s.',
				id: 'tabCargaArchivo',
				items: [
					{
						xtype: 'panel',
						id: 'msjCarga',
						width: 690,
						frame: true,
						hidden: true,
						style: 'margin: 0 auto'
					},
					NE.util.getEspaciador(10),
					fpCarga,fpAcuse,
					NE.util.getEspaciador(10)
				]
			},
			{
				title: 'Consulta estatus de carga',
				id: 'tabConsultaCarga',
				items: [
					{
						xtype: 'panel',
						id: 'msjConsulta',
						width: 690,
						frame: true,
						hidden: true,
						style: 'margin: 0 auto'
					},
					NE.util.getEspaciador(10),
					fpConsulta,
					NE.util.getEspaciador(10),
					grid,
					{
						xtype: 	'label',
						id:	 	'leyenda',
						hidden: 	true,
						cls:		'x-form-item',
						style: 	'font color:white;  background-color: #F00; size:1; text-align:left;margin:10px;',
						html:  	'<b>Nota: Los registros marcados en color "ROJO" indican las Cargas de Proveedores eliminados permanentemente</b>'
					},
					NE.util.getEspaciador(10),
					gridCargaEcon
				]
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			tp
		]
	});

catalogoEstatusData.load();

});