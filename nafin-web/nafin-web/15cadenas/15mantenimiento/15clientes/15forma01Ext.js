
Ext.onReady(function(){
	//var sTipoUsuario =  Ext.getDom('sTipoUsuario').value;	
	
//----------------------Handlers-----------------------
 function validaFormulario(forma){
	var bandera = true;
         forma.getForm().items.each(function(f){
			if(f.xtype != 'label'&&f.xtype != undefined&&bandera){
           if(f.isVisible()){
				   if(!f.isValid()){
						f.focus();
						f.markInvalid('Este campo es Obligatorio');
						bandera= false;
					}
           }
			}
        });
        return bandera;
    }

function procesarAlta(opts, success, response) {
		var fp = Ext.getCmp('formaAfiliacion');
			fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			respuesta=Ext.util.JSON.decode(response.responseText);
			
				Ext.Msg.show({
									title:	"Mensaje",
									msg:		"Su n�mero de Nafin Electr�nico: "+respuesta.lsNoNafinElectronico,
									buttons:	Ext.Msg.OK,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											location.reload();
										}
									},
									closable:false
								});

		}else{
			NE.util.mostrarConnError(response,opts);
		}
}

//----------------------Stores--------------------

	function creditoElec(pagina) {
		if(pagina == 0 ) {			//  Cadena Productiva
			window.location = '15forma0Ext.jsp?internacional=N&cboTipoAfiliacion=0';
		}else if (pagina == 1 ) { 	//Intermediario Financiero
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15forma03Ext.jsp"; 
		} 	else if (pagina == 2) { 	// Distribuidor						
			window.location= '/nafin/15cadenas/15mantenimiento/15clientes/15forma19Ext.jsp?TipoPYME=2&cboTipoAfiliacion=2'; 		 
		} else if (pagina == 3) { 	//Proveedor
			window.location = '15forma01Ext.jsp'; 
		} else if (pagina == 4) {		//Afiliados Credito Electronicos 
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma15ext.jsp'; 
		} 	else if (pagina == 5) { 	//Cadena Productiva Internacional						
			window.location = '/nafin/15cadenas/15mantenimiento/15clientes/15forma0Ext.jsp?internacional=S&cboTipoAfiliacion=5';	
		} else if (pagina == 6) { 	//Contragarante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma21Ext.jsp'; 	
		} else if (pagina == 9) { 	// Mandante
			window.location  = '/nafin/15cadenas/15mantenimiento/15clientes/15forma1_manExt.jsp'; 
		} else if (pagina == 10) { 	//Afianzadora
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliacionAfianzadoraExt.jsp"; 	
		}	else if (pagina == 11) { 	//Universidad
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma27Ext.jsp"; 	
		}else if (pagina == 12) { 	//Cliente Externo
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15AfiliaClienteExternoExt.jsp"; 
		}
	}
	
	var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [[/*'EPO'*/  '0','Cadena Productiva']
				,[/*'IF'*/	'1','Intermediario Financiero']
				,[/*'2'*/	'2','Distribuidor']
				,[/*'1'*/	'3','Proveedor']	
				,[/*'ACE'*/	'4','Afiliados Credito Electronico']
				,[/*'EPOI'*/'5','Cadena Productiva Internacional']
				,[/*'CONTRA'*/'6','Contragarante']				
				,[/*'M'*/	'9','Mandante']	
				,[/*'AF'*/	'10','Afianzadora']
				,[/*'UNI'*/ '11','Universidad-Programa Cr�dito Educativo']
				,['12','Cliente Externo']
					
				]
	});
	
	var catalogoPersona = new Ext.data.ArrayStore({
        fields: [
            'clave',
            'descripcion'
        ],
        data: [[1, 'F�sica'], [5, 'Moral']]
    });
	 
	var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15forma01Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
 	var catalogoPais = new Ext.data.JsonStore({
	   id				: 'catPais',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma01Ext.data.jsp',
		baseParams	: { informacion: 'catalogoPais'},
		autoLoad		: false,
		listeners	:
		{
			exception: NE.util.mostrarDataProxyError
		}
  });
  	var catalogoEstado = new Ext.data.JsonStore({
		id				: 'catEstado',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma01Ext.data.jsp',
		baseParams	: { informacion: 'catalogoEstado'	},
		autoLoad		: false,
		listeners	:
		{
		 load: function(){
			
		 
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMunicipio= new Ext.data.JsonStore
	({
		id				: 'catMunicipio',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '15forma01Ext.data.jsp',
		baseParams	: { informacion: 'catalogoMunicipio'	},
		autoLoad		: false,
		listeners	:
		{
		 load: function(){
		
		 
		 },
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	



//--------------------Domicilio--------------------
/*--____ Datos del Contacto NACIONAL  ____--*/
	
	var datosPersona_izq = {
		xtype			: 'fieldset',
		id 			: 'datosPersona_izq',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_persona',
				name			: 'Apellido_paterno',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				allowBlank	: false,
				msgTarget	: 'side',
				width			: 230
			},
			{
				xtype			: 'textfield',
				id          : 'id_nombre_persona',
				name			: 'Nombre',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				msgTarget	: 'side',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0'
			},
			
		
			
			{ 	xtype: 'displayfield',  value: '',width			: 250 }
			//elementos hidden
		
			
		]
	};
		
	var datosPersona_der = {
		xtype			: 'fieldset',
		id 			: 'datosPersona_der',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_persona',
				name			: 'Apellido_materno',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				msgTarget	: 'side',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0'
			},{
				xtype 		: 'textfield',
				name  		: 'R_F_C',
				id    		: 'rfc',
				fieldLabel	: '* R.F.C.',
				maxLength	: 20,
				width			: 150,
				allowBlank 	: false,
				hidden		: false,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
				regex			:/^([A-Z|&amp;]{4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2}|[0][1235679])([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
				regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
				'en el formato NNNN-AAMMDD-XXX donde:<br>'+
				'NNNN:son las iniciales del nombre de la empresa<br>'+
				'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
				'XXX:es la homoclave'
			},
			
			{ 	xtype: 'displayfield',  value: '',width			: 250 }
		]
	};
	
	//--------------Datos Empresa Nacional------------
	var datosEmpresa_izq = {
		xtype			: 'fieldset',
		id 			: 'datosEmpresa_izq',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				name			: 'Razon_Social',
				id				: 'razon_Social',
				fieldLabel	: '* Raz�n Social',
				maxLength	: 100,
				width			: 340,
				hidden		: false,
				msgTarget	: 'side',
				allowBlank	: false
			},
			{
				xtype 		: 'textfield',
				name  		: 'R_F_C',
				id    		: 'rfc_e',
				fieldLabel	: '* R.F.C.',
				maxLength	: 20,
				width			: 150,
				allowBlank 	: false,
				hidden		: 	false,
				margins		: '0 20 0 0',
				msgTarget	: 'side',
                                /*EGOY modifica expresion regular*/
				regex			:/^([A-Z|&amp;]{3,4})-(([0-9][0-9](0?[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))[-]([A-Z0-9]{3}$))$/,
				regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
				'en el formato NNN-AAMMDD-XXX donde:<br>'+
				'NNN:son las iniciales del nombre de la empresa<br>'+
				'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
				'XXX:es la homoclave'
			},
			{ 	xtype: 'displayfield',  value: '',width			: 500 }
			]
		
		};
	var datosEmpresa_der = {
		xtype			: 'fieldset',
		id 			: 'datosEmpresa_der',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{ 	xtype: 'displayfield',  value: '',height:20 },
			
			{ 	xtype: 'displayfield',  value: '',width			: 238 }
			
		]
		};
	/*--____Domicilio____--*/
	
	var domicilioPanel_izq = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_izq',
		border	: false,
		labelWidth	: 150,
		items		: 
		[
			{
				xtype			: 'textfield',
				id          : 'calle_domicilio',
				name			: 'Calle',
				fieldLabel  : '* Calle, No Exterior y No Interior',
				maxLength	: 100,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false

			},{
				xtype				: 'combo',
				id          	: 'id_cmb_pais_domicilio',
				name				: 'Pais',
				hiddenName 		: 'Pais',
				fieldLabel  	: '* Pa�s',
				forceSelection	: true,
				allowBlank		: false,
				triggerAction	: 'all',
				msgTarget	: 'side',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				//emptyText		: 'Seleccione Pa�s',
				width				: 230,
				store				: catalogoPais,
				listeners: {
				
					select: function(combo, record, index) {
						Ext.getCmp('id_cmb_estado_domicilio').reset();
						Ext.getCmp('id_cmb_delegacion_domicilio').reset();
							catalogoEstado.load({
								params : Ext.apply({
									pais : record.json.clave
								})
							});
							
							
					}
				}
				
			},{
				xtype				: 'combo',
				id          	: 'id_cmb_estado_domicilio',
				name				: 'Estado',
				hiddenName 		: 'Estado',
				fieldLabel  	: '* Estado ',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				typeAhead		: true,
				msgTarget	: 'side',
				allowBlank		: false,
				emptyText		:'Seleccione...',
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				store				: catalogoEstado,
				listeners: {
					
					select: function(combo, record, index) {
						 
						 
						 Ext.getCmp('id_cmb_delegacion_domicilio').reset();
						 
						 catalogoMunicipio.load({
							params: Ext.apply({
								estado : record.json.clave,
								pais : (Ext.getCmp('id_cmb_pais_domicilio')).getValue()
							})
						});
						
					}
				}
				
			},{
				xtype			: 'textfield',
				id          : 'Codigo_postal',
				name			: 'Codigo_postal',
				fieldLabel  : '* C�digo Postal',
				allowBlank	: false,
				hidden		: false,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				maxLength	: 5,
				minLength	: 5,
				width			: 90,
				maskRe:		/[0-9]/
			},
			{
				xtype			: 'textfield',
				id          : 'telefono',
				name			: 'Telefono',
				fieldLabel  : '* Tel�fono',
				allowBlank	: false,
				width			: 230,
				margins		: '0 20 0 0',
				maxLength	: 30,
				regex			:/^(?:\+|-)?\d+$/
				
			},
			{
				xtype			: 'textfield',
				id          : 'fax_domicilio',
				name			: 'Fax',
				fieldLabel  : 'Fax',
				width			: 230,
				msgTarget	: 'side',
				maxLength	: 30,
				margins		: '0 20 0 0',
				allowBlank	: true
			},
			{ 	xtype: 'displayfield',  value: '',width			: 250 }
			
			
			
		
		]
	};
		
	var domicilioPanel_der = {
		xtype		: 'fieldset',
		id 		: 'domicilioPanel_der',
		border	: false,
		//title		: 'derecho',
		labelWidth	: 150,
		items		: 
		[
			
			{
				xtype			: 'textfield',
				id          : 'colonia_domicilio',
				name			: 'Colonia',
				fieldLabel  : '* Colonia',
				maxLength	: 100,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{ 	xtype: 'displayfield',  value: '',width			: 250 },
			{
				xtype				: 'combo',
				id          	: 'id_cmb_delegacion_domicilio',
				name				: 'Delegacion_o_municipio',
				hiddenName 		: 'Delegacion_o_municipio',
				fieldLabel  	: '* Delegaci�n o municipio',
				forceSelection	: true,
				allowBlank		: false,
				msgTarget	: 'side',
				triggerAction	: 'all',
				emptyText		:'Seleccione...',
				mode				: 'local',				
				valueField		: 'clave',
				displayField	: 'descripcion',
				width				: 230,
				store				: catalogoMunicipio
				
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_email_legal',
				name			: 'Email',
				fieldLabel: '* E-mail Notificaciones',
				maxLength	: 50,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				vtype			: 'email',
				allowBlank	: true
			}			
			,{ 	xtype: 'displayfield',  value: '',width			: 250 }
		]
	};
	
	/*--____ Datos del Contacto  ____--*/
	
	var datosContacto_izq = {
		xtype			: 'fieldset',
		id 			: 'datosContacto_izq',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_paterno_contacto',
				name			: 'Apellido_paterno_C',
				fieldLabel  : '* Apellido Paterno',
				maxLength	: 25,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				width			: 230,
				allowBlank	: false
			},
			{
				xtype			: 'textfield',
				id          : 'id_nombre_contacto',
				name			: 'Nombre_C',
				fieldLabel  : '* Nombre(s)',
				maxLength	: 30,
				msgTarget	: 'side',
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			
			{
				xtype			: 'textfield',
				id          : 'faxc',
				name			: 'Fax_C',
				fieldLabel  : 'Fax',
				maxLength	: 30,
				msgTarget	: 'side',
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: true
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_emailc',
				name			: 'Email_C',
				fieldLabel: '* E-mail',
				maxLength	: 50,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				vtype			: 'email',
				allowBlank	: false
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_numProv',
				name			: 'Numero_de_cliente',
				fieldLabel: '* N�mero de Proveedor',
				maxLength	: 50,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
			xtype: 'compositefield',
			combineErrors: false,
			forceSelection: false,
			msgTarget: 'side',			
			items: [
						
						{
						xtype: 'checkbox',
						name: 'chkDescuento',
						id: 'descontar',
						hiddenName:'descontar'
						},
						{
						xtype: 'displayfield',
						value: 'Susceptible a Descontar',
						width: 200
						}									
					]
				},
				{
			xtype: 'compositefield',
			combineErrors: false,
			forceSelection: false,
			msgTarget: 'side',
			items: [
						
						{
						xtype: 'checkbox',
						name: 'genera_cuenta',
						id: 'consulta',
						hiddenName:'consulta'
						},
						{
						xtype: 'displayfield',
						value: 'Generar cuenta de consulta',
						width: 200
						}
					]
				},{
			xtype: 'compositefield',
			combineErrors: false,
			forceSelection: false,
			msgTarget: 'side',
			items: [
						
						{
						xtype: 'checkbox',
						name: 'derechos',
						id: 'derechos',
						hiddenName:'derechos'
						},
						{
						xtype: 'displayfield',
						value: 'Susceptible a Cesi�n Derechos ',
						width: 200
						}
					]
				},{
			xtype: 'compositefield',
			combineErrors: false,
			forceSelection: false,
			msgTarget: 'side',
			items: [
						
						{
						xtype: 'checkbox',
						name: 'fianza',
						id: 'fianza',
						hiddenName:'fianza'
						},
						{
						xtype: 'displayfield',
						value: 'Susceptible a Fianza Electr�nica ',
						width: 200
						}
					]
				},
                                
                                {
                                    xtype: 'compositefield',
                                    combineErrors: false,
                                    forceSelection: false,
                                    msgTarget: 'side',
                                    items: [						
                                        {
                                            xtype: 'checkbox',
                                            name: 'chkProvExtranjero',
                                            id: 'chkProvExtranjero',
                                            hiddenName:'chkProvExtranjero'
					},
					{
                                            xtype: 'displayfield',
                                            value: 'Proveedor Extranjero ',
                                            width: 200
					}
                                    ]
				},
                                {
                                    xtype: 'compositefield',
                                    combineErrors: false,
                                    forceSelection: false,
                                    msgTarget: 'side',
                                    items: [						
                                        {
                                            xtype: 'checkbox',
                                            name: 'chkOperaDescAutoEPO',
                                            id: 'chkOperaDescAutoEPO',
                                            hiddenName:'chkOperaDescAutoEPO'
					},
					{
                                            xtype: 'displayfield',
                                            value: 'Descuento Autom�tico EPO ',
                                            width: 200
					}
                                    ]
				},
                                
			{ 	xtype: 'displayfield',  value: '',width			: 250 }	
		]
	};
		
	var datosContacto_der = {
		xtype			: 'fieldset',
		id 			: 'datosContacto_der',
		border		: false,
		labelWidth	: 150,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : 'id_materno_contacto',
				name			: 'Apellido_materno_C',
				fieldLabel  : '* Apellido Materno',
				maxLength	: 25,
				msgTarget	: 'side',
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false
			},
			{
				xtype			: 'textfield',
				id          : 'telc',
				name			: 'Telefono_C',
				msgTarget	: 'side',
				fieldLabel  : '* Tel�fono',
				maxLength	: 30,
				width			: 230,
				margins		: '0 20 0 0',
				allowBlank	: false,
				regex			:/^(?:\+|-)?\d+$/
			},
			
			{
			xtype: 'compositefield',
			combineErrors: false,
			fieldLabel: '* Celular (Clave LADA)',
			forceSelection: false,
			msgTarget: 'side',
			items: [
			{ 	xtype: 'displayfield',  value: '044' },
			{
				// E-mail
				xtype			: 'numberfield',
				id				: 'id_celular',
				name			: 'numeroCelular',
				
				maxLength	: 10,
				width			: 200,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				allowBlank	: false
			}
			]
			},
			{
				// E-mail
				xtype			: 'textfield',
				id				: 'id_emailcC',
				name			: 'Confirmacion_Email_C',
				fieldLabel: '* Confirmaci�n E-mail',
				maxLength	: 50,
				width			: 230,
				msgTarget	: 'side',
				margins		: '0 20 0 0',
				vtype			: 'email',
				allowBlank	: false
			},
			{
						xtype: 'checkbox',
						fieldLabel: 'Sin N�mero de Proveedor',
						name: 'sinProv',
						id: 'sinProv1',
						hiddenName:'sinProv',
						listeners: {
								check: function(check,isCheck){
									if(isCheck){
										Ext.getCmp('id_numProv').allowBlank=true;
										Ext.getCmp('id_numProv').setValue('');
										Ext.getCmp('id_numProv').setDisabled(true);
										
									}else{
										Ext.getCmp('id_numProv').allowBlank=false;
										Ext.getCmp('id_numProv').setDisabled(false);
									}
								}
						}
			},
			{
						xtype: 'checkbox',
						fieldLabel: 'Opera Fideicomiso para  Desarrollo de Proveedores',
						name: 'chkOperaFideicomiso',
						id: 'chkOperaFideicomiso1',
						hiddenName:'chkOperaFideicomiso'						
			},
			{
						xtype: 'checkbox',
						fieldLabel: 'Entidad de Gobierno',
						name: 'chkEntidadGobierno',
						id: 'chkEntidadGobierno1',
						hiddenName:'chkEntidadGobierno'						
			},
			{
						xtype: 'checkbox',
						fieldLabel: 'Opera Factoraje Distribuido',
						name: 'chkFactDistribuido',
						id: 'chkFactDistribuido1',
						hiddenName:'chkFactDistribuido'						
			},
			{ 	xtype: 'displayfield',  value: '',width			: 250 }
		]
	};
	
var elementosForma = [
				{
		xtype		: 'panel',
		id 		: 'afiliacion',
		layout	: 'hbox',
		
		border	: false,		
		width		: 900,		
		bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:110px;',
		items		: 
		[{
			layout		: 'form',
			width			:400,
			labelWidth	: 60,
			border		: false,
			items			:[{
					xtype				: 'combo',
					id					: 'id_afiliacion',
					name				: 'afiliacion',
					hiddenName 		:'afiliacion', 
					fieldLabel		: 'Afiliaci�n',
					width				: 250,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',
					value				: '3',
					store				: storeAfiliacion,
					listeners: {
						select: {
							fn: function(combo) {
								var valor  = combo.getValue();
								creditoElec(valor);		
							}
						}
					}					
				}]
			}
	]},
	{
				xtype: 'combo',
				fieldLabel: 'Persona',
				displayField: 'descripcion',
				//allowBlank: false,
				anchor: '40%',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				value: 1,
				store: catalogoPersona,
				name:'persona',
				id: 'idPersona',
				mode: 'local',
				hiddenName: 'persona',
				forceSelection: true,
				listeners: {
						select: {
							fn: function(combo) {
								
								if(combo.getValue()==1){
									Ext.getCmp('nombreComp').setVisible(true);
									Ext.getCmp('id_paterno_persona').setVisible(true);
									Ext.getCmp('id_nombre_persona').setVisible(true);
									Ext.getCmp('id_materno_persona').setVisible(true);
									Ext.getCmp('rfc').setVisible(true);
									
									
									Ext.getCmp('empresa').setVisible(false);
									Ext.getCmp('razon_Social').setVisible(false);
									Ext.getCmp('rfc_e').setVisible(false);

								}else{
									Ext.getCmp('nombreComp').setVisible(false);
									Ext.getCmp('id_paterno_persona').setVisible(false);
									Ext.getCmp('id_nombre_persona').setVisible(false);
									Ext.getCmp('id_materno_persona').setVisible(false);
									Ext.getCmp('rfc').setVisible(false);
									
									Ext.getCmp('empresa').setVisible(true);
									Ext.getCmp('razon_Social').setVisible(true);
									Ext.getCmp('rfc_e').setVisible(true);
								}
							}
						}
				}
			
					
			},
	{
				xtype: 'combo',
				fieldLabel: '* Cadenas Productivas',
				emptyText: 'Seleccionar una EPO',
				displayField: 'descripcion',
				//allowBlank: false,
				anchor: '93%',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'cboEPO',
				id: 'icEpo',
				mode: 'local',
				hiddenName: 'cboEPO',
				forceSelection: true
			
					
			}
]

//-----------------Formularios-----------------------
var fpDatos = new Ext.form.FormPanel({
		id					: 'formaAfiliacion',
		layout			: 'form',
		width				: 940,
		style				: ' margin:0 auto;',
		frame				: true,
		hidden			: false,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 160,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			elementosForma,
			{
				layout	: 'hbox',
				title 	: 'Nombre Completo',
				id			:	'nombreComp',
				width		: 930,
				items		: [datosPersona_izq	, datosPersona_der	]
			},
			{
				layout	: 'hbox',
				title 	: 'Empresa',
				hidden	: false,
				id			: 'empresa',
				width		: 930,
				items		: [datosEmpresa_izq	, datosEmpresa_izq	]
			}
			,
			{
				layout	: 'hbox',
				title 	: 'Domicilio',
				width		: 930,
				items		: [domicilioPanel_izq	, domicilioPanel_der	]
			},
			{
				layout	: 'hbox',
				title 	: 'Datos del Contacto',
				width		: 930,
				items		: [datosContacto_izq	, datosContacto_der	]
			}
		],
		buttons			: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				handler: function(boton, evento) 
				{
					/*****************Validaciones***************/

					
					if(!validaFormulario(fpDatos)){
					
						return;
					} 
					if(Ext.getCmp('icEpo').getValue() ==""){
						Ext.getCmp('icEpo').markInvalid('Seleccione una Cadena Productiva.');
						Ext.getCmp('icEpo').focus();
						return;
					}
					
					if(!Ext.getCmp('id_email_legal').isValid()){
						Ext.getCmp('id_email_legal').focus();
						return;
					}
					
					
					if(Ext.getCmp('id_emailcC').getValue()!=Ext.getCmp('id_emailc').getValue()){
					
						Ext.getCmp('id_emailcC').focus();
						Ext.getCmp('id_emailcC').markInvalid('El mail y confirmaci�n debe ser iguales');
						return;
					}
					Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							var params = (fpDatos)?fpDatos.getForm().getValues():{};		
							var fp = Ext.getCmp('formaAfiliacion');							
							fp.el.mask("Procesando", 'x-mask-loading');
							//peticion de Registro de Afiliaci�n
							var tp='M';
							if(Ext.getCmp('idPersona').getValue()==1){
								tp='F';
							}
							Ext.Ajax.request({
									url: '15forma01Ext.data.jsp',
									params: Ext.apply(params,{
											informacion: 'AfiliacionSave',
											tp:tp
									}),
									callback: procesarAlta
							});
						}
					});		
				}
			
			},
			{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					location.reload();
				}
			}
		]
	});	
	
	var menuToolbar =  new Ext.Toolbar({
		width: 410,
		style: 'margin:0 auto;',id:'toolbarIM',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'tbbutton',
				id: 'btnIndividual',
				text: '<b>Individual</b>',
				width: 200,
				handler: function(){				
					window.location ='15forma01Ext.jsp';			
				}
			},'-',
			{
				xtype: 'tbbutton',
				text:'Masiva',
				width: 200,
				id: 'btnMasiva',
				handler: function(){
					window.location ='15forma01MasExt.jsp';	
				}
			}
		]
	});
	
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
			menuToolbar,
			NE.util.getEspaciador(10),
			fpDatos,
			NE.util.getEspaciador(10)
		]
  });
  catalogoEpo.load();
  catalogoPais.load();
  Ext.getCmp('empresa').setVisible(false);
  Ext.getCmp('razon_Social').setVisible(false);
	Ext.getCmp('rfc_e').setVisible(false);
});