<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if (informacion.equals("CatalogoEstatusConsulta")){

		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("comcat_estatus_carga_pyme");
		catalogo.setOrden("ic_estatus");
		infoRegresar = catalogo.getJSONElementos();

}else if (informacion.equals("CatalogoMotivo")){

		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_motivo");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("comcat_rechaza_docto");
		catalogo.setOrden("ic_motivo");
		infoRegresar = catalogo.getJSONElementos();

}else if (informacion.equals("CatalogoEstatusCambia")){

		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("comcat_estatus_carga_pyme");
		catalogo.setValoresCondicionIn("2,3,4",Integer.class);
		catalogo.setOrden("ic_estatus");
		infoRegresar = catalogo.getJSONElementos();

}else if (informacion.equals("CatalogoEPOCarga")){

	CatalogoEpoCargaArchivos cat = new CatalogoEpoCargaArchivos();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta")	||	informacion.equals("ArchivoCSV")){

	String claveEpo = (request.getParameter("claveEpo")!=null)?request.getParameter("claveEpo"):"";
	String icFolio = (request.getParameter("icFolio")!=null)?request.getParameter("icFolio"):"";
	String icEstatus = (request.getParameter("icEstatus")!=null)?request.getParameter("icEstatus"):"";
	String fecha_carga_ini = (request.getParameter("fecha_carga_ini")!=null)?request.getParameter("fecha_carga_ini"):"";
	String fecha_carga_fin = (request.getParameter("fecha_carga_fin")!=null)?request.getParameter("fecha_carga_fin"):"";

	int start = 0;
	int limit = 0;
	ConsultaCargaArchivoPyme cargaArchivo = new ConsultaCargaArchivoPyme();
	cargaArchivo.setClave_epo(claveEpo);
	cargaArchivo.setIcFolio(icFolio);
	cargaArchivo.setClave_estatus(icEstatus);
	cargaArchivo.setFecha_carga_ini(fecha_carga_ini);
	cargaArchivo.setFecha_carga_fin(fecha_carga_fin);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(cargaArchivo);

	if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}
			
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
			
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
}else if (informacion.equals("DescargaZIP")){
	
	String vFolios = (request.getParameter("listFolios")!=null)?request.getParameter("listFolios"):"";
	JSONArray jsonArr = JSONArray.fromObject(vFolios);
	List listaFolios = new ArrayList();
	List fols = new ArrayList();
	
	for (int x = 0;x< jsonArr.size();x++){
		fols = new ArrayList();
		fols.add((String)jsonArr.get(x));
		listaFolios.add(fols);
	}
	CargaArchivoPyme cargaArchivo = new CargaArchivoPyme();

	List listaDocs = new ArrayList();
	List listComprime  = new ArrayList();
	
	listaDocs = cargaArchivo.getArchivosCargados(listaFolios, strDirectorioTemp);

	for(int i=0;i<listaDocs.size();i++) {
		List lItem = (ArrayList)listaDocs.get(i);
		listComprime.add((lItem.get(0).toString()));
	}//for

	JSONObject jsonObj = new JSONObject();

	if (listComprime != null && listComprime.size() > 0){
		ComunesZIP zipear = new ComunesZIP();
		String nombreZip = Comunes.cadenaAleatoria(16)+".zip";
		String rutaNombreZip = strDirectorioTemp+nombreZip;
		zipear.comprimir(listComprime,strDirectorioTemp,rutaNombreZip);
		jsonObj.put("nombreZip", nombreZip);
		jsonObj.put("strDirecVirtualTemp", strDirecVirtualTemp);
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("EliminarArchivos")){
	
	JSONObject jsonObj = new JSONObject();
	String vFolios = (request.getParameter("listFolios")!=null)?request.getParameter("listFolios"):"";
	JSONArray jsonArr = JSONArray.fromObject(vFolios);
	List listaFolios = new ArrayList();
	List fols = new ArrayList();
	
	for (int x = 0;x< jsonArr.size();x++){
		fols = new ArrayList();
		fols.add((String)jsonArr.get(x));
		listaFolios.add(fols);
	}
	CargaArchivoPyme cargaArchivo = new CargaArchivoPyme();

	if (listaFolios != null && listaFolios.size() > 0){
		String result = cargaArchivo.eliminaArchivosCargados(listaFolios);
		jsonObj.put("result", result);
	}

	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("CambiarEstatus")){

	JSONObject jsonObj = new JSONObject();
	String vFolios = (request.getParameter("hidFolios")!=null)?request.getParameter("hidFolios"):"";
	String myData = (request.getParameter("hidUsua")!=null)?request.getParameter("hidUsua"):"";
	String icNewStatus = (request.getParameter("cmbCambia")!=null)?request.getParameter("cmbCambia"):"";
	String icMotivo = (request.getParameter("cmbMotivo")!=null)?request.getParameter("cmbMotivo"):"";
	String txtArea = (request.getParameter("txtArea")!=null)?request.getParameter("txtArea"):"";
	
	JSONArray arrUsua = JSONArray.fromObject(myData);
	List usuaMail = new ArrayList();
	for (int x = 0;x< arrUsua.size();x++){
		usuaMail.add((String)arrUsua.get(x));
	}

	JSONArray jsonArr = JSONArray.fromObject(vFolios);
	List listaFolios = new ArrayList();
	List fols = new ArrayList();
	List folioMail = new ArrayList();

	for (int x = 0;x< jsonArr.size();x++){
		fols = new ArrayList();
		fols.add((String)jsonArr.get(x));
		folioMail.add((String)jsonArr.get(x));
		listaFolios.add(fols);
	}
	CargaArchivoPyme cargaArchivo = new CargaArchivoPyme();
	if (listaFolios != null && listaFolios.size() > 0){
		String result = cargaArchivo.cambiarEstatus(listaFolios,icNewStatus,icMotivo,txtArea);
		jsonObj.put("result", result);
		if (result.equals("ok") && icNewStatus.equals("4")){
			String descMotivo = cargaArchivo.getMotivoRechazoDesc(icMotivo);
			String from = "no_response@nafin.gob.mx";
			String to = "";
			Usuario usuario = new Usuario();
			UtilUsr utilUsr = new UtilUsr();
			String conCopia = "";
			String cc = ParametrosGlobalesApp.getInstance().getParametro("EMAIL_CARGA_PROVEEDORES");
			usuario = utilUsr.getUsuario(iNoUsuario);
			
			if(usuario.getEmail()!=null && !"".equals(usuario.getEmail())){
				conCopia = usuario.getEmail();
			}
			cc += (conCopia.length()>1)?(","+conCopia):"";

			StringBuffer contenido;
			if (folioMail != null && folioMail.size() > 0 && usuaMail != null && usuaMail.size() > 0){
				String subject;
				Correo enviaCorreo = new Correo();
				usuario = new Usuario();
				for (int y = 0;y < folioMail.size();y++){
					contenido = new StringBuffer();
					String folio = (String)folioMail.get(y);
					String elUsuario = (String)usuaMail.get(y);
					usuario = utilUsr.getUsuario(elUsuario);
					if(usuario.getEmail()!=null && !"".equals(usuario.getEmail())){
						to = usuario.getEmail();
					}
					subject = "Notificación de Rechazo de base para carga_folio "+folio;
					contenido.append("<div align="+"left"+">Se le informa que la base de proveedores cargada con el Folio "+ folio +" fue rechazada por:");
					contenido.append("<p><b>"+ descMotivo +"</b>");

					if (icMotivo.equals("6")){
						contenido.append("<br><b>"+txtArea+"</b>");
					}
					contenido.append("<p>Solicitamos verificar la base original, corregirla, en su caso, y enviarla nuevamente a trav&eacute;s del sistema NafiNet.");
                                        contenido.append("<br>");
					contenido.append("<p>Cualquier duda al respecto, puede ponerse en contacto con nosotros a trav&eacute;s de los tel&eacute;fonos: 55-53256000 Ext 8099 y/o 8748");
                                        contenido.append("<p>O a la cuenta de correo altabases@nafin.gob.mx en donde tambi&eacute;n recibimos solicitudes para actualizar datos como el n&uacute;mero de proveedor o la raz&oacute;n social (cuando aplique).");
					contenido.append("<p><b>Atentamente<br>Subdirecci&oacute;n de  Administraci&oacute;n de Productos Electr&oacute;nicos<br>Nacional Financiera, S.N.C.</b>");
					contenido.append("<p><i><b>Nota:</b> Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr&oacute;nicos de entrada.");
					contenido.append("<br>Por favor no responda a este mensaje.</i></div>");

					enviaCorreo.enviarTextoHTML(from, to, subject, contenido.toString(),cc);
				}
			}
		}
	}
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("ConsultaCargaEcon")){
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	
	String numProc = request.getParameter("numProc")==null?"":request.getParameter("numProc");
	
	System.out.println("numProcnumProcnumProcnumProcnumProc========================="+numProc);
	
	ConsCargaProvEcon consCarga = new ConsCargaProvEcon();
	List lstConsCargaArchivo = consCarga.getConsCargaEcon(numProc,"","A","","");
	jsObjArray = JSONArray.fromObject(lstConsCargaArchivo);
	
	infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
	
}else if(informacion.equals("GeneraResumenExcel")){
	JSONObject jsonObj = new JSONObject();
	String numProc = request.getParameter("numProc")==null?"":request.getParameter("numProc");
	String c_afil = request.getParameter("c_afil")==null?"":request.getParameter("c_afil");
	String c_reafil = request.getParameter("c_reafil")==null?"":request.getParameter("c_reafil");
	
	ConsCargaProvEcon consCarga = new ConsCargaProvEcon();
	String nombreArchivo = consCarga.generaArchivoPDF(strDirectorioTemp,numProc,"","");
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("nombreArchivo", nombreArchivo);
	
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("GenerarCSV")){
	JSONObject jsonObj = new JSONObject();
	StringBuffer 		contenidoArchivo 	= new StringBuffer();
	CreaArchivo 		archivo 				= new CreaArchivo();
	String 				nombreArchivo 		= null;
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");	
	List lstRegistros =  JSONArray.fromObject(jsonRegistros);
	Iterator itReg = lstRegistros.iterator();
	
	contenidoArchivo.append("Numero de Proceso,EPO,Registros Totales,Registros Con Error,Registros Sin Error,Ya Existentes,Registros Totales,Duplicados,Unicos,Ya Afiliados,Reafiliaciones,Reafiliaciones Auto.,Con Contrato Unico,Por Afiliar,Ya Cargados,Por Cargar,");
	contenidoArchivo.append("Nombre de Archivo,Fecha de Carga,Datos de Carga,Procesado\n");
	
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		
		contenidoArchivo.append(registro.getString("NUMPROC")+","+(registro.getString("EPO")).replace(',',' ')+","+registro.getString("REG_TOTALES")+","+registro.getString("REG_SINERROR_NE")+","+registro.getString("REG_CONERROR_NE")+","+registro.getString("REG_PROV_YA_EXISTENTES_NE")+",");
		contenidoArchivo.append(registro.getString("REGTOTAL")+","+registro.getString("REGDUPLI")+","+registro.getString("REGUNICO")+","+registro.getString("REGYAAFIL")+","+registro.getString("REGREAFIL")+",");
		contenidoArchivo.append(registro.getString("REGREAFILAUT")+","+registro.getString("REGCONVUNICO")+","+registro.getString("REGPORAFIL")+","+registro.getString("REGCARGADOS")+","+registro.getString("REGPORCARGAR")+",");
		contenidoArchivo.append(registro.getString("NOMBREARCH")+","+"'"+registro.getString("FECHACARGA")+","+registro.getString("DATOSCARGA")+","+registro.getString("CSPROCESADO")+"\n");
	}
	
	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
	}else{
		nombreArchivo = archivo.nombre;
	}
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}

System.out.println("infoRegresar============"+infoRegresar); 


%>
<%=infoRegresar%>