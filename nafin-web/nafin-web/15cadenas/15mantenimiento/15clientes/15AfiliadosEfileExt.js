Ext.onReady(function() {
	var cvePerf = ""; 
	var ExpedienteCargado = "";
	var busqAvanzadaSelect=0;
   var rfc='';
   
	//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT		
	
	var procesarCatalogoNombreEPO = function(store, arrRegistros, opts) {
												store.insert(0,new Todas({ 
													clave: "", 
													descripcion: "Seleccionar Todos", 
													loadMsg: ""})); 
												store.commitChanges(); 
												Ext.getCmp('id_noIc_epo').setValue('');		
	}
   
	var procesarCatalogoIF = function(store, arrRegistros, opts) {
												store.insert(0,new Todas({ 
													clave: "", 
													descripcion: "Seleccionar Todas", 
													loadMsg: ""})); 
												store.commitChanges(); 
												Ext.getCmp('id_noIc_if').setValue('');		
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		Ext.getCmp('btnConsultar').enable();
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');			
			
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnBajarPDF').hide();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	

	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
		}
	}
	
	
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton = Ext.getCmp('btnGenerarArchivo');
			boton.enable();
			var boton2 = Ext.getCmp('btnGenerarPDF');
			boton2.enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		store.insert(0,new Todas({ 
			clave: "", 
			descripcion: "Seleccione proveedor", 
			loadMsg: ""
		})); 
		store.commitChanges(); 
		Ext.getCmp('cmb_num_ne').setValue('');		
		Ext.getCmp('btnBuscar2').enable();
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
		//	Ext.getCmp('cmb_num_ne').focus();//txtRfc
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN

//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	Todas = Ext.data.Record.create([ 
		{name: "clave",         type: "string"}, 
		{name: "descripcion",   type: "string"}, 
		{name: "loadMsg",       type: "string"}
	]);
		
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15AfiliadosEfileExt.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'EPO'},
			{name: 'IF'},
			{name: 'NUMERO_SIRAC'},
			{name: 'PYME'},
			{name: 'MONEDA'},
			{name: 'RFC'},
			{name: 'FECHA_PARAMETRIZACION'},
			{name: 'NUM_NAFIN_ELECTRONICO'},
			{name: 'EXPEDIENTE_EFILE'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});

	var catalogoNombreEPO = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '15AfiliadosEfileExt.data.jsp',
		listeners: {		
			load: procesarCatalogoNombreEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoNombreEPO'			
		},
		totalProperty : 'total',
		autoLoad: true
	});
	
		var catalogoIF = new Ext.data.JsonStore({
		id: 'idCatalogoIF',
		//xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15AfiliadosEfileExt.data.jsp',
		listeners: {
			load: procesarCatalogoIF,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: true
	});

	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion','ic_pyme','loadMsg'],
		url : '15AfiliadosEfileExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN  


	var grid = new Ext.grid.GridPanel({ 
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
      sortable: true, 
      columnLines: true,
		hidden: true,
		columns: [{
				header: '<center>EPO</center>', 
				//tooltip: '',
				dataIndex: 'EPO',
				sortable: true,
				width: 320,
				align: 'left'
			},{
				header: '<center>Intermediario Financiero</center>',
				//tooltip: '',
				dataIndex: 'IF',
				sortable: true,	
				width: 320,
				align: 'left'
			},{
				header: 'No SIRAC',
				//tooltip: '',
				dataIndex: 'NUMERO_SIRAC',
				align: 'center',
				width: 90 
			},{
				header: '<center>Nombre o Raz�n Social </center>', 
				//tooltip: '',
				dataIndex: 'PYME',
				sortable: true,	
				align: 'left',
				width: 380
			},{
				header: '<center>Moneda</center>', 
				//tooltip: '',
				dataIndex: 'MONEDA',
				sortable: true,	
				align: 'left',
				width: 130
			},{
				header: '<center>RFC</center>', 
				//tooltip: '',
				dataIndex: 'RFC',
				sortable: true,	
				width: 120,
				align: 'left'
			},{
				header: 'Fecha de<BR>Parametrizaci�n ',
				//tooltip: '',
				dataIndex: 'FECHA_PARAMETRIZACION',
				sortable: true,	
				align: 'center',
				width: 100
				//renderer: RenAcepto
			},{
				header: '<center>No. Nafin<BR>Electr�nico</center>', 
				//tooltip: '',
				dataIndex: 'NUM_NAFIN_ELECTRONICO',
				sortable: true,	
				width: 90, 
				align: 'center'
			},{
				header: '<center>Expediente<BR>Cargado</center>', 
				//tooltip: '',
				dataIndex: 'EXPEDIENTE_EFILE',
				sortable: true,	
				align: 'center',
				width: 90 
			}
		],
		loadMask: true,
		deferRowRender: false,
		height: 430,
		width: 930,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',
		collapsible: false,
		bbar: {
		xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id:'barraPaginacion',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						//boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '15AfiliadosEfileExt.data.jsp',
							params: Ext.apply(paramSubmit,{
								informacion: 'ArchivoCSV'
							}),
							//callback: procesarSuccessFailureGenerarArchivo
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						//boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '15AfiliadosEfileExt.data.jsp',
							params: Ext.apply(paramSubmit,{
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize,
								informacion: 'ArchivoPDF'
							}),
							//callback: procesarSuccessFailureGenerarPDF
							callback: procesarDescargaArchivos
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
	});
	
	var elementosFecha = [
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'id_noIc_if',
			hiddenName : 'noIc_if',
			fieldLabel: 'Intermediario Financiero',
			//emptyText: 'Seleccione IF',
			store: catalogoIF,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			forceSelection: true,
			hidden:false,
			width:300
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Parametrizaci�n',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [			
				{
					xtype: 'datefield',
					name: 'txt_fecha_acep_de',
					allowBlank: false,
					//value:new Date(),
					id: 'df_consultaMin',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0',  //necesario para mostrar el icono de error
					listeners:{
						blur:function(datefield){
								//alert('Hola Mundo');
							var df_consultaNotMin = Ext.getCmp("df_consultaMin");
							var df_consultaNotMax = Ext.getCmp("df_consultaMax");
						
							df_consultaNotMin.clearInvalid();
							df_consultaNotMax.clearInvalid();
						
						}
					}
				},{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 90
				},{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},{
					xtype: 'datefield',
					name: 'txt_fecha_acep_a',
					allowBlank: false,
					id: 'df_consultaMax',
					//value: new Date(),
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0',  //necesario para mostrar el icono de error
					listeners:{
						blur:function(datefield){
								//alert('Hola Mundo');
							var df_consultaNotMin = Ext.getCmp("df_consultaMin");
							var df_consultaNotMax = Ext.getCmp("df_consultaMax");
						
							df_consultaNotMin.clearInvalid();
							df_consultaNotMax.clearInvalid();
						
						}
					}				
				
				},{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 20
				}
			]
		},
		{
			xtype: 'compositefield',
			id: 'cfExpediente',
			hidden: false,
			fieldLabel:'Expediente Cargado',
			msgTarget: 'side',
			items: [
				{
					xtype: 'displayfield',
					value: 'Si	',
					width: 20
				},				
				{ 
					xtype: 'radio',
					name: 'expediente_cargado',
					id: 'radioSi',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											ExpedienteCargado = "S"
										}
									}
					}
				}, 
				{
					xtype: 'displayfield',
					value: '   No ',
					width: 20
				},{ 
					xtype: 'radio',
					name: 'expediente_cargado',
					id: 'radioNo',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											ExpedienteCargado = "N"
										}
									}
					}
				}
				
			]
		}
	];

	var combo=[
	{
			xtype: 'combo',
			name: 'ic_nombreepo',
			id: 'id_noIc_epo',
			hiddenName : 'noIc_epo',
			fieldLabel: 'Nombre de la EPO',
			//emptyText: 'Seleccione EPO',
			store: catalogoNombreEPO,
			forceSelection: true,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			width:300
		}];
	
	var elementosForma = [
		{
			xtype: 'hidden',
			name: 'ic_pyme',
			id: 'ic_pyme'
		},		
		{
			xtype: 'compositefield',
			width: 800,
			fieldLabel: 'Proveedor',
			labelWidth: 200,
			items:[{
				xtype: 'numberfield',
				name: 'txt_nafelec',
				hiddenName: 'txt_nafelec', 
				id:'txt_nafelec',
				decimalPrecision: 0,
				width:90,
				listeners: {
					'blur': function(){
						// Petici�n b�sica  
						Ext.Ajax.request({  
							url: '15AfiliadosEfileExt.data.jsp',  
							method: 'POST',  
							callback: successAjaxFn,  
							params: {  
								 informacion: 'pymeNombre' ,
								 comboEpo: Ext.getCmp('id_noIc_epo').getValue(),
								 txt_nafelec: Ext.getCmp('txt_nafelec').getValue()
							}  
						});  
					}
				}				
			},
			{
				xtype: 'textfield',
				readOnly: true,
				id:'hid_nombre',
				name:'hid_nombre',
				//disabled: true,
				hiddenName : 'hid_nombre',
				mode: 'local',
				resizable: true,
				triggerAction : 'all',
				width: 250
			},{
				xtype: 'compositefield',
				combineErrors: false,
				msgTarget: 'side',
				items: [
				{
						xtype:'displayfield',
						id:'disEspacio',
						text:''
				},{
						xtype: 'button',
						text: 'B�squeda Avanzada',
						id: 'btnAvanzada',
						iconCls:	'icoBuscar',
						handler: function(boton, evento) {
							var winVen = Ext.getCmp('winBuscaA');
								if (winVen){
									Ext.getCmp('fpWinBusca').getForm().reset();
									Ext.getCmp('fpWinBuscaB').getForm().reset();
									Ext.getCmp('cmb_num_ne').store.removeAll();
									Ext.getCmp('cmb_num_ne').reset();
									if(Ext.getCmp('id_noIc_epo').getValue() == '')
										Ext.getCmp('txtNoProveedor').hide();
									else
										Ext.getCmp('txtNoProveedor').show();									
									winVen.show();
								}else{
									var winBuscaA = new Ext.Window ({
										id:'winBuscaA',
										height: 300,
										x: 300,
										y: 100,
										width: 600,
										heigth: 100,
										modal: true,
										closeAction: 'hide',
										items:[{
											xtype:'form',
											id:'fpWinBusca',
											frame: true,
											border: false,
											style: 'margin: 0 auto',
											bodyStyle:'padding:10px',
											defaults: {
												msgTarget: 'side',
												anchor: '-20'
											},
											labelWidth: 140,
											items:[{
												xtype:'displayfield',
												frame:true,
												border: false,
												value:'Utilice el * para b�squeda gen�rica'
											},{
												xtype:'displayfield',
												frame:true,
												border: false,
												value:''
											},{
												xtype: 'textfield',
												name: 'nombre_pyme',
												id:	'txtNombre',
												fieldLabel:'Nombre',
												maxLength:	100
											},{
												xtype: 'textfield',
												name: 'rfc_pyme',
												id:	'txtRfc',
												fieldLabel:'RFC valida',
												maxLength:	20,
												regex			:/^([A-Z|a-z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d]) | ([1-9]{2})([0][2])([0][1-9] | [12][0-8]) )-([a-zA-Z0-9]{3}$)/,
												regexText	:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones '+
												'en el formato NNN-AAMMDD-XXX donde:<br>'+
												'NNN:son las iniciales del nombre y apellido<br>'+
												'AAMMDD: es la fecha de nacimiento menor al dia de hoy<br>'+
												'XXX:es la homoclave',
												listeners:{change:function(field, newValue){  field.setValue(newValue.toUpperCase()); 	}}	
											},{
												xtype: 'textfield',
												name: 'claveProveedor',
												id: 'txtNoProveedor',
												fieldLabel: 'N�mero Proveedor',
												allowBlank: true,
												hidden:true,
												maxLength: 15,
												width: 80
											}
											],
											buttons:[{
												text:'Buscar',
												id: 'btnBuscar2',
												iconCls:'icoBuscar',
												handler: function(boton) {
													/*if(Ext.getCmp('txtNombre').getValue() == '' && Ext.getCmp('txtRfc').getValue() == '' && Ext.getCmp('txtNoProveedor').getValue() == ''){
														Ext.Msg.alert(boton.text,'No existe informaci�n con los criterios determinados');
														return;
													}*/
													if (Ext.getCmp('txtRfc').isValid()){
													boton.disable();
													catalogoNombreData.load({ 
                                          params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues(),
														{epo: Ext.getCmp('id_noIc_epo').getValue()}) 
                                       });
													} else {
														Ext.getCmp('txtRfc').focus();
													}
												}
											},{
												text:'Cancelar',
												iconCls: 'icoLimpiar',
												handler: function() {
													Ext.getCmp('btnAceptar').disable();
													Ext.getCmp('winBuscaA').hide();
												}
											}]
										},{
											xtype:'form',
											frame: true,
											id:'fpWinBuscaB',
											style: 'margin: 0 auto',
											bodyStyle:'padding:10px',
											monitorValid: true,
											defaults: {
												msgTarget: 'side',
												anchor: '-20'
											},
											items:[{
												xtype: 'combo',
												id:	'cmb_num_ne',
												name: 'cmb_num_neName',
												hiddenName : 'cmb_num_neName',
												fieldLabel: 'Nombre',
												displayField: 'descripcion',
												valueField: 'clave',
												triggerAction : 'all',
												//emptyText: 'Seleccione proveedor',
												forceSelection:true,
												allowBlank: false,
												typeAhead: true,
												mode: 'local',
												minChars : 1,
												store: catalogoNombreData,
												tpl : NE.util.templateMensajeCargaCombo,
												listeners: {
													select: function(combo, record, index) {
														if(combo.getValue() == '' ){
															Ext.getCmp('btnAceptar').disable();
														}else{
															Ext.getCmp('btnAceptar').enable();
														}
														busqAvanzadaSelect=index;
													}
												}
											}],
											buttons:[{
												text:'Aceptar',
												iconCls:'aceptar',
												disabled:true,
												id:'btnAceptar',
												//formBind:true,
												handler: function() {
													if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
														var reg = Ext.getCmp('cmb_num_ne').getStore().getAt(busqAvanzadaSelect).get('ic_pyme');
														Ext.getCmp('ic_pyme').setValue(reg);
														var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
														var desc = disp.slice(disp.indexOf(" ")+1);
														Ext.getCmp('txt_nafelec').setValue(Ext.getCmp('cmb_num_ne').getValue());
														Ext.getCmp('hid_nombre').setValue(desc);
														Ext.getCmp('btnAceptar').disable();
														Ext.getCmp('winBuscaA').hide();
													}
												}
											},{
												text:'Cancelar',
												iconCls: 'icoLimpiar',
												handler: function() {	
													Ext.getCmp('btnAceptar').disable();
													Ext.getCmp('winBuscaA').hide();	
												}
											}]
										}]
									}).show();
									if(Ext.getCmp('id_noIc_epo').getValue() == '')
										Ext.getCmp('txtNoProveedor').hide();
									else
										Ext.getCmp('txtNoProveedor').show();									

								}
							}
						}]
				}]
		}
		];
	
	//Forma para hacer la busqueda filtrada
	var fp = new Ext.form.FormPanel({
		xtype: 'form',
		id: 'forma',
		style: ' margin:0 auto;',
		collapsible: false,
		frame:true,
		width: 630,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[combo,elementosForma,elementosFecha],
		buttons: [
			{
				text: 'Buscar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					var df_consultaNotMin = Ext.getCmp("df_consultaMin");
					var df_consultaNotMax = Ext.getCmp("df_consultaMax");		
               var grid              = Ext.getCmp('grid');
					
					var fechaDe =  Ext.util.Format.date(df_consultaNotMin.getValue(),'d/m/Y');  
					var fechaA  =  Ext.util.Format.date(df_consultaNotMax.getValue(),'d/m/Y');  
					
					if(fechaDe == ''){
						df_consultaNotMin.markInvalid('La fecha es requerida');
					   consultaDataGrid.removeAll();
                  grid.hide();						
						return ;
					}
					if(fechaA == ''){
						df_consultaNotMax.markInvalid('La fecha es requerida');
					   consultaDataGrid.removeAll();
                  grid.hide();						
						return ;
					}
					if(!isdate(fechaDe)){
						df_consultaNotMin.markInvalid('La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa');
					   consultaDataGrid.removeAll();
                  grid.hide();						
						return ;
					}
					if(!isdate(fechaA)){
						df_consultaNotMax.markInvalid('La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa');
					   consultaDataGrid.removeAll();
                  grid.hide();						
						return ;
					}
					if(datecomp(fechaDe,fechaA) == 1 ){
						df_consultaNotMin.markInvalid('La fecha inicial no puede ser mayor a la fecha final');
					   consultaDataGrid.removeAll();
                  grid.hide();						
						return ;
					}
					
					var diferenciaEnDias = mtdCalculaPlazo(fechaDe, fechaA);
					if(diferenciaEnDias > 7 ){
						df_consultaNotMin.markInvalid("El periodo m�ximo de consulta es de 7 d�as.");
						consultaDataGrid.removeAll();
                  grid.hide();	
						return;
					}

				/********   *********/
               boton.disable();
					grid.show();
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaDataGrid.load({
						params: Ext.apply(paramSubmit,{
						expediente: ExpedienteCargado,
						operacion: 'Generar',
						start:0,
						limit:15	
					})
				});
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15AfiliadosEfileExt.jsp';
				}
			}
		]
	});//FIN DE LA FORMA


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});

});
