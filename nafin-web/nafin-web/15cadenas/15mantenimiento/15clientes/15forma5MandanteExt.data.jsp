<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,	
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.afiliacion.*,
		com.netro.seguridad.*,
		com.netro.exception.*,
		com.netro.model.catalogos.*,
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
String txtNumElectronico = (request.getParameter("txtNumElectronico")!=null)?request.getParameter("txtNumElectronico"):"";
String txtNombre = (request.getParameter("txtNombre")!=null)?request.getParameter("txtNombre"):"";
String sel_edo = (request.getParameter("sel_edo")!=null)?request.getParameter("sel_edo"):"";
String txtRFC = (request.getParameter("txtRFC")!=null)?request.getParameter("txtRFC"):"";
String sel_EPO = (request.getParameter("sel_EPO")!=null)?request.getParameter("sel_EPO"):"";

String ic_mandante = (request.getParameter("ic_mandante")!=null)?request.getParameter("ic_mandante"):"";
String tipoAfiliado = (request.getParameter("tipoAfiliado")!=null)?request.getParameter("tipoAfiliado"):"";
String tituloF = (request.getParameter("tituloF")!=null)?request.getParameter("tituloF"):"";
String tituloC = (request.getParameter("tituloC")!=null)?request.getParameter("tituloC"):"";
String modificar = (request.getParameter("modificar")!=null)?request.getParameter("modificar"):""; 
String txtLogin = (request.getParameter("txtLogin")!=null)?request.getParameter("txtLogin"):""; 
String txtNombreMandante = (request.getParameter("txtNombreMandante")!=null)?request.getParameter("txtNombreMandante"):"";
String txtEPO = (request.getParameter("txtEPO")!=null)?request.getParameter("txtEPO"):"";
	
										
								
String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
List parametros   =  new ArrayList();	

JSONObject jsonObj = new JSONObject();


int  start= 0, limit =0;


ConMandante paginador = new ConMandante();	
CambioPerfil clase = new  CambioPerfil();

Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);


if(informacion.equals("catalogoEstado") ) {

	CatalogoEstado cat = new CatalogoEstado();
	cat.setCampoClave("ic_estado");
	cat.setCampoDescripcion("cd_nombre"); 
	cat.setClavePais("24");//Mexico clave 24
	cat.setOrden("ic_estado");
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("catalogoEpo") ) {

	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 		
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();	

} else  if(informacion.equals("catalogoPerfil") ){  

	HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  tipoAfiliado  );	
	List perfiles = (List)catPerfil.get("PERFILES");	
	Iterator it = perfiles.iterator();	
	HashMap datos = new HashMap();
	List registros  = new ArrayList();	
	while(it.hasNext()) {
		List campos = (List) it.next();
		String clave = (String) campos.get(0);
		datos = new HashMap();
		datos.put("clave", clave );
		datos.put("descripcion", clave );
		registros.add(datos);
	}
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();


} else  if(informacion.equals("CatEPO_Mandante") ){  
	
	List catEPOm = paginador.getCatalogoEPO(ic_mandante);
	jsonObj.put("registros", catEPOm);
	jsonObj.put("success",  new Boolean(true));	
	jsonObj.put("txtRFC", txtRFC);
	jsonObj.put("txtNombreMandante", txtNombreMandante);
	jsonObj.put("txtNumElectronico", txtNumElectronico);
	jsonObj.put("ic_mandante", ic_mandante);
   infoRegresar = jsonObj.toString();	
		
}else if (informacion.equals("Consultar") ||  informacion.equals("GeneraArchivoPDF")	||  informacion.equals("GeneraArchivoCSV")) {

	paginador.setTxtNumElectronico(txtNumElectronico);
	paginador.setTxtNombre(txtNombre);
	paginador.setSel_edo(sel_edo);
	paginador.setTxtRFC(txtRFC);
	paginador.setSel_EPO(sel_EPO);
		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if(informacion.equals("Consultar") ||  informacion.equals("GeneraArchivoPDF") ) {
		
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
					
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	
		if(informacion.equals("Consultar")) {
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);										
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
				
			jsonObj = JSONObject.fromObject(consulta);				
	
		}else  if ( informacion.equals("GeneraArchivoPDF") ) {
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
			
	}else  if ( informacion.equals("GeneraArchivoCSV") ) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}			
	}
	
	infoRegresar = jsonObj.toString();	

}else  if (informacion.equals("ConsCuentas") ||   informacion.equals("ImprimirConsCuentas")  ) {
	
	UtilUsr utilUsr = new UtilUsr();
	List cuentas = utilUsr.getUsuariosxAfiliado(ic_mandante, tipoAfiliado);
	Iterator itCuentas = cuentas.iterator();	
	
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	
	if (informacion.equals("ConsCuentas"))  {
		while (itCuentas.hasNext()) {
			String cuenta = (String) itCuentas.next();		
			datos = new HashMap();		
			datos.put("CUENTA", cuenta );				
			registros.add(datos);
		}	
			
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		
	}else  if( informacion.equals("ImprimirConsCuentas") )  {  
	
		String nombreArchivo = paginador.imprimirCuentas(request, cuentas, tituloF, tituloC,strDirectorioTemp );		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	}
	
	jsonObj.put("tituloC", tituloC);	
	jsonObj.put("tituloF", tituloF);	
	infoRegresar = jsonObj.toString();

} else  if( informacion.equals("informacionUsuario") ){  
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true));
	
	HashMap catPerfil = clase.getConsultaUsuario( txtLogin,  "S"  );	
	
	if(catPerfil.size()>0){
	
		List perfiles = (List)catPerfil.get("PERFILES");
	
		String lblNombreEmpresa = (String)catPerfil.get("EMPRESA");
		String lblNafinElec = (String)catPerfil.get("NAELECTRONICO");
		String lblNombre = (String)catPerfil.get("NOMBRE_USUARIO");
		String lblApellidoPaterno = (String)catPerfil.get("APELLIDO_PATERNO");
		String lblApellidoMaterno = (String)catPerfil.get("APELLIDO_MATERNO");
		String lblEmail = (String)catPerfil.get("EMAIL");
		String lblPerfilActual = (String)catPerfil.get("PERFIL_ACTUAL");
		String internacional = (String)catPerfil.get("INTERNACIONAL");
		String sTipoAfiliado = (String)catPerfil.get("TIPOAFILIADO");		
			
		jsonObj.put("lblNombreEmpresa", lblNombreEmpresa);
		jsonObj.put("lblNafinElec", lblNafinElec);
		jsonObj.put("lblNombre", lblNombre);
		jsonObj.put("lblApellidoPaterno", lblApellidoPaterno);
		jsonObj.put("lblApellidoMaterno", lblApellidoMaterno);
		jsonObj.put("lblEmail", lblEmail);
		jsonObj.put("lblPerfilActual", lblPerfilActual);
		jsonObj.put("txtLogin", txtLogin);
		jsonObj.put("mensaje", mensaje);	
		
		jsonObj.put("internacional", internacional);
		jsonObj.put("sTipoAfiliado", sTipoAfiliado);	
		jsonObj.put("txtNafinElectronico", lblNafinElec);	
		jsonObj.put("txtPerfilAnt", lblPerfilActual);	
		jsonObj.put("modificar", modificar);
				
	}else  {
		mensaje = "No se encontró el usuario con la clave especificada, por favor vuelva a intentarlo";
		jsonObj.put("mensaje", mensaje);
	}
	
	infoRegresar = jsonObj.toString();

} else  if( informacion.equals("ModifiarPerfil") ){ 

	String txtPerfil = (request.getParameter("txtPerfil")==null)?"":request.getParameter("txtPerfil");
	String txtNafinElectronico = (request.getParameter("txtNafinElectronico")==null)?"":request.getParameter("txtNafinElectronico");
	String txtPerfilAnt = (request.getParameter("txtPerfilAnt")==null)?"":request.getParameter("txtPerfilAnt");
	String internacional = (request.getParameter("internacional")==null)?"":request.getParameter("internacional");
	String txtTipoAfiliado = (request.getParameter("sTipoAfiliado")==null)?"":request.getParameter("sTipoAfiliado");
	String clave_usuario = (String)session.getAttribute("Clave_usuario");
	String txtLoginC = (request.getParameter("txtLoginC") != null) ? request.getParameter("txtLoginC") : "";
	
	mensaje =  clase.getConsultaUsuario( txtLoginC,  txtPerfil, txtPerfilAnt,  txtNafinElectronico ,  internacional,  txtTipoAfiliado, 	 clave_usuario ) ;

	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 	
	jsonObj.put("mensaje", mensaje);
	infoRegresar = jsonObj.toString();	

}else  if (informacion.equals("Borrar") ) {

	try {
		msg = BeanAfiliacion.BorrarMandate(ic_mandante);		
	} catch(NafinException lexError) {
		msg = lexError.getMsgError();		
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("msg", msg);	
	infoRegresar = jsonObj.toString();

}else  if (informacion.equals("ConsultaReafiliacion") ) {

   txtNumElectronico = (request.getParameter("txtNumElectronico_2")!=null)?request.getParameter("txtNumElectronico_2"):"";
	txtNombreMandante = (request.getParameter("txtNombreMandante_2")!=null)?request.getParameter("txtNombreMandante_2"):"";
	txtRFC = (request.getParameter("txtRFC_2")!=null)?request.getParameter("txtRFC_2"):"";
	
	JSONArray registros = new JSONArray();
	
	parametros   =  new ArrayList();	
	parametros.add(txtNumElectronico);
	parametros.add(ic_mandante);
	parametros.add(txtNombreMandante);
	parametros.add(txtRFC);
	parametros.add(txtEPO);
	
	List registosL =   paginador.getConsultaReafiliacion( parametros );
	registros = JSONArray.fromObject(registosL);
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";					

	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("registros", registros);	
	jsonObj.put("claveEpos", txtEPO);
	infoRegresar = jsonObj.toString();
	
	
}else  if (informacion.equals("AgregarReafiliacion") ) {
	
	String  claveEpos = (request.getParameter("claveEpos")!=null)?request.getParameter("claveEpos"):"";
	
	try {
	 msg = BeanAfiliacion.reafiliarMandate(ic_mandante,claveEpos);	
	 msg = "La actualizacion se llevo a cabo con exito.";
	 
	} catch (Exception e) {
		 msg = "La actualizacion no se llevo a cabo.";	
	} 
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", msg);		
	infoRegresar = jsonObj.toString();
	
	
	
}
%>
<%=infoRegresar%>

