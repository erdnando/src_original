	<%@ page contentType="application/json;charset=UTF-8" import="   
  com.netro.anticipos.*,
	java.util.*,
	com.netro.exception.*,  
	com.netro.model.catalogos.*,
	netropology.utilerias.usuarios.*,   
	net.sf.json.JSONObject,
	com.netro.descuento.*,
	com.netro.cadenas.IFPublicacionProveedores,
	com.netro.cadenas.*,
	org.apache.commons.beanutils.BeanUtils,
	net.sf.json.JSONArray,
	com.netro.afiliacion.*"
	errorPage="/00utils/error_extjs.jsp"
%>
	
<%@ include file="/15cadenas/015secsession.jspf" %>


<%
	//Variables
	CQueryHelper queryHelper;
	String informacion            =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String operacion              =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
	
	
	String txt_nafelec						= (request.getParameter("txt_nafelec")==null)?"":request.getParameter("txt_nafelec");
	String ic_if									= (request.getParameter("noIc_if")==null)?"":request.getParameter("noIc_if");
	String ic_pyme								= (request.getParameter("txt_ne")==null)?"":request.getParameter("txt_ne");
	String rfc										= (request.getParameter("rfc")==null)?"":request.getParameter("rfc");
	String nom_pyme								= (request.getParameter("nombre")==null)?"":request.getParameter("nombre");
	String ic_epo									= (request.getParameter("HicEpo")==null)?"":request.getParameter("HicEpo");
	String fechaIni								= (request.getParameter("dFechaRegIni")==null)?"":request.getParameter("dFechaRegIni");
	String fechaFin								= (request.getParameter("dFechaRegFin")==null)?"":request.getParameter("dFechaRegFin");
	String num_pyme								= (request.getParameter("num_pyme")==null)?"":request.getParameter("num_pyme");
	
	String infoRegresar           = "";
	AccesoDB 	con;
	JSONObject jsonObj = new JSONObject();
	
	
	
	
	
	if(informacion.equals("catologoEpo")){
	
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		infoRegresar=catalogo.getJSONElementos();
		
	} else if(informacion.equals("CatalogoBuscaAvanzada")){		
		//InstanciaciÃ³n para uso del EJB
			CatalogoPymeBusquedaAvanzada cat= new CatalogoPymeBusquedaAvanzada();
			cat.setCampoClave("crn.ic_nafin_electronico");
			cat.setCampoDescripcion("p.cg_razon_social");
			cat.setIc_epo(ic_epo);
			cat.setNum_pyme(num_pyme);
			cat.setRfc_pyme(rfc);
			cat.setNombre_pyme(nom_pyme);
			cat.setIc_pyme(ic_pyme);	
			infoRegresar=cat.getJSONElementos();
	} else if(informacion.equals("Consulta")){
	
			BiSolicAfilia pagina=new BiSolicAfilia();
		
			pagina.setClave_if(iNoCliente);
			pagina.setIc_pyme(ic_pyme);
			pagina.setIc_epo(ic_epo);
			pagina.setDfSol_ini(fechaIni);
			pagina.setDfSol_fin(fechaFin);

			CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(pagina);
			Registros registros=cqhelper.doSearch();
			HashMap	datos;
			List reg=new ArrayList();
			while(registros.next()){
						datos=new HashMap();
						datos.put("EPO",registros.getString("razon_social_epo"));
						datos.put("RAZON",registros.getString("razon_social_pyme"));
						datos.put("RFC",registros.getString("rfc"));
						datos.put("DOMICILIO",registros.getString("domicilio"));
						datos.put("CONTACTO",registros.getString("contacto"));
						datos.put("TELEFONO",registros.getString("telefono"));
						datos.put("FECHA",registros.getString("fecha"));
						reg.add(datos);
					}
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("registros", reg);
			infoRegresar=jsonObj.toString();
			
	
	} else if (informacion.equals("ArchivoCSV")) {
				 try {
										  
							BiSolicAfilia pagina=new BiSolicAfilia();
							pagina.setClave_if(iNoCliente);
							pagina.setIc_pyme(ic_pyme);
							pagina.setIc_epo(ic_epo);
							pagina.setDfSol_ini(fechaIni);
							pagina.setDfSol_fin(fechaFin);
				
							CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(pagina);
	
							String nomArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
							jsonObj.put("success", new Boolean(true));
							jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
							infoRegresar = jsonObj.toString();
					} catch(Throwable e) {
					 throw new AppException("Error al generar el archivo CSV", e);
					}
	}else if (informacion.equals("ArchivoPDF")) {
					 try {
										  
							BiSolicAfilia pagina=new BiSolicAfilia();
							pagina.setClave_if(iNoCliente);
							pagina.setIc_pyme(ic_pyme);
							pagina.setIc_epo(ic_epo);
						
							pagina.setDfSol_ini(fechaIni);
							pagina.setDfSol_fin(fechaFin);
				
							CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(pagina);
	
							String nomArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp,"PDF");
							jsonObj.put("success", new Boolean(true));
							jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
							infoRegresar = jsonObj.toString();
						} catch(Throwable e) {
									 throw new AppException("Error al generar el archivo PDF", e);
						}
					}else if(informacion.equals("nafinElec")){
					
					StringBuffer strSQLPyme2 = new StringBuffer();
					StringBuffer strSQLPyme = new StringBuffer();
					 PreparedStatement ps = null;
					 
					con  = new AccesoDB();
					 boolean resultado = true;
					try { 
						con.conexionDB();
						
						//esto es para que al capturar el numero de Nafin electronico  se muestre el nombre del proveedor	
							String ic_pyme2="";
							String txt_nombre="";
							strSQLPyme2.append(" Select n.ic_epo_pyme_if  as Pyme,  p.cg_razon_social as nombre  ");
							strSQLPyme2.append(" FROM comrel_nafin n, comcat_pyme p ");
							strSQLPyme2.append(" where n.ic_epo_pyme_if = p.ic_pyme AND ic_nafin_electronico = ? ");
							System.out.println("**queryPyme* 2 :::*    "+strSQLPyme2);
							System.out.println("**txt_nafelec :::*    "+txt_nafelec);
							ps = con.queryPrecompilado(strSQLPyme2.toString());
							ps.setInt(1,Integer.parseInt(txt_nafelec));
							ResultSet rsPyme = ps.executeQuery();
							if(rsPyme.next()) 	{
								ic_pyme2 = (rsPyme.getString("Pyme") == null) ? "" : rsPyme.getString("Pyme");
								txt_nombre = (rsPyme.getString("nombre") == null) ? "" : rsPyme.getString("nombre");
							}
							jsonObj.put("ic_pyme",ic_pyme2 );
							jsonObj.put("txt_nombre",txt_nombre );
							rsPyme.close();	
							ps.close();
						
					}catch(Exception e){
						resultado = false;
						e.printStackTrace();
						
					} finally {
						jsonObj.put("success", new Boolean(true));
							
							infoRegresar = jsonObj.toString();
						if(con.hayConexionAbierta()){
							
							con.terminaTransaccion(resultado);
							con.cierraConexionDB();
						}
					}				 
				}
				
%>

<%=infoRegresar%>