<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	java.sql.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	netropology.utilerias.*,
	com.netro.seguridadbean.*,	
	com.netro.afiliacion.*,
	java.io.*,	
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 

/*** OBJETOS ***/
BitacoraAceptacionConvenioUnico paginador;
CQueryHelperRegExtJS queryHelperRegExtJS;
CQueryHelper queryHelper;
CatalogoSimple cat;
JSONObject jsonObj;
Afiliacion afiliacion;
Registros registros;
JSONArray jsonArr;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String ic_consecutivo 	= (request.getParameter("ic_consecutivo") == null)?"":request.getParameter("ic_consecutivo");
String ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String txt_nombre			= (request.getParameter("hid_nombre")==null)?"":request.getParameter("hid_nombre");
String txt_nafelec		= (request.getParameter("txt_nafelec")==null)?"":request.getParameter("txt_nafelec");
String txt_usuario		= (request.getParameter("txt_usuario")==null)?"":request.getParameter("txt_usuario");
String txt_fecha_acep_de= (request.getParameter("txt_fecha_acep_de")==null)?"":request.getParameter("txt_fecha_acep_de");
String txt_fecha_acep_a	= (request.getParameter("txt_fecha_acep_a")==null)?"":request.getParameter("txt_fecha_acep_a");
String noBancoFondeo		= (request.getParameter("noBancoFondeo")==null)?"":request.getParameter("noBancoFondeo");
String hidAction			= (request.getParameter("hidAction")==null)?"":request.getParameter("hidAction");
String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String ic_epo				= (request.getParameter("noIc_epo")==null)?"":request.getParameter("noIc_epo");
String cvePerf = "";

	//SeguridadHome seguridadHome =(SeguridadHome)ServiceLocator.getInstance().getEJBHome("SeguridadEJB",SeguridadHome.class);
	//com.netro.seguridadbean.Seguridad BeanSegFacultad = seguridadHome.create();
	
	com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);

	try{
		cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);
	}catch(Exception e){
		System.err.println(e);
	}
	
	String ic_if = "";
	if(!cvePerf.equals("4"))
		ic_if	=  iNoCliente;
	else
		ic_if = (request.getParameter("noIc_if")==null)?"":request.getParameter("noIc_if");

	//request
	String resultado			= null;
	String epoDefault 		= "";
	String qrySentencia		= "";
	String condicion 			= "";
	int start					= 0;
	int limit 					= 2;
	AccesoDB con 				= null;
	PreparedStatement ps 	= null;
	ResultSet rs 				= null;
/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/


	if(informacion.equals("initPage")) {

			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("cvePerf",cvePerf);
			System.out.println("*** es "+jsonObj.toString());
			resultado = jsonObj.toString();	
	}

/*** INICIO CATALOGO DE BANCO FONDEO ***/
	else if(informacion.equals("catalogoBancoFondeo")){
	cat = new CatalogoSimple();
	
	cat.setTabla("comcat_banco_fondeo");
	cat.setCampoClave("ic_banco_fondeo");
	cat.setCampoDescripcion("cd_descripcion");
	
	switch(!cvePerf.equals("")?Integer.parseInt(cvePerf.trim()):0){
		case 8 : cat.setCondicionIn("2", "ic_banco_fondeo"); break;
		case 4 : cat.setCondicionIn("1", "ic_banco_fondeo"); break;
		default  : cat.setOrden("1");
	}
	resultado = cat.getJSONElementos();
} /*** FIN DEL CATALOGO DE BANCO FONDEO***/

/*** INICIO CATALOGO NOMBRE EPO ***/
else if(informacion.equals("catalogoNombreEPO")){
	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setClaveIf(ic_if);
	resultado = catalogo.getJSONElementos();		
} /*** FIN CATALOGO NOMBRE EPO ***/
else	if (informacion.equals("CatalogoIF")){
	//System.out.println("Entra a catalogoif");
	CatalogoIF catalogo = new CatalogoIF();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setG_convenio_unico("S");
	//catalogo.setClaveIf(ic_if);
	resultado = catalogo.getJSONElementos();		
	}
/*** INICIO PYME NOMBRE ***/
/*else if(informacion.equals("pymeNombre")) {
	try{
		con = new AccesoDB();
		con.conexionDB();
		ic_epo=iNoCliente;
		queryHelper = new CQueryHelper(new ClausuladoNafinCA());
		queryHelper.cleanSession(request);
	
		if(!"".equals(txt_nafelec) && "".equals(ic_pyme)) {
			String mensaje = "";
			if(!"".equals(ic_if)) {
				condicion = " AND con.ic_if = ?"  ;
			}
			qrySentencia =
				" SELECT pym.ic_pyme, pym.cg_razon_social"   +
				"   FROM comrel_nafin crn, comrel_pyme_epo cpe, comcat_pyme pym,  com_aceptacion_contrato_if con"   +
				"   WHERE crn.ic_epo_pyme_if = cpe.ic_pyme"   +
				"   AND cpe.ic_pyme = pym.ic_pyme"   +
				"   AND crn.ic_nafin_electronico = ?"   +
				"   AND crn.cg_tipo = 'P'"   +
				"   AND cpe.cs_habilitado = 'S'"   +
				condicion;
				
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(txt_nafelec));
								
			if(!"".equals(ic_if)){
				ps.setInt(2,Integer.parseInt(ic_if));
			}
			
			rs = ps.executeQuery(); 
			
			if(rs.next()){
				ic_pyme = rs.getString(1)==null?"":rs.getString(1);
				txt_nombre = rs.getString(2)==null?"":rs.getString(2);
			} 
			
			rs.close();
			ps.close();
		}
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("pyme", txt_nombre);
	jsonObj.put("ic_pyme",ic_pyme);
	resultado = jsonObj.toString();		
} /*** FIN PYME NOMBRE ***/

else if(informacion.equals("pymeNombre")) {
	ic_epo				= (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
	
	//ParametrosDescuentoHome parametrosDescuentoHome =(ParametrosDescuentoHome)ServiceLocator.getInstance().getEJBHome("ParametrosDescuentoEJB",ParametrosDescuentoHome.class);
	//ParametrosDescuento parametrosDescuento = parametrosDescuentoHome.create();
	ParametrosDescuento parametrosDescuento  = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
	
	String nombrePyme = parametrosDescuento.getNombrePyme(txt_nafelec,ic_epo);	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	String mensaje = "";
	jsonObj.put("pyme",nombrePyme);

	resultado = jsonObj.toString();	
}
/*** INICIO CONSULTA ***/
else if(informacion.equals("Consulta")) { 
	try {
		start = Integer.parseInt((request.getParameter("start")==null)?"0":request.getParameter("start"));
		limit = Integer.parseInt((request.getParameter("limit")==null)?"15":request.getParameter("limit"));				
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	paginador = new com.netro.cadenas.BitacoraAceptacionConvenioUnico();
	String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
	/* SE PASAN LOS VALORES AL OBJETO PAGINADOR PARA REALIZAR LA CONSULTA */
	paginador.setClave_pyme(ic_pyme);
	paginador.setNumero_nafele(txt_nafelec);
	paginador.setNombre_pyme(txt_nombre);
	paginador.setFecha_convenio_unico_ini(txt_fecha_acep_de);
	paginador.setFecha_convenio_unico_fin(txt_fecha_acep_a);
	paginador.setClave_if(ic_if);
	paginador.setClave_epo(ic_epo);
	paginador.setBanco_fondeo(noBancoFondeo);
	queryHelperRegExtJS	= new CQueryHelperRegExtJS(paginador); 
	
	if((informacion.equals("Consulta") && operacion.equals("Generar")) || (informacion.equals("Consulta") && operacion.equals(""))){ 
		try {
			if(operacion.equals("Generar")){ //Nueva consulta
				queryHelperRegExtJS.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			String cadena = queryHelperRegExtJS.getJSONPageResultSet(request,start,limit);			
			resultado	= cadena;
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if(operacion.equals("XLS")){
		jsonObj = new JSONObject();
		String nombreArchivo = queryHelperRegExtJS.getCreateCustomFile(request, strDirectorioTemp,operacion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
	}else if(operacion.equals("PDF")){
			jsonObj = new JSONObject();
			String nombreArchivo = queryHelperRegExtJS.getCreatePageCustomFile(request, start,limit, strDirectorioTemp,operacion);
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			resultado = jsonObj.toString();
	}else if(operacion.equals("Clausulado")){
	/*
		paginador = new BitacoraAceptacionConvenioUnico();
		paginador.setClave_pyme(ic_pyme);
		paginador.setClave_if(ic_if);
		paginador.setClave_epo(ic_epo);
		paginador.setOper("Clausulado");
		queryHelperRegExtJS	= new CQueryHelperRegExtJS( paginador ); 
		jsonObj = new JSONObject();
		String nombreArchivo = queryHelperRegExtJS.getCreateCustomFile(request, strDirectorioTemp,operacion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
	*/
	}	
} /*** FIN DE CONSULTA ***/

/*** INICIA CATALOGO DE BUSQUEDA AVANZADA ***/
else if (informacion.equals("CatalogoBuscaAvanzada")) {
	String ic_producto_nafin 	= (request.getParameter("ic_producto_nafin")==null)?"":request.getParameter("ic_producto_nafin");
   //String ic_epo 					= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
   ic_if 					      = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
   //String ic_pyme 				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
   String cg_pyme 				= (request.getParameter("cg_pyme")==null)?"":request.getParameter("cg_pyme");
   String envia 					= (request.getParameter("envia")==null)?"":request.getParameter("envia");
   String nombre_pyme  			= (request.getParameter("nombre_pyme")==null)?"":request.getParameter("nombre_pyme");
   String rfc_pyme 				= (request.getParameter("rfc_pyme")==null)?"":request.getParameter("rfc_pyme");
   String num_pyme 				= (request.getParameter("num_pyme")==null)?"":request.getParameter("num_pyme");
   String muestraBotonAceptar = (request.getParameter("muestra_boton_aceptar")==null)?"false":request.getParameter("muestra_boton_aceptar");
   
   String ret_num_pyme 			= (request.getParameter("ret_num_pyme")==null)?"false":request.getParameter("ret_num_pyme");
   String pantalla 			= (request.getParameter("pantalla")==null)?"":request.getParameter("pantalla");
   String nafin_electronico 	= "";
   String nombre 				= "";
   boolean descripcionConNumeroProveedor = false;
   if( ret_num_pyme.equals("true") ){
      descripcionConNumeroProveedor = true;
   }
   
   /* Variables de los datos de las pymes */
   Vector	vecFilas 	= new Vector();
   Vector	vecColumnas = null;
   int i=0;
   String cadenaPyme = "";
   //_________________________ INICIO DEL PROGRAMA _________________________
      if (envia.equals("CONSULTAR")) {
         /**
         *  getProveedores(ic_producto_nafin, ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pyme,pantalla,ic_if);
         **/
            String				condicionDoc	= "";
            String				tablaDoc		= "";
            Vector vecRows = new Vector();
            Vector vecCols = null;
            String campoProveedor = "0"; //Geag
            
            try {
               con = new AccesoDB();
               con.conexionDB();
               if(!"".equals(ic_pyme)) {
                  condicion = "";
         
                  if(!"".equals(ic_epo)) {
                     condicion += "    AND r.ic_epo = ?";
                     campoProveedor = "r.cg_pyme_epo_interno"; //Geag
                  } else {
                     //Si la EPO es nula, entonces el numero de proveedor no tiene sentido
                     //por lo cual se pone un valor fijo para poder hacer un distinct
                     campoProveedor = "0"; //Geag
                  }
                  
                  qrySentencia = 
                     " SELECT /*+index(crn CP_COMREL_NAFIN_PK) use_nl(r p)*/"   +
         //				"        p.ic_pyme, r.cg_pyme_epo_interno num_prov, p.cg_razon_social,"   +
                     "        distinct p.ic_pyme, " + campoProveedor + " as num_prov, p.cg_razon_social,"   +	//Geag
                     "        crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega, "   +
                     "        crn.ic_nafin_electronico"   +
                     "   FROM comrel_nafin crn, comrel_pyme_epo r, comcat_pyme p"   +
                     "  WHERE crn.ic_epo_pyme_if = p.ic_pyme"   +
                     "    AND p.ic_pyme = r.ic_pyme"   +
                     "    AND crn.cg_tipo = 'P'"   +
                     "    AND p.ic_pyme = ?"   +
                     "    AND r.ic_pyme = ?"+condicion;
         //			System.out.println("\n qrySentencia: "+qrySentencia);
                  if(!ic_if.equals("")){
                     qrySentencia +=  
                        " AND p.ic_pyme in ( select "  +
                        "	      distinct "  +
                        "		      cuenta.ic_pyme "  + 
                        "     from  "  +
                        "	      comrel_pyme_if rel, "  +
                        "	      comrel_cuenta_bancaria cuenta "  +
                        "     where "  +
                        "	      rel.cs_vobo_if = 'S' and "  + 
                        "	      rel.cs_borrado = 'N' and "  +
                        "	      rel.ic_if      =  ?  and "  +
                        "	      rel.ic_cuenta_bancaria = cuenta.ic_cuenta_bancaria and "  +
                        "	cuenta.cs_borrado = 'N' ) ";
                  }
         
                  System.out.println(qrySentencia);
                  ps = con.queryPrecompilado(qrySentencia);
                  ps.setInt(1,Integer.parseInt(ic_pyme));
                  ps.setInt(2,Integer.parseInt(ic_pyme));
                  int cont = 2;
                  if(!"".equals(ic_epo)){
                     cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
                  }
                  if(!"".equals(ic_if)){ 
                     cont++;ps.setString(cont,ic_if);
                  }
               } else {
                  condicion = "";
                  
                  if(!"".equals(ic_epo)) {
                     condicion += "    AND pe.ic_epo = ?";
                     campoProveedor = "pe.cg_pyme_epo_interno"; //Geag
                  } else {
                     //Si la EPO es nula, entonces el numero de proveedor no tiene sentido
                     //por lo cual se pone un valor fijo para poder hacer un distinct
                     campoProveedor = "0"; //Geag
                  }
                  if(!"".equals(num_pyme))
                     condicion += "    AND cg_pyme_epo_interno LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
                  if(!"".equals(rfc_pyme))
                     condicion += "    AND cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
                  if(!"".equals(nombre_pyme))
                     condicion += "    AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
                  if(!("2".equals(ic_producto_nafin)||"5".equals(ic_producto_nafin)) && !pantalla.equals("listReq") && !pantalla.equals("consSinNumProv")) {
                     condicionDoc = 
                        "    AND d.ic_pyme = pe.ic_pyme"   +
                        "    AND d.ic_epo = pe.ic_epo";
                     tablaDoc = "com_documento d,";
                  }
                  
                  if( pantalla.equals("MantenimientoDoctos") || pantalla.equals("InformacionDocumentos") || pantalla.equals("CambioEstatus") ) {
                     condicionDoc = 
                        "		AND PE.cs_habilitado 			= 	'S' " +
                        "		AND P.cs_habilitado 				= 	'S' " +
                        "		AND P.cs_invalido 				!= 'S' " +
                        "		AND d.ic_pyme 	= pe.ic_pyme			 " +
                        "		AND pe.cs_num_proveedor 		= 'N'  " +  // Tiene CG_PYME_EPO_INTERNO
                        "		AND d.ic_epo 	= pe.ic_epo";
                     tablaDoc = "com_documento d,";
                  }
                  
                  qrySentencia = 
                     " SELECT /*+index(pe CP_COMREL_PYME_EPO_PK) index(p CP_COMCAT_PYME_PK) use_nl(pe d p)*/ " +
         //				"        p.ic_pyme, pe.cg_pyme_epo_interno, p.cg_razon_social," +
                     "        distinct p.ic_pyme, " + campoProveedor + " as cg_pyme_epo_interno, p.cg_razon_social," + //GEAG
                     "        crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega," +
                     "        crn.ic_nafin_electronico" +
                     "   FROM comrel_pyme_epo pe, "+tablaDoc+"comrel_nafin crn, comcat_pyme p" +
                     "  WHERE p.ic_pyme = pe.ic_pyme" +
                     "    AND p.ic_pyme = crn.ic_epo_pyme_if" +
                     condicionDoc+
                     "    AND crn.cg_tipo = 'P' "+condicion;
                     if(!pantalla.equals("listReq") && !pantalla.equals("consSinNumProv") && !pantalla.equals("MantenimientoDoctos") && !pantalla.equals("InformacionDocumentos") && !pantalla.equals("CambioEstatus") ){
                          qrySentencia += "    AND pe.cs_habilitado = 'S'"+
                              "    AND p.cs_habilitado = 'S'"+
                              "    AND pe.cg_pyme_epo_interno IS NOT NULL ";
                     }
                     if(pantalla.equals("consSinNumProv")){
                        qrySentencia += 
                              " AND pe.cg_pyme_epo_interno IS NULL "+
                              " AND pe.cs_num_proveedor = 'S'";
                     }
                     
                     if(!ic_if.equals("")){
                     qrySentencia +=  
                        " AND p.ic_pyme in ( select "  +
                        "	      distinct "  +
                        "		      cuenta.ic_pyme "  + 
                        "     from  "  +
                        "	      comrel_pyme_if rel, "  +
                        "	      comrel_cuenta_bancaria cuenta "  +
                        "     where "  +
                        "	      rel.cs_vobo_if = 'S' and "  + 
                        "	      rel.cs_borrado = 'N' and "  +
                        "	      rel.ic_if      =  ?  and "  +
                        "	      rel.ic_cuenta_bancaria = cuenta.ic_cuenta_bancaria and "  +
                        "	cuenta.cs_borrado = 'N' ) ";
                     }
                     qrySentencia += 
                     
                     "  GROUP BY p.ic_pyme," +
                     "           pe.cg_pyme_epo_interno," +
                     "           p.cg_razon_social," +
                     "           RPAD (pe.cg_pyme_epo_interno, 10, ' ') || p.cg_razon_social," +
                     "           crn.ic_nafin_electronico" +
                     "  ORDER BY p.cg_razon_social"  ;
                  //System.err.println("\n qrySentencia = < "+qrySentencia + ">"); // Debug info
                  System.out.println(qrySentencia);
                  ps = con.queryPrecompilado(qrySentencia);
                  int cont = 0;
                  if(!"".equals(ic_epo)) {
                     cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
                  }
                  if(!"".equals(num_pyme)) {
                     cont++;ps.setString(cont,num_pyme);
                  }
                  if(!"".equals(rfc_pyme)) {
                     cont++;ps.setString(cont,rfc_pyme);
                  }
                  if(!"".equals(nombre_pyme)) {
                     cont++;ps.setString(cont,nombre_pyme);
                  }
                  if(!"".equals(ic_if)) {
                     cont++;ps.setString(cont,ic_if);
                  }
               } // fin else
               rs = ps.executeQuery();
               while(rs.next()) {
                  vecCols = new Vector();
                  vecCols.addElement(rs.getString(1));
                  vecCols.addElement(rs.getString(2));
                  vecCols.addElement(rs.getString(3));
                  vecCols.addElement(rs.getString(4));
                  vecCols.addElement(rs.getString(5));
                  vecRows.addElement(vecCols);
               }
               rs.close();
               if(ps!=null) ps.close();
            } catch(Exception e){
               System.out.println("getProveedores:: Exception "+e);
               e.printStackTrace();
            } finally{
               if(con.hayConexionAbierta())
                  con.cierraConexionDB();
               System.out.println("\n getProveedores::(S) ");
            }
            vecFilas = vecRows;
         /**
         *  END getProveedores()
         **/
         
         
         //System.out.println("Busqueda de las pymes...");
      } else {
         vecFilas = null;
      }
	
   jsonArr = new JSONArray();
   jsonObj = new JSONObject();
   
	for (i=0;i<vecFilas.size();i++) { 
      vecColumnas=(Vector)vecFilas.get(i);
      String auxPyme = "";
      String auxDescripcion = "";
      
      if (((String)vecColumnas.get(0)).equals(ic_pyme)) { 
         num_pyme=(String)vecColumnas.get(1);
         nombre_pyme=(String)vecColumnas.get(2);
         nafin_electronico=(String)vecColumnas.get(4);
         auxPyme = "selected";
      }
      
      if(descripcionConNumeroProveedor)
         auxDescripcion = vecColumnas.get(1) + " " + vecColumnas.get(2).toString();
      else if("2".equals(ic_producto_nafin)||"5".equals(ic_producto_nafin))
         auxDescripcion = vecColumnas.get(2).toString();	
      else
         auxDescripcion = vecColumnas.get(3).toString();
      
		jsonObj = new JSONObject();		
		jsonObj.put("clave", (String)vecColumnas.get(4));
		jsonObj.put("descripcion", auxDescripcion);
		//jsonObj.put("ic_pyme",registros.getString("IC_PYME"));
		jsonArr.add(jsonObj);
	}	
	
	jsonObj.put("success", new Boolean(true));
	
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	
	resultado = jsonObj.toString();
	
} /*** FIN CATALOGO BUSQUEDA AVANZADA ***/


/*** INICIA BUSQUEDA EXPEDIENTE ***/
else if(informacion.equals("BusquedaExpediente")){
   String rfc = (request.getParameter("rfc")==null)?"":request.getParameter("rfc");
   String version = (request.getParameter("version")==null)?"":request.getParameter("version");
   String expediente = (request.getParameter("expediente")==null)?"":request.getParameter("expediente");
   String accion = (request.getParameter("accion")==null)?"":request.getParameter("accion");
   //AccesoDB con         = new AccesoDB();
   //JSONObject jsonObj = new JSONObject();
      
   try{
		//AfiliacionHome afiliacionHome1 =	(AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB",AfiliacionHome.class); 
		//Afiliacion 		BeanAfiliacion 		= afiliacionHome1.create();
		Afiliacion 		BeanAfiliacion  = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
      //saca el expediente de efile y lo copia a nafin
      BeanAfiliacion.getExpedientePymeEFile(rfc);
      
      //consultar el expediente en tabla de nafin
      FileOutputStream fos = null;
      File file            = null;	
      String pathname      = "";
      //PreparedStatement ps = null;
     // ResultSet rs         = null;
      String qryDocto      = ""; 
      String nombreArchivo ="ExpedienteDigital";
     String ContentType = "application/pdf";
     //OutputStream os = response.getOutputStream();
     Blob bin = null;                
     InputStream inStream = null; 
	  String ext = "";
     
      boolean existearchivo=false;
     int size = 0;
      con = new AccesoDB();
      con.conexionDB();
      qryDocto = 	" SELECT pym.IC_PYME,  efdoc.BI_DOCUMENTO, efdoc.CG_EXTENSION, '15BitacoraAceptacionConvenioUnicoext.jsp'  " +
                        " FROM COMTMP_DOC_PYME efdoc, comcat_pyme  pym " +
                        " WHERE efdoc.IC_PYME = pym.ic_pyme " +
                        " and pym.cg_rfc = '" + rfc + "' ";
      System.out.println("qryDocto : "+qryDocto);
   
      ps = con.queryPrecompilado(qryDocto);
      rs = ps.executeQuery();
      if(rs.next())
      {
      /*
       // SE INCORPORA EL LLAMADO A LA BITACORA DE ACCESOS DE EXPEDIENTES DEL EFILE
       CallableStatement cs = con.ejecutaSP("PROC_BIT_ACCESOS (?, ?, ?, ?)");
   
       cs.setString(1, rfc);
       cs.setInt(2, 2);
       cs.setString(3, strLogin);
       cs.setString(4, "Nafin Electr�nico");
   
       cs.execute();
   
       cs.close(); 
       con.terminaTransaccion(true);
      */    
   
         pathname = strDirectorioTemp + "Efile_"+nombreArchivo+ rs.getString("CG_EXTENSION");  
         ext = rs.getString("CG_EXTENSION");
         System.err.println("pathname : "+pathname);
         
         file = new File(pathname);                
         fos = new FileOutputStream(file);                                    
        bin = rs.getBlob("BI_DOCUMENTO");                
        inStream = bin.getBinaryStream();                
        size = (int)bin.length();                
       
       
      byte[] buffer = new byte[size];
        int length = -1;
       while ((length = inStream.read(buffer)) != -1){
          fos.write(buffer, 0, length);
        }
       
       existearchivo = true;
    }
      if(rs!=null)rs.close();
      if(ps!=null)ps.close();
   
      jsonObj = new JSONObject();
      jsonObj.put("success", new Boolean(true));
      jsonObj.put("existearchivo",Boolean.toString(existearchivo));
     jsonObj.put("urlArchivo", strDirecVirtualTemp +"Efile_"+nombreArchivo+ext);
      resultado = jsonObj.toString();

   }catch(Exception e){
     con.terminaTransaccion(false);
     e.printStackTrace(); 
     System.err.println("Error::: "+e);
   }
   finally{
      if(con.hayConexionAbierta())
         con.cierraConexionDB();
   }
}
/*** TERMINA BUSQUEDA EXPEDIENTE ***/
%>
<%= resultado%>

