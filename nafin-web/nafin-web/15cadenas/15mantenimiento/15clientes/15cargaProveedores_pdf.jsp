<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.afiliacion.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();

String fecini = request.getParameter("fecini")==null?Fecha.getFechaActual():request.getParameter("fecini");
String fecfin = request.getParameter("fecfin")==null?Fecha.getFechaActual():request.getParameter("fecfin");
String cveEpo = request.getParameter("cveEpo")==null?"":request.getParameter("cveEpo");

if("EPO".equals(strTipoUsuario)){
	cveEpo = iNoCliente;
}

try {
	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion afiliacion = afiliacionHome.create();
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
	List lstConsultaBitCarga = new ArrayList();
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	if(!"NAFIN".equals(strTipoUsuario)){
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),	
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);
	}

	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
	List lEncabezados = new ArrayList();
	int nRow = 0;
	
	//lstConsultaBitCarga = afiliacion.getBitacoraCargaMasiva("225", "01/09/2011", "22/11/2011");	//Para realizar pruebas
	lstConsultaBitCarga = afiliacion.getBitacoraCargaMasiva(cveEpo, fecini, fecfin);
	if(lstConsultaBitCarga!=null && lstConsultaBitCarga.size()>0){
		for(int x=0; x<lstConsultaBitCarga.size();x++){
			List lstRegConsulta = (List)lstConsultaBitCarga.get(x);
			if (nRow == 0){
				if("NAFIN".equals(strTipoUsuario)){
					lEncabezados.add("EPO");
				}
					lEncabezados.add("Fecha de Carga de Archivo");
				if("NAFIN".equals(strTipoUsuario)){
					lEncabezados.add("Nombre Archivo");
					lEncabezados.add("Usuario Responsable");
				}
					lEncabezados.add("Total de Registros");
					lEncabezados.add("Registros Procesados");
					lEncabezados.add("Resgistros Inválidos");
				pdfDoc.setTable(lEncabezados, "formasmenB", 100);
			}

			if("NAFIN".equals(strTipoUsuario)){
				pdfDoc.setCell((String)lstRegConsulta.get(9), "formasmen", ComunesPDF.CENTER);
			}
			pdfDoc.setCell((String)lstRegConsulta.get(6), "formasmen", ComunesPDF.CENTER);
			if("NAFIN".equals(strTipoUsuario)){
				pdfDoc.setCell((String)lstRegConsulta.get(2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((String)lstRegConsulta.get(8), "formasmen", ComunesPDF.CENTER);
			}
			pdfDoc.setCell((String)lstRegConsulta.get(3), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell((String)lstRegConsulta.get(4), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell((String)lstRegConsulta.get(5), "formasmen", ComunesPDF.CENTER);

			nRow++;
		}
	}else{
		pdfDoc.addText("No se encontró ningún registro, intente de nuevo con otro criterio de búsqueda","formas",ComunesPDF.CENTER);
	}

	pdfDoc.addTable();
	pdfDoc.endDocument();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
}
%>
<%=jsonObj%>