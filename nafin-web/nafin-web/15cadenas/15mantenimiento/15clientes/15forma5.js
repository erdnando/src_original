Ext.onReady(function(){

	var strPerfil = Ext.getDom('strPerfil').value;
	var idMenu = Ext.getDom('idMenu').value;

	function creditoElec(pagina) {
		if (pagina == 0) { 
			window.location  = "15forma05AfianzadoraExt.jsp"; 	
		}	else if (pagina == 1 ) { 
			window.location ="15consEPOext.jsp?idMenu="+idMenu;
		}	else if (pagina == 2) {
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15forma05ProveedoresExt.jsp?idMenu="+idMenu; //Agust�n Bautista Ruiz
		}	else if (pagina == 4) { 			
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consSinNumProvExt.jsp?idMenu="+idMenu;	//Modificado
		}	else if (pagina == 5) {
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15formaDist05ext.jsp?idMenu="+idMenu;					
		}	else if (pagina == 6) { 
			//window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15consfiados.jsp?selecOption=FIA&consultar=N&valTerminos=N"; 			
			window.location ="/nafin/15cadenas/15mantenimiento/15clientes/15consfiadosExt.jsp?idMenu="+idMenu;	
		}	else if (pagina == 7) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=B&idMenu="+idMenu; 	
		} else if (pagina == 8) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=NB&idMenu="+idMenu; 	 	
		} else if (pagina == 9) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma09ConsExt.jsp?interFinan=FB&idMenu="+idMenu; 		 	
		} else if (pagina == 10) { // CargaMasivaEContrac
			window.location = "/nafin/15cadenas/15mantenimiento/15clientes/15consCargaEcon_ext.jsp?idMenu="+idMenu;			
		}	else if (pagina == 11) { 			
			window.location = "15forma05ext_AfCredElectronico.jsp?idMenu="+idMenu;
		}	else if (pagina == 14) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma5MandanteExt.jsp?idMenu="+idMenu;	
		}	else if (pagina == 15) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15forma28Ext.jsp?idMenu="+idMenu;	
		}	else if (pagina == 16) { 
			window.location  = "/nafin/15cadenas/15mantenimiento/15clientes/15ConsAfiliaClienteExternoExt.jsp?idMenu="+idMenu;	
		}			

	}

	var dataStatus = new Ext.data.ArrayStore({	 
	  fields: ['clave','descripcion'  ],	 
	  autoLoad: false	  
	});
 
	if(strPerfil== "ADMIN NAFIN" ||  strPerfil== "ADMIN NAFIN EXT" ||  strPerfil== "ADMIN NAFIN RES"   )  {
		dataStatus.loadData(	[	
			['0','Afianzadora'],
			['1','Cadena Productiva'],
			['2','Proveedores'],			
			['4','Proveedor sin n�mero'],
			['5','Distribuidores'],
			['6','Fiados'],
			['7','Intermediario Financiero Bancario'],
			['8','Intermediario Financiero No Bancario']	,
			['9','Intermediarios Bancarios/No Bancarios'],	
			['10','Proveedor Carga Masiva EContract'],	
			['11','Afiliados a Cr�dito Electr�nico'],				
			['14','Mandante'],
			['15','Universidad-Programa Cr�dito Educativo'],
			['16','Cliente Externo']	
		]);
		
	}else  if(strPerfil== "PROMO NAFIN FAC"  ||  strPerfil== "PROMO NAFIN" || strPerfil== "NAFIN OPERADOR"   ||   strPerfil== "NAFIN CON PROM"  ||   strPerfil== "NAFIN CON"    ||  strPerfil== "SUPER CALL CENT" )   {
		dataStatus.loadData(	[			
			['2','Proveedores'],			
			['5','Distribuidores']
		]);
	
	}else  if(strPerfil== "CALL CENTER CAD")   { // F021 - 2015
		dataStatus.loadData(	[
			['1','Cadena Productiva'],
			['2','Proveedores'],	
			['5','Distribuidores'],
			['9','Intermediarios Bancarios/No Bancarios']	
		]);
	}else  if(strPerfil=== "ADMIN OPERC")   { //QC-2018
		dataStatus.loadData(	[			
				['2','Proveedores']
			]);	
	}
	
	
	
	var  elementosForma =  [
		{
			xtype: 'combo',	
			name: 'status',	
			id: 'cmbTipoAfiliado',	
			fieldLabel: 'Tipo de Afiliado', 
			mode: 'local',	
			hiddenName : 'status',	
			emptyText: 'Seleccionar  ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : dataStatus,	
			displayField : 'descripcion',	
			valueField : 'clave',
			listeners: {
				select: {
					fn: function(combo) {
						var valor  = combo.getValue();
						creditoElec(valor);		
					}
				}
			}
		}
	];
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Consulta',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true	
	});
	
	
		var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp		
		]
	});
	
	
});