Ext.onReady(function(){

	var fc = Ext.form;

//------------------------------ Handlers ------------------------------//
	// Llena los campos del form y realiza la consulta de las epos
	function procesaValoresIniciales(){

		var icPyme        = Ext.getDom('icPyme').value;
		var razonSocial   = Ext.getDom('razonSocial').value;
		var rfc           = Ext.getDom('rfc').value;
		var pyme_epo_int  = Ext.getDom('pyme_epo_int').value;
		var cs_aceptacion = Ext.getDom('cs_aceptacion').value;

		Ext.getCmp('ic_pyme_id').setValue(icPyme);
		Ext.getCmp('pyme_id').setValue(razonSocial);
		Ext.getCmp('rfc_id').setValue(rfc);
		Ext.getCmp('pyme_epo_int_id').setValue(pyme_epo_int);
		Ext.getCmp('cs_aceptacion_id').setValue(cs_aceptacion);

		catalogoEPO.load({
			params:        Ext.apply({
				ic_pyme:    Ext.getCmp('ic_pyme_id').getValue(),
				filtro_epo: ''
			})
		});
	}

	// Realiza la consulta del grid de acuerdo a las EPOS seleccionadas.
	function agregarEpo(){
		var pyme = Ext.getCmp('ic_pyme_id').getValue();
		var epo  = Ext.getCmp('nombre_epo_id').getValue();
		if(epo == ''){
			Ext.Msg.alert('Mensaje', 'Debe seleccionar al menos una EPO');
			return;
		}
		consultaData.load({
			params: Ext.apply({
				ic_pyme: pyme,
				ic_epo:  epo
			})
		});
	}

	// Regresa a la pantalla anterior
	function cancelar(){
		window.location.href = '15formaDist05ext.jsp';
	}

	// Cancela el proceso del grid de Reafiliaci�n
	function cancelaReafiliacion(){
		Ext.getCmp('gridConsulta').hide();
		Ext.getCmp('nombre_epo_id').reset();
		Ext.getCmp('textfieldFila_id').reset();
		catalogoEPO.load({
			params:        Ext.apply({
				ic_pyme:    Ext.getCmp('ic_pyme_id').getValue(),
				filtro_epo: ''
			})
		});
	}

	// Valida los datos del grid, y actualiza los datos correspondientes
	function aceptarReafiliacion(){

		var datar = new Array();
		var jsonDataEncode = '';
		var records = consultaData.getRange();
		var banderaOk = true;

		// Primero valido los campos obligatorios del grid
		for (var i = 0; i < records.length; i++){
			if(records[i].data.TIPO_CREDITO == ''){
				banderaOk = false;
				Ext.MessageBox.alert('Mensaje...','El campo Tipo de Cr�dito es obligatorio. <br>Por favor capt�relo.',
					function(){
						gridConsulta.startEditing(i, 4);
						return;
					}
				);
				break;
			} else if(records[i].data.NUMERO_DISTRIBUIDOR == ''){
				banderaOk = false;
				Ext.MessageBox.alert('Mensaje','El campo N�mero de Distribuidor es obligatorio. <br>Por favor capt�relo.',
					function(){
						gridConsulta.startEditing(i, 5);
						return;
					}
				);
				break;
			}
		}

		if(banderaOk == true){
			// Si todo es correcto, env�o los datos
			for (var i = 0; i < records.length; i++){
				datar.push(records[i].data);
			}
			jsonDataEncode = Ext.util.JSON.encode(datar);
			Ext.Ajax.request({
				url: '15reafiliaDistext.data.jsp',
				params: Ext.apply({
					informacion:  'REAFILIA_EPO',
					ic_pyme:      Ext.getCmp('ic_pyme_id').getValue(),
					pyme_epo_int: Ext.getCmp('pyme_epo_int_id').getValue(),
					rfc:          Ext.getCmp('rfc_id').getValue(),
					cs_aceptacion:Ext.getCmp('cs_aceptacion_id').getValue(),
					datos:        jsonDataEncode
				}),
				callback: procesaReafiliacion
			});
		} else{
			return;
		}


	}

//------------------------------ Callbacks ------------------------------//

	// Muestra / Oculta el grid despues de realizar la consulta
	var procesarConsultaData = function(store, arrRegistros, opts){

		var fp = Ext.getCmp('formaPrincipal');
		fp.el.unmask();

		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();

		if (arrRegistros != null){
			if (!gridConsulta.isVisible()){
				gridConsulta.show();
			}
			if(store.getTotalCount() > 0){
				el.unmask();
				Ext.getCmp('btnAceptaReafiliacion').enable();
			} else{
				Ext.getCmp('btnAceptaReafiliacion').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}

	};

	// Procesa la respuesta de la reafiliaci�n
	var procesaReafiliacion = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			
			Ext.Msg.alert('Mensaje', 
								Ext.util.JSON.decode(response.responseText).mensaje,
								cancelar
			);
		}
	}

//------------------------------ Stores ------------------------------//

	// Se crea el STORE del grid 'Reafiliaci�n'
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url:  '15reafiliaDistext.data.jsp',
		baseParams: {
			informacion: 'CONSULTA_DATA'
		},
		fields: [
			{name: 'IC_EPO'                 },
			{name: 'CADENA_PRODUCTIVA'      },
			{name: 'NAFIN_ELECTRONICO'      },
			{name: 'RAZON_SOCIAL'           },
			{name: 'RFC'                    },
			{name: 'TIPO_CREDITO'           },
			{name: 'NUMERO_DISTRIBUIDOR'    }
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	})

	// Se crea el STORE para el cat�logo 'Cadena Productiva'
	var catalogoEPO = new Ext.data.JsonStore({
		id:     'catalogoEPO',
		root:   'registros',
		fields: ['clave', 'descripcion','loadMsg'],
		url:    '15reafiliaDistext.data.jsp',
		baseParams: {
			informacion: 'CATALOGO_EPO'
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners:      {
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	// Se crea el STORE para el cat�logo 'Tipo de Cr�dito'
	var catalogoTipoCredito = new Ext.data.JsonStore({
		id:     'catalogoTipoCredito',
		root:   'registros',
		fields: ['clave', 'descripcion','loadMsg'],
		url:    '15reafiliaDistext.data.jsp',
		baseParams: {
			informacion: 'CATALOGO_TIPO_CREDIT0'
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners:      {
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

//------------------------------ Componentes ------------------------------//

	var comboTipoCredito = new fc.ComboBox({
		triggerAction: 'all',
		displayField:  'descripcion',
		valueField:    'descripcion',
		typeAhead:     true,
		allowBlank:    true,
		enableKeyEvents: true,
		minChars:      1,
		store:           catalogoTipoCredito,
		listeners:{
			focus: function (e){
				var fila  = Ext.getCmp('textfieldFila_id').getValue();
				var rec   = consultaData.getAt(fila);
				var ic_epo= rec.get('IC_EPO');
				catalogoTipoCredito.load({
					params: Ext.apply({
						ic_epo: ic_epo
					})
				});
			}
		}
	});

	// Se crea el grid 'Reafiliaci�n'
	var gridConsulta = new Ext.grid.EditorGridPanel({
		height:         300,
		width:          '90%',
		id:             'gridConsulta',
		title:          'Reafiliaci�n',
		style:          'margin:0 auto;',
		frame:          false,
		autoScroll:     true,
		border:         true,
		hidden:         true,
		clicksToEdit:   1,
		store:          consultaData,
		listeners: {
			cellclick: function(gridConsulta, rowIndex, columnIndex, e){
				// Este l�stener es para saber cual registro del grid estoy seleccionando
				Ext.getCmp('textfieldFila_id').setValue(rowIndex);
			}
		},
		columns: [{
			width:       250,
			header:      '<div align="center"> Cadena Productiva </div>',
			dataIndex:   'CADENA_PRODUCTIVA',
			align:       'left'
		},{
			width:       150,
			header:      '<div align="center"> No. Nafin Electr�nico </div>',
			dataIndex:   'NAFIN_ELECTRONICO',
			align:       'center'
		},{
			width:       250,
			header:      '<div align="center"> Nombre o Raz�n Social </div>',
			dataIndex:   'RAZON_SOCIAL',
			align:       'left'
		},{
			width:       120,
			header:      '<div align="center"> RFC </div>',
			dataIndex:   'RFC', 
			align:       'center'
		},{
			width:       250,
			header:      '<div align="center"> Tipo de Cr�dito </div>',
			dataIndex:   'TIPO_CREDITO',
			align:       'left',
			editor:      comboTipoCredito
		},{
			width:       115,
			header:      '<div align="center"> No. distribuidor </div>',
			dataIndex:   'NUMERO_DISTRIBUIDOR',
				align:     'center',
			editor:      new fc.TextField({ allowBlank: false })
		}],
		bbar: ['->',{ 
			xtype:       'button',
			text:        'Aceptar',
			id:          'btnAceptaReafiliacion',
			iconCls:     'icoAceptar',
			handler:     aceptarReafiliacion
		},'-',{ 
			xtype:       'button',
			text:        'Cancelar',
			id:          'btnCancelaReafiliacion',
			iconCls:     'icoCancelar',
			handler:     cancelaReafiliacion
		}]
	});

	// Se crea el form para agregar o editar grupos
	var formaPrincipal = new Ext.form.FormPanel({
		width:             850,
		labelWidth:        140,
		id:                'formaPrincipal',
		title:             'Reafiliar por EPO',
		bodyStyle:         'padding: 6px',
		style:             'margin: 0px auto 0px auto;',
		frame:             true,
		border:            false,
		fieldDefaults: {
			msgTarget:      'side'
		},
		items:[{
			width:          '95%',
			xtype:          'textfield', 
			id:             'textfieldFila_id',
			name:           'textfieldFila',
			fieldLabel:     '&nbsp; Fila',
			hidden:         true
		},{
			width:          '95%',
			xtype:          'textfield', 
			id:             'ic_pyme_id',
			name:           'ic_pyme',
			fieldLabel:     '&nbsp; Ic PyME',
			hidden:         true
		},{
			width:          '95%',
			xtype:          'textfield', 
			id:             'pyme_epo_int_id',
			name:           'pyme_epo_int',
			fieldLabel:     '&nbsp; pyme_epo_int',
			hidden:         true
		},{
			width:          '95%',
			xtype:          'textfield', 
			id:             'cs_aceptacion_id',
			name:           'cs_aceptacion',
			fieldLabel:     '&nbsp; cs_aceptacion',
			hidden:         true
		},{
			width:          '95%',
			xtype:          'textfield', 
			id:             'pyme_id',
			name:           'pyme',
			fieldLabel:     '&nbsp; Nombre del Distribuidor',
			disabled:       true
		},{
			width:          '95%',
			xtype:          'textfield', 
			id:             'rfc_id',
			name:           'rfc',
			fieldLabel:     '&nbsp; RFC',
			disabled:       true
		},{
		width:             '95%',
		xtype:             'textfield',
		id:                'filtro_epo_id',
		name:              'filtro_epo',
		fieldLabel:        '&nbsp; Nombre de la EPO',
		emptyText:         'Capture un criterio de b�squeda y presione ENTER',
		enableKeyEvents:   true,
		listeners:         {
			specialkey:     function(f,e){
				if (e.getKey() == e.ENTER){
					catalogoEPO.load({
						params:        Ext.apply({
							ic_pyme:    Ext.getCmp('ic_pyme_id').getValue(),
							filtro_epo: Ext.getCmp('filtro_epo_id').getValue()
						})
					});
				}
			}
		}
	},{
		xtype:             'itemselector',
		id:                'nombre_epo_id',
		name:              'nombre_epo',
		imagePath:         '/nafin/00utils/extjs/ux/images/',
		drawUpIcon:        false, 
		drawDownIcon:      false, 
		drawTopIcon:       false, 
		drawBotIcon:       false,
		border:            false,
		allowBlank:        false,
		multiselects:      [{
			width:          318,
			height:         150,
			legend:         '',
			displayField:   'descripcion',
			valueField:     'clave',
			store:          catalogoEPO
		},{
			width:          318,
			height:         150,
			legend:         'EPOs a reafiliar',
			displayField:   'descripcion',
			valueField:     'clave',
			msgTarget:      'side',
			store:          new Ext.data.JsonStore({
				id:          'storeTempData',
				root:        'registros',
				fields:      ['clave', 'descripcion', 'loadMsg'],
				url:         '15forma05ProveedoresExt.data.jsp',
				baseParams:  { informacion: 'CatalogoVirtual'},
				totalProperty:	'total',
				autoLoad:    false,
				listeners:   {
					exception:  NE.util.mostrarDataProxyError,
					beforeload: NE.util.initMensajeCargaCombo
				}
			})
		}]
	}],
		buttons: [{
			text:           'Agregar',
			id:             'btnAgregar',
			iconCls:        'icoAceptar',
			handler:        agregarEpo
		},{
			text:           'Cancelar',
			id:             'btnCancelar',
			iconCls:        'icoCancelar',
			handler:        cancelar
		}]
	});

	// Contenedor principal
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			NE.util.getEspaciador(20),
			gridConsulta
		]
	});

	procesaValoresIniciales();

});