	<%@ page contentType="application/json;charset=UTF-8" import="   

	java.util.*,
	com.netro.exception.*,  
	com.netro.model.catalogos.*,
	netropology.utilerias.usuarios.*,   
	net.sf.json.JSONObject,
	com.netro.cadenas.*,
	net.sf.json.JSONArray"
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/15cadenas/015secsession.jspf" %>


<%
	String ic_epo									= (request.getParameter("HicEpo")==null)?"":request.getParameter("HicEpo");
	String num_pyme								= (request.getParameter("num_pyme")==null)?"":request.getParameter("num_pyme");
	String rfc										= (request.getParameter("rfc")==null)?"":request.getParameter("rfc");
	String nom_pyme								= (request.getParameter("nombre")==null)?"":request.getParameter("nombre");
	String ic_pyme								= (request.getParameter("txt_ne")==null)?"":request.getParameter("txt_ne");
	String banco									= (request.getParameter("HBanco")==null)?"":request.getParameter("HBanco");
	String sirac									= (request.getParameter("sirac")==null)?"":request.getParameter("sirac");
	String hFechaIni							= (request.getParameter("hFechaRegIni")==null)?"":request.getParameter("hFechaRegIni");
	String hFechaFin							= (request.getParameter("hFechaRegFin")==null)?"":request.getParameter("hFechaRegFin");
	String aFechaIni							= (request.getParameter("aFechaRegIni")==null)?"":request.getParameter("aFechaRegIni");
	String aFechaFin							= (request.getParameter("aFechaRegFin")==null)?"":request.getParameter("aFechaRegFin");
	String pymeHab								= (request.getParameter("pymeHab")==null)?"":request.getParameter("pymeHab");
	String pymeSinProv						= (request.getParameter("pymeSinProv")==null)?"":request.getParameter("pymeSinProv");

	int start=0, limit=0;

	JSONObject resultado 	      = new JSONObject();
	String infoRegresar           = "";
	String informacion            =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String operacion              =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
	
	if(informacion.equals("catologoBancoDist")){

	CatalogoSimple catalogo = new CatalogoSimple();
	
	catalogo.setTabla("COMCAT_BANCO_FONDEO");
	catalogo.setCampoClave("IC_BANCO_FONDEO");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setCampoClave("IC_BANCO_FONDEO");	
	catalogo.setOrden("CD_DESCRIPCION");	
	infoRegresar = catalogo.getJSONElementos();	
	
}else if(informacion.equals("catologoEpo")){
	
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setClaveBancoFondeo(banco);
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveIf(iNoCliente);
		infoRegresar=catalogo.getJSONElementos();
}else if(informacion.equals("CatalogoBuscaAvanzada")){		
			CatalogoPymeBusquedaAvanzada cat= new CatalogoPymeBusquedaAvanzada();
			cat.setCampoClave("crn.ic_nafin_electronico");
			cat.setCampoDescripcion("p.cg_razon_social");
			cat.setIc_epo(ic_epo);
			cat.setNum_pyme(num_pyme);
			cat.setRfc_pyme(rfc);
			cat.setNombre_pyme(nom_pyme);
			cat.setIc_pyme(ic_pyme);	
			infoRegresar=cat.getJSONElementos();
	
}else if(informacion.equals("neElectronico")){

StringBuffer strSQLPyme2 = new StringBuffer();
ResultSet rspym = null; 
PreparedStatement ps = null;
ResultSet rs = null;
AccesoDB con = new AccesoDB();
String txt_nafelec= (request.getParameter("txt_ne")==null)?"":request.getParameter("txt_ne");
String txt_nombre="";
try {
con.conexionDB();

								strSQLPyme2.append(" Select n.ic_epo_pyme_if  as Pyme,  p.cg_razon_social as nombre  "+
											" FROM comrel_nafin n, comcat_pyme p "+
											" where n.ic_epo_pyme_if = p.ic_pyme AND ic_nafin_electronico = "+txt_nafelec);
										
										
							rspym = con.queryDB(strSQLPyme2.toString());																	
							while(rspym.next()) 	{
								ic_pyme = (rspym.getString("Pyme") == null) ? "" : rspym.getString("Pyme");
								txt_nombre = (rspym.getString("nombre") == null) ? "" : rspym.getString("nombre");
							}
							rspym.close();								
					
			}catch(Exception e){
	out.println("Error en jsp");
	System.out.println("error:::"+e);
	e.printStackTrace();
}finally{
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("txt_nombre", txt_nombre);
	infoRegresar = jsonObj.toString();
	if (con.hayConexionAbierta()){
		con.cierraConexionDB();
	}
}
}

else if(informacion.equals("Consulta")|| informacion.equals("ArchivoCSV")){
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));	
			}catch(Exception e){
					System.out.println("Error en parametros");
			}
			String consulta="";
			ConsuReafiliacionAutomatica paginador =  new ConsuReafiliacionAutomatica();
			if(!ic_pyme.equals("")){
				StringBuffer strSQLPyme2 = new StringBuffer();
				ResultSet rspym = null; 
				PreparedStatement ps = null;
				ResultSet rs = null;
				AccesoDB con = new AccesoDB();
				try {
				con.conexionDB();
				strSQLPyme2.append(" Select n.ic_epo_pyme_if  as Pyme,  p.cg_razon_social as nombre  "+
											" FROM comrel_nafin n, comcat_pyme p "+
											" where n.ic_epo_pyme_if = p.ic_pyme AND ic_nafin_electronico = "+ic_pyme);
										
										
							rspym = con.queryDB(strSQLPyme2.toString());																	
							while(rspym.next()) 	{
								ic_pyme = (rspym.getString("Pyme") == null) ? "" : rspym.getString("Pyme");
								//txt_nombre = (rspym.getString("nombre") == null) ? "" : rspym.getString("nombre");
							}
							rspym.close();		
				}catch(Exception e){
					out.println("Error en jsp");
					System.out.println("error:::"+e);
					e.printStackTrace();
				}finally{
					if (con.hayConexionAbierta()){
						con.cierraConexionDB();
					}
				}
			}
			paginador.setIc_pyme(ic_pyme);
			paginador.setBanco(banco);
			paginador.setSirac(sirac);
			paginador.setFhabilitacioIni(hFechaIni);
			paginador.setFhabilitacioFin(hFechaFin);
			paginador.setFafiliacionIni(aFechaIni);
			paginador.setFafiliacionFin(aFechaFin);
			if("true".equals(pymeSinProv)){
				pymeSinProv="S";
			}
			if("true".equals(pymeHab)){
				pymeHab="N";
			}
			paginador.setSinNumProv(pymeSinProv);
			paginador.setPorhabilitar(pymeHab);
			paginador.setUsuario(iNoCliente);
			paginador.setNoepo(ic_epo);
			CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
			//Primer Consulta
			
			try{
				if(informacion.equals("Consulta")){
					if(operacion.equals("Generar")){
						cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
					}
					String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
					resultado = JSONObject.fromObject(consultar);
					infoRegresar=resultado.toString();
				} else if (informacion.equals("ArchivoCSV")) {
							try {
										  
									String nomArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
									JSONObject jsonObj = new JSONObject();
									jsonObj.put("success", new Boolean(true));
									jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
									infoRegresar = jsonObj.toString();
							} catch(Throwable e) {
											 throw new AppException("Error al generar el archivo CSV", e);
								}
						}
				} catch(Exception e) {
					throw new AppException("Error al generar archivo", e);
				}

}


%>

<%=infoRegresar%>