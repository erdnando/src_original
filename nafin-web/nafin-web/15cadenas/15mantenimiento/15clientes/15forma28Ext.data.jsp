<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		netropology.utilerias.usuarios.UtilUsr,
		com.netro.educativo.*,
		com.netro.pdf.*,
		com.netro.model.catalogos.*"
		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion     =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
	String infoRegresar  = "";
	int start=0; int limit=0;
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	

 if(informacion.equals("catalogoGrupo")){
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("cecat_grupo");
		cat.setCampoClave("ic_grupo");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setOrden("ic_grupo");
		infoRegresar = cat.getJSONElementos();		
 }
 else if (informacion.equals("catalogoEstado")){
		String Pais= (request.getParameter("pais")==null)?"":request.getParameter("pais");
		
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setClavePais(Pais);//Mexico clave 24
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();	
 }
 
 else if(informacion.equals("ConsultaGrid") || informacion.equals("GenerarPDF") || informacion.equals("GenerarCSV")  )
 {
		String numNAE 			= (request.getParameter("numeroNE")==null)?"":request.getParameter("numeroNE");
		String universidad  	= (request.getParameter("sUniversidad")==null)?"":request.getParameter("sUniversidad");
		String campus  		= (request.getParameter("sCampus")==null)?"":request.getParameter("sCampus");
		String estado			= (request.getParameter("cmb_estado")==null)?"":request.getParameter("cmb_estado");
		String grupo			= (request.getParameter("grupo")==null)?"":request.getParameter("grupo");
		
		ConsAfiliaUniversidad paginador = new ConsAfiliaUniversidad();
		paginador.setNumeroNE(numNAE);
		paginador.setCgrupo(grupo);
		paginador.setUniversidad(universidad.toUpperCase());
		paginador.setCampus(campus.toUpperCase());
		paginador.setEstado(estado);
		
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
		try
		{
			 start = Integer.parseInt(request.getParameter("start"));
			 limit = Integer.parseInt(request.getParameter("limit"));	
			
		} catch(Exception e)
		  { System.out.println("Error en parametros de paginación"); }
		  
		try 
		{
			if(informacion.equals("ConsultaGrid"))
			{
				if(operacion.equals("Generar")) 
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				
				String  consultar = queryHelper.getJSONPageResultSet(request,start,limit);	
				jsonObj = JSONObject.fromObject(consultar);
				infoRegresar=jsonObj.toString();				
			}
			
			else if (informacion.equals("GenerarPDF"))
			{
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp, "PDF");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			}
			
			else if (informacion.equals("GenerarCSV"))
			{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			}
		} catch(Exception e) 
			{
				throw new AppException("Error en la recuperacion de datos!", e);
			}
 }
 else if(informacion.equals("borraUniversidad")) {
	try {
		
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		String iNoNafin = (request.getParameter("numNE") == null) ? "" : request.getParameter("numNE");
		String msg = BeanAfiliacion.borraUniversidad(iNoUsuario, iNoNafin);
		
		if(msg.equals("Se borro la Universidad")) 
			jsonObj.put("success", new Boolean(true));
		else	
			jsonObj.put("success", new Boolean(false));
		
		infoRegresar = jsonObj.toString();
		
	 } catch(Exception e) { 
			throw new AppException("Error en la eliminacion de la Universidad", e);
		}
 }
 else if (informacion.equals("ConsultaUsuarios")) {
	String iNoUniversidad = (request.getParameter("numUni") == null) ? "" : request.getParameter("numUni");
	UtilUsr utilUsr = new UtilUsr();
	List respuesta = utilUsr.getUsuariosxAfiliado(iNoUniversidad, "U");
	Iterator itCuentas = respuesta.iterator();
	
	List cuentas = new ArrayList();
	while (itCuentas.hasNext()) {
		HashMap resultado = new HashMap();
		String cuenta = (String) itCuentas.next();
		resultado.put("USUARIO",cuenta);
		cuentas.add(resultado);
	}
	jsObjArray = JSONArray.fromObject(cuentas);
	infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
			
 }
 
 else if (informacion.equals("GenerarPDFUsuarios")) {
	String iNoUniversidad = (request.getParameter("numeroUniversidad") == null) ? "" : request.getParameter("numeroUniversidad");
	
	AccesoDB con = new AccesoDB();
	CreaArchivo archivo = new CreaArchivo();	
	jsonObj = new JSONObject();
	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
	try
	{
		con.conexionDB();
		
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		
////////////////////////	////////////////////////	////////////////////////
		UtilUsr utilUsr = new UtilUsr();
		List respuesta = utilUsr.getUsuariosxAfiliado(iNoUniversidad, "U");
		Iterator itCuentas = respuesta.iterator();
		
		pdfDoc.setTable(1, 20); 
		pdfDoc.setCell("Login Usuario","celda01",ComunesPDF.CENTER);
		
		while (itCuentas.hasNext())
		{ 
			String cuenta = (String) itCuentas.next();
			pdfDoc.setCell(cuenta,"formas",ComunesPDF.CENTER);
		}
		
		pdfDoc.addTable();
		pdfDoc.endDocument();
			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString(); 	
	
	} catch(Exception e) {}
 
 }
 
 else if (informacion.equals("DetalleUsuario")) {
	try {
		
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		String inumUsuario = (request.getParameter("usuario") == null) ? "" : request.getParameter("usuario");
			HashMap infoUsuarios = BeanAfiliacion.consultaUsuarioUniversidad(inumUsuario, "U");
		
		List list_info_usr = new ArrayList();
		list_info_usr.add(infoUsuarios);	
		jsObjArray = JSONArray.fromObject(list_info_usr);
		infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		
	 } catch(Exception e) { 
			throw new AppException("Error en consulta de datos del Usuario", e);
		}
 }
 

//System.out.println("infoRegresar============"+infoRegresar); 


%>
<%=infoRegresar%>



