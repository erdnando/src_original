<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.procesos.*,
		com.netro.anticipos.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	if (informacion.equals("ConsultaGrid")){
		JSONObject jsonObj = new JSONObject();	
		JSONArray registros = new JSONArray();
		List regis = new ArrayList();
		
		CapturaTasas BeanCapturaTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB",CapturaTasas.class);
		
		regis =(ArrayList)BeanCapturaTasas.getEstatusDeTasas(); 
		registros = JSONArray.fromObject(regis);

		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
			
		infoRegresar = jsonObj.toString();		
		
	}else if(informacion.equals("CapturaCambios")){

		String txt_cambio[] = request.getParameterValues("txt_cambio");
					
		CapturaTasas BeanCapturaTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB",CapturaTasas.class);
		
		HashMap hm = new HashMap();
		hm = (HashMap) BeanCapturaTasas.cambioTasas( txt_cambio);
		JSONObject jsonObj = new JSONObject();	

		jsonObj.put("success", new Boolean(true));
		jsonObj.put("MENSAJE",hm.get("MENSAJE"));

		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("ConsultaEncabezados")){
		HashMap hm = new HashMap();
		JSONObject 	jsonObj	= new JSONObject();
		
		CapturaTasas BeanCapturaTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB",CapturaTasas.class);
		
		hm =(HashMap)BeanCapturaTasas.getEncabezadosProductos();
		
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("ENCABEZADO1", hm.get("1"));
		jsonObj.put("ENCABEZADO4", hm.get("4"));
		jsonObj.put("CLAVEPRODUCTO1",	hm.get("CLAVEPRODUCTO1"));
		jsonObj.put("CLAVEPRODUCTO4",	hm.get("CLAVEPRODUCTO4"));
		infoRegresar = jsonObj.toString();
	}	
%>	

<%= infoRegresar %>