<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*, 
				com.netro.procesos.*, 
				netropology.utilerias.*, 
				com.netro.cadenas.*,
				com.netro.model.catalogos.CatalogoSimple,   
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%  
String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String registros[]	= request.getParameterValues("listaRegistros");
String infoRegresar = "";

if (informacion.equals("consultaInicial")) {
	CQueryHelperRegExtJS queryHelper;
	ConsCarteraProducto paginador = new ConsCarteraProducto();
	queryHelper = new CQueryHelperRegExtJS(paginador);
	
	Registros reg = queryHelper.doSearch();
	infoRegresar	=		"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";

}else if (informacion.equals("guardarCambios")) {
	ConsCarteraProducto obj = new ConsCarteraProducto();
	List lista = new ArrayList();
	for(int i =0; i<registros.length; i++){	
		List reg = new ArrayList(Arrays.asList(registros[i].split(",")));
		lista.add(reg);
	}
	String resultado = obj.guardarCambios(lista);
	infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + resultado + "\"}";
}

%>
<%=  infoRegresar%>