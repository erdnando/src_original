<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.text.*,
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.anticipos.*,
	com.netro.catalogos.*,
	com.netro.cadenas.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String interFinanciero = request.getParameter("interFinanciero") == null?"":(String)request.getParameter("interFinanciero");

String nombreIf = request.getParameter("nombreIf") == null?"":(String)request.getParameter("nombreIf");
String tipoFinanciamiento = request.getParameter("tipoFinanciamiento") == null?"":(String)request.getParameter("tipoFinanciamiento");
String tipoComision = request.getParameter("tipoComision") == null?"":(String)request.getParameter("tipoComision");
String comision1 = request.getParameter("comision1") == null?"0":(String)request.getParameter("comision1");
String descuentoMercantil = request.getParameter("descuentoMercantil") == null?"0":(String)request.getParameter("descuentoMercantil");
String ventaCartera = request.getParameter("ventaCartera") == null?"0":(String)request.getParameter("ventaCartera");
String riesgoDistribuidor = request.getParameter("riesgoDistribuidor") == null?"0":(String)request.getParameter("riesgoDistribuidor");

LineaCredito  lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB",LineaCredito.class);

ActualizacionCatalogos actualizacionCatalogos = ServiceLocator.getInstance().lookup("ActualizacionCatalogosEJB",ActualizacionCatalogos.class);

String infoRegresar ="", consulta ="", mensaje ="", clave="", descripcion = "";
String cg_razon_social = "", ic_epo= "", cs_tipo_fondeo="", cg_tipo_comision = "", cs_recorte_solic = ""  ;

 


JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
Vector vctFilas = new Vector();
Vector vctFilasAux = new Vector();
List fondeo;
if (informacion.equals("catalogoIF") ) {
	CatalogoElementosIF catIF = new CatalogoElementosIF();
	catIF.setClave("ic_if");
	catIF.setDescripcion("cg_razon_social");
	catIF.setOrden("cg_razon_social");
	Vector vecFilas = catIF.getListaElementos();
	for(int i=0;i<vecFilas.size();i++) {
		Vector vecColumnas = (Vector)vecFilas.get(i);
		clave = (String)vecColumnas.get(1);
		descripcion = (String)vecColumnas.get(0);  
		datos = new HashMap();
		datos.put("clave", clave);
		datos.put("descripcion", descripcion);
		registros.add(datos);
	}
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar =jsonObj.toString();

} else if (informacion.equals("Consulta") ) {
	
	ConsulParametrizacionIf consultaIF = new ConsulParametrizacionIf();
	consultaIF.setIc_if(interFinanciero);
	CQueryHelperRegExtJS queryHelper1 = new CQueryHelperRegExtJS( consultaIF);
   Registros reg	=	queryHelper1.doSearch();
	String existencia = "N";
	if(reg.next()){
		existencia = "S";
		jsonObj.put("IC_IF",reg.getString("IC_IF"));
		jsonObj.put("CG_FINANCIAMIENTO",reg.getString("CG_FINANCIAMIENTO"));
		jsonObj.put("FG_PORC_COMISION_DE",reg.getString("FG_PORC_COMISION_DE"));
		jsonObj.put("FG_PORC_COMISION_DM",reg.getString("FG_PORC_COMISION_DM"));
		jsonObj.put("CG_COMISION",reg.getString("CG_COMISION"));
		jsonObj.put("FG_PORC_COMISION_VC",reg.getString("FG_PORC_COMISION_VC"));
		jsonObj.put("FG_PORC_COMISION_M2",reg.getString("FG_PORC_COMISION_M2"));
	}
	fondeo = new ArrayList();
	CatalogoElementosIF descripcionIF = new CatalogoElementosIF();
	String dato = descripcionIF.getIntermediarioFinanciero(interFinanciero);
	String banderaID = "S";
	vctFilas = actualizacionCatalogos.consultaDetalleComision(interFinanciero, "1", "ADMIN NAFIN");
	vctFilasAux = actualizacionCatalogos.consultaDetalleComision(interFinanciero,"4", "ADMIN NAFIN");
	for(int i=0;i<vctFilas.size();i++) {   
		Vector vctColumnas = (Vector)vctFilas.get(i);
		cs_tipo_fondeo = (String) vctColumnas.get(2);
		fondeo.add(cs_tipo_fondeo);
	}
	for(int i=0;i<vctFilasAux.size();i++) {
		Vector vctColumnas = (Vector)vctFilasAux.get(i);
		cs_tipo_fondeo = (String) vctColumnas.get(2);
		fondeo.add(cs_tipo_fondeo);
	}
	String dato1 = "";
	if(fondeo.size()>0){
		dato1 = (String)fondeo.get(0);
		for(int i = 0; i<fondeo.size();i++){
			if(!dato1.equals(fondeo.get(i))){
				banderaID = "N";
			}
		}
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("IF",dato);
	jsonObj.put("existencia",existencia);
	jsonObj.put("financiamientoDiferenciado",banderaID);
	infoRegresar = jsonObj.toString(); 
	System.out.println("infoRegresar   "+infoRegresar);
	
}else if (informacion.equals("Guardar_Datos") ) {
	jsonObj = new JSONObject();
	System.out.println("interFinanciero_"+interFinanciero+"tipoFinanciamiento_"+tipoFinanciamiento+"tipoComision_"+tipoComision+"comision1_"+comision1+"descuentoMercantil_"+descuentoMercantil+"ventaCartera_"+ventaCartera+"riesgoDistribuidor_"+riesgoDistribuidor);
	boolean dato = actualizacionCatalogos.guardaDatosComisionParamIf(interFinanciero,tipoFinanciamiento,tipoComision,comision1,descuentoMercantil,ventaCartera,riesgoDistribuidor);
	if(dato==true){
		mensaje = "La operación se realizó con éxito";
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("mensaje",mensaje);
		jsonObj.put("accion","G");
		infoRegresar = jsonObj.toString(); 
	}else{
	}
}
%>
<%=infoRegresar%>


