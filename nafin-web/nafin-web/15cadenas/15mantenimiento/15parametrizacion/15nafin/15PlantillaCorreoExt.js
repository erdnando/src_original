Ext.onReady(function(){
	
	
	function verEmail_Para(nomObj){
	
		var  forma =   Ext.ComponentQuery.query('#forma')[0];
		var txtPara =	forma.query('#txtPara1')[0];
		
		if (nomObj.length>0){
			if ( nomObj.indexOf("@")==-1 ){				
				txtPara.markInvalid('Este campo debe ser una direcci�n de correo o una lista de estas: usuario1@ejemplo.com, usuario2@ejemplo.com');
				txtPara.focus();
				return;
			}
			if (nomObj.indexOf(".")==-1 ){
				txtPara.markInvalid('Este campo debe ser una direcci�n de correo o una lista de estas: usuario1@ejemplo.com, usuario2@ejemplo.com');
				txtPara.focus();
				return;
			}
			//return true;
		}
	}
	
	
	function verEmail_CC(nomObj){
	
		var  forma =   Ext.ComponentQuery.query('#forma')[0];
		var txtCC =	forma.query('#txtCC1')[0];
		
		if (nomObj.length>0){
			if ( nomObj.indexOf("@")==-1 ){				
				txtCC.markInvalid('Este campo debe ser una direcci�n de correo o una lista de estas: usuario1@ejemplo.com, usuario2@ejemplo.com');
				txtCC.focus();
				return;
			}
			if (nomObj.indexOf(".")==-1 ){
				txtCC.markInvalid('Este campo debe ser una direcci�n de correo o una lista de estas: usuario1@ejemplo.com, usuario2@ejemplo.com');
				txtCC.focus();
				return;
			}
			//return true;
		}
	}
	
	
	var procesarGuardar = function(options, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  jsonData = Ext.JSON.decode(response.responseText);
			
			Ext.MessageBox.alert('Mensaje','La informaci�n se guard� correctamente',
				function(){ 					
					window.location = '15PlantillaCorreoExt.jsp';
			}); 
								
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	
	var procesarConsulta = function(options, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  jsonData = Ext.JSON.decode(response.responseText);
			
			var  forma =   Ext.ComponentQuery.query('#forma')[0];					
			forma.query('#txtPara1')[0].setValue(jsonData.txtPara);
			forma.query('#txtCC1')[0].setValue(jsonData.txtCC);
			forma.query('#txtAsunto1')[0].setValue(jsonData.txtAsunto);
			forma.query('#txtTexto1')[0].setValue(jsonData.txtTexto);
			
			forma.setTitle('Pantilla - '+jsonData.operacion); 
			forma.show();
						
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
	
	
	var catOperacionData = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15PlantillaCorreoExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catOperacionData'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	
	var fpCombo = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'formaCombo',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: 'Plantillas para correos de notificaci�n ',
      width: 490,
		//height: 520,
      style: 'margin: 0px auto 0px auto;',
      hidden: false,
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 80
		},
		layout: 'anchor',			
		items	:  [
			{
				xtype: 'combo',
				fieldLabel: 'Operaci�n',
				itemId: 'ic_operacion1',
				name: 'ic_operacion',
				hiddenName: 'ic_operacion',
				forceSelection: true,
				hidden: false, 
				width: 450,
				store: catOperacionData,
				emptyText: 'Seleccione...',
				queryMode: 'local',
				allowBlank: true,
				displayField: 'descripcion',
				valueField: 'clave',						
				listeners: {
					select: function(obj, newVal, oldVal){						
						
						var  fpCombo =   Ext.ComponentQuery.query('#formaCombo')[0];
						
						Ext.Ajax.request({
							url: '15PlantillaCorreoExt.data.jsp',
							params: {							
								informacion: 'Comsultar',							
								ic_operacion  : fpCombo.query('#ic_operacion1')[0].getValue()
							},
							callback: procesarConsulta
						});
					}
				}
			}		
		]	
	});
	
	
	
	var fp = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'forma',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: 'Seleccione',
      width: 490,
      style: 'margin: 0px auto 0px auto;',
      hidden: true,
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 80
		},
		layout: 'anchor',			
		items	:  [
			{
				xtype: 'textareafield',
				fieldLabel: 'Para',
				itemId: 'txtPara1',
				name: 'txtPara',					
				//width: 30,
				height: 30,
				anchor: '100%',
				maxLength: 1000,
				listeners: {
					blur: {
						fn: function(objTxt) {
							verEmail_Para(objTxt.getValue());
						}
					}
				}
			},
			{
				xtype: 'textareafield',
				fieldLabel: 'CC',
				itemId: 'txtCC1',
				name: 'txtCC',					
				//width: 50,	
				height: 30,
				anchor: '100%',
				maxLength: 1000,
					listeners: {
					blur: {
						fn: function(objTxt) {
							verEmail_CC(objTxt.getValue());
						}
					}
				}
			},
			{
				xtype: 'textfield',
				fieldLabel: 'Asunto',
				itemId: 'txtAsunto1',
				name: 'txtAsunto',					
				width: 50,
				height: 20,
				anchor: '100%',
				maxLength: 250				
			},
			{
				xtype: 'textareafield',
				fieldLabel: '',
				itemId: 'txtTexto1',
				name: 'txtTexto',					
				width: 50,
				height: 200,
				anchor: '100%'						
			}
		],
			buttons: [		
			{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls: 'icoGuardar',
				formBind: true,
				handler: function(){				
				
					var  fpCombo =   Ext.ComponentQuery.query('#formaCombo')[0];
					var ic_operacion =	fpCombo.query('#ic_operacion1')[0];
					
					var  forma =   Ext.ComponentQuery.query('#forma')[0];
					var txtPara =	forma.query('#txtPara1')[0];
					var txtAsunto =	forma.query('#txtAsunto1')[0];
					var txtTexto =	forma.query('#txtTexto1')[0];
					
					
					if(Ext.isEmpty(  ic_operacion.getValue()  )){
						ic_operacion.markInvalid('Debe seleccionar una operaci�n');
						ic_operacion.focus();
						return;
					}
					
					if(Ext.isEmpty(txtPara.getValue()  )){
						txtPara.markInvalid('Este campo es obligatorio');
						txtPara.focus();
						return;
					}
					
					if(Ext.isEmpty(txtAsunto.getValue()  )){
						txtAsunto.markInvalid('Este campo es obligatorio');
						txtAsunto.focus();
						return;
					}
					
					if(Ext.isEmpty(txtTexto.getValue()  )){
						txtTexto.markInvalid('Este campo es obligatorio');
						txtTexto.focus();
						return;
					}
						
					
					Ext.Ajax.request({
						url: '15PlantillaCorreoExt.data.jsp',
						params: {							
							informacion: 'Guardar',
							ic_operacion: fpCombo.query('#ic_operacion1')[0].getValue(),							
							txtPara : forma.query('#txtPara1')[0].getValue(),
							txtCC : forma.query('#txtCC1')[0].getValue(),
							txtAsunto : forma.query('#txtAsunto1')[0].getValue(),
							txtTexto: forma.query('#txtTexto1')[0].getValue()
						},
						callback: procesarGuardar
					});															
				}
			},
			{
				text: 'Cancelar',
				id: 'btnCancelar',
				iconCls: 'cancelar',
				formBind: true,
				handler: function(){	
					window.location = '15PlantillaCorreoExt.jsp';
				}
			}	
		]	
	});
	
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 920,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [	
			fpCombo,
			NE.util.getEspaciador(20),
			fp
		]
	});
	
	
	catOperacionData.load(); 
	
});