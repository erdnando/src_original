<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.exception.*,
		com.netro.procesos.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.descuento.*,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

ManejoServicio servicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);

if(informacion.equals("catalogoIntermediarios") ) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_if");
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("cg_razon_social");

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("Consultar")) {
	String cveIf 	= request.getParameter("cveIf")==null?"":request.getParameter("cveIf");

	ArrayList list=new ArrayList();
	list=servicio.getHorarioPorIfCE(cveIf);
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size(); i++){
		Vector aux=(Vector)list.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOIF",aux.get(0).toString());
		jo.put("NOMBREIF",aux.get(1).toString());
		jo.put("HORINIIF",aux.get(2).toString());
		jo.put("HORFINIF",aux.get(3).toString());
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("succes",new Boolean (true));
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("Guardar") ){

	ArrayList alHorariosIf = new ArrayList();
	String [] a = request.getParameterValues("arr");
	
	for(int cont=0; cont < a.length; cont++){
		Vector vecHorario = new Vector();
		StringTokenizer tokens = new StringTokenizer(a[cont],",");
		while(tokens.hasMoreTokens()){
			vecHorario.addElement(tokens.nextToken());
		}
		alHorariosIf.add(alHorariosIf.size(), vecHorario);
	}
	servicio.setHorarioPorIfCE(alHorariosIf);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean (true));
	infoRegresar=jsonObj.toString(); 
	
}else if(informacion.equals("Establecer") ){

	String sNoIfB = request.getParameter("numIf")==null?"":request.getParameter("numIf");
	System.out.println("La clave de la If para restablecer horarios es: "+sNoIfB);
	servicio.setBorrarHorarioPorIf(sNoIfB);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean (true));
	infoRegresar=jsonObj.toString();
}

%>
<%=infoRegresar%>
