<%@ page
		import="	java.util.*, 
				netropology.utilerias.*, 
				javax.naming.*,
				java.io.*,
				com.netro.cesion.*,
				org.apache.commons.fileupload.disk.*,
				org.apache.commons.fileupload.servlet.*,
				org.apache.commons.fileupload.*,
				com.netro.descuento.*,
				net.sf.json.*"
	contentType="text/html; charset=UTF-8"
	errorPage="/00utils/error_extjs_fileupload.jsp"							
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../../../../15cadenas/015secsession_extjs.jspf" %>

<%
String  ic_manual = (request.getParameter("ic_manual")!=null)?request.getParameter("ic_manual"):"";
JSONObject resultadoProceso = new JSONObject();

String rutaArchivoTemporal = null;
ParametrosRequest req = null;
String infoRegresar = "";

if (ServletFileUpload.isMultipartContent(request)) {
	
	// Create a factory for disk-based file items
	DiskFileItemFactory factory = new DiskFileItemFactory();
	
	// Set factory constraints
	//factory.setSizeThreshold(yourMaxMemorySize);
	factory.setRepository(new File(strDirectorioTemp));
	// Create a new file upload handler
	ServletFileUpload upload = new ServletFileUpload(factory);
	// Set overall request size constraint
	upload.setSizeMax(10 * 2097152);	//10 Kb
	// Parse the request
	req = new ParametrosRequest(upload.parseRequest(request));
	
	// Process the uploaded items
	FileItem item = (FileItem)req.getFiles().get(0);
	
	//se ingresa la imagen del archivo	
	netropology.utilerias.ManualesPerfilEpo imagenDocumento = new netropology.utilerias.ManualesPerfilEpo();
	String tamanio = Long.toString (item.getSize());
	int  tamaniot = Integer.parseInt(tamanio);
	
	imagenDocumento.setSize(tamaniot);	
	imagenDocumento.setIc_manual(ic_manual);
	imagenDocumento.guardarArchivo(item.getInputStream());
	
			
	resultadoProceso.put("success", new Boolean(true));
	resultadoProceso.put("resultado", "ArchivoGuardado");

	infoRegresar	 =resultadoProceso.toString();
}

%>
<%=infoRegresar%>

