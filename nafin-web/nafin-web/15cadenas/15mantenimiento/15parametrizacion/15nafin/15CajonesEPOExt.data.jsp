<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.anticipos.*,	
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	if (informacion.equals("CatalogoEpo")){
		List cat = new ArrayList(); 
		JSONArray registros = new JSONArray();
		JSONObject jsonObj = new JSONObject();	
		
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		cat = (ArrayList)catalogo.getListaElementos();
		
		registros = JSONArray.fromObject(cat);
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
			
		infoRegresar = jsonObj.toString();
	
	}else if (informacion.equals("CatalogoMoneda")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_moneda");
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre");
		
		List lis= new ArrayList ();
		lis.add("1");
		lis.add("54");
		
		cat.setValoresCondicionIn(lis);   
		cat.setOrden("ic_moneda");		
		
		infoRegresar=cat.getJSONElementos();
	
	}else if (informacion.equals("ConsultaDatos")){
		List registro = new ArrayList (); 
		JSONArray registros = new JSONArray();
		JSONObject jsonObj = new JSONObject();	
		String nombreEpo = "";
		String Nombremoneda = "";
		String NoCajones = ""; 
		
		String ic_epo	  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
		String moneda	  = (request.getParameter("comboMoneda")==null)?"":request.getParameter("comboMoneda");

		Parametro parametro = ServiceLocator.getInstance().lookup("ParametroEJB",Parametro.class);
		
		List  regis = (ArrayList)parametro.getParametrosCajones(ic_epo, moneda);		
		registros = JSONArray.fromObject(regis);
		
		if(registros.size()>0) {
			nombreEpo = (String)registros.get(0);
			Nombremoneda = (String)registros.get(1);
			NoCajones = (String)registros.get(2);
		}
		
		if (ic_epo.equals("T")){
			nombreEpo ="Todas las EPOS";
		}
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("NOMBREEPO", nombreEpo);
		jsonObj.put("NOMBREMONEDA", Nombremoneda);
		jsonObj.put("NUMCAJONES", NoCajones);
		
		infoRegresar = jsonObj.toString();

	}else if (informacion.equals("CajasIguales")){
		String moneda	  = (request.getParameter("comboMoneda")==null)?"":request.getParameter("comboMoneda");
		JSONObject jsonObj = new JSONObject();
		
		Parametro parametro = ServiceLocator.getInstance().lookup("ParametroEJB",Parametro.class);
		String cajas = parametro.getCajonesIguales(moneda); 

		jsonObj.put("success", new Boolean(true));
		jsonObj.put("CAJASIGUALES", cajas);
		infoRegresar = jsonObj.toString();
	
	}else if (informacion.equals("Guardar")){
		String ic_epo	  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
		String moneda	  = (request.getParameter("comboMoneda")==null)?"":request.getParameter("comboMoneda");
		String noCajones = (request.getParameter("noCajones")==null)?"":request.getParameter("noCajones");	

		Parametro parametro = ServiceLocator.getInstance().lookup("ParametroEJB",Parametro.class);
		
		String  insert = parametro.guardaParametrosCajones( ic_epo,  moneda,  noCajones); 
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("OPERACION", insert);
		infoRegresar = jsonObj.toString();		
	}

%>

<%= infoRegresar %>