Ext.onReady(function(){
	var seleccionado="";
	var idRadioSeleccionado="";
	
	
//-------------Handlers----------------------------------------------------------------	
	var procesar = function (){
		Ext.getCmp('txtMonitoreo').setValue("Este proceso tardar� unos minutos. Espere por favor.");
		Ext.Ajax.request({
			url: '15actcatalogosExt.data.jsp',
			params: {
				informacion: "Enviar",
				nom_catalogo: seleccionado
			},
			callback: procesaResultado
		});	
	}
		
	
	var procesaResultado = function (opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('txtMonitoreo').reset();
			Ext.getCmp('txtMonitoreo').setValue(info.RESPUESTA);
			Ext.getCmp('btnProcesar').enable();
			Ext.getCmp(idRadioSeleccionado).setValue(false);
			idRadioSeleccionado="";
			seleccionado="";
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesaValoresIniciales = function(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			
			if (jsonValoresIniciales != null){
				var i = 0;
				var j = 1;
				var catDependientes = parseInt(jsonValoresIniciales.dependientes); 
				var catTotales = parseInt(jsonValoresIniciales.totales); 

				for (var key in jsonValoresIniciales) {
					i++;
					if (i==1 || i == 2 || i == 3){}
					else if (i<(catDependientes+4)){
						var obj = jsonValoresIniciales[key];
						var obj2 = Ext.decode(obj);
						Ext.getCmp('fs1').insert(i,obj2);
						fpCarga.doLayout();
					}else{
						j++;
						var objeto = jsonValoresIniciales[key];
						var objeto2 = Ext.decode(objeto);
						Ext.getCmp('fs2').insert(j,objeto2);
						fpCarga.doLayout();
					}
				}//fin for each


				for(var i=1; i <= catTotales ; i++){
					Ext.getCmp("idCat"+i).addListener('check',
						function(rg,checked){ 
							 if (checked == true) { 
								idRadioSeleccionado=rg.getId(); 
							   seleccionado = rg.getRawValue();
							} 
						}
					);
				}
	
				/*if (i>10)
				{
					Ext.getCmp('fs1').setAutoScroll(true);
					Ext.getCmp('fs2').setAutoScroll(true);
				}
				*/
			}
			fpCarga.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}		
		
		
//--------------Componentes-------------------------------------------------------------

	var elementosFormaCarga = [
			{
			xtype: 'compositefield',
			msgTarget: 'side',
			width: 710,
			autoHeight:true,
			items:[
				{
					xtype:'fieldset',
					title:'Catalogos Dependientes',
					id:'fs1',
					collapsible: false,
					width:250,
					height:320,
					autoHeight: true,
					items:[]
				},{
					xtype:'fieldset',
					title:'Catalogos Individuales',
					autoScroll: true,
					id:'fs2',
					collapsible: false,
					width:250,
					height: 320,
					autoHeight: true,
					items:[{
				
				xtype:  'radio', 
				fieldLabel: ' Check ',  
					name: 'AAA',   
					hidden:  true,
					id: ' base2', 
					value: ' nomCatalogo ',   	
					inputValue: 'AAA',
					width: 100
					
				}]
				
				}     ]
			},	 
				{
					xtype: 	'label',
					id:	 	'leyenda1',
					hidden: 	false,
					html:  	'<BR/><font size="2"><center>Monitoreo de las Actualizacion(es) de Catalogo(s)</center></font></h2><BR/>'
				
					},
			
			
			{
					xtype: 'textarea',
					name: 'ct_referencia',
					id: 'txtMonitoreo',
			
					allowBlank: true,
					hidden: false,
				
					width: 500,
					height: 150
			
				}
				
				

		];
		
		
	var fpCarga = new Ext.form.FormPanel({
		id: 'fpCarga',
		width: 710,
		frame: true,		
		items: elementosFormaCarga,	
		titleCollapse: false,
		style: 'margin:0 auto;',
		buttons: [
			{
				text: 'Procesar',
				id: 'btnProcesar',
				iconCls: 'icoAceptar',
				handler: function (boton,evento){
								if(seleccionado == ""){
									Ext.MessageBox.alert("Alerta","Debe de Seleccionar un Catalogo");
								}else{
									Ext.Msg.confirm('Mensaje', 
									'�Este Catalogo se Actualizara, desea Continuar?',
									function(botonConf) {
										if (botonConf == 'ok' || botonConf == 'yes' || botonConf == 'si' ) {
											procesar();
											boton.disable();
										}
									}	);	
								}	
							}
			},					
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function(boton, evento) {
					window.location = '15actcatalogosExt.jsp';

				}
			}	]
		});



//------ Componente Principal -------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [fpCarga,NE.util.getEspaciador(10) ]
	});

			Ext.Ajax.request({
			url: '15actcatalogosExt.data.jsp',
			params: {
				informacion: "ValoresIniciales"
			},
			callback: procesaValoresIniciales
		});

});