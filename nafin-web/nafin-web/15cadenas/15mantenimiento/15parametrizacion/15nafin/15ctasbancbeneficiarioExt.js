Ext.onReady(function(){ 
	
	var cveFinanciera        = "";
	var blurNacional         = false;
	var cuentaPeso           = "";
	var Banco                = "";
	var numSucursal          = "";
	var icCuentaPeso         = "";
	var cuentaDolar          = "";
	var icCuentaDolar        = "";
	var cDolar               = "";
	var cPeso                = "";

//-------------Handlers----------------------------------------------------------------------------------	
	var limpiarValores = function(){
		cuentaPeso     = "";
		Banco          = "";
		numSucursal    = "";
		icCuentaPeso   = "";
		cuentaDolar    = "";
		icCuentaDolar  = "";
		cveFinanciera  = "";
	}
	
	
	var procesarClabe= function(valor){
		var errorValidacion = false;
		var columnModelGrid = grid.getColumnModel();
		var store           = grid.getStore();	
		var numRegistro     = -1;		

		if(blurNacional){
			if(valor != ''){
				digVerificador1(valor);
			}
		}
	}
	
	
	var procesarDatos = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			cuentaPeso = info.CUENTAPESO;
			Banco = info.BANCO;
			cveFinanciera = Banco;
			numSucursal = info.NUMSUCURSAL;
			icCuentaPeso = info.ICCUENTAPESO;
			cuentaDolar = info.CUENTADOLAR;
			icCuentaDolar = info.ICCUENTADOLAR;

			if(cuentaPeso == ''){
				NoDatos = true;
			}else{
				NoDatos = false;
			}

			ConsultaDataGrid.load({
			params: {	
					informacion : "ConsultaDataGrid"
			}});
			
			grid.show();
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}


	var digVerificador1= function (valor){
			var ruta = document.registro;			
			var car = "";
			var resultado = "";
			var residuo = 0;    
			var mulIndividual = 0;
			var digVerificador = 0;  
			var contenedor = 0;
			var cadena="";
			
			cadena = ""+valor;
			if (cadena != ""){
				digVerificador = parseInt(cadena.substr(17));
				
				for (i=0; i <= 16; i++){
					car = cadena.substring(i,i+1);
					switch(i){
						case 0: case 3: case 6: case 9: case 12: case 15:			/*3*/
							mulIndividual = parseInt(car)*3;
							contenedor += mulIndividual;
							break;
						case 1: case 4: case 7: case 10: case 13: case 16:			/*7*/
							mulIndividual = parseInt(car)*7;
							contenedor += mulIndividual;
							break;
						case 2: case 5: case 8: case 11: case 14:					/*1*/
							mulIndividual = parseInt(car)*1;
							contenedor += mulIndividual;
							break;
					}
				}			
				residuo = contenedor % 10;  /*Obtiene el residuo */
				
				if (residuo > 0)  
					contenedor = 10 - residuo;
				else
					contenedor = 0;
				
				if (digVerificador != contenedor){
					var columnModelGrid = grid.getColumnModel();
									 										
					Ext.Msg.alert('Mensaje', 
					'Cuenta clabe incorrecta, favor de verificar',
					function(botonConf) {												
						blurNacional = true;
						grid.startEditing(0, columnModelGrid.findColumnIndex('CUENTA'));												
					});
				} 
			}
		}
	
	
	var procesarResultado = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			grid.hide();
			Ext.Msg.alert('Mensaje', info.RESULTADO,
			function(){
				limpiarValores();
			
				Ext.Ajax.request({
					url: '15ctasbancbeneficiarioExt.data.jsp',
					params: {
						informacion: 'ConsultaDatos',
						comboEpo: Ext.getCmp('idComboEpo').getValue(), 
						comboIF: Ext.getCmp('idComboIF').getValue()			
					},
					callback: procesarDatos
				});
			});
		
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarGuardar = function(){
		var columnModelGrid = grid.getColumnModel();
		var store = grid.getStore();
		var numRegistro = -1;
		var guardar = false; 
		var datosCompletos = true;
		var tipoError;
		
		store.each(function(record) {	
			numRegistro = store.indexOf(record);
			if(numRegistro == 0){
				if(!Ext.isEmpty(record.data['CUENTA']) || !Ext.isEmpty(cveFinanciera) || !Ext.isEmpty(record.data['NUMSUCURSAL']) ){				
					//if(record.data['CUENTA'] != '' && cveFinanciera != '' && record.data['NUMSUCURSAL'] != ''){
					if(!Ext.isEmpty(record.data['CUENTA']) && !Ext.isEmpty(cveFinanciera) && !Ext.isEmpty(record.data['NUMSUCURSAL'])){
						guardar = true;
						cPeso = record.data['CUENTA'];
						nSucursal = record.data['NUMSUCURSAL'];									
					}else{
						datosCompletos = false
						tipoError = 'Informaci�n incompleta para cuenta bancaria - moneda nacional';
					}
				}else{
					tipoError = 'Debe al menos capturar una cuenta bancaria';
				}
			}
			
			if(numRegistro == 1){
				if(!Ext.isEmpty(record.data['CUENTA']) && datosCompletos){
					guardar = true;
					cDolar = record.data['CUENTA'];
				}
			}
		});
	
		if(!guardar){						
			Ext.Msg.alert('Alerta', tipoError,function(){ 
				Ext.getCmp('btnGuardar').enable();
			if (NoDatos == false){	
				Ext.Ajax.request({
					url: '15ctasbancbeneficiarioExt.data.jsp',
					params: {
						informacion: 'ConsultaDatos',
						comboEpo: Ext.getCmp('idComboEpo').getValue(), 
						comboIF: Ext.getCmp('idComboIF').getValue()			
					},
					callback: procesarDatos
				});	
			}
			});
		}else{					
			Ext.Ajax.request({
				url: '15ctasbancbeneficiarioExt.data.jsp',
				params: {
					informacion: 'Guardar',
					comboEpo: Ext.getCmp('idComboEpo').getValue(), 
					comboIF: Ext.getCmp('idComboIF').getValue(),			
					comboFinanciera: cveFinanciera,
					CuentaDolar: cDolar,
					CuentaPeso: cPeso,
					nSucursal: nSucursal,
					icDolar: icCuentaDolar,
					icPeso: icCuentaPeso
				},
				callback: procesarResultado
			});											
		}	
	}
	
	
	var procesarTodosEpo = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar...", 
		loadMsg: ""})); 		
		store.commitChanges(); 
		Ext.getCmp('idComboEpo').setValue('');
	}
			
	
	var procesarTodosIf = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar...", 
		loadMsg: ""})); 		
		store.commitChanges(); 
		Ext.getCmp('idComboIF').setValue('');
	}

	var procesarTodosFinanciera = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccione un Banco", 
		loadMsg: ""})); 		
		store.commitChanges(); 
		Ext.getCmp('idComboFinanciera').setValue('');
	}

	var procesarConsultaData = function(store,arrRegistros,opts){		
		store.insert(0,new Todas({ 
			CUENTA : cuentaPeso, 
			BANCO: Banco, 
			MONEDA: "MONEDA NACIONAL",
			NUMSUCURSAL: numSucursal,
			ICCUENTAPESO: icCuentaPeso,
			ICCUENTADOLAR: ""
		}	)); 		

		store.insert(1,new Todas({ 
			BANCO: "", 
			MONEDA: "DOLAR AMERICANO",
			NUMSUCURSAL: "",
			ICCUENTAPESO: "",
			CUENTA: cuentaDolar,
			ICCUENTADOLAR: icCuentaDolar
		}		)); 
		
		store.commitChanges();
		Ext.getCmp('btnConsultar').enable();
		Ext.getCmp('btnGuardar').enable();
	}


//-------------Stores----------------------------------------------------------------------------------	
	Todos = Ext.data.Record.create([ 
    {name: "CUENTAPESO", type: "string"}, 
    {name: "BANCO", type: "string"}, 
    {name: "NUMSUCURSAL", type: "string"},
    {name: "ICCUENTAPESO", type: "string"}, 
    {name: "CUENTADOLAR", type: "string"}, 
    {name: "ICCUENTADOLAR", type: "string"},
	 {name: "MONEDA", type: "string"}
	]);
	
	
	Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);

	
	var CatalogoEpo= new Ext.data.JsonStore({
		id: 'catEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ctasbancbeneficiarioExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoEpo'
		},
		autoLoad: false, 
		listeners:
		{
		 load: procesarTodosEpo,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	  });


	var CatalogoIF= new Ext.data.JsonStore({
		id: 'catIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ctasbancbeneficiarioExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoIF'
		},
		autoLoad: false, 
		listeners:
		{
		 load: procesarTodosIf,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	  });


	var CatalogoFinanciera = new Ext.data.JsonStore({

		id: 'catFin',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ctasbancbeneficiarioExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoFinanciera' 
		},
		autoLoad: true, 
		listeners:
		{
		 load: procesarTodosFinanciera, 
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	  });


	var ConsultaDataGrid = new Ext.data.JsonStore({
		root: 'registros',
		url: '15ctasbancbeneficiarioExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaDataGrid'
		},
		fields: [
				{name: 'CUENTA'},
				{name: 'BANCO'},
				{name: 'NUMSUCURSAL'},
				{name: 'ICCUENTAPESO'},
				{name: 'ICCUENTADOLAR'},
				{name: 'MONEDA'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
	
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					}
			}
		}
	});


//--------------Componentes------------------------------------------------------------------------------
	var elementosForma = 
	[	
		{
		xtype: 'combo',
		fieldLabel: 'EPO',
		forceSelection: true,
		autoSelect: true,
		name:'claveEpo',
		id:'idComboEpo',
		displayField: 'descripcion',
		allowBlank: true,
		editable:true,
		valueField: 'clave',
		mode: 'local',
		triggerAction: 'all',
		autoLoad: false,
		typeAhead: true,
		minChars: 1,
		store: CatalogoEpo,  
		width:270,
		
		listeners:{
			select: function(combo){
						CatalogoIF.load({
							params: {cveepo:combo.getValue() }
						});
					}   
				}
		},	{
				xtype: 'combo',
				fieldLabel: 'IF',
				forceSelection: true,
				editable: true,
				autoSelect: true,
				name:'claveIF',
				id:'idComboIF',
				displayField: 'descripcion',
				emptyText: 'Seleccionar...',
				allowBlank: true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: CatalogoIF,  
				width:400,
				listeners:{
					select: function(combo){
						grid.hide();
					}
				}   			
			}	
	]


	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
		style: 'margin:0 auto;',	   
		height: 130,
		buttons: [{
		text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoLupa',
				handler: function (boton,evento){
								if(Ext.getCmp('idComboEpo').getValue() == ''){
								//Ext.MessageBox.alert("Alerta","El valor de la EPO es requerido.");
									Ext.getCmp('idComboEpo').markInvalid("El valor de la EPO es requerido.");
								}else if (Ext.getCmp('idComboIF').getValue() == ''){
								//Ext.MessageBox.alert("Alerta","El valor del beneficiario es requerido");
									Ext.getCmp('idComboIF').markInvalid("El valor del beneficiario es requerido");
								}else{								
									boton.disable();
									Ext.Ajax.request({
										url: '15ctasbancbeneficiarioExt.data.jsp',
										params: {
											informacion: 'ConsultaDatos',
											comboEpo: Ext.getCmp('idComboEpo').getValue(), 
											comboIF: Ext.getCmp('idComboIF').getValue()			
										},
										callback: procesarDatos
									});									
								}
				}
		},{
		text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function (boton,evento){
					boton.disable();
					window.location = '15ctasbancbeneficiarioExt.jsp';
				}
				}],
				width: 580
	});


	var grid= new Ext.grid.EditorGridPanel({
		store: ConsultaDataGrid,
		sortable: true,
		clicksToEdit:1,
		id:'grid',
		hidden: true,  //si hay error en desarrollo ocultar hasta el final
		columns: [
			{
				header: '<center>Moneda</center>',
				dataIndex: 'MONEDA',
				width: 180,
				align: 'left'				
			},{
				header: '<center>Cuentas CLABE/Siwft</center>',
				dataIndex: 'CUENTA', 
				align: 'center',
				width: 170,
				editor: { 
					xtype:'textfield',
					height: 10,
					maxLength: 18,
					id:'txtValor',
									listeners:{
									blur: function(field){
											procesarClabe(field.getValue());																							
										}

									}//fin listener
				},
				renderer: function(value, metadata, record, rowIndex, colIndex, store){            	
								return NE.util.colorCampoEdit(value,metadata,record);					
				}				
			},{
				header: '<center>Nombre del Banco</center>',
				dataIndex: 'BANCO',
				align: 'left',
				width: 250,
				editor: { 
					xtype: 'combo',
					forceSelection: true,
					autoSelect: true,
					name:'claveFinanciera',
					id:'idComboFinanciera',
					displayField: 'descripcion',
					allowBlank: true,
					editable:true,
					hiddenName:'claveFinanciera',
					valueField: 'clave',
					mode: 'local',
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					store: CatalogoFinanciera,  
					width:400,
					listeners:{
						select: function(combo){
							cveFinanciera = combo.getValue();
						}
					}  
				},
				 renderer:function(value,metadata,record2,rowIndex,colIndex,store){
					var record = Ext.getCmp('idComboFinanciera').findRecord(Ext.getCmp('idComboFinanciera').valueField, value);
					if(rowIndex == '1'){
						return '<center>N/A</center>';
					}else{
						record ? text = record.get(Ext.getCmp('idComboFinanciera').displayField) : text = "Seleccione un Banco";
						return NE.util.colorCampoEdit(text,metadata,record);
	
					}
				}
			},{
				header: '<center>N�mero y/o Nombre de <BR>Sucursal</center>',
				dataIndex: 'NUMSUCURSAL',
				align: 'left',				 
				width: 170,
				editor: { 
					xtype: 'textfield',
					name: 'txt_Sucursal',
					maxLength: 50,
					id: 'txtSucursal'

				},
				renderer: function(value, metadata, record, rowIndex, colIndex, store){            	
					if (rowIndex != '1')
						return NE.util.colorCampoEdit(value,metadata,record);					
					else
						return '<center>N/A</center>';
				}
			}
		],
		loadMask: true,	
		style: 'margin:0 auto;',
		frame: true,
		height: 150,
				
		bbar: {
					autoScroll:true,
					id: 'barra',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnGuardar',
						text:		'Guardar',
						iconCls: 'icoGuardar',
						width: 50,
						weight: 70,
						style: 'margin:0 auto;',
						handler: function (boton,evento){
							boton.disable();
							procesarGuardar();
						},
							align:'rigth'
					},
					{
						xtype:	'button',
						id:		'btnCancelar',
						text:		'Cancelar',
						iconCls: 'icoCancelar',
						width: 50,
						handler: function(boton, evento) {
								grid.hide();
						},						
						weight: 70,
						style: 'margin:0 auto;',
						align:'rigth'
					}
			]
		},	
      width: 790,
		listeners:{						
			rowclick: function( grid,rowIndex,e ){
				if(rowIndex == 0){
					blurNacional = true;
				}else{
					blurNacional = false;
				}				
			},						
			beforeedit: function(object){
				if ((object.field == 'NUMSUCURSAL' || object.field == 'BANCO') && object.row == '1'){
					object.cancel = true;
				}

			}//finbefore	
		}//fin listener	
	});


//------ Componente Principal --------------------------------------------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [fp, NE.util.getEspaciador(10), grid]
	});

	CatalogoEpo.load();
	//CatalogoIF.load();

});