<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession2.jsp" %>
<%
String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
String logo = request.getParameter("logo");
String nombreArchivoZip = request.getParameter("nombreArchivoZip");

String operacion2 = (request.getParameter("operacion")==null)?"CAPTURA":request.getParameter("operacion");
%>
<input type="hidden" name="nombreArchivoZip" value="<%=nombreArchivoZip%>">
<input type="hidden" name="ic_epo" value="<%=ic_epo%>">
<input type="hidden" name="operacion" value="<%=operacion2%>">
<html>

<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/15cadenas/15mantenimiento/15parametrizacion/15nafin/15previewExtjs.jspf" %>
<%@ include file="/15cadenas/15mantenimiento/15parametrizacion/15nafin/15preview_menu_nuevo.jspf"%>
<script type="text/javascript">
Ext.onReady(function(){
function getUrlVar() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			vars[key] = value==''?null:value;
		});
		return vars;
	}
var ic_epo = getUrlVar()['ic_epo'];
var  nombreArchivoZip=getUrlVar()['nombreArchivoZip'];
var operacion=getUrlVar()['operacion'];

var banderaAccion;
var mensaje;
var txtButt='Descargar Archivo';


	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('btnDesc');
			boton.setIconClass('');
			boton.enable();	
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesaAceptada = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			 Ext.Msg.show({
									title:	"Mensaje",
									msg:		'Los cambios han sido realizados con ?xito',
									buttons:	Ext.Msg.OK,
									height: 50,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											parent.window.close();
										}
									},
									closable:false
								});

		}else{
				// Mostrar mensaje de error
				Ext.Msg.alert("Mensaje de p?gina web.",'Hubo un error durante la aplicación del cambio.' );
		}
	}

if(operacion=='CAPTURA'){
	txtButt='Aceptar';
	mensaje='El dise&ntilde;o para la EPO '+ic_epo+' se muestra a continuaci&oacute;n. Por favor, confirme los cambios.';
}
else{
	mensaje='El dise&ntilde;o para la EPO '+ic_epo+' se muestra a continuaci&oacute;n.';
}
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 250,
		width: 350,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		frame: true,
		id: 'forma',
		defaults:
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [{
				xtype: 	'label',
				id:	 	'leyenda',
				hidden: 	false,
				cls:		'x-form-item',
				style: 	'font size:1; text-align:center;margin:0 auto;',
				html:  	'<b>'+mensaje+'</b>'
			}],
		buttons:
		[
			 {
			  //Bot?n BUSCAR
				xtype: 'button',
				text: txtButt,
				id: 'btnDesc',
				name: 'btnBuscar',
				hidden: false,
				formBind: true,
				handler: function(boton, evento) 
				{
			if(operacion=='CAPTURA'){
					Ext.Ajax.request({
						url: '15AdmNafParamImgExt.data.jsp',	
						params: {
									
									informacion: 			"disenoAceptado",
									nombreArchivoZip:nombreArchivoZip,
									ic_epo: ic_epo
						},callback: procesaAceptada
					});
					}else{
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '15AdmNafParamImgExt.data.jsp',	
						params: {
									
									informacion: 			"descargaArchivo",
									nombreArchivoZip:nombreArchivoZip,
									ic_epo: ic_epo
						},callback: procesarArchivoSuccess
					});
						
					}
				}
			 },{
				text:'Salir',
				xtype: 'button',
				align: 'right',
				handler: function() {
						
						 parent.window.close();
				}
			 }
			 
		]
	});
	
	var verDetalles = new Ext.Window({
			title: 'Confirmaci?n',
			width: 400,
			height: 300,
			maximizable: false,
			modal: false,
			closable: false,
			draggable: false,
			resizable: false,
			maximized: false,
			constrain: true,
			items: [fp]
});	
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [		
	  verDetalles
	  ]
  });
  verDetalles.show();
	verDetalles.setPosition(350, 700);
});

</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

				<%@ include file="/15cadenas/15mantenimiento/15parametrizacion/15nafin/15preview_cabeza_nuevo.jspf"%>
				
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/15cadenas/15mantenimiento/15parametrizacion/15nafin/15preview_menuLateral.jspf"%>
						<div id="areaContenido"><div style="height:190px"></div></div>
					</div>
				</div>
				<%@ include file="/15cadenas/15mantenimiento/15parametrizacion/15nafin/15preview_pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>

</body>
</html>