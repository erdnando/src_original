<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.distribuidores.*,
	com.netro.model.catalogos.*,	
	com.netro.exception.*,	
	com.netro.parametrosgrales.*,
	java.text.SimpleDateFormat,
	java.util.Date,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>



<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String operaMesesEPO	  = (request.getParameter("operaMesesEPO")==null)?"":request.getParameter("operaMesesEPO");

	HashMap datos = new HashMap(); 
	JSONArray registros = new JSONArray();  
	JSONObject 	jsonObj	= new JSONObject();   

	ParametrosDist  BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
   ParametrosGrales  BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB", ParametrosGrales.class); 
	

	if (informacion.equals("CatalogoEpo")){	
		CatalogoEPO cat = new CatalogoEPO();
		
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("cg_razon_social");	
		
		infoRegresar=cat.getJSONElementos();
	
	}else if (informacion.equals("CatalogoProducto")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_producto_nafin");
		cat.setCampoClave("ic_producto_nafin");
		cat.setCampoDescripcion("ic_nombre");
		
		List lis= new ArrayList ();
		lis.add("1");
		lis.add("4");
				
		cat.setValoresCondicionIn(lis);   
		cat.setOrden("ic_producto_nafin");		
		
		infoRegresar=cat.getJSONElementos();
	
	}else if (informacion.equals("CatalogoMoneda")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_moneda");
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre");
		
		List lis= new ArrayList ();
		lis.add("1");
		lis.add("54");
		
		cat.setValoresCondicionIn(lis);   
		cat.setOrden("ic_moneda");		
		
		infoRegresar=cat.getJSONElementos();
	
	
	}else if (informacion.equals("CatalogoOperacion")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_base_operacion");
		cat.setCampoClave("ig_codigo_base");
		cat.setCampoDescripcion("ig_codigo_base ||' '||cg_descripcion");
   
		cat.setOrden("ig_codigo_base");		
		
		infoRegresar=cat.getJSONElementos();	
		
	}else if (informacion.equals("parametrizacionEPO")){
		
		String epo		  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
		operaMesesEPO	  =   BeanParametro.DesAutomaticoEpo(epo,"CS_MESES_SIN_INTERESES"); 
		
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));		
		jsonObj.put("operaMesesEPO", operaMesesEPO);	

		infoRegresar = jsonObj.toString();
		
	}else if (informacion.equals("ConsiguePlazo")){
	
		String epo		  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
		String moneda	  = (request.getParameter("comboMoneda")==null)?"":request.getParameter("comboMoneda");
		String producto  = (request.getParameter("comboProducto")==null)?"":request.getParameter("comboProducto");
		String cartera	  = (request.getParameter("comboCartera")==null)?"":request.getParameter("comboCartera");
		String plazo	  = (request.getParameter("tipoPlazo")==null)?"":request.getParameter("tipoPlazo");
		String comboTipoPago	  = (request.getParameter("comboTipoPago")==null)?"1":request.getParameter("comboTipoPago");
		if(comboTipoPago.equals(""))  { comboTipoPago = "1";  }
		
		HashMap hm = new HashMap();		 
		
		hm =(HashMap)BeanParametrosGrales.enviaEPOCapturaCadenas(epo,producto,cartera,plazo,moneda, comboTipoPago ); 
			
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("PLAZO", hm.get("PLAZO"));
		jsonObj.put("ULTIMO", hm.get("ULTIMO"));		

		infoRegresar = jsonObj.toString();
	
		
	}else if (informacion.equals("ConsultaGrid") ){
		List regis = new ArrayList();
		
		String epo		  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
		String moneda	  = (request.getParameter("comboMoneda")==null)?"":request.getParameter("comboMoneda");
		String producto  = (request.getParameter("comboProducto")==null)?"":request.getParameter("comboProducto");
		String cartera	  = (request.getParameter("comboCartera")==null)?"":request.getParameter("comboCartera");
		String plazo	  = (request.getParameter("tipoPlazo")==null)?"":request.getParameter("tipoPlazo");
	   String comboTipoPago	  = (request.getParameter("comboTipoPago")==null)?"":request.getParameter("comboTipoPago");
	
			
		regis =(ArrayList)BeanParametrosGrales.getCamposCapturaCadenas(epo,producto,cartera,plazo,moneda, comboTipoPago ); 
		registros = JSONArray.fromObject(regis);

		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("Aceptar")){
	
		String epo		  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
		String moneda	  = (request.getParameter("comboMoneda")==null)?"":request.getParameter("comboMoneda");
		String producto  = (request.getParameter("comboProducto")==null)?"":request.getParameter("comboProducto");
		String cartera	  = (request.getParameter("comboCartera")==null)?"":request.getParameter("comboCartera");
		String plazo	  = (request.getParameter("tipoPlazo")==null)?"":request.getParameter("tipoPlazo");
		String vMin	  = (request.getParameter("valorMinimo")==null)?"0":request.getParameter("valorMinimo");
		String vMax	  = (request.getParameter("valorMaximo")==null)?"0":request.getParameter("valorMaximo");
		String operacion	  = (request.getParameter("comboOperacion")==null)?"":request.getParameter("comboOperacion");
		String acreditado	  = (request.getParameter("comboAcreditado")==null)?"":request.getParameter("comboAcreditado");
		String comboTipoPago	  = (request.getParameter("comboTipoPago")==null)?"":request.getParameter("comboTipoPago");
		
		// 09-2015
		if(comboTipoPago.equals("2")) {
			 Vector vecParametros = new Vector();
			 vMin="1"; 
			 vecParametros =  BeanParametro.getParamEpoa( epo);
				String plazoMeses 	= (vecParametros.get(16)==null)?"": vecParametros.get(16).toString().trim(); // Fodea 13-2014
				int totaldiasMeses = 30 * Integer.parseInt(plazoMeses) ;
		      vMax=String.valueOf(totaldiasMeses);
		}else {
			comboTipoPago ="1"; 
		}
		
		
		String mensaje = BeanParametrosGrales.aceptarCapturaCadenas(epo,producto,cartera,plazo,moneda,operacion,vMin,vMax,acreditado, comboTipoPago );
	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("MENSAJE",mensaje);
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("Eliminar")){
		String mensaje = "";
		String clave	  = (request.getParameter("baseOperacion")==null)?"":request.getParameter("baseOperacion");
		
		boolean resul = BeanParametrosGrales.eliminarCapturaCadenas(clave);
		if(!resul)
			mensaje= "Error al eliminar";
	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("MENSAJE",mensaje);
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("Modificar")){
		String mensaje = "";
		String plazo	  = (request.getParameter("plzMax")==null)?"":request.getParameter("plzMax");
		String operacion	  = (request.getParameter("baseOperacion")==null)?"":request.getParameter("baseOperacion");
		String recurso	  = (request.getParameter("recurso")==null)?"":request.getParameter("recurso");
	
				
		boolean resul = BeanParametrosGrales.modificarCapturaCadenas(plazo,recurso,operacion);
		if(!resul)
			mensaje= "Error al modificar registro";
	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("MENSAJE",mensaje);
		infoRegresar = jsonObj.toString();
	
	}else  if  (informacion.equals("ValidaMesesSinIntereses") ){ 
	
	String epo		  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
	
	String totalMeses = BeanParametrosGrales.getvalidaMesesSinIntereses(epo);
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("TOTALMESES",totalMeses);
	infoRegresar = jsonObj.toString();
	
}	

%>

<%= infoRegresar %>
