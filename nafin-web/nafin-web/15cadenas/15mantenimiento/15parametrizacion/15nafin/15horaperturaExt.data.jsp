<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.procesos.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
	String infoRegresar = ""; 
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	if (informacion.equals("CatalogoProducto")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("COMCAT_PRODUCTO_NAFIN");
		cat.setCampoClave("ic_producto_nafin");
		cat.setCampoDescripcion("ic_nombre");
		
		List lis= new ArrayList ();
		lis.add("1");
		lis.add("4");
		
		cat.setValoresCondicionIn(lis);
		cat.setOrden("ic_producto_nafin");		
		
		infoRegresar=cat.getJSONElementos(); 
	}
	else if (informacion.equals("ConsultaGrid")){
		JSONObject jsonObj        = new JSONObject();
		HashMap datos             = new HashMap();
		JSONArray registros       = new JSONArray();
		boolean[] estatusServicio = new boolean[3]; 
		
		String ic_producto		  = (request.getParameter("ic_producto")==null)?"":request.getParameter("ic_producto");
		
		try{
			
			ManejoServicio BeanManejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);
			Vector infoProducto = BeanManejoServicio.getInfoProducto(ic_producto);
			HashMap hm=new HashMap();

			int totalProductos = infoProducto.size();
			
			for(int i = 0;i<totalProductos;i++) {
				Vector producto = (Vector)infoProducto.get(i); 
				estatusServicio = BeanManejoServicio.getEstatusServicio(((Integer)producto.get(0)).intValue());	
				
				datos = new HashMap();
				datos.put("producto",producto.get(1).toString());
				datos.put("claveProducto",""+producto.get(0));
				datos.put("bcierrepyme",""+estatusServicio[0]);
				datos.put("bcierreif",""+estatusServicio[1]);
				
				registros.add(datos);
			}
		
			String consulta =  "{\"success\": true, \"total\": \"" + totalProductos + "\",\"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);	
			
			infoRegresar = jsonObj.toString();
		}catch(Exception e){
			System.out.println("Error en llenado de estatus de Servicio " + e);
		}
	
	}else if(informacion.equals("ConsultaServicio")){

		boolean servicioAbierto           = false; 
		boolean servicioPymes 	          = true;
		String horaInicioProceso          = "";
		String bDesctoElectronico         = "";
		String bProcesoVencidosOperar     = "";
		String bProcesoVencidosOperarEjec = "";
	
		String chk_apertura_servicio[]	= request.getParameterValues("checkbox");
	
		 try{
			
			ManejoServicio BeanManejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);
				
			if(chk_apertura_servicio!=null){
				servicioAbierto = BeanManejoServicio.abrirServicio(chk_apertura_servicio,"N");
		   }
		
			HashMap hm = new HashMap();
			hm = (HashMap)BeanManejoServicio.getValidaciones();
		
			bProcesoVencidosOperarEjec=hm.get("bProcesoVencidosOperarEjec").toString();
			bProcesoVencidosOperar=hm.get("bProcesoVencidosOperar").toString();
			bDesctoElectronico=hm.get("bDesctoElectronico").toString();
			
			horaInicioProceso=hm.get("horaInicioProceso").toString();
		
			JSONObject jsonObj = new JSONObject();	
			JSONArray registros = new JSONArray();
			HashMap datos = new HashMap();
			
			boolean banderaDescuento=false; 
		
			for (int i=0;i<chk_apertura_servicio.length;i++){
				if(chk_apertura_servicio[i].equals("1")){
					banderaDescuento= true;
				break;
				}
			}
		
			int     cont = 0;
			boolean band = false;
			
			if(banderaDescuento){
			
				if( servicioAbierto && bProcesoVencidosOperarEjec.equals("false") && bProcesoVencidosOperar.equals("true")){
						band = true;
				}
				else{
						if(!servicioAbierto && bProcesoVencidosOperar.equals("true")){
							datos.put("ERROR","No se puede aperturar el servicio <BR >ya que no existen Tasas Operativas capturadas, <BR>para el día de hoy.");
							datos.put("SOLUCION","Capturar las tasas correspondientes.");
							registros.add(datos);
							cont++;
						}
						if(bProcesoVencidosOperarEjec.equals("true")){
							datos.put("ERROR","Se esta ejecutando el Proceso de Vencidos <BR> sin Operar");
							datos.put("SOLUCION","Inicio a las "+horaInicioProceso+" hrs. espere a que finalize <BR>por favor.");
							registros.add(datos);
							cont++;
						}		
						if(bProcesoVencidosOperar.equals("false")){
							datos.put("ERROR","No se ejecuto el Proceso de Vencidos <BR>sin Operar");
							datos.put("SOLUCION","Comuniquese con el administrador del <BR>sistema.");
							registros.add(datos);
							cont++;
						}
				}  
			}else if(!banderaDescuento){ 
				if(servicioAbierto){
					band = true;
				}else {
					if(!servicioAbierto){
						datos.put("ERROR","No se puede aperturar el servicio ya que no <BR/>existen Tasas Operativas capturadas, <BR/>para el día de hoy.");
						datos.put("SOLUCION","Capturar las tasas correspondientes.");
						registros.add(datos);
						cont++;
					}
				}
			}
			
			String consulta =  "{\"success\": true,\"success2\": "+ band +", \"total\": \"" + cont + "\",\"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);	
				
			infoRegresar = jsonObj.toString();
			
		}catch(Exception e){
			System.out.println("Error en apertura servicio "+e);
		}
	}else if(informacion.equals("ConsultaGrid24")){
		
		JSONObject jsonObj        = new JSONObject();
		HashMap datos             = new HashMap();
		JSONArray registros       = new JSONArray();
		boolean[] estatusServicio = new boolean[3]; 
		
		try{
			
			ManejoServicio BeanManejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);
			Vector infoProducto = BeanManejoServicio.getInfoProducto("1");
			HashMap hm=new HashMap();

			int totalProductos = infoProducto.size();
						
			Vector producto = (Vector)infoProducto.get(0); 
			estatusServicio = BeanManejoServicio.getEstatusServicio(((Integer)producto.get(0)).intValue());	
				
			datos = new HashMap();
			datos.put("producto",producto.get(1).toString());
			datos.put("claveProducto",""+producto.get(0));
				
			//Revisa que estén capturadas todas las tasas del día siguiente hábil	
			boolean tasasDiaSig = BeanManejoServicio.revisarTasasDiaHabilSiguient();
			
			//Revisa el estado en que se encuentra la bandera de cierre de servicio
			boolean BanderaCierreServicio= BeanManejoServicio.fnChkCierrePymeDescuentoE(); 
			
			if((tasasDiaSig == false) || (BanderaCierreServicio == true) ){
				datos.put("bcierre24hrs","true");
			}else{
				//Implementando el nuevo metodo para obtener un boolean que indicas si todas las pymes estan en el horario
				boolean lbCierre24hrs = BeanManejoServicio.fnChkCierre24Hrs();
				datos.put("bcierre24hrs",""+lbCierre24hrs);
			}
				
			registros.add(datos);
			
			String consulta =  "{\"success\": true, \"total\": \"" + totalProductos + "\",\"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);	
			
			infoRegresar = jsonObj.toString();
		}catch(Exception e){
			System.out.println("Error en llenado de estatus de Servicio 24 hrs " + e);
		}

	}else if(informacion.equals("ConsultaServicio24")){

		boolean servicioAbierto           = false; 
		boolean servicioPymes 	          = true;
		String horaInicioProceso          = "";
		String bDesctoElectronico         = "";
		String bProcesoVencidosOperar     = "";
		String bProcesoVencidosOperarEjec = "";
	
		String chk_apertura_servicio[]	= request.getParameterValues("checkbox24");
		
		 try{
			
			ManejoServicio BeanManejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);
				
			if(chk_apertura_servicio!=null){
				servicioAbierto = BeanManejoServicio.abrirServicio24hrs(chk_apertura_servicio,"N");
		   }
		
			HashMap hm = new HashMap();
			hm = (HashMap)BeanManejoServicio.getValidaciones();
		
			bProcesoVencidosOperarEjec=hm.get("bProcesoVencidosOperarEjec").toString();
			bProcesoVencidosOperar=hm.get("bProcesoVencidosOperar").toString();
			bDesctoElectronico=hm.get("bDesctoElectronico").toString();
			
			horaInicioProceso=hm.get("horaInicioProceso").toString();
		
			JSONObject jsonObj = new JSONObject();	
			JSONArray registros = new JSONArray();
			HashMap datos = new HashMap();
			
			int     cont = 0;
			boolean band = false;
			boolean bandCierre24 = false;
			
				if( servicioAbierto && bProcesoVencidosOperarEjec.equals("false") && bProcesoVencidosOperar.equals("true")){
						band = true;
						bandCierre24 = BeanManejoServicio.fnChkCierre24Hrs();
							
				}else{
						
						if(!servicioAbierto && bProcesoVencidosOperar.equals("true")){
							datos.put("ERROR","No se puede aperturar el servicio <BR>ya que no existen Tasas Operativas capturadas, <BR>para el siguiente día hábil.");
							datos.put("SOLUCION","Capturar las tasas correspondientes.");
							registros.add(datos);
							cont++;
						}
						if(bProcesoVencidosOperar.equals("false")){
							datos.put("ERROR","No se ejecuto el Proceso de Vencidos <BR>sin Operar");
							datos.put("SOLUCION","Comuniquese con el administrador del <BR>sistema.");
							registros.add(datos);
							cont++;
						}
				}
			
			String consulta =  "{\"success\": true,\"success2\": "+ band +",\"success3\": "+ bandCierre24 +", \"total\": \"" + cont + "\",\"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);	
				
			infoRegresar = jsonObj.toString();
			
		}catch(Exception e){
			System.out.println("Error en apertura servicio 24 horas "+e);
		}
	}
%>
<%= infoRegresar %>