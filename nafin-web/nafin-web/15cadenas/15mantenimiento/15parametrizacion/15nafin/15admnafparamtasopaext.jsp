<!DOCTYPE html>
<%@page contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<% 
String pagina   = request.getParameter("pagina")==null?"Captura":request.getParameter("pagina");
String version = (String)session.getAttribute("version");
if("CALL CENTER CAD".equals(strPerfil)){ // F021 - 2015
	pagina = "Consulta";
}
String idMenuP =  request.getParameter("idMenu")==null?"15ADMNAFPARAMTASOPA":request.getParameter("idMenu");
  

%>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
      
      <%@ include file="/extjs.jspf" %>
		<%if(version!=null){%>
		<%@ include file="/01principal/menu.jspf"%>
		<%}%>
      <script language="JavaScript1.2" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
		<%if(pagina.equals("Captura")) {%>
			<script type="text/javascript" src="15admnafparamtasopaext.js?<%=session.getId()%>"></script>
		<%}else if(pagina.equals("Consulta")) { %>
			<script type="text/javascript" src="15admnafparamtasopaCext.js?<%=session.getId()%>"></script>
		<%}%>
      
      <title>Nafi@net - Tasas Operativas</title>
   </head>   

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%if(version!=null){%>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<%}%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%if(version!=null){%>
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<%}%>
			<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
		</div>
	</div>
	<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<%}%>
	<form id='formAux' name="formAux" target='_new'></form>
	<form id='formParametros' name="formParametros">
	<input type="hidden" id="idMenuP" name="idMenuP" value="<%=idMenuP%>"/>	
	</form>
	</body>
</html>