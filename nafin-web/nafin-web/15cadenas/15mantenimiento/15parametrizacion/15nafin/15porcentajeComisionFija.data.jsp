<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		netropology.utilerias.*,
		com.netro.catalogos.*,
		javax.naming.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String comision = (request.getParameter("comision")!=null)?request.getParameter("comision"):"";
String infoRegresar	=	"", mensaje = "";

ActualizacionCatalogos actualizacionCatalogos = ServiceLocator.getInstance().lookup("ActualizacionCatalogosEJB",ActualizacionCatalogos.class);

if(informacion.equals("DatosIniciales") ) {
	try {
		String  obtenerPorcentaje=actualizacionCatalogos.consultarPorcentajeComision("ADMIN NAFIN");
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("obtenerPorcentaje", obtenerPorcentaje);
		jsonObj.put("success",new Boolean(true));
		infoRegresar= jsonObj.toString();
	} catch(Exception e) {
	  throw new AppException("Error en la paginacion", e);
	 }
}else if(informacion.equals("Guardar_Datos")){
	try {
		
		boolean  obtenerPorcentaje=actualizacionCatalogos.guardarPorcentajeComision(comision, "ADMIN NAFIN");
		if(obtenerPorcentaje==true){
			mensaje = "Porcentaje Actualizado.";
		}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("mensaje", mensaje);
		jsonObj.put("accion", "G");
		infoRegresar = jsonObj.toString();
	}catch(Throwable t){
		t.printStackTrace();
		throw t;
	}
}
%>
<%=infoRegresar%>

