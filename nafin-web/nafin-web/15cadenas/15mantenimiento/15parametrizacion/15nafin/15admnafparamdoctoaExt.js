Ext.onReady(function(){

	var estatusActual; 

//-------------Handlers-----------------------------------------------------------------	
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarCatalogo = function(store, arrRegistros, opts) {
		
		if(arrRegistros!=null){
			if(store.getTotalCount()>0 &&  Ext.isEmpty(arrRegistros[0].data.loadMsg)){
				if(store.getTotalCount() > 1){
					store.insert(0,new Todas({ 
					clave: "", 
					descripcion: "Selecciona Banco de Fondeo", 
					loadMsg: ""})); 
					store.commitChanges(); 
					Ext.getCmp('idComboBanco').setValue('');
				}else{
					Ext.getCmp('idComboBanco').setValue('2');			
				}
				estatusActual = Ext.getCmp('idComboBanco').getValue();
				consultaDataGrid.load({
					params: {noBancoFondeo: estatusActual}
				});
			}
		}
	}
	
	
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');	
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
//-------------Stores-------------------------------------------------------------------

	var CatalogoBanco = new Ext.data.JsonStore
	({
		id: 'catBanco',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admnafparamdoctoaExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoBanco'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarCatalogo,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	  });  
	
	
	var Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);
	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15admnafparamdoctoaExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'EPO'},
			{name: 'IC_NOMBRE'},
			{name: 'CD_DESCRIPCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	

//--------------Componentes-------------------------------------------------------------

	var elementosForma = 
	[	
		{
			xtype: 'combo',
			fieldLabel: 'Banco de Fondeo',
			forceSelection: true,
			autoSelect: false,
			name:'claveProducto',
			id:'idComboBanco',
			displayField: 'descripcion',
			allowBlank: true,
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			autoLoad: false,
			typeAhead: true,
			minChars: 1,
			store: CatalogoBanco,
			
			listeners:{
				select: function(combo){
					if(estatusActual != Ext.getCmp('idComboBanco').getValue()){
						estatusActual = Ext.getCmp('idComboBanco').getValue();
						consultaDataGrid.load({
							params:{noBancoFondeo: combo.getValue() }
						});
					}
				}
			},
			width:170
		}
	]
	
	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: true,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
		style: 'margin:0 auto;',	   
		height: 70,
		width: 335
	});
	
	var grid= new Ext.grid.GridPanel({
		store: consultaDataGrid,
		autoLoad: false,
		sortable: true,
		id:'grid',
		title:'Documento por Producto',
		hidden: false,
		columns: [
			{
				header: '<center>Cadena Productiva</center>',
				dataIndex: 'EPO',
				width: 295,
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								return value;
				}
			},{
				header: '<center>Producto</center>',
				dataIndex: 'IC_NOMBRE',
				align: 'left',
				width: 295
			},{
				header: 'Clase de Documento',
				dataIndex: 'CD_DESCRIPCION',
				align: 'center',
				width: 295
			}
		],
		loadMask: true,	
		style: 'margin:0 auto;',
		frame: true,
		height: 350,
      width: 920,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCons',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '15admnafparamdoctoaExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'
							}),
							callback: procesarDescargaArchivos
						});
						
					}
				}
			]
		}
	});	
	
			
//---Contenedor Principal-----
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [fp,NE.util.getEspaciador(10),grid ]
	});

	CatalogoBanco.load()
	
});