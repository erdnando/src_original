
function cuentaCambio1(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);

	if(check.checked==true)  {
		reg.set('AUXPRODUCTO1','S');
		if( reg.get('CHKPRODUCTO1')=='N'){
			reg.set('CHKACTUAL', "|S");	
		}else{
			reg.set('CHKACTUAL', "");
		}

	}else  {
		reg.set('AUXPRODUCTO1','N');	
		if(reg.get('CHKPRODUCTO1')=='S'){
			reg.set('CHKACTUAL', "|N");
			
		}else{
			reg.set('CHKACTUAL', "");
		}
	}	
	store.commitChanges();	
}



function cuentaCambio4(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);

	if(check.checked==true)  {
		reg.set('AUXPRODUCTO4','S');

		if( reg.get('CHKPRODUCTO4')=='N'){
			reg.set('CHKACTUAL4', "|S");	
		}else{
			reg.set('CHKACTUAL4', "");
		}

	}else  {
		reg.set('AUXPRODUCTO4','N');
		
		if(reg.get('CHKPRODUCTO4')=='S'){
			reg.set('CHKACTUAL4', "|N");
		}else{
			reg.set('CHKACTUAL4', "");
		}
	}	
	store.commitChanges();	
}


Ext.onReady(function(){

	var cveproduc1="";
	var cveproduc4="";
	
	var Encabezado1="";
	var Encabezado4="";


//-------------Handlers--------------------------------------------------------------	
	
	var procesaResultado= function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			texto = "";
			texto=info.MENSAJE;
			Ext.MessageBox.alert('Mensaje',info.MENSAJE);
		}else{
			NE.util.mostrarConnError(response,opts);
		}
		consultaDataGrid.load()
	}
		
	
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');	
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
				Ext.getCmp('btnGrabar').enable();
			}else{
				Ext.getCmp('btnGrabar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarEncabezados = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			var grid = Ext.getCmp('grid');
			var cm = grid.getColumnModel();
			
			Encabezado1 = info.ENCABEZADO1;
			Encabezado4 = info.ENCABEZADO4;
					
			grid.getColumnModel().setColumnHeader(cm.findColumnIndex('PRODUCTO1'),Encabezado1);
			grid.getColumnModel().setColumnTooltip(cm.findColumnIndex('PRODUCTO1'),Encabezado1);
			grid.getColumnModel().setColumnHeader(cm.findColumnIndex('PRODUCTO4'),Encabezado4);
			grid.getColumnModel().setColumnTooltip(cm.findColumnIndex('PRODUCTO4'),Encabezado4);

			cveproduc1 = info.CLAVEPRODUCTO1;
			cveproduc4 = info.CLAVEPRODUCTO4;
  
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	
	var procesarGrabar = function(store, arrRegistros, opts){
			Ext.getCmp('btnGrabar').disable();
			var estatusAnterior = new Array();	
			var  gridConsulta = Ext.getCmp('grid');
			var store = gridConsulta.getStore();	
			var jsonData = store.data.items;
			var numRegistros=0;
			var errorValidacion = false;
				
			store.each(function(record){
				if(record.data['CHKACTUAL']!='' || record.data['CHKACTUAL4'] != '' )
					numRegistros++;
			});
		
			if (numRegistros == 0){
				errorValidacion = true;	
			}
		
			if (errorValidacion) {
				Ext.MessageBox.alert('Mensaje',"Debe de realizar cambios para poder grabar");
				Ext.getCmp('btnGrabar').enable();
				return;
			}

			var ant;
			var claveTas;
			var claveProd;
			var act;
			
			store.each(function(record) {	
				ant = record.data['CHKPRODUCTO1'];
				claveTas = record.data['CLAVETASA'];
				act = record.data['CHKACTUAL'];
			
				if(act != ''){
					estatusAnterior.push(ant+"|"+claveTas+"|"+cveproduc1+act);	
				}else{
					estatusAnterior.push("");
				}
			
				ant = record.data['CHKPRODUCTO4'];
				claveTas = record.data['CLAVETASA'];
				act = record.data['CHKACTUAL4'];
				
				if(act != ''){
					estatusAnterior.push(ant+"|"+claveTas+"|"+cveproduc4+act);	
				}else{
					estatusAnterior.push("");
				}
			});
		
			Ext.Ajax.request({
				url: '15tasasopxprodExt.data.jsp',
				params: {
					informacion: 'CapturaCambios',
					txt_cambio:estatusAnterior
				},
				callback: procesaResultado
			});	
	}	
	
	
//-------------Stores-----------------------------------------------------------	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15tasasopxprodExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'TIPOTASA'},
			{name: 'CHKPRODUCTO1'},
			{name: 'CHKPRODUCTO4'},
			{name: 'VERCHKPRODUCTO1'},
			{name: 'VERCHKPRODUCTO4'},
			{name: 'CLAVETASA'},
			{name: 'CHKACTUAL'},
			{name: 'CHKACTUAL4'},
			{name: 'AUXPRODUCTO1'},
			{name: 'AUXPRODUCTO4'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	

//--------------Componentes-----------------------------------------------------	
	var grid = new Ext.grid.GridPanel( {
		store: consultaDataGrid,
		style: 'margin:0 auto;',
		id: 'grid',
		columns: [{
				header: '<center>Tipo de Tasa</center>',
				tooltip: 'Tipo de Tasa',
				dataIndex: 'TIPOTASA',
				width: 220,
				align: 'left'				
			},{
				header:'Campo1',
				tooltip: Encabezado1,
				dataIndex : 'PRODUCTO1',
				width : 220,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					var ver;
					if(record.data['VERCHKPRODUCTO1'] =='N')
						ver="disabled"; 
					else
						ver="";	
					if(record.data['AUXPRODUCTO1'] == ''){
							if(record.data['CHKPRODUCTO1']=='S' ){
								return '<input  id="chkPRODUCTO1"'+ rowIndex +' value='+ value + ' type="checkbox" checked '+ver+'  onclick="cuentaCambio1(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkPRODUCTO1"'+ rowIndex +' value='+ value + ' type="checkbox" '+ver+' onclick="cuentaCambio1(this, '+rowIndex +','+colIndex+');" />';
							}
					
					}else{
						if(record.data['AUXPRODUCTO1'] == 'S'){
							return '<input  id="chkPRODUCTO1"'+ rowIndex +' value='+ value + ' type="checkbox" checked  '+ver+' onclick="cuentaCambio1(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkPRODUCTO1"'+ rowIndex +' value='+ value + ' type="checkbox"  '+ver+'  onclick="cuentaCambio1(this, '+rowIndex +','+colIndex+');" />';
						}
					}

				}
			}, {
				header:'Campo3',
				tooltip: Encabezado4,
				dataIndex : 'PRODUCTO4',
				width : 220,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					var ver; 
					if(record.data['VERCHKPRODUCTO4'] =='N')
						ver="disabled"; 
					else
						ver="";
					if(record.data['AUXPRODUCTO4'] == ''){
							if(record.data['CHKPRODUCTO4']=='S' ){
								return '<input  id="chkPRODUCTO4"'+ rowIndex +' value='+ value + ' type="checkbox" checked '+ver+' onclick="cuentaCambio4(this, '+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkPRODUCTO4"'+ rowIndex +' value='+ value + ' type="checkbox" '+ver+'  onclick="cuentaCambio4(this, '+rowIndex +','+colIndex+');" />';
							}
					
					}else{
						if(record.data['AUXPRODUCTO4'] == 'S'){
							return '<input  id="chkPRODUCTO4"'+ rowIndex +' value='+ value + ' type="checkbox" checked '+ver+' onclick="cuentaCambio4(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkPRODUCTO4"'+ rowIndex +' value='+ value + ' type="checkbox" '+ver+' onclick="cuentaCambio4(this, '+rowIndex +','+colIndex+');" />';
						}
					}
				}
			} ],
			loadMask: true,
			bbar: {
					autoScroll:true,
					id: 'barra',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnGrabar',
						text:		'Grabar',
						iconCls: 'icoGuardar ',
						width: 50,
						weight: 70,
						style: 'margin:0 auto;',
						handler: procesarGrabar
					},{	
						xtype:	'button',
						id:		'btnLimpiar',
						text:		'Limpiar',
						iconCls: 'icoLimpiar',
						width: 50,
						weight: 70,
						style: 'margin:0 auto;',
						handler: function (boton,evento){
							window.location = '15tasasopxprodExt.jsp'
						},
						align:'rigth'
					}]
				},			
			style: 'margin:0 auto;',
			height: 400,
			width: 680,
			frame: true
	});




	//Contenedor Principal
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [grid
		,NE.util.getEspaciador(10) ]
	});

	
	Ext.Ajax.request({
		url: '15tasasopxprodExt.data.jsp',
		params: {
			informacion: 'ConsultaEncabezados'
		},
		callback: procesarEncabezados
	});


});