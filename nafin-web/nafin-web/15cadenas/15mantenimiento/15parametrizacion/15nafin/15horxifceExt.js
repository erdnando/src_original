Ext.onReady(function() {

/**********CRITERIOS DE BUSQUEDA ***************/		

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: '<b>General</b>',			
				id: 'btnGeneral',					
				handler: function() {
					window.location = '15horxifceExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Por Intermediario',			
				id: 'btnIntermediario',					
				handler: function() {
					window.location = '15horxifcebExt.jsp';
				}
			}	
		]
	});
	
//-----------------------HANDLER'S------------------------------

		var procesarConsultaRegistros = function(total, registros){
		var btnGuardar = Ext.getCmp('btnGuardar');
		if (registros != null) {
			var grid  = Ext.getCmp('gridConsulta');

			var el = grid.getGridEl();
			if(registrosConsultadosData.getTotalCount() > 0) {
				btnGuardar.enable();
			} else {
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
				btnGuardar.disable();
			}
		}
	}
	
	//GUARDAR HORARIOS DEL GRID GENERAL
	var procesarGuardarHorarios =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			Ext.MessageBox.alert('Aviso', 'Se guardo con �xito los horarios generales para los IF', function(){
				registrosConsultadosData.load();
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//-----------------------STORE'S------------------------------

//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'15horxifceExt.data.jsp',
			baseParams: {	informacion:	'ConsultaHorarios'	},
			fields: [
				{ name: 'HORAINI'},
				{ name: 'HORAFIN' }
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				/*beforeload:	{
					fn: function(store, options){
						
						//console.log("options.params(beforeload)");
						//console.dir(options.params);
						
					}
				},*/
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null);
					}
				}
			}
	});
	
//------------------------FUNCIONES PARA VALIDAR HORAS----------------

	var formatoHora = function (hora) {
		var result = false, m;
		var re = /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/;
		if ((m = hora.match(re))) {
			result = (m[1].length == 2 ? "" : "0") + m[1] + ":" + m[2];
		}
		return result;
	};
	
	var comparaHorasDeServicio = function(grid, record, fieldName, indexC, indexR){
		var result = true;
		var hrIni = record.data['HORAINI'];
		var hrFin = record.data['HORAFIN'];
		
		if(  (Date.parse('01/01/2013 '+hrIni+':00') > Date.parse('01/01/2013 '+hrFin+':00'))	){
			result = false;
			Ext.MessageBox.alert('Aviso', 'El horario de cierre debe ser mayor o igual al de apertura', function(){
				grid.startEditing(indexR, indexC);
				registrosConsultadosData.load();
			});	
		}
		
		return result;
	};

//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

		var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Evento', colspan: 2, align: 'center'}
				]
			]
		});

//Elementos del grid de horarios
		var grid = new Ext.grid.EditorGridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			style: 		'margin: 0 auto'/*; padding-top:10px;'*/,
			hidden: 		false,
			margins:		'20 0 0 0',
			title:		'Horarios Generales',
			clicksToEdit:1,
			stripeRows: true,
			loadMask: 	true,
			height: 	130,
			width: 		406,
			plugins:		grupos,
			listeners: {
			cellclick :function(grid, rowIndex, colIndex, evento){

			},
			beforeedit : function(e){

			},
			afteredit : function(e){
					var record = e.record;
					var gridf = e.grid; 
					var store = gridf.getStore();
					var fieldName = gridf.getColumnModel().getDataIndex(e.column); // Se obtine nombre del Campo (Fields del Store)
					
					var horaGral = record.data[fieldName];
					
					var valor = formatoHora(horaGral);
					if(valor){
						record.data[fieldName]=valor;
						if ( !comparaHorasDeServicio(gridf, record, fieldName, e.column, e.row) ){
							record.data[fieldName]='';
							store.commitChanges();
							//gridf.startEditing(e.row, e.column);
						}else{
							store.commitChanges();
						}
					}else{
						record.data[fieldName]='';
						store.commitChanges();
					}
					
			}
			
		},
			columns: [
				{
					header: 		'Apertura',
					tooltip: 	'Horario de Apertura',
					dataIndex: 	'HORAINI',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
					},
					resizable: 	true,
					width: 		200,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Cierre',
					tooltip: 	'Horario de Cierre',
					dataIndex: 	'HORAFIN',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
					},
					resizable: 	true,
					width: 		200,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				}
			],bbar: {
			buttonAlign: 	'right',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Guardar',
					width:	100,
					id: 		'btnGuardar',
					iconCls:	'icoGuardar',
					handler: function(boton, evento) {
							registro = registrosConsultadosData.getAt(0);	//Se obtienen los valores del grid
							if(!(registro.get('HORAINI')=='' || registro.get('HORAFIN')=='' )){
							
								Ext.Ajax.request({
									url: '15horxifceExt.data.jsp',
									params: 
											{
												informacion	:	'GuardarHorarios',
												horaInicio	:	registro.get('HORAINI'),
												horaFinal	:	registro.get('HORAFIN')
											},				
									callback: procesarGuardarHorarios											  
								});
							}else{
								Ext.MessageBox.alert('Aviso', 'Es obligatorio establecer todos los horarios');
							}	
					}
				},'-'
			]
		}
	});
	
//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	930,
		//height: 	'auto',
		align: 'center',
		items: 	[
			NE.util.getEspaciador(10),
			fpBotones,
			NE.util.getEspaciador(20),
			grid
		]

	});
	
	registrosConsultadosData.load();

});