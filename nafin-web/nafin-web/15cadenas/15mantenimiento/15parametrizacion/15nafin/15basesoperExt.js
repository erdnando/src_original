Ext.onReady(function(){
	var radioSeleccionado = "";


//-----------STORE--------------------------------------------------------------
	var dataSeleccionar = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['1','Cadenas'],
			['2','Cr�dito Electr�nico']
		]
	 });

	
		
//----------------COMPONENTES----------------------------------------------------	
	
	var elementosForma2 = 
	[	
		{
			xtype: 'compositefield',
			id: 'cf',
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'opcionmodulo',
					id: 'radioCapturar',
					listeners:{
						check:function(radio){
										if (radio.checked){
											radioSeleccionado = "Capturar"
										}
								}
					}
				},	{
					xtype: 'displayfield',
					value: 'Capturar	',
					width: 100
				}, { 
					xtype: 'radio',
					name: 'opcionmodulo',
					id: 'radioConsultar',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											radioSeleccionado = "Consultar"
										}
									}
					}					
				},{
					xtype: 'displayfield',
					value: 'Consultar ',
					width: 100
				}		
			]
		},NE.util.getEspaciador(30)
		,{
			xtype: 'combo',
			fieldLabel: 'Seleccionar',
			forceSelection: true,
			autoSelect: true,
			name:'claveSeleccionar',
			id:'idComboSeleccionar',
			displayField: 'descripcion',
			allowBlank: true,
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			autoLoad: false,
			typeAhead: true,
			minChars: 1,
			store: dataSeleccionar,  
			value: '1',
			anchor: '89%',
			width:120
		}
	]	
	
	



	var fp2 = new Ext.form.FormPanel({
		id:'formaDetalle2',
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma2,
		buttons:[{
						xtype:	'button',
						id:		'btnAceptar',
						text:		'Aceptar',
						iconCls: 'icoAceptar',
						width: 80,
						weight: 70,
						handler: function(boton, evento) {
							if(radioSeleccionado == ''){
								Ext.MessageBox.alert("Alerta","Debe primero seleccionar una opci�n Capturar o Consultar ");
							}else{
								var cveSeleccionado = Ext.getCmp('idComboSeleccionar').getValue();
								if(radioSeleccionado == 'Consultar' && cveSeleccionado == '1'){
									window.location = '15basesoperc2Ext.jsp';
								}else if(radioSeleccionado == 'Capturar' && cveSeleccionado == '1'){
									window.location = '15basesopercExt.jsp';
								}else if(radioSeleccionado == 'Consultar' && cveSeleccionado == '2'){
									window.location = '15basesoperce2Ext.jsp';
								}else if(radioSeleccionado == 'Capturar' && cveSeleccionado == '2'){
									window.location = '15basesoperceExt.jsp';
								}
							}
						},
						style: 'margin:0 auto;',
						align:'rigth'
					}],
		style: 'margin:0 auto;',	   
		height: 160,
		width: 370
	});
	

//------ Componente Principal ------------------------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [NE.util.getEspaciador(10),fp2 ]
	});


});