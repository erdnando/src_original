Ext.onReady(function(){

//-------------Handlers-------------

var procesarConsultaData = function(store,arrRegistros,opts){
		//accionBotones();
		fp.el.unmask();
			grid.setVisible(true);
			Ext.getCmp('nota').setVisible(true);
			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
			
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					//accionBotonesDisabled();
				}
			}
		}
	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('btnGenerarArchivo');
			boton.setIconClass('');
			boton.enable();	
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//-------------Stores----------------
var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15adminNafinParam02ExtConsulta.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
var catalogoEstatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['','Ambas'],
			['S','Autorizada'],
			['N','No Autorizada']
		 ]
	});
var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '15adminNafinParam02ExtConsulta.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'PYME'},
				{name: 'PYME_RFC'},
				{name: 'NUMNAF_ELEC'},
				{name: 'INTERMEDIARIO'},
				{name: 'EPO'},
				{name: 'CUENTA_ANTERIOR'},
				{name: 'BANCO_ANTERIOR'},
				{name: 'CUENTA_NUEVA'},
				{name: 'BANCO_NUEVO'},
				{name: 'IC_USUARIO_CAMB_CUEN'},
				{name: 'NOM_USUARIO_CAMB_CUEN'},
				{name: 'FECHA_CAMBIO'},
				{name: 'CSAUTORIZAIF'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
						procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
//--------------Componentes------------------
var elementosForma = [
	{
		xtype: 'combo',
		fieldLabel: 'EPO',
		emptyText: 'Seleccionar una EPO',
		displayField: 'descripcion',
		valueField: 'clave',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		store: catalogoEpo,
		tpl: NE.util.templateMensajeCargaCombo,
		name:'HicEpo',
		id: 'icEpo',
		mode: 'local',
		hiddenName: 'HicEpo',
		forceSelection: true
	},
	{
		xtype: 'combo',
		name: 'Hestatus',
		id: 'estatus',
		fieldLabel: 'Estatus',
		emptyText: '',
		value:'',
		mode: 'local',
		displayField: 'descripcion',
		valueField: 'clave',
		hiddenName : 'Hestatus',
		forceSelection : true,
		triggerAction : 'all',
		lazyRender:true,
		typeAhead: true,
		minChars : 1,
		store: catalogoEstatus
	},{
						xtype: 'compositefield',
						fieldLabel: 'Fecha de Cambio',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'datefield',
								name: 'fechaOper1',
								id: 'fechaOper1',
								minValue: '01/01/1901',
								allowBlank: true,
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoFinFecha: 'fechaOper2',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							},
							{
								xtype: 'displayfield',
								value: 'al',
								width: 24
							},
							{
								xtype: 'datefield',
								name: 'fechaOper2',
								id: 'fechaOper2',
								minValue: '01/01/1901',
								allowBlank: true,
								startDay: 1,
								width: 100,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoInicioFecha: 'fechaOper1',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
							}
						]
					}
	
]
var elementosCambioPanel=[
	{
							xtype:'radiogroup',
							id:	'radioGp',
							columns: 4,
							hidden: false,
							items:[
								{boxLabel: 'Individual',id:'individual', name: 'stat',  checked: false,
									listeners:{
										check:	function(radio){
														if (radio.checked){
																window.location.href='15adminNafinParam02Ext.jsp';

														}
													}
									}
								},
								{boxLabel: 'Masiva',id:'masiva', name: 'stat',checked: false,
									listeners:{
										check:	function(radio){
															window.location.href='15adminNafinParam02ExtMas.jsp';
													}
									}
								},
								{boxLabel: 'Consultar Cambios Cta',id:'consulta', name: 'stat',checked: true,
									listeners:{
										check:	function(radio){
														
													}
									}
								},
								{boxLabel: 'Cambio de Cuenta Masiva',id:'cambioM', name: 'stat', checked: false,
									listeners:{
										check:	function(radio){
														window.location.href='15adminNafinParam04Ext.jsp';
													}
									}
								}
							]
						}
]


var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				title: '<center>Criterios de B�squeda</center>',
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						
						header:'<center>Nombre o Raz�n Social</center>',
						tooltip: 'Nombre o Raz�n Social',
						sortable: true,
						dataIndex: 'PYME',
						width: 130,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						
						header: 'RFC',
						tooltip: 'RFC',
						sortable: true,
						dataIndex: 'PYME_RFC',
						width: 150,
						align: 'center'
						},
						{
						header: 'N�mero de Nafin Electr�nico',
						tooltip: 'N�mero de Nafin Electr�nico',
						sortable: true,
						dataIndex: 'NUMNAF_ELEC',
						width: 150,
						align: 'center'
						},
						{
						header: '<center>Intermediario</center>',
						tooltip: 'Intermediario',
						sortable: true,
						dataIndex: 'INTERMEDIARIO',
						width: 150,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: '<center>EPO</center>',
						tooltip: 'EPO',
						sortable: true,
						dataIndex: 'EPO',
						width: 130,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						
						},
						{
						align:'center',
						header: 'Cuenta Anterior',
						tooltip: 'Cuenta Anterior',
						sortable: true,
						dataIndex: 'CUENTA_ANTERIOR',
						width: 130
						},
						{
						
						header: 'Banco',
						tooltip: 'Banco',
						sortable: true,
						dataIndex: 'BANCO_ANTERIOR',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Cuenta Nueva',
						tooltip: 'Cuenta Nueva',
						sortable: true,
						dataIndex: 'CUENTA_NUEVA',
						width: 130,
						align: 'center'
						},
						{
						header: 'Banco',
						tooltip: 'Banco',
						sortable: true,
						dataIndex: 'BANCO_NUEVO',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Usuario que Autoriz�<br/>ultima Actualizaci�n',
						tooltip: 'Usuario que Autoriz�<br/>ultima Actualizaci�n',
						sortable: true,
						dataIndex: 'IC_USUARIO_CAMB_CUEN',
						width: 130,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								
								return value+'<br/>'+record.get('NOM_USUARIO_CAMB_CUEN');
							}
						
						},
						{
						align:'center',
						header: 'Fecha de Cambio Cta',
						tooltip: 'Fecha de Cambio Cta',
						sortable: true,
						dataIndex: 'FECHA_CAMBIO',
						width: 130
						},
						{
						header: 'Autorizaci�n',
						tooltip: 'Autorizaci�n',
						sortable: true,
						dataIndex: 'CSAUTORIZAIF',
						width: 130,
						align: 'center'
						}
						
						
						
				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					autoScroll:true,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					
					items: ['->','-',
							
								{
									xtype: 'button',
									
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '15adminNafinParam02ExtConsulta.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV'}),
											callback: procesarArchivoSuccess
										});
									}
								}
							]
				}
		});

//panel

var panelCambioPanel = new Ext.Panel({
	hidden: false,
		height: 'auto',
		width: 840,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
			id: 'forma2',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosCambioPanel
	
});
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		collapsible: false,
		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Consultar',
			  name: 'btnBuscar',
			  //iconCls: 'icoContinuar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{
						if(!fp.getForm().isValid()){
						return;
						}
						
						if(NE.util.validatorDateRange(Ext.getCmp('fechaOper1'),Ext.getCmp('fechaOper2'),30,true)){
							fp.el.mask('Procesando...', 'x-mask-loading');
							consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
											operacion: 'Generar', //Generar datos para la consulta
											start: 0,
											limit: 15})
											});
								
						}
					}
					
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
		panelCambioPanel,NE.util.getEspaciador(10),fp,NE.util.getEspaciador(15),{width:600,style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',id:'nota',html: 'CONSULTA DE CAMBIOS DE CUENTA BANCARIA PYME SOLICITADAS POR EL CLIENTE',hidden:true	},NE.util.getEspaciador(15),grid
	  ]
  });
  catalogoEpo.load();
});