/**
*  FODEA 04 : Parametrizaci�n - Otros Parametros
*  garellano
*/

Ext.onReady(function(){
   /* Namespace */
   Ext.namespace('NAFIN.otrosparametros');
   
   /* Objeto General */
   function ObjGeneral(){
      this.urlRequestData 	= '15admnafotrosparamext.data.jsp';
      this.urlSendRedirect = '15admnafotrosparamext.jsp';
      this.estado				= 'inicializaPantalla';
      this.cvePerf			=	'';
   }
   
   /* Controlamos Pantalla */
   ObjGeneral.prototype.controller = function(jsonData){
   	if(jsonData){ this.estado = jsonData.ESTADO; }   	

   	switch( this.estado ){
   		case 'inicializaPantalla'  : this.inicializaPantalla(); 				break;
   		case 'creaFormulario'		: this.creaFormulario(jsonData); 		break;
   	}
   }
   
   /* ESTADO 1: Inicializar pantalla */
   ObjGeneral.prototype.inicializaPantalla = function(){
      Ext.Ajax.request({
         method: 'POST',
         url: this.urlRequestData,
         params: { 
            informacion: this.estado
         },
         /* Capturo el response */
         success: function(response, request){ 
            var jsonData = Ext.decode(response.responseText);
            this.estado = jsonData.ESTADO;
            this.cvePerf = jsonData.cvePerf;
            NAFIN.otrosparametros.ObjGeneral.controller(jsonData);   
         },
         /* Manejo el error */
         failure: function(response, request){ 
            var errMessage = '<b>Error en la petici�n</b> ' + request.url + '<br> '  
									+ ' <b>Estatus</b> ' + response.status + ' - ' + response.statusText + '<br>'  
									+ ' ' + response.responseText + '<br>';
				Ext.Msg.show({
					title    :  response.status + ' - ' + response.statusText,
					msg      :  errMessage,
					icon     :  Ext.Msg.ERROR,
					buttons  :  Ext.Msg.OK
				})
         }
      });
   }
   /* ESTADO 2 */
   ObjGeneral.prototype.creaFormulario = function(jsonData){
   	var frm = new Object();
   	frm = forma;  	
   	frm.setTitle('Otros Par�metros');
   	frm.show();
   	if(cvePerf=='8'){
			var ElementosFormaNoEditables = [
				{readOnly:true, fieldLabel: 'D�as Min. Para Descto', emptyText: '0', id:'strDiasMin'},
				{readOnly:true, fieldLabel: 'D�as Max. Para Descto', emptyText: '0', id:'strDiasMax'},
				{readOnly:true, fieldLabel: 'Porcentaje de Descuento NAFIN M.N', emptyText: '0', id:'strPorcientoActual'},
				{readOnly:true, fieldLabel: 'Porcentaje de Descuento NAFIN Dol', emptyText: '0', id:'strPorcientoActualDL'},
				{readOnly:true, fieldLabel: 'D�as Vig. Edos. Cuenta', emptyText: '0', id:'strDiasEdoCuenta'},
				{readOnly:true, fieldLabel: 'D�as Vig.Vencimientos', emptyText: '0', id:'strDiasVencimiento'},
				{readOnly:true, fieldLabel: 'D�as Vig. Operados', emptyText: '0', id:'strDiasOperados'},
				{readOnly:true, fieldLabel: 'Plazo M�ximo de Factoraje M.N', emptyText: '0', id:'strPlazoMaxFactorajeMN'},
				{readOnly:true, fieldLabel: 'Plazo M�ximo de Factoraje Dol', emptyText: '0', id:'strPlazoMaxFactorajeDL'},
				{readOnly:true, fieldLabel: 'Base de Operaci�n Gen�rica', emptyText: '0', id:'strBaseOperGenerica'},
				{readOnly:true, fieldLabel: 'E-mail Contingencias', width: 300,  emptyText: 'email@domainname.com.mx', xtype: 'textfield', id:'strEmailContingencia'},
				{xtype:'panel',labelWidth:10, items:[{xtype:'compositefield', combineErrors: false, items: [{xtype:'button', id:'ayudaH',iconCls:'icoAyuda',handler:muestraAyuda},{xtype:'displayfield',value:'Hora para consultar Reportes Especiales'},{xtype: 'textfield', readOnly:true,maxLength: 5, width: 40, fieldLabel: '', emptyText: '00:00', id:'strHoraReportesEspeciales'},{xtype:'displayfield', value: 'Formato 24 hrs.'}]}]},
				{xtype:'panel',style:'margin-top:5px',labelWidth:10, items:[{xtype:'compositefield', combineErrors: false, items: [{xtype:'button', id:'ayudaE',iconCls:'icoAyuda',handler:muestraAyuda},{xtype:'displayfield',value:'E-mail Notificaciones Nafin'},{xtype: 'textarea', readOnly:true, autoScroll: true, width:360, height: 150, anchor: '90%', id:'strEmailNotificacionesNafin', enableKeyEvents: true, listeners:{change:function(){ doUpdateCharLength();  },keypress:function(){ doUpdateCharLength();  }}}]}]},
				{fieldLabel: ' ', xtype: 'displayfield', value: '400', id: 'charCounter', labelSeparator:' ', style: 'text-align:right; margin-right: 60px' }
			];
   		Ext.getCmp('btnAceptar').hide();
   		Ext.getCmp('btnLimpiar').hide();
   		frm.add(ElementosFormaNoEditables);
   	}else{
			var ElementosFormaEditables = [
				{fieldLabel: 'D�as Min. Para Descto', maxLength: 3, xtype: 'numberfield', emptyText: '', id:'strDiasMin'},
				{fieldLabel: 'D�as Max. Para Descto', maxLength: 3, xtype: 'numberfield', emptyText: '', id:'strDiasMax'},
				{fieldLabel: 'Porcentaje de Descuento NAFIN M.N', maxLength: 3, xtype: 'numberfield', emptyText: '', id:'strPorcientoActual'},
				{fieldLabel: 'Porcentaje de Descuento NAFIN Dol', maxLength: 3, xtype: 'numberfield', emptyText: '', id:'strPorcientoActualDL'},
				{fieldLabel: 'D�as Vig. Edos. Cuenta', maxLength: 3, xtype: 'numberfield', emptyText: '', id:'strDiasEdoCuenta'},
				{fieldLabel: 'D�as Vig.Vencimientos', maxLength: 3, xtype: 'numberfield', emptyText: '', id:'strDiasVencimiento'},
				{fieldLabel: 'D�as Vig. Operados', maxLength: 3, xtype: 'numberfield', emptyText: '', id:'strDiasOperados'},
				{fieldLabel: 'Plazo M�ximo de Factoraje M.N', maxLength: 3, xtype: 'numberfield',  emptyText: '', id:'strPlazoMaxFactorajeMN'},
				{fieldLabel: 'Plazo M�ximo de Factoraje Dol', maxLength: 3, xtype: 'numberfield', emptyText: '', id:'strPlazoMaxFactorajeDL'},
				{fieldLabel: 'Base de Operaci�n Gen�rica',  maxLength: 6, xtype: 'numberfield', emptyText: '', id:'strBaseOperGenerica'},
				{fieldLabel: 'E-mail Contingencias', width: 300, emptyText: 'email@domainname.com.mx', xtype: 'textfield', id:'strEmailContingencia'},
            {xtype:'panel',labelWidth:10, items:[{xtype:'compositefield', combineErrors: false, items: [{xtype:'button', id:'ayudaH',iconCls:'icoAyuda',handler:muestraAyuda},{xtype:'displayfield',value:'Hora para consultar Reportes Especiales'},{xtype: 'textfield', maxLength: 5, width: 40, fieldLabel: '', emptyText: '00:00', id:'strHoraReportesEspeciales'},{xtype:'displayfield', value: 'Formato 24 hrs.'}]}]},
				{xtype:'panel',style:'margin-top:5px',labelWidth:10, items:[{xtype:'compositefield', combineErrors: false, items: [{xtype:'button', id:'ayudaE',iconCls:'icoAyuda',handler:muestraAyuda},{xtype:'displayfield',value:'E-mail Notificaciones Nafin'},{xtype: 'textarea', autoScroll: true, width:360, height: 150, anchor: '90%', id:'strEmailNotificacionesNafin', enableKeyEvents: true, listeners:{change:function(){ doUpdateCharLength();  },keypress:function(){ doUpdateCharLength();  }}}]}]},
				{fieldLabel: ' ', xtype: 'displayfield', value: '400', id: 'charCounter', labelSeparator:' ', style: 'text-align:right; margin-right: 60px' },
				{xtype:'panel', width:550, layout:'form', labelWidth:380,
				items:[{
					xtype: 'radiogroup',
					id:'optOperTel',
					fieldLabel: 'Solicitar clave de Cesi�n de Derechos en la Operaci�n Telef�nica',
					items: [
						 {boxLabel: 'Si', name: 'optOperTel', inputValue: 'S'},
						 {boxLabel: 'No', name: 'optOperTel', inputValue: 'N', checked: true}
					]
					}]
				}
			]; 
   		Ext.getCmp('btnAceptar').show();
   		Ext.getCmp('btnLimpiar').show();
   		frm.add(ElementosFormaEditables);   		
   	}
   	frm.doLayout();
   	
   	pnl.add(frm);
   	pnl.doLayout();
   	
   	/* ESTADO 2 : Llenar el formulario */
   	NAFIN.otrosparametros.ObjGeneral.llenaFormulario(jsonData);
   }
   
   /* ESTADO 2: Setear componentes del formulario */
   ObjGeneral.prototype.llenaFormulario = function(jsonData){
   	var cmp = Ext.getCmp;   	
   	cmp('strDiasMin').setValue(jsonData.strDiasMin);
   	cmp('strDiasMax').setValue(jsonData.strDiasMax);
   	cmp('strPorcientoActual').setValue(jsonData.strPorcientoActual);
   	cmp('strDiasEdoCuenta').setValue(jsonData.strDiasEdoCuenta);
   	cmp('strDiasVencimiento').setValue(jsonData.strDiasVencimiento);
   	cmp('strDiasOperados').setValue(jsonData.strDiasOperados);
   	cmp('strPlazoMaxFactorajeMN').setValue(jsonData.strPlazoMaxFactorajeMN);
   	cmp('strPlazoMaxFactorajeDL').setValue(jsonData.strPlazoMaxFactorajeDL);
   	cmp('strBaseOperGenerica').setValue(jsonData.strBaseOperGenerica);
   	cmp('strEmailContingencia').setValue(jsonData.strEmailContingencia);
   	cmp('strPorcientoActualDL').setValue(jsonData.strPorcientoActualDL);
   	cmp('strHoraReportesEspeciales').setValue(jsonData.strHoraReportesEspeciales);
   	cmp('strEmailNotificacionesNafin').setValue(jsonData.strEmailNotificacionesNafin);
		cmp('optOperTel').setValue(jsonData.optOperTel);
   	this.estado = 'enviarFormulario';
   	doUpdateCharLength();
   }
   
   /* ESTADO 3 */
   function enviarFormulario(){
   	
		var strDiasMin = Ext.getCmp("strDiasMin");
		var strDiasMax = Ext.getCmp("strDiasMax");
		if (parseInt(strDiasMin.getValue(),10) > parseInt (strDiasMax.getValue(),10)){
			alert("Los d�as m�nimos deben ser menor a d�as m�ximos");
			return;
		}
		
		if(validaCorreos()==true) {
			forma.getForm().submit({ 
					url: NAFIN.otrosparametros.ObjGeneral.urlRequestData,	
					params:{ informacion: NAFIN.otrosparametros.ObjGeneral.estado }, 
					success: function(request, response){ 
						
						Ext.Msg.show({
							title:   'Mensaje',
							msg:     response.result.MENSAJE,
							buttons: Ext.Msg.OK,
							icon:   Ext.Msg.INFO
						});
						
						this.estado = response.result.ESTADO;						
					}
			});
		}
   }
   
   function limpiarPantalla(){
   	
		Ext.getCmp('strDiasMin').setValue('');
   	Ext.getCmp('strDiasMax').setValue('');
   	Ext.getCmp('strPorcientoActual').setValue('');
   	Ext.getCmp('strDiasEdoCuenta').setValue('');
   	Ext.getCmp('strDiasVencimiento').setValue('');
   	Ext.getCmp('strDiasOperados').setValue('');
   	Ext.getCmp('strPlazoMaxFactorajeMN').setValue('');
   	Ext.getCmp('strPlazoMaxFactorajeDL').setValue('');
   	Ext.getCmp('strBaseOperGenerica').setValue('');
   	Ext.getCmp('strEmailContingencia').setValue('');
   	Ext.getCmp('strPorcientoActualDL').setValue('');
   	Ext.getCmp('strHoraReportesEspeciales').setValue('');
   	Ext.getCmp('strEmailNotificacionesNafin').setValue('');
	
   }
   
   /* Funciones Generales */
   function muestraAyuda(btn){
      switch(btn.id){
         case 'ayudaH' : winAyudaHora(); break;
         case 'ayudaE' : winAyudaEmail(); break;
      }
   }
   
   function winAyudaHora(){
      new Ext.Window({
         modal			: false,
         resizable	: false,
         frame       : false,
         layout		: 'form',
         x				: 100,
         width			: 250,
         id				: 'winAyudaHora',
         items			: [{
            xtype: 'panel',
            items:[{
               xtype: 'fieldset',
               title: 'Perfil: ADMIN NAFIN',
               style:'margin:5px',
               items: [{
                  xtype: 'label',
                  html: 'REPORTES : <br/><br />Resumen de Pymes <br/>Reporte Anal�tico de Documentos <br/>Resumen de Operaci�n EPO '
               }]
            },{
               xtype: 'fieldset',
               title: 'Perfil: ADMIN EPO',
               style:'margin:5px',
               items: [{
                  xtype: 'label',
                  html: 'REPORTES : <br/><br/>Resumen de Pymes <br/>Reporte Anal�tico de Documentos <br/>Resumen de Operaci�n EPO '
               }]
            },{
               xtype: 'fieldset',
               title: 'Perfil: PROMO NAFIN',
               style:'margin:5px',
               items: [{
                  xtype: 'label',
                  html: 'REPORTES : <br/><br />Resumen de Pymes <br/>Reporte Anal�tico de Documentos <br/>Resumen de Operaci�n EPO '
               }]
            },{
               xtype: 'fieldset',
               title: 'Perfil: ADMIN IF',
               style:'margin:5px',
               items: [{
                  xtype: 'label',
                  html: 'REPORTES : <br/><br />Avisos de Notificaci�n '
               }]
            }]
         }],
         title			: 'Ayuda'
      }).show();
   }
   
   function winAyudaEmail(){
      new Ext.Window({
         modal			: false,
         resizable	: false,
         frame       : false,
         layout		: 'form',
         x				: 100,
         width			: 600,
         id				: 'winAyudaHora',
         items			: [{
            xtype: 'panel',
            items:[{
               xtype: 'fieldset',
               title: '',
               style:'margin:5px',
               items: [{
                  xtype: 'label',
                  html: 'Puede ingresar una o varias direcciones de correo electr�nico, las cuales tienen que <br/>estar separadas debidamente por comas, por ejemplo: usuario@dominio.com, <br/>usuario2@dominio.com, usuario3@dominio.com. <br/>S�lo se permitir� capturar hasta 4000 caracteres.'
               }]
            }]
         }],
         title			: 'Ayuda'
      }).show();
   }
   
   function doUpdateCharLength(){
   	var maxlength = 4000;
		if(Ext.getCmp("strEmailNotificacionesNafin").getValue().length > maxlength) {
			Ext.getCmp("strEmailNotificacionesNafin").setValue(Ext.getCmp("strEmailNotificacionesNafin").getValue().substr(0, maxlength));
		}
		var restante = maxlength - Ext.getCmp("strEmailNotificacionesNafin").getValue().length;
		Ext.getCmp("charCounter").setValue(restante);	
   }
   
   function validaCorreos(){
   	// <%-- Validar los correos correspondientes al campo: E-mail Notificacion Nafin --%>
		var correos 				= Ext.getCmp("strEmailNotificacionesNafin").getValue();
		correos = correos.replace(/[\r\n]+/g,"");
		var listaCorreos			= correos == ""?new Array():correos.split(",");
		var ctaCorreosInvalidos	= 0;
		var correosInvalidos		= "";
		var valida = true;
		if(listaCorreos.length == 0){
			Ext.Msg.alert("Mensaje","Ingresar campo E-mail Notificaciones Nafin.");
			 valida = false;
		}else{ 
			// <%-- Si hay correos especificados, validarlos --%>
			for(var indice=0;indice<listaCorreos.length;indice++){
				if(validaCorreo(listaCorreos[indice]) == false){
					if(ctaCorreosInvalidos == 0 ){
						correosInvalidos = "Los siguientes correos son invalidos:<br>";
						correosInvalidos += "<span style='font-weight:bold; color:#0B4C5F'>";
					}		
					if(ctaCorreosInvalidos>0){
						correosInvalidos += "<br>";
					}
					if(ctaCorreosInvalidos > 5){
						correosInvalidos += "...";
						break;
					}
					correosInvalidos += "&nbsp;&nbsp;&nbsp;" + listaCorreos[indice];
					ctaCorreosInvalidos++;
				}
			}
		}
		correosInvalidos+="</span> <br> ";
		if(ctaCorreosInvalidos>0){
			Ext.Msg.show({
					title:	'Mensaje',
					msg:		'Favor de ingresar direcciones de correo electr�nico v�lidas.<br>Ejemplo: usuario@dominio<br><br>'+correosInvalidos,
					buttons:	Ext.Msg.OK,
					icon:		Ext.Msg.INFO
			});
			 valida = false;
		}	
		return valida;
   }
//-------------------------- COMPONENTES -------------------------------------//
	var forma = new Ext.form.FormPanel({
			title: ' ',
			hidden: true,
			width: 600,
			style: 'margin: 0 auto',
			frame: true,
			waitMsgTarget: true,
			labelWidth: 230,
			defaultType: 'numberfield',
			defaults: { allowBlank: false},
			labelAlign: 'right',
			monitorValid: true,
			buttons: [
				{text: 'Aceptar', iconCls: 'icoAceptar', id:'btnAceptar', formBind: true, handler: enviarFormulario},
				{text: 'Limpiar', iconCls: 'icoLimpiar', id:'btnLimpiar', handler: limpiarPantalla}
			]
	});   
	
	var pnl = new Ext.Container({
		id:      'contenedoPrincipal',
		applyTo: 'areaContenido',
		width:   940,
		heiht:   'auto',
		items: [NE.util.getEspaciador(10)]
	});
   
//---------------------- INICIALIZA PANTALLA ---------------------------------//   

   NAFIN.otrosparametros.ObjGeneral = new ObjGeneral();
   NAFIN.otrosparametros.ObjGeneral.controller();
   
});