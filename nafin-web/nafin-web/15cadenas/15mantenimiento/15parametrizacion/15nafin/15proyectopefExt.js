Ext.onReady(function(){
	var totalepos= 0;
	

	var ic_epo = [];
	var ic_secretaria = [];
	var ic_organismo = [];
	var obra_publica = [];
	
	var cancelar =  function() { 
		ic_epo = [];
		ic_secretaria = [];
		ic_organismo = [];
		obra_publica = [];	
	}

	function transmiteGuardar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.getBody().unmask();	
			if(info.mensaje =='EXITO') {
			
				Ext.Msg.alert('Mensaje', 'Datos actualizados',
				function(btn, text){
					fp.el.mask('Enviando...', 'x-mask-loading');		
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{					
							informacion: 'Consultar'						
						})
					});
				});
			}

		} else {
			NE.util.mostrarConnError(response,opts);						
		}
	}
	
	var  procesarGuardar  = function(){
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();				
		cancelar();
		var anio;
		
		store.each(function(record) {
			ic_epo.push(record.data['IC_EPO']);
			ic_secretaria.push(record.data['SECRETARIA']);
			ic_organismo.push(record.data['ORGANISMO']);
			obra_publica.push(record.data['PORCETAJE_COBRO']);
			totalepos++;
			anio = record.data['ANIO'];
		});
		
		Ext.getBody().mask("Guardando Datos ..", 'x-mask-loading');
		Ext.getCmp('btnGuardar').disable();	
		Ext.Ajax.request({
			url: '15proyectopef.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Guardar',
				ic_epo:ic_epo,
				ic_secretaria:ic_secretaria,
				ic_organismo:ic_organismo,
				obra_publica:obra_publica,
				totalepos:totalepos,
				anio:anio
			}),
			callback: transmiteGuardar
		});		
	}
	
	
	var mostrarHistorico = function(grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var  ic_epo		= registro.get('IC_EPO'); 
		var  ig_anio		= registro.get('ANIO'); 
		var ventana = Ext.getCmp('VerHistorico');
		var fp = Ext.getCmp('forma');
		
		if(ventana){
			ventana.show();
		}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				width: 320,
				height: 200,
				id: 'VerHistorico',
				closeAction: 'hide',
				items: [gridHistorico],
				title: 'PORCENTAJE DE COBRO PARA OBRA PUBLICA'
			}).show();
		}
		
		fp.el.mask('Consultando Histotico...', 'x-mask-loading');			
		consultaHistorico.load({
			params: Ext.apply(fp.getForm().getValues(),{					
				informacion: 'ConsultarHistorico',
				ic_epo:ic_epo,
				ig_anio:ig_anio				
			})
		});
	}
	
	
 var procesarConsultaHistoricoData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridHistorico = Ext.getCmp('gridHistorico');	
		var el = gridHistorico.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridHistorico.isVisible()) {
				gridHistorico.show();
			}					
			//edito el titulo de la columna
			var cm = gridHistorico.getColumnModel();			
			var jsonData = store.reader.jsonData;
			
			if(store.getTotalCount() > 0) {																	
				el.unmask();					
			} else {					
				el.mask('No registra Historico', 'x-mask');				
			}
		}
	}
	
	var consultaHistorico   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15proyectopef.data.jsp',
		baseParams: {
			informacion: 'ConsultarHistorico'
		},		
		fields: [	
		{	name: 'ANIO'},		
		{	name: 'PORCENTAJE'}		
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaHistoricoData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaHistoricoData(null, null, null);					
				}
			}
		}		
	});
	
	var gridHistorico = new Ext.grid.GridPanel 	({
		title:'',
		store: consultaHistorico,
		id: 'gridHistorico',
		margins: '20 0 0 0',
		columns: [
			{
				header: 'A�o',
				tooltip: 'A�o',
				dataIndex: 'ANIO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Porcentaje de Obra Publica',
				tooltip: 'Porcentaje de Obra Publica',
				dataIndex: 'PORCENTAJE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',	
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			}
		],
		stripeRows: true,
		loadMask: true,
		width: 310,
		height: 160,
		frame: true,
		displayInfo: true,		
		clicksToEdit: 1
	});
		
	
	
	//****************+catalogo Organismo**********************

	var catOrganismo = new Ext.data.JsonStore({
		id: 'catOrganismo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopef.data.jsp',
		baseParams: {
			informacion: 'catOrganismo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var comboOrganismoAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catOrganismo
	});
	
	Ext.util.Format.comboRenderer = function(comboOrganismoAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var record = comboOrganismoAsignar.findRecord(comboOrganismoAsignar.valueField, value);
			return record ? record.get(comboOrganismoAsignar.displayField) : comboOrganismoAsignar.valueNotFoundText;		
		}	
	}
	
	
	//****************catalogo EPO**********************
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopef.data.jsp',
		baseParams: {
			informacion: 'CatalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	// ************** Catalogo  Secretaria *****************+
	
	var catSecretaria = new Ext.data.JsonStore({
		id: 'catSecretaria',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopef.data.jsp',
		baseParams: {
			informacion: 'catSecretaria'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var comboSecretariaAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catSecretaria
	});
	
	Ext.util.Format.comboRenderer = function(comboSecretariaAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var record = comboSecretariaAsignar.findRecord(comboSecretariaAsignar.valueField, value);
			return record ? record.get(comboSecretariaAsignar.displayField) : comboSecretariaAsignar.valueNotFoundText;		
		}	
	}
	
	
	//**************Consulta*********************+
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;
			
			if(store.getTotalCount() > 0) {				
					Ext.getCmp('btnSalir').enable();	
					Ext.getCmp('btnGuardar').enable();													
				el.unmask();					
			} else {	
				Ext.getCmp('btnSalir').disable();	
				Ext.getCmp('btnGuardar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15proyectopef.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
		{	name: 'IC_EPO'},
		{	name: 'NOMBRE_EPO'},
		{	name: 'SECRETARIA'},
		{	name: 'ORGANISMO'},
		{	name: 'PORCETAJE_COBRO'},
		{  name: 'ANIO' }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData, 
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		 
		columns: [	
			{
				header: 'No. EPO',
				tooltip: 'No. EPO',
				dataIndex: 'IC_EPO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'EPO',
				tooltip: ' EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 250,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Secretar�a o Cabeza de sector',
				tooltip: 'Secretar�a o Cabeza de sector',
				dataIndex: 'SECRETARIA',
				sortable: true,
				resizable: true,
				width: 250,				
				align: 'left',
				editor: comboSecretariaAsignar,
				renderer: Ext.util.Format.comboRenderer(comboSecretariaAsignar)
			},	
			{
				header: 'Tipo de Organismo',
				tooltip: 'Tipo de Organismo',
				dataIndex: 'ORGANISMO',
				sortable: true,
				resizable: true,
				width: 250,				
				align: 'left',
				editor: comboOrganismoAsignar,
				renderer: Ext.util.Format.comboRenderer(comboOrganismoAsignar)
			},	
			{
				header : '% de Cobro por Obra P�blica', 
				tooltip: '% de Cobro por Obra P�blica',
				dataIndex : 'PORCETAJE_COBRO', 
				fixed:true,
				sortable : false,	
				width : 150,
				align: 'left',
				hiddeable: false,	
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: false
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			},
			{
				xtype: 'actioncolumn',
				header: 'Historico ',
				tooltip: 'Historico',
				width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						} 
						,handler: mostrarHistorico
					}
				]				
			}	
		],					
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		displayInfo: true,		
		clicksToEdit: 1, 
		bbar: {			
			items: [					
				'->',
				'-',
				{
					xtype: 'button',
					id: 'btnGuardar',
					text: 'Guardar',
					iconCls: 'icoAceptar',
					handler: procesarGuardar
				},
				'-',
				{
					xtype: 'button',
					id: 'btnSalir',
					text: 'Salir',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '15proyectopefExt.jsp';					
					}
				}
			]
		}
	});


	var  elementosForma = [
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'EPO',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_epo',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo			
		},		
		{
			xtype: 'numberfield',
			fieldLabel: 'No. EPO',		
			name: 'no_epo',
			id: 'no_epo1',
			allowBlank: true,
			maxLength: 3,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},	
		{
			xtype: 'numberfield',
			fieldLabel: 'N@E EPO',		
			name: 'ne_epo',
			id: 'ne_epo1',
			allowBlank: true,
			maxLength: 38,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Nombre EPO',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					fieldLabel: 'Nombre EPO',	
					name: 'nombre_epo',
					id: 'nombre_epo1',
					allowBlank: true,
					maxLength: 400,
					width: 200,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'Utilice el * para b�squeda gen�rica ',
					width: 200
				}
			]
		},
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'Secretar�a o Cabeza de sector',
			name: 'ic_secretaria',
			id: 'ic_secretaria1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_secretaria',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catSecretaria,
			tpl : NE.util.templateMensajeCargaCombo			
		},
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'Organismo',
			name: 'ic_organismo',
			id: 'ic_organismo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_secretaria',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catOrganismo,
			tpl : NE.util.templateMensajeCargaCombo			
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,							
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {					
				
					fp.el.mask('Enviando...', 'x-mask-loading');		
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{					
							informacion: 'Consultar'						
						})
					});			
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15proyectopefExt.jsp';					
				}
			}
		]
	});
	

	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: 'Etiquetar EPO por Niveles',			
				id: 'boton1',					
				handler: function() {
					window.location = '15proyectopefNivelesExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<h1><b>Etiquetar EPO por Secretaria</h1></b>',			
				id: 'boton2',					
				handler: function() {
					window.location = '15proyectopefExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Reporte Obra Publica',			
				id: 'boton3',					
				handler: function() {
					window.location = '15proyectopefReporteExt.jsp';
				}
			}	
		]
	};
	
//------ Componente Principal -------------------------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [
			NE.util.getEspaciador(10),
			fpBotones,
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			gridConsulta,
			NE.util.getEspaciador(10)
		]
	});

	catalogoEPO.load();
	catSecretaria.load();
	catOrganismo.load();

});