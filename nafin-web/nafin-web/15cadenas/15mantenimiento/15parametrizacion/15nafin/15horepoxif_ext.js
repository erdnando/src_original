Ext.onReady(function(){
	var oldHour = "";
	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);
				
	var procesarCatProductoData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cveProducto = Ext.getCmp('cveProducto1');
			if(cveProducto.getValue()==''){
				
				var newRecord = new recordType({
					clave:'',
					descripcion:'Todos los productos'
				});
				
				store.insert(0,newRecord);
				store.commitChanges();
				cveProducto.setValue('');

			}
		}
  }
  
  var procesarCatEpoData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cveEpo = Ext.getCmp('cveEpo1');
			if(cveEpo.getValue()==''){
				var newRecord = new recordType({
					clave:"T",
					descripcion:"Todas las EPO's (Horarios Especiales)"
				});
				
				store.insert(0,newRecord);
				store.commitChanges();
				cveEpo.setValue("T");
			}
		}
  };
  
  var procesarCatIfData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cveIf = Ext.getCmp('cveIf1');
			if(cveIf.getValue()==''){
				var newRecord = new recordType({
					clave:'',
					descripcion:"Seleccione..."
				});
				
				store.insert(0,newRecord);
				store.commitChanges();
				cveIf.setValue('');
			}
		}
  };
  
  
  var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		pnl.el.unmask();
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var gridColumnMod = gridHorarios.getColumnModel();
		
		gridHorarios.show();
		if (arrRegistros != null) {
			if (!gridHorarios.isVisible()) {
				contenedorPrincipalCmp.add(gridHorarios);
				contenedorPrincipalCmp.doLayout();
			}
			
			var el = gridHorarios.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGuardar').enable();
				el.unmask();
			}else {
				Ext.getCmp('btnGuardar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	}
	
	var fnRealizarConsulta = function(){
		pnl.el.mask('Consultando...', 'x-mask-loading');
		storeHorariosData.load({
			params: Ext.apply(fpCriteriosConsulta.getForm().getValues())
		});
	}
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		pnl.el.unmask();
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');

		if (arrRegistros != null) {
			if (!gridHorarios.isVisible()) {
				contenedorPrincipalCmp.add(gridHorarios);
				contenedorPrincipalCmp.doLayout();
			}
			
			var el = gridHorarios.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGuardar').enable();
				el.unmask();
			}else {
				Ext.getCmp('btnGuardar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}

	}

	var proccesSuccessGuadaHorarios = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			Ext.MessageBox.alert('Aviso', 'Los Horarios se han restablecido con �xito', function(){
				pnl.el.mask('Consultando...', 'x-mask-loading');
				storeHorariosData.load({
					params: Ext.apply(fpCriteriosConsulta.getForm().getValues())
				});
			});
			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var fnGuardaHorarioNafin = function(record){
		var escorrecto = true;
		var registrosEnviar = [];
		registrosEnviar.push(record.data);
		
		if(record.data['APERTURAPYME']=='' || record.data['APERTURAIF']=='' || 
			record.data['CIERREPYME']=='' || record.data['CIERREIF']=='')
		{
			escorrecto = false;
		}
		
		/*
		if(record.data['APERTURAPYME']){
			validaHorarios(record, 'P');
		}else if(record.data['APERTURAIF']){
			validaHorarios(record, 'P');
		}else if(record.data['CIERREPYME']){
			validaHorarios(record, 'P');
		}else if(record.data['CIERREIF]){
			validaHorarios(record, 'P');
		}
		*/
		
		
		
		if(escorrecto){
			Ext.Ajax.request({
				url: '15horepoxif_ext.data.jsp',
				params: {
					informacion: 'guardaHorarios',
					tipoHorario: 'N',
					registros: Ext.encode(registrosEnviar)
				},
				callback: proccesSuccessGuadaHorarios
			});
		}else{
			Ext.MessageBox.alert('Aviso', 'Es obligatorio establecer todos los horarios');
		}
	};
	
	var fnGuardaHorarioEsp = function(tipoHorario){
		var registrosEnviar = [];
		var escorrecto = true;
		var count = 0;
		
		var modificados = storeHorariosData.getModifiedRecords();
		Ext.each(modificados, function(record) {
			if(record.data['APERTURAPYME']=='' || record.data['APERTURAIF']=='' || 
				record.data['CIERREPYME']=='' || record.data['CIERREIF']=='')
			{
				escorrecto = false;
			}
			
			registrosEnviar.push(record.data);
			count++;
		});
		
		storeHorariosData.commitChanges();
		
		if(count>0){
			if(escorrecto){
				Ext.Ajax.request({
					url: '15horepoxif_ext.data.jsp',
					params: {
						informacion: 'guardaHorarios',
						tipoHorario: tipoHorario,
						registros: Ext.encode(registrosEnviar)
					},
					callback: proccesSuccessGuadaHorarios
				});  
			}else{
				Ext.MessageBox.alert('Aviso', 'Es obligatorio establecer todos los horarios');
			}
		}else{
			Ext.MessageBox.alert('Aviso', 'No hay horarios modificados');
		}
	}

//------------------------------------------------------------------------------

function mtdValidaHora(lsCampoPyme, lsCampoIF, lsEvento, lsTipo, lsCampo, lsProducto)	{
		var f = document.frmHorario;
		var lbError = false;
		var lscampo = (lsTipo == "P")?lsCampoPyme:lsCampoIF;
		var lsHora = f.elements[lscampo].value
		var lvHora = lsHora.split(":");
		if ( lvHora.length == 2 )	{
			if ( mtdValidaNumero(lvHora[0]) &&  mtdValidaNumero(lvHora[1]))	{
				var lsHoraTmp = new Date();
				lsHoraTmp.setHours(lvHora[0]);
				lsHoraTmp.setMinutes(lvHora[1]);
				var lsHoras   = "";
				var lsMinutos = "";
				if ( lsHoraTmp.getHours() < 10 )	lsHoras   = "0" + lsHoraTmp.getHours();
				else								lsHoras   = lsHoraTmp.getHours();
				if ( lsHoraTmp.getMinutes() < 10 )	lsMinutos = "0" + lsHoraTmp.getMinutes();
				else								lsMinutos = lsHoraTmp.getMinutes();
				lsHora =  lsHoras + ":" +  lsMinutos;
			}
			else	lbError = true;
		}
		else	lbError = true;
		
		// SI HUBO UN ERROR 
		if (lbError)	{
			alert("Valor de campo no valido");
			f.elements[lscampo].focus();
			f.elements[lscampo].select();
			return;
		}
		else	f.elements[lscampo].value = lsHora;
		
		//////////	COMPARANDO HORAS CON LAS ESTABLECIDAS DE NAFIN
		if (typeof(lsCampoPyme) == "string"  && typeof(lsCampoIF) == "string"  &&  typeof(lsEvento) == "string" &&  typeof(lsCampo) == "string" )	{
			if (lsEvento == "A") {
				if (lsTipo == "P") {
					if ( fnComparaHora(lsHora, f.elements[lsCampoIF].value) == 1 ){
						f.elements[lsCampoPyme].value = f.elements[lsCampoIF].value;
						alert("El horario de apertura de "+lsProducto+"-Pymes debe ser menor o igual al de apertura "+lsProducto+"-IFs");
						//return;
					} 
					if ( fnComparaHora(lsHora, f.elements[lsCampo].value) == 1 ){
						f.elements[lsCampoPyme].value = f.elements[lsCampo].value;
						alert("El horario de apertura de "+lsProducto+"-Pymes debe ser menor o igual al de cierre "+lsProducto+"-Pymes");
						//return;
					}
				} else if (lsTipo == "I") {
					//Compara que sea mayor o igual al horario de Pyme
					if ( fnComparaHora(lsHora, f.elements[lsCampoPyme].value) == 2 ){
						f.elements[lsCampoIF].value = f.elements[lsCampoPyme].value;
						alert("El horario de apertura de "+lsProducto+"-IF debe ser mayor o igual al de apertura "+lsProducto+"-Pymes");
						//return;
					} 
					if ( fnComparaHora(lsHora, f.elements[lsCampo].value) == 1 ){
						f.elements[lsCampoIF].value = f.elements[lsCampo].value;
						alert("El horario de apertura de "+lsProducto+"-IF debe ser menor o igual al de cierre "+lsProducto+"-IF");
						//return;
					}
					
				}
			} else {

				if (lsTipo == "P") {
					//Compara que sea menor o igual al horario de IF
					if ( fnComparaHora(lsHora, f.elements[lsCampoIF].value) == 1 ){
						f.elements[lsCampoPyme].value = f.elements[lsCampoIF].value;
						alert("El horario de cierre de "+lsProducto+"-Pymes debe ser menor o igual al de cierre "+lsProducto+"-IF");
						//return;
					}
					if ( fnComparaHora(lsHora, f.elements[lsCampo].value) == 2 ){
						f.elements[lsCampoPyme].value = f.elements[lsCampo].value;
						alert("El horario de cierre de "+lsProducto+"-Pymes debe ser mayor o igual al de apertura "+lsProducto+"-Pymes");
						//return;
					}
				} else if (lsTipo == "I") {
					//Compara que sea mayor o igual al horario de Pyme
					if ( fnComparaHora(lsHora, f.elements[lsCampoPyme].value) == 2 ){
						f.elements[lsCampoIF].value = f.elements[lsCampoPyme].value;
						alert("El horario de cierre de "+lsProducto+"-IF debe ser mayor o igual al de cierre "+lsProducto+"-Pymes");
						//return;
					} 
					if ( fnComparaHora(lsHora, f.elements[lsCampo].value) == 2 ){
						f.elements[lsCampoIF].value = f.elements[lsCampo].value;
						alert("El horario de cierre de "+lsProducto+"-IF debe ser mayor o igual al de apertura "+lsProducto+"-IF");
						//return;
					}
					
				}
			}
		}
	}
	
	var formatoHora = function (hora) {
		var result = false, m;
		var re = /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/;
		if ((m = hora.match(re))) {
			result = (m[1].length == 2 ? "" : "0") + m[1] + ":" + m[2];
		}
		return result;
	};
	
	var comparaHorasDeServicio = function(grid, record, fieldName, indexC, indexR){
		var result = true;
		var hrIniPyme = record.data['APERTURAPYME'];
		var hrIniIf = record.data['APERTURAIF'];
		var hrFinPyme = record.data['CIERREPYME'];
		var hrFinIf = record.data['CIERREIF'];
		
		
		if(  (Date.parse('01/01/2013 '+hrIniPyme+':00') > Date.parse('01/01/2013 '+hrIniIf+':00')) ||
			  (Date.parse('01/01/2013 '+hrFinPyme+':00') > Date.parse('01/01/2013 '+hrFinIf+':00'))
		){
			result = false;
			Ext.MessageBox.alert('Aviso', 'El horario de cierre y apertura de Descuento Electr�nico-PYME debe ser menor o igual al de cierre de Descuento electr�nico �IF', function(){
				grid.startEditing(indexR, indexC);
			});	
		}else if(  (Date.parse('01/01/2013 '+hrIniIf+':00') < Date.parse('01/01/2013 '+hrIniPyme+':00')) ||
			  (Date.parse('01/01/2013 '+hrFinIf+':00') < Date.parse('01/01/2013 '+hrFinPyme+':00'))
		){
			result = false;
			Ext.MessageBox.alert('Aviso', 'El horario de cierre y apertura de Descuento Electr�nico-IF debe ser mayor o igual al de cierre de Descuento electr�nico �PYME', function(){
				grid.startEditing(indexR, indexC);
			});	
		}else if( Date.parse('01/01/2013 '+hrIniPyme+':00') > Date.parse('01/01/2013 '+hrFinPyme+':00') ){
			result = false;
			Ext.MessageBox.alert('Aviso', 'El horario de apertura de Descuento Electr�nico-PYME debe ser menor o igual a su horario de cierre', function(){
				grid.startEditing(indexR, indexC);
			});	
		}else if( Date.parse('01/01/2013 '+hrIniIf+':00') > Date.parse('01/01/2013 '+hrFinIf+':00') ){
			result = false;
			Ext.MessageBox.alert('Aviso', 'El horario de apertura de Descuento Electr�nico-IF debe ser menor o igual a su horario de cierre', function(){
				grid.startEditing(indexR, indexC);
			});	
		}
		
		return result;
	};


//------------------------------------------------------------------------------
	var storeCatProductoData = new Ext.data.JsonStore({
		id: 'storeCatProductoData',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15horepoxif_ext.data.jsp',
		baseParams: {
			informacion: 'catalogoProducto'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatProductoData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var storeCatEpoData = new Ext.data.JsonStore({
		id: 'storeCatEpoData',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15horepoxif_ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatEpoData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var storeCatIfData = new Ext.data.JsonStore({
		id: 'storeCatIfData',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15horepoxif_ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIf'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatIfData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var storeHorariosData = new Ext.data.JsonStore({
		id: 'storeHorariosData1',
		root : 'registros',
		url : '15horepoxif_ext.data.jsp',
		baseParams: {
			informacion: 'consultaHorarios'
		},
		fields: [
			{name: 'CVEEPO'},
			{name: 'CVEIF'},
			{name: 'CVEPRODUCTO'},
			{name: 'NOMBREEPO'},
			{name: 'NOMBREIF'},
			{name: 'NOMBREPRODUCTO'},
			{name: 'APERTURAPYME'},
			{name: 'APERTURAIF'},
			{name: 'CIERREPYME'},
			{name: 'CIERREIF'},
			{name: 'TIPOHORARIO'},
			{name: 'USAHORARIONAF'},
			{name: 'OPERFONDEO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
		
	});

//------------------------------------------------------------------------------
	var elmentosForma = [
		{
			name:'cveProducto',
			id:'cveProducto1',
			fieldLabel: '* Producto',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'cveProducto',
			emptyText: 'Seleccione...',
			anchor:'70%',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			hidden: false,
			store : storeCatProductoData
		},
		{
			name:'cveEpo',
			id:'cveEpo1',
			fieldLabel: '* Nombre de la EPO',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'cveEpo',
			emptyText: 'Seleccione...',
			anchor:'100%',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			hidden: false,
			store : storeCatEpoData,
			listeners:{
				select:{ 
					fn:function (combo) {
						var valEpo = combo.getValue();
						if(valEpo!='T' && valEpo!=''){
							storeCatIfData.load({
								params:{
									cveEpo: valEpo
								}
							});
						}else{
							Ext.getCmp('cveIf1').setValue('');
							storeCatIfData.removeAll();
						}
					}	
				}
			}
		},
		{
			name:'cveIf',
			id:'cveIf1',
			fieldLabel: 'Nombre el IF',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'cveIf',
			emptyText: 'Seleccione...',
			anchor:'100%',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			hidden: false,
			store : storeCatIfData
		}
	
	]
//-----------------------------------------------------------------------------
	var fpCriteriosConsulta = new Ext.form.FormPanel({
		name:'fpCriteriosConsulta',
		id: 'fpCriteriosConsulta1',
		title:'Nafinet',
		frame: true,
		style: 'margin:0 auto;',
		labelWidth: 150,
		width: 600,
		defaultType: 'combo',
		defaults: {
			msgTarget: 'side',
			anchor: '-2'
		},
		monitorValid: true,
		items: elmentosForma,
		buttons:[
			{
				xtype:'button',
				id:'btnConsulta',
				text:'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: fnRealizarConsulta
			},
			{
				xtype:'button',
				id:'btnLimpiar',
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function(){
					window.location.href = '15horepoxif_ext.jsp';
				}
			}
		]
	
	});
	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan:3 , align: 'center'},
				{header: 'Apertura', colspan:2 , align: 'center'},
				{header: 'Cierre', colspan:2 , align: 'center'},
				{header: '', colspan:3 , align: 'center'}
			]
		]
	});

	var gridHorarios = new Ext.grid.EditorGridPanel({
		id: 'gridHorarios',
		store: storeHorariosData,
		margins: '20 0 0 0',
		clicksToEdit: 1,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		plugins: grupos,
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBREIF',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Producto',
				tooltip: 'Producto',
				dataIndex: 'NOMBREPRODUCTO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'APERTURAPYME',
				sortable: true,
				width: 70,
				resizable: true,
				hidden: false,
				align:'center',
				editor: {
					 xtype: 'textfield',
					 maxLength:5
					 
				},
				renderer:  function (value, columna, registro){
					return NE.util.colorCampoEdit(value,columna,registro);
				}
			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'APERTURAIF',
				sortable: true,
				width: 70,
				align:'center',
				resizable: true,
				hidden: false,
				editor: {
					 xtype: 'textfield',
					 maxLength:5
					 
				},
				renderer:  function (value, columna, registro){
					return NE.util.colorCampoEdit(value,columna,registro);
				}
			},
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'CIERREPYME',
				sortable: true,
				width: 70,
				align:'center',
				resizable: true,
				hidden: false,
				editor: {
					 xtype: 'textfield',
					 maxLength:5
					 
				},
				renderer:  function (value, columna, registro){
					return NE.util.colorCampoEdit(value,columna,registro);
				}
			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'CIERREIF',
				sortable: true,
				width: 70,
				align:'center',
				resizable: true,
				hidden: false,
				editor: {
					 xtype: 'textfield',
					 maxLength:5
					 
				},
				renderer:  function (value, columna, registro){
					return NE.util.colorCampoEdit(value,columna,registro);
					//return causa;
				}
			},
			{
				header: 'Tipo de Horario',
				tooltip: 'Tipo de Horario',
				dataIndex: 'TIPOHORARIO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					if(registro.data['USAHORARIONAF']=='S')
						return 'Especial';
					else
						return 'Horario Nafin';
				}
			},
			{
				xtype: 		'actioncolumn',
				header: 		'Usar Horarios Nafin',
				tooltip: 	'Usar Horarios Nafin',
				width: 		150,
				align:		'center',
				hidden: 		false,
				renderer: function(valor, metadata, registro, rowindex, colindex, store) {
				},
				items: [
				{
					getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						if(registro.data['USAHORARIONAF']=='S')
							return 'icoGuardar';
						else
							return 'icoGuardarb';
					},
					handler: function(grid, rowIndex, colIndex) {
						var store = grid.getStore();
						var record = store.getAt(rowIndex);
						if(record.data['USAHORARIONAF']=='S')
							fnGuardaHorarioNafin(record);
					}
				}
				]
			},
			{
				header: 'Opera Fondeo Propio',
				tooltip: 'Opera Fondeo Propio',
				dataIndex: 'OPERFONDEO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					if(causa=='S')
						return 'SI';
					else
						return 'NO';
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 400,
		width: 885,
		title: "Horarios de Servicio mismo d�a para Pyme's por EPO y por IF - PRODUCTO",
		frame: true,
		listeners: {
			cellclick :function(grid, rowIndex, colIndex, evento){

			},
			beforeedit : function(e){
				var record = e.record;
				var gridf = e.grid;
				var store = gridf.getStore();
				var fieldName = gridf.getColumnModel().getDataIndex(e.column);
				var horaGral = record.data[fieldName];
				
				oldHour = horaGral;
			},
			afteredit : function(e){
					
					var record = e.record;
					var gridf = e.grid; 
					var store = gridf.getStore();
					var fieldName = gridf.getColumnModel().getDataIndex(e.column); // Se obtine nombre del Campo (Fields del Store)
					
					var horaGral = record.data[fieldName];
					
					//if(fieldName == 'APERTURAPYME'){
					var valor = formatoHora(horaGral);
					if(valor){
						record.data[fieldName]=valor;
						if ( !comparaHorasDeServicio(gridf, record, fieldName, e.column, e.row) ){
							record.data[fieldName]=oldHour;
							record.commit(false);
							//store.commitChanges();
							//gridf.startEditing(e.row, e.column);
							
						}else{
							record.commit(true);
							//store.commitChanges();
						}
					}else{
						record.data[fieldName]=oldHour;
						record.commit(false);
						//store.commitChanges();
						
					}
					
					
					
					
					
					/*
					var hrIniPyme = record.data['APERTURAPYME'];
					var hrIniIf = record.data['APERTURAIF'];
					var hrFinPyme = record.data['CIERREPYME'];
					var hrFinIf = record.data['CIERREIF'];
					
					if(fieldName == 'APERTURAPYME'){
						validaHorarios(record, 'P');
					}else if(fieldName == 'APERTURAIF'){
						validaHorarios(record, 'P');
					}else if(fieldName == 'CIERREPYME'){
						validaHorarios(record, 'P');
					}else if(fieldName == 'CIERREIF'){
						validaHorarios(record, 'P');
					}
					*/
			}
			
		},
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Guardar',
					id: 'btnGuardar',
					handler: function(){
						fnGuardaHorarioEsp('S');
					}
				}
			]
		}
	});

//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		renderHidden: true,
		layoutConfig: {
			align:'center'
		},
		items: [
			NE.util.getEspaciador(20),
			fpCriteriosConsulta,
			NE.util.getEspaciador(10)
		]
	});
	
	
	storeCatProductoData.load();
	storeCatEpoData.load();

});