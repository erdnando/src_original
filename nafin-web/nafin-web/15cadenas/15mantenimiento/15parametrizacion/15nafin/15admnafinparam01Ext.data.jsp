<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		javax.naming.Context,
		com.netro.factdistribuido.*,
		com.netro.dispersion.*, 
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoBancoServicio_b,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEPODistribuidores,
		com.netro.model.catalogos.CatalogoPymeBusqAvazada,
		org.apache.commons.logging.Log,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String strMensaje = (request.getParameter("strMensaje") != null)?request.getParameter("strMensaje"):"";

String infoRegresar ="";

ParametrosDescuento paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 

Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class); 


JSONObject jsonObj		= new JSONObject();
boolean success			= true;
	
	
log.debug("informacion ------------------------------> <"+informacion+">");

if (informacion.equals("getClavePerfil")){


	com.netro.seguridadbean.Seguridad beanSegFacultad = null;
	try {
	
		beanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class); 
	
	}catch(Exception e){
		success		= false;
		log.error("getClavePerfil(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");	
	}

	String cvePerf = beanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);

	jsonObj.put("cvePerf",				cvePerf						);
	jsonObj.put("estadoSiguiente",	"RESPUESTA_CLAVE_PERFIL");
	jsonObj.put("success",				new Boolean(success)		);
	infoRegresar = jsonObj.toString();

}else if (	informacion.equals("Catalogo.Epo")){

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setTipoCredito("D");
	cat.setOrden("cg_razon_social");

	infoRegresar = cat.getJSONElementos();

}else if ( informacion.equals("Catalogo.IF")){

	String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	Registros reg = null;

	JSONArray jsonArr = new JSONArray();
	String ic_if_tipo1			= "";

	if(	!"".equals(ic_epo)	){

		
		String ic_finananciera_s 	= "";
		reg = paramDsctoEJB.getDatosPymeCatalogoIf(ic_epo);
		List datos = new ArrayList();
		int c=0;
		while(reg.next()){

			HashMap mapa = new HashMap();

			if(c == 0){
				mapa.put("clave",				"");
				mapa.put("descripcion",		"Seleccionar IF");
				mapa.put("ic_financiera",	"");
				datos.add(mapa);
				mapa = new HashMap();
			}
			String rs_ic_if = reg.getString(1);
			String rs_cg_razon_social = reg.getString(2);
			String rs_ic_financiera = reg.getString(3);
			mapa.put("clave",				rs_ic_if);
			mapa.put("descripcion",		rs_cg_razon_social);
			mapa.put("ic_financiera",	rs_ic_financiera);

			datos.add(mapa);
			ic_if_tipo1 += "".equals(ic_if_tipo1)?rs_ic_financiera:","+rs_ic_financiera;
			c++;
		}
		if(datos != null){
			jsonArr = JSONArray.fromObject(datos);
		}
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("total",  Integer.toString(jsonArr.size()));
	jsonObj.put("registros", jsonArr.toString() );
	jsonObj.put("ic_if_tipo1", ic_if_tipo1 );

	infoRegresar = jsonObj.toString();

}else if (			informacion.equals("Catalogo.Banco.Servicio.distribuidores")){

	String ic_if_tipo1 = (request.getParameter("ic_if_tipo1") == null)?"":request.getParameter("ic_if_tipo1");

	CatalogoBancoServicio_b cat = new CatalogoBancoServicio_b();
	cat.setCampoClave("ic_financiera");
	cat.setCampoDescripcion("TRIM(cd_nombre)");
	cat.setDistinc(true);
	cat.setTipoFinanciera("1");
	if(!"".equals(ic_if_tipo1)){
		cat.setValoresCondicionNotIn(ic_if_tipo1, Integer.class);
	}
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();

}else if (	informacion.equals("Catalogo.Banco.Servicio")){

	CatalogoBancoServicio_b cat = new CatalogoBancoServicio_b();
	cat.setCampoClave("TRIM(cd_nombre)");
	cat.setCampoDescripcion("TRIM(cd_nombre)");
	cat.setDistinc(true);
	cat.setTipoFinanciera("1,14");
	cat.setOrden("1");

	infoRegresar = cat.getJSONElementos();

}else if (	informacion.equals("Catalogo.Moneda")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	infoRegresar = cat.getJSONElementos();

	infoRegresar = cat.getJSONElementos();

}else if (	informacion.equals("Catalogo.Plaza") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_plaza");
	cat.setCampoClave("ic_plaza");
	cat.setCampoDescripcion("INITCAP(cd_descripcion||','||cg_estado)");
	cat.setOrden("1");

	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Descuento.Consulta") ) {


	boolean consulta			= true;
	String estadoSiguiente	= "", sTraeLineas="S";
	Registros registros		= null;

	String txtNE	= (request.getParameter("txtNE") == null)?"":request.getParameter("txtNE");

	Hashtable datos = new Hashtable();
	datos		=	paramDsctoEJB.getDatosPymeNafinElec(txtNE, consulta);
	if (datos!=null) {
		sTraeLineas = (String) datos.get("sTraeLineas");
	}
	if (sTraeLineas.equals("S")){
		registros = (Registros) datos.get("registros");
	}

	estadoSiguiente = "RESPUESTA_CONSULTAR_DESCUENTO";

	jsonObj.put("sTraeLineas",			sTraeLineas					);
	if(registros != null){
		jsonObj.put("registros",			registros.getJSONData()	);	
	}
	jsonObj.put("txtNE",					txtNE							);
	jsonObj.put("estadoSiguiente",	estadoSiguiente			);
	jsonObj.put("success",				new Boolean(success)		);
	infoRegresar = jsonObj.toString();

}else if (	informacion.equals("Descuento.Modificar") ) {

	String estadoSiguiente	= "",sTraeLineas="S",plaza="false",msg="";
	Registros registros		= null;

	String clavePymeIf	= (request.getParameter("clavePyme") == null)?"":request.getParameter("clavePyme");
	String txtNE			= (request.getParameter("txtNE") == null)?"":request.getParameter("txtNE");
	String txtIcCtaBanco	= (request.getParameter("txtIcCtaBanco")== null)?"":request.getParameter("txtIcCtaBanco");
	String cgBanco        = "";
	String icMoneda       = "";
	String cgSucursal     = "";
	String cgNumeroCuenta = "";
	String cgNEPyme       = "";
	String icPlaza        = "";
	String cgCuentaClabe  = "";
	String csCU           = "";
	String cgCuentaClabe_C  = "";

	Hashtable datos = new Hashtable();
	datos		=	paramDsctoEJB.getDatosPymeAmodificar(clavePymeIf, txtIcCtaBanco);
	if (datos!=null) {
		sTraeLineas = (String) datos.get("sTraeLineas");
		plaza			= (String) datos.get("plaza");
	}
	if (sTraeLineas.equals("N")){
		registros = (Registros) datos.get("registros");
		
		if( registros.next() ){
			cgBanco        = registros.getString("cg_banco").trim();
			icMoneda       = registros.getString("ic_moneda").trim();
			cgSucursal     = registros.getString("cg_sucursal").trim();
			cgNumeroCuenta = registros.getString("cg_numero_cuenta").trim();
			cgNEPyme       = registros.getString("ic_nafin_electronico").trim();
			icPlaza        = (registros.getString("cg_plaza") == null)?"":registros.getString("cg_plaza").trim();
			cgCuentaClabe  = (registros.getString("cg_cuenta_clabe") == null)?"":registros.getString("cg_cuenta_clabe").trim();
			csCU           = registros.getString("cs_convenio_unico").trim();
			cgCuentaClabe_C  = (registros.getString("CG_CUENTA_CLABE_C") == null)?"":registros.getString("CG_CUENTA_CLABE_C").trim();
		}

		jsonObj.put("sTraeLineas",		sTraeLineas			);
		jsonObj.put("msg",				msg					);
		jsonObj.put("plaza",				plaza					);
		jsonObj.put("txtNE",				txtNE					);
		jsonObj.put("cgBanco",			cgBanco				);
		jsonObj.put("icMoneda",			icMoneda				);
		jsonObj.put("cgSucursal",		cgSucursal			);
		jsonObj.put("cgNumeroCuenta",	cgNumeroCuenta		);
		jsonObj.put("cgNEPyme",			cgNEPyme				);
		jsonObj.put("icPlaza",			icPlaza				);
		jsonObj.put("cgCuentaClabe",	cgCuentaClabe		);
		jsonObj.put("cgCuentaClabe_C",	cgCuentaClabe_C		);
		jsonObj.put("csCU",				csCU					);

		estadoSiguiente = "RESPUESTA_MODIFICAR_DESCUENTO";

	}else{
		msg = "No se puede modificar la cuenta bancaria porque tiene Lineas de Credito asociadas";
		estadoSiguiente = "No action";
	}

	jsonObj.put("estadoSiguiente",	estadoSiguiente				);
	jsonObj.put("success",				new Boolean(success)			);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Descuento.Actualizar") ){


	String strSucursal	="";
	strMensaje		="";
	String txtAction     = (request.getParameter("txtAction")    == null)?"":request.getParameter("txtAction");
	String txtBanco      = (request.getParameter("txtBanco")     == null)?"":request.getParameter("txtBanco");
	String txtSucursal   = (request.getParameter("txtSucursal")  == null)?"0":request.getParameter("txtSucursal").trim();
			strSucursal		= txtSucursal.trim();
			txtSucursal		= (strSucursal.equals(""))?"0":strSucursal;
	String cboMoneda     = (request.getParameter("cboMoneda")    == null)?"":request.getParameter("cboMoneda");
	String txtCuenta     = (request.getParameter("txtCuenta")    == null)?"":request.getParameter("txtCuenta");
	String txtIcCtaBanco = (request.getParameter("txtIcCtaBanco")== null)?"":request.getParameter("txtIcCtaBanco");
	String txtNE         = (request.getParameter("txtNE")        == null)?"":request.getParameter("txtNE");
	String cboPlaza         = (request.getParameter("cboPlaza")        == null)?"":request.getParameter("cboPlaza");
	String txtCtaClabe	= (request.getParameter("txtCuentaClabe")== null)?"null":request.getParameter("txtCuentaClabe");
	String confirtxtCuentaClabe	= (request.getParameter("confirtxtCuentaClabe")== null)?"null":request.getParameter("confirtxtCuentaClabe");
	
	String chkCU			= (request.getParameter("chkCU")== null)?"N":request.getParameter("chkCU");
	chkCU						= (chkCU.equals("on"))?"S":"N";

	String sPyme	= "";

	Hashtable datos = new Hashtable();
	datos		=	paramDsctoEJB.getDatosPymeNafinElec(txtNE, false);
	if (datos!=null) {
		sPyme = (String) datos.get("ic_epo_pyme_if");
	}

	strMensaje = paramDsctoEJB.setDatosPymeAmodificar(txtBanco,	txtSucursal,	cboMoneda,	txtCuenta,	sPyme,	txtNE,
																		cboPlaza,	txtCtaClabe,	chkCU,	txtIcCtaBanco,	iNoUsuario , confirtxtCuentaClabe );

	jsonObj.put("estadoSiguiente",	"RESPUESTA_ACTUALIZAR_DESCUENTO"	);
	jsonObj.put("txtNE",					txtNE										);
	jsonObj.put("strMensaje",			strMensaje								);
	jsonObj.put("success",				new Boolean(success)					);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Descuento.Eliminar") ){

	String estadoSiguiente	= "";

	String clavePymeIf	= (request.getParameter("clavePyme") == null)?"":request.getParameter("clavePyme");
	String txtNE			= (request.getParameter("txtNE") == null)?"":request.getParameter("txtNE");
	String txtIcCtaBanco	= (request.getParameter("txtIcCtaBanco")== null)?"":request.getParameter("txtIcCtaBanco");

	strMensaje = paramDsctoEJB.eliminaDatosPymeAmodificar(clavePymeIf,	txtNE,	txtIcCtaBanco,	iNoUsuario );
	
	jsonObj.put("txtNE",				txtNE					);
	jsonObj.put("strMensaje",		strMensaje			);

	estadoSiguiente = "RESPUESTA_ELIMINAR_DESCUENTO";

	jsonObj.put("estadoSiguiente",	estadoSiguiente				);
	jsonObj.put("success",				new Boolean(success)			);
	infoRegresar = jsonObj.toString();


log.debug(" Fodea 033-2014 -------Inicia  ----------------------->");

} else if (	informacion.equals("Catalogo.CatalogoBanco_tef")			){	

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_bancos_tef");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_bancos_tef");
	cat.setOrden("cd_descripcion");
	infoRegresar = cat.getJSONElementos();
	
}else if (	informacion.equals("Descuento.ValidaDispersion") ){
	
	String estadoSiguiente	= "";
	String txtNE         = (request.getParameter("txtNE")        == null)?"":request.getParameter("txtNE");
	
	String sPyme	= "";
	Hashtable datos = new Hashtable();
	datos		=	paramDsctoEJB.getDatosPymeNafinElec(txtNE, false);
	if (datos!=null) {
		sPyme = (String) datos.get("ic_epo_pyme_if");
	}
	
	 String eposDispersion =  paramDsctoEJB.getValidaEposDispersionPyme(sPyme );

	estadoSiguiente = "RESPUESTA_VALIDA_DISPERSION";

	jsonObj.put("estadoSiguiente",	estadoSiguiente);
	jsonObj.put("eposDispersion",	eposDispersion);
	jsonObj.put("txtNE",	txtNE);
	jsonObj.put("strMensaje",	strMensaje);
	jsonObj.put("success",				new Boolean(success)			);
	infoRegresar = jsonObj.toString();

}else if (	informacion.equals("Descuento.ConsEposDispersion") ){

	String estadoSiguiente	= "";
	 Registros registros        = null;
	 
	String txtNE         = (request.getParameter("txtNE")        == null)?"":request.getParameter("txtNE");
	String txtCuentaClabe	= (request.getParameter("txtCuentaClabe")== null)?"null":request.getParameter("txtCuentaClabe");
	String cboMoneda     = (request.getParameter("cboMoneda")    == null)?"":request.getParameter("cboMoneda");
	String txtCtaClabe	= (request.getParameter("txtCuentaClabe")== null)?"null":request.getParameter("txtCuentaClabe");
	String sPyme	= "", smoneda ="", sTraeLineas ="", srazon_social ="", srfc ="", ic_cuenta = "", ic_cuenta_bancaria ="";
	
	Hashtable datos = new Hashtable();
	datos		=	paramDsctoEJB.getDatosPymeNafinElec(txtNE, false);
		
	if (datos!=null) {
		sPyme = (String) datos.get("ic_epo_pyme_if");
		srfc =  (String) datos.get("srfc");
		srazon_social =  (String) datos.get("srazon_social");		
	}
	
	smoneda=  paramDsctoEJB.getNombreMoneda( cboMoneda );
	
	Hashtable  datosR = new Hashtable();
   datosR  =    paramDsctoEJB.getDatosDispersion(sPyme, txtNE , cboMoneda , txtCtaClabe );	
	String totalCuentas = (String) datosR.get("totalCuentas");
	registros = (Registros) datosR.get("registros");	
	if(datos != null){
		jsonObj.put("registros",            registros.getJSONData()    );    
    }

	estadoSiguiente = "RESPUESTA_CONS_EPO_DISPERSION";
	jsonObj.put("lblTipo", "PROVEEDOR"    );
	jsonObj.put("lblProducto", "DESCUENTO ELECTRONICO"    );
	jsonObj.put("lblnaPyme",   txtNE    );
	jsonObj.put("lblNombrePyme",   srazon_social    );
	jsonObj.put("lblRFCPyme",   srfc    );
	jsonObj.put("lblMoneda",   smoneda    );
	jsonObj.put("lblCuentaClabe",   txtCuentaClabe    );		
	jsonObj.put("totalCuentas",   totalCuentas    );	
	jsonObj.put("cboMoneda",   cboMoneda    );
   jsonObj.put("estadoSiguiente",  estadoSiguiente  );
   jsonObj.put("success",  new Boolean(success)  );
   infoRegresar = jsonObj.toString();

}else if (	informacion.equals("Descuento.Agregar_Epos") ){
	
	String strSucursal	="",  estadoSiguiente ="";

	String txtAction     = (request.getParameter("txtAction")    == null)?"":request.getParameter("txtAction");
	String txtBanco      = (request.getParameter("txtBanco")     == null)?"":request.getParameter("txtBanco");
	String txtSucursal   = (request.getParameter("txtSucursal")  == null)?"0":request.getParameter("txtSucursal").trim();
	strSucursal		= txtSucursal.trim();
	txtSucursal		= (strSucursal.equals(""))?"0":strSucursal;
	String cboMoneda     = (request.getParameter("cboMoneda")    == null)?"":request.getParameter("cboMoneda");
	String txtCuenta     = (request.getParameter("txtCuenta")    == null)?"":request.getParameter("txtCuenta");
	String txtNE         = (request.getParameter("txtNE")        == null)?"":request.getParameter("txtNE");
	String cboPlaza         = (request.getParameter("cboPlaza")        == null)?"":request.getParameter("cboPlaza");	
	String txtCtaClabe	= (request.getParameter("txtCuentaClabe")== null)?"null":request.getParameter("txtCuentaClabe");
	String chkCU			= (request.getParameter("chkCU")== null)?"N":request.getParameter("chkCU");
	chkCU						= (chkCU.equals("on"))?"S":"N";
	String confirtxtCuentaClabe			= (request.getParameter("confirtxtCuentaClabe")== null)?"":request.getParameter("confirtxtCuentaClabe");
	String ic_bancos_tef      = (request.getParameter("ic_bancos_tef")     == null)?"":request.getParameter("ic_bancos_tef");	
		
	String ic_epos[]		= request.getParameterValues("ic_epos");
	String ic_cuentas[]		= request.getParameterValues("ic_cuentas");
	
	//-------------Moneda  DOLAR AMERICANO----------------------
	String cg_CuentaDips      = (request.getParameter("cg_CuentaDips")     == null)?"":request.getParameter("cg_CuentaDips");
	String txtPlazaDisp      = (request.getParameter("txtPlazaDisp")     == null)?"":request.getParameter("txtPlazaDisp");
	String txtSucursalDisp      = (request.getParameter("txtSucursalDisp")     == null)?"":request.getParameter("txtSucursalDisp");
	
	String ic_Swift      = (request.getParameter("ic_Swift")     == null)?"":request.getParameter("ic_Swift");
	String ic_aba      = (request.getParameter("ic_aba")     == null)?"":request.getParameter("ic_aba");
	String ic_tipo_cuenta ="";
	
	if(cboMoneda.equals("1") ) {
		cg_CuentaDips =txtCuenta;
		txtPlazaDisp = cboPlaza; 
		txtSucursalDisp = txtSucursal;
		ic_tipo_cuenta ="40";
	}else if(cboMoneda.equals("54") ) {
		ic_tipo_cuenta ="50";		
	}

	String sPyme	= "", smoneda ="", sTraeLineas ="", srazon_social ="", srfc =""; 
	Hashtable datos = new Hashtable();
	datos		=	paramDsctoEJB.getDatosPymeNafinElec(txtNE, false);
		
	if (datos!=null) {
		sPyme = (String) datos.get("ic_epo_pyme_if");
		srfc =  (String) datos.get("srfc");
		srazon_social =  (String) datos.get("srazon_social");		
	}
	try  {
		  
		 
		strMensaje =  paramDsctoEJB.ovinsertarCuentaCBP( iNoUsuario ,sPyme ,   txtNE ,	"1", cboMoneda ,  ic_bancos_tef ,  	txtSucursalDisp,  txtPlazaDisp, cg_CuentaDips, 	ic_tipo_cuenta, 	"P", ic_Swift ,  ic_aba , 	txtCtaClabe,  ic_epos, ic_cuentas, srfc   ) ;   //Fodea 033-2014 
						 										
																		 
		estadoSiguiente = "RESPUESTA_GUARDAR_EPO";
		
	}catch(Exception e){
		success = false;		
		e.printStackTrace();
		throw new AppException("Ocurrió un error al agregar un registro.");
	}finally{
		strMensaje = "Se realizo con éxito la parametrización de las Cadenas Productivas";
	}

	jsonObj.put("strMensaje",			strMensaje	);
	jsonObj.put("estadoSiguiente",	estadoSiguiente);
	jsonObj.put("eposDispersion",	"S");	
	
	/*jsonObj.put("lblTipo", "PROVEEDOR"    );
	jsonObj.put("lblProducto", "DESCUENTO ELECTRONICO"    );
	jsonObj.put("lblnaPyme",   txtNE    );
	jsonObj.put("lblNombrePyme",   srazon_social    );
	jsonObj.put("lblRFCPyme",   srfc    );
	jsonObj.put("lblMoneda",   smoneda    );
	jsonObj.put("lblCuentaClabe",   txtCtaClabe    );			
	jsonObj.put("cboMoneda",   cboMoneda    );
	*/
	jsonObj.put("success",	new Boolean(success)	);
	infoRegresar = jsonObj.toString();

log.debug(" Fodea 033-2014 -------Final  ----------------------->");


}else if (	informacion.equals("Descuento.Agregar") ){

	

	String strSucursal	="", 	sPyme	= "";
	String txtAction     = (request.getParameter("txtAction")    == null)?"":request.getParameter("txtAction");
	String txtBanco      = (request.getParameter("txtBanco")     == null)?"":request.getParameter("txtBanco");
	String txtSucursal   = (request.getParameter("txtSucursal")  == null)?"0":request.getParameter("txtSucursal").trim();
	strSucursal		= txtSucursal.trim();
	txtSucursal		= (strSucursal.equals(""))?"0":strSucursal;
	String cboMoneda     = (request.getParameter("cboMoneda")    == null)?"":request.getParameter("cboMoneda");
	String txtCuenta     = (request.getParameter("txtCuenta")    == null)?"":request.getParameter("txtCuenta");
	String txtNE         = (request.getParameter("txtNE")        == null)?"":request.getParameter("txtNE");
	String cboPlaza         = (request.getParameter("cboPlaza")        == null)?"":request.getParameter("cboPlaza");	
	String txtCtaClabe	= (request.getParameter("txtCuentaClabe")== null)?"null":request.getParameter("txtCuentaClabe");
	String chkCU			= (request.getParameter("chkCU")== null)?"N":request.getParameter("chkCU");
	chkCU						= (chkCU.equals("on"))?"S":"N";
	String confirtxtCuentaClabe			= (request.getParameter("confirtxtCuentaClabe")== null)?"":request.getParameter("confirtxtCuentaClabe");
	
	Hashtable datos = new Hashtable();
	datos		=	paramDsctoEJB.getDatosPymeNafinElec(txtNE, false);
	if (datos!=null) {
		sPyme = (String) datos.get("ic_epo_pyme_if");
	}

	strMensaje = paramDsctoEJB.addDatosPymeAmodificar(txtBanco,	txtSucursal,	cboMoneda,	txtCuenta,	sPyme,	txtNE,
																	cboPlaza,	txtCtaClabe,	chkCU,	iNoUsuario, confirtxtCuentaClabe );

	jsonObj.put("txtNE",					txtNE									);
	jsonObj.put("strMensaje",			strMensaje							);
	jsonObj.put("estadoSiguiente",	"RESPUESTA_AGREGAR_DESCUENTO"	);
	jsonObj.put("success",				new Boolean(success)				);
	infoRegresar = jsonObj.toString();

}else if (	informacion.equals("Distribuidores.Agregar") ){

	String existeError="";
	String ic_epo			= (request.getParameter("ic_epo") 			== null)?"":request.getParameter("ic_epo");	
	String txtNE			= (request.getParameter("txtNE")        	== null)?"":request.getParameter("txtNE");	
	String ic_if			= (request.getParameter("ic_if") 			== null)?"":request.getParameter("ic_if");	
	String txtBanco		= (request.getParameter("txtBanco") 		== null)?"":request.getParameter("txtBanco");	
	String txtSucursal	= (request.getParameter("txtSucursal")  	== null)?"":request.getParameter("txtSucursal").trim();
	String cboMoneda		= (request.getParameter("cboMoneda")    	== null)?"":request.getParameter("cboMoneda");
	String txtCuenta		= (request.getParameter("txtCuenta")    	== null)?"":request.getParameter("txtCuenta");		
	String cboPlaza		= (request.getParameter("cboPlaza")			== null || request.getParameter("cboPlaza").equals(""))?"null":request.getParameter("cboPlaza");
	String CtasCapturadas= (request.getParameter("CtasCapturadas")	== null)?"":request.getParameter("CtasCapturadas");

	try{
		Hashtable datos = new Hashtable();
		datos		=	paramDsctoEJB.addDatosCtasDist(ic_epo,txtBanco,txtSucursal,cboMoneda,txtCuenta,ic_if,txtNE,cboPlaza,CtasCapturadas );
		if (datos!=null) {
			strMensaje		= (String)datos.get("strMensaje");
			CtasCapturadas	= (String)datos.get("CtasCapturadas");
			existeError		= (String)datos.get("existeError");
		}
	}catch(Exception e){
		success = false;
		log.error("Distribuidores.Agregar(Exception): Error al agregar registro");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al agregar un registro.");
	}

	jsonObj.put("strMensaje",			strMensaje);
	jsonObj.put("CtasCapturadas",		CtasCapturadas);
	jsonObj.put("existeError",			existeError);
	jsonObj.put("estadoSiguiente",	"RESPUESTA_AGREGAR_DISTRIBUIDORES"	);
	jsonObj.put("success",				new Boolean(success)						);
	infoRegresar = jsonObj.toString();

}else if (	informacion.equals("Distribuidores.Consulta") ){

	String txtNE			= (request.getParameter("txtNE") == null)?"":request.getParameter("txtNE");
	String ic_epo			= (request.getParameter("ic_epo")== null)?"":request.getParameter("ic_epo");
	String ic_if			= (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
	String txtBanco		= (request.getParameter("txtBanco") == null)?"":request.getParameter("txtBanco");
	String txtSucursal	= (request.getParameter("txtSucursal") == null)?"":request.getParameter("txtSucursal").trim();
	String cboMoneda		= (request.getParameter("cboMoneda")   == null)?"":request.getParameter("cboMoneda");
	String txtCuenta		= (request.getParameter("txtCuenta")   == null)?"":request.getParameter("txtCuenta");
	String cboPlaza		= (request.getParameter("cboPlaza")		== null)?"":request.getParameter("cboPlaza");
	String CtasCapturadas= (request.getParameter("CtasCapturadas") == null)?"":request.getParameter("CtasCapturadas");

	int start = 0;
	int limit = 0;

	ConsCtaBancariasDist paginador =new  ConsCtaBancariasDist();
	paginador.setClaveEpo(ic_epo);
	paginador.setNEPyme(txtNE);
	paginador.setClaveIf(ic_if);
	paginador.setBanco(txtBanco);
	paginador.setSucursal(txtSucursal);
	paginador.setMoneda(cboMoneda);
	paginador.setCuenta(txtCuenta);
	paginador.setPlaza(cboPlaza);
	paginador.setCuentasCaptura(CtasCapturadas);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request);
		}
		infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}

}
//----- FODEA 016_2018 INICIA -----//
else if("FACT_DIST.CATALOGO_EPO".equals(informacion)) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setDistinc(true);
	catalogo.setCampoClave("A.IC_EPO");
	catalogo.setCampoDescripcion("A.CG_RAZON_SOCIAL");
	catalogo.setTabla("COMCAT_EPO A, COMREL_PRODUCTO_EPO B");
	catalogo.setCondicionesAdicionales("A.IC_EPO=B.IC_EPO");
	catalogo.setCondicionesAdicionales("B.CS_FACTORAJE_DISTRIBUIDO='S'");
	catalogo.setOrden("A.CG_RAZON_SOCIAL");
	infoRegresar = catalogo.getJSONElementos();	

} else if("FACT_DIST.CATALOGO_IF".equals(informacion)) {

	String factDistIcEpo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setDistinc(true);
	catalogo.setCampoClave("A.IC_IF");
	catalogo.setCampoDescripcion("A.CG_RAZON_SOCIAL");
	catalogo.setOrden("A.CG_RAZON_SOCIAL");
	catalogo.setTabla("COMCAT_IF A, COMREL_IF_EPO B");
	catalogo.setCondicionesAdicionales("A.IC_IF=B.IC_IF");
	catalogo.setCondicionesAdicionales("A.CS_FACT_DISTRIBUIDO='S'");
	catalogo.setCondicionesAdicionales("B.CS_FACTORAJE_DISTRIBUIDO='S'");
	if(!"".equals(factDistIcEpo)) {
		catalogo.setCondicionesAdicionales("B.IC_EPO="+factDistIcEpo);
	}
	infoRegresar = catalogo.getJSONElementos();

} else if("FACT_DIST.CATALOGO_BENEF".equals(informacion)) {

	String factDistIcEpo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setDistinc(true);
	catalogo.setCampoClave("A.IC_IF");
	catalogo.setCampoDescripcion("A.CG_RAZON_SOCIAL");
	catalogo.setOrden("A.CG_RAZON_SOCIAL");
	catalogo.setTabla("COMCAT_IF A, COMREL_IF_EPO B");
	catalogo.setCondicionesAdicionales("A.IC_IF=B.IC_IF");
	catalogo.setCondicionesAdicionales("B.CS_FACTORAJE_DISTRIBUIDO='S'");
	catalogo.setCondicionesAdicionales("B.CS_BENEFICIARIO='S'");
	if(!"".equals(factDistIcEpo)) {
		catalogo.setCondicionesAdicionales("B.IC_EPO="+factDistIcEpo);
	}
	infoRegresar = catalogo.getJSONElementos();

} else if("FACT_DIST.CATALOGO_MONEDA".equals(informacion)) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setDistinc(true);
	catalogo.setCampoClave("A.IC_MONEDA");
	catalogo.setCampoDescripcion("A.CD_NOMBRE");
	catalogo.setTabla("COMCAT_MONEDA A");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	catalogo.setOrden("A.CD_NOMBRE");
	infoRegresar = catalogo.getJSONElementos();

} else if("FACT_DIST.CATALOGO_BANCO_SERVICIO".equals(informacion)) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_bancos_tef");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_bancos_tef");
	cat.setOrden("cd_descripcion");
	infoRegresar = cat.getJSONElementos();

} else if("FACT_DIST.CATALOGO_PLAZA".equals(informacion)) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_plaza");
	cat.setCampoClave("ic_plaza");
	cat.setCampoDescripcion("INITCAP(cd_descripcion||','||cg_estado)");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

} else if("FACT_DIST.NOMBRE_PYME".equals(informacion)) {

	String num_electronico = (request.getParameter("num_ne_pyme") == null)?"":request.getParameter("num_ne_pyme");
	String ic_pyme = "";
	String txtNombre = "";
	List datosPymes = paramDsctoEJB.datosPymes(num_electronico);
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("ic_pyme", ic_pyme);
	jsonObj.put("txtNombre", txtNombre);
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("FACT_DIST.BUSQUEDA_AVANZADA")) {

	String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setDistinc(true);
	catalogo.setCampoClave("N.IC_NAFIN_ELECTRONICO");
	catalogo.setCampoDescripcion("P.CG_RAZON_SOCIAL");
	catalogo.setTabla("COMREL_NAFIN N, COMCAT_PYME P");
	catalogo.setCondicionesAdicionales("N.IC_EPO_PYME_IF = P.IC_PYME AND P.CS_FACT_DISTRIBUIDO='S'");
	if(!"".equals(rfcPyme)) {
		catalogo.setCondicionesAdicionales("P.CG_RFC LIKE NVL (REPLACE (UPPER ('"+rfcPyme+"'), '*', '%'), '%')");
	}
	if(!"".equals(nombrePyme)) {
		catalogo.setCondicionesAdicionales("P.CG_RAZON_SOCIAL LIKE NVL (REPLACE (UPPER ('"+nombrePyme+"'), '*', '%'), '%')");
	}
	catalogo.setOrden("P.CG_RAZON_SOCIAL");
	infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("FACT_DIST.CONSULTAR") || informacion.equals("FACT_DIST.GENERAR_CSV") || informacion.equals("FACT_DIST.GENERAR_PDF")) {

	String factDistNumNePyme = request.getParameter("factDist_num_ne_pyme")==null?"":request.getParameter("factDist_num_ne_pyme");
	String factDistIcEpo     = request.getParameter("factDist_ic_epo")     ==null?"":request.getParameter("factDist_ic_epo");
	String factDistIcIf      = request.getParameter("factDist_if")         ==null?"":request.getParameter("factDist_if");
	String factDistBenef     = request.getParameter("factDist_benef")      ==null?"":request.getParameter("factDist_benef");
	String factDistMoneda    = request.getParameter("factDist_moneda")     ==null?"":request.getParameter("factDist_moneda");
	String operacion         = request.getParameter("operacion")           ==null?"":request.getParameter("operacion");
	//Estos son para generar el PDF
	String[] icConsecutivos  = request.getParameterValues("ic_consecutivos");
	String fecha             = request.getParameter("fecha")   ==null?"":request.getParameter("fecha");
	String usuario           = request.getParameter("usuario") ==null?"":request.getParameter("usuario");
	String total             = request.getParameter("total")   ==null?"":request.getParameter("total");

	int start = 0;
	int limit = 0;
	JSONObject resultado = new JSONObject();
	ConsultaFactDistribuido paginador = new ConsultaFactDistribuido();
	paginador.setIcNafinElectronico(Long.parseLong("".equals(factDistNumNePyme)?"0":factDistNumNePyme));
	paginador.setIcEpo(             Long.parseLong("".equals(factDistIcEpo)    ?"0":factDistIcEpo)    );
	paginador.setIcIf(              Long.parseLong("".equals(factDistIcIf)     ?"0":factDistIcIf)     );
	paginador.setIcBeneficiario(    Long.parseLong("".equals(factDistBenef)    ?"0":factDistBenef)    );
	paginador.setIcMoneda(          Long.parseLong("".equals(factDistMoneda)   ?"0":factDistMoneda)   );
	paginador.setIcConsecutivos(icConsecutivos);
	paginador.setFecha(fecha);
	paginador.setUsuario(usuario);
	paginador.setTotalReg(total);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if (informacion.equals("FACT_DIST.CONSULTAR")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try{
			if("GENERAR".equals(operacion)){
				queryHelper.executePKQuery(request);
			}
			String consulta = queryHelper.getJSONPageResultSet(request,start,limit);
			jsonObj = JSONObject.fromObject(consulta);
		}catch(Exception e){
			throw new AppException("Error en la paginación",e);
		}
	} else if (informacion.equals("FACT_DIST.GENERAR_CSV") || informacion.equals("FACT_DIST.GENERAR_PDF")) {
		String tipoArchivo = "PDF";
		try {
			if (informacion.equals("FACT_DIST.GENERAR_CSV")){
				tipoArchivo = "CSV";
			}
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo " + tipoArchivo, e);
		}
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("FACT_DIST.CONSULTA_CONTRATOS")) {

	String id = request.getParameter("id")==null?"":request.getParameter("id");
	String tabla = request.getParameter("tabla")==null?"":request.getParameter("tabla");
	infoRegresar = paramDsctoEJB.obtenerJSONContratosFactDistribuido(id, tabla);

} else if (informacion.equals("FACT_DIST.VALIDA_CTA_DUPLICADA")) {

	String icConsecutivos= request.getParameter("icConsecutivo") ==null?"" :request.getParameter("icConsecutivo");
	String icEpoStr      = request.getParameter("ic_epo")        ==null?"" :request.getParameter("ic_epo");
	String icPymeStr     = request.getParameter("ic_pyme")       ==null?"" :request.getParameter("ic_pyme");
	String icIfStr       = request.getParameter("ic_if")         ==null?"" :request.getParameter("ic_if");
	String monedaStr     = request.getParameter("moneda")        ==null?"" :request.getParameter("moneda");
	String benefStr      = request.getParameter("benef")         ==null?"" :request.getParameter("benef");

	//Para comparar en la tabla temporal, tengo que considerar tambien el ic_consecutivo ya que estos datos son propios de la pantalla y el usuario.
	List<String> ids = new ArrayList();
	if(!"".equals(icConsecutivos)){
		if(icConsecutivos.indexOf("A")==-1){
			icConsecutivos = icConsecutivos+"A";
		}
		String[] cadenaComoArray = icConsecutivos.split("A");
		for(int i=0; i<cadenaComoArray.length; i++){
			ids.add(cadenaComoArray[i]);
		}
	}

	//Convierto las variables a su tipo correcto
	Long icPyme = Long.parseLong(icPymeStr);
	Long icEpo  = Long.parseLong(icEpoStr);
	Long icIf   = Long.parseLong(icIfStr);
	Long benef  = Long.parseLong(benefStr);
	Long moneda = Long.parseLong(monedaStr);
	boolean existeCtaDuplicada = false;
	boolean existeCtaParamDescto = false;
	String mensaje = "OK";
	//Realizo las consultas para validar la cuenta nueva
	if(paramDsctoEJB.existeCuentaDuplicada(ids, icEpo, icPyme, icIf, moneda, benef, "")==true){//Valido si existe en la tabla final
		mensaje = "No es posible crear la nueva cuenta del beneficiario, existe una ya creada. Favor de verificar.";
	} else if(!ids.isEmpty() && paramDsctoEJB.existeCuentaDuplicada(ids, icEpo, icPyme, icIf, moneda, benef, "TMP")==true){//Valido si existe en la tabla temporal
		mensaje = "La cuenta que intenta agregar ya se encuentra en proceso de Registro. Favor de verificar.";
	} else if(paramDsctoEJB.existeCuentaParametrizada(icEpo, icPyme, icIf, moneda)==false){//Valido que esté parametrizada
		mensaje = "No se podrá registrar la cuenta, verifique si tiene cuenta parametrizada de producto Descuento.";
	} else {
		mensaje = "OK";
	}
	//Información a regresar
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", mensaje);
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("FACT_DIST.PRE_AGREGAR")) {

	String numNeStr         = request.getParameter("num_ne_pyme")   ==null?"" :request.getParameter("num_ne_pyme");
	String icPymeStr        = request.getParameter("ic_pyme")       ==null?"" :request.getParameter("ic_pyme");
	String icEpoStr         = request.getParameter("ic_epo")        ==null?"" :request.getParameter("ic_epo");
	String icIfStr          = request.getParameter("ic_if")         ==null?"" :request.getParameter("ic_if");
	String benefStr         = request.getParameter("benef")         ==null?"" :request.getParameter("benef");
	String monedaStr        = request.getParameter("moneda")        ==null?"" :request.getParameter("moneda");
	String bancoServicioStr = request.getParameter("bancoServicio") ==null?"0":request.getParameter("bancoServicio");
	String plazaStr         = request.getParameter("plaza")         ==null?"0":request.getParameter("plaza");
	String sucursalStr      = request.getParameter("sucursal")      ==null?"" :request.getParameter("sucursal");
	String cuentaStr        = request.getParameter("cuenta")        ==null?"" :request.getParameter("cuenta");
	String cuentaClabeStr   = request.getParameter("cuentaClabe")   ==null?"" :request.getParameter("cuentaClabe");
	String cuentaSpidStr    = request.getParameter("cuentaSpid")    ==null?"" :request.getParameter("cuentaSpid");
	String swiftStr         = request.getParameter("swift")         ==null?"" :request.getParameter("swift");
	String abaStr           = request.getParameter("aba")           ==null?"" :request.getParameter("aba");
	String icConsecutivos   = request.getParameter("ic_cons")       ==null?"" :request.getParameter("ic_cons");
	String[] cgContratos    = request.getParameterValues("cg_contratos");

	//Convierto las variables a su tipo correcto
	Long icPyme        = Long.parseLong(icPymeStr);
	Long icEpo         = Long.parseLong(icEpoStr);
	Long icIf          = Long.parseLong(icIfStr);
	Long benef         = Long.parseLong(benefStr);
	Long moneda        = Long.parseLong(monedaStr);
	Long bancoServicio = Long.parseLong(bancoServicioStr);
	Long plaza         = "".equals(plazaStr)?0:Long.parseLong(plazaStr);
	String sucursal    = sucursalStr.trim();
	String cuenta      = cuentaStr.trim();
	String cuentaClabe = cuentaClabeStr.trim();
	String cuentaSpid  = cuentaSpidStr.trim();
	String swift       = swiftStr.trim();
	String aba         = abaStr.trim();

	//Mando a insertar en la tabla temporal
	Long icConsecutivo = paramDsctoEJB.agregarCuentaTmp(numNeStr, icEpo, icIf, benef, moneda, bancoServicio, plaza, sucursal, cuenta, cuentaClabe, cuentaSpid, swift, aba, icPyme, cgContratos);

	//Obtengo la lista de ids que ya fueron agregados, y agrego el nuevo
	List<String> ids = new ArrayList();
	if(!"".equals(icConsecutivos)){
		if(icConsecutivos.indexOf("A")==-1){
			icConsecutivos = icConsecutivos+"A";
		}
		String[] cadenaComoArray = icConsecutivos.split("A");
		for(int i=0; i<cadenaComoArray.length; i++){
			ids.add(cadenaComoArray[i].trim());
		}
	}
	ids.add(""+icConsecutivo);

	//Consulto la tabla temporal para agregar al grid el nuevo registro
	infoRegresar = paramDsctoEJB.obtenerJSONCuentasAgregadasTmp(ids, icConsecutivo, "TMP", "");

} else if (informacion.equals("FACT_DIST.AGREGAR_CUENTAS")) {

	String icConsecutivos = request.getParameter("ic_cons") ==null?""  :request.getParameter("ic_cons");
	List<String> ids = new ArrayList();
	List<String> idsAgregados = new ArrayList();
	if(!"".equals(icConsecutivos)){
		String[] cadenaComoArray = icConsecutivos.split("A");
		for(String s: cadenaComoArray){
			if(!"".equals(s)){
				Long id = Long.parseLong(s);
				Long idAgregado = paramDsctoEJB.agregarCuenta(id,iNoUsuario);
				idsAgregados.add(""+idAgregado);
			}
		}
	}
	infoRegresar = paramDsctoEJB.obtenerJSONCuentasAgregadasTmp(idsAgregados, 0L,"",iNoUsuario + " - " + strNombreUsuario);

} else if (informacion.equals("FACT_DIST.ELIMINAR_CUENTAS_TMP")) {

	String icConsecutivos = request.getParameter("ic_cons") ==null?"" :request.getParameter("ic_cons");
	if("".equals(icConsecutivos)){
		jsonObj.put("mensaje", "El proceso ha sido cancelado.");
		jsonObj.put("success", new Boolean(true));
	} else {
		List<String> ids = new ArrayList();
		if(!"".equals(icConsecutivos)){
			if(icConsecutivos.indexOf("A")==-1){
				icConsecutivos = icConsecutivos+"A";
			}
			String[] cadenaComoArray = icConsecutivos.split("A");
			for(int i=0; i<cadenaComoArray.length; i++){
				ids.add(cadenaComoArray[i]);
			}
		}
		if(paramDsctoEJB.eliminarCuentasTemporales(ids)==true){
			jsonObj.put("mensaje", "El proceso ha sido cancelado.");
			jsonObj.put("success", new Boolean(true));
		}
	}
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("FACT_DIST.ELIMINAR_CUENTA")) {

	String icBenef       = request.getParameter("icBenef")       ==null?"0" :request.getParameter("icBenef");
	String icConsecutivo = request.getParameter("icConsecutivo") ==null?"0" :request.getParameter("icConsecutivo");
	if(paramDsctoEJB.eliminarCuenta(Long.parseLong(icConsecutivo),Long.parseLong(icBenef),iNoUsuario)==true){
		jsonObj.put("mensaje", "La cuenta ha sido borrada.");
		jsonObj.put("success", new Boolean(true));
	}
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("FACT_DIST.MODIFICAR")) {

	String numNeStr         = request.getParameter("num_ne_pyme")   ==null?"" :request.getParameter("num_ne_pyme");
	String icPymeStr        = request.getParameter("ic_pyme")       ==null?"" :request.getParameter("ic_pyme");
	String icEpoStr         = request.getParameter("ic_epo")        ==null?"" :request.getParameter("ic_epo");
	String icIfStr          = request.getParameter("ic_if")         ==null?"" :request.getParameter("ic_if");
	String benefStr         = request.getParameter("benef")         ==null?"" :request.getParameter("benef");
	String monedaStr        = request.getParameter("moneda")        ==null?"" :request.getParameter("moneda");
	String bancoServicioStr = request.getParameter("bancoServicio") ==null?"0":request.getParameter("bancoServicio");
	String plazaStr         = request.getParameter("plaza")         ==null?"0":request.getParameter("plaza");
	String sucursalStr      = request.getParameter("sucursal")      ==null?"" :request.getParameter("sucursal");
	String cuentaStr        = request.getParameter("cuenta")        ==null?"" :request.getParameter("cuenta");
	String cuentaClabeStr   = request.getParameter("cuentaClabe")   ==null?"" :request.getParameter("cuentaClabe");
	String cuentaSpidStr    = request.getParameter("cuentaSpid")    ==null?"" :request.getParameter("cuentaSpid");
	String swiftStr         = request.getParameter("swift")         ==null?"" :request.getParameter("swift");
	String abaStr           = request.getParameter("aba")           ==null?"" :request.getParameter("aba");
	String icConsecutivoStr = request.getParameter("icConsecutivo") ==null?"" :request.getParameter("icConsecutivo");
	String[] cgContratos    = request.getParameterValues("cg_contratos");

	//Convierto las variables a su tipo correcto
	Long icConsecutivo = Long.parseLong(icConsecutivoStr);
	Long icPyme        = Long.parseLong(icPymeStr);
	Long icEpo         = Long.parseLong(icEpoStr);
	Long icIf          = Long.parseLong(icIfStr);
	Long benef         = Long.parseLong(benefStr);
	Long moneda        = Long.parseLong(monedaStr);
	Long bancoServicio = Long.parseLong(bancoServicioStr);
	Long plaza         = "".equals(plazaStr)?0:Long.parseLong(plazaStr);
	String sucursal    = sucursalStr.trim();
	String cuenta      = cuentaStr.trim();
	String cuentaClabe = cuentaClabeStr.trim();
	String cuentaSpid  = cuentaSpidStr.trim();
	String swift       = swiftStr.trim();
	String aba         = abaStr.trim();

	boolean registrosModificados = paramDsctoEJB.modificarCuenta(icConsecutivo, numNeStr, icEpo, icIf, icPyme, benef, moneda, bancoServicio, plaza, sucursal, cuenta, cuentaClabe, cuentaSpid, swift, aba, iNoUsuario, cgContratos);
	if(registrosModificados==true){
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("mensaje", "La cuenta ha sido actualizada.");
	}
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("FACT_DIST.CONSULTAR_ACUSE")) {

	String fecha   = request.getParameter("fecha")   ==null?"" :request.getParameter("fecha");
	String usuario = request.getParameter("usuario") ==null?"" :request.getParameter("usuario");
	String total   = request.getParameter("total")   ==null?"" :request.getParameter("total");

	StringBuffer cadenaJSONparametros = new StringBuffer();
	HashMap parametro = new HashMap();
	JSONArray jsonArr = new JSONArray();

	parametro = new HashMap();
	parametro.put("ID", "A");
	parametro.put("DESCRIPCION", "Fecha y Hora:" );
	parametro.put("DATO", fecha);
	jsonArr.add(JSONObject.fromObject(parametro));
	parametro = new HashMap();
	parametro.put("ID", "B");
	parametro.put("DESCRIPCION", "Usuario:" );
	parametro.put("DATO", usuario);
	jsonArr.add(JSONObject.fromObject(parametro));
	parametro = new HashMap();
	parametro.put("ID", "C");
	parametro.put("DESCRIPCION", "Total de registros:" );
	parametro.put("DATO", total);
	jsonArr.add(JSONObject.fromObject(parametro));

	cadenaJSONparametros.append("({\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString() + "})");
	System.out.println(cadenaJSONparametros.toString());
	infoRegresar = cadenaJSONparametros.toString();

}
//----- FODEA 016_2018 TERMINA -----//

%>
<%=infoRegresar%>