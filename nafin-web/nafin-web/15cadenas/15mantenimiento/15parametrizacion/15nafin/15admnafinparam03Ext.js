Ext.onReady(function() {

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//----------------------- VALIDACIONES --------------------------

    Ext.apply(Ext.form.VTypes, {
         // This function validates digito verificador
        digVerificador:  function(contenido) {

				var car = "";
				var resultado = "";
				var residuo = 0;    
				var mulIndividual = 0;
				var digVerificador = 0;  
				var contenedor = 0;
				var cadena="";
				cadena=contenido;

				if (cadena != ""){
			
					digVerificador = parseInt(cadena.substr(17));
					
					for (i=0; i <= 16; i++){
						car = cadena.substring(i,i+1);
						switch(i){
							case 0: case 3: case 6: case 9: case 12: case 15:			/*3*/
								mulIndividual = parseInt(car)*3;
								contenedor += mulIndividual;
								break;
							case 1: case 4: case 7: case 10: case 13: case 16:			/*7*/
								mulIndividual = parseInt(car)*7;
								contenedor += mulIndividual;
								break;
							case 2: case 5: case 8: case 11: case 14:					/*1*/
								mulIndividual = parseInt(car)*1;
								contenedor += mulIndividual;
								break;
						}
					}
				
					residuo = contenedor % 10;  /*Obtiene el residuo */
					
					if (residuo > 0)  
						contenedor = 10 - residuo;
					else
						contenedor = 0;
					
					if (digVerificador != contenedor){
						return false;
					}
				}
				return true;
        },
		  digVerificadorText:	'Cuenta clabe incorrecta, favor de verificar.'
    });

//-  - - - - - - - - - - - Variable global

	var varGlobal = {cvePerf:null}

//- - - - - - - - - - - - FUNCTIONS�S- - - - - - - - - - - - - - - - - - - - - -

	function submitCuenta(){

	/*	if(	Ext.isEmpty(	Ext.getCmp("_noBancoFondeo").getValue()	)	){
			Ext.getCmp("_noBancoFondeo").markInvalid('Favor de seleccionar un Banco de Fondeo');
			Ext.getCmp("_noBancoFondeo").focus();
			return;
		}*/
		if(	Ext.isEmpty(	Ext.getCmp("_ic_epo").getValue()	)	){
			Ext.getCmp("_ic_epo").markInvalid('Favor de seleccionar una EPO');
			Ext.getCmp("_ic_epo").focus();
			return;
		}
		if(	Ext.isEmpty(	Ext.getCmp("_ic_if").getValue()	)	)	{
			Ext.getCmp("_ic_if").markInvalid('Favor de seleccionar un Intermediario Financiero');
			Ext.getCmp("_ic_if").focus();
			return;
		}
		var  operaFideicomisoIF = Ext.getCmp("_operaFideicomisoIF").getValue(); 
		
		if(operaFideicomisoIF=='N')  {		
			if(	Ext.isEmpty(	Ext.getCmp("_txtBanco").getValue()	)	)	{
				Ext.getCmp("_txtBanco").markInvalid('Favor de proporcionar un banco para operar en la cadena');
				Ext.getCmp("_txtBanco").focus();
				return;
			}
			if(	Ext.isEmpty(	Ext.getCmp("_txtSucursal").getValue()	)	){
				Ext.getCmp("_txtSucursal").markInvalid('Favor de proporcionar la sucursal del banco para operar en la cadena');
				Ext.getCmp("_txtSucursal").focus();
				return;
			}
			if(	Ext.isEmpty(	Ext.getCmp("_txtCuenta").getValue()	)	)	{
				Ext.getCmp("_txtCuenta").markInvalid('Favor de proporcionar el n�mero de cuenta para operar en la cadena');
				Ext.getCmp("_txtCuenta").focus();
				return;
			}	
			
			accionArealizar("BOTON_AGREGAR",null);
			
		}else  if(operaFideicomisoIF=='S')  {	
			Ext.Ajax.request({
				url: '15admnafinparam03Ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'OperaFideicomiso', 
					tipoFide:'EPO'
				}),
				callback: procesarOperaFideicomiso
			});	
		
		}		
		
	}

	function submitCuentaActualiza(){


		if(	Ext.isEmpty(	Ext.getCmp("_txtSucursal").getValue()	)	){
			Ext.getCmp("_txtSucursal").markInvalid('Favor de escribir la sucursal');
			Ext.getCmp("_txtSucursal").focus();
			return;
		}
		if(	Ext.isEmpty(	Ext.getCmp("_txtCuenta").getValue()	)	)	{
			Ext.getCmp("_txtCuenta").markInvalid('Favor de escribir el No. de Cuenta');
			Ext.getCmp("_txtCuenta").focus();
			return;
		}
		if(	!Ext.isEmpty(	Ext.getCmp("_txtBanco").getValue()	)	)	{

			Ext.Msg.confirm("Mensaje de p�gina web.","�Est� seguro de que desea modificar el Banco?",
				function(res){
					if(res=='no' || res == 'NO'){
						accionArealizar("FIN",null);
					}else{

						new Ext.Window({
							modal: true,
							resizable: false,
							x: 100, y:75,
							width: 300,
							height: 150,
							layout:'fit',
							//autoScroll:true,
							id: 'winClave',
							closeAction: 'destroy',
							title: 'Confirmaci�n de clave',
							items: [
								{
									xtype:	'form',
									//layout:	'form',
									id:		'formaClave',
									title:	undefined,
									style:	'margin:0 auto;',
									collapsible:false,
									titleCollapse:	false,
									frame:	true,
									labelWidth:	120,
									//bodyStyle:	'padding-left:20px;,padding-top:10px;,text-align:left',
									defaults:	{	msgTarget:	'side',	anchor:	'-20'	},
									monitorValid:	true,
									items:		[
										{
											xtype:	'textfield',
											fieldLabel:	'Clave de usuario',
											id:		'_cgLogin',
											name:	'cgLogin',
											allowBlank:	false,
											maxLength:	8
										},{
											xtype:	'textfield',
											fieldLabel:	'Contrase�a',
											id:		'_cg_password',
											name:	'cg_password',
											inputType: 'password',
											blankText: 'Digite su contrase�a',
											allowBlank:	false,
											maxLength:	8
										}
									],
									buttons: [
										{
											xtype: 'button',
											text: 'Aceptar',
											iconCls:'icoAceptar',
											id: 'btnAceptar',
											formBind:	true,
											handler:function(){
			
														if(!Ext.getCmp('formaClave').getForm().isValid()){
															return;
														}
														var respuesta = new Object();
														respuesta["cgLogin"] = Ext.getCmp('_cgLogin').getValue();
														respuesta["cg_password"] = Ext.getCmp('_cg_password').getValue();
														accionArealizar("VALIDA_CLAVE_USUARIO",respuesta);
													}
										},
										{
											xtype: 'button',
											text: 'Cerrar',
											iconCls:'icoRechazar',
											id: 'btnCancelar',
											handler:function(){
														accionArealizar("FIN",null);
													}
										}
									]
								}
							]
						}).show();
					}
			});
		}else{
			accionArealizar("BOTON_ACTUALIZAR",null);
		}
	}

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -

	var procesarConsulta = function(store, registros, opts){

		var forma = Ext.getCmp('forma');
		forma.el.unmask();

		if (registros != null) {

			var grid  = Ext.getCmp('grid');

			if (!grid.isVisible()) {
				grid.show();
			}

			var cm = grid.getColumnModel();

			if(varGlobal.cvePerf == "8"){
				cm.setHidden(cm.findColumnIndex(''), true);
			}

			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: '' 
				}
			);
		}
	}

	var procesaCatalogoFondeo = function(store, arrRegistros, opts) {
		if(	varGlobal.cvePerf == "8"	){
			Ext.getCmp('_noBancoFondeo').setValue("2");
		}else{
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'Seleccionar Todos',	loadMsg: null	})	);
			Ext.getCmp('_noBancoFondeo').setValue("");
		}
	}

	var procesaCatalogoEpo = function(store, arrRegistros, opts) {
		var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
		store.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un EPO',	loadMsg: null	})	);
		Ext.getCmp('_ic_epo').setValue("");
	}

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 

	var catalogoFondeoData = new Ext.data.JsonStore({
		id:				'catalogoFondeoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admnafinparam03Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.BancoFondeo'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			load:			procesaCatalogoFondeo,
			exception:	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEpoData = new Ext.data.JsonStore({
		id:				'catalogoEpoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admnafinparam03Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.Epo'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			load:	procesaCatalogoEpo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoIfData = new Ext.data.JsonStore({
		id:				'catalogoIfDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg', 'ic_financiera'],
		url:				'15admnafinparam03Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.IF'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception:	NE.util.mostrarDataProxyError,
			beforeload:	NE.util.initMensajeCargaCombo
		}
	});

	var catalogoBancoData = new Ext.data.JsonStore({
		id:				'catalogoBancoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admnafinparam03Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.Banco.Servicio'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoPlazaData = new Ext.data.JsonStore({
		id:				'catalogoPlazaDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admnafinparam03Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.Plaza'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var registrosData = new Ext.data.JsonStore({
		root: 		'registros',
		id:			'registrosDataStore',
		url: 			'15admnafinparam03Ext.data.jsp',
		baseParams: {	informacion:	'.Consulta'	},
		fields: [
			{ name: 'CS_TIPO'},
			{ name: 'NOMIF'},
			{ name: 'NOMBANCO'},
			{ name: 'NOCTA'},
			{ name: 'ACEPTACION'},
			{ name: 'PLAZA'},
			{ name: 'SUCURSAL'},
			{ name: 'NOMEPO'},
			{ name: 'IC_EPO'},
			{ name: 'IC_IF'},
			{ name: 'CLABE'},
			{ name: 'AUTORIZA'},
			{ name: 'OPERA_FIDEICOMISO_IF'} //Fodea 017-2013
			
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarConsulta,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsulta(null, null, null);
				}
			}
		}
	});

	var procesarOperaFideicomiso = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {		
			var  jsonData = Ext.util.JSON.decode(response.responseText);
					
			if(jsonData.tipoFide=='IF')  {
			
				Ext.getCmp('_operaFideicomisoIF').setValue(jsonData.operaFideicomisoIF);
				if(jsonData.operaFideicomisoIF=='S')  {
					Ext.getCmp('_txtBanco').hide();
					Ext.getCmp('_txtSucursal').hide();
					Ext.getCmp('_txtCuenta').hide();
					Ext.getCmp('_cboPlaza').hide();
					Ext.getCmp('_txtCuentaClabe').hide();
				}else  {
					Ext.getCmp('_txtBanco').show();
					Ext.getCmp('_txtSucursal').show();
					Ext.getCmp('_txtCuenta').show();
					Ext.getCmp('_cboPlaza').show();
					Ext.getCmp('_txtCuentaClabe').show();
				}
				
			}else  if(jsonData.tipoFide=='EPO')  {
			
				if(jsonData.bOperaFideicomisoEPO=='N')  {
					Ext.Msg.alert(	'Mensaje',"�La EPO seleccionada no Opera Fideicomiso para Desarrollo de Proveedores, no se puede crear la cuenta con un Fideicomiso�");				
				
				}else  if(jsonData.bOperaFideicomisoEPO=='S')  {
					accionArealizar("BOTON_AGREGAR",null);
				}
			}		
			
			
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}
//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - - 

	var procesaAccion = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionArealizar(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionArealizar(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionArealizar = function(estadoSiguiente, respuesta){

		if(  estadoSiguiente == "parametrizacion.getClavePerfil"						){

			Ext.getCmp('forma').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url: '15admnafinparam03Ext.data.jsp',
				params:	{	informacion:	'getClavePerfil'},
				callback: procesaAccion
			});
		
		} else if(	estadoSiguiente == "RESPUESTA_CLAVE_PERFIL"						){

			varGlobal.cvePerf = respuesta.cvePerf;
			if(varGlobal.cvePerf == "8"){

				Ext.getCmp('desc.btnAgregar').hide();
				Ext.getCmp('desc.btnLimpiar').hide();

			}

			Ext.StoreMgr.key('catalogoFondeoDataStore').load({
				params: {
					cvePerf:	varGlobal.cvePerf
				}
			});
			Ext.StoreMgr.key('catalogoEpoDataStore').load({
				params: {
					cvePerf:			varGlobal.cvePerf,
					bancoFondeo:	(varGlobal.cvePerf == "8")?"2":null
				}
			});

		} else if(				estadoSiguiente == "BOTON_AGREGAR"						){

			Ext.getCmp('forma').el.mask('Enviando...', 'x-mask-loading');
		
			Ext.Ajax.request({
				url:		'15admnafinparam03Ext.data.jsp',
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion:	'.Agregar',
								txtBanco:Ext.getCmp('_txtBanco').getValue()
							}
				),
				callback: procesaAccion
			});

		} else if(						estadoSiguiente == "RESPUESTA_AGREGAR"			){

			accionArealizar("FIN",null);

		} else if(				estadoSiguiente == "BOTON_CONSULTAR"					){

			grid.hide();
			Ext.getCmp('forma').el.mask('Enviando...', 'x-mask-loading');

			Ext.StoreMgr.key('registrosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								operacion: 'Generar',
								start: 0,
								limit: 15
							}
				)
			});

		} else if(	estadoSiguiente == "BOTON_CANCELAR"					){

			Ext.getCmp('forma').getForm().reset();
			grid.hide();

		} else if(	estadoSiguiente == "BOTON_MODIFICAR"					){

			Ext.getCmp('forma').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url:		'15admnafinparam03Ext.data.jsp',
				params:	{	informacion:	'.Modificar',
								claveIf:		respuesta.claveIf,
								claveEpo:	respuesta.claveEpo
				},
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA_MODIFICAR"				){


			//LLenamos el formulario con la respuesta de los datos a modificar...
			Ext.getCmp('_razonSocialEpo').setValue(respuesta.RazonSocialEPO);
			Ext.getCmp('_razonSocialIf').setValue(respuesta.RazonSocialIF);
			Ext.getCmp('_bco_anterior').setValue(respuesta.cgBanco);
			Ext.getCmp('_ic_if').setValue(respuesta.claveIf);
			Ext.getCmp('_ic_epo').setValue(respuesta.claveEpo);
			Ext.getCmp('_razonSocialEpo').show();
			Ext.getCmp('_razonSocialIf').show();
			Ext.getCmp('_bco_anterior').show();
			Ext.getCmp('_noBancoFondeo').hide();
			Ext.getCmp('_ic_if').hide();
			Ext.getCmp('_ic_epo').hide();
			Ext.getCmp('_txtBanco').setValue("");
			Ext.getCmp('_txtBanco').label.update("Banco Nuevo");
			Ext.getCmp('_txtSucursal').setValue(respuesta.cgSucursal);
			Ext.getCmp('_txtCuenta').setValue(respuesta.cgNumeroCuenta);
			Ext.getCmp('_cboPlaza').setValue(respuesta.icPlaza);
			Ext.getCmp('_txtPlazaAnt').setValue(respuesta.icPlaza);
			Ext.getCmp('_txtCuentaClabe').setValue(respuesta.cgCuentaClabe);
			Ext.getCmp('_VoBoNafin').setValue(respuesta.VoBoNafin);

			if(grid.isVisible()){
				grid.hide();
			}

			Ext.getCmp('desc.btnAgregar').hide();
			Ext.getCmp('desc.btnLimpiar').hide();
			Ext.getCmp('desc.btnConsultar').hide();
			Ext.getCmp('desc.btnRegresar').show();
			Ext.getCmp('desc.btnActualizar').show();

		} else if(	estadoSiguiente == "VALIDA_CLAVE_USUARIO"							){

			contenedorPrincipal.el.mask('Enviando...', 'x-mask-loading');
			
			Ext.getCmp('winClave').hide();

			Ext.Ajax.request({
				url:		'15admnafinparam03Ext.data.jsp',
				params:		{
								informacion:	'.validaClaveUser',
								cgLogin:		respuesta.cgLogin,
								cg_password:		respuesta.cg_password
							},
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA_VALIDACION"							){

			Ext.getCmp('winClave').destroy();
			contenedorPrincipal.el.unmask();

			if(	Ext.isEmpty(respuesta.strMensaje)	){
				accionArealizar("BOTON_ACTUALIZAR",null);
			}else{
				Ext.Msg.alert("Mensaje de p�gina web", respuesta.strMensaje,
					function(){
						accionArealizar("FIN",null);
					}
				);
			}

		} else if(	estadoSiguiente == "BOTON_ACTUALIZAR"								){

			Ext.getCmp('forma').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url:		'15admnafinparam03Ext.data.jsp',
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion:	'.Actualizar'
							}
				),
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA_ACTUALIZAR"			){

			Ext.Msg.alert("Mensaje de p�gina web", respuesta.strMensaje,
				function(){
					accionArealizar("FIN",respuesta);
				}
			);

		} else if(	estadoSiguiente == "FIN"												){

			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "15admnafinparam03Ext.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}

//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -
	var renderColumna =	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									if(	record.get("ACEPTACION") == "N"	&& record.get("AUTORIZA") == "N"	){
										value ="<font color=red>"+value+"</font>";
									}
									return value;
								}

	var grid = new Ext.grid.GridPanel({
		store: 		registrosData,
		id:			'grid',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		margins:		'20 0 0 0',
		title:		undefined,
		//view:			new Ext.grid.GridView({forceFit:	true}),
		stripeRows: true,
		loadMask: 	true,
		height: 		400,
		width: 		930,
		frame: 		true,
		columns: [
			{
				header: 		'EPO',
				tooltip: 	'EPO',
				dataIndex: 	'NOMEPO',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				fixed:		true,
				renderer:	renderColumna
			},{
				header: 		'Tipo IF',
				tooltip: 	'Tipo IF',
				dataIndex: 	'CS_TIPO',
				sortable: 	true,
				resizable: 	true,
				width: 		50,
				align:		'center',
				hidden: 		false,
				hideable:	false,
				renderer:	renderColumna
			},{
				header: 		'Nombre IF',
				tooltip: 	'Nombre IF',
				dataIndex: 	'NOMIF',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				renderer:	renderColumna
			},{
				header: 		'Banco',
				tooltip: 	'Banco',
				dataIndex: 	'NOMBANCO',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				renderer:	renderColumna
         },{
				header: 		'Sucursal',
				tooltip: 	'Sucursal',
				dataIndex: 	'SUCURSAL',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				hideable:	false,
				renderer:	renderColumna
			},{
				header: 		'Datos Cuenta',
				tooltip: 	'Datos Cuenta',
				dataIndex: 	'NOCTA',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				hideable:	false,
				renderer:	renderColumna
			},{
				header: 		'Cuenta CLABE',
				tooltip: 	'Cuenta CLABE',
				dataIndex: 	'CLABE',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				hideable:	false,
				renderer:	renderColumna
         },{
				header: 		'Plaza',
				tooltip: 	'Plaza',
				dataIndex: 	'PLAZA',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	renderColumna
         },{
				header: 		'Autorizaci�n',
				tooltip: 	'Autorizaci�n',
				dataIndex: 	'ACEPTACION',
				sortable: 	true,
				resizable: 	true,
				width: 		70,
				align:		'center',
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	renderColumna
         },{
				xtype:	'actioncolumn',
				header:	'Seleccionar',
				dataIndex:'',
				menuDisabled:true,
				hidden:	false,
				hideable:false,
				dataIndex: '',	align: 'center',	sortable: true,	resizable: true,	width: 100,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('OPERA_FIDEICOMISO_IF') =='N'){ 
								this.items[0].tooltip = 'Modificar';
								return 'modificar';
							}
						},
						handler:	function(grid, rowIndex, colIndex, item, event)	{
										var record = registrosData.getAt(rowIndex);
										var respuesta				=	new Object();
										respuesta["claveIf"]	=	record.json['IC_IF'];
										respuesta["claveEpo"]=	record.json['IC_EPO'];
										accionArealizar("BOTON_MODIFICAR",respuesta);
									}
					}
				]
			}
		],
		bbar: {
			xtype:		'paging',
			pageSize:	15,
			buttonAlign:'left',
			id:			'barraPaginacion',
			displayInfo:true,
			store:		registrosData,
			displayMsg:	'{0} - {1} de {2}',
			emptyMsg:	"No hay registros."
		}
	});

	var elementosForma = [
		{
			xtype:			'combo',
			id:				'_noBancoFondeo',
			name:				'noBancoFondeo',
			hiddenName:		'noBancoFondeo',
			fieldLabel:		'Banco de Fondeo',
			emptyText:		'Seleccionar Banco de Fondeo...',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		true,
			hidden: true,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoFondeoData,
			tpl:				NE.util.templateMensajeCargaCombo,
			listeners:	{
				'select':function(cbo){
									var cboEpo = Ext.getCmp('_ic_epo');
									cboEpo.setValue('');
									cboEpo.store.removeAll();
									cboEpo.store.load({
										params: {
											cvePerf:			varGlobal.cvePerf,
											bancoFondeo:	cbo.getValue()
										}
									});
							}
			}
		},{
			xtype:			'displayfield',
			name:				'razonSocialEpo',
			id:				'_razonSocialEpo',
			fieldLabel:		'EPO',
			value:			null,
			hidden:			true
		},{
			xtype:			'combo',
			id:				'_ic_epo',
			name:				'ic_epo',
			hiddenName:		'ic_epo',
			fieldLabel:		'Nombre de la EPO',
			emptyText:		'Seleccione una EPO',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		true,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoEpoData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'displayfield',
			name:				'razonSocialIf',
			id:				'_razonSocialIf',
			fieldLabel:		'IF',
			value:			null,
			hidden:			true
		},{
			xtype:			'combo',
			id:				'_ic_if',
			name:				'ic_if',
			hiddenName:		'ic_if',
			fieldLabel:		'Intermediario Financiero',
			emptyText:		'Seleccione IF',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		true,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoIfData,
			tpl:				NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {									
						Ext.Ajax.request({
							url: '15admnafinparam03Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'OperaFideicomiso',
								tipoFide:'IF',
								ic_if:combo.getValue()
							}),
							callback: procesarOperaFideicomiso
						});			
					}
				}
			}
		},{
			xtype:			'displayfield',
			fieldLabel:		'Banco Anterior',
			name:				'bco_anterior',
			id:				'_bco_anterior',
			value:			null,
			hidden:			true
		},{
			xtype:			'combo',
			id:				'_txtBanco',
			name:				'txtBanco',
			hiddenName:		'txtBanco',
			fieldLabel:		'Banco',
			emptyText:		'Seleccione un Banco',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		true,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoBancoData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'textfield',
			fieldLabel:		'Sucursal',
			name:				'txtSucursal',
			id:				'_txtSucursal',
			maskRe:			/[0-9]/,
			allowBlank:		true,
			maxLength:		40
		},{
			xtype:			'textfield',
			fieldLabel:		'No. de Cuenta',
			name:				'txtCuenta',
			id:				'_txtCuenta',
			allowBlank:		true,
			maxLength:		15,
			anchor:			'50%'
		},{
			xtype:			'combo',
			id:				'_cboPlaza',
			name:				'cboPlaza',
			hiddenName:		'cboPlaza',
			fieldLabel:		'Plaza',
			emptyText:		'Seleccione una plaza',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			triggerAction:	'all',
			typeAhead:		true,
			minChars:		1,
			store:			catalogoPlazaData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'textfield',
			fieldLabel:		'Cuenta CLABE',
			name:			'txtCuentaClabe',
			id:				'_txtCuentaClabe',
			allowBlank:		true,
			maxLength:		18,
			anchor:			'50%',
			vtype:			'digVerificador'
		},{
			xtype:			'hidden',
			name:			'txtPlazaAnt',
			id:				'_txtPlazaAnt',
			value:			null
		},{
			xtype:			'hidden',
			name:			'VoBoNafin',
			id:				'_VoBoNafin',
			value:			null
		},
		{ 	xtype: 'textfield', 	 	hidden: true,  id: '_operaFideicomisoIF', 	value: '' }
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		title:			undefined,
		width:			800,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			true,
		labelWidth:		160,
		bodyStyle:		'padding-left:20px;,padding-top:10px;,text-align:left',
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			[elementosForma],
		buttons: [
			{
				text:		'Agregar',
				id:		'desc.btnAgregar',
				iconCls:	'aceptar',
				width:	100,
				handler: function(boton, evento) {

								if(!Ext.getCmp('forma').getForm().isValid()){
									return;
								}
								submitCuenta();

				} //fin handler
			},
			{
				text:		'Consultar',
				id:		'desc.btnConsultar',
				iconCls:	'icoBuscar',
				width:	100,
				handler: function(boton, evento) {

								if(!Ext.getCmp('forma').getForm().isValid()){
									return;
								}
								accionArealizar("BOTON_CONSULTAR",null);
				} //fin handler
			},{
				text:		'Limpiar',
				id:		'desc.btnLimpiar',
				iconCls:	'icoLimpiar',
				width:	100,
				handler: function(boton, evento) {
								accionArealizar("BOTON_CANCELAR",null);
				} //fin handler
			},{
				text:		'Regresar',
				id:		'desc.btnRegresar',
				iconCls:	'icoLimpiar',
				width:	100,
				hidden:	true,
				handler: function(boton, evento) {
								accionArealizar("FIN",null);
				} //fin handler
			},{
				text:		'Actualizar',
				id:		'desc.btnActualizar',
				iconCls:	'icoAceptar',
				width:	100,
				hidden:	true,
				handler: function(boton, evento) {

								if(	!(Ext.getCmp('forma').getForm().isValid())	){
									return;
								}
								submitCuentaActualiza();
				} //fin handler
			}
		]
	});

//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		renderHidden:	true,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10)
		],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if(Ext.getCmp("forma").isVisible()){
				if( element.isMasked()){
					element.unmask();
				}
			}

			// Derefenrencia ultimo elemento...
			element = null;

		}

	});

	Ext.StoreMgr.key('catalogoIfDataStore').load();
	Ext.StoreMgr.key('catalogoBancoDataStore').load();
	Ext.StoreMgr.key('catalogoPlazaDataStore').load();
	accionArealizar("parametrizacion.getClavePerfil",null);

});