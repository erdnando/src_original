Ext.onReady(function() {

	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);
	
//- - - - - - -  - - - - - HANDLER�S - - - - - - - - - - - - - - - - - - - - - -

	var procesarCatIntermediariosData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var claveIF = Ext.getCmp('_cmb_if');
			if(claveIF.getValue()==''){
				
				var newRecord = new recordType({
					clave:'',
					descripcion:'Todos'
				});
				
				store.insert(0,newRecord);
				store.commitChanges();
				claveIF.setValue('');

			}
		}
  }
  
  var procesarCatMonedaData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var mon = Ext.getCmp('_cmb_mon');
			if(mon.getValue()==''){
				
				var newRecord = new recordType({
					clave:'',
					descripcion:'Seleccione...'
				});
				
				store.insert(0,newRecord);
				store.commitChanges();
				mon.setValue('');

			}
		}
  }
  
  //GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	function procesarSuccessFailureGenerarPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
    
  //Funcion para validar la peticion Ajax y mostrar el usuario y fecha
	function procesaValoresIniciales(opts, success, response){
		fusr.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if(jsonValoresIniciales != null){	
				var strNombreUsuario = Ext.getCmp('strNombreUsuario');
				var fechaHora = Ext.getCmp('fechaHora');
				var hora = Ext.getCmp('hora');
				strNombreUsuario.getEl().update('Usuario: '+jsonValoresIniciales.strNombreUsuario);
				fechaHora.getEl().update('Fecha: '+jsonValoresIniciales.fechaHora);	
				hora.getEl().update('Hora:' +jsonValoresIniciales.hora);
				fusr.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}
	}

	var procesarConsultaRegistros = function(total, registros){
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		var btnAceptar = Ext.getCmp('btnAceptar');
		if (registros != null) {
			var grid  = Ext.getCmp('gridConsulta');

			if (!grid.isVisible()) {
				grid.show();
			}

			var el = grid.getGridEl();
			if(registrosConsultadosData.getTotalCount() > 0) {
				el.unmask();
				btnAceptar.enable();
			} else {
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
				//grid.hide();
				btnAceptar.disable();
			}
		}
	}
	
	var procesarConsultaAceptados = function(total, registros){
		if(registros != null){
			var forma = Ext.getCmp("forma");
			forma.hide();
			Ext.getCmp('gridConsulta').hide();
			gri.show();
			gridFec.show();
			var el = gri.getGridEl();
			var grid  = Ext.getCmp('gridConsulta');
			grid.el.unmask();
			
		}
	}
	
	var procesarConsultaHistoricos = function(total, registros){
		var gr = Ext.getCmp('gridHistoricos');
		var el = gr.getGridEl();
		gr.el.unmask();
		if (registros != null) {
			if (!gr.isVisible()) {
				gr.show();
			}
			
			if(historicosData.getTotalCount() > 0) {
				el.unmask();
			}else{
				el.mask('No existen registros que mostrar', 'x-mask');
			}
			
		}
	}
	
	//Lanza la ventana emergente de datos historicos
	var verHistoricos = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var cveIF = registro.get('CVEIF');
		Ext.getCmp('winHistoricos').show();		//Se muestra la pantalla emergente
		var gr = Ext.getCmp("gridHistoricos");
		gr.hide();
		//gr.el.mask('Buscando...','x-mask-loading'); 
		Ext.StoreMgr.key('historicosDataStore').load({
				params:
							{
								informacion: 'Ver',
								cveIF:	cveIF
							}
				});
		
	}

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - -

	var catalogoIntermediariosData = new Ext.data.JsonStore({
		id:				'catalogoIntermediariosDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admparamfaculExt.data.jsp',
		baseParams:		{	informacion: 'catalogoIntermediarios'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: procesarCatIntermediariosData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMonedasData = new Ext.data.JsonStore({
		id:				'catalogoMonedasDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admparamfaculExt.data.jsp',
		baseParams:		{	informacion: 'catalogoMonedas'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: procesarCatMonedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'15admparamfaculExt.data.jsp',
			baseParams: {	informacion:	'ConsultaRegistros'	},
			fields: [
				{ name: 'CVEIF'},
				{ name: 'NOMBREIF' },
				{ name: 'FACULTAD', convert: NE.util.string2boolean},
				{ name: 'MONEDA'},
				{ name: 'MONTOANT'},
				{ name: 'MONTO'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
						
					}
				},
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null);
					}
				}
			}
	});
	
	//Este store es para los datos que seran cargados en el grid despues de clickear el boton Aceptar
		var registrosAceptadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosAceptadosDataStore',
			url: 			'15admparamfaculExt.data.jsp',
			baseParams: {	informacion:	'ConsultaRegistros'	},
			fields: [
				{ name: 'CVEIFI'},
				{ name: 'NOMBREIFI' },
				{ name: 'MONEDAI'},
				{ name: 'MONANTI'},
				{ name: 'MONACT'},
				{ name: 'FACULTADI'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
						
					}
				},
				load: 	procesarConsultaAceptados,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaAceptados(null, null);
					}
				}
			}
	});
	
	//Este store es para los datos que seran cargados en el grid de la ventana emergente de historicos
		var historicosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'historicosDataStore',
			url: 			'15admparamfaculExt.data.jsp',
			baseParams: {	informacion:	'ConsultaRegistros'	},
			fields: [
				{ name: 'NOMIF'},
				{ name: 'FECMOD' },
				{ name: 'MONEDAB' },
				{ name: 'MONANT'},
				{ name: 'MONMOD'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
						
					}
				},
				load: 	procesarConsultaHistoricos,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaHistoricos(null, null);
					}
				}
			}
	});
	
	//Este store es para los datos de fecha y usuario
		var usuarioData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'usuarioDataStore',
			url: 			'15admparamfaculExt.data.jsp',
			baseParams: {	informacion:	'valoresIniciales'	},
			fields: [
				{ name: 'DATOS'},
				{ name: 'VALORES' }
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
						
					}
				},
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					}
				}
			}
	});
	
//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - -

	var procesaConsulta = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){
	
		 if(  estadoSiguiente == "CONSULTAR" ){
			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');
			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("gridConsulta").hide();
			Ext.StoreMgr.key('registrosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Consultar'
							}
				)
			});
			
		}else if(	estadoSiguiente == "ACEPTAR"){
			Ext.StoreMgr.key('usuarioDataStore').load({
					params:	
								{
									informacion: 'valoresIniciales'
								}
			});
			var f = new Date();
			var d = f.getDate();
			var m = (f.getMonth()+1).toString();
			var a = f.getFullYear();
			var fechaActual = d+'/'+(m.length == 1 ? '0'+m : m)+'/'+a+' '+f.getHours()+':'+f.getMinutes()+':'+f.getSeconds();
			var arr = new Array();
			var bandera = true;
			var j=0;
			//gri.show();
			for(var i=0; i<registrosConsultadosData.getTotalCount(); i++){
				var aux = new Array();
				registro = grid.getStore().getAt(i);
				if(registro.get('FACULTAD')==true /*&& (registro.get('MONEDA')=="" || registro.get('MONTO')=="")*/){
					if(registro.get('MONTO')==""){
						Ext.MessageBox.alert('Aviso', 'Debe escribir un monto');
						bandera = false;
						break;
					}
					else if(registro.get('MONEDA')==""){
						Ext.MessageBox.alert('Aviso', 'Debe de seleccionar la moneda');
						bandera = false;
						break;
					}else{
						aux[0] = registro.get('CVEIF');
						aux[1] = registro.get('MONEDA');
						aux[2] = registro.get('MONTOANT');
						aux[3] = registro.get('MONTO');
						aux[4] = registro.get('FACULTAD');
						aux[5] = registro.get('NOMBREIF');
						arr[j] = aux;
						j++;
					}
				}else if(registro.get('MONEDA')!="" || registro.get('MONTO')!=""){
					if(registro.get('MONEDA')!="" && registro.get('MONTO')==""){
						Ext.MessageBox.alert('Aviso', 'Debe escribir un monto');
						bandera = false;
						break;
					}
					else if(registro.get('MONTO')!="" && registro.get('MONEDA')==""){
						Ext.MessageBox.alert('Aviso', 'Debe de seleccionar la moneda');
						bandera = false;
						break;
					}else{
						aux[0] = registro.get('CVEIF');
						aux[1] = registro.get('MONEDA');
						aux[2] = registro.get('MONTOANT');
						aux[3] = registro.get('MONTO');
						aux[4] = registro.get('FACULTAD');
						aux[5] = registro.get('NOMBREIF');
						arr[j] = aux;
						j++;
					}
				}
			}
			
			if(bandera){
				var gr = Ext.getCmp("gridConsulta");
				gr.el.mask('Procesando...','x-mask-loading');
				Ext.StoreMgr.key('registrosAceptadosDataStore').load({
					params:	
								{
									informacion: 'Aceptar',
									fechaActual: fechaActual,
									arr: arr
								}
				});	
			}
			
		}else if(	estadoSiguiente == "REGRESAR"){
			gri.hide();
			gridFec.hide();
			var forma = Ext.getCmp("forma");
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			btnGenerarPDF.enable();
			Ext.getCmp('forma').getForm().reset();
			forma.show();
			grid.hide();
			
		}else if(	estadoSiguiente == "LIMPIAR"){
			Ext.getCmp('forma').getForm().reset();
			grid.hide();
		}
	}

//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

	
	//Combo que va dentro del EditorGridPanel
	var comboMoneda = new Ext.form.ComboBox({  
	 mode: 'local',
	 id: '_cmb_mon',	 
	 displayField : 'descripcion',
	 valueField : 'clave',
    triggerAction : 'all',
	 typeAhead: true,
	 minChars : 1,
    allowBlank: true,
	 editable: false,
    store: catalogoMonedasData 
	});
	
	Ext.util.Format.comboRenderer = function(comboMoneda){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var valor = registro.data['MONEDA'];					
			if(valor !=''){
				var record = comboMoneda.findRecord(comboMoneda.valueField, value);
				return record ? record.get(comboMoneda.displayField) : comboMoneda.valueNotFoundText;
			} 
			if(valor !=''){
				metadata.attr='style="border: thin solid #3399CC;  color: black;"';
				return valor;
			}
         if(valor==''){
            return NE.util.colorCampoEdit('Seleccione...',metadata,registro);
         }
		}
	}	
	
	/*
	renderer:function(value,metadata,registro, rowIndex, colIndex){
				metadata.attr='style="border: thin solid #3399CC;  color: black;"';
                   return value;
				},
	*/

	//Elementos del grid de la consulta
		var grid = new Ext.grid.EditorGridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			style: 		'margin: 0 auto'/*; padding-top:10px;'*/,
			hidden: 		true,
			margins:		'20 0 0 0',
			clicksToEdit:1,
			stripeRows: true,
			loadMask: 	true,
			height: 	427,
			width: 		765,
			columns: [
				{
					header: 		'Clave',
					tooltip: 	'Clave de IF',
					dataIndex: 	'CVEIF',
					align:		'center',
					resizable: 	false,
					width: 		50,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Intermediario',
					tooltip: 	'Nombre del intermediario',
					dataIndex: 	'NOMBREIF',
					align:		'left',
					resizable: 	true,
					width: 		225,
					hidden: 		false,
					hideable:	false
				},
				{
					header: 'Moneda',
					dataIndex: 'MONEDA',
					width: 		170,
					editor: comboMoneda,
					//renderer: Ext.util.Format.comboRenderer(comboMoneda)
					renderer:function(value,metadata,registro){
									var record = comboMoneda.findRecord(comboMoneda.valueField, value);
									return NE.util.colorCampoEdit(record ? record.get(comboMoneda.displayField) : comboMoneda.valueNotFoundText,metadata,registro);
					}
				},{
					header: 		'Monto',
					tooltip: 	'Monto',
					dataIndex: 	'MONTO',
					align:		'right',
					editor: {
					 xtype: 'textfield',
					 maskRe:		/[0-9]/,
					 maxLength:12		 
					},
					resizable: 	false,
					width: 		110,
					hidden: 		false,
					hideable:	false,
					//css:"background-color:#F5FFFA;",	
					/* renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					 }*/
					renderer:	function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(Ext.util.Format.number(value,'$0,0.00'),metadata,registro);
							}
				},{
					xtype: 'checkcolumn',				
					header:'Aplica Facultades por IF',
					tooltip: 'Aplica Facultades por IF',
					dataIndex : 'FACULTAD',
					width : 125,
					align: 'center',
					sortable : false			
				 },
				 {
					xtype: 'actioncolumn',
					header: 'Historico',
					tooltip: 'Ver historico',
					resizable: 	false,
					width: 60,
					align: 'center',					
					items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoBuscar';										
						},	
						handler: verHistoricos
					}
				]				
			}
			],bbar: {
			buttonAlign: 	'right',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Aceptar',
					width:	100,
					iconCls:	'icoAceptar',
					id: 		'btnAceptar',
					handler: function(boton, evento) {
						accionConsulta("ACEPTAR",null)
					}
				},'-'
			]
		}
	});
	
	//Elementos del grid de datos Aceptados
		var gri = new Ext.grid.GridPanel({
			store: 		registrosAceptadosData,
			id:			'gridAceptados',
			style: 		'margin: 0 auto',
			hidden: 		true,
			margins:		'20 0 0 0',
			stripeRows: true,
			loadMask: 	true,
			height: 	427,
			width: 		795,
			columns: [
				{
					header: 		'Clave',
					tooltip: 	'Clave de IF',
					dataIndex: 	'CVEIFI',
					align:		'center',
					resizable: 	false,
					width: 		50,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Intermediario',
					tooltip: 	'Nombre del intermediario',
					dataIndex: 	'NOMBREIFI',
					align:		'left',
					resizable: 	true,
					width: 		225,
					hidden: 		false,
					hideable:	false
				},
				{
					header: 'Moneda',
					dataIndex: 'MONEDAI',
					width: 		170,
					align:		'left',
					resizable: 	true,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Monto Anterior',
					tooltip: 	'Monto anterior',
					dataIndex: 	'MONANTI',
					align:		'right',
					resizable: 	false,
					width: 		110,
					hidden: 		false,
					hideable:	false,
					renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
				},{
					header: 		'Monto Actual',
					tooltip: 	'Monto actual',
					dataIndex: 	'MONACT',
					align:		'right',
					resizable: 	false,
					width: 		110,
					hidden: 		false,
					hideable:	false,
					renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
				},
				 {
					xtype: 'actioncolumn',
					header: 'Aplica Facultades por IF',
					tooltip: 'Se aplican facultades',
					resizable: 	false,
					width: 127,
					hidden: 		false,
					hideable:	false,
					align: 'center',					
					items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['FACULTADI'] == "true" ){
								this.items[0].tooltip = 'Se aplican facultades';
								return 'icoAceptar';
							}
						}
					}
				]				
			}
			],bbar: {
			buttonAlign: 	'right',
			id: 			'barraPaginacion',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Imprimir',
					iconCls:	'icoPdf',
					id: 		'btnGenerarPDF',
					handler: function(boton, evento) {
							boton.disable();
							//boton.setIconClass('loading-indicator');
							var barraPaginacion = Ext.getCmp("barraPaginacion");
							var arreglo = new Array();
							for(var i=0; i<registrosAceptadosData.getTotalCount(); i++){
								var aux = new Array();
								registro = gri.getStore().getAt(i);
								
								aux[0] = registro.get('CVEIFI');
								aux[1] = registro.get('NOMBREIFI');
								aux[2] = registro.get('MONEDAI');
								aux[3] = registro.get('MONANTI');
								aux[4] = registro.get('MONACT');
								aux[5] = registro.get('FACULTADI');
								
								arreglo[i] = aux;
							}
							Ext.Ajax.request({
								url: '15admparamfaculExt.data.jsp',
								params:	
											{
												informacion:'ArchivoPDF',
												arreglo: arreglo
											}
								,
								callback: procesarSuccessFailureGenerarPDF
							});
					}
				},
				{
					xtype:	'button',
					text:		'Regresar',
					width:	100,
					iconCls:	'icoRegresar',
					id: 		'btnRegresar',
					handler: function(boton, evento) {
						accionConsulta("REGRESAR", null)
					}
				},'-'
			]
		}
	});
	
	//Elementos del grid de datos de fecha y usuario
		var gridFec = new Ext.grid.GridPanel({
			store: 		usuarioData,
			id:			'gridFecha',
			style: 		'margin: 0 auto',
			hidden: 		true,
			margins:		'20 0 0 0',
			stripeRows: true,
			loadMask: 	true,
			height: 	77,
			width: 		367,
			columns: [
				{
					header: 		' ',
					dataIndex: 	'DATOS',
					align:		'left',
					resizable: 	false,
					width: 		180,
					hidden: 		false,
					hideable:	false
				},{
					header: 		' ',
					dataIndex: 	'VALORES',
					align:		'left',
					resizable: 	false,
					width: 		180,
					hidden: 		false,
					hideable:	false
				}
			]
	});
	
	var elementoFormaConsulta = [
			{
				xtype: 'combo',
				name: 'cmb_if',
				id: '_cmb_if',
				fieldLabel: 'Intermediario Financiero',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'claveIF',
				emptyText: 'Todos',
				forceSelection : true,
				triggerAction : 'all',
				editable: true,
				typeAhead: true,
				minChars : 1,
				store: 		catalogoIntermediariosData,
				anchor:	'95%'
			}
	];
	
	var fp = new Ext.form.FormPanel({
			id:				'forma',
			title:	'Criterios de Busqueda', 
			width:			550,
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			labelWidth:		150,
			items:			elementoFormaConsulta,
			monitorValid:	true,
			buttons: [
			{
				text:		'Consultar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:false,
				handler: function(boton, evento) {
								accionConsulta("CONSULTAR",null)
							}
				}, //fin handler
				{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR",null);
							}
				}
			]
		});
		
//Ventana emergente de datos histricos
	var winHistoricos = new Ext.Window ({
		id:'winHistoricos',
		height: 314,
		x: 300,
		y: 100,
		width: 580,
		modal: true,
		closeAction: 'hide',
		items:[
			NE.util.getEspaciador(10),
			{
				xtype:'grid',
				id:'gridHistoricos',
				store: 		historicosData,
				//margins:		'20 0 0 0',
				style: 'margin: 0 auto',
				frame: false,
				hidden: 		true,
				height: 	264,
				width: 	530,
				columns:[
					{
						header: 		'Usuario',
						tooltip: 	'Nombre de usuario',
						dataIndex: 	'NOMIF',
						align:		'center',
						resizable: 	true,
						width: 		85,
						hidden: 		false,
						hideable:	false
					},{
						header: 		'Fecha de modificaci�n',
						tooltip: 	'Fecha de la ultima modificaci�n',
						dataIndex: 	'FECMOD',
						align:		'center',
						resizable: 	true,
						width: 		118,
						hidden: 		false,
						hideable:	false
					},{
						header: 		'Moneda',
						tooltip: 	'Tipo de moneda',
						dataIndex: 	'MONEDAB',
						align:		'center',
						resizable: 	true,
						width: 		100,
						hidden: 		false,
						hideable:	false
					},{
						header: 		'Monto Anterior',
						tooltip: 	'Monto anterior',
						dataIndex: 	'MONANT',
						align:		'right',
						resizable: 	true,
						width: 		100,
						hidden: 		false,
						hideable:	false,
						renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
					},{
						header: 		'Monto Modificado',
						tooltip: 	'Monto modificado',
						dataIndex: 	'MONMOD',
						align:		'right',
						resizable: 	true,
						width: 		100,
						hidden: 		false,
						hideable:	false,
						renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
					}
				],bbar: {
					buttonAlign: 	'right',
					items: [
						'->','-',
						{
							xtype:	'button',
							text:		'Cerrar',
							width:	100,
							iconCls:	'icoRechazar',
							id: 		'btnCerrar',
							handler: function(boton, evento) {
								Ext.getCmp('winHistoricos').hide();
							}
						},'-'
					]
				}
			}
		]
	});
		
//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	930,
		//height: 	'auto',
		align: 'center',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			grid,
			gridFec,
			NE.util.getEspaciador(10),
			gri
		]
	});
	
	Ext.StoreMgr.key('catalogoIntermediariosDataStore').load();
	Ext.StoreMgr.key('catalogoMonedasDataStore').load();

});