Ext.onReady(function(){

//-------------Handlers-------------
	var escondeBoton;
	var escondeBoton24;
	var estatusActual; 

	var esconderBotonFP = function(){
		Ext.getCmp('btnIniciarServicio').setVisible(escondeBoton);
	}
	
	var esconderBotonFP24 = function(){
		Ext.getCmp('btnIniciarServicio24').setVisible(escondeBoton24);
	}
	
	var procesarCondicion = function(store, arrRegistros, opts) {
		if(store.reader.jsonData.success2==true){
			Ext.getCmp('leyenda').show();
			Ext.getCmp('leyenda24').hide();
			gridError24.setVisible(false);
			var params = (fpDetalle)?fpDetalle.getForm().getValues():{};
			consultaDataGrid.load({
				params: Ext.apply(params)
			});
			Ext.getCmp('btnIniciarServicio').enable();
			if(Ext.getCmp('id_ic_producto').getValue() != '4'){
				Ext.getCmp('btnIniciarServicio24').disable();
				consultaDataGrid24.load();
				Ext.getCmp('btnIniciarServicio24').enable();
			}
		}else{
			sm.clearSelections();
			Ext.getCmp('leyenda24').hide();
			gridError24.setVisible(false);
			gridError.setVisible(true);
			Ext.getCmp('btnIniciarServicio').enable();
		}
	}


	var procesarCondicion24 = function(store, arrRegistros, opts) {
		if(store.reader.jsonData.success2==true){
			if(store.reader.jsonData.success3 == true){
				Ext.getCmp('leyenda2').setText('<BR/><center><font size="2">No se puede aperturar el servicio, verifique que al ' +
												'menos una PYME <BR>se encuentre fuera de horario de servicio mismo d�a</font></h2></center><BR/>', false);
			}
			else{
				Ext.getCmp('leyenda2').setText('<BR/><center><font size="2">El servicio fue abierto satisfactoriamente para el producto seleccionado</font></h2></center><BR/>',false);
			}
			Ext.getCmp('leyenda24').show();
			Ext.getCmp('leyenda').hide();
			gridError.setVisible(false);
			Ext.getCmp('btnIniciarServicio24').enable();
			consultaDataGrid24.load();
			if(Ext.getCmp('id_ic_producto').getValue() != '4'){
				Ext.getCmp('btnIniciarServicio').disable();  
				var params = (fpDetalle)?fpDetalle.getForm().getValues():{};
				consultaDataGrid.load({
					params: Ext.apply(params)
				});
				Ext.getCmp('btnIniciarServicio').enable();  
			}
		}else{
			sm2.clearSelections();
			gridError24.setVisible(true);
			Ext.getCmp('leyenda').hide();
			gridError.setVisible(false);
			Ext.getCmp('btnIniciarServicio24').enable();
		}
	}

	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Todos los productos", 
		loadMsg: ""})); 
		store.commitChanges(); 
		
		Ext.getCmp('id_ic_producto').setValue('');
		estatusActual = Ext.getCmp('id_ic_producto').getValue();
	}


	var Rencierrepyme24= function(value, metadata, record, rowindex, colindex, store) {
									if(record.data.bcierre24hrs == "false")
									return '<font color="GREEN">Servicio Abierto</font>';
									else{
									escondeBoton24=true;
									return 'Servicio Cerrado';
									}
								}

      
	var Rencierrepyme= function(value, metadata, record, rowindex, colindex, store) {
									if(record.data.bcierrepyme == "false")
									return '<font color="GREEN">Servicio Abierto</font>';
									else{
									escondeBoton=true;
									return 'Servicio Cerrado';
									}
								}

	
	var Rencierreif= function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.bcierreif == "false")
									return '<font color="GREEN">Servicio Abierto</font>';
									else
									return 'Servicio Cerrado';
								}



//-------------Stores----------------

	var CatalogoProducto= new Ext.data.JsonStore
	  ({
			id: 'catProducto',
			root : 'registros',
			fields : ['clave', 'descripcion', 'loadMsg'],
			url : '15horaperturaExt.data.jsp',
			baseParams: 
			{
			 informacion: 'CatalogoProducto'
			},
			autoLoad: false,
			listeners:
			{
			 load: procesarConsultaData,
			 exception: NE.util.mostrarDataProxyError,
			 beforeload: NE.util.initMensajeCargaCombo
			}
	  });

  
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15horaperturaExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'producto'},
			{name: 'claveProducto'},
			{name: 'bcierrepyme'},
			{name: 'bcierreif'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			beforeLoad: function(){
			escondeBoton=false;
			},
			load:esconderBotonFP,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	
	var consultaDataGrid24 = new Ext.data.JsonStore({
		root : 'registros',
		url : '15horaperturaExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid24'
		},
		fields: [		
			{name: 'producto'},
			{name: 'claveProducto'},
			{name: 'bcierre24hrs'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			beforeLoad: function(){
			escondeBoton24=false;
			},
			load:esconderBotonFP24,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	
	
	
	 
	var consultaErrores = new Ext.data.JsonStore({
		root : 'registros',
		url : '15horaperturaExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaServicio'
		},
		fields: [		
			{name: 'ERROR'},
			{name: 'SOLUCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		style: ' margin:0 auto;',
		autoLoad: false,
		listeners: {
			load: procesarCondicion,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});

	
	var consultaErrores24 = new Ext.data.JsonStore({
		root : 'registros',
		url : '15horaperturaExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaServicio24'
		},
		fields: [		
			{name: 'ERROR'},
			{name: 'SOLUCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		style: ' margin:0 auto;',
		autoLoad: false,
		listeners: {
			load: procesarCondicion24,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	
	Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);




//--------------Componentes----------

	var leyendaCorrecto =
		  {
		  xtype:'panel',
		  id:'leyenda',
		  style: ' margin:0 auto;',
		  frame: true,
		  width:520,
		  height: 60,
		  hidden: true,
		  items:[
				{
					xtype: 	'label',
					id:	 	'leyenda1',
					hidden: 	false,
					html:  	'<BR/><center><font size="2"> El servicio fue abierto satisfactoriamente para los productos seleccionados</font></h2></center><BR/>'
					}
				]
			};

	var leyendaCorrecto24 =
	{
		xtype:'panel',
		id:'leyenda24',
		style: ' margin:0 auto;',
		frame: true,
		width:520,
		height: 65,
		hidden: true,
		items:[
		{
			xtype: 	'label',
			id:	 	'leyenda2',
			hidden: 	false
		}
		]
	};
			
	var elementosForma = 
	{
		xtype: 'combo',
		fieldLabel: 'Producto',
		forceSelection: true,
		autoSelect: true,
		name:'ic_producto',
		hiddenName:'ic_producto',
		id:'id_ic_producto',
		displayField: 'descripcion',
		allowBlank: true,
		editable:false,
		valueField: 'clave',
		mode: 'local',
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		store: CatalogoProducto,
		width:120,
		
		listeners:{
			select: function(combo){
				if(estatusActual != Ext.getCmp('id_ic_producto').getValue()){
					estatusActual = Ext.getCmp('id_ic_producto').getValue();
					Ext.getCmp('leyenda').hide();
					Ext.getCmp('gridErr').hide();
					Ext.getCmp('leyenda24').hide();
					Ext.getCmp('gridErr24').hide();
					var params = (fpDetalle)?fpDetalle.getForm().getValues():{};
					consultaDataGrid.load({
							params: Ext.apply(params)
					});
				}
			}
		}
  };

	var fpDetalle = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
	   height: 'auto',
		width: 340
	});	
	
	
	var sm = new Ext.grid.CheckboxSelectionModel({
		header: ' ',
		checkOnly: false,
		id:'chk',
		tooltip: 'Apertura Pymes',
		width: 25,
		renderer: function(value, metadata, record, rowindex, colindex, store) {
			if (record.data['bcierrepyme']!='false'){
				return '<div class="x-grid3-row-checker">&#160;</div>';
			}else{
				return '<div>&#160;</div>';
			}
		}
	});
	
	
	var grid = new Ext.grid.GridPanel( {
		store: consultaDataGrid,
		title: '<center>Estado del servicio Mismo d�a</center>',
		id: 'grid',
		sm:sm,
		columns: [
			sm,
			{
				header: 'Producto',
				dataIndex: 'producto',
				tooltip: 'Producto',
				width: 220,
				align: 'center'				
			},{
				header: 'Proveedores o <br>Clientes',
				dataIndex: 'bcierrepyme',
				tooltip: 'Proveedores o Clientes',
				align: 'center',
				width: 129,
				renderer: Rencierrepyme
			}, {
				header: 'Intermediarios<br> Financieros',
				dataIndex: 'bcierreif',
				tooltip: 'Intermediarios Financieros',
				align: 'center',
				width: 129,
				renderer: Rencierreif				
			}
			],
		
		loadMask: true,
		style: 'margin:0 auto;',
		height: 160,
		bbar: {
					autoScroll:true,
					id: 'barra',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnIniciarServicio',
						text:		'Iniciar Servicio',
						width: 50,
						weight: 70,
						style: 'margin:0 auto;',
						align:'rigth',
						handler: function(boton, evento) {
							boton.disable();
							var arrRegistros = sm.getSelections();
							if (arrRegistros.length == 0) {
								Ext.MessageBox.alert("Alerta","Debe seleccionar al menos una opci�n para la Apertura de Servicio");
								Ext.getCmp('btnIniciarServicio').enable();
							}else {
								Ext.getCmp('leyenda').hide();
								Ext.getCmp('gridErr').hide();
								var arrClaveSel= new Array();
								for(var i=0;i<arrRegistros.length;i++){
									var reg = arrRegistros[i];
									arrClaveSel[i]=""+ reg.get("claveProducto");
								}
								consultaErrores.load({
								params: {checkbox:arrClaveSel}
								});
							}
							
						}
					}		
					]
					},
					width: 520,
					frame: true
			});
	


	var gridError = new Ext.grid.GridPanel( {
		store: consultaErrores,
		title: '<center>Errores en el Proceso de Apertura del Servicio</center>',
		id: 'gridErr',
		
		columns: [
			{
				header: 'Error',
				dataIndex: 'ERROR',
				width: 251,
				align: 'center'				
			},{
				header: 'Solucion',
				dataIndex: 'SOLUCION',
				align: 'center',
				width: 251
			}  
		],
		hidden: true,
		style: 'margin:0 auto;',
		loadMask: true,
		height: 135,
		width: 520,
		frame: true
	});


	var sm2 = new Ext.grid.CheckboxSelectionModel({
		header: ' ',
		checkOnly: false,
		id:'chk2',
		tooltip: 'Apertura Pymes',
		width: 25,
		renderer: function(value, metadata, record, rowindex, colindex, store) {
			if (record.data['bcierre24hrs']!='false'){
				return '<div class="x-grid3-row-checker">&#160;</div>';
			}else{
				return '<div>&#160;</div>';
			}  
		} 
	});

var grid24 = new Ext.grid.GridPanel( {
		store: consultaDataGrid24,
		title: '<center>Estado del servicio 24 Horas</center>',
		id: 'grid24',
		sm:sm2,
		columns: [
			sm2,
			{
				header: 'Producto',
				dataIndex: 'producto',
				tooltip: 'Producto',
				width: 220,
				align: 'center'				
			},{
				header: 'Proveedores o <br>Clientes',
				dataIndex: 'bcierre24hrs',
				tooltip: 'Proveedores o Clientes',
				align: 'center',
				width: 129,
				renderer: Rencierrepyme24
			}
			],
		
		loadMask: true,
		style: 'margin:0 auto;',
		height: 130,
		bbar: {
					autoScroll:true,
					id: 'barra2',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnIniciarServicio24',
						text:		'Iniciar Servicio',
						width: 50,
						weight: 70,
						style: 'margin:0 auto;',
						align:'rigth',
						handler: function(boton, evento) {
							boton.disable();
							var arrRegistros = sm2.getSelections();
							if (arrRegistros.length == 0) {
								Ext.MessageBox.alert("Alerta","Debe seleccionar una opci�n para la Apertura de Servicio");
								Ext.getCmp('btnIniciarServicio24').enable();
							}else {
								Ext.getCmp('leyenda24').hide();
								Ext.getCmp('gridErr24').hide();
								var arrClaveSel= new Array();
								for(var i=0;i<arrRegistros.length;i++){
									var reg = arrRegistros[i];
									arrClaveSel[i]=""+ reg.get("claveProducto");
								}
								consultaErrores24.load({
								params: {checkbox24:arrClaveSel}
								});
								}
							}    
						}		
					]
					},
					width: 390,
					frame: true
			});
	

		var gridError24 = new Ext.grid.GridPanel( {
		store: consultaErrores24,
		title: '<center>Errores en el Proceso de Apertura del Servicio</center>',
		id: 'gridErr24',
		
		columns: [
			{
				header: 'Error',
				dataIndex: 'ERROR',
				width: 251,
				align: 'center'				
			},{
				header: 'Solucion',
				dataIndex: 'SOLUCION',
				align: 'center',
				width: 251
			}  
		],
		hidden: true,
		style: 'margin:0 auto;',
		loadMask: true,
		height: 135,
		width: 520,
		frame: true
	});


	//Contenedor Principal
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [fpDetalle, NE.util.getEspaciador(10),
		grid, NE.util.getEspaciador(10),
		leyendaCorrecto, NE.util.getEspaciador(10),
		gridError, NE.util.getEspaciador(10),
		grid24, NE.util.getEspaciador(10),
		leyendaCorrecto24, NE.util.getEspaciador(10),
		gridError24, NE.util.getEspaciador(10)
		]
	});
	
	CatalogoProducto.load();

});