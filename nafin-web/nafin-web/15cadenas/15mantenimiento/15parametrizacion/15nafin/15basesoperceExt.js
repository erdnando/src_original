Ext.onReady(function(){
	var TipoPlazo          = "C";
	var operacionDinamica  = "N";	 
	var automaticaGarantia = "N";	
	var renta              = "";	
	
	var valorIF            = "";
	var valorTasa          = "";
	var valorCredito       = "";
	var valorAmortizacion  = "";
	var valorCartera       = "";
	
	var valorEmisor        = "";
	var valorPeriocidad    = "";

	var ultimo             = "";
	var maximo             = "";
	var ultimoRenglon      = "-1";

	var registroUnico      = false;	

//-------------Handlers----------------------------------------------------------------------------	
	
	var procesarPlazoMinimo = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			plazo = info.PLAZO;
			maximo = info.ULTIMO;
			
			var localplazo= parseInt(plazo);
			if(localplazo != 1){
				Ext.getCmp('numMinimo').setValue(plazo);
				Ext.getCmp('numMinimo').disable();
			
				operacionDinamica = info.BASEDINAMICA;
				if(operacionDinamica == 'S')
					Ext.getCmp('radioBaseSi').setValue(true);
				else if(operacionDinamica == 'N')
					Ext.getCmp('radioBaseNo').setValue(true);	
				
				Ext.getCmp('radioBaseSi').disable();
				Ext.getCmp('radioBaseNo').disable();
			}else{
				Ext.getCmp('numMinimo').enable();
				Ext.getCmp('numMinimo').setValue(plazo);
			
				Ext.getCmp('radioBaseNo').setValue(true);
				operacionDinamica  = "N";	 
				Ext.getCmp('radioBaseSi').enable();
				Ext.getCmp('radioBaseNo').enable();
			}

			Ext.getCmp('numMaximo').setValue('');
		
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}		
		
	
	
	var procesarPlazoMinimo2 = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			plazo = info.PLAZO;
			maximo = info.ULTIMO;
			
			var localplazo= parseInt(plazo);
			if(localplazo != 1){
				Ext.getCmp('numMinimo').setValue(plazo);
				Ext.getCmp('numMinimo').disable();
			
				operacionDinamica = info.BASEDINAMICA;
				if(operacionDinamica == 'S')
					Ext.getCmp('radioBaseSi').setValue(true);
				else if(operacionDinamica == 'N')
					Ext.getCmp('radioBaseNo').setValue(true);	
				
				Ext.getCmp('radioBaseSi').disable();
				Ext.getCmp('radioBaseNo').disable();
			}else{
				Ext.getCmp('numMinimo').enable();
				Ext.getCmp('numMinimo').setValue(plazo);
			
				Ext.getCmp('radioBaseNo').setValue(true);
				operacionDinamica  = "N";	 
				Ext.getCmp('radioBaseSi').enable();
				Ext.getCmp('radioBaseNo').enable();
			}

			Ext.getCmp('numMaximo').setValue('');
									consultaDataGrid.load({
							params: {
								comboIF:Ext.getCmp('idComboIF').getValue(),
								comboTasa:Ext.getCmp('idComboTasa').getValue(),
								comboCredito:Ext.getCmp('idComboCredito').getValue(),
								comboAmortizacion:Ext.getCmp('idComboAmortizacion').getValue(),
								comboCartera:Ext.getCmp('idComboCartera').getValue(),
								comboEmisor:Ext.getCmp('idComboEmisor').getValue(),
								comboPeriocidad:Ext.getCmp('idComboPeriocidad').getValue(),
								tipoRenta: renta,
								tipoPlazo: TipoPlazo
							}
						});
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
		}
		
		
	var revisarSelecciones = function(){
		grid.hide(); //?
		
		if(Ext.getCmp('idComboIF').getValue() == ''){
			Ext.getCmp('numMinimo').reset();
			Ext.getCmp('numMaximo').reset();
			return ;
		}
		if(Ext.getCmp('idComboTasa').getValue() == ''){
			Ext.getCmp('numMinimo').reset();
			Ext.getCmp('numMaximo').reset();
			return ;
		}
		if(Ext.getCmp('idComboCredito').getValue() == ''){
			Ext.getCmp('numMinimo').reset();
			Ext.getCmp('numMaximo').reset();	
			return ;
		}
		if(Ext.getCmp('idComboAmortizacion').getValue() == ''){
			Ext.getCmp('numMinimo').reset();
			Ext.getCmp('numMaximo').reset();	
			return ;
		}
		if(Ext.getCmp('idComboCartera').getValue() == ''){
			Ext.getCmp('numMinimo').reset();
			Ext.getCmp('numMaximo').reset();	
			return ;
		}
		if(TipoPlazo == ''){
			Ext.getCmp('numMinimo').reset();
			Ext.getCmp('numMaximo').reset();	
			return ;
		}
		
		
		Ext.Ajax.request({
			url: '15basesoperceExt.data.jsp',
			params: {
				informacion: 'ConsiguePlazoMinimo',
				comboIF:Ext.getCmp('idComboIF').getValue(),
				comboTasa:Ext.getCmp('idComboTasa').getValue(),
				comboCredito:Ext.getCmp('idComboCredito').getValue(),
				comboAmortizacion:Ext.getCmp('idComboAmortizacion').getValue(),
				comboCartera:Ext.getCmp('idComboCartera').getValue(),
				comboEmisor:Ext.getCmp('idComboEmisor').getValue(),
				comboPeriocidad:Ext.getCmp('idComboPeriocidad').getValue(),
				tipoRenta: renta,
				tipoPlazo: TipoPlazo		
			},
			callback: procesarPlazoMinimo
		});	
	}
	
	
	var procesarAceptar = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);

			if(info.MENSAJE != '')
				Ext.MessageBox.alert("Mensaje",info.MENSAJE);
			
			grid.hide();		
			Ext.Ajax.request({
				url: '15basesoperceExt.data.jsp',
				params: {
					informacion: 'ConsiguePlazoMinimo',
					comboIF:Ext.getCmp('idComboIF').getValue(),
					comboTasa:Ext.getCmp('idComboTasa').getValue(),
					comboCredito:Ext.getCmp('idComboCredito').getValue(),
					comboAmortizacion:Ext.getCmp('idComboAmortizacion').getValue(),
					comboCartera:Ext.getCmp('idComboCartera').getValue(),
					comboEmisor:Ext.getCmp('idComboEmisor').getValue(),
					comboPeriocidad:Ext.getCmp('idComboPeriocidad').getValue(),
					tipoRenta: renta,
					tipoPlazo: TipoPlazo					
				},
				callback: procesarPlazoMinimo
			});	
						
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}	
	
	
	var fnAceptar = function (){
		if(Ext.getCmp('idComboIF').getValue() == ''){
			Ext.getCmp('idComboIF').markInvalid('Debe de Seleccionar un IF');
			return ;
		}
		if(Ext.getCmp('idComboTasa').getValue() == ''){
			Ext.getCmp('idComboTasa').markInvalid('Debe de Seleccionar una Tasa');
			return ;
		}
		if(Ext.getCmp('idComboCredito').getValue() == ''){
			Ext.getCmp('idComboCredito').markInvalid('Debe de Seleccionar un Tipo de Cr�dito');
			return ;
		}
		if(Ext.getCmp('idComboAmortizacion').getValue() == ''){
			Ext.getCmp('idComboAmortizacion').markInvalid('Debe de Seleccionar un Tipo de Amortizaci�n');
			return ;
		}	
		if(Ext.getCmp('idComboCartera').getValue() == ''){
			Ext.getCmp('idComboCartera').markInvalid('Debe de Seleccionar un Tipo de Cartera');
			return ;
		}			
		if(Ext.getCmp('numMaximo').getValue() == ''){
			Ext.getCmp('numMaximo').markInvalid('Debe de Poner un Plazo M�ximo');
			return ;
		}
		if(Ext.getCmp('numMinimo').getValue() == ''){
			Ext.getCmp('numMinimo').markInvalid('Debe de Poner un Plazo M�nimo');
			return ;		
		} 		
		if(parseInt(Ext.getCmp('numMaximo').getValue()) <= parseInt(Ext.getCmp('numMinimo').getValue())){
			Ext.getCmp('numMaximo').markInvalid('El Plazo Maximo debe de ser Mayor al Plazo Minimo');
			return ;
		}
		if(Ext.getCmp('idComboOperacion').getValue() == ''){
			Ext.getCmp('idComboOperacion').markInvalid('Debe de Seleccionar un C�digo de Operaci�n');
			return ;
		}
		if(Ext.getCmp('idComboPeriocidad').getValue() != ''){
			if(Ext.getCmp('idComboInteres').getValue() == ''){
				Ext.getCmp('idComboInteres').markInvalid('Debe seleccionar el Tipo de Inter�s');
				return ;
			}
		}
		
		Ext.Ajax.request({
			url: '15basesoperceExt.data.jsp',
			params: {
				informacion: 'Aceptar',
				comboIF:Ext.getCmp('idComboIF').getValue(),
				comboTasa:Ext.getCmp('idComboTasa').getValue(),
				comboOperacion:Ext.getCmp('idComboOperacion').getValue(),			
				comboCredito:Ext.getCmp('idComboCredito').getValue(),
				comboAmortizacion:Ext.getCmp('idComboAmortizacion').getValue(),
				comboCartera:Ext.getCmp('idComboCartera').getValue(),
				comboEmisor:Ext.getCmp('idComboEmisor').getValue(),
				valorMinimo: Ext.getCmp('numMinimo').getValue() ,
				valorMaximo: Ext.getCmp('numMaximo').getValue(),
				tipoPlazo: TipoPlazo,
				altaAutomatica: automaticaGarantia,
			
				codigoBanco: Ext.getCmp('numCodBanco').getValue(),
				comboPeriocidad:Ext.getCmp('idComboPeriocidad').getValue(),
				comboInteres:Ext.getCmp('idComboInteres').getValue(),
				tipoRenta: renta,
				baseOperacion: operacionDinamica
			},
			callback: procesarAceptar
		});
		
	}
		
	
	var determinaPlazo = function(){
		Ext.Ajax.request({
			url: '15basesoperce2Ext.data.jsp',
			params: {
						informacion:'ConsiguePlazo',
						no_if:Ext.getCmp('idComboIF').getValue(),											
						tipoTasa:Ext.getCmp('idComboTasa').getValue(),
						tipoCred:Ext.getCmp('idComboCredito').getValue(),
						tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
						tipoCartera:Ext.getCmp('idComboCartera').getValue(),
						tipo_plazo:TipoPlazo
			},	
			callback: procesarPlazo
		});	
	}
	
	var ocultarPlazo = function(){
				Ext.getCmp('radioFactoraje').hide();
				Ext.getCmp('radioCredito').hide();
				Ext.getCmp('radioCotizaciones').hide();
				Ext.getCmp('dfCotizaciones').hide()
				Ext.getCmp('dfCredito').hide();
				Ext.getCmp('dfFactoraje').hide();
	
	}
	
	function procesarPlazo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var info = Ext.util.JSON.decode(response.responseText);
			if(info.PLAZOS == '1'){
				if(info.TIPO == 'F'){
					Ext.getCmp('leyenda').setText('Factoraje');
				}else if (info.TIPO == 'C'){
					Ext.getCmp('leyenda').setText('Credito');
				}else if(info.TIPO == 'E'){
					Ext.getCmp('leyenda').setText('Cotizaciones Espec�ficas');
				}
				
				Ext.getCmp('cfPlazo').hide();
				Ext.getCmp('cfPlazoEtiqueta').show();
				
			}else{
				TipoPlazo = "";  //???????
				ocultarPlazo();
				Ext.getCmp('cfPlazoEtiqueta').hide();
				Ext.getCmp('cfPlazo').show();
				
				if(info.VARF == 'true'){
					
					Ext.getCmp('radioFactoraje').show();
					Ext.getCmp('dfFactoraje').show();
				}
				if(info.VARC == 'true'){
					Ext.getCmp('radioCredito').show();
					Ext.getCmp('dfCredito').show();
				}
				if(info.VARE == 'true'){
					Ext.getCmp('radioCotizaciones').show();
					Ext.getCmp('dfCotizaciones').show();
				}
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var RenTipoPlazo = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.PLAZO == "F")
									return 'Factoraje';
								else if(record.data.PLAZO == "C"){
									return 'Credito';
								}else if (record.data.PLAZO == "E"){
									return 'Cotizaciones Espec�ficas'
								}							
						}	
								
	
	var RenCartera = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.CARTERA == "1")
									return '1er Piso';
								else 
									return '2do Piso';
						}		
	
	var RenAlta = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.ALTA == "S")
									return 'Si';
								else 
									return 'No';
						}	

	var RenBase = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.BASEDINAMICA == "S")
									return 'Si';
								else 
									return 'No';
						}
						
	var RenInteres = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.TIPOINTERES == "S")
									return 'Simple';
								else if(record.data.TIPOINTERES == "C")
									return 'Compuesto';
								else
									return '';
						}					
						
	var RenRenta = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.TIPORENTA == "" || record.data.TIPORENTA == null){
									return 'N/A';
								}else if(record.data.TIPORENTA == "S"){ 
									return 'Si';
								}else{
									return 'No';
								}
						}	
	
	
	var verEliminar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave = registro.get('ICBASEOPECRED'); 
		Ext.Msg.confirm('Mensaje', '�Esta seguro de Eliminar el Registro?',
				function(botonConf) {
					if (botonConf == 'ok' || botonConf == 'yes' || botonConf == 'si' ) {
						Ext.Ajax.request({
							url: '15basesoperceExt.data.jsp',
							params: {
								informacion: 'Eliminar',
								baseOperacion: clave
								
							},
							callback: procesarAceptar
						});	
				}
				});		
	}
	
	
	
	var verModificar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var plazoMax = registro.get('PLZMAX'); 
		var plazoMin = registro.get('PLZMIN');
		var icbase = registro.get('ICBASEOPECRED'); 
		var csbase = registro.get('BASEDINAMICA');
		var alta = registro.get('ALTA');
		var comboOp = registro.get('CODIGOBASE');
		
		if( parseInt(plazoMin) >= parseInt(plazoMax)){
			Ext.MessageBox.alert("Alerta","El Plazo M�ximo debe de ser Mayor al Plazo M�nimo");
			return ;
		}

		Ext.Ajax.request({
			url: '15basesoperceExt.data.jsp',
			params: {
				informacion: 'Modificar',
				plzMax: plazoMax,
				baseOpeCredito: icbase,
				baseOpDinamica: csbase,
				altaAutomatica:alta,
				claveCombo: comboOp
			},
			callback: procesarAceptar
		});		
	}		
	
	function procesarPiso(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var info = Ext.util.JSON.decode(response.responseText);
			var cvePiso = info.PISO
			if(cvePiso == '1'){
				dataCartera.removeAll();
				dataCartera.insert(0,new Todas({ 
									clave: "", 
									descripcion: "Seleccionar Cartera", 
									loadMsg: ""})); 
				dataCartera.insert(1,new Todas({ 
									clave: "1", 
									descripcion: "1er Piso", 
									loadMsg: ""})); 
									dataCartera.commitChanges();
			}else{	
				dataCartera.removeAll();
				dataCartera.insert(0,new Todas({ 
									clave: "", 
									descripcion: "Seleccionar Cartera", 
									loadMsg: ""})); 
				dataCartera.insert(1,new Todas({ 
									clave: "2", 
									descripcion: "2do Piso", 
									loadMsg: ""})); 
									dataCartera.commitChanges();
			}

			
			valorCartera = cvePiso;
			Ext.getCmp('idComboCartera').setValue(info.PISO);
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');	
		if(arrRegistros!=null){
			Ext.getCmp('btnConsultar').enable();
			ultimo = ""+(store.getTotalCount() - 1);
	
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');			
			}
		}
	}	
		
	
	var procesarInicio= function(){
		Ext.getCmp('radioBaseNo').setValue(true);
		Ext.getCmp('radioAltaNo').setValue(true);
		Ext.getCmp('radioRentaNa').setValue(true);
		Ext.getCmp('radioCredito').setValue(true);
	}
	
	var procesarCredito = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Cr�dito", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboCredito').setValue(valorCredito);
	}
	
	var procesarIF = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar IF", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboIF').setValue(valorIF);
	}
	
	var procesarTasa = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Tasa", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboTasa').setValue(valorTasa); 
	}
	
	var procesarAmortizacion = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Amortizaci�n", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboAmortizacion').setValue(valorAmortizacion);
	}
	
	var procesarPeriocidad = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Periocidad", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboPeriocidad').setValue('');
	}	
	
		var procesarOperacion = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboOperacion').setValue(''); 
	}
	
		var procesarOperacion2 = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar el Cod. Operaci�n", 
		loadMsg: ""})); 
		store.commitChanges(); 
	}


	var procesarInteres = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Tipo de Interes", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboInteres').setValue('');
	}
	
	var procesarEmisor = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Emisor", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboEmisor').setValue('');
	}		
	
	
//-------------Stores--------------------------------------------------------------------------  	
	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15basesoperceExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'NOMIF'},
			{name: 'NOMTASA'},
			{name: 'NOMTIPCRED'},
			{name: 'NOMAMORTIZA'},
			{name: 'PLZMIN'},
			{name: 'PLZMAX'},
			{name: 'ICBASEOPECRED'},
			{name: 'TIPOPLAZO'},
			{name: 'PLAZO'},
			{name: 'CARTERA'},
			{name: 'NOMBASEOPER'},
			{name: 'CODIGOBASE'},
			{name: 'NOMEMISOR'},
			{name: 'ALTA'},			
			{name: 'NOMPERIODICIDAD'},
			{name: 'PRODUCTOBANCO'},
			{name: 'TIPOINTERES'},
			{name: 'TIPORENTA'},
			{name: 'BASEDINAMICA'},
			{name: 'TAM'}
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
		
	
	var CatalogoIF = new Ext.data.JsonStore
	({
		id: 'catIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperceExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoIF'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarIF,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});  
	
	var CatalogoTasa = new Ext.data.JsonStore
	({
		id: 'catTasa',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperceExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoTasa'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarTasa,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
	
	var CatalogoCredito = new Ext.data.JsonStore
	({
		id: 'catCredito',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperceExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoCredito'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarCredito,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});  
	
	var CatalogoAmortizacion = new Ext.data.JsonStore
	({
		id: 'catAmortizacion',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperceExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoAmortizacion'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarAmortizacion,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 


	var CatalogoPeriocidad = new Ext.data.JsonStore
	({
		id: 'catPeriocidad',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperceExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoPeriocidad'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarPeriocidad,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
	
			var CatalogoOperacion = new Ext.data.JsonStore
	({
		id: 'catOperacion',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperceExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoOperacion'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarOperacion,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var CatalogoOperacion2 = new Ext.data.JsonStore
	({
		id: 'catOperacion',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperceExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoOperacion'
		},
		autoLoad: true,
		listeners:
		{
		 load: procesarOperacion2,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var CatalogoEmisor = new Ext.data.JsonStore
	({
		id: 'catEmisor',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperceExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoEmisor'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarEmisor,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
	
		
	var CatalogoInteres = new Ext.data.JsonStore
	({
		id: 'catInteres',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperceExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoInteres'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarInteres,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
  
	 var dataCartera = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['','Seleccionar Cartera'],
			['1','1er Piso'],
			['2','2do Piso']
		]		
	 });
	
	
	var dataInteres = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['','Seleccionar Tipo Interes'],
			['S','Simple'],
			['C','Compuesto']
		]		
	 });	
	 
	var dataOpciones = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['S','Si'],
			['N','No']
		]		
	 });	

	
	var Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);

	
	var elementosForma = 
	[			
			{
				xtype: 'combo',
				fieldLabel: 'Nombre IF',
				forceSelection: true,
				autoSelect: true,
				name:'claveIF',
				id:'idComboIF',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoIF,  
				listeners:{
					select: function(combo){								
							if(valorIF == combo.getValue()){
								return ;
							}
								
							valorIF = combo.getValue();	
							if(combo.getValue() != ""){
								Ext.Ajax.request({
									url: '15basesoperceExt.data.jsp',
									params: {
											no_if:combo.getValue(),
											informacion:'ConsiguePiso'
									},	
									callback: procesarPiso
								});
							}
							revisarSelecciones();							
					}
				},
				anchor:	'89%'
		},
		{
				xtype: 'combo',
				fieldLabel: 'Tasa',
				forceSelection: true,
				autoSelect: true,
				name:'claveTasa',
				id:'idComboTasa',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoTasa, 
				listeners:{
					select: function(combo){
								if(valorTasa == combo.getValue()){
									return ;
								}								
								valorTasa = combo.getValue();
								revisarSelecciones();
					}					
			},					
				anchor:	'75%'
		},
		{
				xtype: 'combo',
				fieldLabel: 'Tipo Cr�dito',
				forceSelection: true,
				autoSelect: true,
				name:'claveCredito',
				id:'idComboCredito',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoCredito,  
				listeners:{
					select: function(combo){
								if(valorCredito == combo.getValue()){
									return ;
								}																
								valorCredito = combo.getValue();
								revisarSelecciones();				
					}					
				},	
				anchor: '77%'
		},		
		{
				xtype: 'combo',
				fieldLabel: 'Tipo Amortizaci�n',
				forceSelection: true,
				autoSelect: true,
				name:'claveAmortizacion',
				id:'idComboAmortizacion',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoAmortizacion,  
				listeners:{
					select: function(combo){
						if(valorAmortizacion == combo.getValue()){
							return ;
						}
						
						valorAmortizacion = combo.getValue();
						
						if(combo.getValue() == '5'){
							Ext.getCmp('radioRentaSi').enable();
							Ext.getCmp('radioRentaNo').enable();
						}else{
							Ext.getCmp('radioRentaSi').disable();
							Ext.getCmp('radioRentaNo').disable();						
						} 
						revisarSelecciones();
					}					
				},	
				anchor: '65%'
		},
		{
				xtype: 'combo',
				fieldLabel: 'Tipo de Cartera',
				forceSelection: true,
				autoSelect: true,
				name:'claveCartera',
				id:'idComboCartera',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: dataCartera,  
				listeners:{
					select:	function(combo){
						if(valorCartera == combo.getValue()){
							return ;
						}
						
						valorCartera = combo.getValue();
								
						valorIF = "";
						if(combo.getValue() == ''){
							dataCartera.removeAll();
							dataCartera.insert(0,new Todas({ 
									clave: "", 
									descripcion: "Seleccionar Cartera", 
									loadMsg: ""})); 
								
							dataCartera.insert(1,new Todas({ 
									clave: "1", 
									descripcion: "1er Piso", 
									loadMsg: ""})); 
								
							dataCartera.insert(2,new Todas({ 
									clave: "2", 
									descripcion: "2do Piso", 
									loadMsg: ""})); 
							dataCartera.commitChanges();
								
						}else if(combo.getValue() == '1'){
							dataCartera.removeAll();
							dataCartera.insert(0,new Todas({ 
									clave: "", 
									descripcion: "Seleccionar Cartera", 
									loadMsg: ""})); 
								dataCartera.insert(1,new Todas({ 
									clave: "1", 
									descripcion: "1er Piso", 
									loadMsg: ""}));
								dataCartera.commitChanges();
								
						}else if(combo.getValue() == '2'){
								dataCartera.removeAll();
								dataCartera.insert(0,new Todas({ 
									clave: "", 
									descripcion: "Seleccionar Cartera", 
									loadMsg: ""})); 
								dataCartera.insert(1,new Todas({ 
									clave: "2", 
									descripcion: "2do Piso", 
									loadMsg: ""}));
								dataCartera.commitChanges();								
						}
								
						CatalogoIF.load({
							params: {
											tipoCartera:combo.getValue()																											
										}
						});
								
					}
				},
				value: '',
				anchor: '50%'
		},
		{
			xtype: 'compositefield',
			id: 'cfPlazo',
			//width:300,
			fieldLabel:'Tipo de Plazo',
			hidden:false,
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioFactoraje',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "F"
											revisarSelecciones();
										}
									}
					}

				},	{
					xtype: 'displayfield',
					id: 'dfFactoraje',
					value: 'Factoraje	',
					width: 100
				}, 				{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioCredito',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "C";
											revisarSelecciones();
										}
									}
					}					
				},{
					xtype: 'displayfield',
					id:'dfCredito',
					value: 'Cr�dito ',
					width: 100
				},	{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioCotizaciones',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "E";
											revisarSelecciones();										
										}
									}
				}			
			},{
					xtype: 'displayfield',
					id: 'dfCotizaciones',
					value: 'Cotizaciones Espec�ficas ',
					width: 200
				}
			]
		},	
		{
				xtype:'numberfield',
				height: 20,
				fieldLabel: 'Plazo M�nimo',
				decimalPrecision: 0,
				width: 70,
				maxLength: 6,
				value: '',
				id:'numMinimo',
				anchor: '40%'
		},
		{
				xtype:'numberfield',
				height: 20,
				fieldLabel: 'Plazo M�ximo',
				width: 70,
				decimalPrecision: 0,
				maxLength: 6,
				value: '',
				id:'numMaximo',
				anchor: '40%'
		},			
		{
				xtype: 'combo',
				fieldLabel: 'Emisor',
				forceSelection: true,
				autoSelect: true,
				name:'claveEmisor',
				id:'idComboEmisor',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoEmisor,  
				anchor: '85%',
				listeners:{
					select: function(combo){
							if(valorEmisor == combo.getValue()){
								return ;
							}	
							valorEmisor = combo.getValue();
							revisarSelecciones();
					}				
				},																
				width:470
			},			
			{
				xtype: 'combo',
				fieldLabel: 'Cod. Operaci�n',
				forceSelection: true,
				autoSelect: true,
				name:'claveOperacion',
				id:'idComboOperacion',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoOperacion,  
				anchor: '98%',
				width:470
		},					
		{
			xtype: 'compositefield',
			id: 'cfBase',
			hidden: false,
			fieldLabel:'Base de Operaci�n Din�mica',
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'tipo_base',
					id: 'radioBaseNo',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											operacionDinamica = "N";
										}
									}
					}					
				},	
				{
					xtype: 'displayfield',
					value: 'No	',
					width: 100
				},
				{ 
					xtype: 'radio',
					name: 'tipo_base',
					id: 'radioBaseSi',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											operacionDinamica = "S";
										}
									}
					}
					
				},
				{
					xtype: 'displayfield',
					value: 'Si ',
					width: 100
				}				
			]
		},
		{
			xtype: 'compositefield',
			id: 'cfAlta',
			hidden: false,
			fieldLabel:'Alta Autom�tica de Garant�a',
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'tipo_alta',
					id: 'radioAltaNo',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											automaticaGarantia = "N";
										}
									}
					}
				},	{
					xtype: 'displayfield',
					value: 'No	',
					width: 100
				}, 				{ 
					xtype: 'radio',
					name: 'tipo_alta',
					id: 'radioAltaSi',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											automaticaGarantia = "S";
										}
									}
					}
				},{
					xtype: 'displayfield',
					value: 'Si ',
					width: 100
				}
			]
		},		
		{
					xtype:'numberfield',
					height: 20,
					fieldLabel: '*Cod. Producto Banco',
					decimalPrecision: 0,
					width: 70,
					maxLength: 6,
					value: '',
					id:'numCodBanco',
					anchor: '40%'
		},	
		{
				xtype: 'combo',
				fieldLabel: '*Periocidad de Pago a Capital',
				forceSelection: true,
				autoSelect: true,
				name:'clavePeriocidad',
				id:'idComboPeriocidad',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoPeriocidad, 
				listeners:{
					select: function(combo){
								if(valorPeriocidad == combo.getValue()){
									return ;
								}

								valorPeriocidad = combo.getValue();
								revisarSelecciones();
					}		
				},					
				anchor:	'50%'
		},
		{
				xtype: 'combo',
				fieldLabel: '*Tipo de Inter�s',
				forceSelection: true,
				autoSelect: true,
				name:'claveInteres',
				id:'idComboInteres',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: dataInteres,  
				anchor: '50%',
				value: ''
		},
		{
			xtype: 'compositefield',
			id: 'cfRenta',
			fieldLabel:'Tipo Renta',
			hidden:false,
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'tipo_renta',
					id: 'radioRentaNa',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											renta = "";

										}
									}
					}	
				},	{
					xtype: 'displayfield',
					value: 'N/A	',
					width: 100
				}, 				
				{ 
					xtype: 'radio',
					name: 'tipo_renta',
					id: 'radioRentaSi',
					disabled: true,
					listeners:{
						check:	function(radio){
										if (radio.checked){
											renta = "S";
										}
									}
					}
				},{
					xtype: 'displayfield',
					value: 'Si',
					width: 100
				},	{ 
					xtype: 'radio',
					name: 'tipo_renta',
					disabled: true,
					id: 'radioRentaNo',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											renta = "N";
										}
									}
					}			
				},{
					xtype: 'displayfield',
					value: 'No ',
					width: 200
				}	
			]
		},
		{
					xtype: 'displayfield',
					value: ' ',
					width: 100
		},
		{
					xtype: 	'label',
					id:	 	'leyendaCampos',
					hidden: 	false,
					html:  	'<font size="1">*Campos no obligatorios. En caso de no especificar su valor,�ste se determinar� en el API de acuerdo a las reglas vigentes</h1><BR/>'
		}
	]// fin elementos forma
	


	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: false,
		title: '<center>Mantenimiento a Bases de Operaci�n Cr�dito electr�nico</center>',
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
		buttons:[{
				xtype:	'button',
				id:		'btnConsultar',
				text:		'Consultar',
				iconCls: 'icoBuscar',
				width: 50,
				weight: 70,
				handler: function(boton, evento) { 
						if(Ext.getCmp('idComboIF').getValue() == ''){
							Ext.getCmp('idComboIF').markInvalid('Debe de seleccionar un IF para realizar la consulta');
							return ;
						}
						if(Ext.getCmp('idComboTasa').getValue() == ''){
							Ext.getCmp('idComboTasa').markInvalid('Debe de seleccionar un Tipo de Tasa para realizar la consulta');
							return ;
						}
						if(Ext.getCmp('idComboCredito').getValue() == ''){
							Ext.getCmp('idComboCredito').markInvalid('Debe de seleccionar un Tipo de Cr�dito para realizar la consulta');
							return ;
						}
						if(Ext.getCmp('idComboAmortizacion').getValue() == ''){
								Ext.getCmp('idComboAmortizacion').markInvalid('Debe de seleccionar un Tipo de Amortizaci�n para realizar la consulta');
								return ;
						}
						if(Ext.getCmp('idComboCartera').getValue() == ''){
								Ext.getCmp('idComboCartera').markInvalid('Debe de elegir un tipo de Cartera');
								return ;
						}						ultimoRenglon = "-1";		
												grid.show();

						boton.disable();		
		
					Ext.Ajax.request({
						url: '15basesoperceExt.data.jsp',
						params: {
							informacion: 'ConsiguePlazoMinimo',
							comboIF:Ext.getCmp('idComboIF').getValue(),
							comboTasa:Ext.getCmp('idComboTasa').getValue(),
							comboCredito:Ext.getCmp('idComboCredito').getValue(),
							comboAmortizacion:Ext.getCmp('idComboAmortizacion').getValue(),
							comboCartera:Ext.getCmp('idComboCartera').getValue(),
							comboEmisor:Ext.getCmp('idComboEmisor').getValue(),
							comboPeriocidad:Ext.getCmp('idComboPeriocidad').getValue(),
							tipoRenta: renta,
							tipoPlazo: TipoPlazo							
						},
						callback: procesarPlazoMinimo2
					});					
				},
						style: 'margin:0 auto;',
						align:'rigth'
				},{
						xtype:	'button',
						id:		'btnAceptar',
						text:		'Aceptar',
						iconCls: 'icoAceptar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							fnAceptar();
						}
				},{
						xtype:	'button',
						id:		'btnCancel',
						text:		'Limpiar',
						iconCls: 'icoLimpiar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							window.location = '15basesoperceExt.jsp';
						},
						style: 'margin:0 auto;'
				},{
						xtype:	'button',
						id:		'btnRegresar',
						text:		'Regresar',
						iconCls: 'icoContinuar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							window.location = '15basesoperExt.jsp';
						},
						style: 'margin:0 auto;'
				}],
		labelWidth: 180,
		style: 'margin:0 auto;',	   
		height: 550,
		width: 740
	});


	var grid = new Ext.grid.EditorGridPanel({
		store: consultaDataGrid,
		sortable: true,
		id: 'grid',
		clicksToEdit:1,
		hidden: true,
		columns: [
			{
				header: '<center>Nombre IF</center>',
				dataIndex: 'NOMIF',
				width: 280,
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';								
								return value;
							},				
				align: 'left'				
			},{
				header: '<center>Tasa</center>',
				dataIndex: 'NOMTASA',
				align: 'left',
				width: 260
			},{
				header: '<center>Tipo <BR> Cr�dito</center>',
				dataIndex: 'NOMTIPCRED',
				align: 'left',
				width: 280
			},{
				header: '<center>Tipo <BR> Amortizaci�n</center>',
				dataIndex: 'NOMAMORTIZA',
				align: 'left',
				width: 250
			},{
				header: '<center>Tipo de<BR> Cartera</center>',
				dataIndex: 'CARTERA',
				align: 'left',
				width: 100,
				renderer: RenCartera
			},{
				header: '<center>Tipo Plazo</center>',
				dataIndex: 'PLAZO',
				align: 'left',
				width: 130,
				renderer: RenTipoPlazo
			},{
				header: 'Plazo M�nimo',
				dataIndex: 'PLZMIN',
				align: 'center',
				width: 100
			},{
				header: 'Plazo M�ximo',
				dataIndex: 'PLZMAX',		
				align: 'center',
				editor: { 
								xtype:'numberfield',
								height: 10,
								allowBlank: false,
								maxLength:6,
								decimalPrecision: 0,
								id:'txtmodif',							
								listeners:{
								}				
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					if(registro.get('PLZMAX') == maximo){
						ultimoRenglon = rowIndex;
					}

					if(rowIndex == ultimoRenglon){	
						return NE.util.colorCampoEdit(value,metadata,registro);
					}else{
						return value;
					}
				},	
				width: 110
			},{
				header: '<center>Emisor</center>',
				dataIndex: 'NOMEMISOR',
				align: 'left',
				width: 300
			},{
				header: '<center>C�digo <BR> Operaci�n</center>',
				dataIndex: 'CODIGOBASE',
				align: 'left',
				width: 380,
				editor: {
						xtype: 'combo',
						forceSelection: true,
						autoSelect: true,
						name:'claveOperacion2',
						id:'idComboOperacion2',
						displayField: 'descripcion',
						allowBlank: false,
						editable:true,
						valueField: 'clave',
						mode: 'local',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: CatalogoOperacion2  
				},
				renderer:function(value,metadata,record2,rowIndex,colIndex,store){
								if(ultimoRenglon == rowIndex){
									var record = Ext.getCmp('idComboOperacion2').findRecord(Ext.getCmp('idComboOperacion2').valueField, value);
									record ? txt = record.get(Ext.getCmp('idComboOperacion2').displayField) : txt = value;
									return NE.util.colorCampoEdit(txt,metadata,record2);
								}else {
									//return value;
									var record = Ext.getCmp('idComboOperacion2').findRecord(Ext.getCmp('idComboOperacion2').valueField, value);
									record ? txt = record.get(Ext.getCmp('idComboOperacion2').displayField) : txt = value;					
									return txt;
								}
				}	
			},{
				header: '<center>Base<BR>Operaci�n<BR>Din�mica</center>',
				dataIndex: 'BASEDINAMICA',
				align: 'center',
				width: 80,
				editor: {
						xtype: 'combo',
						forceSelection: true,
						autoSelect: true,
						name:'claveOpciones2',
						id:'idComboOpciones2',
						displayField: 'descripcion',
						allowBlank: false,
						editable:true,
						valueField: 'clave',
						mode: 'local',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: dataOpciones  
				},
				renderer:function(value,metadata,record2,rowIndex,colIndex,store){
						if(ultimoRenglon == rowIndex && record2.get('TAM') == '1' ){
							var record = Ext.getCmp('idComboOpciones').findRecord(Ext.getCmp('idComboOpciones').valueField, value);
							record ? txt = record.get(Ext.getCmp('idComboOpciones').displayField) : txt = value;
							return NE.util.colorCampoEdit(txt,metadata,record2);
						}else {
							//return value;
							var record = Ext.getCmp('idComboOpciones').findRecord(Ext.getCmp('idComboOpciones').valueField, value);
							return record ? record.get(Ext.getCmp('idComboOpciones').displayField) : '';						
						}
				}				
			}, {
				header: '<center>Alta Aut.<BR>Garant. </center>',
				dataIndex: 'ALTA',
				align: 'center',
				width: 80,
				editor: {
						xtype: 'combo',
						forceSelection: true,
						autoSelect: true,
						name:'claveOpciones',
						id:'idComboOpciones',
						displayField: 'descripcion',
						allowBlank: false,
						valueField: 'clave',
						mode: 'local',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: dataOpciones,  
						width:120
				},
				renderer:function(value,metadata,record2,rowIndex,colIndex,store){
								if(ultimoRenglon == rowIndex){
									var record = Ext.getCmp('idComboOpciones').findRecord(Ext.getCmp('idComboOpciones').valueField, value);
									record ? txt = record.get(Ext.getCmp('idComboOpciones').displayField) : txt = value;
									return NE.util.colorCampoEdit(txt,metadata,record2);
								}else {
									//return value;
									var record = Ext.getCmp('idComboOpciones').findRecord(Ext.getCmp('idComboOpciones').valueField, value);
									return record ? record.get(Ext.getCmp('idComboOpciones').displayField) : '';	
								}
				}				
			},{
				header: '<center>C�digo<BR>Producto<BR>Banco</center>',
				dataIndex: 'PRODUCTOBANCO',
				align: 'center',
				width: 80
			},{
				header: '<center>Periodicidad<BR>de Pago a<BR>Capital</center>',
				dataIndex: 'NOMPERIODICIDAD',
				align: 'left',
				width: 80
			},{
				header: '<center>Tipo de<BR>Inter�s</center>',
				dataIndex: 'TIPOINTERES',
				align: 'left',
				width: 80,
				renderer: RenInteres
			},{
				header: '<center>Tipo Renta</center>',
				dataIndex: 'TIPORENTA',
				align: 'center',
				width: 80,
				renderer: RenRenta
			},{
	      xtype: 'actioncolumn',
				header: 'Modificar o Eliminar',
				width: 110,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
										if(ultimoRenglon == rowIndex){
											this.items[0].tooltip = 'Modificar';
											return 'modificar';
										}else{
											return '';	
										}										
						},
						handler: verModificar
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(ultimoRenglon == rowIndex){
								this.items[1].tooltip = 'Eliminar';
								return 'borrar';										
							}else{
								return '';	
							}
						},	
						handler: verEliminar	
					}
				]				
			}			
		],		
		loadMask: true,
		style: 'margin:0 auto;',
		height: 400,
		width: 930,
		frame: true,
		listeners:{												
			beforeedit: function(object){
								if ((object.field == 'CODIGOBASE' || object.field == 'PLZMAX' || object.field == "ALTA") && object.row != ultimo){
									object.cancel = true;
								}
				
								if(object.field == 'BASEDINAMICA' && ultimo != 0){
									object.cancel = true;
								}	
							}//finbefore	
		}//fin listen					
	});


//------ Componente Principal -------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [NE.util.getEspaciador(10),
				  fp,NE.util.getEspaciador(10),grid ]
	});


	CatalogoIF.load();
	CatalogoTasa.load();
	CatalogoCredito.load();
	CatalogoAmortizacion.load();
	CatalogoPeriocidad.load();
	CatalogoEmisor.load();
	CatalogoOperacion.load();

	procesarInicio();

});