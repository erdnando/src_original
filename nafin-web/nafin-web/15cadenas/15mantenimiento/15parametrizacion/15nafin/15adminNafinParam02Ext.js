Ext.onReady(function(){
var convenio='';
var icCuenta;
var VOBO;
var csOpera;
var color;
var color2;
var rowSelection;
var EjecutarCambioCtaCliente='si';
var hidOperaIF;
var reafiliacion;
var cuentaActiva;
var lista;
var listaEpos='';
var instruccionIrrevocable=false;
var Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//-------------Handlers-------------

var cargaCatalogoIF = function (){
	var epo=Ext.getCmp('icEpo').getValue();
	var moneda=Ext.getCmp('cbMoneda').getValue();
	var ic_pyme=Ext.getCmp('hid_ic_pyme').getValue();
	if (epo!=''&&moneda!=''){
		catalogoIF.load({
					params: {epo:epo,moneda:moneda, ic_pyme:ic_pyme}
				});
	}

}
	var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	var procesarCatalogoEpoMulti = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if(arrRegistros=!null){
			if (jsonData.success){
				if(store.getTotalCount()==0){
					catalogoEpoMulti.insert(0,new Todas({ 
						clave: "-1", 
						descripcion: "No existen EPOs a las que se le pueda reasignar la cuenta", 
						loadMsg: ""})); 
					Ext.getCmp('aceptarReafiliacion').setVisible(false);
				}else{
					Ext.getCmp('aceptarReafiliacion').setVisible(true);
					}	
				}
			}
		}
		
	
	
	var procesaConsulta = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesaCuentas = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				Ext.Msg.show({
									title:	"Mensaje",
									msg:		resp.mensaje,
									buttons:	Ext.Msg.OK,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											location.reload();
										}
									},
									closable:false,
									icon: Ext.MessageBox.QUESTION
								});
				
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}
	var procesaCuentaSeleccionada = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			 icCuenta=Ext.util.JSON.decode(response.responseText).IC_CUENTA;
			 VOBO=Ext.util.JSON.decode(response.responseText).CS_VOBO;
			 csOpera=Ext.util.JSON.decode(response.responseText).CS_OPERA;
			 rowSelection='';
			 Ext.getCmp('descuento').setValue(false);
			 Ext.getCmp('select').setValue(false);
			 consultaData.load({params: Ext.apply(fp.getForm().getValues(),{
								ic_pyme: Ext.getCmp('hid_ic_pyme').getValue()
								
							})
						});

		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}
	var procesaCambioCuenta = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

		fp.setVisible(false);
		grid.setVisible(false);
		Ext.Msg.alert("Mensaje de p�gina web.",'Los siguientes intermediarios fueron parametrizados correctamente' );
			
		}else{
				// Mostrar mensaje de error
				Ext.Msg.alert("Mensaje de p�gina web.",'Error al almacenar la parametrizacion de cuentas bancarias' );
		}
	}
	
	//***********Fodea 019-2014
	var validaFideicomisoEPO_PYME = function(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var fp= Ext.getCmp('forma');
			var  jsonData = Ext.util.JSON.decode(response.responseText);
			
			if(jsonData.respuesta=='N')  {
				Ext.Msg.alert(	'Mensaje',"No se puede seleccionar la EPO que opera <br> Fideicomiso para Desarrollo de Proveedores por que la <br> PYME no opera dicho producto ");				
				Ext.getCmp('btnBuscar').disable();	
			}else  {
				Ext.getCmp('btnBuscar').enable();
			}
			fp.el.unmask();
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}
	
var procesarConsultaData = function (store, arrRegistros, opts) {
		
		fp.el.unmask();
		if(arrRegistros!= null) {
			if(!grid.isVisible()) {
				grid.setVisible(true);
				Ext.getCmp('leyenda').show();				
			}

			
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				instruccionIrrevocable = store.reader.jsonData.instruccion;
				el.unmask();
				
					grid.getSelectionModel().selectRow(rowSelection);
				fp2.setVisible(true);
			}else {
				el.mask('No se encontr� ning�n registro','x-mask');
				fp2.setVisible(false);
			}
		}
	}
var accionConsulta = function(estadoSiguiente, respuesta){
if(  estadoSiguiente == "OBTENER_NOMBRE_PYME" 										){
		
			fp.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15adminNafinParam02Ext.data.jsp',	
				params: {
							numeroDeProveedor:	respuesta.noNafinElec,
							ic_banco_fondeo:		respuesta.ic_banco_fondeo,
							ic_epo:					respuesta.ic_epo,
							informacion: 			"obtenNombrePyme" 
				},
				callback: procesaConsulta
			});

		}else if(  estadoSiguiente == "RESPUESTA_OBTENER_NOMBRE_PYME"){
		
			Ext.getCmp('hid_ic_pyme').setValue(respuesta.ic_pyme);
			Ext.getCmp('_txt_nombre').setValue(respuesta.txtCadenasPymes);
			
			fp.el.unmask();
			if (respuesta.muestraMensaje != undefined && respuesta.muestraMensaje){
				Ext.Msg.alert("Mensaje de p�gina web.",	respuesta.textoMensaje);
				Ext.getCmp('_txt_nafelec').setValue('');
				grid.setVisible(false);
				fp2.setVisible(false);
				fp.getForm().reset();
				catalogoEpo.removeAll();
				catalogoMoneda.removeAll();
			}else{
				grid.setVisible(false);
				fp2.setVisible(false);
				Ext.getCmp('icEpo').setValue('');  
				Ext.getCmp('cbMoneda').setValue('');
				Ext.getCmp('intermediario1').setValue('');
				catalogoEpo.load({
					params: {ic_pyme:respuesta.ic_pyme}
				});
				catalogoMoneda.load({
					params: {ic_pyme:respuesta.ic_pyme}
				});
			}

		}

}



var ConfirmaGen=function(selc){
	
	 var selections = selc.getSelections();
		//Realiza la agrupacion de info											
		var i = 0;

	var selections = selc.getSelections();
			  if (selections.length<=0 ||!Ext.getCmp('select').getValue()) {
						
						Ext.MessageBox.alert("Mensaje", "Debes de seleccionar al menos una cuenta para operar con un IF");	
						                 
              }else if(selections.length>0)
			  {
					
					
											CambiarCuentas(selc);
										
					
				}
};
var CambiarCuentas=function(selc){
		
		var selections = selc.getSelections();
		//Realiza la agrupacion de info											
		var i = 0;
		var recordVOBO = "";
		var recordIC_IF="";
			consultaProcesados.removeAll();		

				
				 txtCtaAnterior=icCuenta;
				 rdCta=selections[0].json.IC_CUENTA_BANCARIA;
				 if(Ext.getCmp('select').getValue())
				 chkIF='1';
				 else
				 chkIF='0';
				 
				 if(Ext.getCmp('descuento').getValue())
				 chkOperaIF='1';
				 else
				 chkOperaIF='0';
				 /*{name: 'CG_BANCO'},
					{name: 'CG_NUMERO_CUENTA'},
					{name: 'IC_CUENTA_BANCARIA'},
					{name: 'CS_CONVENIO_UNICO'}
				*/

				Ext.Ajax.request({
							url: '15adminNafinParam02Ext.data.jsp',	
							params: Ext.apply(fp.getForm().getValues(),{
										chkIF:	chkIF,
										chkOperaIF:		chkOperaIF,
										txtCtaAnterior:	txtCtaAnterior,
										rdCta:	rdCta,
										ic_pyme: Ext.getCmp('hid_ic_pyme').getValue(),
										hidOperaIF:hidOperaIF,
										EjecutarCambioCtaCliente:EjecutarCambioCtaCliente,
										informacion: 			"confirmarCuentas" 
							}),
							callback: procesaCuentas
						});
	};
//-------------Stores------------
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15adminNafinParam02Ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catologoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15adminNafinParam02Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoMoneda'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15adminNafinParam02Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  var catalogoEpoMulti = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStoreMulti',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15adminNafinParam02Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpoPymeIF'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarCatalogoEpoMulti,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15adminNafinParam02Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		var catalogoCuenta = new Ext.data.JsonStore({
		id:				'catalogoCuenta',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15adminNafinParam02Ext.data.jsp',
		baseParams:		{	informacion: 'catalogoCuenta'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaProcesados = new Ext.data.JsonStore({

		fields: [
					{name: 'CUENTA'},
					{name: 'MONEDA'},
					{name: 'INTERMEDIARIO'}
		]
	});
	var consultaDataReafiliacion = new Ext.data.JsonStore({
		
		fields: [
					{name: 'CADENAS'},
					{name: 'IC_EPO'}
		]
	});
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15adminNafinParam02Ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
					{name: 'CG_BANCO'},
					{name: 'CG_NUMERO_CUENTA'},
					{name: 'IC_CUENTA_BANCARIA'},
					{name: 'CS_CONVENIO_UNICO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});
//-------------Elementos---------------

var elementosFormaReafiliacion = [
	{
		xtype: 'displayfield',
		name: 'dPyme',
		id: 'dPyme',
		allowBlank: false,
		fieldLabel: 'Nombre Pyme',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dIF',
		id: 'dIF',
		allowBlank: false,
		fieldLabel: 'Nombre IF',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dCuenta',
		id: 'dCuenta',
		allowBlank: false,
		fieldLabel: 'Cuenta Activa',
		value:''
	},{
		xtype: 'multiselect',
		fieldLabel: 'Nombre Epo',
		name: 'HicEpoM',
		hiddenName: 'HicEpoM',
		id: 'icEpoM',
		width: 400,
		height: 200, 
		autoLoad: false,
		hidden: false,
		allowBlank:false,
		mode: 'local',
		store:catalogoEpoMulti,
		displayField: 'descripcion',
		valueField: 'clave',		 
		ddReorder: true,
		listeners: {
			change: function(combo){
			
			}
		}
	}
]

var elementosAyuda = [
	{
				xtype: 	'label',
				id:	 	'leyendaAyda',
				hidden: 	false,
				cls:		'x-form-item',
				style: 	'font size:1; text-align:center;margin:0 auto;',
				html:  	'<b>Seleccione a la Opci�n de la cual desea conocer su forma de operaci�n</b><br/>'
			},
			{
						xtype:'displayfield',
						value:'<br/>'
					},
			{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
					{
						xtype:'displayfield',
						value:'<div class="x-tool x-tool-ayudaLayout x-btn-icon" style="float:left;width:15px;" onclick="Ext.getCmp(\'venanaAyuda_A\').show();">&#160</div>'+'Operar con Descuento'
					},
				{
						xtype: 'checkbox',
						readOnly: true,
						
						name: 'selectAyuda',
						id: 'selectAyuda',
						checked: true,
						hiddenName:'selectAyuda',
						listeners:{
										check:	function(item){
															item.setValue(true);
													}
									}
					},
					{
						xtype:'displayfield',
						value:'<div class="x-tool x-tool-ayudaLayout x-btn-icon" style="float:left;width:15px;" onclick="Ext.getCmp(\'venanaAyuda_B\').show();">&#160</div>'+'Operar con Descuento'
					},
					{
						xtype: 'checkbox',
						readOnly: true,
						name: 'descuentoAyuda',
						id: 'descuentoAyuda',
						listeners:{
										check:	function(item){
															item.setValue(false);
													}
									}
					}
		]
	},
	{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
					{
						xtype:'displayfield',
						value:'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
						'&nbsp;&nbsp;&nbsp;&nbsp;Seleccionada',
						width:			200
					},{
						xtype:'displayfield',
						value:'Sin Seleccionar',
						width:			100
					}
					
					]
	}
	
]

var elementosForma2 = [
			{
				xtype: 	'label',
				id:	 	'leyenda',
				hidden: 	true,
				cls:		'x-form-item',
				style: 	'font size:1; text-align:center;margin:0 auto;',
				html:  	'<font color="Red">(*)Pendiente de autorizar por la IF</font>'
			},
			{
				xtype: 'checkbox',
				fieldLabel: 'Seleccionar',
				name: 'select',
				id: 'select',
				hiddenName:'select'
			},
			{
						xtype: 'checkbox',
						fieldLabel: '<div class="x-tool x-tool-ayudaLayout x-btn-icon" style="float:left;width:15px;"  onclick="Ext.getCmp(\'ventanaAyuda\').show();" >&#160</div>'+'Operar con Descuento',
						name: 'descuento',
						id: 'descuento',
						hiddenName:'descuento'
			}

]
var elementosForma = [

				{
					xtype:'hidden',
					id:	'hid_nombre',
					value:''
				},{
					xtype:'hidden',
					id:	'hid_ic_pyme',
					value:''
				},
				{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
				{
					xtype:		'numberfield',
					name:			'txt_nafelec',
					id:			'_txt_nafelec',
					maskRe:		/[0-9]/,
					allowBlank:	true,
					fieldLabel: 'Num. N@E',
					maxLength:	25,
					listeners:	{
						'change':function(field){
										if ( !Ext.isEmpty(field.getValue())&&field.isValid() ) {
											var respuesta = new Object();
											respuesta["noNafinElec"]		= field.getValue();
											accionConsulta("OBTENER_NOMBRE_PYME",respuesta);

										}
									}
					}
				},{
					xtype:			'textfield',
					name:				'txt_nombre',
					id:				'_txt_nombre',
					width:			257,
					disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
					disabled:		true,
					allowBlank:		true,
					maxLength:		100
					}
					]
				},{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:'displayfield',
							id:'disEspacio',
							text:''
						},
						{
							xtype:	'button',
							id:		'btnBuscaA',
							iconCls:	'icoBuscar',
							text:		'B�squeda Avanzada',
							handler: function(boton, evento) {
										var winVen = Ext.getCmp('winBuscaA');
											if (winVen){
												Ext.getCmp('fpWinBusca').getForm().reset();
												Ext.getCmp('fpWinBuscaB').getForm().reset();
												
												Ext.getCmp('cmb_num_ne').setValue();
												Ext.getCmp('cmb_num_ne').store.removeAll();
												Ext.getCmp('cmb_num_ne').reset();
												winVen.show();
											}else{
												var winBuscaA = new Ext.Window ({
													id:'winBuscaA',
													height: 320,
													x: 300,
													y: 100,
													width: 550,
													heigth: 100,
													modal: true,
													closeAction: 'hide',
													title: 'B�squeda Avanzada',
													items:[
														{
															xtype:'form',
															id:'fpWinBusca',
															frame: true,
															border: false,
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 130,
															items:[
																{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:'Utilice el * para b�squeda gen�rica'
																},{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:''
																},{
																	xtype: 'textfield',
																	name: 'nombre_pyme',
																	id:	'txtNombre',
																	fieldLabel:'Nombre',
																	maxLength:	100
																},{
																	xtype: 'textfield',
																	name: 'rfc_pyme',
																	id:	'txtRfc',
																	fieldLabel:'RFC',
																	maxLength:	20
																},{
																	xtype: 'textfield',
																	name: 'num_pyme',
																	id:	'txtNe',
																	hidden: true,
																	fieldLabel:'N�mero Proveedor',
																	maxLength:	25
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Buscar',
																	iconCls:'icoBuscar',
																	handler: function(boton) {
																					catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {
																					Ext.getCmp('winBuscaA').hide();
																				}
																}
															]
														},{
															xtype:'form',
															frame: true,
															id:'fpWinBuscaB',
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															monitorValid: true,
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 80,
															items:[
																{
																	xtype: 'combo',
																	id:	'cmb_num_ne',
																	name: 'ic_pyme',
																	hiddenName : 'ic_pyme',
																	fieldLabel: 'Nombre',
																	emptyText: 'Seleccione Proveedor. . .',
																	displayField: 'descripcion',
																	valueField: 'clave',
																	triggerAction : 'all',
																	forceSelection:true,
																	allowBlank: false,
																	typeAhead: true,
																	mode: 'local',
																	minChars : 1,
																	store: catalogoNombreProvData,
																	tpl : NE.util.templateMensajeCargaCombo
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Aceptar',
																	iconCls:'aceptar',
																	formBind:true,
																	//hidden:true,
																	handler: function() {
																					if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																						var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																						var cveP = disp.substr(0,disp.indexOf(" "));
																						var desc = disp.slice(disp.indexOf(" ")+1);
																						Ext.getCmp('hid_ic_pyme').setValue(Ext.getCmp('cmb_num_ne').getValue())
																						Ext.getCmp('_txt_nafelec').setValue(cveP);
																						Ext.getCmp('_txt_nombre').setValue(desc);
																						Ext.getCmp('winBuscaA').hide();
																					}
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {	Ext.getCmp('winBuscaA').hide();	}
																}
															]
														}
													]
												}).show();
											}
										}
						}
					]
				},{
				xtype: 'combo',
				fieldLabel: 'Nombre de la EPO',
				emptyText: 'Seleccionar una EPO',
				displayField: 'descripcion',
				//allowBlank: false,
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'HicEpo',
				id: 'icEpo',
				mode: 'local',
				hiddenName: 'HicEpo',
				forceSelection: true,
				listeners: {
					select: {
						fn: function(combo) {
						
							Ext.getCmp('intermediario1').setValue('');
							cargaCatalogoIF();									
							
							Ext.Ajax.request({
								url: '15adminNafinParam02Ext.data.jsp',	
								params: Ext.apply(fp.getForm().getValues(),{
									ic_pyme: Ext.getCmp('hid_ic_pyme').getValue(),
									informacion: "validaFideicomisoEPO_PYME" 
								}),
								callback: validaFideicomisoEPO_PYME									
							});
						
								
						}
					}
				}
			},
			{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			name:'HcbMoneda',
			id: 'cbMoneda',  
			mode: 'local',
			forceSelection : true,
			//allowBlank: false,
			hiddenName: 'HcbMoneda',
			hidden: false,
			emptyText: 'Seleccionar Moneda',
			store: catalogoMoneda,
			tpl: NE.util.templateMensajeCargaCombo,
			listeners: {
							select: {
								fn: function(combo) {
									Ext.getCmp('intermediario1').setValue('');
									cargaCatalogoIF();
								
								}
							}
					}
			},{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			name: 'intermediario',
			id: 'intermediario1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'intermediario',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIF,
			tpl : NE.util.templateMensajeCargaCombo
		}
	
]

var elementosCambioPanel=[
	{
							xtype:'radiogroup',
							id:	'radioGp',
							columns: 4,
							hidden: false,
							items:[
								{boxLabel: 'Individual',id:'individual', name: 'stat',  checked: true,
									listeners:{
										check:	function(radio){
														if (radio.checked){

														}
													}
									}
								},
								{boxLabel: 'Masiva',id:'masiva', name: 'stat',checked: false,
									listeners:{
										check:	function(radio){
															window.location.href='15adminNafinParam02ExtMas.jsp';
													}
									}
								},
								{boxLabel: 'Consultar Cambios Cta',id:'consulta', name: 'stat',checked: false,
									listeners:{
										check:	function(radio){
														window.location.href='15adminNafinParam02ExtConsulta.jsp';
													}
									}
								},
								{boxLabel: 'Cambio de Cuenta Masiva',id:'cambioM', name: 'stat', checked: false,
									listeners:{
										check:	function(radio){
														window.location.href='15adminNafinParam04Ext.jsp';
													}
									}
								}
							]
						}
]

	var sm = new Ext.grid.CheckboxSelectionModel({
		 header : 'Sel.',tooltip: 'Seleccionar Cuenta Bancaria', checkOnly:true,hideable:true,singleSelect:true, id:'chkIR',width:35
			});
				
var gridReafiliacion = new Ext.grid.GridPanel({
		hidden:true,
		store: consultaDataReafiliacion,
		viewConfig:{markDirty:false},
		width: 400,
		height: 190,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		stripeRows: true,
		loadMask: true,
		frame: true,
		columns: [
			{
				header : 'Cadenas Productivas',	dataIndex : 'CADENAS',
				sortable : true, width : 370, align: 'center',  hidden: false
			}
		]
});
var grid = new Ext.grid.GridPanel({
		store: consultaData,
		viewConfig:{markDirty:false},
		width: 500,
		height: 350,
		id:'grid',
		sm:sm,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		stripeRows: true,
		loadMask: true,
		title: ' ',
		frame: true,
		hidden: true,
		columns: [
			{
				header : 'Cuenta', tooltip: 'Cuenta',	dataIndex : 'INTERMEDIARIO',
				sortable : true, width : 400, align: 'center',  hidden: false,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								convenio='';
								color='';
								color2='';
								
								if(icCuenta==registro.get('IC_CUENTA_BANCARIA')){
								Ext.getCmp('select').setValue(true);
								
								cuentaActiva=registro.get('CG_BANCO')+' ('+registro.get('CG_NUMERO_CUENTA')+')';
									if(VOBO!="N"){
										Ext.getCmp('btnReasignacion').setVisible(true);
										Ext.getCmp('btnCliente').setVisible(true);
									}else{
										color='<font color=red>';
										color2='</font>';
									}
									rowSelection=rowindex;
									if(csOpera=='S'){
										
										Ext.getCmp('descuento').setValue(true);
										hidOperaIF=true;
									}
								}
								if (registro.get('CS_CONVENIO_UNICO')=='S')
									convenio='<br/><b>(Convenio �nico)<b/>';
								return color+registro.get('CG_BANCO')+'<br/>('+registro.get('CG_NUMERO_CUENTA')+')'+convenio+color2;
				}

				
			},{
				header : 'Seleccionar Cuenta Bancaria', tooltip: 'Seleccionar Cuenta Bancaria',
				sortable : true, width : 250, align: 'center',  hidden: true,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
								
								return '';
				}
			},sm
			],
	bbar: {
			buttonAlign: 'right',
			id: 'barra',
			hidden:true,
			displayInfo: true,
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Cambio de Cuenta',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						//boton.disable();
						//boton.setIconClass('loading-indicator');
						ConfirmaGen(sm);
					}
				}
			]
		}
		
	}
);


//-------------Principal-------------------

var panelAyuda_A= new Ext.FormPanel({  
    hidden: false,
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px', 
    html: '<table width="100%" cellpadding="0" cellspacing="0" border="0">'
	+'<tr>'
	+'<td valign="top">'
	+'	<table cellpadding="0" cellspacing="0" border="0" align="center">'
	+'	<tr>'
	+'		<td>'
	+'			<table width="500" cellpadding="0" cellspacing="3" border="0" align="center">'
	+'			<tr class="formas">'
	+'				<td class="formas" align="aling" >'
	+'				<img src="/nafin/00utils/gif/chk_h.gif" border="0"><strong><font color="#0000FF">Operar Descuento </font></strong></td>'
	+'			</tr> '
	+'			<tr class="formas">'
	+'				<td class="formas" align="aling" ><font color="#0000FF">Si el campo se encuentra activado, permitir� la Selecci�n de Documentos (Factoraje) con el intermediario parametrizado.</font></td>'
	+'			</tr>  '
	+'			<tr class="formas">'
	+'				<td class="formas" align="aling" >&nbsp;</td>'
	+'			</tr> '
	+'			<tr class="formas">'
	+'				<td class="formas" align="aling" ><strong>PASO 1.-Realizar parametrizaci�n</strong> </td>'
	+'			</tr> '
	+'			<tr class="formas">'
	+'				<td class="formas" align="aling" colspan="2"><img src="img/pantalla1b.jpg" alt="logo" border="0"  ></td>'
	+'			</tr>'
	+'			<tr class="formas">'
	+'				<td class="formas" align="aling" >&nbsp; </td>'
	+'			</tr>'
	+'			<tr class="formas">'
	+'				<td class="formas" align="aling" ><strong>Paso 2.- Selecci�n de documentos</strong></td>'
	+'			</tr>'
	+'			<tr class="formas">'
	+'				<td class="formas" align="aling" ><font color="#0000FF">Los campos 1,2,3 y 4 deben coincidir con la parametrizaci�n realizada en el <strong>paso 1</strong> </font></td>'
	+'			</tr>'
	+'			<tr class="formas">'
	+'				<td class="formas" align="aling" colspan="2"><img src="img/pantalla2a.jpg" alt="logo" border="0"  ></td>'
	+'			</tr>'
	+'			<tr class="formas">'
	+'				<td class="formas" align="aling" >&nbsp; </td>'
	+'			</tr>'
	+'			</table>'
	+'		</td>'
	+'	</tr>'
	+'</td>'
+'</tr>'
+'</table>'
		
	 });

var panelAyuda_B= new Ext.FormPanel({  
    hidden: false,
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px', 
    html: '<table width="100%" cellpadding="0" cellspacing="0" border="0">'
+'<tr>'
+'	<td valign="top">'
+'		<table cellpadding="0" cellspacing="0" border="0" align="center">'
+'		<tr>'
+'			<td>'
+'				<table width="500" cellpadding="0" cellspacing="3" border="0" align="center">'
+'				<tr class="formas">'
+'					<td class="formas" align="aling" >'
+'					<img src="/nafin/00utils/gif/chk_d.gif" border="0"> <strong><font color="#0000FF">Operar Descuento</font></strong></td>'
+'				</tr> '
+'				<tr class="formas">'
+'					<td class="formas" align="aling" ><font color="#0000FF">Si el campo no se encuentra activado No permitir� la selecci�n del documento (Factoraje) con el intermediario parametrizado.</font></td>'
+'				</tr>   '
+'				<tr class="formas">'
+'					<td class="formas" align="aling" >&nbsp; </td>'
+'				</tr>'
+'				<tr class="formas">'
+'					<td class="formas" align="aling" ><strong>Paso 1 .- Realizar parametrizaci�n</strong></td>'
+'				</tr> '
+'				<tr class="formas">'
+'					<td class="formas" align="aling" colspan="2"><img src="img/pantalla3a.jpg" alt="logo" border="0"  ></td>'
+'				</tr>'
+'				<tr class="formas">'
+'					<td class="formas" align="aling" >&nbsp; </td>'
+'				</tr>'
+'				<tr class="formas">'
+'					<td class="formas" align="aling" ><strong>Paso 2 .- Selecci�n de documentos</strong></td>'
+'				</tr>'
+'				<tr class="formas">'
+'					<td class="formas" align="aling" ><font color="#0000FF">En el combo "Selecci�n de Intermediario Financiero", no mostrar� el nombre del Intermediario Financiero  por que este no fue parametrizado en el <strong>paso 1</strong></font></td>'
+'				</tr>'
+'				<tr class="formas">'
+'					<td class="formas" align="aling" colspan="2"><img src="img/pantalla3b.jpg" alt="logo" border="0"  ></td>'
+'				</tr>'
+'				<tr class="formas">'
+'					<td class="formas" align="aling" >&nbsp; </td>'
+'				</tr>'
+'				<tr class="formas">'
+'					<td class="formas" align="aling" ><strong>Paso 3 .- Obra Publica</strong></td>'
+'				</tr>'
+'				<tr class="formas">'
+'					<td class="formas" align="aling" ><font color="#0000FF">Si la PYME no esta parametrizada en descuento, en la pantalla de  Registros de Cr�dito por Cobrar (Obra publica) en el combo de "Contratista Pyme " mostrar� la PYME</font></td>'
+'				</tr>'
+'				<tr class="formas">'
+'					<td class="formas" align="aling" colspan="2"><img src="img/pantalla4a.jpg" alt="logo" border="0"  ></td>'
+'				</tr>		'
+'				</table>'
+'			</td>'
+'		</tr>'
+'	</td>'
+'</tr>'
+'</table>'
		
	 });

var panelCambioPanel = new Ext.Panel({
		hidden: false,
		height: 'auto',
		width: 840,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
			id: 'forma2',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosCambioPanel
	
});

var fpReafiliacion = new Ext.form.FormPanel({
		hidden: false,
		height: 'auto',
		width: 650,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'formaReafiliacion',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaReafiliacion,
		buttons:
		[
			 {
				text:'Aceptar',
				iconCls: '',
				id: 'aceptarReafiliacion',
				handler: function() {
					lista=Ext.getCmp('icEpoM').getValue().split(',');
					listaEpos=Ext.getCmp('icEpoM').getValue();
					var record;
					consultaDataReafiliacion.removeAll();		
					var regi = Ext.data.Record.create(['CADENAS','IC_EPO']);
					for(var i=0;i<lista.length;i++){
							record=catalogoEpoMulti.getAt(catalogoEpoMulti.find('clave',lista[i]));
							consultaDataReafiliacion.add(	new regi({CADENAS: record.get('descripcion'),IC_EPO:lista[i]}) );
						}
					Ext.getCmp('botonCancela').setVisible(true);
					Ext.getCmp('botonAcep').setVisible(true);
					gridReafiliacion.setVisible(true);
					}
			 },{
			 text:'Cancelar',
			 hidden:false,
			 handler: function() {
					ventanaDetalle.setVisible(false);
					gridReafiliacion.setVisible(false);
					}
			 }
		]
	
});
var fp2 = new Ext.form.FormPanel
  ({
		hidden: true,
		height: 'auto',
		width: 500,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'formaAceptar',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma2,
		buttons:
		[
			 {
				text:'Aceptar',
				iconCls: '',
				handler: function() {
					if(instruccionIrrevocable){
						Ext.Msg.alert('Error','No es posible realizar el cambio de cuenta ya que la PYME opera bajo Instrucci�n Irrevocable');
						return;
					}
					EjecutarCambioCtaCliente='no';
					ConfirmaGen(sm);
					}
			 },{
			 text:'Reasignar Cuenta',
			 hidden:true,
			 id:'btnReasignacion',
			 handler: function() {
					if(instruccionIrrevocable){
						Ext.Msg.alert('Error','No es posible realizar el cambio de cuenta ya que la PYME opera bajo Instrucci�n Irrevocable');
						return;
					}
					Ext.getCmp('aceptarReafiliacion').setVisible(true);
					catalogoEpoMulti.load({params:{
						ic_pyme: Ext.getCmp('hid_ic_pyme').getValue(),
						HcbMoneda: Ext.getCmp('cbMoneda').getValue(),
						intermediario:Ext.getCmp('intermediario1').getValue()
					}
					
					});
					Ext.getCmp('dPyme').setValue(Ext.getCmp('_txt_nombre').getValue());
					Ext.getCmp('dIF').setValue(Ext.getCmp('intermediario1').lastSelectionText);
					Ext.getCmp('dCuenta').setValue(cuentaActiva);
					ventanaDetalle.setVisible(true);
					
					}
			 },{
				text:'Cambio de Cuenta',
				iconCls: '',
				id:'btnCliente',
				hidden:true,
				handler: function() {
					if(instruccionIrrevocable){
						Ext.Msg.alert('Error','No es posible realizar el cambio de cuenta ya que la PYME opera bajo Instrucci�n Irrevocable');
						return;
					}
					EjecutarCambioCtaCliente='si';
					ConfirmaGen(sm);
					}
				}
		]
	});
	
	var ventanaDetalle = new Ext.Window({
			title: '',
			hidden: true,
			width: 920,
			height: 560,
			maximizable: false,
			closable: false,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			items: [fpReafiliacion,gridReafiliacion],
			buttons:[
					{
					xtype:'button',
					text:'Aceptar',
					formBind: true,
					id: 'botonAcep',
					hidden:true,
					handler: function(){
					if(listaEpos!=''){
							Ext.Ajax.request({
								url: '15adminNafinParam02Ext.data.jsp',	
								params: Ext.apply(fp.getForm().getValues(),{
											ic_pyme: Ext.getCmp('hid_ic_pyme').getValue(),
											icCuenta:icCuenta,
											epos:listaEpos,
											
											informacion: 		"reasignarCtas" 
								}),
								callback: procesaCuentas
							});
							}else{
								Ext.Msg.alert('Error','Debe de seleccionar al menos una Epo');
								
							}
						}
					},
					{
					text: 'Cancelar',
					hidden: true,
					id: 'botonCancela',
					handler: function(){
							ventanaDetalle.setVisible(false);
							gridReafiliacion.setVisible(false);
						}
					}
			
				]
		
});
var fpAyuda = new Ext.Panel
  ({
		hidden: false,
		height: 300,
		width: 400,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'formaAyuda',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosAyuda
	});
var ventanaAyuda = new Ext.Window({
			title: 'AYUDA',
			id: 'ventanaAyuda',
			hidden: true,
			width: 400,
			height: 250,
			maximizable: false,
			closable: true,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			items: [fpAyuda]
			
		
});

var ventanaAyudaImgA = new Ext.Window({
			title: '',
			hidden: true,
			id: 'venanaAyuda_A',
			width: 920,
			height: 'auto',
			maximizable: false,
			closable: true,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			items: [panelAyuda_A]
});

var ventanaAyudaImgB = new Ext.Window({
			title: '',
			hidden: true,
			id: 'venanaAyuda_B',
			width: 920,
			height: 'auto',
			maximizable: false,
			closable: true,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			items: [panelAyuda_B]
			
		
});
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Seleccionar',
			  name: 'btnBuscar',
			  id: 'btnBuscar',
			  iconCls: 'icoContinuar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						
						Ext.getCmp('btnReasignacion').setVisible(false);
						Ext.getCmp('btnCliente').setVisible(false);
						if(Ext.getCmp('_txt_nafelec').getValue()==''){
							Ext.getCmp('_txt_nafelec').markInvalid('Seleccione un Proveedor');
							return;
						}
						if(Ext.getCmp('icEpo').getValue()==''){
							Ext.getCmp('icEpo').markInvalid('Seleccione una EPO');
							return;
						}
						
						if(Ext.getCmp('cbMoneda').getValue()==''){
							Ext.getCmp('cbMoneda').markInvalid('Seleccione una Moneda');
							return;
						}
						if(Ext.getCmp('intermediario1').getValue()==''){
							Ext.getCmp('intermediario1').markInvalid('Seleccione un Intermediario Financiero');
							return;
						}
						grid.setTitle('<center> IF: '+Ext.getCmp('intermediario1').lastSelectionText+'</center>');
						
						
						Ext.getCmp('grid').hide();
						fp.el.mask('Procesando...', 'x-mask-loading');	
						
						Ext.Ajax.request({
							url: '15adminNafinParam02Ext.data.jsp',	
							params: Ext.apply(fp.getForm().getValues(),{
							ic_pyme: Ext.getCmp('hid_ic_pyme').getValue(),
							informacion: 			"Consulta2" 
						}),
						callback: procesaCuentaSeleccionada
					});				
						
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
	  panelCambioPanel,NE.util.getEspaciador(10),fp,NE.util.getEspaciador(20),grid,fp2,ventanaDetalle
	  ]
  });
});