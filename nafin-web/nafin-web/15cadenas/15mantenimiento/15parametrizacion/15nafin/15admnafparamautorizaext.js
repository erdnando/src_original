

function selecREAFILIACION_AUTO(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
 
	if(check.checked==true)  {
		reg.set('REAFILIACION_AUTO', "S");
		reg.set('AUX_REAFILIACION_AUTO', "checked");
		
	}else  {
	 reg.set('REAFILIACION_AUTO', "N"); 	
	 reg.set('AUX_REAFILIACION_AUTO', "");
	}	
	store.commitChanges();	
}

function selecCSBLOQUEO(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
 
	if(check.checked===true)  {
		reg.set('CSBLOQUEO', "S");
		reg.set('AUXCSBLOQUEO', "checked");
		
	}else  {
	 reg.set('CSBLOQUEO', "N"); 	
	 reg.set('AUXCSBLOQUEO', "");
	}	
	store.commitChanges();	
}
function selecFACTORAJENORMAL(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
 
	if(check.checked==true)  {
		reg.set('FACTORAJENORMAL', "S");
		reg.set('AUXNORMAL', "checked");
		
	}else  {
	 reg.set('FACTORAJENORMAL', "N"); 			
	 reg.set('AUXNORMAL', "");
	}	
	store.commitChanges();	
}


function selecFactVencido(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
	
	if(check.checked==true)  {
		
		reg.set('OPERACION_VENCIMIENTO', "S");
		reg.set('AUXFACTVENCIDO', "checked");
		
	}else  {
		
	 reg.set('OPERACION_VENCIMIENTO', "N"); 	
	 reg.set('AUXFACTVENCIDO', " ");
	}	
	store.commitChanges();	
}

    function selecFactDistribuido(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
    
	if(check.checked==true)  {
	    reg.set('OPERACION_DISTRIBUIDO', "S");
	    reg.set('AUXFACTDISTRIBUIDO', "checked");
	}else  {
	    reg.set('OPERACION_DISTRIBUIDO', "N"); 	
	    reg.set('AUXFACTDISTRIBUIDO', " ");
	}	
	store.commitChanges();	
    }


function selecCUENTA_ACTIVA(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
 
	if(check.checked==true)  {
		reg.set('CUENTA_ACTIVA', "S");
		reg.set('AUXCTAACTIVA', "checked");
		
	}else  {
	 reg.set('CUENTA_ACTIVA', "N"); 			
	 reg.set('AUXCTAACTIVA', "");
	}	
	store.commitChanges();	
}

function selecAUTORIZACION(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
 
	if(check.checked==true)  {
		reg.set('AUTORIZACION', "S");	
		reg.set('AUX_AUTORIZACION', "checked");		
	}else  {
	 reg.set('AUTORIZACION', "N"); 
	 reg.set('AUX_AUTORIZACION', "");
	}	
	store.commitChanges();	
}



function selecCSBENEFICIARIO(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
 
	if(check.checked===true)  {
	    reg.set('CSBENEFICIARIO', "S");	
	    reg.set('AUX_BENEF', "checked");
	    Ext.MessageBox.alert("Mensaje","Favor de Capturar el N�mero de Referencia del Beneficiario.");
	}else  {
	 reg.set('CSBENEFICIARIO', "N"); 
	 reg.set('AUX_BENEF', "");
	}	
	store.commitChanges();	
	
}


function selecCSCONVENIOUNICO(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
 
	if(check.checked==true)  {
		reg.set('CSCONVENIOUNICO', "S");	
		reg.set('AUX_CONVENIO', "checked");
	}else  {
	 reg.set('CSCONVENIOUNICO', "N"); 
	 reg.set('AUX_CONVENIO', "");
	}	
	store.commitChanges();	
}
			
Ext.onReady(function() {

	var chkEPO = [];
   var porcentaje = [];
	var porcentaje_dl = [];
	var csBeneficiario_aux = [];
	var if_epo =[];
	var csBloqueo = [];
	var csBloqueo_aux = [];
	var csConvUnico_aux =[];
	var csFactoNormal_aux =[];
	var csFactoVencido_aux =[];
	var csCuentaActiva_aux = [];
	var csReafiAutomatico = [];
	
	var csfactoDistribuido = [];
	var csReferencia = [];
	
	
	
	
	var cancelar =  function() {  
		chkEPO = [];
		porcentaje = [];
		porcentaje_dl = [];
		csBeneficiario_aux = [];
		if_epo =[];
		csBloqueo = [];
		csBloqueo_aux = [];
		csConvUnico_aux =[];
		csFactoNormal_aux =[];
		csCuentaActiva_aux = [];
		csReafiAutomatico = [];
		csFactoVencido_aux =[];
		
		csfactoDistribuido = [];
		csReferencia = [];
	
	}
	
	
	var enviarArchivo = function() {
		var tipoArchivo='0';		
		var tipoArchivo1 = Ext.getCmp("tipoArchivo1").getValue();
		var tipoArchivo2 = Ext.getCmp("tipoArchivo2").getValue();
		var archivo = Ext.getCmp('archivo');
		
		if(tipoArchivo1 ==true) tipoArchivo= "1";
		if(tipoArchivo2 == true) tipoArchivo= "2";
		
		if(tipoArchivo=='0'){
			Ext.MessageBox.alert("Mensaje","Por favor seleccione el archivo para subir");	
			return;				
		}				
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid('Favor de cargar un archivo');
			archivo.focus();
			return;
		}
						
		
		var noEpo = Ext.getCmp("noEPOC").getValue();
		var noIf = Ext.getCmp("noIFC").getValue();
		
		var parametros = "noEpo="+noEpo+"&noIf="+noIf+"&tipoArchivo="+tipoArchivo;
		
		fpCarga.getForm().submit({
			 url: '15admnafparamautoriza.ma.jsp?'+parametros,									
			 waitMsg: 'Enviando datos...',
			 waitTitle :'Por favor, espere',			
			success: function(form, action) {
				var resp = action.result;
				var resultado = resp.resultado;
				var mensaje = resp.mensaje;
			
				if(resultado=='S') {
					Ext.MessageBox.alert('Mensaje','El Archivo ha sido Cargado ',
					function(){ 					
						Ext.getCmp('VerArchivo').hide();
						fp.el.mask('Enviando...', 'x-mask-loading');			
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
							})
						});
						Ext.getCmp("btnAceptar").disable();					
					});
					
				}else if(resultado=='N') {
					Ext.MessageBox.alert("Mensaje",mensaje);
					return;
				}								
			}
			,failure: NE.util.mostrarSubmitError
		});			
	}
	

	function fnModificarCargaArchivoGridPrincipal(grid, rowIndex, colIndex, item, e){
		var record = grid.getStore().getAt(rowIndex);
		var noEpo =  record.data['IC_EPO'] ;
		var noIf =  record.data['IC_IF'] ;
		Ext.getCmp("noEPOC").setValue(noEpo);
		Ext.getCmp("noIFC").setValue(noIf);
		Ext.getCmp("archivo").setValue('');		
		Ext.getCmp("btnAceptar").enable();			
		var ventana = Ext.getCmp('VerArchivo');
		
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				x: 20,
				y: 100,
				width: 840,	
				id:'VerArchivo',
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'hide',	
				items: [					  
					fpCarga
				],
				title: 'Carga Archivo'			
			}).show();
		}
			
	}

	var myValidFn = function(v) {
		var myRegex = /^.+\.([pP][dD][fF])$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivopdf 		: myValidFn,
		archivopdfText 	: 'Favor de cargar un archivo con formato PDF.'
	});
	
	
	var fpCarga = new Ext.form.FormPanel({
		id:'fpCarga',	
		width: 830,
		title: '',
		frame: true,	
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 30,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},	
		items: [	
			{ 	xtype: 'textfield',  hidden:true,  id: 'noEPOC', 	value: '' },	
			{ 	xtype: 'textfield',  hidden:true,   id: 'noIFC', 	value: '' }	,
			{  
				xtype:  'radiogroup',   
				fieldLabel: "",    
				name: 'tipoArchivo',   				 
				value: '',  			 
				columns:  1,			
				items:         
					[         
						{ 
							boxLabel:    "Cargar Archivo Convenio (Solicitud y Parametrizaci�n de Afiliaci�n Electr�nica)",             
							name:        'tipoArchivo', 
							id:        'tipoArchivo2',
							inputValue:  "2" ,
							width: 20
						},         
						{           
							boxLabel: "Cargar Archivo Informaci�n IF (Solicitud de Afiliaci�n Electr�nica)",             
							name: 'tipoArchivo',  
							id:  'tipoArchivo1',
							inputValue:  "1",  
							width: 20	
						}   
					],   
					style: {      
						paddingLeft: '10px'   
					},
					listeners : {
						change : {
							fn: function(radio) {							
								var valor = radio.getValue().inputValue;									
								Ext.getCmp("archivo").setValue('');	
								Ext.getCmp("cargaArchivo").show();									
						}
					}
				}	
			},		
			{
				xtype: 'panel',
				id:		'pnlArchivo',				
				layout: 'form',
				fileUpload: true,
				labelWidth: 150,					
				items: [
					{
						xtype: 'compositefield',
						fieldLabel: '',
						id: 'cargaArchivo',
						combineErrors: false,
						msgTarget: 'side',
						hidden: true,
						width: 400,
						items: [														
							{
								xtype: 'fileuploadfield',
								id: 'archivo',
								emptyText: '',
								fieldLabel: '',
								name: 'archivo',   
								buttonText: 'Examinar..',
								width: 320,	  																		
								vtype: 'archivopdf'
							}						
						]
					}
				]
			}
		],
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivo
			}			
		]
	});
	
	
	   
		//muestra el archivo de la Columna  Carga de Archivo
	var procesarSuccessFailureArchivo =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	//descarga archivo
	var descargaArchivo= function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var noEpo = registro.get('IC_EPO');
		var noIf = registro.get('IC_IF');		
	
		Ext.Ajax.request({
			url: '15admnafparamautorizaext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'DescargaArchivo',
				noEpo: noEpo,
				noIf:noIf
			}),
			callback: procesarSuccessFailureArchivo
		});
	}
	
	
   // Guardo Modifica Moneda
   function fnGuardarModificaMoneda(boton, evento){   
		var store = Ext.getCmp('gridModificaMoneda').getStore();
   	var vMoneda = [];
   	var seleccion = [];
   	var actualizaMoneda = true;
   	var numRegistros=0;	
		var noEpo;
		var noIf;
   	store.data.each(function(item, index, totalItems){    				
			var campoPermitir 		= item.get('V_PERMITIR');
			var campoSeleccion		= Ext.getDom('chkMoneda_' + index); 
			var campoNombreMoneda 	= item.get('V_DESC_MONEDA');
			noEpo 	= item.get('IC_EPO');
			noIf 	= item.get('IC_IF');
			vMoneda[index] 	= item.get('V_MONEDA');
			seleccion[index] 	= item.get('CHECKBOX');
   		numRegistros++;	
			if(campoPermitir=='N' && !campoSeleccion.checked){
				Ext.Msg.show({
					title:	'Mensaje',
					msg:		'No puede desasignar la moneda '+campoNombreMoneda+' debido a que ya fue autorizada la tasa',
					icon:		Ext.Msg.INFO,
					buttons:	Ext.Msg.OK
				});
				actualizaMoneda = false;
				return;
			}
			if(campoSeleccion.checked){
				seleccion[index] = 'S'
			}else{
				seleccion[index] = 'N'						
			}
					
   	});
   	// SUBMIT / INSERT
   	if(actualizaMoneda){   			
			Ext.Ajax.request({
				url: '15admnafparamautorizaext.data.jsp',
				method: 'POST',
				params: {
					informacion: 'insertaMoneda',
					numRegistros: numRegistros,
					noEpo: noEpo,
					noIf: noIf,
					vMoneda: vMoneda,
					seleccion: seleccion
				},
				success: function(opts, success){
					var jsonObj = Ext.util.JSON.decode(opts.responseText);	
					if(jsonObj.ESTATUSMENSAJE=='ERROR'){
						Ext.Msg.show({
							title:	'Mensaje',
							msg:		jsonObj.MENSAJE,
							icon:		Ext.Msg.ERROR,
							buttons:	Ext.Msg.OK
						});
					}else{
					
						Ext.MessageBox.alert('Mensaje',jsonObj.MENSAJE,
						function(){ 					
							Ext.getCmp('VerMonedas').hide();
							fp.el.mask('Enviando...', 'x-mask-loading');			
							consultaData.load({
								params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar'
								})
							});									
						});
						
						
					}
				}
			});
		} // FIN Actualiza Moneda		
   }
	
	
	function fnVerMonedaGridPrincipal(grid, rowIndex, colIndex, item, e){
		var record = grid.getStore().getAt(rowIndex);
		var noEpo =  record.data['IC_EPO'] ;
		var noIf =  record.data['IC_IF'] ;
		
		var dataMoneda = [ [record.data['CADENA'] , record.data['INTERMEDIARIOFINANCIERO'], record.data['BANCO']  ] ];
		storeGridMonedaData.loadData(dataMoneda);
		
		storeGridMonedaModificaData.load({
			params: {
				informacion: 'ConsultarMoneda',
				noEpo:noEpo,
				noIf:noIf
			}
		});
			
		var ventana = Ext.getCmp('VerMonedas');			
			if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({					
					x: 20,
					y: 100,
					width: 840,
					id:'VerMonedas',
					modal: true,
					autoHeight: true,
					resizable:false,
					closeAction: 'hide',			
					items: [  gridMoneda , NE.util.getEspaciador(10), gridModificaMoneda ],					
					title: 'Monedas',
					bbar: {
					xtype: 'toolbar',	
					buttons: [
						'-',
						'->',
						{	xtype: 'button', 	buttonAlign:'right', 	text: 'Guardar', iconCls: 'icoContinuar', id: 'btnGuardarM', handler: fnGuardarModificaMoneda }						
					]
				}
			}).show();
		}
			
	}
	
	var storeGridMonedaData = new Ext.data.ArrayStore({
		fields: [			
			{name: 'CADENA' },
			{name: 'INTERMEDIARIO'},	
			{name: 'BANCO'}	
		]	
	});
	
	var gridMoneda = {
		xtype: 'grid',
		id: 'gridMoneda',
		store: storeGridMonedaData,	
		title: '',			
		columns: [	
			{
				header: 'Cadena',
				dataIndex: 'CADENA',
				width: 250,
				align: 'center'				
			},
			{
				header: 'Intermediario financiero',
				dataIndex: 'INTERMEDIARIO',
				width: 250,
				align: 'left'				
			},
			{
				header: 'Banco',
				dataIndex: 'BANCO',
				width: 250,
				align: 'left'				
			}	
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height:110,	
		width:800,
		frame: true	,
	   style: 'margin:0 auto;'		
	};
	
	var procesarConsultaMonedaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridModificaMoneda = Ext.getCmp('gridModificaMoneda');	
	
		if (arrRegistros != null) {
			if (!gridModificaMoneda.isVisible()) {
				gridModificaMoneda.show();
			}						
			//edito el titulo de la columna
			var el = gridModificaMoneda.getGridEl();
			var cm = gridModificaMoneda.getColumnModel();
			var jsonData = store.reader.jsonData;	
								
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var storeGridMonedaModificaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15admnafparamautorizaext.data.jsp',
		baseParams: {
			informacion: 'ConsultarMoneda'
		},		
		fields: [			
			{	name: 'MONEDA'	},
			{	name: 'V_MONEDA'	},
			{	name: 'V_DESC_MONEDA'	},
			{  name: 'V_PERMITIR'	},
			{	name: 'MONEDA'	},
			{	name: 'MONEDA'	},
			{	name: 'ASOCIADA'},
			{  name: 'CHECKBOX'}	,
			{  name: 'IC_IF'}	,
			{  name: 'IC_EPO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaMonedaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaMonedaData(null, null, null);					
				}
			}
		}		
	});
	
	

	
	var gridModificaMoneda = {
		xtype: 'grid',
		id: 'gridModificaMoneda',
		store: storeGridMonedaModificaData,	
		title: '',			
		columns: [	
			{
				header: 'Moneda',
				dataIndex: 'MONEDA',
				width: 210,
				align: 'center'				
			},
			{
				header: '�Asociar?',
				dataIndex: 'CHECKBOX',
				sortable: false,
				hideable: false,
				width: 265,
				align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					var asociada = record.data.ASOCIADA=='true'?'checked':'';
					var cvePerf = record.data.cvePerf;
					if(cvePerf=='8'){
						return '<input class="x-row-checkbox" style="height: 12px;" name="chkMoneda_'+rowIndex+'" id="chkMoneda_'+rowIndex+'" value="'+value+'" type="checkbox" '+asociada+' disabled />';
					}
					return '<input class="x-row-checkbox" style="height: 12px;" name="chkMoneda_'+rowIndex+'" id="chkMoneda_'+rowIndex+'" value="'+value+'" type="checkbox" '+asociada+' />';
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 100,
		width: 500,		
		frame: true	,
	   style: 'margin:0 auto;'		
	};
	
	//Respuesta de Guarda Relaci�n EPO-IF 
	function transmiterRelEPO_IF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.Msg.alert(
				'Mensaje',
				info.mensaje,
				function(btn, text){
				window.location = '15admnafparamautorizaext.jsp';	
				}
			);
				
			

		} else {
				NE.util.mostrarConnError(response,opts);							
		}
	}
	
	// valida  al guardar 
	var procesarR_E_I = function() {
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		var jsonData = store.data.items;
		cancelar();		
		var bandera =0;
		
		Ext.each(jsonData, function(item,inx,arrItem){
				
			
			if(item.data.PORCENTAJE_MN !='' ) {
				if(item.data.PORCENTAJE_MN == '0') {
					Ext.MessageBox.alert(' Mensaje','El porcentaje de Descuento debe ser mayor a Cero');
					bandera++;
					return;
				}else  	if( parseFloat(item.data.PORCENTAJE_MN )> parseFloat(item.data.FN_AFORO_EPO)  && parseFloat(item.data.FN_AFORO_EPO)>0) { 
					Ext.MessageBox.alert(' Mensaje','El Registro con la cadena '+item.data.CADENA+'\n'+
					' y el intermediario '+item.data.INTERMEDIARIOFINANCIERO+'\n'+
					' sobre pasa el Porcentaje Autorizado \n que es de: '+item.data.FN_AFORO_EPO);
					bandera++;
					return;
				}else  if( parseFloat(item.data.PORCENTAJE_MN) > parseFloat(item.data.FN_AFORO_PRONAF)  ) { 
					Ext.MessageBox.alert(' Mensaje','El Registro con la cadena '+item.data.CADENA+'\n'+
					' y el intermediario '+item.data.INTERMEDIARIOFINANCIERO+'\n'+
					' sobre pasa el Porcentaje Autorizado \n que es de: '+item.data.FN_AFORO_EPO);
					bandera++;
					return;					
				}							
			}
			
			if( item.data.PORCENTAJE_DOL !='' ) {
			
				if(item.data.PORCENTAJE_DOL == '0') {
					Ext.MessageBox.alert(' Mensaje','El porcentaje de Descuento debe ser mayor a Cero');
					bandera++;
					return;
					
				}else  if( parseFloat(item.data.PORCENTAJE_DOL) > parseFloat(item.data.FN_AFORO_EPO)  && parseFloat(item.data.FN_AFORO_EPO)>0) { 
					Ext.MessageBox.alert(' Mensaje','El Registro con la cadena '+item.data.CADENA+'\n'+
					' y el intermediario '+item.data.INTERMEDIARIOFINANCIERO+'\n'+
					' sobre pasa el Porcentaje Autorizado \n que es de: '+item.data.FN_AFORO_EPO );
					bandera++;
					return;
			
				}else  if( parseFloat(item.data.PORCENTAJE_DOL) > parseFloat(item.data.FN_AFORO_PRONAF_DL)  ) { 
					Ext.MessageBox.alert(' Mensaje','El Registro con la cadena '+item.data.CADENA+'\n'+
					' y el intermediario '+item.data.INTERMEDIARIOFINANCIERO+'\n'+
					' sobre pasa el Porcentaje Autorizado \n que es de: '+item.data.FN_AFORO_EPO);
					bandera++;
					return;
				}	
			}
			
			if(item.data.CUENTA_ACTIVA==''){
				if(item.data.VOBO_NAFIN =='N'){
					if(item.data.CLAVEPERFIL=='8'){
						if(item.data.AUTORIZACION=='S'){
							chkEPO.push(item.data.IC_EPO +'|'+item.data.IC_IF);
						}
					}else{
						if(item.data.AUTORIZACION=='S'){
							chkEPO.push(item.data.IC_EPO +'|'+item.data.IC_IF);
						}
					}
				}
			}	
			
			
			csConvUnico_aux.push(item.data.CSCONVENIOUNICO);	
			csBeneficiario_aux.push(item.data.CSBENEFICIARIO);			
			porcentaje.push(item.data.PORCENTAJE_MN);	
			porcentaje_dl.push(item.data.PORCENTAJE_DOL);	
			if_epo.push(item.data.IC_IF +'|'+item.data.IC_EPO);
			csFactoNormal_aux.push(item.data.FACTORAJENORMAL);
			csFactoVencido_aux.push(item.data.OPERACION_VENCIMIENTO);//csFactoVencido_aux
			csCuentaActiva_aux.push(item.data.CUENTA_ACTIVA);	
			if(item.data.CS_BLOQUEO_AUX!='') {
				csBloqueo.push( item.data.CSBLOQUEO +'|'+ item.data.IC_EPO +'|'+item.data.IC_IF);	
				csBloqueo_aux.push(item.data.CS_BLOQUEO_AUX );
			}	
			
			csReafiAutomatico.push(item.data.REAFILIACION_AUTO +'|'+ item.data.IC_EPO +'|'+item.data.IC_IF);
			csfactoDistribuido.push(item.data.OPERACION_DISTRIBUIDO); //2017-03 DLHC
			csReferencia.push(item.data.REFERENCIA); //2017-03 DLHC 
			
			
			
		});
		
	
		if(bandera==0) {
			Ext.Ajax.request({
				url: '15admnafparamautorizaext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'GuardarRelacionEPO_IF',
					chkEPO:chkEPO,
					porcentaje:porcentaje,		
					porcentaje_dl:porcentaje_dl,		
					csBeneficiario_aux:csBeneficiario_aux,
					if_epo:if_epo,
					csBloqueo:csBloqueo,
					csBloqueo_aux:csBloqueo_aux,
					csConvUnico_aux:csConvUnico_aux,
					csFactoNormal_aux:csFactoNormal_aux,
					csFactoVencido_aux:csFactoVencido_aux,
					csCuentaActiva_aux:csCuentaActiva_aux,
					csReafiAutomatico:csReafiAutomatico,
					csfactoDistribuido:csfactoDistribuido,
					csReferencia:csReferencia
					
					}),
					callback: transmiterRelEPO_IF
				});
		}			
	
	}
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;	
				
			if(jsonData.cvePerf=='8'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CARGAARCHIVO'), true);	
			}else  {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CARGAARCHIVO'), false);
			}
				
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15admnafparamautorizaext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{  name: 'CUENTA_ACTIVA'  },
			{  name: 'VOBO_NAFIN'  },
			{  name: 'CLAVEPERFIL'  },
			{  name: 'BLOQUEO'  },
			{  name: 'COLOR'  },
			{  name: 'CADENA'  },
			{  name: 'INTERMEDIARIOFINANCIERO'  },
			{  name: 'BANCO'  },
			{  name: 'AUTORIZACION'  },
			{  name: 'PORCENTAJE_MN'  },
			{  name: 'PORCENTAJE_DOL'  },
			{  name: 'AUX_BENEF'  },
			{  name: 'CSBENEFICIARIO'  },
			{  name: 'CSBENEFICIARIO_AUX'  },
			{  name: 'CONVENIO_UNICO'  },
			{  name: 'AUX_CONVENIO'  },
			{  name: 'CSCONVENIOUNICO'  },
			{  name: 'CS_CONVENIO_UNICO_AUX'  },
			{  name: 'APLICA_CONVENIO_UNICO'  },
			{  name: 'IC_EPO'  },
			{  name: 'IC_IF'  },
			{  name: 'FN_AFORO_EPO'  },
			{  name: 'FN_AFORO_PRONAF'  },
			{  name: 'FN_AFORO_PRONAF_DL'  },
			{  name: 'BLOQUEO'  },
			{  name: 'CS_BLOQUEO_AUX'  },
			{  name: 'CSBLOQUEO'  },
			{  name: 'FECHABLOQUEO'  },
			{  name: 'USUARIO'  },
			{  name: 'FACTORAJENORMAL'  },
			{  name: 'AUXNORMAL'  },
			{  name: 'AUXFACTVENCIDO'  },
			{  name: 'CSFACTORAJENORMAL_AUX'  },
			{  name: 'APLICAFACTONORMAL'  },
			{  name: 'AUXCTAACTIVA'  },
			{  name: 'CUENTA_ACTIVA'  },
			{  name: 'CUENTA_ACTIVA_AUX'  },
			{  name: 'APLICACUENTAACTIVA'  },
			{  name: 'FECHA_CONVENIO'  },
			{  name:'AUXCSBLOQUEO' },
			{  name: 'CARGAARCHIVO' },
			{  name: 'REAFILIACION_AUTO' },
			{  name: 'AUX_REAFILIACION_AUTO' },
			{  name: 'APLICA_REAFILIACION_AUTO' },
			{  name: 'CS_ESTATUS_REAFILIACION' },
			{  name: 'FACTORAJE_VENCIDO' },
			{  name: 'OPERACION_VENCIMIENTO' },
			{  name: 'FIDEICOMISO_EPO' },
			{  name: 'FIDEICOMISO_IF' },
			
			{  name: 'FACTORAJE_DISTRIBUIDO' },
			{  name: 'OPERACION_DISTRIBUIDO' },
			{  name: 'AUXFACTDISTRIBUIDO' },	
			{  name: 'REFERENCIA' }
					
			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title: 'Relaci�n EPO-IF',
		clicksToEdit: 1,
		columns: [		
		{
				header: 'Cadena', 
				tooltip: 'Cadena',
				dataIndex: 'CADENA',
				sortable: true,
				resizable: true	,
				width: 200,			
				align: 'left',				
				renderer:function(value,metadata,registro){
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					var color = registro.data['COLOR'];	
					if(color !='') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Intermediario Financiero', 
				tooltip: 'Intermediario Financiero',
				dataIndex: 'INTERMEDIARIOFINANCIERO',
				sortable: true,
				resizable: true	,
				width: 200,			
				align: 'left',
				renderer:function(value,metadata,registro){                                
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					var color = registro.data['COLOR'];	
					if(color !='') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Banco', 
				tooltip: 'Banco',
				dataIndex: 'BANCO',
				sortable: true,
				resizable: true	,
				width: 200,			
				align: 'left',
				renderer:function(value,metadata,registro){ 
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					var color = registro.data['COLOR'];	
					if(color !='') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Autorizaci�n',
				tooltip: 'Autorizaci�n',
            dataIndex: 'AUTORIZACION',
            sortable: false,
            hideable: false,
            menuDisabled: true,
            width: 80,
            align: 'center',
            renderer: function(value, metadata, record, rowIndex, colIndex, store){
            	if(record.data['CUENTA_ACTIVA']==''){
            		if(record.data['VOBO_NAFIN'] =='N'){
            			if(record.data['CLAVEPERFIL']=='8'){
								return '<input id="chkAUTORIZACION"'+ rowIndex + 'value="'+value+'" type="checkbox" '+record.data['AUX_AUTORIZACION']+' disabled   onclick="selecAUTORIZACION(this,'+rowIndex +','+colIndex+');" />';
            			}else{							
            				return '<input id="chkAUTORIZACION"'+ rowIndex + 'value="'+value+'" type="checkbox"  '+record.data['AUX_AUTORIZACION']+'  onclick="selecAUTORIZACION(this,'+rowIndex +','+colIndex+');" />';
            			}
            		}else{
            			return value;
            		}
            	}else{
            		return value;
            	}
            }
         },
			{ 
				header: 'Porcentaje de <br/>Descuento <br/>IF-EPO M.N.',
            tooltip: 'Porcentaje de <br/>Descuento <br/>IF-EPO M.N.',
			   dataIndex: 'PORCENTAJE_MN',
            sortable: false,
            hideable: false,
            width: 80,
            align: 'center',
				editor: { 
					xtype:'numberfield',
					maxValue : 99999,
					allowBlank: true
				},
            renderer: function(value, metadata, record, rowIndex, colIndex, store){            	
					return NE.util.colorCampoEdit(value,metadata,record);					
            }
         },
			 {
         	header: 'Porcentaje de <br>Descuento <br/>IF-EPO Dol.',
				tooltip: 'Porcentaje de <br>Descuento <br/>IF-EPO Dol.',
            dataIndex: 'PORCENTAJE_DOL',
           // sortable: false,
            hideable: false,			
            width: 80,
            align: 'center',
				editor: { 
					xtype:'numberfield',
					maxValue : 99999,
					allowBlank: true
				},
            renderer: function(value, metadata, record, rowIndex, colIndex, store){
					return NE.util.colorCampoEdit(value,metadata,record);	
            }
         },
	 		
         {
            header: 'Beneficiario <br/>/ <br/>Entidad',
				tooltip:'Beneficiario <br/>/ <br/>Entidad',
            dataIndex: 'CSBENEFICIARIO',
            sortable: false,
            hideable: false,
            width: 70,
            align: 'center',
            renderer: function(value, metadata, record, rowIndex, colIndex, store){
	   
		if(record.data['FACTORAJE_DISTRIBUIDO']==='S'){
		
		    if(record.data['CLAVEPERFIL']=='8'){
			return '<input id="chkCSBENEFICIARIO"'+ rowIndex + 'value='+value+' type="checkbox" '+record.data['AUX_BENEF']+'  onclick="selecCSBENEFICIARIO(this,'+rowIndex +','+colIndex+');" />';							
		    }else{
			if(record.data['FIDEICOMISO_EPO']=='S')  {
			    return "N/A";
			}else {
			    if(record.data['CSBLOQUEO']=='S' &&  record.data['CSBLOQUEO'] ){
				return '<input  id="chkCSBENEFICIARIO"'+ rowIndex + 'value='+value+' type="checkbox" '+record.data['AUX_BENEF']+' disabled  onclick="selecCSBENEFICIARIO(this,'+rowIndex +','+colIndex+');" />';
			    }else {
				return '<input  id="chkCSBENEFICIARIO"'+ rowIndex + 'value='+value+' type="checkbox" '+record.data['AUX_BENEF']+' onclick="selecCSBENEFICIARIO(this,'+rowIndex +','+colIndex+');" />';
			    }
			}
		    }
		}else  {
		    return '';
			
		}
            }
         },{ 
	    header: 'N�mero de Referencia <br> del Beneficiario',
            tooltip: 'N�mero de Referencia <br> del Beneficiario',
	    dataIndex: 'REFERENCIA',
            sortable: false,
            hideable: false,
            width: 150,
            align: 'center',
	    editor: { 
		xtype:'textfield',
		maxLength : 10,
		allowBlank: true
	    },
	    renderer: function(value, metadata, record, rowIndex, colIndex, store){  
		if(record.data['CSBENEFICIARIO']==='S'  &&  record.data['FACTORAJE_DISTRIBUIDO']==='S' ){ 
		    return NE.util.colorCampoEdit(value,metadata,record);	
		}else  {
		    return '';
		}		
	    }	   
	    
	  },	
         {
            header: 'Opera <br/>Convenio <br/>�nico',
				tooltip:'Opera <br/>Convenio <br/>�nico',
            dataIndex: 'CSCONVENIOUNICO',
            sortable: false,
            hideable: false,
            width: 70,
            align: 'center',
            renderer: function(value, metadata, record, rowIndex, colIndex, store){
					if(record.data['APLICA_CONVENIO_UNICO']=='S'){
            		return '<input id="chkCSCONVENIOUNICO"'+ rowIndex + 'value="S" type="checkbox" '+record.data['AUX_CONVENIO']+' onclick="selecCSCONVENIOUNICO(this,'+rowIndex +','+colIndex+');"/>';
            	}else{
            		return 'NA';
            	}
            }
         },			
			{
            header: 'Opera <br/>Reafiliaciones<br/> Autom�tica',
				tooltip:'Opera <br/>Reafiliaciones<br/> Autom�tica',
            dataIndex: 'REAFILIACION_AUTO',
            sortable: false,
            hideable: false,
            width: 70,
            align: 'center',
            renderer: function(value, metadata, record, rowIndex, colIndex, store){
					if(record.data['FIDEICOMISO_EPO']=='S')  {
							return "N/A";
					}else {
						if(record.data['APLICA_REAFILIACION_AUTO']=='S' ||  record.data['CS_ESTATUS_REAFILIACION']=='S' ){
							return '<input id="chkREAFILIACION_AUTO"'+ rowIndex + 'value="S" type="checkbox" '+record.data['AUX_REAFILIACION_AUTO']+' onclick="selecREAFILIACION_AUTO(this,'+rowIndex +','+colIndex+');"/>';
						}else{ 
							return 'NA';
						}
					}
            }
         },
			{
         	xtype:  'actioncolumn',
            header: 'Monedas',
				tooltip:'Monedas',
            dataIndex: 'MONEDAS',
            sortable: false,
            hideable: false,
            width: 70,
            align: 'center',           
            items:  [
					{
						getClass:   function(valor, metadata, registro, rowIndex, colIndex, store) {	
							return 'icoBuscar'
						},
						tooltip:  'Ver',
						handler:  fnVerMonedaGridPrincipal
					}
            ] 
         },
		   {
         	xtype: 'actioncolumn',
				header: 'Carga de <br/>Archivo',
				tooltip:'Carga de <br/>Archivo',
            dataIndex: 'CARGAARCHIVO',
            sortable: false,
            hideable: false,
            width: 80,
            align: 'center',
            items:[
		      	{
            		iconCls: 'modificar',
            		tooltip: 'Modificar',
            		handler: fnModificarCargaArchivoGridPrincipal
            	},
					{ //para enviar 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							if(registro.data['FECHA_CONVENIO']!=''){
								this.items[0].tooltip = 'Descargar';
								return 'icoPdf';											
							}			
						}
						,handler: descargaArchivo					
					}
            ]
         },			
			{
            header: 'Bloqueo',
				tooltip: 'Bloqueo',
            dataIndex: 'CSBLOQUEO',
            sortable: false,
            hideable: false,
            width: 80,
            align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
				if(record.data['VOBO_NAFIN']!='N' ){
						if(record.data['BLOQUEO']=='N' ){
							if(record.data['CLAVEPERFIL']=='8' ){
								return '<input  id="chkCSBLOQUEO"'+ rowIndex + ' value='+ record.data['BLOQUEO'] + ' type="checkbox" DISABLED onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
							}else{
								return '<input  id="chkCSBLOQUEO"'+ rowIndex + ' value='+ record.data['BLOQUEO'] + ' type="checkbox"  '+record.data['AUXCSBLOQUEO'] +' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
							}
						}else {
							return '<input  id="chkCSBLOQUEO"'+ rowIndex + ' value='+ record.data['BLOQUEO'] + ' type="checkbox"  '+record.data['AUXCSBLOQUEO'] +' onclick="selecCSBLOQUEO(this, '+rowIndex +','+colIndex+');" />';
						}
					}
				}
         },
         {
            header: 'Fecha<br/>Bloqueo',
				tooltip:'Fecha<br/>Bloqueo',
            dataIndex: 'FECHABLOQUEO',
            sortable: true,
            hideable: true,
            width: 80,
            align: 'center'
         },
         {
            header: 'Usuario',
				tooltip:'Usuario',
            dataIndex: 'USUARIO',
            sortable: true,
            hideable: true,
            width: 80,
            align: 'center'
         },
         {
            header: 'Opera<br/>Factoraje<br/>Normal',
				tooltip: 'Opera<br/>Factoraje<br/>Normal',
            dataIndex: 'FACTORAJENORMAL',
            sortable: true,
            hideable: true,
            width: 80,
            align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
				
					return '<input id="chkFACTORAJENORMAL"'+ rowIndex + ' value="S" type="checkbox"'+ record.data['AUXNORMAL']+' onclick="selecFACTORAJENORMAL(this,'+rowIndex +','+colIndex+');"/>';            
            }				
         },	
			 {
            header: 'Opera<br/>Factoraje<br/>al Vencimiento',
				tooltip: 'Opera<br/>Factoraje<br/>al Vencimiento',
            dataIndex: 'OPERACION_VENCIMIENTO',
            sortable: true,
            hideable: true,
            width: 80,
            align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
						if(record.data['FACTORAJE_VENCIDO']=='S'&& record.data['OPERACION_VENCIMIENTO']=='S' ){
								return '<input id="chkFACTORAJE_VENCIDO" '+ rowIndex + '  value='+ record.data['OPERACION_VENCIMIENTO'] + ' type="checkbox"'+record.data['AUXFACTVENCIDO']+'  onclick="selecFactVencido(this,'+rowIndex +','+colIndex+');" />';
						}else{
							if(record.data['FACTORAJE_VENCIDO']=='S'&& record.data['OPERACION_VENCIMIENTO']=='N' ){
								return '<input id="chkFACTORAJE_VENCIDO"'+ rowIndex + ' value='+record.data['OPERACION_VENCIMIENTO']+'  type="checkbox"'+record.data['AUXFACTVENCIDO']+'  onclick="selecFactVencido(this,'+rowIndex +','+colIndex+');" />';
							}else{
								return '';
							}
						}
				}					
         }, 
	 
	 {
            header: 'Opera<br/>Factoraje<br/>Distribuido',
	    tooltip: 'Opera<br/>Factoraje<br/> Distribuido',
            dataIndex: 'OPERACION_DISTRIBUIDO',
            sortable: true,
            hideable: true,
            width: 80,
            align: 'center',
	    renderer: function(value, metadata, record, rowIndex, colIndex, store){
		if(record.data['FACTORAJE_DISTRIBUIDO']==='S'&& record.data['OPERACION_DISTRIBUIDO']==='S' ){
		    return '<input id="chkFACTORAJE_DISTRIBUIDO" '+ rowIndex + '  value='+ record.data['OPERACION_DISTRIBUIDO'] + ' type="checkbox"'+record.data['AUXFACTDISTRIBUIDO']+'  onclick="selecFactDistribuido(this,'+rowIndex +','+colIndex+');" />';
		}else{
		    if(record.data['FACTORAJE_DISTRIBUIDO']==='S'&& record.data['OPERACION_DISTRIBUIDO']==='N' ){
			return '<input id="chkFACTORAJE_DISTRIBUIDO"'+ rowIndex + ' value='+record.data['OPERACION_DISTRIBUIDO']+'  type="checkbox"'+record.data['AUXFACTDISTRIBUIDO']+'  onclick="selecFactDistribuido(this,'+rowIndex +','+colIndex+');" />';
		    }else{
			return '';
		    }
		}
	    }					
         },		
	 
         {
            header: 'Inactivar<br/>Relaci�n',
				tooltip: 'Inactivar<br/>Relaci�n',
            dataIndex: 'CUENTA_ACTIVA',
            sortable: true,
            hideable: true,
            width: 80,
            align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					
					if(record.data['CUENTA_ACTIVA']!='' || record.data['VOBO_NAFIN']=='S' ){
						return '<input id="chkCUENTA_ACTIVA"'+ rowIndex + '  value="'+record.data['CUENTA_ACTIVA']+'" type="checkbox"'+record.data['AUXCTAACTIVA']+'  onclick="selecCUENTA_ACTIVA(this,'+rowIndex +','+colIndex+');" />';            
					}else  {
						return '';     
					}
				}	
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
				
				if(campo == 'PORCENTAJE_MN'  ||  campo == 'PORCENTAJE_DOL' ){
					if(record.data['CLAVEPERFIL']=='8'  ||  record.data['CSBLOQUEO']=='S' )	 {	
						return false;	 //se inhabilita			
					}else  if(record.data['CLAVEPERFIL']!='8'  ||  record.data['CSBLOQUEO']=='N' )	 {
						return true;	//se habilita
					}					
				}
				
				if(campo==='REFERENCIA'  &&  record.data['CSBENEFICIARIO']==='S'  && record.data['FACTORAJE_DISTRIBUIDO']==='S'  ){
				    return true;  //se habilita				
				}else {
				    return false;	 //se inhabilita  
				}
			}			
			
		},
		bbar: {		
			items: [
				'->',	
				'-',	
				{
					text: 'Guardar',
					xtype: 'button',					
					iconCls: 'icoContinuar',
					id: 'btnGuardarR_E_I',
					handler: procesarR_E_I
				}		
			]	
		}
	});
	
	
	
	
	var cmbIF = new Ext.data.JsonStore({
		id: 'cmbIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15admnafparamautorizaext.data.jsp',
		baseParams: {
			informacion: 'cmbIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	

	var cmbEpo = new Ext.data.JsonStore({
		id: 'cmbEpo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15admnafparamautorizaext.data.jsp',
		baseParams: {
			informacion: 'cmbEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var  elementosForma = [
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'EPO',
			name: 'noEpo',
			id: 'noEpo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'noEpo',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: cmbEpo,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {							
						var cmbIF = Ext.getCmp('noIf1');
						cmbIF.store.load({
							params: {
								noEpo:combo.getValue()
							}
						});
					}						
				}
			}
		},	
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'IF',
			name: 'noIf',
			id: 'noIf1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'noIf',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: cmbIF,
			tpl : NE.util.templateMensajeCargaCombo
		}
	]
	
	

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,							
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {					
				
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar'
						})
					});
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15admnafparamautorizaext.jsp';					
				}
			}
		]
	});
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
			
		]
	});

	cmbEpo.load();
	cmbIF.load();
});	
