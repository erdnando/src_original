<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.parametrosgrales.*,
		java.sql.*, 
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String mes  = (request.getParameter("mes") != null)?request.getParameter("mes"):"";
String anio  = (request.getParameter("anio") != null)?request.getParameter("anio"):"";


ParametrosGrales parametrosbean = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);

	

JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();	
List registros  = new ArrayList();
List registrosT  = new ArrayList();
String infoRegresar ="";

	LinkedHashMap meses_nombre = new LinkedHashMap();
	meses_nombre.put("01","ENERO");
	meses_nombre.put("02","FEBRERO");
	meses_nombre.put("03","MARZO");
	meses_nombre.put("04","ABRIL");
	meses_nombre.put("05","MAYO");
	meses_nombre.put("06","JUNIO");
	meses_nombre.put("07","JULIO");
	meses_nombre.put("08","AGOSTO");
	meses_nombre.put("09","SEPTIEMBRE");
	meses_nombre.put("10","OCTUBRE");
	meses_nombre.put("11","NOVIEMBRE");
	meses_nombre.put("12","DICIEMBRE");
	
if (informacion.equals("catalogoMes")){

	Set MesesKeys = meses_nombre.keySet();
	Iterator It = MesesKeys.iterator();	

	registros  = new ArrayList();
	while (It.hasNext()) {
		String HostNow = (String)(It.next());					
		datos = new HashMap();			
		datos.put("clave", HostNow);
		datos.put("descripcion", meses_nombre.get(HostNow));
		registros.add(datos);
	}
	jsonObj.put("registros", registros);
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString();		

}else   if (informacion.equals("catalogoAnio")){

	List lanioParam = parametrosbean.consAnioParamEpoPef();
	for(int x=0;x < lanioParam.size(); x++){
		datos = new HashMap();			
		datos.put("clave", (String)lanioParam.get(x));
		datos.put("descripcion", (String)lanioParam.get(x));
		registros.add(datos);
	}
	jsonObj.put("registros", registros);
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString();

}else   if (informacion.equals("Consultar")  ||  informacion.equals("ConsultarTotales") ){
	
	List regReporte = new ArrayList();
	List datReporte = new ArrayList();
	
	double totOpPeriodo=0,  totPorcObra=0,  totMontoObra=0;

	regReporte = parametrosbean.consEpoPefReporte(anio, mes);				
	if(regReporte!=null && regReporte.size()>0){
		for(int x=0;x<regReporte.size();x++){
			datReporte = (List)regReporte.get(x);
			double montoOpeObr = 0;
			totOpPeriodo += Double.parseDouble((String)datReporte.get(3));
			totPorcObra  += Double.parseDouble((String)datReporte.get(4));						 
			montoOpeObr = ((Double.parseDouble((String)datReporte.get(4)))*(Double.parseDouble((String)datReporte.get(5))))/100;
			totMontoObra += montoOpeObr;		
			datos = new HashMap();	
			datos.put("IC_EPO",(String)datReporte.get(0));
			datos.put("DEPENDENCIA",(String)datReporte.get(1));
			datos.put("MONTO_PERIODO",(String)datReporte.get(3));
			datos.put("SALDO_PROMEDIO",(String)datReporte.get(4));
			datos.put("PORCENTAJE_OBRA_PUBLICA",(String)datReporte.get(5));
			datos.put("MONTO_OBRA_PUBLICA",Double.toString (montoOpeObr));
			registros.add(datos);
		}
	}
	
	if (informacion.equals("Consultar") ) 			 {   jsonObj.put("registros", registros);   }
	if (informacion.equals("ConsultarTotales") )  {   
		datos = new HashMap();			
		datos.put("DESCRIPCION","TOTAL");		
		datos.put("MONTO_PERIODO_TOTAL",Double.toString (totOpPeriodo));
		datos.put("SALDO_PROMEDIO_TOTAL",Double.toString (totPorcObra));
		datos.put("MONTO_OBRA_PUBLICA_TOTAL",Double.toString (totMontoObra));
		registrosT.add(datos);	
		jsonObj.put("registros", registrosT);   
	}
	
	jsonObj.put("success",  new Boolean(true)); 	
   infoRegresar = jsonObj.toString();


}else   if (informacion.equals("GenerarArchivo")  ){

	StringBuffer sbArchivo = new StringBuffer();
	String mesDoc= String.valueOf(meses_nombre.get(mes));

	String encaprim = "REPORTE DE COBRO POR OBRA PUBLICA";
	String enca1 = "MES DE CONSULTA "+mesDoc+" DEL "+anio+" (MONEDA NACIONAL)";
	
	
	sbArchivo.append(encaprim.toUpperCase()+"\r\n");
	sbArchivo.append(enca1.toUpperCase()+"\r\n");
	
	sbArchivo.append("No.EPO,DEPENDENCIA/ENTIDAD,MONTO OPERADO EN EL PERIODO,SALDO PROMEDIO DIARIO DEL PERIODO,PORCENTAJE CORRESPONDIENTE OBRA PUBLICA,MONTO OPERADO CORRESOPNDIENTE OBRA PUBLICA");
	sbArchivo.append("\r\n");
	
	List regReporte = new ArrayList();
	List datReporte = new ArrayList();
	
	double totOpPeriodo=0,  totPorcObra=0,  totMontoObra=0;

	regReporte = parametrosbean.consEpoPefReporte(anio, mes);				
	if(regReporte!=null && regReporte.size()>0){
		for(int x=0;x<regReporte.size();x++){
			datReporte = (List)regReporte.get(x);
			double montoOpeObr = 0;
			totOpPeriodo += Double.parseDouble((String)datReporte.get(3));
			totPorcObra  += Double.parseDouble((String)datReporte.get(4));						 
			montoOpeObr = ((Double.parseDouble((String)datReporte.get(4)))*(Double.parseDouble((String)datReporte.get(5))))/100;
			totMontoObra += montoOpeObr;		
			sbArchivo.append( ((String)datReporte.get(0)).replace(',', ' ')+"," );
			sbArchivo.append( ((String)datReporte.get(1)).replace(',', ' ')+",");
			sbArchivo.append(((String)datReporte.get(3)).replace(',', ' ')+",");
			sbArchivo.append(((String)datReporte.get(4)).replace(',', ' ')+",");
			sbArchivo.append(((String)datReporte.get(5)).replace(',', ' ')+",");
			sbArchivo.append((Double.toString (montoOpeObr)).replace(',', ' ')+",");
			sbArchivo.append("\r\n");
		}
		sbArchivo.append(" ,TOTAL,"+totOpPeriodo+","+totPorcObra+",,"+totMontoObra);
		sbArchivo.append("\r\n");
	}
	

	System.out.println("sbArchivo ----------->"+sbArchivo );
	
	String nombreArchivo= "";
	CreaArchivo archivo = new CreaArchivo();
	if (archivo.make(sbArchivo.toString(), strDirectorioTemp, ".csv"))
	nombreArchivo = archivo.nombre;	
	
	System.out.println("nombreArchivo ----------->"+nombreArchivo );
	
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();		
}	

System.out.println("infoRegresar ----------->"+infoRegresar );

%>

<%=infoRegresar%>


