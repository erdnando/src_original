Ext.onReady(function(){ 

var cajIguales="";
var globalEpo = "";
var globalMoneda = "";
var estatusMoneda= "";

//-------------Handlers------------------------------------------------------------------------------------
	var procesaDatos = function(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fp2.el.unmask();
			info = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('fieldEpo').setValue(info.NOMBREEPO);
			Ext.getCmp('fieldMoneda').setValue(info.NOMBREMONEDA);
		
			if(info.NUMCAJONES == ""){ 
				Ext.getCmp('numCajones').setValue("10");
			}else{
				Ext.getCmp('numCajones').setValue(info.NUMCAJONES);
			}
			Ext.getCmp('btnConsultar').enable();
		}
	}
	
	var procesaCajas = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			info = Ext.util.JSON.decode(response.responseText);
			cajIguales = info.CAJASIGUALES;
			Ext.getCmp('btnConsultar').enable();
			Ext.getCmp('btnGuardar').enable();	
		}
	}


	var procesaGuardado = function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			info = Ext.util.JSON.decode(response.responseText);						
			
			if (Ext.getCmp('idComboEpo').getValue() == 'T' && info.OPERACION != 'N'){
				cajIguales = '1';
			}
			
			if(info.OPERACION == 'M' || info.OPERACION == 'G'){
				Ext.MessageBox.alert("Mensaje","Se guardo con �xito");
			}else{
				Ext.MessageBox.alert("Mensaje","No se pudo guardar con �xito");
			}
	
			fp2.el.mask('Procesando...','x-mask-loading');
			Ext.Ajax.request({
				url: '15CajonesEPOExt.data.jsp',
				params: {
					informacion: "ConsultaDatos",
					comboEpo: Ext.getCmp('idComboEpo').getValue(),
					comboMoneda: Ext.getCmp('idComboMoneda').getValue() 
				},
				callback: procesaDatos
			});			
			Ext.getCmp('btnGuardar').enable();	
		}
	}


	var procesarTodosEpo = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 		
		store.insert(1,new Todas({ 
		clave: "T", 
		descripcion: "Todas las EPOS", 
		loadMsg: ""})); 		
		store.commitChanges(); 
		Ext.getCmp('idComboEpo').setValue('');
	}
		
	
	var procesarTodosMoneda = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 		
		store.commitChanges(); 
		Ext.getCmp('idComboMoneda').setValue('');
		estatusMoneda = Ext.getCmp('idComboMoneda').getValue();
	}


//-------------Stores----------------------------------------------------------------------------------	
	Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);

	var CatalogoEpo= new Ext.data.JsonStore({
		id: 'catEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15CajonesEPOExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoEpo'
		},
		autoLoad: false, 
		listeners:
		{
		 load: procesarTodosEpo,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	  });


	var CatalogoMoneda= new Ext.data.JsonStore({
		id: 'catMon',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15CajonesEPOExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoMoneda'
		},
		autoLoad: false, 
		listeners:
		{
		 load: procesarTodosMoneda,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	  });


/**********CRITERIOS DE BUSQUEDA ***************/		

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: '<b>Parametrizaci�n</b>',			
				id: 'btnParametrizacion',					
				handler: function() {
					window.location = '15CajonesEPOExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Reportes',			
				id: 'btnReportes',					
				handler: function() {
					window.location = '15ReporteIfExt.jsp';
				}
			}	
		]
	});



//--------------Componentes------------------------------------------------------------------------------
	var elementosForma = 
	[	
		{
		xtype: 'combo',
		fieldLabel: 'Nombre de la EPO',
		forceSelection: true,
		autoSelect: true,
		name:'claveEpo',
		id:'idComboEpo',
		displayField: 'descripcion',
		allowBlank: true,
		editable:true,
		valueField: 'clave',
		mode: 'local',
		triggerAction: 'all',
		autoLoad: false,
		typeAhead: true,
		minChars: 1,
		store: CatalogoEpo,  
		width:270,
		
		listeners:{
			select: function(combo){
				if(combo.getValue() == 'T' && Ext.getCmp('idComboMoneda').getValue() != ""){
					Ext.getCmp('btnConsultar').disable();
					Ext.getCmp('btnGuardar').disable();
					Ext.Ajax.request({
					url: '15CajonesEPOExt.data.jsp',
					params: {
						informacion: "CajasIguales",
						comboMoneda: Ext.getCmp('idComboMoneda').getValue() 
					},
					callback: procesaCajas
					});
				}
			}   
		}
	},{
		xtype: 'compositefield',
			id: 'cfMoneda',
			combineErrors: false,
			items: [
			{
				xtype: 'combo',
				fieldLabel: 'Moneda',
				forceSelection: true,
				autoSelect: true,
				name:'claveMoneda',
				id:'idComboMoneda',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: CatalogoMoneda,  
				width:270,
				listeners:{
					select: function(combo){
						//if(combo.getValue() == "" && estatusMoneda != combo.getValue()){
						if(combo.getValue() == ""){
							cajIguales = "";
							if(Ext.getCmp('idComboEpo').getValue() == 'T'){
								Ext.getCmp('fieldEpo').setValue("Todas las EPOS");
								Ext.getCmp('fieldMoneda').setValue("");
								Ext.getCmp('numCajones').setValue("10");
							}else {
								Ext.getCmp('fieldEpo').setValue("");
								Ext.getCmp('fieldMoneda').setValue("");
								Ext.getCmp('numCajones').setValue("10");
							}
						}
						//else if(estatusMoneda != combo.getValue()){
						else{
							Ext.Ajax.request({
								url: '15CajonesEPOExt.data.jsp',
								params: {
									informacion: "CajasIguales",
									comboMoneda: Ext.getCmp('idComboMoneda').getValue() 
								},
								callback: procesaCajas
							});
							if(Ext.getCmp('idComboEpo').getValue() == 'T' && estatusMoneda != combo.getValue() ){
								Ext.getCmp('fieldEpo').setValue("Todas las EPOS");
								Ext.getCmp('fieldMoneda').setValue("");
								Ext.getCmp('numCajones').setValue("10");
							}else if(estatusMoneda != combo.getValue()){
								Ext.getCmp('fieldEpo').setValue("");
								Ext.getCmp('fieldMoneda').setValue("");
								Ext.getCmp('numCajones').setValue("10");
							}
						}
						estatusMoneda = combo.getValue();
					}
				}   
			}	]
		}
	]


	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		title: '<center>Parametrizaci�n de Cajones</center>',
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
		style: 'margin:0 auto;',	   
		height: 150,
		buttons: [{
		text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function (boton,evento){
								if (Ext.getCmp('idComboMoneda').getValue() == "" || Ext.getCmp('idComboEpo').getValue() == "") {
									if(Ext.getCmp('idComboMoneda').getValue() == "")
										Ext.getCmp('idComboMoneda').markInvalid('Debe ingresar los datos requeridos');
								if(Ext.getCmp('idComboEpo').getValue() == "")
										Ext.getCmp('idComboEpo').markInvalid('Debe ingresar los datos requeridos');					
									//Ext.MessageBox.alert("Alerta","Debe ingresar los datos requeridos");
								}else if (Ext.getCmp('idComboEpo').getValue() == 'T' && (Ext.getCmp('idComboMoneda').getValue() != "" && cajIguales != 1)){
										boton.disable();
										Ext.Msg.confirm('Mensaje', 
									'Las EPOS seleccionadas no cuentan con el mismo N�mero de Cajones �Desea continuar?',
									function(botonConf) {
										if (botonConf == 'ok' || botonConf == 'yes' || botonConf == 'si' ) {
									fp2.el.mask('Procesando...','x-mask-loading');
									Ext.Ajax.request({
										url: '15CajonesEPOExt.data.jsp',
										params: {
											informacion: "ConsultaDatos",
											comboEpo: Ext.getCmp('idComboEpo').getValue(),
											comboMoneda: Ext.getCmp('idComboMoneda').getValue() 
										},
										callback: procesaDatos
									});
										}else{
											window.location = '15CajonesEPOExt.jsp';
										}
									});
								}else {
									fp2.el.mask('Procesando...','x-mask-loading');
									Ext.Ajax.request({
									url: '15CajonesEPOExt.data.jsp',
									params: {
										informacion: "ConsultaDatos",
										comboEpo: Ext.getCmp('idComboEpo').getValue(),
										comboMoneda: Ext.getCmp('idComboMoneda').getValue() 
									},
									callback: procesaDatos  });
								}
							}
				}],
				width: 820
	});

	
	var elementosForma2 = [{
		xtype: 'displayfield',
		name: 'nameEpo',
		id: 'fieldEpo',
		allowBlank: false,
		fieldLabel: 'Epo ',
		value:''
	},{
		xtype: 'displayfield',
		name: 'nameMoneda',
		id: 'fieldMoneda',
		allowBlank: false,
		fieldLabel: 'Moneda ',
		value:''
	},{
		xtype:'numberfield',
		height: 20,
		fieldLabel: 'N�mero de Cajones ',
		//align: 'center',
		width: 70,
		decimalPrecision: 0,		
		maxLength: 3,
		value: '10',
		anchor: '24%',
		id:'numCajones'
	},{
		xtype: 	'label',
		id:	 	'leyenda1',
		hidden: 	false,
		html:  	'<BR/><font size="1">Nota: El n�mero de cajones parametrizados reflejara en el c�lculo de los Rangos de Publicaci�n en la Pantalla de Proveedores con Prioridad.</font><BR/>'
		}			
	] 
	
		
	var fp2 = new Ext.form.FormPanel({
		id:'formaDetalle2',
		style: ' margin:0 auto;',
		hidden: false,
		labelWidth: 120,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma2,
		style: 'margin:0 auto;',	   
		height: 180,
		buttons: [{
					text: 'Guardar',
					id: 'btnGuardar',
					iconCls: 'icoGuardar',
					handler: function (boton,evento){
						boton.disable();
						var nfield = Ext.getCmp('numCajones');
						var valor = parseInt(nfield.getValue());
							
						if (Ext.getCmp('idComboEpo').getValue() == "" || Ext.getCmp('idComboMoneda').getValue() == ""){
							//Ext.MessageBox.alert("Alerta","Debe ingresar los datos requeridos");
								if(Ext.getCmp('idComboMoneda').getValue() == "")
										Ext.getCmp('idComboMoneda').markInvalid('Debe ingresar los datos requeridos');
								if(Ext.getCmp('idComboEpo').getValue() == "")
										Ext.getCmp('idComboEpo').markInvalid('Debe ingresar los datos requeridos');
							boton.enable();
						}else if( valor > 20){
							//Ext.MessageBox.alert("Alerta","El n�mero de cajones no debe ser mayor a 20");						
							nfield.markInvalid('El n�mero de cajones no debe ser mayor a 20');
							boton.enable();
						}else if (valor < 1 || nfield.getValue() == ""){
							//Ext.MessageBox.alert("Alerta","El n�mero de cajones debe ser mayor o igual a 1");
							nfield.markInvalid('El n�mero de cajones debe ser mayor o igual a 1');
							boton.enable();
						}else{						
							Ext.Ajax.request({
							url: '15CajonesEPOExt.data.jsp',
							params: {
								informacion: "Guardar",
								comboEpo: Ext.getCmp('idComboEpo').getValue(),
								comboMoneda: Ext.getCmp('idComboMoneda').getValue(), 
								noCajones: valor
							},
							callback: procesaGuardado  });						
						}
					}				
				},{
					text: 'Limpiar',
					id: 'btnLimpiar',
					iconCls: 'icoLimpiar',
					handler: function (boton,evento){				
						window.location = '15CajonesEPOExt.jsp';
					}				
				}],
		width: 820
	});


//------ Componente Principal --------------------------------------------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [NE.util.getEspaciador(10),fpBotones,NE.util.getEspaciador(30),fp,
		NE.util.getEspaciador(10), fp2]
	});

	CatalogoEpo.load();
	CatalogoMoneda.load();

});