<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		com.netro.parametrosgrales.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 

	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	HashMap datos = new HashMap(); 
	JSONArray registros = new JSONArray();  
	JSONObject 	jsonObj	= new JSONObject();  

	if (informacion.equals("CatalogoIF")){
		String habilitado = "S";
		String tipoOperacion = "credito"; 	 
		
		String noIf = (request.getParameter("noIf")==null)?"":request.getParameter("noIf");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");
			
		String tipoCred = (request.getParameter("tipoCred")==null)?"":request.getParameter("tipoCred");
		String tipoAmort = (request.getParameter("tipoAmort")==null)?"":request.getParameter("tipoAmort");
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
		String tipo_plazo = (request.getParameter("tipo_plazo")==null)?"":request.getParameter("tipo_plazo");
	
		CatalogoCreditoCons cat = new CatalogoCreditoCons();
			
	   cat.setCampoClave("t.ic_if"); 
	   cat.setCampoDescripcion("t.cg_razon_social"); 
	
	   cat.setIntermediario(noIf);
	   cat.setTasa(tipoTasa);
		cat.setCredito(tipoCred);
		cat.setAmort(tipoAmort);
	
		cat.setPlazo(tipo_plazo);
		cat.setHabilitado(habilitado);
		cat.setPiso(tipoCartera);
	
		cat.setOperacion(tipoOperacion);		
	
		infoRegresar=cat.getJSONElementos();
		
	}else if (informacion.equals("CatalogoTasa")){		
		String habilitado = "S";
		String tipoOperacion = "credito"; 	 
		
		String noIf = (request.getParameter("noIf")==null)?"":request.getParameter("noIf");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");
	
		
		String tipoCred = (request.getParameter("tipoCred")==null)?"":request.getParameter("tipoCred");
		String tipoAmort = (request.getParameter("tipoAmort")==null)?"":request.getParameter("tipoAmort");
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
		String tipo_plazo = (request.getParameter("tipo_plazo")==null)?"":request.getParameter("tipo_plazo");
	
		CatalogoCreditoCons cat = new CatalogoCreditoCons();
			
		cat.setCampoClave("t.ic_tasa"); 
		cat.setCampoDescripcion("t.ic_tasa||' - '||t.cd_nombre"); 
	
		cat.setOrden("1");
		cat.setIntermediario(noIf);
		cat.setTasa(tipoTasa);
		cat.setCredito(tipoCred);
		cat.setAmort(tipoAmort);
		cat.setPlazo(tipo_plazo);
		cat.setHabilitado(habilitado);
		cat.setOperacion(tipoOperacion);		
		cat.setPiso(tipoCartera);
		infoRegresar=cat.getJSONElementos();
	
	}else if (informacion.equals("CatalogoCredito")){
		String habilitado = "S";
		String tipoOperacion = "credito"; 	 
		
		String noIf = (request.getParameter("noIf")==null)?"":request.getParameter("noIf");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");
	
		
		String tipoCred = (request.getParameter("tipoCred")==null)?"":request.getParameter("tipoCred");
		String tipoAmort = (request.getParameter("tipoAmort")==null)?"":request.getParameter("tipoAmort");
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
		String tipo_plazo = (request.getParameter("tipo_plazo")==null)?"":request.getParameter("tipo_plazo");		
			
		CatalogoCreditoCons cat = new CatalogoCreditoCons();
			
		cat.setCampoClave("t.ic_tipo_credito");
		cat.setCampoDescripcion("t.ic_tipo_credito||' - ' ||t.cd_descripcion");
		cat.setIntermediario(noIf);
		cat.setTasa(tipoTasa);
		cat.setAmort(tipoAmort);
		cat.setPlazo(tipo_plazo);
		cat.setHabilitado(habilitado);
		cat.setPiso(tipoCartera);
		cat.setOperacion(tipoOperacion);	
		cat.setValoresCondicionNotIn("1,2,3,5,9,11", java.lang.Integer.class);
			
		infoRegresar=cat.getJSONElementos();	
	}else if (informacion.equals("CatalogoAmortizacion")){
		
		String tipoOperacion = "credito"; 	 
		
		String noIf = (request.getParameter("noIf")==null)?"":request.getParameter("noIf");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");	
		String tipoCred = (request.getParameter("tipoCred")==null)?"":request.getParameter("tipoCred");
		String tipoAmort = (request.getParameter("tipoAmort")==null)?"":request.getParameter("tipoAmort");
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
		String tipo_plazo = (request.getParameter("tipo_plazo")==null)?"":request.getParameter("tipo_plazo");		
	
		CatalogoCreditoCons cat = new CatalogoCreditoCons();
		
		cat.setCampoClave("t.ic_tabla_amort");
		cat.setCampoDescripcion("t.ic_tabla_amort||' - '|| t.cd_descripcion");
		
		cat.setIntermediario(noIf);
		cat.setTasa(tipoTasa);
		cat.setCredito(tipoCred);
		cat.setAmort(tipoAmort);
		cat.setPlazo(tipo_plazo);
		cat.setPiso(tipoCartera);

		cat.setOperacion(tipoOperacion);	
		
		cat.setValoresCondicionNotIn("0", java.lang.Integer.class);

		infoRegresar=cat.getJSONElementos();	
	}else if (informacion.equals("CatalogoPeriocidad")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_periodicidad");
		cat.setCampoClave("ic_periodicidad");
		cat.setCampoDescripcion("cd_descripcion");
		
		infoRegresar=cat.getJSONElementos();
	}else if (informacion.equals("CatalogoOperacion")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_base_operacion");
		cat.setCampoClave("ig_codigo_base");
		cat.setCampoDescripcion("ig_codigo_base ||' '||cg_descripcion");
		
		cat.setOrden("ig_codigo_base");		
		
		infoRegresar=cat.getJSONElementos();	
	}else if(informacion.equals("ConsiguePiso")){
		String mensaje = "";
		String no_if	  = (request.getParameter("no_if")==null)?"":request.getParameter("no_if");
		
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
	
		mensaje = BeanParametrosGrales. consigueCarteraCredito(no_if);
	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("PISO",mensaje);
		infoRegresar = jsonObj.toString();	
	}else if(informacion.equals("ConsiguePlazo")){
		String noIf = (request.getParameter("no_if")==null)?"":request.getParameter("no_if");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");
		String tipoCred = (request.getParameter("tipoCred")==null)?"":request.getParameter("tipoCred");
		String tipoAmort = (request.getParameter("tipoAmort")==null)?"":request.getParameter("tipoAmort");
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
		String tipo_plazo = (request.getParameter("tipo_plazo")==null)?"":request.getParameter("tipo_plazo");
		
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
		
		HashMap hm = new HashMap();
		hm = (HashMap)BeanParametrosGrales.consiguePlazoCredito(noIf,tipoTasa,tipoCred,tipoAmort,tipoCartera,tipo_plazo);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("TIPO", hm.get("TIPO"));
		jsonObj.put("VARC", hm.get("VARC"));
		jsonObj.put("VARE", hm.get("VARE"));
		jsonObj.put("VARF", hm.get("VARF"));
		jsonObj.put("PLAZOS", hm.get("PLAZOS"));

		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("ConsultaGrid") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF") ){
	
		String noIf = (request.getParameter("noIf")==null)?"":request.getParameter("noIf");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");
		String baseOperacionDinamica 	= (request.getParameter("baseOperacionDinamica")  == null || request.getParameter("baseOperacionDinamica").equals("") )?"N":request.getParameter("baseOperacionDinamica");
		String altaAutomaticaGarantia = (request.getParameter("altaAutomaticaGarantia") == null || request.getParameter("altaAutomaticaGarantia").equals(""))?"N":request.getParameter("altaAutomaticaGarantia");
		String tipoCred = (request.getParameter("tipoCred")==null)?"":request.getParameter("tipoCred");
		String tipoAmort = (request.getParameter("tipoAmort")==null)?"":request.getParameter("tipoAmort");
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
		String tipo_plazo = (request.getParameter("tipo_plazo")==null)?"":request.getParameter("tipo_plazo");
		String operacion = "credito"; 
		//String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
		String tipoOperacion = (request.getParameter("tipoOperacion")==null)?"":request.getParameter("tipoOperacion");
		String noEmisor = (request.getParameter("noEmisor")==null)?"":request.getParameter("noEmisor");
	
		//se agragn campos nuevo spara bases de operacion F004-2010 FVR
		String periodicidad = (request.getParameter("periodicidad")==null)?"":request.getParameter("periodicidad");
		String tipoInteres = (request.getParameter("tipoInteres")==null)?"":request.getParameter("tipoInteres");
		
		String tipoRenta = (request.getParameter("tipoRenta")==null)?"":request.getParameter("tipoRenta");
			
		ConsCredito paginador = new ConsCredito();
	
		paginador.setIntermediario(noIf);
		paginador.setTasa(tipoTasa);
		paginador.setBaseOperacion(baseOperacionDinamica);
		paginador.setAltaAutomatica(altaAutomaticaGarantia);
		paginador.setCredito(tipoCred);
		paginador.setAmortizacion(tipoAmort);
		paginador.setCartera(tipoCartera);		
		paginador.setPlazo(tipo_plazo);
		paginador.setOperacion(operacion);
		paginador.setPeriodicidad(periodicidad);	
		
		paginador.setInteres(tipoInteres);
		paginador.setRenta(tipoRenta);

		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	 
		if (informacion.equals("ConsultaGrid") ){ 
			try {
				Registros reg	=	queryHelper.doSearch();
				String consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
				jsonObj.put("success", new Boolean(true));
				jsonObj = JSONObject.fromObject(consulta);
			}catch(Exception e) {
				throw new AppException("Error en la paginación", e);
			}
		}
		else  if(informacion.equals("ArchivoCSV") ) {				
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}else  if(informacion.equals("ArchivoPDF") ) {		
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}	
		infoRegresar = jsonObj.toString();
	}
%>

<%= infoRegresar %>
