Ext.onReady(function(){

	var TipoPlazo = "";

//-------------Handlers----------------------------------------------------------------------------	
	var RenPlazo = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.TIPOPLAZO == "F")
									return 'Factoraje';
								else if(record.data.TIPOPLAZO == "C"){
									return 'Cobranza';
								}else if (record.data.TIPOPLAZO == "O"){
									return 'Factoraje Obra P�blica'
								}							
						}
							
	
	var RenCartera = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.TIPOCARTERA == "1")
									return '1er Piso';
								else 
									return '2do Piso';
						}								
	
	
	var limpiaOperacion = function(){
		Ext.getCmp('radioFactoraje').setValue(false);
		Ext.getCmp('radioCobranza').setValue(false);
		Ext.getCmp('radioFactorajeObra').setValue(false);
		TipoPlazo = "";
	}  
	
	
	var limpiaCredito = function(){
		Ext.getCmp('radioSi').setValue(false);
		Ext.getCmp('radioNo').setValue(false);
		TipoPlazo = "";		
	}
	
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton = Ext.getCmp('btnGenerarArchivo');
			boton.enable();
			Ext.getCmp('btnImprimir').enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');	
		if(arrRegistros!=null){
			Ext.getCmp('btnConsultar').enable();
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnImprimir').enable();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnGenerarArchivo').disable();
				Ext.getCmp('btnImprimir').disable();				
			}
		}
	}
	
	
	var procesarMoneda = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboMoneda').setValue('');
	}
	
	
	var procesarEpo = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboEpo').setValue('');
	}
	
	
	var procesarProducto = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboProducto').setValue('');
		estatusActualProducto= Ext.getCmp('idComboProducto').getValue(); 
	}
	
	
	var procesarCartera = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboCartera').setValue('');
	}		
	

	var procesarDatosEPO = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('operaMesesEPO').setValue(info.operaMesesEPO); //F09-2015
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
//-------------Stores--------------------------------------------------------------------------  	
	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15basesoperc2Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'BASEOPERACION'},
			{name: 'RAZONSOCIAL'},
			{name: 'CODIGOBASE'},
			{name: 'PLAZOMINIMO'},
			{name: 'PLAZOMAXIMO'},
			{name: 'DESCRIPCION'},
			{name: 'NOMBRE'},
			{name: 'RECURSO'},
			{name: 'MONEDA'},
			{name: 'TIPOPLAZO'},
			{name: 'TIPOCARTERA'},
			{name: 'PANTALLA'},
			{name: 'TIPOPAGO'}		 	
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
		
	var CatalogoMoneda = new Ext.data.JsonStore
	({
		id: 'catMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperc2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoMoneda'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarMoneda,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});  
	
	var CatalogoEpo = new Ext.data.JsonStore
	({
		id: 'catEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperc2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarEpo,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
	
	
	var CatalogoProducto = new Ext.data.JsonStore
	({
		id: 'catProducto',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperc2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoProducto'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarProducto,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});  
	
  var dataTipoPago = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['','Seleccionar'],
			['1','Financiamiento con intereses '],
			['2','Meses sin intereses ']
		]		
	 });
	 
	var dataCartera = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['','Seleccionar'],
			['1','1er Piso'],
			['2','2do Piso']
		]		
	 });

		
	var Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);

	
//----------------COMPONENTES---------------------------------------------------------------------	
	var elementosForma = 
	[			
		{
			xtype: 'combo',
			fieldLabel: 'Nombre EPO',
			forceSelection: true,
			autoSelect: true,
			name:'claveEpo',
			id:'idComboEpo',
			displayField: 'descripcion',
			allowBlank: true,
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			autoLoad: false,
			typeAhead: true,
			minChars: 1,
			store: CatalogoEpo,  
			listeners:{
				select: function(combo){
					grid.hide();
					Ext.Ajax.request({
						url: '15basesoperc2Ext.data.jsp',
						params: {
							informacion: 'parametrizacionEPO',
							comboEpo:Ext.getCmp('idComboEpo').getValue()								
						},
						callback: procesarDatosEPO
					});
					
				}				
			},
			anchor: '99%'
		},
		//NE.util.getEspaciador(10),
		{
			xtype: 'compositefield',
			id: 'cf2',
			msgTarget: 'side',
			items: [
			{
				xtype: 'combo',
				fieldLabel: 'Moneda',
				forceSelection: true,
				autoSelect: true,
				name:'claveMoneda',
				id:'idComboMoneda',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoMoneda,  
				listeners:{
					select: function(combo){
						grid.hide();
					}					
				},				
				width:200
			}
			] 
		},
	//	NE.util.getEspaciador(10),
		{
			xtype: 'compositefield',
			id: 'cf3',
			msgTarget: 'side',
			items: [
			{
				xtype: 'combo',
				fieldLabel: 'Producto',
				forceSelection: true,
				autoSelect: true,
				name:'claveProducto',
				id:'idComboProducto',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoProducto,  
				listeners:{
					select: function(combo){
							if(Ext.getCmp('idComboEpo').getValue() == ''){
								Ext.getCmp('idComboEpo').markInvalid('Debe seleccionar una EPO para realizar la consulta');
							}else if(estatusActualProducto != combo.getValue()){
								//TipoPlazo = "";
								grid.hide();
								estatusActualProducto = combo.getValue();
								if(combo.getValue() == '1'){
									limpiaOperacion();
									Ext.getCmp('cf5').show();
									Ext.getCmp('cf6').hide();

								}else if(combo.getValue() == '5'){
									limpiaCredito();
									Ext.getCmp('cf6').show();
									Ext.getCmp('cf5').hide();
								}else{
									TipoPlazo = "";
									Ext.getCmp('cf6').hide();
									Ext.getCmp('cf5').hide();				
								}   
							}
					}		
				},	
				width:250
			}
			] 
		},	
		{
			xtype: 'compositefield',
			id: 'cf4',
			width:300,
			msgTarget: 'side',
			items: [
			{
				xtype: 'combo',
				fieldLabel: 'Tipo de Cartera',
				forceSelection: true,
				autoSelect: true,
				name:'claveCartera',
				id:'idComboCartera',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: dataCartera,  
				value: '',
				listeners:{
					select: function(combo){
						grid.hide();
						if(Ext.getCmp('idComboMoneda').getValue() == '1' &&  Ext.getCmp('idComboProducto').getValue() == '4' 	&&  Ext.getCmp('idComboCartera').getValue() == '2' && Ext.getCmp('operaMesesEPO').getValue() == 'S'   ){
							Ext.getCmp('idComboTipoPago').show();							
						}else {
							Ext.getCmp('idComboTipoPago').hide();	
						}
					}	
				},				
				width:180
			}
			]
		},
		{
			xtype: 'combo',
			fieldLabel: 'Tipo de Pago',
			forceSelection: true,
			hidden:true,
			autoSelect: true,
			name:'comboTipoPago',
			id:'idComboTipoPago',
			displayField: 'descripcion',
			allowBlank: true,					
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			autoLoad: false,
			typeAhead: true,
			minChars: 1,
			value: '',
			width	: 200,
			store: dataTipoPago,
			anchor: '40%'							
		},
				
		{
			xtype: 'compositefield',
			id: 'cf5',
			fieldLabel:'Tipo de Operaci�n',
			hidden:true,
			msgTarget: 'side',
			items: [
			{ 
				xtype: 'radio',
				name: 'tipo_plazo',
				id: 'radioFactoraje',
				listeners:{
					check:function(radio){
								if (radio.checked){
									grid.hide();
									TipoPlazo = "F"
								}
							}
				}				
			},	
			{
				xtype: 'displayfield',
				value: 'Factoraje	',
				width: 100
			}, 				
			{ 
				xtype: 'radio',
				name: 'tipo_plazo',
				id: 'radioCobranza',
				listeners:{
					check:function(radio){
								if (radio.checked){
									grid.hide();
									TipoPlazo = "C"
								}
							}
				}					
			},
			{
				xtype: 'displayfield',
				value: 'Cobranza ',
				width: 100
			},
			{ 
				xtype: 'radio',
				name: 'tipo_plazo',
				id: 'radioFactorajeObra',
				listeners:{
					check:function(radio){
								if (radio.checked){
									grid.hide();
									TipoPlazo = "O"
								}
							}
				}				
			},
			{
				xtype: 'displayfield',
				value: 'Factoraje Obra P�blica ',
				width: 200
			}			
			]
		},	
		{
			xtype: 'compositefield',
			id: 'cf6',
			hidden: true,
			fieldLabel:'Cr�dito de Exportaci�n',
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioSi',
					listeners:{
						check:function(radio){
									if (radio.checked){
										grid.hide();
										TipoPlazo = "E"
									}
								}
					}					
				},	{
					xtype: 'displayfield',
					value: 'Si	',
					width: 100
				}, 			
				{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioNo',
					listeners:{
						check:function(radio){
									if (radio.checked){
										grid.hide();
										TipoPlazo = "C"
									}
								}
					}					
				},
				{
					xtype: 'displayfield',
					value: 'No ',
					width: 100
				}				
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'operaMesesEPO', 	value: '' }		
	]// fin elementos forma
	
	
	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: false,
		title: '<center>Consultar Cadenas</center>',
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
				buttons:[{
						xtype:	'button',
						id:		'btnConsultar',
						text:		'Consultar',
						iconCls: 'icoBuscar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							if(Ext.getCmp('idComboEpo').getValue() == ''){
									Ext.getCmp('idComboEpo').markInvalid('Debe seleccionar una EPO para realizar la consulta');
							}else{
								grid.show();
								if(Ext.getCmp('idComboProducto').getValue() == '1'){
									Ext.getCmp('cf5').show();
									var cm = grid.getColumnModel();
									grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOPLAZO'), false);
								}else{
									if(Ext.getCmp('idComboProducto').getValue() == '5')
										Ext.getCmp('cf6').show();
									var cm = grid.getColumnModel();
									grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOPLAZO'), true);
								}
								
								consultaDataGrid.load({
									params: {
													comboEpo:Ext.getCmp('idComboEpo').getValue(),
													comboMoneda:Ext.getCmp('idComboMoneda').getValue(),
													comboProducto:Ext.getCmp('idComboProducto').getValue(),
													comboCartera:Ext.getCmp('idComboCartera').getValue(),
													tipoPlazo: TipoPlazo,
													comboTipoPago: Ext.getCmp('idComboTipoPago').getValue()
												}
								});
							}	
						},
						style: 'margin:0 auto;',
						align:'rigth'
					},{
						xtype:	'button',
						id:		'btnCancel',
						text:		'Limpiar',
						iconCls: 'icoLimpiar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							window.location = '15basesoperc2Ext.jsp';
						},
						style: 'margin:0 auto;'
					},{
						xtype:	'button',
						id:		'btnRegresar',
						text:		'Regresar',
						iconCls: 'icoContinuar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							window.location = '15basesoperExt.jsp';
						},
						style: 'margin:0 auto;'
					}
					
					],
		labelWidth: 140,
		style: 'margin:0 auto;',	   
		height: 215,
		width: 890
	});



	var grid = new Ext.grid.GridPanel( {
		store: consultaDataGrid,
		sortable: true,
		id: 'grid',
		hidden: true,
		columns: [
			{
				header: '<center>Nombre EPO</center>',
				dataIndex: 'RAZONSOCIAL',
				width: 280,
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';								
								return value;
							},				
				align: 'left'				
			},{
				header: '<center>Moneda</center>',
				dataIndex: 'MONEDA',
				align: 'left',
				width: 170
			},{
				header: '<center>Producto</center>',
				dataIndex: 'NOMBRE',
				align: 'left',
				width: 170
			},{
				header: '<center>Tipo de Cartera</center>',
				dataIndex: 'TIPOCARTERA',
				align: 'left',
				width: 100,
				renderer: RenCartera
			},
				{
				header: '<center>Tipo de Pago<c/enter>',
				dataIndex: 'TIPOPAGO',
				align: 'left',
				width: 110				
			},			
			{
				header: '<center>Tipo Operaci�n</center>',
				dataIndex: 'TIPOPLAZO',
				hidden:true,
				align: 'left',
				width: 130,
				renderer: RenPlazo
			},{
				header: 'Plazo M�nimo',
				dataIndex: 'PLAZOMINIMO',
				align: 'center',
				width: 100
			},{
				header: 'Plazo M�ximo',
				dataIndex: 'PLAZOMAXIMO',
				align: 'center',
				width: 100
			},{
				header: 'C�digo Base',
				dataIndex: 'CODIGOBASE',
				align: 'center',
				width: 100
			},{
				header: '<center>Descripci�n</center>',
				dataIndex: 'DESCRIPCION',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								return value;
							},				
				width: 380
			},{
				header: '<center>Acreditado</center>',
				dataIndex: 'RECURSO',
				align: 'left',
				width: 80
			}			
		],
		
		loadMask: true,
		style: 'margin:0 auto;',
		height: 250,
		bbar: {
					autoScroll:true,
					id: 'barra',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnGenerarArchivo',
						text:		'Generar Archivo',
						iconCls: 'icoXls',
						width: 90,
						weight: 70,
						style: 'margin:0 auto;',
						align:'rigth',
						handler: function(boton, evento) {
							boton.disable();
							Ext.Ajax.request({
								url: '15basesoperc2Ext.data.jsp',
								params: {
								comboEpo:Ext.getCmp('idComboEpo').getValue(),
													comboMoneda:Ext.getCmp('idComboMoneda').getValue(),
													comboProducto:Ext.getCmp('idComboProducto').getValue(),
													comboCartera:Ext.getCmp('idComboCartera').getValue(),
													tipoPlazo: TipoPlazo,
													informacion:'ArchivoCSV',
													comboTipoPago: Ext.getCmp('idComboTipoPago').getValue()
								},	
								callback: procesarDescargaArchivos
							});
						}
					},	
					{	
						xtype:	'button',
						id:		'btnImprimir',
						iconCls: 'icoPdf',
						text:		'Imprimir',
						width: 90,
						weight: 70,
						style: 'margin:0 auto;',
						align:'rigth',
						handler: function(boton, evento) {
							boton.disable();
							Ext.Ajax.request({
								url: '15basesoperc2Ext.data.jsp',
								params: {
								comboEpo:Ext.getCmp('idComboEpo').getValue(),
													comboMoneda:Ext.getCmp('idComboMoneda').getValue(),
													comboProducto:Ext.getCmp('idComboProducto').getValue(),
													comboCartera:Ext.getCmp('idComboCartera').getValue(),
													tipoPlazo: TipoPlazo,
													informacion:'ArchivoPDF',
													comboTipoPago: Ext.getCmp('idComboTipoPago').getValue()
								},	
								callback: procesarDescargaArchivos
							});
						}    
					}    								
					]
					},
					width: 930,
					frame: true
			});



//------ Componente Principal -------------------------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [NE.util.getEspaciador(10),
				  fp,NE.util.getEspaciador(10),grid ]
	});

	CatalogoEpo.load();
	CatalogoProducto.load();
	CatalogoMoneda.load();

});