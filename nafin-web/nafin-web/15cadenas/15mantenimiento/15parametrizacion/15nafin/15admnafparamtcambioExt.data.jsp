<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.seguridadbean.*,,
		com.netro.parametrosgrales.*,	
		com.netro.toperativastcambio.*,	
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	if (informacion.equals("CatalogoMoneda")){						
		CatalogoSimple cat = new CatalogoSimple();
			
		cat.setTabla("comcat_moneda");
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre");
			
		infoRegresar=cat.getJSONElementos(); 	
	
	}else if (informacion.equals("ConsultaPerfil")){
		SimpleDateFormat fecha_hoy = new SimpleDateFormat("dd/MM/yyyy");
		String fecha_Actual = fecha_hoy.format(new java.util.Date());
		JSONObject 	jsonObj	= new JSONObject();

		com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
		
		String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);

		//BeanSegFacultad.validaFacultad( (String) strLogin, (String) strPerfil,(String) session.getAttribute("sesCvePerfilProt"), "15ADMNAFPARAMTCAMBIO", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("CLAVEPERFIL",cvePerf);
		jsonObj.put("FECHAHOY",fecha_Actual);
		
		infoRegresar = jsonObj.toString();

	}else if (informacion.equals("ConsultaGrid")){
		SimpleDateFormat fecha_hoy = new SimpleDateFormat("dd/MM/yyyy");
		String fecha_Actual = fecha_hoy.format(new java.util.Date());
		
		String cveMoneda	= (request.getParameter("comboMoneda")==null)?"":request.getParameter("comboMoneda");

		JSONArray registros       = new JSONArray();
		HashMap hm = new HashMap();
		HashMap valores = new HashMap(); 
		
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
 
		hm =(HashMap)BeanParametrosGrales.getMoneda(cveMoneda); 
		valores.put("CLAVEMONEDA", hm.get("CLAVEMONEDA"));
		valores.put("NOMBREMONEDA", hm.get("NOMBREMONEDA"));
		valores.put("Inicio",fecha_Actual);
		valores.put("Final", fecha_Actual);

		String consulta = "";
		JSONObject 	jsonObj	= new JSONObject();
		registros.add(valores);	
		consulta	=	"{\"success\": true, \"total\": \""	+ registros.size() + "\", \"registros\": " + registros.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("Borrar")){
	
		JSONObject 	jsonObj	= new JSONObject();
		String txtFechaAplic 	= request.getParameter("txtFechaAplic");
		String strMensaje 	= "";
		String txtCveMoneda 		= request.getParameter("txtCveMoneda");
		
		ITOperativasTCambio BeanTCambio = ServiceLocator.getInstance().lookup("TOperativasTCambioEJB",ITOperativasTCambio.class);
	
		try{
			BeanTCambio.vborrarTCambio(txtFechaAplic,txtCveMoneda);
			strMensaje = "El tipo de cambio ha sido borrado";	}
		catch(Exception e){
			strMensaje = ""+e;
		}	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("MENSAJE",strMensaje);
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("Aceptar")){
		JSONObject 	jsonObj	= new JSONObject();
		String txtCveMoneda 		= request.getParameter("txtCveMoneda");
		String txtValorCompra 	= request.getParameter("txtValorCompra");

		String txtFechaAplic 	= request.getParameter("txtFechaAplic");
		String txtFechaAplicfin = (request.getParameter("txtFechaAplicfin")==null)?"":request.getParameter("txtFechaAplicfin");
		String strMensaje 	= "El tipo de cambio ha sido actualizado";

		ITOperativasTCambio BeanTCambio = ServiceLocator.getInstance().lookup("TOperativasTCambioEJB",ITOperativasTCambio.class);
	
		try{
			BeanTCambio.vactualizarTCambio(txtFechaAplic,txtCveMoneda,txtValorCompra, txtFechaAplicfin);
		}
		catch(Exception e){
			strMensaje = ""+e;
		}	
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("MENSAJE",strMensaje);
			infoRegresar = jsonObj.toString();
	
	}else if (informacion.equals("ConsultaGrid2")){
		
		String cboMoneda = request.getParameter("cboMoneda");
		String txtDesde = request.getParameter("txtDesde");
		String txtHasta = request.getParameter("txtHasta");
				
		String cveMoneda	= (request.getParameter("comboMoneda")==null)?"":request.getParameter("comboMoneda");
		JSONArray registros       = new JSONArray();
		HashMap hm = new HashMap();
		HashMap valores = new HashMap(); 
		
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
		
		hm =(HashMap)BeanParametrosGrales.consultaValorMoneda(txtDesde,txtHasta,cboMoneda); 
		valores.put("VALORCOMPRA", hm.get("VALORCOMPRA"));
		valores.put("VALORVENTA", hm.get("VALORVENTA"));
		valores.put("FECHA",hm.get("FECHA"));

		String consulta = "";
		JSONObject 	jsonObj	= new JSONObject();
		registros.add(valores);	
		consulta	=	"{\"success\": true, \"total\": \""	+ registros.size() + "\", \"registros\": " + registros.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString();	
	}
%>	

<%= infoRegresar %>