<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		netropology.utilerias.*,
		com.netro.catalogos.*,
		com.netro.model.catalogos.*,
		javax.naming.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String interFinanciero = (request.getParameter("interFinanciero")!=null)?request.getParameter("interFinanciero"):"";
String tipoFinan = (request.getParameter("cmbTipoFinan")!=null)?request.getParameter("cmbTipoFinan"):"";
String producto = (request.getParameter("producto")!=null)?request.getParameter("producto"):"";
String infoRegresar	=	"", mensaje = "", clave="", descripcion = "",  consulta ="";
String cg_razon_social = "", ic_epo= "", cs_tipo_fondeo="", cg_tipo_comision = "", cs_recorte_solic = ""  ;
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
JSONObject jsonObjG = new JSONObject();
boolean bandera = true;
Vector vctFilas = new Vector();
Vector vctFilasAux = new Vector();

ActualizacionCatalogos actualizacionCatalogos = ServiceLocator.getInstance().lookup("ActualizacionCatalogosEJB",ActualizacionCatalogos.class);

if (informacion.equals("catalogoIF") ) {
	CatalogoElementosIF catIF = new CatalogoElementosIF();
	catIF.setClave("ic_if");
	catIF.setDescripcion("cg_razon_social");
	catIF.setOrden("cg_razon_social");
	Vector vecFilas = catIF.getListaElementos();
	for(int i=0;i<vecFilas.size();i++) {
		Vector vecColumnas = (Vector)vecFilas.get(i);
		clave = (String)vecColumnas.get(1);
		descripcion = (String)vecColumnas.get(0);
		
		datos = new HashMap();
		datos.put("clave", clave);
		datos.put("descripcion", descripcion);
		registros.add(datos);
	}
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObjG = JSONObject.fromObject(consulta);
	infoRegresar = jsonObjG.toString();
}else if(informacion.equals("DatosIniciales") ) {
		String  obtenerPorcentaje=actualizacionCatalogos.consultarPorcentajeComision("ADMIN NAFIN");
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("obtenerPorcentaje", obtenerPorcentaje);
		jsonObj.put("success",new Boolean(true));
		infoRegresar= jsonObj.toString();
		
}else if(informacion.equals("Guardar_Datos")){
	String numRegistros  =(request.getParameter("numRegistros")!=null) ? request.getParameter("numRegistros"):"";
	int i_numReg = Integer.parseInt(numRegistros);
		for(int i = 0; i< i_numReg; i++){
			String epo[] = request.getParameterValues("epo1");
			String cmbTipoFondeo[] = request.getParameterValues("cmbTipoFondeo");
			String clavePC[] = request.getParameterValues("clavePC");
			if(actualizacionCatalogos.guardaDetalleComision(cmbTipoFondeo[i],"F","", interFinanciero,epo[i], clavePC[i], "ADMIN NAFIN")){
				bandera = true;
			}else{
			 bandera = false;
			}
		}
		if(bandera ==true){
			mensaje = "Datos Grabados.";
		}else{
			mensaje = "No hay cambios.";
		}
		JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("success", new Boolean(true));
		jsonObj1.put("mensaje", mensaje);
		jsonObj1.put("accion", "G");
		infoRegresar = jsonObj1.toString();

}else if(informacion.equals("Consultar")){
		if(producto.equals("1")){
			vctFilas = actualizacionCatalogos.consultaDetalleComision(interFinanciero, producto, "ADMIN NAFIN");
		}else if (producto.equals("4")){
			vctFilas = actualizacionCatalogos.consultaDetalleComision(interFinanciero, producto, "ADMIN NAFIN");
		}else{
			vctFilas = actualizacionCatalogos.consultaDetalleComision(interFinanciero, "1", "ADMIN NAFIN");
			vctFilasAux = actualizacionCatalogos.consultaDetalleComision(interFinanciero,"4", "ADMIN NAFIN");
		}
		for(int i=0;i<vctFilas.size();i++) {
		Vector vctColumnas = (Vector)vctFilas.get(i);
		cg_razon_social = (String) vctColumnas.get(0);
		ic_epo = (String) vctColumnas.get(1);
                if ( !tipoFinan.equals("") ){
                    cs_tipo_fondeo = tipoFinan;
                }else{
		cs_tipo_fondeo = (String) vctColumnas.get(2);
                }
		cg_tipo_comision = (String) vctColumnas.get(3);
		cs_recorte_solic = (String) vctColumnas.get(4);
		datos = new HashMap();
		datos.put("cg_razon_socialg", cg_razon_social);
		datos.put("ic_epo", ic_epo);
		datos.put("cs_tipo_fondeo", cs_tipo_fondeo);
		datos.put("cg_tipo_comision", cg_tipo_comision);
		datos.put("cs_recorte_solic", cs_recorte_solic);
		if(producto.equals("1")){
			datos.put("producto_comision", "Descuento Electrónico");
			datos.put("clavePC", "1");
		}
		else if(producto.equals("4")){
			datos.put("producto_comision", "Financiamiento a Distribuidores");
			datos.put("clavePC", "4");
		}else{
			datos.put("producto_comision", "Descuento Electrónico");
			datos.put("clavePC", "1");}
			registros.add(datos);
		}
		if(vctFilasAux.size()>0){
			for(int i=0;i<vctFilasAux.size();i++) {
				Vector vctColumnas = (Vector)vctFilasAux.get(i);
				cg_razon_social = (String) vctColumnas.get(0);
				ic_epo = (String) vctColumnas.get(1);
                                if ( !tipoFinan.equals("") ){
                                    cs_tipo_fondeo = tipoFinan;
                                }else{
				cs_tipo_fondeo = (String) vctColumnas.get(2);
                                }
				cg_tipo_comision = (String) vctColumnas.get(3);
				cs_recorte_solic = (String) vctColumnas.get(4);
				datos = new HashMap();
				datos.put("cg_razon_socialg", cg_razon_social);
				datos.put("ic_epo", ic_epo);
				datos.put("cs_tipo_fondeo", cs_tipo_fondeo);
				datos.put("cg_tipo_comision", cg_tipo_comision);
				datos.put("cs_recorte_solic", cs_recorte_solic);
				datos.put("producto_comision", "Financiamiento a Distribuidores");
				datos.put("clavePC", "4");
				registros.add(datos);
			}
		}
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObjG = JSONObject.fromObject(consulta);
		infoRegresar = jsonObjG.toString();
} 
%>
<%=infoRegresar%>


