Ext.onReady(function(){



	function procesarDescargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('btnArchivo').setIconClass('icoXls');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var consTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15proyectopefReporte.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},		
		fields: [	
		   {  name: 'DESCRIPCION' },
		   {  name: 'MONTO_PERIODO_TOTAL' },
			{  name: 'SALDO_PROMEDIO_TOTAL' },			
			{  name: 'MONTO_OBRA_PUBLICA_TOTAL' }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}		
	});
	
		var gridConsTotal = new Ext.grid.EditorGridPanel({	
		store: consTotalesData, 
		id: 'gridConsTotal',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '',
		clicksToEdit: 1,
		hidden: true,		 
		columns: [	
			{
				
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 250,			
				resizable: true,				
				align: 'center'	
			},				
			{
				dataIndex: 'MONTO_PERIODO_TOTAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				dataIndex: 'SALDO_PROMEDIO_TOTAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: '',
				tooltip: '',
				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				dataIndex: 'MONTO_OBRA_PUBLICA_TOTAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],					
		stripeRows: true,
		loadMask: true,
		height: 80,
		width: 900,		
		frame: true,
		displayInfo: true,		
		clicksToEdit: 1
	});
	
	
	//**************Consulta*********************+
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;
			
			if(store.getTotalCount() > 0) {				
				
				Ext.getCmp('gridConsTotal').show();	
				Ext.getCmp('btnSalir').enable();	
				Ext.getCmp('btnArchivo').enable();	
					
				el.unmask();					
			} else {	
				Ext.getCmp('btnSalir').disable();	
				Ext.getCmp('btnArchivo').disable();
				Ext.getCmp('gridConsTotal').hide();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15proyectopefReporte.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{  name: 'IC_EPO' },
			{  name: 'DEPENDENCIA' },
			{  name: 'MONTO_PERIODO' },
			{  name: 'SALDO_PROMEDIO' },
			{  name: 'PORCENTAJE_OBRA_PUBLICA' },
			{  name: 'MONTO_OBRA_PUBLICA' }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData, 
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		 
		columns: [	
			{
				header: 'No. EPO',
				tooltip: 'No. EPO',
				dataIndex: 'IC_EPO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Dependencia/Entidad',
				tooltip: 'Dependencia/Entidad',
				dataIndex: 'DEPENDENCIA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Monto Operado en el Periodo',
				tooltip: 'Monto Operado en el Periodo',
				dataIndex: 'MONTO_PERIODO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Saldo Promedio Diario del Periodo',
				tooltip: 'Saldo Promedio Diario del Periodo',
				dataIndex: 'SALDO_PROMEDIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: '% Correspondiente a Obra P�blica',
				tooltip: '% Correspondiente a Obra P�blica',
				dataIndex: 'PORCENTAJE_OBRA_PUBLICA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Monto Operado Correspondiente a Obra P�blica',
				tooltip: 'Monto Operado Correspondiente a Obra P�blica',
				dataIndex: 'MONTO_OBRA_PUBLICA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],					
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		displayInfo: true,		
		clicksToEdit: 1, 
		bbar: {			
			items: [					
				'->',
				{
					xtype: 'button',
					id: 'btnArchivo',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					handler: function(boton, evento) {	
						boton.setIconClass('loading-indicator');	
						Ext.Ajax.request({
							url: '15proyectopefReporte.data.jsp',
							params:{	
								informacion: 'GenerarArchivo',
								anio:Ext.getCmp("anio1").getValue(),
								mes:Ext.getCmp("mes1").getValue()
							},
							callback: procesarDescargaArchivo
						});					
					}
				},
				'-',				
				{
					xtype: 'button',
					id: 'btnSalir',
					text: 'Salir',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '15proyectopefReporteExt.jsp';					
					}
				}
			]
		}
	});
	
	///******************************Criterios de Busqueda**************************************
	var catalogoMes  = new Ext.data.JsonStore({
		id: 'catalogoMes',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefReporte.data.jsp',
		baseParams: {
			informacion: 'catalogoMes'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoAnio = new Ext.data.JsonStore({
		id: 'catalogoAnio',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefReporte.data.jsp',
		baseParams: {
			informacion: 'catalogoAnio'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	

	var  elementosForma = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Mes de consulta',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					width: 150,
					fieldLabel: 'Mes de Consulta',
					name: 'mes',
					id: 'mes1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',	
					hiddenName : 'mes',
					forceSelection : true,			
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoMes,
					tpl : NE.util.templateMensajeCargaCombo			
				},	
				{
					xtype: 'combo',
					width: 150,
					fieldLabel: 'Organismo',
					name: 'anio',
					id: 'anio1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',	
					hiddenName : 'anio',
					forceSelection : true,			
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoAnio,
					tpl : NE.util.templateMensajeCargaCombo			
				}
			]
		}		
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,							
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {					
				
					var mes = Ext.getCmp("mes1");
					var anio = Ext.getCmp("anio1");
					if (Ext.isEmpty(mes.getValue()) ){
						mes.markInvalid('El campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(anio.getValue()) ){
						anio.markInvalid('El campo es obigatorio');
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');		
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{					
							informacion: 'Consultar'						
						})
					});
					
					consTotalesData.load({
						params: Ext.apply(fp.getForm().getValues(),{					
							informacion: 'ConsultarTotales'						
						})
					});
					
				}
			}
		]
	});
	

	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: 'Etiquetar EPO por Niveles',			
				id: 'boton1',					
				handler: function() {
					window.location = '15proyectopefNivelesExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Etiquetar EPO por Secretaria',			
				id: 'boton2',					
				handler: function() {
					window.location = '15proyectopefExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<h1><b>Reporte Obra Publica </h1></b> ',			
				id: 'boton3',					
				handler: function() {
					window.location = '15proyectopefReporteExt.jsp';
				}
			}	
		]
	};
	
//------ Componente Principal -------------------------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [
			NE.util.getEspaciador(10),
			fpBotones,
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			gridConsulta,
			gridConsTotal,
			NE.util.getEspaciador(10)
		]
	});

	catalogoMes.load();
	catalogoAnio.load();
	

});