Ext.onReady(function(){
Ext.MessageBox.minWidth = 400;
//--------------------------------HANDLERS-----------------------------------
	//Funcion para cerrar servicios mediante una peticion Ajax 
	var cerrarServicio = function(sel){
		var selections = sel.getSelections();
		var chk_cierre_serv = new Array();
		if(selections.length <= 0){
			Ext.MessageBox.alert("Alerta","Debe seleccionar al menos una opci�n para el Cierre de Servicio");
			Ext.getCmp('btnCerrarServicio').enable();
			Ext.getCmp('btnCerrarServicio24Hrs').enable();
		}else{		
			for(i=0 ;i < selections.length; i++){			
				chk_cierre_serv[i] = selections[i].json.ID;
			}
			Ext.Ajax.request({
				url: '15horcierre01ext.data.jsp',
				params: {
					informacion: 'cerrarServicio',
					chk_cierre_servicio: chk_cierre_serv
				},
				callback: procesarCierreServicio
			});
		}
	}	
	
	//Funcion para mostrar las secciones de validacion de documentos.
	
	var validacionDocs = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		consultaData.each( function(record){
			if( record.data['HAY_DOC_SEL_PYME'] == 'S'  )
				Ext.getCmp('formaDocDescuentoE').show();
			if( record.data['HAY_DOC_SEL_PYME_DIST'] == 'S'  )
				Ext.getCmp('formaDocFinDist').show();
			if( record.data['HAY_DOC_SEL_IF'] == 'S'  )
				Ext.getCmp('formaDocDescuentoE2').show();
			if( record.data['HAY_DOC_SEL_IF_DIST'] == 'S'  )
				Ext.getCmp('formaDocFinDist2').show();
			if( record.data['BTN_CERRAR_ACTIVO'] == 'S'  )
				Ext.getCmp('btnCerrarServicio').enable();		
			else
				Ext.getCmp('btnCerrarServicio').disable();
		});
	}
	
	var validacionDocs24 = function(){
		consultaData24Hrs.each( function(record){
			if( record.data['SER_DESC'] == 'Servicio Abierto')
				Ext.getCmp('btnCerrarServicio24Hrs').enable();
			else
				Ext.getCmp('btnCerrarServicio24Hrs').disable();
		});
	}
	
	//Funcion para actualizar el grid si la peticion Ajax fue exitosa (cerrarServicio) 
	var procesarCierreServicio =  function(opts, success, response) {
		var grid = Ext.getCmp('grid');
		grid.el.unmask();
		if (success == true) {
			Ext.getCmp('btnCerrarServicio').enable();
			consultaData.load();
			consultaData24Hrs.load();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Funcion para cambiar estatus a negociable docs Descuento Electronico
	var cambiarEstatus = function(){
		Ext.Msg.show({
			title: "Confirmar",
			msg: "Este proceso cambiara todos los documentos con estatus: "+
						"\"Seleccionada Pyme\" a \"Negociable\" "+
						"�Desea continuar?",
			buttons: Ext.Msg.OKCANCEL,
			fn: function peticionAjax(btn){
				if (btn == 'ok') {
					Ext.Ajax.request({
						url: '15horcierre01ext.data.jsp',
						params: {
							informacion: 'cambiarEstatus'
						},
						callback: procesarCambiarEstatus
					});
				}
			},
			closable:false,
			icon: Ext.MessageBox.QUESTION
		});
	}
	
	//Funcion para cambiar estatus a negociable docs Financiamiento Dist
	var cambiarEstatusDist = function(){		
		Ext.Msg.show({
			title: "Confirmar",
			msg: "Este proceso cambiara todos los documentos con estatus: "+
						"\"Seleccionada Pyme\" a \"Negociable\" "+
						"�Desea continuar?",
			buttons: Ext.Msg.OKCANCEL,
			fn: function peticionAjax(btn){
				if (btn == 'ok') {
					Ext.Ajax.request({
						url: '15horcierre01ext.data.jsp',
						params: {
							informacion: 'cambiarEstatusDist'
						},
						callback: procesarCambiarEstatusDist
					});
				}
			},
			closable:false,
			icon: Ext.MessageBox.QUESTION
		});
	}
	
	//Funcion para mostrar # docs cambiados si peticion Ajax fue exitosa Descuento E
	var procesarCambiarEstatus =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa.
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var num = Ext.util.JSON.decode(response.responseText).NUM_AFECTADOS;
			Ext.getCmp('lblNumDocs').setText(num);
			Ext.getCmp('formaCambioEstatus').show();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Funcion para mostrar # docs cambiados si peticion Ajax fue exitosa Financiamiento Dist
	var procesarCambiarEstatusDist =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa.
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var num = Ext.util.JSON.decode(response.responseText).NUM_AFECTADOS
			Ext.getCmp('lblNumDocs').setText(num);
			Ext.getCmp('formaCambioEstatus').show();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------------STORES------------------------------------
	
	//store para combo de productos
	var catalogoProducto = new Ext.data.JsonStore({
		id: 'catalogoProductoStore',
		root: 'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url: '15horcierre01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoProducto'
		},
		totalProperty: 'Total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//store para grid de estatus servicio
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15horcierre01ext.data.jsp',
		baseParams: {
			informacion: 'consultaProductos'
		},
		fields: [
					{name: 'ID'},
					{name: 'NOM_PRODUCTO'},
					{name: 'CIERRE_PYME'},
					{name: 'CIERRE_IF'},
					{name: 'CIERRE_NAFIN'},
					{name: 'SERVICIO_PYME'},
					{name: 'SERVICIO_IF'},
					{name: 'SERVICIO_NAFIN'},				
					{name: 'HAY_DOC_SEL_PYME'},
					{name: 'HAY_DOC_SEL_PYME_DIST'},
					{name: 'HAY_DOC_SEL_IF'},
					{name: 'HAY_DOC_SEL_IF_DIST'},
					{name: 'BTN_CERRAR_ACTIVO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: validacionDocs,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});
	
	//store para grid de estatus servicio 24 hrs
	var consultaData24Hrs = new Ext.data.JsonStore({
		root: 'registros',
		url: '15horcierre01ext.data.jsp',
		baseParams: {
			informacion: 'consultaProductos24Hrs'
		},
		fields: [
					{name: 'ID'},
					{name: 'NOM_PRODUCTO'},					
					{name: 'TODOS_DENTRO'},
					{name: 'SER_DESC'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: validacionDocs24,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});
	
	//Store para obtener el numero de registros cambiados Descuento E.
	var cambiarEstatusData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15horcierre01ext.data.jsp',
		baseParams: {
			informacion: 'cambiarEstatus'
		},
		fields: [
					{name: 'NUM_AFECTADOS'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});
	
	//Store para obtener el numero de registros cambiados Financiamiento Dist.
	var cambiarEstatusDistData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15horcierre01ext.data.jsp',
		baseParams: {
			informacion: 'cambiarEstatusDist'
		},
		fields: [
					{name: 'NUM_AFECTADOS'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});
//------------------------------COMPONENTES----------------------------------
	
	//Combobox de productos
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_producto',
			id: 'cmbProducto',
			allowBlank: true,
			fieldLabel : 'Producto',
			mode: 'local',
			displayField : 'descripcion',
			valueField: 'clave',
			editable: false,
			emptyText: 'Todos los Productos',
			forceSeleccion : false,
			triggerAction: 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoProducto,
			listeners:{
				select: function(combo){
					var cmbProducto = combo.getValue();
					fp.el.mask('Enviando...', 'x-mask-loading');		
					consultaData.load({
						params: {
							icProducto: cmbProducto						
						}						
					});
	   		}
			}		
		}
	];
	
	//Columna de checkbox
	var sm = new Ext.grid.CheckboxSelectionModel({
		header : ' ',
		checkOnly:true,
		hideable:true,
		singleSelect:false,
		id:'chkIR',
		renderer: function(value, metadata, record, rowindex, colindex, store) {
			if (record.data['CIERRE_PYME']!='S'){
				return '<div class="x-grid3-row-checker">&#160;</div>';
			}else{
				return '<div>&#160;</div>';
			}
		},
		width:30	  			
	});
	
	//Grid de estatus servicio
	var grid = new Ext.grid.GridPanel({
		id: 'grid',
		store: consultaData,
		height: 150,
		width: 518,
		hidden: false,
		margins: '20 0 0 0',
		align: 'center',
		style: 'margin:0 auto',
		title: 'Estado del servicio mismo d�a',
		sm: sm,
		columns:[
			sm,
			{
				header: 'Producto',
				tooltip: 'Producto',
				width: 180,
				dataIndex: 'NOM_PRODUCTO',
				sortable: true,
				resizable: false,
				align: 'left'
			},
			{
				header: 'Proovedores � Clientes',
				tooltip: 'Proovedores � Clientes',
				width: 140,
				dataIndex: 'SERVICIO_PYME',
				sortable: true,
				resizable: false,
				align: 'left',
				renderer: function(val, meta, record, rowIndex, colIndex, store) {
					if(val == 'Servicio Cerrado')
						return  '<div style="display:inline;color:red">' + val + '</div>'
					else
					return val
				}
			},			
			{
				header: 'Intermediarios Financieros',
				tooltip: 'Intermediarios Financieros',
				width: 160,
				dataIndex: 'SERVICIO_IF',
				sortable: true,
				resizable: false,
				align: 'left'
			}
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Cerrar Servicio PyME�S',
					id: 'btnCerrarServicio',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						cerrarServicio(sm);
					}
				},
				'-'
			]
		}

	});
	
	//Columna de checkbox
	var sm24 = new Ext.grid.CheckboxSelectionModel({
		header : ' ',
		checkOnly:true,
		hideable:true,
		singleSelect:false,
		id:'chkIR24',
		renderer: function(value, metadata, record, rowindex, colindex, store) {
			if (record.data['SER_DESC']!='Servicio Cerrado'){
				return '<div class="x-grid3-row-checker">&#160;</div>';
			}else{
				return '<div>&#160;</div>';
			}
		},
		width:30	  			
	});
	
	//Grid Estatus Servicio 24 hrs
	var grid24Hrs = new Ext.grid.GridPanel({
		id: 'grid24Hrs',
		store: consultaData24Hrs,
		height: 120,
		width: 358,
		hidden: false,
		margins: '20 0 0 0',
		align: 'center',
		style: 'margin:0 auto',
		title: 'Estado del servicio 24 Horas',
		sm: sm24,
		columns:[
			sm24,
			{
				header: 'Producto',
				tooltip: 'Producto',
				width: 180,
				dataIndex: 'NOM_PRODUCTO',
				sortable: true,
				resizable: false,
				align: 'left'
			},
			{
				header: 'Proovedores � Clientes',
				tooltip: 'Proovedores � Clientes',
				width: 140,
				dataIndex: 'SER_DESC',
				sortable: true,
				resizable: false,
				align: 'left',
				renderer: function(val, meta, record, rowIndex, colIndex, store) {
					if(val == 'Servicio Cerrado')
						return  '<div style="display:inline;color:red">' + val + '</div>'
					else
					return val
				}
			}
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Cerrar Servicio PyME�S',
					id: 'btnCerrarServicio24Hrs',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						cerrarServicio(sm24);
					}
				},
				'-'
			]
		}
	});
	
	//Panel para agregar Combobox productos
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 350,
		frame: true,
		align: 'center',
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid: true		
	});
	
	//Panel contador de Documentos cambiados a negociable
	var fpCambioEstatus = new Ext.form.FormPanel({
		id: 'formaCambioEstatus',
		frame: true,
		height: 30,
		hidden: true,
	//	width: 350,
		layout: {
        type: 'hbox',
        align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			flex: 1,
			align: 'left',
			items: [{
				xtype: 'label',
				name: 'lblCambioEstatus',
				id: 'lblCambioEstatus',
				text: 'Numero de documentos cambiados a Negociables: ',
				align: 'left'
			},
			{
				xtype: 'label',
				name: 'lblNumDocs',
				id: 'lblNumDocs',
				align: 'left'
			}
			]
		}]
	});		
	
	//Panel de validacion docs Servicio Cerrado - Servicio Abierto / Descuento E
	var fpDocDescuentoE = new Ext.form.FormPanel({
		id: 'formaDocDescuentoE',
		title: 'No se puede cerrar el servicio mientras existan documentos seleccionados pyme',
		frame: true,
		height: 60,
		hidden: true,
	//	width: 450,
		layout: {
        type: 'hbox',
        align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			flex: 3,
			align: 'left',
			items: [{
				xtype: 'label',
				name: 'lblCambiarDescuentoE',
				id: 'lblCambiarDescuentoE',
				text: 'Cambio autom�tico a documento negociable',
				align: 'left'
			}]
		},	{
			xtype: 'panel',
			flex: 1,
			items: [{
				xtype: 'button',
				name: 'btnCambiarDescuentoE',
				id: 'btnCambiarDescuentoE',
				text: 'Aceptar',
				handler: function(boton, evento){
						cambiarEstatus();					
					}
				}
			]
		}]
	});
	
	//Panel de validacion docs Servicio Cerrado - Servicio Abierto / Financiamiento Dist
	var fpDocFinDist = new Ext.form.FormPanel({
		id: 'formaDocFinDist',
		title: 'No se puede cerrar el servicio mientras existan documentos seleccionados pyme',
		frame: true,
		height: 60,
		hidden: true,
	//	width: 450,
		layout: {
        type: 'hbox',
        align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			flex: 3,
			align: 'left',
			items: [{
				xtype: 'label',
				name: 'lblCambiarDocFinDist',
				id: 'lblCambiarDocFinDist',
				text: 'Cambiar automaticamente los documentos Distribuidores a Negociable',
				align: 'left'
			}]
		},	{
			xtype: 'panel',
			flex: 1,
			items: [{
				xtype: 'button',
				name: 'btnCambiarDocFinDist',
				id: 'btnCambiarDocFinDist',
				text: 'Aceptar',
				handler: function(boton, evento){
						cambiarEstatusDist();
					}
				}
			]
		}]
	});
	
	//Panel de validacion docs Servicio Cerrado - Servicio Cerrado - Servicio Abierto / Descuento E
	var fpDocDescuentoE2 = new Ext.form.FormPanel({
		id: 'formaDocDescuentoE2',
		frame: true,
		height: 40,
		hidden: true,
//		width: 350,
		layout: {
        type: 'hbox',
        align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			flex: 1,
			align: 'left',
			items: [{
				xtype: 'label',
				name: 'lblDocConsDescuentoE2',
				id: 'lblDocConsDescuentoE2',
				text: 'Existen documentos con estatus Seleccionada IF (Descuento Electronico). Si no se procesan estas solicitudes, no se podra realizar la apertura de servicio el proximo dia habil. �Favor de procesarlas!',
				align: 'left'
			}]
		}]
	});
	
	//Panel de validacion docs Servicio Cerrado - Servicio Cerrado - Servicio Abierto / Financiamiento Dist
	var fpDocFinDist2 = new Ext.form.FormPanel({
		id: 'formaDocFinDist2',
		frame: true,
		height: 60,
		align: 'center',
		hidden: true,
	//	width: '50%',
		layout: {
        type: 'hbox',
        align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			flex: 1,
			align: 'left',
			items: [{
				xtype: 'label',
				name: 'lblDocFinDist2',
				id: 'lblDocFinDist2',
				text: 'Existen solicitudes con estatus Seleccionada IF (Financiamiento a Distribuidores). Si no se procesan estas solicitudes, no se podra realizar la apertura de servicio el proximo dia habil. �Favor de procesarlos!',
				align: 'left'
			}]
		}]
	});
	
//-------------------------COMPONENTE PRINCIPAL------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 950,
		align: 'center',
		items: [
			fp,
			NE.util.getEspaciador(5),
			grid,
			NE.util.getEspaciador(5),
			fpCambioEstatus,
			NE.util.getEspaciador(5),
			fpDocDescuentoE,
			NE.util.getEspaciador(5),
			fpDocFinDist,
			NE.util.getEspaciador(5),
			fpDocDescuentoE2,
			NE.util.getEspaciador(5),
			fpDocFinDist2,
			NE.util.getEspaciador(5),
			grid24Hrs
		]
	});
	catalogoProducto.load();
	consultaData.load();
	consultaData24Hrs.load();
});
