Ext.onReady(function() {
	var esEsquemaExtJS =  Ext.getDom('esEsquemaExtJS').value;
//- - - - - - -  - - - - - HANDLER�S - - - - - - - - - - - - - - - - - - - - - -
	function procesaConsultaValores(opts, success, response) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = Ext.util.JSON.decode(response.responseText);
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if (jsonData != null){	
				var mensaje = jsonData.mensaje;
				Ext.getCmp('btnGuardar').enable();		
				if(jsonData.accion=='G') {
					Ext.MessageBox.alert('Mensaje',mensaje);		
				}
			}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessDatosIniciales = function(opts, success, response) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var obtenPorcentaje = Ext.getCmp('tfComision').setValue(jsonData.obtenerPorcentaje);
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesarGrabar() {
		var porcentajeComisionFija=Ext.getCmp('tfComision');
		var b;
		var c;
		if (Ext.isEmpty(porcentajeComisionFija.getValue())){
			porcentajeComisionFija.markInvalid("Escriba una cantidad n�merica.");
			return;
		}
		if (porcentajeComisionFija.getValue() == 0){
			porcentajeComisionFija.markInvalid("Debe escribir cuando menos un entero.");
			return;
		}
		if (porcentajeComisionFija.getValue()  > 99.99){
			porcentajeComisionFija.markInvalid("El tama�o m�ximo del campos es de 2 enteros 2 decimales");
			return;
		}
		fp.el.mask('Procesando...', 'x-mask-loading');	
		Ext.Ajax.request({
			url: '15porcentajeComisionFija.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{  
			informacion: "Guardar_Datos"
			}),
			callback: procesaConsultaValores
		});
			
	}

	var elementosFormaConsulta = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Comisi�n Fija',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [				
				{
					xtype: 'numberfield',
					name: 'comision',
					id: 'tfComision',
					displayField: 'descripcion',	
					maxText:'El tama�o m�ximo del campos es de 2 enteros 2 decimales',				
					maxValue: '99.99',
					allowDecimals: true,
					allowNegative: false,
				//	maxLength: 5,
					width			: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: '%',
					width: 5
				}
			]	
		}
		];
	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
		hidden: 		true,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: 'Porcentaje Comisi�n Fija',			
				id: 'boton1',					
				handler: function() {
					window.location = '15porcentajeComisionFija.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Parametrizaci�n IF',			
				id: 'boton2',					
				handler: function() {
					window.location = '15parametrizacionIf.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Detalle IF',			
				id: 'boton3',					
				handler: function() {
					window.location = '15parametrizacionDetalleIf.jsp';
				}
			}	
		]
	};

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		width:			290,
		style:			'margin:0 auto;',
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		100,
		collapsible: true,
		title:'Porcentaje Comisi�n Fija',
		items:			elementosFormaConsulta,
		buttons: [		
			{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls: 'icoGuardar',
				handler: procesarGrabar
			}
		]
	});
//----------------------------------- CONTENEDOR -------------------------------------
	var contenedorPrincipal = new Ext.Container({
		applyTo: 'areaContenido',
		width: 	930,
		style: 'margin:0 auto;',
		align: 'center',
		items: 	[
			NE.util.getEspaciador(10),
			fpBotones,
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(20)
		]

	});
	fp.el.mask('Cargando...','x-mask-loading');
	Ext.Ajax.request({
		url: '15porcentajeComisionFija.data.jsp',
		params: {
			informacion: 'DatosIniciales'
		},
		callback: procesarSuccessDatosIniciales
	});
	 if(esEsquemaExtJS!='true'){
		var btn1 = Ext.getCmp('fpBotones');
		btn1.show();
	}
});