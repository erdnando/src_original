Ext.onReady(function(){ 

	var claveEpo = "";
	var gIF = "";	
	var gEPO = "";
	var gPV = "";
	var gMoneda = "";
	var montoVigente = "";

//-------------Handlers------------------------------------------------------------------------------------

	var RenRango = function(value, metadata, record, rowindex, colindex, store) {
			var RANGOPUBLICINI = "";
			if(record.data.RANGOPUBLIC == "0.01")
				RANGOPUBLICINI = "1";
			else
				RANGOPUBLICINI = record.data.RANGOPUBLIC 
			return 'De  '+  Ext.util.Format.number(RANGOPUBLICINI, '$0,0.00') + '  hasta  '+ Ext.util.Format.number(record.data.RANGOPUBLICFIN, '$0,0.00');
		}


	var procesarConsultaData = function(store,arrRegistros,opts){
			grid.setVisible(true);
			gridDetallado.setVisible(false);
			
			fp2.setVisible(true);
			fp2.setHeight(75);
			
			var fepo = Ext.getCmp('fieldEPO');
			var fif = Ext.getCmp('fieldIF');
			var fpv = Ext.getCmp('fieldPV');
			
			gIF = Ext.getCmp('idComboIF').getValue();
			gEPO = Ext.getCmp('idComboEpo').getValue(); 
			gMoneda = Ext.getCmp('idComboMoneda').getValue();
			
			
			fif.setValue(Ext.getCmp('idComboIF').getRawValue());
			fepo.setVisible(false);
			fpv.setVisible(false);
			
			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){			
				el.unmask();
				Ext.getCmp('btnPDF').enable();
				Ext.getCmp('btnCSV').enable();
				Ext.getCmp('btnConsultar').enable();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					fp2.setVisible(false);
					Ext.getCmp('btnPDF').disable();
					Ext.getCmp('btnCSV').disable();
					Ext.getCmp('btnConsultar').enable();
				}
			}
		}

	
	var procesarConsultaData2 = function(store,arrRegistros,opts){
			montoVigente = store.reader.jsonData.MONTO;			
			gridDetallado.setVisible(true);
			grid.setVisible(false);
			
			gIF = Ext.getCmp('idComboIF').getValue();
			gEPO = Ext.getCmp('idComboEpo').getValue(); 
			gMoneda = Ext.getCmp('idComboMoneda').getValue();
			
			fp2.setVisible(true);
			fp2.setHeight(150);
			
			var fepo = Ext.getCmp('fieldEPO');
			var fif = Ext.getCmp('fieldIF');
			var fpv = Ext.getCmp('fieldPV');
			
			fif.setValue(Ext.getCmp('idComboIF').getRawValue());
			fepo.setValue(Ext.getCmp('idComboEpo').getRawValue());
			fpv.setValue(montoVigente);
			
			fepo.setVisible(true);
			fpv.setVisible(true);
			
			if(arrRegistros!=null){
				var el = gridDetallado.getGridEl();
				if(store.getTotalCount()>0){
					el.unmask();
					Ext.getCmp('btnPDF2').enable();
					Ext.getCmp('btnCSV2').enable();
					Ext.getCmp('btnConsultar').enable();				
				}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					fp2.setVisible(false);
					Ext.getCmp('btnPDF2').disable();
					Ext.getCmp('btnCSV2').disable();		
					Ext.getCmp('btnConsultar').enable();
				}
			}
		}

	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
			Ext.getCmp('btnPDF').enable();
			Ext.getCmp('btnPDF2').enable();
			Ext.getCmp('btnCSV').enable();
			Ext.getCmp('btnCSV2').enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		
	var verDetallado = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave = registro.get('IC_EPO'); 
		claveEpo = clave;
		Ext.getCmp('idComboEpo').setValue(claveEpo);
		ConsultaDetallado.load({ params:{
					operacion: 'Generar', 
					start: 0,
					limit: 15}
		}); 
	}


	var procesarTodosEpo = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 		
		store.insert(1,new Todas({ 
		clave: "Todas", 
		descripcion: "Todas las EPOS", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboEpo').setValue('');
		grid.setVisible(false);
		gridDetallado.setVisible(false);
		fp2.setVisible(false);
	}
		
	
	var procesarTodosMoneda = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 	
		store.commitChanges(); 
		Ext.getCmp('idComboMoneda').setValue('');
	}
	
	
	var procesarTodosIf = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 		
		store.commitChanges(); 
		Ext.getCmp('idComboIF').setValue('');
	}


//-------------Stores----------------------------------------------------------------------------------
	
	Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);


	var CatalogoEpo= new Ext.data.JsonStore({
		id: 'catEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ReporteIfExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoEpo'
		},
		autoLoad: false, 
		listeners:
		{
		 load: procesarTodosEpo,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	  });


	var CatalogoMoneda= new Ext.data.JsonStore({
		id: 'catMon',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ReporteIfExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoMoneda'
		},
		autoLoad: false, 
		listeners:
		{
		 load: procesarTodosMoneda,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	  });

	
	var CatalogoIF= new Ext.data.JsonStore({
		id: 'catIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ReporteIfExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoIF'
		},
		autoLoad: false, 
		listeners:
		{
		 load: procesarTodosIf,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	  });


	var ConsultaDataGrid = new Ext.data.JsonStore({
		root: 'registros',
		url: '15ReporteIfExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaDataGrid'
		},
		fields: [
				{name: 'RANKING'},
				{name: 'NO_NAFIN_ELECTRONICO'},
				{name: 'RFC'},
				{name: 'IC_EPO'},
				{name: 'EPO'},
				{name: 'MONEDA'},
				{name: 'PUBLICACION_VIGENTE'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			beforeLoad:	{fn: function(store, options){
					Ext.apply(options.params, {
									comboEpo:Ext.getCmp('idComboEpo').getValue(), 
									comboIF:	Ext.getCmp('idComboIF').getValue(),
									comboMoneda:Ext.getCmp('idComboMoneda').getValue()
					});
			}
		},
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					}
			}
		}
	});


	var ConsultaDetallado = new Ext.data.JsonStore({
		root: 'registros',
		url: '15ReporteIfExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetallado'
		},
		fields: [
				{name: 'RANKING'},
				{name: 'PYME'},
				{name: 'NAFINELECTRONICO'},
				{name: 'IN_NUMERO_SIRAC'},
				{name: 'RFC'},
				{name: 'MONEDA'},
				{name: 'TELEFONO'},
				{name: 'RAZONSOCIALPROVEEDOR'},
				{name: 'RANGOPUBLIC'},
				{name: 'RANGOPUBLICFIN'},
				{name: 'PUBLICACION_VIGENTE'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaData2,
		beforeLoad:	{fn: function(store, options){
				var epo = "";
				if (Ext.getCmp('idComboEpo').getValue() == 'Todas'){
					epo = claveEpo;
				}else{
					epo = Ext.getCmp('idComboEpo').getValue();
				}
					
				Ext.apply(options.params, {
							comboEpo: epo,
							comboIF:	Ext.getCmp('idComboIF').getValue(),
							comboMoneda:Ext.getCmp('idComboMoneda').getValue()
				});
			}
		},
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					}
			}
		}
	});



/**********CRITERIOS DE BUSQUEDA ***************/		

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: 'Parametrizaci�n',			
				id: 'btnParametrizacion',					
				handler: function() {
					window.location = '15CajonesEPOExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<b>Reportes</b>',			
				id: 'btnReportes',					
				handler: function() {
					window.location = '15ReporteIfExt.jsp';
				}
			}	
		]
	});



//--------------Componentes------------------------------------------------------------------------------
	var elementosForma = 
	[	
		{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'cfIF',
			items: [	
			{
				xtype: 'combo',
				fieldLabel: 'Intermediario Financiero',
				forceSelection: true,
				autoSelect: true,
				name:'claveIF',
				id:'idComboIF',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: CatalogoIF,  
				width:400,
				listeners:{
					select: function(combo){
						CatalogoEpo.load({
							params: {intermediario:combo.getValue() }
						});
					}
				}   			
			}]
		},	{
				xtype: 'combo',
				combineErrors: false,
				fieldLabel: 'Nombre de la EPO',
				forceSelection: true,
				autoSelect: true,
				name:'claveEpo',
				id:'idComboEpo',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoEpo,  
				width:270		
		}, {
				xtype: 'compositefield',
				combineErrors: false,
				id: 'cfMoneda',		
				items: [		
				{
					xtype: 'combo',
					fieldLabel: 'Moneda',
					forceSelection: true,
					autoSelect: true,
					name:'claveMoneda',
					id:'idComboMoneda',
					displayField: 'descripcion',
					allowBlank: true,
					editable:true,
					valueField: 'clave',
					mode: 'local',
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					store: CatalogoMoneda,  
					width:270   
				} ]
			}]


	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		title: '<center>Reporte IF</center>',
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
		style: 'margin:0 auto;',	   
		height: 200,
		buttons: [{
		text: 'Consultar',
		id: 'btnConsultar',
		iconCls: 'icoBuscar',
		handler: function (boton,evento){ 
						if (Ext.getCmp('idComboMoneda').getValue() == "" || Ext.getCmp('idComboEpo').getValue() == "" || Ext.getCmp('idComboIF').getValue() == "" ) {
							//Ext.MessageBox.alert("Alerta","Debe Seleccionar un IF, EPO, y tipo de Moneda");
							if(Ext.getCmp('idComboMoneda').getValue() == ""){
								Ext.getCmp('idComboMoneda').markInvalid("Debe seleccionar un tipo de Moneda");
							}
							if(Ext.getCmp('idComboEpo').getValue() == ""){
								Ext.getCmp('idComboEpo').markInvalid("Debe seleccionar un EPO");
							}
							if(Ext.getCmp('idComboIF').getValue() == ""){
								Ext.getCmp('idComboIF').markInvalid("Debe seleccionar un IF");
							}
						}else {
						boton.disable();						
							if(Ext.getCmp('idComboEpo').getValue() == 'Todas'){
								ConsultaDataGrid.load({ params:{
								operacion: 'Generar', 
								start: 0,
								limit: 15}
								});	
							}
							else{
								ConsultaDetallado.load({ params:{
									operacion: 'Generar',
									start: 0,
									limit: 15}
								});
							}				
						} 
					} 
				},
		{
		text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function (boton,evento){ 
					window.location = '15ReporteIfExt.jsp';
				}
		}],
		width: 830
	});


	var elementosForma2 = [{
		xtype: 'displayfield',
		name: 'nameIF',
		id: 'fieldIF',
		allowBlank: false,
		fieldLabel: 'INTERMEDIARIO FINANCIERO ',
		value:''
	},{
		xtype: 'displayfield',
		name: 'nameEPO',
		id: 'fieldEPO',
		allowBlank: false,
		fieldLabel:'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspEPO ',
		value:''
	},{
		xtype:'displayfield',
		fieldLabel: '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPUBLICACION VIGENTE ',
		id:'fieldPV'
	}]
	
	
	var fp2 = new Ext.form.FormPanel({
		id:'formaDetalle2',
		style: ' margin:0 auto;',
		hidden: true,
		labelWidth:200,
		frame: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma2,	   
		height: 150,
		width: 830
	});


	var grid = new Ext.grid.GridPanel({
			store: 		ConsultaDataGrid,
			id:			'grid',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			sortable: true,
			margins:		'20 0 0 0',
			title:		undefined,
			stripeRows: true,
			loadMask: 	true,
			height: 		420,
			width: 		920,
			frame: 		true,
			columns: [
				{
					header: 		'Ranking',
					tooltip: 	'Ranking',
					dataIndex: 	'RANKING',
					sortable: 	true,
					align:		'center',
					resizable: 	true,
					width: 		60,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'No Nafin Electr�nico',
					tooltip: 	'NO_NAFIN_ELECTRONICO',
					dataIndex: 	'NO_NAFIN_ELECTRONICO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'RFC',
					tooltip: 	'RFC',
					dataIndex: 	'RFC',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'EPO',
					tooltip: 	'EPO',
					dataIndex: 	'EPO',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		200,
					hidden: 		false,
					renderer: function(value,metadata,record,rowindex,colindex,store){
							metadata.attr = 'ext:qtip="' + value + '"';
							return value;
					},		
					hideable:	false
				},{
					header: 		'Moneda',
					tooltip: 	'Moneda',
					dataIndex: 	'MONEDA',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		145,
					hidden: 		false,
					renderer: function(value,metadata,record,rowindex,colindex,store){
							metadata.attr = 'ext:qtip="' + value + '"';
							return value;
					},	
					hideable:	false
				},{
					header: 		'Publicaci�n Vigente',
					tooltip: 	'Publicaci�n Vigente',
					dataIndex: 	'PUBLICACION_VIGENTE',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					hidden: 		false,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
				},{
	      xtype: 'actioncolumn',
				header: 'Reporte Detallado',
				tooltip: 'Reporte Detallado',
				width: 100,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoLupa';										
						},	
						handler: verDetallado
					}
				]				
			}
			],bbar: {
					xtype: 'paging',
					autoScroll:true,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: ConsultaDataGrid,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Imprimir',
					width:	100,
					iconCls: 'icoPdf',
					id: 		'btnPDF',
					handler: function(boton, evento) {
					boton.disable();
					var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");	
						Ext.Ajax.request({
							url: '15ReporteIfExt.data.jsp',
							params: {							
								informacion: 'ArchivoPDF',
									start: cmpBarraPaginacion.cursor,
									limit: cmpBarraPaginacion.pageSize,
									comboEpo:gEPO,
									comboIF: gIF,
									comboMoneda: gMoneda
							},
							callback: procesarDescargaArchivos
						});		
					}
				},'-',
				{
					xtype:	'button',
					text:		'Generar Archivo',
					width:	100,
					iconCls: 'icoXls',
					id: 		'btnCSV',
					handler: function(boton, evento) {
									boton.disable();
									Ext.Ajax.request({
										url: '15ReporteIfExt.data.jsp',
										params: {							
											informacion: 'ArchivoCSV',
											comboEpo:gEPO,
											comboIF: gIF,
											comboMoneda: gMoneda
										},
										callback: procesarDescargaArchivos
									});	
					}
				},'-'
			]
		}
	});


	var gridDetallado = new Ext.grid.GridPanel({
			store: 		ConsultaDetallado,
			id:			'gridD',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			sortable: true,
			title:		undefined,
			stripeRows: true,
			loadMask: 	true,
			height: 		420,
			width: 		930,
			frame: 		true,
			columns: [
				{
					header: 		'Ranking',
					tooltip: 	'Ranking',
					dataIndex: 	'RANKING',
					sortable: 	true,
					align:		'center',
					resizable: 	true,
					width: 		60,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'No Proveedor',
					tooltip: 	'No Proveedor',
					dataIndex: 	'PYME',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		115,
					hidden: 		false,
					hideable:	false
				},				
				{
					header: 		'No Nafin Electr�nico',
					tooltip: 	'NO_NAFIN_ELECTRONICO',
					dataIndex: 	'NAFINELECTRONICO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		115,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'N�mero de SIRAC',
					tooltip: 	'N�mero de SIRAC',
					dataIndex: 	'IN_NUMERO_SIRAC',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		115,
					hidden: 		false,
					hideable:	false
				},				
				{
					header: 		'RFC',
					tooltip: 	'RFC',
					dataIndex: 	'RFC',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		125,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Tel�fono del Contacto',
					tooltip: 	'Tel�fono del Contacto',
					dataIndex: 	'TELEFONO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		125,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Nombre o Raz�n Social',
					tooltip: 	'Nombre o Raz�n Social',
					dataIndex: 	'RAZONSOCIALPROVEEDOR',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		200,
					hidden: 		false,
					renderer: function(value,metadata,record,rowindex,colindex,store){
							metadata.attr = 'ext:qtip="' + value + '"';
							return value;
					},				
					hideable:	false
				},{
					header: 		'Moneda',
					tooltip: 	'Moneda',
					dataIndex: 	'MONEDA',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		125,
					hidden: 		false,
					renderer: function(value,metadata,record,rowindex,colindex,store){
							metadata.attr = 'ext:qtip="' + value + '"';
							return value;
					},				
					hideable:	false
				},{
					header: 		'Rango de publicaci�n',
					tooltip: 	'Rango de publicaci�n',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		210,
					hidden: 		false,
					hideable:	false,
					renderer:   RenRango
				}
			],bbar: {
					xtype: 'paging',
					autoScroll:true,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion2',
					displayInfo: true,
					store: ConsultaDetallado,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Imprimir',
					width:	100,
					iconCls: 'icoPdf',
					id: 		'btnPDF2',
					handler: function(boton, evento) {
									boton.disable();														
									var cmpBarraPaginacion = Ext.getCmp("barraPaginacion2");	
									Ext.Ajax.request({
										url: '15ReporteIfExt.data.jsp',
										params: {							
											informacion: 'ArchivoPDF2',
											start: cmpBarraPaginacion.cursor,
											limit: cmpBarraPaginacion.pageSize,					
											comboEpo: gEPO,
											comboIF: gIF,
											comboMoneda: gMoneda
									},
							callback: procesarDescargaArchivos
						});		
					}
				},'-',
				{
					xtype:	'button',
					text:		'Generar Archivo',
					width:	100,
					iconCls: 'icoXls',
					id: 		'btnCSV2',
					handler: function(boton, evento) {
									boton.disable();						
									Ext.Ajax.request({
										url: '15ReporteIfExt.data.jsp',
										params: {							
											informacion: 'ArchivoCSV2',								
											comboEpo: gEPO,
											comboIF: gIF,
											comboMoneda: gMoneda
										},
										callback: procesarDescargaArchivos
									});						
					}
				},'-'
			]
		}
	});


//------ Componente Principal -------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [fpBotones,NE.util.getEspaciador(30),fp,NE.util.getEspaciador(10),
		fp2,NE.util.getEspaciador(10), grid,NE.util.getEspaciador(10),gridDetallado]
	});

	CatalogoIF.load();
	CatalogoEpo.load();
	CatalogoMoneda.load();

});