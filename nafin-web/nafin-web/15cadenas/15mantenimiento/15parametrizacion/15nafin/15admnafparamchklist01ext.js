var funcionInterna;
function deshabilitaActivar(check, rowIndex, colIds){

		
	//if(check.name == 'epo5'+reg.get('EPO') || check.name == 'epo8'+reg.get('EPO')){
	var ischecked	= !check.checked;
	console.log(ischecked);
	if(!ischecked){	
		check.checked = false;
		Ext.MessageBox.alert("Alerta","La activacion manual ha sido desactivada");
	}else{
		funcionInterna(rowIndex);
		//check.checked = true;
	}
	
}

Ext.onReady(function(){
	
	funcionInterna = function(rowIndex){
		var record = consultaData.getAt(rowIndex);
		record.data.VAL_NUEVO = '';
	}
//--------------------------------HANDLERS-----------------------------------
	
	var resetFromEPO = function(){
		Ext.getCmp('txtNafinE').reset();		
		Ext.getCmp('txt_cadenas_pymes').reset();		
		Ext.getCmp('msEposRelacionadas').reset();	
		Ext.getCmp('msEposRelacionadas').hide();	
	}
	
	var seleccionCombo = function(combo){
		var claveEpo = Ext.getCmp('cmbNombreEPO').getValue();	
		if(combo.id == 'cmbNombreEPO'){
			resetFromEPO();
		}
		
		if(combo.id == 'cmbProducto'){
			var nafin = Ext.getCmp('txtNafinE');
			relacionEPOS(nafin);
			var cm = gridGeneral.getColumnModel();
			if (combo.getValue() == 1){
				cm.setHidden(cm.findColumnIndex('RESPONSABLE'), true);
				cm.setHidden(cm.findColumnIndex('REALIZO'), true);
				gridGeneral.getColumnModel().setHidden(cm.findColumnIndex('BLOQUEADA'), false);	
			}
			if (combo.getValue() == 4){
				cm.setHidden(cm.findColumnIndex('RESPONSABLE'), false);
				cm.setHidden(cm.findColumnIndex('REALIZO'), false);
				gridGeneral.getColumnModel().setHidden(cm.findColumnIndex('BLOQUEADA'), true);	
			}
		}
	}
	
	var relacionEPOS = function(field){
		Ext.getCmp('txt_cadenas_pymes').reset();
		if(!Ext.isEmpty(field.getValue())){
			Ext.Ajax.request({
				url: '15admnafparamchklist01ext.data.jsp',	
				params: {
					informacion: "obtenerNombrePyme",
					txtNafinE: field.getValue()
				},
				callback: procesaConsultaNombrePyme
			});
		}
	}
	
	var procesaConsultaNombrePyme = function(opts, success, response){		
		var cadenaPyme = Ext.getCmp('txt_cadenas_pymes');
		var clavePyme = Ext.getCmp('txtAuxNafinE');
		var claveEpo = Ext.getCmp('cmbNombreEPO').getValue();
		var claveProducto = Ext.getCmp('cmbProducto').getValue();
		var msEposRel = Ext.getCmp('msEposRelacionadas');
		
		if (success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			if (resp.muestraMensaje != undefined && resp.muestraMensaje){
				Ext.Msg.alert("Mensaje de p�gina web.",	resp.textoMensaje);
				cadenaPyme.setValue('');
				clavePyme.setValue('');
				msEposRel.hide();
				msEposRel.reset();			
			}
			else{
				cadenaPyme.setValue(resp.descripcion);
				clavePyme.setValue(resp.clave);
				catalogoEpoRelacionadas.load({
					params:{
						informacion: "catalogoEpoRelacionadas",
						txtAuxNafinE: resp.clave,
						cmbNombreEPO : claveEpo,
						cmbProducto: claveProducto
					}
				});
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);	
	
	var procesarEpoRelacionadas = function(store, arrRegistros, opts){
		var msEposRel = Ext.getCmp('msEposRelacionadas');
		var jsonData = store.reader.jsonData;
		var newRecord1 = new recordType({
			clave:'0',
			descripcion:'Seleccionar...'
		});
		store.insert(0,newRecord1);
		store.commitChanges();
		msEposRel.setValue('0');
			
		if(jsonData.success){
			if(jsonData.total == 'excede'){
				Ext.Msg.alert('Alerta','La PYME no esta relacionada con la EPO seleccionada');
				Ext.getCmp('txtNafinE').reset();		
				Ext.getCmp('txt_cadenas_pymes').reset();
				msEposRel.reset()
				msEposRel.hide();
			}else{
				msEposRel.show();
			}
		}
	}
	
	var procesarNafinE = function(store, arrRegistros, opts){
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
		}
	}
	
	var realizarConsultaGeneral = function(){		
		var claveEpo = Ext.getCmp('cmbNombreEPO');
		var claveNafin = Ext.getCmp('txtNafinE');
		var clavePyme = Ext.getCmp('txtAuxNafinE');		
		var msEposRel = Ext.getCmp('msEposRelacionadas');
		var claveProducto = Ext.getCmp('cmbProducto');
		var fechaInicial = Ext.getCmp('fechaInicio');
		var fechaFinal = Ext.getCmp('fechaFin');
		var aux = 0;
		var bandera = 0;
		var banderaAux = 0;
		var menorArray = new Array();

		var store = claveEpo.getStore();
		var jsonData = store.data.items;
		
		if(claveEpo.getValue() == ''){
			if(claveNafin.getValue() == ''){
				claveEpo.markInvalid('Debe seleccionar una Cadena');
				claveEpo.focus();
			}else if(claveProducto.getValue() == ''){
				claveProducto.markInvalid('Debe seleccionar un producto');
				claveProducto.focus();
			}else{
				bandera = 1;
			}
		}else	if(claveEpo.getValue() != ''){
		//	claveEpo.reset();
			if(claveNafin.getValue() == ''){
				claveNafin.markInvalid('Debe seleccionar una PYME');
				return;
			}
			
		//	claveNafin.focus();
			else if(claveProducto.getValue() != ''){
				bandera = 1;
			}else{
				claveProducto.markInvalid('Debe seleccionar un producto');
				claveProducto.focus();
			}
		}
		
		var n = Ext.getCmp('msEposRelacionadas').getValue().split(',');
		for(i =0; i< n.length; i++){		
			menorArray[i] = parseInt(n[i]);
			if(claveEpo.getValue() == n[i]){
				aux = 1;
			}	
		}
		
		if(aux == 0){
			var menor = menorArray[0];
			for(i = 0; i< menorArray.length; i++){
				if(menorArray[i] <= menor){
					menor = menorArray[i];
				}
			}

			if(menor != 0){
				claveEpo.setValue('');
				Ext.each(jsonData, function(item,inx,arrItem){
					if(item.data.clave == menor){
						claveEpo.setValue(menor);
					}		
				});
			}
		}
		if(Ext.getCmp('msEposRelacionadas').getValue() == '0')
			claveEpo.setValue('');
		
		if(Ext.isEmpty(fechaInicial.getValue()) && Ext.isEmpty(fechaFinal.getValue())){
			banderaAux = 1;
		}else if(!Ext.isEmpty(fechaInicial.getValue()) || !Ext.isEmpty(fechaFinal.getValue())){			
			if(Ext.isEmpty(fechaInicial.getValue())){
				fechaInicial.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
				fechaInicial.focus();
				return;
			}else	if(fechaInicial.isValid(false)){
				if(!isdate(fechaInicial.getRawValue())){
					fechaInicial.markInvalid("La fecha es incorrecta. Verifique que el formato sea dd/mm/aaaa");
					return;
				}
			}
			
			if(Ext.isEmpty(fechaFinal.getValue())){
				fechaFinal.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
				fechaFinal.focus();
				return;
			}else	if(fechaFinal.isValid(false)){
				if(!isdate(fechaFinal.getRawValue())){
					fechaFinal.markInvalid("La fecha es incorrecta. Verifique que el formato sea dd/mm/aaaa");
					return
				}
			}
			
			
			if(!Ext.isEmpty(fechaInicial.getValue()) && !Ext.isEmpty(fechaFinal.getValue())){
				if(fechaInicial.getValue() > fechaFinal.getValue()){
					fechaInicial.markInvalid('Las fechas de ejecuci�n deben ir de menor a mayor');
				return;
				}else{
					banderaAux = 1;
				}
			}
		}
		var gridConsulta = Ext.getCmp('gridGeneral');
								
		if(bandera == 1 && banderaAux == 1){
			gridConsulta.show();
			consultaData.load({
				params:{
					informacion: "consultaGeneral",				
					cmbNombreEPO : claveEpo.getValue(),
					txtNafinE: claveNafin.getValue(),
					txtAuxNafinE: clavePyme.getValue(),
					epoVec: msEposRel.getValue(),
					cmbProducto: claveProducto.getValue(),
					fechaInicio: fechaInicial.getRawValue(),
					fechaFin: fechaFinal.getRawValue()
				}
			});
		}
	}
	
	var generarArchivo = function(){
		var claveEpo = Ext.getCmp('cmbNombreEPO');
		var claveNafin = Ext.getCmp('txtNafinE');
		var clavePyme = Ext.getCmp('txtAuxNafinE');		
		var msEposRel = Ext.getCmp('msEposRelacionadas');
		var claveProducto = Ext.getCmp('cmbProducto');
		var fechaInicial = Ext.getCmp('fechaInicio');
		var fechaFinal = Ext.getCmp('fechaFin');
		
		Ext.Ajax.request({
			url: '15admnafparamchklist01ext.data.jsp',	
			params: {
				informacion: "ArchivoCSV",
				cmbNombreEPO : claveEpo.getValue(),
				txtNafinE: claveNafin.getValue(),
				txtAuxNafinE: clavePyme.getValue(),
				epoVec: msEposRel.getValue(),
				cmbProducto: claveProducto.getValue(),
				fechaInicio: fechaInicial.getRawValue(),
				fechaFin: fechaFinal.getRawValue()
			},
			callback: procesarDescargaArchivo
		});
	}
	
	var actualizarLista = function(){
		var epo1 = [];
		var epo5 = [];
		var epo8 = [];
		var valEPO1 = [];
		var valEPO5 = [];
		var valEPO8 = [];
		var valNuevoEPO1 = [];
		var valNuevoEPO5 = [];
		var valNuevoEPO8 = [];
		var claveEpo = Ext.getCmp('cmbNombreEPO');
		var claveNafin = Ext.getCmp('txtNafinE');
		var clavePyme = Ext.getCmp('txtAuxNafinE');		
		var msEposRel = Ext.getCmp('msEposRelacionadas');
		var claveProducto = Ext.getCmp('cmbProducto');
		var fechaInicial = Ext.getCmp('fechaInicio');
		var fechaFinal = Ext.getCmp('fechaFin');
		
		var gridConsulta = Ext.getCmp('gridGeneral');	
		var store = gridConsulta.getStore();
		var jsonData = store.data.items;
		
		Ext.each(jsonData, function(item,inx,arrItem){
			console.log('TIPO ' + item.data.TIPO + 
				' EPO ' + item.data.EPO + ' VAL_ACTUAL '+ item.data.VAL_ACTUAL +
				' VAL_NUEVO ' + item.data.VAL_NUEVO);
			if(item.data.TIPO == '1'){
				epo1.push(item.data.EPO);
				valEPO1.push(item.data.VAL_ACTUAL);
				valNuevoEPO1.push(item.data.VAL_NUEVO);
			}
			if(item.data.TIPO == '5'){
				epo5.push(item.data.EPO);
				valEPO5.push(item.data.VAL_ACTUAL);
				valNuevoEPO5.push(item.data.VAL_NUEVO);
			}
			if(item.data.TIPO == '8'){
				epo8.push(item.data.EPO);
				valEPO8.push(item.data.VAL_ACTUAL);
				valNuevoEPO8.push(item.data.VAL_NUEVO);
			}
		});
		Ext.Ajax.request({
			url: '15admnafparamchklist01ext.data.jsp',	
			params: {
				informacion: "actualizarLista",
				cmbNombreEPO: claveEpo.getValue(),
				txtNafinE: claveNafin.getValue(),
				txtAuxNafinE: clavePyme.getValue(),
				epoVec: msEposRel.getValue(),
				cmbProducto: claveProducto.getValue(),
				fechaInicio: fechaInicial.getRawValue(),
				fechaFin: fechaFinal.getRawValue(),
				epo1: epo1,
				valEpo1: valEPO1,
				valNuevaEpo1: valNuevoEPO1,
				epo5: epo5,
				valEpo5: valEPO5,
				valNuevaEpo5: valNuevoEPO5,
				epo8: epo8,
				valEpo8: valEPO8,
				valNuevaEpo8: valNuevoEPO8
			},
			callback: procesarActualizarLista
		});
	}
	
	var procesarActualizarLista = function(opts, success, response){
		var resp = 	Ext.util.JSON.decode(response.responseText);
		if (resp.success){
			Ext.Msg.alert("Mensaje de p�gina web.",	resp.textoMensaje, function(){
				realizarConsultaGeneral();
			});
			//	
		}
	}
	
	var procesarDescargaArchivo = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			Ext.getCmp('btnArchivo').enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts){
		var pyme = Ext.getCmp('txt_cadenas_pymes').getValue();
		var producto = Ext.getCmp('cmbProducto').getValue();
		
		gridGeneral.setTitle("PYME: " + pyme);
		var el = gridGeneral.getGridEl();
		if(consultaData.getTotalCount() > 10){
			el.unmask();
			
			var cm = gridGeneral.getColumnModel();
			if(producto==1)  {
				gridGeneral.getColumnModel().setHidden(cm.findColumnIndex('BLOQUEADA'), false);	
			}else  {
				gridGeneral.getColumnModel().setHidden(cm.findColumnIndex('BLOQUEADA'), true);	
			}
				
			Ext.getCmp('btnArchivo').enable();
			Ext.getCmp('btnActualizar').enable();
		}else{
			Ext.getCmp('btnArchivo').disable();
			Ext.getCmp('btnActualizar').disable();
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
		gridGeneral.show();
	}
//---------------------------------STORES------------------------------------
	
	var consultaData = new Ext.data.GroupingStore({
		root: 'registros',
		url: '15admnafparamchklist01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGeneral'
		},
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
				{name: 'EPO_REL'},
				{name: 'COLDINAMICA'},
				{name: 'NOM_REQUISITO'},
				{name: 'CAMPOFECHA'},
				{name: 'RESPONSABLE'},
				{name: 'REALIZO'},
				{name: 'EPO'},
				{name: 'VAL_ACTUAL'},
				{name: 'VAL_NUEVO'},			
				{name: 'TIPO'},
				{name: 'BLOQUEADA'}				
			]
		}),
		groupField:'EPO_REL',
		sortInfo:{field: 'EPO_REL',direction:"ASC"},
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy,type,action,optionRequest,response,args)	{
					NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
				//	procesarConsultaData(null,null,null);
				}
			}
		}
  });
	
	var catalogoNombreEPO = new Ext.data.JsonStore({
		id: 'catalogoNombreEPO',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admnafparamchklist01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoNombreEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEpoRelacionadas = new Ext.data.JsonStore({
		id: 'catalogoEpoRelacionadas',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admnafparamchklist01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpoRelacionadas'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarEpoRelacionadas,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var catalogoProducto = new Ext.data.JsonStore({
		id: 'catalogoProducto',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admnafparamchklist01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoProducto'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});	
	
	var catalogoNafinE = new Ext.data.JsonStore({
		id: 'catalogoNafinE',
		root : 'registros',
		fields : ['CLAVE', 'DESCRIPCION', 'loadMsg'],
		url : '15admnafparamchklist01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoNafinE'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarNafinE,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//------------------------------COMPONENTES----------------------------------
	var elementosForma = [
		{
			xtype: 'combo',
			name: '_cmbNombreEPO',
			id: 'cmbNombreEPO',
			fieldLabel: 'Nombre EPO',
			mode: 'local',
			width: '100%',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cmb_nombre_epo',
			emptyText: 'Seleccionar...',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoNombreEPO,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select: seleccionCombo
			}
		},
		{
			xtype: 'compositefield',
			id:'nafinElectronico',
			combineErrors: false,
			width: '100%',
			msgTarget: 'side',
			fieldLabel: 'No. Nafin Electr�nico',
			items: [
				{
					xtype:		'textfield',
					name:			'_txtNafinE',
					id:			'txtNafinE',
					hiddenName:		'txt_nafin_e',
					maskRe:		/[0-9]/,
					allowBlank:	true,
					maxLength:	15,
					margins: '0 20 0 0',
					listeners:{
						'change': relacionEPOS
					}
				},
				{
					xtype:		'textfield',
					name:			'_txtAuxNafinE',
					id:			'txtAuxNafinE',
					hiddenName:		'txt_aux_nafin_e',
					maskRe:		/[0-9]/,
					allowBlank:	true,
					maxLength:	5,
					margins: '0 20 0 0',
					hidden: true
				},

				{
					xtype:			'textfield',
					name:				'_txtCadenasPymes',
					id:				'txt_cadenas_pymes',
					width:			350,
					disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
					disabled:		true,
					allowBlank:		true,
					maxLength:		100
				}
			]
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'busqava',
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					id:'disEspacio',
					text:''
				},
				{
					xtype: 'button',
					text: 'B�squeda Avanzada...',
					name: '_btnBusquedaAvanzada',
					id: 'btnBusquedaAvanzada',
					hiddenName: 'btn_busqueda_avanzada',
					iconCls: 'icoBuscar',
					handler: function(boton, evento){
						var nomEpo = Ext.getCmp('cmbNombreEPO');
						if(nomEpo.getValue() == '0' || nomEpo.getValue() == ''){
							nomEpo.markInvalid('Se requiere seleccionar una EPO');
						}else{
							Ext.getCmp('winBusqueda').show();
						}
					}
				}
			]
		},
		{	
			xtype: 'multiselect',
			fieldLabel: 'Epos Relacionadas',
			name: '_msEposRelacionadas',
			id: 'msEposRelacionadas',
			hiddenName: 'ms_epos_relacionadas',
			blankText : 'Seleccionar...',
			width: '100%',
			height: 150,
			hidden: true,
			autoLoad: false,
			allowBlank:false,
			store: catalogoEpoRelacionadas,
			displayField: 'descripcion',
			valueField: 'clave'	 
		},
		{
			xtype: 'combo',
			name: '_cmbProducto',
			id: 'cmbProducto',
			fieldLabel: 'Producto',
			width: '40%',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cmb_producto',
			emptyText: 'Seleccionar...',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoProducto,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select: seleccionCombo
			}
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '100%',
			items:[			
				{
					xtype: 'datefield',
					fieldLabel: 'Desde',
					name: '_fechaInicio',
					id: 'fechaInicio',
					hiddenName: 'fecha_inicio',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'Hasta:',
					width: 50
				},
				{
					xtype: 'datefield',
					name: '_fechaFin',
					id: 'fechaFin',
					hiddenName: 'fecha_fin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		}
		
	];
	
	var elementosBusquedaAvanzada = [		
		{
			xtype:'form',
			id:'fpWinBusca',
			frame: true,
			labelAlign : 'right',
			border: false,
			style: 'margin: 0 auto',
			bodyStyle:'padding:10px',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			labelWidth: 130,
			items:[
				{
					xtype:'displayfield',
					frame:true,
					border: false,
					value:'Utilice el * para b�squeda gen�rica'
				},{
					xtype:		'textfield',
					fieldLabel: 'EPO',
					name:			'_txtEPO',
					id:			'txtEPO',
					hiddenName:		'txt_epo',
					hidden: true,
					allowBlank:	true
				},{
					xtype:		'textfield',
					fieldLabel: 'Nombre',
					name:			'_txtNombre',
					id:			'txtNombre',
					hiddenName:		'txt_nombre',
					allowBlank:	true
				},
				{
					xtype:		'textfield',
					fieldLabel: 'RFC',
					name:			'_txtRFC',
					id:			'txtRFC',
					hiddenName:		'txt_RFC',
					maxLength:	20,
					allowBlank:	true
				},
				{
					xtype:		'textfield',
					fieldLabel: 'N�mero de Proveedor',
					name:			'_txtNumProveedor',
					id:			'txtNumProveedor',
					hiddenName:		'txt_num_proveedor',
					maxLength:	20,
					allowBlank:	true
				}
			],
			buttonAlign:'center',
			buttons:[
				{
					text:'Buscar',
					id: 'idBtnBuscar',
					iconCls:'icoBuscar',
					handler: function(btn){
						var epo = Ext.getCmp("txtEPO");
						epo.setValue(Ext.getCmp("cmbNombreEPO").getValue());
						var nombre = Ext.getCmp("txtNombre");
						var rfc = Ext.getCmp("txtRFC");
						var proveedor = Ext.getCmp("txtNumProveedor");
						catalogoNafinE.load({ 
							params: {
								cmbNombreEPO: epo.getValue(),
								txtNombre: nombre.getValue(),
								txtRFC: rfc.getValue(),
								txtNumProveedor: proveedor.getValue()
							}
						});
					}
				},{
					text:'Cancelar',
					iconCls: 'icoCancelar',
					handler: function() {
						Ext.getCmp('fpWinBusca').getForm().reset();
						Ext.getCmp('fpWinBuscaB').getForm().reset();
						Ext.getCmp('winBusqueda').hide();
					}
				}
			]
		},{
			xtype:'form',
			id:'fpWinBuscaB',
			frame: true,
			border: false,
			labelAlign : 'right',
			style: 'margin: 0 auto',
			bodyStyle:'padding:10px',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			labelWidth: 130,
			items:[
				{
					xtype: 'combo',
					id:	'cmbNumNE',
					name: 'ic_pyme',
					hiddenName : 'ic_pyme',
					fieldLabel: 'Nombre',
					emptyText: 'Seleccione. . .',
					displayField: 'DESCRIPCION',
					valueField: 'CLAVE',
					triggerAction : 'all',
					forceSelection:true,
					allowBlank: false,
					typeAhead: true,
					mode: 'local',
					minChars : 1,
					store: catalogoNafinE
			//		tpl : NE.util.templateMensajeCargaCombo
				}
			],
			buttonAlign:'center',
			buttons:[
				{
					text:'Aceptar',
					iconCls:'aceptar',
					formBind:true,
					handler: function() {
						var claveProd = Ext.getCmp('cmbNumNE');
						var txtNafinE = Ext.getCmp('txtNafinE');
						if (!Ext.isEmpty(claveProd.getValue())){
							txtNafinE.setValue(claveProd.getValue());
							Ext.getCmp('fpWinBusca').getForm().reset();
							Ext.getCmp('fpWinBuscaB').getForm().reset();
							Ext.getCmp('winBusqueda').hide();
							txtNafinE.focus();						
						}
						relacionEPOS(txtNafinE);
					}
				},{
					text:'Cancelar',
					iconCls: 'icoCancelar',
					handler: function() {
						Ext.getCmp('fpWinBusca').getForm().reset();
						Ext.getCmp('fpWinBuscaB').getForm().reset();
						Ext.getCmp('winBusqueda').hide();	
					}
				}
			]
		}
	];
	
	var ventanaBusqueda = new Ext.Window({
		id: 'winBusqueda',
		width: 550,
		height: 340,
		modal: true,
		labelAlign : 'right',
		closeAction: 'hide',
		title: '.:: Nafinet ::.',
		items:[
			elementosBusquedaAvanzada
		]
	});
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 140,
		labelAlign : 'right',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: false,
				handler: function(boton, evento){
					realizarConsultaGeneral();
				}
			}
		]
	});
	
	var gridGeneral = new Ext.grid.GridPanel
  ({
		store: consultaData,
		style: 'margin: 0 auto;',
		hidden: true,
      id: 'gridGeneral',
		columns: [		
			{
				header: 'Cadena Productiva',
				sortable: true,
				resizable: true,
				align: 'left',
				hidden: true,
				dataIndex: 'EPO_REL'
			},
			{
				header: '',
				sortable: true,
				width: 10,
				resizable: true,
				align: 'CENTER',
				hidden: false,
				dataIndex: 'COLDINAMICA',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					var auxCheck = '';
					if(record.data['TIPO'] == '1'){
						if(record.data['VAL_ACTUAL'] == 'S'){
							auxCheck = 'checked'
						}else{
							auxCheck = '';
						}
					}else if(record.data['TIPO'] == '5'){
						if(record.data['VAL_ACTUAL'] == 'H' || record.data['VAL_ACTUAL'] == 'R'){
							auxCheck = 'checked'
						}else{
							auxCheck = '';
						}
					}else if(record.data['TIPO'] == '7'){
						if(record.data['VAL_ACTUAL'] == 'S'){
							auxCheck = 'checked'
						}else{
							auxCheck = '';
						}
					}else if(record.data['TIPO'] == '8'){
						if(record.data['VAL_ACTUAL'] == 'H'){
							auxCheck = 'checked'
						}else{
							auxCheck = '';
						}
					}
					
					if(value == 'CHECK' || value == 'CHECKED' || value == 'CHECKED_DISABLED'){
						return '<input  name="epo'+record.data['TIPO'] + record.data['EPO'] + '" value="S" type="checkbox" '+auxCheck+' onclick="deshabilitaActivar(this,'+rowIndex +','+colIndex+');" />';
					}else if(value == 'PALOMA'){
						return '<div class="icoPalomaRoja"></div>'
					}
				}
			},
			{
				header: 'Requisito',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left',
				hidden: false,
				dataIndex: 'NOM_REQUISITO'
			},
			{
				header: 'Fecha',
				sortable: true,
				width: 30,
				resizable: true,
				align: 'center',
				hidden: false,
				dataIndex: 'CAMPOFECHA'
			},
			{
				header: 'Responsable',
				sortable: true,
				width: 30,
				resizable: true,
				align: 'center',
				hidden: false,
				dataIndex: 'RESPONSABLE'
			},
			{
				header: 'Realiz�',
				sortable: true,
				width: 30,
				resizable: true,
				align: 'left',
				hidden: false,
				dataIndex: 'REALIZO'
			},
			{
				header: 'Bloqueda',
				sortable: true,
				width: 30,
				resizable: true,
				align: 'center',
				hidden: true,
				dataIndex: 'BLOQUEADA',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					if(record.data['TIPO'] == '7' ) {
						return record.data['BLOQUEADA'];
					}					
				}
				
			}
		],	
		view: new Ext.grid.GroupingView({ forceFit: true, groupTextTpl: '{text}'}),
		stripeRows: true,
	   loadMask: true,
		frame: true,
		autoWidth: true,
		height: 400,
		title: 'PYME: ',
		bbar: {
			items: [
				{
					xtype: 'label',
					text: 'El Requisito 4 no depende de la Epo y solo se modificar� en la primer Epo seleccionada',
					id: 'lblGrid'
				},
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnArchivo',
					iconCls: 'icoXls',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						generarArchivo();
					}
				},
				{
					xtype: 'button',
					text: 'Actualizar',
					id: 'btnActualizar',
					iconCls: 'icoActualizar',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						actualizarLista();
					}
				},
				{
					xtype: 'button',
					text: 'Deshacer',
					id: 'btnDeshacer',
					iconCls: 'icoCancelar',
					hidden: false,
					handler: function(boton, evento){
						location.href = "15admnafparamchklist01ext.jsp";
					}
				}
			]
		}
	});


//-------------------------COMPONENTE PRINCIPAL------------------------------

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 950,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			gridGeneral
		]
	});
	
	catalogoProducto.load();
	catalogoNombreEPO.load();

});