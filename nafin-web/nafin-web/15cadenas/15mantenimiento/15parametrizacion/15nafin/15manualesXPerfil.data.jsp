<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		java.io.File,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../../../../15cadenas/015secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String perfil = (request.getParameter("perfil")!=null)?request.getParameter("perfil"):"";
String nomManual = (request.getParameter("nomManual")!=null)?request.getParameter("nomManual"):"";
String descripcion = (request.getParameter("descripcion")!=null)?request.getParameter("descripcion"):"";
String infoRegresar =""; 	

if (informacion.equals("catalogoEPO")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_epo");
	cat.setCampoClave("IC_EPO");	
	cat.setCampoDescripcion("CG_RAZON_SOCIAL");
	cat.setOrden("CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("catalogoPerfil")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("SEG_PERFIL");
	cat.setCampoClave("cc_perfil");	
	cat.setCampoDescripcion("cc_perfil");
	cat.setOrden("cc_perfil");
	infoRegresar = cat.getJSONElementos();
	
	/*
	select  cc_perfil  from SEG_PERFIL
	where cs_bloqueado = 'N'
	and sc_tipo_usuario  = 3 --solo pyme 
	*/
	
}else if (informacion.equals("Guardar")) {

	netropology.utilerias.ManualesPerfilEpo imagenDocumento = new netropology.utilerias.ManualesPerfilEpo();
	
	List datos  = new ArrayList();
	datos.add(perfil);
	datos.add(nomManual);
	datos.add(descripcion);
	datos.add(clave_epo);
	
	String ic_manual = imagenDocumento.guardarManual(datos);
		
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("ic_manual",ic_manual);
	infoRegresar = jsonObj.toString();
	

}else if (informacion.equals("Eliminar")) {

	String ic_manual = (request.getParameter("ic_manual")!=null)?request.getParameter("ic_manual"):"";

	netropology.utilerias.ManualesPerfilEpo imagenDocumento = new netropology.utilerias.ManualesPerfilEpo();
	imagenDocumento.eliminarManual(ic_manual);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("Eliminado","S");
	infoRegresar = jsonObj.toString();
			

}else if (informacion.equals("Consultar") ||  informacion.equals("ArchivoPDF")) {
	
	com.netro.cadenas.ConsManualesXPerfil paginador = new com.netro.cadenas.ConsManualesXPerfil();
	paginador.setPerfil(perfil);
	paginador.setClave_epo(clave_epo);
	paginador.setManual(nomManual);
	paginador.setDescripcion(descripcion);
		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
	
	if (informacion.equals("Consultar")){
	
		try {
			Registros reg	=	queryHelper.doSearch();
			infoRegresar	=		"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
			}
			
			
	}else if (informacion.equals("ArchivoPDF")){
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
			}catch(Throwable e){
			throw new AppException("Error al generar el archivo PDF",e);
		}
	}
	
	
}else if (informacion.equals("VerManual")) {

	String ic_manual = (request.getParameter("ic_manual")!=null)?request.getParameter("ic_manual"):"";
	JSONObject jsonObj = new JSONObject();			
		
	File file = null;		
	netropology.utilerias.ManualesPerfilEpo imagenDocumento = new netropology.utilerias.ManualesPerfilEpo();
	file = imagenDocumento.descargaManual(ic_manual, strDirectorioTemp);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+file.getName());
	infoRegresar = jsonObj.toString();

}
			


%>

<%=infoRegresar%>