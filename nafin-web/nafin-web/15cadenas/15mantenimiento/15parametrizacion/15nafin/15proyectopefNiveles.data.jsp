<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.cadenas.*, 
		com.netro.model.catalogos.*,		
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String ic_epo  = (request.getParameter("ic_epo") != null)?request.getParameter("ic_epo"):"";
String ic_tipo_epo  = (request.getParameter("ic_tipo_epo") != null)?request.getParameter("ic_tipo_epo"):"";
String ic_subtipo_epo  = (request.getParameter("ic_subtipo_epo") != null)?request.getParameter("ic_subtipo_epo"):"";
String ic_sector_epo  = (request.getParameter("ic_sector_epo") != null)?request.getParameter("ic_sector_epo"):"";
String ic_area_promocion  = (request.getParameter("ic_area_promocion") != null)?request.getParameter("ic_area_promocion"):"";
String ic_region  = (request.getParameter("ic_region") != null)?request.getParameter("ic_region"):"";
String ic_subdireccion  = (request.getParameter("ic_subdireccion") != null)?request.getParameter("ic_subdireccion"):"";
String ic_lider_promotor  = (request.getParameter("ic_lider_promotor") != null)?request.getParameter("ic_lider_promotor"):"";

String infoRegresar ="";
JSONObject jsonObj = new JSONObject();

ConsProyectoPEF paginador = new ConsProyectoPEF();


if (informacion.equals("catalogoEPO")){

	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setOrden("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();

} else  if (informacion.equals("catTipoEpo")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_tipo_epo");
	catalogo.setCampoDescripcion("cg_descripcion");
	catalogo.setTabla("comcat_tipo_epo");		
	catalogo.setOrden("ic_tipo_epo");	
	infoRegresar = catalogo.getJSONElementos();	

} else  if (informacion.equals("catSubTipoEpo")){  
  
	CatalogoSubtipoEPO catalogo = new CatalogoSubtipoEPO();
	catalogo.setCampoClave("s.ic_subtipo_epo");  
	catalogo.setCampoDescripcion("s.cg_descripcion");
	catalogo.setIc_tipo_epo(ic_tipo_epo);
	catalogo.setOrden("s.cg_descripcion");   
	infoRegresar = catalogo.getJSONElementos();

} else  if (informacion.equals("catSubTipoEpoGrid")){  
  
	CatalogoSubtipoEPO catalogo = new CatalogoSubtipoEPO();
	catalogo.setCampoClave("s.ic_subtipo_epo");  
	catalogo.setCampoDescripcion("s.cg_descripcion");
	catalogo.setIc_tipo_epo(ic_tipo_epo);
	catalogo.setOrden("s.cg_descripcion");   
	infoRegresar = catalogo.getJSONElementos();
	
	
} else  if (informacion.equals("catSectorEpo")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_sector_epo");
	catalogo.setCampoDescripcion("cg_descripcion");
	catalogo.setTabla("comcat_sector_epo");		
	catalogo.setOrden("ic_sector_epo");	
	infoRegresar = catalogo.getJSONElementos();	
	
} else  if (informacion.equals("catAreaPromocion")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_AREA_PROMOCION");
	catalogo.setCampoDescripcion("CG_DESCRIPCION");
	catalogo.setTabla("COMCAT_AREA_PROMOCION");		
	catalogo.setOrden("CG_DESCRIPCION");	
	infoRegresar = catalogo.getJSONElementos();	
	

} else  if (informacion.equals("catRegion")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_REGION");
	catalogo.setCampoDescripcion("CG_DESCRIPCION");
	catalogo.setTabla("COMCAT_REGION");		
	catalogo.setOrden("CG_DESCRIPCION");	
	infoRegresar = catalogo.getJSONElementos();	

} else  if (informacion.equals("catSubDireccion")){
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_subdireccion");
	catalogo.setCampoDescripcion("CG_DESCRIPCION");
	catalogo.setTabla("COMCAT_subdireccion");		
	catalogo.setOrden("CG_DESCRIPCION");	
	infoRegresar = catalogo.getJSONElementos();	


} else  if (informacion.equals("catLiderPromotor")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_lider_promotor");
	catalogo.setCampoDescripcion("CG_DESCRIPCION");
	catalogo.setTabla("COMCAT_lider_promotor");		
	catalogo.setOrden("CG_DESCRIPCION");	
	infoRegresar = catalogo.getJSONElementos();	

} else  if (informacion.equals("Consultar")){

	paginador.setIc_epo(ic_epo);
	paginador.setIc_area_promocion(ic_area_promocion);
	paginador.setIc_subdireccion(ic_subdireccion);
	paginador.setIc_lider_promotor(ic_lider_promotor);
	paginador.setIc_region(ic_region);
	paginador.setIc_tipo_epo(ic_tipo_epo);
	paginador.setIc_subtipo_epo(ic_subtipo_epo);
	paginador.setIc_sector_epo(ic_sector_epo);	
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);	

	try {
		Registros reg	=	queryHelper.doSearch();
		String consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj.put("success", new Boolean(true));
		jsonObj = JSONObject.fromObject(consulta);		 			
			
	}	catch(Exception e) {
		throw new AppException("Error en la consulta", e);
	}
	infoRegresar = jsonObj.toString();	
	
} else  if (informacion.equals("Guardar")){
	
	String totalepos = (request.getParameter("totalepos") == null) ? "" : request.getParameter("totalepos");
	String ic_epoG[] = request.getParameterValues("ic_epoG");
	String ic_area_promocionG[] = request.getParameterValues("ic_area_promocionG");
	String ic_subdireccionG[] = request.getParameterValues("ic_subdireccionG");
	String ic_lider_promotorG[] = request.getParameterValues("ic_lider_promotorG");
	String ic_regionG[] = request.getParameterValues("ic_regionG");
	String ic_tipo_epoG[] = request.getParameterValues("ic_tipo_epoG");
	String ic_subtipo_epoG[] = request.getParameterValues("ic_subtipo_epoG");
	String ic_sector_epoG[] = request.getParameterValues("ic_sector_epoG");

	List lcols = null;
	List datos = new ArrayList();
	String mensaje ="";		
		
	for(int a=0;a<Integer.parseInt(totalepos);a++){
		lcols = new ArrayList();
		
		lcols.add(ic_epoG[a]);
		lcols.add(ic_area_promocionG[a]);
		lcols.add(ic_subdireccionG[a]);
		lcols.add(ic_lider_promotorG[a]);
		lcols.add(ic_regionG[a]);
		lcols.add(ic_tipo_epoG[a]);
		lcols.add(ic_subtipo_epoG[a]);
		lcols.add(ic_sector_epoG[a]);
		datos.add(lcols);
	}
	
	try{
		paginador.ovactualizarEpos(datos);
		mensaje = "EXITO";
	}catch(Exception e){
		mensaje = "ERROR";					
	}

	jsonObj.put("success",  new Boolean(true)); 
	jsonObj.put("mensaje", mensaje);	
	infoRegresar = jsonObj.toString();
}	

%>

<%=infoRegresar%>

