<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		java.lang.String,
		com.netro.cadenas.*,
		com.netro.exception.*,
		com.netro.procesos.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.descuento.*,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoCargaTasas,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoPlazosEPO,
		com.netro.model.catalogos.CatalogoEPOBanco,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

AutorizacionTasas autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB",AutorizacionTasas.class);

if(informacion.equals("catalogoProductos") ) {
	List l = new ArrayList();
	l.add("1");
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COMCAT_PRODUCTO_NAFIN");
	cat.setCampoClave("ic_producto_nafin");
	cat.setCampoDescripcion("ic_nombre");
	cat.setOrden("ic_nombre");
	cat.setValoresCondicionIn(l);
	
	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoBancoFondeo") ) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_banco_fondeo");
	cat.setCampoClave("ic_banco_fondeo");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setOrden("ic_banco_fondeo");

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoEPO") ) {
	String icBanco		=	(request.getParameter("icBanco") == null)?"1":request.getParameter("icBanco");
	CatalogoEPOBanco cat = new CatalogoEPOBanco();
	cat.setTabla("COMCAT_EPO");
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setCveBanco(icBanco);
	cat.setOrden("2");

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoMoneda") ) {
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre"); 
	cat.setOrden("CLAVE");

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoTasas") ) {
	String cveMoneda		=	(request.getParameter("cveMoneda") == null)?"":request.getParameter("cveMoneda");
	CatalogoCargaTasas cat= new CatalogoCargaTasas();
   cat.setClave("ic_tasa");
   cat.setDescripcion("cd_nombre");
   cat.setCveProd("1");
   cat.setCveMoneda(cveMoneda);

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoTasasB") ) {
	String moneda		=	(request.getParameter("cboMoneda") == null)?"":request.getParameter("cboMoneda");
	CatalogoCargaTasas cat= new CatalogoCargaTasas();
   cat.setClave("ic_tasa");
   cat.setDescripcion("cd_nombre");
   cat.setCveProd("1");
   cat.setCveMoneda(moneda);

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoPlazo") ) {
	String cveProd		=	(request.getParameter("cveProd") == null)?"":request.getParameter("cveProd");
	String cveEPO		=	(request.getParameter("cveEPO") == null)?"":request.getParameter("cveEPO");
	String cveMoneda		=	(request.getParameter("cveMoneda") == null)?"":request.getParameter("cveMoneda");
	
	CatalogoPlazosEPO cat = new CatalogoPlazosEPO();
	cat.setTabla("comcat_plazo");
   cat.setCampoClave("ic_plazo");
   cat.setCampoDescripcion("cg_descripcion");
	cat.setCveProducto("1");
	cat.setCveEPO(cveEPO);
	cat.setCveMoneda(cveMoneda);
	cat.setOrden("in_plazo_dias");
	cat.setBandera("0");

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoPlazosB") ) {
	String cveProd		=	(request.getParameter("cveProd") == null)?"":request.getParameter("cveProd");
	String cvePlazo		=	(request.getParameter("cvePlazo") == null)?"":request.getParameter("cvePlazo");
	
	CatalogoPlazosEPO cat = new CatalogoPlazosEPO();
	cat.setTabla("comcat_plazo");
   cat.setCampoClave("ic_plazo");
   cat.setCampoDescripcion("cg_descripcion");
	cat.setCveProducto(cveProd);
	cat.setCvePlazo(cvePlazo);
	cat.setOrden("in_plazo_dias");
	cat.setBandera("1");

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("Consultar") || informacion.equals("Grabar") || informacion.equals("ObtenerTasa")) {
	
	String operacion		=	(request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	String ic_producto 	= request.getParameter("cveProducto")==null?"":request.getParameter("cveProducto");
	String ic_epo		=	(request.getParameter("cveEPO") == null) ? "": request.getParameter("cveEPO");
	String ic_moneda		=	(request.getParameter("cveMoneda") == null) ? "": request.getParameter("cveMoneda");

		if(informacion.equals("Consultar")){
			Thread.sleep(110);	//Para esperar a que la peticion ajax retorne una respuesta
			int start = 0;
			int limit = 0;
		
			String ultimaClaveTasaProductoEpo = 
			autorizacionTasas.getUltimaClaveTasaProductoEpo(ic_epo, ic_producto, ic_moneda);
	
			TasasOp paginador = new TasasOp();
			paginador.setCve_producto(ic_producto);
			paginador.setCveEpo(ic_epo);
			paginador.setCveMoneda(ic_moneda);
	
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));

			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
	
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}

				infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		}
		else if(informacion.equals("Grabar") ){
			String ic_tasa		=	(request.getParameter("cveTasa") == null) ? "": request.getParameter("cveTasa");
			String ic_plazo		=	(request.getParameter("cvePlazo") == null) ? "": request.getParameter("cvePlazo");
			
			try{
				autorizacionTasas.guardarTasaProductoEpo(ic_producto, ic_epo, ic_tasa, ic_plazo);
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				infoRegresar = jsonObj.toString();
			}catch(Exception e) {
				throw new AppException("Error al guardar Tasas por Producto y por EPO", e);
			}
			
		}else if(informacion.equals("ObtenerTasa")){
	
			String ultimaClaveTasaProductoEpo = 
				autorizacionTasas.getUltimaClaveTasaProductoEpo(ic_epo, ic_producto, ic_moneda);
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("tasa",ultimaClaveTasaProductoEpo);
			infoRegresar = jsonObj.toString();

		}
}else if(informacion.equals("Eliminar")){

	String tasa		=	(request.getParameter("tasa") == null) ? "": request.getParameter("tasa");
	autorizacionTasas.eliminarTasaProductoEpo(tasa);
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("Actualizar")){
	
	String cveTasaProd	=	(request.getParameter("cveTasaProd") == null) ? "": request.getParameter("cveTasaProd");
	String Tasa		=	(request.getParameter("cveTasa") == null) ? "": request.getParameter("cveTasa");
	String Plazo		=	(request.getParameter("cvePlazo") == null) ? "": request.getParameter("cvePlazo");

	autorizacionTasas.actualizarTasaProductoEpo(cveTasaProd, Tasa, Plazo);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}
	
%>
<%=infoRegresar%>
