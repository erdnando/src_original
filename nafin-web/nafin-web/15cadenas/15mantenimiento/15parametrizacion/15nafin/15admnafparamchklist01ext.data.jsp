<%@ page contentType="application/json;charset=UTF-8" 
	import=" java.util.*, 
				com.netro.procesos.*, 
				netropology.utilerias.*,
				com.netro.cadenas.*,
				com.netro.seguridadbean.SeguException,
				com.netro.model.catalogos.*,   
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%  
String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String claveEpo = (request.getParameter("cmbNombreEPO") !=null)?request.getParameter("cmbNombreEPO"):"";
String claveNafinE = (request.getParameter("txtNafinE") !=null)?request.getParameter("txtNafinE"):"";
String clavePyme = (request.getParameter("txtAuxNafinE") !=null)?request.getParameter("txtAuxNafinE"):"";
String epo1[] = request.getParameterValues("epo1");
String epo5[] = request.getParameterValues("epo5");
String epo8[] = request.getParameterValues("epo8");
String epoVec[] = request.getParameterValues("epoVec");
String valEpo1[] = request.getParameterValues("valEpo1");
String valEpo5[] = request.getParameterValues("valEpo5");
String valEpo8[] = request.getParameterValues("valEpo8");
String valNuevoEpo1[] = request.getParameterValues("valNuevaEpo1");
String valNuevoEpo5[] = request.getParameterValues("valNuevaEpo5");
String valNuevoEpo8[] = request.getParameterValues("valNuevaEpo8");
String claveProducto = (request.getParameter("cmbProducto") !=null)?request.getParameter("cmbProducto"):"";
String fechaInicio = (request.getParameter("fechaInicio") !=null)?request.getParameter("fechaInicio"):"";
String fechaFin = (request.getParameter("fechaFin") !=null)?request.getParameter("fechaFin"):"";

String txtNombre = (request.getParameter("txtNombre") !=null)?request.getParameter("txtNombre"):"";
String txtRFC = (request.getParameter("txtRFC") !=null)?request.getParameter("txtRFC"):"";
String txtProveedor = (request.getParameter("txtNumProveedor") !=null)?request.getParameter("txtNumProveedor"):"";

//String cvePerf =	BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);

String infoRegresar = "";
int numRegAfectados = 0;

if (informacion.equals("catalogoProducto")) {
	List lista = new ArrayList();
	lista.add("1");
	lista.add("4");

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_producto_nafin");
	cat.setCampoClave("ic_producto_nafin");	
	cat.setCampoDescripcion("ic_nombre");
	cat.setOrden("ic_nombre");
	cat.setValoresCondicionIn(lista);
	infoRegresar = cat.getJSONElementos();

}else if(informacion.equals("catalogoNombreEPO")){
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();


}else if(informacion.equals("catalogoEpoRelacionadas")){
	ConsListaRequisitosInd obj = new ConsListaRequisitosInd();
	obj.setClaveProducto(claveProducto);
	obj.setClavePyme(clavePyme);
	obj.setClaveEpo(claveEpo);
	List lista = obj.getEpoRelacionadas();
	JSONArray jsonA = new JSONArray();
	jsonA = JSONArray.fromObject(lista);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if(lista.isEmpty()){
		infoRegresar = "{\"success\": true, \"total\": \""	+	jsonA.size() + "\", \"registros\": " + jsonA.toString()+ "}";
	}else{
		if(lista.get(0).equals("false")){
			jsonObj.put("total", "excede" );
			jsonObj.put("registros", "" );
		}else{
			jsonObj.put("total",  Integer.toString(jsonA.size()));
			jsonObj.put("registros", jsonA.toString() );
		}
		infoRegresar = jsonObj.toString();
	}
	
}else if (informacion.equals("obtenerNombrePyme")){
	ConsListaRequisitosInd obj = new ConsListaRequisitosInd();
	obj.setClaveNafin(claveNafinE);
	List lista = obj.getNombrePyme();
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if(!lista.isEmpty()){			
		jsonObj.put("clave", lista.get(0));
		jsonObj.put("descripcion", lista.get(1));		
	}else{
		jsonObj.put("muestraMensaje", new Boolean(true));
		jsonObj.put("textoMensaje", 	"Número Nafin Electronico no existe.");	
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("catalogoNafinE")){
	ConsListaRequisitosInd obj = new ConsListaRequisitosInd();
	obj.setClaveEpo(claveEpo);
	List lista = obj.getBusquedaAvanzada(txtNombre, txtRFC, txtProveedor);
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	JSONArray jsonA = new JSONArray();
	
	for(int i = 0 ; i < lista.size(); i++){
		HashMap lis = (HashMap)lista.get(i);
	}

	if (lista.isEmpty()){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	
	}else if (lista.size() > 0 && lista.size() < 1001 ){
		jsonA = JSONArray.fromObject(lista);
		jsonObj.put("total",  Integer.toString(jsonA.size()));
		jsonObj.put("registros", jsonA.toString() );
		
	}else if (lista.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("consultaGeneral") || informacion.equals("ArchivoCSV")){
	Vector epoVector = new Vector(Arrays.asList(epoVec[0].split(",")));
	ConsListaRequisitosInd obj = new ConsListaRequisitosInd();
	if(claveEpo.equals(""))
		claveEpo = "0";

	obj.setClaveEpo(claveEpo);
	obj.setClaveNafin(claveNafinE);
	obj.setClavePyme(clavePyme);
	obj.setClavePerfil("8");
	obj.setClaveProducto(claveProducto);
	obj.setFechaInicio(fechaInicio);
	obj.setFechaFin(fechaFin);
	obj.setEpoVector(epoVector);
	List lista = obj.getConsultaGeneral();

	if(lista.size() <= 10)
		lista.clear();

	if(informacion.equals("consultaGeneral")){
		JSONArray jsonA = new JSONArray();
		jsonA = JSONArray.fromObject(lista);
		infoRegresar = "{\"success\": true, \"total\": \""	+	jsonA.size() + "\", \"registros\": " + jsonA.toString()+ "}";

	}
	if(informacion.equals("ArchivoCSV")){
		String nombreArchivo = obj.crearCustomFileFromList(lista, strDirectorioTemp, "CSV");
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		infoRegresar = jsonObj.toString();
	}

} else if(informacion.equals("actualizarLista")){
	ConsListaRequisitosInd obj = new ConsListaRequisitosInd();
	obj.setClaveProducto(claveProducto);
	obj.setClavePyme(clavePyme);
	obj.setClaveEpo(claveEpo);
	obj.setEpo1(epo1);
	obj.setEpo5(epo5);
	obj.setEpo8(epo8);
	obj.setValEpo1(valEpo1);
	obj.setValEpo5(valEpo5);
	obj.setValEpo8(valEpo8);
	obj.setValNuevoEpo1(valNuevoEpo1);
	obj.setValNuevoEpo5(valNuevoEpo5);
	obj.setValNuevoEpo8(valNuevoEpo8);	
	String resultado = obj.actualizarLista();
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("textoMensaje", resultado);
	infoRegresar = jsonObj.toString();
}
%>
<%=  infoRegresar%>
