<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.model.catalogos.*,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.cadenas.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String infoRegresar="";
String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String cveEpo = (request.getParameter("HicEpo")== null) ? "" : request.getParameter("HicEpo");
String cmbAutorizacion = (request.getParameter("Hestatus")== null) ? "" : request.getParameter("Hestatus");
String fecCambioCtaIni 	= (request.getParameter("fechaOper1")== null) ? "" : request.getParameter("fechaOper1");
String fecCambioCtaFin 		= (request.getParameter("fechaOper2")== null) ? "" : request.getParameter("fechaOper2");
if(informacion.equals("catalogoEpo")){
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");	
	cat.setCampoDescripcion("cg_razon_social");
	infoRegresar=cat.getJSONElementos();
	
}else if(informacion.equals("Consulta")||informacion.equals("ArchivoCSV")){
	JSONObject jsonObj 	      = new JSONObject();
	int start=0,limit=0;
	String operacion= (request.getParameter("operacion")== null) ? "" : request.getParameter("operacion");
	ConsCambioCtasPyme paginador= new ConsCambioCtasPyme();
	paginador.setCveEpo(cveEpo);
	paginador.setFecCambioCtaIni(fecCambioCtaIni);
	paginador.setFecCambioCtaFin(fecCambioCtaFin);
	paginador.setCmbAutorizacion(cmbAutorizacion);
	
	CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
		try{
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));	
			}catch(Exception e){
					System.out.println("Error en parametros");
			}
		if (operacion.equals("Generar")) {	//Nueva consulta
				
				cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		if (informacion.equals("Consulta")){
			String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
			jsonObj = JSONObject.fromObject(consultar);
			infoRegresar=jsonObj.toString();
		}else if(informacion.equals("ArchivoCSV")){
			String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
	
	
}



%>
<%=infoRegresar%>