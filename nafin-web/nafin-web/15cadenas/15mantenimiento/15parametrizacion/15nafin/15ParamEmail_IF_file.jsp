<%@ page contentType="text/html; charset=UTF-8" import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
                com.netro.cadenas.CargaArchivoCorreosIF,
		com.netro.exception.*,
		java.sql.*,
		java.util.regex.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject" errorPage="/00utils/error_extjs_fileupload.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="../../../../15cadenas/015secsession_extjs.jspf"%>
<%
    String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
    ParametrosRequest req = null;
    String itemArchivo	="",  rutaArchivo ="",   error_tam =""; 
    String PATH_FILE	=strDirectorioTemp;
    String nombreArchivo= "";

    boolean codificacionValida = true;
    String codificacionArchivo = "";
	
    if (ServletFileUpload.isMultipartContent(request)) {
		
        // Create a factory for disk-based file items
	DiskFileItemFactory factory = new DiskFileItemFactory();
	
	// Set factory constraints		
	factory.setRepository(new File(PATH_FILE));
	// Create a new file upload handler
	ServletFileUpload upload = new ServletFileUpload(factory);
	
	req = new ParametrosRequest(upload.parseRequest(request));
			 
	FileItem fItem = (FileItem)req.getFiles().get(0);
	itemArchivo = (String)fItem.getName();
	InputStream archivo = fItem.getInputStream();
	rutaArchivo = PATH_FILE+itemArchivo;
	int tamanio = (int)fItem.getSize();		
               
	nombreArchivo= itemArchivo; 
	String rutaArchivoTemporal = PATH_FILE + "/" + nombreArchivo;
	fItem.write(new File(rutaArchivoTemporal));
		
	if(tamanio>2097152){
            error_tam ="El Archivo es muy Grande, excede el Limite que es de 2 MB.";
	}		
    }
    if(!error_tam.equals("")){
        nombreArchivo="";
    }
%>
{
    "success": true,
    "archivo":	'<%=nombreArchivo%>',
    "error_tam":	'<%=error_tam%>'	
}
