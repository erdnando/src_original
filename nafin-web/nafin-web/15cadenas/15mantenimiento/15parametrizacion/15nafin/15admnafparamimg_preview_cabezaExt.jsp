<%@ page import="netropology.utilerias.*, java.text.*, java.util.*"
		errorPage="/00utils/error.jsp"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/extjs.jspf" %>
<%response.setHeader("Pragma","no-cache");%>
<!-- NOTA: Se introduce el codigo extjs en el jsp por solo ser 2 botones los que se agregan a la pantalla.-->
<%

String nombreMes[] = {"enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"};
TimeZone.setDefault(TimeZone.getTimeZone("CST"));

Calendar fecha = Calendar.getInstance();
int dia = fecha.get(Calendar.DAY_OF_MONTH);
int mes = fecha.get(Calendar.MONTH);
int anio = fecha.get(Calendar.YEAR);
SimpleDateFormat formatoHora = new SimpleDateFormat ("hh:mm:ss");

String ic_epo = request.getParameter("ic_epo");

//Es necesario recibir el nombre del logo, ya que puede mostrar XXX.gif o XXX_home.gif
String logo = request.getParameter("logo");

String nombreArchivoZip = request.getParameter("nombreArchivoZip");
String operacion = (request.getParameter("operacion")==null)?"CAPTURA":request.getParameter("operacion");

%>
<input type="hidden" name="nombreArchivoZip" value="<%=nombreArchivoZip%>">
<input type="hidden" name="ic_epo" value="<%=ic_epo%>">
<input type="hidden" name="operacion" value="<%=operacion%>">
<html>
<head>
<title>Nafi@net - Entrada</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Cache-Control" content="no-cache">
<link rel="stylesheet" href="<%=strDirecVirtualTemp%><%=ic_epo%>.css">
<script type="text/javascript">

Ext.onReady(function(){
var operacion=(Ext.get('operacion').getValue());
var ic_epo=(Ext.get('ic_epo').getValue());
var nombreArchivoZip=(Ext.get('nombreArchivoZip').getValue());
var banderaAccion;
var mensaje;
var txtButt='Descargar Archivo';


	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('btnDesc');
			boton.setIconClass('');
			boton.enable();	
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesaAceptada = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			 Ext.Msg.show({
									title:	"Mensaje",
									msg:		'Los cambios han sido realizados con exito',
									buttons:	Ext.Msg.OK,
									height: 50,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											parent.window.close();
										}
									},
									closable:false
								});

		}else{
				// Mostrar mensaje de error
				Ext.Msg.alert("Mensaje de p�gina web.",'Error al consultar las cuentas.' );
		}
	}

if(operacion=='CAPTURA'){
	txtButt='Aceptar';
	mensaje='El dise&ntilde;o para la EPO '+ic_epo+' se muestra a continuaci&oacute;n. Por favor, confirme los cambios.';
}
else{
	mensaje='El dise&ntilde;o para la EPO '+ic_epo+' se muestra a continuaci&oacute;n.';
}
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 150,
		width: 940,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		frame: true,
		id: 'forma',
		defaults:
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [{
				xtype: 	'label',
				id:	 	'leyenda',
				hidden: 	false,
				cls:		'x-form-item',
				style: 	'font size:1; text-align:center;margin:0 auto;',
				html:  	'<b>'+mensaje+'</b>'
			}],
		buttons:
		[
			 {
			  //Bot�n BUSCAR
				xtype: 'button',
				text: txtButt,
				id: 'btnDesc',
				name: 'btnBuscar',
				hidden: false,
				formBind: true,
				handler: function(boton, evento) 
				{
			if(operacion=='CAPTURA'){
					Ext.Ajax.request({
						url: '15AdmNafParamImgExt.data.jsp',	
						params: {
									
									informacion: 			"disenoAceptado",
									nombreArchivoZip:nombreArchivoZip,
									ic_epo: ic_epo
						},callback: procesaAceptada
					});
					}else{
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '15AdmNafParamImgExt.data.jsp',	
						params: {
									
									informacion: 			"descargaArchivo",
									nombreArchivoZip:nombreArchivoZip,
									ic_epo: ic_epo
						},callback: procesarArchivoSuccess
					});
						
					}
				}
			 },{
				text:'Salir',
				xtype: 'button',
				align: 'right',
				handler: function() {
						
						 parent.window.close();
				}
			 }
			 
		]
	});
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido2',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [		
	  fp
	  ]
  });

});

</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table align="center" width="900" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>
</td>
<td>

<div align = "center" id="Central">
         <div  id="areaContenido2"><div style="height:230px"></div></div>
</div>
</td>
<td>
</td>
</tr>
<tr>
	<td colspan="3">


</table>

</body>
</html>







