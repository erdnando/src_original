Ext.onReady(function() {

	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);

//- - - - - - -  - - - - - HANDLER�S - - - - - - - - - - - - - - - - - - - - - -

	var procesarCatProductoData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cveProducto = Ext.getCmp('_cmb_prod');
			if(cveProducto.getValue()==''){
				
				var newRecord = new recordType({
					clave:'T',
					descripcion:'Todos los productos'
				});
				
				store.insert(0,newRecord);
				store.commitChanges();
				cveProducto.setValue('T');

			}
		}
  }
  
  var procesarCatEpoData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cveEpo = Ext.getCmp('_cmb_epo');
			Ext.getCmp('_cmb_epo').setValue("");
			if(cveEpo.getValue()==""){
				var newRecord = new recordType({
					clave:"",
					descripcion:"Seleccionar EPO"
				});
							
				store.insert(0,newRecord);
				store.commitChanges();
				cveEpo.setValue("");
				
				var newRecord2 = new recordType({
					clave:"T",
					descripcion:"Todas las EPO's (Horarios Especiales)"
				});	
				store.insert(1,newRecord2);
				store.commitChanges();
				cveEpo.setValue("T");
			}
			Ext.getCmp('_cmb_epo').setValue("");
										
		}
  };
	
//GUARDAR HORARIOS DEL GRID GENERAL
	var procesarGuardarHorarios =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			Ext.MessageBox.alert('Aviso', 'Los Horarios se han actualizado con �xito', function(){
				//contenedorPrincipal.el.mask('Consultando...', 'x-mask-loading');
				accionConsulta("CONSULTAR",null);
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Restablece los horarios Nafin
	var procesarRestablecerHorarios =  function(opts, success, response) {
	var cveEpo = Ext.getCmp('_cmb_epo');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			Ext.MessageBox.alert('Aviso', 'Los Horarios se han restablecido con �xito', function(){
				//contenedorPrincipal.el.mask('Consultando...', 'x-mask-loading');
				
					accionConsulta("CONSULTAR",null);
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

var procesarConsultaRegistros = function(total, registros){
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		var btnGuardar = Ext.getCmp('btnGuardar');
		if (registros != null) {
			var grid  = Ext.getCmp('gridConsulta');

			if (!grid.isVisible()) {
				grid.show();
			}

			var el = grid.getGridEl();
			if(registrosConsultadosData.getTotalCount() > 0) {
				el.unmask();
				btnGuardar.enable();
			} else {
				el.mask('No se encontraron horarios especiales parametrizados', 'x-mask');
				//grid.hide();
				btnGuardar.disable();
			}
		}
	}
	
//------------------------------------------------------------------------------
	
	var formatoHora = function (hora) {
		var result = false, m;
		var re = /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/;
		if ((m = hora.match(re))) {
			result = (m[1].length == 2 ? "" : "0") + m[1] + ":" + m[2];
		}
		return result;
	};
	
	var comparaHorasDeServicio = function(grid, record, fieldName, indexC, indexR){
		var result = true;
		var hrIniPyme = record.data['HORIP'];
		var hrIniIf = record.data['HORII'];
		var hrFinPyme = record.data['HORFP'];
		var hrFinIf = record.data['HORFI'];
		
		
		if(  (Date.parse('01/01/2013 '+hrIniPyme+':00') > Date.parse('01/01/2013 '+hrIniIf+':00')) ||
			  (Date.parse('01/01/2013 '+hrFinPyme+':00') > Date.parse('01/01/2013 '+hrFinIf+':00'))
		){
			result = false;
			Ext.MessageBox.alert('Aviso', 'El horario de cierre y apertura de Financiamiento a Distribuidores-PYME debe ser menor o igual al de cierre de Descuento electr�nico �IF', function(){
				grid.startEditing(indexR, indexC);
			});	
		}else if(  (Date.parse('01/01/2013 '+hrIniIf+':00') < Date.parse('01/01/2013 '+hrIniPyme+':00')) ||
			  (Date.parse('01/01/2013 '+hrFinIf+':00') < Date.parse('01/01/2013 '+hrFinPyme+':00'))
		){
			result = false;
			Ext.MessageBox.alert('Aviso', 'El horario de cierre y apertura de Financiamiento a Distribuidores-IF debe ser mayor o igual al de cierre de Descuento electr�nico �PYME', function(){
				grid.startEditing(indexR, indexC);
			});	
		}else if( Date.parse('01/01/2013 '+hrIniPyme+':00') > Date.parse('01/01/2013 '+hrFinPyme+':00') ){
			result = false;
			Ext.MessageBox.alert('Aviso', 'El horario de apertura de Financiamiento a Distribuidores-PYME debe ser menor o igual a su horario de cierre', function(){
				grid.startEditing(indexR, indexC);
			});	
		}else if( Date.parse('01/01/2013 '+hrIniIf+':00') > Date.parse('01/01/2013 '+hrFinIf+':00') ){
			result = false;
			Ext.MessageBox.alert('Aviso', 'El horario de apertura de Financiamiento a Distribuidores-IF debe ser menor o igual a su horario de cierre', function(){
				grid.startEditing(indexR, indexC);
			});	
		}
		
		return result;
	};

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - -

	var catalogoProductosData = new Ext.data.JsonStore({
		id:				'catalogoProductosDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15horxepoext.data.jsp',
		baseParams:		{	informacion: 'catalogoProductos'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: procesarCatProductoData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEPOsData = new Ext.data.JsonStore({
		id:				'catalogoEPOsDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15horxepoext.data.jsp',
		baseParams:		{	informacion: 'catalogoEPOs'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: procesarCatEpoData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'15horxepoext.data.jsp',
			baseParams: {	informacion:	'ConsultaRegistros'	},
			fields: [
				{ name: 'IDPROD'},
				{ name: 'NOMBREE'},
				{ name: 'IDEPO'},
				{ name: 'NOMBREP' },
				{ name: 'HORIP'},
				{ name: 'HORFP'},
				{ name: 'HORII'},
				{ name: 'HORFI'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null);
					}
				}
			}
	});
	
//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - -

	var procesaConsulta = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){
		 if(  estadoSiguiente == "CONSULTAR" ){
			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');
			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("gridConsulta").hide();
			Ext.StoreMgr.key('registrosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Consultar'
							}
				)
			});
		}else if(	estadoSiguiente == "LIMPIAR"){
			//Ext.getCmp('forma').getForm().reset();
			Ext.getCmp('_cmb_prod').reset();
			grid.hide();
		}
	}
	
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

		var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 2, align: 'center'},
					{header: 'PYME', colspan: 2, align: 'center'},
					{header: 'IF', colspan: 2, align: 'center'},
					{header: '', colspan: 1, align: 'center'}
				]
			]
		});
		

		//Elementos del grid de la consulta
		var grid = new Ext.grid.EditorGridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			style: 		'margin: 0 auto'/*; padding-top:10px;'*/,
			hidden: 		true,
			margins:		'20 0 0 0',
			title:		"Horarios de Servicio Mismo d�a para Pyme's e IF's por Epo - Producto",
			clicksToEdit:1,
			//view:			new Ext.grid.GridView({forceFit:	true}),
			stripeRows: true,
			loadMask: 	true,
			height: 		300,
			width: 		720,
			frame: 		false,
			plugins:		grupos,
			listeners: {
			cellclick :function(grid, rowIndex, colIndex, evento){

			},
			beforeedit : function(e){

			},
			afteredit : function(e){
					var record = e.record;
					var gridf = e.grid; 
					var store = gridf.getStore();
					var fieldName = gridf.getColumnModel().getDataIndex(e.column); // Se obtine nombre del Campo (Fields del Store)
					
					var horaGral = record.data[fieldName];
					
					//if(fieldName == 'APERTURAPYME'){
					var valor = formatoHora(horaGral);
					if(valor){
						record.data[fieldName]=valor;
						if ( !comparaHorasDeServicio(gridf, record, fieldName, e.column, e.row) ){
							record.data[fieldName]='';
							store.commitChanges();
							//gridf.startEditing(e.row, e.column);
						}else{
							store.commitChanges();
						}
					}else{
						record.data[fieldName]='';
						store.commitChanges();
					}
					
			}
			
		},
			columns: [
				{
					header: 		'EPO',
					tooltip: 	'EPOs',
					dataIndex: 	'NOMBREE',
					align:		'left',
					resizable: 	true,
					width: 		140,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Producto',
					tooltip: 	'Producto',
					dataIndex: 	'NOMBREP',
					align:		'left',
					resizable: 	true,
					width: 		170,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Apertura',
					tooltip: 	'Horario de apertura PYME',
					dataIndex: 	'HORIP',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
				},
					hiddenName : 'aperturaPyme',
					resizable: 	true,
					width: 		70,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Cierre',
					tooltip: 	'Horario de cierre PYME',
					dataIndex: 	'HORFP',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
				},
					hiddenName : 'cierrePyme',
					resizable: 	true,
					width: 		70,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Apertura',
					tooltip: 	'Horario de apertura IF',
					dataIndex: 	'HORII',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
				},
					hiddenName : 'aperturaIf',
					resizable: 	true,
					width: 		70,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Cierre',
					tooltip: 	'Horario de cierre IF',
					dataIndex: 	'HORFI',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
				},
					hiddenName : 'cierreIf',
					resizable: 	true,
					width: 		70,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
					},{
	      xtype: 'actioncolumn',
				header: 'Usar Horarios Nafin',
				tooltip: 'Establecer horarios Nafin',
				width: 125,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ok';
							return 'icoValidar';										
						},	
						handler: function(grid, rowIndex, colIndex, item, event) {
						registro = registrosConsultadosData.getAt(rowIndex);	//Se obtienen los valores del grid
						Ext.Ajax.request({
							url: '15horxepoext.data.jsp',
							params: 
										{
											informacion:'Reestablecer',
											cveprod:registro.get('IDPROD'),
											cvepo:registro.get('IDEPO')
										},				
							callback: procesarRestablecerHorarios											  
						});
					}
					}
				]				
			}
			],bbar: {
			buttonAlign: 	'right',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Guardar',
					iconCls:	'icoGuardar',
					width:	100,
					id: 		'btnGuardar',
					handler: function(grid, rowIndex, colIndex, item, event) {
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						var grid = Ext.getCmp('gridConsulta');
						var bandera = true;
						var arr = new Array();
						
						for(var i=0;i<registrosConsultadosData.getTotalCount();i++){
							registro = registrosConsultadosData.getAt(i);
							if(registro.get('HORIP')!='' && registro.get('HORFP')!='' && registro.get('HORII')!='' && registro.get('HORFI')!=''){
								var h = registro.get('IDPROD');
								var s = registro.get('IDEPO');
								var j = registro.get('HORIP');
								var k = registro.get('HORFP');
								var l = registro.get('HORII');
								var m = registro.get('HORFI');
								arr[i] = h+','+s+','+j+','+k+','+l+','+m;
							}else{
								Ext.MessageBox.alert('Aviso', 'Es obligatorio establecer todos los horarios');
								accionConsulta("CONSULTAR",null)
								bandera = false;
								break;
							}	
						}
						if(bandera){
							Ext.Ajax.request({
									url: '15horxepoext.data.jsp',
									params: {
										informacion: 'Guardar',
										arr : arr
									},
									callback: procesarGuardarHorarios
							});
						}
					}
				},'-'
			]
		}
	});

		var elementosFormaConsulta = [
			{
				xtype: 'combo',
				name: 'cmb_prod',
				id: '_cmb_prod',
				fieldLabel: 'Producto',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'claveProducto',
				forceSelection : true,
				triggerAction : 'all',
				editable: false,
				typeAhead: true,
				minChars : 1,
				store: 		catalogoProductosData,
				anchor:	'95%',
				listeners:{
						select: function (combo)
						{
							if(Ext.getCmp('_cmb_epo').getValue()==""){
								Ext.MessageBox.alert('Aviso', 'Debe de seleccionar una EPO para poder realizar la Consulta');
							}
							else{
								var g = Ext.getCmp('gridConsulta');
								g.hide();
								accionConsulta("CONSULTAR", null)
							}
						}
					}
			},{
				xtype: 'combo',
				name: 'cmb_epo',
				id: '_cmb_epo',
				fieldLabel: 'Epo',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'claveEPO',
				emptyText: 'Seleccionar EPO',
				forceSelection : true,
				triggerAction : 'all',
				editable: true,
				typeAhead: true,
				minChars : 1,
				store: 		catalogoEPOsData,
				anchor:	'95%',
				listeners:{
						select: function (combo)
						{
							if(Ext.getCmp('_cmb_epo').getValue()==""){
								Ext.MessageBox.alert('Aviso', 'Debe de seleccionar una EPO para poder realizar la Consulta');
							}
							else{
								var g = Ext.getCmp('gridConsulta');
								g.hide();
								accionConsulta("CONSULTAR", null)
							}
						}
					} 
				}
		];

		var fp = new Ext.form.FormPanel({
			id:				'forma',
			width:			590,
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			labelWidth:		150,
			items:			elementosFormaConsulta,
			monitorValid:	true
		});
		
//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	930,
		//height: 	'auto',
		align: 'center',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			grid
		]

	});

	Ext.StoreMgr.key('catalogoProductosDataStore').load();
	Ext.StoreMgr.key('catalogoEPOsDataStore').load();
	
});