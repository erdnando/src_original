<%@ page
		import="	java.util.*, 
				netropology.utilerias.*, 
				javax.naming.*,
				java.io.*,
				com.netro.cesion.*,
				org.apache.commons.fileupload.disk.*,
				org.apache.commons.fileupload.servlet.*,
				org.apache.commons.fileupload.*,
				com.netro.cadenas.*,
				net.sf.json.*"
	contentType="text/html; charset=UTF-8"
	errorPage="/00utils/error_extjs_fileupload.jsp"							
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../../../../15cadenas/015secsession_extjs.jspf" %>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload"/>


<%
String  noEpo = (request.getParameter("noEpo")!=null)?request.getParameter("noEpo"):"";
String  noIf = (request.getParameter("noIf")!=null)?request.getParameter("noIf"):"";
String  tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
String path_destino = strDirectorioTemp+"15archcadenas/";
String mensaje ="", resultado= "N";
String clave[]  = null;
int registros   =0,	 numFiles = 0;
boolean ok_file = true;
	
System.out.println("noEpo -------------> "+noEpo);
System.out.println("noIf ---------->"+noIf);
System.out.println("tipoArchivo ---------->"+tipoArchivo);	

JSONObject resultadoProceso = new JSONObject();

String rutaArchivoTemporal = null;
ParametrosRequest req = null;
String infoRegresar = "";
int tamanio = 0;
String itemArchivo = "";

if (ServletFileUpload.isMultipartContent(request)) {

	try {	
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		factory.setRepository(new File(strDirectorioTemp));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// Set overall request size constraint
		upload.setSizeMax(10 * 2097152);	//10 Kb
		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));		
		// Process the uploaded items
		FileItem item = (FileItem)req.getFiles().get(0);
		
		itemArchivo		= (String)item.getName();
		tamanio			= (int)item.getSize();
		
		if(tamanio > 2097152) {
			mensaje= "Error, el Archivo es muy Grande, excede el L�mite que es de 2 MB.";
			ok_file=false;
		}
		
		 
		path_destino = strDirectorioTemp+"15archcadenas/"+itemArchivo;
		item.write(new File(path_destino));
			
		/************************************************************/
		//se ingresa la imagen del archivo	
		DisenoIF dis = new DisenoIF();	

		String archivo1=path_destino;
		String archivo2=strDirectorioPublicacion+"00archivos/if/conv/"+noIf+"_"+noEpo+".pdf";
		dis.Clonar(archivo1,archivo2);
		
		dis.guardarConvenio(noIf,noEpo,archivo2,tipoArchivo);		
		resultado = "S";

	} catch(Exception e) {
		mensaje = "Error al actualizar archivo";
		e.printStackTrace();  
  }
  
  
	resultadoProceso.put("success", new Boolean(true));
	resultadoProceso.put("resultado", resultado);
	resultadoProceso.put("mensaje", mensaje);
	
	infoRegresar	 =resultadoProceso.toString();
	 
}

%>
<%=infoRegresar%>

