Ext.onReady(function() {

    var procesarvalidaExisteArchivo = function(opts, success, response) {
        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
            var info = Ext.util.JSON.decode(response.responseText);  
           
           if(info.existe ==='S'){
               Ext.getCmp('btnGenArchivo').enable();   
           }else if(info.existe ==='N'){
                Ext.getCmp('btnGenArchivo').disable();  
           }
           
        }else {
            NE.util.mostrarConnError(response,opts);
	}
    };

    var procesarDescargaArchivos = function(opts, success, response) {
        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
                      
            Ext.getCmp('btnGenArchivo').enable();           
            Ext.getCmp('btnGenArchivo').setIconClass('icoTxt');
            Ext.getCmp('btnGenArchivo').setText('Abrir Archivo');
                        
            var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};
            
            Ext.getCmp('btnGenArchivo').urlArchivo =  NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
            Ext.getCmp('btnGenArchivo').el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
           
	}else {
            NE.util.mostrarConnError(response,opts);
	}
    };    
    
   var procesoGuardaArchivo = function(opts, success, response) {
    
        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
            var resp = Ext.util.JSON.decode(response.responseText);
            var fp = Ext.getCmp('formaCarga');
            fp.el.unmask();
          
            var acuseInfo = [
               ['Archivo cargado:',resp.nombreArch],
               ['Total de registros cargados:', resp.totalLineas],
               ['Fecha de carga:', resp.fecha],
               ['Hora de carga:', resp.hora],
               ['Usuario:', resp.usuario]
            ];
            
            storeAcuseData.loadData(acuseInfo);
            Ext.getCmp('gridAcuse').show();
            Ext.getCmp('formaCarga').hide();
            Ext.getCmp('pnlLayout').hide();
            
        }else {
            NE.util.mostrarConnError(response,opts);
	}
    };
    
    
    var procesarValidaArchivo = function(opts, success, response) {
    
        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
            var resp = Ext.util.JSON.decode(response.responseText);
            var fp = Ext.getCmp('formaCarga');
            fp.el.unmask();
                      
            var respuesta = resp.respuesta;
            
            if(respuesta==='ConError'){
            
                Ext.getCmp('dataError').setValue(resp.resultado);
                Ext.getCmp('pnlErrores').show();
                Ext.getCmp('formaCarga').hide();
                Ext.getCmp('pnlLayout').hide();
                
            }else if(respuesta==='Exitoso'){
            
                fpCarga.el.mask('Guardando datos del archivo....', 'x-mask-loading');
                Ext.Ajax.request({
                    url: '15DestXEmail.data.jsp',
                    params:{
                        informacion: 'guardaArchivo',
                        archivo:Ext.getCmp('nombreArchivo').getValue()
                    },
                    callback: procesoGuardaArchivo
                });                
            }
            
        }else {
            NE.util.mostrarConnError(response,opts);
	}
    };
   
    var storeAcuseData = new Ext.data.ArrayStore({
        fields: [
            {name: 'etiqueta'},
            {name: 'informacion'}
	]
    });
    
    var gridAcuse = new Ext.grid.GridPanel({
        id: 'gridAcuse',
	store: storeAcuseData,
	style: 'margin:0 auto;',
	margins: '20 0 0 0',
	hidden: true,
	columns: [
            {
                header : 'Etiqueta',
		dataIndex : 'etiqueta',
		width : 190,
		align: 'right',
		sortable : false
            },
            {
                header : 'Informaci�n',
		tooltip: 'Informaci�n',
		dataIndex : 'informacion',
                width : 300,
		sortable : true
            }
        ],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 500,
	height: 200,
	title: ' ',
	frame: true,
	bbar: {
            xtype: 'toolbar',
            buttonAlign: 'center',
            items: [
                '-',
                {
                    text: 'Regresar',					
                    handler: function(){
                        window.location.href='15DestXEmail.jsp';
                    }
		},
		'-'
            ]
	}
    });
        

    var pnlErrores = new Ext.Panel({
        id: 'pnlErrores',
	width: 760,
	title: '<p align="center">Errores durante la carga del archivo</p>',
	frame: true,
	style: 'margin:0 auto;',
        hidden:true,
	items: [
            {
                xtype: 'textarea',
                id: 'dataError',
                width: 750,
		style: 'margin:0 auto;',
		readOnly: true,
		height: 200
            }
	],
	bbar: {
            xtype: 'toolbar',
            buttonAlign: 'center',
            items: [
                '-',
                {
                    text: 'Regresar',
                    handler: function(){
                        window.location.href='15DestXEmail.jsp';
                    }
                }
            ]
        }
    });
       
    
    var storeLayoutData = new Ext.data.ArrayStore({
        fields: [
            {name: 'NUMCAMPO'},
            {name: 'DESC'},
            {name: 'TIPODATO'},
            {name: 'LONGITUD'},
            {name: 'OBLIGATORIO'}
        ]
    });
    var infoLayout = [];
    infoLayout = [        
        ['1','Lista de Correos electr�nicos separados por punto y coma " ; "<br>Ejemplo: usuario@nafin.gob.mx;usuario2@nafin.gob.mx','Alfanum�rico','1000','Si']
    ];
    storeLayoutData.loadData(infoLayout);
    
    var gridLayout = new Ext.grid.GridPanel({
        id: 'gridLayout',
	store: storeLayoutData,	
	margins: '20 0 0 0',
	style: 'margin:0 auto;',
	columns: [
            {
                header : 'No. de Campo',
		dataIndex : 'NUMCAMPO',
		width : 100,
		resizable: false,
		align: 'center'
            },
            {
                header : 'Descripci�n',
		tooltip: 'Descripci�n',
		dataIndex : 'DESC',
		width : 350,
		resizable: false
            },
            {
                header : 'Tipo de Dato',
		dataIndex : 'TIPODATO',
		width : 100,
		resizable : false
            },
            {
                header : 'Longitud',
		dataIndex : 'LONGITUD',
		width : 100,
		resizable: false,
                align: 'center'
            },
            {
                header : 'Obligatorio',
		dataIndex : 'OBLIGATORIO',
		width : 100,
		resizable: false,
		align: 'center'
            }
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 770,
	height: 120,
	frame: true
    });
        
    var pnlLayout = new Ext.Panel({
        id: 'pnlLayout',
	width: 780,
	title: 'Se deber� cargar un archivo de texto (.txt)',
	frame: true,
	hidden: true,
	style: 'margin:0 auto;',
	items: [
            gridLayout
	],
	tools: [
            {
                id: 'close',
                handler: function(evento, toolEl, panel, tc) {
                    panel.hide();
                }
            }
	]
    });
     
        
    var elementosForma = [
        {
            xtype: 'panel',
            name: 'pnlMsgValid',
            id: 'pnlMsgValid1',
            width: 605,
            style: 'margin:0 auto;',
            frame: false,
            hidden: false,
            layout: 'hbox',
            items:[
                {
                    xtype: 'displayfield',
                    value: '* Consultar �ltimo archivo cargado'
		},
		{
                    xtype: 'button',
                    text:'Descargar',
                    id: 'btnGenArchivo',
                    disabled:true,
                    iconCls: 'icoGenerarDocumento',
                    urlArchivo: '',
                    handler: function(boton){
                        if(Ext.isEmpty(boton.urlArchivo)){
                            boton.disable();
                            boton.setIconClass('loading-indicator');
                            Ext.Ajax.request({
                                url: '15DestXEmail.data.jsp',
                                params:{
                                    informacion: 'descargaUltimoCarga'
                                },
                                callback: procesarDescargaArchivos
                            });
                        }else{
                            var fp = Ext.getCmp('formaCarga');
                            fp.getForm().getEl().dom.action = boton.urlArchivo;
                            fp.getForm().getEl().dom.submit();
                        }
                    }
                }
            ]
        },
        NE.util.getEspaciador(20),                
        {
            xtype:	'panel',
            layout:	'table',
            width: 600,
            anchor: '100%',
            id:'cargaArchivo1',
            layoutConfig: {
                columns: 3
            },
            defaults: {
                bodyStyle:'padding:4px'
            },
            items:	[
                {
                    xtype: 'button',
                    id: 'btnAyuda',
                    autoWidth: true,
                    autoHeight: true,
                    iconCls: 'icoAyuda',
                    handler: function(){
			
                        pnlLayout.show();
                        
                    }
		},
                {
                    xtype: 'panel',
                    id:'pnlArchivo',
                    layout: 'form',
                    fileUpload: true,
                    labelWidth: 140,
                    defaults: {
                        bodyStyle:'padding:5px',
                        msgTarget: 'side'
                    },
                    items: [
                        {
                            xtype: 'compositefield',
                            fieldLabel: '',
                            combineErrors: false,
                            msgTarget: 'side',
                            items: [
                                {
                                    xtype: 'fileuploadfield',
                                    id: 'archivo',
                                    width: 320,
                                    emptyText: 'Ruta del Archivo',
                                    fieldLabel: 'Ruta del Archivo Origen',
                                    name: 'archivoCarga',
                                    allowBlank: false,
                                    buttonCfg: {
                                        iconCls: 'upload-icon'
                                    },
                                    buttonText: null,
                                    regex: /^.*\.(txt|TXT)$/,
                                    regexText:'Solo se admiten archivos TXT'
                                }                     
                            ]
                        }
                    ]
                },		
                {
                    xtype:'button',
                    text:'Continuar',
                    id: 'btnContinuar',
                    iconCls: 'icoContinuar',
                    style: { 
                        marginBottom:  '10px' 
                    },
                    handler: function(){
                    
                        var cargaArchivo = Ext.getCmp('archivo');
			if (!cargaArchivo.isValid()){
                            cargaArchivo.focus();
                            return;
			} 
                        
                        fpCarga.getForm().submit({
                            url: '15ParamEmail_IF_file.jsp',
                            waitMsg: 'Cargando Archivo...',
                            success: function(form, action) {
                                
                                var resp = action.result;
                                 var mArchivo =resp.archivo;
				 
                                 if(mArchivo!=='') {
                                 
                                    Ext.getCmp('nombreArchivo').setValue(mArchivo);
                                  
                                    fpCarga.el.mask('Procesando Validaci�n ...', 'x-mask-loading');	
                                    
                                        Ext.Ajax.request({
                                        url: '15DestXEmail.data.jsp',
                                            params:{
                                                informacion: 'validarArchivo',
                                                archivo:resp.archivo
                                            },
                                            callback: procesarValidaArchivo
					});         
                                 }else  if(error_tam!==''){
                                    Ext.MessageBox.alert("Mensaje","El Archivo es muy Grande, excede el L�mite que es de 2 MB."); 
                                 }
                                 
                            },
                            failure: NE.util.mostrarSubmitError
			});                     
                                                
                    }
                }
            ]
        },
        { xtype: 'textfield',  hidden: true, id: 'nombreArchivo', value: '' }        
    ];
    
    
    var fpCarga = new Ext.form.FormPanel({
        id: 'formaCarga',
	width: 605,
	title: 'Destinatarios por actualizaciones de l�nea EPO/IF',
	frame: true,
	fileUpload: true,
	style: 'margin:0 auto;',
	bodyStyle: 'padding: 6px; text-align:left;',
	items: elementosForma,		
	monitorValid: true
    }); 

   new Ext.Container({
        id: 'contenedorPrincipal',
	applyTo: 'areaContenido',
	width: 890,
	height: 'auto',
	items: [
            fpCarga,
            NE.util.getEspaciador(20),         
            pnlLayout,
            pnlErrores,
            gridAcuse
	]
    });
    
    
    Ext.Ajax.request({
        url: '15DestXEmail.data.jsp',
        params:{
            informacion: 'validaExisteArchivo'
        },
        callback: procesarvalidaExisteArchivo
    });   
                                        

});