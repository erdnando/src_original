Ext.onReady(function(){
var convenio='';
var icCuenta;
var VOBO;
var csOpera;
var color;
var color2;
var rowSelection;
//-------------Handlers-------------


	var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	var procesaConsulta = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
				
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}
	var procesaCuentaSeleccionada = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fp.el.unmask();
			 grid.setVisible(true);

		}else{
				// Mostrar mensaje de error
				Ext.Msg.alert("Mensaje de p�gina web.",'Error al consultar las cuentas.' );
		}
	}
	var procesaCambioCuenta = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var resp = 	Ext.util.JSON.decode(response.responseText);
			fp.setVisible(false);
			grid.setVisible(false);
			//Ext.Msg.alert("Mensaje de p�gina web.",'Los siguientes intermediarios fueron parametrizados correctamente' );
			gridAcuse.setVisible(true);
			consultaProcesados.loadData(Ext.util.JSON.decode(response.responseText).registros);
			
		}else{
				// Mostrar mensaje de error
				Ext.Msg.alert("Mensaje de p�gina web.",'Error al almacenar la parametrizacion de cuentas bancarias' );
		}
	}
var procesarConsultaData = function (store, arrRegistros, opts) {
		
		fp.el.unmask();
		if(arrRegistros!= null) {
			if(!grid.isVisible()) {
				grid.setVisible(true);
			}
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
				
				Ext.getCmp('btnConfirmar').enable();
				//grid.getSelectionModel().selectRow(rowSelection);
				//fp2.setVisible(true);
			}else {
				Ext.getCmp('btnConfirmar').disable();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
var accionConsulta = function(estadoSiguiente, respuesta){
if(  estadoSiguiente == "OBTENER_NOMBRE_PYME" 										){
		
			fp.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15adminNafinParam02ExtMas.data.jsp',	
				params: {
							numeroDeProveedor:	respuesta.noNafinElec,
							ic_banco_fondeo:		respuesta.ic_banco_fondeo,
							ic_epo:					respuesta.ic_epo,
							informacion: 			"obtenNombrePyme" 
				},
				callback: procesaConsulta
			});

		}else if(  estadoSiguiente == "RESPUESTA_OBTENER_NOMBRE_PYME"){
		
			Ext.getCmp('hid_ic_pyme').setValue(respuesta.ic_pyme);
			Ext.getCmp('_txt_nombre').setValue(respuesta.txtCadenasPymes);
			grid.setTitle('PROVEEDOR: '+respuesta.txtCadenasPymes+'<center> N@E: '+Ext.getCmp('_txt_nafelec').getValue()+'</center>');
			gridAcuse.setTitle('<center><b>LAS SIGUIENTES PARAMETRIZACIONES SE REALIZARON CORRECTAMENTE</b></center><br/>'+'N�mero Electr�nico: '+Ext.getCmp('_txt_nafelec').getValue()+'<center>Proveedor: '+respuesta.txtCadenasPymes+'</center>');
			fp.el.unmask();
			if (respuesta.muestraMensaje != undefined && respuesta.muestraMensaje){
				Ext.Msg.alert("Mensaje de p�gina web.",	respuesta.textoMensaje);
				Ext.getCmp('_txt_nafelec').setValue('');
				grid.setVisible(false);
				//fp.getForm().reset();
				Ext.getCmp('Hcuenta').setValue('');
				Ext.getCmp('icEpo').setValue('');
				Ext.getCmp('icEpoGrupo').setValue('');

				catalogoEpo.removeAll();
				catalogoCuenta.removeAll();
				catalogoEpoGrupo.removeAll();
			}else{
				grid.setVisible(false);
				//fp.getForm().reset();
				Ext.getCmp('Hcuenta').setValue('');
				Ext.getCmp('icEpo').setValue('');
				Ext.getCmp('icEpoGrupo').setValue('');
				catalogoEpo.load({
					params: {ic_pyme:respuesta.ic_pyme}
				});
				catalogoEpoGrupo.load({
					params: {ic_pyme:respuesta.ic_pyme}
				});
				catalogoCuenta.load({
					params: {ic_pyme:respuesta.ic_pyme}
				});
			}

		}

}



var ConfirmaGen=function(selc){
	
	 var selections = selc.getSelections();
		//Realiza la agrupacion de info											
		var i = 0;

	var selections = selc.getSelections();
			  if (selections.length<=0 ) {			            
						Ext.MessageBox.alert("Mensaje", "Es necesario seleccionar al menos un registro");	
						                 
              }else if(selections.length>0)  {
					
					Ext.Msg.show({
									title:	"Mensaje",
									msg:		'Los registros seleccionados se parametrizar�n para la autorizaci�n por el Intermediario Financiero, �desea continuar?',
									buttons:	Ext.Msg.OKCANCEL,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
										 	Ext.getCmp('btnConfirmar').disable();	
											CambiarCuentas(selc)
										}
									},
									closable:false,
									icon: Ext.MessageBox.QUESTION
								});
					
				}
};
var CambiarCuentas=function(selc){
		
		var selections = selc.getSelections();
		//Realiza la agrupacion de info											
		var i = 0;
		var recordVOBO = "";
		var recordIC_IF="";
		var epo="";
			consultaProcesados.removeAll();		
		var valores="";
		var regi = Ext.data.Record.create(['INTERMEDIARIO', 'EPO','CUENTA']);
			for(;i<selections.length;i++){											 
				/*recordVOBO += selections[i].json.CS_VOBO_IF+",";						 
				recordIC_IF += selections[i].json.IC_IF+",";
			   consultaProcesados.add(	new regi({EPO:'',INTERMEDIARIO: '',CUENTA: ''}) );
				var reg = consultaProcesados.getAt(i);
				reg.set('EPO',selections[i].json.EPO);
				reg.set('INTERMEDIARIO',selections[i].json.INTERMEDIARIO);
				reg.set('CUENTA',Ext.getCmp('Hcuenta').lastSelectionText);*/
				if(Ext.getCmp('epo').getValue()){
					
					valores+=selections[i].json.IC_IF+","+selections[i].json.IC_EPO+","+Ext.getCmp('Hcuenta').getValue()+"|";
				}else{
					valores+=selections[i].json.VALORES+"|";
				}
			}
			
			if(Ext.getCmp('epo').getValue()){
					epo=Ext.getCmp('icEpo').getValue().split(',')[0];
				}
			//alert(recordIC_IF);
				Ext.Ajax.request({
							url: '15adminNafinParam02ExtMas.data.jsp',	
							params: Ext.apply(fp.getForm().getValues(),{
										valores:	valores,
										ic_pyme: Ext.getCmp('hid_ic_pyme').getValue(),
										ic_epo:					epo,
										informacion: 			"confirmarCuentas" 
							}),
							callback: procesaCambioCuenta
						});
	};
//-------------Stores------------



var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15adminNafinParam02ExtMas.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  var catalogoEpoGrupo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoGrupoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15adminNafinParam02ExtMas.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpoGrupo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15adminNafinParam02ExtMas.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		var catalogoCuenta = new Ext.data.JsonStore({
		id:				'catalogoCuenta',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15adminNafinParam02ExtMas.data.jsp',
		baseParams:		{	informacion: 'catalogoCuenta'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaProcesados = new Ext.data.JsonStore({

		fields: [
					{name: 'EPO'},
					{name: 'CUENTA'},
					{name: 'MONEDA'},
					{name: 'INTERMEDIARIO'},
					{name: 'MENSAJE'}
		]
	});

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15adminNafinParam02ExtMas.data.jsp',
		/*baseParams: {
			informacion: 'Consulta'
		},*/
		fields: [
					{name: 'INTERMEDIARIO'},
					{name: 'EPO'},
					{name: 'IC_IF'},
					{name: 'IC_EPO'},
					{name: 'PARAMETRIZACION'},
					{name: 'VALORES'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});
//-------------Elementos---------------

var elementosForma = [
				{
							xtype:'radiogroup',
							id:	'radioGp2',
							columns: 2,
							hidden: false,
							items:[
								{boxLabel: 'EPO',id:'epo', name: 'stat',  checked: true,
									listeners:{
										check:	function(radio){
															grid.setVisible(false);
															Ext.getCmp('icEpoGrupo').setVisible(true);
															Ext.getCmp('icEpo').setVisible(false);
															Ext.getCmp('notaDesc').setVisible(false);
															var cm=grid.getColumnModel();
															cm.setHidden(cm.findColumnIndex('EPO'), true);
															Ext.getCmp('_txt_nombre').setValue('');
															Ext.getCmp('_txt_nafelec').setValue('');
															Ext.getCmp('icEpoGrupo').setValue('');
															Ext.getCmp('Hcuenta').setValue('');
													}
									}
								},
								{boxLabel: 'Grupal',id:'grupal', name: 'stat',checked: false,
									listeners:{
										check:	function(radio){
															
															grid.setVisible(false);
															Ext.getCmp('icEpo').setVisible(true);
															Ext.getCmp('notaDesc').setVisible(true);
															Ext.getCmp('icEpoGrupo').setVisible(false);
															var cm=grid.getColumnModel();
															cm.setHidden(cm.findColumnIndex('EPO'), false);
															Ext.getCmp('_txt_nafelec').setValue('');
															Ext.getCmp('icEpoGrupo').setValue('');
															Ext.getCmp('Hcuenta').setValue('');
															Ext.getCmp('_txt_nombre').setValue('');
															
													}
									}
								}
							]
						},
				{
					xtype:'hidden',
					id:	'hid_nombre',
					value:''
				},{
					xtype:'hidden',
					id:	'hid_ic_pyme',
					value:''
				},
				{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
						xtype:		'textfield',
						name:			'txt_nafelec',
						id:			'_txt_nafelec',
						maskRe:		/[0-9]/,
						allowBlank:	true,
						fieldLabel: 'N@E<br/>Proveedor',
						maxLength:	25,
						listeners:	{
							'change':function(field){
											if ( !Ext.isEmpty(field.getValue()) ) {
	
												var respuesta = new Object();
												respuesta["noNafinElec"]		= field.getValue();
												//respuesta["ic_banco_fondeo"]	= Ext.getCmp('_ic_banco_fondeo').getValue();
												//respuesta["ic_epo"]				= Ext.getCmp('ic_epo').getValue();
												
												accionConsulta("OBTENER_NOMBRE_PYME",respuesta);
	
											}
										}
						}
					},{
						xtype:			'textfield',
						name:				'txt_nombre',
						id:				'_txt_nombre',
						width:			270,
						disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
						disabled:		true,
						allowBlank:		true,
						maxLength:		100
					}
				]
				},{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:'displayfield',
							id:'disEspacio',
							text:''
						},
						{
							xtype:	'button',
							id:		'btnBuscaA',
							iconCls:	'icoBuscar',
							text:		'B�squeda Avanzada',
							handler: function(boton, evento) {
										var winVen = Ext.getCmp('winBuscaA');
											if (winVen){
												Ext.getCmp('fpWinBusca').getForm().reset();
												Ext.getCmp('fpWinBuscaB').getForm().reset();
												
												Ext.getCmp('cmb_num_ne').setValue();
												Ext.getCmp('cmb_num_ne').store.removeAll();
												Ext.getCmp('cmb_num_ne').reset();
												winVen.show();
											}else{
												var winBuscaA = new Ext.Window ({
													id:'winBuscaA',
													height: 320,
													x: 300,
													y: 100,
													width: 550,
													heigth: 100,
													modal: true,
													closeAction: 'hide',
													title: 'B�squeda Avanzada',
													items:[
														{
															xtype:'form',
															id:'fpWinBusca',
															frame: true,
															border: false,
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 130,
															items:[
																{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:'Utilice el * para b�squeda gen�rica'
																},{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:''
																},{
																	xtype: 'textfield',
																	name: 'nombre_pyme',
																	id:	'txtNombre',
																	fieldLabel:'Nombre',
																	maxLength:	100
																},{
																	xtype: 'textfield',
																	name: 'rfc_pyme',
																	id:	'txtRfc',
																	fieldLabel:'RFC',
																	maxLength:	20
																},{
																	xtype: 'textfield',
																	name: 'num_pyme',
																	id:	'txtNe',
																	hidden:true,
																	fieldLabel:'N�mero Proveedor',
																	maxLength:	25
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Buscar',
																	iconCls:'icoBuscar',
																	handler: function(boton) {
																					catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {
																					Ext.getCmp('winBuscaA').hide();
																				}
																}
															]
														},{
															xtype:'form',
															frame: true,
															id:'fpWinBuscaB',
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															monitorValid: true,
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 80,
															items:[
																{
																	xtype: 'combo',
																	id:	'cmb_num_ne',
																	name: 'ic_pyme',
																	hiddenName : 'ic_pyme',
																	fieldLabel: 'Nombre',
																	emptyText: 'Seleccione Proveedor. . .',
																	displayField: 'descripcion',
																	valueField: 'clave',
																	triggerAction : 'all',
																	forceSelection:true,
																	allowBlank: false,
																	typeAhead: true,
																	mode: 'local',
																	minChars : 1,
																	store: catalogoNombreProvData,
																	tpl : NE.util.templateMensajeCargaCombo
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Aceptar',
																	iconCls:'aceptar',
																	formBind:true,
																	//hidden:true,
																	handler: function() {
																					if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																						var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																						var cveP = disp.substr(0,disp.indexOf(" "));
																						var desc = disp.slice(disp.indexOf(" ")+1);
																						Ext.getCmp('hid_ic_pyme').setValue(Ext.getCmp('cmb_num_ne').getValue())
																						Ext.getCmp('_txt_nafelec').setValue(cveP);
																						Ext.getCmp('_txt_nombre').setValue(desc);
																						Ext.getCmp('winBuscaA').hide();
																					}
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {	Ext.getCmp('winBuscaA').hide();	}
																}
															]
														}
													]
												}).show();
											}
										}
						}
					]
				},{
				xtype: 'combo',
				fieldLabel: 'Grupo de EPOs',
				emptyText: 'Seleccionar',
				displayField: 'descripcion',
				//allowBlank: false,
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				hidden: false,
				store: catalogoEpoGrupo,
				tpl: NE.util.templateMensajeCargaCombo,
				id: 'icEpoGrupo',
				mode: 'local',
				name: 'HicEpoG',
				hiddenName: 'HicEpoG',
				forceSelection: true,
				listeners: {
							select: {
								fn: function(combo) {
									
								
								}
							}
					}
			},{
					xtype: 'multiselect',
					fieldLabel: 'Seleccione EPOs a <br/>parametrizar la relaci�n PYME - IF',
					name: 'HicEpo',
					hiddenName: 'HicEpo',
					id: 'icEpo',
					width: 500,
					height: 200, 
					autoLoad: false,
					hidden: false,
					mode: 'local',
					store:catalogoEpo,
					displayField: 'descripcion',
					valueField: 'clave',		 
					tbar:[{
						text: 'Limpiar',
						handler: function(){
							fp.getForm().findField('icEpo').reset();
						}
					}],
					ddReorder: true,
					listeners: {
						change: function(combo){
						
						}
					}		
				},
				{
						xtype:'displayfield',
						id: 'notaDesc',
						value:'<b>Nota</b>: S�lo se despliegan las EPOs en los que el proveedor ya se encuentra<br/>afiliado a descuento'
					},{
					xtype:			'combo',
					id:				'Hcuenta',
					name:				'cuenta',
					hiddenName:		'cuenta',
					fieldLabel:		'Seleccione la Cuenta Bancaria',
					emptyText:		'Seleccionar. . .',
					mode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					forceSelection:false,
					typeAhead:		true,
					triggerAction:	'all',
					minChars:		1,
					store:			catalogoCuenta,
					tpl:				NE.util.templateMensajeCargaCombo
		}
	
]

var elementosCambioPanel=[
	{
							xtype:'radiogroup',
							id:	'radioGp',
							columns: 4,
							hidden: false,
							items:[
								{boxLabel: 'Individual',id:'individual', name: 'stat',  checked: false,
									listeners:{
										check:	function(radio){
														if (radio.checked){
																window.location.href='15adminNafinParam02Ext.jsp';

														}
													}
									}
								},
								{boxLabel: 'Masiva',id:'masiva', name: 'stat',checked: true,
									listeners:{
										check:	function(radio){
													}
									}
								},
								{boxLabel: 'Consultar Cambios Cta',id:'consulta', name: 'stat',checked: false,
									listeners:{
										check:	function(radio){
														window.location.href='15adminNafinParam02ExtConsulta.jsp';
													}
									}
								},
								{boxLabel: 'Cambio de Cuenta Masiva',id:'cambioM', name: 'stat', checked: false,
									listeners:{
										check:	function(radio){
														window.location.href='15adminNafinParam04Ext.jsp';
													}
									}
								}
							]
						}
]

	var sm = new Ext.grid.CheckboxSelectionModel({
		 header : '  ', checkOnly:true,hideable:true,singleSelect:false, id:'chkIR',width:35,
		  renderer: function(v, p, record){
				if (!record.data['PARAMETRIZACION']){
					return '<div class="x-grid3-row-checker">&#160;</div>';
					
				}else{
					return '<div>&#160;</div>';
				}
			}
			});
				
var grid = new Ext.grid.GridPanel({
		store: consultaData,
		viewConfig:{markDirty:false},
		width: 880,
		height: 350,
		sm:sm,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		stripeRows: true,
		loadMask: true,
		title: ' ',
		frame: true,
		hidden: true,
		columns: [
		{
				header : '<center>EPO</center>', tooltip: 'EPO',dataIndex : 'EPO',
				sortable : true, width : 220, align: 'left',  hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header : '<center>Intermediario Financiero</center>', tooltip: 'Intermediario Financiero',	dataIndex : 'INTERMEDIARIO',
				sortable : true, width : 250, align: 'left',  hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
								}
				
			},{
				header : '<center>Cuenta</center>', tooltip: 'Cuenta',
				sortable : true, width : 300, align: 'left',  hidden: false,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
								
								if (registro.get('PARAMETRIZACION')) {
									//this.items[0].tooltip = 'Ver';
									
									return '<font color=blue>Parametrizaci�n Realizada</font>';
								}else{
									 metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return Ext.getCmp('Hcuenta').lastSelectionText;
								}
								
				}
			},sm
			],
	bbar: {
			buttonAlign: 'right',
			id: 'barra',
			hidden:false,
			displayInfo: true,
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Confirmar Parametrizaci�n',
					id: 'btnConfirmar',
					handler: function(boton, evento) {																		
						ConfirmaGen(sm);
					}
				}
			]
		}
		
	}
);


var gridAcuse = new Ext.grid.GridPanel({
		store: consultaProcesados,
		viewConfig:{markDirty:false},
		width: 840,
		height: 350,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		stripeRows: true,
		loadMask: true,

		title: ' ',
		frame: true,
		hidden: true,
		columns: [
		{
				header : '<center>EPO</center>', tooltip: 'EPO',	dataIndex : 'EPO',
				sortable : true, width : 250, align: 'left',  hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						return value;
					}
			},
			{
				header : '<center>Nombre IF</center>', tooltip: 'Nombre IF',	dataIndex : 'INTERMEDIARIO',
				sortable : true, width : 250, align: 'left',  hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						return value;
					}
			},{
				header : '<center>Cuenta Bancaria</center>', tooltip: 'Cuenta Bancaria',	dataIndex : 'CUENTA',
				sortable : true, width : 380, align: 'left',  hidden: false,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
								return Ext.getCmp('Hcuenta').lastSelectionText;
				}
			},
			{
				header : '<center>Mensaje</center>', tooltip: 'Mensaje',	dataIndex : 'MENSAJE',
				sortable : true, width : 380, align: 'left',  hidden: false				
			}			
			],
	bbar: {
	
			buttonAlign: 'right',
			displayInfo: true,
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Regresar',
					id: 'btnSalir',
					iconCls: 'icoLimpiar',
					handler: function() {
						 location.reload();
						
					}
				}
			]
		}
		
	}
);

//-------------Principal-------------------

var panelCambioPanel = new Ext.Panel({
	hidden: false,
		height: 'auto',
		width: 840,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
			id: 'forma2',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosCambioPanel
	
});

var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 700,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Parametrizar',
			  name: 'btnBuscar',
			  iconCls: 'icoContinuar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						/*if(Ext.getCmp('_txt_nafelec').getValue()==''){
							Ext.getCmp('_txt_nafelec').markInvalid('Seleccione un Proveedor');
							return;
						}
						if(Ext.getCmp('icEpo').getValue()==''){
							Ext.getCmp('icEpo').markInvalid('Seleccione una EPO');
							return;
						}
						
						if(Ext.getCmp('cbMoneda').getValue()==''){
							Ext.getCmp('cbMoneda').markInvalid('Seleccione una Moneda');
							return;
						}
						if(Ext.getCmp('intermediario1').getValue()==''){
							Ext.getCmp('intermediario1').markInvalid('Seleccione un Intermediario Financiero');
							return;
						}*/
						//grid.setTitle('<center> IF: '+Ext.getCmp('intermediario1').lastSelectionText+'</center>');
						if(Ext.getCmp('epo').getValue()){
						if(Ext.getCmp('_txt_nafelec').getValue()==''){
							Ext.getCmp('_txt_nafelec').markInvalid('Seleccione un Proveedor');
							return;
						}
						if(Ext.getCmp('icEpo').getValue()==''){
							Ext.getCmp('icEpo').markInvalid('Seleccione al menos una EPO');
							return;
						}
						if(Ext.getCmp('Hcuenta').getValue()==''){
							Ext.getCmp('Hcuenta').markInvalid('Seleccione una Cuenta');
							return;
						}
						fp.el.mask('Procesando...', 'x-mask-loading');
						consultaData.load({params: Ext.apply(fp.getForm().getValues(),{
							ic_cuenta: Ext.getCmp('Hcuenta').getValue(),
							ic_pyme: Ext.getCmp('hid_ic_pyme').getValue(),
							informacion: 'Consulta'
						})});
						}
						else{
						
						if(Ext.getCmp('_txt_nafelec').getValue()==''){
							Ext.getCmp('_txt_nafelec').markInvalid('Seleccione un Proveedor');
							return;
						}
						if(Ext.getCmp('icEpoGrupo').getValue()==''){
							Ext.getCmp('icEpoGrupo').markInvalid('Seleccione un grupo de  EPO�s');
							return;
						}
						if(Ext.getCmp('Hcuenta').getValue()==''){
							Ext.getCmp('Hcuenta').markInvalid('Seleccione una Cuenta');
							return;
						}
						fp.el.mask('Procesando...', 'x-mask-loading');
						consultaData.load({params: Ext.apply(fp.getForm().getValues(),{
						
							ic_cuenta: Ext.getCmp('Hcuenta').getValue(),
							ic_pyme: Ext.getCmp('hid_ic_pyme').getValue(),
							informacion: 'Consulta2'
						})});
						
						}
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
	  panelCambioPanel,NE.util.getEspaciador(10),fp,NE.util.getEspaciador(20),grid,gridAcuse
	  ]
  });
	Ext.getCmp('icEpo').setVisible(true);
	Ext.getCmp('notaDesc').setVisible(true);
	Ext.getCmp('icEpoGrupo').setVisible(false);
});