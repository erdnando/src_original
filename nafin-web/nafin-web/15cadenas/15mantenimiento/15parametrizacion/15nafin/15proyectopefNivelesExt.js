Ext.onReady(function(){

	var totalepos =0;
	var ic_epoG = [];
	var ic_area_promocionG = [];
	var ic_subdireccionG = [];
	var ic_lider_promotorG = [];
	var ic_regionG = [];
	var ic_tipo_epoG = [];
	var ic_subtipo_epoG = [];
	var ic_sector_epoG = [];
	
	var cancelar =  function() { 
		totalepos =0;
		ic_epoG = [];
		ic_area_promocionG = [];
		ic_subdireccionG = [];
		ic_lider_promotorG = [];
		ic_regionG = [];
		ic_tipo_epoG = [];
		ic_subtipo_epoG = [];
		ic_sector_epoG = [];
	}
	
	function transmiteGuardar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.getBody().unmask();	
			if(info.mensaje =='EXITO') {
			
				Ext.Msg.alert('Mensaje', 'Datos actualizados',
				function(btn, text){
					fp.el.mask('Enviando...', 'x-mask-loading');		
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{					
							informacion: 'Consultar'						
						})
					});
				});
			}

		} else {
			NE.util.mostrarConnError(response,opts);						
		}
	}
	
	var  procesarGuardar  = function(){
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();				
		cancelar();
		cancelar();
		
		store.each(function(record) {
			ic_epoG.push(record.data['IC_EPO']);
			ic_tipo_epoG.push(record.data['TIPO_EPO']);
			ic_subtipo_epoG.push(record.data['SUBTIPO_EPO']);
			ic_sector_epoG.push(record.data['SECTOR_EPO']);
			ic_area_promocionG.push(record.data['AREA_PROMOCION']);
			ic_regionG.push(record.data['REGION']);
			ic_subdireccionG.push(record.data['SUB_DIRECCION']);
			ic_lider_promotorG.push(record.data['LIDER_PROMOTOR']);
			totalepos++;
		
		});
		
		Ext.getBody().mask("Guardando Datos ..", 'x-mask-loading');
		Ext.getCmp('btnGuardar').disable();	
		Ext.Ajax.request({
			url: '15proyectopefNiveles.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Guardar',
				totalepos:totalepos,
				 ic_epoG:ic_epoG,
				 ic_area_promocionG:ic_area_promocionG,
				 ic_subdireccionG:ic_subdireccionG,
				 ic_lider_promotorG:ic_lider_promotorG,
				 ic_regionG:ic_regionG,
				 ic_tipo_epoG:ic_tipo_epoG,
				 ic_subtipo_epoG:ic_subtipo_epoG,
				 ic_sector_epoG:ic_sector_epoG
			}),
			callback: transmiteGuardar
		});		
	}
	//****************+catalogo Organismo**********************

	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefNiveles.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	// ----------------  Tipo EPO --------------------------------------
	var catTipoEpo = new Ext.data.JsonStore({
		id: 'catTipoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefNiveles.data.jsp',
		baseParams: {
			informacion: 'catTipoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var comboTipoEPOAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catTipoEpo
	});
	
	Ext.util.Format.comboRenderer = function(comboTipoEPOAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var record = comboTipoEPOAsignar.findRecord(comboTipoEPOAsignar.valueField, value);
			return record ? record.get(comboTipoEPOAsignar.displayField) : comboTipoEPOAsignar.valueNotFoundText;		
		}	
	}
	
	// ---------------- Sub Tipo EPO --------------------------------------
	var catSubTipoEpo = new Ext.data.JsonStore({
		id: 'catSubTipoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefNiveles.data.jsp',
		baseParams: {
			informacion: 'catSubTipoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catSubTipoEpoGrid = new Ext.data.JsonStore({
		id: 'catSubTipoEpoGrid',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefNiveles.data.jsp',
		baseParams: {
			informacion: 'catSubTipoEpoGrid'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	

	var comboSubTipoEPOAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catSubTipoEpoGrid
	});
	
	Ext.util.Format.comboRenderer = function(comboSubTipoEPOAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var record = comboSubTipoEPOAsignar.findRecord(comboSubTipoEPOAsignar.valueField, value);
			return record ? record.get(comboSubTipoEPOAsignar.displayField) : comboSubTipoEPOAsignar.valueNotFoundText;		
		}	
	}
	
	// ---------------- Sector EPO --------------------------------------
	
	
	var catSectorEpo = new Ext.data.JsonStore({
		id: 'catSectorEpo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefNiveles.data.jsp',
		baseParams: {
			informacion: 'catSectorEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var comboSectorEPOAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catSectorEpo
	});
	
	Ext.util.Format.comboRenderer = function(comboSectorEPOAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var record = comboSectorEPOAsignar.findRecord(comboSectorEPOAsignar.valueField, value);
			return record ? record.get(comboSectorEPOAsignar.displayField) : comboSectorEPOAsignar.valueNotFoundText;		
		}	
	}
	
	// ---------------- Area Promoci�n --------------------------------------
	
	var catAreaPromocion = new Ext.data.JsonStore({
		id: 'catAreaPromocion',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefNiveles.data.jsp',
		baseParams: {
			informacion: 'catAreaPromocion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var comboAreaPromocionAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catAreaPromocion
	});
	
	Ext.util.Format.comboRenderer = function(comboAreaPromocionAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var record = comboAreaPromocionAsignar.findRecord(comboAreaPromocionAsignar.valueField, value);
			return record ? record.get(comboAreaPromocionAsignar.displayField) : comboAreaPromocionAsignar.valueNotFoundText;		
		}	
	}
	

// ---------------- Region --------------------------------------
	
	var catRegion = new Ext.data.JsonStore({
		id: 'catRegion',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefNiveles.data.jsp',
		baseParams: {
			informacion: 'catRegion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var comboRegionAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catRegion
	});
	
	Ext.util.Format.comboRenderer = function(comboRegionAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var record = comboRegionAsignar.findRecord(comboRegionAsignar.valueField, value);
			return record ? record.get(comboRegionAsignar.displayField) : comboRegionAsignar.valueNotFoundText;		
		}	
	}
	
	// ---------------- Sub Direccion  --------------------------------------
	var catSubDireccion = new Ext.data.JsonStore({
		id: 'catSubDireccion',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefNiveles.data.jsp',
		baseParams: {
			informacion: 'catSubDireccion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var comboSubDireccionAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catSubDireccion
	});
	
	Ext.util.Format.comboRenderer = function(comboSubDireccionAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var record = comboSubDireccionAsignar.findRecord(comboSubDireccionAsignar.valueField, value);
			return record ? record.get(comboSubDireccionAsignar.displayField) : comboSubDireccionAsignar.valueNotFoundText;		
		}	
	}
	
	// ---------------- Lider Promotor --------------------------------------
	
	var catLiderPromotor = new Ext.data.JsonStore({
		id: 'catLiderPromotor',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15proyectopefNiveles.data.jsp',
		baseParams: {
			informacion: 'catLiderPromotor'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var comboLiderPromotorAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catLiderPromotor
	});
	
	Ext.util.Format.comboRenderer = function(comboLiderPromotorAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var record = comboLiderPromotorAsignar.findRecord(comboLiderPromotorAsignar.valueField, value);
			return record ? record.get(comboLiderPromotorAsignar.displayField) : comboLiderPromotorAsignar.valueNotFoundText;		
		}	
	}
	
	// ---------------- Cosulta --------------------------------------
	

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;
			
			if(store.getTotalCount() > 0) {				
				Ext.getCmp('btnSalir').enable();	
				Ext.getCmp('btnGuardar').enable();													
				el.unmask();					
			} else {	
				Ext.getCmp('btnSalir').disable();	
				Ext.getCmp('btnGuardar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15proyectopefNiveles.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'IC_EPO'},
			{  name: 'NOMBRE_EPO' },
			{  name: 'TIPO_EPO' },
			{  name: 'SUBTIPO_EPO' },
			{  name: 'SECTOR_EPO' },
			{  name: 'AREA_PROMOCION' },
			{  name: 'REGION' },
			{  name: 'SUB_DIRECCION' },
			{  name: 'LIDER_PROMOTOR' }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});

	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData, 
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		 
		columns: [	
			{
				header: 'EPO',
				tooltip: ' EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 250,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Tipo de EPO',
				tooltip: 'Tipo de EPO',
				dataIndex: 'TIPO_EPO',
				sortable: true,
				resizable: true,
				width: 250,				
				align: 'left',
				editor: comboTipoEPOAsignar,
				renderer: Ext.util.Format.comboRenderer(comboTipoEPOAsignar)
			},	
			{
				header: 'Subtipo EPO',
				tooltip: 'Subtipo EPO',
				dataIndex: 'SUBTIPO_EPO',
				sortable: true,
				resizable: true,
				width: 250,				
				align: 'left',
				editor: comboSubTipoEPOAsignar,
				renderer: Ext.util.Format.comboRenderer(comboSubTipoEPOAsignar)
			},
			{
				header: 'Sector EPO',
				tooltip: 'Sector EPO',
				dataIndex: 'SECTOR_EPO',
				sortable: true,
				resizable: true,
				width: 250,				
				align: 'left',
				editor: comboSectorEPOAsignar,
				renderer: Ext.util.Format.comboRenderer(comboSectorEPOAsignar)
			},
			{
				header: '�rea de Promoci�n',
				tooltip: '�rea de Promoci�n',
				dataIndex: 'AREA_PROMOCION',
				sortable: true,
				resizable: true,
				width: 250,				
				align: 'left',
				editor: comboAreaPromocionAsignar,
				renderer: Ext.util.Format.comboRenderer(comboAreaPromocionAsignar)
			},
			{
				header: 'Regi�n',
				tooltip: 'Regi�n',
				dataIndex: 'REGION',
				sortable: true,
				resizable: true,
				width: 250,				
				align: 'left',
				editor: comboRegionAsignar,
				renderer: Ext.util.Format.comboRenderer(comboRegionAsignar)
			},
			{
				header: 'Subdirecci�n',
				tooltip: 'Subdirecci�n',
				dataIndex: 'SUB_DIRECCION',
				sortable: true,
				resizable: true,
				width: 250,				
				align: 'left',
				editor: comboSubDireccionAsignar,
				renderer: Ext.util.Format.comboRenderer(comboSubDireccionAsignar)
			},
			{
				header: 'L�der Promotor',
				tooltip: 'L�der Promotor',
				dataIndex: 'LIDER_PROMOTOR',
				sortable: true,
				resizable: true,
				width: 250,				
				align: 'left',
				editor: comboLiderPromotorAsignar,
				renderer: Ext.util.Format.comboRenderer(comboLiderPromotorAsignar)
			}	
		],					
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		displayInfo: true,		
		clicksToEdit: 1, 
		listeners: {
			beforeedit : function(e){// se dispara para calular datos que al caputar los campos editables 
							
				if (e.field === 'TIPO_EPO'  ||  e.field === 'SUBTIPO_EPO' ){
					catSubTipoEpoGrid.load({
						params:{											
							ic_tipo_epo:e.record.data['TIPO_EPO']										
						}
					});
				}		
				
			}
		},
		bbar: {			
			items: [					
				'->',
				'-',
				{
					xtype: 'button',
					id: 'btnGuardar',
					text: 'Guardar',
					iconCls: 'icoAceptar',
					handler: procesarGuardar
				},
				'-',
				{
					xtype: 'button',
					id: 'btnSalir',
					text: 'Salir',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '15proyectopefNivelesExt.jsp';					
					}
				}
			]
		}
	});













	var  elementosForma = [
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'EPO',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_epo',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo			
		},
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'Tipo EPO',
			name: 'ic_tipo_epo',
			id: 'ic_tipo_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_tipo_epo',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catTipoEpo,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {
					
						Ext.getCmp("ic_subtipo_epo1").setValue('');
					
						catSubTipoEpo.load({
							params:{											
								ic_tipo_epo:Ext.getCmp("ic_tipo_epo1").getValue()											
							}
						});						
					}
				}
			}
		},
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'Subtipo EPO',
			name: 'ic_subtipo_epo',
			id: 'ic_subtipo_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_subtipo_epo',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catSubTipoEpo,
			tpl : NE.util.templateMensajeCargaCombo			
		},
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'Sector EPO',
			name: 'ic_sector_epo',
			id: 'ic_sector_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_sector_epo',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catSectorEpo,
			tpl : NE.util.templateMensajeCargaCombo			
		},
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: '�rea de Promoci�n',
			name: 'ic_area_promocion',
			id: 'ic_area_promocion1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_area_promocion',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catAreaPromocion,
			tpl : NE.util.templateMensajeCargaCombo			
		},
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'Regi�n',
			name: 'ic_region',
			id: 'ic_region1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_region',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catRegion,
			tpl : NE.util.templateMensajeCargaCombo			
		},
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'Subdirecci�n',
			name: 'ic_subdireccion',
			id: 'ic_subdireccion1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_subdireccion',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catSubDireccion,
			tpl : NE.util.templateMensajeCargaCombo			
		},
		{
			xtype: 'combo',
			width: 400,
			fieldLabel: 'L�der Promotor',
			name: 'ic_lider_promotor',
			id: 'ic_lider_promotor1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_lider_promotor',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catLiderPromotor,
			tpl : NE.util.templateMensajeCargaCombo			
		}
	
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,							
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {					
				
					fp.el.mask('Enviando...', 'x-mask-loading');		
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{					
							informacion: 'Consultar'						
						})
					});							
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15proyectopefNivelesExt.jsp';					
				}
			}
		]
	});
	

	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: '<h1><b>Etiquetar EPO por Niveles</h1></b>',			
				id: 'boton1',					
				handler: function() {
					window.location = '15proyectopefNivelesExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Etiquetar EPO por Secretaria',			
				id: 'boton2',					
				handler: function() {
					window.location = '15proyectopefExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Reporte Obra Publica ',			
				id: 'boton3',					
				handler: function() {
					window.location = '15proyectopefReporteExt.jsp';
				}
			}	
		]
	};
	
//------ Componente Principal -------------------------------------------------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [
			NE.util.getEspaciador(10),
			fpBotones,
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			gridConsulta,
			NE.util.getEspaciador(10)
		]
	});

	catalogoEPO.load();
	catTipoEpo.load();
	catSectorEpo.load();
	catAreaPromocion.load();
	catRegion.load();
	catSubDireccion.load();
	catLiderPromotor.load();
	catSubTipoEpoGrid.load();
	
});