Ext.onReady(function() {
	var esEsquemaExtJS =  Ext.getDom('esEsquemaExtJS').value;
  
	function procesaConsultaValores(opts, success, response) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = Ext.util.JSON.decode(response.responseText);
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if (jsonData != null){	
				var interFinan = Ext.getCmp('tfNombreIf').setValue(jsonData.IF);
				var financiamientoDiferenciado = jsonData.financiamientoDiferenciado;
				Ext.getCmp('btnGuardar').hide();
				Ext.getCmp('btnConsultar').enable();	
				
				Ext.getCmp('formaDatos').show();
				
				if(jsonData.financiamientoDiferenciado=='N'){
				
					Ext.getCmp('cmbTipoFinanciamiento').setValue(jsonData.CG_FINANCIAMIENTO);
				
					if(jsonData.existencia=='S'){
						if(jsonData.CG_COMISION=='G'){
							Ext.getCmp('panel1').show();
							Ext.getCmp('panel2').hide();
							Ext.getCmp('panel3').hide();
							Ext.getCmp('cmbTipoComision').setValue(jsonData.CG_COMISION);
						}else if(jsonData.CG_COMISION=='E'){
						
							Ext.getCmp('panel1').show();
							Ext.getCmp('panel2').show();
							Ext.getCmp('panel3').show();
							Ext.getCmp('cmbTipoComision').setValue(jsonData.CG_COMISION);
						
							Ext.getCmp('tfComision1').setValue(jsonData.FG_PORC_COMISION_DE);
							Ext.getCmp('tfDescuentoMercantil').setValue(jsonData.FG_PORC_COMISION_DM);
							Ext.getCmp('tfVentaCartera').setValue(jsonData.FG_PORC_COMISION_VC);
							Ext.getCmp('tfRiesgoDistribuidor').setValue(jsonData.FG_PORC_COMISION_M2);
							
						}
						Ext.getCmp('dfFinanciamientoDiferenciado').show();
						Ext.getCmp('cmbTipoFinanciamiento').disable();
						Ext.getCmp('btnGuardar').show();
					}else{
						Ext.getCmp('dfFinanciamientoDiferenciado').show();
						Ext.getCmp('cmbTipoFinanciamiento').disable();
					}
				}else{
					if(jsonData.existencia=='S'){
					//	Ext.getCmp('cmbTipoFinanciamiento').reset();
						Ext.getCmp('dfFinanciamientoDiferenciado').hide();
						Ext.getCmp('cmbTipoFinanciamiento').enable();
						Ext.getCmp('btnGuardar').show();
						if(jsonData.CG_COMISION=='G'){
							Ext.getCmp('panel1').show();
							Ext.getCmp('panel2').hide();
							Ext.getCmp('panel3').hide();
							Ext.getCmp('cmbTipoFinanciamiento').setValue(jsonData.CG_FINANCIAMIENTO);
							Ext.getCmp('cmbTipoComision').setValue(jsonData.CG_COMISION);
						}else if(jsonData.CG_COMISION=='E'){					
							Ext.getCmp('panel1').show();
							Ext.getCmp('panel2').show();
							Ext.getCmp('panel3').show();
							Ext.getCmp('cmbTipoFinanciamiento').setValue(jsonData.CG_FINANCIAMIENTO);
							Ext.getCmp('cmbTipoComision').setValue(jsonData.CG_COMISION);
							Ext.getCmp('tfComision1').setValue(jsonData.FG_PORC_COMISION_DE);
							Ext.getCmp('tfDescuentoMercantil').setValue(jsonData.FG_PORC_COMISION_DM);
							Ext.getCmp('tfVentaCartera').setValue(jsonData.FG_PORC_COMISION_VC);
							Ext.getCmp('tfRiesgoDistribuidor').setValue(jsonData.FG_PORC_COMISION_M2);
						}
					}else{
						//Ext.getCmp('cmbTipoFinanciamiento').reset();
						Ext.getCmp('dfFinanciamientoDiferenciado').hide();
						Ext.getCmp('cmbTipoFinanciamiento').enable();
					}
				}			
			}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesaValoresIniciales(opts, success, response) {
		var formaDatos = Ext.getCmp('formaDatos');
		formaDatos.el.unmask();	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				if(jsonData.accion =='G')  {
					Ext.MessageBox.alert('Mensaje',jsonData.mensaje);
				}
			
				var comboDif = Ext.getCmp('cmbTipoFinanciamiento');
			
				Ext.Ajax.request({
						url: '15parametrizacionIf.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consulta'							
						}),
						callback:procesaConsultaValores 
				});	
				
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesarGuardar() {
		//Ext.getCmp('btnGuardar').disable();		
		var comisionValida =  Ext.getCmp('tfComision1');
		var descuentoMValida =  Ext.getCmp('tfDescuentoMercantil');
		var ventaCarteraValida =  Ext.getCmp('tfVentaCartera');
		var riesgoDistribuidorValida =  Ext.getCmp('tfRiesgoDistribuidor');
		var tipoComision =  Ext.getCmp('cmbTipoComision');
		if(tipoComision.getValue()=='E'){
			if(Ext.isEmpty(comisionValida.getValue())&&Ext.isEmpty(descuentoMValida.getValue())&&Ext.isEmpty(ventaCarteraValida.getValue())&&Ext.isEmpty(riesgoDistribuidorValida.getValue())){
				alert("Capturar el  porcentaje de  comisi�n para  Descuento Electr�nico y/o Financiamiento a Distribuidores ");
				return;
			}
		}
		formaDatos.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '15parametrizacionIf.data.jsp',
			params: Ext.apply(formaDatos.getForm().getValues(),fp.getForm().getValues(),{  
			informacion: "Guardar_Datos"
			}),
			callback: procesaValoresIniciales
		});
			
	}
	
	//***************Criterios de busqueda  ****************************
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15parametrizacionIf.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	
	var catalogoPYME = new Ext.data.JsonStore({
		id: 'catalogoPYME',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15parametrizacionIf.data.jsp',
		baseParams: {
			informacion: 'catalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var procesarConsulta = function(grid, rowIndex, colIndex, item, event) {
		Ext.Ajax.request({
			url: '15parametrizacionIf.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Consulta'							
			})
		});	
	}
	

	function limpiar() {
		Ext.getCmp('tfComision1').setValue('');
		Ext.getCmp('tfRiesgoDistribuidor').setValue('');
		Ext.getCmp('tfVentaCartera').setValue('');
		Ext.getCmp('tfDescuentoMercantil').setValue('');
		Ext.getCmp('cmbTipoComision').setValue('');
		Ext.getCmp('dfFinanciamientoDiferenciado').setValue('');
		Ext.getCmp('cmbTipoFinanciamiento').setValue('');
		Ext.getCmp('tfNombreIf').setValue('');
	}
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15parametrizacionIf.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catTipoFinanciamiento = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['N','NAFIN'],
			['P','Propio'],
			['M','Mixto']
		 ]
	});
	var catComision = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['G','GENERAL'],
			['E','ESPECIFICA']			
		 ]
	});
	var elementosForma  =[	
		{
			xtype: 'combo',
			name: 'interFinanciero',
			id: 'cmbInterFinanciero',
			hiddenName : 'interFinanciero',
			fieldLabel: '&nbsp;&nbsp;IF',
			mode: 'local', 
			autoLoad: false,
			valueField : 'clave',
			displayField : 'descripcion',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catalogoIF,
			 tpl:  '<tpl for=".">' +
                '<tpl if="!Ext.isEmpty(loadMsg)">'+
                '<div class="loading-indicator">{loadMsg}</div>'+
                '</tpl>'+
                '<tpl if="Ext.isEmpty(loadMsg)">'+
                '<div class="x-combo-list-item">' +
                '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                '</div></tpl></tpl>',
			listeners: {
				select:{ 
					fn:function (combo) {
						Ext.getCmp('formaDatos').hide();	
						limpiar(); 
					}
				}
			}	
		}
	];
	
	var parametrizacionPanel = {
		labelWidth	: 350,
		layout: {
			type: 'table',
			columns: 2
		},
		width	: 600,
		items		: [
			{
				xtype: 'displayfield',
				align: 'left',
				value: 'Nombre IF:  ',
				autoLoad: false,
				mode: 'local'						
			},
			{
				xtype:         'displayfield',
				name:          'nombreIf',
				fieldLabel: 	'Nombre IF',
            id:        'tfNombreIf',
            fieldStyle:    'font-weight: bold;',
				autoLoad: false,
				mode: 'local'           
			},			
			//---
			{
				xtype: 'displayfield',
				align: 'left',
				value: 'Tipo de Financiamiento:',
				autoLoad: false,
				mode: 'local'						
			},
			{
				xtype				: 'combo',
				name				: 'tipoFinanciamiento',
				id					: 'cmbTipoFinanciamiento',
				fieldLabel		: 'Tipo de Financiamiento',
				mode: 'local', 
				hiddenName		: 'tipoFinanciamiento',
				emptyText: 'Seleccionar...',	
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : catTipoFinanciamiento,
				autoLoad: false,
				displayField : 'descripcion',
				valueField : 'clave',		
				width: 150,
				allowBlank: true
			},
			//---------		
			{
				xtype: 'displayfield',
				align: 'left',
				value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
				autoLoad: false,
				mode: 'local'						
			},
			{
				xtype: 'displayfield',
				name:  'financiamientoDiferenciado',
            id:    'dfFinanciamientoDiferenciado',
            fieldStyle:    'font-weight: bold;',
				autoLoad: false,
				mode: 'local',               
				value: '* El IF tiene Financiamiento Diferenciado ',
				hidden: false
			},	
			
			//---
			{
				xtype: 'displayfield',
				align: 'left',
				value: 'Comisi�n',
				autoLoad: false,
				mode: 'local'						
			},
			{
				xtype				: 'combo',
				name			: 'tipoComision',
				id				: 'cmbTipoComision',
				fieldLabel	: 'Comisi�n',
				mode: 'local',	
				hiddenName : 'tipoComision',	
				emptyText: 'Seleccione ... ',
				forceSelection : true,	
				triggerAction : 'all',	
				typeAhead: true,
				minChars : 1,	
				store :catComision,	
				displayField : 'descripcion',	
				valueField : 'clave',
				width: 150,
				listeners: {
					select: {
						fn: function(combo) {
								Ext.getCmp('btnGuardar').show();
								 if(combo.getValue()=='E'){
									Ext.getCmp('panel2').show();
									Ext.getCmp('panel3').show();
									
								}else if(combo.getValue()=='G'){
									Ext.getCmp('panel2').hide();
									Ext.getCmp('panel3').hide();
								}
							}
						}
					}
				}
		]
	};
	
	//
	
	
	
	var  financiamientoDistribuidores = {
		labelWidth	: 300,
		layout: {
			type: 'table',
			columns: 3
		},
		items		: [
			{
				xtype: 'displayfield',
				align: 'left',
				value: 'Modalidad 1 (Riesgo Empresa de Primer Orden)',
				autoLoad: false,
				mode: 'local'						
			},
			{
				xtype: 'displayfield',
				align: 'left',
				value: '&nbsp;&nbsp;&nbsp;&nbsp',
				autoLoad: false,
				mode: 'local'						
			},
			{
				xtype: 'displayfield',
				align: 'left',
				value: '&nbsp;&nbsp;&nbsp;&nbsp',
				autoLoad: false,
				mode: 'local'						
			},
			//-----------
			{
				xtype: 'displayfield',
				value: ' Descuento Mercantil ',
				autoLoad: false,
				mode: 'local'						
			},
			{
				xtype			: 'numberfield',
				fieldLabel: 'Descuento Mercantil',
				name			: 'descuentoMercantil',
				id				: 'tfDescuentoMercantil',
				maxText:'El tama�o m�ximo del campos es de 2 enteros 2 decimales',				
				maxValue: '99.99',
				allowDecimals: true,
				allowNegative: false,
				width			: 100,
				msgTarget: 'side',
				margins: '0 20 0 0'
			},
			{
				xtype: 'displayfield',
				value: ' &nbsp;&nbsp;&nbsp;&nbsp;% Comisi�n',
				autoLoad: false,
				mode: 'local'						
			},
			//-----------			
			{
				xtype: 'displayfield',
				value: ' Venta Cartera',
				autoLoad: false,
				mode: 'local'						
			},
			{
				xtype			: 'numberfield',
				name			: 'ventaCartera',
				id				: 'tfVentaCartera',
				fieldLabel	: 'Venta Cartera',
				maxText:'El tama�o m�ximo del campos es de 2 enteros 2 decimales',				
				maxValue: '99.99',
				allowDecimals: true,
				allowNegative: false,
				width			: 100,
				msgTarget: 'side',
				margins: '0 20 0 0'
			},
			{
				xtype: 'displayfield',
				value: ' &nbsp;&nbsp;&nbsp;&nbsp;% Comisi�n',
				autoLoad: false,
				mode: 'local'						
			},
			//-----------	
			{
				xtype: 'displayfield',
				value: ' Modalidad 2 (Riesgo Distribuidor)',
				autoLoad: false,
				mode: 'local'						
			},
			{
				xtype			: 'numberfield',
				name			: 'riesgoDistribuidor',
				id				: 'tfRiesgoDistribuidor',
				fieldLabel	: 'Modalidad 2 (Riesgo Distribuidor)',
				maxText:'El tama�o m�ximo del campos es de 2 enteros 2 decimales',				
				maxValue: '99.99',
				allowDecimals: true,
				allowNegative: false,
				width			: 100,
				msgTarget: 'side',
				margins: '0 20 0 0'
			},
			{
				xtype: 'displayfield',
				value: ' &nbsp;&nbsp;&nbsp;&nbsp;% Comisi�n',
				autoLoad: false,
				mode: 'local'						
			}
			//-----------	
	
		]
	};
	
	var  descuentoElectronico = {
		labelWidth	: 300,
		layout: {
			type: 'table',
			columns: 3
		},
		items		: [
			{
				xtype: 'displayfield',
				value: '&nbsp; Comisi�n ',
				autoLoad: false,
				width: 230,
				mode: 'local'						
			},
			{
				xtype			: 'numberfield',
				name			: 'comision1',
				id				: 'tfComision1',
				fieldLabel	: ' Comisi�n',
				maxText:'El tama�o m�ximo del campos es de 2 enteros 2 decimales',				
				maxValue: '99.99',
				allowDecimals: true,
				allowNegative: false,
				width			: 100,
				msgTarget: 'side',
				margins: '0 20 0 0'
			},
			{
				xtype: 'displayfield',
				value: ' &nbsp;&nbsp;&nbsp;&nbsp;% Comisi�n',
				autoLoad: false,
				mode: 'local'						
			}					
		]
	}
	
			
	var formaDatos = new Ext.form.FormPanel({
		id					: 'formaDatos',
		layout			: 'form',
		width				: 600,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false	,
		title				: ' ',
		collapsible: true,
		hidden	   : true,
		height: 'auto',
		labelWidth:		100,
		bodyStyle		: 'padding: 2px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[		
			{
				layout	: 'hbox',
				title		: '',
				id			: 'panel1',						
				align: 'left',
				items		: [parametrizacionPanel ]
			},						
			{
				layout	: 'hbox',
				id			: 'panel2',	
				hidden	: true,
				title 	: 'DESCUENTO ELECTR�NICO',
				align: 'left',
				items		: [descuentoElectronico	]
			},					
			
			{
				layout	: 'hbox',
				title		: 'FINANCIAMIENTO A DISTRIBUIDORES',
				id			: 'panel3',	
				hidden	: true,
				align: 'left',
				items		: [financiamientoDistribuidores ]
			}
		],
		//monitorValid	: true,
		buttons			: [
			{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls:'icoGuardar',				
				handler: procesarGuardar
			
			} 
			
		]
	});
	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
		hidden: 		true,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: 'Porcentaje Comisi�n Fija',			
				id: 'boton1',					
				handler: function() {
					window.location = '15porcentajeComisionFija.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Parametrizaci�n IF',			
				id: 'boton2',					
				handler: function() {
					window.location = '15parametrizacionIf.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Detalle IF',			
				id: 'boton3',					
				handler: function() {
					window.location = '15parametrizacionDetalleIf.jsp';
				}
			}	
		]
	};
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: 'margin:0 auto;',
		frame: true,	
		bodyStyle: 'padding: 6px',
		labelWidth: 100,	
		titleCollapse: false,
		collapsible: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		buttons: [		
			{
				text: 'Consultar',
				id: 'btnConsultar',				
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					var inter = Ext.getCmp('cmbInterFinanciero');
					if(Ext.isEmpty(inter.getValue())){
						inter.markInvalid('Debe seleccionar un IF');
						return;
					}
					fp.el.mask('Cargando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '15parametrizacionIf.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consulta'							
						}),
						callback:procesaConsultaValores 
					});				
				 }
				}	
			]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		defaults:{
			style: {margin: '0 auto'}
		},
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			fpBotones,
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(20),
			formaDatos
		]
	});
	
	catalogoIF.load();
	
	if(esEsquemaExtJS!='true'){
		var btn1 = Ext.getCmp('fpBotones');
		btn1.show();
	}
	
	
});
