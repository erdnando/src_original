<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoIFhorEpoXif,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fondojr.*,
		com.netro.cadenas.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		net.sf.json.*,
		com.netro.exception.*,
		com.netro.procesos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";

/*	SE CREA NUEVO OBJETO PARA ACCESAR EL EJB */
ManejoServicio manejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);


if(informacion.equals("catalogoProducto")) {
	List lstCves = new ArrayList();
	lstCves.add(new Integer(1));
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_producto_nafin");
	cat.setCampoDescripcion("ic_nombre");
	cat.setTabla("COMCAT_PRODUCTO_NAFIN");
	cat.setCondicionIn(lstCves);
	infoRegresar = cat.getJSONElementos();	

} else if(informacion.equals("catalogoEpo")) {
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();	

} else if(informacion.equals("catalogoIf")) {
	String cveEpo  = request.getParameter("cveEpo")==null?"":request.getParameter("cveEpo");
	
	CatalogoIFhorEpoXif cat = new CatalogoIFhorEpoXif();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveEpo(cveEpo);
	cat.setClaveProducto("1");
	
	infoRegresar = cat.getJSONElementos();	

} else if(informacion.equals("consultaHorarios")) {
	JSONObject jsonObj = new JSONObject();
	String cveProducto  = request.getParameter("cveProducto")==null?"":request.getParameter("cveProducto");
	String cveEpo  = request.getParameter("cveEpo")==null?"":request.getParameter("cveEpo");
	String cveIf  = request.getParameter("cveIf")==null?"":request.getParameter("cveIf");
	
	List lstHorarios = manejoServicio.getHorariosEpoxIf(cveProducto, cveEpo, cveIf);
	
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lstHorarios);
	jsonObj.put("registros", jsObjArray.toString());
	jsonObj.put("total", jsObjArray.size()+"");
	jsonObj.put("success", new Boolean(true));
	
	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("guardaHorarios")){
	JSONObject jsonObj = new JSONObject();
	String tipoHorario = (request.getParameter("tipoHorario") == null)?"N":request.getParameter("tipoHorario");
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistros = JSONArray.fromObject(jsonRegistros);
	
	Iterator itReg = arrRegistros.iterator();
	int index = 0;
	HashMap hmHorarios = null;
	List lstHorarois = new ArrayList();
	while (itReg.hasNext()) {
		hmHorarios = new HashMap();
		JSONObject registro = (JSONObject)itReg.next();
		hmHorarios.put("CVEEPO", registro.getString("CVEEPO"));
		hmHorarios.put("CVEIF", registro.getString("CVEIF"));
		hmHorarios.put("CVEPROD", registro.getString("CVEPRODUCTO"));
		hmHorarios.put("NOMBREEPO", registro.getString("NOMBREEPO"));
		hmHorarios.put("NOMBREIF", registro.getString("NOMBREIF"));
		hmHorarios.put("NOMBREPRODUCTO", registro.getString("NOMBREPRODUCTO"));
		hmHorarios.put("APPYME", registro.getString("APERTURAPYME"));
		hmHorarios.put("APIF", registro.getString("APERTURAIF"));
		hmHorarios.put("CIPYME", registro.getString("CIERREPYME"));
		hmHorarios.put("CIIF", registro.getString("CIERREIF"));
		hmHorarios.put("OPFONDEO", registro.getString("OPERFONDEO"));
		hmHorarios.put("CSESPECIAL", tipoHorario);
		
		lstHorarois.add(hmHorarios);
		index++;
	}
	
	manejoServicio.setHorariosEpoXif(lstHorarois);
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}


System.out.println("infoRegresar = "+infoRegresar);

%>
<%=infoRegresar%>