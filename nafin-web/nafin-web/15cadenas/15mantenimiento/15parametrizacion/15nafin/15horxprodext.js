Ext.onReady(function() {

var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);

//- - - - - - -  - - - - - HANDLER�S - - - - - - - - - - - - - - - - - - - - - -

	var procesarCatProductoData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cveProducto = Ext.getCmp('_cmb_producto');
			if(cveProducto.getValue()==''){
				
				var newRecord = new recordType({
					clave:'',
					descripcion:'Todos los productos'
				});
				
				store.insert(0,newRecord);
				store.commitChanges();
				cveProducto.setValue('');

			}
		}
  }

	//GUARDAR HORARIOS DEL GRID GENERAL
	var procesarGuardarHorarios =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			Ext.MessageBox.alert('Aviso', 'Los Horarios se han actualizado con �xito', function(){
				//contenedorPrincipal.el.mask('Consultando...', 'x-mask-loading');
				accionConsulta("CONSULTAR",null);
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaRegistros = function(total, registros){
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		var btnGuardar = Ext.getCmp('btnGuardar');
		if (registros != null) {
			var grid  = Ext.getCmp('gridConsulta');
			grid.show();
			var el = grid.getGridEl();
			if(registrosConsultadosData.getTotalCount() > 0) {
				btnGuardar.enable();
			} else {
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
				btnGuardar.disable();
			}
		}
	}
	
	var formatoHora = function (hora) {
		var result = false, m;
		var re = /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/;
		if ((m = hora.match(re))) {
			result = (m[1].length == 2 ? "" : "0") + m[1] + ":" + m[2];
		}
		return result;
	};
	
	var comparaHorasDeServicio = function(grid, record, fieldName, indexC, indexR){
		var result = true;
		var id_prod = record.data['IDPROD'];
		var hrIniPyme = record.data['HORAP'];
		var hrIniIf = record.data['HORAI'];
		var hrFinPyme = record.data['HORCP'];
		var hrFinIf = record.data['HORCI'];
		
		
		if(  (Date.parse('01/01/2013 '+hrIniPyme+':00') > Date.parse('01/01/2013 '+hrIniIf+':00')) /*||
			  (Date.parse('01/01/2013 '+hrFinPyme+':00') > Date.parse('01/01/2013 '+hrFinIf+':00'))*/
		){
			result = false;
			if(indexC=='3'){
				if(id_prod=='1'){
					Ext.MessageBox.alert('Aviso', 'El horario de apertura de Descuento electr�nico-IF debe ser mayor o igual al de apertura de Descuento electr�nico �PYME ', function(){
						grid.startEditing(indexR, indexC);
					});
				}
				else{
					Ext.MessageBox.alert('Aviso', 'El horario de apertura de Financiamiento a Distribuidores-IF debe ser mayor o igual al de apertura de Financiamiento a Distribuidores �PYME ', function(){
						grid.startEditing(indexR, indexC);
					});	
				}
			}else{
				if(id_prod=='1'){
				Ext.MessageBox.alert('Aviso', 'El horario de apertura de Descuento electr�nico-PYME debe ser menor o igual al de apertura de Descuento electr�nico �IF ', function(){
					grid.startEditing(indexR, indexC);
				});
				}
				else{
					Ext.MessageBox.alert('Aviso', 'El horario de apertura de Financiamiento a Distribuidores-PYME debe ser menor o igual al de apertura de Financiamiento a Distribuidores �IF ', function(){
						grid.startEditing(indexR, indexC);
					});	
				}
			}
		}else if( (Date.parse('01/01/2013 '+hrFinIf+':00') < Date.parse('01/01/2013 '+hrFinPyme+':00'))){
			result = false;
			if(indexC=='4'){
				if(id_prod=='1'){
					Ext.MessageBox.alert('Aviso', 'El horario de cierre de Descuento electr�nico-IF debe ser mayor o igual al de cierre de Descuento electr�nico �PYME', function(){
						grid.startEditing(indexR, indexC);
					});
				}
				else{
					Ext.MessageBox.alert('Aviso', 'El horario de cierre de Financiamiento a Distribuidores-IF debe ser mayor o igual al de cierre de Financiamiento a Distribuidores �PYME ', function(){
						grid.startEditing(indexR, indexC);
					});	
				}
			}else{
				if(id_prod=='1'){
				Ext.MessageBox.alert('Aviso', 'El horario de cierre de Descuento electr�nico-PYME debe ser menor o igual al de cierre de Descuento electr�nico �IF ', function(){
					grid.startEditing(indexR, indexC);
				});
				}
				else{
					Ext.MessageBox.alert('Aviso', 'El horario de cierre de Financiamiento a Distribuidores-PYME debe ser menor o igual al de cierre de Financiamiento a Distribuidores �IF ', function(){
						grid.startEditing(indexR, indexC);
					});	
				}
			}
		}
		
		return result;
	};

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - -

	var catalogoProductosData = new Ext.data.JsonStore({
		id:				'catalogoProductosDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15horxprodext.data.jsp',
		baseParams:		{	informacion: 'catalogoProductos'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: procesarCatProductoData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'15horxprodext.data.jsp',
			baseParams: {	informacion:	'Consultar'	},
			fields: [
				{ name: 'IDPROD'},
				{ name: 'NOMPROD' },
				{ name: 'HORAP'},
				{ name: 'HORCP'},
				{ name: 'HORAI'},
				{ name: 'HORCI'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
						/*
						console.log("options.params(beforeload)");
						console.dir(options.params);
						*/
					}
				},
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null);
					}
				}
			}
	});
	
//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - -

	var procesaConsulta = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){
		 if(  estadoSiguiente == "CONSULTAR" ){
			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');
			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("gridConsulta").hide();
			Ext.StoreMgr.key('registrosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Consultar'
							}
				)
			});
		}else if(	estadoSiguiente == "LIMPIAR"){
			Ext.getCmp('forma').getForm().reset();
			grid.hide();
		}
	}	
	
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

		var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 1, align: 'center'},
					{header: 'PYME', colspan: 2, align: 'center'},
					{header: 'IF', colspan: 2, align: 'center'}
				]
			]
		});

		//Elementos del grid de la consulta
		var grid = new Ext.grid.EditorGridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			style: 		'margin: 0 auto'/*; padding-top:10px;'*/,
			hidden: 		false,
			margins:		'20 0 0 0',
			title:		'Horarios de Servicio Mismo d�a por Producto',
			clicksToEdit:1,
			//view:			new Ext.grid.GridView({forceFit:	true}),
			stripeRows: true,
			loadMask: 	true,
			height: 	170,
			//height: 		400,
			width: 		510,
			frame: 		true,
			plugins:		grupos,
			listeners: {
			cellclick :function(grid, rowIndex, colIndex, evento){

			},
			beforeedit : function(e){

			},
			afteredit : function(e){
					var record = e.record;
					var gridf = e.grid; 
					var store = gridf.getStore();
					var fieldName = gridf.getColumnModel().getDataIndex(e.column); // Se obtine nombre del Campo (Fields del Store)
					
					var horaGral = record.data[fieldName];
					
					//if(fieldName == 'APERTURAPYME'){
					var valor = formatoHora(horaGral);
					if(valor){
						record.data[fieldName]=valor;
						if ( !comparaHorasDeServicio(gridf, record, fieldName, e.column, e.row) ){
							record.data[fieldName]='';
							store.commitChanges();
							//gridf.startEditing(e.row, e.column);
						}else{
							store.commitChanges();
						}
					}else{
						record.data[fieldName]='';
						store.commitChanges();
					}
					
			}
			
		},
			columns: [
				{
					header: 		'Producto',
					tooltip: 	'Producto',
					dataIndex: 	'NOMPROD',
					align:		'left',
					//sortable: 	true,
					resizable: 	true,
					width: 		200,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Apertura',
					tooltip: 	'Horario de apertura PYME',
					dataIndex: 	'HORAP',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
					},
					//sortable: 	true,
					resizable: 	true,
					width: 		70,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Cierre',
					tooltip: 	'Horario de cierre PYME',
					dataIndex: 	'HORCP',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
					},
					//sortable: 	true,
					resizable: 	true,
					width: 		70,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Apertura',
					tooltip: 	'Horario de apertura IF',
					dataIndex: 	'HORAI',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
					},
					//sortable: 	true,
					resizable: 	true,
					width: 		70,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Cierre',
					tooltip: 	'Horario de cierre IF',
					dataIndex: 	'HORCI',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
					},
					//sortable: 	true,
					resizable: 	true,
					width: 		70,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				}
			],bbar: {
			buttonAlign: 	'right',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Guardar',
					iconCls:	'icoGuardar',
					width:	100,
					id: 		'btnGuardar',
					handler: function(grid, rowIndex, colIndex, item, event) {
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						var grid = Ext.getCmp('gridConsulta');
						var bandera = true;
						var arr = new Array();
						
						for(var i=0;i<registrosConsultadosData.getTotalCount();i++){
							registro = registrosConsultadosData.getAt(i);	//Se obtienen los valores del grid
							if(registro.get('HORAP')!='' && registro.get('HORCP')!='' && registro.get('HORAI')!='' && registro.get('HORCI')!=''){
								
								var h = registro.get('IDPROD');
								var j = registro.get('HORAP');
								var k = registro.get('HORCP');
								var l = registro.get('HORAI');
								var m = registro.get('HORCI');
								arr[i] = h+','+j+','+k+','+l+','+m;
							}else{
								Ext.MessageBox.alert('Aviso', 'Es obligatorio establecer todos los horarios');
								registrosConsultadosData.load();
								bandera = false;
								break;
							}	
						}
						if(bandera){
							Ext.Ajax.request({
									url: '15horxprodext.data.jsp',
									params: {
										informacion: 'Guardar',
										arr : arr
									},
									callback: procesarGuardarHorarios
							});
						}
					}
				},'-'
			]
		}
	});

		var elementoFormaConsulta = [
			{
				xtype: 'combo',
				name: 'cmb_producto',
				id: '_cmb_producto',
				fieldLabel: 'Producto',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'claveProducto',
				emptyText: 'Todos los Productos',
				forceSelection : true,
				triggerAction : 'all',
				editable: false,
				typeAhead: true,
				minChars : 1,
				store: 		catalogoProductosData,
				anchor:	'95%',
				listeners: { select: function (combo)
				{
					accionConsulta("CONSULTAR", null)
				}}
			}
		];

		var fp = new Ext.form.FormPanel({
			id:				'forma',
			width:			550,
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			labelWidth:		150,
			//defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
			items:			elementoFormaConsulta,
			monitorValid:	true
		});
		
//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	930,
		//height: 	'auto',
		align: 'center',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			grid
		]

	});

	Ext.StoreMgr.key('catalogoProductosDataStore').load();
	registrosConsultadosData.load();
	
});