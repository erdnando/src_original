Ext.onReady(function(){
	var cvePerf = "";
	var hoy = "";	
	var estatusActual; 
	var claveSel = "";
	var fechaBorrar = "";
//-------------Handlers-----------------------------------------------------------------	
	
	var procesarCatalogo = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccione una Moneda", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboMoneda').setValue('');
	}
	
	
	var procesaMensaje = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);			
					Ext.MessageBox.alert('Mensaje',info.MENSAJE,
						function(){
							window.location = '15admnafparamtcambioExt.jsp';
						}
					);		
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesaPerfil = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			cvePerf = info.CLAVEPERFIL;
			hoy = info.FECHAHOY;
			fechaBorrar = hoy;
			Ext.getCmp("fechaActual").setValue(info.FECHAHOY);
			//cvePerf = "8";
			if(cvePerf != '8'){	
				Ext.getCmp('btnConsultar').setVisible(false);			
			}else{
				Ext.getCmp('btnAceptar2').setVisible(false);
				Ext.getCmp('btnAceptar').setVisible(false);
				Ext.getCmp('btnBorrar').setVisible(false);
			}				
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}

	
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');	
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	
	var procesarDatos2 = function (store,arrRegistros,opts){
		Ext.getCmp('btnAceptar2').enable();
		if(arrRegistros!=null){			
			var el = gridDolar.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarConsultar = function (){
		fpDolar.show();
		gridDolar.setTitle('<center>'+ Ext.getCmp('idComboMoneda').getRawValue()+'</center>');
		grid.hide();
		gridDolar.show();
		consultaDataGrid2.load({
		params:{ cboMoneda: Ext.getCmp('idComboMoneda').getValue(),
					txtDesde: hoy,
					txtHasta: hoy
				}
		});		
	}


	var procesarBorrar = function (){
		Ext.Ajax.request({
		url: '15admnafparamtcambioExt.data.jsp',
		params: {
			informacion: 'Borrar',
			txtCveMoneda: claveSel, 
			txtFechaAplic: fechaBorrar  
		},
		callback: procesaMensaje
	});
	}	


	var procesarAceptar = function(){
		var store = grid.getStore();
		var errorValidacion = false;
		var numRegistro = -1;
		var columnModelGrid = grid.getColumnModel();
		var fechaActual = Ext.getCmp("fechaActual");
		var fechaAplicacion;
		var fechaInicial;
		var fechaFinal;

		var ValorCompra;
		var FAplic;
		var FAplicFin;
		
		store.each(function(record) {	
			numRegistro = store.indexOf(record);
			if(record.data['VALOR'] == ''){	
				errorValidacion = true;					
				Ext.MessageBox.alert('Error de validaci�n',' Favor de teclear el valor de compra ',
					function(){
					//Ext.getCmp('btnConfirmar').enable();
							grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('VALOR'));
					});
				return false;
			}
			else if(Ext.getCmp('idComboMoneda').getValue() == '40'){
				var fechaAplic =  record.data['Inicio'];   
				var fechaActual_2 =  Ext.util.Format.date(fechaActual.getValue(),'d/m/Y');   
				
				if(!isdate(fechaAplic)){
					errorValidacion = true;
					Ext.MessageBox.alert('Error de validaci�n','La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa',
						function(){
							grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('Inicio'));
						});					
					return false;
				}
				
				if(!isdate(record.data['Final'])){
					errorValidacion = true;
					Ext.MessageBox.alert('Error de validaci�n','La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa',
						function(){
							grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('Final'));
						});					
					return false;
				}
				
				var diferencia = datecomp(fechaActual_2,fechaAplic);
				if(diferencia==1)  {
					errorValidacion = true;				
					Ext.MessageBox.alert('Error de validaci�n','No es posible dar de alta una Fecha Aplicaci�n Inicial menor al d�a de hoy',
						function(){
							//Ext.getCmp('btnConfirmar').enable();
							grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('Inicio'));
						});
					return false;			
				}
		
				var diferencia2 = datecomp(record.data['Inicio'],record.data['Final']);
				if(diferencia2 == 1 ){
					errorValidacion = true;				
					Ext.MessageBox.alert('Error de validaci�n','La Fecha Aplicaci�n Inicial debe ser menor que la Fecha Aplicaci�n Final',
						function(){
							grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('Final'));
						});
					return false;
				}		
			}//else if 40
			else {
				var fechaAplic =  record.data['Inicio'];   
				var fechaActual_2 =  Ext.util.Format.date(fechaActual.getValue(),'d/m/Y');   
				if(!isdate(fechaAplic)){
					errorValidacion = true;
					Ext.MessageBox.alert('Error de validaci�n','La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa',
						function(){
							grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('Inicio'));
						});					
					return false;
				}
				var diferencia = datecomp(fechaActual_2,fechaAplic);
				if(diferencia==2)  {
					errorValidacion = true;				
					Ext.MessageBox.alert('Error de validaci�n','No es posible dar de alta un tipo de cambio con fecha mayor al dia de hoy',
						function(){
							grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('Inicio'));
						});
					return false;
				}
			}
		
		valorCompra = record.data['VALOR'];
		FAplic = record.data['Inicio'];
		//if(Ext.getCmp('idComboMoneda').getValue() == '40'){
		if(claveSel == '40'){
			FAplicFin = record.data['Final'];
		}else{
			FAplicFin = "";
		}
		
		}); //store
		
		if (errorValidacion) {
			return;
		}

		Ext.Ajax.request({
			url: '15admnafparamtcambioExt.data.jsp',
			params: {
				informacion: 'Aceptar',
				txtCveMoneda: claveSel ,
				txtValorCompra: valorCompra,
				txtFechaAplic: FAplic,
				txtFechaAplicfin: FAplicFin
			},
			
			callback: procesaMensaje
		});	

	}// fin function	


	var procesarAceptar2 = function(){
		var fechaDesde = Ext.getCmp("dateDesde");
		var fechaHasta = Ext.getCmp("dateHasta");
		var errorValidacion = false;

		var Desde =  Ext.util.Format.date(fechaDesde.getValue(),'d/m/Y');   
		var Hasta =  Ext.util.Format.date(fechaHasta.getValue(),'d/m/Y');   
		

		 if(Ext.isEmpty(Desde)){
				Ext.getCmp('btnAceptar2').enable();
				errorValidacion = true;
				fechaDesde.focus();
		}else	if(!isdate(Desde)) { 
				errorValidacion = true;
				Ext.getCmp('btnAceptar2').enable();
				fechaDesde.markInvalid("Favor de teclear la fecha con el formato  (dd/mm/aaaa)");				
				fechaDesde.focus();				
		}else if (Ext.isEmpty(Hasta)){
				Ext.getCmp('btnAceptar2').enable();
				errorValidacion = true;
				fechaHasta.focus();
		}else if(!isdate(Hasta)){
				errorValidacion = true;
				Ext.getCmp('btnAceptar2').enable();
				fechaHasta.markInvalid("Favor de teclear la fecha con el formato  (dd/mm/aaaa)");				
				fechaHasta.focus();		
		}else{
			var diferencia = datecomp(Hasta, Desde);
			if(diferencia == 2)  {
				errorValidacion = true;				
				//Ext.MessageBox.alert('Error de validaci�n','La fechaActual hasta debe ser mayor a la fechaActual desde',
				fechaHasta.markInvalid("La fechaActual hasta debe ser mayor a la fechaActual desde");	
					//function(){
						Ext.getCmp('btnAceptar2').enable();
						fechaHasta.focus();
					//});
			}
		}	//fin else 


		if(errorValidacion) {
			return;
		}

		fpDolar.show();
		gridDolar.setTitle('<center>'+ Ext.getCmp('idComboMoneda').getRawValue()+'</center>');

		grid.hide();
		gridDolar.show();
		consultaDataGrid2.load({
			params:{ 
			//cboMoneda: Ext.getCmp('idComboMoneda').getValue(),
						cboMoneda: claveSel,
						txtDesde: Desde,
						txtHasta: Hasta
					 }
		});	

	}	
		
//-------------Stores-------------------------------------------------------------------

	var CatalogoMoneda = new Ext.data.JsonStore
	({
		id: 'catMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admnafparamtcambioExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoMoneda'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarCatalogo,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});  
	
	
	var Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);
	
	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15admnafparamtcambioExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'CLAVEMONEDA'},
			{name: 'VALOR'},
			{name:'Inicio'},
			{name: 'Final'},
			{name: 'NOMBREMONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			//load: procesarDatos, 
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	
	var consultaDataGrid2 = new Ext.data.JsonStore({
		root : 'registros',
		url : '15admnafparamtcambioExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid2'
		},
		fields: [		
			{name: 'VALORCOMPRA'},
			{name: 'VALORVENTA'},
			{name:'FECHA'}	
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos2, 
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	

//--------------Componentes-------------------------------------------------------------

	var elementosForma = 
	[	
		{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			forceSelection: true,
			autoSelect: false,
			name:'claveProducto',
			id:'idComboMoneda',
			displayField: 'descripcion',
			allowBlank: true,
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			autoLoad: false,
			typeAhead: true,
			minChars: 1,
			store: CatalogoMoneda,
			listeners:{				
				select: function(combo){
					var cm = grid.getColumnModel();
					if(combo.getValue() != ''){
						claveSel = combo.getValue();
						Ext.getCmp('btnConsultar').enable();
						Ext.getCmp('btnAceptar').enable();
						Ext.getCmp('btnAceptar2').enable();
						Ext.getCmp('btnBorrar').enable();
						if(combo.getValue() == '54'){
							Ext.getCmp('dateDesde').setValue(hoy);
							Ext.getCmp('dateHasta').setValue(hoy);
							fpDolar.show();							
							gridDolar.setTitle('<center>'+ Ext.getCmp('idComboMoneda').getRawValue()+'</center>');
							grid.hide();
							gridDolar.show();
							consultaDataGrid2.load({
								params:{ cboMoneda: combo.getValue(),
											txtDesde: hoy,
											txtHasta: hoy
										}
							});													
						}
						else{
							gridDolar.hide();
							fpDolar.hide();
							grid.show();
							if(combo.getValue() == '40'){								
								grid.getColumnModel().setColumnHeader(cm.findColumnIndex('Inicio'),'Fecha de Aplicaci�n<BR>Inicial (dd/mm/aaaa)');	
								grid.getColumnModel().setHidden(cm.findColumnIndex('Final'), false);
								grid.setWidth(790)
								consultaDataGrid.load({
									params:{comboMoneda: combo.getValue() } 
								});						
							}else{								
								grid.getColumnModel().setColumnHeader(cm.findColumnIndex('Inicio'),'Fecha de Aplicaci�n<BR>(dd/mm/aaaa)');	
								grid.getColumnModel().setHidden(cm.findColumnIndex('Final'), true);
								grid.setWidth(620)											
								consultaDataGrid.load({
									params:{comboMoneda: combo.getValue() }
								});							
							} 
						}// fin else 54
					}				
				} //fin select
			},
			width:170
		},{ 	
				xtype: 'datefield',  
				hidden: true, 
				id: 'fechaActual', 
				value: '' 
		}
	]
	
	
	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
		style: 'margin:0 auto;',	   
		height: 70,
		width: 410
	});
	
	
	var elementosFormaDolar =
		[	
			{
			xtype: 'compositefield',
			id: 'cf',
			combineErrors: false,
			width:400,
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'datefield',
					fieldLabel: 'Desde',
					value: new Date(),
					format:'d/m/Y',
					id: 'dateDesde',
					allowBlank: false,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',		
					margins: '0 20 0 0'
				},	{
					xtype: 'displayfield',
					value: '(dd/mm/aaaa)   ',
					width: 110
				}, {
					xtype: 'displayfield',
					value: 'Hasta :   ',
					width: 80
				},{ 
					xtype: 'datefield',
					value: new Date(),
					format:'d/m/Y',
					id: 'dateHasta',
					allowBlank: false,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',			
					margins: '0 20 0 0'
				},	{
					xtype: 'displayfield',
					value: '(dd/mm/aaaa)   ',
					width: 30
				}			
			]
		}	
	]
	
	
	var fpDolar = new Ext.form.FormPanel({
		id:'formaDolar',
		hidden: true,
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosFormaDolar,
		style: 'margin:0 auto;',	   
		//height: 70,
		width: 670
	});
	
	
	var grid= new Ext.grid.EditorGridPanel({
		store: consultaDataGrid,
		autoLoad: false,
		clicksToEdit:1,
		sortable: true,
		id:'grid',
		hidden: true,
		columns: [
			{
				header: '<center>Moneda</center>',
				dataIndex: 'NOMBREMONEDA',
				width: 260,
				align: 'left'				
			},{
				header: '<center>Valor</center>',
				dataIndex: 'VALOR',
				align: 'center',
				width: 170,
				editor: { 
					xtype:'numberfield',
					height: 10,
					maxLength: 3,
					id:'txtValorCompra'
				},
				renderer: function(value, metadata, record, rowIndex, colIndex, store){            	
					if (cvePerf != '8')
						return NE.util.colorCampoEdit(value,metadata,record);					
					else
						return value;
				}
			},{
				header: 'Fecha de Aplicaci�n <BR>(dd/mm/aaaa)',
				dataIndex: 'Inicio',
				align: 'center',
				width: 170,
				editor: { 
					xtype: 'datefield',
					name: 'txtFechaAplic',
					//format:'d/m/Y',
					id: 'txtFechInicio',
					allowBlank: false,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',			
					margins: '0 20 0 0'
				},
				renderer: function(value, metadata, record, rowIndex, colIndex, store){            	
					if (cvePerf != '8')
						return NE.util.colorCampoEdit(value,metadata,record);					
					else
						return value;
				}
			},{
				header: 'Fecha de Aplicaci�n<BR> Final (dd/mm/aaaa)',
				dataIndex: 'Final',
				align: 'center',				
				hidden:true,
				width: 170,
				editor: { 
					xtype: 'datefield',
					name: 'txt_fec',
					id: 'txtFechFin',
					allowBlank: false,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 			
					margins: '0 20 0 0'
				},
				renderer: function(value, metadata, record, rowIndex, colIndex, store){            	
					if (cvePerf != '8')
						return NE.util.colorCampoEdit(value,metadata,record);					
					else
						return value;
				}
			}
		],
		loadMask: true,	
		style: 'margin:0 auto;',
		frame: true,
		height: 120,
				
		bbar: {
					autoScroll:true,
					id: 'barra',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnAceptar',
						text:		'Aceptar',
						iconCls: 'icoAceptar',
						disabled: false,
						width: 50,
						weight: 70,
						style: 'margin:0 auto;',
						handler: function (boton,evento){
							procesarAceptar();
						},
							align:'rigth'
					},
					{
						xtype:	'button',
						id:		'btnBorrar',
						text:		'Borrar',
						iconCls: 'borrar',
						disabled: false,
						width: 50,
						handler: function(boton, evento) {
								Ext.Msg.confirm('Mensaje', 
											'�Est� seguro que desea borrar el tipo cambio del d�a de ahora ('+fechaBorrar+')?',
											function(botonConf) {
												if (botonConf == 'ok' || botonConf == 'yes' || botonConf == 'si' ) {
																procesarBorrar();									
												}
											}
										);
						},						
						weight: 70,
						style: 'margin:0 auto;',
						align:'rigth'
					},{	
						xtype:	'button',
						id:		'btnConsultar',
						text:		'Consultar',
						iconCls: 'icoLupa',
						disabled: false,
						width: 50,
						weight: 70,
						style: 'margin:0 auto;',
						handler: function (boton,evento){
							procesarConsultar();
						},
							align:'rigth'
					}
			]
		},	
      width: 620,
		listeners:{
			afteredit : function(e){
							var record = e.record;
							var gridEditable = e.gridEditable; 
							var campo= e.field;        
                        if(campo=='Inicio') { 
								  var fecha =Ext.util.Format.date(record.data['Inicio'],'d/m/Y'); 
								  fechaBorrar = fecha;
								  record.data['Inicio'] = fecha;             
								  record.commit();            
							 }else if (campo == 'Final'){
								  var fecha =Ext.util.Format.date(record.data['Final'],'d/m/Y'); 
								  record.data['Final'] = fecha;             
								  record.commit();
							 }
            },
			beforeedit: function(object){
				if ((object.field == 'VALOR' || object.field == 'Inicio' || object.field == 'Final') && cvePerf == '8'){
					object.cancel = true;
				}
			}//finbefore		 
		}		
	});	
	
			
	var gridDolar= new Ext.grid.GridPanel({
		store: consultaDataGrid2,
		autoLoad: false,
		sortable: true,
		title: 'Titulo',
		id:'gridDol',
		hidden: true,
		columns: [
			{
				header: 'Fecha de aplicaci�n (dd/mm/aaaa)',
				dataIndex: 'FECHA',
				align: 'center',
				width: 250
			},
			{
				header: '<center>Valor</center>',
				dataIndex: 'VALORCOMPRA',
				align: 'center',
				width: 150

			}
		],
		loadMask: true,	
		style: 'margin:0 auto;',
		frame: true,
		height: 120,
				
		bbar: {
					autoScroll:true,
					id: 'barra2',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnAceptar2',
						text:		'Aceptar',
						iconCls: 'icoAceptar',
						disabled: true,
						width: 50,
						weight: 70,
						style: 'margin:0 auto;',
						handler: function (boton,evento){
							if(Ext.getCmp('idComboMoneda').getValue() == ''){
								//Ext.MessageBox.alert("Alerta",'Favor de seleccionar una moneda');	
								Ext.getCmp('idComboMoneda').markInvalid('Favor de seleccionar una moneda');
							}else{
								boton.disable();
								procesarAceptar2();
							}						
						},
							align:'rigth'
					},
					{
						xtype:	'button',
						id:		'btnRegresar',
						text:		'Regresar',
						iconCls: 'icoContinuar',				
						width: 50,
						handler: function(boton, evento) {
							window.location = '15admnafparamtcambioExt.jsp';						
						},						
						weight: 70,
						style: 'margin:0 auto;',
						align:'rigth'
					}
				]
			},	
      width: 420	
	});			
					
			
//---Contenedor Principal-----
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [ fp,NE.util.getEspaciador(10),grid,
		NE.util.getEspaciador(10), fpDolar, NE.util.getEspaciador(10),
		gridDolar ]
	});

	CatalogoMoneda.load()
	
		Ext.Ajax.request({
		url: '15admnafparamtcambioExt.data.jsp',
		params: {
			informacion: 'ConsultaPerfil'
		},
		callback: procesaPerfil
	});	
	
	//fpDolar.setVisible(false);
	
});