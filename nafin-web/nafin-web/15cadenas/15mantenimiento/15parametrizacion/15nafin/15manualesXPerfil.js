Ext.onReady(function() {


	//Elimina el registro del manual 
	var procesarSuccessFailureBorrar =  function(opts, success, response) {		
		var grid = Ext.getCmp('grid');
		grid.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			//grid.getStore().commitChanges();
			Ext.MessageBox.alert('Mensaje','El registr� se borr� exitosamente.');
			consultaData.load();						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	//Elimina el registro del manual 
	var procesaAccionBorrar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_manual = registro.get('IC_MANUAL');
		
		var grid = Ext.getCmp('grid');
		var store = grid.getStore();
		
			grid.el.mask('Actualizando...', 'x-mask-loading');
			grid.stopEditing();
			
			Ext.Ajax.request({
				url : '15manualesXPerfil.data.jsp',
				params : {
					informacion: 'Eliminar',
					ic_manual:ic_manual							
				},
				callback: procesarSuccessFailureBorrar
			});				
	}
	
		
	//descargar el  Manual
	var procesarSuccessFailureManual =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	var descargaArchivo = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_manual = registro.get('IC_MANUAL');
	
		Ext.Ajax.request({
			url: '15manualesXPerfil.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'VerManual',
				ic_manual: ic_manual
			}),
			callback: procesarSuccessFailureManual
		});
	}
	
	
		var procesarSuccessFailureGuardar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var datos = Ext.util.JSON.decode(response.responseText);
			var ic_manual =datos.ic_manual;	
			var fp = Ext.getCmp('forma');		
			var forma = fp.getForm();
			var parametros = "ic_manual="+ic_manual;
			if(	ic_manual !=''){					
				forma.submit({
					url: '15manualesXPerfil.ma.jsp?'+parametros,
					waitMsg: 'Enviando datos...',
					success: function(form, action) {					
						document.location.href = "15manualesXPerfil.jsp";					
				},
				failure: NE.util.mostrarSubmitError
				});	
			}	
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//generar PDF con los datos  de la consulta
	var procesarSuccessFailureGenerarPDF = function(opts,success,response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if(success = true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton, evento){
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//consulta para mostrar los manuales
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
	
	var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}		
		}		
		var btnBajarPDF = Ext.getCmp('btnBajarPDF');
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		var el = grid.getGridEl();						
		if(store.getTotalCount() > 0) {
			btnGenerarPDF.enable();
			btnBajarPDF.hide();
			el.unmask();		
		}else{
			btnGenerarPDF.disable();
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
		
	}
	
	
	
	var myValidFn = function(v) {
		var myRegex = /^.+\.([pP][dD][fF])$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivopdf 		: myValidFn,
		archivopdfText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): PDF.'
	});
	
 //guardar el registro del Manual 
	var Guardar = function() {		
		var perfil = Ext.getCmp("perfil1");
		if (Ext.isEmpty(perfil.getValue()) ) {
			perfil.markInvalid('El valor del perfil es requerido.');	
			return;
		}
		
		var nomManual = Ext.getCmp("nomManual");
		if (Ext.isEmpty(nomManual.getValue()) ) {
			nomManual.markInvalid('El valor del Nombre del Manual es requerido.');	
			return;
		}
		
		var archivo = Ext.getCmp("archivo");
			if (Ext.isEmpty(archivo.getValue()) ) {
				archivo.markInvalid('El valor de la Ruta del Archivo es requerido.');	
				return;
			}
			
			Ext.Ajax.request({
				url : '15manualesXPerfil.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion:'Guardar'			
				}),
				callback: procesarSuccessFailureGuardar
			});	
	}
	
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPOStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15manualesXPerfil.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	 var catalogoPerfil = new Ext.data.JsonStore({
		id: 'catalogoPerfilStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15manualesXPerfil.data.jsp',
		baseParams: {
			informacion: 'catalogoPerfil'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'clave_epo',
			id: 'clave_epo1',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_epo',
			emptyText: 'Seleccione...',
			autoSelect :true,
			width: 200,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'perfil',
			id: 'perfil1',
			fieldLabel: 'Perfil',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'perfil',
			emptyText: 'Seleccione...',
			autoSelect :true,
			width: 200,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoPerfil,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'textfield',
			name: 'nomManual',
			id: 'nomManual',
			fieldLabel: 'Nombre Manual',
			allowBlank: true,
			hidden: false,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,			
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'descripcion',
			id: 'descripcion',
			fieldLabel: 'Descripci�n',
			allowBlank: true,
			hidden: false,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,			
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
		  xtype: 'fileuploadfield',
      id: 'archivo',
			width: 300,	  
      emptyText: 'Ruta del Archivo',
      fieldLabel: 'Ruta del Archivo',
      name: 'archivoCesion',
      buttonText: 'Examinar...',
      buttonCfg: {
			  iconCls: 'upload-icon'
      },
	  anchor: '95%',
	  vtype: 'archivopdf'
    }	
	];
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15manualesXPerfil.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'IC_MANUAL'},
			{name: 'PERFIL'},
			{name: 'NOMBRE_MANUAL'},
			{name: 'DESCRIPCION_MANUAL'},
			{name: 'NOMBRE_EPO'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	
		
	var grid = new Ext.grid.EditorGridPanel({
		id: 'grid',
		store: consultaData,
		hidden: false,
		margins: '20 0 0 0',
		align: 'center',
		style: 'margin:0 auto;',
		title: 'Consulta de Manuales por Perfil y EPO',
		columns: [
			{
				header: 'Perfil', 
				tooltip: 'Perfil',
				dataIndex: 'PERFIL',
				sortable: true,
				resizable: true	,
				width: 150,			
				align: 'left'
			},
			{
				header: 'Nombre del Manual', 
				tooltip: 'Nombre del Manual',
				dataIndex: 'NOMBRE_MANUAL',
				sortable: true,
				resizable: true	,
				width: 150,			
				align: 'left'
			},
			{
				header: 'Descripci�n', 
				tooltip: 'Descripci�n',
				dataIndex: 'DESCRIPCION_MANUAL',
				sortable: true,
				resizable: true	,
				width: 150,			
				align: 'left'
			},
			{
				header: 'Nombre EPO', 
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 150,			
				align: 'left'
			},
			{
	      xtype: 'actioncolumn',
				header: 'Manual',
				tooltip: 'Manual',
        width: 100,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	
						handler: descargaArchivo
					}
				]				
			},
			{
	      xtype: 'actioncolumn',
				header: 'Eliminar',
				tooltip: 'Eliminar',
        width: 100,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Eliminar';
							return 'borrar';										
						},	
						handler: procesaAccionBorrar
					}
				]				
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 820,		
		frame: true,
		bbar: {
		items: [
			'->',
			'-',
			{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnGenerarPDF',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');					
					Ext.Ajax.request({
						url: '15manualesXPerfil.data.jsp',
						params: {
							informacion: 'ArchivoPDF'							
						},
						callback: procesarSuccessFailureGenerarPDF
					});
				}
			},
			{
				xtype: 'button',
				text: 'Bajar PDF',
				id: 'btnBajarPDF',
				hidden: true
			},
			'-'
		]
	}
	});
	
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Manuales por Perfil y EPO ',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid: true,
		buttons: [
			{
				text: 'Guardar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: Guardar
			},			
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					document.location.href  = "15manualesXPerfil.jsp";
				}
			}
		]
	});
	
		
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	});
	
	
	catalogoEPO.load();
	catalogoPerfil.load();
	consultaData.load();
});