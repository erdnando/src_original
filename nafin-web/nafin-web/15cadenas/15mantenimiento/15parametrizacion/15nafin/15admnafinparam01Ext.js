
Ext.onReady(function() {

	//var tabActiva = Ext.get('tabActiva').getValue();

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//----------------------- VALIDACIONES --------------------------

    Ext.apply(Ext.form.VTypes, {
         // This function validates digito verificador
        digVerificador:  function(contenido) {

				var car = "";
				var resultado = "";
				var residuo = 0;    
				var mulIndividual = 0;
				var digVerificador = 0;  
				var contenedor = 0;
				var cadena="";
				cadena=contenido;

				if (cadena != ""){
			
					digVerificador = parseInt(cadena.substr(17));
					
					for (i=0; i <= 16; i++){
						car = cadena.substring(i,i+1);
						switch(i){
							case 0: case 3: case 6: case 9: case 12: case 15:			/*3*/
								mulIndividual = parseInt(car)*3;
								contenedor += mulIndividual;
								break;
							case 1: case 4: case 7: case 10: case 13: case 16:			/*7*/
								mulIndividual = parseInt(car)*7;
								contenedor += mulIndividual;
								break;
							case 2: case 5: case 8: case 11: case 14:					/*1*/
								mulIndividual = parseInt(car)*1;
								contenedor += mulIndividual;
								break;
						}
					}
				
					residuo = contenedor % 10;  /*Obtiene el residuo */
					
					if (residuo > 0)  
						contenedor = 10 - residuo;
					else
						contenedor = 0;
					
					if (digVerificador != contenedor){
						return false;
					}
				}
				return true;
        },
		  digVerificadorText:	'La Cuenta clabe no es correcta (D�gito Verificador).'
    });


	Ext.apply(Ext.form.VTypes, {
		// comfirmar d�gito Verificador 
        confDigVerificador:  function(contenido) {
			if ( Ext.getCmp("_cboMoneda").getValue()==1)  {
				if ( Ext.getCmp("_confirtxtCuentaClabe").getValue()  !=Ext.getCmp("_txtCuentaClabe").getValue()   ){						
					return false;
				}
			}
			return true;
        },
		  confDigVerificadorText:	'La confirmaci�n de la Cuenta CLABE no es igual.'
    });




//-  - - - - - - - - - - - Variable global

	var varGlobal = {cvePerf:null}

//- - - - - - - - - - - - FUNCTIONS�S- - - - - - - - - - - - - - - - - - - - - -

	function submitCuentaDistribuidores(action,nocta,numele){
		//f.txtAction.value=action;
		if(	Ext.isEmpty(	Ext.getCmp("dis_ic_epo").getValue()	)	){
			Ext.getCmp("dis_ic_epo").markInvalid('Favor de seleccionar una EPO');
			Ext.getCmp("dis_ic_epo").focus();
			return;
		}
		if(	Ext.isEmpty(	numele	)	){
			Ext.getCmp("dis_txtNE").markInvalid('Favor de capturar el N�m. NE PYME');
			Ext.getCmp("dis_txtNE").focus();
			return;
		}
		if(	Ext.isEmpty(Ext.getCmp("dis_txtBanco").getValue())	&& Ext.isEmpty(	Ext.getCmp("dis_ic_if").getValue())	)	{
			Ext.getCmp("dis_ic_if").markInvalid('Favor de seleccionar el Intermediario Financiero<br> o Banco de Servicio');
			Ext.getCmp("dis_ic_if").focus();
			return;
		}
		if(	Ext.isEmpty(	Ext.getCmp("dis_txtSucursal").getValue()	)	){
			Ext.getCmp("dis_txtSucursal").markInvalid('Favor de escribir la sucursal');
			Ext.getCmp("dis_txtSucursal").focus();
			return;
		}
		if(	Ext.isEmpty(	Ext.getCmp("dis_cboMoneda").getValue()	)	)	{
			Ext.getCmp("dis_cboMoneda").markInvalid('Favor de seleccionar una moneda');
			Ext.getCmp("dis_cboMoneda").focus();
			return;
		}
		if(	Ext.isEmpty(	Ext.getCmp("dis_txtCuenta").getValue()	)	)	{
			Ext.getCmp("dis_txtCuenta").markInvalid('Favor de escribir el No. de Cuenta');
			Ext.getCmp("dis_txtCuenta").focus();
			return;
		}
		if(	Ext.isEmpty(	Ext.getCmp("dis_cboPlaza").getValue()	)	)	{
			Ext.getCmp("dis_cboPlaza").markInvalid('Favor de seleccionar una plaza ');
			Ext.getCmp("dis_cboPlaza").focus();
			return;
		}
		accionArealizar("BOTON_AGREGAR_DISTRIBUIDORES",null);
	}

	function submitCuentaDescuento(action,nocta,numele){
		Ext.getCmp("aux_txtNE").setValue(numele);
		Ext.getCmp("_txtIcCtaBanco").setValue(nocta);
		Ext.getCmp("_txtAction").setValue(action);

		if(action == 4){

			if(	Ext.isEmpty(	numele	)	){
				Ext.getCmp("_txtNE").markInvalid("Favor de escribir el N�m NE PyME");
				Ext.getCmp("_txtNE").focus();
				return;
			}
			
			var respuesta = new Object();
			respuesta["txtNE"] = numele;
			accionArealizar("BOTON_CONSULTAR_DESCUENTO",respuesta);
			
		}else{

			if(action == 3){
				//Esta opci�n se reemplaza por la simulacion de estado siguiente
				//location.href = "15admnafinparam01b.jsp?txtIcCtaBanco="+nocta +"&txtNE="+numele;
				return;
			}
			if(action == 2){
				//Esta opci�n se reemplaza por la simulacion de estado siguiente
				return;
				//bConfim=confirm('Esta seguro que desea borrar esta cuenta?');
				//if(bConfim)	document.frmCtasBancarias.submit();
			}
			else{

				if(	Ext.isEmpty(	numele	)	){
					Ext.getCmp("_txtNE").markInvalid("Favor de escribir el N�m NE PyME");
					Ext.getCmp("_txtNE").focus();
					return;
				}
				if(	Ext.isEmpty(	Ext.getCmp("_txtBanco").getValue()	)	)	{
					Ext.getCmp("_txtBanco").markInvalid('Favor de seleccionar el Banco de Servicio');
					Ext.getCmp("_txtBanco").focus();
					return;
				}
				if(	Ext.isEmpty(	Ext.getCmp("_txtSucursal").getValue()	)	){
					Ext.getCmp("_txtSucursal").markInvalid('Favor de escribir la sucursal');
					Ext.getCmp("_txtSucursal").focus();
					return;
				}
				if(	Ext.isEmpty(	Ext.getCmp("_txtCuenta").getValue()	)	)	{
					Ext.getCmp("_txtCuenta").markInvalid('Favor de escribir el No. de Cuenta');
					Ext.getCmp("_txtCuenta").focus();
					return;
				}
				
				if ( Ext.getCmp("_cboMoneda").getValue()==1)  {
					if( Ext.isEmpty(	Ext.getCmp("_confirtxtCuentaClabe").getValue()	)	)	{
						Ext.getCmp("_confirtxtCuentaClabe").markInvalid('Favor de escribir la confirmaci�n de la Cuenta Clabe ');
						Ext.getCmp("_confirtxtCuentaClabe").focus();
						return;
					}				
				}
				accionArealizar("BOTON_AGREGAR_DESCUENTO",null);
			}
		}
	}

	function submitCuentaDescuentoActualiza(action,nocta){
		Ext.getCmp("_txtAction").setValue(action);

		if(	Ext.isEmpty(	Ext.getCmp("_txtBanco").getValue()	)	)	{
			Ext.getCmp("_txtBanco").markInvalid('Favor de seleccionar el Banco de Servicio');
			Ext.getCmp("_txtBanco").focus();
			return;
		}
		if(	Ext.isEmpty(	Ext.getCmp("_txtSucursal").getValue()	)	){
			Ext.getCmp("_txtSucursal").markInvalid('Favor de escribir la sucursal');
			Ext.getCmp("_txtSucursal").focus();
			return;
		}
		if(	Ext.isEmpty(	Ext.getCmp("_txtCuenta").getValue()	)	)	{
			Ext.getCmp("_txtCuenta").markInvalid('Favor de escribir el No. de Cuenta');
			Ext.getCmp("_txtCuenta").focus();
			return;
		}
		if(	Ext.isEmpty(	Ext.getCmp("_txtNE").getValue()	)	){
			Ext.getCmp("_txtNE").markInvalid("Favor de escribir el N�m NE PyME");
			Ext.getCmp("_txtNE").focus();
			return;
		}

		accionArealizar("BOTON_ACTUALIZAR_DESCUENTO",null);
	}

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -

	var procesarConsultaDistribuidores = function(store, registros, opts){

		var tabP = Ext.getCmp('tp');
		tabP.el.unmask();

		if (registros != null) {

			var grid  = Ext.getCmp('gridDistribuidores');

			if (!grid.isVisible()) {
				grid.show();
			}

			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: '' 
				}
			);
		}
	}

	var procesaCatalogoEpo = function(store, arrRegistros, opts) {
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione una EPO',	loadMsg: null	})	);
			Ext.getCmp('dis_ic_epo').setValue("");
	}

	var procesaCatIF = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.ic_if_tipo1 != undefined){
			var comboBanco = Ext.getCmp('dis_txtBanco');
			comboBanco.setValue('');
			comboBanco.store.removeAll();
			comboBanco.store.load({
				params: {
					ic_if_tipo1:	jsonData.ic_if_tipo1
				}
			});
		}
	}
//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 

	var catalogoEpoData = new Ext.data.JsonStore({
		id:				'catalogoEpoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admnafinparam01Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.Epo'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			load:	procesaCatalogoEpo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoIfData = new Ext.data.JsonStore({
		id:				'catalogoIfDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg', 'ic_financiera'],
		url:				'15admnafinparam01Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.IF'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			load:			procesaCatIF,
			exception:	NE.util.mostrarDataProxyError,
			beforeload:	NE.util.initMensajeCargaCombo
		}
	});

	var catalogoBancoDisData = new Ext.data.JsonStore({
		id:				'catalogoBancoDisDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admnafinparam01Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.Banco.Servicio.distribuidores'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoBancoData = new Ext.data.JsonStore({
		id:				'catalogoBancoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admnafinparam01Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.Banco.Servicio'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoBancoTefData = new Ext.data.JsonStore({
		id:				'catalogoBancoTefDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admnafinparam01Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.CatalogoBanco_tef'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	

	var catalogoMonedaData = new Ext.data.JsonStore({
		id:				'catalogoMonedaDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admnafinparam01Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.Moneda'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoPlazaData = new Ext.data.JsonStore({
		id:				'catalogoPlazaDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admnafinparam01Ext.data.jsp',
		baseParams:		{	informacion:	'Catalogo.Plaza'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var registrosDescuentoData = new Ext.data.JsonStore({
		fields: [
			{name: 'CG_BANCO'				},
			{name: 'CG_SUCURSAL'			},
			{name: 'CD_NOMBRE'			},
			{name: 'CG_NUMERO_CUENTA'	},
			{name: 'IC_CUENTA_BANCARIA'},		//, 	convert: NE.util.string2boolean 
			{name: 'PLAZA'					},
			{name: 'CG_CUENTA_CLABE'	},
			{name: 'CS_CONVENIO_UNICO'	},
			{name: 'IC_PYME'	}
		],
		data:	[
			{'CG_BANCO':''}
		],
		autoLoad:	false,
		listeners:	{exception: NE.util.mostrarDataProxyError}
	});

	var registrosDistribuidoresData = new Ext.data.JsonStore({
		root: 		'registros',
		id:			'registrosDistribuidoresDataStore',
		url: 			'15admnafinparam01Ext.data.jsp',
		baseParams: {	informacion:	'Distribuidores.Consulta'	},
		fields: [
			{ name: 'NOMEPO'},
			{ name: 'NOMIF'},
			{ name: 'NOMFIN'},
			{ name: 'NOMPYME'},
			{ name: 'RFC'},
			{ name: 'SUCURSAL'},
			{ name: 'MONEDA'},
			{ name: 'NOCTA'},
			{ name: 'PLAZA'}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarConsultaDistribuidores,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDistribuidores(null, null, null);
				}
			}
		}
	});

//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - - 

	var procesaAccion = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionArealizar(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionArealizar(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionArealizar = function(estadoSiguiente, respuesta){

		if(  estadoSiguiente == "parametrizacion.getClavePerfil"						){

			Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url: '15admnafinparam01Ext.data.jsp',
				params:	{	informacion:	'getClavePerfil'},
				callback: procesaAccion
			});
		
		} else if(	estadoSiguiente == "RESPUESTA_CLAVE_PERFIL"						){

			varGlobal.cvePerf = respuesta.cvePerf;
			if(varGlobal.cvePerf == "8"){
				tp.hideTabStripItem(1);
				Ext.getCmp('desc.btnAgregar').hide();
				Ext.getCmp('desc.btnCancelar').hide();
			}else{
				Ext.StoreMgr.key('catalogoEpoDataStore').load();
				Ext.StoreMgr.key('catalogoBancoDisDataStore').load();
			}
		
		} else if(	estadoSiguiente == "RESPUESTA_VALIDA_DISPERSION" ){
		
			if(respuesta.eposDispersion =='S') {
				Ext.MessageBox.confirm('Mensaje','�Desea registrar la cuenta para Dispersi�n?', 
				function showResult(btn){
					if (btn == 'yes') {
					
						gridDescuento.hide();
					
						Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');
						Ext.Ajax.request({
							url:		'15admnafinparam01Ext.data.jsp',
							params:	Ext.apply(fpDescuento.getForm().getValues(),		
							{
								informacion:	'Descuento.ConsEposDispersion',
								txtNE: respuesta.txtNE
								
							}),
							callback: procesaAccion
						});			
					}else  {
					
						Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');
						Ext.Ajax.request({
							url:		'15admnafinparam01Ext.data.jsp',
							params:	Ext.apply(fpDescuento.getForm().getValues(),	{
								informacion:	'Descuento.Consulta'							
							}),
							callback: procesaAccion 
						});		 			
					
					}
					
				});
			}else  {
			
				Ext.Msg.alert("Mensaje de p�gina web", respuesta.strMensaje,
					function(){						
						window.location = '15admnafinparam01Ext.jsp';
					}
				)
				
			}
			
		} else if(	estadoSiguiente == "RESPUESTA_CONS_EPO_DISPERSION"){
			
			Ext.getCmp('formaDescuento').hide();
			Ext.getCmp('fpDispEpo').show();
			
			Ext.getCmp('lblTipo').update(respuesta.lblTipo);
			Ext.getCmp('lblProducto').update(respuesta.lblProducto);
			Ext.getCmp('lblnaPyme').update(respuesta.lblnaPyme);
			Ext.getCmp('lblNombrePyme').update(respuesta.lblNombrePyme);
			Ext.getCmp('lblRFCPyme').update(respuesta.lblRFCPyme);
			Ext.getCmp('lblMoneda').update(respuesta.lblMoneda);
			Ext.getCmp('lblCuentaClabe').update(respuesta.lblCuentaClabe);			
			
			if(respuesta.cboMoneda ==1){
				Ext.getCmp('lblCuentaClabe').show();
				Ext.getCmp('_ic_bancos_tef').show();
				
				Ext.getCmp('_cg_CuentaDips').hide();
				Ext.getCmp('_txtPlazaDisp').hide();
				Ext.getCmp('_txtSucursalDisp').hide();
				Ext.getCmp('_ic_Swift').hide();
				Ext.getCmp('_ic_aba').hide();			
				
			}else  if(respuesta.cboMoneda ==54){
				Ext.getCmp('lblCuentaClabe').hide();
				Ext.getCmp('_ic_bancos_tef').show();
				
				Ext.getCmp('_cg_CuentaDips').show();
				Ext.getCmp('_txtPlazaDisp').show();
				Ext.getCmp('_txtSucursalDisp').show();
				Ext.getCmp('_ic_Swift').show();
				Ext.getCmp('_ic_aba').show();			
			}
			
			if(!gridDescuentoDE.isVisible()){
				gridDescuentoDE.show();
         }
         
			var el = gridDescuentoDE.getGridEl();		
			var cm = gridDescuentoDE.getColumnModel();
				
			if(respuesta.registros.length > 0){
				regDescuentoDEData.loadData(respuesta.registros);				
				
				if(respuesta.cboMoneda==1 )  {
					if(respuesta.totalCuentas!=0){
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('BANCO'), false);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_CLABE'), false);						
					}else  {
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('BANCO'), true);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_CLABE'), true);					
					}				
				}else  if(respuesta.cboMoneda==54 )  {
					
					gridDescuentoDE.getColumnModel().setColumnHeader(cm.findColumnIndex('CUENTA_CLABE'),'Cuenta');
				
					if(respuesta.totalCuentas!=0){
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('BANCO'), false);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_CLABE'), false);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('PLAZA'), false);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL'), false);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('IC_SWIFT'), false);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('IC_ABA'), false);	
					}else {
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('BANCO'), true);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_CLABE'), true);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('PLAZA'), true);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL'), true);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('IC_SWIFT'), true);	
						gridDescuentoDE.getColumnModel().setHidden(cm.findColumnIndex('IC_ABA'), true);						
					}
				}				
				
            el.unmask();
         }else{
				el.mask('No se Encontro Ning&uacute;n Registro', 'x-mask');
         }
		
		} else if(	estadoSiguiente == "BOTON_ACEPTAR_DISP_EPO"	){
		
			gridDescuentoDE = Ext.getCmp('gridDescuentoDE');
			var ic_bancos_tef = Ext.getCmp('_ic_bancos_tef');
					
			var  cg_CuentaDips = Ext.getCmp('_cg_CuentaDips');
			var  txtPlazaDisp = Ext.getCmp('_txtPlazaDisp');
			var  txtSucursalDisp = Ext.getCmp('_txtSucursalDisp');
			var  ic_Swift = Ext.getCmp('_ic_Swift');
			var  ic_aba = Ext.getCmp('_ic_aba');
			
			
			if(	Ext.isEmpty(ic_bancos_tef.getValue()	)	)	{
				Ext.getCmp("_ic_bancos_tef").markInvalid('Favor de seleccionar el banco ');
				Ext.getCmp("_ic_bancos_tef").focus();
				return;
			}
			
			if(Ext.getCmp('_cboMoneda').getValue()==54)  {
				if(	Ext.isEmpty(cg_CuentaDips.getValue()	)	)	{
					Ext.getCmp("_cg_CuentaDips").markInvalid('Favor de escribir la Cuenta ');
					Ext.getCmp("_cg_CuentaDips").focus();
					return;
				}
				
				if(	Ext.isEmpty(txtPlazaDisp.getValue()	)	)	{
					Ext.getCmp("_txtPlazaDisp").markInvalid('Favor de escribir la Plaza ');
					Ext.getCmp("_txtPlazaDisp").focus();
					return;
				}
				
				if(	Ext.isEmpty(txtSucursalDisp.getValue()	)	)	{
					Ext.getCmp("_txtSucursalDisp").markInvalid('Favor de escribir la Sucursal ');
					Ext.getCmp("_txtSucursalDisp").focus();
					return;
				}
				if(	Ext.isEmpty(ic_Swift.getValue()	)	)	{
					Ext.getCmp("_ic_Swift").markInvalid('Favor de escribir la Swift ');
					Ext.getCmp("_ic_Swift").focus();
					return;
				}		
			}
			
			var store = gridDescuentoDE.getStore();
			var totalEpos =0;			
			var totalReg = 0;
			var ic_epos = [];
			var ic_cuentas = [];		
			
			store.each(function(record) {
				var numRegistro = store.indexOf(record);			
				if(record.data['SELECCION']=='S' ){
					ic_epos.push(record.data['IC_EPO']);
					ic_cuentas.push(record.data['IC_CUENTA']);				
					totalReg++;						
					if(record.data['CUENTA_CLABE']!='' ) {
						totalEpos++;
					}
				}
			});
			
			if(totalReg==0) {
				Ext.MessageBox.alert('Mensaje','Debe seleccionar al menos un registro para continuar');
				return false;		
			
			}else  {
			
				if(totalEpos>0) {
					Ext.MessageBox.confirm('Mensaje','Existen '+totalEpos+' Cadenas Productivas ya parametrizadas que se modificar�n �Desea Continuar?', 
						function showResult(btn){
							if (btn == 'yes') {
								Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');
									Ext.Ajax.request({
										url:		'15admnafinparam01Ext.data.jsp',
										params:	Ext.apply(fpDescuento.getForm().getValues(),
										{
											informacion:	'Descuento.Agregar_Epos',
											ic_epos:ic_epos,
											ic_cuentas:ic_cuentas,											
											ic_bancos_tef:ic_bancos_tef.getValue(),
											cg_CuentaDips:cg_CuentaDips.getValue(),
											txtPlazaDisp:txtPlazaDisp.getValue(),
											txtSucursalDisp:txtSucursalDisp.getValue(),											
											ic_Swift:ic_Swift.getValue(),
											ic_aba:ic_aba.getValue()
										}
								),
								callback: procesaAccion
							});						
						}
					});
					
				}else  {
				
					Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url:		'15admnafinparam01Ext.data.jsp',
						params:	Ext.apply(fpDescuento.getForm().getValues(),
							{
								informacion:	'Descuento.Agregar_Epos',
								ic_epos:ic_epos,
								ic_cuentas:ic_cuentas,								
								ic_bancos_tef:ic_bancos_tef.getValue(),								
								cg_CuentaDips:cg_CuentaDips.getValue(),
								txtPlazaDisp:txtPlazaDisp.getValue(),
								txtSucursalDisp:txtSucursalDisp.getValue(),											
								ic_Swift:ic_Swift.getValue(),
								ic_aba:ic_aba.getValue()
							}
						),
						callback: procesaAccion
					});				
				}
			}
		} else if(	estadoSiguiente == "RESPUESTA_GUARDAR_EPO"	){	
		
			Ext.Msg.alert("Mensaje de p�gina web", respuesta.strMensaje,
					function(){						
						window.location = '15admnafinparam01Ext.jsp';
					}
				);		
			
		} else if(	estadoSiguiente == "BOTON_AGREGAR_DESCUENTO"	){

			Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url:		'15admnafinparam01Ext.data.jsp',
				params:	Ext.apply(fpDescuento.getForm().getValues(),
							{
								informacion:	'Descuento.Agregar'
							}
				),
				callback: procesaAccion
			});
			
		} else if(	estadoSiguiente == "RESPUESTA_AGREGAR_DESCUENTO"			){
		
			Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');

				Ext.Ajax.request({
				url:		'15admnafinparam01Ext.data.jsp',
				params:	Ext.apply(fpDescuento.getForm().getValues(),
					{
						informacion:	'Descuento.ValidaDispersion',
						strMensaje:respuesta.strMensaje
					}
				),
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "BOTON_CONSULTAR_DESCUENTO"					){

			Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url:		'15admnafinparam01Ext.data.jsp',
				params:	{	informacion:	'Descuento.Consulta',
								txtNE:			respuesta.txtNE
				},
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA_CONSULTAR_DESCUENTO"				){

			if(respuesta.sTraeLineas == "N"){

				if(!gridDescuento.isVisible()){
					gridDescuento.show();
				}
				registrosDescuentoData.removeAll();
				var el = gridDescuento.getGridEl();
				el.mask('N�mero PYME inexistente', 'x-mask');

			}else if(respuesta.sTraeLineas == "S"){

				if(!gridDescuento.isVisible()){
					gridDescuento.show();
				}
				var el = gridDescuento.getGridEl();

				if(respuesta.registros.length > 0){
					registrosDescuentoData.loadData(respuesta.registros);
					el.unmask();
				}else{
					el.mask('No se Encontro Ning&uacute;n Registro', 'x-mask');
				}
			}

		} else if(	estadoSiguiente == "BOTON_CANCELAR_DESCUENTO"					){

			Ext.getCmp('formaDescuento').getForm().reset();
			gridDescuento.hide();

		} else if(	estadoSiguiente == "BOTON_MODIFICAR_DESCUENTO"					){

			Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url:		'15admnafinparam01Ext.data.jsp',
				params:	{	informacion:	'Descuento.Modificar',
								clavePyme:		respuesta.clavePyme,
								txtNE:			respuesta.txtNE,
								txtIcCtaBanco:	respuesta.ctaBancaria
				},
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA_MODIFICAR_DESCUENTO"				){

			//LLenamos el formulario con la respuesta de los datos a modificar...
			if (respuesta.sTraeLineas == "N"){

				tp.hideTabStripItem(0);
				tp.hideTabStripItem(1);

				Ext.getCmp('_txtNE').setValue(respuesta.txtNE);
				Ext.getCmp('_txtBanco').setValue(respuesta.cgBanco);
				Ext.getCmp('_txtSucursal').setValue(respuesta.cgSucursal);
				Ext.getCmp('_cboMoneda').setValue(respuesta.icMoneda);
				Ext.getCmp('_txtCuenta').setValue(respuesta.cgNumeroCuenta);
				Ext.getCmp('_cboPlaza').setValue(respuesta.icPlaza);
				Ext.getCmp('_txtCuentaClabe').setValue(respuesta.cgCuentaClabe);
				Ext.getCmp('_confirtxtCuentaClabe').setValue(respuesta.cgCuentaClabe_C);

				if(respuesta.csCU == "S"){
					Ext.getCmp('_chkCU').setValue('on');
				}

				if(respuesta.plaza == "true"){
					Ext.getCmp('_txtNE').hide();
					Ext.getCmp('_txtBanco').hide();
					Ext.getCmp('_txtSucursal').hide();
					Ext.getCmp('_cboMoneda').hide();
					Ext.getCmp('_txtCuenta').hide();
				}

				if(gridDescuento.isVisible()){
					gridDescuento.hide();
				}
				Ext.getCmp('desc.btnAgregar').hide();
				Ext.getCmp('desc.btnCancelar').hide();
				Ext.getCmp('desc.btnConsultar').hide();
				Ext.getCmp('desc.btnRegresar').show();
				Ext.getCmp('desc.btnActualizar').show();

			}else{
				Ext.Msg.alert("Mensaje de p�gina web",respuesta.mensaje);
			}

		} else if(	estadoSiguiente == "BOTON_ACTUALIZAR_DESCUENTO"					){

			Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url:		'15admnafinparam01Ext.data.jsp',
				params:	Ext.apply(fpDescuento.getForm().getValues(),
							{
								informacion:	'Descuento.Actualizar'
							}
				),
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA_ACTUALIZAR_DESCUENTO"			){

			Ext.getCmp('formaDescuento').getForm().reset();
			gridDescuento.hide();
			Ext.getCmp("_txtNE").setValue(respuesta.txtNE);
			Ext.getCmp("aux_txtNE").setValue(respuesta.txtNE);
			tp.unhideTabStripItem(0);
			tp.unhideTabStripItem(1);
			Ext.getCmp('desc.btnAgregar').show();
			Ext.getCmp('desc.btnCancelar').show();
			Ext.getCmp('desc.btnConsultar').show();
			Ext.getCmp('desc.btnRegresar').hide();
			Ext.getCmp('desc.btnActualizar').hide();

			Ext.Msg.alert("Mensaje de p�gina web", respuesta.strMensaje,
				function(){
					accionArealizar("BOTON_CONSULTAR_DESCUENTO",respuesta);
				}
			);

		} else if(	estadoSiguiente == "BOTON_ELIMINAR_DESCUENTO"					){

			Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url:		'15admnafinparam01Ext.data.jsp',
				params:	{	informacion:	'Descuento.Eliminar',
								clavePyme:		respuesta.clavePyme,
								txtNE:			respuesta.txtNE,
								txtIcCtaBanco:	respuesta.ctaBancaria
				},
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA_ELIMINAR_DESCUENTO"				){

			Ext.getCmp('formaDescuento').getForm().reset();
			gridDescuento.hide();
			Ext.getCmp("_txtNE").setValue(respuesta.txtNE);
			Ext.getCmp("aux_txtNE").setValue(respuesta.txtNE);
			Ext.Msg.alert("Mensaje de p�gina web", respuesta.strMensaje,
				function(){
					accionArealizar("BOTON_CONSULTAR_DESCUENTO",respuesta);
				}
			);

		} else if(	estadoSiguiente == "BOTON_AGREGAR_DISTRIBUIDORES"				){
			
			Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url:		'15admnafinparam01Ext.data.jsp',
				params:	Ext.apply(fpDistribuidores.getForm().getValues(),
							{
								informacion:	'Distribuidores.Agregar'
							}
				),
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA_AGREGAR_DISTRIBUIDORES"			){

			Ext.getCmp('_CtasCapturadas').setValue(respuesta.CtasCapturadas);
			Ext.Msg.alert("Mensaje de p�gina web", respuesta.strMensaje,
				function(){
					if(respuesta.existeError == "false"){
						Ext.getCmp('formaDescuento').getForm().reset();
					}
					accionArealizar("BOTON_CONSULTAR_DISTRIBUIDORES",null);
				}
			);

		} else if(	estadoSiguiente == "BOTON_LIMPIAR_DISTRIBUIDORES"				){

			//window.location = "15admnafinparam01Ext.jsp?tabActiva=1";
			var cboIf = Ext.getCmp('dis_ic_if');
			cboIf.setValue('');
			cboIf.setRawValue('');
			cboIf.store.removeAll();
			var comboBanco = Ext.getCmp('dis_txtBanco');
			comboBanco.setValue('');
			comboBanco.store.removeAll();
			comboBanco.store.load({
				params: {
					ic_if_tipo1:	null
				}
			});
			comboBanco.show();

			gridDistribuidores.hide();
			Ext.getCmp('formaDistribuidores').getForm().reset();

		} else if(	estadoSiguiente == "BOTON_CONSULTAR_DISTRIBUIDORES"			){

			gridDistribuidores.hide();
			Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');

			Ext.StoreMgr.key('registrosDistribuidoresDataStore').load({
				params:	Ext.apply(fpDistribuidores.getForm().getValues(),
							{
								informacion: 'Distribuidores.Consulta',
								operacion: 'Generar',
								start: 0,
								limit: 15
							}
				)
			});

		} else if(	estadoSiguiente == "FIN"												){

			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "15admnafinparam01Ext.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}

//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

	var gridDistribuidores = new Ext.grid.GridPanel({
		store: 		registrosDistribuidoresData,
		id:			'gridDistribuidores',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		margins:		'20 0 0 0',
		title:		undefined,
		//view:			new Ext.grid.GridView({forceFit:	true}),
		stripeRows: true,
		loadMask: 	true,
		height: 		400,
		width: 		930,
		frame: 		true,
		columns: [
			{
				header: 		'EPO',
				tooltip: 	'EPO',
				dataIndex: 	'NOMEPO',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'IF',
				tooltip: 	'IF',
				dataIndex: 	'NOMIF',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'Banco de servicio',
				tooltip: 	'Banco de servicio',
				dataIndex: 	'NOMFIN',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'Distribuidor',
				tooltip: 	'Distribuidor',
				dataIndex: 	'NOMPYME',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
         },{
				header: 		'Sucursal',
				tooltip: 	'Sucursal',
				dataIndex: 	'SUCURSAL',
				sortable: 	true,
				resizable: 	true,
				width: 		70,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
         },{
				header: 		'RFC',
				tooltip: 	'RFC',
				dataIndex: 	'RFC',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
         },{
				header: 		'Moneda',
				tooltip: 	'Moneda',
				dataIndex: 	'MONEDA',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{					
				header: 		'No. de Cuenta',
				tooltip: 	'No. de Cuenta',
				dataIndex: 	'NOCTA',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
         },{
				header: 		'Plaza',
				tooltip: 	'Plaza',
				dataIndex: 	'PLAZA',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			}
		],
		bbar: {
			xtype:		'paging',
			pageSize:	15,
			buttonAlign:'left',
			id:			'barraPaginacion',
			displayInfo:true,
			store:		registrosDistribuidoresData,
			displayMsg:	'{0} - {1} de {2}',
			emptyMsg:	"No hay registros."
		}
	});

	var elementosDistribuidores = [
		{
			xtype:			'combo',
			id:				'dis_ic_epo',
			name:				'ic_epo',
			hiddenName:		'ic_epo',
			fieldLabel:		'Nombre de la EPO',
			emptyText:		'Seleccione una EPO',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		true,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoEpoData,
			tpl:				NE.util.templateMensajeCargaCombo,
			listeners:	{
				'select':function(cbo){

								var cboIf = Ext.getCmp('dis_ic_if');

								if(!Ext.isEmpty(cbo.getValue())){

									cboIf.setValue('');
									cboIf.store.removeAll();
									cboIf.store.load({
										params: {
											ic_epo:	cbo.getValue()
										}
									});

								}else{

									cboIf.setValue('');
									cboIf.setRawValue('');
									cboIf.store.removeAll();
									var comboBanco = Ext.getCmp('dis_txtBanco');
									comboBanco.setValue('');
									comboBanco.store.removeAll();
									comboBanco.store.load({
										params: {
											ic_if_tipo1:	null
										}
									});

								}
							}
			}
		},{
			xtype:			'textfield',
			fieldLabel:		'N�m NE PyME',
			name:				'txtNE',
			id:				'dis_txtNE',
			allowBlank:		true,
			regex:			/^\d*$/,
			regexText:		'Favor de proporcionar solamente d�gitos en el N�m NE PyME',
			maxLength:		100
		},{
			xtype:			'combo',
			id:				'dis_ic_if',
			name:				'ic_if',
			hiddenName:		'ic_if',
			fieldLabel:		'Intermediario Financiero',
			emptyText:		'Seleccione IF',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		true,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoIfData,
			value:			"",
			tpl:				NE.util.templateMensajeCargaCombo,
			listeners:	{
				'select':function(combo){
								if(!Ext.isEmpty(combo.getValue())){

									var clave = combo.getValue();
									var record = combo.findRecord("clave",clave);

									if (record) {
										Ext.getCmp('dis_txtBanco').setValue(record.get('ic_financiera'));
									}
									Ext.getCmp('dis_txtBanco').hide();
								}else{
									var cboBanco = Ext.getCmp('dis_txtBanco');
									cboBanco.setValue('');
									/*cboBanco.store.removeAll();
									cboBanco.store.reload();*/
									cboBanco.show();
								}
							}
			}
		},{
			xtype:			'combo',
			id:				'dis_txtBanco',
			name:				'txtBanco',
			hiddenName:		'txtBanco',
			fieldLabel:		'* Banco de Servicio',
			emptyText:		'Seleccione un Banco',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		true,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoBancoDisData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'textfield',
			fieldLabel:		'Sucursal',
			name:				'txtSucursal',
			id:				'dis_txtSucursal',
			maskRe:			/[0-9]/,
			allowBlank:		true,
			maxLength:		100
		},{
			xtype:			'combo',
			id:				'dis_cboMoneda',
			name:				'cboMoneda',
			hiddenName:		'cboMoneda',
			fieldLabel:		'Moneda',
			emptyText:		'Seleccione una Moneda',	
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			triggerAction:	'all',
			typeAhead:		true,
			minChars:		1,
			store:			catalogoMonedaData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'textfield',
			fieldLabel:		'No. de Cuenta',
			name:				'txtCuenta',
			id:				'dis_txtCuenta',
			maskRe:			/[0-9]/,
			regex:			/^\d*$/,
			regexText:		'Favor de proporcionar solamente d�gitos en el n�mero de cuenta',
			allowBlank:		true,
			maxLength:		20,
			anchor:			'50%'
		},{
			xtype:			'combo',
			id:				'dis_cboPlaza',
			name:				'cboPlaza',
			hiddenName:		'cboPlaza',
			fieldLabel:		'Plaza',
			emptyText:		'Seleccione una plaza',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			triggerAction:	'all',
			typeAhead:		true,
			minChars:		1,
			store:			catalogoPlazaData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'hidden',
			id:				'_CtasCapturadas',
			name:				'CtasCapturadas',
			value:			null
		}
	];

	var fpDistribuidores = new Ext.form.FormPanel({
		id:				'formaDistribuidores',
		title:			undefined,
		width:			700,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			true,
		labelWidth:		150,
		bodyStyle:		'padding-left:20px;,padding-top:10px;,text-align:left',
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			[elementosDistribuidores],
		buttons: [
			{
				text:		'Agregar',
				id:		'dist.btnAgregar',
				iconCls:	'aceptar',
				width:	100,
				handler: function(boton, evento) {

								if(!Ext.getCmp('formaDistribuidores').getForm().isValid()){
									return;
								}
								var txtNE = Ext.getCmp("dis_txtNE").getValue();
								submitCuentaDistribuidores(1,0,txtNE);

				} //fin handler
			},
			{
				text:		'Limpiar',
				id:		'dist.btnLimpiar',
				iconCls:	'icoLimpiar',
				width:	100,
				handler: function(boton, evento) {
								accionArealizar("BOTON_LIMPIAR_DISTRIBUIDORES",null);
				} //fin handler
			},
			{
				text:		'Consultar',
				id:		'dist.btnConsultar',
				iconCls:	'icoBuscar',
				width:	100,
				handler: function(boton, evento) {

								if(!Ext.getCmp('formaDistribuidores').getForm().isValid()){
									return;
								}
								accionArealizar("BOTON_CONSULTAR_DISTRIBUIDORES",null);
				} //fin handler
			}
		]
	});

//----- FODEA 016_2018 INICIA -----//

	//Limpia los campos del form de busqueda.
	function limpiaCriteriosDeBusqueda() {
		Ext.getCmp('factDist_num_ne_pyme').setValue('');
		Ext.getCmp('factDist_txtNombre').setValue('');
		Ext.getCmp('factDist_ic_epo').setValue('');
		Ext.getCmp('factDist_if').setValue('');
		Ext.getCmp('factDist_benef').setValue('');
		Ext.getCmp('factDist_moneda').setValue('');
	}

	//Limpia los campos del form de captura.
	function limpiarCamposDeCaptura() {
		Ext.getCmp('factDistCapt_num_ne_pyme').setValue('');
		Ext.getCmp('factDistCapt_txtNombre').setValue('');
		Ext.getCmp('factDistCapt_ic_pyme').setValue('');
		Ext.getCmp('factDistCapt_ic_epo').setValue('');
		Ext.getCmp('factDistCapt_if').setValue('');
		Ext.getCmp('factDistCapt_benef').setValue('');
		Ext.getCmp('factDistCapt_moneda').setValue('1');
		Ext.getCmp('factDistCapt_bancoServicio').setValue('');
		Ext.getCmp('factDistCapt_plaza').setValue('');
		Ext.getCmp('factDistCapt_sucursal').setValue('');
		Ext.getCmp('factDistCapt_cuenta').setValue('');
		Ext.getCmp('factDistCapt_cuentaClabe').setValue('');
		Ext.getCmp('factDistCapt_cuentaClabeConf').setValue('');
		Ext.getCmp('factDistCapt_cuentaSpid').setValue('');
		Ext.getCmp('factDistCapt_cuentaSpidConf').setValue('');
		Ext.getCmp('factDistCapt_swift').setValue('');
		Ext.getCmp('factDistCapt_aba').setValue('');
		Ext.getCmp('factDistCapt_modificacion').setValue('');

		Ext.getCmp('factDistCapt_cuentaClabe').show();
		Ext.getCmp('factDistCapt_cuentaClabeConf').show();

		//Cargo el grid de contratos autorizados en la tabla temporal
		consGridContratos.load({
			params: {
				informacion: 'FACT_DIST.CONSULTA_CONTRATOS',
				id: 0,
				tabla: 'TMP'
			}
		});
	}

	//Valida los campos obligatorios en el form de Captura. Regresa el numero de errores encontrados.
	function validarCamposObligatorios(accion){
		var numErrores=0;
		//Solo se validan cuando se agrega una cuenta
		if(accion==='AGREGAR') {
			if(Ext.getCmp('factDistCapt_num_ne_pyme').getValue()===''){
				Ext.getCmp('factDistCapt_num_ne_pyme').markInvalid('El campo es obligatorio');
				numErrores++;
			}
			if(Ext.getCmp('factDistCapt_ic_epo').getValue()===''){
				Ext.getCmp('factDistCapt_ic_epo').markInvalid('El campo es obligatorio');
				numErrores++;
			}
			if(Ext.getCmp('factDistCapt_if').getValue()===''){
				Ext.getCmp('factDistCapt_if').markInvalid('El campo es obligatorio');
				numErrores++;
			}
			if(Ext.getCmp('factDistCapt_benef').getValue()===''){
				Ext.getCmp('factDistCapt_benef').markInvalid('El campo es obligatorio');
				numErrores++;
			}
			if(Ext.getCmp('factDistCapt_moneda').getValue()===''){
				Ext.getCmp('factDistCapt_moneda').markInvalid('El campo es obligatorio');
				numErrores++;
			}
		}
		//Se validan cuando se agrega o modifica
		if(Ext.getCmp('factDistCapt_bancoServicio').getValue()===''){
			Ext.getCmp('factDistCapt_bancoServicio').markInvalid('El campo es obligatorio');
			numErrores++;
		}
		if(Ext.getCmp('factDistCapt_sucursal').getValue()===''){
			Ext.getCmp('factDistCapt_sucursal').markInvalid('El campo es obligatorio');
			numErrores++;
		}
		if(Ext.getCmp('factDistCapt_cuenta').getValue()===''){
			Ext.getCmp('factDistCapt_cuenta').markInvalid('El campo es obligatorio');
			numErrores++;
		}
		//Se validan dependiendo de la moneda
		if(Ext.getCmp('factDistCapt_moneda').getValue()!=='' && Ext.getCmp('factDistCapt_moneda').getValue()==='1') {
			if(Ext.getCmp('factDistCapt_cuentaClabe').getValue()===''){
				Ext.getCmp('factDistCapt_cuentaClabe').markInvalid('El campo es obligatorio');
				numErrores++;
			}
			if(Ext.getCmp('factDistCapt_cuentaClabeConf').getValue()===''){
				Ext.getCmp('factDistCapt_cuentaClabeConf').markInvalid('El campo es obligatorio');
				numErrores++;
			}
		} else if(Ext.getCmp('factDistCapt_moneda').getValue()!=='' && Ext.getCmp('factDistCapt_moneda').getValue()==='54') {
			if(Ext.getCmp('factDistCapt_cuentaSpid').getValue()===''){
				Ext.getCmp('factDistCapt_cuentaSpid').markInvalid('El campo es obligatorio');
				numErrores++;
			}
			if(Ext.getCmp('factDistCapt_cuentaSpidConf').getValue()===''){
				Ext.getCmp('factDistCapt_cuentaSpidConf').markInvalid('El campo es obligatorio');
				numErrores++;
			}
		}
		return numErrores;
	}

	//Valida los campos de confirmaci�n y que el grid no est� vac�o.
	function validaActualizaciones(){
		var errores = false;
		if(Ext.getCmp('factDistCapt_moneda').getValue()==='1' && Ext.getCmp('factDistCapt_cuentaClabe').getValue() !== Ext.getCmp('factDistCapt_cuentaClabeConf').getValue()) {
			Ext.MessageBox.alert('Mensaje de la p�gina web','La confirmaci�n de su cuenta CLABE no coincide. Favor de verificarlo.');
			errores = true;
		} else if(Ext.getCmp('factDistCapt_moneda').getValue()==='54' && Ext.getCmp('factDistCapt_cuentaSpid').getValue() !== Ext.getCmp('factDistCapt_cuentaSpidConf').getValue()) {
			Ext.MessageBox.alert('Mensaje de la p�gina web','La confirmaci�n de su cuenta Spid no coincide. Favor de verificarlo.');
			errores = true;
		} else if(validaGridVacio()===0){
			Ext.MessageBox.alert('Mensaje de la p�gina web','Debe existir al menos un contrato. Favor de verificarlo.');
			errores = true;
		}
		return errores;
	}

	/**
	 * Verifica si el grid de contratos asociados a una cuenta tiene datos
	 */
	function validaGridVacio(){
		var contador = 0;
		var jsonData = consGridContratos.data.items;
		Ext.each(jsonData, function(item,index,arrItem) {
			var campo = ''+item.data.CG_NUM_CONTRATO;
			if(campo!==null && campo.trim()!==''){
				contador++;
			}
		});
		return contador;
	}

	//Al crear o modificar una cuenta, se debe validar que no existan contratos duplicados para la misma cuenta.
	function validaContratosDuplicados(){
		var mensaje = '';
		var contratos_tmp = [];
		var jsonData = consGridContratos.data.items;
		//Valido que no existan registros vacios
		Ext.each(jsonData, function(item,index,arrItem) {
			if(mensaje===''){
				var campo = ''+item.data.CG_NUM_CONTRATO;
				if(campo.trim()===''){
					mensaje = 'Existen registros vac�os en los Contratos autorizados. Favor de verificarlo.';
				} else{
					contratos_tmp.push(item.data.CG_NUM_CONTRATO);
				}
			}
		});
		//Si no hay registros vac�os, valido que no existan registros duplicados.
		if(mensaje==='' && contratos_tmp.length>0) {
			var regDupl = false;
			for(var x=0;x<contratos_tmp.length;x++) {
				var tmp1 = contratos_tmp[x];
				for(var y=x+1;y<contratos_tmp.length;y++) {
					var tmp2 = contratos_tmp[y];
					if(tmp1===tmp2){
						mensaje = 'Existen registros duplicados en los Contratos autorizados ('+tmp1+'). Favor de verificarlo.';
						regDupl = true;
						break;
					}
				}
				if(regDupl===true) {
					break;
				}
			}
			
		}
		return mensaje;
	}

	//Funci�n para eliminar datos de las tablas temporales cuando se desar cancelar el proceso de agregar cuentas.
	function confirmaCancelar(){
		Ext.Msg.confirm('Mensaje de p�gina web','�Est� seguro de cancelar?',
			function(btn){
				if(btn==='ok' || btn==='yes'){
				Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '15admnafinparam01Ext.data.jsp',
						params: Ext.apply({
							informacion: 'FACT_DIST.ELIMINAR_CUENTAS_TMP',
							ic_cons:     Ext.getCmp('factDistCapt_icConsecutivo').getValue()
						}),
						callback: cuentaEliminada
					});
				}
			}
		);
	}

	//Funci�n para descargar archivos
	function descargaArchivo(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
			Ext.getCmp('imprimirPDF').setIconClass('icoPdf');
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fpFactDistribuido.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpFactDistribuido.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//Proceso posterior a la validaci�n de una cuenta nueva que se quiere agregar
	var cuentaNuevaValidada = function(opts, success, response) {
		var tabP = Ext.getCmp('tp');
		tabP.el.unmask();
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			if(jsonObj.mensaje==='OK') {
				Ext.Msg.confirm('Mensaje de p�gina web','�Desea agregar la cuenta?',
					function(btn){
						if(btn==='ok' || btn==='yes'){
							var jsonData = consGridContratos.data.items;
							var cg_contratos = [];
							Ext.each(jsonData, function(item,index,arrItem) {
								cg_contratos.push(item.data.CG_NUM_CONTRATO);
							});
							consultaDataTmp.load({
								params: {
									informacion:   'FACT_DIST.PRE_AGREGAR',
									num_ne_pyme:   Ext.getCmp('factDistCapt_num_ne_pyme').getValue(),
									ic_pyme:       Ext.getCmp('factDistCapt_ic_pyme').getValue(),
									ic_epo:        Ext.getCmp('factDistCapt_ic_epo').getValue(),
									ic_if:         Ext.getCmp('factDistCapt_if').getValue(),
									benef:         Ext.getCmp('factDistCapt_benef').getValue(),
									moneda:        Ext.getCmp('factDistCapt_moneda').getValue(),
									bancoServicio: Ext.getCmp('factDistCapt_bancoServicio').getValue(),
									plaza:         Ext.getCmp('factDistCapt_plaza').getValue(),
									sucursal:      Ext.getCmp('factDistCapt_sucursal').getValue(),
									cuenta:        Ext.getCmp('factDistCapt_cuenta').getValue(),
									cuentaClabe:   Ext.getCmp('factDistCapt_cuentaClabe').getValue(),
									cuentaSpid:    Ext.getCmp('factDistCapt_cuentaSpid').getValue(),
									swift:         Ext.getCmp('factDistCapt_swift').getValue(),
									aba:           Ext.getCmp('factDistCapt_aba').getValue(),
									ic_cons:       Ext.getCmp('factDistCapt_icConsecutivo').getValue(),
									cg_contratos:  cg_contratos
								}
							});
						}
					}
				);
			} else {
				Ext.MessageBox.alert('Mensaje de la p�gina web',jsonObj.mensaje);
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

	//Proceso posterior a la modificacion exitosa de la cuenta.
	var cuentaModificada = function(opts, success, response) {
		var tabP = Ext.getCmp('tp');
		tabP.el.unmask();
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			if(jsonObj.mensaje!=='') {
				Ext.MessageBox.alert('Mensaje de la p�gina web',jsonObj.mensaje);
			} else {
				Ext.MessageBox.alert('Mensaje de la p�gina web','Proceso exitoso.');
			}
			//Limpio los campos
			limpiarCamposDeCaptura();
			limpiaCriteriosDeBusqueda();
			//En este punto se eliminan las llaves primarias.
			Ext.getCmp('factDistCapt_icConsecutivo').setValue('');
			//Oculto los componentes mecesarios
			Ext.getCmp('gridFactDistribuido').hide();
			Ext.getCmp('fpCapturaFactDistribuido').hide();
			Ext.getCmp('gridFactDistribuidoTmp').hide();
			//Muestro el form principal
			Ext.getCmp('formaFactDistribuido').show();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

	//Proceso posterior a la eliminaci�n de la cuenta
	var cuentaEliminada = function(opts, success, response) {
		var tabP = Ext.getCmp('tp');
		tabP.el.unmask();
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			//Limpio los campos
			limpiarCamposDeCaptura();
			limpiaCriteriosDeBusqueda();
			//En este punto se eliminan las llaves primarias.
			Ext.getCmp('factDistCapt_icConsecutivo').setValue('');
			//Oculto los componentes mecesarios
			Ext.getCmp('fpCapturaFactDistribuido').hide();
			Ext.getCmp('gridFactDistribuidoTmp').hide();
			Ext.getCmp('gridFactDistribuido').hide();
			//Muestro el form principal
			Ext.getCmp('formaFactDistribuido').show();
			if(jsonObj.mensaje!=='') {
				Ext.MessageBox.alert('Mensaje de la p�gina web',jsonObj.mensaje);
			} else {
				Ext.MessageBox.alert('Mensaje de la p�gina web','Proceso exitoso.');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

	// Visor que muestra los contratos parametrizados.
	var verDetalle = function(grid, rowIndex, colIndex, item, event) {
		var formCapturaFactDistribuido = Ext.getCmp('fpCapturaFactDistribuido');
		var registro = grid.getStore().getAt(rowIndex);
		var tabla = '';
		if (formCapturaFactDistribuido.isVisible()) {
			tabla = 'TMP';
		} else {
			tabla = registro.get('TABLA');
		}
		consDetalleContratos.load({
			params: {
				informacion: 'FACT_DIST.CONSULTA_CONTRATOS',
				id: registro.get('IC_CONSECUTIVO'),
				tabla: tabla
			}
		});
		var verDetalle = Ext.getCmp('verDetalle');
		if(verDetalle){
			verDetalle.show();
		}else{
			verDetalle = new Ext.Window({
				id:                 'verDetalle',
				layout:             'fit',
				closeAction:        'hide',
				width:              350,
				height:             265,
				resizable:          false,
				closable:           true,
				modal:              true,
				items:              [{
					xtype:          'label',
					html:           '<\/br><b>EPO:<\/b> ' + registro.get('NOMBRE_EPO')+'<\/br>',
					style:          {
						textAlign:  'left'
					} 
				},{ 
					xtype:          'label',
					html:           '<\/br><b>PROVEEDOR:<\/b> ' + registro.get('NOMBRE_PYME')+'<\/br>',
					style:          {
						textAlign:  'left'
					} 
				},{ 
					xtype:          'label',
					html:           '<\/br><b>BENEFICIARIO:<\/b> ' + registro.get('NOMBRE_BENEFICIARIO')+'<\/br><\/br>',
					style:          {
						textAlign:  'left'
					} 
				},gridDetalleContratos
				],
				bbar: {
					xtype:          'toolbar',
					buttonAlign:    'center',
					buttons:        [{
						xtype:      'button',
						text:       'Cerrar',
						iconCls:    'icoLimpiar', 
						id:         'btnCerraP',
						handler:    function(){
							Ext.getCmp('verDetalle').hide();
						}
					}]
				}
			}).show().setTitle('Contratos');
		}
	};

	//Proceso para la b�squeda del campo N�m. NE y PyME.
	var successAjaxFn = function(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			var formCapturaFactDistribuido = Ext.getCmp('fpCapturaFactDistribuido');
			if(jsonObj.txtNombre===''){
				Ext.MessageBox.alert('Mensaje','N�mero de Nafin Electr�nico incorrecto o el Proveedor no opera Factoraje Distribuido.');
				if (formCapturaFactDistribuido.isVisible()) {
					Ext.getCmp('factDistCapt_txtNombre').setValue('');
					Ext.getCmp('factDistCapt_num_ne_pyme').setValue('');
					Ext.getCmp('factDistCapt_ic_pyme').setValue('');
				} else {
					Ext.getCmp('factDist_txtNombre').setValue('');
					Ext.getCmp('factDist_num_ne_pyme').setValue('');
				}
			} else {
				if (formCapturaFactDistribuido.isVisible()) {
					Ext.getCmp('factDistCapt_txtNombre').setValue(jsonObj.txtNombre);
					Ext.getCmp('factDistCapt_ic_pyme').setValue(jsonObj.ic_pyme);
				} else{
					Ext.getCmp('factDist_txtNombre').setValue(jsonObj.txtNombre);
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

	//Proceso posterior a la consulta del grid de acuse detalle de las cuentas agregadas.
	var procesarGridAcuseDetalle = function(store, arrRegistros, opts) {
		var tabP = Ext.getCmp('tp');
		tabP.el.unmask();
		var gridConsulta = Ext.getCmp('gridAcuseDetalle');
		var el = gridConsulta.getGridEl();
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0) {
				//Limpio los campos
				limpiarCamposDeCaptura();
				limpiaCriteriosDeBusqueda();
				//En este punto se eliminan las llaves primarias.
				Ext.getCmp('factDistCapt_icConsecutivo').setValue('');
				//Oculto los componentes no necesarios
				Ext.getCmp('fpCapturaFactDistribuido').hide();
				Ext.getCmp('gridFactDistribuidoTmp').hide();
				//Proceso los grids de acuse
				gridConsulta.show();
				el.unmask();
				Ext.getCmp('imprimirPDF').enable();
				var total = store.getTotalCount();
				consGridAcuse.load({
					params: {
						informacion: 'FACT_DIST.CONSULTAR_ACUSE',
						fecha: jsonData.fecha,
						usuario: jsonData.usuario,
						total: total
					}
				});
			} else {
				gridConsulta.hide();
			}
		}
	};

	//Proceso del grid Acuse de cuentas agregadas
	var procesarGridAcuse = function(store,arrRegistros,opts){
		var tabP = Ext.getCmp('tp');
		tabP.el.unmask();
		if(arrRegistros != null){
			var gridAcuse = Ext.getCmp('gridAcuse');
			if (!gridAcuse.isVisible()) {
				gridAcuse.show();
			}
			var el = gridAcuse.getGridEl();
			if(store.getTotalCount() <= 0){
				el.mask('No se encontr� ning�n registro','x-mask');
			} else {
				el.unmask();
			}
		}
	};

	//Proceso del grid de contratos por cuenta
	var procesarContratos = function(store,arrRegistros,opts){
		var tabP = Ext.getCmp('tp');
		tabP.el.unmask();
		if(arrRegistros != null){
		var gridContratos = Ext.getCmp('gridContratos');
			if (!gridContratos.isVisible()) {
				gridContratos.show();
			}
			var el = gridContratos.getGridEl();
			if(store.getTotalCount() > 0){
				el.unmask();
			}
		}
	};

	//Proceso del grid de contratos parametrizados
	var procesarDetalleContratos = function(store,arrRegistros,opts){
		var tabP = Ext.getCmp('tp');
		tabP.el.unmask();
		if(arrRegistros != null){
			var gridDetalleContratos = Ext.getCmp('gridDetalleContratos');
			if (!gridDetalleContratos.isVisible()) {
				gridDetalleContratos.show();
			}
			var el = gridDetalleContratos.getGridEl();
			if(store.getTotalCount() <= 0){
				el.mask('No se encontr� ning�n registro','x-mask');
			} else {
				el.unmask();
			}
		}
	};

	//Proceso posterior a la consulta del grid de registros temporales.
	var procesarConsultaDataTmp = function(store, arrRegistros, opts) {
		var tabP = Ext.getCmp('tp');
		tabP.el.unmask();
		var gridConsulta = Ext.getCmp('gridFactDistribuidoTmp');
		var el = gridConsulta.getGridEl();
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			Ext.getCmp('factDistCapt_icConsecutivo').setValue(jsonData.ids);//Guarda la lista de ids agregados a la tabla temporal
			Ext.getCmp('factDistCapt.btnCancelar').hide();
			if(store.getTotalCount() > 0) {
				//Limpio el form
				limpiarCamposDeCaptura();
				gridConsulta.show();
				el.unmask();
				Ext.getCmp('btnAgregarFactDist').enable();
			} else {
				gridConsulta.show();
				Ext.getCmp('btnAgregarFactDist').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

	//Proceso posterior a la consulta del grid.
	var procesarConsultaFactDistribuido = function(store, arrRegistros, opts) {
		var tabP = Ext.getCmp('tp');
		tabP.el.unmask();
		var gridConsulta = Ext.getCmp('gridFactDistribuido');
		var el = gridConsulta.getGridEl();

		if (arrRegistros != null) {
			if(store.getTotalCount() > 0) {
				gridConsulta.show();
				el.unmask();
				Ext.getCmp('btnGenerarCSV').enable();
			} else {
				gridConsulta.show();
				Ext.getCmp('btnGenerarCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

	//Procesamiento del combo: Nombre de la EPO.
	var procesa_catalogo_factDist_epo = function(store, arrRegistros, opts) {
		Ext.getCmp('factDist_ic_epo').setValue('');
	};

	//Procesamiento del combo: Intermediario Financiero.
	var procesa_catalogo_factDist_if = function(store, arrRegistros, opts) {
		if(Ext.getCmp('factDist_if').getValue()!=='') {
			Ext.getCmp('factDist_if').setValue(Ext.getCmp('factDist_if').getValue());
		}
		if(Ext.getCmp('factDistCapt_if').getValue()!=='') {
			Ext.getCmp('factDistCapt_if').setValue(Ext.getCmp('factDistCapt_if').getValue());
		}
	};

	//Procesamiento del combo: Beneficiario.
	var procesa_catalogo_factDist_benef = function(store, arrRegistros, opts) {
		if(Ext.getCmp('factDist_benef').getValue()!=='') {
			Ext.getCmp('factDist_benef').setValue(Ext.getCmp('factDist_benef').getValue());
		}
		if(Ext.getCmp('factDistCapt_benef').getValue()!=='') {
			Ext.getCmp('factDistCapt_benef').setValue(Ext.getCmp('factDistCapt_benef').getValue());
		}
	};

	//Procesamiento del combo: Moneda.
	var procesa_catalogo_factDist_moneda = function(store, arrRegistros, opts) {
		Ext.getCmp('factDist_moneda').setValue('');
	};

	//Store del grid de acuse detalle de las cuentas agregadas.
	var consGridAcuseDetalle = new Ext.data.JsonStore({
		root:            'registros',
		id:              'consGridAcuseDetalle',
		url:             '15admnafinparam01Ext.data.jsp',
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		baseParams:      {
			informacion: 'FACT_DIST.AGREGAR_CUENTAS'
		},
		fields: [
			{name: 'NOMBRE_EPO'              },
			{name: 'NOMBRE_PYME'             },
			{name: 'IC_NAFIN_ELECTRONICO'    },
			{name: 'INTERMEDIARIO_FINANCIERO'},
			{name: 'BANCO_SERVICIO'          },
			{name: 'NOMBRE_BENEFICIARIO'     },
			{name: 'CG_SUCURSAL'             },
			{name: 'NOMBRE_MONEDA'           },
			{name: 'CG_NUMERO_CUENTA'        },
			{name: 'CG_CUENTA_CLABE'         },
			{name: 'CG_CUENTA_SPID'          },
			{name: 'CG_SWIFT'                },
			{name: 'CG_ABA'                  },
			{name: 'CG_PLAZA'                },
			{name: 'IC_CONSECUTIVO'          }
		],
		listeners: {
			load: procesarGridAcuseDetalle,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarGridAcuseDetalle(null, null, null);
				}
			}
		}
	});

	//Store del grid de acuse de cuentas agregadas
	var consGridAcuse = new Ext.data.JsonStore({
		root: 'registros',
		url: '15admnafinparam01Ext.data.jsp',
		baseParams: {
			informacion: 'FACT_DIST.CONSULTAR_ACUSE'
		},
		fields: [
			{name: 'ID'},
			{name: 'DESCRIPCION'},
			{name: 'DATO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarGridAcuse,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarGridAcuse(null, null, null);
				}
			}
		}
	});

	//Store del grid de cotratos asociados a una cuenta
	var consGridContratos = new Ext.data.JsonStore({
		root: 'registros',
		url: '15admnafinparam01Ext.data.jsp',
		baseParams: {
			informacion: 'FACT_DIST.CONSULTA_CONTRATOS'
		},
		fields: [
			{name: 'CG_NUM_CONTRATO'},
			{name: 'IC_CONSECUTIVO' }
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarContratos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarContratos(null, null, null);
				}
			}
		}
	});

	//Store del grid de cotratos parametrizados
	var consDetalleContratos = new Ext.data.JsonStore({
		root: 'registros',
		url: '15admnafinparam01Ext.data.jsp',
		baseParams: {
			informacion: 'FACT_DIST.CONSULTA_CONTRATOS'
		},
		fields: [{name: 'CG_NUM_CONTRATO'}],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDetalleContratos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarDetalleContratos(null, null, null);
				}
			}
		}
	});

	//Store del combo: Nombre de la EPO.
	var catalogo_factDist_epo = new Ext.data.JsonStore({
		id:              'catalogo_factDist_epo',
		root:            'registros',
		totalProperty:   'total',
		url:             '15admnafinparam01Ext.data.jsp',
		fields:          ['clave', 'descripcion', 'loadMsg'],
		baseParams:      {
			informacion: 'FACT_DIST.CATALOGO_EPO'
		},
		autoLoad:        true,
		listeners:       {
			load:        procesa_catalogo_factDist_epo,
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	//Store del combo: Intermediario Financiero.
	var catalogo_factDist_if = new Ext.data.JsonStore({
		id:              'catalogo_factDist_if',
		root:            'registros',
		totalProperty:   'total',
		url:             '15admnafinparam01Ext.data.jsp',
		fields:          ['clave', 'descripcion', 'loadMsg'],
		baseParams:      {
			informacion: 'FACT_DIST.CATALOGO_IF'
		},
		autoLoad:        true,
		listeners:       {
			load:        procesa_catalogo_factDist_if,
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	//Store del combo: Beneficiario.
	var catalogo_factDist_benef = new Ext.data.JsonStore({
		id:              'catalogo_factDist_benef',
		root:            'registros',
		totalProperty:   'total',
		url:             '15admnafinparam01Ext.data.jsp',
		fields:          ['clave', 'descripcion', 'loadMsg'],
		baseParams:      {
			informacion: 'FACT_DIST.CATALOGO_BENEF'
		},
		autoLoad:        true,
		listeners:       {
			load:        procesa_catalogo_factDist_benef,
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	//Store del combo: Moneda.
	var catalogo_factDist_moneda = new Ext.data.JsonStore({
		id:              'catalogo_factDist_moneda',
		root:            'registros',
		totalProperty:   'total',
		url:             '15admnafinparam01Ext.data.jsp',
		fields:          ['clave', 'descripcion', 'loadMsg'],
		baseParams:      {
			informacion: 'FACT_DIST.CATALOGO_MONEDA'
		},
		autoLoad:        true,
		listeners:       {
			load:        procesa_catalogo_factDist_moneda,
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	//Store del combo: Banco de servicio.
	var catalogo_factDist_bancoServicio = new Ext.data.JsonStore({
		id:              'catalogo_factDist_bancoServicio',
		root:            'registros',
		totalProperty:   'total',
		url:             '15admnafinparam01Ext.data.jsp',
		fields:          ['clave', 'descripcion', 'loadMsg'],
		baseParams:      {
			informacion: 'FACT_DIST.CATALOGO_BANCO_SERVICIO'
		},
		autoLoad:        true,
		listeners:       {
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	//Store del combo: plaza.
	var catalogo_factDist_plaza = new Ext.data.JsonStore({
		id:              'catalogo_factDist_plaza',
		root:            'registros',
		totalProperty:   'total',
		url:             '15admnafinparam01Ext.data.jsp',
		fields:          ['clave', 'descripcion', 'loadMsg'],
		baseParams:      {
			informacion: 'FACT_DIST.CATALOGO_PLAZA'
		},
		autoLoad:        true,
		listeners:       {
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	//Store del form: Busqueda Avanzada.
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id:              'storeBusqAvanzPyme',
		root:            'registros',
		url:             '15admnafinparam01Ext.data.jsp',
		fields:          ['clave', 'descripcion','loadMsg'],
		baseParams:      {
			informacion: 'FACT_DIST.BUSQUEDA_AVANZADA'
		},
		totalProperty:   'total',
		autoLoad:        false,
		listeners:       {
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	//Store del grid de los datos temporales.
	var consultaDataTmp = new Ext.data.JsonStore({
		root:            'registros',
		id:              'consultaDataTmp',
		url:             '15admnafinparam01Ext.data.jsp',
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		baseParams:      {
			informacion: 'FACT_DIST.PRE_AGREGAR'
		},
		fields: [
			{name: 'NOMBRE_EPO'              },
			{name: 'NOMBRE_PYME'             },
			{name: 'IC_NAFIN_ELECTRONICO'    },
			{name: 'INTERMEDIARIO_FINANCIERO'},
			{name: 'BANCO_SERVICIO'          },
			{name: 'NOMBRE_BENEFICIARIO'     },
			{name: 'CG_SUCURSAL'             },
			{name: 'NOMBRE_MONEDA'           },
			{name: 'CG_NUMERO_CUENTA'        },
			{name: 'CG_CUENTA_CLABE'         },
			{name: 'CG_CUENTA_SPID'          },
			{name: 'CG_SWIFT'                },
			{name: 'CG_ABA'                  },
			{name: 'CG_PLAZA'                },
			{name: 'IC_CONSECUTIVO'          },
			{name: 'TABLA'                   }
		],
		listeners: {
			load: procesarConsultaDataTmp,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataTmp(null, null, null);
				}
			}
		}
	});

	//Store del grid de consulta.
	var consultaData = new Ext.data.JsonStore({
		root:            'registros',
		id:              'consultaData',
		url:             '15admnafinparam01Ext.data.jsp',
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		baseParams:      {
			informacion: 'FACT_DIST.CONSULTAR'
		},
		fields: [
			{name: 'NOMBRE_EPO'              },
			{name: 'NOMBRE_PYME'             },
			{name: 'IC_NAFIN_ELECTRONICO'    },
			{name: 'INTERMEDIARIO_FINANCIERO'},
			{name: 'BANCO_SERVICIO'          },
			{name: 'NOMBRE_BENEFICIARIO'     },
			{name: 'CG_SUCURSAL'             },
			{name: 'NOMBRE_MONEDA'           },
			{name: 'CG_NUMERO_CUENTA'        },
			{name: 'CG_CUENTA_CLABE'         },
			{name: 'CG_CUENTA_SPID'          },
			{name: 'CG_SWIFT'                },
			{name: 'CG_ABA'                  },
			{name: 'CG_PLAZA'                },
			{name: 'CG_AUTORIZA_IF'          },
			{name: 'CG_CAMBIO_CUENTA'        },
			{name: 'COLOR'                   },
			{name: 'IC_CONSECUTIVO'          },
			{name: 'TABLA'                   },
			{name: 'IC_IF'                   },
			{name: 'IC_EPO'                  },
			{name: 'IC_PYME'                 },
			{name: 'IC_BENEFICIARIO'         },
			{name: 'IC_MONEDA'               },
			{name: 'IC_BANCOS_TEF'           },
			{name: 'IC_PLAZA'                }
		],
		listeners: {
			load: procesarConsultaFactDistribuido,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaFactDistribuido(null, null, null);
				}
			}
		}
	});

	//Grid de consulta del grid de acuse detalle de las cuentas agregadas.
	var gridAcuseDetalle = new Ext.grid.GridPanel({
		id:               'gridAcuseDetalle',
		style:            'margin: 0 auto; padding-top:10px;',
		emptyMsg:         'No hay registros.',
		margins:          '20 0 0 0',
		stripeRows:       true,
		loadMask:         true,
		frame:            true,
		hidden:           true,
		displayInfo:      true,
		height:           200,
		width:            930,
		store:            consGridAcuseDetalle,
		title:            'Cuentas Bancarias del Beneficiario',
		columns: [{
			header:       'EPO',
			dataIndex:    'NOMBRE_EPO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Proveedor',
			dataIndex:    'NOMBRE_PYME',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'N�mero de Nafin Electr�nico',
			dataIndex:    'IC_NAFIN_ELECTRONICO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'IF',
			dataIndex:    'INTERMEDIARIO_FINANCIERO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Banco de servicio',
			dataIndex:    'BANCO_SERVICIO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Beneficiario',
			dataIndex:    'NOMBRE_BENEFICIARIO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Sucursal',
			dataIndex:    'CG_SUCURSAL',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Moneda',
			dataIndex:    'NOMBRE_MONEDA',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'No. de cuenta',
			dataIndex:    'CG_NUMERO_CUENTA',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'No. Cuenta CLABE',
			dataIndex:    'CG_CUENTA_CLABE',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'No. Cuenta Spid',
			dataIndex:    'CG_CUENTA_SPID',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'SWIFT',
			dataIndex:    'CG_SWIFT',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'ABA',
			dataIndex:    'CG_ABA',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Plaza',
			dataIndex:    'CG_PLAZA',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			xtype:        'actioncolumn',
			header:       'Ver Contratos',
			align:        'center',
			width:        150,
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
					return 'iconoLupa';
				},
				handler: verDetalle
			}]
		}],
		bbar: {
			items: [
				'->','-',
				{
					xtype:   'button',
					text:    'Imprimir',
					iconCls: 'icoPdf',
					id:      'imprimirPDF',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						var ic_consecutivos = [];
						var fecha = '';
						var usuario = '';
						var total = '';
						var jsonData = consGridAcuseDetalle.data.items;
						var jsonData1 = consGridAcuse.data.items;
						Ext.each(jsonData, function(item,index,arrItem) {
							ic_consecutivos.push(item.data.IC_CONSECUTIVO);
						});
						Ext.each(jsonData1, function(item,index,arrItem) {
							if(item.data.ID==='A'){
								fecha = item.data.DATO;
							}
							if(item.data.ID==='B'){
								usuario = item.data.DATO;
							}
							if(item.data.ID==='C'){
								total = item.data.DATO;
							}
						});
						Ext.Ajax.request({
							url: '15admnafinparam01Ext.data.jsp',
							params: Ext.apply({
								informacion: 'FACT_DIST.GENERAR_PDF',
								ic_consecutivos: ic_consecutivos,
								fecha: fecha,
								usuario: usuario,
								total: total
							}),
							callback: descargaArchivo
						});
					}
				},'-',
				{
					xtype:   'button',
					text:    'Regresar',
					iconCls: 'icoRegresar',
					id:      'regresar',
					handler: function(boton, evento) {
						limpiarCamposDeCaptura();
						Ext.getCmp('factDistCapt_icConsecutivo').setValue('');
						Ext.getCmp('gridAcuse').hide();
						Ext.getCmp('gridAcuseDetalle').hide();
						limpiaCriteriosDeBusqueda();
						Ext.getCmp('formaFactDistribuido').show();
					}
				}
			]
		}
	});

	//Grid que muestra el acuse de las cuentas agregadas.
	var gridAcuse = {
		xtype:         'grid',
		id:            'gridAcuse',
		title:         'Acuse',
		style:         'margin: 0 auto; padding-top:10px;',
		margins:       '20 0 0 0',
		hidden:        true,
		stripeRows:    true,
		loadMask:      true,
		frame:         false,
		height:        130,
		width:         300,
		store:         consGridAcuse,
		columns: [{
			dataIndex: 'DESCRIPCION',
			align:     'left',
			width:     100
		},{
			dataIndex: 'DATO',
			align:     'left',
			width:     195
		}]
	};

	//Grid que muestra los contratos asociados a cada cuenta
	var gridContratos = new Ext.grid.EditorGridPanel({
		fieldLabel:          'Contratos autorizados',
		id:                  'gridContratos',
		autoHeight:          true,
		hidden:              false,
		stripeRows:          true,
		loadMask:            true,
		frame:               true,
		clicksToEdit:        2,
		sm:                  new Ext.grid.CellSelectionModel(),
		listeners:           {
			beforeedit:      function(e){//Para poner los campos editables
				return true;
			}
		},
		store:               consGridContratos,
		columns:             [{
			header:          'No. de Contrato',
			dataIndex:       'CG_NUM_CONTRATO',
			align:           'center',
			width:           475,
			editor:          {
				xtype:       'textfield',
				allowBlank:  false,
				maxLength:   10
			}
		}],
		tbar: {
			items:           ['-',
				{
					xtype:   'button',
					text:    'Agregar',
					iconCls: 'aceptar',
					scope: this,
					handler: function(boton, evento) {
						//http://svn.geoext.org/ext/3.3.1/examples/writer/writer.html
						var hayErrores = 0;
						var id = '';
						var gridConsulta = Ext.getCmp('gridContratos');
						var columnModelGrid = gridConsulta.getColumnModel();
						var store = gridConsulta.getStore();
						//Reviso que no existan registros vac�os
						store.each(function(record) {
							var numReg = store.indexOf(record);
							if(record.data['CG_NUM_CONTRATO']===null || record.data['CG_NUM_CONTRATO']===''){
								Ext.MessageBox.alert('Mensaje de p�gina web','Existen campos vac�os. Favor de verificarlo.',
									function(){
										gridConsulta.startEditing(numReg, columnModelGrid.findColumnIndex('CG_NUM_CONTRATO'));
									}
								);
								hayErrores++;
								return false;
							}
						});
						if(hayErrores===0){
							//Agrego el registro al store
							if(Ext.getCmp('factDistCapt_modificacion').getValue()==='S'){
								id = Ext.getCmp('factDistCapt_icConsecutivo').getValue();
							}
							var u = new consGridContratos.recordType({
								IC_CONSECUTIVO: id,
								CG_NUM_CONTRATO: ''
							});
							consGridContratos.insert(0, u);
							//La celda agregada se pone por default como editable
							gridConsulta.startEditing(0, columnModelGrid.findColumnIndex('CG_NUM_CONTRATO'));
						}
					}
				},'-',
				{
					xtype:   'button',
					text:    'Eliminar',
					iconCls: 'borrar',
					handler: function(boton, evento) {
						var index = Ext.getCmp('gridContratos').getSelectionModel().getSelectedCell();
						if (!index) {
							return false;
						}
						var rec = consGridContratos.getAt(index[0]);
							consGridContratos.remove(rec);
						}
				},'-'
			]
		}
	});

	//Grid que muestra los contratos parametrizados
	var gridDetalleContratos = {
		xtype:         'grid',
		id:            'gridDetalleContratos',
		title:         '',
		autoHeight:    true,
		hidden:        false,
		stripeRows:    true,
		loadMask:      true,
		frame:         false,
		//height:        200,
		width:         340,
		store:         consDetalleContratos,
		columns: [{
			header:    'No. de Contrato',
			dataIndex: 'CG_NUM_CONTRATO',
			align:     'center',
			width:     330
		}]
	};

	//Grid que muestra los registros agregados temporalmente
	var gridFactDistribuidoTmp = new Ext.grid.GridPanel({
		id:               'gridFactDistribuidoTmp',
		style:            'margin: 0 auto; padding-top:10px;',
		margins:          '20 0 0 0',
		stripeRows:       true,
		loadMask:         true,
		frame:            true,
		hidden:           true,
		displayInfo:      true,
		height:           250,
		width:            930,
		store:            consultaDataTmp,
		title:            undefined,
		columns: [{
			header:       'EPO',
			dataIndex:    'NOMBRE_EPO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Proveedor',
			dataIndex:    'NOMBRE_PYME',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'N�mero de NafinElectr�nico',
			dataIndex:    'IC_NAFIN_ELECTRONICO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'IF',
			dataIndex:    'INTERMEDIARIO_FINANCIERO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Banco de servicio',
			dataIndex:    'BANCO_SERVICIO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Beneficiario',
			dataIndex:    'NOMBRE_BENEFICIARIO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Sucursal',
			dataIndex:    'CG_SUCURSAL',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Moneda',
			dataIndex:    'NOMBRE_MONEDA',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'No. de cuenta',
			dataIndex:    'CG_NUMERO_CUENTA',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'No. Cuenta CLABE',
			dataIndex:    'CG_CUENTA_CLABE',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'No. Cuenta Spid',
			dataIndex:    'CG_CUENTA_SPID',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'SWIFT',
			dataIndex:    'CG_SWIFT',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'ABA',
			dataIndex:    'CG_ABA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Plaza',
			dataIndex:    'CG_PLAZA',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			xtype:     'actioncolumn',
			header:    'Ver Contratos',
			align:     'center',
			width:     150,
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
					return 'iconoLupa';
				},
				handler: verDetalle
			}]
		}],
		bbar: {
			items: [
				'->','-',
				{
					xtype:   'button',
					text:    'Cancelar',
					iconCls: 'cancelar',
					id:      'btnCancelarFactDist',
					handler: function(boton, evento) {
						confirmaCancelar();
					}
				},'-',{
					xtype:   'button',
					text:    'Confirmar',
					iconCls: 'aceptar',
					id:      'btnAgregarFactDist',
					handler: function(boton, evento) {
						Ext.Msg.confirm('Mensaje de p�gina web','�Est� seguro de agregar las cuentas?',
							function(btn){
								if(btn==='ok' || btn==='yes'){
									Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');
									consGridAcuseDetalle.load({
										params: {
											informacion: 'FACT_DIST.AGREGAR_CUENTAS',
											ic_cons:     Ext.getCmp('factDistCapt_icConsecutivo').getValue()
										}
									});
								}
							}
						);
					}
				}
			]
		}
	});

	//Grid de consulta
	var gridFactDistribuido = new Ext.grid.GridPanel({
		id:               'gridFactDistribuido',
		style:            'margin: 0 auto; padding-top:10px;',
		emptyMsg:         'No hay registros.',
		margins:          '20 0 0 0',
		stripeRows:       true,
		loadMask:         true,
		frame:            true,
		hidden:           true,
		displayInfo:      true,
		height:           400,
		width:            930,
		store:            consultaData,
		title:            undefined,
		columns: [{
			header:       'EPO',
			dataIndex:    'NOMBRE_EPO',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Proveedor',
			dataIndex:    'NOMBRE_PYME',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'N�mero de Nafin Electr�nico',
			dataIndex:    'IC_NAFIN_ELECTRONICO',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'IF',
			dataIndex:    'INTERMEDIARIO_FINANCIERO',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Banco de servicio',
			dataIndex:    'BANCO_SERVICIO',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Beneficiario',
			dataIndex:    'NOMBRE_BENEFICIARIO',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Sucursal',
			dataIndex:    'CG_SUCURSAL',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Moneda',
			dataIndex:    'NOMBRE_MONEDA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'No. de cuenta',
			dataIndex:    'CG_NUMERO_CUENTA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'No. Cuenta CLABE',
			dataIndex:    'CG_CUENTA_CLABE',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'No. Cuenta Spid',
			dataIndex:    'CG_CUENTA_SPID',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'SWIFT',
			dataIndex:    'CG_SWIFT',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'ABA',
			dataIndex:    'CG_ABA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Plaza',
			dataIndex:    'CG_PLAZA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			xtype:     'actioncolumn',
			header:    'Ver Contratos',
			align:     'center',
			width:     150,
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
					return 'iconoLupa';
				},
				handler: verDetalle
			}]
		},{
			header:       'Autorizaci�n IF',
			dataIndex:    'CG_AUTORIZA_IF',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Cambio de Cuenta',
			dataIndex:    'CG_CAMBIO_CUENTA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			xtype:     'actioncolumn',
			header:    'Seleccionar',
			align:     'center',
			width:     150,
			items: [{
				iconCls: 'modificar',
				handler: function(grid, rowIndex, colIndex, item, event){
					var registro = grid.getStore().getAt(rowIndex);
					//Cargo el grid de contratos autorizados
					consGridContratos.load({
						params: {
							informacion: 'FACT_DIST.CONSULTA_CONTRATOS',
							id: registro.get('IC_CONSECUTIVO'),
							tabla: registro.get('TABLA') 
						}
					});
					//Cargo los combos dependientes
					if(registro.get('IC_EPO')!==''){
						catalogo_factDist_if.load({
							params: {
								ic_epo: registro.get('IC_EPO')
							}
						});
						catalogo_factDist_benef.load({
							params: {
								ic_epo: registro.get('IC_EPO')
							}
						});
					}
					//Asigno los valores del grid al form de captura.
					Ext.getCmp('fpCapturaFactDistribuido').setTitle('Modificaci�n de Cuenta');
					Ext.getCmp('factDistCapt_num_ne_pyme').setValue(  registro.get('IC_NAFIN_ELECTRONICO'));
					Ext.getCmp('factDistCapt_ic_pyme').setValue(      registro.get('IC_PYME')             );
					Ext.getCmp('factDistCapt_ic_epo').setValue(       registro.get('IC_EPO')              );
					Ext.getCmp('factDistCapt_icConsecutivo').setValue(registro.get('IC_CONSECUTIVO')      );
					Ext.getCmp('factDistCapt_moneda').setValue(       registro.get('IC_MONEDA')           );
					Ext.getCmp('factDistCapt_bancoServicio').setValue(registro.get('IC_BANCOS_TEF')       );
					Ext.getCmp('factDistCapt_sucursal').setValue(     registro.get('CG_SUCURSAL')         );
					Ext.getCmp('factDistCapt_cuenta').setValue(       registro.get('CG_NUMERO_CUENTA')    );
					Ext.getCmp('factDistCapt_if').setValue(           registro.get('IC_IF')               );
					Ext.getCmp('factDistCapt_benef').setValue(        registro.get('IC_BENEFICIARIO')     );
					Ext.getCmp('factDistCapt_modificacion').setValue( 'S'                                 );
					//Asigno los valores no obligatorios
					if(registro.get('IC_PLAZA')!==0 && registro.get('IC_PLAZA')!=='0') {
						Ext.getCmp('factDistCapt_plaza').setValue(        registro.get('IC_PLAZA')            );
					}
					if(registro.get('CG_CUENTA_CLABE')!=='') {
						Ext.getCmp('factDistCapt_cuentaClabe').setValue(  registro.get('CG_CUENTA_CLABE')     );
					}
					if(registro.get('CG_CUENTA_SPID')!=='') {
						Ext.getCmp('factDistCapt_cuentaSpid').setValue(   registro.get('CG_CUENTA_SPID')      );
					}
					if(registro.get('CG_SWIFT')!=='') {
						Ext.getCmp('factDistCapt_swift').setValue(        registro.get('CG_SWIFT')            );
					}
					if(registro.get('CG_ABA')!=='') {
						Ext.getCmp('factDistCapt_aba').setValue(          registro.get('CG_ABA')              );
					}
					//Proceso el tipo de moneda
					if(registro.get('IC_MONEDA')==='1') {
						Ext.getCmp('factDistCapt_cuentaClabe').show();
						Ext.getCmp('factDistCapt_cuentaClabeConf').show();
						Ext.getCmp('factDistCapt_cuentaSpid').hide();
						Ext.getCmp('factDistCapt_cuentaSpidConf').hide();
						Ext.getCmp('factDistCapt_swift').hide();
						Ext.getCmp('factDistCapt_aba').hide();
					} else if(registro.get('IC_MONEDA')==='54') {
						Ext.getCmp('factDistCapt_cuentaClabe').hide();
						Ext.getCmp('factDistCapt_cuentaClabeConf').hide();
						Ext.getCmp('factDistCapt_cuentaSpid').show();
						Ext.getCmp('factDistCapt_cuentaSpidConf').show();
						Ext.getCmp('factDistCapt_swift').show();
						Ext.getCmp('factDistCapt_aba').show();
					}
					//Obtengo el nombre del proveedor
					if(!Ext.isEmpty(Ext.getCmp('factDistCapt_num_ne_pyme').getValue())){
						Ext.Ajax.request({
							url: '15admnafinparam01Ext.data.jsp',
							method: 'POST',
							callback: successAjaxFn,
							params: {
								informacion: 'FACT_DIST.NOMBRE_PYME' ,
								num_ne_pyme: Ext.getCmp('factDistCapt_num_ne_pyme').getValue()
							}
						});
					}
					//Muestro y oculto los componentes correspondientes
					Ext.getCmp('formaFactDistribuido').hide();
					Ext.getCmp('gridFactDistribuido').hide();
					Ext.getCmp('fpCapturaFactDistribuido').show();
					Ext.getCmp('factDistCapt.btnAgregar').hide();
					Ext.getCmp('factDistCapt.btnLimpiar').hide();
					Ext.getCmp('factDistCapt.btnCancelar').show();
					Ext.getCmp('factDistCapt.btnActualizar').show();
					Ext.getCmp('factDistCapt_num_ne_pyme').setDisabled(true);
					Ext.getCmp('factDistCapt_ic_epo').setDisabled(true);
					Ext.getCmp('factDistCapt_if').setDisabled(true);
					Ext.getCmp('factDistCapt_moneda').setDisabled(true);
					Ext.getCmp('factDistCapt_benef').setDisabled(true);
				}
			},{
				iconCls: 'borrar',
				handler: function(grid, rowIndex, colIndex, item, event){
					Ext.Msg.confirm('Mensaje de p�gina web','�Esta seguro que desea borrar esta cuenta?',
						function(btn){
							if(btn==='ok' || btn==='yes'){
								Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');
								var registro = grid.getStore().getAt(rowIndex);
								Ext.Ajax.request({
									url: '15admnafinparam01Ext.data.jsp',
									params: Ext.apply({
										informacion:   'FACT_DIST.ELIMINAR_CUENTA',
										icBenef:       registro.get('IC_BENEFICIARIO'),
										icConsecutivo: registro.get('IC_CONSECUTIVO')
									}),
									callback: cuentaEliminada
								});
							}
						}
					);
				}
			}]
		}],
		bbar: {
			xtype:           'paging',
			buttonAlign:     'left',
			id:              'barraPaginacionFactDistribuido',
			displayMsg:      '{0} - {1} de {2}',
			emptyMsg:        'No hay registros.',
			displayInfo:     true,
			store:           consultaData,
			pageSize:        15,
			items: [
				'->','-',
				{
					xtype:   'button',
					text:    'Generar Archivo',
					tooltip: 'Generar Archivo ',
					iconCls: 'icoXls',
					id:      'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15admnafinparam01Ext.data.jsp',
							params: Ext.apply({
								informacion:          'FACT_DIST.GENERAR_CSV',
								factDist_num_ne_pyme: Ext.getCmp('factDist_num_ne_pyme').getValue(),
								factDist_ic_epo:      Ext.getCmp('factDist_ic_epo').getValue(),
								factDist_if:          Ext.getCmp('factDist_if').getValue(),
								factDist_benef:       Ext.getCmp('factDist_benef').getValue(),
								factDist_moneda:      Ext.getCmp('factDist_moneda').getValue()
							}),
							callback: descargaArchivo
						});
					}
				}
			]
		}
	});

	//Form de B�squeda Avanzada
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id:                'fBusqAvanzada',
		bodyStyle:         'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:            'form',
		defaults:          {
			xtype:         'textfield',
			msgTarget:     'side',
			anchor:        '-20'
		},
		frame:             true,
		monitorValid:      true,
		labelWidth:        57,
		items:              [{
			xtype:          'textfield',
			name:           'nombrePyme',
			id:             'nombrePyme1',
			fieldLabel:     'Nombre',
			msgTarget:      'side',
			margins:        '0 20 0 0', //necesario para mostrar el icono de error
			allowBlank:     true,
			maxLength:      100,
			width:          80
		},{
			xtype:          'textfield',
			name:           'rfcPyme',
			id:             'rfcPyme1',
			fieldLabel:     'RFC',
			msgTarget:      'side',
			margins:        '0 20 0 0',
			allowBlank:     true,
			maxLength:      20,
			width:          80
		},{ 
			xtype:          'label',
			html:           'Utilice el * para realizar una b�squeda gen�rica.',
			cls:            'x-form-item',
			style:          {
				width:      '100%',
				textAlign:  'center'
			} 
		},{
			xtype:          'panel',
			bodyStyle:      'padding: 10px',
			layout: {
				type:       'hbox',
				pack:       'center',
				align:      'middle'
			},
			items:          [{
				xtype:      'button',
				text:       'Buscar',
				id:         'btnBuscar',
				iconCls:    'icoBuscar',
				width:      75,
				handler:    function(boton, evento) {
					var pymeComboCmp = Ext.getCmp('cbPyme1');
					pymeComboCmp.setValue('');
					pymeComboCmp.setDisabled(false);
					storeBusqAvanzPyme.load({
						params: {
							informacion: 'FACT_DIST.BUSQUEDA_AVANZADA',
							nombrePyme:  Ext.getCmp('nombrePyme1').getValue(),
							rfcPyme:     Ext.getCmp('rfcPyme1').getValue()
						}
					});
				},
				style: {
					marginBottom: '10px'
				}
			}]
		},{
			xtype:          'combo',
			name:           'cbPyme',
			id:             'cbPyme1',
			mode:           'local',
			displayField:   'descripcion',
			emptyText:      'Seleccione...',
			valueField:     'clave',
			hiddenName:     'cbPyme',
			fieldLabel:     'Nombre',
			triggerAction:  'all',
			typeAhead:      true,
			autoLoad:       false,
			disabled:       true,
			forceSelection: true,
			minChars:       1,
			store:          storeBusqAvanzPyme,
			tpl:            '<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}<\/div>'+
				'<\/tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}<\/div>' +
				'<\/div><\/tpl><\/tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		}],
		buttons:            [{
			text:           'Aceptar',
			iconCls:        'icoAceptar',
			formBind:       true,
			disabled:       true,
			handler:        function(boton, evento) {
				var cmbPyme= Ext.getCmp('cbPyme1');
				var ventana = Ext.getCmp('winBusqAvan');
				if (Ext.isEmpty(cmbPyme.getValue())) {
					cmbPyme.markInvalid('Seleccione Proveedor');
					return;
				}
				var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
				record = record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
				var a = [];
				a = record.split(" ",1);
				var nombre = record.substring(a[0].length,record.length);
				var formCaptura = Ext.getCmp('fpCapturaFactDistribuido');
				var num_ne_pyme = '';
				if(formCaptura.isVisible()){
					Ext.getCmp('factDistCapt_num_ne_pyme').setValue(cmbPyme.getValue());
					Ext.getCmp('factDistCapt_txtNombre').setValue(nombre);
					num_ne_pyme = Ext.getCmp('factDistCapt_num_ne_pyme').getValue();
				} else{
					Ext.getCmp('factDist_num_ne_pyme').setValue(cmbPyme.getValue());
					Ext.getCmp('factDist_txtNombre').setValue(nombre);
					num_ne_pyme = Ext.getCmp('factDist_num_ne_pyme').getValue();
				}
				Ext.Ajax.request({
					url: '15admnafinparam01Ext.data.jsp',
					method: 'POST',
					callback: successAjaxFn,
					params: {
						informacion: 'FACT_DIST.NOMBRE_PYME',
						num_ne_pyme: num_ne_pyme
					}
				});
				fpBusqAvanzada.getForm().reset();
				ventana.hide();
			}
		},{
			text:           'Cancelar',
			iconCls:        'icoLimpiar',
			handler:        function() {
				var ventana = Ext.getCmp('winBusqAvan');
				fpBusqAvanzada.getForm().reset();
				ventana.hide();
			}
		}]
	});

	//Form: Agregar/Modificar.
	var fpCapturaFactDistribuido = new Ext.form.FormPanel({
		id:                 'fpCapturaFactDistribuido',
		title:              'Criterios de Captura',
		style:              'margin:0 auto;',
		bodyStyle:          'padding-left:20px;,padding-top:10px;,text-align:left',
		defaults:           {
			msgTarget:      'side',
			anchor:         '-20'
		},
		collapsible:        false,
		titleCollapse:      false,
		frame:              true,
		hidden:             true,
		width:              700,
		labelWidth:         150,
		items:              [{
			xtype:          'compositefield',
			fieldLabel:     '*N�m. NE Proveedor',
			msgTarget:      'side',
			combineErrors:  false,
			items: [{
				xtype:      'textfield',
				name:       'factDistCapt_num_ne_pyme',
				id:         'factDistCapt_num_ne_pyme',
				msgTarget:  'side',
				margins:    '0 20 0 0',
				allowBlank: true,
				maxLength:  38,
				width:      55,
				regex:      /^[0-9]*$/,
				listeners:  {
					'blur': function(){
						if(!Ext.getCmp('fpCapturaFactDistribuido').getForm().isValid()){
							return;
						}
						if(!Ext.isEmpty(Ext.getCmp('factDistCapt_num_ne_pyme').getValue())){
							Ext.Ajax.request({
								url: '15admnafinparam01Ext.data.jsp',
								method: 'POST',
								callback: successAjaxFn,
								params: {
									informacion: 'FACT_DIST.NOMBRE_PYME' ,
									num_ne_pyme: Ext.getCmp('factDistCapt_num_ne_pyme').getValue()
								}
							});
						}
					}
				}
			},{
				xtype:      'textfield',
				name:       'factDistCapt_txtNombre',
				id:         'factDistCapt_txtNombre',
				msgTarget:  'side',
				margins:    '0 20 0 0',//Necesario para mostrar el icono de error
				allowBlank: true,
				disabled:   true,
				maxLength:  100,
				width:      270
			},{
				xtype:      'button',
				text:       'B�squeda Avanzada',
				iconCls:    'icoBuscar',
				id:         'factDistCapt.btnBusqAv',
				handler:    function(boton, evento) {
					var pymeComboCmp = Ext.getCmp('cbPyme1');
					pymeComboCmp.setValue('');
					pymeComboCmp.setDisabled(true);
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
							title:       'B�squeda Avanzada',
							buttonAlign: 'center',
							id:          'winBusqAvan',
							closeAction: 'hide',
							layout:      'fit',
							width:       400,
							height:      300,
							minWidth:    400,
							minHeight:   300,
							items:       fpBusqAvanzada
						}).show();
					}
				}
			}]
		},{
			xtype:          'combo',
			id:             'factDistCapt_ic_epo',
			name:           'factDistCapt_ic_epo',
			hiddenName:     'factDistCapt_ic_epo',
			fieldLabel:     '*Nombre de la EPO',
			emptyText:      'Seleccione una EPO',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			mode:           'local',
			forceSelection: true,
			typeAhead:      false,
			allowBlank:     true,
			minChars:       1,
			store:          catalogo_factDist_epo,
			tpl:            NE.util.templateMensajeCargaCombo,
			style:          {
				boder:      '0px'
			},
			listeners:      {
				'select': function(cbo){
					var cboIf = Ext.getCmp('factDistCapt_if');
					var cboBenef = Ext.getCmp('factDistCapt_benef');
					cboIf.setValue('');
					cboBenef.setValue('');
					cboIf.store.removeAll();
					cboBenef.store.removeAll();
					if(!Ext.isEmpty(cbo.getValue())) {
						cboIf.store.load({
							params: {
								ic_epo: cbo.getValue()
							}
						});
						cboBenef.store.load({
							params: {
								ic_epo: cbo.getValue()
							}
						});
					} else {
						cboIf.store.load();
						cboBenef.store.load();
					}
				}
			}
		},{
			xtype:          'combo',
			id:             'factDistCapt_if',
			name:           'factDistCapt_if',
			hiddenName:     'factDistCapt_if',
			fieldLabel:     '*Intermediario Financiero',
			emptyText:      'Seleccione IF',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			mode:           'local',
			forceSelection: true,
			typeAhead:      false,
			allowBlank:     true,
			minChars:       1,
			store:          catalogo_factDist_if,
			tpl:            NE.util.templateMensajeCargaCombo,
			style:          {
				boder:      '0px'
			}
		},{
			xtype:          'combo',
			id:             'factDistCapt_benef',
			name:           'factDistCapt_benef',
			hiddenName:     'factDistCapt_benef',
			fieldLabel:     '*Beneficiario',
			emptyText:      'Seleccione',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			mode:           'local',
			forceSelection: true,
			typeAhead:      false,
			allowBlank:     true,
			minChars:       1,
			store:          catalogo_factDist_benef,
			tpl:            NE.util.templateMensajeCargaCombo,
			style:          {
				boder:      '0px'
			}
		},{
			xtype:          'combo',
			id:             'factDistCapt_moneda',
			name:           'factDistCapt_moneda',
			hiddenName:     'factDistCapt_moneda',
			fieldLabel:     '*Moneda',
			emptyText:      'Seleccione una Moneda',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			mode:           'local',
			forceSelection: true,
			typeAhead:      false,
			allowBlank:     true,
			minChars:       1,
			store:          catalogo_factDist_moneda,
			tpl:            NE.util.templateMensajeCargaCombo,
			style:          {
				boder:      '0px'
			},
			listeners:      {
				'select': function(cbo){
					if(!Ext.isEmpty(cbo.getValue()) && cbo.getValue()==='1') {
						//Se muestran con moneda nacional
						Ext.getCmp('factDistCapt_cuentaClabe').show();
						Ext.getCmp('factDistCapt_cuentaClabeConf').show();
						//Se ocultan con moneda nacional
						Ext.getCmp('factDistCapt_cuentaSpid').hide();
						Ext.getCmp('factDistCapt_cuentaSpidConf').hide();
						Ext.getCmp('factDistCapt_swift').hide();
						Ext.getCmp('factDistCapt_aba').hide();
					} else if(!Ext.isEmpty(cbo.getValue()) && cbo.getValue()==='54') {
						//Se muestran con moneda nacional
						Ext.getCmp('factDistCapt_cuentaClabe').hide();
						Ext.getCmp('factDistCapt_cuentaClabeConf').hide();
						//Se ocultan con moneda nacional
						Ext.getCmp('factDistCapt_cuentaSpid').show();
						Ext.getCmp('factDistCapt_cuentaSpidConf').show();
						Ext.getCmp('factDistCapt_swift').show();
						Ext.getCmp('factDistCapt_aba').show();
					} else{
						//Se ocultan cuando no hay nada seleccionado
						Ext.getCmp('factDistCapt_cuentaClabe').hide();
						Ext.getCmp('factDistCapt_cuentaClabeConf').hide();
						Ext.getCmp('factDistCapt_cuentaSpid').hide();
						Ext.getCmp('factDistCapt_cuentaSpidConf').hide();
						Ext.getCmp('factDistCapt_swift').hide();
						Ext.getCmp('factDistCapt_aba').hide();
					}
					Ext.getCmp('factDistCapt_cuentaClabe').setValue('');
					Ext.getCmp('factDistCapt_cuentaClabeConf').setValue('');
					Ext.getCmp('factDistCapt_cuentaSpid').setValue('');
					Ext.getCmp('factDistCapt_cuentaSpidConf').setValue('');
					Ext.getCmp('factDistCapt_swift').setValue('');
					Ext.getCmp('factDistCapt_aba').setValue('');
				}
			}
		},{
			xtype:          'combo',
			id:             'factDistCapt_bancoServicio',
			name:           'factDistCapt_bancoServicio',
			hiddenName:     'factDistCapt_bancoServicio',
			fieldLabel:     '*Banco de servicio',
			emptyText:      'Seleccione un Banco',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			mode:           'local',
			forceSelection: true,
			typeAhead:      false,
			allowBlank:     true,
			minChars:       1,
			store:          catalogo_factDist_bancoServicio,
			tpl:            NE.util.templateMensajeCargaCombo,
			style:          {
				boder:      '0px'
			}
		},{
			xtype:          'combo',
			id:             'factDistCapt_plaza',
			name:           'factDistCapt_plaza',
			hiddenName:     'factDistCapt_plaza',
			fieldLabel:     'Plaza',
			emptyText:      'Seleccione una plaza',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			mode:           'local',
			forceSelection: true,
			typeAhead:      false,
			allowBlank:     true,
			minChars:       1,
			store:          catalogo_factDist_plaza,
			tpl:            NE.util.templateMensajeCargaCombo,
			style:          {
				boder:      '0px'
			}
		},{
			xtype:          'textfield',
			name:           'factDistCapt_sucursal',
			id:             'factDistCapt_sucursal',
			msgTarget:      'side',
			fieldLabel:     '*Sucursal',
			margins:        '0 20 0 0',//Necesario para mostrar el icono de error
			allowBlank:     true,
			maxLength:      100,
			width:          270
		},{
			xtype:          'textfield',
			name:           'factDistCapt_cuenta',
			id:             'factDistCapt_cuenta',
			msgTarget:      'side',
			fieldLabel:     '*No. de Cuenta',
			margins:        '0 20 0 0',//Necesario para mostrar el icono de error
			allowBlank:     true,
			maxLength:      11,
			width:          270
		},{
			xtype:          'textfield',
			name:           'factDistCapt_cuentaClabe',
			id:             'factDistCapt_cuentaClabe',
			msgTarget:      'side',
			fieldLabel:     '*Cuenta Clabe',
			vtype:          'digVerificador',
			margins:        '0 20 0 0',//Necesario para mostrar el icono de error
			allowBlank:     true,
			hidden:         true,
			maxLength:      18,
			width:          270
		},{
			xtype:          'textfield',
			name:           'factDistCapt_cuentaClabeConf',
			id:             'factDistCapt_cuentaClabeConf',
			msgTarget:      'side',
			fieldLabel:     '*Confirmar Cuenta Clabe',
			margins:        '0 20 0 0',//Necesario para mostrar el icono de error
			allowBlank:     true,
			hidden:         true,
			enableKeyEvents:true,
			maxLength:      18,
			width:          270,
			listeners:{
				keydown:function(obj, e){
					var c = e.keyCode;
					if(e.ctrlKey && c===86){
						e.stopEvent();
					}
				}
			}
		},{
			xtype:          'textfield',
			name:           'factDistCapt_cuentaSpid',
			id:             'factDistCapt_cuentaSpid',
			msgTarget:      'side',
			fieldLabel:     '*Cuenta Spid',
			margins:        '0 20 0 0',//Necesario para mostrar el icono de error
			allowBlank:     true,
			hidden:         true,
			maxLength:      20,
			width:          270
		},{
			xtype:          'textfield',
			name:           'factDistCapt_cuentaSpidConf',
			id:             'factDistCapt_cuentaSpidConf',
			msgTarget:      'side',
			fieldLabel:     '*Confirmar Cuenta Spid',
			regexText:      'Solo debe contener n�meros',
			margins:        '0 20 0 0',//Necesario para mostrar el icono de error
			allowBlank:     true,
			hidden:         true,
			enableKeyEvents:true,
			maxLength:      20,
			width:          270,
			listeners:{
				keydown:function(obj, e){
					var c = e.keyCode;
					if(e.ctrlKey && c===86){
						e.stopEvent();
					}
				}
			}
		},{
			xtype:          'textfield',
			name:           'factDistCapt_swift',
			id:             'factDistCapt_swift',
			msgTarget:      'side',
			fieldLabel:     'SWIFT',
			margins:        '0 20 0 0',//Necesario para mostrar el icono de error
			allowBlank:     true,
			hidden:         true,
			maxLength:      20,
			width:          270
		},{
			xtype:          'textfield',
			name:           'factDistCapt_aba',
			id:             'factDistCapt_aba',
			msgTarget:      'side',
			fieldLabel:     'ABA',
			margins:        '0 20 0 0',//Necesario para mostrar el icono de error
			allowBlank:     true,
			hidden:         true,
			maxLength:      20,
			width:          270
		},{
			xtype:          'textfield',
			name:           'factDistCapt_ic_pyme',
			id:             'factDistCapt_ic_pyme',
			msgTarget:      'side',
			fieldLabel:     'Proveedor',
			margins:        '0 20 0 0',
			hidden:         true
		},{
			xtype:          'textfield',
			name:           'factDistCapt_modificacion',
			id:             'factDistCapt_modificacion',
			fieldLabel:     'Es modificacion',
			margins:        '0 20 0 0',
			hidden:         true
		},{
			xtype:          'textfield',
			name:           'factDistCapt_icConsecutivo',
			id:             'factDistCapt_icConsecutivo',
			fieldLabel:     'Consecutivo',
			margins:        '0 20 0 0',
			hidden:         true
		},gridContratos
		],
		buttons: [{
			width:          100,
			text:           'Agregar',
			id:             'factDistCapt.btnAgregar',
			iconCls:        'aceptar',
			handler:        function(boton, evento) {
				//Valida campos obligatorios
				var numErrores = validarCamposObligatorios('AGREGAR');
				if(numErrores>0){
					return;
				}
				//Valida longitud y tipo de los campos
				if(!Ext.getCmp('fpCapturaFactDistribuido').getForm().isValid()){
					return;
				}
				//Valida los campos de confirmaci�n
				var errores = validaActualizaciones();
				if(errores===false) {
					var mensaje = validaContratosDuplicados();
					if(mensaje!=='') {
						Ext.MessageBox.alert('Mensaje de la p�gina web',mensaje);
						return;
					}
				}
				if(errores===false) {
					Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '15admnafinparam01Ext.data.jsp',
						params: {
							informacion:  'FACT_DIST.VALIDA_CTA_DUPLICADA',
							icConsecutivo:Ext.getCmp('factDistCapt_icConsecutivo').getValue(),
							ic_pyme:      Ext.getCmp('factDistCapt_ic_pyme').getValue(),
							ic_epo:       Ext.getCmp('factDistCapt_ic_epo').getValue(),
							ic_if:        Ext.getCmp('factDistCapt_if').getValue(),
							benef:        Ext.getCmp('factDistCapt_benef').getValue(),
							moneda:       Ext.getCmp('factDistCapt_moneda').getValue()
						},
						callback: cuentaNuevaValidada
					});
				}
			}
		},{
			width:          100,
			text:           'Aceptar',
			id:             'factDistCapt.btnActualizar',
			iconCls:        'aceptar',
			
			handler:        function(boton, evento) {
				//Valida campos obligatorios
				var numErrores = validarCamposObligatorios('AGREGAR');
				if(numErrores>0){
					return;
				}
				//Valida longitud y tipo de los campos
				if(!Ext.getCmp('fpCapturaFactDistribuido').getForm().isValid()){
					return;
				}
				//Valida los campos de confirmaci�n
				var errores = validaActualizaciones();
				if(errores===false) {
					var mensaje = validaContratosDuplicados();
					if(mensaje!=='') {
						Ext.MessageBox.alert('Mensaje de la p�gina web',mensaje);
						return;
					}
				}
				if(errores===false) {
					Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');
					var jsonData = consGridContratos.data.items;
					var cg_contratos = [];
					Ext.each(jsonData, function(item,index,arrItem) {
						cg_contratos.push(item.data.CG_NUM_CONTRATO);
					});
					Ext.Ajax.request({
						url: '15admnafinparam01Ext.data.jsp',
						params: {
							informacion:   'FACT_DIST.MODIFICAR',
							num_ne_pyme:   Ext.getCmp('factDistCapt_num_ne_pyme').getValue(),
							ic_pyme:       Ext.getCmp('factDistCapt_ic_pyme').getValue(),
							ic_epo:        Ext.getCmp('factDistCapt_ic_epo').getValue(),
							ic_if:         Ext.getCmp('factDistCapt_if').getValue(),
							benef:         Ext.getCmp('factDistCapt_benef').getValue(),
							moneda:        Ext.getCmp('factDistCapt_moneda').getValue(),
							bancoServicio: Ext.getCmp('factDistCapt_bancoServicio').getValue(),
							plaza:         Ext.getCmp('factDistCapt_plaza').getValue(),
							sucursal:      Ext.getCmp('factDistCapt_sucursal').getValue(),
							cuenta:        Ext.getCmp('factDistCapt_cuenta').getValue(),
							cuentaClabe:   Ext.getCmp('factDistCapt_cuentaClabe').getValue(),
							cuentaSpid:    Ext.getCmp('factDistCapt_cuentaSpid').getValue(),
							swift:         Ext.getCmp('factDistCapt_swift').getValue(),
							aba:           Ext.getCmp('factDistCapt_aba').getValue(),
							icConsecutivo: Ext.getCmp('factDistCapt_icConsecutivo').getValue(),
							cg_contratos:  cg_contratos
						},
						callback: cuentaModificada
					});
				}
			}
		},{
			width:          100,
			text:           'Limpiar',
			id:             'factDistCapt.btnLimpiar',
			iconCls:        'icoLimpiar',
			handler:        function(boton, evento) {
				limpiarCamposDeCaptura();
			}
		},{
			width:          100,
			text:           'Cancelar',
			id:             'factDistCapt.btnCancelar',
			iconCls:        'cancelar',
			handler:        function(boton, evento) {
				confirmaCancelar();
			}
		}]
	});

	//Form: Criterios de b�squeda.
	var fpFactDistribuido = new Ext.form.FormPanel({
		id:                 'formaFactDistribuido',
		title:              'Criterios de B�squeda',
		style:              'margin:0 auto;',
		bodyStyle:          'padding-left:20px;,padding-top:10px;,text-align:left',
		defaults:           {
			msgTarget:      'side',
			anchor:         '-20'
		},
		collapsible:        false,
		titleCollapse:      false,
		frame:              true,
		width:              700,
		labelWidth:         150,
		items:              [{
			xtype:          'compositefield',
			fieldLabel:     'N�m. NE Proveedor',
			msgTarget:      'side',
			combineErrors:  false,
			items: [{
				xtype:      'textfield',
				name:       'factDist_num_ne_pyme',
				id:         'factDist_num_ne_pyme',
				msgTarget:  'side',
				margins:    '0 20 0 0',
				allowBlank: true,
				maxLength:  38,
				width:      55,
				regex:      /^[0-9]*$/,
				listeners:  {
					'blur': function(){
						if(!Ext.getCmp('formaFactDistribuido').getForm().isValid()){
							return;
						}
						if(!Ext.isEmpty(Ext.getCmp('factDist_num_ne_pyme').getValue())){
							Ext.Ajax.request({
								url: '15admnafinparam01Ext.data.jsp',
								method: 'POST',
								callback: successAjaxFn,
								params: {
									informacion: 'FACT_DIST.NOMBRE_PYME' ,
									num_ne_pyme: Ext.getCmp('factDist_num_ne_pyme').getValue()
								}
							});
						}
					}
				}
			},{
				xtype:      'textfield',
				name:       'factDist_txtNombre',
				id:         'factDist_txtNombre',
				msgTarget:  'side',
				margins:    '0 20 0 0',//Necesario para mostrar el icono de error
				allowBlank: true,
				disabled:   true,
				maxLength:  100,
				width:      270
			},{
				xtype:      'button',
				text:       'B�squeda Avanzada',
				iconCls:    'icoBuscar',
				id:         'factDist.btnBusqAv',
				handler:    function(boton, evento) {
					var pymeComboCmp = Ext.getCmp('cbPyme1');
					pymeComboCmp.setValue('');
					pymeComboCmp.setDisabled(true);
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
							title:          'B�squeda Avanzada',
							buttonAlign:    'center',
							id:             'winBusqAvan',
							closeAction:    'hide',
							layout:         'fit',
							width:          400,
							height:         300,
							minWidth:       400,
							minHeight:      300,
							items:          fpBusqAvanzada
						}).show();
					}
				}
			}]
		},{
			xtype:          'combo',
			id:             'factDist_ic_epo',
			name:           'factDist_ic_epo',
			hiddenName:     'factDist_ic_epo',
			fieldLabel:     'Nombre de la EPO',
			emptyText:      'Seleccione una EPO',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			mode:           'local',
			forceSelection: true,
			typeAhead:      false,
			allowBlank:     true,
			minChars:       1,
			store:          catalogo_factDist_epo,
			tpl:            NE.util.templateMensajeCargaCombo,
			style:          {
				boder:      '0px'
			},
			listeners:      {
				'select': function(cbo){
					var cboIf = Ext.getCmp('factDist_if');
					var cboBenef = Ext.getCmp('factDist_benef');
					cboIf.setValue('');
					cboBenef.setValue('');
					cboIf.store.removeAll();
					cboBenef.store.removeAll();
					if(!Ext.isEmpty(cbo.getValue())) {
						cboIf.store.load({
							params: {
								ic_epo: cbo.getValue()
							}
						});
						cboBenef.store.load({
							params: {
								ic_epo: cbo.getValue()
							}
						});
					} else {
						cboIf.store.load();
						cboBenef.store.load();
					}
				}
			}
		},{
			xtype:          'combo',
			id:             'factDist_if',
			name:           'factDist_if',
			hiddenName:     'factDist_if',
			fieldLabel:     'Intermediario Financiero',
			emptyText:      'Seleccione IF',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			mode:           'local',
			forceSelection: true,
			typeAhead:      false,
			allowBlank:     true,
			minChars:       1,
			store:          catalogo_factDist_if,
			tpl:            NE.util.templateMensajeCargaCombo,
			style:          {
				boder:      '0px'
			}
		},{
			xtype:          'combo',
			id:             'factDist_benef',
			name:           'factDist_benef',
			hiddenName:     'factDist_benef',
			fieldLabel:     'Beneficiario',
			emptyText:      'Seleccione',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			mode:           'local',
			forceSelection: true,
			typeAhead:      false,
			allowBlank:     true,
			minChars:       1,
			store:          catalogo_factDist_benef,
			tpl:            NE.util.templateMensajeCargaCombo,
			style:          {
				boder:      '0px'
			}
		},{
			xtype:          'combo',
			id:             'factDist_moneda',
			name:           'factDist_moneda',
			hiddenName:     'factDist_moneda',
			fieldLabel:     'Moneda',
			emptyText:      'Seleccione una Moneda',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			mode:           'local',
			forceSelection: true,
			typeAhead:      false,
			allowBlank:     true,
			minChars:       1,
			store:          catalogo_factDist_moneda,
			tpl:            NE.util.templateMensajeCargaCombo,
			style:          {
				boder:      '0px'
			}
		}],
		buttons: [{
			width:          100,
			text:           'Cosultar',
			id:             'factDist.btnConsultar',
			iconCls:        'icoBuscar',
			handler:        function(boton, evento) {
				if(!Ext.getCmp('formaFactDistribuido').getForm().isValid()){
					return;
				}
				Ext.getCmp('tp').el.mask('Enviando...', 'x-mask-loading');
				consultaData.load({
					params: Ext.apply({
						informacion:          'FACT_DIST.CONSULTAR',
						operacion:            'GENERAR',
						start:                0,
						limit:                15,
						factDist_num_ne_pyme: Ext.getCmp('factDist_num_ne_pyme').getValue(),
						factDist_ic_epo:      Ext.getCmp('factDist_ic_epo').getValue(),
						factDist_if:          Ext.getCmp('factDist_if').getValue(),
						factDist_benef:       Ext.getCmp('factDist_benef').getValue(),
						factDist_moneda:      Ext.getCmp('factDist_moneda').getValue()
					})
				});
			}
		},{
			width:          100,
			text:           'Limpiar',
			id:             'factDist.btnLimpiar',
			iconCls:        'icoLimpiar',
			handler:        function(boton, evento) {
				limpiaCriteriosDeBusqueda();
				Ext.getCmp('gridFactDistribuido').hide();
			}
		},{
			width:          100,
			text:           'Agregar Cuenta',
			id:             'factDist.btnAgregar',
			iconCls:        'aceptar',
			handler:        function(boton, evento) {
				//Me aseguro que no haya llaves primarias guardadas
				Ext.getCmp('factDistCapt_icConsecutivo').setValue('');
				//Muestro solo los componentes necesarios
				Ext.getCmp('fpCapturaFactDistribuido').setTitle('Criterios de Captura');
				Ext.getCmp('factDistCapt_moneda').setValue('1');
				Ext.getCmp('factDistCapt_cuentaClabe').show();
				Ext.getCmp('factDistCapt_cuentaClabeConf').show();
				Ext.getCmp('factDistCapt_cuentaSpid').hide();
				Ext.getCmp('factDistCapt_cuentaSpidConf').hide();
				Ext.getCmp('factDistCapt_swift').hide();
				Ext.getCmp('factDistCapt_aba').hide();
				Ext.getCmp('formaFactDistribuido').hide();
				Ext.getCmp('gridFactDistribuido').hide();
				Ext.getCmp('fpCapturaFactDistribuido').show();
				Ext.getCmp('factDistCapt.btnActualizar').hide();
				Ext.getCmp('factDistCapt.btnAgregar').show();
				Ext.getCmp('factDistCapt.btnLimpiar').show();
				Ext.getCmp('factDistCapt.btnCancelar').show();
				//Habilito los campos correspondientes
				Ext.getCmp('factDistCapt_num_ne_pyme').setDisabled(false);
				Ext.getCmp('factDistCapt_ic_epo').setDisabled(false);
				Ext.getCmp('factDistCapt_if').setDisabled(false);
				Ext.getCmp('factDistCapt_moneda').setDisabled(false);
				Ext.getCmp('factDistCapt_benef').setDisabled(false);
			}
		}]
	});

//----- FODEA 016_2018 TERMINA -----//

	var gridDescuento = new Ext.grid.GridPanel({
		store: 		registrosDescuentoData,
		id:			'gridCDescuento',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		margins:		'20 0 0 0',
		title:		undefined,
		//view:			new Ext.grid.GridView({forceFit:	true}),
		stripeRows: true,
		loadMask: 	true,
		height: 		400,
		width: 		930,
		frame: 		true,
		columns: [
			{
				header: 		'Banco de servicio',
				tooltip: 	'Banco de servicio',
				dataIndex: 	'CG_BANCO',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
         },{
				header: 		'Sucursal',
				tooltip: 	'Sucursal',
				dataIndex: 	'CG_SUCURSAL',
				sortable: 	true,
				resizable: 	true,
				width: 		70,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
         },{
				header: 		'Moneda',
				tooltip: 	'Moneda',
				dataIndex: 	'CD_NOMBRE',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{					
				header: 		'No. de Cuenta',
				tooltip: 	'No. de Cuenta',
				dataIndex: 	'CG_NUMERO_CUENTA',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
         },{
				header: 		'Cuenta Clabe',
				tooltip: 	'Cuenta Clabe',
				dataIndex: 	'CG_CUENTA_CLABE',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
         },{
				header: 		'Plaza',
				tooltip: 	'Plaza',
				dataIndex: 	'PLAZA',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
         },{
				header: 		'Convenio �nico',
				tooltip: 	'Convenio �nico',
				dataIndex: 	'CS_CONVENIO_UNICO',
				align:		'center',
				sortable: 	false,
				resizable: 	false,
				width: 		150,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
         },{
				xtype:	'actioncolumn',
				header:       'Modificar',
				menuDisabled: true,
				hidden:	(varGlobal.cvePerf)=="8"?true:false,
				hideable:     (varGlobal.cvePerf)=="8"?true:false,
				dataIndex:    '',
				align:        'center',
				sortable:     true,
				resizable:    true,
				width:        80,
				items: [{
					iconCls:   'modificar',
					tooltip:   'Modificar',
					handler:   function(grid, rowIndex, colIndex, item, event){
										var record = registrosDescuentoData.getAt(rowIndex);
										var respuesta				=	new Object();
										respuesta["clavePyme"]	=	record.json['IC_PYME'];
						respuesta["ctaBancaria"] = record.json['IC_CUENTA_BANCARIA'];
										respuesta["txtNE"]		=	Ext.getCmp("aux_txtNE").getValue();
										Ext.getCmp("_txtNE").setValue(Ext.getCmp("aux_txtNE").getValue());
										Ext.getCmp("_txtIcCtaBanco").setValue(record.json['IC_CUENTA_BANCARIA']);
										accionArealizar("BOTON_MODIFICAR_DESCUENTO",respuesta);
									}
				}]
					},{
				xtype:        'actioncolumn',
				header:       'Eliminar',
				menuDisabled: true,
				hidden:       (varGlobal.cvePerf)=="8"?true:false,
				hideable:     (varGlobal.cvePerf)=="8"?true:false,
				dataIndex:    '',
				align:        'center',
				sortable:     true,
				resizable:    true,
				width:        80,
				items: [{
					iconCls:  'borrar',
					tooltip:  'Eliminar',
					handler:  function(grid, rowIndex, colIndex, item, event){
										Ext.getCmp("_txtNE").setValue(Ext.getCmp("aux_txtNE").getValue());
										Ext.Msg.confirm('Mensaje de p�gina web','�Esta seguro que desea borrar esta cuenta?',
											function(btn){
												if(btn=='ok' || btn=='yes'){
													var record = registrosDescuentoData.getAt(rowIndex);
													var respuesta				=	new Object();
													respuesta["clavePyme"]	=	record.json['IC_PYME'];
									respuesta["ctaBancaria"] = record.json['IC_CUENTA_BANCARIA'];
													respuesta["txtNE"]		=	Ext.getCmp("_txtNE").getValue();
													Ext.getCmp("_txtIcCtaBanco").setValue(record.json['IC_CUENTA_BANCARIA']);
													accionArealizar("BOTON_ELIMINAR_DESCUENTO",respuesta);
												}
											}
										);
									}
				}]
			}
		]
	});
	
	var regDescuentoDEData = new Ext.data.JsonStore({
		fields: [
			{name: 'TIPO_AFILIADO'	},
			{name: 'IC_EPO'	},
			{name: 'NOMBRE_EPO'	},			
			{name: 'IC_CUENTA'	},
			{name: 'BANCO'	},
			{name: 'CUENTA_CLABE'	},
			{name: 'PLAZA'	},
			{name: 'SUCURSAL'	},
			{name: 'IC_SWIFT'	},
			{name: 'IC_ABA'	},			
			{name: 'SELECCIONAR'	}		
		],		
		autoLoad:	false,
		listeners:	{exception: NE.util.mostrarDataProxyError}
	});
	
	
	var selectModel = new Ext.grid.CheckboxSelectionModel( {		
		checkOnly: true,
		listeners: {
			rowdeselect: function(selectModel, rowIndex, record) {		
				record.data['SELECCION']='N';
				record.commit();		
				
			},		
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ 
				record.data['SELECCION']='S';
				record.commit();						
			}
		}
	});
	
	var gridDescuentoDE = new Ext.grid.GridPanel({
		store: 		regDescuentoDEData,
		id:			'gridDescuentoDE',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		margins:		'20 0 0 0',		
		stripeRows: true,
		loadMask: 	true,
		height: 		200,
		width: 		490,
		frame: 		true,
		columns: [
			{
				header: 		'Tipo Afiliado',
				tooltip: 	'Tipo Afiliado',
				dataIndex: 	'TIPO_AFILIADO',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				align: 'center'
         },
			{
				header: 		'EPO',
				tooltip: 	'EPO',
				dataIndex: 	'NOMBRE_EPO',
				sortable: 	true,
				resizable: 	true,
				align: 'left',
				width: 		300,				
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
					metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
					return value;
				}
         },
			{
				header: 		'Banco',
				tooltip: 	'Banco',
				dataIndex: 	'BANCO',
				sortable: 	true,
				resizable: 	true,
				align: 'left',
				width: 		150,				
				hidden:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
					metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
					return value;
				}
         },
			{
				header: 		'Cuenta CLABE',
				tooltip: 	'Cuenta CLABE',
				dataIndex: 	'CUENTA_CLABE',
				sortable: 	true,
				resizable: 	true,
				align: 'center',
				width:150,
				hidden:true
         },
			{
				header: 		'Plaza',
				tooltip: 	'Plaza',
				dataIndex: 	'PLAZA',
				sortable: 	true,
				resizable: 	true,
				align: 'center',
				width:150,
				hidden:true
         },
			{
				header: 		'Sucursal',
				tooltip: 	'Sucursal',
				dataIndex: 	'SUCURSAL',
				sortable: 	true,
				resizable: 	true,
				align: 'center',
				width:150,
				hidden:true
         },
			{
				header: 		'Swift',
				tooltip: 	'Swift',
				dataIndex: 	'IC_SWIFT',
				sortable: 	true,
				resizable: 	true,
				align: 'center',
				width:150,
				hidden:true
         },
			{
				header: 		'Aba',
				tooltip: 	'Aba',
				dataIndex: 	'IC_ABA',
				sortable: 	true,
				resizable: 	true,
				align: 'center',
				width:150,
				hidden:true
         },
			selectModel			
		],
		sm:selectModel,	
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Aceptar',					
					tooltip:	'Aceptar',
					iconCls: 'aceptar',
					id: 'btnAceptarDisp',
					handler: function(boton, evento) {				
						accionArealizar("BOTON_ACEPTAR_DISP_EPO",null);	
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Cancelar',					
					tooltip:	'Cancelar',
					iconCls: 'icoRechazar',
					id: 'btnCancelar',
					handler: function(boton, evento) {				
						window.location = '15admnafinparam01Ext.jsp';
					}
				}
			]
		}
	});
			
	var elementosDispEPO = [
		{
			xtype: 'displayfield',
			id: 'lblTipo',
			style: 'text-align:left;',
			fieldLabel: 'Tipo ',
			text: '-'
		},	
		{
			xtype: 'displayfield',
			id: 'lblProducto',
			style: 'text-align:left;',
			fieldLabel: 'Producto ',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblnaPyme',
			style: 'text-align:left;',
			fieldLabel: 'N@E Pyme ',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblNombrePyme',
			style: 'text-align:left;',
			fieldLabel: 'Nombre Pyme ',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblRFCPyme',
			style: 'text-align:left;',
			fieldLabel: 'RFC Pyme ',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblMoneda',
			style: 'text-align:left;',
			fieldLabel: 'Moneda ',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblCuentaClabe',
			style: 'text-align:left;',
			fieldLabel: 'Cuenta CLABE ',
			text: '-'
		},
		{
			xtype:			'combo',
			id:				'_ic_bancos_tef',
			name:				'ic_bancos_tef',
			hiddenName:		'ic_bancos_tef',
			fieldLabel:		'Banco de Servicio',
			emptyText:		'Seleccione un Banco',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		true,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoBancoTefData,
			tpl:				NE.util.templateMensajeCargaCombo
		},
		{
			xtype:			'textfield',
			fieldLabel:		'Cuenta',
			name:				'cg_CuentaDips',
			id:				'_cg_CuentaDips',
			maskRe:			/[0-9]/,
			regex:			/^\d*$/,
			regexText:		'Favor de proporcionar solamente d�gitos en el n�mero de cuenta',
			allowBlank:		true,
			maxLength:		20,
			anchor:			'50%'
		},
		{
			xtype:			'textfield',
			fieldLabel:		'Plaza',
			name:				'txtPlazaDisp',
			id:				'_txtPlazaDisp',
			allowBlank:		true,
			maxLength:		20,
			anchor:			'50%'			
		}, 
		{
			xtype:			'textfield',
			fieldLabel:		'Sucursal',
			name:				'txtSucursalDisp',
			id:				'_txtSucursalDisp',
			allowBlank:		true,
			maxLength:		20,
			anchor:			'50%'			
		}, 
		{
			xtype:			'textfield',
			fieldLabel:		'Swift',
			name:				'ic_Swift',
			id:				'_ic_Swift',
			//maskRe:			/[0-9]/,
			//regex:			/^\d*$/,
			//regexText:		'Favor de proporcionar solamente d�gitos en el n�mero de cuenta',
			allowBlank:		true,
			maxLength:		20,
			anchor:			'50%'
		},
		{
			xtype:			'textfield',
			fieldLabel:		'Aba',
			name:				'ic_aba',
			id:				'_ic_aba',
			maskRe:			/[0-9]/,
			regex:			/^\d*$/,
			regexText:		'Favor de proporcionar solamente d�gitos en el n�mero de cuenta',
			allowBlank:		true,
			maxLength:		20,
			anchor:			'50%'
		}		
	];
	
	
	var fpDispEpo = new Ext.form.FormPanel({
		id:				'fpDispEpo',		
		width:			490,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			true,
		labelWidth:		110,
		hidden:true,
		bodyStyle:		'padding-left:20px;,padding-top:10px;,text-align:left',
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			[elementosDispEPO]
	});
	

	var elementosDescuento = [
		{
			xtype:			'textfield',
			fieldLabel:		'N�m NE PyME',
			name:				'txtNE',
			id:				'_txtNE',
			allowBlank:		true,
			maskRe:			/^\d*$/,
			regex:			/^\d*$/,
			regexText:		'Favor de proporcionar solamente d�gitos en el N�m NE PyME',
			maxLength:		40,
			listeners:{
				blur: function(obj){
					if(obj.getValue()!='' && obj.isValid()){
						Ext.Ajax.request({
							url: '15admnafinparam01Ext.data.jsp',
							params:	{	informacion:	'getClavePyme', txtNE:obj.getValue() },
							callback: function(opts, success, response) {
								if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
									var resp = 	Ext.util.JSON.decode(response.responseText);
									if(resp.sPyme==''){
										Ext.Msg.alert('Aviso','N�mero PYME inexistente');
										obj.setValue('');
									}
								}else{
									NE.util.mostrarConnError(response,opts);
								}
							}
						});
					}
				}
			}
		},{
			xtype:			'combo',
			id:				'_txtBanco',
			name:				'txtBanco',
			hiddenName:		'txtBanco',
			fieldLabel:		'Banco de Servicio',
			emptyText:		'Seleccione un Banco',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		true,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoBancoData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'textfield',
			fieldLabel:		'Sucursal',
			name:				'txtSucursal',
			id:				'_txtSucursal',			
			allowBlank:		true,
			maxLength:		40
		},{
			xtype:			'combo',
			id:				'_cboMoneda',
			name:				'cboMoneda',
			hiddenName:		'cboMoneda',
			fieldLabel:		'Moneda',
			emptyText:		'Seleccione una Moneda',	
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			triggerAction:	'all',
			typeAhead:		true,
			minChars:		1,
			store:			catalogoMonedaData,
			tpl:				NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						if(combo.getValue()==1 )  {
							Ext.getCmp('_confirtxtCuentaClabe').show();
						}else  {
							Ext.getCmp('_confirtxtCuentaClabe').hide(); 
						}
					}
				}
			}
		},
		{
			xtype:			'textfield',
			fieldLabel:		'No. de Cuenta',
			name:				'txtCuenta',
			id:				'_txtCuenta',
			maskRe:			/[0-9]/,
			regex:			/^\d*$/,
			regexText:		'Favor de proporcionar solamente d�gitos en el n�mero de cuenta',
			allowBlank:		true,
			maxLength:		20,
			anchor:			'50%'
		},		
		{
			xtype:			'textfield',
			fieldLabel:		'Plaza',
			name:				'cboPlaza',
			id:				'_cboPlaza',
			allowBlank:		true,
			maxLength:		40,
			anchor:			'50%'			
		}, 
		{
			xtype:			'textfield',
			fieldLabel:		'Cuenta CLABE',
			name:				'txtCuentaClabe',
			id:				'_txtCuentaClabe',
			allowBlank:		true,
			maxLength:		18,
			anchor:			'50%',
			//readOnly : true ,
			vtype:			'digVerificador',
			listeners:{
				/*keypress: function(txtField, e) {
					var key = e.getKey();				
					if(key == Ext.EventObject.ctrlKey = e.ctrlKey || key == Ext.EventObject.metaKey ){
						ev.preventDefault();
					}			
				}
				*/
			}
		},
		{
			xtype:			'textfield',
			fieldLabel:		'Confirmaci�n Cuenta CLABE', 
			name:				'confirtxtCuentaClabe',
			id:				'_confirtxtCuentaClabe',
			allowBlank:		true,
			maxLength:		18,
			anchor:			'50%',
			//readOnly : true ,
			vtype:			'confDigVerificador',
			enableKeyEvents: true,
      listeners:{
          keydown:function(obj, e){
            var c = e.keyCode;
            if(e.ctrlKey && c==86){
              e.stopEvent();
            }
          }
			}
		},	
		{
			xtype:			'checkbox',
			fieldLabel:		'Convenio �nico',
			boxLabel:		'(S�lo se podr� seleccionar una cuenta bancaria)',
			name:				'chkCU',
			id:				'_chkCU',
			value:			'S'
		},{
			xtype:			'hidden',
			id:				'_txtIcCtaBanco',
			name:				'txtIcCtaBanco',
			value:			null
		},{
			xtype:			'hidden',
			id:				'_txtAction',
			name:				'txtAction',
			value:			null
		},{
			xtype:			'hidden',
			id:				'aux_txtNE',
			name:				'aux_txtNE',
			value:			null
		}	];

	var fpDescuento = new Ext.form.FormPanel({
		id:				'formaDescuento',
		title:			undefined,
		width:			700,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			true,
		labelWidth:		110,
		bodyStyle:		'padding-left:20px;,padding-top:10px;,text-align:left',
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			[elementosDescuento],
		buttons: [
			{
				text:		'Agregar',
				id:		'desc.btnAgregar',
				iconCls:	'aceptar',
				width:	100,
				handler: function(boton, evento) {

								if(!Ext.getCmp('formaDescuento').getForm().isValid()){
									return;
								}
								var txtNE = Ext.getCmp("_txtNE").getValue();
								submitCuentaDescuento(1,0,txtNE);

				} //fin handler
			},
			{
				text:		'Cancelar',
				id:		'desc.btnCancelar',
				iconCls:	'icoRechazar',
				width:	100,
				handler: function(boton, evento) {
								accionArealizar("BOTON_CANCELAR_DESCUENTO",null);
				} //fin handler
			},
			{
				text:		'Consultar',
				id:		'desc.btnConsultar',
				iconCls:	'icoBuscar',
				width:	100,
				handler: function(boton, evento) {

								if(!Ext.getCmp('formaDescuento').getForm().isValid()){
									return;
								}
								var txtNE = Ext.getCmp("_txtNE").getValue();
								submitCuentaDescuento(4,0,txtNE);
				} //fin handler
			},{
				text:		'Cancelar',
				id:		'desc.btnCancelar',
				iconCls:	'icoRechazar',
				width:	100,
				handler: function(boton, evento) {
								accionArealizar("BOTON_CANCELAR_DESCUENTO",null);
				} //fin handler
			},{
				text:		'Regresar',
				id:		'desc.btnRegresar',
				iconCls:	'icoLimpiar',
				width:	100,
				hidden:	true,
				handler: function(boton, evento) {
								accionArealizar("FIN",null);
				} //fin handler
			},{
				text:		'Actualizar',
				id:		'desc.btnActualizar',
				iconCls:	'icoAceptar',
				width:	100,
				hidden:	true,
				handler: function(boton, evento) {

								if(	!(Ext.getCmp('formaDescuento').getForm().isValid())	){
									return;
								}
								submitCuentaDescuentoActualiza(3,0);
				} //fin handler
			}
		]
	});

	var tp = new Ext.TabPanel({
		id:			'tp',
		activeTab:	0,
		//activeTab:	(Ext.isEmpty(tabActiva))?0:1,
		width:		940,
		plain:		true,
		defaults:	{autoHeight: true},
		items:[
			{
				title:	'Descuento',
				id:		'tabDescuento',
				style:	'margin:0 auto;',
				items: [
					NE.util.getEspaciador(10),
					fpDescuento,
					fpDispEpo,
					NE.util.getEspaciador(10),
					gridDescuento,
					gridDescuentoDE,
					NE.util.getEspaciador(10)
				]
			},{
				title:	'Distribuidores',
				id:		'tabDistribuidores',
				style:	'margin:0 auto;',
				items: [
					NE.util.getEspaciador(10),
					fpDistribuidores,
					{
						xtype:	'label',
						id:		'lblFiso',
						cls:		'x-form-item',
						style: 	'text-align:left;margin:12px;',
						html:		'* Solo cuando el IF es FISO'
					},
					NE.util.getEspaciador(5),
					gridDistribuidores,
					NE.util.getEspaciador(10)
				]
			},
//----- FODEA 016_2018 INICIA -----
			{
				title: 'Factoraje Distribuido',
				id:    'tabFactDistribuido',
				style: 'margin:0 auto;',
				items: [
					NE.util.getEspaciador(10),
					fpFactDistribuido,
					fpCapturaFactDistribuido,
					gridAcuse,
					NE.util.getEspaciador(5),
					gridFactDistribuido,
					gridFactDistribuidoTmp,
					gridAcuseDetalle,
					NE.util.getEspaciador(10)
				]
			}
//----- FODEA 016_2018 TERMINA -----//
		],
		listeners:{
			tabchange:	function(tab, panel){

								if (panel.getItemId() == 'tabDescuento') {

										accionArealizar("BOTON_CANCELAR_DESCUENTO",null);

								}else if (panel.getItemId() == 'tabDistribuidores') {

									accionArealizar("BOTON_LIMPIAR_DISTRIBUIDORES",null);

								} else if (panel.getItemId() === 'tabFactDistribuido') {//FODEA 016_2018
									limpiarCamposDeCaptura();
									limpiaCriteriosDeBusqueda();
									Ext.getCmp('factDistCapt_icConsecutivo').setValue('');
									Ext.getCmp('gridAcuse').hide();
									Ext.getCmp('gridAcuseDetalle').hide();
									Ext.getCmp('fpCapturaFactDistribuido').hide();
									Ext.getCmp('gridFactDistribuidoTmp').hide();
									Ext.getCmp('gridFactDistribuido').hide();
									Ext.getCmp('formaFactDistribuido').show();
								}
							}
		}
	});

//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		renderHidden:	true,
		height: 	'auto',
		items: 	[tp],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("tp").getEl();
			if( element.isMasked()){
				element.unmask();
			}

         element = Ext.getCmp("formaDescuento").getEl();
			if(Ext.getCmp("formaDescuento").isVisible()){
				if( element.isMasked()){
					element.unmask();
				}
			}

         element = Ext.getCmp("formaDistribuidores").getEl();
			if(Ext.getCmp("formaDistribuidores").isVisible()){
				if( element.isMasked()){
					element.unmask();
				}
			}
			// Derefenrencia ultimo elemento...
			element = null;

		}

	});


	Ext.StoreMgr.key('catalogoBancoDataStore').load();
	Ext.StoreMgr.key('catalogoMonedaDataStore').load();
	Ext.StoreMgr.key('catalogoPlazaDataStore').load();	
	Ext.StoreMgr.key('catalogoBancoTefDataStore').load();		
	accionArealizar("parametrizacion.getClavePerfil",null);
	 

});