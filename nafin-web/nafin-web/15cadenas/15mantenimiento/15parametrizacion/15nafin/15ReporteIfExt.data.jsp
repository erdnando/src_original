<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		com.netro.anticipos.*,	
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	if (informacion.equals("CatalogoEpo")){
		String intermediario	= (request.getParameter("intermediario") != null) ? request.getParameter("intermediario") :"";
		
		List cat = new ArrayList(); 
		JSONArray registros = new JSONArray();
		JSONObject jsonObj = new JSONObject();	

		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");	
		catalogo.setClaveIf(intermediario);
		cat = (ArrayList)catalogo.getListaElementos();
		
		registros = JSONArray.fromObject(cat);
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
			
		infoRegresar = jsonObj.toString();
	
	}else if (informacion.equals("CatalogoMoneda")){
		CatalogoSimple cat = new CatalogoSimple();		
		cat.setTabla("comcat_moneda");
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre");		
		List lis= new ArrayList ();
		lis.add("1");
		lis.add("54");
		
		cat.setValoresCondicionIn(lis);   
		cat.setOrden("ic_moneda");		
		
		infoRegresar=cat.getJSONElementos();
	
	} else if (informacion.equals("CatalogoIF")){
		
		CatalogoReporteIF cat = new CatalogoReporteIF();
		infoRegresar = cat.getJSONElementos();	
	
	}else if (informacion.equals("ConsultaDataGrid") || informacion.equals("ArchivoPDF") || informacion.equals("ArchivoCSV")){

		JSONObject jsonObj 	      = new JSONObject();
		int start = 0, limit = 0;
		String operacion = (request.getParameter("operacion")== null) ? "" : request.getParameter("operacion");		
		String noEpo = (request.getParameter("comboEpo")== null) ? "" : request.getParameter("comboEpo");	 
		String moneda = (request.getParameter("comboMoneda")== null) ? "" : request.getParameter("comboMoneda");		
		String intermediario = (request.getParameter("comboIF")== null) ? "" : request.getParameter("comboIF");	

		Parametro parametro = ServiceLocator.getInstance().lookup("ParametroEJB",Parametro.class);
		
		if (!informacion.equals("ArchivoCSV")){
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));	
			}catch(Exception e){
				throw new AppException("Error en la conversión de tipo de datos ", e);
			}
 		}
		ReporteEPOs datos =  new ReporteEPOs();
			
		datos.setMoneda(moneda);
		datos.setIntermediario(intermediario);
		
		//Instancia para el uso del paginador
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(datos);
		
		if (operacion.equals("Generar")) {	//Nueva consulta
			cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
				
		if(informacion.equals("ConsultaDataGrid")){	
			String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
			jsonObj = JSONObject.fromObject(consultar);
			infoRegresar=jsonObj.toString();
		}else if (informacion.equals("ArchivoCSV")){
			try {
				String nombreArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}		
			infoRegresar = jsonObj.toString();
			
		}else if (informacion.equals("ArchivoPDF")){
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				
				String nombreArchivo = cqhelper.getCreatePageCustomFile(request, start, limit ,strDirectorioTemp,"PDF");				
				jsonObj.put("success", new Boolean(true));          
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}		
			infoRegresar = jsonObj.toString();
		}
		
	}else if(informacion.equals("ConsultaDetallado") || informacion.equals("ArchivoPDF2") || informacion.equals("ArchivoCSV2")){
		JSONObject jsonObj 	      = new JSONObject();
		int start=0 , limit=0;
		String operacion = (request.getParameter("operacion")== null) ? "" : request.getParameter("operacion");		
		String noEpo = (request.getParameter("comboEpo")== null) ? "" : request.getParameter("comboEpo");	 
		String moneda = (request.getParameter("comboMoneda")== null) ? "" : request.getParameter("comboMoneda");		
		String intermediario = (request.getParameter("comboIF")== null) ? "" : request.getParameter("comboIF");	
		String montoVigente = null;

		Parametro parametro = ServiceLocator.getInstance().lookup("ParametroEJB",Parametro.class);
		
		if (!informacion.equals("ArchivoCSV2")){
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));	
			}catch(Exception e){
				throw new AppException("Error en parámetros", e);
			}
		}
		montoVigente = parametro.getMontoPublicacionVigente(intermediario,noEpo,moneda);
		
		ReporteIf datos =  new ReporteIf();
		datos.setMoneda(moneda);
		datos.setIntermediario(intermediario);
		datos.setNoEpo(noEpo);
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(datos);
		
		if (operacion.equals("Generar")) {	//Nueva consulta
			cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		
		if(informacion.equals("ConsultaDetallado")){	
			String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
			jsonObj = JSONObject.fromObject(consultar);
			jsonObj.put("MONTO",montoVigente); 

			infoRegresar=jsonObj.toString();
		}else if(informacion.equals("ArchivoPDF2")){
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				String nombreArchivo = cqhelper.getCreatePageCustomFile(request, start, limit ,strDirectorioTemp,"PDF");				
				jsonObj.put("success", new Boolean(true));          //start,limit
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("ArchivoCSV2")){
			try {
				String nombreArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
				infoRegresar = jsonObj.toString();
		}	
	}    
%>

<%= infoRegresar %>