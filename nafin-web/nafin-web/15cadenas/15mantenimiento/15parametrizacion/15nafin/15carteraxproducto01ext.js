function selecCSBLOQUEO(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
	
	if(check.checked == false){
		check.checked = true;
	}
		
	if(check.id == 'chkFISCAL'){
		if(check.checked == true){
			reg.set('FISCAL','S');
			reg.set('AUXFISCAL','checked');
			reg.set('TRAMITADORA','N');
			reg.set('AUXTRAMITADORA','');
			reg.set('MATRIZ','N');
			reg.set('AUXMATRIZ','');
			reg.set('CG_AGENCIA_SIRAC','F');
		}
		
	}else if(check.id == 'chkTRAMITADORA'){
		if(check.checked==true){
			reg.set('FISCAL','N');
			reg.set('AUXFISCAL','');
			reg.set('TRAMITADORA','S');
			reg.set('AUXTRAMITADORA','checked');
			reg.set('MATRIZ','N');
			reg.set('AUXMATRIZ','');
			reg.set('CG_AGENCIA_SIRAC','T');
		}
	}else	if(check.id == 'chkMATRIZ'){
		if(check.checked==true){
			reg.set('FISCAL','N');
			reg.set('AUXFISCAL','');
			reg.set('TRAMITADORA','N');
			reg.set('AUXTRAMITADORA','');
			reg.set('MATRIZ','S');
			reg.set('AUXMATRIZ','checked');
			reg.set('CG_AGENCIA_SIRAC','M');
		}
	}
	store.commitChanges();
}

Ext.onReady(function(){

//--------------------------------HANDLERS-----------------------------------

	var procesarConsultaInicialData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		var store = grid.getStore();
		var checkFiscal = Ext.getCmp('chkFiscal');
		var checkTramitadora = Ext.getCmp('chkTramitadora');
		var checkMatriz = Ext.getCmp('chkMatriz');
		grid.el.mask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			if(store.getTotalCount() > 0) {
				grid.el.unmask();
			}else{
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
			}			
		}
	}
	
	var guardarCambios = function(){
		var  gridConsulta = Ext.getCmp('grid');
		var store = gridConsulta.getStore();	
		var jsonData = store.data.items;
		var lista = new Array();
		
		Ext.each(jsonData, function(item, index, arrItem){
			var registro = new Array();
			var claveProducto = item.data.IC_PRODUCTO_NAFIN;
			var tipo = item.data.CC_TIPO;
			var agenciaSirac = item.data.CG_AGENCIA_SIRAC;
			registro[0] = agenciaSirac;
			registro[1] = claveProducto;
			registro[2] = tipo;
			lista[index] = registro;
		});
		Ext.Ajax.request({
			url: '15carteraxproducto01ext.data.jsp',
			params: {
				informacion: 'guardarCambios',
				listaRegistros: lista
			},
			callback: procesarGuardarCambios
		});
	}
	
	var procesarGuardarCambios = function(opts, success, response) {
		
		var btnGuardar = Ext.getCmp('btnGuardar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var mensaje = Ext.util.JSON.decode(response.responseText).MENSAJE;
			Ext.MessageBox.alert("Mensaje de confirmaci�n", mensaje);
			btnGuardar.enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//---------------------------------STORES------------------------------------

	//STORE PARA GRID PRINCIPAL
	var consultaInicialData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15carteraxproducto01ext.data.jsp',
		baseParams: {
			informacion: 'consultaInicial'
		},
		fields: [
			{name: 'IC_PRODUCTO_NAFIN'},
			{name: 'IC_NOMBRE'},					
			{name: 'CC_TIPO'},
			{name: 'FISCAL', convert: NE.util.string2boolean}, 
			{name: 'TRAMITADORA', convert: NE.util.string2boolean},					
			{name: 'MATRIZ', convert: NE.util.string2boolean},
			{name: 'AUXFISCAL'},
			{name: 'AUXTRAMITADORA'},
			{name: 'AUXMATRIZ'},
			{name: 'CG_AGENCIA_SIRAC'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			//beforesave :{fn: function(){alert();}},
			load: procesarConsultaInicialData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
				}
			}
		}
	});

//------------------------------COMPONENTES----------------------------------
	
	var grid = new Ext.grid.GridPanel({
		id:'grid',
		enableColumnMove:false,
		store: consultaInicialData,	
	//	clicksToEdit:1,
		columLines:true,
		hidden:false,
		stripeRows:true,
		loadMask:true,
		height:180,
		width:565,
		style:'margin:0 auto;',
		columns: [
			{
				header:'Producto',
				tooltip:'Producto',
				hideable:false,
				width: 200,
				dataIndex:'IC_NOMBRE',
				sortable:false,
				align:'left',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					if (record.data['CC_TIPO']=='C'){
						return value + ' / Cobranza';
					}else if(record.data['CC_TIPO']=='F'){
						return value + ' / Factoraje';
					}else{
						return value;
					}
				}
			},
			{
				header:'Domicilio Fiscal',
				tooltip:'Domicilio Fiscal',
				dataIndex:'FISCAL',
				sortable:false,
				width: 120,
				hidden:false,
				hideable:false,
				align:'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					return '<input  id="chkFISCAL"'+ rowIndex + ' value='+ record.data['FISCAL'] + ' type="checkbox"  '+record.data['AUXFISCAL']+' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
				}
			},
			{
				header:'Oficina Tramitadora',
				tooltip:'Oficina Tramitadora',
				dataIndex:'TRAMITADORA',
				sortable:false,
				width: 130,
				hidden:false,
				hideable:false,
				align:'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					return '<input  id="chkTRAMITADORA"'+ rowIndex + ' value='+ record.data['TRAMITADORA'] + ' type="checkbox"  '+record.data['AUXTRAMITADORA']+' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
				}
			},
			{
				header:'Oficina Matriz',
				tooltip:'Oficina Matriz',
				dataIndex:'MATRIZ',
				sortable:false,
				editor:{
				xtype: 'checkbox'
				},
				width: 110,
				hidden:false,
				hideable:false,
				align:'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					return '<input  id="chkMATRIZ"'+ rowIndex + ' value='+ record.data['MATRIZ'] + ' type="checkbox"  '+record.data['AUXMATRIZ']+' onclick="selecCSBLOQUEO(this,'+rowIndex +','+colIndex+');" />';
				}
			}
		],
		listeners:{
			afterEdit: {fn:function(){
			//alert();
			}
			}
		},
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Guardar',
					id: 'btnGuardar',
					iconCls: 'icoGuardar',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						guardarCambios();
					}
				}
			]
		}
	});

//-------------------------COMPONENTE PRINCIPAL------------------------------

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 800,
		align: 'center',
		items: grid
	});

	consultaInicialData.load();

});