<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.exception.*,
		com.netro.procesos.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.descuento.*,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

ManejoServicio servicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);

if(informacion.equals("catalogoProductos") ) {
	List l = new ArrayList();
	l.add("1");
	l.add("4");
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COMCAT_PRODUCTO_NAFIN");
	cat.setCampoClave("ic_producto_nafin");
	cat.setCampoDescripcion("ic_nombre");
	cat.setOrden("ic_nombre");
	cat.setValoresCondicionIn(l);

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("Consultar")) {
	String ic_producto 	= request.getParameter("claveProducto")==null?"":request.getParameter("claveProducto");
	System.out.println("\nIc_Producto vale: "+ic_producto);

	ArrayList list = new ArrayList();
	list = servicio.getHorariosxProducto(ic_producto);
	System.out.println("\nla lista es: "+list.toString());
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size(); i++){
		if(i%2==0){
			Vector aux=(Vector)list.get(i);
			JSONObject jo=new JSONObject();
			jo.put("IDPROD",aux.get(0).toString());
			jo.put("NOMPROD",aux.get(1).toString());
			jo.put("HORAP",aux.get(2).toString());
			jo.put("HORCP",aux.get(3).toString());
			jo.put("HORAI",aux.get(4).toString());
			jo.put("HORCI",aux.get(5).toString());
			a.add(jo);
		}
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("Guardar")){
		ArrayList alHorariosIf = new ArrayList();
		String [] a = request.getParameterValues("arr");
		String aperturaNa		=	(request.getParameter("horain") == null) ? "": request.getParameter("horain");
		String cierreNa		=	(request.getParameter("horacn") == null) ? "": request.getParameter("horacn");
		for(int cont=0; cont < a.length; cont++){
			String [] b = a[cont].split(",");
			String cveProducto = b[0];
			String aperturaPy = b[1];
			String cierrePy = b[2];
			String aperturaIf = b[3];
			String cierreIf = b[4];
			
			servicio.setHorariosGenerales(cveProducto, aperturaPy.trim(), cierrePy.trim(), aperturaIf.trim(), cierreIf.trim(), aperturaNa.trim(), cierreNa.trim());
		}
					
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			infoRegresar = jsonObj.toString();
		
}

%>
<%=infoRegresar%>



