<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.model.catalogos.*,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.nafin.descuento.ws.*,
		org.apache.commons.logging.Log,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar ="";

ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 
Hashtable alParamEPO1 = new Hashtable();


if (	informacion.equals("CatalogoBuscaAvanzada") ) {

	String num_pyme	= (request.getParameter("num_pyme")==null)?"":request.getParameter("num_pyme");
	String rfc_pyme	= (request.getParameter("rfc_pyme")==null)?"":request.getParameter("rfc_pyme");
	String nombre_pyme= (request.getParameter("nombre_pyme")==null)?"":request.getParameter("nombre_pyme");
	String ic_epo		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class); 

	Registros registros = afiliacion.getProveedores("",ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pyme,"","");

	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (registros.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(registros.getString("IC_PYME"));
		elementoCatalogo.setDescripcion(registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("CG_RAZON_SOCIAL"));
		coleccionElementos.add(elementoCatalogo);
	}	
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenNombrePyme")) {

	
	JSONObject jsonObj			= new JSONObject();
	String numeroDeProveedor	= (request.getParameter("numeroDeProveedor")!=null)?request.getParameter("numeroDeProveedor"):"";

	Registros registros			= new Registros();

	String ic_pyme					= "";
	String txtCadenasPymes		= "";

	

		registros					= BeanParamDscto.getParametrosPymeNafinSeleccionIF(numeroDeProveedor);

		if(registros != null && registros.next()){
			ic_pyme					= (registros.getString("PYME")	== null)?"":registros.getString("PYME");
			txtCadenasPymes		= (registros.getString("NOMBRE")	== null)?"":registros.getString("NOMBRE");
		}else{
			jsonObj.put("muestraMensaje", new Boolean(true));
			jsonObj.put("textoMensaje", 	"El nafin electrónico no corresponde a una<br>PyME afiliada a la EPO o no existe.");
		
	}

	jsonObj.put("ic_pyme",					ic_pyme									);
	jsonObj.put("txt_nafelec",				numeroDeProveedor						);
	jsonObj.put("txtCadenasPymes",		txtCadenasPymes						);
	jsonObj.put("estadoSiguiente",		"RESPUESTA_OBTENER_NOMBRE_PYME"	);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("catalogoCuenta")){
	
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	Registros reg=BeanParamDscto.getCtasPyme(ic_pyme);
	List elementos=new ArrayList();
		JSONArray jsonArr = new JSONArray();
			while(reg.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(reg.getString("CLAVE"));
				elementoCatalogo.setDescripcion(reg.getString("DESCRIPCION"));
				elementos.add(elementoCatalogo);
			}
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
			
		infoRegresar = "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
}else if(informacion.equals("catologoEpo")){
		String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		if(!ic_pyme.equals("")){
		CatalogoEPOPorPyme catalogo = new CatalogoEPOPorPyme();
		catalogo.setCampoClave("ic_epo");
		catalogo.setClavePyme(ic_pyme);
		catalogo.setCampoDescripcion("cg_razon_social");
		infoRegresar=catalogo.getJSONElementos();
		}
}else if(informacion.equals("catologoEpoPymeIF")){
		String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String ic_if		= (request.getParameter("intermediario")==null)?"":request.getParameter("intermediario");
		String ic_moneda	= (request.getParameter("HcbMoneda")==null)?"":request.getParameter("HcbMoneda");
		CatalogoEPOPorPyme catalogo = new CatalogoEPOPorPyme();
		catalogo.setMoneda(ic_moneda);
		catalogo.setClavePyme(ic_pyme);
		catalogo.setClaveIF(ic_if);
		catalogo.setCampoClave("IC_EPO");
		catalogo.setCampoDescripcion("CG_RAZON_SOCIAL");
		List elementos=catalogo.getEpoPymeIF();
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
			
		infoRegresar = "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
} else if(informacion.equals("catologoMoneda")){
		String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		if(!ic_pyme.equals("")){
		CatalogoMonedaPyme catalogo = new CatalogoMonedaPyme();
		catalogo.setCampoClave("IC_MONEDA");
		catalogo.setClavePyme(ic_pyme);
		catalogo.setCampoDescripcion("CD_NOMBRE");
		catalogo.setOrden("1");
		infoRegresar=catalogo.getJSONElementos();
		}
} else if(informacion.equals("catalogoIF")){
		String epoC		= (request.getParameter("epo")==null)?"":request.getParameter("epo");
		String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");	
		String monedaC		= (request.getParameter("moneda")==null)?"":request.getParameter("moneda");
		
		alParamEPO1 = new Hashtable();
		alParamEPO1 = BeanParamDscto.getParametrosEPO(epoC,1);
		String bOperaFideicomisoEPO = alParamEPO1.get("CS_OPERA_FIDEICOMISO").toString();
		//	int bOperaFideYCon4 =  	BeanParamDscto.getOperaConevio4PYME(ic_pyme);
		String operaFide_Pyme =   BeanParamDscto. getOperaFideicomisoPYME( ic_pyme );
		String bOperaFideicomisoEPOyCU4="N";
		if(bOperaFideicomisoEPO.equals("S") && operaFide_Pyme.equals("S") )  {
			bOperaFideicomisoEPOyCU4="S";
		}

		CatalogoIFEpoMoneda catalogo = new CatalogoIFEpoMoneda();
		catalogo.setCampoClave("ic_if");
		catalogo.setClaveMoneda(monedaC);
		catalogo.setClaveEpo(epoC);
		catalogo.setCampoDescripcion("cg_nombre_comercial");
		catalogo.setOperaFideicomisoEPO(bOperaFideicomisoEPOyCU4);
		infoRegresar=catalogo.getJSONElementos();
		
}else if (informacion.equals("Consulta")) {
		String ic_epo		= (request.getParameter("HicEpo")==null)?"":request.getParameter("HicEpo");
	String ic_if		= (request.getParameter("intermediario")==null)?"":request.getParameter("intermediario");
	String ic_moneda	= (request.getParameter("HcbMoneda")==null)?"":request.getParameter("HcbMoneda");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	Registros reg=BeanParamDscto.obtenerCuentasBancarias( ic_pyme, ic_moneda);
	boolean instruccion =BeanParamDscto.obtenerInstruccionIrrevocable(ic_pyme,ic_if,ic_epo);
	 try {
	  infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ",\"instruccion\": "+instruccion+" ,\"registros\": " + reg.getJSONData() + "}";
	 }
	 catch(Exception e) {
	  throw new AppException("Error en la paginacion", e);
	 }
}else if (informacion.equals("Consulta2")) {
		String ic_epo		= (request.getParameter("HicEpo")==null)?"":request.getParameter("HicEpo");
	String ic_if		= (request.getParameter("intermediario")==null)?"":request.getParameter("intermediario");
	String ic_moneda	= (request.getParameter("HcbMoneda")==null)?"":request.getParameter("HcbMoneda");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	Registros reg=BeanParamDscto.obtenerCuentaSeleccionada( ic_pyme, ic_moneda,ic_if,ic_epo);
	JSONObject jsonObj=new JSONObject();
	if(reg.next()){
		jsonObj.put("IC_CUENTA", reg.getString("IC_CUENTA_BANCARIA"));
		jsonObj.put("CS_VOBO", reg.getString("CS_VOBO_IF"));
		jsonObj.put("CS_OPERA",reg.getString("CS_OPERA_DESCUENTO"));
		if (reg.next()){
				throw new Exception("Error Inesperado. Existen dos cuentas seleccionadas");
		}
	}
	 jsonObj.put("success", new Boolean(true));
	 infoRegresar = jsonObj.toString();
}else if (informacion.equals("confirmarCuentas")) {

	String ic_epo		= (request.getParameter("HicEpo")==null)?"":request.getParameter("HicEpo");
	String ic_if		= (request.getParameter("intermediario")==null)?"":request.getParameter("intermediario");
	String ic_moneda	= (request.getParameter("HcbMoneda")==null)?"":request.getParameter("HcbMoneda");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	
	String EjecutarCambioCtaCliente		= (request.getParameter("EjecutarCambioCtaCliente")==null)?"":request.getParameter("EjecutarCambioCtaCliente");
	String chkIF		= (request.getParameter("chkIF")==null)?"":request.getParameter("chkIF");
	String chkOperaIF	= (request.getParameter("chkOperaIF")==null)?"":request.getParameter("chkOperaIF");
	String hidOperaIF		= (request.getParameter("hidOperaIF")==null)?"":request.getParameter("hidOperaIF");
	String txtCtaAnterior		= (request.getParameter("txtCtaAnterior")==null)?"":request.getParameter("txtCtaAnterior");
	String rdCta		= (request.getParameter("rdCta")==null)?"":request.getParameter("rdCta");
	String txt_nafelec		= (request.getParameter("txt_nafelec")==null)?"":request.getParameter("txt_nafelec");	
	
	
	String mensaje  = BeanParamDscto.cambioCuentaIndividual( EjecutarCambioCtaCliente, ic_if, chkIF,
		 chkOperaIF, hidOperaIF, txtCtaAnterior, rdCta, ic_moneda,
		 ic_epo, ic_pyme, iNoUsuario,strNombreUsuario,  txt_nafelec,  strLogin);
		
		AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB",AutorizacionDescuento.class); 
		
		String operaFide_Pyme =   BeanParamDscto. getOperaFideicomisoPYME( ic_pyme ); //Fodea 019-2014
		if(ic_moneda.equals("54") && BeanAutDescuento.getOperaIFEPOFISO(ic_if,ic_epo)   && operaFide_Pyme.equals("S") ){
			CuentasPymeIFWS  cuentaConfirmar = new CuentasPymeIFWS();
			cuentaConfirmar.setRfcPyme(BeanAutDescuento.getRFCPyme(ic_pyme));
			cuentaConfirmar.setClaveEpo(ic_epo);
			cuentaConfirmar.setClaveMoneda(ic_moneda);
			BeanAutDescuento.confirmacionCuentasIFWS ( ic_if, strLogin,new CuentasPymeIFWS[]{ cuentaConfirmar});
		}	
	
	
	JSONObject jsonObj			= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", mensaje);
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("reasignarCtas")){
	String ic_epo		= (request.getParameter("HicEpo")==null)?"":request.getParameter("HicEpo");
	String ic_if		= (request.getParameter("intermediario")==null)?"":request.getParameter("intermediario");
	String ic_moneda	= (request.getParameter("HcbMoneda")==null)?"":request.getParameter("HcbMoneda");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String epos			= (request.getParameter("epos")==null)?"":request.getParameter("epos");
	String icCuenta   = (request.getParameter("icCuenta")==null)?"":request.getParameter("icCuenta");
	//(String ic_if,String ic_pyme,String ic_epo_base,String ic_cuenta_bancaria,String [] clavesEpo)
	String[] listaEpos=epos.split(",");

		
	boolean reasignar=BeanParamDscto.reasignarCuentasBancarias( ic_if, ic_pyme,ic_epo,icCuenta,listaEpos, strLogin,  strNombreUsuario);
	
										
	JSONObject jsonObj			= new JSONObject();
	jsonObj.put("success", new Boolean(reasignar));
	jsonObj.put("mensaje", "La cuenta ha sido reasignada");
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("validaFideicomisoEPO_PYME")){

	String ic_epo		= (request.getParameter("HicEpo")==null)?"":request.getParameter("HicEpo");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String respuesta ="S";
	String  operaFide_EPO =  BeanParamDscto.getOperaFideicomisoEpo( ic_epo); // Fodea 19-2014
	String operaFide_Pyme =   BeanParamDscto. getOperaFideicomisoPYME( ic_pyme ); // Fodea 19-2014
	if(operaFide_Pyme.equals("N") && operaFide_EPO.equals("S")  )  {  
		respuesta ="N";
	}
	
	
	JSONObject jsonObj			= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", respuesta);
	infoRegresar = jsonObj.toString();	

}

%>
<%=infoRegresar%>

