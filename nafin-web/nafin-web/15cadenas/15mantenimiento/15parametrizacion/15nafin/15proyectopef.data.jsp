<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.parametrosgrales.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String ic_epo  = (request.getParameter("ic_epo") != null)?request.getParameter("ic_epo"):"";
String no_epo  = (request.getParameter("no_epo") != null)?request.getParameter("no_epo"):"";
String ne_epo  = (request.getParameter("ne_epo") != null)?request.getParameter("ne_epo"):"";
String nombre_epo  = (request.getParameter("nombre_epo") != null)?request.getParameter("nombre_epo"):"";
String ic_secretaria  = (request.getParameter("ic_secretaria") != null)?request.getParameter("ic_secretaria"):"";
String ic_organismo  = (request.getParameter("ic_organismo") != null)?request.getParameter("ic_organismo"):"";
String ig_anio  = (request.getParameter("ig_anio") != null)?request.getParameter("ig_anio"):"";


String infoRegresar ="", anio="";
List lresultados = new ArrayList();
HashMap datos = new HashMap();
List registros = new  ArrayList();
JSONObject jsonObj = new JSONObject();

Calendar anioActual = Calendar.getInstance();
anio = String.valueOf(anioActual.get(Calendar.YEAR));


ParametrosGrales parametrosbean = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);


if (informacion.equals("CatalogoEpo")){

	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setOrden("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();
	
}else  if (informacion.equals("catSecretaria")){
		
	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("comcat_secretaria");
   cat.setCampoClave("ic_secretaria");
   cat.setCampoDescripcion("cg_descripcion"); 
	cat.setOrden("cg_descripcion");
   infoRegresar = cat.getJSONElementos();
	
}else  if (informacion.equals("catOrganismo")){

	CatalogoSimple cat = new CatalogoSimple();
   cat.setTabla("comcat_tipo_organismo");
   cat.setCampoClave("ic_tipo_organismo");
   cat.setCampoDescripcion("cg_descripcion"); 
	cat.setOrden("cg_descripcion");
   infoRegresar = cat.getJSONElementos();

}else  if (informacion.equals("Consultar")){

	lresultados = parametrosbean.getEposProyectoPEF(ic_epo, no_epo, ne_epo, nombre_epo, ic_secretaria, ic_organismo);
	
	for(int cont=0;cont<lresultados.size();cont++){
		List	lregistro = new ArrayList();
		lregistro = (List)lresultados.get(cont);
		datos = new HashMap();
		datos.put("IC_EPO", lregistro.get(0));
		datos.put("NOMBRE_EPO", lregistro.get(1));
		datos.put("SECRETARIA", lregistro.get(2));
		datos.put("ORGANISMO", lregistro.get(3));		
		datos.put("PORCETAJE_COBRO",  lregistro.get(4));
		datos.put("ANIO",  anio);
		registros.add(datos);
	}
	jsonObj.put("registros", registros);			
	jsonObj.put("success",  new Boolean(true)); 
	infoRegresar = jsonObj.toString();	
	
}else  if (informacion.equals("ConsultarHistorico")){	
	
	List colHisEpoPef = parametrosbean.consHistEpoPefObraPub(ic_epo, ig_anio);
	for(int x=0; x<colHisEpoPef.size();x++ ){
		List	regHisEpoPef = (List)colHisEpoPef.get(x);
		datos = new HashMap();
		datos.put("ANIO", (String)regHisEpoPef.get(0));
		datos.put("PORCENTAJE", (String)regHisEpoPef.get(1));
		registros.add(datos);
	}
	jsonObj.put("registros", registros);			
	jsonObj.put("success",  new Boolean(true)); 
	infoRegresar = jsonObj.toString();

}else  if (informacion.equals("Guardar")){	

	String totalepos = (request.getParameter("totalepos") == null) ? "" : request.getParameter("totalepos");
	String ic_epoG[] = request.getParameterValues("ic_epo");
	String ic_secretariaG[] = request.getParameterValues("ic_secretaria");
	String ic_organismoG[] = request.getParameterValues("ic_organismo");
	String obra_publicaG[] = request.getParameterValues("obra_publica");

	List lcols = null;
	List lfil = new ArrayList();
	String mensaje ="";		
		
	for(int a=0;a<Integer.parseInt(totalepos);a++){
		lcols = new ArrayList();
		lcols.add(ic_epoG[a]);
		lcols.add(ic_secretariaG[a]);
		lcols.add(ic_organismoG[a]);
		lcols.add(obra_publicaG[a]);
		lcols.add(anio);			
		lfil.add(lcols);
	}

	try{
		parametrosbean.ovactualizarEpos(lfil);			
		mensaje= "EXITO";			
	}catch(Exception e){
		mensaje="ERROR";			
	}
		
	jsonObj.put("success",  new Boolean(true)); 
	jsonObj.put("mensaje", mensaje);	
	infoRegresar = jsonObj.toString();
	
}

%>
<%=infoRegresar%>
