<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.sql.*,java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		java.lang.String,
		com.netro.cadenas.*,
		com.netro.exception.*,
		com.netro.procesos.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.descuento.*,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
SimpleDateFormat formatoHora2 = new SimpleDateFormat("dd/MM/yyyy; hh:mm:ss a");
String infoRegresar	=	"";

Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

if(informacion.equals("catalogoIntermediarios") ) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_if");
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("2");
	
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("valoresIniciales")) {  	
	JSONArray val = new JSONArray();
	JSONObject obj = new JSONObject();
	obj.put("DATOS", "Fecha de modificación");
	obj.put("VALORES", new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()));
	val.add(obj);
	obj.put("DATOS", "Hora de modificación");
	obj.put("VALORES", new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date()));
	val.add(obj);
	obj.put("DATOS", "Usuario");
	obj.put("VALORES", strNombreUsuario);
	val.add(obj);
	
	JSONObject obj1 = new JSONObject();
	obj1.put("total",  Integer.toString(val.size()));
	obj1.put("registros", val.toString());
	infoRegresar = obj1.toString();
	
}else if(informacion.equals("catalogoMonedas") ){
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_moneda");
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("2");
	
	infoRegresar = cat.getJSONElementos();

}else if(informacion.equals("Consultar")) {

	String ic_if = (request.getParameter("claveIF") == null) ? "": request.getParameter("claveIF");
		
	List lDoctosCargados = new ArrayList();
	lDoctosCargados = afiliacion.consulta_general(ic_if);
	
	JSONArray a= new JSONArray();
	for(int i=0;i<lDoctosCargados.size(); i++){
		List aux=(List)lDoctosCargados.get(i);
		JSONObject jo=new JSONObject();
		jo.put("CVEIF",aux.get(0)==null?"":aux.get(0).toString());
		jo.put("MONTOANT",aux.get(3)==null?"":aux.get(3).toString());
		jo.put("MONTO",aux.get(3)==null?"":aux.get(3).toString());
		jo.put("FACULTAD",aux.get(2)==null?"":aux.get(2).toString());
		jo.put("NOMBREIF",aux.get(1)==null?"":aux.get(1).toString());
		jo.put("MONEDA",aux.get(4)==null?"":aux.get(4).toString());
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("Aceptar")){
	List lDoctosAceptados = new ArrayList();
	AccesoDB con = new AccesoDB();
	try{
	con.conexionDB();
	ResultSet rs;
	String [] a = request.getParameterValues("arr");
	String fechaAct = (request.getParameter("fechaActual") == null) ? "": request.getParameter("fechaActual");
	for(int i=0; i<a.length; i++){
		List aux = new ArrayList();
		String moneda_nombre = "";
		String [] b = a[i].split(",");
		String ic_if = b[0];
		String cveMon = b[1];
		String montoAnt = b[2];
		String montoAc = b[3];
		String fac_if = b[4];
		String nomIf = b[5];
		String query1 = " SELECT cd_nombre"   +
							"   FROM comcat_moneda"   +
							"  WHERE ic_moneda = " + cveMon;
		rs =  con.queryDB(query1);
		while (rs.next()) {
				moneda_nombre = (rs.getString("cd_nombre")==null)?"":rs.getString("cd_nombre");
		}
		rs.close();
		con.cierraStatement();
		
		aux.add(b[0]);
		aux.add(moneda_nombre);
		aux.add(b[2]);
		aux.add(b[3]);
		aux.add(b[4]);
		aux.add(b[5]);
		lDoctosAceptados.add(aux);
		//Se realiza la actualización de los datos para su posterior recarga
		if(b[4].equals("true")){
			afiliacion.guardar_facultades(ic_if, cveMon, montoAc, montoAnt, strLogin, fechaAct, "S");
		}else
			afiliacion.guardar_facultades(ic_if, cveMon, montoAc, montoAnt, strLogin, fechaAct, "N");
	}
	
	JSONArray b= new JSONArray();
	for(int i=0;i<lDoctosAceptados.size(); i++){
		List aux=(List)lDoctosAceptados.get(i);
		JSONObject jo=new JSONObject();
		jo.put("CVEIFI",aux.get(0)==null?"":aux.get(0).toString());
		jo.put("NOMBREIFI",aux.get(5)==null?"":aux.get(5).toString());
		jo.put("MONEDAI",aux.get(1)==null?"":aux.get(1).toString());
		jo.put("MONANTI",aux.get(2)==null?"":aux.get(2).toString());
		jo.put("MONACT",aux.get(3)==null?"":aux.get(3).toString());
		jo.put("FACULTADI",aux.get(4)==null?"":aux.get(4).toString());
		b.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(b.size()));
	jsonObj.put("registros", b.toString());
	infoRegresar=jsonObj.toString();
	
	}
	catch(Throwable e){
		throw new AppException("Error al guardar los datos",e);
	}
	finally{
		con.cierraConexionDB();
	}
		
}else if(informacion.equals("Ver")){
	
	String clavesif = (request.getParameter("cveIF") == null) ? "": request.getParameter("cveIF");
	
	List lDoctosCargados = new ArrayList();
	lDoctosCargados = afiliacion.imprimir_acuse(clavesif);
	
	JSONArray a= new JSONArray();
	for(int i=0;i<lDoctosCargados.size(); i++){
		List aux=(List)lDoctosCargados.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOMIF",aux.get(2)==null?"":aux.get(2).toString());
		jo.put("FECMOD",aux.get(3)==null?"":aux.get(3).toString());
		jo.put("MONEDAB",aux.get(5)==null?"":aux.get(5).toString());
		jo.put("MONANT",aux.get(6)==null?"":aux.get(6).toString());
		jo.put("MONMOD",aux.get(7)==null?"":aux.get(7).toString());
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("ArchivoPDF")){
		List lAceptados = new ArrayList();
		String [] ar = request.getParameterValues("arreglo");
		
		for(int cont = 0;cont < ar.length; cont++){
			List au = new ArrayList();
			String [] ac = ar[cont].split(",");
			au.add(ac[0]);
			au.add(ac[1]);
			au.add(ac[2]);
			au.add(ac[3]);
			au.add(ac[4]);
			au.add(ac[5]);
			lAceptados.add(au);
		}
		try {
			CreaPDFacultades nmbre = new CreaPDFacultades();
			String nombreArchivo = nmbre.crearPDFFac(request, lAceptados, strDirectorioTemp, "PDF");
		
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
}
	
%>
<%=infoRegresar%>
