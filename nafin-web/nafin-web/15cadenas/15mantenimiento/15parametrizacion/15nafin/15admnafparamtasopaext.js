

function selecChekEmiminar(check, rowIndex, colIds){
	var gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
   var reg = gridConsulta.getStore().getAt(rowIndex);
 
	if(check.checked==true)  {
		reg.set('ELIMINAR', "S");	
		reg.set('AUXELIMINAR', "checked");
	}else  {
	 reg.set('ELIMINAR', "N");
	 reg.set('AUXELIMINAR', "");
	}	
	store.commitChanges();	
}


Ext.onReady(function() {

	var idMenuP =  Ext.getDom('idMenuP').value;

	var txtCveTasa = [];
	var txtValorTasa = [];
	var chk_eliminar = [];
	
	var cancelar =  function() { 
		txtCveTasa = [];
		txtValorTasa = [];
		chk_eliminar = [];
	}
	
	//----------------ELIMINAR DE TASAS------------------------------
	
	
	function transmiteBorrarTasas(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.MessageBox.alert(' Mensaje',info.strMensaje);
			Ext.getCmp('gridConsulta').hide();
		} else {
				NE.util.mostrarConnError(response,opts);							
		}
	}
	
	var procesoBorraTasas = function() {
	
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		var jsonData = store.data.items;
		cancelar();		
		var total =0;
		var txt_fecha_app = Ext.getCmp("txt_fecha_app");
		var txt_fecha_app_2 =  Ext.util.Format.date(txt_fecha_app.getValue(),'d/m/Y'); 
		
		Ext.each(jsonData, function(item,inx,arrItem){			
			if(item.data.ELIMINAR=='S') { 
				chk_eliminar.push('S'+'|'+item.data.IC_TASA);
				total++;
			}
		});

		if(total==0) {		
			Ext.MessageBox.alert(' Mensaje','Debe de seleccionar al menos un registro');
			return;
			
		}else  {
					
			Ext.Msg.show({
				title:	"Confirmaci�n",
				msg:		'Est� seguro que desea borrar la tasa del d�a '+txt_fecha_app_2,
				buttons:	Ext.Msg.OKCANCEL,
				fn: function resultMsj(btn){
					if(btn =='ok'){	
						Ext.Ajax.request({
							url: '15admnafparamtasopa.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'EliminaTasas',
								chk_eliminar:chk_eliminar	
							}),
							callback: transmiteGuardarTasas
						});
					}
				}
			});		
		}
	}
	
	
	//----------------CAPTURA DE TASAS------------------------------
	
	function transmiteGuardarTasas(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert(' Mensaje',info.strMensaje);
			Ext.getCmp('gridConsulta').hide();
		} else {
				NE.util.mostrarConnError(response,opts);							
		}
	}
	
	var procesoGuardaTasas = function() {
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		cancelar();		
		var total =0;
		var numRegistro=0;
		var txt_fecha_app = Ext.getCmp("txt_fecha_app");
		var fechaActual = Ext.getCmp("fechaActual");
		
		if ( Ext.isEmpty(txt_fecha_app.getValue()) ){
			txt_fecha_app.markInvalid('Debe capturar la fecha de aplicaci�n.');
			return;
		}
		
		var txt_fecha_app_2 =  Ext.util.Format.date(txt_fecha_app.getValue(),'d/m/Y');   
		var fechaActual_2 =  Ext.util.Format.date(fechaActual.getValue(),'d/m/Y');   
		var diferencia = datecomp(txt_fecha_app_2, fechaActual_2);
		if(diferencia==2)  {
			txt_fecha_app.markInvalid('La fecha de aplicaci�n no debe ser menor al d�a actual.');
		}
		
		
		store.each(function(record) {
			numRegistro = store.indexOf(record);	
			if(record.data['VALOR'] == '') {
				total+1;	
			}else {
				txtCveTasa.push(record.data['IC_TASA']);
				txtValorTasa.push(record.data['VALOR']);
				total = 1;
			}
		});
		
		if(total==numRegistro) {
			Ext.MessageBox.alert(' Mensaje','Favor de teclear el valor de al menos una de las tasas');
			return;
		}else  {
		
			Ext.Ajax.request({
				url: '15admnafparamtasopa.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'CapturaTasas',
					txtCveTasa:txtCveTasa,
					txtValorTasa:txtValorTasa
				}),
				callback: transmiteGuardarTasas
			});
		
		}
		
	}
	//-----------------CONSULTA  ------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}				
			var jsonData = store.reader.jsonData;	
			
			if(store.getTotalCount() > 0) {			
				el.unmask();				
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15admnafparamtasopa.data.jsp',
		baseParams: {
			informacion: 'ConsultarCaptura'
		},		
		fields: [	
			{	name: 'IC_TASA'},
			{	name: 'NOMBRE_TASA'},
			{	name: 'VALOR'},
			{	name: 'ELIMINAR', convert: NE.util.string2boolean},
			{  name: 'AUXELIMINAR'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		clicksToEdit:1,
		style: 'margin:0 auto;',
		title: 'Tasas Operativas',
		columns: [		
			{
				header: 'Tasas', 
				tooltip: 'Tasas',
				dataIndex: 'NOMBRE_TASA',
				sortable: false,
				resizable: true,
                                hideable: false,
				width: 200,			
				align: 'left',
                                menuDisabled:true 
			},
			{ 
            header: 'Valor',
            tooltip: 'Valor',
            dataIndex: 'VALOR',
            sortable: false,
            hideable: false,
            width: 150,
            align: 'center',
            menuDisabled:true,
				editor: { 
					xtype:'numberfield',
					decimalPrecision: 8,
					maxValue : 999.9999999
					/*allowBlank: false*/
				},
            renderer: function(value, metadata, record, rowIndex, colIndex, store){            	
					return NE.util.colorCampoEdit(value,metadata,record);					
            }
         },
			{
            header: 'Eliminar',
            dataIndex: 'ELIMINAR',
            sortable: false,
            hideable: false,
            width: 80,   
            align: 'center',
            menuDisabled:true,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['VALOR']!='' ){
						return '<input  id="chkELIMINAR"'+ rowIndex + ' value='+ value + ' type="checkbox" '+record.data['AUXELIMINAR'] +' onclick="selecChekEmiminar(this,'+rowIndex +','+colIndex+');" />';
					}
				}
         }	
			
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 460,
		align: 'center',
		frame: false,
		bbar: {
			items: [
			'->','-',
				{
					xtype: 'button',
					text: 'Aceptar',					
					tooltip:	'Aceptar',
					iconCls: 'aceptar',
					id: 'btnAceptar',
					handler: procesoGuardaTasas
				},
				{
					xtype: 'button',
					text: 'Borrar',					
					tooltip:	'Borrar',	
					iconCls: 'borrar',
					id: 'btnBorrar',
					handler: procesoBorraTasas
				},
				{
					xtype: 'button',
					text: 'Cancelar',					
					tooltip:	'Cancelar',	
					iconCls: 'icoLimpiar',
					id: 'btnCancelar',
					handler: function(boton, evento) {				
						window.location = '15admnafparamtasopaext.jsp';
					}
				}
			]
		}
	});
			
	//-----------------INTERFAZ GRAFICA  ------------------------	
	function procesaValoresIniciales(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
	
			Ext.getCmp("txt_fecha_app").setValue(jsonValoresIniciales.fecha_Actual);
			Ext.getCmp("fechaActual").setValue(jsonValoresIniciales.fecha_Actual);
			
			var  total= jsonValoresIniciales.permisos.length; 
			
			if(total>0) {
				for(i=0;i<total;i++){ 
					boton=  jsonValoresIniciales.permisos[i];							
					Ext.getCmp(boton).show();						
				}
			}else  {		
				Ext.getCmp("BTNCAPTURA").hide();	
				window.location = '15admnafparamtasopaext.jsp?pagina=Consulta';
				   
			}
						
				
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var catalogoTasa = new Ext.data.JsonStore({
		id: 'catalogoTasa',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15admnafparamtasopa.data.jsp',
		baseParams: {
			informacion: 'catalogoTasa'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var  elementosForma  = [
		{ 	xtype: 'datefield',  hidden:true, id: 'fechaActual', 	value: '' },
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Aplicaci�n',
			combineErrors: false,
			msgTarget: 'side',
			width: 700,
			items: [
				{
					xtype: 'datefield',
					name: 'txt_fecha_app',
					id: 'txt_fecha_app',
					allowBlank: true,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 			
					margins: '0 20 0 0'  					
				},
				{
					xtype: 'displayfield',
					value: 'Tasa:    ',
					width: 30
				},
				{
					xtype: 'combo',
					fieldLabel: 'Tasa',
					name: 'cboTasa',
					id: 'cboTasa1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccione...',	
					valueField: 'clave',
					hiddenName : 'cboTasa',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,				
					store: catalogoTasa,
					width: 200,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners: {
						select:{ 
							fn:function (combo) {
					
								var txt_fecha_app = Ext.getCmp("txt_fecha_app");						
								var txt_fecha_app_ = Ext.util.Format.date(txt_fecha_app.getValue(),'d/m/Y');
								if(!Ext.isEmpty(txt_fecha_app.getValue())){
									if(!isdate(txt_fecha_app_)) { 
										txt_fecha_app.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
										txt_fecha_app.focus();
										return;
									}
								}
								fp.el.mask('Enviando...', 'x-mask-loading');			
								consultaData.load({
									params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'ConsultarCaptura'									
									})
								});	
							}
						}
					}
				}
			]
		}
		
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de Captura',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma
	});
	
	var fpBotones = new Ext.Container({
		layout: 'table',		
		style: 'margin:0 auto;',
		id: 'fpBotones',		
	   width: '200',
		heigth:'auto',		
		items: [
			{
				xtype: 'button',
				text: 'Captura',
				tooltip:	'Captura',	 									
				id: 'BTNCAPTURA',	
				hidden: true,
				handler: function(boton, evento) {	
				
					window.location = '15admnafparamtasopaext.jsp?pagina=Captura&idMenu='+idMenuP;
					
				}
			},	
			{
					xtype: 'displayfield',
					value: '     ',
					width: 20
			},
			{
				xtype: 'button',
				text: 'Consulta',
				tooltip:	'Consulta',				
				id: 'BTNCONSULTA',	
				hidden: true,
				handler: function(boton, evento) {				
					window.location = '15admnafparamtasopaext.jsp?pagina=Consulta&idMenu='+idMenuP;
				}
			}		
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	
	catalogoTasa.load({
		params: {
			pantalla:'Captura'
		}
	});
						
	Ext.Ajax.request({
		url: '15admnafparamtasopa.data.jsp',
		params: {
			informacion: "valoresIniciales",
			idMenuP:idMenuP 
		},
		callback: procesaValoresIniciales
	});
		
});	



