<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*, 
				com.netro.procesos.*, 
				netropology.utilerias.*,   
				com.netro.model.catalogos.CatalogoSimple,   
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%  
String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String icProducto = (request.getParameter("icProducto") !=null)?request.getParameter("icProducto"):"";
String chk_cierre_servicio[]	= request.getParameterValues("chk_cierre_servicio");
String infoRegresar = "";

int numRegAfectados = 0;

//Llenar combo productos
if (informacion.equals("catalogoProducto")) {
	List lista = new ArrayList();
	lista.add("1");
	lista.add("4");
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COMCAT_PRODUCTO_NAFIN");
	cat.setCampoClave("ic_producto_nafin");	
	cat.setCampoDescripcion("ic_nombre");
	cat.setOrden("ic_nombre");
	cat.setValoresCondicionIn(lista);
	infoRegresar = cat.getJSONElementos();
/*	List listaElementos = cat.getListaElementos();
	ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
	elementoCatalogo.setClave("");
	elementoCatalogo.setDescripcion("Todos los Productos");
	listaElementos.add(elementoCatalogo); 
	
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(listaElementos);
	infoRegresar = "{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";*/
	
}else if(informacion.equals("consultaProductos") || 
			informacion.equals("cerrarServicio") ||
			informacion.equals("cambiarEstatus") ||
			informacion.equals("cambiarEstatusDist") ||
			informacion.equals("consultaProductos24Hrs")
			){
			
	ManejoServicio beanProd = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);
	
	//Llenar grid productos
	if(informacion.equals("consultaProductos")){
		JSONArray jsObjArray = new JSONArray();		
		List lista = beanProd.getProductosById(icProducto);		
		jsObjArray = JSONArray.fromObject(lista);

		infoRegresar	=		"{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
	//Cerrar servicios seleccionados	
	}else if(informacion.equals("consultaProductos24Hrs")){
		JSONArray jsObjArray = new JSONArray();		
		List lista = beanProd.getHorariosDescuentoElec();		
		jsObjArray = JSONArray.fromObject(lista);
		infoRegresar	=		"{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
		System.out.println("\n"+ infoRegresar);
	}
	
	else if(informacion.equals("cerrarServicio")){
		beanProd.setEstatusServicioPyme(chk_cierre_servicio,"S");
	//Cambiar Estatus Descuento Electronico
	}else if(informacion.equals("cambiarEstatus")){
		numRegAfectados = beanProd.cambiarEstatusDoctos();
		JSONObject jObj = new JSONObject();
		jObj.put("success", Boolean.TRUE);
		jObj.put("NUM_AFECTADOS", new Integer(numRegAfectados));
		infoRegresar = jObj.toString();
	//Cambiar Estatus Financiamiento Distribuidores
	}else if(informacion.equals("cambiarEstatusDist")){
		numRegAfectados = beanProd.cambiarEstatusDoctosDist();
		JSONObject jObj = new JSONObject();
		jObj.put("success", Boolean.TRUE);
		jObj.put("NUM_AFECTADOS", new Integer(numRegAfectados));
		infoRegresar = jObj.toString();
	}
}
%>
<%=  infoRegresar%>
