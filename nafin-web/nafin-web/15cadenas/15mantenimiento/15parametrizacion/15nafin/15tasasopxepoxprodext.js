Ext.onReady(function() {

var tasaActual=null;	//Variable para saber con cual tasa trabaja actualmente
var nomProd=null;
var nomTasa=null;
var cveTasaProd=null;
var ic_plazo;
var ic_tasa;

//- - - - - - -  - - - - - HANDLER�S - - - - - - - - - - - - - - - - - - - - - -
	var procesarConsultaRegistros = function(store, registros, opts){
		
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		
		if (registros != null) {
			var grid  = Ext.getCmp('gridConsulta');

			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
				tasaActual=null;
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}
	}
	

	var procesarGrabarTasas =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			Ext.MessageBox.alert('Aviso', 'Tasa Asignada Satisfactoriamente', function(){
				//contenedorPrincipal.el.mask('Consultando...', 'x-mask-loading');
				accionConsulta("CONSULTAR",null);
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarActualizarTasas =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			Ext.MessageBox.alert('Aviso', 'Tasa Actualizada Satisfactoriamente', function(){
				//contenedorPrincipal.el.mask('Consultando...', 'x-mask-loading');
				Ext.getCmp('winModificar').hide();
				accionConsulta("CONSULTAR",null);
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaTasaActual =  function(opts, success, response, tasa) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			//var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			tasaActual = Ext.util.JSON.decode(response.responseText).tasa;
					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Lanza los procesos de la ventana emergente para modificar tasas
	var modificarTasa = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		cveTasaProd = registro.get('IDTASAPROD');
		nomProd = registro.get('PRODNOM');
		nomTasa = registro.get('CG_RAZON_SOCIAL');
		var t = registro.get('TASANOM');
		var p = registro.get('PLAZO');
		ic_tasa = registro.get('INI_TASA'); 
		ic_plazo = registro.get('INI_PLAZO');
		var ini_plazo = registro.get('IC_PLAZO');
		var prod= Ext.getCmp('_cmb_prod').getValue();
		var mon= Ext.getCmp('_cmb_mon').getValue();
		var epo= Ext.getCmp('_cmb_epo').getValue();
		var monDesc = mon=='1'?'MONEDA NACIONAL':'DOLAR AMERICANO';
		Ext.getCmp('lbl_prod').setText('Producto: '+nomProd);
		Ext.getCmp('lbl_epo').setText('Nombre de la EPO: '+nomTasa);
		Ext.getCmp('lbl_mon').setText('Moneda: '+monDesc);
		
		//Se cargan los catalogos de la ventana
		catalogoTasasDataB.load({ 
			params: {
				cboMoneda: mon
			}
		});
		catalogoPlazoDataB.load({ 
			params: {cveProd: prod,
						cvePlazo: ini_plazo
					}
		});
		//Se muestra la ventana
		Ext.getCmp('winModificar').show();
	}
	
	//Eliminar tasa
	var procesarEliminarTasas =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			Ext.MessageBox.alert('Aviso', 'La tasa ha sido eliminada', function(){
				//contenedorPrincipal.el.mask('Consultando...', 'x-mask-loading');
				accionConsulta("CONSULTAR",null);
			});
			
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	var eliminarTasa = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var tasa = registro.get('IDTASAPROD');
		Ext.Msg.confirm('Confirmaci�n', '�Est� seguro que desea eliminar la tasa?', function(btn){
								if(btn=='yes'){
									Ext.Ajax.request({
										url: '15tasasopxepoxprodext.data.jsp',
										params: {
											informacion: 'Eliminar',
											tasa: tasa
										},
										callback: procesarEliminarTasas
									});
								}
		});	
		
	}

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - -

	var catalogoProductosData = new Ext.data.JsonStore({
		id:				'catalogoProductosDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15tasasopxepoxprodext.data.jsp',
		baseParams:		{	informacion: 'catalogoProductos'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoBancoFondeoData = new Ext.data.JsonStore({
		id:				'catalogoBancoFondeoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15tasasopxepoxprodext.data.jsp',
		baseParams:		{	informacion: 'catalogoBancoFondeo'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id:				'catalogoEPODataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15tasasopxepoxprodext.data.jsp',
		baseParams:		{	informacion: 'catalogoEPO'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id:				'catalogoMonedaDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15tasasopxepoxprodext.data.jsp',
		baseParams:		{	informacion: 'catalogoMoneda'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoTasasData = new Ext.data.JsonStore({
		id:				'catalogoTasasDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15tasasopxepoxprodext.data.jsp',
		baseParams:		{	informacion: 'catalogoTasas'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoTasasDataB = new Ext.data.JsonStore({
		id:				'catalogoTasasDataBStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15tasasopxepoxprodext.data.jsp',
		baseParams:		{	informacion: 'catalogoTasasB'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: function (){
				Ext.getCmp('_cmb_tasab').setValue(ic_tasa);
			},
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoPlazoData = new Ext.data.JsonStore({
		id:				'catalogoPlazoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15tasasopxepoxprodext.data.jsp',
		baseParams:		{	informacion: 'catalogoPlazo'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoPlazoDataB = new Ext.data.JsonStore({
		id:				'catalogoPlazosDataBStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15tasasopxepoxprodext.data.jsp',
		baseParams:		{	informacion: 'catalogoPlazosB'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: function (){
				Ext.getCmp('_cmb_plazob').setValue(ic_plazo);
			},
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'15tasasopxepoxprodext.data.jsp',
			baseParams: {	informacion:	'ConsultaRegistros'	},
			fields: [
				{ name: 'IDTASAPROD'},
				{ name: 'PRODNOM'},
				{ name: 'CG_RAZON_SOCIAL'},
				{ name: 'TASANOM'},
				{ name: 'PLAZO'},
				{ name: 'INI_PLAZO'},
				{ name: 'IC_PLAZO'},
				{ name: 'INI_TASA'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null,null);
					}
				}
			}
	});
	
//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - -

	var procesaConsulta = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){
		 if(  estadoSiguiente == "CONSULTAR" ){
			var forma = Ext.getCmp("forma");
			forma.el.mask('Consultando...','x-mask-loading');
			Ext.getCmp('_cmb_tasa').reset();
			Ext.getCmp('_cmb_plazo').reset();
			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("gridConsulta").show();
			Ext.Ajax.request({
				url: '15tasasopxepoxprodext.data.jsp',
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'ObtenerTasa'
							}
				),
				callback: procesarConsultaTasaActual
			});
			Ext.StoreMgr.key('registrosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Consultar',
								operacion: 'Generar',
								start: 0,
								limit: 100
							}
				)
			});
			
		}else if(estadoSiguiente == "GRABAR"){
			Ext.Ajax.request({
				url: '15tasasopxepoxprodext.data.jsp',
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Grabar'
							}
				),
				callback: procesarGrabarTasas
			});
			
		}else if(estadoSiguiente == "ACTUALIZAR"){
			var cveTasa = Ext.getCmp('_cmb_tasab').getValue();
			var cvePlazo = Ext.getCmp('_cmb_plazob').getValue(); 
			Ext.Msg.confirm('Confirmaci�n', '�Est� seguro que desea modificar la tasa?', function(btn){
									if(btn=='yes'){
										Ext.Ajax.request({
											url: '15tasasopxepoxprodext.data.jsp',
											params:	Ext.apply(fp.getForm().getValues(),
											{
												informacion: 'Actualizar',
												cveTasaProd: cveTasaProd,
												cveTasa: cveTasa,
												cvePlazo: cvePlazo
											}
											),
											callback: procesarActualizarTasas
										});
									}
								});
			
		}else if(	estadoSiguiente == "LIMPIAR"){
			Ext.getCmp('forma').getForm().reset();
			grid.hide();
		}
	}

//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

//Elementos del grid de la consulta
		var grid = new Ext.grid.GridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			title:		undefined,
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			width: 		845,
			frame: 		false,
			columns: [
				{
					header: 		'Producto',
					tooltip: 	'Nombre del producto',
					dataIndex: 	'PRODNOM',
					sortable: 	true,
					align:		'left',
					resizable: 	true,
					width: 		170,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Nombre de la EPO',
					tooltip: 	'Nombre de la EPO',
					dataIndex: 	'CG_RAZON_SOCIAL',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		190,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Tipo de Tasa',
					tooltip: 	'Tipo de tasa',
					dataIndex: 	'TASANOM',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		225,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Plazo',
					tooltip: 	'Numero de plazos',
					dataIndex: 	'PLAZO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		95,
					hidden: 		false,
					hideable:	false
				},{
					xtype: 'actioncolumn',
					header: 'Modificar',
					tooltip: 'Modificar los datos',
					width: 80,
					align: 'center',
					items: [
						{
							getClass: function(valor, metadata, record, rowIndex, colIndex, store) {
								if(record.data['IDTASAPROD']==tasaActual){
									this.items[0].tooltip = 'Modificar';
									return 'modificar';
								}
							},
							handler: modificarTasa
						}
					]
				},{
					xtype: 'actioncolumn',
					header: 'Eliminar',
					tooltip: 'Eliminar los datos',
					width: 80,
					align: 'center',	
					items: [
						{
							getClass: function(valor, metadata, record, rowIndex, colIndex, store) {
								if(record.data['IDTASAPROD']==tasaActual){
									this.items[0].tooltip = 'Eliminar';
									return 'rechazar';
								}
							},
							handler: eliminarTasa
						}
					]
				}
			]
	});


	var elementosFormaConsulta = [
				{
					xtype: 'combo',
					name: 'cmb_prod',
					id: '_cmb_prod',
					fieldLabel: '* Producto',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'cveProducto',
					emptyText: 'Seleccionar Producto',
					forceSelection : true,
					triggerAction : 'all',
					editable: false,
					typeAhead: true,
					minChars : 1,
					store: 		catalogoProductosData,
					anchor:	'95%'
				},{
					xtype: 'combo',
					name: 'cmb_bf',
					id: '_cmb_bf',
					fieldLabel: '* Banco de Fondeo',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'cveBanco',
					emptyText: 'Seleccionar Banco',
					forceSelection : true,
					triggerAction : 'all',
					editable: false,
					typeAhead: true,
					hidden:true,
					minChars : 1,
					store: 		catalogoBancoFondeoData,
					anchor:	'95%',
					listeners: { select: function(combo){
							var cmbBanco = combo.getValue();
							Ext.getCmp('_cmb_epo').reset();
							catalogoEPOData.load({
								params: {
									icBanco: cmbBanco
								}
							});
						}
					}
				},{
					xtype: 'combo',
					name: 'cmb_epo',
					id: '_cmb_epo',
					fieldLabel: '* Nombre de la EPO',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'cveEPO',
					emptyText: 'Nombre de la EPO',
					forceSelection : true,
					triggerAction : 'all',
					editable: true,
					typeAhead: true,
					minChars : 1,
					store: 		catalogoEPOData,
					anchor:	'95%',
					listeners: { select: function(combo){
										var cmbProd = Ext.getCmp('_cmb_prod').getValue();
										var cmbMoneda = Ext.getCmp('_cmb_mon').getValue();
										var cmbEPO = combo.getValue();
										if(cmbMoneda!=""){
											Ext.getCmp('_cmb_plazo').reset();
											catalogoPlazoData.load({
												params: {
													cveProd: cmbProd,
													cveMoneda: cmbMoneda,
													cveEPO: cmbEPO
												}
											});
										}
									}
								}
				},{
					xtype: 'combo',
					name: 'cmb_mon',
					id: '_cmb_mon',
					fieldLabel: '* Moneda',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'cveMoneda',
					emptyText: 'Seleccionar Moneda',
					forceSelection : true,
					triggerAction : 'all',
					editable: false,
					typeAhead: true,
					minChars : 1,
					store: 		catalogoMonedaData,
					anchor:	'95%',
					listeners: { select: function(combo){
										var cmbProd = Ext.getCmp('_cmb_prod').getValue();
										var cmbEPO = Ext.getCmp('_cmb_epo').getValue();
										var cmbMoneda = combo.getValue();
										Ext.getCmp('_cmb_tasa').reset();
										catalogoTasasData.load({
											params: {
												cveMoneda: cmbMoneda
											}
										});
										//Para cargar el combo de plazos
										if(cmbEPO!=""){
											Ext.getCmp('_cmb_plazo').reset();
											catalogoPlazoData.load({
												params: {
													cveProd: cmbProd,
													cveMoneda: cmbMoneda,
													cveEPO: cmbEPO
												}
											});
										};
										Ext.getCmp('gridConsulta').hide()
									}
								}
				},{
					xtype: 'combo',
					name: 'cmb_tasa',
					id: '_cmb_tasa',
					fieldLabel: 'Tipo de Tasa',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'cveTasa',
					emptyText: 'Seleccionar Tasa',
					forceSelection : true,
					triggerAction : 'all',
					editable: false,
					typeAhead: true,
					minChars : 1,
					store: 		catalogoTasasData,
					anchor:	'95%'
				},{
					xtype: 'combo',
					name: 'cmb_plazo',
					id: '_cmb_plazo',
					fieldLabel: 'Plazo',
					mode: 'local',
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'cvePlazo',
					emptyText: 'Seleccionar',
					forceSelection : true,
					triggerAction : 'all',
					editable: false,
					typeAhead: true,
					minChars : 1,
					store: 		catalogoPlazoData,
					anchor:	'95%'
				}
			];
			
			var fp = new Ext.form.FormPanel({
			id:				'forma',
			width:			570,
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			labelWidth:		150,
			items:			elementosFormaConsulta,
			monitorValid:	true,
			buttons: [
				{
				text:		'Grabar',
				id:		'btnGrabar',
				iconCls:	'icoGuardar',
				formBind:false,
				handler: function(boton, evento) {
									var tasa = Ext.getCmp('_cmb_tasa');
									var plazo = Ext.getCmp('_cmb_plazo');
									if(Ext.getCmp('_cmb_prod').getValue()==''){
										Ext.Msg.alert("Aviso","Debe seleccionar un Producto");
									}
									else if(tasa.getValue()=="" || plazo.getValue()==""){
										if(tasa.getValue()=="")
											Ext.Msg.alert("Aviso","Debe seleccionar una Tasa");
										else
											Ext.Msg.alert("Aviso","Debe seleccionar un Plazo");
									}
									else
										accionConsulta("GRABAR", null)
								}//fin handler
				},{
				text:		'Consultar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:false,
				handler: function(boton, evento) {
									var prod = Ext.getCmp('_cmb_prod');
									var ban = Ext.getCmp('_cmb_bf');
									var epo = Ext.getCmp('_cmb_epo');
									var mon = Ext.getCmp('_cmb_mon');
									if(prod.getValue()=="" || epo.getValue()=="" || mon.getValue()==""){
										if(prod.getValue()=="")
											Ext.Msg.alert("Aviso","Debe seleccionar un Producto");
								//		else if(ban.getValue()=="")
								//			Ext.Msg.alert("Aviso","Debe seleccionar un Banco de Fondeo");
										else if(epo.getValue()=="")
											Ext.Msg.alert("Aviso","Debe seleccionar una EPO");
										else if(mon.getValue()=="")
											Ext.Msg.alert("Aviso","Debe seleccionar un tipo de Moneda");
									}
									else
										accionConsulta("CONSULTAR", null)
								}//fin handler
				},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR", null)
							}
				}
			]
		});
		
//Ventana emergente de modificacion de tasa
	var winModificar = new Ext.Window ({
		id:'winModificar',
		height: 250,
		x: 300,
		y: 100,
		width: 550,
		modal: true,
		closeAction: 'hide',
		title: 'Tasas por EPO-Producto',
		items:[
			{
				xtype:'form',
				id:'fpModificar',
				frame: true,
				border: false,
				style: 'margin: 0 auto',
				bodyStyle:'padding:10px',
				defaults: {
					msgTarget: 'side',
					anchor: '-20'
				},
				labelWidth: 130,
				items:[
					{
						xtype:'label',
						id:'lbl_prod',
						frame:true,
						border: false,
						value:'Producto: Descuento Electronico'
					},NE.util.getEspaciador(10),{
						xtype:'label',
						id:'lbl_epo',
						frame:true,
						border: false,
						value:'Nombre de la EPO: '
					},NE.util.getEspaciador(10),{
						xtype:'label',
						id:'lbl_mon',
						frame:true,
						border: false,
						value:'Moneda: '
					},{
						xtype:'displayfield',
						frame:true,
						border: false,
						value:''
					},{
						xtype: 'combo',
						name: 'tipo_tasab',
						id:	'_cmb_tasab',
						mode: 'local',
						displayField : 'descripcion',
						valueField : 'clave',
						fieldLabel:'Tipo de Tasa',
						maxLength:	100,
						forceSelection : true,
						triggerAction : 'all',
						editable: false,
						typeAhead: true,
						minChars : 1,
						store: 		catalogoTasasDataB
					},{
						xtype: 'combo',
						name: 'plazob',
						id:	'_cmb_plazob',
						fieldLabel:'Plazo',
						mode: 'local',
						displayField : 'descripcion',
						valueField : 'clave',
						maxLength:	20,
						forceSelection : true,
						triggerAction : 'all',
						editable: false,
						typeAhead: true,
						minChars : 1,
						store: 		catalogoPlazoDataB
					}
				],
					buttonAlign:'right',
					buttons:[
						{
							text:'Actualizar',
							iconCls:'icoAceptar',
							handler: function(boton) {
								accionConsulta("ACTUALIZAR",null);
							}
						},{
							text:'Cancelar',
							iconCls: 'icoRechazar',
							handler: function() {
								Ext.getCmp('winModificar').hide();
							}
						}
					]
			}
		]
	});

//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	930,
		//height: 	'auto',
		align: 'center',
		items: 	[
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(10),
			grid
		]

	});	
	
	Ext.StoreMgr.key('catalogoProductosDataStore').load();
	Ext.StoreMgr.key('catalogoBancoFondeoDataStore').load();
	Ext.StoreMgr.key('catalogoEPODataStore').load();
	Ext.StoreMgr.key('catalogoMonedaDataStore').load();

});