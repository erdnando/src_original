Ext.onReady(function() {

	var esEsquemaExtJS =  Ext.getDom('esEsquemaExtJS').value;
	var epo1 = [];
	var cmbTipoFondeo = [];
	var clavePC = [];

	var cancelar =  function() {  
		epo1 = [];
		cmbTipoFondeo = [];
		clavePC = [];

	}
//- - - - - - -  - - - - - HANDLER�S - - - - - - - - - - - - - - - - - - - - - -
	function procesaConsultaValores(opts, success, response) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = Ext.util.JSON.decode(response.responseText);
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if (jsonData != null){	
				var mensaje = jsonData.mensaje;
				Ext.getCmp('btnGuardar').enable();		
				if(jsonData.accion=='G') {
					Ext.MessageBox.alert('Mensaje',mensaje);		
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesaRecargaResultado(opts, success, response) {
		var fpDatos = Ext.getCmp('gridConsulta');
			fpDatos.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var grid1 = Ext.getCmp('gridConsulta');
			grid1.el.unmask();
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var mensaje = jsonData.mensaje;
				if(jsonData.accion=='G') {
					Ext.MessageBox.alert('Mensaje',mensaje);		
				}
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Consultar'
					})
				});
			} 
		}else {
			NE.util.mostrarConnError(response,opts);
			
			
		}
	}
	var procesarSuccessDatosIniciales = function(opts, success, response) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var obtenPorcentaje = Ext.getCmp('tfComision').setValue(jsonData.obtenerPorcentaje);
			}
		}else{
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15parametrizacionDetalleIf.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catProducto = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['1','Descuento Electr�nico'],
			['4','Financiamiento a Distribuidores']			
		 ]
	});
	var tipoFinanciamiento = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['N','NAFIN'],
			['P','PROPIO '],
			['M','MIXTO ']
		 ]
	});
	
	var procesarConsultaData = function(store,arrRegistros,opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
                var fpTipoFinan = Ext.getCmp('formaTipoFinan');
		fpTipoFinan.el.unmask();
                Ext.getCmp('btnTipoFinan').enable();
                
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		if (arrRegistros != null) {		
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0) {
				gridConsulta.show();			
				el.unmask();
				Ext.getCmp('btnGuardar').enable();
			} else {		
				gridConsulta.show();
				Ext.getCmp('btnGuardar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var comboLineas = new Ext.form.ComboBox({  
		id: 'cmbFinan',
		mode: 'local',
		displayField: 'descripcion',
		store:tipoFinanciamiento,
		forceSelection : true,
		triggerAction : 'all',
		
		allowBlank: true,
		valueField: 'clave',
		typeAhead: true,
		minChars : 1
		
	});
	
	Ext.util.Format.comboRenderer = function(comboLineas){
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			if(value!=''){
				var record = comboLineas.findRecord(comboLineas.valueField, value);
				return record ? record.get(comboLineas.displayField) : comboLineas.valueNotFoundText;
			}				
		}
	}
	var consultaData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '15parametrizacionDetalleIf.data.jsp',
		fields : ['clave', 'descripcion','loadMsg'],
		baseParams: {
			informacion: 'Consultar',
		},
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
				{name: 'cg_razon_socialg'},
				{name: 'ic_epo'},
				{name: 'cs_tipo_fondeo'},
				{name: 'cg_tipo_comision'},
				{name: 'cs_recorte_solic'},
				{name: 'producto_comision'},
				{name: 'clavePC'}
			]
		}),
		groupField: 'producto_comision',
		sortInfo:{field: 'producto_comision', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});
	
	var grid = new Ext.grid.EditorGridPanel({
		store: consultaData,
		columnLines:false,
		id: 'gridConsulta',
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		title: ' ',
		hidden: true,
		frame:true,
	   height: 400,
		width:800,
		align: 'center',
		border: true,  
		stripeRows:true,
		clicksToEdit:1,
		loadMask: true,
		columns: [
			{
				header:'EPO',
				tooltip:'EPO',
				dataIndex:'cg_razon_socialg',
				sortable: true,
				resizable: true,
				width:400,
				align: 'left'
			},
			{
				header: 'Tipo de Financiamiento',
				tooltip: 'Tipo de Financiamiento',
				dataIndex: 'cs_tipo_fondeo',
				sortable: true,
				resizable: true,
				width: 400,
				hidden: false,		
				minChars : 1,
				align: 'center',
				editor:comboLineas,
				renderer: Ext.util.Format.comboRenderer(comboLineas)
			},
			{
				header:'producto',
				dataIndex:'producto_comision',
				hidden: true
			},
			{
				header:'producto',
				dataIndex:'clavePC',
				hidden: true
			}
		],
		view: new Ext.grid.GroupingView({
		forceFit:true,
			groupTextTpl: '{[ values.rs[0].data["producto_comision"] ]}  '
		}),
		bbar: {
			xtype: 'toolbar',
			items: [
				'-',
				'->',
				{
					xtype: 'button',
					text: 'Guardar',
					id: 'btnGuardar',
					iconCls: 'icoGuardar',
					handler: function(boton, evento) {
						var  gridConsulta = Ext.getCmp('gridConsulta');
						var store = gridConsulta.getStore();	
						var jsonData = store.data.items;
						var numRegistros=0;
						var fp = Ext.getCmp('forma'); 
						fp.el.unmask();
						cancelar();
						var i=0;
						Ext.each(jsonData, function(item,inx,arrItem){
							epo1.push(item.data.ic_epo);	
							cmbTipoFondeo.push(item.data.cs_tipo_fondeo);
							clavePC.push(item.data.clavePC);
							numRegistros++;
							i++;
						});
                                                var cmbTipoFinan = Ext.getCmp("cmbTipoFinan");
                                                cmbTipoFinan.reset();
						grid.el.mask('Procesando....', 'x-mask-loading');

						Ext.Ajax.request({
							url: '15parametrizacionDetalleIf.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'Guardar_Datos',
								epo1:epo1,
								cmbTipoFondeo:cmbTipoFondeo,
								clavePC:clavePC,
								numRegistros:numRegistros
							}),
							callback:procesaRecargaResultado
						});
					
					}
					
				}		
			]
		}	
	});
	var elementosFormaConsulta = [
		{
			xtype: 'combo',
			name: 'interFinanciero',
			id: 'cmbInterFinanciero',
			hiddenName : 'interFinanciero',
			fieldLabel: '&nbsp;&nbsp;IF',
			mode: 'local', 
			autoLoad: false,
			msgTarget: 'side',
			valueField : 'clave',
			displayField : 'descripcion',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,	
			width: 200,
			store : catalogoIF,
			 tpl:  '<tpl for=".">' +
                '<tpl if="!Ext.isEmpty(loadMsg)">'+
                '<div class="loading-indicator">{loadMsg}</div>'+
                '</tpl>'+
                '<tpl if="Ext.isEmpty(loadMsg)">'+
                '<div class="x-combo-list-item">' +
                '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                '</div></tpl></tpl>'

		},
		{
			xtype: 'combo',
			name: 'producto',
			id: 'cmbProducto',
			hiddenName : 'producto',
			fieldLabel: 'Producto',
			mode: 'local', 
			autoLoad: false,
			valueField : 'clave',
			displayField : 'descripcion',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			width: 200,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catProducto
				
		}                
	];
	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
		hidden: 		true,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: 'Porcentaje Comisi�n Fija',			
				id: 'boton1',					
				handler: function() {
					window.location = '15porcentajeComisionFija.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Parametrizaci�n IF',			
				id: 'boton2',					
				handler: function() {
					window.location = '15parametrizacionIf.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			}, 
			{
				xtype: 'button',
				text: 'Detalle IF',			
				id: 'boton3',					
				handler: function() {
					window.location = '15parametrizacionDetalleIf.jsp';
				}
			}	
		]
	};

	var fp = new Ext.form.FormPanel({
		id				 :'forma',
		width			 :400,
		align: 'center',
		style			 :'margin:0 auto;',
		frame			 :true,
		bodyStyle	 :'padding: 6px',
		labelWidth	 :100,
		title:' ',
		titleCollapse: false,
		collapsible: true,
		defaults		 : {
			msgTarget	: 'side',
			anchor		: '-20'
		},
		items			 :elementosFormaConsulta,
		buttons: [		
                        {
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					var IF = Ext.getCmp("cmbInterFinanciero");
					if(IF.getValue()==""){
						IF.markInvalid('Se requiere seleccionar una IF');
						return "";
					}
					fp.el.mask('Consultando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
                                        var cmbTipoFinan = Ext.getCmp("cmbTipoFinan");
                                         cmbTipoFinan.reset();
				}
			}
		]
	});

	var fpTipoFinan = new Ext.form.FormPanel({
		id				 :'formaTipoFinan',
		width			 :400,
		align: 'center',
		style			 :'margin:0 auto;',
		bodyStyle	 :'padding: 6px',
		labelWidth	 :250,
		title:' ',
		titleCollapse: false,
		collapsible: false,
		defaults		 : {
			msgTarget	: 'side',
			anchor		: '-20'
		},
		items :{
                            xtype: 'combo',
                            name: 'cmbTipoFinan',
                            id: 'cmbTipoFinan',
                            hiddenName : 'cmbTipoFinan',
                            fieldLabel: 'SELECCIONAR TIPO FINANCIAMIENTO',
                            mode: 'local', 
                            autoLoad: false,
                            valueField : 'clave',
                            displayField : 'descripcion',
                            emptyText: 'Seleccione...',					
                            forceSelection : true,
                            width: 10,
                            triggerAction : 'all',
                            typeAhead: true,
                            minChars : 1,
                            allowBlank: true,			
                            store : tipoFinanciamiento				
                        },buttons: [		
			{
				text: 'Aplicar para todas la EPOs',
				id: 'btnTipoFinan',
                                disabled: true,
				iconCls: 'correcto',
				handler: function(boton, evento) {                                
                                    var IF = Ext.getCmp("cmbInterFinanciero");
                                    if(IF.getValue()==""){
                                        IF.markInvalid('Se requiere seleccionar una IF');
                                        return "";
                                    }
                                    var tipoFinan = Ext.getCmp("cmbTipoFinan");
                                    if(tipoFinan.getValue()==""){
                                        tipoFinan.markInvalid('Se requiere seleccionar el tipo financiamiento');
                                        return "";
                                    }                                
                                    Ext.Msg.confirm('Aplicar a EPOs', '�Est� Seguro de Aplicar para todas las EPOs? : ',
                                        function(botonConf) {
                                            if (botonConf == 'ok' || botonConf == 'yes') {                                                    
                                                fp.el.mask('Consultando...', 'x-mask-loading');
                                                fpTipoFinan.el.mask('Consultando...', 'x-mask-loading');
                                                consultaData.load({
                                                    params: Ext.apply(fp.getForm().getValues(),{
                                                    informacion: 'Consultar',
                                                    cmbTipoFinan: Ext.getCmp("cmbTipoFinan").getValue()
                                                    })
                                                });
                                            }
                                        }
                                    );
				}
			}
		]
	});


//----------------------------------- CONTENEDOR -------------------------------------
	var contenedorPrincipal = new Ext.Container({
		applyTo: 'areaContenido',
		width: 	950,
		style: 'margin:0 auto;',
		defaults:{
			style: {margin: '0 auto'}
		},
		align: 'center',
		items: 	[
			NE.util.getEspaciador(10),
			fpBotones,
			NE.util.getEspaciador(10),
			fp,
                        NE.util.getEspaciador(10),
                        fpTipoFinan,
			NE.util.getEspaciador(20),
			grid
		]
	});
	catalogoIF.load();
	if(esEsquemaExtJS!='true'){
		var btn1 = Ext.getCmp('fpBotones');
		btn1.show();
	}
});