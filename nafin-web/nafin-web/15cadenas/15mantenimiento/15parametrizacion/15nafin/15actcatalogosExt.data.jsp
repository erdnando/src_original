<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		java.io.*,
		com.netro.catalogos.*,	
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	if(informacion.equals("ValoresIniciales")){
		response.addHeader("Pragma", "No-cache");
		response.addHeader("Cache-Control", "no-cache");

		JSONObject 	jsonObj	= new JSONObject();
		String separador=""; 
		Properties pc = new Properties(); 
		separador = File.separator;
		File f=new File(strDirectorioPublicacion+"15cadenas"+separador+"15mantenimiento"+separador+"15parametrizacion"+separador+"15nafin"+separador+"","Catalogos.txt");
		FileInputStream fis=new FileInputStream(f);
		pc.load(fis);
		int no_catalogos=Integer.parseInt(pc.getProperty("total"));
		int no_cat_fijos=Integer.parseInt(pc.getProperty("fijos"));

		jsonObj.put("success", new Boolean(true));
		jsonObj.put("dependientes",""+no_cat_fijos);
		jsonObj.put("totales",""+no_catalogos);
		
		for(int i=1; i<=no_cat_fijos; i++) {
			String nomCatalogo = pc.getProperty("cat"+i); 
			String idCatalogo  = "idCat"+i;
			String nameCsession= "nameCat"+i;
		
			String campos = " { "+  					
				"	xtype:  'radio', " +  
				"	fieldLabel: '"+ nomCatalogo +"'," +    
				"	name: 'AAA',"+   
				"	id: '"+ idCatalogo +"', " +
				"	value: '"+ nomCatalogo +"', "+  	
				" inputValue: '"+nomCatalogo +"' ,"+
				"	width: 100"+
				" } ";
		 
				jsonObj.put("cat"+i,campos.toString());
			}//fin del for
		
		for(int i=no_cat_fijos+1; i<=no_catalogos; i++) {
			String nomCatalogo = pc.getProperty("cat"+i); 
			String idCatalogo = "idCat"+i;
			String nameCat = "nameCat"+i;
		
			String campos = " { "+  					
				"	xtype:  'radio', " +  
				"	fieldLabel: '"+ nomCatalogo +"'," +    
				"	name: 'AAA',"+   
				"	id: '"+ idCatalogo +"', " +
				"	value: '"+ nomCatalogo +"', "+  	
				" inputValue: '"+nomCatalogo +"' ,"+
				"	width: 100"+
				" } ";
				
				jsonObj.put("cat"+i,campos.toString());
		}//fin del for	
		
		infoRegresar = jsonObj.toString();	
	}else if (informacion.equals("Enviar")){
		JSONObject 	jsonObj	= new JSONObject();
	 
		String nom_catalogo= (request.getParameter("nom_catalogo") != null)?request.getParameter("nom_catalogo"):"";
		ActualizacionCatalogos actualizacionCatalogos = ServiceLocator.getInstance().lookup("ActualizacionCatalogosEJB",ActualizacionCatalogos.class); 

		String resultado = actualizacionCatalogos.menu(nom_catalogo);
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("RESPUESTA", resultado);
		
		infoRegresar = jsonObj.toString();
	}
%>	

<%= infoRegresar %>