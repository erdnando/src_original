Ext.onReady(function(){
	var plazo         = "";
	var TipoPlazo     = "";
	var ultimo        = "";
	var maximo        = "";
	var ultimoRenglon = "-1";
		
	var limpiaOperacion = function(){
		Ext.getCmp('radioFactoraje').setValue(false);
		Ext.getCmp('radioCobranza').setValue(false);
		Ext.getCmp('radioFactorajeObra').setValue(false);
		TipoPlazo = "";
	}  
			
	var limpiaCredito = function(){
		Ext.getCmp('radioSi').setValue(false);
		Ext.getCmp('radioNo').setValue(false);
		TipoPlazo = "";		
	}
		
	var procesarPlazo = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			plazo = info.PLAZO;
			maximo = info.ULTIMO;
			
			var localplazo= parseInt(plazo);
			if(localplazo != 1){
				Ext.getCmp('numMinimo').setValue(plazo);
				Ext.getCmp('numMinimo').disable();
			}else{
				Ext.getCmp('numMinimo').enable();
				Ext.getCmp('numMinimo').setValue(plazo);
			}
			Ext.getCmp('leyendaEspere').hide();
			Ext.getCmp('numMaximo').setValue('');
			
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	
	var procesarDatosEPO = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('operaMesesEPO').setValue(info.operaMesesEPO); //F09-2015
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	
	
	var procesarPlazo2 = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			plazo = info.PLAZO;
			maximo = info.ULTIMO;
			
			var localplazo= parseInt(plazo);
			if(localplazo != 1){
				Ext.getCmp('numMinimo').setValue(plazo);
				Ext.getCmp('numMinimo').disable();
			}else{
				Ext.getCmp('numMinimo').enable();
				Ext.getCmp('numMinimo').setValue(plazo);
			}
			Ext.getCmp('leyendaEspere').hide();
			Ext.getCmp('numMaximo').setValue('');
		
			consultaDataGrid.load({
						params: {
										comboEpo:Ext.getCmp('idComboEpo').getValue(),
										comboMoneda:Ext.getCmp('idComboMoneda').getValue(),
										comboProducto:Ext.getCmp('idComboProducto').getValue(),
										comboCartera:Ext.getCmp('idComboCartera').getValue(),
										tipoPlazo: TipoPlazo,
										comboTipoPago: Ext.getCmp('idComboTipoPago').getValue()
									}
			}); 
		
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}	
	
	
	var procesarAceptar = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);

			if(info.MENSAJE != '')
				Ext.MessageBox.alert("Mensaje",info.MENSAJE);	
			//alert(info.MENSAJE);
			
			grid.hide();		
			Ext.Ajax.request({
				url: '15basesopercExt.data.jsp',
				params: {
					informacion: 'ConsiguePlazo',
					comboEpo:Ext.getCmp('idComboEpo').getValue(),
					comboMoneda:Ext.getCmp('idComboMoneda').getValue(),
					comboProducto:Ext.getCmp('idComboProducto').getValue(),
					comboCartera:Ext.getCmp('idComboCartera').getValue(),
					tipoPlazo: TipoPlazo,
					comboTipoPago: Ext.getCmp('idComboTipoPago').getValue()
				},
				callback: procesarPlazo
			});			
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}
		
	
	var procesarValidaMesesSinIntereses = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);

			if(info.TOTALMESES == '0')  { 
			
				Ext.Ajax.request({
				url: '15basesopercExt.data.jsp',
				params: {
					informacion: 'Aceptar',
					comboEpo:Ext.getCmp('idComboEpo').getValue(),
					comboMoneda:Ext.getCmp('idComboMoneda').getValue(),
					comboProducto:Ext.getCmp('idComboProducto').getValue(),
					comboCartera:Ext.getCmp('idComboCartera').getValue(),
					tipoPlazo: TipoPlazo,
					valorMinimo: Ext.getCmp('numMinimo').getValue() ,
					valorMaximo: Ext.getCmp('numMaximo').getValue(),
					comboOperacion: Ext.getCmp('idComboOperacion').getValue(),
					comboAcreditado: Ext.getCmp('idComboAcreditado').getValue(),
					comboTipoPago: Ext.getCmp('idComboTipoPago').getValue()
				},
				callback: procesarAceptar
			});
			
			}else {			
				Ext.MessageBox.alert("Mensaje","Usted ya cuenta con una Base de Operaci�n Registrada para Meses sin Intereses");
			}
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	var fnAceptar = function (){
		if(Ext.getCmp('idComboEpo').getValue() == ''){
			Ext.getCmp('idComboEpo').markInvalid('Debe de Seleccionar una EPO para realizar la consulta');
			return ;
		}
		if(Ext.getCmp('idComboMoneda').getValue() == ''){
			Ext.getCmp('idComboMoneda').markInvalid('Debe de Seleccionar una Moneda');
			return ;
		}
		if(Ext.getCmp('idComboProducto').getValue() == ''){
			Ext.getCmp('idComboProducto').markInvalid('Debe de Seleccionar un Producto');
			return ;
		}
		if(Ext.getCmp('idComboCartera').getValue() == ''){
			Ext.getCmp('idComboCartera').markInvalid('Debe de elegir un tipo de Cartera');
			return ;
		}	
		
		if(Ext.getCmp('idComboTipoPago').getValue() == '1') {
		
			if(Ext.getCmp('numMaximo').getValue() == ''){
				Ext.MessageBox.alert("Alerta","Debe de poner un Plazo M�ximo");
				Ext.getCmp('numMaximo').markInvalid('Debe de Poner un Plazo M�ximo');
				return ;
			}
			if(Ext.getCmp('numMinimo').getValue() == ''){
				Ext.getCmp('numMinimo').markInvalid('Debe de Poner un Plazo M�nimo');
				return ;		
			} 		
			if(parseInt(Ext.getCmp('numMaximo').getValue()) <= parseInt(Ext.getCmp('numMinimo').getValue())){
				Ext.getCmp('numMaximo').markInvalid('El Plazo Maximo debe de ser Mayor al Plazo Minimo');
				return ;
			}
			
		}
			
		if(Ext.getCmp('idComboOperacion').getValue() == ''){
			Ext.getCmp('idComboOperacion').markInvalid('Debe de Seleccionar un C�digo de Operaci�n');
			return ;
		}	
		//Fodea 09-2015
		if(Ext.getCmp('idComboProducto').getValue() == '4'  && Ext.getCmp('idComboMoneda').getValue() == '1' &&  Ext.getCmp('idComboCartera').getValue() == '2' &&  Ext.getCmp('operaMesesEPO').getValue() == 'S'   ){
			if(Ext.getCmp('idComboTipoPago').getValue() == ''){
				Ext.getCmp('idComboTipoPago').markInvalid('Debe de elegir un tipo de Pago');
				return ;		
			}
		
			Ext.Ajax.request({
				url: '15basesopercExt.data.jsp',
				params: {
					informacion: 'ValidaMesesSinIntereses',
					comboEpo:Ext.getCmp('idComboEpo').getValue()				
				},
				callback: procesarValidaMesesSinIntereses
			});
		
		}else {
		
			Ext.Ajax.request({
				url: '15basesopercExt.data.jsp',
				params: {
					informacion: 'Aceptar',
					comboEpo:Ext.getCmp('idComboEpo').getValue(),
					comboMoneda:Ext.getCmp('idComboMoneda').getValue(),
					comboProducto:Ext.getCmp('idComboProducto').getValue(),
					comboCartera:Ext.getCmp('idComboCartera').getValue(),
					tipoPlazo: TipoPlazo,
					valorMinimo: Ext.getCmp('numMinimo').getValue() ,
					valorMaximo: Ext.getCmp('numMaximo').getValue(),
					comboOperacion: Ext.getCmp('idComboOperacion').getValue(),
					comboAcreditado: Ext.getCmp('idComboAcreditado').getValue(),
					comboTipoPago: Ext.getCmp('idComboTipoPago').getValue()
				},
				callback: procesarAceptar
			});
		
		}
	}
	
		
	var revisarSelecciones = function(){
		if(Ext.getCmp('idComboEpo').getValue() == ''){
			Ext.getCmp('idComboEpo').markInvalid('Debe de seleccionar una EPO para realizar la consulta');
			return ;
		}
		if(Ext.getCmp('idComboMoneda').getValue() == ''){
			Ext.getCmp('idComboMoneda').markInvalid('Debe de seleccionar una Moneda');
			return ;
		}
		if(Ext.getCmp('idComboProducto').getValue() == ''){
			Ext.getCmp('idComboProducto').markInvalid('Debe de seleccionar un Producto');
			return ;
		}
				
		if(Ext.getCmp('idComboProducto').getValue() == '1'){
			if(TipoPlazo == ''){
				Ext.getCmp('radioFactoraje').setValue(true);//la var global
				TipoPlazo = 'F';
			}
			Ext.getCmp('cfCredito').hide();
			Ext.getCmp('cf5').show();
			if(Ext.getCmp('idComboCartera').getValue() == ''){
				Ext.getCmp('idComboCartera').markInvalid('Debe de elegir un tipo de Cartera');
				return ;
			}
		}else if(Ext.getCmp('idComboProducto').getValue() == '5'){
			if(TipoPlazo == 'C'){
				Ext.getCmp('radioNo').setValue(true)
			}			
			Ext.getCmp('cf5').hide();
			Ext.getCmp('cfCredito').show();

			if(TipoPlazo != 'E' && TipoPlazo != 'C'){
				Ext.MessageBox.alert("Alerta","Debe seleccionar si opera Credito de Exportaci�n o no");
				return ;
			}
		
		} else if(Ext.getCmp('idComboProducto').getValue() == '4'){
			//F09-2015
			if(Ext.getCmp('idComboMoneda').getValue() == '1' &&  Ext.getCmp('idComboCartera').getValue() == '2' &&  Ext.getCmp('operaMesesEPO').getValue() == 'S'   ){
				Ext.getCmp('idComboTipoPago').show();
				Ext.getCmp('idComboTipoPago').markInvalid('Debe de elegir un tipo de Pago');
				return ;
			}
							
		}else{
		//limpiar variables			
			Ext.getCmp('cf5').hide();
			Ext.getCmp('cfCredito').hide();

		}
		if(Ext.getCmp('idComboCartera').getValue() == ''){
			Ext.getCmp('idComboCartera').markInvalid('Debe de elegir un tipo de Cartera');
			return ;
		} 

	   //F09-2015
		if(Ext.getCmp('idComboProducto').getValue() != '4' ||  Ext.getCmp('idComboMoneda').getValue() != '1' || Ext.getCmp('idComboCartera').getValue() != '2' ||  Ext.getCmp('operaMesesEPO').getValue() == 'N' ){
			Ext.getCmp('idComboTipoPago').hide();			
		}
		
		Ext.getCmp('leyendaEspere').show();	
		
		Ext.Ajax.request({
			url: '15basesopercExt.data.jsp',
			params: {
				informacion: 'ConsiguePlazo',
				comboEpo:Ext.getCmp('idComboEpo').getValue(),
				comboMoneda:Ext.getCmp('idComboMoneda').getValue(),
				comboProducto:Ext.getCmp('idComboProducto').getValue(),
				comboCartera:Ext.getCmp('idComboCartera').getValue(),
				tipoPlazo: TipoPlazo,
				comboTipoPago: Ext.getCmp('idComboTipoPago').getValue()
			},
			callback: procesarPlazo
		});	
		
	}
	
	var verEliminar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave = registro.get('BASEOPERACION'); 
						Ext.Msg.confirm('Mensaje', 
							'�Esta seguro de Eliminar el Registro?',
							function(botonConf) {
								if (botonConf == 'ok' || botonConf == 'yes' || botonConf == 'si' ) {
									Ext.Ajax.request({
										url: '15basesopercExt.data.jsp',
										params: {
											informacion: 'Eliminar',
											baseOperacion: clave											
										},
										callback: procesarAceptar
									});	
								}
							}
						);		
	}
	
	
	var verModificar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var plazoMax = registro.get('PLAZOMAXIMO'); 
		var plazoMin = registro.get('PLAZOMINIMO');
		var clave = registro.get('BASEOPERACION');
		var comborecurso = registro.get('RECURSO');
		
		if( parseInt(plazoMin) >= parseInt(plazoMax)){
			Ext.MessageBox.alert("Alerta","El Plazo M�ximo debe de ser Mayor al Plazo M�nimo");
			return ;
		}
 
		if(comborecurso == "Pyme")
			comborecurso = 'S';
		else if(comborecurso == "Epo")
			comborecurso = 'N';
										
		Ext.Ajax.request({
			url: '15basesopercExt.data.jsp',
			params: {
				informacion: 'Modificar',
				plzMax: plazoMax,
				baseOperacion: clave,
				recurso: comborecurso
			},
			callback: procesarAceptar
		});
	}	
	
	
	var RenPlazo = function(value, metadata, record, rowindex, colindex, store) {
									if(record.data.TIPOPLAZO == "F")
										return 'Factoraje';
									else if(record.data.TIPOPLAZO == "C"){
										return 'Cobranza';
									}else if (record.data.TIPOPLAZO == "O"){
										return 'Factoraje Obra P�blica'
									}else if (record.data.TIPOPLAZO === "L"){
										return 'Floating';
									}
								
								}
								
		var RenCartera = function(value, metadata, record, rowindex, colindex, store) {
									if(record.data.TIPOCARTERA == "1")
										return '1er Piso';
									else 
										return '2do Piso';
								}								
	
	
	
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');	
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			
			Ext.getCmp('btnConsultar').enable();
			  ultimo = ""+(store.getTotalCount() - 1);
			 	  			  
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15basesopercExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [	
			{name: 'BASEOPERACION'},
			{name: 'RAZONSOCIAL'},
			{name: 'CODIGOBASE'},
			{name: 'PLAZOMINIMO'},
			{name: 'PLAZOMAXIMO'},
			{name: 'DESCRIPCION'},
			{name: 'NOMBRE'},
			{name: 'RECURSO'},
			{name: 'MONEDA'},
			{name: 'TIPOPLAZO'},
			{name: 'TIPOCARTERA'},
			{name: 'PANTALLA'},
			{name: 'TIPOPAGO'},
			{name: 'IG_TIPO_PAGO'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var procesarMoneda = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboMoneda').setValue('');
	}
	
	var procesarEpo = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboEpo').setValue('');
	}
	
	var procesarProducto = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboProducto').setValue('');
		estatusActualProducto= Ext.getCmp('idComboProducto').getValue(); 
	}
	
	var procesarOperacion = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboOperacion').setValue(''); 
	}
	
	var procesarCartera = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboCartera').setValue('');
	}	
	
	
	var CatalogoMoneda = new Ext.data.JsonStore
	({
		id: 'catMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesopercExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoMoneda'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarMoneda,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});  
	
	var CatalogoEpo = new Ext.data.JsonStore
	({
		id: 'catEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesopercExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarEpo,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
	
	var CatalogoProducto = new Ext.data.JsonStore
	({
		id: 'catProducto',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesopercExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoProducto'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarProducto,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});  
	
	var CatalogoOperacion = new Ext.data.JsonStore
	({
		id: 'catOperacion',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesopercExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoOperacion'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarOperacion,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
	
  
  var dataTipoPago = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['','Seleccionar'],
			['1','Financiamiento con intereses '],
			['2','Meses sin intereses ']
		]		
	 });
	var dataCartera = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['','Seleccionar'],
			['1','1er Piso'],
			['2','2do Piso']
		]		
	 });
	 
	var dataAcreditado = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['N','EPO'],
			['S','PYME']
		]		
	 });
	 
	var dataAcreditado2 = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['N','EPO'],
			['S','PYME']
		]		
	 });	 

	
	var Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);

	var elementosForma = 
	[			
		{
			xtype: 'combo',
			fieldLabel: 'Nombre EPO',
			forceSelection: true,
			autoSelect: true,
			name:'claveEpo',
			id:'idComboEpo',
			displayField: 'descripcion',
			allowBlank: true,
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			autoLoad: false,
			typeAhead: true,
			minChars: 1,
			store: CatalogoEpo,  
			listeners:{
				select: function(combo){
						grid.hide();
						revisarSelecciones();
							
							Ext.Ajax.request({
							url: '15basesopercExt.data.jsp',
							params: {
								informacion: 'parametrizacionEPO',
								comboEpo:Ext.getCmp('idComboEpo').getValue()								
							},
							callback: procesarDatosEPO
						});
		
				}
			},
			anchor:	'97%'
		},
		{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			forceSelection: true,
			autoSelect: true,
			name:'claveMoneda',
			id:'idComboMoneda',
			displayField: 'descripcion',
			allowBlank: true,
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			autoLoad: false,
			typeAhead: true,
			minChars: 1,
			store: CatalogoMoneda, 
			listeners:{
				select: function(combo){
						grid.hide();
						if(combo.getValue() != ''){
							revisarSelecciones();
						}
				}				
			},					
			anchor:	'35%'
		},
		{
				xtype: 'combo',
				fieldLabel: 'Producto',
				forceSelection: true,
				autoSelect: true,
				name:'claveProducto',
				id:'idComboProducto',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoProducto,  
				listeners:{
					select: function(combo){
							grid.hide();
							limpiaOperacion();
							limpiaCredito();
							if(combo.getValue() != ''){
								revisarSelecciones();
							}
							
							Ext.getCmp('disRadiofloating').hide();
							Ext.getCmp('radiofloating').hide();							
							
							if(combo.getValue() === '1'){
								Ext.getCmp('radiofloating').show();
								Ext.getCmp('disRadiofloating').show();
							}
							
					}					
			},	
			anchor: '40%'
		},
		{
			xtype: 'compositefield',
			id: 'cfCredito',
			hidden: true,
			fieldLabel:'Cr�dito de Exportaci�n',
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioSi',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "E"
											grid.hide();
											revisarSelecciones();
										}
									}
					}
				},	{
					xtype: 'displayfield',
					value: 'Si	',
					width: 100
				}, { 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioNo',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "C"
											grid.hide();
											revisarSelecciones();
										}
									}
					}
				},{
					xtype: 'displayfield',
					value: 'No ',
					width: 100
				}
				
			]
		},
		{
			xtype: 'combo',
			fieldLabel: 'Tipo de Cartera',
			forceSelection: true,
			autoSelect: true,
			name:'claveCartera',
			id:'idComboCartera',
			displayField: 'descripcion',
			allowBlank: true,
			editable:true,
			valueField: 'clave',
			mode: 'local',
			triggerAction: 'all',
			autoLoad: false,
			typeAhead: true,
			minChars: 1,
			store: dataCartera,  
					listeners:{
					select:	function(combo){
						grid.hide();
						if(combo.getValue() != ''){
							revisarSelecciones();
						}
					}
				},
			value: '',
			anchor: '30%'
		},		
			{
					xtype: 'combo',
					fieldLabel: 'Tipo de Pago',
					forceSelection: true,
					hidden:true,
					autoSelect: true,
					name:'comboTipoPago',
					id:'idComboTipoPago',
					displayField: 'descripcion',
					allowBlank: true,					
					editable:true,
					valueField: 'clave',
					mode: 'local',
					triggerAction: 'all',
					autoLoad: false,
					typeAhead: true,
					minChars: 1,
					value: '',
					width	: 200,
					store: dataTipoPago,
					anchor: '40%',
					listeners:{
						select:	function(combo){
							if(combo.getValue() == '2' ){
								Ext.getCmp('numMinimo').hide();
								Ext.getCmp('numMaximo').hide();
							}else  {
								Ext.getCmp('numMinimo').show();
								Ext.getCmp('numMaximo').show();
							
							}
						}	
						
					}				
				},
			
			{
			xtype: 'compositefield',
			id: 'cf5',
			fieldLabel:'Tipo de Operaci�n',
			hidden:true,
			msgTarget: 'side',
			items: [

				{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioFactoraje',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "F"
											grid.hide();
											revisarSelecciones();
										}
									}
					}
				},	{
					xtype: 'displayfield',
					value: 'Factoraje	',
					width: 100
				}, 				{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioCobranza',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "C"
											grid.hide();
											revisarSelecciones();
										}
									}
					}
				},{
					xtype: 'displayfield',
					value: 'Cobranza ',
					width: 100
				},	{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioFactorajeObra',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "O"
											grid.hide();
											revisarSelecciones();
										}
									}
					}
					
				},{
					xtype: 'displayfield',
					value: 'Factoraje Obra P�blica ',
					width: 200
				}	,
				
				{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radiofloating',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "L";
											grid.hide();
											revisarSelecciones();
										}
									}
					}
					
				},
				{
					xtype: 'displayfield',
					id: 'disRadiofloating',
					value: 'Floating ',
					width: 200
				}
				
			]
		},
		{
				xtype: 	'label',
				id:	 	'leyendaEspere',
				hidden: 	true,
				html:  	'<center><font size="2">Espere un momento ...</font></h2></center><BR/>'
		},
		{
				xtype:'numberfield',
				height: 20,
				fieldLabel: 'Plazo M�nimo',
				decimalPrecision: 0,
				width: 70,
				maxLength: 3,
				value: '',
				id:'numMinimo',
				anchor: '25%'
		},
		{
				xtype:'numberfield',
				height: 20,
				fieldLabel: 'Plazo M�ximo',
				width: 70,
				decimalPrecision: 0,
				maxLength: 3,
				value: '',
				id:'numMaximo',
				anchor: '25%'
		},
		{
				xtype: 'combo',
				fieldLabel: 'Cod. Operaci�n',
				forceSelection: true,
				autoSelect: true,
				name:'claveOperacion',
				id:'idComboOperacion',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoOperacion,  
				anchor: '75%',
				width:470
		},
		{
			xtype: 'compositefield',
			id: 'cfAcreditado',
			msgTarget: 'side',
			items: [ 
				{
					xtype: 'combo',
					fieldLabel: 'Acreditado',
					forceSelection: true,
					autoSelect: true,
					name:'claveAcreditado',
					id:'idComboAcreditado',
					displayField: 'descripcion',
					allowBlank: true,
					editable:true,
					valueField: 'clave',
					mode: 'local',
					triggerAction: 'all',
					autoLoad: false,
					typeAhead: true,
					minChars: 1,
					store: dataAcreditado,  
					value: 'S',
					width: 180
				}
			] 
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'operaMesesEPO', 	value: '' }			
	]// fin elementos forma
	


	
	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: false,
		title: '<center>Mantenimiento a Bases de Operaci�n Cadenas</center>',
		frame: true,
		bodyStyle: 'padding: 10px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
				buttons:[
				{
					xtype:	'button',
					id:		'btnConsultar',
					width: 50,
					text:		'Consultar',
					iconCls: 'icoBuscar',
					handler: function(boton, evento) {
									if(Ext.getCmp('idComboEpo').getValue() == ''){
										Ext.getCmp('idComboEpo').markInvalid('Debe de Seleccionar una EPO para realizar la consulta');
										return ;
									}
									if(Ext.getCmp('idComboMoneda').getValue() == ''){
										Ext.getCmp('idComboMoneda').markInvalid('Debe de Seleccionar una Moneda');
										return ;
									}
									if(Ext.getCmp('idComboProducto').getValue() == ''){
										Ext.getCmp('idComboProducto').markInvalid('Debe de Seleccionar un Producto');
										return ;
									}
									if(Ext.getCmp('idComboCartera').getValue() == ''){
											Ext.getCmp('idComboCartera').markInvalid('Debe de elegir un tipo de Cartera');
											return ;
									}						
									ultimoRenglon = "-1";		
									grid.show();
									if(Ext.getCmp('idComboProducto').getValue() == '1'){
										var cm = grid.getColumnModel();
										grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOPLAZO'), false);
									}else{
										var cm = grid.getColumnModel();
										grid.getColumnModel().setHidden(cm.findColumnIndex('TIPOPLAZO'), true);
									}
									
									if(Ext.getCmp('idComboProducto').getValue() == '4'  && Ext.getCmp('idComboMoneda').getValue() == '1' &&  Ext.getCmp('idComboCartera').getValue() == '2' &&  Ext.getCmp('operaMesesEPO').getValue() == 'S'   ){
										if(Ext.getCmp('idComboTipoPago').getValue() == ''){
											Ext.getCmp('idComboTipoPago').markInvalid('Debe de elegir un tipo de Pago');
											return ;	
										}
									}									
														
									boton.disable();		
									
									
									Ext.Ajax.request({
										url: '15basesopercExt.data.jsp',
										params: {
											informacion: 'ConsiguePlazo',
											comboEpo:Ext.getCmp('idComboEpo').getValue(),
											comboMoneda:Ext.getCmp('idComboMoneda').getValue(),
											comboProducto:Ext.getCmp('idComboProducto').getValue(),
											comboCartera:Ext.getCmp('idComboCartera').getValue(),
											tipoPlazo: TipoPlazo,
											comboTipoPago: Ext.getCmp('idComboTipoPago').getValue()
										},
										callback: procesarPlazo2
									});
									/*
									consultaDataGrid.load({
											params: {
															comboEpo:Ext.getCmp('idComboEpo').getValue(),
															comboMoneda:Ext.getCmp('idComboMoneda').getValue(),
															comboProducto:Ext.getCmp('idComboProducto').getValue(),
															comboCartera:Ext.getCmp('idComboCartera').getValue(),
															tipoPlazo: TipoPlazo
														}
									});   */		
						},
						style: 'margin:0 auto;',
						align:'rigth'
					},{
						xtype:	'button',
						id:		'btnAceptar',
						text:		'Aceptar',
						iconCls: 'icoAceptar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							fnAceptar();
						}						
					},{
						xtype:	'button',
						id:		'btnCancel',
						text:		'Limpiar',
						iconCls: 'icoLimpiar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							window.location = '15basesopercExt.jsp';
						}
					},{
						xtype:	'button',
						id:		'btnRegresar',
						text:		'Regresar',
						iconCls: 'icoContinuar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							window.location = '15basesoperExt.jsp';
						}
					}					
				],
		labelWidth: 130,
		height: 320,
		width: 900
	});



	var grid = new Ext.grid.EditorGridPanel( {
		store: consultaDataGrid,
		clicksToEdit:1,
		sortable:true,
		hidden:true,
		id: 'grid',
		columns: [
			{
				header: '<center>Nombre EPO</center>',
				dataIndex: 'RAZONSOCIAL',
				width: 280,
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								return value;
							},				
				align: 'left'				
			},{
				header: '<center>Moneda</center>',
				dataIndex: 'MONEDA',
				align: 'left',
				width: 155
			},{
				header: '<center>Producto</center>',
				dataIndex: 'NOMBRE',
				align: 'left',
				width: 155
			},{
				header: '<center>Tipo de Cartera<c/enter>',
				dataIndex: 'TIPOCARTERA',
				align: 'left',
				width: 110,
				renderer: RenCartera
			},			
			{
				header: '<center>Tipo Operaci�n<c/enter>',
				dataIndex: 'TIPOPLAZO',
				hidden:true,
				align: 'left',
				width: 160,
				renderer: RenPlazo
			},{
				header: 'Plazo M�nimo',
				dataIndex: 'PLAZOMINIMO',
				align: 'center',
				width: 110
			},{
				header: 'Plazo M�ximo',
				dataIndex: 'PLAZOMAXIMO',				
				align: 'center',
				editor: { 
								xtype:'numberfield',
								height: 10,
								maxLength: 3,
								allowBlank: false,
								decimalPrecision: 0,
								id:'txtmodif',							
								listeners:{
								}				
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
				
					if(registro.get('PLAZOMAXIMO') == maximo){
						ultimoRenglon = rowIndex;
					}
					if(rowIndex == ultimoRenglon ){	
						if( registro.get('IG_TIPO_PAGO') == 1 ) {
							return NE.util.colorCampoEdit(value,metadata,registro);
						}else  {
							return value; 
						}
					}else{
						return value;
					}
				},
				width: 110
			},{
				header: 'C�digo Base',
				dataIndex: 'CODIGOBASE',
				align: 'center',
				width: 110
			},{
				header: '<center>Descripci�n</center>',
				dataIndex: 'DESCRIPCION',
				align: 'left',
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';
								return value;
							},				
				width: 390
			},{
				header: '<center>Acreditado</center>',
				dataIndex: 'RECURSO',
				align: 'left',
				width: 110,
				editor: {
						xtype: 'combo',
						forceSelection: true,
						autoSelect: true,
						name:'claveAcreditado2',
						id:'idComboAcreditado2',
						displayField: 'descripcion',
						allowBlank: false,
						valueField: 'clave',
						mode: 'local',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: dataAcreditado2,  
						width:120
				},
				renderer:function(value,metadata,record2,rowIndex,colIndex,store){
					if(ultimoRenglon == rowIndex){
						var record = Ext.getCmp('idComboAcreditado2').findRecord(Ext.getCmp('idComboAcreditado2').valueField, value);
						record ? txt = record.get(Ext.getCmp('idComboAcreditado2').displayField) : txt = value;
						return NE.util.colorCampoEdit(txt,metadata,record2);
					}else {
						return value;
					}
				}	
			},{
	      xtype: 'actioncolumn',
				header: 'Modificar o Eliminar',
				width: 110,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(ultimoRenglon == rowIndex){
								this.items[0].tooltip = 'Modificar';
								return 'modificar';
							}else{
								return '';	
							}										
						},
						handler: verModificar
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(ultimoRenglon == rowIndex){
								this.items[1].tooltip = 'Eliminar';
								return 'borrar';										
							}else{
								return '';	
							}
						},	
						handler: verEliminar
					}
				]				
			}			
		],		
		loadMask: true,
		style: 'margin:0 auto;',
		height: 250,		
		width: 930,
		frame: true,
		id:'grid',	
		listeners:{												
			beforeedit: function(object){
			
				var record = object.record;
				var grid = object.grid; 
				var campo= object.field;				
				var ig_tipo_pago=record.data['IG_TIPO_PAGO'];		
				
				if ( ( (object.field == 'RECURSO' || object.field == 'PLAZOMAXIMO') && object.row != ultimo ) 
				||  (ig_tipo_pago==2 && object.field == 'PLAZOMAXIMO' )   ){
					object.cancel = true;
				}
				
			}//finbefore	
		}//fin listen			
	});


//------ Componente Principal -------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [NE.util.getEspaciador(10),
				  fp,NE.util.getEspaciador(10),grid ]
	});


	CatalogoEpo.load();
	CatalogoProducto.load();
	CatalogoMoneda.load();
	CatalogoOperacion.load();

});