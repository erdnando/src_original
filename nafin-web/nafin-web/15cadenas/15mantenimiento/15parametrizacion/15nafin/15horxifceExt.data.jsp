<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.exception.*,
		com.netro.procesos.*,
		com.netro.descuento.*,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

ManejoServicio servicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);

if(informacion.equals("ConsultaHorarios") ) {
	ArrayList list = new ArrayList();
	list = servicio.getHorarioIfGralCE();
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size()-1; i++){
		JSONObject jo=new JSONObject();
		jo.put("HORAINI",list.get(0).toString());
		jo.put("HORAFIN",list.get(1).toString());
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("GuardarHorarios")){
	
	String horaInicio = (request.getParameter("horaInicio")==null)?"":request.getParameter("horaInicio");
	String horaFinal = (request.getParameter("horaFinal")==null)?"":request.getParameter("horaFinal");
	servicio.setHorarioGralCE(horaInicio, horaFinal);
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}

%>
<%=infoRegresar%>


