Ext.onReady(function(){
//---------------------Variables------------------------------
var Todas = Ext.data.Record.create([
    {name: "clave", type: "string"},
    {name: "descripcion", type: "string"},
    {name: "loadMsg", type: "string"}
	]);

var cvePerf;

//---------------------Handlers---------------------------------


	var procesaCargaArchivo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

			if(resp.validaElementos){

				if(resp.errorImagen){
					gridError.setVisible(true);
					storeImagenesError.loadData(resp.registros);
				}else{
					window.open('15admnafparamimg_previewExt.jsp?ic_epo='+resp.ic_epo+'&nombreArchivoZip='+resp.nombreArchivoZip+'&operacion=CAPTURA&logo='+resp.logo, 'cuentas',
					'toolbar=no,width=990,height=765,toolbar=no,menubar=no,status=no,scrollbars=yes,resizable=yes,top=0,left=0');

				}

			}else
			{
				Ext.Msg.alert("Error",	"El Archivo ZIP no contiene los archivos necesarios para personalizar la"+
				"<br/>imagen de la EPO con el nuevo esquema.");
			}


		}else{
			// Suprimir mascaras segun se requiera
			var element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Ocultar ventana de avance validacion si esta esta siendo mostrada
        // hideWindowAvanceValidacion();

         // Ocultar mascara del panel de resultados de la validacion
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Mostrar mensaje de error
			Ext.Msg.alert("Error",	"Error al cargar los archivos. Favor de probar m�s tarde");

		}

	}
	var procesaValoresIniciales = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);
			cvePerf=resp.cvePerf;
			if(resp.cvePerf==8){
			Ext.getCmp('captura').setVisible(false);
			Ext.getCmp('predeterminado').setVisible(false);
				var radioVista= Ext.getCmp('vistaPrevia');
				radioVista.setValue(true);
				catalogoBanco.load({
					params: {ic_banco:2}
				});
			}


		}else{
			// Suprimir mascaras segun se requiera



		}

	}
var quitarMascara= function(opts, success, response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
	fp.el.unmask();
	}else{
	NE.util.mostrarConnError(response,opts);
	}
}

var procesarPredeterminado = function(opts, success, response){
	fp.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		Ext.Msg.show({
			title:	"Mensaje",
			msg:		'El dise�o predeterminado para la EPO, ha sido restablecido',
			buttons:	Ext.Msg.OK,
			fn: function resultMsj(btn){
				 if (btn == 'ok') {
					fp.getForm().reset();
				}
			},
			closable:false,
			icon: Ext.MessageBox.QUESTION
		});
	}else{
	NE.util.mostrarConnError(response,opts);
	}
}

var procesarVistaPrevia = function(opts, success, response){
	fp.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		resp=Ext.util.JSON.decode(response.responseText);
		window.open('15admnafparamimg_previewExt.jsp?ic_epo='+resp.ic_epo+'&operacion=CONSULTA&logo='+resp.logo, 'cuentas',
				'toolbar=no,width=990,height=600,toolbar=no,menubar=no,status=no,scrollbars=yes,resizable=yes,top=0,left=0');
	}else{
	NE.util.mostrarConnError(response,opts);
	}
}


var procesarCatalogoEpo = function(){
		if(!Ext.getCmp('predeterminado').checked){
			catalogoEpo.insert(0,new Todas({
				clave: "-1",
				descripcion: "Dise�o Predeterminado",
				loadMsg: ""}));
			//catalogoBanco.commitChanges();
		}

		Ext.Ajax.request({
				url: '15AdmNafParamImgExt.data.jsp',
				params: {

							informacion: 			"tiempo"

				},callback: quitarMascara
			});
}

var procesarCatalogoBanco = function(){

		if(cvePerf==8){
			Ext.getCmp('banco').setValue(2);
			catalogoEpo.load({
				params: {nueva:false}
			});
			}
		else{

			catalogoBanco.insert(0,new Todas({
			clave: "",
			descripcion: "Todos los Bancos",
			loadMsg: ""}));
			catalogoBanco.commitChanges();
			Ext.getCmp('banco').setValue('');
	}
}

//---------------------Stores-------------------------------
var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15AdmNafParamImgExt.data.jsp',
		baseParams:
		{
		 informacion: 'catalogoEpo'
		},
		autoLoad: false,
		listeners:
		{

		 load: procesarCatalogoEpo,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload:{
					fn: function(store,options){
							 NE.util.initMensajeCargaCombo(store,options);
						Ext.apply(options.params,{
											Hbanco:Ext.getCmp('banco').getValue()});
					}
				}
		}
  });
 var catalogoBanco = new Ext.data.JsonStore
  ({
	   id: 'catalogoBanco',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15AdmNafParamImgExt.data.jsp',
		baseParams:
		{
		 informacion: 'catalogoBanco'
		},
		autoLoad: false,
		listeners:
		{
		load: procesarCatalogoBanco,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

	var storeImagenesError = new Ext.data.JsonStore({

		fields: [
					{name: 'IMAGEN'},
					{name: 'ERROR'}
		]
	});


//----------------------Componentes-----------------------
var elementosCambioPanel=[
	{
							xtype:'radiogroup',
							id:	'radioGp',
							columns: 3,
							hidden: false,
							items:[
								{boxLabel: 'Captura',id:'captura', name: 'stat',  checked: true,
									listeners:{
										check:	function(radio){
														if (radio.checked){
															gridError.setVisible(false);
															fp.getForm().reset();
															fp.el.mask('Cargando...', 'x-mask-loading');
															catalogoEpo.load({
																params: {nueva:true}
															});
															//
															Ext.getCmp('radioGp2').reset();
															Ext.getCmp('radioGp2').setVisible(true);
															Ext.getCmp('archivo').setVisible(true);
														}

													}
									}
								},
								{boxLabel: 'Vista Previa',id:'vistaPrevia', name: 'stat',checked: false,
									listeners:{
										check:	function(radio){
															if(radio.checked){
															gridError.setVisible(false);
															fp.getForm().reset();
															fp.el.mask('Cargando...', 'x-mask-loading');
															catalogoEpo.load({
																params: {nueva:false}
															});
															Ext.getCmp('archivo').setVisible(false);
															Ext.getCmp('radioGp2').setVisible(false);
															}
													}
									}
								},
								{boxLabel: 'Dise�o Predeterminado',id:'predeterminado', name: 'stat',checked: false,
									listeners:{
										check:	function(radio){
														if(radio.checked){
														gridError.setVisible(false);
															fp.getForm().reset();
															fp.el.mask('Cargando...', 'x-mask-loading');
															catalogoEpo.load({
																params: {nueva:false}
															});
															//
															Ext.getCmp('archivo').setVisible(false);
															Ext.getCmp('radioGp2').setVisible(false);
															}
														}
									}
								}
							]
						}
]

var elementosForma = [
	{
			xtype:'radiogroup',
			id:	'radioGp2',
			columns: 2,
			hidden: false,
			items:[
				{boxLabel: 'Nueva EPO',id:'nueva', name: 'radio2',  checked: true,
					listeners:{
						check:	function(radio){
										if (radio.checked){
											Ext.getCmp('banco').reset();
											Ext.getCmp('ic_epo').reset();
											Ext.getCmp('archivo').reset();
											gridError.setVisible(false);
											fp.el.mask('Cargando...', 'x-mask-loading');
											catalogoEpo.load({
												params: {nueva:true}
											});
										}
									}
					}
				},
				{boxLabel: 'Modificar EPO',id:'modificar', name: 'radio2',checked: false,
					listeners:{
						check:	function(radio){
										if (radio.checked){
										gridError.setVisible(false);
										Ext.getCmp('banco').reset();
										Ext.getCmp('ic_epo').reset();
										Ext.getCmp('archivo').reset();
										fp.el.mask('Cargando...', 'x-mask-loading');
										catalogoEpo.load({
											params: {nueva:false}
										});

									}
								}
					}
				}
			]
		},{
			xtype:			'combo',
			id:				'banco',
			name:				'Hbanco',
			hiddenName:		'Hbanco',
			anchor: 			'-400',
			fieldLabel:		'Banco de Fondeo',
			value:			'',
			//editable:		false,
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			typeAhead:		true,
			hidden: true,
			triggerAction:	'all',
			minChars:		1,
			store:			catalogoBanco,
			tpl:				NE.util.templateMensajeCargaCombo,
			listeners:{
				'select':function(combo){
					var bandera=false;
					if(Ext.getCmp('captura').checked&&Ext.getCmp('nueva').checked)
					bandera=true;
					catalogoEpo.load({
													params: {nueva:bandera}
												});
				}
			}
		},{
			xtype:			'combo',
			id:				'ic_epo',
			name:				'Hic_epo',
			hiddenName:		'Hic_epo',
			anchor: 			'-100',
			fieldLabel:		'EPO',
			//editable:		false,
			emptyText:		'Seleccionar. . .',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			typeAhead:		true,
			triggerAction:	'all',
			minChars:		1,
			store:			catalogoEpo,
			tpl:				NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'panel',
			layout:'column',
			items:[{
					xtype: 'container',
					id:	'panelIzq',
					labelWidth:200,
					columnWidth:.95,
					defaults: {	msgTarget: 'side',	anchor: '-40'	},
					layout: 'form',
					items: [
						{
						  xtype: 		'fileuploadfield',
						  id: 			'archivo',
						  name: 			'archivo',
						  emptyText: 	'Seleccione...',
						  fieldLabel: 	"Archivo Zip",
						  buttonText: 	null,
						  buttonCfg: {
							  iconCls: 	'upload-icon'
						  },
						  anchor: 		'-10'
						  //vtype: 		'archivotxt'
						}
						]
					},
						{
							xtype: 'container',
							id:	'panelDer',
							columnWidth:.05,
							labelWidth:1,
							layout: 'form',
							defaults: {	msgTarget: 'side',	anchor: '-100'	},
							items: [{
							xtype:'displayfield',
							value:'<div class="x-tool x-tool-ayudaLayout x-btn-icon" style="float:left;width:15px;" onclick="Ext.getCmp(\'venanaAyuda\').show();">&#160</div>'
							}]
						}
					]
				}


]








//---------------------Principal-----------------------------------
var panelCambioPanel = new Ext.Panel({
	hidden: false,
	height: 'auto',
	width: 840,
	collapsible: false,
	titleCollapse: false,
	style: 'margin:0 auto;',
	bodyStyle: 'padding: 8px',
	labelWidth: 150,
	defaultType: 'textfield',
		id: 'forma2',
	defaults:
	{
		msgTarget: 'side',
		anchor: '-20'
	},
	items: elementosCambioPanel

});

var gridError = new Ext.grid.GridPanel({
		store: storeImagenesError,
		id: 'gridError',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Error en el tama�o de las Imagenes',
		columns: [
			{
				header: 'Imagen',
				tooltip: 'Imagen',
				dataIndex: 'IMAGEN',
				sortable: true,
				width: 180,
				resizable: true,
				align: 'left'
			},
			{
				header: 'Error',
				tooltip: 'Error',
				dataIndex: 'ERROR',
				sortable: true,
				width: 300,
				resizable: true,
				align: 'left'
			}
		],
     	stripeRows: true,
		loadMask: true,
		height: 250,
		width: 500,
		frame: true
	});

var panelAyuda= new Ext.FormPanel({
    hidden: false,
	 defaults:{xtype:'textfield'},
    //bodyStyle:'padding: 10px',
    html: '<table width="600" leftmargin="0" cellpadding="0" cellspacing="0" border="0">'+
'<tr>'+
'	<td valign="top" align="left"><br>'+
'		<table  leftmargin="0" cellpadding="0" cellspacing="0" border="0">'+
'		<tr>'+
'			<td align="center" class="formas">'+
'				<!--tabla de contenidos-->'+
'				<b>Archivos que deber&aacute; contener el ARCHIVO XXX.ZIP, en '+
'				donde XXX es el n&uacute;mero de la EPO seleccionada<br><br></b>'+
'				<table width="500" cellpadding="3" cellspacing="0" border="1" class="formas">'+
'			'+
'				<tr>'+
'					<td align="left" class="celda01" colspan="3">'+
'						<b>LOGOTIPOS</b>'+
'					</td>'+
'				</tr>'+
'				<tr>'+
'					<td class="formas" >&nbsp;</td>'+
'					<td class="formas" align="left" >'+
'						<b>XXXX.gif</b>'+
'					</td>'+
'					<td class="formas" align="left">'+
'						Logotipo que aparece en la pantalla de login del usuario y de no existir un XXXX_home.gif'+
'						tambi�n aparece como logotipo en la parte superior izquierda dentro de N@E.<br>'+
'						Dimesiones: 175 x 65 pixeles<br>'+
'						Formato: GIF<br>'+
'					</td>'+
'				</tr>'+
'				<tr>'+
'					<td class="celda01" colspan="3" align="left">'+
'						<b>BANNERS PARA EL NUEVO ESQUEMA PYME</b>'+
'					</td>'+
'				</tr>'+
'				'+
'				<tr>'+
'					<td class="formas" >&nbsp;</td>'+
'					<td class="formas" align="left">'+
'						<b>banners.jsp</b>'+
'					</td>'+
'					<td class="formas" align="left">'+
'						Banners del men� lateral<br>'+
'						Formato: jsp<br>'+
'					</td>'+
'				</tr>'+
'				<tr>'+
'					<td class="formas" >&nbsp;</td>'+
'					<td class="formas" align="left">'+
'						<b>banner1s.jpg</b>'+
'					</td>'+
'					<td class="formas" align="left">'+
'						Imagen del banner 1.<br>'+
'						Dimesiones: 198 x 137 pixeles<br>'+
'						Formato: JPG<br>'+
'					</td>'+
'				</tr>'+
'				<tr>'+
'					<td class="formas" >&nbsp;</td>'+
'					<td class="formas" align="left">'+
'						<b>banner2s.jpg</b>'+
'					</td>'+
'					<td class="formas" align="left">'+
'						Imagen del banner 2.<br>'+
'						Dimesiones: 198 x 137 pixeles<br>'+
'						Formato: JPG<br>'+
'					</td>'+
'				</tr>'+
'				<tr>'+
'					<td class="formas" >&nbsp;</td>'+
'					<td class="formas" align="left">'+
'						<b>banner3s.jpg</b>'+
'					</td>'+
'					<td class="formas" align="left">'+
'						Imagen del banner 3.<br>'+
'						Dimesiones: 198 x 137 pixeles<br>'+
'						Formato: JPG<br>'+
'					</td>'+
'				</tr>'+
'				<tr>'+
'					<td class="formas" >&nbsp;</td>'+
'					<td class="formas" align="left">'+
'						<b>banner4s.jpg</b>'+
'					</td>'+
'					<td class="formas" align="left">'+
'						Imagen del banner 4.<br>'+
'						Dimesiones: 198 x 137 pixeles<br>'+
'						Formato: JPG<br>'+
'					</td>'+
'				</tr>'+
'				</table>'+
'			</td>'+
'		</tr>'+
'		</table>'+
'	</td>'+
'</tr>'+
'</table>'

	 });
var ventanaAyuda = new Ext.Window({
			title: '',
			hidden: true,
			id: 'venanaAyuda',
			width: 650,
			height: 'auto',
			maximizable: false,
			closable: true,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			items: [panelAyuda]
});
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 840,
		collapsible: false,
		titleCollapse: false,
		fileUpload: 	true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 200,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults:
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
				xtype: 'button',
				text: 'Aceptar',
				name: 'btnBuscar',
				hidden: false,
				formBind: true,
				handler: function(boton, evento)
				{
					gridError.setVisible(false);
					if(Ext.getCmp('ic_epo').getValue()==''){
						Ext.getCmp('ic_epo').markInvalid('Seleccione una EPO');
						return;
					}

					if(Ext.getCmp('captura').checked){
					var archivo = Ext.getCmp("archivo");
						if( Ext.isEmpty( archivo.getValue() ) ){
							archivo.markInvalid("Debe especificar un archivo");
							return;
						}
					var myRegexZIP = /^.+\.([zZ][iI][pP])$/;
					if( !myRegexZIP.test(archivo.getValue()) ){
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n zip.");
							return;
						}

						Ext.getCmp("forma").getForm().submit({
						clientValidation: 	true,
						url: 						'15AdmNafParamImgExt.data.jsp?informacion=subirArchivo',
						waitMsg:   				'Subiendo archivo...',
						waitTitle: 				'Por favor espere',
						success: function(form, action) {
							 procesaCargaArchivo(null,  true,  action.response );
						},
						failure: function(form, action) {
							 procesaCargaArchivo(null,  true, action.response );
						}
					});


					}else if(Ext.getCmp('vistaPrevia').checked){
					fp.el.mask('Cargando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '15AdmNafParamImgExt.data.jsp',
							params: {

										informacion: 			"vistaPrevia",
										ic_epo:Ext.getCmp('ic_epo').getValue()

							},callback: procesarVistaPrevia
						});

					}else if(Ext.getCmp('predeterminado').checked){
					Ext.Msg.show({
									title:	"Mensaje",
									msg:		'�Desea asignar el dise�o predeterminadoa la EPO seleccionada?',
									buttons:	Ext.Msg.OKCANCEL,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											fp.el.mask('Cargando...', 'x-mask-loading');
											Ext.Ajax.request({
												url: '15AdmNafParamImgExt.data.jsp',
												params: {

															informacion: 			"disenoPredeterminado",
															ic_epo:Ext.getCmp('ic_epo').getValue()

												},callback: procesarPredeterminado
											});
										}
									},
									closable:false,
									icon: Ext.MessageBox.QUESTION
								});


					}
				}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						gridError.setVisible(false);
						 fp.getForm().reset();
				}
			 }

		]
	});
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
	  panelCambioPanel,NE.util.getEspaciador(10),fp,NE.util.getEspaciador(10),gridError
	  ]
  });
	fp.el.mask('Cargando...', 'x-mask-loading');

	catalogoEpo.load({
		params: {nueva:true}
	});
	Ext.Ajax.request({
				url: '15AdmNafParamImgExt.data.jsp',
				params: {

							informacion: 			"valoresIniciales"

				},callback: procesaValoresIniciales
			});

	catalogoBanco.load();
});