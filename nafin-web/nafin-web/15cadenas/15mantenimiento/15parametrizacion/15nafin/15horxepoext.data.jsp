<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.exception.*,
		com.netro.procesos.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.descuento.*,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoEPONafin,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

ManejoServicio servicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB",ManejoServicio.class);

if(informacion.equals("catalogoProductos") ) {
	List l = new ArrayList();
	l.add("4");
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COMCAT_PRODUCTO_NAFIN");
	cat.setCampoClave("ic_producto_nafin");
	cat.setCampoDescripcion("ic_nombre");
	cat.setOrden("ic_nombre");
	cat.setValoresCondicionIn(l);

	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoEPOs") ) {
	CatalogoEPONafin cat = new CatalogoEPONafin();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("2");
	
	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("Consultar")) {
	String ic_producto 	= request.getParameter("claveProducto")==null?"T":request.getParameter("claveProducto");
	String ic_epo		=	request.getParameter("claveEPO")==null? "": request.getParameter("claveEPO");
	if(ic_epo.equals("") || ic_epo.equals("s")){	ic_epo = "T";	}
	System.out.println("\nIc_Producto vale: "+ic_producto);
	System.out.println("\nIc_EPO vale: "+ic_epo);

	ArrayList list=new ArrayList();
	list=servicio.getHorariosxProductoEpo(ic_producto, ic_epo);
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size(); i++){
		Vector aux=(Vector)list.get(i);
		JSONObject jo=new JSONObject();
		jo.put("IDPROD",aux.get(0).toString());
		jo.put("NOMBREP",aux.get(1).toString());
		jo.put("IDEPO",aux.get(2).toString());
		jo.put("NOMBREE",aux.get(3).toString());
		jo.put("HORIP",aux.get(4).toString());
		jo.put("HORFP",aux.get(5).toString());
		jo.put("HORII",aux.get(6).toString());
		jo.put("HORFI",aux.get(7).toString());
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("Guardar")){
		String [] a = request.getParameterValues("arr");
		
		for(int cont=0; cont < a.length; cont++){
			String [] b = a[cont].split(",");
			String cveProducto = b[0];
			String cveEpo = b[1];
			String aperturaPy = b[2];
			String cierrePy = b[3];
			String aperturaIf = b[4];
			String cierreIf = b[5];
			
			servicio.setHorariosXEpo(cveProducto, aperturaPy, cierrePy, aperturaIf, cierreIf, cveEpo, "S");
		}
		
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			infoRegresar = jsonObj.toString();
		
}else if(informacion.equals("Reestablecer")){
		String cveProducto		=	(request.getParameter("cveprod") == null) ? "": request.getParameter("cveprod");
		String cveEpo		=	(request.getParameter("cvepo") == null) ? "": request.getParameter("cvepo");

			servicio.setHorariosXEpo(cveProducto, "", "", "", "", cveEpo, "N");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			infoRegresar = jsonObj.toString();
		
}

%>
<%=infoRegresar%>

