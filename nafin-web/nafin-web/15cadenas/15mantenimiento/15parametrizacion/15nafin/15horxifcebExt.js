Ext.onReady(function() {

	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);

/**********CRITERIOS DE BUSQUEDA ***************/		

	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: 'General',			
				id: 'btnGeneral',					
				handler: function() {
					window.location = '15horxifceExt.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<b>Por Intermediario</b>',			
				id: 'btnIntermediario',					
				handler: function() {
					window.location = '15horxifcebExt.jsp';
				}
			}	
		]
	});
	
//- - - - - - -  - - - - - HANDLER�S - - - - - - - - - - - - - - - - - - - - - -
	
	var procesarCatIntermediarioData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cveIntermediario = Ext.getCmp('_cmb_intermediario');
			if(cveIntermediario.getValue()==''){
				
				var newRecord = new recordType({
					clave:'T',
					descripcion:'Todos los intermediarios (Horarios Especiales)'
				});
				
				store.insert(0,newRecord);
				store.commitChanges();
				cveIntermediario.setValue('T');

			}
		}
  }
  
		var procesarConsultaRegistros = function(total, registros){
			var forma = Ext.getCmp('forma');
			forma.el.unmask();
			var btnGuardar = Ext.getCmp('btnGuardar');
			if (registros != null) {
				var grid  = Ext.getCmp('gridConsulta');
				grid.show();
				var el = grid.getGridEl();
				if(registrosConsultadosData.getTotalCount() > 0) {
					btnGuardar.enable();
				} else {
					btnGuardar.disable();
				}
			}
		}
		
		//Guarda los horarios
		var guardarHorarios = function(grid, rowIndex, colIndex, item, event) {
			var grid = Ext.getCmp('gridConsulta');
			var bandera = true;
			var arr = new Array();
			
			for(var i=0; i<registrosConsultadosData.getTotalCount() ; i++){
				registro = grid.getStore().getAt(i);
				if(registro.get('HORINIIF')!="" && registro.get('HORFINIF')!=""){
					var j = registro.get('NOIF');
					var k = registro.get('HORINIIF');
					var l = registro.get('HORFINIF');
					arr[i] = j+','+k+','+l;
				}
				else{
					Ext.MessageBox.alert('Aviso', 'Es obligatorio establecer todos los horarios');
					bandera = false;
					accionConsulta("CONSULTAR", null)
					break;
				}
			}
			
			if(bandera){
			Ext.Ajax.request({
									url: '15horxifcebExt.data.jsp',
									params: {
										informacion: 'Guardar',
										arr : arr
									},
									callback: procesarGuardarHorarios
						});
			}
			
		}
		
			//Guardar Horarios
		var procesarGuardarHorarios =  function(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var resp = 	Ext.util.JSON.decode(response.responseText);
				var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
				Ext.MessageBox.alert('Aviso', 'Se guardo con exito los horarios para el o los IF seleccionados.', function(){
					//contenedorPrincipal.el.mask('Consultando...', 'x-mask-loading');
					accionConsulta("CONSULTAR",null);
				});
				
			} else {		
				NE.util.mostrarConnError(response,opts);
			}		
		}
		
		//Restablece a horario general el registro seleccionado
		var restablecerHorario = function(grid, rowIndex, colIndex, item, event) {
			var registro = grid.getStore().getAt(rowIndex);
			var numIf = registro.get('NOIF');
			
			Ext.Ajax.request({
									url: '15horxifcebExt.data.jsp',
									params: {
										informacion: 'Establecer',
										numIf: numIf
									},
									callback: procesarEstablecerHorarios
						});
			
		}
		
		//Establecer Horarios
	var procesarEstablecerHorarios =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			Ext.MessageBox.alert('Aviso', 'Los horarios se han actualizado con �xito.', function(){
				//contenedorPrincipal.el.mask('Consultando...', 'x-mask-loading');
				accionConsulta("CONSULTAR",null);
			});
			
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}

	
//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - -

	var catalogoIntermediariosData = new Ext.data.JsonStore({
		id:				'catalogoIntermediariosDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15horxifcebExt.data.jsp',
		baseParams:		{	informacion: 'catalogoIntermediarios'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load: procesarCatIntermediarioData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'15horxifcebExt.data.jsp',
			baseParams: {	informacion:	'ConsultaHorarios'	},
			fields: [
				{ name: 'NOIF'},
				{ name: 'NOMBREIF' },
				{ name: 'HORINIIF' },
				{ name: 'HORFINIF' }
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null);
					}
				}
			}
	});
	
	//------------------------FUNCIONES PARA VALIDAR HORAS----------------

	var formatoHora = function (hora) {
		var result = false, m;
		var re = /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/;
		if ((m = hora.match(re))) {
			result = (m[1].length == 2 ? "" : "0") + m[1] + ":" + m[2];
		}
		return result;
	};
	
	var comparaHorasDeServicio = function(grid, record, fieldName, indexC, indexR){
		var result = true;
		var hrIni = record.data['HORINIIF'];
		var hrFin = record.data['HORFINIF'];
		
		if(  (Date.parse('01/01/2013 '+hrIni+':00') > Date.parse('01/01/2013 '+hrFin+':00'))	){
			result = false;
			Ext.MessageBox.alert('Aviso', 'El horario de cierre debe ser mayor o igual al de apertura', function(){
				grid.startEditing(indexR, indexC);
				accionConsulta("CONSULTAR",null);
			});	
		}
		
		return result;
	};
	
//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - -

	var procesaConsulta = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){
		 if(  estadoSiguiente == "CONSULTAR" ){
			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');
			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			//Ext.getCmp("gridConsulta").hide();
			Ext.StoreMgr.key('registrosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Consultar'
							}
				)
			});
		}
	}
	
//----------------------------------- COMPONENTES -------------------------------------

		var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 1 , align: 'center'},
					{header: 'Evento', colspan: 2, align: 'center'},
					{header: '', colspan: 1 , align: 'center'}
				]
			]
		});

//Elementos del grid de horarios
		var grid = new Ext.grid.EditorGridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			title:		'Horarios Por Intermediario',
			style: 		'margin: 0 auto'/*; padding-top:10px;'*/,
			hidden: 		false,
			margins:		'20 0 0 0',
			clicksToEdit:1,
			stripeRows: true,
			loadMask: 	true,
			height: 	250,
			width: 		597,
			plugins: grupos,
			listeners: {
			cellclick :function(grid, rowIndex, colIndex, evento){

			},
			beforeedit : function(e){

			},
			afteredit : function(e){
					var record = e.record;
					var gridf = e.grid; 
					var store = gridf.getStore();
					var fieldName = gridf.getColumnModel().getDataIndex(e.column); // Se obtine nombre del Campo (Fields del Store)
					
					var horaGral = record.data[fieldName];
					
					var valor = formatoHora(horaGral);
					if(valor){
						record.data[fieldName]=valor;
						if ( !comparaHorasDeServicio(gridf, record, fieldName, e.column, e.row) ){
							record.data[fieldName]='';
							store.commitChanges();
							//gridf.startEditing(e.row, e.column);
						}else{
							store.commitChanges();
						}
					}else{
						record.data[fieldName]='';
						store.commitChanges();
					}
					
			}
			
		},
			columns: [
				{
					header: 		'Intermediario',
					tooltip: 	'Nombre del intermediario financiero',
					dataIndex: 	'NOMBREIF',
					align:		'center',
					resizable: 	true,
					width: 		260,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Apertura',
					tooltip: 	'Horario de Apertura',
					dataIndex: 	'HORINIIF',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
					},
					resizable: 	true,
					width: 		100,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Cierre',
					tooltip: 	'Horario de Cierre',
					dataIndex: 	'HORFINIF',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maxLength:5		 
					},
					resizable: 	true,
					width: 		100,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
				xtype: 'actioncolumn',
				header: 'Usar Horarios Generales',
				tooltip: 'Establecer horarios Generales',
				width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ok';
							return 'icoValidar';										
						},
						handler: restablecerHorario
					}
				]				
			}
			],bbar: {
			buttonAlign: 	'right',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Guardar',
					width:	100,
					id: 		'btnGuardar',
					iconCls:	'icoGuardar',
					handler: guardarHorarios
				},'-'
			]
		}
	});
	
	var elementoFormaConsulta = [
			{
				xtype: 'combo',
				name: 'cmb_intermediario',
				id: '_cmb_intermediario',
				fieldLabel: 'Intermediario',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'cveIf',
				emptyText: 'Seleccione Intermediario',
				forceSelection : true,
				triggerAction : 'all',
				editable: false,
				typeAhead: true,
				minChars : 1,
				store: 		catalogoIntermediariosData,
				anchor:	'95%',
				listeners:{ select: function (combo)
				{
					accionConsulta("CONSULTAR", null)
				}}
			}
		];
		
		var fp = new Ext.form.FormPanel({
			id:				'forma',
			width:			500,
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			labelWidth:		150,
			items:			elementoFormaConsulta,
			monitorValid:	true/*,
			buttons: [
			{
				text:		'Consultar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:false,
				handler: function(boton, evento) {
									accionConsulta("CONSULTAR",null)
								}
				}, //fin handler
				{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR",null);
							}
			}
		]*/
		});
	
	
//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	930,
		//height: 	'auto',
		align: 'center',
		items: 	[
			NE.util.getEspaciador(10),
			fpBotones,
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	});
	
	Ext.StoreMgr.key('catalogoIntermediariosDataStore').load();
	accionConsulta("CONSULTAR", null)
	
});