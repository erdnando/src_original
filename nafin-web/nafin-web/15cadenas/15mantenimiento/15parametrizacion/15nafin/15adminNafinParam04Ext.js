Ext.onReady(function(){
var banderaExtjs= Ext.get('esEsquemaExtJS').getValue();
if (banderaExtjs=='true'||true){
banderaExtjs=true;
}else{
banderaExtjs=false;
}
//-------------Handlers-------------
	var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	var procesaConsulta = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var procesaCambioCuenta = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

		fp.setVisible(false);
		grid.setVisible(false);
		Ext.getCmp('leyenda').hide();			
		gridAcuse.setVisible(true);
		Ext.Msg.alert("Mensaje de p�gina web.",'Los siguientes intermediarios fueron parametrizados correctamente' );
			
		}else{
				// Mostrar mensaje de error
				Ext.Msg.alert("Mensaje de p�gina web.",'Error al almacenar la parametrizacion de cuentas bancarias' );
		}
	}
var procesarConsultaData = function (store, arrRegistros, opts) {
		var boton=Ext.getCmp('btnParam');

		if(arrRegistros!= null) {
			if(!grid.isVisible()) {
				grid.setVisible(true);
				Ext.getCmp('leyenda').show();				
			}

			
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
				boton.enable();
			}else {
				el.mask('No se encontr� ning�n registro','x-mask');
				boton.disable();
			}
		}
	}
var accionConsulta = function(estadoSiguiente, respuesta){
if(  estadoSiguiente == "OBTENER_NOMBRE_PYME" 										){
		
			fp.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15adminNafinParam04Ext.data.jsp',	
				params: {
							numeroDeProveedor:	respuesta.noNafinElec,
							ic_banco_fondeo:		respuesta.ic_banco_fondeo,
							ic_epo:					respuesta.ic_epo,
							informacion: 			"obtenNombrePyme" 
				},
				callback: procesaConsulta
			});

		}else if(  estadoSiguiente == "RESPUESTA_OBTENER_NOMBRE_PYME"){
		
			Ext.getCmp('hid_ic_pyme').setValue(respuesta.ic_pyme);
			Ext.getCmp('_txt_nombre').setValue(respuesta.txtCadenasPymes);
			grid.setTitle('PROVEEDOR: '+respuesta.txtCadenasPymes+'<center> N@E: '+Ext.getCmp('_txt_nafelec').getValue()+'</center>');
			gridAcuse.setTitle('PROVEEDOR: '+respuesta.txtCadenasPymes+'<center> N@E: '+Ext.getCmp('_txt_nafelec').getValue()+'</center>');
			fp.el.unmask();
			if (respuesta.muestraMensaje != undefined && respuesta.muestraMensaje){
				Ext.Msg.alert("Mensaje de p�gina web.",	respuesta.textoMensaje);
				Ext.getCmp('_txt_nafelec').setValue('');
			}else{
				catalogoCuenta.load({
					params: {ic_pyme:respuesta.ic_pyme}
				});
			}

		}

}



var ConfirmaGen=function(selc){
	
	 var selections = selc.getSelections();
		//Realiza la agrupacion de info											
		var i = 0;

	var selections = selc.getSelections();
			  if (selections.length<=0 ) {			            
						Ext.MessageBox.alert("Mensaje", "Es necesario seleccionar al menos un IF para hacer el cambio de cuenta");	
						                 
              }else if(selections.length>0)
			  {
					
					Ext.Msg.show({
									title:	"Mensaje",
									msg:		'�Desea realizar el cambio de cuenta?',
									buttons:	Ext.Msg.OKCANCEL,
									fn: function resultMsj(btn){
										 if (btn == 'ok') {
											CambiarCuentas(selc);
										}
									},
									closable:false,
									icon: Ext.MessageBox.QUESTION
								});
					
				}
};
var CambiarCuentas=function(selc){
		
		var selections = selc.getSelections();
		//Realiza la agrupacion de info											
		var i = 0;
		var recordVOBO = "";
		var recordIC_IF="";
			consultaProcesados.removeAll();		
			var regi = Ext.data.Record.create(['INTERMEDIARIO', 'MONEDA','CUENTA']);

			for(;i<selections.length;i++){											 
				 recordVOBO += selections[i].json.CS_VOBO_IF+",";						 
				 recordIC_IF += selections[i].json.IC_IF+",";	
				 
				 
			   consultaProcesados.add(	new regi({INTERMEDIARIO: '',CUENTA: ''}) );
				var reg = consultaProcesados.getAt(i);
				
				reg.set('INTERMEDIARIO',selections[i].json.INTERMEDIARIO);
				reg.set('CUENTA',Ext.getCmp('Hcuenta').lastSelectionText);
			
				 
			}
			//alert(recordIC_IF);
			//console.log(recordFolio);
			//console.log(recordMonto);
			Ext.Ajax.request({
				url: '15adminNafinParam04Ext.data.jsp',	
				params: {
							clavesIf:	recordIC_IF,
							informacion: 			"confirmarCuentas",
							ic_pyme:Ext.getCmp('hid_ic_pyme').getValue(),
							ic_cuenta:Ext.getCmp('Hcuenta').getValue()
				},callback: procesaCambioCuenta
			});		
	};
//-------------Stores------------
	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15adminNafinParam04Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		var catalogoCuenta = new Ext.data.JsonStore({
		id:				'catalogoCuenta',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15adminNafinParam04Ext.data.jsp',
		baseParams:		{	informacion: 'catalogoCuenta'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaProcesados = new Ext.data.JsonStore({

		fields: [
					{name: 'CUENTA'},
					{name: 'MONEDA'},
					{name: 'INTERMEDIARIO'}
		]
	});

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15adminNafinParam04Ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
					{name: 'IC_CUENTA_BANCARIA'},
					{name: 'CS_VOBO_IF'},
					{name: 'INTERMEDIARIO'},
					{name: 'IC_IF'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});
//-------------Elementos---------------
var elementosForma=[

				{
					xtype:'hidden',
					id:	'hid_nombre',
					value:''
				},{
					xtype:'hidden',
					id:	'hid_ic_pyme',
					value:''
				},{
					xtype:		'textfield',
					name:			'txt_nafelec',
					id:			'_txt_nafelec',
					maskRe:		/[0-9]/,
					allowBlank:	true,
					fieldLabel: 'N@E',
					maxLength:	25,
					listeners:	{
						'change':function(field){
										if ( !Ext.isEmpty(field.getValue()) ) {

											var respuesta = new Object();
											respuesta["noNafinElec"]		= field.getValue();
											//respuesta["ic_banco_fondeo"]	= Ext.getCmp('_ic_banco_fondeo').getValue();
											//respuesta["ic_epo"]				= Ext.getCmp('ic_epo').getValue();
											
											accionConsulta("OBTENER_NOMBRE_PYME",respuesta);

										}
									}
					}
				},{
					xtype:			'textfield',
					name:				'txt_nombre',
					fieldLabel: 	'Nombre de proveedor',
					id:				'_txt_nombre',
					width:			270,
					disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
					disabled:		true,
					allowBlank:		true,
					maxLength:		100
				},{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:'displayfield',
							id:'disEspacio',
							text:''
						},
						{
							xtype:	'button',
							id:		'btnBuscaA',
							iconCls:	'icoBuscar',
							text:		'B�squeda Avanzada',
							handler: function(boton, evento) {
										var winVen = Ext.getCmp('winBuscaA');
											if (winVen){
												Ext.getCmp('fpWinBusca').getForm().reset();
												Ext.getCmp('fpWinBuscaB').getForm().reset();
												
												Ext.getCmp('cmb_num_ne').setValue();
												Ext.getCmp('cmb_num_ne').store.removeAll();
												Ext.getCmp('cmb_num_ne').reset();
												winVen.show();
											}else{
												var winBuscaA = new Ext.Window ({
													id:'winBuscaA',
													height: 320,
													x: 300,
													y: 100,
													width: 550,
													heigth: 100,
													modal: true,
													closeAction: 'hide',
													title: 'B�squeda Avanzada',
													items:[
														{
															xtype:'form',
															id:'fpWinBusca',
															frame: true,
															border: false,
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 130,
															items:[
																{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:'Utilice el * para b�squeda gen�rica'
																},{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:''
																},{
																	xtype: 'textfield',
																	name: 'nombre_pyme',
																	id:	'txtNombre',
																	fieldLabel:'Nombre',
																	maxLength:	100
																},{
																	xtype: 'textfield',
																	name: 'rfc_pyme',
																	id:	'txtRfc',
																	fieldLabel:'RFC',
																	maxLength:	20
																},{
																	xtype: 'textfield',
																	name: 'num_pyme',
																	id:	'txtNe',
																	fieldLabel:'N�mero Proveedor',
																	maxLength:	25
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Buscar',
																	iconCls:'icoBuscar',
																	handler: function(boton) {
																					catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {
																					Ext.getCmp('winBuscaA').hide();
																				}
																}
															]
														},{
															xtype:'form',
															frame: true,
															id:'fpWinBuscaB',
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															monitorValid: true,
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 80,
															items:[
																{
																	xtype: 'combo',
																	id:	'cmb_num_ne',
																	name: 'ic_pyme',
																	hiddenName : 'ic_pyme',
																	fieldLabel: 'Nombre',
																	emptyText: 'Seleccione Proveedor. . .',
																	displayField: 'descripcion',
																	valueField: 'clave',
																	triggerAction : 'all',
																	forceSelection:true,
																	allowBlank: false,
																	typeAhead: true,
																	mode: 'local',
																	minChars : 1,
																	store: catalogoNombreProvData,
																	tpl : NE.util.templateMensajeCargaCombo
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Aceptar',
																	iconCls:'aceptar',
																	formBind:true,
																	//hidden:true,
																	handler: function() {
																					if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																						var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																						var cveP = disp.substr(0,disp.indexOf(" "));
																						var desc = disp.slice(disp.indexOf(" ")+1);
																						Ext.getCmp('hid_ic_pyme').setValue(Ext.getCmp('cmb_num_ne').getValue())
																						Ext.getCmp('_txt_nafelec').setValue(cveP);
																						Ext.getCmp('_txt_nombre').setValue(desc);
																						Ext.getCmp('winBuscaA').hide();
																					}
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {	Ext.getCmp('winBuscaA').hide();	}
																}
															]
														}
													]
												}).show();
											}
										}
						}
					]
				},{
			xtype:			'combo',
			id:				'Hcuenta',
			name:				'cuenta',
			hiddenName:		'cuenta',
			fieldLabel:		'Seleccione la Cuenta Bancaria',
			emptyText:		'Seleccionar. . .',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:false,
			typeAhead:		true,
			triggerAction:	'all',
			minChars:		1,
			store:			catalogoCuenta,
			tpl:				NE.util.templateMensajeCargaCombo
		}
	
]

var elementosCambioPanel=[
	{
							xtype:'radiogroup',
							id:	'radioGp',
							columns: 4,
							hidden: false,
							items:[
								{boxLabel: 'Individual',id:'individual', name: 'stat',  checked: false,
									listeners:{
										check:	function(radio){
														if (radio.checked){
																if(banderaExtjs)
																	window.location.href='15adminNafinParam02Ext.jsp';
																else
																	window.location.href='15adminNafinParam02Ext.jsp';

														}
													}
									}
								},
								{boxLabel: 'Masiva',id:'masiva', name: 'stat',checked: false,
									listeners:{
										check:	function(radio){
															if(banderaExtjs)
																window.location.href='15adminNafinParam02ExtMas.jsp';
															else
																window.location.href='15adminNafinParam02ExtMas.jsp';
													}
									}
								},
								{boxLabel: 'Consultar Cambios Cta',id:'consulta', name: 'stat',checked: false,
									listeners:{
										check:	function(radio){
														if(banderaExtjs)
															window.location.href='15adminNafinParam02ExtConsulta.jsp';
														else
															window.location.href='15admnafinparam02consulcambioscuenta.jsp';
													}
									}
								},
								{boxLabel: 'Cambio de Cuenta Masiva',id:'cambioM', name: 'stat', checked: true,
									listeners:{
										check:	function(radio){
													
													}
									}
								}
							]
						}
]

	var sm = new Ext.grid.CheckboxSelectionModel({
		 checkOnly:true,hideable:true,id:'chkIR',width:35});
				
var grid = new Ext.grid.GridPanel({
		store: consultaData,
		viewConfig:{markDirty:false},
		width: 840,
		height: 350,
		sm:sm,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		stripeRows: true,
		loadMask: true,

		title: ' ',
		frame: true,
		hidden: true,
		columns: [
			{
				header : 'Nombre IF', tooltip: 'Nombre IF',	dataIndex : 'INTERMEDIARIO',
				sortable : true, width : 300, align: 'center',  hidden: false,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
				
				
			},{
				header : 'Seleccionar Cuenta Bancaria', tooltip: 'Seleccionar Cuenta Bancaria',
				sortable : true, width : 250, align: 'center',  hidden: false,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
								
								return '';
				}
				
				
			},sm,
			{
				xtype: 'actioncolumn',
				header : 'Habilitada', tooltip: 'Habilitada',	dataIndex : 'CS_VOBO_IF',
				sortable : true, width : 200, align: 'center',items:[
						{	
							getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
								if (registro.get('CS_VOBO_IF')	!=	'S') {
									//this.items[0].tooltip = 'Ver';
									return 'icoRechazar';
								}else{return 'validarCuenta';}
							
							}
						}
					]
				}
			],
	bbar: {
			buttonAlign: 'right',
			id: 'barra',
			displayInfo: true,
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Cambio de Cuenta',
					id: 'btnParam',
					handler: function(boton, evento) {
						//boton.disable();
						//boton.setIconClass('loading-indicator');
						ConfirmaGen(sm);
					}
				}
			]
		}
		
	}
);

var gridAcuse = new Ext.grid.GridPanel({
		store: consultaProcesados,
		viewConfig:{markDirty:false},
		width: 840,
		height: 350,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		stripeRows: true,
		loadMask: true,

		title: ' ',
		frame: true,
		hidden: true,
		columns: [
			{
				header : 'Nombre IF', tooltip: 'Nombre IF',	dataIndex : 'INTERMEDIARIO',
				sortable : true, width : 400, align: 'center',  hidden: false
			},{
				header : 'Moneda', tooltip: 'Moneda',dataIndex : 'MONEDA',
				sortable : true, width : 250, align: 'center',  hidden: true
			},{
				header : 'Cuenta Bancaria', tooltip: 'Cuenta Bancaria',	dataIndex : 'CUENTA',
				sortable : true, width : 400, align: 'center',  hidden: false
			}
			],
	bbar: {
	
			buttonAlign: 'right',
			displayInfo: true,
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Salir',
					id: 'btnSalir',
					iconCls: 'icoLimpiar',
					handler: function() {
						 location.reload();
						
					}
				}
			]
		}
		
	}
);
//-------------Principal-------------------

var panelCambioPanel = new Ext.Panel({
	hidden: false,
		height: 'auto',
		width: 840,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
			id: 'forma2',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosCambioPanel
	
});

var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 840,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Parametrizar',
			  name: 'btnBuscar',
			  
			  iconCls: 'icoContinuar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						if(Ext.getCmp('_txt_nafelec').getValue()==''){
							Ext.getCmp('_txt_nafelec').markInvalid('Seleccione un Proveedor');
							return;
						}
						if(Ext.getCmp('Hcuenta').getValue()==''){
							Ext.getCmp('Hcuenta').markInvalid('Seleccione una Cuenta Bancaria');
							return;
							}
						
						
						
						consultaData.load({params: {
							ic_cuenta: Ext.getCmp('Hcuenta').getValue(),
							ic_pyme: Ext.getCmp('hid_ic_pyme').getValue()
						}});

	

					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  //style: 'margin:0 auto;',
	  width: 940,
	  items:
	   [
	  panelCambioPanel,NE.util.getEspaciador(10),fp,NE.util.getEspaciador(20),grid,
	  
	  
	  {
				xtype: 	'label',
				id:	 	'leyenda',
				hidden: 	true,
				cls:		'x-form-item',
				style: 	'font size:1; text-align:center;margin:0 auto;',
				html:  	'<b>Nota:</b> S�lo se mostrar�n IF�s que est�n parametrizados con el proveedor'
			},
			gridAcuse
	  ]
  });

});