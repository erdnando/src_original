<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
                com.netro.cadenas.CargaArchivoCorreosIF,               
		java.sql.*,
		java.text.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../../../../15cadenas/015secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String archivo = (request.getParameter("archivo")!=null)?request.getParameter("archivo"):"";
JSONObject jsonObj = new JSONObject();

CargaArchivoCorreosIF  carga = new CargaArchivoCorreosIF();

if ("validarArchivo".equals(informacion)   ) {

    String rutaArchivo = strDirectorioTemp + archivo; 	
  
    Map<String, String> mResultado =  carga.leerArchivo(strDirectorioTemp, archivo);
    String resultado = mResultado.get("resultado").toString();
    String respuesta = mResultado.get("respuesta").toString();
     
    jsonObj = new JSONObject();
    jsonObj.put("respuesta", respuesta);
    jsonObj.put("resultado", resultado);
    jsonObj.put("success", new Boolean(true));
    infoRegresar = jsonObj.toString();

}else  if ("guardaArchivo".equals(informacion)   ) {

    String fecha = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
    String hora = new java.text.SimpleDateFormat("HH:mm").format(new java.util.Date());
    String usuario = strLogin+" - "+strNombreUsuario;
    
    try{   
    
        int totalLineas  =  carga.guardaArchivo(strDirectorioTemp, archivo, strLogin  );
             
        jsonObj = new JSONObject();
        jsonObj.put("nombreArch", archivo);
        jsonObj.put("totalLineas", totalLineas);
        jsonObj.put("usuario", usuario);
        jsonObj.put("fecha", fecha);
        jsonObj.put("hora", hora);        
        jsonObj.put("success", new Boolean(true));
    }catch(Exception e){
        jsonObj.put("success", new Boolean(false));
    }
    
    infoRegresar = jsonObj.toString();   


}else  if ("validaExisteArchivo".equals(informacion)   ) {

    String existe = carga.getExisteCarga();
    jsonObj = new JSONObject();
    jsonObj.put("existe", existe);        
    jsonObj.put("success", new Boolean(true));
        
    infoRegresar = jsonObj.toString();   
 
}else  if ("descargaUltimoCarga".equals(informacion)   ) {

    try {
        String nombreArchivo = carga.getUltimaCarga(strDirectorioTemp);				
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
    } catch(Throwable e) {
        throw new AppException("Error al generar el archivo CSV", e);
    }
    infoRegresar = jsonObj.toString(); 

}
  

%>
<%=infoRegresar%>