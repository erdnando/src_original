<%@ page language="java" %>
<%@ page import="
   java.util.*,
   java.text.*, 
   java.math.*, 
   java.sql.*,
   javax.naming.*,
   netropology.utilerias.*,
   net.sf.json.JSONArray,net.sf.json.JSONObject,
   com.netro.afiliacion.*,
   com.netro.model.catalogos.*,
   com.netro.seguridadbean.SeguException"
   contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../../../015secsession_extjs.jspf" %>
<%
 
   String informacion   = request.getParameter("informacion")==null?"":request.getParameter("informacion");
   String noBancoFondeo   = request.getParameter("noBancoFondeo")==null?"":request.getParameter("noBancoFondeo");
   String noEpo   = request.getParameter("noEpo")==null?"":request.getParameter("noEpo");
	String noIf   = request.getParameter("noIf")==null?"":request.getParameter("noIf");
	String numRegistros   = request.getParameter("numRegistros")==null?"0":request.getParameter("numRegistros");
	
	
   String[] claveMoneda = request.getParameterValues("vMoneda");
   String[] seleccion =  request.getParameterValues("seleccion");

   
	Afiliacion afiliacionBean = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class); 

   String infoRegresar  ="" ,  cvePerf = "";
	JSONObject   jsonObj = new JSONObject(); 
	
   AccesoDB con = null;   

	com.netro.seguridadbean.Seguridad _beanS = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
   
   try{
      con = new AccesoDB(); 
      cvePerf = _beanS.getTipoAfiliadoXPerfil(strPerfil + "");
      try{
         _beanS.validaFacultad( (String) strLogin, (String) strPerfil,(String) session.getAttribute("sesCvePerfilProt"), "15ADMNAFPARAMDOCTOA", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
      }catch(Exception seg){}
   }catch(Exception e){
      e.printStackTrace();
      throw new RuntimeException(e);
   }finally{
      if(con.hayConexionAbierta()){ 
         con.cierraConexionDB();	  
      } 
   } 
 if( informacion.equals("cmbEpo") ){
	
      CatalogoEPOS cat = new CatalogoEPOS();
      cat.setCampoClave("ic_epo");
      cat.setCampoDescripcion("cg_razon_social");  
      cat.setOrden("2");
      infoRegresar = cat.getJSONElementos();
   
	} else if( informacion.equals("cmbIF") ){
		
		List catIF = afiliacionBean.actOtrosParametros(noEpo);
		jsonObj.put("registros", catIF);
		jsonObj.put("success",  new Boolean(true));
      infoRegresar = jsonObj.toString();
	
	} else if( informacion.equals("Consultar") ){
		
		List  ls =  afiliacionBean.consultRelEPO_IF(noEpo, noIf ,  noBancoFondeo, cvePerf  );		
      jsonObj = new JSONObject();
      jsonObj.put("success",  new Boolean(true));
      jsonObj.put("registros",  ls);
		jsonObj.put("cvePerf",  cvePerf);
      infoRegresar = jsonObj.toString();
	
	} else if( informacion.equals("ConsultarMoneda") ){
		
		List  ls =  afiliacionBean.consultaMoneda(noEpo, noIf , cvePerf  );
		
      jsonObj = new JSONObject();
      jsonObj.put("success",  new Boolean(true));
      jsonObj.put("registros",  ls);
		jsonObj.put("cvePerf",  cvePerf);
      infoRegresar = jsonObj.toString();
		
		System.out.println("infoRegresar "+infoRegresar);
		
	} else if( informacion.equals("insertaMoneda") ){
	
		int numRegistros2 = Integer.parseInt(numRegistros);
		List respuesta  =    afiliacionBean.insertaMoneda( noEpo, noIf , claveMoneda ,  seleccion ,  numRegistros2  );
		String strMensaje = respuesta.get(0).toString();
		String estatusMensaje = respuesta.get(1).toString();
		jsonObj = new JSONObject();
		jsonObj.put(	"success"		   ,new Boolean(true)	);
		jsonObj.put(	"MENSAJE"		   ,strMensaje	         );
		jsonObj.put(	"ESTATUSMENSAJE"	,estatusMensaje	   );
		infoRegresar = jsonObj.toString();
	
	
	} else if( informacion.equals("DescargaArchivo") ){
		
		String nombreArchivo = "/nafin/15cadenas/15mantenimiento/15parametrizacion/15nafin/../../../../00archivos/if/conv/"+noIf+"_"+noEpo+".pdf";
		
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", nombreArchivo);
		infoRegresar = jsonObj.toString();			
	
	} else if( informacion.equals("GuardarRelacionEPO_IF") ){
		
		String chkEPO[] = request.getParameterValues("chkEPO");
		String porcent[] = request.getParameterValues("porcentaje");
		String porcent_dl[] = request.getParameterValues("porcentaje_dl");
		String benef[] = request.getParameterValues("csBeneficiario_aux");
		String sIfEpo[] = request.getParameterValues("if_epo");
		String csBloqueo[] = request.getParameterValues("csBloqueo");
		String csBloqueo_aux[] = request.getParameterValues("csBloqueo_aux");
		String convUnico[] = request.getParameterValues("csConvUnico_aux");
		String FactoNormal[] = request.getParameterValues("csFactoNormal_aux"); //Fodea 057-2009 Mejoras III
		String FactoVencido[] = request.getParameterValues("csFactoVencido_aux"); 
		String cuentaActiva[] = request.getParameterValues("csCuentaActiva_aux"); //F040-2010 FVR
		String csReafiAutomatico[] = request.getParameterValues("csReafiAutomatico"); //F040-2010 FVR
		String csfactoDistribuido[] = request.getParameterValues("csfactoDistribuido"); //2017-03 DLHC		
		String csReferencia[] = request.getParameterValues("csReferencia"); //2017-03 DLHC		
		
		
		String  mensaje =   afiliacionBean.guardaRelEPO_IF( chkEPO, porcent, porcent_dl, benef , sIfEpo, csBloqueo, convUnico, FactoNormal, cuentaActiva, strNombreUsuario, csReafiAutomatico,FactoVencido, csfactoDistribuido, csReferencia);
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("mensaje", mensaje);
		infoRegresar = jsonObj.toString();	
		
	}

	
	


%>

<%=infoRegresar%>
