<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		com.netro.parametrosgrales.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 

	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	HashMap datos = new HashMap(); 
	JSONArray registros = new JSONArray();  
	JSONObject 	jsonObj	= new JSONObject();  

	if (informacion.equals("CatalogoIF")){
		String habilitado = "S";
		String tipoOperacion = "credito"; 	 
		
		String noIf = (request.getParameter("noIf")==null)?"":request.getParameter("noIf");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");
	
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
	
		CatalogoCreditoCaptura cat = new CatalogoCreditoCaptura();
			
	  cat.setCampoClave("t.ic_if"); 
	  cat.setCampoDescripcion("t.cg_razon_social"); 
	
		cat.setHabilitado(habilitado);
		cat.setPiso(tipoCartera);
		
		infoRegresar=cat.getJSONElementos();
	
	}else if (informacion.equals("CatalogoTasa")){
		
		String habilitado = "S";
		String tipoOperacion = "credito"; 	 
		
		String noIf = (request.getParameter("noIf")==null)?"":request.getParameter("noIf");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");
	
		
		String tipoCred = (request.getParameter("tipoCred")==null)?"":request.getParameter("tipoCred");
		String tipoAmort = (request.getParameter("tipoAmort")==null)?"":request.getParameter("tipoAmort");
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
		String tipo_plazo = (request.getParameter("tipo_plazo")==null)?"":request.getParameter("tipo_plazo");
	
		CatalogoCreditoCaptura cat = new CatalogoCreditoCaptura();
			
		cat.setCampoClave("t.ic_tasa"); 
		cat.setCampoDescripcion("t.ic_tasa||' - '||t.cd_nombre"); 
	
		cat.setOrden("1");
		cat.setHabilitado(habilitado);
			
		infoRegresar=cat.getJSONElementos();
	
	}else if (informacion.equals("CatalogoCredito")){
		String habilitado = "S";
		String tipoOperacion = "credito"; 	 
		
		String noIf = (request.getParameter("noIf")==null)?"":request.getParameter("noIf");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");
	
		
		String tipoCred = (request.getParameter("tipoCred")==null)?"":request.getParameter("tipoCred");
		String tipoAmort = (request.getParameter("tipoAmort")==null)?"":request.getParameter("tipoAmort");
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
		String tipo_plazo = (request.getParameter("tipo_plazo")==null)?"":request.getParameter("tipo_plazo");		
			
		CatalogoCreditoCaptura cat = new CatalogoCreditoCaptura();
			
		cat.setCampoClave("t.ic_tipo_credito");
		cat.setCampoDescripcion("t.ic_tipo_credito||' - ' ||t.cd_descripcion");
	
		cat.setHabilitado(habilitado);
	
		cat.setValoresCondicionNotIn("1,2,3,5,9,11", java.lang.Integer.class);
			
		infoRegresar=cat.getJSONElementos();	
	}else if (informacion.equals("CatalogoAmortizacion")){
		
		String tipoOperacion = "credito"; 	 	
		String noIf = (request.getParameter("noIf")==null)?"":request.getParameter("noIf");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");	
		String tipoCred = (request.getParameter("tipoCred")==null)?"":request.getParameter("tipoCred");
		String tipoAmort = (request.getParameter("tipoAmort")==null)?"":request.getParameter("tipoAmort");
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
		String tipo_plazo = (request.getParameter("tipo_plazo")==null)?"":request.getParameter("tipo_plazo");		
	
		CatalogoCreditoCaptura cat = new CatalogoCreditoCaptura();
			
		cat.setCampoClave("t.ic_tabla_amort");
		cat.setCampoDescripcion("t.ic_tabla_amort||' - '|| t.cd_descripcion");
						
		cat.setValoresCondicionNotIn("0", java.lang.Integer.class);
	
		infoRegresar=cat.getJSONElementos();
	
	}else if (informacion.equals("CatalogoOperacion")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_base_operacion");
		cat.setCampoClave("ig_codigo_base");
		cat.setCampoDescripcion("ig_codigo_base ||' '||cg_descripcion");
		
		cat.setOrden("ig_codigo_base");		
		
		infoRegresar=cat.getJSONElementos();	
	}else if(informacion.equals("CatalogoEmisor")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_emisor");
		cat.setCampoClave("ic_emisor");
		cat.setCampoDescripcion("cd_descripcion");
		
		infoRegresar=cat.getJSONElementos();	
	
	}else if (informacion.equals("CatalogoPeriocidad")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_periodicidad");
		cat.setCampoClave("ic_periodicidad");
		cat.setCampoDescripcion("cd_descripcion");
		
		List lis= new ArrayList ();

		infoRegresar=cat.getJSONElementos();
	
	}else if (informacion.equals("CatalogoOperacion")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_base_operacion");
		cat.setCampoClave("ig_codigo_base");
		cat.setCampoDescripcion("ig_codigo_base ||' '||cg_descripcion");

		cat.setOrden("ig_codigo_base");		
		
		infoRegresar=cat.getJSONElementos();	
	}else if(informacion.equals("ConsiguePiso")){
		String mensaje = "";
		String no_if	  = (request.getParameter("no_if")==null)?"":request.getParameter("no_if");
		
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
	
		mensaje = BeanParametrosGrales. consigueCarteraCredito(no_if);
	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("PISO",mensaje);
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("ConsiguePlazo")){
		String noIf = (request.getParameter("no_if")==null)?"":request.getParameter("no_if");
		String tipoTasa = (request.getParameter("tipoTasa")==null)?"":request.getParameter("tipoTasa");
		String tipoCred = (request.getParameter("tipoCred")==null)?"":request.getParameter("tipoCred");
		String tipoAmort = (request.getParameter("tipoAmort")==null)?"":request.getParameter("tipoAmort");
		String tipoCartera = (request.getParameter("tipoCartera")==null)?"":request.getParameter("tipoCartera").trim();
		String tipo_plazo = (request.getParameter("tipo_plazo")==null)?"":request.getParameter("tipo_plazo");
			
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
		
		HashMap hm = new HashMap();
		hm = (HashMap)BeanParametrosGrales.consiguePlazoCredito(noIf,tipoTasa,tipoCred,tipoAmort,tipoCartera,tipo_plazo);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("TIPO", hm.get("TIPO"));
		jsonObj.put("VARC", hm.get("VARC"));
		jsonObj.put("VARE", hm.get("VARE"));
		jsonObj.put("VARF", hm.get("VARF"));
		jsonObj.put("PLAZOS", hm.get("PLAZOS"));

		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("ConsultaGrid") ){
		List regis = new ArrayList();
		
		String noif		  = (request.getParameter("comboIF")==null)?"":request.getParameter("comboIF");
		String tasa	  = (request.getParameter("comboTasa")==null)?"":request.getParameter("comboTasa");
		String credito  = (request.getParameter("comboCredito")==null)?"":request.getParameter("comboCredito");
		String amortizacion	  = (request.getParameter("comboAmortizacion")==null)?"":request.getParameter("comboAmortizacion");
		String cartera  = (request.getParameter("comboCartera")==null)?"":request.getParameter("comboCartera");
		String plazo	  = (request.getParameter("tipoPlazo")==null)?"":request.getParameter("tipoPlazo");

		String emisor	  = (request.getParameter("comboEmisor")==null)?"":request.getParameter("comboEmisor");
		String periodicidad  = (request.getParameter("comboPeriocidad")==null)?"":request.getParameter("comboPeriocidad");
		String renta	  = (request.getParameter("tipoRenta")==null)?"":request.getParameter("tipoRenta");	
		
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
		
		
		regis =(ArrayList)BeanParametrosGrales.getCamposCapturaCredito(noif,tasa,credito,amortizacion,cartera,plazo,emisor,periodicidad,renta); 
		registros = JSONArray.fromObject(regis);

		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		
		infoRegresar = jsonObj.toString();	
	}
	else if (informacion.equals("ConsiguePlazoMinimo")){
	
		String noif		  = (request.getParameter("comboIF")==null)?"":request.getParameter("comboIF");
		String tasa	  = (request.getParameter("comboTasa")==null)?"":request.getParameter("comboTasa");
		String credito  = (request.getParameter("comboCredito")==null)?"":request.getParameter("comboCredito");
		String amortizacion	  = (request.getParameter("comboAmortizacion")==null)?"":request.getParameter("comboAmortizacion");
		String cartera  = (request.getParameter("comboCartera")==null)?"":request.getParameter("comboCartera");
		String plazo	  = (request.getParameter("tipoPlazo")==null)?"":request.getParameter("tipoPlazo");

		String emisor	  = (request.getParameter("comboEmisor")==null)?"":request.getParameter("comboEmisor");
		String periodicidad  = (request.getParameter("comboPeriocidad")==null)?"":request.getParameter("comboPeriocidad");
		String renta	  = (request.getParameter("tipoRenta")==null)?"":request.getParameter("tipoRenta");
		
		HashMap hm = new HashMap();
		
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
		
		hm =(HashMap)BeanParametrosGrales.getPlazoyOperacionCapturaCredito(noif,tasa,credito,amortizacion,cartera,plazo,emisor,periodicidad,renta); 
			
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("PLAZO", hm.get("PLAZO"));
		jsonObj.put("ULTIMO", hm.get("ULTIMO"));
		jsonObj.put("BASEDINAMICA",hm.get("BASEOPERACIONDINAMICA"));

		infoRegresar = jsonObj.toString();	
	}else if(informacion.equals("Aceptar")){
				
		String noif		  = (request.getParameter("comboIF")==null)?"":request.getParameter("comboIF");
		String tasa	  = (request.getParameter("comboTasa")==null)?"":request.getParameter("comboTasa");
		String credito  = (request.getParameter("comboCredito")==null)?"":request.getParameter("comboCredito");
		String amortizacion	  = (request.getParameter("comboAmortizacion")==null)?"":request.getParameter("comboAmortizacion");
		String cartera  = (request.getParameter("comboCartera")==null)?"":request.getParameter("comboCartera");
		String plazo	  = (request.getParameter("tipoPlazo")==null)?"":request.getParameter("tipoPlazo");

		String emisor	  = (request.getParameter("comboEmisor")==null)?"":request.getParameter("comboEmisor");
		String periodicidad  = (request.getParameter("comboPeriocidad")==null)?"":request.getParameter("comboPeriocidad");
		String renta	  = (request.getParameter("tipoRenta")==null)?"":request.getParameter("tipoRenta");
		
		String interes	  = (request.getParameter("comboInteres")==null)?"":request.getParameter("comboInteres");
		String plzMin	  = (request.getParameter("valorMinimo")==null)?"":request.getParameter("valorMinimo");
		String plzMax	  = (request.getParameter("valorMaximo")==null)?"":request.getParameter("valorMaximo");
		
		String operacion = (request.getParameter("comboOperacion")==null)?"":request.getParameter("comboOperacion");
		String baseOperacion = (request.getParameter("baseOperacion")==null)?"":request.getParameter("baseOperacion");
		String altaAutomatica = (request.getParameter("altaAutomatica")==null)?"":request.getParameter("altaAutomatica");
		String codigoBanco =  (request.getParameter("codigoBanco")==null)?"":request.getParameter("codigoBanco");
		
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
	
		boolean exito = BeanParametrosGrales.aceptarCapturaCredito(noif,tasa,operacion,credito,amortizacion,emisor,cartera,plzMin,plzMax,plazo,altaAutomatica,
																						codigoBanco, periodicidad,interes,renta,baseOperacion );
		
		String mensaje = "";
		if(exito){
			mensaje = "La base de operación fue actualizada";
		}else{
			mensaje = "Error en la inserción de nuevos datos";
		}
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("MENSAJE",mensaje);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("Eliminar")){
		String mensaje = "";
		String clave	  = (request.getParameter("baseOperacion")==null)?"":request.getParameter("baseOperacion");
		
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
	
		boolean resul = BeanParametrosGrales.eliminarCapturaCredito(clave);
		if(!resul)
			mensaje= "Error al eliminar";
	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("MENSAJE",mensaje);
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("Modificar")){
		String mensaje = "";
		String plzMax	  = (request.getParameter("plzMax")==null)?"":request.getParameter("plzMax");
		String cveoperacion	  = (request.getParameter("claveCombo")==null)?"":request.getParameter("claveCombo");
		String alta	  = (request.getParameter("altaAutomatica")==null)?"":request.getParameter("altaAutomatica");
		String basedinamica	  = (request.getParameter("baseOpDinamica")==null)?"":request.getParameter("baseOpDinamica");
		String basecredito	  = (request.getParameter("baseOpeCredito")==null)?"":request.getParameter("baseOpeCredito");	
		
		ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
	
		boolean resul = BeanParametrosGrales.modificarCapturaCredito(plzMax,cveoperacion,basecredito,alta,basedinamica);
		if(!resul)
			mensaje= "Error al modificar registro";
	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("MENSAJE",mensaje);
		infoRegresar = jsonObj.toString();
	}
%>

<%= infoRegresar %>
