<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.seguridadbean.*,,
		com.netro.cadenas.*,	
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	if (informacion.equals("CatalogoBanco")){
		com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
		
		String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);
		//BeanSegFacultad.validaFacultad( (String) strLogin, (String) strPerfil,(String) session.getAttribute("sesCvePerfilProt"), "15ADMNAFPARAMDOCTOA", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
								
		CatalogoSimple cat = new CatalogoSimple();
		
		if("8".equals(cvePerf)){
			cat.setTabla("comcat_banco_fondeo");
			cat.setCampoClave("ic_banco_fondeo");
			cat.setCampoDescripcion("cd_descripcion");
			
			List lis= new ArrayList ();
			lis.add("2");
			cat.setValoresCondicionIn(lis);
		}else{
			cat.setTabla("comcat_banco_fondeo");
			cat.setCampoClave("ic_banco_fondeo");
			cat.setCampoDescripcion("cd_descripcion");
		}

		infoRegresar=cat.getJSONElementos(); 	
	
	}else if (informacion.equals("ConsultaGrid")  ||  informacion.equals("ArchivoCSV") ){
		
		String noBancoFondeo = (request.getParameter("noBancoFondeo")==null)?"":request.getParameter("noBancoFondeo");
		String consulta = "";
		JSONObject 	jsonObj	= new JSONObject();
	
		ConsDocXProducto paginador = new ConsDocXProducto();
		paginador.setnoBancoFondeo(noBancoFondeo);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
		
		if (informacion.equals("ConsultaGrid")  ){
		
			try {
				Registros reg	=	queryHelper.doSearch();
				
				consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
				jsonObj.put("success", new Boolean(true));
				jsonObj = JSONObject.fromObject(consulta);
			}catch(Exception e) {
				throw new AppException("Error en la paginación", e);
			}
			infoRegresar = jsonObj.toString();
	
	
		}else  if ( informacion.equals("ArchivoCSV") ) {
			try {
					String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo CSV", e);
				}
		
		}
		
	infoRegresar = jsonObj.toString(); 
}
%>	

<%= infoRegresar %>