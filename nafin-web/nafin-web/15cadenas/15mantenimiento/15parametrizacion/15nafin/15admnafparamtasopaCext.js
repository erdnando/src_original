




Ext.onReady(function() {

	var idMenuP =  Ext.getDom('idMenuP').value;
		
	
	//-----------------CONSULTA  ------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
		el.unmask();
		
		if (arrRegistros != null) {
			
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			
			var TODAS_TASAS = opts.params.cboTasa === "TODAS"?true:false;
			
			// Mostrar nombre de la Tasa o TASAS si se seleccion� TODAS
			if( TODAS_TASAS ){
				gridConsulta.setTitle('<div><div style="float:left">TASAS</div>');
			} else {
				gridConsulta.setTitle('<div><div style="float:left">'+jsonData.nombre_tasa+'</div>');
			}
			
			// Mostrar/Ocultar campo NOMBRE_TASA
			var nombreTasaIndex = gridConsulta.getColumnModel().getIndexById("NOMBRE_TASA");
			if( TODAS_TASAS ){
				gridConsulta.getColumnModel().setHidden(nombreTasaIndex,false);
			} else {
				gridConsulta.getColumnModel().setHidden(nombreTasaIndex,true);
			}
			
			// Cambiar longitud campo FECHA
			var fechaIndex = gridConsulta.getColumnModel().getIndexById("FECHA");
			if( TODAS_TASAS ){
				gridConsulta.getColumnModel().setColumnWidth( fechaIndex, 90);
			} else {
				gridConsulta.getColumnModel().setColumnWidth( fechaIndex, 150);
			}
			
			// Cambiar longitud del grid
			if( TODAS_TASAS ){
				gridConsulta.setWidth( 500 ); 
			} else {
				gridConsulta.setWidth( 320 ); 
			}
			
		} 
		
		if(store.getTotalCount() == 0) {							
			el.mask('No se encontr� ning�n registro', 'x-mask');				
		}
			
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15admnafparamtasopa.data.jsp',
		baseParams: {
			informacion: 'ConsultaC'
		},		
		fields: [			
			{	name: 'NOMBRE_TASA' },
			{	name: 'FECHA', type: 'date',   convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } },
			{	name: 'VALOR', type: 'float',  convert: function(value,record){ return (Ext.isEmpty(value)?null:Number(value));                  } } // Se retonar null para evitar que los valores vac�o se conviertan en cero.
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
					
					// Suprimir m�scara que indica: No se encontraron registros
					var gridEl = Ext.getCmp("gridConsulta").getGridEl();
					if( gridEl.isMasked() ){
						gridEl.unmask();
					}
					
				}
			},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(this, null, null);					
				}
			}
		}		
	});
	

	var nombreTasaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;" ';
		return value;
			
	}
	
	var valorRenderer = Ext.util.Format.numberRenderer('0.00000');
	
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title: 'Tasas Operativas',
		autoExpandColumn: 'NOMBRE_TASA',
		columns: [		
			{
				id:		'NOMBRE_TASA',
				header: 'Tasa', 
				tooltip: 'Tasa',
				dataIndex: 'NOMBRE_TASA',
				sortable: true,
				resizable: true,
				//width: 210,			
				align: 'left',
				renderer: nombreTasaRenderer
			},
			{
				id: 'FECHA',
				header: 'Fecha', 
				tooltip: 'Fecha',
				dataIndex: 'FECHA',
				sortable: true,
				resizable: true,
				width: 90,			
				align: 'center',
				xtype: 'datecolumn', 
				format: 'd/m/Y' // 'd-F-Y'
			},
			{ 
				header: 'Valor',
				tooltip: 'Valor',
            dataIndex: 'VALOR',
            sortable: true,
            hideable: false,
            width: 150,
            align: 'center',
            renderer: valorRenderer
         }
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 500,
		align: 'center',
		frame: false		
	});
			
	//-----------------INTERFAZ GRAFICA  ------------------------	
	function procesaValoresIniciales(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp("txt_fecha_app_de").setValue(jsonValoresIniciales.fecha_Actual);
			Ext.getCmp("txt_fecha_app_a").setValue(jsonValoresIniciales.fecha_Actual);
			
			var  total= jsonValoresIniciales.permisos.length; 
			
			if(total>0) {
				for(i=0;i<total;i++){ 
					boton=  jsonValoresIniciales.permisos[i];							
					Ext.getCmp(boton).show();						
				}
			}else  {		
				Ext.getCmp("BTNCAPTURA").hide();	
			}
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var catalogoTasa = new Ext.data.JsonStore({
		id: 'catalogoTasa',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15admnafparamtasopa.data.jsp',
		baseParams: {
			informacion: 'catalogoTasa'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var  elementosForma  = [	
		{
			xtype: 'combo',
			fieldLabel: 'Tasa',
			name: 'cboTasa',
			id: 'cboTasa1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',	
			valueField: 'clave',
			hiddenName : 'cboTasa',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,				
			store: catalogoTasa,
			width: 200,
			tpl : NE.util.templateMensajeCargaCombo					
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Aplicaci�n',
			combineErrors: false,
			msgTarget: 'side',
			width: 700,
			items: [
				{
					xtype: 'datefield',
					name: 'txt_fecha_app_de',
					id: 'txt_fecha_app_de',
					allowBlank: true,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txt_fecha_app_a',
					margins: '0 20 0 0'  					
				},
				{
					xtype: 'displayfield',
					value: 'al ',
					width: 30
				},
				{
					xtype: 'datefield',
					name: 'txt_fecha_app_a',
					id: 'txt_fecha_app_a',
					allowBlank: true,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txt_fecha_app_de',
					margins: '0 20 0 0'  					
				}	
			]
		}		
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Criterios de Busquedad',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {	
				
					var cboTasa1 = Ext.getCmp('cboTasa1');
						
					if (Ext.isEmpty(cboTasa1.getValue()) ){
							cboTasa1.markInvalid('Debe seleccionar una Tasa');
							return;
					}
					var txt_fecha_app_de = Ext.getCmp('txt_fecha_app_de');
					var txt_fecha_app_a = Ext.getCmp('txt_fecha_app_a');
					
					if(Ext.isEmpty(txt_fecha_app_de.getValue())){
						txt_fecha_app_de.markInvalid('Debe capturar ambas fechas ');
						txt_fecha_app_de.focus();
						return;
					}
					if(Ext.isEmpty(txt_fecha_app_a.getValue())){
						txt_fecha_app_a.markInvalid('Debe capturar ambas fechas ');
						txt_fecha_app_a.focus();
						return;
					}
					
					var txt_fecha_app_de_ = Ext.util.Format.date(txt_fecha_app_de.getValue(),'d/m/Y');
					var txt_fecha_app_a_ = Ext.util.Format.date(txt_fecha_app_a.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(txt_fecha_app_de.getValue())){
						if(!isdate(txt_fecha_app_de_)) { 
							txt_fecha_app_de.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txt_fecha_app_de.focus();
							return;
						}
					}
					if( !Ext.isEmpty(txt_fecha_app_a.getValue())){
						if(!isdate(txt_fecha_app_a_)) { 
							txt_fecha_app_a.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							txt_fecha_app_a.focus();
							return;
						}
					}

					if( 
						!Ext.isEmpty(txt_fecha_app_de.getValue())
							&&
						!Ext.isEmpty(txt_fecha_app_a.getValue())
					){
						if( txt_fecha_app_a.getValue() > txt_fecha_app_de.getValue().add(Date.DAY,30) ){
							Ext.Msg.alert('Mensaje',"El rango de fechas excede el per&iacute;odo v&aacute;lido de 30 d&iacute;as, favor de verificar.");
							return;
						}
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ConsultaC'									
						})
					});
				}
			}
		]
	});
	
	var fpBotones = new Ext.Container({
		layout: 'table',		
		style: 'margin:0 auto;',
		id: 'fpBotones',		
	   width: '200',
		heigth:'auto',		
		items: [
			{
				xtype: 'button',
				text: 'Captura',
				tooltip:	'Captura',										
				id: 'BTNCAPTURA',				
				handler: function(boton, evento) {				
					window.location = '15admnafparamtasopaext.jsp?pagina=Captura&idMenu='+idMenuP;
				}
			},	
			{
					xtype: 'displayfield',
					value: '     ',
					width: 20
			},
			{
				xtype: 'button',
				text: 'Consulta',
				tooltip:	'Consulta',											
				id: 'BTNCONSULTA',	
				hidden: true,
				handler: function(boton, evento) {				
					window.location = '15admnafparamtasopaext.jsp?pagina=Consulta&idMenu='+idMenuP;
				}
			}		
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	catalogoTasa.load({
		params: {
			pantalla:'Consulta'
		}
	});
	
	Ext.Ajax.request({
		url: '15admnafparamtasopa.data.jsp',
		params: {
			informacion: "valoresIniciales",
			idMenuP:idMenuP
		},
		callback: procesaValoresIniciales
	});
		
});	



