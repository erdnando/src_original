<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.cadenas.*,
		java.text.*,
		com.netro.seguridadbean.SeguException, 
		javax.imageio.*,
		com.netro.zip.*,
		java.io.*,
		net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar ="";
if (	informacion.equals("catalogoBanco") ) {
String ic_banco = (request.getParameter("ic_banco") != null)?request.getParameter("ic_banco"):"";

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_banco_fondeo");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("comcat_banco_fondeo");
	if(!ic_banco.equals(""))
		catalogo.setCondicionIn(ic_banco,"Integer");
	catalogo.setOrden("1");
	infoRegresar = catalogo.getJSONElementos();

}else if (informacion.equals("catalogoEpo")) {
	
	String nueva = (request.getParameter("nueva") != null)?request.getParameter("nueva"):"";
	String banco = (request.getParameter("Hbanco") != null)?request.getParameter("Hbanco"):"";
	CatalogoEpoDisenio catalogo= new CatalogoEpoDisenio();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social||' (No. Epo: '||e.ic_epo||')'");
	catalogo.setNuevaEpo(nueva.equals("true")?true:false);
	catalogo.setIcBanco(banco);
	catalogo.setOrden("2");
	infoRegresar = catalogo.getJSONElementos();
}else if(informacion.equals("tiempo")){
	Thread.currentThread().sleep(1000);
	JSONObject jsonObj			= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("subirArchivo")) {
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
 
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(2097152);
		myUpload.upload();
		
	} catch(Exception e) {
			
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 2097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 2 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
 
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	Enumeration en = myUpload.getRequest().getParameterNames();
	Map params = new HashMap();
	while (en.hasMoreElements()) {
		String name = en.nextElement().toString().trim();
		String value = myUpload.getRequest().getParameter(name);
		params.put(name, value);
	}
	String ic_epo = (String)params.get("Hic_epo");
	if(ic_epo.equals("-1"))
		ic_epo="nafin";
	String Hbanco = (String)params.get("Hbanco");
	// Guardar Archivo en Disco
	int numFiles = 0;
	boolean banderaImgError=false;
	boolean validaExt=false;
	String logo ="";
	String nombreArchivoZip=null;
	try {
		myUpload.save(strDirectorioTemp);
		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		//myFile.save(strDirectorioTemp);
		nombreArchivoZip = myFile.getFileName();
		String ruta = strDirectorioTemp + nombreArchivoZip;
		resultado.put("fileName", 	ruta			);
		
		DisenoEpo diseno = new DisenoEpo();

		diseno.setClaveEpo( ic_epo );
		
		 validaExt = diseno.validarExtjs(	ruta,false);
		
		banderaImgError=false;
		if (validaExt){
		logo = diseno.descomprimirArchivosDeFS(strDirectorioTemp, ruta, "PREVIEW");
		
		String origen ="", arvo ="";
		File fFile = null;
		//boolean contenidoValidoExt = diseno.validarExtjs(ruta,true);
		//if (!contenidoValidoExt){
%>

<%
		//}else{
			String rutaEpo = application.getRealPath("/") + "/00utils/css/" + ic_epo + "/";
			String imgEpo = rutaEpo + "imgs/";
			Comunes.crearDirectorios(rutaEpo);
			Comunes.crearDirectorios(imgEpo);

			//Descompriimos los archivos del zip en la misma ruta...
			ComunesZIP.descomprimir(ruta,	strDirectorioTemp);
			//Agregamos los archivos por defecto de nafin
		   
			FileInputStream fis		= null;
			OutputStream outstream	= null;
			int bytesRead;
			List archivosDiseno		= new ArrayList();

			archivosDiseno.add("dhtmlxmenu_dhx_nafin.css");
			archivosDiseno.add("estilos.css");
			archivosDiseno.add("imgs/dhtmlxmenu_bg.gif");
			archivosDiseno.add("imgs/dhtmlxmenu_bg.gif");
			String rutaOrigen			= application.getRealPath("/") + "/00utils/css/nafin/";
			boolean flag 				= false;

			try{
				for (int x=0;x<archivosDiseno.size();x++){
					arvo = (String)archivosDiseno.get(x);
					fFile = new File(rutaOrigen + arvo	);
					fis = new FileInputStream (fFile);

					if(flag){
						outstream = new BufferedOutputStream(new FileOutputStream(strDirectorioTemp + arvo.replaceAll("imgs/dhtmlxmenu_bg.gif","dhtmlxmenu_bg.gif") ));
					}else{
						outstream = new BufferedOutputStream(new FileOutputStream(strDirectorioTemp + arvo ));
					}

					if(arvo.equals("imgs/dhtmlxmenu_bg.gif")){
						flag = true;
					}

					byte [  ]  buf = new byte [ 4 * 1024 ] ; // 4K buffer

					while  (  ( bytesRead = fis.read ( buf )  )  != -1 ){
						 outstream.write ( buf, 0, bytesRead );
					}

					if  ( fis != null ){
						 fis.close();
					}
					if (outstream != null){
						outstream.flush();
						outstream.close();
					}
				}

			}catch( java.io.FileNotFoundException e )   {
				out.println ( "File not found: " + origen );
				
				success		= false;
				throw new AppException("File not found: " + origen );
			}catch( IOException e )   {
				out.println ( "Error al copiar " + origen + ": " + e.getMessage (  )  );
				success		= false;
				throw new AppException("Error al copiar " + origen + ": " + e.getMessage (  )  );
			}finally{
			}
			//Validamos el tamaño de las Imagenes
			List reg=new ArrayList();
			HashMap mapaImg=null;
			
			
			if(!diseno.validaTamanioImg(strDirectorioTemp+ic_epo+".gif",65,175)){
				mapaImg=new HashMap();
				mapaImg.put("IMAGEN",ic_epo+".gif");
				mapaImg.put("ERROR","El tamaño de la Imagen debe de ser de 175 X 65");
				reg.add(mapaImg);
				banderaImgError=true;
			}
			
			if(!diseno.validaTamanioImg(strDirectorioTemp+"banner1s.jpg",137,198)){
				mapaImg=new HashMap();
				mapaImg.put("IMAGEN","banner1s.jpg");
				mapaImg.put("ERROR","El tamaño de la Imagen debe de ser de 198 x 137 ");
				reg.add(mapaImg);
				banderaImgError=true;
			}
			if(!diseno.validaTamanioImg(strDirectorioTemp+"banner2s.jpg",137,198)){
				mapaImg=new HashMap();
				mapaImg.put("IMAGEN","banner2s.jpg");
				mapaImg.put("ERROR","El tamaño de la Imagen debe de ser de 198 x 137 ");
				reg.add(mapaImg);
				banderaImgError=true;
			}
			if(!diseno.validaTamanioImg(strDirectorioTemp+"banner3s.jpg",137,198)){
				mapaImg=new HashMap();
				mapaImg.put("IMAGEN","banner3s.jpg");
				mapaImg.put("ERROR","El tamaño de la Imagen debe de ser de 198 x 137 ");
				reg.add(mapaImg);
				banderaImgError=true;
			}
			if(!diseno.validaTamanioImg(strDirectorioTemp+"banner4s.jpg",137,198)){
				mapaImg=new HashMap();
				mapaImg.put("IMAGEN","banner4s.jpg");
				mapaImg.put("ERROR","El tamaño de la Imagen debe de ser de 198 x 137 ");
				reg.add(mapaImg);
				banderaImgError=true;
			}
			if(!diseno.validaTamanioImg(strDirectorioTemp+"dhtmlxmenu_bg.gif",200,3)){
				mapaImg=new HashMap();
				mapaImg.put("IMAGEN","dhtmlxmenu_bg.gif");
				mapaImg.put("ERROR","El tamaño de la Imagen debe de ser de 3 X 200");
				reg.add(mapaImg);
				banderaImgError=true;
			}
			resultado.put("registros",reg);
			resultado.put("errorImagen",new Boolean(banderaImgError));
			
			
			
			//Volvemos a crear el archivo .zip
			archivosDiseno = new ArrayList();
			archivosDiseno.add(ic_epo + ".gif");
			
			fFile = new File(	strDirectorioTemp + ic_epo + "_home.gif");
			if (fFile.exists()){
				archivosDiseno.add(ic_epo+"_home.gif");
			}
			archivosDiseno.add("banners.jsp");
			archivosDiseno.add("banner1s.jpg");
			archivosDiseno.add("banner2s.jpg");
			archivosDiseno.add("banner3s.jpg");
			archivosDiseno.add("banner4s.jpg");
			archivosDiseno.add("dhtmlxmenu_dhx_nafin.css");
			archivosDiseno.add("estilos.css");
			archivosDiseno.add("dhtmlxmenu_bg.gif");
			ComunesZIP.comprimir(archivosDiseno, strDirectorioTemp,	strDirectorioTemp+ic_epo+".zip");
		}
		
	}catch(Exception e){
		
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		 throw new AppException("Error en la carga del archivo", e);
	}
	
	// Enviar parametros adicionales

	resultado.put("ic_epo", 		ic_epo 	);
	resultado.put("nombreArchivoZip", 		nombreArchivoZip 	);
	resultado.put("logo", 		logo 	);	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 		"REALIZAR_VALIDACION" 	);
	// Enviar resultado de la operacion
	resultado.put("validaElementos", new Boolean(validaExt));
	resultado.put("success", 					new Boolean(success)		);
	if(success&&validaExt&&!banderaImgError){
	%>
		
	<%	
	}
 
	infoRegresar = resultado.toString();
}else if (informacion.equals("disenoAceptado")) {
	
	String ic_epo = request.getParameter("ic_epo");
	String nombreArchivoZip = request.getParameter("nombreArchivoZip");
	String ruta = strDirectorioTemp + nombreArchivoZip;
	
	DisenoEpo diseno = new DisenoEpo();
	
	diseno.setClaveEpo( ic_epo );
	diseno.guardar(ruta);
	diseno.descomprimirArchivosDeFS(application.getRealPath("/"), ruta, "FINAL");
	String mensaje = "Los cambios han sido realizados con exito";
	JSONObject	resultado	= new JSONObject();
	resultado.put("mensaje", 		mensaje 	);
	// Especificar el estado siguiente
	

	resultado.put("success", 					new Boolean(true)		);
	infoRegresar = resultado.toString();
	}else
	if(informacion.equals("descargaArchivo")){
		
		
		String ic_epo = request.getParameter("ic_epo");
		DisenoEpo diseno = new DisenoEpo();
		diseno.setClaveEpo(ic_epo);
		diseno.generarArchivoZip(strDirectorioTemp);
		String archivo= ic_epo + ".zip";
		JSONObject jsonObj 	      = new JSONObject();
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp + archivo);
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("vistaPrevia")){
	
		DisenoEpo diseno = new DisenoEpo();
		String logo = "";
		String ic_epo = request.getParameter("ic_epo");
		if(ic_epo.equals("-1"))
			ic_epo="nafin";
		
		com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
		
		diseno.setClaveEpo( ic_epo );
		String tmp = application.getRealPath("/")+ "/00tmp/15cadenas/dhtmlxmenu_dhx_nafin.css";
		String tmpBanner = application.getRealPath("/") + "/00tmp/15cadenas/banners.jsp";
		File fTmp = new File(tmp);
		File fTmpBanner = new File(tmpBanner);
		if (fTmp.exists()){
			fTmp.delete();
		}
		if (fTmpBanner.exists()){
			fTmpBanner.delete();
		}
		logo = diseno.descomprimirArchivosDeBD(strDirectorioTemp, "PREVIEW");

		try{
					BeanSegFacultad.validaFacultad( (String) strLogin, (String) strPerfil,(String) session.getAttribute("sesCvePerfilProt"), "15ADMNAFPARAMIMGCONS", (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
							
		}catch(SeguException seg){}
	
		JSONObject jsonObj 	      = new JSONObject();
		jsonObj = new JSONObject();
		jsonObj.put("ic_epo",ic_epo);
		jsonObj.put("operacion","CONSULTA");
		jsonObj.put("logo",logo);
		jsonObj.put("success",new Boolean(true));
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("valoresIniciales")){
		
		com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
		String cvePerf = BeanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);
		JSONObject jsonObj 	      = new JSONObject();
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("cvePerf",cvePerf);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("disenoPredeterminado")){
		String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
		String mensaje = "";
		DisenoEpo diseno = new DisenoEpo();
		diseno.setClaveEpo(ic_epo);
		String ruta = application.getRealPath("/");
		diseno.eliminar(ruta);
		String rutaEpo = ruta + "/00utils/css/" + ic_epo + "/";
		File fRuta = new File(rutaEpo);
		Comunes.deleteDirectory(fRuta);
		mensaje = "El diseño predeterminado, para la EPO, ha sido restablecido";
		JSONObject jsonObj 	      = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		infoRegresar = jsonObj.toString();
	}

%>
<%=infoRegresar%>