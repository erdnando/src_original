<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.afiliacion.*,	
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";

	if (informacion.equals("CatalogoEpo")){
		List cat = new ArrayList(); 
		JSONArray registros = new JSONArray();
		JSONObject jsonObj = new JSONObject();	
		
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");

		cat = catalogo.getListaElementosAutorizaCtasBenef();
		registros = JSONArray.fromObject(cat);
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
			
		infoRegresar = jsonObj.toString();	
	}else if (informacion.equals("CatalogoIF")){
		String cveepo	= (request.getParameter("cveepo") != null) ? request.getParameter("cveepo") :"";

		List cat = new ArrayList(); 
		JSONArray registros = new JSONArray();
		JSONObject jsonObj = new JSONObject();
		CatalogoIF	catIf = new CatalogoIF();
		catIf.setClave("i.ic_if");
		catIf.setDescripcion("i.cg_razon_social");
		catIf.setIc_epo(cveepo); 
		catIf.setCsBeneficiario("S");
		cat = catIf.getListaElementosGral();	
		
		registros = JSONArray.fromObject(cat);		
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("CatalogoFinanciera")){
		List cat = new ArrayList(); 
		JSONArray registros = new JSONArray();
		JSONObject jsonObj = new JSONObject();
	
		CatalogoFinanciera catFinanciera = new CatalogoFinanciera();
		catFinanciera.setClave("f.ic_financiera");
		catFinanciera.setDescripcion("f.cd_nombre");
		catFinanciera.setTipo("1");
		cat = catFinanciera.getListaElementos();
		registros = JSONArray.fromObject(cat);
	
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString();	
	}else if (informacion.equals("ConsultaDatos")){
		List registro = new ArrayList (); 
		JSONArray registros = new JSONArray();
		JSONObject jsonObj = new JSONObject();	
		String nombreEpo = "";
		String Nombremoneda = "";
		String NoCajones = ""; 
		
		String ic_epo	  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
		String cveif	  = (request.getParameter("comboIF")==null)?"":request.getParameter("comboIF");

		Afiliacion 		afiliacion 		= ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
							
		jsonObj.put("success", new Boolean(true));
		List  lstCuentasBenef = (ArrayList)afiliacion.getCuentasBeneficiarios(ic_epo,cveif);		
		
		jsonObj.put("CUENTAPESO", "");						
		jsonObj.put("BANCO", "");						
		jsonObj.put("NUMSUCURSAL", "");					
		jsonObj.put("ICCUENTAPESO", "");
		jsonObj.put("CUENTADOLAR", "");						
		jsonObj.put("ICCUENTADOLAR", "");	
					
		if(lstCuentasBenef!=null && lstCuentasBenef.size()>0){				
			for(int x=0;x<lstCuentasBenef.size();x++){					
				ArrayList lstCuenta = (ArrayList)lstCuentasBenef.get(x);
				if("1".equals((String)lstCuenta.get(2))){						
					jsonObj.put("CUENTAPESO", (String)lstCuenta.get(3));						
					jsonObj.put("BANCO", (String)lstCuenta.get(4));						
					jsonObj.put("NUMSUCURSAL", (String)lstCuenta.get(5));					
					jsonObj.put("ICCUENTAPESO", (String)lstCuenta.get(9));
				}
				if("54".equals((String)lstCuenta.get(2))){						
					jsonObj.put("CUENTADOLAR", (String)lstCuenta.get(3));						
					jsonObj.put("ICCUENTADOLAR", (String)lstCuenta.get(9));
				}					
			}
		}		

		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("ConsultaDataGrid")){
		JSONArray registros = new JSONArray();
		JSONObject jsonObj = new JSONObject();	
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\",\"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);

		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("Guardar")){	

		String msgGuardar = "";
		
		String epo  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
		String c_if  = (request.getParameter("comboIF")==null)?"":request.getParameter("comboIF");
		String banco  = (request.getParameter("comboFinanciera")==null)?"":request.getParameter("comboFinanciera");
		String cDolar  = (request.getParameter("CuentaDolar")==null)?"":request.getParameter("CuentaDolar");
		String cPeso = (request.getParameter("CuentaPeso")==null)?"":request.getParameter("CuentaPeso");
		String sucursal = (request.getParameter("nSucursal")==null)?"":request.getParameter("nSucursal");
		String icDolar = (request.getParameter("icDolar")==null)?"":request.getParameter("icDolar");
		String icPeso = (request.getParameter("icPeso")==null)?"":request.getParameter("icPeso");
				
		Afiliacion 		afiliacion 		= ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
		
		ArrayList lstDatosForm = new ArrayList();
				
		lstDatosForm.add("G");
		lstDatosForm.add(epo);
		lstDatosForm.add(c_if);
		lstDatosForm.add(banco);
		lstDatosForm.add(cDolar);
		lstDatosForm.add(cPeso);
		lstDatosForm.add(sucursal);
		lstDatosForm.add(icPeso);
		lstDatosForm.add(icDolar);
		
		msgGuardar = afiliacion.setCuentasBeneficiarios(lstDatosForm);
								
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("RESULTADO", msgGuardar);
		infoRegresar = jsonObj.toString();		
	}
%>

<%= infoRegresar %>