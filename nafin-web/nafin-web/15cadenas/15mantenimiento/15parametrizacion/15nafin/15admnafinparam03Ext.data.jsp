<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		com.netro.dispersion.*,
		javax.naming.Context,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoBancoServicio_b,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoIF,
		org.apache.commons.logging.Log,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String icIF			= (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
String icEpo		= (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
String txtNumCuenta	= (request.getParameter("txtCuenta") == null)?"":request.getParameter("txtCuenta");
String txtCuentaClabe = (request.getParameter("txtCuentaClabe") == null)?"":request.getParameter("txtCuentaClabe");
String VoBoNafin	= (request.getParameter("VoBoNafin") == null)?"":request.getParameter("VoBoNafin");
String cboPlaza		= (request.getParameter("cboPlaza") == null)?"":request.getParameter("cboPlaza");
String txtSucursal	= (request.getParameter("txtSucursal") == null)?"":request.getParameter("txtSucursal");
String txtBanco		= (request.getParameter("bco_anterior") == null)?"":request.getParameter("bco_anterior");
String cmb_banco	= (request.getParameter("txtBanco") == null)?"":request.getParameter("txtBanco");
String txtPlazaAnt	= (request.getParameter("txtPlazaAnt") == null)?"":request.getParameter("txtPlazaAnt");
String ic_epo			= (request.getParameter("ic_epo") 			== null)?"":request.getParameter("ic_epo");
String ic_if			= (request.getParameter("ic_if") 			== null)?"":request.getParameter("ic_if");
String txtCuenta		= (request.getParameter("txtCuenta")    	== null)?"":request.getParameter("txtCuenta");
String noBancoFondeo	= "";//(request.getParameter("noBancoFondeo") == null)?"":request.getParameter("noBancoFondeo");
String cboMoneda		= (request.getParameter("cboMoneda")   == null)?"":request.getParameter("cboMoneda");
String infoRegresar ="";
JSONObject jsonObj		= new JSONObject();
boolean success			= true;

ParametrosDescuento paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 

String operaFideicomisoIF= "N", 	bOperaFideicomisoEPO ="N";	

	//Verifico si la EPO e IF operan Fideicomiso Fodea 017-2013
	Hashtable alParamEPO1 = new Hashtable();
	if(!ic_epo.equals("")) {
		alParamEPO1 = paramDsctoEJB.getParametrosEPO(ic_epo,1);
		bOperaFideicomisoEPO = alParamEPO1.get("CS_OPERA_FIDEICOMISO").toString();		
	}
	if(!ic_if.equals("")) {
		operaFideicomisoIF=  paramDsctoEJB.getOperaFideicomiIF( ic_if);
	}	 
	
		
		
if (													informacion.equals("getClavePerfil")){

	jsonObj	= new JSONObject();
	com.netro.seguridadbean.Seguridad beanSegFacultad = null;
	try {
	
		beanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class); 
	
	}catch(Exception e){
		success		= false;
		log.error("getClavePerfil(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
	
	}

	String cvePerf = beanSegFacultad.getTipoAfiliadoXPerfil(strPerfil);

	jsonObj.put("cvePerf",				cvePerf						);
	jsonObj.put("estadoSiguiente",	"RESPUESTA_CLAVE_PERFIL");
	jsonObj.put("success",				new Boolean(success)		);
	infoRegresar = jsonObj.toString();

}else if (									informacion.equals("Catalogo.BancoFondeo")){

	String cvePerf = (request.getParameter("cvePerf") == null)?"":request.getParameter("cvePerf");

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_banco_fondeo");
	cat.setCampoClave("ic_banco_fondeo");
	cat.setCampoDescripcion("cd_descripcion");
	if("8".equals(cvePerf)){
		cat.setValoresCondicionIn("2",	Integer.class);
	}
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (												informacion.equals("Catalogo.Epo")){

	String cvePerf		= (request.getParameter("cvePerf")		== null)?"":request.getParameter("cvePerf");
	String bancoFondeo= (request.getParameter("bancoFondeo")	== null)?"":request.getParameter("bancoFondeo");

	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	if("8".equals(cvePerf)){
		cat.setClaveBancoFondeo("2");
	}else{
		cat.setClaveBancoFondeo(bancoFondeo);
	}
	cat.setOrden("cg_razon_social");

	infoRegresar = cat.getJSONElementos();

}else if (												informacion.equals("Catalogo.IF")){

	CatalogoIF cat = new CatalogoIF();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("cg_razon_social");
	List elementos = cat.getListaElementosGral();
	JSONArray jsonArr = new JSONArray();
	Iterator it = elementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	infoRegresar = "{\"success\": true, \"total\": \"" + 
			jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

}else if (								informacion.equals("Catalogo.Banco.Servicio")){

	CatalogoBancoServicio_b cat = new CatalogoBancoServicio_b();
	cat.setCampoClave("TRIM(cd_nombre)");
	cat.setCampoDescripcion("TRIM(cd_nombre)");
	cat.setDistinc(true);
	cat.setTipoFinanciera("1");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (											informacion.equals("Catalogo.Plaza") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_plaza");
	cat.setCampoClave("ic_plaza");
	cat.setCampoDescripcion("INITCAP(cd_descripcion||','||cg_estado)");
	cat.setOrden("2");

	infoRegresar = cat.getJSONElementos();

}else if (									informacion.equals(".Modificar") ) {

	jsonObj		= new JSONObject();
	String estadoSiguiente	= "";
	Registros registros		= null;
	String claveIf	= (request.getParameter("claveIf") == null)?"":request.getParameter("claveIf");
	String claveEpo= (request.getParameter("claveEpo") == null)?"":request.getParameter("claveEpo");

	String RazonSocialEPO      = "";
	String RazonSocialIF       = "";
	String cgBanco               = "";
	String cgNumeroCuenta           = "";
	String cgCuentaClabe = "";	
	String icPlaza               = "";
	String cgSucursal            = "";

	registros	=	paramDsctoEJB.getDatosCtasEpo(claveIf, claveEpo);

	if( registros.next() ){
		RazonSocialEPO      = (registros.getString(1) == null)?"":registros.getString(1).trim();
		RazonSocialIF       = (registros.getString(7) == null)?"":registros.getString(7).trim();
		cgBanco               = (registros.getString(2) == null)?"":registros.getString(2).trim();
		cgNumeroCuenta           = (registros.getString(3) == null)?"":registros.getString(3).trim();
		cgCuentaClabe = (registros.getString(12) == null)?"":registros.getString(12).trim();
		VoBoNafin           = (registros.getString(4) == null)?"":registros.getString(4).trim();
		icPlaza               = (registros.getString(11) == null)?"":registros.getString(11).trim();
		cgSucursal            = (registros.getString(8) == null)?"":registros.getString(8).trim();
	}

	jsonObj.put("RazonSocialEPO",	RazonSocialEPO			);
	jsonObj.put("RazonSocialIF",	RazonSocialIF					);
	jsonObj.put("cgBanco",			cgBanco				);
	jsonObj.put("cgSucursal",		cgSucursal			);
	jsonObj.put("cgNumeroCuenta",	cgNumeroCuenta		);
	jsonObj.put("icPlaza",			icPlaza				);
	jsonObj.put("cgCuentaClabe",	cgCuentaClabe		);
	jsonObj.put("VoBoNafin",		VoBoNafin					);
	jsonObj.put("claveIf",			claveIf					);
	jsonObj.put("claveEpo",			claveEpo					);

	estadoSiguiente = "RESPUESTA_MODIFICAR";

	jsonObj.put("estadoSiguiente",	estadoSiguiente				);
	jsonObj.put("success",				new Boolean(success)			);
	infoRegresar = jsonObj.toString();

}else if (									informacion.equals(".validaClaveUser") ){

	jsonObj		= new JSONObject();
String strMensaje		= "", estadoSiguiente="";

	Dispersion dispersion = null;
	try {

		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class); 

	}catch(Exception e){
		success		= false;
		log.error("validaClaveUser(Exception): Obtener instancia del EJB de Dispersion");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersion.");
	}

	String cg_login		= (request.getParameter("cgLogin") == null) ? "" : request.getParameter("cgLogin");
	String cg_password	= (request.getParameter("cg_password") == null) ? "" : request.getParameter("cg_password");

	try{
		if(dispersion.esUsuarioCorrecto(strLogin, cg_login, cg_password)) {
			strMensaje = "";
		} else {
			strMensaje = "El login y/o password del usuario es incorrecto, <br>operación cancelada";
		}
	}catch(Exception e){
		strMensaje = "El login y/o password del usuario es incorrecto, <br>operación cancelada";
		e.printStackTrace();
	}
	estadoSiguiente = "RESPUESTA_VALIDACION";
	jsonObj.put("strMensaje",	strMensaje				);
	jsonObj.put("estadoSiguiente",	estadoSiguiente				);
	jsonObj.put("success",			new Boolean(success)		);
	infoRegresar = jsonObj.toString();

}else if (									informacion.equals(".Actualizar") ){

	jsonObj		= new JSONObject();
	String strMensaje = paramDsctoEJB.setDatosCtasEpo(icIF,icEpo, txtNumCuenta, txtCuentaClabe,VoBoNafin,
					cboPlaza, txtSucursal,cmb_banco,txtBanco,txtPlazaAnt, iNoUsuario );

	jsonObj.put("estadoSiguiente",	"RESPUESTA_ACTUALIZAR"						);
	jsonObj.put("strMensaje",			strMensaje								);
	jsonObj.put("success",				new Boolean(success)					);
	infoRegresar = jsonObj.toString();

}else if (													informacion.equals(".Agregar") ){

	jsonObj		= new JSONObject();
	
	String strMensaje			= "";
	boolean ok=false;
	try{
		
		ok	=	paramDsctoEJB.addDatosCtasEpo(ic_epo,	ic_if,	cmb_banco,	txtSucursal,	txtCuenta,	cboPlaza,	txtCuentaClabe );
		
		if(ok)
			strMensaje = "Se dió de alta la relación";
		else
			strMensaje = "La relación entre la Cadena y el Intermediario ya existe";

	}catch(Exception e){
		success = false;
		log.error(".Agregar(Exception): Error al agregar registro");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al agregar un registro.");
	}

	jsonObj.put("msg",					strMensaje					);
	jsonObj.put("ok",						ok==true?"true":"else"	);
	jsonObj.put("estadoSiguiente",	"RESPUESTA_AGREGAR"		);
	jsonObj.put("success",				new Boolean(success)		);
	infoRegresar = jsonObj.toString();

}else if (										informacion.equals(".Consulta") ){
  int start = 0;
	int limit = 0;
	
	ConsCtaBancariasEpo paginador =new  ConsCtaBancariasEpo();
	paginador.setClaveEpo(ic_epo);
	paginador.setClaveIf(ic_if);
	paginador.setBanco(txtBanco);
	paginador.setSucursal(txtSucursal);
	paginador.setCuenta(txtCuenta);
	paginador.setCuentaClabe(txtCuentaClabe);
	paginador.setPlaza(cboPlaza);
	paginador.setBancoFondeo(noBancoFondeo);
	paginador.setOperanFideicomisoEPO(bOperaFideicomisoEPO);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request);
		}
		//infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		Registros reg   = queryHelper.getPageResultSet(request,start,limit);
		while(reg.next()){			
			String ic_ifc = (reg.getString("IC_IF")==null)?"":(reg.getString("IC_IF"));
			String operaFideicomisoIFc=  paramDsctoEJB.getOperaFideicomiIF( ic_ifc);		
			reg.setObject("OPERA_FIDEICOMISO_IF",operaFideicomisoIFc);
		}
		
		String consulta = 	"{\"success\": true, \"total\": \""+ queryHelper.getIdsSize() +"\", \"registros\": " + reg.getJSONData()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString();

	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}


}else if ( informacion.equals("OperaFideicomiso") ){
	
	
	String tipoFide = (request.getParameter("tipoFide") != null)?request.getParameter("tipoFide"):"";
	
	jsonObj.put("success",				new Boolean(success));
	jsonObj.put("bOperaFideicomisoEPO",bOperaFideicomisoEPO);
	jsonObj.put("operaFideicomisoIF",operaFideicomisoIF);
	jsonObj.put("tipoFide",tipoFide		);		
	infoRegresar = jsonObj.toString();

	
}
%>
<%=infoRegresar%>
  