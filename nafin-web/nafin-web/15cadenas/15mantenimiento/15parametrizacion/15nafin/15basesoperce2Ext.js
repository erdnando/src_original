Ext.onReady(function(){
	var TipoPlazo          = "";
	var operacionDinamica  = "N";	 
	var automaticaGarantia = "N";	
	var renta              = "";	
	
	var valorIF            = "";
	var valorTasa          = "";
	var valorCredito       = "";
	var valorAmortizacion  = "";
	var valorCartera       = "";
	

//-------------Handlers----------------------------------------------------------------------------	
	
	var determinaPlazo = function(){	
		Ext.Ajax.request({
			url: '15basesoperce2Ext.data.jsp',
			params: {
						informacion:'ConsiguePlazo',
						no_if:Ext.getCmp('idComboIF').getValue(),											
						tipoTasa:Ext.getCmp('idComboTasa').getValue(),
						tipoCred:Ext.getCmp('idComboCredito').getValue(),
						tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
						tipoCartera:Ext.getCmp('idComboCartera').getValue(),
						tipo_plazo:TipoPlazo
			},	
			callback: procesarPlazo
		});	
	}
	
	var ocultarPlazo = function(){
				Ext.getCmp('radioFactoraje').hide();
				Ext.getCmp('radioCredito').hide();
				Ext.getCmp('radioCotizaciones').hide();
				Ext.getCmp('dfCotizaciones').hide()
				Ext.getCmp('dfCredito').hide();
				Ext.getCmp('dfFactoraje').hide();
	
	}

	function procesarPlazo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var info = Ext.util.JSON.decode(response.responseText);
			if(info.PLAZOS == '1'){
				if(info.TIPO == 'F'){
					Ext.getCmp('leyenda').setText('Factoraje');
				}else if (info.TIPO == 'C'){
					Ext.getCmp('leyenda').setText('Cr�dito');
				}else if(info.TIPO == 'E'){
					Ext.getCmp('leyenda').setText('Cotizaciones Espec�ficas');
				}				
				Ext.getCmp('cfPlazo').hide();
				Ext.getCmp('cfPlazoEtiqueta').show();
		}else{
				TipoPlazo = "";  //?
				
				Ext.getCmp('cfPlazoEtiqueta').hide();
				Ext.getCmp('cfPlazo').reset();

				if(info.VARF == 'true'){	
					Ext.getCmp('radioFactoraje').show();
					Ext.getCmp('dfFactoraje').show();
				}else{
					Ext.getCmp('radioFactoraje').hide();
					Ext.getCmp('dfFactoraje').hide();
				}
				if(info.VARC == 'true'){
					Ext.getCmp('radioCredito').show();
					Ext.getCmp('dfCredito').show();
				}else{
					Ext.getCmp('radioCredito').hide();
					Ext.getCmp('dfCredito').hide();
				}
				if(info.VARE == 'true'){
					Ext.getCmp('radioCotizaciones').show();
					Ext.getCmp('dfCotizaciones').show();
				}else{
					Ext.getCmp('radioCotizaciones').hide();
					Ext.getCmp('dfCotizaciones').hide();
				}	
				Ext.getCmp('cfPlazo').show();		
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var RenPlazo = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.TIPOPLAZO == "F")
									return 'Factoraje';
								else if(record.data.TIPOPLAZO == "C"){
									return 'Cr�dito';
								}else if (record.data.TIPOPLAZO == "E"){
									return 'Cotizaciones Espec�ficas'
								}							
						}
							
	
	var RenCartera = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.CARTERA == "1")
									return '1er Piso';
								else 
									return '2do Piso';
						}		
	
	var RenAlta = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.ALTA == "S")
									return 'Si';
								else 
									return 'No';
						}	

	var RenBase = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.BASEDINAMICA == "S")
									return 'Si';
								else 
									return 'No';
						}
						
	var RenRenta = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.TIPORENTA == ""){
									return 'N/A';
								}else if(record.data.TIPORENTA == "S"){ 
									return 'Si';
								}else{
									return 'No';
								}
						}	
	
	var RenCodigo = function(value, metadata, record, rowindex, colindex, store) {
								if(record.data.CODIGOBASE != "" || record.data.CODIGOBASE != null ){
									var cadena = record.data.CODIGOBASE +" - "+ record.data.NOMBASEOPER; 
									return cadena;
								}else {
									return '';
								}
						}	
	
	
	function procesarPiso(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var info = Ext.util.JSON.decode(response.responseText);
			var cvePiso = info.PISO
			if(cvePiso == '1'){
				dataCartera.removeAll();
				dataCartera.insert(0,new Todas({ 
									clave: "", 
									descripcion: "Seleccionar Cartera", 
									loadMsg: ""})); 
				dataCartera.insert(1,new Todas({ 
									clave: "1", 
									descripcion: "1er Piso", 
									loadMsg: ""})); 
									dataCartera.commitChanges();
			}else{	
				dataCartera.removeAll();
				dataCartera.insert(0,new Todas({ 
									clave: "", 
									descripcion: "Seleccionar Cartera", 
									loadMsg: ""})); 
				dataCartera.insert(1,new Todas({ 
									clave: "2", 
									descripcion: "2do Piso", 
									loadMsg: ""})); 
									dataCartera.commitChanges();
			}
			valorCartera = cvePiso;
			Ext.getCmp('idComboCartera').setValue(info.PISO);
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton = Ext.getCmp('btnGenerarArchivo');
			boton.enable();
			Ext.getCmp('btnImprimir').enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var cargaCombos = function(){
			CatalogoIF.load({
				params: {
								//noIf:Ext.getCmp('idComboIF').getValue(),
								tipoTasa:Ext.getCmp('idComboTasa').getValue(),
								tipoCred:Ext.getCmp('idComboCredito').getValue(),
								tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
								tipoCartera:Ext.getCmp('idComboCartera').getValue(),
								tipo_plazo:TipoPlazo															
							}
			});
						
			CatalogoTasa.load({
				params: {
								noIf:Ext.getCmp('idComboIF').getValue(),
								//tipoTasa:Ext.getCmp('idComboTasa').getValue(),
								tipoCred:Ext.getCmp('idComboCredito').getValue(),
								tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
								tipoCartera:Ext.getCmp('idComboCartera').getValue(),
								tipo_plazo:TipoPlazo															
							}
			});
			
			CatalogoCredito.load({
				params: {
								noIf:Ext.getCmp('idComboIF').getValue(),
								tipoTasa:Ext.getCmp('idComboTasa').getValue(),
								//tipoCred:Ext.getCmp('idComboCredito').getValue(),
								tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
								tipoCartera:Ext.getCmp('idComboCartera').getValue(),
								tipo_plazo:TipoPlazo
																						
							}
			});
			
			CatalogoAmortizacion.load({
				params: {
								noIf:Ext.getCmp('idComboIF').getValue(),
								tipoTasa:Ext.getCmp('idComboTasa').getValue(),
								tipoCred:Ext.getCmp('idComboCredito').getValue(),
								//tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
								tipoCartera:Ext.getCmp('idComboCartera').getValue(),
								tipo_plazo:TipoPlazo															
							}
			});
	}
	
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');	
		if(arrRegistros!=null){
			Ext.getCmp('btnConsultar').enable();
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnImprimir').enable();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnGenerarArchivo').disable();
				Ext.getCmp('btnImprimir').disable();				
			}
		}
	}	
		
	
	var procesarInicio= function(){
		Ext.getCmp('radioBaseNo').setValue(true);
		Ext.getCmp('radioAltaNo').setValue(true);
		Ext.getCmp('radioRentaNa').setValue(true);
	}
	
	var procesarCredito = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Cr�dito", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboCredito').setValue(valorCredito);
	}
	
	var procesarIF = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar IF", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboIF').setValue(valorIF);
	}
	
	var procesarTasa = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Tasa", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboTasa').setValue(valorTasa);
	}
	
	var procesarAmortizacion = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Amortizaci�n", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboAmortizacion').setValue(valorAmortizacion);
	}
	
	var procesarPeriocidad = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Periodicidad", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboPeriocidad').setValue('');
	}	
	
	var procesarInteres = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar Tipo de Interes", 
		loadMsg: ""})); 
		store.commitChanges(); 
		Ext.getCmp('idComboInteres').setValue('');
	}		
	
	
//-------------Stores--------------------------------------------------------------------------  	
	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15basesoperce2Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'NOMIF'},
			{name: 'NOMTASA'},
			{name: 'NOMTIPCRED'},
			{name: 'NOMAMORTIZA'},
			{name: 'PLZMIN'},
			{name: 'PLZMAX'},
			{name: 'ICBASEOPECRED'},
			{name: 'TIPOPLAZO'},
			{name: 'CARTERA'},
			{name: 'NOMBASEOPER'},
			{name: 'CODIGOBASE'},
			{name: 'NOMEMISOR'},
			{name: 'ALTA'},			
			{name: 'NOMPERIODICIDAD'},
			{name: 'PRODUCTOBANCO'},
			{name: 'TIPOINTERES'},
			{name: 'TIPORENTA'},
			{name: 'BASEDINAMICA'}			
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
		
	var CatalogoIF = new Ext.data.JsonStore
	({
		id: 'catIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperce2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoIF'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarIF,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});  
	
	var CatalogoTasa = new Ext.data.JsonStore
	({
		id: 'catTasa',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperce2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoTasa'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarTasa,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
	
	var CatalogoCredito = new Ext.data.JsonStore
	({
		id: 'catCredito',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperce2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoCredito'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarCredito,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});  
	
	var CatalogoAmortizacion = new Ext.data.JsonStore
	({
		id: 'catAmortizacion',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperce2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoAmortizacion'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarAmortizacion,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 


	var CatalogoPeriocidad = new Ext.data.JsonStore
	({
		id: 'catPeriocidad',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperce2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoPeriocidad'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarPeriocidad,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
	
	
	var CatalogoInteres = new Ext.data.JsonStore
	({
		id: 'catInteres',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15basesoperce2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoInteres'
		},
		autoLoad: false,
		listeners:
		{
		 load: procesarInteres,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
  
	var dataCartera = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['','Seleccionar Cartera'],
			['1','1er Piso'],
			['2','2do Piso']
		]		
	 });
	
	
	var dataInteres = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['','Seleccionar Tipo Interes'],
			['S','Simple'],
			['C','Compuesto']
		]		
	 });	

	
	
	var Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);

	
	
	var elementosForma = 
	[			
			{
				xtype: 'combo',
				fieldLabel: 'Nombre IF',
				forceSelection: true,
				autoSelect: true,
				name:'claveIF',
				id:'idComboIF',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoIF,  
				listeners:{
					select: function(combo){						
								if(valorIF == combo.getValue()){
									return ;
								}
		
								grid.hide(); //?
								valorIF = combo.getValue();					
								
								if(combo.getValue() != ''){
									Ext.Ajax.request({
										url: '15basesoperce2Ext.data.jsp',
										params: {
													no_if:combo.getValue(),
													informacion:'ConsiguePiso'
										},	
										callback: procesarPiso
									});
								}
						
								CatalogoTasa.load({
									params: {
													noIf:Ext.getCmp('idComboIF').getValue(),
													//tipoTasa:Ext.getCmp('idComboTasa').getValue(),
													tipoCred:Ext.getCmp('idComboCredito').getValue(),
													tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:Ext.getCmp('idComboCartera').getValue(),
													tipo_plazo:TipoPlazo															
												}
								});
									
								CatalogoCredito.load({
									params: {
													noIf:Ext.getCmp('idComboIF').getValue(),
													tipoTasa:Ext.getCmp('idComboTasa').getValue(),
													//tipoCred:Ext.getCmp('idComboCredito').getValue(),
													tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:Ext.getCmp('idComboCartera').getValue(),
													tipo_plazo:TipoPlazo																										
												}
								});
								
							
								CatalogoAmortizacion.load({
									params: {
													noIf:Ext.getCmp('idComboIF').getValue(),
													tipoTasa:Ext.getCmp('idComboTasa').getValue(),
													tipoCred:Ext.getCmp('idComboCredito').getValue(),
													//tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:Ext.getCmp('idComboCartera').getValue(),
													//tipoCartera: "2",
													tipo_plazo:TipoPlazo																										
												}
								});									
						 determinaPlazo();

					}
				},
				anchor:	'89%'
		},
			{
				xtype: 'combo',
				fieldLabel: 'Tasa',
				forceSelection: true,
				autoSelect: true,
				name:'claveTasa',
				id:'idComboTasa',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoTasa, 
				listeners:{
					select: function(combo){
								if(valorTasa == combo.getValue()){
									return ;
								}
								
								grid.hide(); //??
								valorTasa = combo.getValue();
								CatalogoIF.load({
									params: {
													//noIf:Ext.getCmp('idComboIF').getValue(),
													tipoTasa:Ext.getCmp('idComboTasa').getValue(),
													tipoCred:Ext.getCmp('idComboCredito').getValue(),
													tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:Ext.getCmp('idComboCartera').getValue(),
													tipo_plazo:TipoPlazo
																											
												}
								});
								
								
								CatalogoCredito.load({
									params: {
													noIf:Ext.getCmp('idComboIF').getValue(),
													tipoTasa:Ext.getCmp('idComboTasa').getValue(),
													//tipoCred:Ext.getCmp('idComboCredito').getValue(),
													tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:Ext.getCmp('idComboCartera').getValue(),
													tipo_plazo:TipoPlazo
																											
												}
								});
								
							
								CatalogoAmortizacion.load({
									params: {
													noIf:Ext.getCmp('idComboIF').getValue(),
													tipoTasa:Ext.getCmp('idComboTasa').getValue(),
													tipoCred:Ext.getCmp('idComboCredito').getValue(),
													//tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:Ext.getCmp('idComboCartera').getValue(),
													tipo_plazo:TipoPlazo														
												}
								});
							determinaPlazo();
					}	
				
			},					
			anchor:	'75%'
		},
		{
				xtype: 'combo',
				fieldLabel: 'Tipo Cr�dito',
				forceSelection: true,
				autoSelect: true,
				name:'claveCredito',
				id:'idComboCredito',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoCredito,  
				listeners:{
					select: function(combo){
								if(valorCredito == combo.getValue()){
									return ;
								}								
								
								grid.hide(); //??
								valorCredito = combo.getValue();
								
								CatalogoIF.load({
									params: {
													//noIf:Ext.getCmp('idComboIF').getValue(),
													tipoTasa:Ext.getCmp('idComboTasa').getValue(),
													tipoCred:Ext.getCmp('idComboCredito').getValue(),
													tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:Ext.getCmp('idComboCartera').getValue(),
													tipo_plazo:TipoPlazo
																											
												}
								});
								
								
								CatalogoTasa.load({
									params: {
													noIf:Ext.getCmp('idComboIF').getValue(),
													//tipoTasa:Ext.getCmp('idComboTasa').getValue(),
													tipoCred:Ext.getCmp('idComboCredito').getValue(),
													tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:Ext.getCmp('idComboCartera').getValue(),
													tipo_plazo:TipoPlazo
																											
												}
								});
							
								CatalogoAmortizacion.load({
									params: {
													noIf:Ext.getCmp('idComboIF').getValue(),
													tipoTasa:Ext.getCmp('idComboTasa').getValue(),
													tipoCred:Ext.getCmp('idComboCredito').getValue(),
													//tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:Ext.getCmp('idComboCartera').getValue(),
													//tipoCartera: "2",
													tipo_plazo:TipoPlazo
																											
												}
								});			
								determinaPlazo();					
					}					
			},	
			anchor: '77%'
		},	
		{
				xtype: 'combo',
				fieldLabel: 'Tipo Amortizaci�n',
				forceSelection: true,
				autoSelect: true,
				name:'claveAmortizacion',
				id:'idComboAmortizacion',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoAmortizacion,  
				listeners:{
					select: function(combo){
						if(valorAmortizacion == combo.getValue()){
							return ;
						}
						
						grid.hide(); //??
						valorAmortizacion = combo.getValue();
						
						if(combo.getValue() == '5'){
							Ext.getCmp('radioRentaSi').enable();
							Ext.getCmp('radioRentaNo').enable();
						}else{
							Ext.getCmp('radioRentaSi').disable();
							Ext.getCmp('radioRentaNo').disable();						
						} 
					
						CatalogoIF.load({
								params: {
												//noIf:Ext.getCmp('idComboIF').getValue(),
												tipoTasa:Ext.getCmp('idComboTasa').getValue(),
												tipoCred:Ext.getCmp('idComboCredito').getValue(),
												tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
												tipoCartera:Ext.getCmp('idComboCartera').getValue(),
												tipo_plazo:TipoPlazo																
											}
							});
							
							
							CatalogoTasa.load({
								params: {
												noIf:Ext.getCmp('idComboIF').getValue(),
												//tipoTasa:Ext.getCmp('idComboTasa').getValue(),
												tipoCred:Ext.getCmp('idComboCredito').getValue(),
												tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
												tipoCartera:Ext.getCmp('idComboCartera').getValue(),
												tipo_plazo:TipoPlazo															
											}
							});
							
							CatalogoCredito.load({
								params: {
												noIf:Ext.getCmp('idComboIF').getValue(),
												tipoTasa:Ext.getCmp('idComboTasa').getValue(),
												//tipoCred:Ext.getCmp('idComboCredito').getValue(),
												tipoAmort:Ext.getCmp('idComboAmortizacion').getValue(),
												tipoCartera:Ext.getCmp('idComboCartera').getValue(),
												tipo_plazo:TipoPlazo																
											}
							});						
						determinaPlazo();
					}					
			},	
			anchor: '65%'
		},
		{
				xtype: 'combo',
				fieldLabel: 'Tipo de Cartera',
				forceSelection: true,
				autoSelect: true,
				name:'claveCartera',
				id:'idComboCartera',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: dataCartera,  
						listeners:{
						select:	function(combo){
						if(valorCartera == combo.getValue()){
							return ;
						}	
						valorCartera      = combo.getValue();				
						valorIF           = "";
						valorTasa         = "";
						valorCredito      = "";
						valorAmortizacion = "";
						
						if(combo.getValue() == ''){
							dataCartera.removeAll();
							dataCartera.insert(0,new Todas({ 
									clave: "", 
									descripcion: "Seleccionar Cartera", 
									loadMsg: ""})); 
								
							dataCartera.insert(1,new Todas({ 
									clave: "1", 
									descripcion: "1er Piso", 
									loadMsg: ""})); 
								
							dataCartera.insert(2,new Todas({ 
									clave: "2", 
									descripcion: "2do Piso", 
									loadMsg: ""})); 
									dataCartera.commitChanges();
								
						}else if(combo.getValue() == '1'){
							dataCartera.removeAll();
							dataCartera.insert(0,new Todas({ 
									clave: "", 
									descripcion: "Seleccionar Cartera", 
									loadMsg: ""})); 
							dataCartera.insert(1,new Todas({ 
									clave: "1", 
									descripcion: "1er Piso", 
									loadMsg: ""}));
							dataCartera.commitChanges();
								
						}else if(combo.getValue() == '2'){
							dataCartera.removeAll();
							dataCartera.insert(0,new Todas({ 
									clave: "", 
									descripcion: "Seleccionar Cartera", 
									loadMsg: ""})); 
							dataCartera.insert(1,new Todas({ 
									clave: "2", 
									descripcion: "2do Piso", 
									loadMsg: ""}));
							dataCartera.commitChanges();								
						}
								
						cargaCombos(); 
					}
				},
				value: '',
				anchor: '50%'
		},
		{
			xtype: 'compositefield',
			id: 'cfPlazo',
			fieldLabel:'Tipo de Plazo',
			hidden:false,
			msgTarget: 'side',
			items: [

				{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioFactoraje',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "F"
											grid.hide();
											cargaCombos();
											determinaPlazo();
										}
									}
					}	
				},	{
					xtype: 'displayfield',
					id: 'dfFactoraje',
					value: 'Factoraje	',
					width: 100
				}, 				{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioCredito',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "C";
											grid.hide();
											cargaCombos();
											determinaPlazo();
										}
									}
					}					
				},{
					xtype: 'displayfield',
					id:'dfCredito',
					value: 'Cr�dito ',
					width: 100
				},	{ 
					xtype: 'radio',
					name: 'tipo_plazo',
					id: 'radioCotizaciones',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											TipoPlazo = "E";
											grid.hide();
											cargaCombos();
											determinaPlazo();
										}
									}
					}					
				},{
					xtype: 'displayfield',
					id: 'dfCotizaciones',
					value: 'Cotizaciones Espec�ficas ',
					width: 200
				}
			]
		},		
		{
			xtype: 'compositefield',
			id: 'cfPlazoEtiqueta',
			width:300,
			hidden: true,
			fieldLabel:'Tipo de Plazo',
			msgTarget: 'side',
			items: [
			{
					xtype: 'displayfield',
					value: '',
					width: 0
			},
			{		
						xtype: 	'label',
						name:'nameLeyenda',
						id:	'leyenda',
						text:  	''
			}
			]	
		},		
		{
			xtype: 'compositefield',
			id: 'cfBase',
			hidden: false,
			fieldLabel:'Base de Operaci�n Din�mica',
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'tipo_base',
					id: 'radioBaseNo',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											operacionDinamica = "N";
										}
									}
					}
				},	{
					xtype: 'displayfield',
					value: 'No	',
					width: 100
				}, { 
					xtype: 'radio',
					name: 'tipo_base',
					id: 'radioBaseSi',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											operacionDinamica = "S";
										}
									}
					}	
				},{
					xtype: 'displayfield',
					value: 'Si ',
					width: 100
				}		
			]
		},
		{
			xtype: 'compositefield',
			id: 'cfAlta',
			hidden: false,
			fieldLabel:'Alta Autom�tica de Garant�a',
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'tipo_alta',
					id: 'radioAltaNo',
					listeners:{
						check:  function(radio){
										if (radio.checked){
											automaticaGarantia = "N";
										}
									}
					}
				},	{
					xtype: 'displayfield',
					value: 'No	',
					width: 100
				}, 				{ 
					xtype: 'radio',
					name: 'tipo_alta',
					id: 'radioAltaSi',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											automaticaGarantia = "S";
										}
									}
					}		
				},{
					xtype: 'displayfield',
					value: 'Si ',
					width: 100
				}		
			]
		},		
		{
				xtype: 'combo',
				fieldLabel: 'Periocidad de Pago a Capital',
				forceSelection: true,
				autoSelect: true,
				name:'clavePeriocidad',
				id:'idComboPeriocidad',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: CatalogoPeriocidad, 
				listeners:{
				
				},					
				anchor:	'50%'
		},
		{
				xtype: 'combo',
				fieldLabel: 'Tipo de Inter�s',
				forceSelection: true,
				autoSelect: true,
				name:'claveInteres',
				id:'idComboInteres',
				displayField: 'descripcion',
				allowBlank: true,
				editable:true,
				valueField: 'clave',
				mode: 'local',
				triggerAction: 'all',
				autoLoad: false,
				typeAhead: true,
				minChars: 1,
				store: dataInteres,  
				anchor: '50%',
				value: ''
		},	
		{
			xtype: 'compositefield',
			id: 'cfRenta',
			fieldLabel:'Tipo Renta',
			hidden:false,
			msgTarget: 'side',
			items: [
				{ 
					xtype: 'radio',
					name: 'tipo_renta',
					id: 'radioRentaNa',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											renta = "";
										}
									}
					}			
				},	{
					xtype: 'displayfield',
					value: 'N/A	',
					width: 100
				}, 				{ 
					xtype: 'radio',
					name: 'tipo_renta',
					id: 'radioRentaSi',
					disabled: true,
					listeners:{
						check:	function(radio){
										if (radio.checked){
											renta = "S";
										}
									}
					}		
				},{
					xtype: 'displayfield',
					value: 'Si',
					width: 100
				},	{ 
					xtype: 'radio',
					name: 'tipo_renta',
					disabled: true,
					id: 'radioRentaNo',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											renta = "N";
										}
									}
					}				
				},{
					xtype: 'displayfield',
					value: 'No ',
					width: 200
				}
			]
		}		
	]// fin elementos forma
	


	var fp = new Ext.form.FormPanel({
		id:'formaDetalle',
		style: ' margin:0 auto;',
		hidden: false,
		title: '<center>Consultas Cr�dito electr�nico</center>',
		frame: true,
		bodyStyle: 'padding: 6px',
		defaults: {
			anchor: '-20'
		},
		items: elementosForma,
				buttons:[{
						xtype:	'button',
						id:		'btnConsultar',
						text:		'Consultar',
						iconCls: 'icoBuscar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {												
										if(Ext.getCmp('idComboIF').getValue() == ''){
											Ext.getCmp('idComboIF').markInvalid('Debe seleccionar un IF para realizar la consulta');
										}else{
											boton.disable();
											grid.show();
											consultaDataGrid.load({
												params: {
													noIf: Ext.getCmp('idComboIF').getValue(),
													tipoTasa: Ext.getCmp('idComboTasa').getValue(),
													baseOperacionDinamica: operacionDinamica,
													altaAutomaticaGarantia: automaticaGarantia,
													tipoCred: Ext.getCmp('idComboCredito').getValue(),
													tipoAmort: Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:  Ext.getCmp('idComboCartera').getValue(),
													tipo_plazo: TipoPlazo,
													periodicidad: Ext.getCmp('idComboPeriocidad').getValue(),
													tipoInteres:  Ext.getCmp('idComboInteres').getValue(),													
													tipoRenta: renta
												}
											});		
										}						
						},
						style: 'margin:0 auto;',
						align:'rigth'
					},{
						xtype:	'button',
						id:		'btnCancel',
						text:		'Limpiar',
						iconCls: 'icoLimpiar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							window.location = '15basesoperce2Ext.jsp';
						},
						style: 'margin:0 auto;'
					},{
						xtype:	'button',
						id:		'btnRegresar',
						text:		'Regresar',
						iconCls: 'icoContinuar',
						width: 50,
						weight: 70,
						handler: function(boton, evento) {
							window.location = '15basesoperExt.jsp';
						},
						style: 'margin:0 auto;'
					}					
					],
		labelWidth: 180,
		style: 'margin:0 auto;',	   
		height: 380,
		width: 720
	});



	var grid = new Ext.grid.GridPanel( {
		store: consultaDataGrid,
		sortable: true,
		id: 'grid',
		hidden: true,
		columns: [
			{
				header: '<center>Nombre IF</center>',
				dataIndex: 'NOMIF',
				width: 280,
				renderer: function(value,metadata,record,rowindex,colindex,store){
								metadata.attr = 'ext:qtip="' + value + '"';								
								return value;
							},				
				align: 'left'				
			},{
				header: '<center>Tasa</center>',
				dataIndex: 'NOMTASA',
				align: 'left',
				width: 210
			},{
				header: '<center>Tipo <BR> Cr�dito</center>',
				dataIndex: 'NOMTIPCRED',
				align: 'left',
				width: 290
			},{
				header: '<center>Tipo <BR> Amortizaci�n</center>',
				dataIndex: 'NOMAMORTIZA',
				align: 'left',
				width: 250
			},{
				header: '<center>Tipo de<BR> Cartera</center>',
				dataIndex: 'CARTERA',
				align: 'left',
				width: 130,
				renderer: RenCartera
			},{
				header: '<center>Tipo Plazo</center>',
				dataIndex: 'PLAZO',
				align: 'left',
				width: 130,
				renderer: RenPlazo
			}
			,{
				header: 'Plazo M�nimo',
				dataIndex: 'PLZMIN',
				align: 'center',
				width: 100
			},{
				header: 'Plazo M�ximo',
				dataIndex: 'PLZMAX',
				align: 'center',
				width: 100
			},{
				header: 'Emisor',
				dataIndex: 'NOMEMISOR',
				align: 'center',
				width: 260
			},{
				header: '<center>C�digo <BR> Operaci�n</center>',
				dataIndex: 'CODIGOBASE',
				align: 'left',
				renderer: RenCodigo,			
				width: 410
			},{
				header: '<center>Base<BR>Operaci�n<BR>Din�mica</center>',
				dataIndex: 'BASEDINAMICA',
				align: 'center',
				width: 80,
				renderer: RenBase
			}, {
				header: '<center>Alta Aut.<BR>Garant. </center>',
				dataIndex: 'ALTA',
				align: 'center',
				width: 80,
				renderer: RenAlta
			},{
				header: '<center>C�digo<BR>Producto<BR>Banco</center>',
				dataIndex: 'PRODUCTOBANCO',
				align: 'left',
				width: 80
			},{
				header: '<center>Periodicidad<BR>de Pago a<BR>Capital</center>',
				dataIndex: 'NOMPERIODICIDAD',
				align: 'left',
				width: 80
			},{
				header: '<center>Tipo de<BR>Inter�s</center>',
				dataIndex: 'TIPOINTERES',
				align: 'left',
				width: 80
			},{
				header: '<center>Tipo Renta</center>',
				dataIndex: 'TIPORENTA',
				align: 'center',
				width: 80,
				renderer: RenRenta
			}			
		],
		
		loadMask: true,
		style: 'margin:0 auto;',
		height: 400,
		bbar: {
					autoScroll:true,
					id: 'barra',
					displayInfo: true,
					items: ['->','-',
					{	
						xtype:	'button',
						id:		'btnGenerarArchivo',
						text:		'Generar Archivo',
						iconCls: 'icoXls',
						width: 90,
						weight: 70,
						style: 'margin:0 auto;',
						align:'rigth',
						handler: function(boton, evento) {
							boton.disable();
							Ext.Ajax.request({
								url: '15basesoperce2Ext.data.jsp',
								params: {
													noIf: Ext.getCmp('idComboIF').getValue(),
													tipoTasa: Ext.getCmp('idComboTasa').getValue(),
													baseOperacionDinamica: operacionDinamica,
													altaAutomaticaGarantia: automaticaGarantia,
													tipoCred: Ext.getCmp('idComboCredito').getValue(),
													tipoAmort: Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:  Ext.getCmp('idComboCartera').getValue(),
													tipo_plazo: TipoPlazo,
													periodicidad: Ext.getCmp('idComboPeriocidad').getValue(),
													tipoInteres:  Ext.getCmp('idComboInteres').getValue(),													
													tipoRenta: renta,
													informacion:'ArchivoCSV'
								},	
								callback: procesarDescargaArchivos
							});
						}
					},	
					{	
						xtype:	'button',
						id:		'btnImprimir',
						iconCls: 'icoPdf',
						text:		'Imprimir',
						width: 100,
						weight: 70,
						style: 'margin:0 auto;',
						align:'rigth',
						handler: function(boton, evento) {
							boton.disable();
							Ext.Ajax.request({
								url: '15basesoperce2Ext.data.jsp',
								params: {
												noIf: Ext.getCmp('idComboIF').getValue(),
													tipoTasa: Ext.getCmp('idComboTasa').getValue(),
													baseOperacionDinamica: operacionDinamica,
													altaAutomaticaGarantia: automaticaGarantia,
													tipoCred: Ext.getCmp('idComboCredito').getValue(),
													tipoAmort: Ext.getCmp('idComboAmortizacion').getValue(),
													tipoCartera:  Ext.getCmp('idComboCartera').getValue(),
													tipo_plazo: TipoPlazo,
													periodicidad: Ext.getCmp('idComboPeriocidad').getValue(),
													tipoInteres:  Ext.getCmp('idComboInteres').getValue(),													
													tipoRenta: renta,
													informacion:'ArchivoPDF'
								},	
								callback: procesarDescargaArchivos
							});
						}    
					}    								
					]
					},
					width: 930,
					frame: true
			});


//------ Componente Principal -------
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: '940',
		height: 'auto',
		frame: true,  
		items: [NE.util.getEspaciador(10),
				  fp,NE.util.getEspaciador(10), grid ]
	});


	CatalogoIF.load();
	CatalogoTasa.load();
	CatalogoCredito.load();
	CatalogoAmortizacion.load();
	CatalogoPeriocidad.load();

	procesarInicio();

});