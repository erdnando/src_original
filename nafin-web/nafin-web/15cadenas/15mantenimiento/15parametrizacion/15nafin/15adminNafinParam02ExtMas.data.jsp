<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.model.catalogos.*,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.nafin.descuento.ws.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar ="";


if (	informacion.equals("CatalogoBuscaAvanzada") ) {

	String num_pyme	= (request.getParameter("num_pyme")==null)?"":request.getParameter("num_pyme");
	String rfc_pyme	= (request.getParameter("rfc_pyme")==null)?"":request.getParameter("rfc_pyme");
	String nombre_pyme= (request.getParameter("nombre_pyme")==null)?"":request.getParameter("nombre_pyme");
	String ic_epo		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class); 

	Registros registros = afiliacion.getProveedores("",ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pyme,"","");

	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (registros.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(registros.getString("IC_PYME"));
		elementoCatalogo.setDescripcion(registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("CG_RAZON_SOCIAL"));
		coleccionElementos.add(elementoCatalogo);
	}	
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenNombrePyme")) {

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 

	JSONObject jsonObj			= new JSONObject();
	String numeroDeProveedor	= (request.getParameter("numeroDeProveedor")!=null)?request.getParameter("numeroDeProveedor"):"";

	Registros registros			= new Registros();

	String ic_pyme					= "";
	String txtCadenasPymes		= "";
	

		registros					= BeanParamDscto.getParametrosPymeNafinSeleccionIF(numeroDeProveedor);

		if(registros != null && registros.next()){
			ic_pyme					= (registros.getString("PYME")	== null)?"":registros.getString("PYME");
			txtCadenasPymes		= (registros.getString("NOMBRE")	== null)?"":registros.getString("NOMBRE");			
		}else{
			jsonObj.put("muestraMensaje", new Boolean(true));
			jsonObj.put("textoMensaje", 	"El nafin electrónico no corresponde a una<br>PyME afiliada a la EPO o no existe.");
		
	}

	jsonObj.put("ic_pyme",					ic_pyme									);
	jsonObj.put("txt_nafelec",				numeroDeProveedor						);
	jsonObj.put("txtCadenasPymes",		txtCadenasPymes						);	
	jsonObj.put("estadoSiguiente",		"RESPUESTA_OBTENER_NOMBRE_PYME"	);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("catalogoCuenta")){
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	Registros reg=BeanParamDscto.getCtasPyme(ic_pyme);
	List elementos=new ArrayList();
		JSONArray jsonArr = new JSONArray();
			while(reg.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(reg.getString("CLAVE"));
				elementoCatalogo.setDescripcion(reg.getString("DESCRIPCION"));
				elementos.add(elementoCatalogo);
			}		
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
			
		infoRegresar = "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
				
}else if(informacion.equals("catologoEpo")){

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 
	
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
			
	String operaFide_Pyme =   BeanParamDscto.getOperaFideicomisoPYME( ic_pyme );  // Fodea 19-2014
			
	if(!ic_pyme.equals("")){
		CatalogoEPOPorPyme catalogo = new CatalogoEPOPorPyme();
		catalogo.setCampoClave("ic_epo");
		catalogo.setClavePyme(ic_pyme);
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setOperaFide_Pyme(operaFide_Pyme);
		
		infoRegresar=catalogo.getJSONElementos();
	}
}  else if(informacion.equals("catologoEpoGrupo")){

		String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		
		CatalogoGrupoEpo catalogo = new CatalogoGrupoEpo();
		catalogo.setCampoClave("ic_grupo_epo");
		catalogo.setClavePyme(ic_pyme);
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setOrden("cg_descripcion");
		infoRegresar=catalogo.getJSONElementos();
		
}else if (informacion.equals("Consulta")) {
	String ic_cuenta	= (request.getParameter("cuenta")==null)?"":request.getParameter("cuenta");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String epos			=(request.getParameter("HicEpo")==null)?"":request.getParameter("HicEpo");
	String arrEpos[] = epos.split(",");
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 
	List reg=BeanParamDscto.getCuentasMasiva(arrEpos, ic_pyme, ic_cuenta);
	JSONObject jsonObj=new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", reg);
	jsonObj.put("total",reg.size()+"");
	infoRegresar=jsonObj.toString();
			
}else if (informacion.equals("Consulta2")) {
	String ic_cuenta	= (request.getParameter("cuenta")==null)?"":request.getParameter("cuenta");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String icGrupo	= (request.getParameter("HicEpoG")==null)?"":request.getParameter("HicEpoG");
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 
	List reg=BeanParamDscto.getCuentasMasivasGrupo( ic_pyme, ic_cuenta,icGrupo);
		
	JSONObject jsonObj=new JSONObject();
	
	 jsonObj.put("success", new Boolean(true));
	 jsonObj.put("registros", reg);
	jsonObj.put("total",reg.size()+"");
	 infoRegresar = jsonObj.toString();
}else if (informacion.equals("confirmarCuentas")) {
	String valores		= (request.getParameter("valores")==null)?"":request.getParameter("valores");
	String ic_epo		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String txt_nafelec = (request.getParameter("txt_nafelec")==null)?"":request.getParameter("txt_nafelec");
	List registrosActualizados =  new ArrayList();
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 
	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB",AutorizacionDescuento.class); 
	
	//Fodea 017 confirma cuentas si opera fiso
	String ic_moneda;
	StringTokenizer st = new StringTokenizer(valores,"|");
	VectorTokenizer vt = null;
	String iIF="",iEPO="";
	int iCuentaBancaria;
	String operaFide_Pyme =   BeanParamDscto. getOperaFideicomisoPYME( ic_pyme ); //Fodea 019-2014
	
	registrosActualizados = BeanParamDscto.confirmarCuentasMasiva( valores, ic_epo, ic_pyme, strLogin , strNombreUsuario, txt_nafelec );
				
	while(st.hasMoreTokens()){
				String registros = st.nextToken();
				vt = new VectorTokenizer(registros,",");
				Vector vecCampos = vt.getValuesVector();
	
				iIF 						= vecCampos.get(0).toString();
				iEPO 						= vecCampos.get(1).toString();
				iCuentaBancaria = Integer.parseInt(vecCampos.get(2).toString());
				ic_moneda=BeanAutDescuento.getMonedaPorCuenta(iCuentaBancaria+""); 								
						
				if(ic_moneda.equals("54")&& BeanAutDescuento.getOperaIFEPOFISO(iIF,iEPO) && operaFide_Pyme.equals("S")  ){
					CuentasPymeIFWS  cuentaConfirmar = new CuentasPymeIFWS();
					cuentaConfirmar.setRfcPyme(BeanAutDescuento.getRFCPyme(ic_pyme));
					cuentaConfirmar.setClaveEpo(iEPO+"");
					cuentaConfirmar.setClaveMoneda(ic_moneda);
					BeanAutDescuento.confirmacionCuentasIFWS ( iIF+"", strLogin,new CuentasPymeIFWS[]{ cuentaConfirmar});
				}
		} 	
		
	JSONObject jsonObj			= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", registrosActualizados);
	jsonObj.put("total",registrosActualizados.size()+"");	
	infoRegresar = jsonObj.toString();
	}

//System.out.println("infoRegresar ssssssssssssss "+infoRegresar);

%>
<%=infoRegresar%>