<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		com.netro.distribuidores.*, 
		com.netro.parametrosgrales.*,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 
	String infoRegresar = "";
	String informacion  = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String operaMesesEPO	  = (request.getParameter("operaMesesEPO")==null)?"":request.getParameter("operaMesesEPO");

	ParametrosDist  BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
 

	HashMap datos = new HashMap(); 
	JSONArray registros = new JSONArray();  
	JSONObject 	jsonObj	= new JSONObject();  

	if (informacion.equals("CatalogoEpo")){
		CatalogoNombreEpo cat = new CatalogoNombreEpo();
		infoRegresar = cat.getJSONElementos();	
	
	}else if (informacion.equals("CatalogoProducto")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_producto_nafin");
		cat.setCampoClave("ic_producto_nafin");
		cat.setCampoDescripcion("ic_nombre");
		
		List lis= new ArrayList ();
		lis.add("1");
		lis.add("4");
		
		cat.setValoresCondicionIn(lis);   
		cat.setOrden("ic_producto_nafin");		
		
		infoRegresar=cat.getJSONElementos();
	
	}else if (informacion.equals("CatalogoMoneda")){
		CatalogoSimple cat = new CatalogoSimple();
		
		cat.setTabla("comcat_moneda");
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre");
		
		List lis= new ArrayList ();
		lis.add("1");
		lis.add("54");
		
		cat.setValoresCondicionIn(lis);   
		cat.setOrden("ic_moneda");		
		
		infoRegresar=cat.getJSONElementos();
	
	}else if (informacion.equals("parametrizacionEPO")){
		
		String epo		  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
		operaMesesEPO	  =   BeanParametro.DesAutomaticoEpo(epo,"CS_MESES_SIN_INTERESES"); 
		
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));		
		jsonObj.put("operaMesesEPO", operaMesesEPO);	

		infoRegresar = jsonObj.toString();
		
	}else if (informacion.equals("ConsultaGrid") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF") ){
	
		String epo		  = (request.getParameter("comboEpo")==null)?"":request.getParameter("comboEpo");
		String moneda	  = (request.getParameter("comboMoneda")==null)?"":request.getParameter("comboMoneda");
		String producto  = (request.getParameter("comboProducto")==null)?"":request.getParameter("comboProducto");
		String cartera	  = (request.getParameter("comboCartera")==null)?"":request.getParameter("comboCartera");
		String plazo	  = (request.getParameter("tipoPlazo")==null)?"":request.getParameter("tipoPlazo");
		String comboTipoPago	  = (request.getParameter("comboTipoPago")==null)?"":request.getParameter("comboTipoPago");
		
		
		
		ConsCadenas paginador = new ConsCadenas();
	
		paginador.setEpo(epo);
		paginador.setMoneda(moneda);
		paginador.setProducto(producto);
		paginador.setPlazo(plazo);
		paginador.setCartera(cartera); 
		paginador.setTipoPago(comboTipoPago);
	
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	 
		if (informacion.equals("ConsultaGrid") ){ 
			try {
				Registros reg	=	queryHelper.doSearch();
				String consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
				//System.out.println("Registros: "+reg.getJSONData());
				jsonObj.put("success", new Boolean(true));
				jsonObj = JSONObject.fromObject(consulta);
			}catch(Exception e) {
				throw new AppException("Error en la paginación", e);
			}
		}
		else  if(informacion.equals("ArchivoCSV") ) {				
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}else  if(informacion.equals("ArchivoPDF") )  {		
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
	
		infoRegresar = jsonObj.toString();
	}
%>

<%= infoRegresar %>