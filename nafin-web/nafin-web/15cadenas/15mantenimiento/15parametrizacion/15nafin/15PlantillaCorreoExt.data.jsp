<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.*,
		java.io.*,		
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_operacion  = (request.getParameter("ic_operacion")!=null)?request.getParameter("ic_operacion"):"";
String txtPara  = (request.getParameter("txtPara")!=null)?request.getParameter("txtPara"):"";
String txtCC  = (request.getParameter("txtCC")!=null)?request.getParameter("txtCC"):"";
String txtAsunto  = (request.getParameter("txtAsunto")!=null)?request.getParameter("txtAsunto"):"";
String txtTexto  = (request.getParameter("txtTexto")!=null)?request.getParameter("txtTexto"):"";

String infoRegresar ="";
JSONObject jsonObj = new JSONObject();

Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
		
if (informacion.equals("catOperacionData")){
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_OPERACIONES");
	catalogo.setCampoDescripcion("CG_DESCRIPCION");
	catalogo.setTabla("COMCAT_OPERACIONES");		
	catalogo.setOrden("IC_OPERACIONES");	
	infoRegresar = catalogo.getJSONElementos();	

}else  if (informacion.equals("Comsultar")){

	HashMap	datos =  BeanAfiliacion.getPlantillaCorreos(ic_operacion);  

	jsonObj.put("success", new Boolean(true));		
	jsonObj.put("txtPara", datos.get("CG_DESTINATARIO"));
	jsonObj.put("txtCC", datos.get("CG_CCDESTINATARIO"));
	jsonObj.put("txtAsunto", datos.get("CG_ASUNTO"));
	jsonObj.put("txtTexto", datos.get("CG_CUERPO_CORREO"));
	jsonObj.put("operacion", datos.get("OP_DESCRIPCION"));
	
	infoRegresar = jsonObj.toString(); 
	
}else  if (informacion.equals("Guardar")){

	List parametros  = new ArrayList();
	parametros.add(ic_operacion);	
	parametros.add(txtPara);
	parametros.add(txtCC);
	parametros.add(txtAsunto);
	parametros.add(txtTexto);

	try{
		BeanAfiliacion.setPlantillaCorreos ( parametros  );
	} catch(Exception e) { //catch(NafinException lexError){
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error en la recuperación de datos", e);
	} finally {
		jsonObj.put("success", new Boolean(true));
	}
	
	infoRegresar = jsonObj.toString(); 
	
}	
%>	

<%= infoRegresar %>