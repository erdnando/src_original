<%@ page language="java" %>
<%@ page import="
   java.util.*,
   java.text.*, 
   java.math.*, 
   java.sql.*,
	java.io.*, 
   netropology.utilerias.*,
	com.netro.descuento.*,
   net.sf.json.JSONArray,net.sf.json.JSONObject,
   com.netro.seguridadbean.SeguException"
contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
  String informacion = request.getParameter("informacion")!=null?request.getParameter("informacion"):"";
   String strDiasMin = request.getParameter("strDiasMin")!=null?request.getParameter("strDiasMin"):"";
	String strDiasMax = request.getParameter("strDiasMax")!=null?request.getParameter("strDiasMax"):"";
	String strPorcientoActual = request.getParameter("strPorcientoActual")!=null?request.getParameter("strPorcientoActual"):"";
	String strPorcientoActualDL = request.getParameter("strPorcientoActualDL")!=null?request.getParameter("strPorcientoActualDL"):"";
	String strDiasEdoCuenta = request.getParameter("strDiasEdoCuenta")!=null?request.getParameter("strDiasEdoCuenta"):"";
	String strDiasVencimiento = request.getParameter("strDiasVencimiento")!=null?request.getParameter("strDiasVencimiento"):"";
	String strDiasOperados = request.getParameter("strDiasOperados")!=null?request.getParameter("strDiasOperados"):"";
	String strPlazoMaxFactorajeMN = request.getParameter("strPlazoMaxFactorajeMN")!=null?request.getParameter("strPlazoMaxFactorajeMN"):"";
	String strPlazoMaxFactorajeDL = request.getParameter("strPlazoMaxFactorajeDL")!=null?request.getParameter("strPlazoMaxFactorajeDL"):"";
	String strBaseOperGenerica = request.getParameter("strBaseOperGenerica")!=null?request.getParameter("strBaseOperGenerica"):"";
	String strEmailContingencia = request.getParameter("strEmailContingencia")!=null?request.getParameter("strEmailContingencia"):"";
	String strHoraReportesEspeciales = request.getParameter("strHoraReportesEspeciales")!=null?request.getParameter("strHoraReportesEspeciales"):"";
	String strEmailNotificacionesNafin = request.getParameter("strEmailNotificacionesNafin")!=null?request.getParameter("strEmailNotificacionesNafin"):"";
	String optOperTel = request.getParameter("optOperTel")!=null?request.getParameter("optOperTel"):"";

   String cvePerf ="", regresar ="", mensaje ="";
	int bandera =0;

	ParametrosDescuento parametrosBean = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 
	com.netro.seguridadbean.Seguridad _beanS = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class); 
	
	cvePerf = _beanS.getTipoAfiliadoXPerfil(strPerfil);
    
   if( informacion.equals("inicializaPantalla") ){   
     
      
      HashMap registros = parametrosBean.creaFormulario(); 
		
      JSONObject jsonObj = new JSONObject();
      jsonObj = JSONObject.fromObject(registros);
		jsonObj.put("success",     new Boolean(true));
      jsonObj.put("cvePerf",     cvePerf);
      jsonObj.put("ESTADO",      "creaFormulario");
		regresar=jsonObj.toString();
   }  else if( informacion.equals("enviarFormulario") ){
	
		List  parametros = new ArrayList();
		parametros.add(strDiasMin);
		parametros.add(strDiasMax);
		parametros.add(strPorcientoActual);
		parametros.add(strPorcientoActualDL);
		parametros.add(strDiasEdoCuenta);
		parametros.add(strDiasVencimiento);
		parametros.add(strDiasOperados);
		parametros.add(strPlazoMaxFactorajeMN);
		parametros.add(strPlazoMaxFactorajeDL);
		parametros.add(strBaseOperGenerica);
		parametros.add(strEmailContingencia);
		parametros.add(strHoraReportesEspeciales);
		parametros.add(strEmailNotificacionesNafin);
		parametros.add(optOperTel);
				
		mensaje  =  parametrosBean.actOtrosParametros(parametros);
		
      JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",     new Boolean(true));
      jsonObj.put("ESTADO",      bandera==0?"inicializaPantalla":"creaFormulario");
      jsonObj.put("BANDERA",     Integer.toString(bandera));
      jsonObj.put("MENSAJE",     mensaje);
		regresar=jsonObj.toString();
	
   }
   
   
%>

<%=regresar%>
