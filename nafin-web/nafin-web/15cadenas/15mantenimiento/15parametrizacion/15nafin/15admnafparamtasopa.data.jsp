<%@ page language="java" %>
<%@ page import="
   java.util.*,
   java.text.*, 
   java.math.*, 
   java.sql.*,
   javax.naming.*,
	com.netro.exception.*,
   netropology.utilerias.*,
   net.sf.json.JSONArray,net.sf.json.JSONObject,
   com.netro.toperativastcambio.*"
   contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../../../015secsession_extjs.jspf" %>
<%
	String informacion   = request.getParameter("informacion")==null?"":request.getParameter("informacion");
	String cboTasa   = request.getParameter("cboTasa")==null?"":request.getParameter("cboTasa");
	String txt_fecha_app   = request.getParameter("txt_fecha_app")==null?"":request.getParameter("txt_fecha_app");
	
	String txtCveTasa[]		= request.getParameterValues("txtCveTasa");
	String txtValorTasa[]	= request.getParameterValues("txtValorTasa");
	String chk_eliminar[]	= request.getParameterValues("chk_eliminar");

	SimpleDateFormat fecha_hoy = new SimpleDateFormat("dd/MM/yyyy");
	String fecha_Actual = fecha_hoy.format(new java.util.Date());
	
	String pantalla   = request.getParameter("pantalla")==null?"":request.getParameter("pantalla");
	
	String infoRegresar  ="" ,  cvePerf = "";
	JSONObject   jsonObj = new JSONObject(); 

	
	ITOperativasTCambio BeanTOperativas = ServiceLocator.getInstance().lookup("TOperativasTCambioEJB",ITOperativasTCambio.class);
 
if (informacion.equals("valoresIniciales") ) {
 
	//** Fodea 021-2015 (E);
	com.netro.seguridadbean.Seguridad seguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
		
	String idMenuP   = request.getParameter("idMenuP")==null?"15ADMNAFPARAMTASOPA":request.getParameter("idMenuP");
	String  [] permisosSolicitados = {"BTNCAPTURA", "BTNCONSULTA" };       
	
	List permisos =  seguridadBean.getPermisosPorMenu( iTipoPerfil, strPerfil, idMenuP, permisosSolicitados ); 
		
	jsonObj.put("permisos",  permisos);   
	
	//** Fodea 021-2015 (S);   

	 jsonObj.put("success",  new Boolean(true));
	 jsonObj.put("fecha_Actual", fecha_Actual);	
	 infoRegresar = jsonObj.toString();	
 
}else if( informacion.equals("catalogoTasa") ){

	List catIF = BeanTOperativas.getCatalogoTasa(pantalla);
	jsonObj.put("registros", catIF);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();

}else if( informacion.equals("ConsultarCaptura") ){

	List consulta = BeanTOperativas.getConsCapturaTasas(cboTasa , txt_fecha_app );

	jsonObj.put("registros", consulta);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();
	
}else if( informacion.equals("CapturaTasas") ){

	String strMensaje		= "La tasa ha sido actualizada";	
	try{
		for(int i=0;i<txtCveTasa.length;i++){
			if(!"".equals(txtValorTasa[i]))
				BeanTOperativas.vactualizarTasa(txt_fecha_app,txtCveTasa[i],txtValorTasa[i]);
			}
	} catch(NafinException neError){
		strMensaje = neError.getMsgError();		
	}

	
	jsonObj.put("strMensaje", strMensaje);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();
	
}else if( informacion.equals("EliminaTasas") ){
	String strMensaje		= "";
	try {
		for(int i=0;i<chk_eliminar.length;i++){
			StringTokenizer st = new StringTokenizer(chk_eliminar[i],"|");
			String bandera = st.nextToken();
			String ic_tasa = st.nextToken();
			if("S".equals(bandera)){
				BeanTOperativas.vborrarTasa(txt_fecha_app,ic_tasa);
				strMensaje = "Las tasas han sido borradas";
			}
		}
	} catch(NafinException neError){
		strMensaje = neError.getMsgError();		
	}
			
	jsonObj.put("strMensaje", strMensaje);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();

}else if( informacion.equals("ConsultaC") ){

	String txt_fecha_app_de   = request.getParameter("txt_fecha_app_de")==null?"":request.getParameter("txt_fecha_app_de");
	String txt_fecha_app_a   = request.getParameter("txt_fecha_app_a")==null?"":request.getParameter("txt_fecha_app_a");
	
	/*
		F021 - 2015
		En caso de seleccionar un rango de fechas con periodo en d�a mayor a 30 se mostrar� el siguiente 
		mensaje y NO SE PERMITIR� REALIZAR LA CONSULTA.
	*/
	if( Fecha.restaFechas(txt_fecha_app_de, txt_fecha_app_a) > 30 ){
		throw new AppException("El rango de fechas excede el per\u00EDodo v\u00E1lido de 30 d\u00EDas, favor de verificar.");
	}
	
	infoRegresar = BeanTOperativas.getConsTasas(cboTasa , txt_fecha_app_de, txt_fecha_app_a  );
	
}

%>
<%=infoRegresar%>


<%!




%>