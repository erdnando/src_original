	
Ext.onReady(function() {
var ahora=new Date();
var mes = (parseInt(ahora.getMonth()) + 1)<10?0+(parseInt(ahora.getMonth()) + 1).toString():(parseInt(ahora.getMonth()) + 1).toString();


TaskLocation = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
]);
//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT

	 //----------------------- CALCULA FECHA -------------------------------------
   function getDiferenciaEnDias(fechaIni, fechaFin){         
      var string1 			= "";
      var temp 				= "";
      var diferenciaEnDias	= "";

      string1 = fechaIni;         
      string = "" + string1;
      splitstring = string.split("/");
      var fechaInicial= new Date();
      fechaInicial.setDate(splitstring[0]);
      fechaInicial.setMonth(splitstring[1]-1);
      fechaInicial.setYear(splitstring[2]);
      string1 = fechaFin;

      string = "" + string1;
      splitstring = string.split("/");
      var fechaFinal= new Date();
      fechaFinal.setDate(splitstring[0]);
      fechaFinal.setMonth(splitstring[1]-1);
      fechaFinal.setYear(splitstring[2]);
      //Calcular el numero de milisegundos de un dia
      var milisegundosPorDia=1000*60*60*24;         
      //Calculate difference btw the two dates, and convert to days
      diferenciaEnDias = Math.ceil((fechaFinal.getTime()-fechaInicial.getTime())/(milisegundosPorDia));
      return diferenciaEnDias;
   }

	var procesarCatalogo=function(store, arrRegistros, opts){
      if(store.getTotalCount() > 0) {
         store.insert(0,new TaskLocation({ 
          clave: "TODAS", 
          descripcion: "Seleccione Todas", 
          loadMsg: ""})); 
         store.commitChanges(); 
      }	
	}
	var procesarConsultaData = function(store, arrRegistros, opts) {
	
		var fp = Ext.getCmp('forma');
			
		if (arrRegistros != null) {
			
			if (!gridGeneral.isVisible()) {
				gridGeneral.show();				
			}
			var btnGenerarArchivo = Ext.getCmp('btnGenerarCSV');
			var btnBajarArchivo = Ext.getCmp('btnBajarCSV');		

			var el = gridGeneral.getGridEl();
			
			if(store.getTotalCount() > 0) {
				btnGenerarArchivo.enable();
				btnBajarArchivo.hide();
				el.unmask();				
			} else {
				btnGenerarArchivo.disable();
				btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarCSV');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarCSV');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT

	var catalogoProyectoData = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admparamiftopext.data.jsp',
		baseParams: {
			informacion: 'CatalogoTasaOper'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
		load: procesarCatalogo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var consultaData =  new Ext.data.GroupingStore({
		root : 'registros',
		url : '15admparamiftopext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
         fields: [
            {name: 'CD_NOMBRE'},
            {name: 'IC_TASA'},
            {name: 'FN_VALOR'},
            {name: 'FECHA', type:'date', dateFormat:'d/m/Y'}
         ]
		}),
		groupField: 'FECHA', sortInfo:{field: 'FECHA', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
		
	});

//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
	var elementosFecha = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_Inicio',
					id: 'df_consultaMin',
					allowBlank: false,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_consultaMax',
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					formBind: true,
					value:new Date()
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_Fin',
					id: 'df_consultaMax',
					allowBlank: false,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_consultaMin',
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					value:new Date()
				}
			]
		}
	];

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'idTasa',
			id: 'cmbProyecto',
			hiddenName : 'idTasa',
			fieldLabel: 'Tasa',
			emptyText: 'Seleccione una Tasa',
			store: catalogoProyectoData,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			allowBlank : false,
			minChars : 1,
			editable:false,
			tpl : NE.util.templateMensajeCargaCombo,
         anchor: '89%'
		}
	];

	var fp = new Ext.form.FormPanel({
		xtype: 'form',
		id: 'forma',
		width: 400,
		title: '<div style="text-align:center">Tasas Operativas</div>',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 80,
		defaultType: 'textfield',
		monitorValid: true,
		items:[elementosFecha, elementosForma],
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var cmpForma = Ext.getCmp('forma');
					var df_consultaMin = Ext.getCmp('df_consultaMin');
					var df_consultaMax = Ext.getCmp('df_consultaMax');
					var Diferencia = Math.abs(getDiferenciaEnDias(Ext.util.Format.date(df_consultaMin.getValue(),'d/m/Y'), Ext.util.Format.date(df_consultaMax.getValue(),'d/m/Y')));
					
					if(Ext.getCmp('cmbProyecto').getValue()=='TODAS'){
						if(Diferencia>90){
							Ext.Msg.show({
								title    :  'Excedio el rango de fecha',
								msg      :  'El rango de fechas para la consulta de TODAS las Tasas Operativas corresponde a 90 d�as',
								icon     :  Ext.Msg.INFO,
								buttons  :  Ext.Msg.OK
							});
							 var gridGeneral =  Ext.getCmp('gridGeneral');
							 if(gridGeneral.isVisible){
								gridGeneral.hide();
								consultaData.removeAll();
							}
							return;
						}  
            	}
					
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};					
					consultaData.load({
							params: Ext.apply(paramSubmit,{
							operacion: 'Consultar'							
						})
					});
				} //fin handler
			}
		]
	});//FIN DE LA FORMA
	
	
	//CREA EL GRID PARA LA OPCION DE VER TASAS 
		var gridGeneral = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridGeneral',
      style: 'margin:0 auto',
		hidden: true,
		columns: [		
			{
				header: 'Fecha:',
				tooltip: 'Fecha:',
				dataIndex: 'FECHA',
				sortable: true,				
				align: 'left',
				width: 230,
				hidden: true,
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},			
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'CD_NOMBRE',//IC_TASA
				sortable: true,
				width: 300,
				resizable: true,
				hidden: false, 	
				align: 'left'	
			},						
			{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'FN_VALOR',
				sortable: true,
				width: 230,
				resizable: true,
				hidden: false,
				align: 'center'	
			}
			
		],	
		view: new Ext.grid.GroupingView({
            forceFit:true,   
            groupTextTpl: '{text}'
        }),
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 400,
		width: 550,
		title: '',
		frame: true,
			bbar: {
			xtype: 'toolbar',			
			items: [						
					{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSV',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						Ext.Ajax.request({
							url: '15admparamiftopext.data.jsp',
							params: Ext.apply(paramSubmit,{
								tipoArchivo:'CSV',
								informacion: 'GenerarArchivo'								
							}),	
							callback: procesarSuccessFailureGenerarArchivo
						});			
					}
				},
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				}
			]
		}
		
	});//FIN DEL GRID		

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
      style: 'margin:0 auto;',
		width: '949',
		items: [
			fp,
			NE.util.getEspaciador(5),
			gridGeneral
		]
	});
		//catalogoProyectoData.load();
});
