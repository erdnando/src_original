<%@ page language="java"%>
<%@ page import="  java.util.*,  com.netro.afiliacion.*,  com.netro.cadenas.*,  com.netro.descuento.*,  com.netro.exception.*,  com.netro.model.catalogos.*,  net.sf.json.JSONArray,net.sf.json.JSONObject" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<%  

/*** OBJETOS ***/
CQueryHelperRegExtJS queryHelperRegExtJS;
JSONObject jsonObj;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String informacion	= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion")==null)	?"":request.getParameter("operacion");
String ic_cta_bank	= (request.getParameter("ic_cta_bank")==null)?"":request.getParameter("ic_cta_bank");
String csAutoriza		= (request.getParameter("csAutoriza")==null)	?"":request.getParameter("csAutoriza");
String 	ic_epo   	= (request.getParameter("ic_epo") == null ) 	? "":request.getParameter("ic_epo");
String 	tConsulta   = (request.getParameter("tConsulta") == null ) 	? "":request.getParameter("tConsulta");
String resultado		= null;
String ic_if 			= iNoCliente;
/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/

/*** INICIO CATALOGO EPO ***/
if(informacion.equals("catalogoEpo")){	
	List resultCatEpo = new ArrayList();   
	CatalogoEPO catEpo = new CatalogoEPO();
	catEpo.setCampoClave("ic_epo");
	catEpo.setCampoDescripcion("cg_razon_social"); 
	catEpo.setClaveIf(ic_if);
	resultCatEpo	= catEpo.getListaElementosAutorizaCtasBenef();
	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", resultCatEpo);
	resultado=jsonObj.toString();
} /*** FIN EPO ***/

/*** INICIO CONSULTA ***/
else if(informacion.equals("Consulta")) {	
	
	if(operacion.equals("Accion")){
		String qrySentencia		= "";
		int existeCuenta = 0;

		try{
			CatalogoEPO catEpo = new CatalogoEPO();
			List resultCatEpo = new ArrayList();
			
			Afiliacion 		afiliacion 		= ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			
			csAutoriza = csAutoriza.equals("N") ? "S" : "N";
					
			catEpo.setCampoClave("ic_epo");
			catEpo.setCampoDescripcion("cg_razon_social");
			catEpo.setClaveIf(ic_if);
			resultCatEpo	= catEpo.getListaElementosAutorizaCtasBenef();
			request.setAttribute("resultCatEpo", resultCatEpo);
				
			String msgOperacion = afiliacion.setAutorizaCtasBancBenef(ic_cta_bank,ic_if,csAutoriza,"00000023");
	
			ArrayList lstCuentasBenef = new ArrayList();
			lstCuentasBenef = afiliacion.getCtasBenefAutorizaDesautoriza("1",ic_if);
			request.setAttribute("lstCuentasBenef", lstCuentasBenef);
			request.setAttribute("msgOperacion", msgOperacion);
			request.setAttribute("accion",csAutoriza);
		}catch(Exception e) {
			e.printStackTrace();
			throw new AppException("Unexpected Error");			
		}finally {	}
	}

	AfiliaCtasBenef paginador = new AfiliaCtasBenef();
	paginador.setOpcion(tConsulta);
	paginador.setIc_if(ic_if);
	paginador.setIc_epo(ic_epo);
	queryHelperRegExtJS	= new CQueryHelperRegExtJS( paginador ); 
	
	try {
		if(operacion.equals("Generar")){
			Registros reg	=	queryHelperRegExtJS.doSearch();
			resultado	= "{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
} /*** FIN DE CONSULTA ***/



%>
<%=  resultado%>
