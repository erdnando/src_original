Ext.onReady(function() {

var ic_if =  Ext.getDom('ic_if').value;

	//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
	var procesarCatalogoEpo = function(store, arrRegistros, opts) {
		//Ext.getCmp("sIc_epo").setValue((sIc_epo));	
	}
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15autorizactasbenefext.data.jsp',
		baseParams: {
			informacion: 'Consulta',
			tConsulta: '1'
		},
		fields: [
			{name: 'CVECUENTA'},
			{name: 'BENEFICIARIO'},
			{name: 'CUENTA'},
			{name: 'BANCO'},
			{name: 'SUCURSAL'},
			{name: 'AUTORIZACION'},
			{name: 'NOMBANCO'},
			{name: 'NOMMONEDA'},
			{name: 'NOMBENEF'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	var catalogoEpo = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '15autorizactasbenefext.data.jsp',
		listeners: {
			load: procesarCatalogoEpo,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: true
	});

//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		columns: [{
				header: 'Beneficiario', tooltip: 'Nombre Beneficiario',
				dataIndex: 'NOMBENEF',
				sortable: true,
				width: 230, resizable: true,
				align: 'left',
				renderer: function(value){
					return value!='' ? value : 'N/A';
				}
			},{
				header: 'Moneda', tooltip: 'Moneda',
				dataIndex: 'NOMMONEDA',
				sortable: true,	align: 'center',
				width: 130, resizable: true,
				align: 'left',
				renderer: function(value){
					return value!='' ? value : 'N/A';
				}
			},{
				header: 'Cuenta CLABE/Siwft', tooltip: 'Cuenta CLABE/Siwft',
				dataIndex: 'CUENTA',
				sortable: true,	align: 'center',
				width: 160, resizable: true,
				align: 'left',
				renderer: function(value){
					return value!='' ? value : 'N/A';
				}
			},{
				header: 'Nombre del Banco', tooltip: 'Nombre del Banco',
				dataIndex: 'NOMBANCO',
				sortable: true,	align: 'center',
				width: 190, resizable: true,
				align: 'left',
				renderer: function(value){
					return value!='' ? value : 'N/A';
				}
			},{
				header: 'N�mero y/o Nombre de Sucursal', tooltip: 'N�mero y/o Nombre de Sucursal',
				dataIndex: 'SUCURSAL',
				sortable: true,	align: 'center',
				width: 50, resizable: true,
				align: 'left',
				renderer: function(value){
					return value!='' ? value : 'N/A';
				}
			},{
				xtype: 'actioncolumn',
				header: 'Acci�n', tooltip: 'Acci�n',
				dataIndex: 'AUTORIZACION',
				width:100, resizable: true,
				align: 'left', 
				renderer: function(value){
					return value=='S' ? 'Desautorizar ' : 'Autorizar ';
				},
				items: [
					{ //para cambiar icono segun extension PDF/DOC 
						iconCls: 'irAplicacion',
						tooltip: 'Detalle Pago Anticipado',
						handler: function(grid, rowIdx, colIds){
							var reg = grid.getStore().getAt(rowIdx);
							Ext.Ajax.request({
								url: '15autorizactasbenefext.data.jsp',
								params: {
									informacion: 'Consulta',
									operacion: 'Accion',
									ic_cta_bank:reg.get('CVECUENTA'),
									csAutoriza:reg.get('AUTORIZACION')
									//ic_epo:var_ic_epo
								},
								callback: function(){
									consultaDataGrid.load({
										params: Ext.apply(paramSubmit,{
											operacion: 'Generar'					
										})
									});
								},
								success: function(){
									Ext.Msg.alert("Mensaje", "Operaci�n realizada con �xito");
								}
							});
						}	
					}
				]
			}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 900,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true
	};
	
	var elementosForma = [{
		xtype: 'compositefield',
		fieldLabel: 'Epo',
		combineErrors: false,
		msgTarget: 'side',
		width: 500,
		items:[{
			xtype : 'combo',  
			emptyText: 'Seleccione EPO...',  
			hiddenName : 'ic_epo',  
			name : 'ic_epo',
			id: 'sIc_epo',
			store: catalogoEpo,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			allowBlank : false,
			minChars : 1,
			width:300
		},{
			xtype:'displayfield',
			width:1
		}]		
	}];
	
		var fp = {
		xtype: 'form',
		id: 'forma',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 600,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		monitorValid:true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[elementosForma],
		buttons: [
			{
				formBind:true,
				text: 'Buscar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():"";				
					consultaDataGrid.load({
						params: Ext.apply(paramSubmit,{
						operacion: 'Generar',
						start:0,
						limit:15							
					})
				});
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15autorizactasbenefext.jsp';
				}
			}
		]
	};//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});
});
