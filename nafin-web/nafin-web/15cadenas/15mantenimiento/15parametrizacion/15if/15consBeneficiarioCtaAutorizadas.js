Ext.onReady(function() {


	function procesarArchivoSuccess(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			Ext.getCmp('btnArchivoCSV').enable();
			Ext.getCmp('btnArchivoCSV').setIconClass('icoXls');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	

	var procesarConsultaData = function(store, registros, opts){		
		fp.el.unmask();
		if (registros !== null) {
			Ext.getCmp('btnConsultar').enable();
			gridConsulta.el.unmask();
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
			
			var el = gridConsulta.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnArchivoCSV').enable();
				el.unmask();	
			}else  {
				gridConsulta.el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnArchivoCSV').disable();
			}
		}
	};
		

	var consulta = new Ext.data.JsonStore({
		root: 		'registros',
		id:			'consulta',
		url: 			'15consBeneficiarioCtaAutorizadas.data.jsp',
		baseParams: {	
			informacion:	'Consulta'	
		},
		fields: [
			{ name: 'NOM_BENEFICIARIO' },
			{ name: 'NOM_MONEDA' },
			{ name: 'BANCO_SERVICIO' },
			{ name: 'NO_CUENTA' },
			{ name: 'NO_CUENTA_CLABE' },
			{ name: 'NO_CUENTA_SPID' },
			{ name: 'SWIFT' },
			{ name: 'ABA' },
			{ name: 'SUCURSAL' },
			{ name: 'NOM_PROVEEDOR' },
			{ name: 'NOM_EPO' },
			{ name: 'RFC_PYME' },
			{ name: 'PLAZA' },
			{ name: 'AUTO_IF' },
			{ name: 'NUM_ELECTRONICO' },
			{ name: 'FECHA_ALTA' }
			],
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				beforeLoad: {fn: function(store, options){
					Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
				}},
				load: procesarConsultaData,
				exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
						NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);						
					}
				}
			}
	});


	var gridConsulta = new Ext.grid.GridPanel({		
		id: 'gridConsulta',
		width:      940,
		height:     400,		
		title:      'Consulta Cuentas Bancarias ',
		stripeRows: true,
		loadMask:   true,
		frame:      true,
		hidden:     true,
		header:     true,
		store:      consulta,
		columns: [
			{
				header: 		'Beneficiario',
				tooltip: 	'Beneficiario',
				dataIndex: 	'NOM_BENEFICIARIO',
				sortable: 	true,
				align:		'left',
				resizable: 	true,
				width: 		250		
			},
			{
				header: 		'Moneda',
				tooltip: 	'Moneda',
				dataIndex: 	'NOM_MONEDA',
				sortable: 	true,
				align:		'left',
				resizable: 	true,
				width: 		150		
			},
			{
				header: 		'Banco de Servicio',
				tooltip: 	'Banco de Servicio',
				dataIndex: 	'BANCO_SERVICIO',
				sortable: 	true,
				align:		'left',
				resizable: 	true,
				width: 		250		
			},
			{
				header: 		'No. De Cuenta',
				tooltip: 	'No. De Cuenta',
				dataIndex: 	'NO_CUENTA',
				sortable: 	true,
				align:		'center',
				resizable: 	true,
				width: 		120		
			},
			{
				header: 		'No. De Cuenta Clabe',
				tooltip: 	'No. De Cuenta Clabe',
				dataIndex: 	'NO_CUENTA_CLABE',
				sortable: 	true,
				align:		'center',
				resizable: 	true,
				width: 		120		
			},
			{
				header: 		'No. Cuenta Spid',
				tooltip: 	'No. Cuenta Spid',
				dataIndex: 	'NO_CUENTA_SPID',
				sortable: 	true,
				align:		'center',
				resizable: 	true,
				width: 		120		
			},
			{
				header: 		'SWIFT',
				tooltip: 	'SWIFT',
				dataIndex: 	'SWIFT',
				sortable: 	true,
				align:		'left',
				resizable: 	true,
				width: 		100		
			},
			{
				header: 		'ABA',
				tooltip: 	'ABA',
				dataIndex: 	'ABA',
				sortable: 	true,
				align:		'left',
				resizable: 	true,
				width: 		100		
			},
			{
				header: 		'Sucursal',
				tooltip: 	'Sucursal',
				dataIndex: 	'SUCURSAL',
				sortable: 	true,
				align:		'left',
				resizable: 	true,
				width: 		150		
			},
			{
				header: 		'Proveedor',
				tooltip: 	'Proveedor',
				dataIndex: 	'NOM_PROVEEDOR',
				sortable: 	true,
				align:		'left',
				resizable: 	true,
				width: 		250		
			},
			{
				header: 		'EPO',
				tooltip: 	'EPO',
				dataIndex: 	'NOM_EPO',
				sortable: 	true,
				align:		'left',
				resizable: 	true,
				width: 		250		
			},
			{
				header: 		'RFC',
				tooltip: 	'RFC',
				dataIndex: 	'RFC_PYME',
				sortable: 	true,
				align:		'center',
				resizable: 	true,
				width: 		250		
			},
			{
				header: 		'Plaza',
				tooltip: 	'Plaza',
				dataIndex: 	'PLAZA',
				sortable: 	true,
				align:		'left',
				resizable: 	true,
				width: 		200		
			},
			{
				header: 		'Autorizaci�n IF',
				tooltip: 	'Autorizaci�n IF',
				dataIndex: 	'AUTO_IF',
				sortable: 	true,
				align:		'center',
				resizable: 	true,
				width: 		100		
			},
			{
				header: 		'N�m. Nafin Electr�nico',
				tooltip: 	'N�m. Nafin Electr�nico',
				dataIndex: 	'NUM_ELECTRONICO',
				sortable: 	true,
				align:		'center',
				resizable: 	true,
				width: 		130		
			},
			{
				header: 		'Fecha de Alta',
				tooltip: 	'Fecha de Alta',
				dataIndex: 	'FECHA_ALTA',
				sortable: 	true,
				align:		'center',
				resizable: 	true,
				width: 		100		
			}
		],
		bbar: {
			xtype: 'paging',
			autoScroll:true,
			height: 30,
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consulta,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: ['->','-',
				{
					xtype:	'button',
					iconCls: 'icoXls',
					text: 	'Generar Archivo',
					id: 		'btnArchivoCSV',
					disabled:true,
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url: '15consBeneficiarioCtaAutorizadas.data.jsp',
								params:	Ext.apply(fp.getForm().getValues(),{
									informacion:'ArchivoCSV'
								}),
								callback: procesarArchivoSuccess
							});
						}
					}
				]
			}
	});

	var catBeneficiarioData = new Ext.data.JsonStore({
		id: 'catBeneficiarioData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consBeneficiarioCtaAutorizadas.data.jsp',
		baseParams: {
			informacion: 'CatBeneficiario'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	

	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPOData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15consBeneficiarioCtaAutorizadas.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15consBeneficiarioCtaAutorizadas.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0' 
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0' 
		},	
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 	width:  '100%', textAlign: 'center'	} 
		},
		{
			xtype: 		'panel',
			bodyStyle: 	'padding: 10px',
			layout: { type:'hbox', 	pack: 'center', align: 'middle' 	},
			items: [
				{
					xtype: 'button',
					text: 'Buscar',
					id: 'btnBuscar',
					iconCls: 'icoBuscar',
					width: 	75,
					handler: function(boton, evento) {
						var pymeComboCmp = Ext.getCmp('cbPyme1');
						pymeComboCmp.setValue('');
						pymeComboCmp.setDisabled(false);		
						
						storeBusqAvanzPyme.load({
							params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
								informacion: 'busquedaAvanzada'					
							})																	
						});				
					},
					style: {  	marginBottom:  '10px'  	} 
				}
			]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	

	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {  xtype: 	'textfield', msgTarget: 'side', 	anchor: '-20'  },
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");					
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = [];
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});
	
	var successAjaxFn = function(opts, success, response) { 
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);				
			Ext.getCmp('icPyme').setValue(jsonObj.icPyme);	
			
			if(jsonObj.txtNombre===''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};	
	

	var elementosForma = [
		{	xtype: 'textfield',	hidden: true, id: 'icPyme' },
		{
			xtype: 'compositefield',
			fieldLabel: 'N�m. NE Proveedor',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'N�mero Nafin Electr�nico',
					allowBlank: true,
					maxLength: 38,	
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){
												
							Ext.getCmp('txtNombre1').setValue('');	
							Ext.getCmp("icPyme").setValue('');
							if(Ext.getCmp('num_electronico1').getValue()!==''){					
							
								Ext.Ajax.request({  
									url: '15consBeneficiarioCtaAutorizadas.data.jsp',  
									method: 'POST',  
									callback: successAjaxFn,  
									params: {  
										informacion: 'pymeNombre' ,
										num_electronico: Ext.getCmp('num_electronico1').getValue()
									}  
								}); 
							}
						}
					}
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 280,
					msgTarget: 'side',
					disabled:true,
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
					xtype: 'button',
					text: 'B�squeda Avanzada...',
					iconCls: 'icoBuscar',
					id: 'btnBusqAv',
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winBusqAvan');
						if (ventana) {
							ventana.show();
						} else {
							new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
						}
					}
				}
			]
		},
		{
			xtype: 'combo',
			name: 'cboEPO',
			id: 'cboEPO1',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEPO',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						Ext.getCmp('cboBeneficiario1').reset();
						catBeneficiarioData.load({
							params:{				
								cboEPO:Ext.getCmp('cboEPO1').getValue()
							}
						});	
					}					
				}
			}
		},
		{
			xtype: 'combo',
			name: 'cboBeneficiario',
			id: 'cboBeneficiario1',
			fieldLabel: 'Beneficiario',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboBeneficiario',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catBeneficiarioData,
			tpl : NE.util.templateMensajeCargaCombo			
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Alta',
			combineErrors: false,
			msgTarget: 'side',				
			items: [
				{
					xtype: 'datefield',
					name: 'fechaAltaIni',
					id: 'fechaAltaIni',
					allowBlank: false,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecha: 'fechaAltaFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 25
				},
				{
					xtype: 'datefield',
					name: 'fechaAltaFin',
					id: 'fechaAltaFin',
					allowBlank: false,
					startDay: 1,
					width: 100,
					msgTarget: 'side',	
					vtype: 'rangofecha',
					campoInicioFecha: 'fechaAltaIni',
					margins: '0 20 0 0'  
				}
			]
		}
	];

	var fp = new Ext.form.FormPanel({		
		title: 'Criterios de Consulta',
		id: 'forma',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 750,
		labelWidth:		150,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[elementosForma],
		buttons: [
			{
				formBind:true,
				text: 'Buscar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
										
					if(Ext.getCmp('num_electronico1').getValue()===''  &&  Ext.getCmp('cboEPO1').getValue()===''  &&  Ext.getCmp('cboBeneficiario1').getValue()==='' 
					&&   ( Ext.getCmp('fechaAltaIni').getValue()===''  &&  Ext.getCmp('fechaAltaFin').getValue()==='')   ){	
							Ext.MessageBox.alert("Mensaje","Es necesario seleccionar un criterio para la consulta");
							return false;					
					}					
					
					if( Ext.getCmp('fechaAltaIni').getValue()!==''|| Ext.getCmp('fechaAltaFin').getValue()!==''){
						if(Ext.getCmp('fechaAltaIni').getValue()===''){
							Ext.getCmp('fechaAltaIni').markInvalid('Ingrese Fecha Inicial y/o Ingrese Fecha Final');
							Ext.getCmp('fechaAltaIni').focus();
							return false;
						}
						if(Ext.getCmp('fechaAltaFin').getValue()===''){
							Ext.getCmp('fechaAltaFin').markInvalid('Ingrese Fecha Inicial y/o Ingrese Fecha Final');
							Ext.getCmp('fechaAltaFin').focus();
							return false;
						}
					}
		
					fp.el.mask('cargando','x-mask');
				
					consulta.load({ 
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start: 0,
							limit: 15
						})
					});
				}
			},			
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15consBeneficiarioCtaAutorizadas.jsp';
				}
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			gridConsulta
		]
	});
	
	catalogoEPOData.load();
	
});