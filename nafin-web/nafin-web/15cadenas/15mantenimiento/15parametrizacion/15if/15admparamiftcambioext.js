Ext.onReady(function(){
//-----------------------Handlers------------------------------

var procesarConsulta = function(store,arrRegistros,opts){
			if(arrRegistros!=null){
					var el = grid.getGridEl();
					if(store.getTotalCount()>0){
							el.unmask();
					}else{
						el.mask('No se encontr� ning�n registro', 'x-mask');
			}
	}

}
//-----------------------Stores--------------------------------

var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catologoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admparamiftcambioext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '15admparamiftcambioext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'FECHA'},
				{name: 'VALOR'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsulta,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							
					}
		}
	}
});


//---------------------------Componentes--------------------
var dt= Date();
var fecha=Ext.util.Format.date(dt,'d/m/Y');

var grid = new Ext.grid.GridPanel({
				id: 'grid',
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				hidden: true,
				stripeRows: true,
				loadMask: true,
				height: 300,
				width: 340,
				frame: true,
				columns:[
				{
						
						header:'Fecha de Aplicaci�n',
						tooltip: 'Fecha de Aplicaci�n',
						sortable: true,
						dataIndex: 'FECHA',
						width: 160,
						align: 'center'
						},
						{
						
						header: 'Valor',
						tooltip: 'Valor',
						sortable: true,
						dataIndex: 'VALOR',
						width: 160,
						align: 'center'
						}
				]
		});



var elementosForma = [
		{
	  //MONEDA
	  xtype: 'combo',
	  fieldLabel: 'Moneda',
	  emptyText: 'Seleccionar',
	  displayField: 'descripcion',
	  valueField: 'clave',
	  triggerAction: 'all',
	  typeAhead: true,
	  minChars: 1,
		allowBlank: false,
	  store: catalogoMoneda,
	  tpl: NE.util.templateMensajeCargaCombo,
	  name:'cbMoneda',
	  id: 'cbMoneda',
	  mode: 'local',
	  hiddenName: 'HcbMoneda',
	  forceSelection: true
    },
		{
	 xtype: 'compositefield',
	 fieldLabel: 'Desde',
	 combineErrors: false,
	 msgTarget: 'side',
	 items: [
            {
			    // Fecha Inicio
			   xtype: 'datefield',
         name: 'dFechaRegIni',
         id: 'dFechaRegIni',
         vtype: 'rangofecha',
			value:fecha,
         campoFinFecha: 'dFechaRegFin',
				 allowBlank: false,
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 margins: '0 20 0 0'
            },
			   {
				 xtype: 'displayfield',
				 value: 'hasta',
				 width: 50
			   },
		     {
			    // Fecha Final
			   xtype: 'datefield',
         name: 'dFechaRegFin',
         id: 'dFechaRegFin',
         vtype: 'rangofecha',
			value:fecha,
         campoInicioFecha: 'dFechaRegIni',
				 allowBlank: false,
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 margins: '0 20 0 0'
         },
				 {
				 xtype: 'displayfield',
				 value: '(dd/mm/aaaa)',
				 width: 40
			    }
            ]
	 }
		

];



var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		labelWidth: 130,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						grid.show();
						grid.setTitle('<div align="center">\"'+Ext.getCmp('cbMoneda').lastSelectionText+'\"</div>');
						consulta.load({ params: Ext.apply(fp.getForm().getValues())});
					}
			 }
		]
	});



//----------------Principal------------------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [ 
		  fp,
	    NE.util.getEspaciador(30),
			grid
			 
		 ]
  });

catalogoMoneda.load();




});