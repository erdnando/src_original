Ext.onReady(function() {
   Ext.ns('App', 'App.user');	 
   
   var valorSiguiente = false;
//------------------- VARIABLES Y FUNCIONES GLOBALES ---------------------------
   ObjGeneral = {    
      conta             :  0,
      contadorMod       :  0,
      sIcEpo            :  '',
      arrMin            :  new Array(),
      arrMax            :  new Array(),
      arrDias           :  new Array(),
      arrCont           :  0,
      tempMin           :  '',
      selections        :  new Array(),
      modificados       :  '',
      recordsToSend     :  '',
      banderaAgregar    :  false,
      banderaEliminar   :  false,
      totalDetallesReg  :  true,
      recordsToSendArr  :  new Array(),
      arrPagoAnticipado :  new Array(),
      arrPagoAnticipadoS:  new Array(),
      arrAutCtaClabe    :  new Array(),
      arrAutorizacion   :  new Array(),
      arrPagoAnticipadoSinCheck  :  new Array(),
      arrPagoAnticipadoSinCheckS  :  new Array(),
      validaAutCtaClabe    :  false,
      validaPagoAnticipadoSinCheck :  false,
      validaPagoAnticipadoSinCheckS:  false,
      validaPagoAnticipado :  false,
      validaPagoAnticipadoS :  false,
      validaAutorizacion   :  false,   
      
      
      // Inicializa Variables Globales
      InicializaVariables  :  function(){ 
         this.arrCont      =  1;
         this.arrMin[0]    =  '';
         this.arrMax[0]    =  '';
         this.arrDias[0]   =  '';
      },
      
      // Limpia Variables Globales
      LimpiarVariables  : function(){ 
         this.conta              =  0;
         this.sIcEpo             =  '';
         this.tempMin            =  '';
         this.arrCont            =  1;
         this.selections         =  new Array();
         this.modificados        =  '';
         this.recordsToSend      =  '';
         this.banderaAgregar     =  false;
         this.banderaEliminar    =  false;
         this.totalDetallesReg   =  true;
         this.recordsToSendArr   =  new Array();
         this.arrMin[0]          =  '';
         this.arrMax[0]          =  '';
         this.arrDias[0]         =  '';
         this.arrPagoAnticipado  =  new Array();
         this.arrPagoAnticipadoS =  new Array();
         this.arrAutCtaClabe     =  new Array();
         this.arrAutorizacion    =  new Array();
         this.arrPagoAnticipadoSinCheck   =  new Array();
         this.arrPagoAnticipadoSinCheckS  =  new Array();
         this.validaAutCtaClabe     =  false;
         this.validaPagoAnticipado  =  false;
         this.validaPagoAnticipadoS =  false;
         this.validaAutorizacion    =  false;
         this.validaPagoAnticipadoSinCheck   =  false;
         this.validaPagoAnticipadoSinCheckS  =  false;
      },
      
      // Refresca Pantalla
      RedireccionarFn : function(){ 
         Ext.getBody().mask('Actualizando pantalla...');
         window.location = "15admparamifepoext.jsp";
      }
   }
   
   ObjGeneral.InicializaVariables(); // Inicializa Variables
//------------------------------------------------------------------------------
 	
	//consultaDataGridDetalles
	var procesarConsultaDataDetalle = function(store, arrRegistros, opts) {
		var grid       = Ext.getCmp('gridPagoAnt');
		var el         = grid.getGridEl();
		var jsonData   = store.reader.jsonData;	
		var windowD    = Ext.getCmp('windowPagoAnt');
		gridPagoAnt.setTitle('EPO: ' + cg_razon_social);
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		var btnAutorizar = Ext.getCmp('autorizar');
		var el = grid.getGridEl();
		
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}			
			if(store.getTotalCount() > 0) {
				btnAutorizar.enable();
				el.unmask();
			}else{
				btnAutorizar.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);			
		}
	}

//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '15admparamifepoext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'CG_BANCO'},
			{name: 'CG_NUM_CUENTA'},
			{name: 'CS_ACEPTACION'},
			{name: 'IC_EPO'},
			{name: 'CS_PAGO_ANTICIPADO'},
			{name: 'DET_CAPTURADOS'},
			{name: 'CG_CUENTA_CLABE'},
			{name: 'CS_AUTORIZACION_CLABE'},
			{name: 'CHECKBOX'},
			{name: 'TOTAL_DETALLES_REG'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	var consultaDataGridDetalles = new Ext.data.JsonStore({
		root : 'registros',
		url : '15admparamifepoext.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetPago',
			sIcEpo: ObjGeneral.sIcEpo
		},
		fields: [
			{name: 'MIN', mapping: 'MIN', type: 'string'},
			{name: 'MAX', mapping: 'MAX', type: 'string'},
			{name: 'DIAS', mapping: 'DIAS', type: 'string'},
			{name: 'PLUS', type: 'string'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaDataDetalle,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	/*** *****/
	var procesarGuardar = function() {
      ObjGeneral.contadorMod++; // Suma 1 getStore().totalLenght
      ObjGeneral.banderaAgregar = true;
		var gridConsulta = Ext.getCmp('gridPagoAnt');
		var store = gridConsulta.getStore();
		ObjGeneral.modificados = store.getModifiedRecords();
		//var columnModelGrid = gridConsulta.getColumnModel();
		var min;
		
		if(!Ext.isEmpty(ObjGeneral.modificados)){ 
			 var error  = false;
			 var cont   = 0;
			 var cont2  = 1;
			 var cont3  = 1;
          
			 Ext.each(ObjGeneral.modificados, function(record){cont++;});
			 Ext.each(ObjGeneral.modificados,function(record) { 
				var inx = store.indexOf(record);
				            
            if(Ext.isEmpty(record.data['MAX'])){ Ext.Msg.alert('Mensaje','Debe capturar un Plazo M�ximo'); error=true;  return; }
            else if(Ext.isEmpty(record.data['DIAS'])){ Ext.Msg.alert('Mensaje','Debe capturar los D�as'); error=true;  return; }
            
				if(record.data['MAX']<=record.data['MIN'] && (inx != -1 )){ Ext.Msg.alert("Mensaje","El plazo m�ximo debe de ser mayor al plazo m�nimo"); error=true; return;}
				else if(record.data['DIAS']>record.data['MAX'] && (inx != -1 )) { Ext.Msg.alert("mensaje","Los d�as no pueden ser mayores al plazo m�ximo"); error=true;  return;}
				else {
					if(cont==cont2){
						ObjGeneral.recordsToSendArr.push(Ext.apply({id:record.id},record.data));
					}
					cont2++;
					ObjGeneral.tempMin = record.data.MIN
					min = (record.data['MAX']+1);
				} 
				
			 });  
			if(!error){
				var reg = Ext.data.Record.create(['MAX', 'MIN','DIAS']);
				consultaDataGridDetalles.add(new reg({MAX:'',MIN:min,DIAS:''}));
            store.commitChanges();
			}else{
            ObjGeneral.contadorMod--;
         }
		}else{
         Ext.Msg.alert('Mesnaje...','Debe capturar un Plazo M�ximo'); return;
      }
	}
	
//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN

	/**** GRID PAGO ANTICIPADO *****/		
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Plazo', colspan: 2, align: 'center'},
				{header: '+', colspan: 1, align: 'center'},
				{header: 'Dias', colspan: 1, align: 'center'}
			]
		]
	});
	//var textField = new Ext.form.NumberField();
			var gridPagoAnt = new Ext.grid.EditorGridPanel({
				id: 'gridPagoAnt',
				store: consultaDataGridDetalles,
				title:'.',
				stripeRows: true,
				loadMask: true,
				deferRowRender: false,
				clicksToEdit:1,
				columns: [{
					header: 'Minimo', tooltip: 'Minimo',
					dataIndex: 'MIN',
					sortable: false, menuDisabled:true,
					width: 120, resizable: true, enableColumnMove:false,
					align: 'center'
				},{
					header: 'Maximo', tooltip: 'Maximo',
					dataIndex: 'MAX',enableColumnMove:false,
					sortable: false,	menuDisabled:true,
					width: 120, resizable: true, editor: {xtype:'numberfield', maxLength:3},
					align: 'center',
					renderer: function(value,metadata,registro){
						if(value==""){NE.util.colorCampoEdit(value,metadata,registro);}
						return value;
					}
				},{
					header: '',
					dataIndex: 'PLUS', menuDisabled:true,
					width: 30, resizable: true,enableColumnMove:false,
					align: 'center'
				},{
					header: 'Dias', tooltip: 'Dias',
					dataIndex: 'DIAS', menuDisabled:true,
					sortable: false, editor: {xtype:'numberfield', maxLength:3},
					width: 120, resizable: true, enableColumnMove:false,
					align: 'center',
					renderer: function(value,metadata,registro){
						if(value==""){NE.util.colorCampoEdit(value,metadata,registro);}
						return value;
					}
				}],
				height: 300,
				frame: false,
				plugins: [grupos],
				bbar: {
					xtype: 'toolbar',
					items: [
						'->',
						'-',
						{
							text: 'Agregar Registro',
							handler: procesarGuardar,
							iconCls: 'aceptar'
						},
						'-',
						{
							text: 'Eliminar Registro',
                     handler  :  function(){     
                         Ext.Msg.confirm('Confirmaci�n','�Est� seguro de eliminar el Registro?', function(btn){
                              if(btn==='yes' || btn=='ok'){
                                 ObjGeneral.contadorMod--; // Resta 1 getStore().totalLenght
                                 ObjGeneral.banderaEliminar = true;
                                 var gridConsulta = Ext.getCmp('gridPagoAnt');
                                 var store = gridConsulta.getStore();
                                 var min=[];
                                 var tot = 0;  
                                 var count = (store.getCount())-1;
                                 var i = 0;
                                 var strCount=1;
                                 
                                 var strMin=[]; var strMax=[]; var strDias=[];
                                 strMin[0]=''; strMax[0]=''; strDias[0]='';
                                 
                                 store.each(function(record) { 
                                    strMin[strCount] = record.data['MIN'];
                                    strMax[strCount] = record.data['MAX'];
                                    strDias[strCount] = record.data['DIAS'];
                                    strCount++;
                                 });                         
                                                                                                      
                                 ObjGeneral.arrMin[ObjGeneral.arrCont] = strMin[store.getCount()-1];
                                 ObjGeneral.arrMax[ObjGeneral.arrCont] = strMax[store.getCount()-1];
                                 ObjGeneral.arrDias[ObjGeneral.arrCont] = strDias[store.getCount()-1];
                                                                  
                                 ObjGeneral.arrCont++;
                                 
                                 store.removeAt(count);
                                 var count = store.getCount()-1;
                                 
                                 store.getAt(count).set('MAX','');
                                 store.getAt(count).set('DIAS','');
                                 store.commitChanges();
                              }
                        })
                     },
							iconCls: 'cancelar'
						},
						'-',
						{
							text: 'Cancelar',
							handler: function(){
								Ext.Msg.confirm("Mensaje","Se perder�n todos los cambios efectuados, Esta seguro?", function(action){
									if(action=='yes'){
                              ObjGeneral.LimpiarVariables(); consultaDataGrid.load();
                              windowPagoAnt.hide();
                              //window.location = '15admparamifepoext.jsp';
									}
								},this);
								return false;
							},
							iconCls: 'icoRechazar'
						},
						'-',
						{
							text: 'Terminar',
							handler:function(){
                        var accion = "Inserta";
                        
                        if(ObjGeneral.banderaEliminar==true){
                           accion = "Elimina";
                           
                           for(var x=1; x<ObjGeneral.arrCont; x++){
                              Ext.Ajax.request({
                                 url      :  '15admparamifepoext.data.jsp',
                                 method   :  'POST',
                                 params   :  {min:ObjGeneral.arrMin[x],max:ObjGeneral.arrMax[x],dias:ObjGeneral.arrDias[x],operacion:'ModificaDetPago',accion:'Elimina',sIcEpo:ObjGeneral.sIcEpo,informacion:'ConsultaDetPago'},
                                 failure  :  function() {alert('Error');}
                              });
                           }
                           ObjGeneral.LimpiarVariables();
                           consultaDataGrid.rejectChanges();
                        }
                        if(ObjGeneral.banderaAgregar==true){ 
                           accion = "Inserta";
                           var gridConsulta = Ext.getCmp('gridPagoAnt');
                           var store = gridConsulta.getStore();
                           ObjGeneral.modificados = store.getModifiedRecords();   
                           
                           Ext.Ajax.request({
                              url      :  '15admparamifepoext.data.jsp',
                              method   :  'POST',
                              params   :  {informacion: 'pruebaModificados', modificados : ObjGeneral.modificados},
                              failure  :  function() {alert('Error');}
                           });
                        }
                        
								ObjGeneral.recordsToSendArr = Ext.encode(ObjGeneral.recordsToSendArr); 
								consultaDataGridDetalles.load({
									params :{operacion:'ModificaDetPago',accion:accion,sIcEpo:ObjGeneral.sIcEpo,informacion:'ConsultaDetPago',records:ObjGeneral.recordsToSendArr}
								});
								ObjGeneral.LimpiarVariables();
                        consultaDataGrid.rejectChanges();
								windowPagoAnt.hide();
								consultaDataGrid.load();
							},
							iconCls: 'correcto'
						}
		
					]
				},
            listeners : {
               beforeedit: function(e){
                  //e.grid.getStore().getTotalCount()
                  if((e.row + 1) < e.grid.getStore().totalLength+ObjGeneral.contadorMod){
                     return false;
                  }
               }
            },
            border: false,  
            stripeRows: true
			});
			var windowPagoAnt = new Ext.Window({
				  title: '<div style="text-align:center">Detalle de Pago Anticipado</div>',
				  width: 420,
				  height:300,
				  minWidth: 300,
				  minHeight: 200,
				  constrain:true,
				  minimizable: false,  
				  maximizable: false, 
				  closeAction: 	'hide',
				  layout: 'fit',
				  modal:true,
				  bodyStyle: {
					padding:'5px'
				  },
				  buttonAlign:'center',
				  items: [gridPagoAnt]
			 });
	/**** FIN GRID PAGO ANTICIPADO *****/
	function changeColorText(value, record1, record2){
		var cad = value;
		if(record1=="N" && record2=="N"){
			cad = '<span style="color: red;">'+cad+'</span>';
		}
		return cad; 
	}
	
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: false,
		title: 'Cuentas Bancarias EPOs',
		viewConfig: {forceFit: true},
		columns: [{
				header: 'Clave EPO', tooltip: 'Clave EPO',
				dataIndex: 'IC_EPO',
				sortable: false,
				width: 70, resizable: true,
				align: 'center',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
					return changeColorText(value,record.get('CS_ACEPTACION'),record.get('CS_AUTORIZACION_CLABE'));
            }
			},{
				header: 'EPO', tooltip: 'EPO',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: false,
				width: 180, resizable: true,
				align: 'left',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
					return changeColorText(value,record.get('CS_ACEPTACION'),record.get('CS_AUTORIZACION_CLABE'));
            }
			},{
				header: 'Banco', tooltip: 'Banco',
				dataIndex: 'CG_BANCO',
				sortable: false,
				width: 90, resizable: true,
				align: 'left',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
					return changeColorText(value,record.get('CS_ACEPTACION'),record.get('CS_AUTORIZACION_CLABE'));
            }
			},{
				header: 'Datos Cuenta', tooltip: 'Datos Cuenta',
				dataIndex: 'CG_NUM_CUENTA',
				sortable: false,
				width: 90, resizable: true,
				align: 'left',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
					return changeColorText(value,record.get('CS_ACEPTACION'),record.get('CS_AUTORIZACION_CLABE'));
            }
			},{
				//dataIndex: 'CHECKBOX',
				sortable: false,
				width: 70, resizable: true,
				align: 'center',
				header: 'Autorizacion', tooltip: 'Autorizacion',
				dataIndex: 'CS_ACEPTACION',
				sortable: false,
				width: 70, resizable: true,
				align: 'center',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
					if(value=='N'){
						op = '<input type="checkbox" id="chk'+ rowIdx +'" name="seleccionado" value="' + value + '"/>';
					}else{
						op = '<input type="hidden" id="chk'+ rowIdx +'">';
						op += changeColorText(value,record.get('CS_ACEPTACION'),record.get('CS_AUTORIZACION_CLABE'));
					}
					return op;
            }
			},{
				header: 'Cuenta Clabe', tooltip: 'Cuenta Clabe',
				dataIndex: 'CG_CUENTA_CLABE',
				sortable: false,
				width: 180, resizable: true,
				align: 'left',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
					return changeColorText(value,record.get('CS_ACEPTACION'),record.get('CS_AUTORIZACION_CLABE'));
            }
			},{
				header: 'Aut de Cta Clabe', tooltip: 'Autorizacion de Cuenta Clabe',
				dataIndex: 'CS_AUTORIZACION_CLABE',
				sortable: false,
				width: 80, resizable: true,
				align: 'center',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
					if(value=='N'){
						op = '<input type="checkbox" id="chkCtaClabe'+ rowIdx +'" name="seleccionado"/>';
					}else{
						op = '<input type="hidden" id="chkCtaClabe'+ rowIdx +'">';
						op += changeColorText(value,record.get('CS_ACEPTACION'),record.get('CS_AUTORIZACION_CLABE'));
					}
					return op;
				}
			},{
				//xtype: 'checkcolumn',
				header: 'Pago Anticipado', 
				tooltip: 'Pago Anticipado',
				dataIndex: 'CHECKBOX',
				sortable: false,
				width: 70, resizable: true,
				align: 'center',
				renderer:	function(value, metaData, record, rowIdx, colIdx, store, view){
					var checked = value!="S"?'':'checked';
					return '<input type="checkbox" id="chkPagoAnticipado' + rowIdx + '" '+checked+' />';	
				}
			},{
				header: 'Det Capturados', 
				tooltip: 'Detalles Capturados',
				dataIndex: 'DET_CAPTURADOS',
				sortable: false,
				width: 65, resizable: true,
				align: 'center'
			},{
				xtype: 'actioncolumn',
				header: 'Detalle Pago Anticipado', tooltip: 'Detalle Pago Anticipado',
				dataIndex: '',
				width: 50, resizable: true,
				align: 'center', 
				items: [{ //para cambiar icono segun extension PDF/DOC 
               iconCls: 'icoTxt',
               tooltip: 'Detalle Pago Anticipado',
               handler: function(grid, rowIdx, colIds){
                  var reg = grid.getStore().getAt(rowIdx);
                  ObjGeneral.sIcEpo = reg.get('IC_EPO');
                  cg_razon_social = reg.get('CG_RAZON_SOCIAL');
                  consultaDataGridDetalles.load({
                     params: {
                        informacion: 'ConsultaDetPago',
                        sIcEpo:reg.get('IC_EPO')
                     }
                  });
                  
						var gridConsulta = Ext.getCmp('grid');
						var store = gridConsulta.getStore();
                   var jsonData = store.data.items;
                   var chkPagoAnticipado 	= Ext.getDom("chkPagoAnticipado"+rowIdx.toString());
                   
						if(chkPagoAnticipado.checked){
                     ObjGeneral.contadorMod=0;
							windowPagoAnt.show(); 
						}else{
							Ext.Msg.alert("Mensaje","Debe seleccionar Pago Anticipado de la EPO correspondiente para capturar Detalles");
						}
               }	
            }]
			}],
         stripeRows: true,
         loadMask: true,
         deferRowRender: false,
         height: 533,
         width: 940,
         colunmWidth: true,
         frame: true,
         style: 'margin:0 auto;',// para centrar el grid
         collapsible: true,
         bbar: {
         xtype: 'toolbar',
			items: [
				'->',
				{
					xtype: 'button',
					text: 'Autorizar',
					iconCls: 'icoAceptar',
					id: 'autorizar',
					disabled : false,
					handler: function(boton, evento) {
						ObjGeneral.LimpiarVariables();
						var gridConsulta = Ext.getCmp('grid');
						var store = gridConsulta.getStore();
						ObjGeneral.modificados = store.getModifiedRecords();	
						var chkAut = false;
						var chkAction = false;
                  var chkActionCtaClabe = false;
                  var pagoAnticipadoChk = false;
                  var arrRegresaPagoAnticipado = [];
                  var i = 1;
                  
						if(ObjGeneral.conta==0){

                     var jsonData = store.data.items;
                     var arregloEpo = [];
                     Ext.each(jsonData, function(item,inx,arrItem){

                        //--------- PAGO ANTICIPADO ----------
                        if(item.data.CHECKBOX != 'S'){
									var chkPagoAnticipado 	= Ext.getDom("chkPagoAnticipado"+inx.toString());
                           
                           if(chkPagoAnticipado.checked){
                           	ObjGeneral.arrPagoAnticipado.push(item.data.IC_EPO);
                           	ObjGeneral.validaPagoAnticipado = true;
                           	if(item.data.TOTAL_DETALLES_REG==0){ ObjGeneral.totalDetallesReg=false; }
                           }
                        } else {
									var chkPagoAnticipado 	= Ext.getDom("chkPagoAnticipado"+inx.toString());
                           
                           if(!chkPagoAnticipado.checked){
                           	ObjGeneral.arrPagoAnticipadoS.push(item.data.IC_EPO);
                           	ObjGeneral.validaPagoAnticipadoS = true;
                           	if(item.data.TOTAL_DETALLES_REG==0){ ObjGeneral.totalDetallesRegS=true; }
                           }
                        }

                        //--------- AUTORIZACION ----------
                        if(item.data.CS_ACEPTACION != 'S'){
									var autorizacionChk 	= Ext.getDom("chk"+inx.toString());
                           
                           if(autorizacionChk.checked){
                              ObjGeneral.arrAutorizacion.push(item.data.IC_EPO);
                              ObjGeneral.validaAutorizacion = true;
                           }
                        }
                        
                        //----- AUTORIZACION CUENTA CLABE ------
                        if(item.data.CS_AUTORIZACION_CLABE != 'S'){
									var autCtaClabeChk = Ext.getDom("chkCtaClabe"+inx.toString());
                                 
                           if(autCtaClabeChk.checked){
                              ObjGeneral.arrAutCtaClabe.push(item.data.IC_EPO);
                              ObjGeneral.validaAutCtaClabe = true;
                           }
                        }
                     });
						}
                 // return false;
                  if(ObjGeneral.validaPagoAnticipado==false && ObjGeneral.validaPagoAnticipadoS==false && ObjGeneral.validaAutCtaClabe==false && ObjGeneral.validaAutorizacion==false && ObjGeneral.validaPagoAnticipadoSinCheck==false && ObjGeneral.validaPagoAnticipadoSinCheckS==false){
                     Ext.Msg.show({
                        title    :  'Autorizar',
                        msg      :  'No ha seleccionado cuentas para autorizar',
                        buttons  :  Ext.Msg.OK,
                        icon     :  Ext.Msg.INFO
                     });
                     return ;
                  }
                  
                  if((ObjGeneral.totalDetallesReg==false )){
                     Ext.Msg.alert("Mensaje","No se puede autorizar pago anticipado porque una o mas EPOs seleccionadas no tienen detalles capturados",  function() { ObjGeneral.LimpiarVariables(); consultaDataGrid.load(); });
                     consultaDataGrid.rejectChanges();
                     return;
                  }
                  
						if((ObjGeneral.validaPagoAnticipadoS || ObjGeneral.validaPagoAnticipado || ObjGeneral.validaAutCtaClabe || ObjGeneral.validaAutorizacion || ObjGeneral.validaPagoAnticipadoSinCheck || ObjGeneral.validaPagoAnticipadoSinCheckS)){
                  
                     if(ObjGeneral.totalDetallesReg==false && ObjGeneral.validaPagoAnticipado==false && ObjGeneral.validaPagoAnticipadoS==false && ObjGeneral.validaAutCtaClabe==false && ObjGeneral.validaAutorizacion==false && validaPagoAnticipadoSinCheck==false && validaPagoAnticipadoSinCheckS==false){
                        Ext.Msg.alert("Mensaje","No se puede autorizar pago anticipado porque una o mas EPOs seleccionadas no tienen detalles capturados", function() { ObjGeneral.LimpiarVariables(); consultaDataGrid.load();});
                        consultaDataGrid.rejectChanges();
                        return;
                     }
                     if(ObjGeneral.validaPagoAnticipadoS || ObjGeneral.validaPagoAnticipado || ObjGeneral.validaAutCtaClabe || ObjGeneral.validaAutorizacion || ObjGeneral.validaPagoAnticipadoSinCheck  || ObjGeneral.validaPagoAnticipadoSinCheckS){
								boton.disable();
                        
                         Ext.Ajax.request({
                           url      :  '15admparamifepoext.data.jsp',
                           method   :  'POST',
                           params   :  {
                              operacion:'Autorizar',
										informacion: 'Consulta',
                              pagoAnticipadoSinCheck 	: ObjGeneral.arrPagoAnticipadoSinCheck,
                              pagoAnticipadoSinCheckS	: ObjGeneral.arrPagoAnticipadoSinCheckS,
                              pagoAnticipado 			: ObjGeneral.arrPagoAnticipado,
                              autorizacion   			:  ObjGeneral.arrAutorizacion,
                              autorizacionCtaClabe  	:  ObjGeneral.arrAutCtaClabe,
                              
                              validaPagoAnticipado 	:  ObjGeneral.validaPagoAnticipado,
                              validaPagoAnticipadoS 	:  ObjGeneral.validaPagoAnticipadoS,
                              validaAutCtaClabe 		:  ObjGeneral.validaAutCtaClabe,
                              validaAutorizacion   	:  ObjGeneral.validaAutorizacion,
                              validaPagoAnticipadoSinCheck  :  ObjGeneral.validaPagoAnticipadoSinCheck,
                              validaPagoAnticipadoSinCheckS  :  ObjGeneral.validaPagoAnticipadoSinCheckS
                           },
                           success  :  function()  { 
                              consultaDataGrid.load();
                              
                              if(ObjGeneral.validaAutorizacion){
												Ext.Msg.show({
                                       title    :  "Autorizaci�n de cuenta",
                                       msg      :  'La(s) cuenta(s) ha(n) sido autorizada(s)',
                                       icon     :  Ext.Msg.INFO,
                                       buttons  :  Ext.Msg.OK,
                                       fn       :  function(){
                                          if(ObjGeneral.validaAutCtaClabe){
                                             Ext.Msg.show({
                                                title    :  "Autorizaci�n de cuenta",
                                                msg      :  'La(s) cuenta(s) ha(n) sido autorizada(s) o desautorizada(s) para pago anticipado',
                                                icon     :  Ext.Msg.INFO,
                                                buttons  :  Ext.Msg.OK
                                             });
                                          }
                                       }
                                    });
											}
                                 
                                 if(ObjGeneral.validaPagoAnticipado || ObjGeneral.validaPagoAnticipadoSinCheck){
                                     Ext.Msg.show({
                                          title    :  "Autorizaci�n de cuenta",
                                          msg      :  'La(s) cuenta(s) ha(n) sido autorizada(s) o desautorizada(s) para pago anticipado',
                                          icon     :  Ext.Msg.INFO,
                                          buttons  :  Ext.Msg.OK
                                       });
                                 }
                                 
                                 if(ObjGeneral.validaAutCtaClabe){
                                     Ext.Msg.show({
                                       title    :  "Autorizaci�n de cuenta",
                                       msg      :  'La(s) cuenta(s) ha(n) sido autorizada(s)',
                                       icon     :  Ext.Msg.INFO,
                                       buttons  :  Ext.Msg.OK,
                                       fn       :  function(){
                                          Ext.Msg.show({
                                             title    :  "Autorizaci�n de cuenta",
                                             msg      :  'La(s) cuenta(s) ha(n) sido autorizada(s) o desautorizada(s) para pago anticipado',
                                             icon     :  Ext.Msg.INFO,
                                             buttons  :  Ext.Msg.OK
                                          });
                                    }
                                    });
                                 }
                              
                           },
                           failure  :  function() {alert('Error');}
                        });
                        
								boton.enable();
							}else{
								Ext.Msg.alert("Mensaje","Debe de seleccionar Pago Anticipado de la \nEPO correspondiente para capturar Detalles",  function() { ObjGeneral.LimpiarVariables(); consultaDataGrid.load(); });
                        consultaDataGrid.rejectChanges();
							}
						}else{
							Ext.Msg.alert("Mensaje","No se puede autorizar pago anticipado porque una o mas EPOs seleccionadas no tienen detalles capturados",  function() { ObjGeneral.LimpiarVariables(); consultaDataGrid.load(); });
                     consultaDataGrid.rejectChanges();
                     return;
                  }
					}
				},
				{
					xtype:   'button',
					text:    'Bajar Archivo',
					id:      'btnBajarArchivo',
					hidden:  true
				}
			]
		}
	};

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			NE.util.getEspaciador(10),
			grid
		]
	});
	consultaDataGrid.load();
});


