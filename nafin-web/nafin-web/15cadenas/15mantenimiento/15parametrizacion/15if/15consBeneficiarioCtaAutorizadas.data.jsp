<%@ page contentType="application/json;charset=UTF-8" import="java.util.*,
		java.text.SimpleDateFormat,		
		com.netro.model.catalogos.*,
		com.netro.descuento.*,
		com.netro.cadenas.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String numElectronico = (request.getParameter("num_electronico") == null)?"":request.getParameter("num_electronico");


String icPyme = (request.getParameter("icPyme") == null)?"":request.getParameter("icPyme");
String cboEPO = (request.getParameter("cboEPO") == null)?"":request.getParameter("cboEPO");
String cboBeneficiario = (request.getParameter("cboBeneficiario") == null)?"":request.getParameter("cboBeneficiario");
String fechaAltaIni = (request.getParameter("fechaAltaIni") == null)?"":request.getParameter("fechaAltaIni");
String fechaAltaFin = (request.getParameter("fechaAltaFin") == null)?"":request.getParameter("fechaAltaFin");
String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	
String infoRegresar	=	"";
String txtNombre ="";

JSONObject jsonObj = new JSONObject();

ParametrosDescuento parametrosDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
CargaDocumento cargaDocumento = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);


System.out.println ("***************************************************************************");
System.out.println ("************informacion >>>>>>>>>>>>>>>>>>>>> "+informacion);
System.out.println ("************operacion >>>>>>>>>>>>>>>>>>>>> "+operacion);
System.out.println ("***************************************************************************");

if(informacion.equals("pymeNombre") ) {
	
	List datosPymes =  parametrosDescuento.datosPymes(numElectronico, "" ); //para obtener 
	if(datosPymes.size()>0){
		icPyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("icPyme", icPyme);
	jsonObj.put("txtNombre", txtNombre);	
	infoRegresar = jsonObj.toString();		

}else if(informacion.equals("busquedaAvanzada") ) {

	String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
		
	CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");	
	if(!rfcPyme.equals(""))
	cat.setRfc_pyme(rfcPyme);	
	if(!nombrePyme.equals(""))
	cat.setNombre_pyme(nombrePyme);	
	cat.setNafin_electronico("");
	cat.setPantalla("SoliAfi");
	cat.setOrden("p.cg_razon_social");		
	infoRegresar = cat.getJSONElementos();

}else if(informacion.equals("CatalogoEPO") ) {


	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");	
	cat.setClaveIf(iNoCliente);  
	List listaElementos = cat.getListaElementosInfoDoctos();	
	infoRegresar      = cat.getJSONElementos(listaElementos);
	
	

}else if(informacion.equals("CatBeneficiario") ) {

List resultCatIF = new ArrayList();
	CatalogoIF catalogo = new CatalogoIF();	
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");		
	catalogo.setClaveEpo(cboEPO);
	List listaElementos = catalogo.getListaElementosBeneficiario();	
	infoRegresar      = catalogo.getJSONElementos(listaElementos);
	
		
}else if (informacion.equals("Consulta") || informacion.equals("ArchivoCSV")){

	int start = 0;
	int limit = 0;
	
	ConsBeneficiarioCtaAutorizadas paginador = new ConsBeneficiarioCtaAutorizadas();
	paginador.setIcPyme(icPyme);
	paginador.setCboEPO(cboEPO);
	paginador.setCboBeneficiario(cboBeneficiario);
	paginador.setFechaAltaIni(fechaAltaIni);
	paginador.setFechaAltaFin(fechaAltaFin);
	

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
		if (informacion.equals("Consulta")){ // Datos para la Consulta con Paginacion
			
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));				
				
			} catch(Exception e){
				throw new AppException("Error en los parametros recibidos. ", e);
			}
			try{
				if (operacion.equals("Generar")) { //Nueva consulta
					queryHelper.executePKQuery(request);
				}
				infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
				
			} catch(Exception e){
				throw new AppException("Error en la paginacion. ", e);
			}
		} else if(informacion.equals("ArchivoCSV")){
			String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
		
}	
%>
<%=infoRegresar%>