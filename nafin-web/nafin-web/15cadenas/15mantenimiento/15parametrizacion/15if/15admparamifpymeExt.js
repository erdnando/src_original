Ext.onReady(function() {

	
	var num_naf_elect =  Ext.getDom('num_naf_elect').value;
	var chkCtaBanc = [];
		
	//  Ventana Emergente  de cuentas Pymes Pendientes de Autorizar  
	var ventanaCuentasPendientes = function() {
	
		consultaCuentas.load();	
		new Ext.Window({
			title: 'EXISTEN CAMBIOS DE CUENTAS BANCARIAS PYME PENDIENTES DE AUTORIZACION',	
			width: 520,	height: 300,	
			id: 'verCuentas',	
			closeAction: 'hide',
			autoScroll: true,	
			resizable:false,	
			modal:true,
			items: [	gridCuentas ]
			}).show();	
	}

	var procesarConsultaCuentasData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridCuentas = Ext.getCmp('gridCuentas');
			var el = gridCuentas.getGridEl();
			var store = consultaCuentas;
			if(store.getTotalCount()>0){
				el.unmask();
			}else {
				el.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}
	
	var consultaCuentas = new Ext.data.JsonStore({
		root: 'registros',
		url: '15admparamifpyme.data.jsp',
		baseParams: {
			informacion: 'consultaCuentas'
		},
		fields: [
			{name: 'RFC'},
			{name: 'NOMBRE_PYME'},
			{name: 'NOMBRE_EPO'}					
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {	
				load: procesarConsultaCuentasData,
				exception: {
					fn: function(proxy,type,action,optionRequest,response,args){
						NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
						procesarConsultaCuentasData(null,null,null);
					}
				}
			}
	});
	
	var gridCuentas = {
		xtype: 'grid',
		store: consultaCuentas,
		id: 'gridCuentas',
		columns: [
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				align: 'left',
				width: 150
			},
			{
				header: 'Pyme ',
				tooltip: 'Pyme',
				dataIndex: 'NOMBRE_PYME',
				align: 'left',				
				width: 150
			},			
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				align: 'center',
				width: 150
			}							
		],
		stripeRows: true,
		loadMask: true,
		height: 260,
		width: 500,
		title: '',
		frame: true		
	}
	
	
	
	
	// procesar rechazo	
	var procesarRechazo = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var ic_cuenta = registro.get('IC_CUENTA_BANCARIA');
		var ic_if = registro.get('IC_IF');
		var ic_epo = registro.get('IC_EPO');	
		var parametros = "pantalla=C"+ '&origenPantalla=15admparamifpymeExt.jsp'+"&ic_if="+ic_if+"&ic_epo="+ic_epo+"&ic_cuenta="+ic_cuenta;								
														
		document.location.href = "/nafin/15cadenas/15pki/15clientes/15ConsAfiliaElecIFExt.jsp?"+parametros;			
		
	}
	
	
 // ************************ ACUSE ************************
 
	var procesarGenerarArchivoPDF =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarPDF');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarPDF');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
		
	var procesarConsultaAcuseData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();							
		var gridAcuse = Ext.getCmp('gridAcuse');	
	
		if (arrRegistros != null) {
			if (!gridAcuse.isVisible()) {
				gridAcuse.show();
			}						
			//edito el titulo de la columna		
			var jsonData = store.reader.jsonData;	
			var el = gridAcuse.getGridEl();
			var cm = gridAcuse.getColumnModel();
						
			if(jsonData.sIfConvenioUnico=='S') {
				gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('OPERA_CONVENIO_UNICO'), false);	
				gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('FIRMA_AUTOGRAFA_CU'), false);	
				gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('FIRMA_PARAM_CU'), false);	
			}else {
				gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('OPERA_CONVENIO_UNICO'), true);	
				gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('FIRMA_AUTOGRAFA_CU'), true);	
				gridAcuse.getColumnModel().setHidden(cm.findColumnIndex('FIRMA_PARAM_CU'), true);	
			}
			
			Ext.getCmp('iCta').setValue(jsonData.iCta);
			Ext.getCmp('iEPO').setValue(jsonData.iEPO);
			Ext.getCmp('iPyme').setValue(jsonData.iPyme);
					
			 Ext.getCmp('btnBajarPDF').hide();	
			 Ext.getCmp('gridConsulta').hide();
			 Ext.getCmp('forma').hide();			 
			
			if(store.getTotalCount() > 0) {						
				el.unmask();					 			
				 Ext.getCmp('btnGenerarPDF').enable();						
			} else {					
				Ext.getCmp('btnGenerarPDF').disable();				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	var consultaAcuseData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15admparamifpyme.data.jsp',
		baseParams: {
			informacion: 'Autorizar'		
		},		
		fields: [			
			{name: 'NUMERO_SIRAC'},
			{name: 'NOMBRE_PYME'},
			{name: 'MONEDA'},
			{name: 'BANCO_SERVICIO'},
			{name: 'NO_CUENTA'},
			{name: 'CUENTA_CLABE'},
			{name: 'SUCURSAL'},
			{name: 'NOMBRE_EPO'},
			{name: 'RFC'},
			{name: 'PLAZA'},
			{name: 'AUTORIZACION_IF'},
			{name: 'USUARIO_MODIFICO'},
			{name: 'OPERA_CONVENIO_UNICO'},
			{name: 'FIRMA_AUTOGRAFA_CU'},
			{name: 'FIRMA_PARAM_CU'},
			{name: 'NUM_NAFIN_ELECT'}
			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaAcuseData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaAcuseData(null, null, null);					
				}
			}
		}		
	});
	
	var gridAcuse = new Ext.grid.GridPanel({
		store: consultaAcuseData,
		clicksToEdit: 1,	
		id: 'gridAcuse',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Acuse de recibo de Autorizaci�n de Cuentas Bancaria(s) PYME',
		columns: [	
			{
				header: 'N�mero SIRAC',
				tooltip: 'N�mero SIRAC',
				dataIndex: 'NUMERO_SIRAC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},	
			{
				header: 'PyME',
				tooltip: 'PyME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},	
			{
				header: 'Banco de Servicio',
				tooltip: 'Banco de Servicio',
				dataIndex: 'BANCO_SERVICIO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},	
			{
				header: 'No. de Cuenta',
				tooltip: 'No. de Cuenta',
				dataIndex: 'NO_CUENTA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},	
			{
				header: 'Cuenta Clabe',
				tooltip: 'Cuenta  Clabe',
				dataIndex: 'CUENTA_CLABE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},	
			{
				header: 'Sucursal',
				tooltip: 'Sucursal',
				dataIndex: 'SUCURSAL',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},	
			{
				header: 'EPO',
				tooltip: 'EP',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},	
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'				
			},	
			{
				header: 'Plaza',
				tooltip: 'Plaza',
				dataIndex: 'PLAZA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},	
			{
				header: 'Autorizaci�n IF',
				tooltip: 'Autorizaci�n IF',
				dataIndex: 'AUTORIZACION_IF',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'				
			},	
			{
				header: 'Usuario - Modifico',
				tooltip: 'Usuario - Modifico',
				dataIndex: 'USUARIO_MODIFICO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Pyme Opera con Convenio Unico',
				tooltip: 'Pyme Opera con Convenio Unico',
				dataIndex: 'OPERA_CONVENIO_UNICO',
				sortable: true,
				width: 130,				
				resizable: true,
				hidden: true,
				align: 'center'				
			},	
			{
				header: 'Fecha de Firma Autografa de CU',
				tooltip: 'Fecha de Firma Autografa de CU',
				dataIndex: 'FIRMA_AUTOGRAFA_CU',
				sortable: true,
				width: 130,				
				resizable: true,
				hidden: true,
				align: 'center'				
			},	
			{
				header: 'Fecha Parametrizaci�n Automatica CU',
				tooltip: 'Fecha Parametrizaci�n Automatica CU',
				dataIndex: 'FIRMA_PARAM_CU',
				sortable: true,
				width: 130,				
				resizable: true,	
				hidden: true,
				align: 'center'				
			},	
			{
				header: 'N�m.Nafin Electr�nico',
				tooltip: 'N�m.Nafin Electr�nico',
				dataIndex: 'NUM_NAFIN_ELECT',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'				
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,		
		frame: true,
		bbar: {
			items: [			
				'-',
				'->',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						var iCta = Ext.getCmp('iCta').getValue();
						var iEPO = Ext.getCmp('iEPO').getValue();
						var iPyme = Ext.getCmp('iPyme').getValue();
										
						Ext.Ajax.request({
							url: '15admparamifpyme.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoPDFAcuse',
								iCta:iCta,
								iEPO:iEPO,
								iPyme:iPyme								
							})
							,callback: procesarGenerarArchivoPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Salir',
					tooltip:	'Salir',
					iconCls: 'icoLimpiar',
					id: 'btnSalir',
					handler: function() {
						window.location = '15admparamifpymeExt.jsp';
					}
				}
			]
		}		
	});
		
			
	// funcion para realizar la  autorizacion de las cuentas 
	var procesoAutorizar=  function() {
		chkCtaBanc = [];
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
	
		store.each(function(record) {					
			if( record.data['SELECCIONAR'] ==true){			
				chkCtaBanc.push(record.data['IC_CUENTA_BANCARIA'] +'|'+record.data['IC_EPO'] +'|'+record.data['IC_PYME'] +'|'+'|'+record.data['OPERA_CONVENIO_UNICO'] );		
			}		
		});
		
		if(chkCtaBanc =='') {
			Ext.MessageBox.alert('Mensaje','Debe seleccionar al menos un registro');
			return false;	
		}
		
		consultaAcuseData.load({
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Autorizar',
				chkCtaBanc:chkCtaBanc
			})
		});
	
	}
	
	
	// CONSULTA  
	var procesarGenerarArchivoCSV =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarCSV');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarCSV');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var descargaArchivoExpediente = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var rfc = registro.get('RFC');
		
		new Ext.Window({  
			title: 'Expediente', 
			modal: true,
			constrain:true,
			width: 300,  
			height:150,  
			minimizable: false,  
			maximizable: false,  
			html: '<iframe src="/nafin/15cadenas/15mantenimiento/15clientes/popupexpediente.jsp?rfc='+escape(rfc)+'&version=ext"></iframe>'
		}).show();
	}
	

//************Consulta  Grid ************************
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna		
			var jsonData = store.reader.jsonData;	
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			
			if(jsonData.sIfConvenioUnico=='S') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_CLABE'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('OPERA_CONVENIO_UNICO'), false);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_FIRMA_AUTO'), false);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_PARAM_CU'), false);				
			}else if(jsonData.sIfConvenioUnico=='N') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_CLABE'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('OPERA_CONVENIO_UNICO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_FIRMA_AUTO'), true);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_PARAM_CU'), true);							
			}
			
			Ext.getCmp('btnBajarCSV').hide();					 			
			
			if(store.getTotalCount() > 0) {						
				el.unmask();	
				 Ext.getCmp('btnGenerarCSV').enable();				
				 Ext.getCmp('btnAutorizar').enable();						
			} else {		
				Ext.getCmp('btnGenerarCSV').disable();
				Ext.getCmp('btnAutorizar').disable();
						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}

	
	var consultaData   = new Ext.data.GroupingStore({	 
		root : 'registros',
		url : '15admparamifpyme.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [			
				{name: 'CS_VOBO_IF'},
				{name: 'IC_PYME'},
				{name: 'IC_EPO'},
				{name: 'IC_IF'},
				{name: 'IC_CUENTA_BANCARIA'},				
				{name: 'NUMERO_SIRAC'},
				{name: 'NOMBRE_PYME'},
				{name: 'NOMBRE_MONEDA'},
				{name: 'BANCO_SERVICIO'},
				{name: 'NO_CUENTA'},
				{name: 'CUENTA_CLABE'},
				{name: 'SUCURSAL'},
				{name: 'NOMBRE_EPO'},
				{name: 'RFC'},
				{name: 'PLAZA'},
				{name: 'SELECCIONAR'},
				{name: 'OPERA_CONVENIO_UNICO'},
				{name: 'FECHA_FIRMA_AUTO'},
				{name: 'FECHA_PARAM_CU'},
				{name: 'NUM_NAFIN_ELEC'},
				{name: 'PRIORIDAD'}			
			]
		}),
		groupField: 'NOMBRE_PYME',
		sortInfo:{field: 'NOMBRE_PYME', direction: "ASC", align: 'left' },
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}		
	});
	

	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		clicksToEdit: 1,	
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: ' ',
		columns: [	
			{
				header: 'N�mero SIRAC',
				tooltip: 'N�mero SIRAC',
				dataIndex: 'NUMERO_SIRAC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{
				header: 'PyME',
				tooltip: 'PyME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'NOMBRE_MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{
				header: 'Banco de Servicio',
				tooltip: 'Banco de Servicio',
				dataIndex: 'BANCO_SERVICIO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'No. de Cuenta',
				tooltip: 'No. de Cuenta',
				dataIndex: 'NO_CUENTA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'Cuenta Clabe',
				tooltip: 'Cuenta Clabe',
				dataIndex: 'CUENTA_CLABE',
				sortable: true,
				hidden: true, 
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'Sucursal',
				tooltip: 'Sucursal',
				dataIndex: 'SUCURSAL',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{
				header: 'Plaza',
				tooltip: 'Plaza',
				dataIndex: 'PLAZA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				xtype: 'checkcolumn',
				header : 'Autorizaci�n IF',
				tooltip: 'Autorizaci�n IF',
				dataIndex : 'SELECCIONAR',
				width : 150,
				align: 'center',
				sortable : false			 
			},	
			{
				xtype: 'actioncolumn',
				header: 'Rechazo',
				tooltip: 'Rechazo',
				 width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'borrar';								
						} 
						,handler: procesarRechazo
					}
				]				
			},
			{
				header: 'Pyme Opera con Convenio Unico',
				tooltip: 'Pyme Opera con Convenio Unico',
				dataIndex: 'OPERA_CONVENIO_UNICO',
				sortable: true,
				hidden: true, 
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				xtype: 'actioncolumn',
				header: 'Expediente',
				tooltip: 'Expediente',
				 width: 130,
				align: 'center',				
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';								
						} 
						,handler: descargaArchivoExpediente
					}
				]				
			},
			
			{
				header: 'Fecha de Firma Autografa de CU.',
				tooltip: 'Fecha de Firma Autografa de CU.',
				dataIndex: 'FECHA_FIRMA_AUTO',
				sortable: true,
				hidden: true, 
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Fecha Parametrizaci�n Automatica CU',
				tooltip: 'Fecha Parametrizaci�n Automatica CU',
				dataIndex: 'FECHA_PARAM_CU',
				sortable: true,
				hidden: true, 
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'N�m Nafin Electr�nico',
				tooltip: 'N�m Nafin Electr�nico',
				dataIndex: 'NUM_NAFIN_ELEC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Prioridad',
				tooltip: 'Prioridad',
				dataIndex: 'PRIORIDAD',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			}
		],
		/*
		view: new Ext.grid.GroupingView({
			forceFit:true,
         groupTextTpl: '{text}',
			align: 'left'
      }),*/
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,		
		frame: true,
		bbar: {		
			items: [			
				'-',
				'->',				
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.disable();
						Ext.Ajax.request({
							url: '15admparamifpyme.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV'															
							})
							,callback: procesarGenerarArchivoCSV
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Autorizar',
					iconCls: 'icoAceptar',
					id: 'btnAutorizar',
					handler: procesoAutorizar
				}
			]
		}
	});


			

	//************Forma************************
		
	function procesarValIniciales(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
			
				if(jsonData.sIfConvenioUnico=='S') {
					Ext.getCmp('chkPymesConvenio2').show();					
				}else if(jsonData.sIfConvenioUnico=='N') {
					Ext.getCmp('chkPymesConvenio2').hide();					
				}		
				// para mostrar la ventana emergente de  Cuentas Pendientes
				if(jsonData.numeroCuentasPendientes!='0') {
					ventanaCuentasPendientes();
				}
			
			}
			fp.el.unmask();
		}else {
		NE.util.mostrarConnError(response,opts);
		}
	}
	
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15admparamifpyme.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admparamifpyme.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPOData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admparamifpyme.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},		
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 		'panel',
			bodyStyle: 	'padding: 10px',
			layout: {
				type: 	'hbox',
				pack: 	'center',
				align: 	'middle'
			},
			items: [
				{
					xtype: 'button',
					text: 'Buscar',
					id: 'btnBuscar',
					iconCls: 'icoBuscar',
					width: 	75,
					handler: function(boton, evento) {
						var pymeComboCmp = Ext.getCmp('cbPyme1');
						pymeComboCmp.setValue('');
						pymeComboCmp.setDisabled(false);		
						
						storeBusqAvanzPyme.load({
							params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
								informacion: 'busquedaAvanzada'					
							})																	
						});				
					},
					style: { 
						marginBottom:  '10px' 
					} 
				}
			]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});
	
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);	
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var  elementosForma =  [
		{	xtype: 'textfield',	hidden: true, id: 'iCta' },
		{	xtype: 'textfield',	hidden: true, id: 'iEPO' },
		{	xtype: 'textfield',	hidden: true, id: 'iPyme' },
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'displayfield',
					value: '',
					width: 80
				},					
				{
					xtype: 'button',
					text: 'Individual',
					id: 'individual',
					handler: function(boton, evento) {
						window.location = '15admparamifpymeExt.jsp';
					}
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 80
				},		
				{
					xtype: 'button',
					text: 'Masiva',
					id: 'masiva',
					handler: function(boton, evento) {
						window.location = '15admparamifpymemasivaext.jsp';
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�m. Electr�nico PyME',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'N�mero Nafin Electr�nico',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){
						 // Petici�n b�sica  
							Ext.Ajax.request({  
								url: '15admparamifpyme.data.jsp',  
								method: 'POST',  
								callback: successAjaxFn,  
								params: {  
									 informacion: 'pymeNombre' ,
									 num_electronico: Ext.getCmp('num_electronico1').getValue()
								}  
							}); 
							
							Ext.getCmp("cboEPO1").setValue('');
							Ext.getCmp("clave_pyme1").setValue('');
							
						}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 250,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
					xtype: 'button',
					text: 'B�squeda Avanzada...',
					iconCls: 'icoBuscar',
					id: 'btnBusqAv',
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winBusqAvan');
						if (ventana) {
							ventana.show();
						} else {
							new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
						}
					}
				}
			]
		},	
		{
			xtype: 'combo',
			name: 'cboEPO',
			id: 'cboEPO1',
			fieldLabel: 'Cadena Productiva',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEPO',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var cmbPyme = Ext.getCmp('clave_pyme1');
						cmbPyme.setValue('');
						cmbPyme.setDisabled(false);
						cmbPyme.store.load({
							params: {
								cboEPO:combo.getValue()
							}
						});
						Ext.getCmp("num_electronico1").setValue('');
						Ext.getCmp("txtNombre1").setValue('');
					}					
				}
			}
		},
		{
			xtype: 'combo',
			name: 'clave_pyme',
			id: 'clave_pyme1',
			fieldLabel: 'Nombre de la PyME',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_pyme',
			emptyText: 'Todas las PYME ...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoPYMEData,
			tpl : NE.util.templateMensajeCargaCombo					
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: 'PyMEs con Convenio �nico',
			combineErrors: false,
			msgTarget: 'side',	
			id: 'chkPymesConvenio2',
			items: [
				{
					xtype: 'checkbox',
					name: 'chkPymesConvenio',
					id: 'chkPymesConvenio',
					fieldLabel: '',
					allowBlank: true,
					startDay: 0,
					width: 100,
					height:20,					
					msgTarget: 'side',
					margins: '0 20 0 0'
				}				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Parametrizaci�n',
			combineErrors: false,
			msgTarget: 'side',
			id: 'fechaparamCU',			
			items: [
				{
					xtype: 'datefield',
					name: 'fechaparamCU_ini',
					id: 'fechaparamCU_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaparamCU_fin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaparamCU_fin',
					id: 'fechaparamCU_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fechaparamCU_ini',
					margins: '0 20 0 0'  
				}
			]
		}	
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 750,
		title: 'Criterios de Busqueda Individual',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
							
					var cboEPO =  Ext.getCmp("cboEPO1");
					var num_electronico =  Ext.getCmp("num_electronico1");
					var fechaparamCU_ini =  Ext.getCmp("fechaparamCU_ini");
					var fechaparamCU_fin =  Ext.getCmp("fechaparamCU_fin");
					
					if (Ext.isEmpty(cboEPO.getValue())  &&  Ext.isEmpty(num_electronico.getValue())   ){
						Ext.MessageBox.alert("Mensaje","Debe seleccionar una Epo o un n�mero Electr�nico de Pyme.");											
						return;
					}
				
					if ( ( !Ext.isEmpty(fechaparamCU_ini.getValue()) &&  Ext.isEmpty(fechaparamCU_fin.getValue()) )   || ( Ext.isEmpty(fechaparamCU_ini.getValue()) &&  !Ext.isEmpty(fechaparamCU_fin.getValue()) ) ) {	
						fechaparamCU_fin.markInvalid('Debe seleccionar un rango de fechas');	
						fechaparamCU_ini.markInvalid('Debe seleccionar un rango de fechas');	
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar'							
						})
					});							
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15admparamifpymeExt.jsp';
				}
			}
		]
	});



	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,		
		items: [		
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,			
			gridAcuse,
			NE.util.getEspaciador(20)
		]
	});
	

	catalogoEPOData.load();
	
	var valIniciales = function(){
		
		Ext.Ajax.request({
			url : '15admparamifpyme.data.jsp',
			params : {
				informacion: 'valIniciales'			
			}
			,callback: procesarValIniciales
		});
	}
	
	valIniciales();
	
	if(num_naf_elect !='') {
	
		Ext.getCmp('num_electronico1').setValue(num_naf_elect);
	
			Ext.Ajax.request({  
				url: '15admparamifpyme.data.jsp',  
				method: 'POST',  
				callback: successAjaxFn,  
				params: {  
					informacion: 'pymeNombre' ,
					num_electronico:num_naf_elect
				} 
			}); 
							
		consultaData.load({
			params: Ext.apply(fp.getForm().getValues(),{
				operacion: 'Generar'							
			})
		});	
	}
	
	
});
