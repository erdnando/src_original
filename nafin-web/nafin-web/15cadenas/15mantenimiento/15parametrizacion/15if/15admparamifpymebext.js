Ext.onReady(function() {

	 TaskLocation = Ext.data.Record.create([ 
		 {name: "clave", type: "string"}, 
		 {name: "descripcion", type: "string"}, 
		 {name: "loadMsg", type: "string"}
	]);
	

	var descargaArchivoExpediente = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var rfc = registro.get('RFC');
		new Ext.Window({  
		 title: 'Expediente', 
		 modal: true,
		 constrain:true,
		 width: 300,  
		 height:150,  
		 minimizable: false,  
		 maximizable: false,  
		 html: '<iframe src="/nafin/15cadenas/15mantenimiento/15clientes/popupexpediente.jsp?rfc='+escape(rfc)+'&version=ext"></iframe>'
		}).show();
	}
	
	var procesarGenerarArchivo =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarArchivo');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarArchivo');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//************Consulta  Grid ************************
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna		
			var jsonData = store.reader.jsonData;	
			var el = gridConsulta.getGridEl();
			
			 Ext.getCmp('btnBajarArchivo').hide();
				
								
			if(store.getTotalCount() > 0) {						
				el.unmask();	
				 Ext.getCmp('btnGenerarArchivo').enable();				
			} else {		
				Ext.getCmp('btnGenerarArchivo').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15admparamifpymebext.data.jsp',
		baseParams: {
			informacion: 'Consultar'		
		},		
		fields: [			
			{name: 'NUM_SIRAC'},
			{name: 'NOMBRE_PYME'},
			{name: 'MONEDA'},
			{name: 'BANCO_SERVICIO'},
			{name: 'CUENTA'},
			{name: 'CUENTA_CLABE'},
			{name: 'SUCURSAL'},
			{name: 'NOMBRE_EPO'},	
			{name: 'RFC'},
			{name: 'PLAZA'},	
			{name: 'AUTORIZACION_IF'},
			{name: 'OPERA_CONVENIO_UNICO'},
			{name: 'FECHA_FIRMA_CU'},
			{name: 'FECHA_PARAMETRIZACION_CU'},
			{name: 'NAFIN_ELECTRONICO'},
			{name: 'CLAVE_EPO'},
			{name: 'CLAVE_PYME'},
			{name: 'CS_EXPEDIENTE_EFILE'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		clicksToEdit: 1,	
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Consulta  Cuentas Bancarias PYMEs',
		columns: [	
			{
				header: 'N�mero SIRAC',
				tooltip: 'N�mero SIRAC',
				dataIndex: 'NUM_SIRAC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'				
			},			
			{
				header: 'Pyme',
				tooltip: 'Pyme)',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Banco de Servicio',
				tooltip: 'Banco de Servicio',
				dataIndex: 'BANCO_SERVICIO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'No de Cuenta',
				tooltip: 'No de Cuenta',
				dataIndex: 'CUENTA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Cuenta Clabe',
				tooltip: 'Cuenta Clabe',
				dataIndex: 'CUENTA_CLABE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Sucursal',
				tooltip: 'Sucursal',
				dataIndex: 'SUCURSAL',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Plaza',
				tooltip: 'Plaza',
				dataIndex: 'PLAZA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Autorizaci�n IF',
				tooltip: 'Autorizaci�n IF',
				dataIndex: 'AUTORIZACION_IF',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Pyme Opera con Convenio Unico',
				tooltip: 'Pyme Opera con Convenio Unico',
				dataIndex: 'OPERA_CONVENIO_UNICO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				xtype: 'actioncolumn',
				header: 'Expediente',
				tooltip: 'Expediente',
				 width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
				 if(registro.get('CS_EXPEDIENTE_EFILE') !='S' ){
						 return 'No Disponible ';
					}					
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('CS_EXPEDIENTE_EFILE') =='S' ){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';	
							}
						} 
						,handler: descargaArchivoExpediente
					}
				]				
			},
			{
				header: 'Fecha de Firma Autografa de CU.',
				tooltip: 'Fecha de Firma Autografa de CU.',
				dataIndex: 'FECHA_FIRMA_CU',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Fecha Parametrizacion Automatica CU',
				tooltip: 'Fecha Parametrizacion Automatica CU',
				dataIndex: 'FECHA_PARAMETRIZACION_CU',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Num. Nafin Electr�nico',
				tooltip: 'Num. Nafin Electr�nico',
				dataIndex: 'NAFIN_ELECTRONICO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['AUTORIZACION_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,		
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [			
				'-',
				'->',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						Ext.Ajax.request({
							url: '15admparamifpymebext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								operacion: 'ArchivoCSV'								
							}),
							callback: procesarGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				}			
			]
		}
	});




	//************Forma************************

	function procesarValIniciales(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
			
				if(jsonData.sIfConvenioUnico=='S') {
					Ext.getCmp('fechaparamCU').show();					
				}else if(jsonData.sIfConvenioUnico=='N') {
					Ext.getCmp('fechaparamCU').hide();					
				}
			
			}
			fp.el.unmask();
		}else {
		NE.util.mostrarConnError(response,opts);
		}
	}	
	
	var procesarCatPYMEData=function(store, arrRegistros, opts){
      if(store.getTotalCount() > 0) {
         store.insert(0,new TaskLocation({ 
          clave: "0", 
          descripcion: "Todas las PYME", 
          loadMsg: ""})); 
         store.commitChanges(); 
      }	
	}
	
	
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admparamifpymebext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatPYMEData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPOData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admparamifpymebext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var  elementosForma =  [
		{
			xtype: 'compositefield',
			fieldLabel: 'N�m. Electr�nico PyME',
			id: 'campoCompuesto1',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					name: 'ic_nafin_electronico',
					id: 'ic_nafin_electronico1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',					
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){
							Ext.getCmp("cboEPO1").setValue('');
							Ext.getCmp("clave_pyme1").setValue('');							
						}
					}//necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'combo',
			name: 'cboEPO',
			id: 'cboEPO1',
			fieldLabel: 'Cadena Productiva',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEPO',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
				listeners: {
				select: {
					fn: function(combo) {
						var cmbPyme = Ext.getCmp('clave_pyme1');
						cmbPyme.setValue('');
						cmbPyme.setDisabled(false);
						cmbPyme.store.load({
							params: {
								cboEPO:combo.getValue()
							}
						});
						
						Ext.getCmp("ic_nafin_electronico1").setValue('');
					}
				}
			}
		},
		{
			xtype: 'combo',
			name: 'clave_pyme',
			id: 'clave_pyme1',
			fieldLabel: 'PYME',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_pyme',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoPYMEData,
			tpl : NE.util.templateMensajeCargaCombo					
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'PyMEs con Convenio �nico',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'checkbox',
					name: 'chkPymesConvenio',
					id: 'chkPymesConvenio',
					fieldLabel: '',
					allowBlank: true,
					startDay: 0,
					width: 100,
					height:20,					
					msgTarget: 'side',
					margins: '0 20 0 0'
				}				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Parametrizaci�n Aut�matica de CU',
			combineErrors: false,
			msgTarget: 'side',
			id: 'fechaparamCU',
			hidden: true,
			items: [
				{
					xtype: 'datefield',
					name: 'fechaparamCU_ini',
					id: 'fechaparamCU_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaparamCU_fin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaparamCU_fin',
					id: 'fechaparamCU_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fechaparamCU_ini',
					margins: '0 20 0 0'  
					}
				]
			}
	
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de Busqueda de Consulta  Cuentas Bancarias PYMEs',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
							
					
					var cboEPO =  Ext.getCmp("cboEPO1");
					var ic_nafin_electronico =  Ext.getCmp("ic_nafin_electronico1");
					var fechaparamCU_ini =  Ext.getCmp("fechaparamCU_ini");
					var fechaparamCU_fin =  Ext.getCmp("fechaparamCU_fin");
					var cboPYME =  Ext.getCmp("clave_pyme1");
					
					if (Ext.isEmpty(cboEPO.getValue())  &&  Ext.isEmpty(ic_nafin_electronico.getValue())   ){
						Ext.MessageBox.alert("Mensaje","Debe seleccionar una Epo o un n�mero Electr�nico de Pyme.");											
						return;
					}
					
					if ( ( !Ext.isEmpty(fechaparamCU_ini.getValue()) &&  Ext.isEmpty(fechaparamCU_fin.getValue()) )   || ( Ext.isEmpty(fechaparamCU_ini.getValue()) &&  !Ext.isEmpty(fechaparamCU_fin.getValue()) ) ) {	
						fechaparamCU_fin.markInvalid('Debe seleccionar un rango de fechas');	
						fechaparamCU_ini.markInvalid('Debe seleccionar un rango de fechas');	
						return;
					}
					
					if (!Ext.isEmpty(cboEPO.getValue())  &&  Ext.isEmpty(cboPYME.getValue()) ){
						Ext.MessageBox.alert("Mensaje","Debe seleccionar una Pyme o todas.");											
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15
						})
					});				
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15admparamifpymebext.jsp';
				}
			}
		]
	});



	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,		
		items: [		
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	

	catalogoEPOData.load();


	var valIniciales = function(){
		
		Ext.Ajax.request({
			url : '15admparamifpymebext.data.jsp',
			params : {
				informacion: 'valIniciales'			
			}
			,callback: procesarValIniciales
		});
	}
	
	valIniciales();
	
});
