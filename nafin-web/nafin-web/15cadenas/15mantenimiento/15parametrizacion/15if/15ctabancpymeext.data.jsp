	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,  
	com.netro.model.catalogos.*,
	net.sf.json.JSONObject,
	com.netro.distribuidores.ConsCuentaIfDist,
	net.sf.json.JSONArray,
	com.netro.distribuidores.*"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>

<%


	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String informacion         =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String ic_if 					=iNoCliente;
	String ic_moneda 				=(request.getParameter("cbMoneda")    != null) ?   request.getParameter("cbMoneda") :"";
	String CtasCapturadas		=(request.getParameter("CtasCapturadas")    != null) ?   request.getParameter("CtasCapturadas") :"";
	String cg_sucursal			=(request.getParameter("sucursal")    != null) ?   request.getParameter("sucursal") :"";
	String cg_numcta				=(request.getParameter("noCuenta")    != null) ?   request.getParameter("noCuenta") :"";
	String ic_plaza				=(request.getParameter("plaza")    != null) ?   request.getParameter("plaza") :"";
	String ic_epo					=(request.getParameter("icEpo")    != null) ?   request.getParameter("icEpo") :"";
	String cg_rfc					=(request.getParameter("rfc")    != null) ?   request.getParameter("rfc") :"";
	String ic_nafinElec			=(request.getParameter("ne")    != null) ?   request.getParameter("ne") :"";
	String ic_pyme					=(request.getParameter("icDist")    != null) ?   request.getParameter("icDist") :"";
	String ic_financiera			=(request.getParameter("cbBanco")    != null) ?   request.getParameter("cbBanco") :"";
	String operacion				=(request.getParameter("operacion")    != null) ?   request.getParameter("operacion") :"";
		
	boolean bandera=false;
	int accion= (request.getParameter("accion")    != null) ?   Integer.parseInt(request.getParameter("accion")) :0;
	if(request.getParameter("bandera")!=null){
	bandera =(request.getParameter("bandera").equals("true")) ?   true :false;
	}
	if(informacion.equals("catologoEpo")){
	
		CatalogoEPODistribuidores catalogo = new CatalogoEPODistribuidores();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setTipoCredito("D");
		catalogo.setClaveIf(iNoCliente);
		infoRegresar=catalogo.getJSONElementos();
	}else if(informacion.equals("catologoDist")){
	
		CatalogoPymeDistribuidores catalogo = new CatalogoPymeDistribuidores();
		catalogo.setCampoClave("cp.ic_pyme");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveEpo(ic_epo);
		catalogo.setTipoCredito("D");
		catalogo.setIcIF(iNoCliente);
		catalogo.setCondicionBancaria(bandera);
		infoRegresar=catalogo.getJSONElementos();
	}else if (informacion.equals("catologoMonedaDist")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
		
	}else if (informacion.equals("catologoBanco")) {
	  CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_financiera");
		cat.setDistinc(true);
		cat.setCampoClave("ic_financiera");
	  cat.setCampoDescripcion("cd_nombre");
		List condicion=new ArrayList();
		cat.setCampoLlave("ic_tipo_financiera");
		condicion.add("1");
		cat.setValoresCondicionIn(condicion);
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}else if (informacion.equals("catologoPlaza")) {
	  CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_plaza");
		cat.setCampoClave("ic_plaza");
		cat.setCampoDescripcion("initcap(CD_DESCRIPCION||', '||CG_ESTADO)");
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();
	}else if (informacion.equals("informacionDist")) {
		
		ParametrosDist parametrosBean = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class); 
	
		Registros registros=new Registros();
		try{
		if(!ic_nafinElec.equals("")){
			Integer.parseInt(ic_nafinElec);
		}
		registros= parametrosBean.getInformacionDistribuidor(ic_epo, cg_rfc,ic_nafinElec,ic_pyme,iNoCliente);
		
		List reg=new ArrayList();
	
		while(registros.next()){
			
				for(int i=0;i<registros.getNombreColumnas().size();i++){
								if(registros.getString(registros.getNombreColumnas().get(i).toString()).equals("NULL")){
									
								}
								jsonObj.put(registros.getNombreColumnas().get(i).toString().toUpperCase(),registros.getString(registros.getNombreColumnas().get(i).toString()));	
								jsonObj.put("success", new Boolean(true));

					}
			}
		}catch(Exception e){
				jsonObj.put("success", new Boolean(false));
		}
		if(jsonObj.toString().equals("{}")){
				jsonObj.put("success", new Boolean(false));
		}
   		infoRegresar=jsonObj.toString();

	}else if (informacion.equals("Insertar")) {
		
		ParametrosDist parametrosBean = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class); 
		String respuesta[]=parametrosBean.insertaCuentaBancaria(ic_financiera, ic_if, ic_moneda, ic_epo,
	  cg_rfc, ic_nafinElec, ic_pyme, cg_sucursal, cg_numcta, ic_plaza);
	  
	  if(!respuesta[1].equals("true")){
		
		jsonObj.put("success", new Boolean(true));
	  }
	  else{
		jsonObj.put("success", new Boolean(false));
	  }
		jsonObj.put("mensaje", respuesta[0]);
		
		jsonObj.put("id", respuesta[2]);
  		infoRegresar=jsonObj.toString();
		

	}else if (informacion.equals("Aceptar")) {
		
		ParametrosDist parametrosBean = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class); 
		String respuesta[]=parametrosBean.actualizaCuentaBancaria(ic_financiera, ic_if, ic_moneda, ic_epo,
	  cg_rfc, ic_nafinElec, ic_pyme,CtasCapturadas, cg_sucursal, cg_numcta, ic_plaza);
	  if(!respuesta[1].equals("true")){
		
		jsonObj.put("success", new Boolean(true));
	  }
	  else{
		jsonObj.put("success", new Boolean(false));
	  }
		jsonObj.put("mensaje", respuesta[0]);
		jsonObj.put("id", respuesta[2]);
  		infoRegresar=jsonObj.toString();

	}else if (informacion.equals("Eliminar")) {
		
		ParametrosDist parametrosBean = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class); 
		String respuesta[]=parametrosBean.eliminarCuenta(ic_financiera, ic_if, ic_moneda, ic_epo,
	  cg_rfc, ic_nafinElec, ic_pyme,CtasCapturadas, cg_sucursal, cg_numcta, ic_plaza);
	  if(!respuesta[1].equals("true")){	
		jsonObj.put("success", new Boolean(true));
	  }
	  else{
		jsonObj.put("success", new Boolean(false));
	  }
		jsonObj.put("mensaje", respuesta[0]);
		jsonObj.put("id", respuesta[2]);
		
  		infoRegresar=jsonObj.toString();

	}else if (informacion.equals("Consulta")) {
		ConsCuentaIfDist pagina=new ConsCuentaIfDist();
		pagina.setIc_if(ic_if);
		pagina.setIc_moneda(ic_moneda);
		pagina.setIc_epo(ic_epo);
		pagina.setCg_rfc(cg_rfc);
		pagina.setIc_nafinElec(ic_nafinElec);
		pagina.setIc_pyme(ic_pyme);
		pagina.setCg_sucursal(cg_sucursal);
		pagina.setCg_numcta(cg_numcta);
		pagina.setIc_plaza(ic_plaza);
		pagina.setIc_financiera(ic_financiera);
	
	CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(pagina);
			Registros registros=cqhelper.doSearch();
			HashMap	datos;
			List reg=new ArrayList();
			while(registros.next()){
						datos=new HashMap();
						datos.put("NOMBREEPO",registros.getString("nomepo"));
						datos.put("BANCO",registros.getString("nomfin"));
						datos.put("DISTRIBUIDOR",registros.getString("nompyme"));
						datos.put("RFC",registros.getString("rfc"));
						datos.put("SUCURSAL",registros.getString("sucursal"));
						datos.put("MONEDA",registros.getString("moneda"));
						datos.put("NOCUENTA",registros.getString("nocta"));
						datos.put("PLAZA",registros.getString("plaza"));
						datos.put("AUTORIZADO",registros.getString("autorizado"));
						datos.put("ID",registros.getString("ID"));
						datos.put("IC_PLAZA",registros.getString("IC_PLAZA"));
						datos.put("IC_EPO",registros.getString("ic_epo"));
						datos.put("IC_PYME",registros.getString("ic_pyme"));
						datos.put("IC_MONEDA",registros.getString("ic_moneda"));
						datos.put("IC_BANCO",registros.getString("ic_banco"));
						datos.put("NEPYME",registros.getString("ne"));
						reg.add(datos);
					}
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("registros", reg);
			infoRegresar=jsonObj.toString();
			
			 if (operacion.equals("ArchivoCSV")) {
					 try {
					  
						String nomArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
						jsonObj = new JSONObject();
						jsonObj.put("success", new Boolean(true));
						jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
						infoRegresar = jsonObj.toString();
					} catch(Throwable e) {
					 throw new AppException("Error al generar el archivo CSV", e);
				}
			}else  if (operacion.equals("ArchivoPDF")) {
					 try {
					  
						String nomArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp,"PDF");
						jsonObj = new JSONObject();
						jsonObj.put("success", new Boolean(true));
						jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
						infoRegresar = jsonObj.toString();
					} catch(Throwable e) {
					 throw new AppException("Error al generar el archivo PDF", e);
				}
			}

	}else if (informacion.equals("Actualizar")) {
		
		ParametrosDist parametrosBean = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class); 
		String respuesta[]=parametrosBean.aceptaCuenta( CtasCapturadas, cg_sucursal, cg_numcta, ic_plaza);
	  if(!respuesta[1].equals("true")){

		jsonObj.put("success", new Boolean(true));
	  }
	  else{
		jsonObj.put("success", new Boolean(false));
	  }
		jsonObj.put("mensaje", respuesta[0]);
		jsonObj.put("id", respuesta[2]);
		
  		infoRegresar=jsonObj.toString();
		
	} else if(informacion.equals("CuentaExiste")){
		if(!"".equals(ic_epo)&&!"".equals(ic_pyme)&&!"".equals(ic_moneda)&&!"".equals(ic_financiera)) {
			AccesoDB con = new AccesoDB();
			try{
			
			String query = "";
			PreparedStatement ps = null;
			ResultSet rs = null;
			con.conexionDB();
			query = 
				" SELECT ic_cuenta_bancaria,cg_numero_cuenta,cg_sucursal,ic_plaza "   +
				" FROM comrel_cuenta_bancaria_x_prod "   +
				" WHERE ic_epo = ? "   +
				" AND ic_pyme = ? "   +
				" AND ic_financiera = ? "   +
				" AND ic_moneda = ?"  ;
			ps = con.queryPrecompilado(query);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, Integer.parseInt(ic_pyme));
			ps.setInt(3, Integer.parseInt(ic_financiera));
			ps.setInt(4, Integer.parseInt(ic_moneda));
			rs = ps.executeQuery();
			if(rs.next()) {
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("ic_cuenta_b", rs.getString(1)==null?"":rs.getString(1));
				jsonObj.put("cg_numcta", rs.getString(2)==null?"":rs.getString(2));
				jsonObj.put("cg_sucursal", rs.getString(3)==null?"":rs.getString(3));
				jsonObj.put("ic_plaza", rs.getString(4)==null?"":rs.getString(4));
			}else{
				jsonObj.put("success", new Boolean(false));
				
				}
			infoRegresar=jsonObj.toString();
			}catch(Exception e){
				String strErr = e.toString();
				out.println(e.toString());
			}finally{
				con.cierraConexionDB();
			}
		}
	}

%>

<%=infoRegresar%>