Ext.onReady(function() {



//|||||||||||||||||||||||||HANDLERS||||||||||||||||||||||||||||||||||||||||||||

var procesarConsultaCveEpoWs = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('gridCveEpoWSIF');
		//var btnAutorizar = Ext.getCmp('autorizar');
		var el = gridCveEpoWSIF.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridCveEpoWSIF.isVisible()) {
				gridCveEpoWSIF.show();
			}			
			if(store.getTotalCount() > 0) {
				//btnAutorizar.enable();
				el.unmask();
			}else{
				//btnAutorizar.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarSuccessFailureGuardarCveEposWS =  function(opts, success, response) {
		var gridCveEpoWS = Ext.getCmp('gridCveEpoWSIF');
		gridCveEpoWS.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			consCveEpoWsIfData.commitChanges();
			var resp = Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
				
			Ext.MessageBox.alert('Aviso','Guardado con �xito');
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGuardar = function() {
      
		//var gridConsulta = Ext.getCmp('gridPagoAnt');
		//var store = gridConsulta.getStore();
		var arrayModif = consCveEpoWsIfData.getModifiedRecords();
		var registrosEnviar = [];
		
		if(arrayModif.length>0){
			var error  = false;
			
			Ext.each(arrayModif, function(record){
			//var inx = store.indexOf(record);
			registrosEnviar.push(record.data);
			});
			registrosEnviar = Ext.encode(registrosEnviar);
			gridCveEpoWSIF.el.mask('Guardando...', 'x-mask-loading');
			
			Ext.Ajax.request({
				url : '15cveepowsif_ext.data.jsp',
				params :{
					informacion: 'GuardarCeEposWS',
					registros : registrosEnviar
				},
				callback: procesarSuccessFailureGuardarCveEposWS
			});
			
         //store.commitChanges();
			
		}else{
         Ext.Msg.alert('Aviso','No se ha modificado ningun registro'); return;
      }
	}

//|||||||||||||||||||||||||STORES||||||||||||||||||||||||||||||||||||||||||||||

	var consCveEpoWsIfData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15cveepowsif_ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaCveEpos'
		},
		fields: [
			{name: 'ic_if', type: 'string'},
			{name: 'ic_epo', type: 'string'},
			{name: 'cg_razon_social', type: 'string'},
			{name: 'ig_epo_externa', type: 'string'},
			{name: 'cc_tipo_factoraje', type: 'string'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaCveEpoWs,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});

	var gridCveEpoWSIF = new Ext.grid.EditorGridPanel({
		id: 'gridCveEpoWSIF',
		store: consCveEpoWsIfData,
		title:'',
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		clicksToEdit:1,
		columns: [{
			header: 'Clave EPO NAFIN', tooltip: 'Clave EPO NAFIN',
			dataIndex: 'ic_epo',
			sortable: false, menuDisabled:true,
			width: 120, resizable: true, enableColumnMove:false,
			align: 'center'
		},{
			header: 'Nombre EPO', tooltip: 'Nombre EPO',
			dataIndex: 'cg_razon_social',enableColumnMove:false,
			sortable: false,	menuDisabled:true,
			width: 400, resizable: true,
			align: 'center'
		},{
			header: 'Tipo de Factoraje',
			dataIndex: 'cc_tipo_factoraje', menuDisabled:true,
			width: 120, resizable: true,enableColumnMove:false,
			align: 'center'
		},{
			header: 'Clave EPO IF', tooltip: 'Clave EPO IF',
			dataIndex: 'ig_epo_externa', menuDisabled:true,
			sortable: false, editor: {xtype:'numberfield', maxLength:4, allowBlank:true},
			width: 120, resizable: true, enableColumnMove:false,
			align: 'center',
			renderer: function(value,metadata,registro){
				//if(value==""){
				NE.util.colorCampoEdit(value,metadata,registro);
				//}
				return value;
			}
		}],
		height: 350,
		width: 800,
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Guardar',
					iconCls: 'aceptar',
					handler: procesarGuardar
				},
				'-',
				{
					text: 'Cancelar',
					iconCls: 'cancelar',
					handler: function(btn){
						consCveEpoWsIfData.rejectChanges();
					}
				}
			]
		},
		listeners : {
			beforeedit: function(e){
				//e.grid.getStore().getTotalCount()
				/*if((e.row + 1) < e.grid.getStore().totalLength+ObjGeneral.contadorMod){
					return false;
				}*/
			}
		},
		border: false,  
		stripeRows: true
	});

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(10),
			gridCveEpoWSIF
		]
	});
	
	consCveEpoWsIfData.load();
});