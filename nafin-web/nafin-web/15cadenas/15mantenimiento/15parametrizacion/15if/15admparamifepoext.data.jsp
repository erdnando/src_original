<%@ page language="java" %>
<%@ page import="
	java.util.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<% 

/*** OBJETOS ***/
CQueryHelperRegExtJS queryHelper;
CatalogoSimple cat;
JSONObject jsonObj;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String informacion= (request.getParameter("informacion")	==null)?"":request.getParameter("informacion");
String operacion 	= (request.getParameter("operacion")	==null)?"":request.getParameter("operacion");
String sIcEpo 		= (request.getParameter("sIcEpo")		==null)?"":request.getParameter("sIcEpo").trim();
String sNomEpo 		= (request.getParameter("sNomEpo")		==null)?"":request.getParameter("sNomEpo").trim();
String sPlazoMin 	= (request.getParameter("plazoMin")		==null)?"":request.getParameter("plazoMin").trim();
String sPlazoMax 	= (request.getParameter("plazoMax")		==null)?"":request.getParameter("plazoMax").trim();
String sDias	 		= (request.getParameter("dias")			==null)?"":request.getParameter("dias").trim();
String sOperacion = (request.getParameter("operacion")	==null)?"":request.getParameter("operacion");
String resultado	= null;

int iNumReg 			= (request.getParameter("iNumReg")==null)?0:Integer.parseInt(request.getParameter("iNumReg"));
int iPlazoMin 		= (!sPlazoMax.equals(""))?Integer.parseInt(sPlazoMax)+1:1;
/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/



if(informacion.equals("pruebaModificados")){
    doPrintRequestParameters(request);
}









/*** INICIO CONSULTA ***/
if(informacion.equals("Consulta") || informacion.equals("ConsultaTotales")) { 	

	/*** INICIO DE AUTORIZA DETALLE DE PAGO ***/
	if(operacion.equals("Autorizar")){
		AccesoDB con 					= null;
		PreparedStatement ps 	= null;
		ResultSet rs 					= null;
		String qrySentencia		= "";
		String autorizados 		= (request.getParameter("autorizar")==null)?"":request.getParameter("autorizar");
		String[] arrAll 			= autorizados.split("[}]");
      
		String validaPagoAnticipado 		   = (request.getParameter("validaPagoAnticipado")==null)?"":request.getParameter("validaPagoAnticipado");
		String validaPagoAnticipadoS 		   = (request.getParameter("validaPagoAnticipadoS")==null)?"":request.getParameter("validaPagoAnticipadoS");
		String validaAutCtaClabe 		      = (request.getParameter("validaAutCtaClabe")==null)?"":request.getParameter("validaAutCtaClabe");
		String validaAutorizacion 		      = (request.getParameter("validaAutorizacion")==null)?"":request.getParameter("validaAutorizacion");
		String validaPagoAnticipadoSinCheck = (request.getParameter("validaPagoAnticipadoSinCheck")==null)?"":request.getParameter("validaPagoAnticipadoSinCheck");
		String validaPagoAnticipadoSinCheckS= (request.getParameter("validaPagoAnticipadoSinCheckS")==null)?"":request.getParameter("validaPagoAnticipadoSinCheckS");
      
      String[] pagoAnticipado = request.getParameterValues("pagoAnticipado");
      String[] pagoAnticipadoS = request.getParameterValues("pagoAnticipadoS");
      String[] pagoAnticipadoSinCheck = request.getParameterValues("pagoAnticipadoSinCheck");
      String[] pagoAnticipadoSinCheckS = request.getParameterValues("pagoAnticipadoSinCheckS");
      String[] autorizacionCtaClabe = request.getParameterValues("autorizacionCtaClabe");
      String[] autorizacion = request.getParameterValues("autorizacion");
      		
		try{
			con = new AccesoDB();
			con.conexionDB();
				
            System.err.println("pagoAnticipado.length " + pagoAnticipado.length);
            System.err.println("autorizacionCtaClabe.length " + autorizacionCtaClabe.length);
            System.err.println("autorizacion.length " + autorizacion.length);
            
         /* PAGO ANTICIP�DO SIN CHECK */
         if(validaPagoAnticipadoSinCheck.equals("true")){
            for(int i=0; i<pagoAnticipadoSinCheck.length; i++){			
               qrySentencia = " update comrel_if_epo set cs_pago_anticipado = 'N' "+
                  " where ic_if = " +iNoCliente+
                  " and ic_epo = " + pagoAnticipadoSinCheck[i];
               con.ejecutaSQL(qrySentencia);
               con.terminaTransaccion(true);
               con.cierraStatement();
               System.err.println("Pago Anticipado ::::::::: " + qrySentencia);
            }
         } 
                  
         if(validaPagoAnticipadoSinCheckS.equals("true")){
            for(int i=0; i<pagoAnticipadoSinCheckS.length; i++){			
               qrySentencia = " update comrel_if_epo set cs_pago_anticipado = 'N' "+
                  " where ic_if = " +iNoCliente+
                  " and ic_epo = " + pagoAnticipadoSinCheckS[i];
               con.ejecutaSQL(qrySentencia);
               con.terminaTransaccion(true);
               con.cierraStatement();
               System.err.println("Pago Anticipado S ::::::::: " + qrySentencia);
            }
         }
         
         /* PAGO ANTICIP�DO */
         if(validaPagoAnticipado.equals("true")){
            for(int i=0; i<pagoAnticipado.length; i++){			
               qrySentencia = " update comrel_if_epo set cs_pago_anticipado = 'S' "+
                  " where ic_if = " +iNoCliente+
                  " and ic_epo = " + pagoAnticipado[i];
               con.ejecutaSQL(qrySentencia);
               con.terminaTransaccion(true);
               con.cierraStatement();
               System.err.println("Pago Anticipado ::::::::: " + qrySentencia);
            }
         }
         if(validaPagoAnticipadoS.equals("true")){
            for(int i=0; i<pagoAnticipadoS.length; i++){			
               qrySentencia = " update comrel_if_epo set cs_pago_anticipado = 'S' "+
                  " where ic_if = " +iNoCliente+
                  " and ic_epo = " + pagoAnticipadoS[i];
               con.ejecutaSQL(qrySentencia);
               con.terminaTransaccion(true);
               con.cierraStatement();
               System.err.println("Pago Anticipado S ::::::::: " + qrySentencia);
            }
         }
				
         /* AUTORIZACI�N CUENTA CLABE */
         if(validaAutCtaClabe.equals("true")){
            for(int i=0; i<autorizacionCtaClabe.length; i++){			
               qrySentencia = " update comrel_if_epo set cs_autorizacion_clabe = 'S'" +
                     " where ic_if = " + iNoCliente +
                     " and ic_epo = " + autorizacionCtaClabe[i] + "";
                  con.ejecutaSQL(qrySentencia);
               con.terminaTransaccion(true);
               con.cierraStatement();
               System.err.println("Aut Cta Clabe ::::::::: " + qrySentencia);
            }
         }
				
         /* AUTORIZACI�N */
         if(validaAutorizacion.equals("true")){
            for(int i=0; i<autorizacion.length; i++){			
               qrySentencia = " update comrel_if_epo set cs_aceptacion = 'S' " +
                     " where ic_if = " + iNoCliente +
                     " and ic_epo = " + autorizacion[i] + "";
                     
                  con.ejecutaSQL(qrySentencia);
               con.terminaTransaccion(true);
               con.cierraStatement();
               System.err.println("Autorizacion ::::::::: " + qrySentencia);
            }
         }
         
		}catch(Exception e) {
			out.println(e.getMessage()); 
			e.printStackTrace();
		}finally {	
			if(con.hayConexionAbierta()){ 
				con.cierraConexionDB();	
			}
		}						
	}/*** FIN DE ACTUALIZA DETALLE DE PAGO ***/

	CtasBancariasEpo paginador = new CtasBancariasEpo();
	paginador.setINoCliente(iNoCliente);
	queryHelper	= new CQueryHelperRegExtJS( paginador ); 
	
	if(informacion.equals("Consulta")){
		Registros registros = queryHelper.doSearch();
		HashMap datos;
		List reg=new ArrayList();

		while(registros.next()){
			datos=new HashMap();
			datos.put("CG_RAZON_SOCIAL",registros.getString("cg_razon_social"));
			datos.put("CG_BANCO",registros.getString("cg_banco"));
			datos.put("TOTAL_DETALLES_REG", registros.getString(8));
			datos.put("CG_NUM_CUENTA",registros.getString("cg_num_cuenta"));
			datos.put("CS_ACEPTACION",registros.getString("cs_aceptacion"));
			datos.put("IC_EPO",registros.getString("ic_epo"));
			datos.put("CS_PAGO_ANTICIPADO",registros.getString("cs_pago_anticipado"));
			datos.put("DET_CAPTURADOS",Integer.parseInt(registros.getString(8))>=1 ? "S" : "N");
			datos.put("CG_CUENTA_CLABE",registros.getString("cg_cuenta_clabe"));
			datos.put("CS_AUTORIZACION_CLABE",registros.getString("cs_autorizacion_clabe"));
			datos.put("CHECKBOX", registros.getString(7).equals("S") ? "S" : "");
			reg.add(datos);
		}
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		resultado=jsonObj.toString();
	}
} /*** FIN DE CONSULTA ***/

/*** INICIO DE CONSULTA DETALLE DE PAGO ***/
else if(informacion.equals("ConsultaDetPago")){
	String records		= (request.getParameter("records")==null)	?"":request.getParameter("records");
	String registros	= (request.getParameter("records")==null)	?"":request.getParameter("records");
	String accion 		= (request.getParameter("accion")==null)	?"":request.getParameter("accion");
	String mins 		= (request.getParameter("min")==null)		?"0":request.getParameter("min");
	String maxs 		= (request.getParameter("max")==null)		?"0":request.getParameter("max");
	String diass 		= (request.getParameter("dias")==null)		?"0":request.getParameter("dias");
	
	System.out.println("\n\n\n\nREGISTROS: " + registros + "\n\n\n");
	
	jsonObj 			      = new JSONObject();
	JSONArray reg			= new JSONArray();
	HashMap datos        = new HashMap();
	
	CQueryHelper queryHelper2;
	
	AccesoDB con 			= null;
	PreparedStatement ps = null;
	ResultSet rs 			= null;
	String min				= "0";
	String max				= "0"; 
	String dias				= "0";
	String qrySentencia	= "";
	int cont 					= 0;
		
	/*** INSERTA DATOS EN LA DB ***/
	if(operacion.equals("ModificaDetPago")){
		try{
			con = new AccesoDB();
			con.conexionDB();
			if(accion.equals("Inserta")){	
				List arrRegistros = JSONArray.fromObject(registros);
				String seleccionados[]	= new String[arrRegistros.size()];
				Iterator itReg = arrRegistros.iterator();			
					
					System.out.println("\n\n\n Registros::: "+registros);
					while (itReg.hasNext()) {
						JSONObject registro = (JSONObject)itReg.next();
						
						qrySentencia = "insert into com_pago_antic_detalle (ic_if, ic_epo, "+
							" in_plazo_minimo, in_plazo_maximo, in_dia) values ('"+
							iNoCliente+"','"+sIcEpo+"','"+registro.getString("MIN")+"','"+registro.getString("MAX")+"','"+registro.getString("DIAS")+"')";
													
               
                  System.err.println("..::::: Accion Inserta ::::.. " + qrySentencia + "\n-------------------------------------------");
               
						con.ejecutaSQL(qrySentencia);
						con.terminaTransaccion(true);
					}
					
			}else if(accion.equals("Elimina")){            
					qrySentencia = "delete from com_pago_antic_detalle where " +
					"ic_if='" + iNoCliente + "' and ic_epo='" + sIcEpo + "' "+
					"and in_plazo_minimo='"+mins+"' and in_plazo_maximo='"+maxs+"' and in_dia='"+diass+"'";
               
               System.err.println("..::::: Accion Eliminar ::::.. " + qrySentencia + "\n-------------------------------------------");
               
				con.ejecutaSQL(qrySentencia);
				con.terminaTransaccion(true);
			}
			
		}catch(Exception e) {
			out.println(e.getMessage()); 
			e.printStackTrace();
		}finally {	
			if(con.hayConexionAbierta()){ 
				con.cierraConexionDB();	
			}
		}
	}/*** FIN INSERTA DATOS EN LA DB ***/
	
	try{
		con = new AccesoDB();
		con.conexionDB();
		queryHelper2 = new CQueryHelper(new ClausuladoNafinCA());
		queryHelper2.cleanSession(request);
		
		qrySentencia =
			"select in_plazo_minimo, in_plazo_maximo, in_dia "+
			"from com_pago_antic_detalle "+
			"where ic_if = '"+iNoCliente+"'"+
			" and ic_epo = '"+sIcEpo+"'"+
			" order by in_plazo_minimo";
		
      System.err.println("qrySentencia " + qrySentencia);
      		
		ps = con.queryPrecompilado(qrySentencia);		
		rs = ps.executeQuery(); 
			
		while(rs.next()){
			min = rs.getString(1)==null?"0":rs.getString(1);
			max = rs.getString(2)==null?"":rs.getString(2);
			dias = rs.getString(3)==null?"":rs.getString(3);
			datos = new HashMap();
			datos.put("MAX", max);
			datos.put("MIN", min);
			datos.put("DIAS", dias);
			datos.put("PLUS", "+");
			reg.add(datos);
			cont++;
		} 
		rs.close();
		ps.close();		
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
	
	datos = new HashMap();
	datos.put("MAX", "");
	datos.put("MIN", Integer.toString(Integer.parseInt(max)+1));
	datos.put("DIAS", "");
	datos.put("PLUS", "");
	reg.add(datos);	
	String resultado2	= "{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
	//jsonObj = JSONObject.fromObject(resultado2);	
	resultado = resultado2;
}
/*** FIN DE CONSULTA DETALLE DE PAGO ***//*** INICIO DE CONSULTA DETALLE DE PAGO ***/

%>




<%= resultado%>



 <%!

	private void doPrintRequestParameters(HttpServletRequest request){
      Enumeration paramNames = request.getParameterNames();
      while(paramNames.hasMoreElements()) {
         String paramName = (String)paramNames.nextElement();
         System.out.print(" ---> " + paramName + " = "); 
         String[] paramValues = request.getParameterValues(paramName);
         if (paramValues.length == 1) { 
           String paramValue = paramValues[0];
           if (paramValue.length() == 0)
             System.out.println("<SIN VALOR>");
           else    
             System.out.println("<"+paramValue+">");
         } else {
           for(int i=0; i<paramValues.length; i++) {
             System.out.println("<" + paramValues[i] + ">"); 
           }       

         }       
       }       
   }
   
%>