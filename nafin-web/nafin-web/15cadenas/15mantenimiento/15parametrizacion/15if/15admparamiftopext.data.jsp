<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.model.catalogos.*"
	errorPage = "/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../../../015secsession.jspf" %>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar 	= "";

if(informacion.equals("CatalogoTasaOper")) {
	// Obtiene el catálogo de Tasas
	CatalogoTasa cat = new CatalogoTasa();
	cat.setCampoClave("ic_tasa");
	cat.setCampoDescripcion("cd_nombre"); 
    cat.setDisponible("S");
    cat.setOrden("2");
	mensaje_param.getMensaje("15admparamiftoext.SeleccioneTasa",sesIdiomaUsuario);
	infoRegresar = cat.getJSONElementos();
}else if (informacion.equals("Consulta") || informacion.equals("GenerarArchivo")){
	// Envia los parámetros de consulta y muestra los resultados...
	String idTasa		=	request.getParameter("idTasa")		!=null ? request.getParameter("idTasa")		:"";
	String fechaInicio	=	request.getParameter("fecha_Inicio")!=null ? request.getParameter("fecha_Inicio"):"";
	String fechaFin		=	request.getParameter("fecha_Fin")	!=null ? request.getParameter("fecha_Fin")	:"";
		
	com.netro.cadenas.ConsTasasOper paginador	=	new com.netro.cadenas.ConsTasasOper();	
	paginador.setFechaInicio(fechaInicio);
	paginador.setFechaFin(fechaFin);
	paginador.setIcTasa(idTasa);
			
	CQueryHelperRegExtJS queryHelper	=	new CQueryHelperRegExtJS( paginador ); 
			
	if(informacion.equals("Consulta")){
		try {
			Registros reg	=	queryHelper.doSearch();
			infoRegresar	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("GenerarArchivo")) {
		try {
			String nombreArchivo=	queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj 	= new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}catch(Throwable e){
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}
}
%>
<%=infoRegresar%>
