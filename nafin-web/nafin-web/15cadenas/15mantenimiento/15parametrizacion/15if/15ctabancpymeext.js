Ext.onReady(function() {
var bandera=true;
var contGrid=0;
var formularios=new Array();
var terminaFormularios=false;
var elementoEliminar=0;
var idActual=0;
var elementoModificar;
var cuantasAceptadas=true;
var mensaje;
//--------------Handlers---------------------------

function IsNumeric(sText)
{
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   }
var procesarCuenta = function(opts, success, response){
	if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
		Ext.getCmp('sucursal1').setValue(Ext.util.JSON.decode(response.responseText).cg_sucursal);
		Ext.getCmp('noCuenta1').setValue(Ext.util.JSON.decode(response.responseText).cg_numcta);
		Ext.getCmp('plaza1').setValue(Ext.util.JSON.decode(response.responseText).ic_plaza);
	
	}

}

var cargaDatos = function(){
		if(bandera)
		
		if(Ext.getCmp('icEpo1').getValue()!=''&&Ext.getCmp('rfc1').getValue()!=''&&Ext.getCmp('icDist1').getValue()!=''
		&&Ext.getCmp('cbMoneda1').getValue()!=''&&Ext.getCmp('cbBanco1').getValue()!=''){
			
			Ext.Ajax.request({
				url: '15ctabancpymeext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'CuentaExiste',
					icDist: Ext.getCmp('icDist1').getValue(),
					icEpo: Ext.getCmp('icEpo1').getValue(),
					cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
					cbBanco : Ext.getCmp('cbBanco1').getValue(),
					rfc : Ext.getCmp('rfc1').getValue(),
					plaza : Ext.getCmp('plaza1').getValue()	
				}),
				callback: procesarCuenta
				});
		}
}

var procesarAceptar = function(opts, success, response){
	if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
		terminaFormularios =true;
		cuantasAceptadas=true;	
	}else{
		cuantasAceptadas=false;					
	}
			
		if(cuantasAceptadas){
				if(terminaFormularios){
					Ext.Msg.show({
						title:	'',
						msg:		Ext.util.JSON.decode(response.responseText).mensaje,
						buttons:	Ext.Msg.OK,
						fn: leeRespuesta,
						closable:false,
						icon: Ext.MessageBox.INFO
					});
				}
			}else{
				if(terminaFormularios){
					Ext.Msg.show({
						title:	'',
						msg:		'Una o m�s de las cuentas no pudo confirmarse',
						buttons:	Ext.Msg.OK,
						fn: leeRespuesta,
						closable:false,
						icon: Ext.MessageBox.INFO
					});
				}
			}
			
}

function leeRespuesta(){
		location.reload();
	}
	
		var procesaActualizar = function(opts, success, response){
			winActualiza.hide();
			if(bandera){
				var registro = grid.getStore().getAt(elementoModificar);

				registro.set('IC_PLAZA',Ext.getCmp('acPlaza1').getValue());
				registro.set('PLAZA',Ext.getCmp('acPlaza1').lastSelectionText);
				registro.set('NOCUENTA',Ext.getCmp('actCuenta1').getValue());
				registro.set('SUCURSAL',Ext.getCmp('actSucursal1').getValue());
				
			}else{
				consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
					bandera:bandera,
					icDist: Ext.getCmp('icDist1').getValue(),
					icEpo: Ext.getCmp('icEpo1').getValue(),
					cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
					plaza : Ext.getCmp('plaza1').getValue(),
					cbBanco : Ext.getCmp('cbBanco1').getValue()
				})})
			}
			Ext.Msg.show({
						title:	'',
						msg:		Ext.util.JSON.decode(response.responseText).mensaje,
						buttons:	Ext.Msg.OK,
						//fn: leeRespuesta,
						closable:false,
						icon: Ext.MessageBox.INFO
					});
	
	}
	
	var procesaConsulta = function(store,arrRegistros,opts){
	pnl.el.unmask();
	
			if(arrRegistros!=null){
			grid.show();
					var el = grid.getGridEl();
					Ext.getCmp('btnBajarCSV').hide();
					Ext.getCmp('btnBajarPDF').hide();
					if(store.getTotalCount()>0){
							el.unmask();
							Ext.getCmp('btnGenerarCSV').enable();
							Ext.getCmp('btnGenerarPDF').enable();
					
					
					for(var j=0;j<store.getTotalCount();j++){
						var registro = grid.getStore().getAt(j);
						formularios[j]=fp.getForm().getValues();
						formularios[j].icEpo=registro.get('IC_EPO');//Ext.getCmp('icEpo').getValue();
						formularios[j].rfc=registro.get('RFC');//Ext.getCmp('rfc').getValue();
						formularios[j].icDist=registro.get('IC_PYME');//Ext.getCmp('icDist').getValue();
						formularios[j].cbMoneda=registro.get('IC_MONEDA');//Ext.getCmp('cbMoneda').getValue();
						formularios[j].Banco=registro.get('IC_BANCO');//Ext.getCmp('cbBanco').getValue();
						formularios[j].noCuenta=registro.get('NOCUENTA');//Ext.getCmp('noCuenta').getValue();
						formularios[j].sucursal=registro.get('SUCURSAL');//Ext.getCmp('sucursal').getValue();
						formularios[j].plaza=registro.get('IC_PLAZA');//Ext.getCmp('plaza').getValue();						
						contGrid++;					
						}
					
					}else{
						el.mask('No se encontr� ning�n registro', 'x-mask');
						Ext.getCmp('btnGenerarCSV').disable();
						Ext.getCmp('btnGenerarPDF').disable();
			}
	}
}

var procesarSuccessFailureGenerarCSV = function(opts, success, response){
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarCSV.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
		}


var procesarSuccessFailureGenerarPDF = function(opts,success,response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	
var procesaModificar = function(grid, rowIndex, colIndex, item, event) {
		idActual=grid.getStore().getAt(rowIndex).get('ID');
		winActualiza.show();
		elementoModificar=rowIndex;
		var registro = grid.getStore().getAt(rowIndex);
		Ext.getCmp('idTexEpo1').setValue(registro.get('NOMBREEPO'));
		Ext.getCmp('idTexDist1').setValue(registro.get('DISTRIBUIDOR'));
		Ext.getCmp('idTexNumPyme1').setValue(registro.get('NEPYME'));
		Ext.getCmp('idTexBanco1').setValue(registro.get('BANCO'));
		Ext.getCmp('actSucursal1').setValue(registro.get('SUCURSAL'));
		Ext.getCmp('idTexMoneda1').setValue(registro.get('MONEDA'));
		Ext.getCmp('actCuenta1').setValue(registro.get('NOCUENTA'));
		Ext.getCmp('acPlaza1').setValue(registro.get('IC_PLAZA'));
	}
	
	var procesaEliminar = function(grid, rowIndex, colIndex, item, event) {
		Ext.Msg.show({
			title:'',
			msg: '�Est� seguro de que desea Eliminar la cuenta?',
			buttons: Ext.Msg.OKCANCEL,
			fn: function (res){
				if (res == 'ok' || res == 'yes'){
					elementoEliminar=rowIndex;
					Ext.Ajax.request({
								url: '15ctabancpymeext.data.jsp',
								params: Ext.apply({//formularios[rowIndex],{
													informacion: 'Eliminar',
													CtasCapturadas:grid.getStore().getAt(rowIndex).get('ID')
												}),
						callback: successEliminar
							});
				}
			
			
			
			},
			animEl: 'elId',
			icon: Ext.MessageBox.QUESTION
		})
		

	}
var successEliminar= function(opts, success, response){
			
			if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
				grid.getStore().removeAt(elementoEliminar);
				contGrid--;
				
				}
			if(!bandera)
			consulta.load({ 
				params: Ext.apply(fp.getForm().getValues(),{
					bandera:bandera,
					icDist: Ext.getCmp('icDist1').getValue(),
					icEpo: Ext.getCmp('icEpo1').getValue(),
					cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
					plaza : Ext.getCmp('plaza1').getValue(),
					cbBanco : Ext.getCmp('cbBanco1').getValue()
					
				}
			)})

			Ext.Msg.show({
						title:	'',
						msg:		Ext.util.JSON.decode(response.responseText).mensaje,
						buttons:	Ext.Msg.OK,
						//fn: leeRespuesta,
						closable:false,
						icon: Ext.MessageBox.INFO
					});
			if(grid.getStore().getTotalCount()==0){
						leeRespuesta();
					}
}
	


var procesaCampos = function(opts, success, response){
			Ext.getCmp('rfc1').setValue();
			Ext.getCmp('ne1').setValue();
			Ext.getCmp('icDist1').setValue();
			if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
				
				Ext.getCmp('rfc1').setValue(Ext.util.JSON.decode(response.responseText).CG_RFC);
				Ext.getCmp('ne1').setValue(Ext.util.JSON.decode(response.responseText).IC_NAFIN_ELECTRONICO);
				Ext.getCmp('icDist1').setValue(Ext.util.JSON.decode(response.responseText).IC_PYME);
			}else{
				Ext.Msg.alert("Error",mensaje+"<br/>1)El Distribuidor no existe o <br/>2)El Distribuidor no tiene una cuenta autorizada");
				
			}
}

Ext.apply(Ext.form.VTypes,{  
	
   numeros: function(value,field){  
	if(IsNumeric(value))
    return value;  
},  
numerosText: 'Debes de introducir solo numeros',  
numerosMask: /[ \d]/  
  
  
});  

 
var procesarInsertarCta= function(opts, success, response){
	if( Ext.util.JSON.decode(response.responseText).success == true){
	
		consulta.load({
			params: Ext.apply( {  
				bandera: bandera,						
				icEpo: Ext.getCmp('icEpo1').getValue(),						
				icDist: Ext.getCmp('icDist1').getValue(),
				cbMoneda: Ext.getCmp('cbMoneda1').getValue()
			})
		});				
		Ext.MessageBox.alert('Mensaje',Ext.util.JSON.decode(response.responseText).mensaje);
		
		fp.getForm().reset();				
	}else{
		Ext.MessageBox.alert("Error",Ext.util.JSON.decode(response.responseText).mensaje);
			if(Ext.util.JSON.decode(response.responseText).mensaje=='Ya existe una cuenta para ese Distribuidor'){
				consulta.load({
					params: Ext.apply( {  
						bandera: bandera,						
						icEpo: Ext.getCmp('icEpo1').getValue(),						
						icDist: Ext.getCmp('icDist1').getValue(),
						cbMoneda: Ext.getCmp('cbMoneda1').getValue()
					})
				});
			}
	}
}


//---------------STORES-------------------------------
	var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ctabancpymeext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var consulta = new Ext.data.JsonStore({
		root: 'registros',
		url: '15ctabancpymeext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
			
		},
		fields: [
					{name: 'NOMBREEPO'},
					{name: 'BANCO'},
					{name: 'DISTRIBUIDOR'},
					{name: 'RFC'},
					{name: 'SUCURSAL'},
					{name: 'MONEDA'},
					{name: 'NOCUENTA'},
					{name: 'PLAZA'},
					{name: 'AUTORIZADO'},
					{name: 'NEPYME'},
					{name: 'ID'},
					{name: 'IC_EPO'},
					{name: 'IC_PYME'},
					{name: 'IC_BANCO'},
					{name: 'IC_MONEDA'},
					{name: 'IC_PLAZA'}
					
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesaConsulta,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							
					}
		}
	}
	});

	var catalogoDist = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ctabancpymeext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoDist'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError
		}
  });

	var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catologoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ctabancpymeext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

	var catalogoBanco = new Ext.data.JsonStore
  ({
	   id: 'catologoBanco',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ctabancpymeext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoBanco'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

	var catalogoPlaza = new Ext.data.JsonStore
  ({
	   id: 'catologoPlaza',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15ctabancpymeext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoPlaza'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
//---------------COMPONENTES-------------------------



var winActualiza = new Ext.Window ({
		id:'winActualiza',
		//height: 285,
		//x: 300,
		//y: 100,
		width: 600,
		heigth: 500,
		modal: true,
		closeAction: 'hide',
		title: 'Actializa Cuenta Bancaria',
		items:[
			{
				xtype:'form',
				id:'fpWinBusca',
				frame: true,
				border: false,
				style: 'margin: 0 auto',
				bodyStyle:'padding:10px',
				defaults: {
					msgTarget: 'side',
					anchor: '-20'
				},
				labelWidth: 0,
				items:[
				//Campo 1
					{
						xtype: 'compositefield',
						combineErrors: false,				
						forceSelection: true,
						msgTarget: 'side',
						items: [
							
							{
							xtype: 'displayfield',
							value: 'Nombre de la EPO:',
							width: 150
							},
							{
							xtype: 'displayfield',
							id: 'idTexEpo1',
							width: 150
							}			
						]
					},
					//Campo 2
					{
						xtype: 'compositefield',
						combineErrors: false,				
						forceSelection: true,
						msgTarget: 'side',
						items: [
							{
							xtype: 'displayfield',
							value: 'Nombre del Distribuidor:',
							width: 150
							},
							{
							xtype: 'displayfield',
							value: '',
							id: 'idTexDist1',
							width: 150
							}			
						]
					},
					//Campo3
					{
						xtype: 'compositefield',
						combineErrors: false,				
						forceSelection: true,
						msgTarget: 'side',
						items: [
							{
							xtype: 'displayfield',
							value: 'N�m. NE Pyme:',
							width: 150
							},
							{
							xtype: 'displayfield',
							value:'',
							id: 'idTexNumPyme1',
							width: 150
							}			
						]
					},
					//Campo 4
					{
						xtype: 'compositefield',
						combineErrors: false,				
						forceSelection: true,
						msgTarget: 'side',
						items: [
							{
							xtype: 'displayfield',
							value: 'Banco de Servicio:',
							width: 150
							},
							{
							xtype: 'displayfield',
							value:'',
							id: 'idTexBanco1',
							width: 150
							}			
						]
					},
					//Campo 5
					{
						xtype: 'compositefield',
						combineErrors: false,				
						forceSelection: true,
						msgTarget: 'side',
						items: [
						{
							xtype: 'displayfield',
							value: 'Sucursal1',
							width: 150
							},
							
							{
						xtype: 'textfield',
						name: 'suc',
						vtype: 'numeros',
						forceSelection: true,
						id:	'actSucursal1',
						fieldLabel:'',
						width: 150,
						maxLength:	100
						}]
						},
					
					{
						xtype: 'compositefield',
						combineErrors: false,				
						forceSelection: true,
						msgTarget: 'side',
						items: [
							{
							xtype: 'displayfield',
							value: 'Moneda',
							width: 150
							},
							{
							xtype: 'displayfield',
							value:'',
							id: 'idTexMoneda1',
							width: 150
							}
							
						]
					},
					{
						xtype: 'compositefield',
						combineErrors: false,				
						forceSelection: true,
						msgTarget: 'side',
						items: [
						{
							xtype: 'displayfield',
							value: 'No. de Cuenta',
							width: 150
							},
							{
							xtype: 'textfield',
							name: 'Cuenta',
							id:	'actCuenta1',
							vtype: 'numeros',
							fieldLabel:'',
							width: 150,
							forceSelection: true,
							maxLength:	20
							}
						]
						},
						{
						xtype: 'compositefield',
						combineErrors: false,				
						forceSelection: true,
						msgTarget: 'side',
						items: [
						{
							xtype: 'displayfield',
							value: 'Plaza',
							width: 150
						},
						{
						xtype: 'combo',
						fieldLabel: '',
						displayField: 'descripcion',
						valueField: 'clave',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						name:'acPlaza',
						id: 'acPlaza1',
						mode: 'local',
						forceSelection : true,
						allowBlank: false,
						hiddenName: 'Plaza',
						hidden: false,
						emptyText: 'Seleccionar una Plaza',
						store: catalogoPlaza,
						tpl: NE.util.templateMensajeCargaCombo
						}]
					}
					
				],
				buttons:[
					{
						text:'Actualizar',
						iconCls:'',
						handler: function(boton) {
							if(Ext.getCmp('actCuenta1').isValid()){
								if(Ext.getCmp('actSucursal1').isValid()){
										Ext.Ajax.request({
											url: '15ctabancpymeext.data.jsp',
											params: {
												plaza:Ext.getCmp('acPlaza1').getValue(),
												noCuenta:Ext.getCmp('actCuenta1').getValue(),
												sucursal:Ext.getCmp('actSucursal1').getValue(),
												CtasCapturadas:idActual,
												informacion: 'Actualizar'
											},
											callback: procesaActualizar
										});
						 // catalogoNombreData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
									}else
									{
										Ext.getCmp('actSucursal1').focus();
									}
								}else{
									Ext.getCmp('actCuenta1').focus();
								}
								}
					},{
						text:'Cancelar',
						iconCls: 'icoLimpiar',
						handler: function() {
										Ext.getCmp('winActualiza').hide();
									}
					}
				]
			}
		]
	});


var elementosForma = [
	{
				//EPO
				xtype: 'combo',
				fieldLabel: 'Nombre de la EPO',
				emptyText: 'Seleccionar una EPO',
				displayField: 'descripcion',
				allowBlank: false,
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'icEpo',
				id: 'icEpo1',
				mode: 'local',				
				forceSelection: true,
				tpl:  '<tpl for=".">' +
						'<tpl if="!Ext.isEmpty(loadMsg)">'+
						'<div class="loading-indicator">{loadMsg}</div>'+
						'</tpl>'+
						'<tpl if="Ext.isEmpty(loadMsg)">'+
						'<div class="x-combo-list-item">' +
						'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
						'</div></tpl></tpl>',
				listeners: {
							select: {
								fn: function(combo) {
									cargaDatos();
									Ext.getCmp('rfc1').setValue();
									Ext.getCmp('ne1').setValue();
									Ext.getCmp('icDist1').setValue();
									var cEpo = combo.getValue();
									var cDist = Ext.getCmp('icDist1');
									cDist.setValue('');
									cDist.store.removeAll();
						
									cDist.store.load({
											params: {
												icEpo: combo.getValue(),
												bandera:bandera
											}
									});
								
								}
							}
						}
    },{
				xtype: 'textfield',
				name: 'rfc',
				id:	'rfc1',
				forceSelection: true,
				fieldLabel:'RFC Distribuidor',
				width: 100,
				maxLength:	25,
				listeners: {
									change: {
									
									fn:function(combo) {
									if(bandera&&Ext.getCmp('icEpo1').getValue()!=''){
									mensaje='El RFC: '+combo.getValue();
									cargaDatos();
									Ext.getCmp('ne1').setValue();
									Ext.getCmp('icDist1').setValue();
									Ext.Ajax.request({
												url: '15ctabancpymeext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'informacionDist',
													icDist: Ext.getCmp('icDist1').getValue(),
													icEpo: Ext.getCmp('icEpo1').getValue(),
													cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
													plaza : Ext.getCmp('plaza1').getValue(),
													cbBanco : Ext.getCmp('cbBanco1').getValue() 
												}),
												callback: procesaCampos
												});
									
											}
										}
									}
								}
			},{
				xtype: 'compositefield',
				fieldLabel: 'Nombre del Distribuidor',
				combineErrors: false,				
				forceSelection: true,
				msgTarget: 'side',
				items: [
					{
						//distribuidor
						xtype: 'combo',
						width: 230,
						emptyText: 'Seleccionar un Distribuidor',
						displayField: 'descripcion',
						valueField: 'clave',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: catalogoDist,
						tpl: NE.util.templateMensajeCargaCombo,
						name:'icDist',
						id: 'icDist1',
						mode: 'local',						
						allowBlank: false,
						forceSelection: true,
						tpl:  '<tpl for=".">' +
						'<tpl if="!Ext.isEmpty(loadMsg)">'+
						'<div class="loading-indicator">{loadMsg}</div>'+
						'</tpl>'+
						'<tpl if="Ext.isEmpty(loadMsg)">'+
						'<div class="x-combo-list-item">' +
						'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
						'</div></tpl></tpl>',
						listeners: {
									select: {
									
									fn:function(combo) { 
									if(bandera&&Ext.getCmp('icEpo1').getValue()!=''){
									cargaDatos();
									Ext.getCmp('rfc1').setValue();
									Ext.getCmp('ne1').setValue();
									mensaje='El Distribuidor: '+combo.getValue();
									Ext.Ajax.request({
												url: '15ctabancpymeext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'informacionDist',
													icDist: Ext.getCmp('icDist1').getValue(),
													icEpo: Ext.getCmp('icEpo1').getValue(),
													cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
													plaza : Ext.getCmp('plaza1').getValue(),
													cbBanco : Ext.getCmp('cbBanco1').getValue()
												}),
												callback: procesaCampos
												});
												
											}
										}
									}
								}
				},{
						xtype: 'displayfield',
						value: 'N@E Distribuidor',
						width: 70
					},{
						xtype: 'textfield',
						vtype: 'numeros',
						name: 'ne',
						id:	'ne1',
						width: 70,
						maxLength:	25,
						listeners: {
									change: {
									
									fn:function(combo) { 
									if(bandera&&Ext.getCmp('icEpo1').getValue()!=''){
									mensaje='El N@E: '+combo.getValue();
									Ext.getCmp('rfc1').setValue();
									Ext.getCmp('icDist1').setValue();
									Ext.Ajax.request({
												url: '15ctabancpymeext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'informacionDist',
													icDist: Ext.getCmp('icDist1').getValue(),
													icEpo: Ext.getCmp('icEpo1').getValue(),
													cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
													plaza : Ext.getCmp('plaza1').getValue(),
													cbBanco : Ext.getCmp('cbBanco1').getValue()
												}),
												callback: procesaCampos
											});
											
											}
										}
									}
								}
					}
		]},
		{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			name:'cbMoneda',
			id: 'cbMoneda1',
			mode: 'local',
			forceSelection : true,
			allowBlank: false,			
			hidden: false,
			emptyText: 'Seleccionar Moneda',
			store: catalogoMoneda,
			tpl: NE.util.templateMensajeCargaCombo,
			listeners: {
									select: {
									
									fn:function(combo) { 
									if(bandera){
									cargaDatos();
									}
								}
							}
						}
		},{
			xtype: 'combo',
			fieldLabel: 'Banco de servicio',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			name:'cbBanco',
			id: 'cbBanco1',
			mode: 'local',
			forceSelection : true,
			allowBlank: false,			
			hidden: false,
			emptyText: 'Seleccionar Banco',
			store: catalogoBanco,
			tpl: NE.util.templateMensajeCargaCombo,
			listeners: {
									select: {
									
									fn:function(combo) { 
									if(bandera){
									cargaDatos();
									}
								}
							}
						}
		},{
			xtype: 'textfield',
			name: 'noCuenta',
			id:	'noCuenta1',
			vtype: 'numeros',
			fieldLabel:'No. de Cuenta',
			width: 100,
			forceSelection: true,
			maxLength:	20
			},{
			xtype: 'textfield',
			name: 'sucursal',
			vtype: 'numeros',
			forceSelection: true,
			id:	'sucursal1',
			fieldLabel:'Sucursal',
			width: 100,
			maxLength:	100
			},{
			xtype: 'combo',
			fieldLabel: 'Plaza',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			name:'plaza',
			id: 'plaza1',
			mode: 'local',
			forceSelection : true,
			allowBlank: false,			
			hidden: false,
			emptyText: 'Seleccionar una Plaza',
			store: catalogoPlaza,
			tpl: NE.util.templateMensajeCargaCombo
		}
];

var grid = new Ext.grid.GridPanel({
	id:'grid',
	header:true,
	store: consulta,
	style: 'margin:0 auto;',
	hidden: true,
	stripeRows: true,
	loadMask: true,
	height:400,
	width: 940,
	frame: true,
	columns:[
				//CAMPOS DEL GRID
						{
						header:'EPO',
						tooltip: 'EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'center',
						renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
								}
						},
						{
						align: 'center',
						header: 'Banco de Servicio',
						tooltip: 'Banco de Servicio',
						sortable: true,
						dataIndex: 'BANCO',
						width: 150,
						renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
								}
						},
						{
						align: 'center',
						header: 'Distribuidor',
						tooltip: 'Distribuidor',
						sortable: true,
						dataIndex: 'DISTRIBUIDOR',
						width: 150
						},
						{
						align: 'center',
						header: 'RFC',
						tooltip: 'RFC',
						sortable: true,
						dataIndex: 'RFC',
						width: 140
						},
						{
						align: 'center',
						header: 'Sucursal',
						tooltip: 'Sucursal',
						sortable: true,
						dataIndex: 'SUCURSAL',
						width: 130
						},
						{
						align: 'center',
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'MONEDA',
						width: 130
						},
						{
						align: 'center',
						header: ' No. de Cuenta',
						tooltip: ' No. de Cuenta',
						sortable: true,
						dataIndex: 'NOCUENTA',
						width: 130
						},
						{
						align: 'center',
						header: 'Plaza',
						tooltip: 'Plaza',
						sortable: true,
						dataIndex: 'PLAZA',
						width: 140
						},
						{
						align: 'center',
						header: 'Autorizado',
						tooltip: 'Autorizado',
						sortable: true,
						dataIndex: 'AUTORIZADO',
						width: 130
						},
						{//10
							xtype:	'actioncolumn',
							header : 'Seleccionar', tooltip: 'Seleccionar',
							dataIndex : '',align: 'center',
							sortable : true,	width : 80, hidden: false,	//renderer: renderButton
							items: [
								{
									getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
											this.items[0].tooltip = 'Modificar';
											
											return 'modificar';
									},
									handler:	procesaModificar
								},{
									getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
											this.items[1].tooltip = 'Eliminar';
											return 'borrar';
									},
									handler:	procesaEliminar
								}
							]
						}
						],
						bbar: {
							
							id: 'barra',
							displayInfo: true,
							items: [
								'->','-',
								
									{
										xtype: 'button',
										text: 'Confirmar',
										id: 'btnConfirmar',
										handler: function(boton, evento) {
	
											boton.setIconClass('loading-indicator');
											for(var i=0;i<formularios.length;i++){
											
												if(formularios.length>1){
													terminaFormularios=true;
												}
											var registro = grid.getStore().getAt(i);
											if(registro.get('AUTORIZADO')=='No'){	
											
												Ext.Ajax.request({
													url: '15ctabancpymeext.data.jsp',
													params: Ext.apply(formularios[i],{
														informacion: 'Aceptar',
														CtasCapturadas:registro.get('ID'),
														icDist: Ext.getCmp('icDist1').getValue(),
														icEpo: Ext.getCmp('icEpo1').getValue(),
														cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
														plaza : Ext.getCmp('plaza1').getValue(),	
														cbBanco : Ext.getCmp('cbBanco1').getValue()	
														
													}),
													callback: procesarAceptar
												});
											}
										}
									}
								},
				
									{
										xtype: 'button',
										text: 'Generar archivo',
										iconCls: '',
										id: 'btnGenerarCSV',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
												url: '15ctabancpymeext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'Consulta',
													operacion: 'ArchivoCSV',
													icDist: Ext.getCmp('icDist1').getValue(),
													icEpo: Ext.getCmp('icEpo1').getValue(),
													cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
													plaza : Ext.getCmp('plaza1').getValue(),
													cbBanco : Ext.getCmp('cbBanco1').getValue()
												}),
												callback: procesarSuccessFailureGenerarCSV
											});
										}
									},{
										xtype: 'button',
										text: 'Bajar archivo',
										id: 'btnBajarCSV',
										hidden: true
									},'-',
									{
										xtype: 'button',
										text: 'Generar PDF',
										iconCls: '',
										id: 'btnGenerarPDF',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
												url: '15ctabancpymeext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'Consulta',
													operacion: 'ArchivoPDF',
													icDist: Ext.getCmp('icDist1').getValue(),
													icEpo: Ext.getCmp('icEpo1').getValue(),
													cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
													plaza : Ext.getCmp('plaza1').getValue(),
													cbBanco : Ext.getCmp('cbBanco1').getValue()
												}),
												callback: procesarSuccessFailureGenerarPDF
											});
										}
									},{
										xtype: 'button',
										text: 'Bajar PDF',
										id: 'btnBajarPDF',
										hidden: true
									}
							]
						}
	


});

var fp = new Ext.form.FormPanel
  ({
	
		hidden: false,
		height: 'auto',
		
		width: 600,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 160,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		
		monitorValid: true,
		items: [elementosForma],
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  id:'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			 // formBind: true,
			  handler: function(boton, evento) 	{		
					var icEpo1 = Ext.getCmp('icEpo1');
					if(Ext.isEmpty(icEpo1.getValue())){
						icEpo1.markInvalid('Debe de seleccionar una EPO ');
						icEpo1.focus();
						return;
					}
						pnl.el.mask('Enviando...', 'x-mask-loading');
							consulta.load({ 
								params: Ext.apply(
									fp.getForm().getValues(),{
									bandera:bandera,									
									icDist: Ext.getCmp('icDist1').getValue(),
									icEpo: Ext.getCmp('icEpo1').getValue(),
									cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
									plaza : Ext.getCmp('plaza1').getValue(),
									cbBanco : Ext.getCmp('cbBanco1').getValue()
								})})
							
					}
			 },{
			  xtype: 'button',
			  text: 'Insertar',
			  name: 'btnInsertar',
			  id: 'btnInsertar',
			  iconCls: '',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{		
							
					
							Ext.Ajax.request({
								url: '15ctabancpymeext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion:'Insertar',
									bandera: bandera, 
									accion:1,
									icDist: Ext.getCmp('icDist1').getValue(),
									icEpo: Ext.getCmp('icEpo1').getValue(),
									cbMoneda: Ext.getCmp('cbMoneda1').getValue(),	
									plaza : Ext.getCmp('plaza1').getValue(),	
									cbBanco : Ext.getCmp('cbBanco1').getValue()	
									}),
								callback: procesarInsertarCta
							});
					}
			 },
			 {
			  //Bot�n Limpiar
			  xtype: 'button',
			  text: 'Limpiar',
			  name: 'btnLimpiar',
			  hidden: false,
			  iconCls: 'icoLimpiar',
			  handler: function(boton, evento) 
			  {
			   location.reload();
			  }
			 }
		  ]
  });




//---------------INICIO-------------------------

var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 950,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid,
			winActualiza
			
		]
	});
	catalogoEpo.load();
	catalogoMoneda.load();
	catalogoBanco.load();
	catalogoPlaza.load();
	//Accion de la bandera
	bandera = Ext.get('bandera').getValue();
	if(bandera=='true'){
		Ext.getCmp('btnBuscar').hide();
		Ext.getCmp('btnGenerarPDF').hide();
		Ext.getCmp('btnGenerarCSV').hide();
		bandera=true;
	}
	else{
		Ext.getCmp('btnConfirmar').hide();
		Ext.getCmp('btnInsertar').hide();
		Ext.getCmp('icDist1').allowBlank= true;
		Ext.getCmp('cbMoneda1').allowBlank= true;
		Ext.getCmp('cbBanco1').allowBlank= true;
		Ext.getCmp('noCuenta1').allowBlank= true;
		Ext.getCmp('sucursal1').allowBlank= true;
		Ext.getCmp('plaza1').allowBlank= true;
		bandera=false;
	}

});