<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,					
		com.netro.model.catalogos.*,
		com.netro.descuento.*,							
		net.sf.json.JSONArray,				
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
String num_electronico = (request.getParameter("num_electronico") == null)?"":request.getParameter("num_electronico");
String chkPymesConvenio = (request.getParameter("chkPymesConvenio") == null)?"N":request.getParameter("chkPymesConvenio");
String fechaparamCU_ini = (request.getParameter("fechaparamCU_ini") == null)?"":request.getParameter("fechaparamCU_ini");
String fechaparamCU_fin = (request.getParameter("fechaparamCU_fin") == null)?"":request.getParameter("fechaparamCU_fin");
String expediente_digital = (request.getParameter("expediente_digital") == null)?"":request.getParameter("expediente_digital");
String txtNombre = (request.getParameter("txtNombre") == null)?"":request.getParameter("txtNombre");
String cboEPO = (request.getParameter("cboEPO") == null)?"":request.getParameter("cboEPO");
String clave_pyme = (request.getParameter("clave_pyme") == null)?"":request.getParameter("clave_pyme");
String infoRegresar	=	"", ic_pyme ="", consulta ="", sIfConvenioUnico ="";
int start =0, limit =0;
JSONObject jsonObj = new JSONObject();
List parametros= new ArrayList();

ParametrosDescuento 		parametrosDescuento 		= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

// al inicar la pantalla se 
if(informacion.equals("valIniciales") ){
	int numeroCuentasPendientes = parametrosDescuento.verifCuentasPendResul(iNoCliente);
	sIfConvenioUnico =parametrosDescuento.sIfConvenioUnico(iNoCliente);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("sIfConvenioUnico", sIfConvenioUnico);	
	jsonObj.put("numeroCuentasPendientes", String.valueOf(numeroCuentasPendientes));		
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("pymeNombre") ) {
	
	List datosPymes =  parametrosDescuento.datosPymes(num_electronico, "" ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);
	infoRegresar = jsonObj.toString();		

}else  if (informacion.equals("busquedaAvanzada") ) {

	String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
		
	CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");	
	if(!rfcPyme.equals(""))
	cat.setRfc_pyme(rfcPyme);
	if(!nombrePyme.equals(""))
	cat.setNombre_pyme(nombrePyme);	
	cat.setPantalla("SoliAfi");
	cat.setOrden("p.cg_razon_social");		
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");	
	cat.setClaveIf(iNoCliente);  
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoPYME")){

	infoRegresar = parametrosDescuento.catalogoPyme(cboEPO, iNoCliente);
	
}else if(informacion.equals("Consultar") ){
	
	sIfConvenioUnico =parametrosDescuento.sIfConvenioUnico(iNoCliente);
	
	parametros= new ArrayList();
	parametros.add(iNoCliente);
	parametros.add(chkPymesConvenio);
	parametros.add(num_electronico);
	parametros.add(cboEPO);
	parametros.add(clave_pyme);
	parametros.add(fechaparamCU_ini);
	parametros.add(fechaparamCU_fin);
			
	consulta = parametrosDescuento.consAutotizacion(parametros);

	jsonObj = JSONObject.fromObject(consulta);	
	jsonObj.put("sIfConvenioUnico", sIfConvenioUnico);	
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("consultaCuentas")){

	consulta  = parametrosDescuento.cuentasPedientes(iNoCliente);
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();	
	
}else if(informacion.equals("ArchivoCarga")){
	
	sIfConvenioUnico =parametrosDescuento.sIfConvenioUnico(iNoCliente);
	
	parametros= new ArrayList();
	parametros.add(iNoCliente);
	parametros.add(chkPymesConvenio);
	parametros.add(num_electronico);
	parametros.add(cboEPO);
	parametros.add(clave_pyme);
	parametros.add(fechaparamCU_ini);
	parametros.add(fechaparamCU_fin);
	parametros.add(sIfConvenioUnico);
	parametros.add(strDirectorioTemp);
	
	String nombreArchivo = parametrosDescuento.archivoConsAutoCarga(parametros );
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();		
	
}
%>
<%=infoRegresar%>
