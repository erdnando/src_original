<%@ page language="java" %>
<%@ page import="
	java.util.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<% 


JSONObject jsonObj;
String informacion= (request.getParameter("informacion")	==null)?"":request.getParameter("informacion");
String infoRegresar	= null;

ParametrosDescuento paramDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class); 


if(informacion.equals("ConsultaCveEpos")) {
	JSONArray jsonArray = new JSONArray();
	List lstCveEposWSIF = paramDescuento.consultaClavesEpoWSIF(iNoCliente);	
				
	jsonArray = JSONArray.fromObject(lstCveEposWSIF);
	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", jsonArray.toString());
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("GuardarCeEposWS")) {
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosModificados = JSONArray.fromObject(jsonRegistros);
	List lstCveEposWS = new ArrayList();
	Iterator itReg = arrRegistrosModificados.iterator();
	
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		HashMap mapCveEpo = new HashMap();
		mapCveEpo.put("ic_if",registro.getString("ic_if"));
		mapCveEpo.put("ic_epo",registro.getString("ic_epo"));
		mapCveEpo.put("cg_razon_social",(registro.getString("cg_razon_social")).replace(',',' '));
		mapCveEpo.put("ig_epo_externa",registro.getString("ig_epo_externa"));
		mapCveEpo.put("cc_tipo_factoraje",registro.getString("cc_tipo_factoraje"));
		lstCveEposWS.add(mapCveEpo);
	}
	
	System.out.println("lstCveEposWS==="+lstCveEposWS);
	paramDescuento.guardaCveEposWsIF(lstCveEposWS);

	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}


%>

<%= infoRegresar%>


