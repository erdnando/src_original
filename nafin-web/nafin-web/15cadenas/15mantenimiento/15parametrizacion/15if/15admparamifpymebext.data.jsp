<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.text.SimpleDateFormat,
		netropology.utilerias.*,	
		com.netro.cadenas.*, 
		java.sql.*,
		netropology.utilerias.usuarios.*, 
		com.netro.model.catalogos.*,
		com.netro.descuento.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession.jspf"%>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
String ic_nafin_electronico = (request.getParameter("ic_nafin_electronico") == null)?"":request.getParameter("ic_nafin_electronico");
String clave_pyme = (request.getParameter("clave_pyme") == null)?"":request.getParameter("clave_pyme");
String chkPymesConvenio = (request.getParameter("chkPymesConvenio") == null)?"":request.getParameter("chkPymesConvenio");
String fechaparamCU_ini = (request.getParameter("fechaparamCU_ini") == null)?"":request.getParameter("fechaparamCU_ini");
String fechaparamCU_fin = (request.getParameter("fechaparamCU_fin") == null)?"":request.getParameter("fechaparamCU_fin");
String cboEPO = (request.getParameter("cboEPO")!=null)?request.getParameter("cboEPO"):"";
if(clave_pyme.equals("0")) {  clave_pyme =""; }
String infoRegresar	=	"", sIfConvenioUnico  ="";
JSONObject jsonObj = new JSONObject();
int start =0, limit =0;

ParametrosDescuento 		parametrosDescuento 		= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
	
if(informacion.equals("valIniciales")){

	sIfConvenioUnico =parametrosDescuento.sIfConvenioUnico(iNoCliente);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("sIfConvenioUnico", sIfConvenioUnico);	
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");	
	cat.setClaveIf(iNoCliente);  
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoPYME")){

	CatalogoPymeCuenBan cat = new CatalogoPymeCuenBan();
	cat.setCampoClave("PYME.IC_PYME");
	cat.setCampoDescripcion("PYME.CG_RAZON_SOCIAL");		
	cat.setClaveIF(iNoCliente);  
	cat.setClaveEPO(cboEPO);  
	cat.setOrden("PYME.CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();  

}else if (informacion.equals("Consultar") ||  informacion.equals("ArchivoCSV")  ) {		//Datos para la Consulta con Paginacion
	
	String conExpedienteEfile  =""; 
	sIfConvenioUnico =parametrosDescuento.sIfConvenioUnico(iNoCliente);

	if(!chkPymesConvenio.equals("on")) chkPymesConvenio ="N";
	if(chkPymesConvenio.equals("on")) chkPymesConvenio ="S";
	
	CuenBancIfCA paginador = new CuenBancIfCA();
	
	paginador.setClaveEPO(cboEPO);
	paginador.setClavePyme(clave_pyme);
	paginador.setClaveIF(iNoCliente);
	paginador.setNafinElectronico(ic_nafin_electronico);
	paginador.setFechaparamCU_ini(fechaparamCU_ini);
	paginador.setFechaparamCU_fin(fechaparamCU_fin);
	paginador.setChkPymesConvenio(chkPymesConvenio);		
	paginador.setSIfConvenioUnico(sIfConvenioUnico);	
		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);	
		
	// Verificar si hay pymes nuevas que ya tengan expedientes cargados en el Sistema Efile
	request.setAttribute("conExpedienteEfile","N");
	CuenBancIfCA c = new CuenBancIfCA();
	parametrosDescuento.actualizaPymesConExpedientesCargadosEnEfile(paginador.getDocumentQuery(request));
	request.setAttribute("conExpedienteEfile",null);
	conExpedienteEfile=null;
	conExpedienteEfile = (conExpedienteEfile != null && conExpedienteEfile.equals("N"))?"N":"S";
	paginador.setConExpedienteEfile(conExpedienteEfile);
	
	if (informacion.equals("Consultar")) {	
			
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));		
	
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")) {
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta	
			}		
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);	
			
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if (informacion.equals("ArchivoCSV")) {
	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}		

}
	
%>
<%=infoRegresar%>

