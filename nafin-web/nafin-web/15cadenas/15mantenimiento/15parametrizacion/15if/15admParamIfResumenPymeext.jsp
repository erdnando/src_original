<!DOCTYPE html>
<%@ page import="
		java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf"%>

<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs.jspf" %>  
<%if(esEsquemaExtJS)  { %>
<%@ include file="/01principal/menu.jspf"%>
<%}%>

<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="15admParamIfResumenPymeext.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS)  { %>
<%@ include file="/01principal/01if/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01if/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
<%} else  {  %>

	<div id="_menuApp"></div>
	<div id="Contcentral">
		<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
   </div>
	</div>
	<form id='formAux' name="formAux" target='_new'></form>	
<%}%>
</body>
</html>