<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.text.SimpleDateFormat,
		netropology.utilerias.*,	
		com.netro.cadenas.*, 
		java.sql.*,
		netropology.utilerias.usuarios.*, 
		com.netro.model.catalogos.*,
		com.netro.descuento.*,
		com.netro.cadenas.*,			
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
String num_electronico = (request.getParameter("num_electronico") == null)?"":request.getParameter("num_electronico");
String chkPymesConvenio = (request.getParameter("chkPymesConvenio") == null)?"N":request.getParameter("chkPymesConvenio");
String fechaparamCU_ini = (request.getParameter("fechaparamCU_ini") == null)?"":request.getParameter("fechaparamCU_ini");
String fechaparamCU_fin = (request.getParameter("fechaparamCU_fin") == null)?"":request.getParameter("fechaparamCU_fin");
String expediente_digital = (request.getParameter("expediente_digital") == null)?"":request.getParameter("expediente_digital");
String txtNombre = (request.getParameter("txtNombre") == null)?"":request.getParameter("txtNombre");
String infoRegresar	=	"", ic_pyme ="", consulta ="";
int start =0, limit =0;

JSONObject jsonObj = new JSONObject();

ParametrosDescuento 		parametrosDescuento 		= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

if(informacion.equals("InicializaPantalla") ) {

	try {
	
		parametrosDescuento.getPymesSinExpEfile(iNoCliente);

	} catch(Exception e) {
		e.printStackTrace();
      throw new AppException("Unexpected Error");    
	 } finally{     
	  jsonObj.put("success", new Boolean(true));				
	  }
	infoRegresar = jsonObj.toString();	
	
}else  if(informacion.equals("pymeNombre") ) {
	
	List datosPymes =  parametrosDescuento.datosPymes(num_electronico, "" ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);
	infoRegresar = jsonObj.toString();		

}else  if (informacion.equals("busquedaAvanzada") ) {

	String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
	
		
	CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");	
	if(!rfcPyme.equals(""))
	cat.setRfc_pyme(rfcPyme);
	if(!nombrePyme.equals(""))
	cat.setNombre_pyme(nombrePyme);
	cat.setNafin_electronico("");
	cat.setPantalla("SoliAfi");
	cat.setOrden("p.cg_razon_social");		
	infoRegresar = cat.getJSONElementos();

}else  if (informacion.equals("Consultar")  ||  informacion.equals("ArchivoCSV")  ||  informacion.equals("ArchivoPDF")  ) {

	if(!chkPymesConvenio.equals("on")) { chkPymesConvenio =""; }
	if(chkPymesConvenio.equals("on"))  { chkPymesConvenio ="S"; }
	
	ResumenPymesPorAutorizar paginador = new ResumenPymesPorAutorizar();
	
	paginador.setTxt_nafelec(num_electronico);
	paginador.setNombre_pyme_aux( txtNombre);
	paginador.setChkPymesConvenio( chkPymesConvenio);
	paginador.setDesde(fechaparamCU_ini);
	paginador.setHasta(fechaparamCU_fin);
	paginador.setIc_if(iNoCliente);
  	paginador.setExpediente_digital(expediente_digital);
	paginador.setPerfil(strPerfil);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);	
	
	if (informacion.equals("Consultar")  ||  informacion.equals("ArchivoPDF") ) {
			
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));		
	
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	}
	
	if (informacion.equals("Consultar")  ) {
		try {
			if (operacion.equals("Generar")) {
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta	
			}		
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
			
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("strPerfil", strPerfil);
		infoRegresar =jsonObj.toString();
		
	}else if (informacion.equals("ArchivoCSV")) {
	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ArchivoPDF")) {
	
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}	
}

%>
<%=infoRegresar%>
