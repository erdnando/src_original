Ext.onReady(function() {

	var getDiferenciaEnDias = function(fechaIni, fechaFin) { 
		fechaIni =	Ext.util.Format.date(fechaIni,'d/m/Y');
		fechaFin =	Ext.util.Format.date(fechaFin,'d/m/Y');
		var string1 			= "";
		var temp 				= "";
		var diferenciaEnDias	= "";
		string1 = fechaIni;
		string = "" + string1;
		splitstring = string.split("/");
		var fechaInicial= new Date();
		fechaInicial.setDate(splitstring[0]);
		fechaInicial.setMonth(splitstring[1]-1);
		fechaInicial.setYear(splitstring[2]);
		string1 = fechaFin;
		string = "" + string1;
		splitstring = string.split("/");
		var fechaFinal= new Date();
		fechaFinal.setDate(splitstring[0]);
		fechaFinal.setMonth(splitstring[1]-1);
		fechaFinal.setYear(splitstring[2]);
		//Calcular el numero de milisegundos de un dia
		var milisegundosPorDia=1000*60*60*24;
		//Calculate difference btw the two dates, and convert to days
		diferenciaEnDias = Math.ceil((fechaFinal.getTime()-fechaInicial.getTime())/(milisegundosPorDia));
		return diferenciaEnDias;
	}
	
	//  Ventana Emergente  de cuentas Pymes Pendientes de Autorizar  
	var ventanaCuentasPendientes = function() {
	
		consultaCuentas.load();	
		new Ext.Window({
			title: 'EXISTEN CAMBIOS DE CUENTAS BANCARIAS PYME PENDIENTES DE AUTORIZACI�N',	
			width: 520,	height: 300,	
			id: 'verCuentas',	
			closeAction: 'hide',
			autoScroll: true,	
			resizable:false,	
			modal:true,
			items: [	gridCuentas ]
			}).show();
	
	}

	var procesarConsultaCuentasData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridCuentas = Ext.getCmp('gridCuentas');
			var el = gridCuentas.getGridEl();
			var store = consultaCuentas;
			if(store.getTotalCount()>0){
				el.unmask();
			}else {
				el.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}
	
	var consultaCuentas = new Ext.data.JsonStore({
		root: 'registros',
		url: '15admparamifpymemasiva.data.jsp',
		baseParams: {
			informacion: 'consultaCuentas'
		},
		fields: [
			{name: 'RFC'},
			{name: 'NOMBRE_PYME'},
			{name: 'NOMBRE_EPO'}					
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {	
				load: procesarConsultaCuentasData,
				exception: {
					fn: function(proxy,type,action,optionRequest,response,args){
						NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
						procesarConsultaCuentasData(null,null,null);
					}
				}
			}
	});
	
	var gridCuentas = {
		xtype: 'grid',
		store: consultaCuentas,
		id: 'gridCuentas',
		columns: [
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				align: 'left',
				width: 150
			},
			{
				header: 'Pyme ',
				tooltip: 'Pyme',
				dataIndex: 'NOMBRE_PYME',
				align: 'left',				
				width: 150
			},			
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				align: 'center',
				width: 150
			}							
		],
		stripeRows: true,
		loadMask: true,
		height: 260,
		width: 500,
		title: '',
		frame: true		
	}
	
	// CONSULTA  
		
	function procesarGenerarArchivoCarga(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	
	var descargaArchivoExpediente = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var rfc = registro.get('RFC');
		
		new Ext.Window({  
			title: 'Expediente', 
			modal: true,
			constrain:true,
			width: 300,  
			height:150,  
			minimizable: false,  
			maximizable: false,  
			html: '<iframe src="/nafin/15cadenas/15mantenimiento/15clientes/popupexpediente.jsp?rfc='+escape(rfc)+'&version=ext"></iframe>'
		}).show();
	}
	

//************Consulta  Grid ************************
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna		
			var jsonData = store.reader.jsonData;	
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			
			if(jsonData.sIfConvenioUnico=='S') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_CLABE'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('OPERA_CONVENIO_UNICO'), false);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_FIRMA_AUTO'), false);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_PARAM_CU'), false);				
			}else if(jsonData.sIfConvenioUnico=='N') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_CLABE'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('OPERA_CONVENIO_UNICO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_FIRMA_AUTO'), true);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_PARAM_CU'), true);							
			}
							 			
			
			if(store.getTotalCount() > 0) {						
				el.unmask();	
				 Ext.getCmp('btnGenerarAchCarga').enable();					 				
			} else {		
				Ext.getCmp('btnGenerarAchCarga').disable();				
						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}

	
	//var consultaData   = new Ext.data.GroupingStore({	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15admparamifpymemasiva.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'CS_VOBO_IF'},
			{name: 'IC_PYME'},
			{name: 'IC_EPO'},
			{name: 'IC_IF'},
			{name: 'IC_CUENTA_BANCARIA'},				
			{name: 'NUMERO_SIRAC'},
			{name: 'NOMBRE_PYME'},
			{name: 'NOMBRE_MONEDA'},
			{name: 'BANCO_SERVICIO'},
			{name: 'NO_CUENTA'},
			{name: 'CUENTA_CLABE'},
			{name: 'SUCURSAL'},
			{name: 'NOMBRE_EPO'},
			{name: 'RFC'},
			{name: 'PLAZA'},
			{name: 'SELECCIONAR'},
			{name: 'OPERA_CONVENIO_UNICO'},
			{name: 'FECHA_FIRMA_AUTO'},
			{name: 'FECHA_PARAM_CU'},
			{name: 'NUM_NAFIN_ELEC'},
			{name: 'PRIORIDAD'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}		
	});
	

	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		clicksToEdit: 1,	
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: ' ',
		columns: [	
			{
				header: 'N�mero SIRAC',
				tooltip: 'N�mero SIRAC',
				dataIndex: 'NUMERO_SIRAC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{
				header: 'PyME',
				tooltip: 'PyME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'NOMBRE_MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{
				header: 'Banco de Servicio',
				tooltip: 'Banco de Servicio',
				dataIndex: 'BANCO_SERVICIO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'No. de Cuenta',
				tooltip: 'No. de Cuenta',
				dataIndex: 'NO_CUENTA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'Cuenta Clabe',
				tooltip: 'Cuenta Clabe',
				dataIndex: 'CUENTA_CLABE',
				sortable: true,
				hidden: true, 
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'Sucursal',
				tooltip: 'Sucursal',
				dataIndex: 'SUCURSAL',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'No. de EPO',
				tooltip: 'No. de EPO',
				dataIndex: 'IC_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},		
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{
				header: 'Plaza',
				tooltip: 'Plaza',
				dataIndex: 'PLAZA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},					
			{
				header: 'Pyme Opera con Convenio �nico',
				tooltip: 'Pyme Opera con Convenio �nico',
				dataIndex: 'OPERA_CONVENIO_UNICO',
				sortable: true,
				hidden: true, 
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				xtype: 'actioncolumn',
				header: 'Expediente',
				tooltip: 'Expediente',
				 width: 130,
				align: 'center',				
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';								
						} 
						,handler: descargaArchivoExpediente
					}
				]				
			},
			
			{
				header: 'Fecha de Firma Aut�grafa de CU.',
				tooltip: 'Fecha de Firma Aut�grafa de CU.',
				dataIndex: 'FECHA_FIRMA_AUTO',
				sortable: true,
				hidden: true, 
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Fecha Parametrizaci�n Autom�tica CU',
				tooltip: 'Fecha Parametrizaci�n Autom�tica CU',
				dataIndex: 'FECHA_PARAM_CU',
				sortable: true,
				hidden: true, 
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'N�m. Nafin Electr�nico',
				tooltip: 'N�m. Nafin Electr�nico',
				dataIndex: 'NUM_NAFIN_ELEC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{
				header: 'Prioridad',
				tooltip: 'Prioridad',
				dataIndex: 'PRIORIDAD',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){ 
					if(registro.data['CS_VOBO_IF']=='N') {
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			}
		],		
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,		
		frame: true,
		bbar: {		
			items: [			
				'-',
				'->',				
				{
					xtype: 'button',
					text: 'Generar Archivo de Carga',
					id: 'btnGenerarAchCarga',
					handler: function(boton, evento) {					
						Ext.Ajax.request({
							url: '15admparamifpymemasiva.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCarga'															
							})
							,callback: procesarGenerarArchivoCarga
						});
					}
				}	
			]
		}
	});
			

	//************Forma************************
		
	function procesarValIniciales(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
			
				if(jsonData.sIfConvenioUnico=='S') {
					Ext.getCmp('chkPymesConvenio2').show();					
				}else if(jsonData.sIfConvenioUnico=='N') {
					Ext.getCmp('chkPymesConvenio2').hide();					
				}		
				// para mostrar la ventana emergente de  Cuentas Pendientes
				if(jsonData.numeroCuentasPendientes!='0') {
					ventanaCuentasPendientes();
				}
			
			}
			fp.el.unmask();
		}else {
		NE.util.mostrarConnError(response,opts);
		}
	}
	
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15admparamifpymemasiva.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admparamifpymemasiva.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPOData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admparamifpymemasiva.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},		
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 		'panel',
			bodyStyle: 	'padding: 10px',
			layout: {
				type: 	'hbox',
				pack: 	'center',
				align: 	'middle'
			},
			items: [
				{
					xtype: 'button',
					text: 'Buscar',
					id: 'btnBuscar',
					iconCls: 'icoBuscar',
					width: 	75,
					handler: function(boton, evento) {
						var pymeComboCmp = Ext.getCmp('cbPyme1');
						pymeComboCmp.setValue('');
						pymeComboCmp.setDisabled(false);		
						
						storeBusqAvanzPyme.load({
							params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
								informacion: 'busquedaAvanzada'					
							})																	
						});				
					},
					style: { 
						marginBottom:  '10px' 
					} 
				}
			]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});
	
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);	
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var  elementosForma =  [
		{	xtype: 'textfield',	hidden: true, id: 'iCta' },
		{	xtype: 'textfield',	hidden: true, id: 'iEPO' },
		{	xtype: 'textfield',	hidden: true, id: 'iPyme' },
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'displayfield',
					value: '',
					width: 80
				},	
				{
					xtype: 'button',
					text: 'Individual',
					id: 'individualPKI',
					hidden: true,
					handler: function(boton, evento) {
						window.location = '/nafin/15cadenas/15pki/15mantenimiento/15parametrizacion/15if/15admparamifpymeext.jsp';
					}
				},
				{
					xtype: 'button',
					text: 'Individual',
					hidden: true,
					id: 'individual',
					handler: function(boton, evento) {
						window.location = '15admparamifpymeExt.jsp';
					}
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 80
				},		
				{
					xtype: 'button',
					text: 'Masiva',
					id: 'masiva',
					handler: function(boton, evento) {
						window.location = '15admparamifpymemasivaext.jsp';
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�m. Electr�nico PyME',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'N�mero Nafin Electr�nico',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){
						 // Petici�n b�sica  
							Ext.Ajax.request({  
								url: '15admparamifpymemasiva.data.jsp',  
								method: 'POST',  
								callback: successAjaxFn,  
								params: {  
									 informacion: 'pymeNombre' ,
									 num_electronico: Ext.getCmp('num_electronico1').getValue()
								}  
							}); 
							
							Ext.getCmp("cboEPO1").setValue('');
							Ext.getCmp("clave_pyme1").setValue('');
							
						}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 250,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
					xtype: 'button',
					text: 'B�squeda Avanzada...',
					iconCls: 'icoBuscar',
					id: 'btnBusqAv',
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winBusqAvan');
						if (ventana) {
							ventana.show();
						} else {
							new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
						}
					}
				}
			]
		},	
		{
			xtype: 'combo',
			name: 'cboEPO',
			id: 'cboEPO1',
			fieldLabel: 'Cadena Productiva',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEPO',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var cmbPyme = Ext.getCmp('clave_pyme1');
						cmbPyme.setValue('');
						cmbPyme.setDisabled(false);
						cmbPyme.store.load({
							params: {
								cboEPO:combo.getValue()
							}
						});
						Ext.getCmp("num_electronico1").setValue('');
						Ext.getCmp("txtNombre1").setValue('');
					}					
				}
			}
		},
		{
			xtype: 'combo',
			name: 'clave_pyme',
			id: 'clave_pyme1',
			fieldLabel: 'Nombre de la PyME',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_pyme',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoPYMEData,
			tpl : NE.util.templateMensajeCargaCombo					
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: 'PyMEs con Convenio �nico',
			combineErrors: false,
			msgTarget: 'side',	
			id: 'chkPymesConvenio2',
			items: [
				{
					xtype: 'checkbox',
					name: 'chkPymesConvenio',
					id: 'chkPymesConvenio',
					fieldLabel: '',
					allowBlank: true,
					startDay: 0,
					width: 100,
					height:20,					
					msgTarget: 'side',
					margins: '0 20 0 0'
				}				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Parametrizaci�n',
			combineErrors: false,
			msgTarget: 'side',
			id: 'fechaparamCU',			
			items: [
				{
					xtype: 'datefield',
					name: 'fechaparamCU_ini',
					id: 'fechaparamCU_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaparamCU_fin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaparamCU_fin',
					id: 'fechaparamCU_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fechaparamCU_ini',
					margins: '0 20 0 0'  
					}
				]
			}	
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 750,
		title: 'Criterios de Busqueda Masiva',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
							
					var cboEPO =  Ext.getCmp("cboEPO1");
					var num_electronico =  Ext.getCmp("num_electronico1");
					var num_electronico =  Ext.getCmp("num_electronico1");
					var fechaparamCU_ini =  Ext.getCmp("fechaparamCU_ini");
					var fechaparamCU_fin =  Ext.getCmp("fechaparamCU_fin");
					
					if (Ext.isEmpty(cboEPO.getValue())  &&  Ext.isEmpty(num_electronico.getValue())  
					&&  Ext.isEmpty(fechaparamCU_ini.getValue()) &&  Ext.isEmpty(fechaparamCU_fin.getValue()) 	){
						Ext.MessageBox.alert("Mensaje","Es necesario seleccionar un criterio para la consulta.");											
						return;
					}					
					var diferencia = Math.abs(getDiferenciaEnDias(fechaparamCU_fin.getValue(),fechaparamCU_ini.getValue() ));					
					if ( diferencia >90 )		{
						Ext.MessageBox.alert("Mensaje","El rango m�ximo de fechas es de 90 d�as naturales");							
						return;
					}				
					if ( ( !Ext.isEmpty(fechaparamCU_ini.getValue()) &&  Ext.isEmpty(fechaparamCU_fin.getValue()) )   || ( Ext.isEmpty(fechaparamCU_ini.getValue()) &&  !Ext.isEmpty(fechaparamCU_fin.getValue()) ) ) {	
						fechaparamCU_fin.markInvalid('Debe seleccionar un rango de fechas');	
						fechaparamCU_ini.markInvalid('Debe seleccionar un rango de fechas');	
						return;
					}
				
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar'							
						})
					});							
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15admparamifpymemasivaext.jsp';
				}
			},
			{
				text: 'Cargar Archivo ',	
				iconCls: 'autorizar',
				handler: function() {
					window.location = '../../../15pki/15mantenimiento/15parametrizacion/15if/15admparamifpymemasivacargaext.jsp';
	
				}
			}
		]
	});



	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,		
		items: [		
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,						
			NE.util.getEspaciador(20)
		]
	});
	

	catalogoEPOData.load();
	

	var valIniciales = function(){
		
		Ext.Ajax.request({
			url : '15admparamifpymemasiva.data.jsp',
			params : {
				informacion: 'valIniciales'			
			}
			,callback: procesarValIniciales
		});
	}
	
	valIniciales();

	var origenPantalla  = Ext.getDom('origenPantalla').value;
	if(origenPantalla=='PKI') {
		Ext.getCmp("individualPKI").show();
	}else {
		Ext.getCmp("individual").show();
	}
	
});
