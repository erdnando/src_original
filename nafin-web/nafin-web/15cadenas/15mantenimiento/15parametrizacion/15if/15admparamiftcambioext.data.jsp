<%@ page contentType="application/json;charset=UTF-8" import="   
  com.netro.anticipos.*,
	java.util.*,
	com.netro.exception.*,  
	com.netro.model.catalogos.*,
	netropology.utilerias.usuarios.*,   
	net.sf.json.JSONObject,
	com.netro.descuento.*,
	com.netro.cadenas.*,
	net.sf.json.JSONArray"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/15cadenas/015secsession.jspf" %>

<%
	//Codigo java
	String infoRegresar           = "";
	JSONObject resultado 	      = new JSONObject();
	String informacion            =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String moneda=(request.getParameter("HcbMoneda")    != null) ?   request.getParameter("HcbMoneda") :"";
	String fechaInicio=(request.getParameter("dFechaRegIni")    != null) ?   request.getParameter("dFechaRegIni") :"";
	String fechaFin=(request.getParameter("dFechaRegFin")    != null) ?   request.getParameter("dFechaRegFin") :"";
	
	
	
	if(informacion.equals("catologoMonedaDist")){
	
			 CatalogoSimple cat = new CatalogoSimple();
			 cat.setTabla("comcat_moneda");
			 cat.setDistinc(false);
			 cat.setCampoClave("ic_moneda");
			 cat.setCampoDescripcion("cd_nombre");
			 cat.setOrden("2");	
			 
			 infoRegresar = cat.getJSONElementos();
			 
	}else if(informacion.equals("Consulta")){
	
			 IFValorMoneda valorMoneda=new IFValorMoneda();
			 valorMoneda.setMoneda(moneda);
			 valorMoneda.setFechaIni(fechaInicio);
			 valorMoneda.setFechaFin(fechaFin);			 
			 Registros registros = valorMoneda.executeQuery();
			 
			 infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
			 
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

%>

<%=infoRegresar%>