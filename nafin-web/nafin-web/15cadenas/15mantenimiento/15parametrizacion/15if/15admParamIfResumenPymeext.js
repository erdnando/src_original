Ext.onReady(function() {

	function procesaValoresIniciales(opts, success, response) {			
		var fp = Ext.getCmp('forma');		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {					
			fp.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarArchivoPDF =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarPDF');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarPDF');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarArchivoCSV =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarCSV');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarCSV');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var autoriza = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var num_naf_elect = registro.get('NUM_NAF_ELECT');	
		//document.location.href = "15admparamifpymeExt.jsp?num_naf_elect="+num_naf_elect;	
      document.location.href = "../../../15pki/15mantenimiento/15parametrizacion/15if/15admparamifpymeext.jsp?num_naf_elect="+num_naf_elect;	
	}	
	
	var descargaArchivoExpediente = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var rfc = registro.get('PYME_RFC');
		new Ext.Window({  
			title: 'Expediente', 
			modal: true,
			constrain:true,
			width: 300,  
			height:150,  
			minimizable: false,  
			maximizable: false,  
			html: '<iframe src="/nafin/15cadenas/15mantenimiento/15clientes/popupexpediente.jsp?rfc='+escape(rfc)+'&version=ext"></iframe>'
		}).show();
	}
	

//************Consulta  Grid ************************
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna		
			var jsonData = store.reader.jsonData;	
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			
			if(jsonData.strPerfil=='CONSUL IF'){ 
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('EXPEDIENTE'), false);		
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('EXPEDIENTE_DISPONIBLE'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('VER_DETALLE'), true);
			}else {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('EXPEDIENTE'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('VER_DETALLE'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('EXPEDIENTE_DISPONIBLE'), false);
				Ext.getCmp("mensaje").setValue('Nota: Para autorizar o rechazar una Pyme deber� hacer clic en la liga "Ver detalle por EPO" ');				
			}
			 Ext.getCmp('btnBajarCSV').hide();
			 Ext.getCmp('btnBajarPDF').hide();			 			
			
			if(store.getTotalCount() > 0) {						
				el.unmask();	
				 Ext.getCmp('btnGenerarCSV').enable();				
				 Ext.getCmp('btnGenerarPDF').enable();	
				  Ext.getCmp('mensajeConsulta').show();	
			} else {		
				Ext.getCmp('btnGenerarCSV').disable();
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('mensajeConsulta').hide();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var mensajeConsulta = new Ext.Container({
		layout: 'table',		
		id: 'mensajeConsulta',							
		width:	'750',
		heigth:	'auto',
		align: 'left',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
		{ 
			xtype: 'displayfield', 
			align: 'left',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			id: 'mensaje', 	
			value: '' }				
		]
	});
	

	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15admParamIfResumenPyme.data.jsp',
		baseParams: {
			informacion: 'Consultar'		
		},		
		fields: [			
			{name: 'NUMERO_SIRAC'},
			{name: 'NOMBRE_PYME'},
			{name: 'NOMBRE_MONEDA'},
			{name: 'PYME_RFC'},
			{name: 'NUM_NAF_ELECT'},
			{name: 'FECHA_PARAM'},
			{name: 'NOMBRE_EPO'},
			{name: 'CS_DOC_VIGENTES'},
			{name: 'EXPEDIENTE_DISPONIBLE'},
			{name: 'PRIORIDAD'}				
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});

	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		clicksToEdit: 1,	
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '(1) El expediente estar� disponible / actualizado (Reafiliaci�n) en un lapso de 24 / 72 horas a partir de la fecha de parametrizaci�n',
		columns: [	
			{
				header: 'N�mero SIRAC',
				tooltip: 'N�mero SIRAC',
				dataIndex: 'NUMERO_SIRAC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'				
			},	
			{
				header: 'PyME',
				tooltip: 'PyME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},		
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'NOMBRE_MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},		
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'PYME_RFC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'				
			},		
			{
				header: 'N�m. Nafin Electr�nico',
				tooltip: 'N�m. Nafin Electr�nico',
				dataIndex: 'NUM_NAF_ELECT',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'				
			},		
			{
				header: 'Fecha de Parametrizaci�n',
				tooltip: 'Fecha de Parametrizaci�n',
				dataIndex: 'FECHA_PARAM',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'				
			},	
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'				
			},	
			{
				xtype: 'actioncolumn',
				header: 'Expediente',
				tooltip: 'Expediente',
				dataIndex: 'EXPEDIENTE',
				hidden: true, 
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
				 if(registro.get('EXPEDIENTE_DISPONIBLE') !='SI' ){
						 return 'No Disponible ';
					}					
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('EXPEDIENTE_DISPONIBLE') =='SI' ){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';	
							}
						} 
						,handler: descargaArchivoExpediente
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Expediente Disponible (1)',
				tooltip: 'Expediente Disponible (1)',
				dataIndex: 'EXPEDIENTE_DISPONIBLE',
				width: 130,
				hidden: true, 
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					return registro.get('EXPEDIENTE_DISPONIBLE');
				}					
			},
			{
				xtype: 'actioncolumn',
				header: 'Ver detalle por EPO',
				tooltip: 'Ver detalle por EPO',
				dataIndex: 'VER_DETALLE',
				hidden: true, 
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
				 if(registro.get('EXPEDIENTE_DISPONIBLE') !='SI' ){
						 return 'No Disponible ';
					}					
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('EXPEDIENTE_DISPONIBLE') =='SI' ){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';	
							}
						}
						,handler: autoriza
					}
				]				
			},
			{
				header: 'Prioridad',
				tooltip: 'Prioridad',
				dataIndex: 'PRIORIDAD',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'
			}		
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,		
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [			
				'-',
				'->',
				{
					xtype: 'button',
					text: 'Imprimir pagina PDF',
					tooltip:	'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '15admParamIfResumenPyme.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoPDF',
								start: barraPaginacion.cursor,
								limit: barraPaginacion.pageSize							
							})
							,callback: procesarGenerarArchivoPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},	
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15admParamIfResumenPyme.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								operacion: 'ArchivoCSV'								
							})
							,callback: procesarGenerarArchivoCSV
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				}			
			]
		}
	});


			

	//************Forma************************
	
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15admParamIfResumenPyme.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},		
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 		'panel',
			bodyStyle: 	'padding: 10px',
			layout: {
				type: 	'hbox',
				pack: 	'center',
				align: 	'middle'
			},
			items: [
				{
					xtype: 'button',
					text: 'Buscar',
					id: 'btnBuscar',
					iconCls: 'icoBuscar',					
					width: 	75,
					handler: function(boton, evento) {
						var pymeComboCmp = Ext.getCmp('cbPyme1');
						pymeComboCmp.setValue('');
						pymeComboCmp.setDisabled(false);		
						
						storeBusqAvanzPyme.load({
							params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
								informacion: 'busquedaAvanzada'					
							})																	
						});				
					},
					style: { 
						marginBottom:  '10px' 
					} 
				}
			]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});
	
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);	
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var dataExpediente = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['S','Si'],
			['N','No']			
		 ]
	});
	
	var  elementosForma =  [	
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero Nafin Electr�nico',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'N�mero Nafin Electr�nico',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){
						 // Petici�n b�sica  
							Ext.Ajax.request({  
								url: '15admParamIfResumenPyme.data.jsp',  
								method: 'POST',  
								callback: successAjaxFn,  
								params: {  
									 informacion: 'pymeNombre' ,
									 num_electronico: Ext.getCmp('num_electronico1').getValue()
								}  
						}); 				}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 250,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
					xtype: 'button',
					text: 'B�squeda Avanzada...',
					iconCls: 'icoBuscar',
					id: 'btnBusqAv',
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winBusqAvan');
						if (ventana) {
							ventana.show();
						} else {
							new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
						}
					}
				}
			]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'PyMEs con Convenio �nico',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'checkbox',
					name: 'chkPymesConvenio',
					id: 'chkPymesConvenio',
					fieldLabel: '',
					allowBlank: true,
					startDay: 0,
					width: 100,
					height:20,					
					msgTarget: 'side',
					margins: '0 20 0 0'
				}				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Parametrizaci�n',
			combineErrors: false,
			msgTarget: 'side',
			id: 'fechaparamCU',			
			items: [
				{
					xtype: 'datefield',
					name: 'fechaparamCU_ini',
					id: 'fechaparamCU_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaparamCU_fin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaparamCU_fin',
					id: 'fechaparamCU_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fechaparamCU_ini',
					margins: '0 20 0 0'  
					}
				]
			},
			{
			xtype: 'compositefield',
			fieldLabel: 'Expediente Disponible',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
				xtype: 'combo',
				name: 'expediente_digital',
				id: 'expediente_digital1',
				fieldLabel: 'Expediente Disponible',
				emptyText: 'Seleccione...',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				hiddenName : 'expediente_digital',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : dataExpediente
				}
			]
		}
	
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 710,
		title: 'Criterios de B�squeda de Resumen de Pymes  por Autorizar ',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
							
					var fechaparamCU_ini =  Ext.getCmp("fechaparamCU_ini");
					var fechaparamCU_fin =  Ext.getCmp("fechaparamCU_fin");
					if (  !Ext.isEmpty(fechaparamCU_ini.getValue()) &&  Ext.isEmpty(fechaparamCU_fin.getValue())   ) {	
						fechaparamCU_fin.markInvalid('El valor de la fecha final de parametrizaci�n es requerido.');					
						return;
					}
					
					if ( Ext.isEmpty(fechaparamCU_ini.getValue()) &&  !Ext.isEmpty(fechaparamCU_fin.getValue())   ) {	
						fechaparamCU_ini.markInvalid('El valor de la fecha inicial de parametrizaci�n es requerido.');					
						return;
					}
				
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15
						})
					});				
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15admParamIfResumenPymeext.jsp';
				}
			}
		]
	});



	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,		
		items: [		
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			mensajeConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	
	var procesaIniciales =  function() {		
		fp.el.mask('Cargando Valores Iniciales...', 'x-mask-loading');	
		
		Ext.Ajax.request({
			url: 						'15admParamIfResumenPyme.data.jsp',
			params: 	{
				informacion:		'InicializaPantalla'
			},
			callback: 				procesaValoresIniciales
		});
		
	}	
	
	procesaIniciales();

	
});
