<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*,
		com.netro.exception.*,
		com.netro.afiliacion.*,
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar = "";
	if(informacion.equals("Consulta")){
		com.netro.cadenas.ConsDatosPersonales paginador = new com.netro.cadenas.ConsDatosPersonales();
		paginador.setClaveEPO(iNoEPO);
		paginador.setClavePyme(iNoCliente);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		try{
			Registros reg	= queryHelper.doSearch();
			infoRegresar	= 
				"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";		
		} catch(Exception e){
			throw new AppException("Error en la consulta", e);
		}
	}else if(informacion.equals("ActualizarDatos")){
		String telefono = (request.getParameter("numTelefonico")!=null)?request.getParameter("numTelefonico"):"";
		String mail = (request.getParameter("txtEmail")!=null)?request.getParameter("txtEmail"):"";
		String contacto = (request.getParameter("contacto")!=null)?request.getParameter("contacto"):"";
		try{
			Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
			BeanAfiliacion.actualizaCorreoTelefono(contacto,telefono,mail);
			infoRegresar = "{\"success\":true,\"mensaje\":\"Los datos han sido actualizados correctamente\"}";
		} catch(Exception e){
			throw new AppException("Hubo un error al actualizar los datos",e);
		}
	}
%>
<%=infoRegresar%>
