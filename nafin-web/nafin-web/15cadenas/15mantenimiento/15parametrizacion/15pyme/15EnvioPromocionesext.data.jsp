<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.servicios.*,
		netropology.utilerias.*,
		java.io.File,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String nuevoCorreo	=	(request.getParameter("nuevoCorreo")!=null)?request.getParameter("nuevoCorreo"):"";
String confirmacionCorreo	=	(request.getParameter("confirmacionCorreo")!=null)?request.getParameter("confirmacionCorreo"):"";
String nuevoCelular	=	(request.getParameter("nuevoCelular")!=null)?request.getParameter("nuevoCelular"):"";
String confirmacionCelular	=	(request.getParameter("confirmacionCelular")!=null)?request.getParameter("confirmacionCelular"):"";
String recibeFactoraje	=	(request.getParameter("recibeFactoraje")!=null)?request.getParameter("recibeFactoraje"):"";
String desactivacion = "";

String infoRegresar = "";

Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB",Servicios.class);

String bancoFondeo = servicios.getBancoFondeo(iNoEPO);
String icTerminos = servicios.getVersionTerminosCondiciones(bancoFondeo);
String pymeRegistrada = servicios.recibePromociones(iNoCliente, bancoFondeo);
String servicioAceptado = servicios.servicioAceptado(iNoCliente, bancoFondeo);

if(pymeRegistrada.equals("1")){
	pymeRegistrada="S";
}else{
	pymeRegistrada="N";
}

JSONObject jsonObj = new JSONObject();

if(informacion.equals("valoresIniciales")){
	try{
		String modoAcceso = ((String)session.getAttribute("inicializar.FINALIZADO")!=null)?(String)session.getAttribute("inicializar.FINALIZADO"):"";
		String correoRegistrado = "";
		String numeroCelularRegistrado = "";
		List datosPyme = servicios.getDatosPymeFactorajeMovil(iNoCliente);
		if(datosPyme.size()==0){
			correoRegistrado = "--";
			numeroCelularRegistrado = "--";
		}else{
			correoRegistrado = datosPyme.get(0).toString();
			numeroCelularRegistrado = "044 "+datosPyme.get(1).toString();
		}
		
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("modoAcceso",modoAcceso);
		jsonObj.put("correoRegistrado",correoRegistrado);
		jsonObj.put("numeroCelularRegistrado",numeroCelularRegistrado);
		jsonObj.put("pymeRegistrada",pymeRegistrada);
		infoRegresar = jsonObj.toString();
	}catch (Exception e) {
			throw new AppException("Error al intentar mostrar Correo y Celular Registrados",e);
	}
}
else if(informacion.equals("Aceptar")||informacion.equals("ActivarServicio")){

	UtilUsr utilUsr = new UtilUsr();
	Usuario usuario = utilUsr.getUsuario(strLogin);
	String nombreUsuario = usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();

	List datosRegistroFactorajeMovil = new ArrayList();
	datosRegistroFactorajeMovil.add(iNoCliente);
	datosRegistroFactorajeMovil.add(bancoFondeo);
	datosRegistroFactorajeMovil.add(recibeFactoraje);
	datosRegistroFactorajeMovil.add(nuevoCorreo);
	datosRegistroFactorajeMovil.add(confirmacionCorreo);
	datosRegistroFactorajeMovil.add(nuevoCelular);
	datosRegistroFactorajeMovil.add(confirmacionCelular);
	datosRegistroFactorajeMovil.add(nombreUsuario);
	datosRegistroFactorajeMovil.add(strLogin);
	datosRegistroFactorajeMovil.add(icTerminos);
	
	if(informacion.equals("Aceptar")){
		try{
			desactivacion = "1";
			datosRegistroFactorajeMovil.add(desactivacion);
			servicios.guardarRegistroFactorajeMovil(datosRegistroFactorajeMovil);
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("mensaje", "Información guardada exitosamente");			
		}catch (Exception e) {
			jsonObj.put("success", new Boolean(false));
			String  msg	= "Error al actualizar los datos "+e;
			jsonObj.put("msg",msg	);
			throw new AppException("Error al actualizar los datos",e);
		}
	}
	else if (informacion.equals("ActivarServicio")){
		try{
			Comunes validacion = new Comunes();
			desactivacion = "0";
			datosRegistroFactorajeMovil.add(desactivacion);
			servicios.guardarRegistroFactorajeMovil(datosRegistroFactorajeMovil);
			
			List datosPyme = servicios.getDatosPymeFactorajeMovil(iNoCliente);
			
			String numeroCelular = (String)datosPyme.get(1);
			String claveConfirmacion = validacion.cadenaAleatoria(6);
			request.getSession().setAttribute("claveConfirmacion", claveConfirmacion.toLowerCase());
			
			SMS sms = new SMS();
			sms.enviarCodigoVerificacion(numeroCelular, claveConfirmacion.toLowerCase());				
			
		}catch (Exception e) {			
			String  msg	= "Error al Activar el servicio "+e;
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg",msg	);
			throw new AppException("Error al Activar el servicio",e);
	 
		}finally{
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("mensaje", "Le ha sido enviado a su celular registrado el Código de Verificación para continuar el registro.");
		}	
		
	}
	infoRegresar = jsonObj.toString();  
	 
}else if (informacion.equals("Continuar")){
		List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
		pantallasNavegacionComplementaria.remove("/15cadenas/15mantenimiento/15parametrizacion/15pyme/15EnvioPromocionesext.jsp"); //El nombre debe coincidir con el especificado en Navegación
		infoRegresar = "{\"success\": true, \"urlPagina\": \"" + appWebContextRoot + pantallasNavegacionComplementaria.get(0) + "\"}";
}else if (informacion.equals("DESCARGATerminos_y_Condiciones")){
		
		File file = null;
		try{
			bancoFondeo = servicios.getBancoFondeo(iNoEPO);
			icTerminos = servicios.getVersionTerminosCondiciones(bancoFondeo);
			file = servicios.getArchivoTerminosCondiciones(bancoFondeo, icTerminos, strDirectorioTemp);
		}catch(Exception e){
			System.out.println("..:: Error en DescargaConsultaPromocionPyme : "+e.toString());
		}
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+file.getName());
		infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>