Ext.onReady(function(){
	Ext.QuickTips.init();
//----------------------------------HADNLERS------------------------------------
	var procesaActualizacion = function(opts, success, response){
		var btnAceptar = Ext.getCmp('btnAceptar');
		btnAceptar.setIconClass('');
		var btnCancelar = Ext.getCmp('btnCancelar');
		var fp4 = Ext.getCmp('forma4');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.Msg.alert('Mensaje informativo',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){  
					window.location = '15datosperext.jsp';	 
			});
		}else{
			Ext.Msg.alert('Mensaje de Error',"�Error al actualizar!",function(btn){  
					window.location = '15datosperext.jsp';	 
			});
		}
	}
	var procesarConsultaData = function (store, arrRegistros, opts) {
		var lblNumProveedor = Ext.getCmp('lblNumProveedor');
		var lblNumDistribuidor = Ext.getCmp('lblNumDistribuidor');
		
		var lblApellidoPaterno = Ext.getCmp('lblApellidoPaterno');
		var lblApellidoMaterno = Ext.getCmp('lblApellidoMaterno');
		var lblNombres = Ext.getCmp('lblNombres');
		var lblCurp = Ext.getCmp('lblCURP');
		var lblPaisOrigen = Ext.getCmp('lblPaisOrigen');
		var lblRFC = Ext.getCmp('lblRFC');
		var lblFiel = Ext.getCmp('lblFiel');
		
		var lblRazonSocial = Ext.getCmp('lblRazonSocial');
		var lblNombreComercial = Ext.getCmp('lblNombreComercial');
		var lblFiel2 = Ext.getCmp('lblFiel2');
		var lblRFC2 = Ext.getCmp('lblRFC2');
		var lblPaisOrigen2 = Ext.getCmp('lblPaisOrigen2');
		
		var lblCalle = Ext.getCmp('lblCalle');
		var lblEstado = Ext.getCmp('lblEstado');
		var lblCodigoPostal = Ext.getCmp('lblCodigoPostal');
		var lblCiudad = Ext.getCmp('lblCiudad');
		var lblFax = Ext.getCmp('lblFax');
		var lblColonia = Ext.getCmp('lblColonia');
		var lblDelegacionMunicipio = Ext.getCmp('lblDelegacionMunicipio');
		var lblPais = Ext.getCmp('lblPais');
		var lblTelefono = Ext.getCmp('lblTelefono');
		
		var txtNumTelefonico = Ext.getCmp('txtNumTelefonico');
		var txtNumEmail = Ext.getCmp('txtNumEmail');
		
		var fp1 = Ext.getCmp('forma1');
		fp1.el.unmask();
		var fp2 = Ext.getCmp('forma2');
		fp2.el.unmask();
		var fp3 = Ext.getCmp('forma3');
		fp3.el.unmask();
		var fp4 = Ext.getCmp('forma4');
		fp4.el.unmask();
		
		if(arrRegistros!= null) {	
			if(store.getTotalCount() > 0) {
				var row = store.getAt(0);
				
				if(row.get('NUMERO_DE_CLIENTE')!=""){
					lblNumProveedor.getEl().update(row.get('NUMERO_DE_CLIENTE'));
					lblNumDistribuidor.hide();
				} else if(row.get('NUMERO_DE_CLIENTE_DIST')!=""){
					lblNumDistribuidor.getEl().update(row.get('NUMERO_DE_CLIENTE_DIST'));
					lblNumProveedor.hide();
				}
				if(row.get('CS_TIPO_PERSONA')=="F"){
					fp1.show();
					fp2.hide();
					lblApellidoPaterno.getEl().update(row.get('APELLIDO_PATERNO'));
					lblApellidoMaterno.getEl().update(row.get('APELLIDO_MATERNO'));
					lblNombres.getEl().update(row.get('NOMBRE'));
					lblCurp.getEl().update(row.get('CURP'));
					lblPaisOrigen.getEl().update(row.get('PAISORIGEN'));
					lblRFC.getEl().update(row.get('CG_RFC'));
					lblFiel.getEl().update(row.get('FIEL'));
				}else if (row.get('CS_TIPO_PERSONA')=="M"){
					fp1.hide();
					fp2.show();
					lblRazonSocial.getEl().update(row.get('CG_RAZON_SOCIAL'));
					lblNombreComercial.getEl().update(row.get('CG_NOMBRE_COMERCIAL'));
					lblFiel2.getEl().update(row.get('FIEL'));
					lblRFC2.getEl().update(row.get('CG_RFC'));
					lblPaisOrigen2.getEl().update(row.get('PAISORIGEN'));
				}
				lblCalle.getEl().update(row.get('CG_CALLE'));
				lblEstado.getEl().update(row.get('ESTADO'));
				lblCodigoPostal.getEl().update(row.get('CN_CP'));
				lblCiudad.getEl().update(row.get('CIUDAD'));
				lblFax.getEl().update(row.get('CG_FAX'));
				lblColonia.getEl().update(row.get('CG_COLONIA'));
				lblDelegacionMunicipio.getEl().update(row.get('DELEGACION_MUNICIPIO'));
				lblPais.getEl().update(row.get('PAIS'));
				lblTelefono.getEl().update(row.get('CG_TELEFONO1'));
				
				txtNumTelefonico.setValue(row.get('TELEFONO_C'));
				txtNumEmail.setValue(row.get('EMAIL_C'));
				
			}else {
				fp1.el.mask('No se encontr� ning�n registro','x-mask');
				fp2.el.mask('No se encontr� ning�n registro','x-mask');
				fp3.el.mask('No se encontr� ning�n registro','x-mask');
				fp4.el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
//-----------------------------------STORES-------------------------------------
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15datosperext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CIUDAD'},
			{name: 'PAISORIGEN'},
			{name: 'STRHABANT'},
			{name: 'STRHABINV'},
			{name: 'STRTAFILIA'},
			{name: 'NUMERO_DE_CLIENTE'},
			{name: 'NUMERO_DE_CLIENTE_DIST'},
			{name: 'APELLIDO_PATERNO'},
			{name: 'APELLIDO_MATERNO'},
			{name: 'NOMBRE'},
			{name: 'CG_RFC'},			
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'CG_CALLE'},
			{name: 'CG_NUMERO_EXT'},
			{name: 'CG_NUMERO_INT'},
			{name: 'CG_COLONIA'},
			{name: 'IC_ESTADO'},
			{name: 'ESTADO'},
			{name: 'IC_PAIS'},
			{name: 'PAIS'},
			{name: 'DELEGACION_MUNICIPIO'},
			{name: 'CN_CP'},
			{name: 'CG_TELEFONO1'},
			{name: 'CG_FAX'},
			{name: 'CG_EMAIL'},
			{name: 'APELLIDO_PATERNO_C'},
			{name: 'APELLIDO_MATERNO_C'},
			{name: 'NOMBRE_C'},
			{name: 'TELEFONO_C'},
			{name: 'FAX_C'},
			{name: 'EMAIL_C'},
			{name: 'IC_DOMICILIO_EPO'},
			{name: 'IC_CONTACTO'},
			{name: 'CG_LOGIN'},
			{name: 'CS_TIPO_PERSONA'},
			{name: 'CG_NOMBRE_COMERCIAL'},
			{name: 'CG_APPAT_REP_LEGAL'},
			{name: 'CG_APMAT_LEGAL'},
			{name: 'CG_NOMBREREP_LEGAL'},
			{name: 'TELEFONO_REP_LEGAL'},
			{name: 'FAX_REP_LEGAL'},
			{name: 'EMAIL_REP_LEGAL'},
			{name: 'IC_IDENTIFICACION'},
			{name: 'IC_GRADO_ESC'},
			{name: 'IC_TIPO_CATEGORIA'},
			{name: 'CG_NO_ESCRITURA'},
			{name: 'IN_NUMERO_NOTARIA'},
			{name: 'IN_EMPLEOS_GENERAR'},
			{name: 'FN_ACTIVO_TOTAL'},
			{name: 'FN_CAPITAL_CONTABLE'},
			{name: 'FN_VENTAS_NET_EXP'},
			{name: 'FN_VENTAS_NET_TOT'},
			{name: 'DF_CONSTITUCION'},
			{name: 'IN_NUMERO_EMP'},
			{name: 'IC_SECTOR_ECON'},
			{name: 'IC_SUBSECTOR'},
			{name: 'IC_RAMA'},
			{name: 'IC_CLASE'},
			{name: 'IC_TIPO_EMPRESA'},
			{name: 'IC_DOM_CORRESPONDENCIA'},
			{name: 'CG_PRODUCTOS'},
			{name: 'NUMSIRAC'},
			{name: 'EDO_CIVIL'},
			{name: 'PAIS_ORIGEN'},
			{name: 'NACIMIENTO'},
			{name: 'CG_REGIMEN_MAT'},
			{name: 'CG_SEXO'},
			{name: 'CG_APP_CASADA'},
			{name: 'IC_MUNICIPIO'},
			{name: 'CG_RFC_LEGAL'},
			{name: 'CG_NO_IDENTIFICACION'},
			{name: 'CD_VERSION_CONVENIO'},
			{name: 'DF_VERSION_CONVENIO'},
			{name: 'IN_NUMERO_TROYA'},
			{name: 'CURP'},
			{name: 'FIEL'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});
//---------------------------------ELEMENTOS------------------------------------
	var elementosForma = [
									{
										xtype: 'label',
										id: 'lblNumProveedor',
										fieldLabel: 'N�mero Proveedor',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblNumDistribuidor',
										fieldLabel: 'N�mero Distribuidor',
										text: '-'
									}
								];
	var elementosForma1 = [{
		xtype: 'container',
		anchor: '100%',
		layout: 'column',
		items: [
					{
						xtype: 'container',
						id:	'panelIzquierdo',
						columnWidth:.5,
						layout: 'form',
						items: [
									{
										xtype: 'label',
										id: 'lblApellidoPaterno',
										fieldLabel: 'Apellido Paterno',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblNombres',
										fieldLabel: 'Nombre(s)',
										text: '-'	
									},
									{
										xtype: 'label',
										id: 'lblCURP',
										fieldLabel: 'Curp',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblPaisOrigen',
										fieldLabel: 'Pa�s de Origen',
										text: '-'
									}
								]
						},
						{
						xtype: 'container',
						id: 'panelDerecho',
						columnWidth:.5,
						layout: 'form',
						items: [
									{
										xtype: 'label',
										id: 'lblApellidoMaterno',
										fieldLabel: 'Apellido Materno',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblRFC',
										fieldLabel: 'R.F.C.',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblFiel',
										fieldLabel: 'Fiel',
										text: '-'
									}
								]
							}
					]
	}];
	var elementosForma2 = [{
		xtype: 'container',
		anchor: '100%',
		layout: 'column',
		items: [
					{
						xtype: 'container',
						id:	'panelIzquierdo2',
						columnWidth:.5,
						layout: 'form',
						items: [
									{
										xtype: 'label',
										id: 'lblRazonSocial',
										fieldLabel: 'Razon Social',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblNombreComercial',
										fieldLabel: 'NombreComercial',
										text: '-'	
									},
									{
										xtype: 'label',
										id: 'lblFiel2',
										fieldLabel: 'Fiel',
										text: '-'
									}
								]
						},
						{
						xtype: 'container',
						id: 'panelDerecho2',
						columnWidth:.5,
						layout: 'form',
						items: [
									{
										xtype: 'label',
										id: 'lblRFC2',
										fieldLabel: 'R.F.C.',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblPaisOrigen2',
										fieldLabel: 'Pais de Origen',
										text: '-'
									}
								]
							}
					]
	}];
	var elementosForma3 = [{
		xtype: 'container',
		anchor: '100%',
		layout: 'column',
		items: [
					{
						xtype: 'container',
						id:	'panelIzquierdo3',
						columnWidth:.5,
						layout: 'form',
						items: [
									{
										xtype: 'label',
										id: 'lblCalle',
										fieldLabel: 'Calle, No.Exterior y No.Interior',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblEstado',
										fieldLabel: 'Estado',
										text: '-'	
									},
									{
										xtype: 'label',
										id: 'lblCodigoPostal',
										fieldLabel: 'CodigoPostal',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblCiudad',
										fieldLabel: 'Ciudad',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblFax',
										fieldLabel: 'Fax',
										text: '-'
									}
								]
						},
						{
						xtype: 'container',
						id: 'panelDerecho3',
						columnWidth:.5,
						layout: 'form',
						items: [
									{
										xtype: 'label',
										id: 'lblColonia',
										fieldLabel: 'Colonia',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblDelegacionMunicipio',
										fieldLabel: 'Delegaci�n o Municipio',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblPais',
										fieldLabel: 'Pa�s',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblTelefono',
										fieldLabel: 'Tel�fono',
										text: '-'
									}
								]
							}
					]
	}];
	var elementosForma4 = [{
		xtype: 'container',
		anchor: '100%',
		layout: 'column',
		items: [
					{
						xtype: 'container',
						id:	'panelIzquierdo4',
						columnWidth:.5,
						layout: 'form',
						items: [
									{
										xtype: 'numberfield',
										name: 'numTelefonico',
										id: 'txtNumTelefonico',
										fieldLabel: 'Tel�fono',
										allowBlank: true,
										allowDecimals: false,
										allowNegative: false,
										decimalPrecision: 0,
										maxLength: 10,
										minValue: 0,
										anchor: '70%'
									}
								]
						},
						{
						xtype: 'container',
						id: 'panelDerecho4',
						columnWidth:.5,
						layout: 'form',
						items: [
									{
										xtype: 'textfield',
										name: 'txtEmail',
										id: 'txtNumEmail',
										fieldLabel: 'E-mail',
										allowBlank: true,
										anchor: '90%',
										vtype: 'email',
										msgTarget: 'side'
									}
								]
							}
					]
	}];
	var fp = new Ext.FormPanel({
		id: 'forma',
		width: 885,
		style: ' margin:0 auto;',
		title: '',
		hidden: false,
		frame: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfield',
		items: elementosForma,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		monitorValid: false
	});
	var fp1 = new Ext.FormPanel({
		id: 'forma1',
		width: 885,
		style: ' margin:0 auto;',
		title: 'Nombre Completo',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma1,
		monitorValid: false
	});
	var fp2 = new Ext.FormPanel({
		id: 'forma2',
		width: 885,
		style: ' margin:0 auto;',
		title: 'Nombre de la Empresa',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma2,
		monitorValid: false
	});
	var fp3 = new Ext.FormPanel({
		id: 'forma3',
		width: 885,
		style: ' margin:0 auto;',
		title: 'Domicilio',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma3,
		monitorValid: false
	});
	var fp4 = new Ext.FormPanel({
		id: 'forma4',
		width: 885,
		style: ' margin:0 auto;',
		title: 'Datos del contacto',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma4,
		monitorValid: false,
		buttons: [
						{
							text: 'Aceptar',
							id: 'btnAceptar',
							iconCls: 'aceptar',
							formBind: true,
							handler: function (boton,evento){
								boton.disable();
								boton.setIconClass('loading-indicator');
								var store = consultaData;
								var row = consultaData.getAt(0);
								var contacto = row.get('IC_CONTACTO');
								var txtEmail =Ext.getCmp('txtNumEmail');
								if(!Ext.form.VTypes.email(txtEmail.getValue())){
									txtEmail.markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato "usuario@dominio.com"');
									boton.setIconClass('');
									boton.enable();
									txtEmail.focus();
									return;
								};
								Ext.Ajax.request({
									url:'15datosperext.data.jsp',
									params:Ext.apply(fp4.getForm().getValues(),{
												informacion: 'ActualizarDatos',
												contacto: contacto
									}),
									callback: procesaActualizacion
								});
							}
						},
						{
							text: 'Cancelar',
							id: 'btnCancelar',
							iconCls: 'borrar',
							handler: function(){
							window.location = '15datosperext.jsp'
						}
					}
		]
	});
	var fp5 = new Ext.FormPanel({
		style: 'margin:0 auto;',
		title: '',
		hidden: false,
		frame: false,
		titleCollapse: false,
		bodyStyle: 'padding: 30px',
		labelWidth: 130,
		defaultType: 'textfield',
		height:100,
		width: 885,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		items: [{
        xtype: 'label',
		  id: 'lblNota',
        fieldLabel: 'IMPORTANTE',
		  text: 'Si los datos incorrectos son tel�fono o e-mail favor de actualizarlos en �sta p�gina.'+'\n'+
					'Si alg�n otro dato est� incorrecto favor de reportarlo al 01800 623 46 72'
		}]
});
//---------------------------------PRINCIPAL------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			fp1,
			fp2,
			fp3,
			fp4,
			fp5
		]
	});
});