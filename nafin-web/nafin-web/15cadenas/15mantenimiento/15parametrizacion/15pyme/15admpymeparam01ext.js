	Ext.onReady(function() {
	
	//--------------------------------HANDLERS-----------------------------------
	var procesarConsultaData = function (store, arrRegistros, opts) {
	   
		var fp = Ext.getCmp('forma');
	   fp.el.unmask();
		if (arrRegistros != null) {
		
			if(!grid.isVisible()) {
				grid.show();
			}
		var btnBajarPDF = Ext.getCmp('btnBajarPDF');
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		var el = grid.getGridEl();
		
		if(store.getTotalCount() > 0) {
			btnGenerarPDF.enable();
			btnBajarPDF.hide();
			if(!btnBajarPDF.isVisible()){
				btnGenerarPDF.enable();
				} else {
					btnGenerarPDF.disable();
				}
			el.unmask();
			}else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro','x-mask');	
			}
		}
	}
	
	var procesarSuccessFailureGenerarPDF = function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success == true si la petici�n Ajax fue exitosa.)
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarPDF.setHandler(function (boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//---------------------------------STORES------------------------------------
	
	var catalogoEPOData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave','descripcion','loadMsg'],
		url: '15admpymeparam01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPOPyme'
			},
			totalProperty: 'total',
			autoload: false,
			listeners:  {
				exception: NE.util.mostrarDataProxyError,
				beforeload:	NE.util.initMensajeCargaCombo
			            }
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
			root : 'registros',
			fields : ['clave', 'descripcion', 'loadMsg'],
			url : '15admpymeparam01ext.data.jsp',
			baseParams : {
				informacion: 'CatalogoMoneda'
			},
			totalProperty : 'total',
			autoload: false,
			listeners: {
					exception: NE.util.mostrarDataProxyError,
					beforeload: NE.util.initMensajeCargaCombo
			}
		});
		
	var consultaData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '15admpymeparam01ext.data.jsp',
		baseParams : {
			informacion : 'Consulta'
		},
		reader: new Ext.data.JsonReader({
			root: 'registros',
			totalProperty: 'total',
			fields: [
				{name: 'RAZON_SOCIAL'},
				{name: 'CG_BANCO'},
				{name: 'CG_NUMERO_CUENTA'},
				{name: 'CG_SUCURSAL'},
				{name: 'CG_RAZON_SOCIAL'}
				]
		}),
		groupField: 'RAZON_SOCIAL',
      sortInfo:{field: 'RAZON_SOCIAL', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
			 fn: function(proxy, type, action, optionRequest, response, args) {
			  NE.util.mostrarDataProxyError(proxy, type, action, optionRequest, response, args);
			  //Llama procesar consulta, para que desbloquee los componentes.
			  procesarConsultaData(null,null,null);
			}
		}
	}
});
//---------------------------------------------------------------------------
	var elementosForma = [
			{
				xtype: 'combo',
				name: 'ic_epo',
				id: 'cboEpo',
				allowBlank:false,
				fieldLabel : 'Nombre de la EPO',
				mode: 'local',
				displayField : 'descripcion',
				valueField: 'clave',
				hiddenName: 'ic_epo',
				emptyText: 'Seleccione un EPO',
				forceSeleccion : true,
				triggerAction: 'all',
				typeAhead: true,
				minChars : 1,
				store : catalogoEPOData,
				tpl: NE.util.templateMensajeCargaCombo
				},
				{
				xtype: 'combo',
				name: 'comcat_moneda',
				id: 'cmbMoneda',
				allowBlank: false,
				fieldLabel : 'Moneda',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'comcat_moneda',
				emptyText: 'Seleccionar',
				forceSelection : true,
				triggerAction: 'all',
				typeAhead: true,
				minChars : 1,
				store : catalogoMonedaData,
				tpl : NE.util.templateMensajeCargaCombo
				}
			];
			
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns:[
				{
				header:'IF',
				tooltip: 'IF',
				sortable: 'true',
				dataIndex: 'RAZON_SOCIAL',
				hidden: true
				},
				{
				header:'Banco de Servicio',
				tooltip: 'Banco de Servicio',
				sortable: 'true',
				dataIndex: 'CG_BANCO',
				width: 250,
				hidden: false
				},
				{
				header:'No. de Cuenta',
				tooltip: 'No. de Cuenta',
				sortable: 'true',
				dataIndex: 'CG_NUMERO_CUENTA',
				width: 150,
				hidden: false,
				align: 'center'
				},
				{
				header:'Sucursal',
				tooltip: 'Sucursal',
				sortable: 'true',
				dataIndex: 'CG_SUCURSAL',
				width: 150,
				hidden: false,
				align: 'center'
				},
				{
				header: 'EPO',
				tooltip: 'EPO',
				sortable: 'true',
				dataIndex: 'CG_RAZON_SOCIAL',
				width: 250,
				hidden: false,
				align: 'center'
				}
				],
		view: new Ext.grid.GroupingView({
            forceFit: true,
				groupTextTpl: '{text}'
        }),
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask:true,
		frame: true,
		width: 800,
		height: 400,
		style: 'margin:0 auto',
		title: 'Cuentas Bancarias',
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cboEpo = Ext.getCmp("cboEpo");
						var cmbMoneda = Ext.getCmp("cmbMoneda");
						Ext.Ajax.request({
							url: '15admpymeparam01ext.data.jsp',
							params: {
								informacion: 'ArchivoPDF',
								ic_epo: cboEpo.getValue(),
								comcat_moneda: cmbMoneda.getValue()
							},
							callback: procesarSuccessFailureGenerarPDF
						});
					}	
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-'
			]
		}
		}); 
	//--------------------------------PRINCIPAL----------------------------------
	var fp = new Ext.form.FormPanel({ 
		id: 'forma',
		width: 800, //la anchura del panel
		title: 'Cuentas Bancarias', //el t�tulo del panel
		frame: true,  
		collapsible: true,
		style: 'margin:0 auto',
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
						msgTarget: 'side',
						anchor: '-20'
					},
		items: elementosForma,
		monitorValid:true,
		buttons: [
				{
					text: 'Consultar',
					iconCls: 'icoBuscar',
					formBind: true,
					handler: function(boton, evento) {

						fp.el.mask('Enviando...','x-msk-loading');
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(), {
								operacion: 'Generar',
								start: 0,
								limit: 5
							})
						});
						Ext.getCmp('btnGenerarPDF').enable();
						Ext.getCmp('btnBajarPDF').hide();
					}
				},
				{
					text:'Limpiar',
					iconCls: 'icoLimpiar',
					handler: function() {
						fp.getForm().reset();
						grid.hide();
						}
				}]
}); 
		
// La aplicaci�n se mostrar� en un div de cierto ancho, para lo cual se define el siguiente contenedor.
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
         NE.util.getEspaciador(20),
			grid
		]
	});
//------------------------------------------------------------------------------
	catalogoEPOData.load();
	catalogoMonedaData.load();
	
});
