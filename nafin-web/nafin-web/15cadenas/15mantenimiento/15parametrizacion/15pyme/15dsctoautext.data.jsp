<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,
		java.sql.*,		
		java.util.Vector,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		net.sf.json.JSONArray,
		net.sf.json.util.JSONUtils,
		netropology.utilerias.*,
		com.netro.exception.*,
      javax.naming.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoPYME,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%

String accion 			= (request.getParameter("accion")!=null)?request.getParameter("accion"):"";
String infoRegresar 	= "";

if(accion.equals("GUARDAR")){
		
	String   buffer			= request.getParameter("ultimoIndice");
	buffer						= buffer == null || buffer.trim().equals("")?"-1":buffer.trim();
	int    	ultimoIndice 	= Integer.parseInt(buffer);
	
	String   autorizacion   = request.getParameter("radioAutorizarDsctoAutomatico");
			
	String 	clavePyme 		= iNoCliente;
	String 	claveEpo 		= iNoEPO;
	
	// Formatear parametros leidos para que puedan ser pasados al metodo ParametrosDescuento.setParametrosDsctoAutomatico
	int		numeroElementos			= ultimoIndice + 1;
	String[] if_cta						= new String[numeroElementos];
	String[] hidHuboCambio				= new String[numeroElementos];
	String[] hidHuboCambioDia			= new String[numeroElementos];
	String[] hidDsctoAutomaticoDia	= new String[numeroElementos];
	String[] hidIcEpo						= new String[numeroElementos];
	String[] hidIcMoneda					= new String[numeroElementos];
	
	for(int i=0;i<numeroElementos;i++){
		
		if_cta[i] 							= request.getParameter("intermediarioFinanciero"+i);
		hidHuboCambio[i] 					= request.getParameter("hidHuboCambio"+i);
		hidHuboCambioDia[i] 				= request.getParameter("hidHuboCambioDia"+i);
		hidDsctoAutomaticoDia[i]		= request.getParameter("modalidad"+i);				
		hidIcEpo[i]							= request.getParameter("hidIcEpo"+i); 
		hidIcMoneda[i]						= request.getParameter("hidIcMoneda"+i);
				
	}
	
	// Obtener EJB ParametrosDescuentoEJB
	ParametrosDescuento 			parametrosDescuento 		= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

	// Guardar Nueva Parametrizacion
	HashMap 							resultado 					= parametrosDescuento.setParametrosDsctoAutomatico(
																					autorizacion,
																					clavePyme, 
																					claveEpo, 
																					iNoUsuario,
																					if_cta, 
																					hidHuboCambio,	
																					hidHuboCambioDia,	
																					hidDsctoAutomaticoDia, 
																					hidIcEpo, 
																					hidIcMoneda
																				);
	
	// Debido a que la modalidad cambia dependiendo del valor seleccionado
	AccesoDB		con			= new AccesoDB();
	ResultSet	rs				= null;

	JSONObject 	modalidad		= new JSONObject();
	HashMap		intermediario	= new HashMap();
	try {	
		
		// Construir relacion de indices con las parejas (ic_epo, ic_moneda)
		HashMap indiceAsociado = new HashMap();
		for(int i=0;i<numeroElementos;i++){
			
			String llave = request.getParameter("hidIcEpo"+i) + "," + request.getParameter("hidIcMoneda"+i);
			String valor = String.valueOf(i);
			
			indiceAsociado.put(llave,valor);
			
		}
		
		// Conectarse a la base de datos
		con.conexionDB();
		
		// Definir parametros de la consulta
		ParametrizacionDsctoAutomaticoDE parametros 	= new ParametrizacionDsctoAutomaticoDE();
		CQueryHelperExtJS 					queryHelper = new CQueryHelperExtJS(parametros);
		parametros.setClavePyme(Integer.parseInt(clavePyme));	
		parametros.setTipoConsulta(ParametrizacionDsctoAutomaticoDE.PARAMETRIZACION);
		
		String icEpo 				= null;
		String icIf 				= null;
		String nombreIf 			= null;
		String icMoneda			= null;
		String icCtaBancaria		= null;
		String nombreBanco 		= null;
		String numeroCuenta 		= null;
		String numeroSucursal 	= null;
		String dia 					= null;
		String dsctoAut 			= null;
		String nombreMoneda 		= null;
		int    numMonedas 		= 0;
		
		String 		icEpoAnt						= "";
		String 		monedaAnt					= "";
		int   		r 								= 0;
		HashMap		intermediarioFinanciero	= null;
		ArrayList	registroTemporal			= null;
		String 		indice 						= null; 
		ArrayList	data							= null;
      String      dsctoObligatorio        = null;
		
		rs = queryHelper.getCreateFile(request, con);
		while( rs != null && rs.next() ){
			
			icEpo 				= rs.getString("IC_EPO");
			icIf 					= rs.getString("IC_IF");
			nombreIf				= rs.getString("NOMBRE_IF");
			icCtaBancaria		= rs.getString("IC_CUENTA_BANCARIA");
			nombreBanco			= rs.getString("CG_BANCO");
			numeroCuenta		= rs.getString("CG_NUMERO_CUENTA");
			numeroSucursal 	= rs.getString("CG_SUCURSAL");
			dsctoAut				= rs.getString("CS_DSCTO_AUTOMATICO");
			nombreMoneda		= rs.getString("CD_NOMBRE");
			icMoneda				= rs.getString("IC_MONEDA");
			dsctoObligatorio	= rs.getString("OBLIGATORIO");
			numMonedas			= rs.getInt("NUMMONEDAS");
			
			if(!icEpo.equals(icEpoAnt)){
				monedaAnt 		= "";
			}
			
			if(!nombreMoneda.equals(monedaAnt)){
				
				dia 	= "";
				r 		= 1;
				
				intermediarioFinanciero = new HashMap();
				indice 						= (String) indiceAsociado.get(icEpo+","+icMoneda);
				intermediario.put("intermediarioFinanciero"+indice,intermediarioFinanciero);
				
				data = new ArrayList();
				intermediarioFinanciero.put("data",			data);
				
				
				registroTemporal = new ArrayList();
				if ("N".equals(dsctoObligatorio)) {
					registroTemporal.add("");
					registroTemporal.add("No parametrizar IF para esta EPO");
					data.add(registroTemporal);					
				}else {
					registroTemporal.add("OBLG");
					registroTemporal.add("Seleccione IF para esta EPO");
					data.add(registroTemporal);
				}
				
			}
			
			registroTemporal = new ArrayList();
			registroTemporal.add(icIf +"|" + icCtaBancaria +"|" + dsctoAut);
			registroTemporal.add(nombreIf +" Banco: "+nombreBanco+" Cta: "+numeroCuenta+" Suc: "+numeroSucursal);
			data.add(registroTemporal);
			
			if("S".equals(dsctoAut)){ // "selected"
				intermediarioFinanciero.put("selected", (icIf +"|" + icCtaBancaria +"|" + dsctoAut));
			}
			
			if("S".equals(dsctoAut)){
				dia				= rs.getString("CS_DSCTO_AUTOMATICO_DIA");
			}
			
			if(r == numMonedas){
				
				if( intermediarioFinanciero.get("selected") == null ){
					intermediarioFinanciero.put("selected",	((ArrayList)data.get(0)).get(0) );
				}
				
				dia = ( !"P".equals(dia) && !"U".equals(dia) )?"P":dia;// Emular combo html
				
				modalidad.put("modalidad"+indice,dia);
 
			}
			
			icEpoAnt  = icEpo;
			monedaAnt = nombreMoneda;
			
			r++;
			
		}
		
	}catch(Exception e){
		
		throw e;
		
	}finally{
		
		if(rs != null){ try{ rs.close(); }catch(Exception e){} }
		
		if( con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
		
	}
	
	// Enviar respuesta de la operacion realizada
	JSONObject respuesta = JSONObject.fromObject(resultado);
	respuesta.put("modalidad",			modalidad);
	respuesta.put("intermediario",	JSONObject.fromObject(intermediario));
	
   infoRegresar = respuesta.toString();
	
}else if(accion.equals("GET_COMPONENTES_FORMA")){
 
	AccesoDB 	con = new AccesoDB();
	ResultSet 	rs	 = null;
	
	ParametrizacionDsctoAutomaticoDE parametros 	= new ParametrizacionDsctoAutomaticoDE();
	CQueryHelperExtJS 					queryHelper = new CQueryHelperExtJS(parametros);
 
	boolean 		esUsuarioPyme 				= false;
	boolean 		esUsuarioNafin 			= false;
	String 		clavePyme 					= iNoCliente;
	boolean 		pantallaDeshabilitada	= false;
	String 		claveEpo 					= iNoEPO;
	boolean 		hayClaveEPO 				= (claveEpo != null && !claveEpo.equals(""))?true:false;
	
	JSONObject 	respuesta 					= new JSONObject();
	boolean		success						= true;
	
	try {
	
		con.conexionDB();
		
		// Determinar tipo de usuario
		esUsuarioPyme 	= strTipoUsuario.equals("PYME");
		esUsuarioNafin	= strTipoUsuario.equals("NAFIN");
		
		// Obtener la clave de la PYME
		parametros.setClavePyme(Integer.parseInt(clavePyme));	
 
		// A. Si es usuario pyme
		JSONArray componentesPrincipales = new JSONArray();
		if(esUsuarioPyme){
			
			//  1. Si es convenio cuatro o convenio tres la funcionalidad de esta pantalla queda deshabilitada 
			ParametrosDescuento 			parametrosDescuento 		= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
	
			if( hayClaveEPO && parametrosDescuento.esConvenioTres(claveEpo,clavePyme)){
				
				pantallaDeshabilitada = true;
				respuesta.put("pantallaDeshabilitada",new Boolean(pantallaDeshabilitada));
				
				componentesPrincipales.add(
					"{ "  +
					"			xtype: 	'label',  "  +
					"			html: 	'Esta opci&oacute;n ha sido deshabilitada.', " +  
					"			cls:		'x-form-item', "  +
					"			style: { "  +
					"				width: 			'100%', "  +
					"				marginBottom: 	'10px', "  +
					"				textAlign:		'center', "  +
					"				fontWeight:		'bold', "  +
					"				color:			'#ff0000' " +
					"			} "  +
					"} "  
				);
				respuesta.put("componentesPrincipales",componentesPrincipales);
				
				throw new Exception("Pantalla Desahbilitada");
			}
			
		}
		
		String autorizacion = request.getParameter("rdAutoriza");
		autorizacion = autorizacion == null?"":autorizacion.trim();
		
		// B. Si el usuario no ha cambiado la seleccion para operar descuento automatico
		// consultar valor en la base de datos.
		String 	autorizaValorOriginal 				= "";
		// Consultar descuento automatico	
		parametros.setTipoConsulta(ParametrizacionDsctoAutomaticoDE.DESCUENTO_AUTOMATICO_PYME);
		rs = queryHelper.getCreateFile(request, con);
		if( rs != null && rs.next()){
			autorizaValorOriginal = rs.getString(1);
		}
		if(rs != null) rs.close();
		
		if("".equals(autorizacion)){
			autorizacion = autorizaValorOriginal;
		}
		
		// Si ya se tiene la autorizacion, activar bandera
		respuesta.put("autorizacion", new Boolean("S".equals(autorizacion)) );
		// Enviar valor original de la autorizacion a descuento.
		componentesPrincipales.add(
			"					{ "  +
			"						xtype: 'hidden', "  +
			"						name:	 'autorizaValorOriginal', "  +
			"						id:	 'autorizaValorOriginal', "  +	
			"						value: '"+autorizaValorOriginal+"' "  +
			"					} " 
		);
		
		// C. Si esta habilitada la operacion de descuento automatico, buscar las EPOs con dscto automatico obligatorio
		boolean hayEposConDescuentoObligatorio = false;
		ArrayList listaEPOSConDsctoObligatorio = new ArrayList();
		if("S".equals(autorizacion)){
			parametros.setTipoConsulta(ParametrizacionDsctoAutomaticoDE.EPOS_DSCTO_AUTOMATICO_OBLIGATORIO);
			rs = queryHelper.getCreateFile(request, con);
			while(rs != null && rs.next()){
				listaEPOSConDsctoObligatorio.add(
					(rs.getString("NOMBRE")==null?"":rs.getString("NOMBRE"))
				);
			}
		}
		if(rs != null) rs.close();
		hayEposConDescuentoObligatorio = listaEPOSConDsctoObligatorio.size() > 0;
 
		// D. Si es Usuario Nafin - Mostrar label datos Pyme
		if(esUsuarioNafin){
			componentesPrincipales.add(
				"{ "  +
				"			xtype: 	'label',  "  +
				"			html: 	"+JSONUtils.valueToString((Object)(session.getAttribute("strNombrePymeAsigna")+"<br>No. Nafin Electrónico: "+session.getAttribute("strNePymeAsigna")))+", "  +
				"			cls:		'x-form-item', "  +
				"			style: { "  +
				"				width: 			'100%', "  +
				"				marginBottom: 	'10px', "  +
				"				textAlign:		'center', "  +
				"				fontWeight:		'bold' "  +
				"			} "  +
				"} "  
			);
		}
		
		// E. Mostrar labelMensaje Autorizacion
		componentesPrincipales.add(
			"{ "  +
			"  xtype:   'label', "  +
			"  text:    'Autorizo expresamente que todos los DOCUMENTOS que me sean publicados en la CADENA PRODUCTIVA, '+ "  +
			"           'sean descontados automáticamente con el INTERMEDIARIO FINANCIERO y los recursos que se generen '+ "  +
			"           'por dicho descuento me sean depositados en la cuenta que aparece a continuación.', "  +
			"  cls:'x-form-item', "  +
			"  style: { "  +
			"     width:   '100%', "  +
			"     marginBottom:  '10px', "  +
			"     textAlign:'justify' "  +
			"  } "  +
			"} " 
		);
		
		// F. Si bDsctoObligatorio == true Mostrar las EPOs parametrizadas para operar descuento automatico obligatorio con la pyme
		if(hayEposConDescuentoObligatorio){
			
			StringBuffer listaEpos = new StringBuffer();
			for(int i=0;i<listaEPOSConDsctoObligatorio.size();i++){
				if(i>0) listaEpos.append("<br>");
				listaEpos.append((String) listaEPOSConDsctoObligatorio.get(i));				
			}	
			componentesPrincipales.add(
				"{ "  +
				"  xtype:   'label', "  +
				"	id:		'eposDsctoAutmaticoObligatorio', "  +
				"  html:    '<table style=\"width:100%;\">'+ "  +
				"              '<tr>'+ "  +
				"                 '<td class=\"x-form-item\" style=\"text-align:left;\">Operar <b>Descuento Automático Obligatorio</b> con:</td>'+ "  +
				"              '</tr>'+ "  +
				"              '<tr>'+ "  +
				"                 '<td class=\"x-form-item\" style=\"text-align:center;\">'+ "  +                    
				"                    "+ JSONUtils.valueToString((Object)listaEpos.toString())+" + "  +
				"                 '</td>'+ "  +
				"              '</tr>' + "  +
				"           '</table>', "  +
				"  cls:'x-form-item', "  +
				"  style: { "  +
				"     width:   '100%', "  +
				"     marginBottom:  '10px' "  +
				"  } "  +
				"}  "
			);
		}
		
		// G. Mostrar radio de autorizacion descuento automatico
		// NOTA: Falta definir parametro por default "S".equals(autorizacion) => checked
		componentesPrincipales.add(
			"{ "  +
			"  xtype:         'radiogroup', "  +
			"  fieldLabel:    \"Operar Descuento Automático\",  "  +
			"  name:          'radioAutorizarDsctoAutomatico', "  +
			"  id: 'radioAutorizarDsctoAutomatico', "  +
			( ( esUsuarioPyme && hayEposConDescuentoObligatorio ) || "S".equals(autorizacion)?" value: 'S', ":" value: 'N', ")  +
			//  G.1 Si el usuario es PYME y tiene epos configuradas para operar descuento automatico obligatorio
			//  No mostrar radiobotones
		   ( esUsuarioPyme && hayEposConDescuentoObligatorio?"  hidden: true, ":"")+
			"  columns:       6, "  +
			"  items:         [  "  +
			"        { "  +
			"           boxLabel:    \"Sí autorizo\",  "  +
			"           name:        'radioAutorizarDsctoAutomatico',  "  +
			( ( esUsuarioPyme && hayEposConDescuentoObligatorio ) || "S".equals(autorizacion)?" checked: true, ":"")  +
			"           inputValue:  \"S\" "  +
			"        }, "  +
			"        { "  +
			"           boxLabel:    \"No autorizo\",  "  +
			"           name:        'radioAutorizarDsctoAutomatico',  "  +
			( ( esUsuarioPyme && hayEposConDescuentoObligatorio ) || "S".equals(autorizacion)?"":"  checked: true, ")  +
			"           inputValue:  \"N\" "  +
			"        } "  +
			"  ], "  +
			"  style: { "  +
			"     paddingLeft: '10px' "  +
			"  } "  +
			"} "
		);
 
		// H. Consultar detalles de la parametrizacion
		if("S".equals(autorizacion)){
			String icEpo 				= null;
			String icIf 				= null;
			String nombreEpo 			= null;
			String nombreIf 			= null;
			String icCtaBancaria 	= null;
			String nombreBanco 		= null;
			String numeroCuenta 		= null;
			String numeroSucursal 	= null;
			String dsctoAut 			= null;
			int    numFilas 			= 0;
			String nombreMoneda 		= null;
			int    numMonedas 		= 0;
			String icMoneda			= null;
			String dsctoObligatorio = null;
			String numSelec 			= null;
			String dia 					= null;
			
			String icEpoAnt			= "";
			String monedaAnt 			= "";
			String mensajeTA 			= "";
			
			int 	 j 					= 0;
			int 	 i 					= 0;
			int    r 					= 0;
			int    numeroRegistro   = 0;
			
			JSONObject 	listaEpos 				= new JSONObject();
			ArrayList 	listaOrdenadaEpos 	= new ArrayList();
			JSONObject  registro  				= null;
			JSONArray	listaRegistros   		= null;
			JSONObject	registroEpoMoneda 	= null;
			JSONArray	registrosComboIF		= null;
			JSONArray	registroTemporal		= null;
			
			parametros.setTipoConsulta(ParametrizacionDsctoAutomaticoDE.PARAMETRIZACION);
			rs = queryHelper.getCreateFile(request, con);
			while(rs != null && rs.next()){
				
				icEpo 				= rs.getString("IC_EPO");
				icIf 					= rs.getString("IC_IF");
				nombreEpo			= rs.getString("NOMBRE_EPO");
				nombreIf				= rs.getString("NOMBRE_IF");
				icCtaBancaria		= rs.getString("IC_CUENTA_BANCARIA");
				nombreBanco			= rs.getString("CG_BANCO");
				numeroCuenta		= rs.getString("CG_NUMERO_CUENTA");
				numeroSucursal 	= rs.getString("CG_SUCURSAL");
				dsctoAut				= rs.getString("CS_DSCTO_AUTOMATICO");
				numFilas 			= rs.getInt("NUMFILAS");
				nombreMoneda		= rs.getString("CD_NOMBRE");
				numMonedas			= rs.getInt("NUMMONEDAS");
				icMoneda				= rs.getString("IC_MONEDA");
				dsctoObligatorio	= rs.getString("OBLIGATORIO");
				numSelec				= rs.getString("MARCA");//Indica si ya tiene seleccionada una cuenta
	 
				if(!icEpo.equals(icEpoAnt)){
	 
					if(i>0){
						
						registroEpoMoneda.put("indice", 				String.valueOf(numeroRegistro++));
						registroEpoMoneda.put("catalogoComboIF", 	registrosComboIF);
						listaRegistros.add(registroEpoMoneda);
						
						registro.put("listaRegistros",listaRegistros);
						listaEpos.put(registro.get("IC_EPO"),registro);
						listaOrdenadaEpos.add(registro.get("IC_EPO"));
						
						registroEpoMoneda = null;
						
					}
						
					registro = new JSONObject();
					registro.put("IC_EPO",     icEpo);
					registro.put("NOMBRE_EPO", nombreEpo);
					
					listaRegistros = new JSONArray();
					
					j++;
					monedaAnt = "";
					mensajeTA = "";
					
				}
	 
				if(!nombreMoneda.equals(monedaAnt)){
					
					dia 		 = "";
					if(i>0 && registroEpoMoneda != null){
						
						registroEpoMoneda.put("indice", 			  String.valueOf(numeroRegistro++));
						registroEpoMoneda.put("catalogoComboIF", registrosComboIF);
						listaRegistros.add(registroEpoMoneda);
						
					}
					
					registroEpoMoneda = new JSONObject();
					registrosComboIF  = new JSONArray();
	 
					registroEpoMoneda.put("NOMBRE_MONEDA", 		nombreMoneda);
					registroEpoMoneda.put("IC_MONEDA",     		icMoneda);
					
					registroTemporal = new JSONArray();
	
					if ("N".equals(dsctoObligatorio)) {
						registroTemporal.add("");
						registroTemporal.add("No parametrizar IF para esta EPO");
						registrosComboIF.add(registroTemporal);					
					}else {
						registroTemporal.add("OBLG");
						registroTemporal.add("Seleccione IF para esta EPO");
						registrosComboIF.add(registroTemporal);
					}
	 
					r = 1;
				}	// Fin de que sea un cambio en la moneda
				
				if("S".equals(dsctoAut)){
					mensajeTA = "El Intermediario Financiero a utilizar será el designado por la EPO en la selección automática para Descuento al vencimiento.";
				}
				
				registroTemporal = new JSONArray();
				registroTemporal.add(icIf +"|" + icCtaBancaria +"|" + dsctoAut);
				registroTemporal.add(nombreIf +" Banco: "+nombreBanco+" Cta: "+numeroCuenta+" Suc: "+numeroSucursal);
				registrosComboIF.add(registroTemporal);
				
				if("S".equals(dsctoAut)){ // "selected"
					registroEpoMoneda.put("DEFAULT_VALUE_COMBO_IF", (icIf +"|" + icCtaBancaria +"|" + dsctoAut));
				}
				
				if("S".equals(dsctoAut)){
					dia				= rs.getString("CS_DSCTO_AUTOMATICO_DIA");
				}
				
				if(r == numMonedas){
					
					if( registroEpoMoneda.get("DEFAULT_VALUE_COMBO_IF") == null ){ // Emular combo html 
						registroEpoMoneda.put("DEFAULT_VALUE_COMBO_IF", ((JSONArray) registrosComboIF.get(0)).get(0));
					}
					i++;
					
					if("P".equals(dia)){
						registroEpoMoneda.put("DEFAULT_VALUE_MODALIDAD", "P");
					}
					if("U".equals(dia)){
						registroEpoMoneda.put("DEFAULT_VALUE_MODALIDAD", "U");
					}
					if( !"P".equals(dia) && !"U".equals(dia) ){ // Emular combo html
						registroEpoMoneda.put("DEFAULT_VALUE_MODALIDAD", "P");
					}
					
					registroEpoMoneda.put("mensajeTA",mensajeTA);
					
				}
				
				icEpoAnt  = icEpo;
				monedaAnt = nombreMoneda;
				
				r++;
				
			}
			// Agregar ultimo registro
			if(i>0){
				
				registroEpoMoneda.put("indice", 			  String.valueOf(numeroRegistro++));
				registroEpoMoneda.put("catalogoComboIF", registrosComboIF);
				listaRegistros.add(registroEpoMoneda);
				
				registro.put("listaRegistros",listaRegistros);
				listaEpos.put(registro.get("IC_EPO"),registro);
				listaOrdenadaEpos.add(registro.get("IC_EPO"));
				
			}	
			if( rs != null ) rs.close();
			respuesta.put("listaEpos",listaEpos);
			
			// Agregar valor del ultimo indice
			componentesPrincipales.add(
				"					{ "  +
				"						xtype: 'hidden', "  +
				"						name:	 'ultimoIndice', "  +
				"						id:	 'ultimoIndice', "  +	
				"						value: '"+String.valueOf(numeroRegistro-1)+"' "  +
				"					} " 
			);
 
			// Construir lista de componentes
			JSONArray 	eposParametrizables 				= new JSONArray();
			String		numeroRegistroEpoMoneda			= null;
			for(int indice = 0; listaEpos != null && indice<listaOrdenadaEpos.size();indice++){
				
				// Obtener registro Epo
				claveEpo			               = (String) listaOrdenadaEpos.get(indice);
				JSONObject 		registroEpo 	= (JSONObject) listaEpos.get(claveEpo);
				listaRegistros	= (JSONArray)  registroEpo.get("listaRegistros");
				
				StringBuffer 	buffer 			= new StringBuffer();
				String 			epoInputName 	= "nombreEpo"+ registroEpo.get("IC_EPO");
				
				buffer.append(
						"{ "  +
						"				 "  +
						// Nombre de La EPO 
						"            xtype:			'fieldset', "  +
						"            name:			'"+epoInputName+"', "  + // nombreEpo + ic_epo
						"				 id: 	 			'"+epoInputName+"', "  + // nombreEpo + ic_epo
						"            title: 			" +JSONUtils.valueToString((Object)registroEpo.get("NOMBRE_EPO"))+", "  +
						"            claveEpo: 		'"+ registroEpo.get("IC_EPO")+"', "  +
						"            collapsible: 	true, "  +
						"            autoHeight:	true, "  +
						"            defaults: 		{ "  +
						"					width: 			'100%',  "  +
						"					msgTarget:		'side' "  +
						"				 }, "  +
						"            defaultType: 	'textfield', "  +
						"            items: [ "  
				);
				
				// Obtener registro Epo Moneda
				for(j=0;j<listaRegistros.size();j++){
					
					registroEpoMoneda = (JSONObject) listaRegistros.get(j);
	 
					numeroRegistroEpoMoneda = (String) registroEpoMoneda.get("indice");
					
					buffer.append(
						(j>0?",":"") +
						// R.1. List Header: Nombre de la Moneda 
						"					{ "  +
						"									xtype: 	'box', "  +
						"									name:		'nombreMoneda"+numeroRegistroEpoMoneda+"', "  +
						"									id: 	 	'nombreMoneda"+numeroRegistroEpoMoneda+"', "  +
						"									anchor:	'0', "  +
						"									autoEl: 	{ "  +
						"											tag: 	'div', "  +
						"											html: '<div class=\"x-list-header\" style=\"text-align: center;padding:5px\">' + "+JSONUtils.valueToString((Object)registroEpoMoneda.get("NOMBRE_MONEDA"))+" + '</div>' "  +
						"									}, "  +
						"									style: { "  +
						"										marginBottom: 	'3px' "  +
						"									} "  +
						"					}, "  +
						// R.2. Combo Seleccion Intermediario Financiero
						"					{ "  +
						"						xtype: 			'combo', "  +
						"						name: 			'intermediarioFinanciero"+numeroRegistroEpoMoneda+"', "  +
						//"					id: 				'intermediarioFinanciero"+numeroRegistroEpoMoneda+"', "  +
						"						ref: 				'intermediarioFinanciero"+numeroRegistroEpoMoneda+"', "  +
						"						fieldLabel: 	'Intermediario Financiero', "  +
						"						mode: 			'local', "  +
						"						displayField: 	'descripcion', "  +
						"						valueField: 	'clave', "  +
						"						hiddenName: 	'intermediarioFinanciero"+numeroRegistroEpoMoneda+"', "  +
						//"					emptyText: 		'Seleccione...', "  +
						"						forceSelection: true, "  +
						"						triggerAction: 'all', "  +
						"						typeAhead: 		true, "  +
						"						minChars: 		1, "  +
						"						allowBlank:		false, "  +
						"						value:			'"+registroEpoMoneda.get("DEFAULT_VALUE_COMBO_IF")+"', "  + 
						"						anchor:			'-20' "  + 
						"					}, "  +
						// R.6. Combo de Modalidad
						"					{ "  +
						"						xtype: 				'combo', "  +
						"						name: 				'modalidad"+numeroRegistroEpoMoneda+"', "  +
						//"					id: 					'modalidad"+numeroRegistroEpoMoneda+"', "  +
						"						ref: 					'modalidad"+numeroRegistroEpoMoneda+"', "  +
						"						fieldLabel: 		'Modalidad', "  +
						"						mode: 				'local', "  +
						"						displayField: 		'descripcion', "  +
						"						valueField: 		'clave', "  +
						"						hiddenName: 		'modalidad"+numeroRegistroEpoMoneda+"', "  +
						//"						emptyText: 			'Seleccione...', "  +
						"						forceSelection: 	true, "  +
						"						triggerAction: 	'all', "  +
						"						typeAhead: 			true, "  +
						"						allowBlank:			false, "  +
						"						minChars: 			1, "  + 
						"						value:			   '"+registroEpoMoneda.get("DEFAULT_VALUE_MODALIDAD")+"', "  + 
						"						anchor:				'66%' "  + //, 
						//"						tpl: 					NE.util.templateMensajeCargaCombo, "  +
						//"						store:				eval('catalogoModalidadData') "  +
						"					}, "  +
						// R.7 Etiqueta Advertencia Operacion
						"					{ "  +
						"						xtype: 	'label', "  +
						"						name:		'labelAdvertenciaOperacion"+numeroRegistroEpoMoneda+"', "  +
						"						id:		'labelAdvertenciaOperacion"+numeroRegistroEpoMoneda+"', "  +
						"						text: 	"+JSONUtils.valueToString((Object)registroEpoMoneda.get("mensajeTA"))+ ", " + 
						"						cls:		'x-form-item', "  +
						"						style: { "  +
						"							width: 			'100%', "  +
						"							marginBottom: 	'10px', "  +
						"							textAlign:		'justify' "  +
						"						} "  +
						"					}, "  +
						// hidHuboCambio
						"					{ "  +
						"						xtype: 'hidden', "  +
						"						name:	 'hidHuboCambio"+numeroRegistroEpoMoneda+"', "  +
						"						id:	 'hidHuboCambio"+numeroRegistroEpoMoneda+"', "  +	
						"						value: 'N' "  +
						"					}, "  +
						// hidHuboCambioDia
						"					{ "  +
						"						xtype: 'hidden', "  +
						"						name:	 'hidHuboCambioDia"+numeroRegistroEpoMoneda+"', "  +
						"						id:	 'hidHuboCambioDia"+numeroRegistroEpoMoneda+"', "  +	
						"						value: 'N' "  +
						"					}, "  +
						// hidIcEpo
						"					{ "  +
						"						xtype: 'hidden', "  +
						"						name:	 'hidIcEpo"+numeroRegistroEpoMoneda+"', "  +
						"						id:	 'hidIcEpo"+numeroRegistroEpoMoneda+"', "  +	
						"						value: '"+registroEpo.get("IC_EPO")+"' "  +
						"					}, "  +
						// hidIcMoneda
						"					{ "  +
						"						xtype: 'hidden', "  +
						"						name:	 'hidIcMoneda"+numeroRegistroEpoMoneda+"', "  +
						"						id:	 'hidIcMoneda"+numeroRegistroEpoMoneda+"', "  +	
						"						value: '"+registroEpoMoneda.get("IC_MONEDA")+"' "  +
						"					} "  
					);
	 
				}
				
				buffer.append(
					"					 "  +
					"            ] "  +
					"				 "  +
					"	} "
				);
			
				eposParametrizables.add(buffer.toString());
				
			}
			respuesta.put("eposParametrizables",eposParametrizables);
		}
		respuesta.put("componentesPrincipales",componentesPrincipales);
		
	}catch(Exception e){
		
		if(!pantallaDeshabilitada){
         success				= false;
         e.printStackTrace();
			throw e;
		}
		
	}finally{
		
		if(rs  != null){ try{ rs.close(); }catch(Exception e){} }
		
		if(con != null && con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
		
		respuesta.put("success",new Boolean(success));
		
	}
	
	infoRegresar = respuesta.toString();
	
}
 
%>


<%=infoRegresar%>
