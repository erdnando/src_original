var redirecciona = "";
Ext.onReady(function(){
		Ext.QuickTips.init(); 
//-------------------------------VARIABLES--------------------------------------
	var TerminosCondiciones = false;
	var jsonValoresIniciales = null;
	var modoAcceso = "";

//-------------------------------FUNCIONES--------------------------------------
	function procesaValoresIniciales(opts, success, response) {
		var lblEmail = Ext.getCmp('lblEmail');
		var lblCelular = Ext.getCmp('lblCelular');
		var radioAutorizacion3 = Ext.getCmp('radioAutorizacion3');
		var btnAceptar = Ext.getCmp('btnAceptar');
		var contenedorPrincipal = Ext.getCmp('contenedorPrincipal');
		fp.el.mask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
				jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
				modoAcceso = Ext.util.JSON.decode(response.responseText).modoAcceso;
				if(jsonValoresIniciales != null){
					if(modoAcceso!="S"){
						btnAceptar.setText('Continuar');
						contenedorPrincipal.setWidth(710);
					}else{
						radioAutorizacion3.hide();
						contenedorPrincipal.setWidth(949);
					}
					lblEmail.getEl().update("Correo Registrado: " + jsonValoresIniciales.correoRegistrado);
					lblCelular.getEl().update("Celular Registrado: " + jsonValoresIniciales.numeroCelularRegistrado);
					fp.el.unmask();
				}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}
//-------------------------------HANDLERS---------------------------------------
	var procesarActivarServicio =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('Mensaje informativo',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){   
				location.href="/nafin/15cadenas/15mantenimiento/15parametrizacion/15pyme/15EnvioPromocionesActivaServicioext.jsp";
			});
		}  else {			
			NE.util.mostrarConnError(response,opts);
			
		}
		fp.el.unmask();
	}
	function mostrarArchivoPDF(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function DescargaTermios() {
	alert("hola ");
		/*Ext.Ajax.request({
			url: "15EnvioPromocionesext.data.jsp",
			params: Ext.apply({
				informacion: "DESCARGATerminos_y_Condiciones"
			}),
			callback: mostrarArchivoPDF
		});*/
	}
	var procesarAceptarActualizarDatos =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if(modoAcceso!="S"){
				Ext.Ajax.request({
					url:'15EnvioPromocionesext.data.jsp',
					params: {
						informacion: 'Continuar'
					},
					callback: procesaContinuar
				});
			}else{
				Ext.Msg.alert('Mensaje informativo',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){
					fp.el.unmask();
				});
			}
		} else {
			Ext.Msg.alert('Mensaje de Error',"�Error al actualizar los datos!",function(btn){  
					window.location = '15EnvioPromocionesext.jsp';	 
			});
		}
	}
	function procesaContinuar(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			fp.el.unmask();
			redirecciona = Ext.util.JSON.decode(response.responseText).urlPagina;
			Ext.Msg.show({	msg: 'Su informaci�n fue actualizada con �xito',buttons: Ext.Msg.OK,	fn: processResultIns,animEl: 'elId',icon: Ext.MessageBox.INFO});
		} else {
			NE.util.mostrarConnError(response, opts);
		}
	}
	function processResultIns(){
		window.location = redirecciona;
	}
//------------------------------COMPONENTES-------------------------------------
	var elementosForma = [
									{
										xtype: 'container',
										anchor: '100%',
										layout: {
													type: 'table',
													columns: 2
										},
										items: [
														{
															xtype: 'box',
															margins: '5 5 5 5',
															region: 'west',
															width: 700,
															height: 300,
															autoEl: {
																			tag: 'img', 
																			src: '/nafin/00utils/gif/banner_factoraje_movil_1.jpg', 
																			width: 700, 
																			height: 300
																		},
															colspan: 2
														},
														{
															html:'<div style="height:30px"></div>',
															colspan: 2
														},
														{
															xtype: 'radio',
															boxLabel: '<font size="2" color="#006699" face="Arial">Si desea recibir el servicio</font>',
															checked: true,
															name: 'recibeFactoraje',
															id: 'radioAutorizacion1',
															height:40,
															width: 650,
															inputValue: 'S',
															colspan:2,
															handler: function(ctl, val) { 
																var txtEmailNuevo = Ext.getCmp('txtEmailNuevo');
																var txtEmailConfirm = Ext.getCmp('txtEmailConfirmacion');
																var txtCelularNuevo = Ext.getCmp('txtCelularNuevo');
																var txtCelularNuevoConfirmacion = Ext.getCmp('txtCelularNuevoConfirmacion');
																var radioAutorizacion2 = Ext.getCmp('radioAutorizacion2');
																var radioAutorizacion3 = Ext.getCmp('radioAutorizacion3');
																if(radioAutorizacion2.getValue()==true||radioAutorizacion3.getValue()==true){
																	txtEmailNuevo.disable();
																	txtEmailConfirm.disable();
																	txtCelularNuevo.disable();
																	txtCelularNuevoConfirmacion.disable();
																}
															} 
														},
														{
															xtype: 'label',
															id: 'lblEnunciado1',
															style: 'font-family:Arial;font-size:small;text-indent:1em;text-align:left;color:#006699;display:block;height:30px;',
															text: 'Ingresa los siguientes datos:',
															width: 650,
															colspan:2
														},
														{
															xtype: 'label',
															id: 'lblEmail',
															style: 'font-family:Arial;font-size:small;font-weight:bold;color:#006699;text-align:left;text-indent:2em;display:block;height:30px;',
															text: 'Correo Registrado: ',
															width: 650,
															colspan: 2
														},
														{
															xtype: 'label',
															id: 'lblEnunciado2',
															style: 'font-family:Arial;font-size:small;text-indent:1em;text-align:left;color:#006699;display:block;height:30px;',
															text: 'Si el correo no es correcto favor de actualizarlo:',
															width: 650,
															colspan: 2
														},
														{
															xtype: 'label',
															id: 'lblCorreoNuevo',
															style: 'font-family:Arial;font-size:small;text-align:right;color:#006699;display:block;',
															text: 'Correo Nuevo:',
															width: 420,
															height:20
														},
														{
															xtype: 'textfield',
															name: 'nuevoCorreo',
															id: 'txtEmailNuevo',
															allowBlank: true,
															vtype: 'email',
															msgTarget: 'side',
															style: 'font-family:Arial;font-size:small;text-align:left;color:#006699;display:block;',
															width: 230
														},
														{
															html:'<div style="height:5px"></div>',
															colspan: 2
														},
														{
															xtype: 'label',
															id: 'lblCorreoNuevoConfirmacion',
															style: 'font-family:Arial;font-size:small;text-align:right;color:#006699;display:block;',
															text: 'Confirmaci�n de Correo:',
															width: 420,
															height:20
														},
														{
															xtype: 'textfield',
															name: 'confirmacionCorreo',
															id: 'txtEmailConfirmacion',
															allowBlank: true,
															vtype: 'email',
															msgTarget: 'side',
															width: 230,
															style: 'font-family:Arial;font-size:small;text-align:left;color:#006699;display:block;'
														},
														{
															xtype: 'label',
															id: 'lblCelular',
															style: 'font-family:Arial;font-size:small;font-weight:bold;color:#006699;text-indent:2em;text-align:left;display:block;height:30px;',
															text: 'Celular Registrado: ',
															width: 650,
															colspan:2
														},
														{
															xtype: 'label',
															id: 'lblEnunciado3',
															style: 'font-family:Arial;font-size:small;text-indent:1em;text-align:left;color:#006699;display:block;height:30px;',
															text: 'Si el n�mero celular no es correcto favor de actualizarlo:',
															colspan: 2,
															width: 650
														},
														{
															xtype: 'label',
															id: 'lblCelularNuevo',
															style: 'font-family:Arial;font-size:small;text-align:right;color:#006699;display:block;',
															text: 'Tel�fono Celular (10 d�gitos): 	044',
															width: 420,
															height:20
														},
														{
															xtype: 'numberfield',
															name: 'nuevoCelular',
															id: 'txtCelularNuevo',
															allowBlank: true,
															allowDecimals: false,
															allowNegative: false,
															decimalPrecision: 0,
															maxLength: 10,
															minValue: 10,
															msgTarget: 'side',
															width: 230,
															style: 'font-family:Arial;font-size:small;text-align:left;color:#006699;display:block;'
														},
														{
															html:'<div style="height:5px"></div>',
															colspan: 2
														},
														{
															xtype: 'label',
															id: 'lblCelularNuevoConfirmacion',
															style: 'font-family:Arial;font-size:small;text-align:right;color:#006699;display:block;',
															text: 'Confirmaci�n (10 d�gitos): 	044 ',
															width: 420,
															height:20
														},
														{
															xtype: 'numberfield',
															name: 'confirmacionCelular',
															id: 'txtCelularNuevoConfirmacion',
															allowBlank: true,
															allowDecimals: false,
															allowNegative: false,
															decimalPrecision: 0,
															maxLength: 10,
															minValue: 10,
															msgTarget: 'side',
															width: 230,
															style: 'font-family:Arial;font-size:small;text-align:left;color:#006699;display:block;'
														},
														{
															xtype: 'radio',
															boxLabel: '<font size="2" color="#006699" face="Arial">No le interesa el servicio</font>',
															name: 'recibeFactoraje',
															id: 'radioAutorizacion2',
															height: 50,
															width: 650,
															inputValue: 'N',
															colspan:2,
																handler: function(ctl, val) { 
																	var txtEmailNuevo = Ext.getCmp('txtEmailNuevo');
																	var txtEmailConfirm = Ext.getCmp('txtEmailConfirmacion');
																	var txtCelularNuevo = Ext.getCmp('txtCelularNuevo');
																	var txtCelularNuevoConfirmacion = Ext.getCmp('txtCelularNuevoConfirmacion');
																	var radioAutorizacion1 = Ext.getCmp('radioAutorizacion1');
																	var radioAutorizacion3 = Ext.getCmp('radioAutorizacion3');
																	if(radioAutorizacion1.getValue()==true){
																		txtEmailNuevo.enable();
																		txtEmailConfirm.enable();
																		txtCelularNuevo.enable();
																		txtCelularNuevoConfirmacion.enable();
																	}else if(radioAutorizacion3.getValue()==true){
																		txtEmailNuevo.disable();
																		txtEmailConfirm.disable();
																		txtCelularNuevo.disable();
																		txtCelularNuevoConfirmacion.disable();
																	}
															}
														},
														{
															xtype: 'radio',
															boxLabel: '<font size="2" color="#006699" face="Arial">Record�rmelo m�s tarde.</font>',
															name: 'recibeFactoraje',
															id: 'radioAutorizacion3',
															height: 50,
															width: 650,
															inputValue: 'N',
															colspan:2,
																handler: function(ctl, val) { 
																	var txtEmailNuevo = Ext.getCmp('txtEmailNuevo');
																	var txtEmailConfirm = Ext.getCmp('txtEmailConfirmacion');
																	var txtCelularNuevo = Ext.getCmp('txtCelularNuevo');
																	var txtCelularNuevoConfirmacion = Ext.getCmp('txtCelularNuevoConfirmacion');
																	var radioAutorizacion1 = Ext.getCmp('radioAutorizacion1');
																	var radioAutorizacion2 = Ext.getCmp('radioAutorizacion2');
																	if(radioAutorizacion1.getValue()==true){
																		txtEmailNuevo.enable();
																		txtEmailConfirm.enable();
																		txtCelularNuevo.enable();
																		txtCelularNuevoConfirmacion.enable();
																	}else if(radioAutorizacion2.getValue()==true){
																		txtEmailNuevo.disable();
																		txtEmailConfirm.disable();
																		txtCelularNuevo.disable();
																		txtCelularNuevoConfirmacion.disable();
																	}
															}
														},
														{
															xtype: 'button',
															height: 20,
															width: 650,
															colspan: 2,
															align:'center',
															text:'<p align="center"><font color="blue" face="Arial" size="2" weight:"bold" align:"center"><b>T�rminos y Condiciones</font></b><br><br></p>',
															handler: function (boton,evento) {
																TerminosCondiciones=true;
																Ext.Ajax.request({
																	url: '15EnvioPromocionesext.data.jsp',
																	params: Ext.apply({
																		informacion: 'DESCARGATerminos_y_Condiciones'
																	}),
																	callback: mostrarArchivoPDF
																});
															}
														}
										]
									}
	];
	var fp = new Ext.FormPanel({
		id: 'forma',
		style: 'margin:0px auto;',
		title: '',
		hidden: false,
		frame: true,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		labelWidth: 1,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		width: 700,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Activar Servicio',
				id: 'btnActivarServicio',
				formBind: true,
				handler: function(boton, evento) {
					var txtEmailNuevo = Ext.getCmp('txtEmailNuevo');
					var txtEmailConfirm = Ext.getCmp('txtEmailConfirmacion');
					var txtCelularNuevo = Ext.getCmp('txtCelularNuevo');
					var txtCelularNuevoConfirmacion = Ext.getCmp('txtCelularNuevoConfirmacion');
					var radioAutorizacion1 = Ext.getCmp('radioAutorizacion1');
					
					if(radioAutorizacion1.getValue()==true){
						if(TerminosCondiciones){
								//Valida si el nuevo correo es una direcci�n valida
								if(!Ext.form.VTypes.email(txtEmailNuevo.getValue())&&!Ext.isEmpty(txtEmailNuevo.getValue())){
									txtEmailNuevo.markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato "usuario@dominio.com"');
									txtEmailNuevo.focus();
									return;
								}
								//Valida si la confirmaci�n de correo es una direcci�n valida
								if(!Ext.form.VTypes.email(txtEmailConfirm.getValue())&&!Ext.isEmpty(txtEmailConfirm.getValue())){
									txtEmailConfirm.markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato "usuario@dominio.com"');
									txtEmailConfirm.focus();
									return;
								}
								//Valida si se introdujo una confirmaci�n de correo al introducir un correo nuevo
								if(!Ext.isEmpty(txtEmailNuevo.getValue())&&Ext.isEmpty(txtEmailConfirm.getValue())){
									txtEmailConfirm.markInvalid('Debe agregar tanto el Correo Nuevo como la Confirmaci�n de Correo o bien dejar ambos campos en blanco si no desea actualizar el correo');
									txtEmailConfirm.focus();
									return;
								}
								//Valida si se introdujo un correo nuevo al introducir la confirmaci�n de correo
								if(Ext.isEmpty(txtEmailNuevo.getValue())&&!Ext.isEmpty(txtEmailConfirm.getValue())){
									txtEmailNuevo.markInvalid('Debe agregar tanto el Correo Nuevo como la Confirmaci�n de Correo o bien dejar ambos campos en blanco si no desea actualizar el correo');
									txtEmailNuevo.focus();
									return;
								}
								//Valida si el correo nuevo y la confirmaci�n de correo son iguales
								if(!Ext.isEmpty(txtEmailNuevo.getValue())&&!Ext.isEmpty(txtEmailConfirm.getValue())&&(txtEmailNuevo.getValue()!=txtEmailConfirm.getValue())){
									txtEmailConfirm.markInvalid('El campo Correo Nuevo y Confirmaci�n de Correo deben ser iguales');
									txtEmailConfirm.focus();	
									return;
								}
								//Valida si se introdujo una confirmaci�n de n�mero celular al introducir celular nuevo
								if(!Ext.isEmpty(txtCelularNuevo.getValue())&&Ext.isEmpty(txtCelularNuevoConfirmacion.getValue())){
									txtCelularNuevoConfirmacion.markInvalid('Debe agregar tanto el Tel�fono Celular Nuevo como la Confirmaci�n o bien dejar ambos campos en blanco si no desea actualizar el n�mero de celular');
									txtCelularNuevoConfirmacion.focus();
									return;									
								}
								//Valida si se introdujo un n�mero de celular nuevo cuando se introdujo una confirmaci�n de celular
								if(Ext.isEmpty(txtCelularNuevo.getValue())&&!Ext.isEmpty(txtCelularNuevoConfirmacion.getValue())){
									txtCelularNuevo.markInvalid('Debe agregar tanto el Tel�fono Celular Nuevo como la Confirmaci�n o bien dejar ambos campos en blanco si no desea actualizar el n�mero de celular');
									txtCelularNuevo.focus();
									return;									
								}
								//Valida si el nuevo celular es v�lido
								if(!Ext.isEmpty(txtCelularNuevo.getValue())&&(txtCelularNuevo.getValue()>9999999999||txtCelularNuevo.getValue()<1000000000)){
										txtCelularNuevo.markInvalid('Debe introducir 10 d�gitos');
										txtCelularNuevo.focus();
										return;
								}
								//V�lida si el n�mero de confirmaci�n es v�lido
								if(!Ext.isEmpty(txtCelularNuevoConfirmacion.getValue())&&(txtCelularNuevoConfirmacion.getValue()>9999999999||txtCelularNuevoConfirmacion.getValue()<1000000000)){
										txtCelularNuevoConfirmacion.markInvalid('Debe introducir 10 d�gitos');
										txtCelularNuevoConfirmacion.focus();
										return;
								}
								//V�lida si el nuevo celular y el n�mero de confirmaci�n son iguales
								if(!Ext.isEmpty(txtCelularNuevo.getValue())&&!Ext.isEmpty(txtCelularNuevoConfirmacion.getValue())&&(txtCelularNuevo.getValue()!=txtCelularNuevoConfirmacion.getValue())){
											txtCelularNuevoConfirmacion.markInvalid('El campo Tel�fono Celular y Confirmaci�n deben ser iguales');
											txtCelularNuevoConfirmacion.focus();	
											return;
								}
								fp.el.mask('Guardando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '15EnvioPromocionesext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ActivarServicio'
									}),
									callback: procesarActivarServicio
								});
								
					}else{
						Ext.Msg.alert('Mensaje Informativo',"Debe leer los t�rminos y condiciones para poder continuar.",function(btn){  
							return;
						});
					}
				}else{
						Ext.Msg.alert('Mensaje Informativo',"Debe aceptar el servicio para poder activarlo.",function(btn){  
							return;
						});
					}
				}
			},
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				handler: function() {
					var txtEmailNuevo = Ext.getCmp('txtEmailNuevo');
					var txtEmailConfirm = Ext.getCmp('txtEmailConfirmacion');
					var txtCelularNuevo = Ext.getCmp('txtCelularNuevo');
					var txtCelularNuevoConfirmacion = Ext.getCmp('txtCelularNuevoConfirmacion');
					var radioAutorizacion1 = Ext.getCmp('radioAutorizacion1');
					var radioAutorizacion2 = Ext.getCmp('radioAutorizacion2');
					var radioAutorizacion3 = Ext.getCmp('radioAutorizacion3');
						//Valida que si seleciona que s� se desea recibir el servicio, entonces se lean los t�rminos y condiciones.
						if(radioAutorizacion1.getValue()==true&&TerminosCondiciones==false){
								Ext.Msg.alert('Mensaje Informativo',"Debe leer los t�rminos y condiciones para poder continuar.",function(btn){});
								return;
						}
						if(radioAutorizacion1.getValue()==true||radioAutorizacion2.getValue()==true){
							//Valida si el nuevo correo es una direcci�n valida
							if(!Ext.form.VTypes.email(txtEmailNuevo.getValue())&&!Ext.isEmpty(txtEmailNuevo.getValue())){
									txtEmailNuevo.markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato "usuario@dominio.com"');
									txtEmailNuevo.focus();
									return;
							}
							//Valida si la confirmaci�n de correo es una direcci�n valida
							if(!Ext.form.VTypes.email(txtEmailConfirm.getValue())&&!Ext.isEmpty(txtEmailConfirm.getValue())){
									txtEmailConfirm.markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato "usuario@dominio.com"');
									txtEmailConfirm.focus();
									return;
							}
							//Valida si se introdujo una confirmaci�n de correo al introducir un correo nuevo
							if(!Ext.isEmpty(txtEmailNuevo.getValue())&&Ext.isEmpty(txtEmailConfirm.getValue())){
									txtEmailConfirm.markInvalid('Debe agregar tanto el Correo Nuevo como la Confirmaci�n de Correo o bien dejar ambos campos en blanco si no desea actualizar el correo');
									txtEmailConfirm.focus();
									return;
							}
							//Valida si se introdujo un correo nuevo al introducir la confirmaci�n de correo
							if(Ext.isEmpty(txtEmailNuevo.getValue())&&!Ext.isEmpty(txtEmailConfirm.getValue())){
									txtEmailNuevo.markInvalid('Debe agregar tanto el Correo Nuevo como la Confirmaci�n de Correo o bien dejar ambos campos en blanco si no desea actualizar el correo');
									txtEmailNuevo.focus();
									return;
							}
							//Valida si el correo nuevo y la confirmaci�n de correo son iguales
							if(!Ext.isEmpty(txtEmailNuevo.getValue())&&!Ext.isEmpty(txtEmailConfirm.getValue())&&(txtEmailNuevo.getValue()!=txtEmailConfirm.getValue())){
									txtEmailConfirm.markInvalid('El campo Correo Nuevo y Confirmaci�n de Correo deben ser iguales');
									txtEmailConfirm.focus();	
									return;
							}
							//Valida si se introdujo una confirmaci�n de n�mero celular al introducir celular nuevo
							if(!Ext.isEmpty(txtCelularNuevo.getValue())&&Ext.isEmpty(txtCelularNuevoConfirmacion.getValue())){
									txtCelularNuevoConfirmacion.markInvalid('Debe agregar tanto el Tel�fono Celular Nuevo como la Confirmaci�n o bien dejar ambos campos en blanco si no desea actualizar el n�mero de celular');
									txtCelularNuevoConfirmacion.focus();
									return;									
							}
							//Valida si se introdujo un n�mero de celular nuevo cuando se introdujo una confirmaci�n de celular
							if(Ext.isEmpty(txtCelularNuevo.getValue())&&!Ext.isEmpty(txtCelularNuevoConfirmacion.getValue())){
									txtCelularNuevo.markInvalid('Debe agregar tanto el Tel�fono Celular Nuevo como la Confirmaci�n o bien dejar ambos campos en blanco si no desea actualizar el n�mero de celular');
									txtCelularNuevo.focus();
									return;									
							}
							//Valida si el nuevo celular es v�lido
							if(!Ext.isEmpty(txtCelularNuevo.getValue())&&(txtCelularNuevo.getValue()>9999999999||txtCelularNuevo.getValue()<1000000000)){
										txtCelularNuevo.markInvalid('Debe introducir 10 d�gitos');
										txtCelularNuevo.focus();
										return;
							}
							//V�lida si el n�mero de confirmaci�n es v�lido
							if(!Ext.isEmpty(txtCelularNuevoConfirmacion.getValue())&&(txtCelularNuevoConfirmacion.getValue()>9999999999||txtCelularNuevoConfirmacion.getValue()<1000000000)){
										txtCelularNuevoConfirmacion.markInvalid('Debe introducir 10 d�gitos');
										txtCelularNuevoConfirmacion.focus();
										return;
							}
							//V�lida si el nuevo celular y el n�mero de confirmaci�n son iguales
							if(!Ext.isEmpty(txtCelularNuevo.getValue())&&!Ext.isEmpty(txtCelularNuevoConfirmacion.getValue())&&(txtCelularNuevo.getValue()!=txtCelularNuevoConfirmacion.getValue())){
										txtCelularNuevoConfirmacion.markInvalid('El campo Tel�fono Celular y Confirmaci�n deben ser iguales');
										txtCelularNuevoConfirmacion.focus();	
										return;
							}
						fp.el.mask('Guardando...', 'x-mask-loading');
						Ext.Ajax.request({
								url: '15EnvioPromocionesext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Aceptar'
								}),
								callback: procesarAceptarActualizarDatos
						});
						}
						if(radioAutorizacion3.getValue()==true){
							Ext.Ajax.request({
								url:'15EnvioPromocionesext.data.jsp',
								params: {
								informacion: 'Continuar'
								},
								callback: procesaContinuar
							});
						}
				}
			}
		]
	}); 
//--------------------------------PRINCIPAL-------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp
		]
	}); 

//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '15EnvioPromocionesext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});
});