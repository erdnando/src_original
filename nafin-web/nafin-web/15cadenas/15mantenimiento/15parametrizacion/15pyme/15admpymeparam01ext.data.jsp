<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String cboEpo	=	request.getParameter("ic_epo")!=null?request.getParameter("ic_epo"):"";
String cboMoneda	=	request.getParameter("comcat_moneda")!=null?request.getParameter("comcat_moneda"):"";
String infoRegresar = "";

if(informacion.equals("CatalogoEPOPyme")) {
	CatalogoEPOPyme cat = new CatalogoEPOPyme();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClavePyme(iNoCliente);
	infoRegresar = cat.getJSONElementos();
	}
else if(informacion.equals("CatalogoMoneda")) {
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre"); 
	///cat.setValoresCondicionIn("1,54", Integer.class);
   infoRegresar = cat.getJSONElementos();
	}
else if (informacion.equals("Consulta") ||
			informacion.equals("ArchivoPDF")){
			
			com.netro.cadenas.ConsCtasBancarias paginador	=	new com.netro.cadenas.ConsCtasBancarias();
			paginador.setClaveEPO(cboEpo);
			paginador.setPyme(iNoCliente);
			paginador.setMoneda(cboMoneda);
			CQueryHelperRegExtJS queryHelper	=	new CQueryHelperRegExtJS( paginador ); 
			
	if(informacion.equals("Consulta")){
		try {
			Registros reg	=	queryHelper.doSearch();
			infoRegresar	=
				"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		}
	else if (informacion.equals("ArchivoPDF")) {
		try {
			String nombreArchivo	=	queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}catch(Throwable e){
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}
}	//Thread.sleep(10*1000);
%>
<%=infoRegresar%>