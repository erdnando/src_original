Ext.onReady(function (){
	Ext.QuickTips.init(); 
//---------------------------------FUNCIONES------------------------------------	
	var jsonValoresIniciales = null;
	var modoAcceso = "";
	var cancelar = "";
	function procesaValoresIniciales(opts, success, response) {
		var momentoEnvio2 = Ext.getCmp("momentoEnvio2");
		var diasPrevios = "";
		var btnCancelar = Ext.getCmp('btnCancelar');
		modoAcceso = Ext.util.JSON.decode(response.responseText).modoAcceso;
		fp.el.mask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
				jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
				diasPrevios = jsonValoresIniciales.diasPrevios;
				if(modoAcceso!="S"){
					contenedorPrincipal.setWidth(710);
				}else{
					btnCancelar.hide();
				}
				if(jsonValoresIniciales != null){	
					momentoEnvio2.wrap.child('.x-form-cb-label').update('<font size="2" color="#006699" face="Arial">' + diasPrevios + ' d�as h�biles previos a su vencimiento.</font>');
					fp.el.unmask();
				}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}
//-------------------------------HANDLERS---------------------------------------
	var procesarConfirmarServicio =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			if(modoAcceso!="S"){
				Ext.Ajax.request({
					url:'15EnvioPromocionesActivaServicioext.data.jsp',
					params: {
						informacion: 'Continuar'
					},
					callback: procesaContinuar
				});
			}else{
				Ext.Msg.alert('Mensaje informativo',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){  
					fp.el.unmask();	
					window.location = '15EnvioPromocionesActivaServicioext.jsp';
				});
			}
		} else {
			Ext.Msg.alert('Mensaje de Error',"�Error al activar el servicio!",function(btn){  
					window.location = '15EnvioPromocionesActivaServicioext.jsp';	 
			});
		}
	}
	function procesaContinuar(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			fp.el.unmask();
			redirecciona = Ext.util.JSON.decode(response.responseText).urlPagina;
			if(cancelar!="S")
				{Ext.Msg.show({	msg: 'Su informaci�n fue actualizada con �xito',buttons: Ext.Msg.OK,	fn: processResultIns,animEl: 'elId',icon: Ext.MessageBox.INFO});}
			else{
				fn: processResultIns;
			}
		} else {
			NE.util.mostrarConnError(response, opts);
		}
	}
	function processResultIns(){
		window.location = redirecciona;
	}
//---------------------------------STORES------------------------------------
	var catalogoCompCelularData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave','descripcion','loadMsg'],
		url: '15EnvioPromocionesActivaServicioext.data.jsp',
		baseParams: {
			informacion: 'catalogoCompCelular'
			},
			totalProperty: 'total',
			autoload: false,
			listeners:  {
				exception: NE.util.mostrarDataProxyError,
				beforeload:	NE.util.initMensajeCargaCombo
			            }
	});
//------------------------------COMPONENTES-------------------------------------
	var elementosForma = [
									{
										xtype: 'container',
										anchor: '100%',
										layout: {
													type: 'table',
													columns: 2
										},
										items: [
														{
															xtype: 'box',
															margins: '5 5 5 5',
															region: 'west',
															width: 700,
															height: 300,
															autoEl: {
																			tag: 'img', 
																			src: '/nafin/00utils/gif/banner_factoraje_movil_1.jpg', 
																			width: 700, 
																			height: 300
																		},
															colspan: 2
														},
														{
															html:'<div style="height:30px"></div>',
															colspan: 2
														},
														{
															xtype: 'label',
															id: 'lblEnunciado1',
															style: 'font-family:Arial;font-size:small;text-align:left;color:#006699;display:block;',
															text: 'Indique el momento en el que desea recibir los mensajes:',
															width: 350,
															height:30,
															colspan:1
														},
														{
															xtype: 'label',
															id: 'lblEnunciado2',
															style: 'font-family:Arial;font-size:small;text-align:left;color:#006699;display:block;',
															text: 'Indique el tipo de informaci�n que desea recibir:',
															width: 350,
															height:30,
															colspan:1
														},
														{
															xtype: 'checkbox',
															boxLabel:'<font size="2" color="#006699" face="Arial">Cuando me hayan publicado documentos.</font>',
															name:'momentoEnvioA',
															id:'momentoEnvio1',
															inputValue:'P',
															width: 350,
															height:20,
															msgTarget: 'side',
															anchor : '-25',
															colspan:1
														},
														{
															xtype: 'checkbox',
															boxLabel:'<font size="2" color="#006699" face="Arial">Monto.</font>',
															name:'tipoInformacionA',
															id:'tipoInformacion1',
															inputValue:'M',
															width: 350,
															height:20,			
															msgTarget: 'side',
															colspan:1
														},
														{
															xtype: 'checkbox',
															boxLabel:'<font size="2" color="#006699" face="Arial">d�as h�biles previos a su vencimiento.</font>',
															name:'momentoEnvioB',
															id:'momentoEnvio2',
															inputValue:'V',
															width: 350,
															height:20,
															msgTarget: 'side',
															colspan:1
														},
														{
															xtype: 'checkbox',
															boxLabel:'<font size="2" color="#006699" face="Arial">N�mero de documentos.</font>',
															name:'tipoInformacionB',
															id:'tipoInformacion2',
															inputValue:'N',
															width: 350,
															height:20,
															msgTarget: 'side',
															colspan:1
														},
														{
															html:'<div style="height:30px"></div>',
															colspan: 2
														},
														{
															xtype: 'label',
															id: 'lblEnunciado3',
															style: 'font-family:Arial;font-size:small;text-indent:1em;text-align:right;color:#006699;display:block',
															text: 'Seleccione su actual compa�ia de celular:',
															width: 350,
															height:20,
															colspan:1
														},
														{
															xtype: 'combo',
															name: 'compCelular',
															id: 'cboEpo',
															allowBlank:false,
															fieldLabel : '',
															mode: 'local',
															displayField : 'descripcion',
															valueField: 'clave',
															hiddenName: 'compCelular',
															emptyText: 'Seleccionar...',
															forceSeleccion : true,
															triggerAction: 'all',
															msgTarget: 'side',
															typeAhead: true,
															minChars : 1,
															store : catalogoCompCelularData,
															tpl: NE.util.templateMensajeCargaCombo,
															width: 250,
															colspan:1
														},
														{
															html:'<div style="height:5px"></div>',
															colspan: 2
														},
														{
															xtype: 'label',
															id: 'lblEnunciado4',
															style: 'font-family:Arial;font-size:small;text-indent:1em;text-align:right;color:#006699;display:block',
															text: 'Ingresar C�digo de Confirmaci�n: ',
															width: 350,
															height:20,
															colspan:1
														},
														{
															xtype: 'textfield',
															name: 'claveConfirmacion',
															id: 'txtClaveConfirmacion',
															allowBlank: true,
															maxLength: 6,
															minLength: 6,
															msgTarget: 'side',
															style: 'font-family:Arial;font-size:small;text-align:left;color:#006699;display:block;',
															width: 200
														},
														{
															xtype: 'box',
															html: '</br></br></br>Si no ha recibido su c�digo de verificaci�n:'+
																	'</br>1. Regrese y valide que su n�mero de celular es correcto.'+
																	'</br>2. Drir�jase a la pantalla Administraci�n - Servicios - Env�o de promociones para completar su registro.'+
																	'</br>3. Llame al 01 800 6234672 para mayor referencia.',
															height: 120,
															width: 700,
															style: 'font-family:Arial;font-size:small;color:#006699;text-align:left;display:block;',
															colspan: 2
														}
										]
									}
	];
	var fp = new Ext.FormPanel({
		id: 'forma',
		style: 'margin:0 auto;',
		title: '',
		hidden: false,
		frame: true,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		width: 705,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		items: elementosForma,
		monitorValid: false,
		buttons: [
						{
							text: 'Regresar',
							id: 'btnRegresar',
							handler: function() {
								location.href="/nafin/15cadenas/15mantenimiento/15parametrizacion/15pyme/15EnvioPromocionesext.jsp";
							}
						},
						{
							text: 'Confirmar',
							id: 'btnConfirmar',
							formBind: true,
							handler: function(boton, evento){
								var momentoEnvio1 = Ext.getCmp('momentoEnvio1');
								var momentoEnvio2 = Ext.getCmp('momentoEnvio2');
								var tipoInformacion1 = Ext.getCmp('tipoInformacion1');
								var tipoInformacion2 = Ext.getCmp('tipoInformacion2');
								var cboEpo = Ext.getCmp('cboEpo');
								var txtClaveConfirmacion = Ext.getCmp('txtClaveConfirmacion');
								var claveConfirmacionSesion = jsonValoresIniciales.claveConfirmacionSesion;
								//Valida que al menos un checkbox de Momento de Env�o sea seleccionado
								if(momentoEnvio1.getValue()==""&&momentoEnvio2.getValue()==""){
									momentoEnvio1.markInvalid('Debe indicar el momento en el que desea recibir los mensajes');
									momentoEnvio1.setValue();
									momentoEnvio1.focus();
									return;
								}
								//Valida que al menos un checkbox de Tipo de Informaci�n sea selecionado
								if(tipoInformacion1.getValue()==""&&tipoInformacion2.getValue()==""){
									tipoInformacion1.markInvalid('Debe indicar el tipo de informaci�n que desea recibir los mensajes');
									tipoInformacion1.focus();
									return;
								}
								//Valida que se ha escogido una compa�ia telef�nica
								if(Ext.isEmpty(cboEpo.getValue())){
									cboEpo.markInvalid('Debe seleccionar una Compa��a Telef�nica');
									cboEpo.focus();
									return;
								}
								//Valida que se ha introducido un n�mero de Confirmaci�n
								if(Ext.isEmpty(txtClaveConfirmacion.getValue())){
									txtClaveConfirmacion.markInvalid('Debe introducir un N�mero de Confirmaci�n');
									txtClaveConfirmacion.focus();
									return;
								}
								//Valida si la clave de confirmaci�n de sesi�n es igual a la introducida.
								if(txtClaveConfirmacion.getValue()!=claveConfirmacionSesion){
									txtClaveConfirmacion.markInvalid('La clave de confirmaci�n introducida es incorrecta');
									txtClaveConfirmacion.focus();
									return;
								}
								fp.el.mask('Guardando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '15EnvioPromocionesActivaServicioext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ConfirmarServicio'
									}),
									callback: procesarConfirmarServicio
								});								
							}
						},
						{
							text: 'Cancelar',
							id: 'btnCancelar',
							handler: function() {
								cancelar="S";
								Ext.Ajax.request({
									url:'15EnvioPromocionesActivaServicioext.data.jsp',
									params: {
									informacion: 'Continuar'
									},
									callback: procesaContinuar
								});
							}
						}
		]
		});
//--------------------------------PRINCIPAL-------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20)
		]
	}); 
//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '15EnvioPromocionesActivaServicioext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});
//------------------------------------------------------------------------------
catalogoCompCelularData.load();
});