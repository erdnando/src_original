<%@ page contentType="text/html; charset=UTF-8"
		import="	java.util.*,
				java.text.*,
				netropology.utilerias.*, 
				javax.naming.*,
				java.io.*,
				com.netro.cesion.*,
				org.apache.commons.fileupload.disk.*,
				org.apache.commons.fileupload.servlet.*,
				org.apache.commons.fileupload.*,
				com.netro.cadenas.*,
				net.sf.json.*
				"
	errorPage="/00utils/error_extjs_fileupload.jsp"							
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../../../../15cadenas/015secsession_extjs.jspf" %>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload"/>

<%
String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
String  cadena = (request.getParameter("cadena")!=null)?request.getParameter("cadena"):"";
String  titulo = (request.getParameter("titulo")!=null)?request.getParameter("titulo"):"";
String  fecIni = (request.getParameter("fecIni")!=null)?request.getParameter("fecIni"):"";
String  fecFin = (request.getParameter("fecFin")!=null)?request.getParameter("fecFin"):"";
String  cont = (request.getParameter("cont")!=null)?request.getParameter("cont"):"";
String  multiS = (request.getParameter("multiS")!=null)?request.getParameter("multiS"):"";

String path_destino = strDirectorioTemp+"15archcadenas/";
String mensaje ="", resultado= "N";
int numFiles = 0;
boolean ok_file = true;

JSONObject resultadoProceso = new JSONObject();

String infoRegresar = "";
int tamanio = 0;
String path_destarch = "", path_destimgn = "";
String arch_destino = "", imgn_destino = "";
String ruta_destarch = "", ruta_destimgn = "";

try {	
	myUpload.initialize(pageContext);
	myUpload.setTotalMaxFileSize(2000000);
	myUpload.upload();
	
	path_destino = strDirTrabajo + "15archcadenas/pub_epo" + cadena + "/";
	java.io.File fdPath1 = new java.io.File(path_destino);
	if(!fdPath1.exists()) {
		if (!fdPath1.mkdir()) {
			out.println("No se pudo crear el directorio " + path_destino);
		}
	}
			
	path_destarch = path_destino + "archivos/";
	java.io.File fdPath2 = new java.io.File(path_destarch);
	if(!fdPath2.exists()) {
		if (!fdPath2.mkdir()) {
			out.println("No se pudo crear el directorio " + path_destarch);
		}
	}
			
	path_destimgn = path_destino + "imagenes/";
	java.io.File fdPath3 = new java.io.File(path_destimgn);
	if(!fdPath3.exists()) {
		if (!fdPath3.mkdir()) {
			out.println("No se pudo crear el directorio " + path_destimgn);
		}
	}
			
	arch_destino = (myUpload.getFiles().getFile(0).getFileName() == null) ? "" : myUpload.getFiles().getFile(0).getFileName();
	imgn_destino = (myUpload.getFiles().getFile(1).getFileName() == null) ? "" : myUpload.getFiles().getFile(1).getFileName();
			
	ruta_destarch = path_destarch + arch_destino;
	ruta_destimgn = path_destimgn + imgn_destino;
			
	numFiles = myUpload.save(path_destarch);
	java.io.File f1 = new java.io.File(path_destarch + imgn_destino);
	f1.delete();
	numFiles = myUpload.save(path_destimgn);
	java.io.File f2 = new java.io.File(path_destimgn + arch_destino);
	f2.delete();
	
	Tablaimgtxt timg = new Tablaimgtxt(); //Clase en la que estan los metodos para Guardar y Actualizar
	
	//***Aqui se hace la llamada al metodo de la clase que guarda en la BD***
	if(opcion.equals("G")){
		timg.guardaImgTxt(cadena, titulo, fecIni, fecFin, cont, imgn_destino, arch_destino, multiS);
		resultado= "S";
	}
	//***Aqui se hace la llamada al metodo de la clase que modifica los datos en la BD***
	else if(opcion.equals("A")){
		String  cadMod = (request.getParameter("cadMod")!=null)?request.getParameter("cadMod"):"";
		String  publicacion = (request.getParameter("publicacion")!=null)?request.getParameter("publicacion"):"";
		String  titMod = (request.getParameter("titMod")!=null)?request.getParameter("titMod"):"";
		String  fecIniMod = (request.getParameter("fecIniMod")!=null)?request.getParameter("fecIniMod"):"";
		String  fecFinMod = (request.getParameter("fecFinMod")!=null)?request.getParameter("fecFinMod"):"";
		String  contMod = (request.getParameter("contMod")!=null)?request.getParameter("contMod"):"";
		String  archMod = (request.getParameter("archMod")!=null)?request.getParameter("archMod"):"";
		String  imgMod = (request.getParameter("imgMod")!=null)?request.getParameter("imgMod"):"";
		//Se hace la actualizaci�n de los datos modificados
		timg.actualizaDatosImg(titMod, fecIniMod, fecFinMod, contMod, archMod, imgMod, cadMod, publicacion);
		
		String arch_actual= "15archcadenas/pub_epo"+cadMod+"/archivos/"+archMod;
		String imgn_actual= "15archcadenas/pub_epo"+cadMod+"/imagenes/"+imgMod;
		String ruta_actualarch = strDirectorioTemp + arch_actual;
		java.io.File f3 = new java.io.File(ruta_actualarch);
		String ruta_actualimgn = strDirectorioTemp + imgn_actual;
		java.io.File f4 = new java.io.File(ruta_actualimgn);
		
		if (!ruta_destarch.equals(ruta_actualarch)) {
			if (f3.isFile() && f3.exists()) f3.delete();
		}
		if (!ruta_destimgn.equals(ruta_actualimgn)) {
			if (f4.isFile() && f4.exists()) f4.delete();
		}
	}
	
}catch(Exception e) {
			out.println("Exception: " + e.toString());
			e.printStackTrace();	
}	   
	resultadoProceso.put("success", new Boolean(true));
	resultadoProceso.put("resultado", resultado);
	resultadoProceso.put("mensaje", mensaje);
	
	infoRegresar	 =resultadoProceso.toString();

%>
<%=infoRegresar%>
