<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.servicios.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
//Se crea la instancia del EJB para su utilización.
Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB",Servicios.class);
JSONObject jsonObj = new JSONObject();

if(informacion.equals("valoresIniciales")){
	jsonObj= new JSONObject();
	try{
		String modoAcceso = ((String)session.getAttribute("inicializar.FINALIZADO")!=null)?(String)session.getAttribute("inicializar.FINALIZADO"):"";
		String claveConfirmacionSesion	= (String)request.getSession().getAttribute("claveConfirmacion");
		String diasPrevios = servicios.getDiasPreviosVencimiento();
		
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("modoAcceso",modoAcceso);
		jsonObj.put("claveConfirmacionSesion",claveConfirmacionSesion);
		jsonObj.put("diasPrevios",diasPrevios);
		infoRegresar = jsonObj.toString();
	} catch(Exception e){
		throw new AppException("Error al generar los valores iniciales");
	}
}
else if(informacion.equals("catalogoCompCelular")){
try{
	JSONObject jsonObj0 = new JSONObject();
	jsonObj0.put("descripcion","Iusacell");
	jsonObj0.put("clave","IUSACELL");
	
	JSONObject jsonObj1 = new JSONObject();
	jsonObj1.put("descripcion","Movistar");
	jsonObj1.put("clave","MOVISTAR");
	
	JSONObject jsonObj2 = new JSONObject();
	jsonObj2.put("descripcion","Nextel");
	jsonObj2.put("clave","NEXTEL");
	
	JSONObject jsonObj3 = new JSONObject();
	jsonObj3.put("descripcion","Telcel");
	jsonObj3.put("clave","TELCEL");
	
	JSONObject jsonObj4 = new JSONObject();
	jsonObj4.put("descripcion","Unefon");
	jsonObj4.put("clave","UNEFON");

	JSONArray jsonArray = new JSONArray();
	jsonArray.add(jsonObj0);
	jsonArray.add(jsonObj1);
	jsonArray.add(jsonObj2);
	jsonArray.add(jsonObj3);
	jsonArray.add(jsonObj4);
	
	infoRegresar = 
						"{\"success\": true, \"total\": \"5\", \"registros\": " + jsonArray.toString()+"}";
	} catch(Exception e){
		throw new AppException("Error al generar el catálogo");
	}
}else if(informacion.equals("ConfirmarServicio")){
	try{
			String claveConfirmacionIngresada	= (request.getParameter("claveConfirmacion")!=null)?request.getParameter("claveConfirmacion"):"";
			
				String bancoFondeo	= servicios.getBancoFondeo(iNoEPO);
				String compCelular	= (request.getParameter("compCelular")!=null)?request.getParameter("compCelular"):"";
				String momentoEnvioA	= (request.getParameter("momentoEnvioA")!=null)?request.getParameter("momentoEnvioA"):"";
				String momentoEnvioB	= (request.getParameter("momentoEnvioB")!=null)?request.getParameter("momentoEnvioB"):"";
				String tipoInformacionA	= (request.getParameter("tipoInformacionA")!=null)?request.getParameter("tipoInformacionA"):"";
				String tipoInformacionB	= (request.getParameter("tipoInformacionB")!=null)?request.getParameter("tipoInformacionB"):"";
				String momentoEnvioS = "";
				String tipoInformacionS = "";
				
				if(momentoEnvioA.equals("")){
					momentoEnvioS=momentoEnvioB;
				} else if(momentoEnvioB.equals("")){
					momentoEnvioS=momentoEnvioA;
				} else if(momentoEnvioA!=""&&momentoEnvioB!="") {
					momentoEnvioS="A";
				}
				if(tipoInformacionA.equals("")){
					tipoInformacionS=tipoInformacionB;
				} else if(tipoInformacionB.equals("")){
					tipoInformacionS=tipoInformacionA;
				} else if(tipoInformacionA!=""&&tipoInformacionB!="") {
					tipoInformacionS="A";
				}
			
				List datosActivacionFactorajeMovil = new ArrayList();
			
				datosActivacionFactorajeMovil.add(iNoCliente);
				datosActivacionFactorajeMovil.add(bancoFondeo);
				datosActivacionFactorajeMovil.add(momentoEnvioS);
				datosActivacionFactorajeMovil.add(tipoInformacionS);
				datosActivacionFactorajeMovil.add(compCelular.toUpperCase());

				servicios.activarServicioFactorajeMovil(datosActivacionFactorajeMovil);
				List datosPyme = servicios.getDatosPymeFactorajeMovil(iNoCliente);
				String numeroCelular = (String)datosPyme.get(1);
				
				SMS sms = new SMS();
				sms.enviarRegistroExitoso(numeroCelular);	
				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("mensaje", "El servicio ha sido activado");	
			
	} catch(Exception e){
		jsonObj.put("success", new Boolean(false));
		String  msg	= "Error al confirmar el servicio. "+e;
		jsonObj.put("msg",msg	);
		throw new AppException("Error al confirmar el servicio.",e);
	}
	infoRegresar = jsonObj.toString();  
}else if (informacion.equals("Continuar")){
		List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
		pantallasNavegacionComplementaria.remove("/15cadenas/15mantenimiento/15parametrizacion/15pyme/15EnvioPromocionesActivaServicioext.jsp"); //El nombre debe coincidir con el especificado en Navegación
		infoRegresar = "{\"success\": true, \"urlPagina\": \"" + appWebContextRoot + pantallasNavegacionComplementaria.get(0) + "\"}";
	}
%>
<%=infoRegresar%>
