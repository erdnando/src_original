<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String fechaActual = (new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
//String txt_fecha_app = request.getParameter("txt_fecha_app")==null?fechaActual:request.getParameter("txt_fecha_app"); 
String cmbTasa =request.getParameter("ic_tasa")==null?"":request.getParameter("ic_tasa");
String txt_fecha_app = request.getParameter("txt_fecha_app")==null?"":request.getParameter("txt_fecha_app"); 

if(txt_fecha_app.equals("")){
txt_fecha_app = fechaActual;
}
System.out.println("txt_fecha_app ----"+txt_fecha_app);
System.out.println("cmb tasa ----"+cmbTasa);

String infoRegresar = "";

if (informacion.equals("CatalogoTasas")) {
  CatalogoTasa cat = new CatalogoTasa();
  cat.setCampoClave("ic_tasa");
  cat.setCampoDescripcion("cd_nombre"); 
  cat.setDisponible("S");
  cat.setOrden("cd_nombre");
  
  infoRegresar = cat.getJSONElementos();
} else if (
	informacion.equals("Consulta") || 
	informacion.equals("ArchivoCSV") ||
	informacion.equals("ArchivoPDF") ){
	com.netro.cadenas.ConsTasa paginador = new com.netro.cadenas.ConsTasa();
	paginador.setFechaAplicacion(txt_fecha_app);
	paginador.setIcTasa(cmbTasa); 
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );

	if (informacion.equals("Consulta")){
		
		try {
			Registros reg = queryHelper.doSearch();
			infoRegresar = 
				"{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+"}";
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	} else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
		}
	} else if (informacion.equals("ArchivoPDF")) {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
	}
} 	
//Thread.sleep(10*1000);
%>
<%=infoRegresar%>