<%--
	FODEA 025-2012: DESCUENTO AUTOM�TICO
	@fecha 27 Dic 2012
	@autor garellano 
--%>
<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.util.Vector,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		net.sf.json.JSONArray,
		net.sf.json.util.JSONUtils,
		netropology.utilerias.*,
		com.netro.exception.*,
		java.util.Calendar,
      javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>
<%
   
	String informacion   		= request.getParameter("informacion")==null?"":request.getParameter("informacion");
	String operacion   			= request.getParameter("operacion")==null?"":request.getParameter("operacion");
	String operacion2   			= request.getParameter("operacion2")==null?"":request.getParameter("operacion2");
	String idDinamico    		= request.getParameter("idDinamico")==null?"":request.getParameter("idDinamico");
	String ic_epo    				= request.getParameter("ic_epo")==null?"":request.getParameter("ic_epo");
   String cc_tipo_factoraje 	= request.getParameter("cc_tipo_factoraje")==null?"N":request.getParameter("cc_tipo_factoraje");
   
	String clavePyme 		= request.getParameter("ic_pyme")==null?iNoCliente:request.getParameter("ic_pyme");
   String claveEpo      = iNoEPO;
   String respuesta     = null;  
   
   boolean 	esUsuarioPyme 				= false;
	boolean 	esUsuarioNafin 			= false;
	boolean actualizacionExitosa 		= false;
   boolean actualizacionExitosa1 		= false;
	boolean actualizacionExitosa2 		= false;
	
	String[] arrModalidadModificado = request.getParameterValues("arrModalidadModificado");
   String[] arrIcMonedaModificado = request.getParameterValues("arrIcMonedaModificado");
   String[] arrIcEpoModificado = request.getParameterValues("arrIcEpoModificado");
   String[] arrIcIfModificado = request.getParameterValues("arrIcIfModificado");
   String[] arrIcPymeModificado = request.getParameterValues("arrIcPymeModificado");
   String[] arrOrdenModificado = request.getParameterValues("arrOrdenModificado");
   String[] arrModificadosID = request.getParameterValues("arrModificadosID");
		
	String[] arrModalidadModificadoV = request.getParameterValues("arrModalidadModificadoV");
   String[] arrIcMonedaModificadoV = request.getParameterValues("arrIcMonedaModificadoV");
   String[] arrIcEpoModificadoV = request.getParameterValues("arrIcEpoModificadoV");
   String[] arrIcIfModificadoV = request.getParameterValues("arrIcIfModificadoV");
   String[] arrIcPymeModificadoV = request.getParameterValues("arrIcPymeModificadoV");
   String[] arrOrdenModificadoV = request.getParameterValues("arrOrdenModificadoV");
   String[] arrModificadosIDV = request.getParameterValues("arrModificadosIDV");
		
	String num_Normal   		= request.getParameter("num_Normal")==null?"0":request.getParameter("num_Normal");
	String num_Vencido   		= request.getParameter("num_Vencido")==null?"0":request.getParameter("num_Vencido");
	String operarDsctoAut = request.getParameter("operarDsctoAut")==null?"NO":request.getParameter("operarDsctoAut");
	operarDsctoAut = operarDsctoAut.equals("NO")?"N":"S";	
	
	//2017_003
    String numDistribuido = request.getParameter("numDistribuido")==null?"0":request.getParameter("numDistribuido"); 
    String[] arrModalidadModificadoD = request.getParameterValues("arrModalidadModificadoD");
    String[] arrIcMonedaModificadoD = request.getParameterValues("arrIcMonedaModificadoD");
    String[] arrIcEpoModificadoD = request.getParameterValues("arrIcEpoModificadoD");
    String[] arrIcIfModificadoD = request.getParameterValues("arrIcIfModificadoD");
    String[] arrIcPymeModificadoD = request.getParameterValues("arrIcPymeModificadoD");
    String[] arrOrdenModificadoD = request.getParameterValues("arrOrdenModificadoD");
    String[] arrModificadosIDD = request.getParameterValues("arrModificadosIDD");
    boolean actualizacionExitosaD = false;    
       
    String numDescuentoAutoEPO  =  request.getParameter("numDescuentoAutoEPO")==null?"0":request.getParameter("numDescuentoAutoEPO"); 
    String[] arrIcEpoModificadoAE = request.getParameterValues("arrIcEpoModificadoAE");
    String[] arrIcIfModificadoAE = request.getParameterValues("arrIcIfModificadoAE");
    String[] arrIcPymeModificadoAE = request.getParameterValues("arrIcPymeModificadoAE");
    String[] arrIcMonedaModificadoAE = request.getParameterValues("arrIcMonedaModificadoAE");
    String[] arrOrdenModificadoAE = request.getParameterValues("arrOrdenModificadoAE");
    String[] arrModalidadModificadoAE = request.getParameterValues("arrModalidadModificadoAE");
    String[] arrModificadosIDAE = request.getParameterValues("arrModificadosIDAE");
     boolean actualizacionExitosaDAE = false;
    
   
    JSONObject  jsonObj    	=  new JSONObject();  
    
    if ("A".equals(cc_tipo_factoraje)  ) {
     operacion ="";   
    }
   System.out.println (" ********************************************************           ");		
    System.out.println (" informacion           "+informacion);	
    System.out.println (" cc_tipo_factoraje           "+cc_tipo_factoraje);	
    System.out.println (" clavePyme           "+clavePyme);	
    System.out.println (" operacion           "+operacion);
    
    
   if(   informacion.equals("obtieneEpos") ){  	
	
		List 		ls 						= 	null;    
      Integer totalEpos             =  null;		
      int totalEposVencimiento      =  0,  ic_producto_nafin = 	1;				
		String cs_factoraje_vencido	=	"S",cs_aceptacion = 	"H";
		String csFactorajeDistribuido = "S";
    int totalEposDistri=0; //2017_003	
    int totalEposAutoDesc =0;
      ParametrizacionDsctoAutomaticoDE parametros 	= 	new ParametrizacionDsctoAutomaticoDE();
      CQueryHelperExtJS 					queryHelper = 	new CQueryHelperExtJS(parametros);   
      
		// LISTA DE EPOS
		ls = parametros.listaHashMapEpos(clavePyme, ic_producto_nafin, cc_tipo_factoraje, cs_factoraje_vencido, cs_aceptacion, operacion, csFactorajeDistribuido);
		
		if(ls.size()==0 || ls==null){
			totalEpos = new Integer(0);
		}else{
			totalEpos = new Integer(ls.size());
		}
			
		
		// OBTIENE CANTIDAD DE EPOS CON FACTORAJE VENCIDO
		totalEposVencimiento = parametros.existenEposFactorajeVencido(clavePyme, ic_producto_nafin, cc_tipo_factoraje, cs_factoraje_vencido, cs_aceptacion, operacion);
				
		totalEposDistri  =  parametros.existenEposFactorajeDistribuido( clavePyme); //2017_003
		
                
                totalEposAutoDesc  =  parametros.getEposOperanDescAutomticoEPO( clavePyme); //2019 QC 
		 
                String  pymeDescAutoEPO  =  parametros.getPymeDescAutoEPO(clavePyme); //2019 QC
                  
                  
		// OBITIENE OPERAR DESCUENTO AUTOMATICO 
		jsonObj    	=  new JSONObject(); 
		jsonObj.put("cs_dscto_automatico",	parametros.obtieneOperaDescuentoAutomatico(clavePyme));		
		jsonObj.put("registros",            ls);
		jsonObj.put("totalEpos",            totalEpos);
		jsonObj.put("totalEposVenc", 			new Integer(totalEposVencimiento));
		jsonObj.put("totalEposDistri",  new Integer(totalEposDistri));	
                jsonObj.put("totalEposAutoDesc",  new Integer(totalEposAutoDesc));
                jsonObj.put("pymeDescAutoEPO", pymeDescAutoEPO); 
		jsonObj.put("success",              new Boolean(true));
		respuesta = jsonObj.toString();
   
	}else if( informacion.equals("obtieneFactorajeNormal") ){
	
		ParametrizacionDsctoAutomaticoDE parametros 	= new ParametrizacionDsctoAutomaticoDE();
         
		List ls = parametros.obtieneListaIF(ic_epo, clavePyme, cc_tipo_factoraje);
			
      jsonObj   =  new JSONObject();
      jsonObj.put("total",       new Integer(ls.size()));
      jsonObj.put("success",     new Boolean(true));
      jsonObj.put("registros",   ls);
       respuesta = jsonObj.toString();
	
	}  else if( informacion.equals("enviarParametrizacionNormal") ){
	
		String 	mensaje 		= 	null;
      String 	icoMensaje 	= 	null;
		String	cveUsuario	=	iNoCliente;
		String 	cc_acuse		=	null;
	
		ParametrizacionDsctoAutomaticoDE parametros 	= new ParametrizacionDsctoAutomaticoDE();
		
		if(!num_Normal.equals("0") ) {
			actualizacionExitosa1 = parametros.procesoActualizacionParametrizacion(arrModalidadModificado, arrIcMonedaModificado, arrIcEpoModificado,	arrIcIfModificado,
																				arrIcPymeModificado, arrOrdenModificado,	arrModificadosID, "N", cc_acuse, strLogin, strNombreUsuario, operarDsctoAut,  clavePyme	);
		}			
		
		if(!num_Vencido.equals("0") ) {		
			actualizacionExitosa2 = parametros.procesoActualizacionParametrizacion( arrModalidadModificadoV,	arrIcMonedaModificadoV,			arrIcEpoModificadoV,
																				arrIcIfModificadoV, arrIcPymeModificadoV,	arrOrdenModificadoV,	arrModificadosIDV, "V",	cc_acuse,
																				strLogin,	strNombreUsuario, operarDsctoAut,  clavePyme	);			
		}
		
		
		//2017_003		
		if (!"0".equals(numDistribuido)  ) {
		    actualizacionExitosaD = parametros.procesoActualizacionParametrizacion( arrModalidadModificadoD,arrIcMonedaModificadoD,arrIcEpoModificadoD,	arrIcIfModificadoD, arrIcPymeModificadoD,arrOrdenModificadoD,arrModificadosIDD, "D",cc_acuse,strLogin,strNombreUsuario, operarDsctoAut, clavePyme );			
		}
		
                // 2019_QC
                if (!"0".equals(numDescuentoAutoEPO)  ) {
		    actualizacionExitosaDAE = parametros.procesoActualizacionParametrizacion( arrModalidadModificadoAE,arrIcMonedaModificadoAE,arrIcEpoModificadoAE,	arrIcIfModificadoAE, arrIcPymeModificadoAE, arrOrdenModificadoAE, arrModificadosIDAE, "A",cc_acuse,strLogin,strNombreUsuario, operarDsctoAut, clavePyme );			
		}
                
		if( (  !num_Normal.equals("0")  && actualizacionExitosa1==true )    ||  ( !num_Vencido.equals("0") &&  actualizacionExitosa2  ==true )     ||   (!"0".equals(numDistribuido)   && actualizacionExitosaD==true ) 
                 ||   (!"0".equals(numDescuentoAutoEPO)   && actualizacionExitosaDAE==true )  ) { 
			actualizacionExitosa = true;
		}else  {
			actualizacionExitosa = true;
		}
		
		
		
		//Actualiza el  Descuento Autmatico
		parametros.actOperarDescuentoAutomatico(operarDsctoAut, clavePyme, strLogin, "N");
	
			
      if(actualizacionExitosa){
			mensaje = "Los Datos se han guardado con éxito ...";
			icoMensaje = "INFO";	
		}else{
			mensaje = "Ocurrio un problema al guardar la parametrización ...";
			icoMensaje = "ERROR";		
		}
      		
      jsonObj   =  new JSONObject();
      jsonObj.put("success",   new Boolean(actualizacionExitosa));
      jsonObj.put("mensaje",   mensaje);
      jsonObj.put("icoMensaje", icoMensaje);
      respuesta = jsonObj.toString();
		
		
   } else if(informacion.equals("actualizaOperarDescuentoAutomatico")){
		/*
		*	CUANDO MARCA LA OPCION DE OPERAR DESCUENTO AUTOMATICO CAMBIA EL ESTATUS
		*	PARA OPERAR O NO CON DESCUENTO AUTOMATICO
		*/
		
		ParametrizacionDsctoAutomaticoDE parametros 	= new ParametrizacionDsctoAutomaticoDE();
		boolean hayActualiacion = false;
		String mensaje = "";
		
			
		hayActualiacion = parametros.actualizaOperarDescuentoAutomatico(operarDsctoAut, clavePyme, strLogin, "S");
		
		if(hayActualiacion){
			mensaje = "Los datos se han guardado con éxito ...";
		}else{
			mensaje = "Hubo problemas al guardar la información ...";
		}
		
      jsonObj   =  new JSONObject();
      jsonObj.put("success",   new Boolean(hayActualiacion));
      jsonObj.put("mensaje",   mensaje);
      respuesta = jsonObj.toString();
	
	} else if(informacion.equals("ConfirmarCertificado")){
	
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
							
		// ----- Variables de seguridad -----
		String pkcs7 = request.getParameter("Pkcs7");
		String folioCert = "";
		String externContent = request.getParameter("TextoFirmado");
		char getReceipt = 'Y';
		String cc_acuse = "";
		jsonObj   =  new JSONObject();
						
		// ----- Autentificación -----
		Seguridad s = new Seguridad();
		Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
		if(true){
		//if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			folioCert = acuse.toString();
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
					
				cc_acuse = s.getAcuse();
				
				String 	mensaje 		= 	null;
				String 	icoMensaje 	= 	null;
				String	cveUsuario	=	iNoCliente;
				
				ParametrizacionDsctoAutomaticoDE parametros 	= new ParametrizacionDsctoAutomaticoDE();
				
				
				if(operacion.equals("actualizaOperarDescuentoAutomaticoAcuse")){	
					
					actualizacionExitosa 	= 	parametros.actualizaOperarDescuentoAutomatico(operarDsctoAut, clavePyme, strLogin,"S");					
				
				}else{				
					
					if(!num_Normal.equals("0") ) {
						actualizacionExitosa1 = parametros.procesoActualizacionParametrizacion(arrModalidadModificado, arrIcMonedaModificado, arrIcEpoModificado,	arrIcIfModificado,
																							arrIcPymeModificado, arrOrdenModificado,	arrModificadosID, "N", cc_acuse, strLogin, strNombreUsuario, operarDsctoAut,  clavePyme 	);
					}			
					
					if(!num_Vencido.equals("0") ) {		
						actualizacionExitosa2 = parametros.procesoActualizacionParametrizacion( arrModalidadModificadoV,	arrIcMonedaModificadoV,			arrIcEpoModificadoV,
																							arrIcIfModificadoV, arrIcPymeModificadoV,	arrOrdenModificadoV,	arrModificadosIDV, "V",	cc_acuse,
																							strLogin,	strNombreUsuario, operarDsctoAut,  clavePyme );			
					}
					
					//2017_003
					if (!"0".equals(numDistribuido)  ) {		
					    actualizacionExitosaD = parametros.procesoActualizacionParametrizacion( arrModalidadModificadoD,arrIcMonedaModificadoD,arrIcEpoModificadoD,	arrIcIfModificadoD, arrIcPymeModificadoD,arrOrdenModificadoD,arrModificadosIDD, "D",cc_acuse,strLogin,strNombreUsuario, operarDsctoAut, clavePyme );			
					}
                                        
                                        //2019_QC
                                        if (!"0".equals(numDescuentoAutoEPO)  ) {		
					    actualizacionExitosaDAE = parametros.procesoActualizacionParametrizacion( arrModalidadModificadoAE,arrIcMonedaModificadoAE, arrIcEpoModificadoAE,	arrIcIfModificadoAE, arrIcPymeModificadoAE, arrOrdenModificadoAE, arrModificadosIDAE, "A",cc_acuse,strLogin,strNombreUsuario, operarDsctoAut, clavePyme );			
					}
					
		      	if( (  !num_Normal.equals("0")  && actualizacionExitosa1==true )    ||  ( !num_Vencido.equals("0") &&  actualizacionExitosa2  ==true )   ||   (!"0".equals(numDistribuido)   && actualizacionExitosaD==true )  
                                ||   (!"0".equals(numDescuentoAutoEPO)   && actualizacionExitosaDAE==true )  ) { 
                                
						actualizacionExitosa = true;
					}else  {
						actualizacionExitosa = true;
					}
				  //Actualiza el  Descuento Autmaico
					parametros.actOperarDescuentoAutomatico(operarDsctoAut, clavePyme, strLogin, "N");
				
					
				}
																
				if(actualizacionExitosa){
					mensaje = "Los Datos se han guardado con éxito ...";
					icoMensaje = "INFO";	
				}else{
					mensaje = "Ocurrio un problema al guardar la parametrización ...";
					icoMensaje = "ERROR";		
				}
				
				List listaHoraFecha = parametros.fechaHoraOperacion();
				
				String usuario_autorizacion = strLogin + " - " + strNombreUsuario;
				HashMap hm = null;
				List lista = new ArrayList();
				
				hm = new HashMap();
				hm.put(	"TITULO", 				"Número de Acuse"		);
				hm.put(	"VALOR", 				cc_acuse					);
				lista.add(hm);
				
				hm = new HashMap();
				hm.put(	"TITULO", 				"Fecha de Autorización"	);
				hm.put(	"VALOR", 				listaHoraFecha.get(0)	);
				lista.add(hm);
				
				hm = new HashMap();
				hm.put(	"TITULO", 				"Hora de Autorización"	);
				hm.put(	"VALOR", 				listaHoraFecha.get(1)	);
				lista.add(hm);
				
				hm = new HashMap();
				hm.put(	"TITULO", 				"Usuario de Autorización"	);
				hm.put(	"VALOR", 				usuario_autorizacion			);
				lista.add(hm);
				
				jsonObj.put("success",   new Boolean(actualizacionExitosa));
				jsonObj.put("mensaje",   mensaje);
				jsonObj.put("icoMensaje", icoMensaje);
				jsonObj.put("registros", lista);
			
			} else {	//autenticación fallida
				String _error = s.mostrarError();
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("mensaje", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
				jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
				jsonObj.put("cc_acuse", cc_acuse);
			}
		}
		respuesta = jsonObj.toString();
	}
%>


<%=respuesta%>