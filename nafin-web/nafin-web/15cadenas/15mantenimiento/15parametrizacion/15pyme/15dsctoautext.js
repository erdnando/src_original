Ext.onReady(function() {

//---------------------------------- VARIABLES ------------------------------

	var catalogosIF 						= null;
	var cuentasAsociadasPorComboIF 	= null;
	var valoresPredeterminados			= null;
	
	var autorizacion						= false;
	
	// NOTA(ERROR): que pasa si una pyme no tiene ninguna epo
	// se podra guardar su autorizacion a descuento?
	
//---------------------------------- HANDLERS -------------------------------

	function procesarRadioDsctoAutomaticoChange( radioGroup, checkedRadio){
		
		if(radioGroup.getValue().inputValue == "S"){
			
			// Mostrar los siguientes elementos de la forma
			var forma = Ext.getCmp('forma');
			
			if(autorizacion){
 
				// Mostrar los siguientes componentes: 
				// 1. Lista de EPOs con Dscto Automatico Obligatorio
				Ext.getCmp('eposDsctoAutmaticoObligatorio').show();
				// 2. Lista de Campos EPO
				for(var i=0;i<forma.items.items.length;i++){
					
					var componente = forma.items.items[i];
					if(Ext.isEmpty(componente.name)) continue;
						
					if( componente.name.substr(0,9) == 'nombreEpo'){
						componente.show();
					}
					
				}
				// 3. Habilitar los Botones Guardar e Imprimir
				Ext.getCmp('botonGuardar').enable();
				Ext.getCmp('botonImprimir').enable();
 
				// Redibujar forma
				forma.doLayout();
				
			}else{
			
				// Actualizar Cambios // Solo implementado para pyme
				forma.el.mask('Procesando...', 'x-mask-loading');
				Ext.Ajax.request({
					url: '15dsctoautext.data.jsp',
					params: {
						accion:  	"GET_COMPONENTES_FORMA",
						rdAutoriza: "S",
						resetForma: "true"
					},
					callback: procesarSuccessFailureGetComponentesForma
				});
	
			}
			
		}else if(radioGroup.getValue().inputValue == "N"){
			
			var forma = Ext.getCmp('forma');
			
			// Actualizar Cambios // Solo implementado para pyme
			forma.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15dsctoautext.data.jsp',
				params: {
					accion:  	"GET_COMPONENTES_FORMA",
					rdAutoriza: "N",
					resetForma: "true"
				},
				callback: procesarSuccessFailureGetComponentesForma
			});
				
			/*
			// Ocultar los siguientes componentes: 
			// 1. Lista de EPOs con Dscto Automatico Obligatorio
			Ext.getCmp('eposDsctoAutmaticoObligatorio').hide();
			// 2. Lista de Campos EPO
			for(var i=0;i<forma.items.items.length;i++){
				
				var componente = forma.items.items[i];
				if(Ext.isEmpty(componente.name)) continue;
				
				if( componente.name.substr(0,9) == 'nombreEpo'){
					componente.hide();
				}
				
			}
			
			// Redibujar forma
			forma.doLayout();
			*/
		}
		
	}
	
	function procesarComboModalidadSelect(combo, record, index){
 
		// Si se selecciono en el combo de Modalidad ( DsctoAutomaticoDia ) un registro diferente del registro actual, poner
		// hubocambio en true.
		var hubocambio = combo.originalValue != String(combo.getValue());
		
		//Si hubo cambio, poner hidHuboCambioDia en "S", en caso contrario, ponerlo en "N"
		var indice 					= combo.name.substr(9);
		Ext.getCmp('hidHuboCambioDia'+indice).setValue( (hubocambio)?'S':'N' );
		
	}
 
	function procesarComboIFSelect(combo, record, index){
		
		// Buscar caracter "N" en el registro seleccion del combo IF
		var newValue 		= combo.getValue();
		var parametrizado = String(newValue).indexOf('N'); 
		// Si se selecciono el primer elemento del combo de IFs poner parametrizado = 1 
		if(index == 0){
			parametrizado = 1;
		}
 
		// Si en el Combo IF donde se realizaron los cambios y parametrizado > 0 indicar que hubo cambios
		var indice 					= combo.name.substr(23);
		Ext.getCmp('hidHuboCambio'+indice).setValue( (parametrizado>0)?'S':'N' );

		// Si se selecciono el primer registro del combo IF, resetear el campo de mensaje, de tal manera que este se muestre vacio.
		var claveIF 				= combo.getValue();
		if( index == 0 ){
			Ext.getCmp('labelAdvertenciaOperacion'+indice).setText("");
		} else {
			// si se selecciono en el combo IF un registro diferente el primero, para la EPO parametrizable en cuestion (referida por el indice)
			// mostrar la siguiente leyenda en el campo de mensajes
			Ext.getCmp('labelAdvertenciaOperacion'+indice).setText("El Intermediario Financiero a utilizar ser� el designado por la EPO en la selecci�n autom�tica para Descuento al vencimiento.");
		}
 
	}
	
	function procesarSuccessFailureGuardar(opts, success, response){
		
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

				// 1. DECODIFICAR RESPUESTA
				var respuesta 	= Ext.util.JSON.decode(response.responseText);
				
				var forma      		= Ext.getCmp("forma");
				var volverAEntrar 	= false;
				
				// Actualizar valor original del Radio de Autorizacion en el hidden
				Ext.getCmp('autorizaValorOriginal').setValue(opts.params.radioAutorizacionDsctoAutomatico);
				
				// Actualizar valor original del Radio de Autorizacion
				var radioAutorizacionDsctoAutomatico = Ext.getCmp('radioAutorizarDsctoAutomatico');
				for(var i=0;i<radioAutorizacionDsctoAutomatico.items.items.length;i++){
					var radioButton = radioAutorizacionDsctoAutomatico.items.items[i];
					
					if(opts.params.radioAutorizacionDsctoAutomatico == radioButton.inputValue){
						radioButton.originalValue = true;
					}else{
						radioButton.originalValue = false;
					}
 
				}
				 
				// ComboIF:        Actualizar valores originales con los nuevos valores guardados
				// ComboModalidad: Actualizar el valor actual y el valor original del combo de modalidad, 
				//                 el cual depende del if seleccionado.
				for(var i=0;i<forma.items.items.length;i++){
						
					var componente = forma.items.items[i];
					if(Ext.isEmpty(componente.name)) continue;
					
					// Se encontro una caja EPO
					if( componente.name.substr(0,9) == 'nombreEpo'){
						
						for(var j=0;j<componente.items.items.length;j++){
							
							var elementoForma = componente.items.items[j];
							
							if(Ext.isEmpty(elementoForma.name)) continue;
 
							if( elementoForma.name.substr(0,23) == "intermediarioFinanciero" ){
								
								if(Ext.isEmpty(respuesta.intermediario[elementoForma.name])){
									volverAEntrar = true;
									continue;
								}
								
								// Actualizar store
								elementoForma.getStore().loadData(respuesta.intermediario[elementoForma.name].data);
								// Definir valor original
								elementoForma.setValue(respuesta.intermediario[elementoForma.name].selected);
								// Definir valor actual
								elementoForma.originalValue = respuesta.intermediario[elementoForma.name].selected;
								
							}else if( elementoForma.name.substr(0,9) == "modalidad" ){
								
								if(Ext.isEmpty(respuesta.modalidad[elementoForma.name])){
									volverAEntrar = true;
									continue;
								}
								
								elementoForma.setValue(respuesta.modalidad[elementoForma.name]);
								elementoForma.originalValue = respuesta.modalidad[elementoForma.name];
								
							}
							
						}
					}
						
				}
 
				// Resetear parametros de modificacion
				if( !Ext.isEmpty(Ext.getCmp('ultimoIndice')) ){
					var ultimoIndice = parseInt(Ext.getCmp('ultimoIndice').getValue(),10);
					for(var k=0;k<=ultimoIndice;k++){
						Ext.getCmp('hidHuboCambio'+k).setValue("N");
						Ext.getCmp('hidHuboCambioDia'+k).setValue("N");
					}
				}
				
				// Suprimir mascara
				var forma = Ext.getCmp('forma');
				forma.el.unmask();
				
				// Mostrar mensaje
				if(volverAEntrar){
					Ext.Msg.alert('Guardar',"Se perdi� la sincron�a. Por favor vuelva a entrar a la pantalla.");
				}else if(!Ext.isEmpty(respuesta.msg)){
					Ext.Msg.alert('Guardar',respuesta.msg);
				} 
 
			} else {
			
				NE.util.mostrarConnError(response,opts);
				
				// Suprimir mascara
				var forma = Ext.getCmp('forma');
				forma.el.unmask();
			
			}
			
	}
	
	function procesarSuccessFailureGetComponentesForma(opts, success, response){
		
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			// 1. DECODIFICAR RESPUESTA
			var respuesta 	= Ext.util.JSON.decode(response.responseText); 
 
			// Borrar los Elementos del Catalogo Anterior
			borraCamposForma();
			
			autorizacion		= respuesta.autorizacion;
 
			// Insertar componentes principales de la forma
			for(var i=0;i<respuesta.componentesPrincipales.length;i++){
				
				var componente = respuesta.componentesPrincipales[i];
				componente 		= Ext.decode(componente);
				
				if(componente.name == "radioAutorizarDsctoAutomatico"){
					componente.listeners = { 
						'change': procesarRadioDsctoAutomaticoChange 
					};
				}
				
				forma.insert(forma.items.length, componente);
				
			}
 
			// Insertar componentes correspondientes a las EPOs Parametrizables
			if(!Ext.isEmpty(respuesta.eposParametrizables)){
				
				for(var m=0;m<respuesta.eposParametrizables.length;m++){
					
					var componente = respuesta.eposParametrizables[m];
					componente = Ext.decode(componente);
	 
					var data = respuesta.listaEpos[componente.claveEpo];
	 
					// Asignar store al catalogo de modalidad
					var repeticiones = data.listaRegistros.length;
					for(var i=0;i<componente.items.length;i++){
						
						var elemento = componente.items[i].name;
						if(Ext.isEmpty(elemento)) continue;
						
						if(elemento.substr(0,9) == 'modalidad'){
							
							componente.items[i].store = 
								new Ext.data.SimpleStore({
									 fields: ['clave', 'descripcion'],
									 data: 	Ext.constantdata.modalidad
								});// catalogoModalidadData;
							
							// Registrar listener	
							componente.items[i].listeners = { 
								'select': procesarComboModalidadSelect 
							};
							
							repeticiones--;
							if(repeticiones == 0) break;
							
						}
						
					}
					
					// Crear stores para los combos de los intermediarios financieros
					repeticiones = data.listaRegistros.length;
					for(var i=0;i<componente.items.length;i++){
						
						var elemento = componente.items[i].name;
						if(Ext.isEmpty(elemento)) continue;
						
						if(elemento.substr(0,23) == 'intermediarioFinanciero'){
							
							// Obtener indice
							var indice = elemento.substr(23);
							
							// Obtener datos
							var registroEpoMoneda = null;
							for(var k=0;k<data.listaRegistros.length;k++){
								if(data.listaRegistros[k].indice == indice ){
									registroEpoMoneda = data.listaRegistros[k];
									break;
								}
							}
							if(registroEpoMoneda == null) continue;
							
							// Registrar store
							componente.items[i].store =
								new Ext.data.SimpleStore({
									 fields: ['clave', 'descripcion'],
									 data: registroEpoMoneda.catalogoComboIF
								});
								
							// Registrar listener	
							componente.items[i].listeners = { 
								'select': procesarComboIFSelect 
							};
							
							repeticiones--;
							if(repeticiones == 0) break;
							
						}
						
					}
	 
					forma.insert(forma.items.length, componente);
					
				}
				
			}
			forma.doLayout();
			
			// Habilitar los botones guardar e imprimir
			if(!Ext.isEmpty(respuesta.eposParametrizables) && respuesta.eposParametrizables.length > 0 || !Ext.isEmpty(respuesta.autorizacion)){
				
				Ext.getCmp('botonGuardar').enable();
				Ext.getCmp('botonImprimir').enable();
				
			}else{
				
				Ext.getCmp('botonGuardar').hide();
				Ext.getCmp('botonImprimir').hide();
				
			}
			
		} else {
			
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}

//----------------------------------- STORES --------------------------------

	// Catalogo Tipo TASA
	Ext.namespace('Ext.constantdata');
	Ext.constantdata.modalidad = [
			  ['P','Primer d�a desde su publicaci�n'],
			  ['U','�ltimo d�a desde su vencimiento considerando d�as m�nimos']
	];

//--------------------------------- FUNCIONES -------------------------------

	var fnValidaciones = function(){
		Ext.Ajax.request({
			url: '/nafin/00utils/NEcesionDerechos.jsp',
			params: Ext.apply(objFormLogCesion.getForm().getValues(),{
				informacion: 	'validaClaveCesionDerec',
				cesionAltaCve: 'N',
				metodo: 			'utilerias'
			}),
			callback: obj.procesarSuccessNEclaveCesion
		});
	}
	
	var resultValidCesion= function(okResp, siCorreo, email, errorMessage){
		
		if(okResp=='S'){
			guardar();		
		}
		
	}
	
	var transmitirModificaciones = function(btn){
		
		// 1. VALIDAR ELEMENTOS
		
		//var f = document.frmDsctoAut;
		var forma = Ext.getCmp('forma');
		
		var radioAutorizacionDsctoAutomatico = Ext.getCmp('radioAutorizarDsctoAutomatico');
 
		// Leer Valor Seleccionado del radio de autorizacion de descuento automatico
		var radioAutorizacionDsctoAutomatico_currentValue 	= radioAutorizacionDsctoAutomatico.getValue().inputValue;
		// Leer Valor original del radio de autorizacion de descuento automatico
		var radioAutorizacionDsctoAutomatico_originalValue	= Ext.getCmp('autorizaValorOriginal').getValue();
		
		// Si se ha seleccionado el radio: Operar descuento automatico = "N" y el valor actual configurado 
		// es "N", no realizar ningun cambio
		if ( radioAutorizacionDsctoAutomatico_currentValue == 'N' && radioAutorizacionDsctoAutomatico_originalValue == 'N' ) {	
			return; //No guarda nada porque no hubo cambios
		}
				
		// Si se ha seleccionado checkbox: Operar descuento automatico = "S", 
		// realizar las siguientes validaciones:
		if( radioAutorizacionDsctoAutomatico_currentValue != 'N' ) {

			// Obtener forma
			var forma = Ext.getCmp('forma');
			
			// Poner flag de cambios en false
			var huboCambios 	= false;
			var hayErrores 	= false;
			
			// Validar que se hayan seleccionado los parametros IF donde la seleccion es obligatoria
			// Debido a un bug en el ID se valida asi :(
			for(var i=0;i<forma.items.items.length;i++){
					
				var componente = forma.items.items[i];
				if(Ext.isEmpty(componente.name)) continue;
				
				// Se encontro una caja EPO
				if( componente.name.substr(0,9) == 'nombreEpo'){
					
					for(var j=0;j<componente.items.items.length;j++){
						
						var elementoForma = componente.items.items[j];
						
						if(Ext.isEmpty(elementoForma.name)) continue;
						
						if( elementoForma.name.substr(0,23) == "intermediarioFinanciero" ){
							
							var idx = elementoForma.name.substr(23);
							
							if( elementoForma.getValue() == "OBLG" ){	
								hayErrores = true;
								elementoForma.markInvalid('Por favor, especifique un Intermediario Finaciero');
							}
							
						}
						
					}
					
					
				}
					
			}
			
			// Si hay errores finalizar las validaciones, y alertar al usuario
			if(hayErrores){
				Ext.Msg.alert('Validaci�n', "Por favor, especifique un Intermediario Finaciero");
				return;
			}
			
			// Revisar si hubo cambios en la seleccion de combos
			var ultimoIndice = parseInt(Ext.getCmp('ultimoIndice').getValue(),10);
			for(var k=0;k<=ultimoIndice;k++){
				
				if( Ext.getCmp('hidHuboCambio'+k).getValue() == "S" ){	
					huboCambios = true;
					break;
				}
				
				if( Ext.getCmp('hidHuboCambioDia'+k).getValue() == "S" ){	
					huboCambios = true;
					break;
				}
 
			}
			
			// Si no hubieron cambios cancelar el proceso de guardado.
			if(!huboCambios){
				Ext.Msg.alert('Validaci�n', "No se ha modificado la parametrizaci�n actual");
				return;
			}
			
		}
		
		// 2. VALIDAR PARAMETROS DE USUARIO
		var ventana = Ext.getCmp('winCesionDerechos');
		var win = new NE.cesion.WinCesionDerechos({
			getResultValid:  resultValidCesion
		}).show();
		win.setHandlerBtnLogAceptar(fnValidaciones);
		win.el.dom.scrollIntoView();
		
	}
 
	function borraCamposForma(){
		
		var forma = Ext.getCmp('forma');
		for(var elemento=forma.items.length-1;elemento>=0;elemento--){
			forma.remove(forma.get(elemento), true);
		}
      
	}
	
	function guardar(boton, evento){
 
		var forma = Ext.getCmp('forma');
		
		// Guardar Cambios // Solo implementado para pyme
		forma.el.mask('Guardando cambios...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '15dsctoautext.data.jsp',
			params:
				Ext.apply(forma.getForm().getValues(),{
								accion: 'GUARDAR' 
							}),
			callback: procesarSuccessFailureGuardar
		});
 
	}
	
	function imprimir(boton, evento){
		
	}
	
//-------------------------------- PRINCIPAL -----------------------------------

	var fp = new Ext.form.FormPanel({
		id: 			'forma',
		labelWidth: 174, 
    frame:		true,
    title: 		'Descuento Autom�tico - Parametrizaci�n',
    bodyStyle: 	'padding:6px 6px 0',
   	collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		
		
		// Elementos de la Forma
		items: 		[],
		buttons: [
			// Boton Guardar
			{
				id:			'botonGuardar',
				text: 		'Guardar',
				iconCls: 	'icoGuardar',
				disabled:	true,
				handler: 	transmitirModificaciones
					
			},
			// Boton Imprimir
			{
				id:			'botonImprimir',
				text: 		'Imprimir',
				iconCls: 	'icoImprimir',
				disabled:	true,
				hidden: 		true,
				handler: 	imprimir
			}

		]

	});

	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		items: [
			fp
		]
	});

//-------------------------------- 	LOAD -----------------------------------

	// Cargar Componentes de la Forma
	fp.el.mask('Cargando Elementos...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '15dsctoautext.data.jsp',
		params: {
			accion:  "GET_COMPONENTES_FORMA"
		},
		callback: procesarSuccessFailureGetComponentesForma
	});
	
});
