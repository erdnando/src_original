Ext.onReady(function() {

//_--------------------------------- HANDLERS -------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) {
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();   
		if (arrRegistros != null) {
			
			if (!grid.isVisible()) {
				grid.show();
			}
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		var btnBajarPDF = Ext.getCmp('btnBajarPDF');
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		var el = grid.getGridEl();
		el.unmask();
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
			btnGenerarPDF.disable();
			btnBajarPDF.show();
		} else {
				btnGenerarPDF.enable();
				NE.util.mostrarConnError(response,opts);
			}
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
			btnGenerarArchivo.disable();
			btnBajarArchivo.show();
		}  else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
//-------------------------------- STORES -----------------------------------

var catalogoTasasData = new Ext.data.JsonStore({
    root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admpymeparam04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoTasas'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15admpymeparam04ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CD_NOMBRE'},
      {name: 'VALOR'}
    ],
		//totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
});

var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_tasa',
			id: 'cmbTasa',
			fieldLabel: 'Tasas',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_tasa',
			emptyText: 'TODAS',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoTasasData,
			listeners:{
				select: function(combo){
							var cmbTasa = combo.getValue();
							Ext.Ajax.request({
							url: '13consulta04ext.data.jsp',
							params: {
								icTasa: 'cmbTasa'								
							}
						});
	   				}
			},		
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'datefield',
			fieldLabel: 'Fecha de Aplicación',
				name: 'txt_fecha_app',
				id: 'txt_fecha_app',
				allowBlank: false,
				startDay: 1,
				width: 100,
				msgTarget: 'side',
				margins: '0 20 0 0',
				tpl : NE.util.templateMensajeCargaCombo	
		}
];

//grid
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'CD_NOMBRE',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false,
				align:'left'
			},
			{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'VALOR',
				sortable: true,
				hideable: false,
				width: 150,
				align: 'center'
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 300,
		width:405,
		style: 'margin: 0 auto',
		title: 'Tasas',
		frame: false,
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15admpymeparam04ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV'
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '15admpymeparam04ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF'
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-'
			]
		}
	});

//-------------------------------- PRINCIPAL -----------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Tasas Operativas',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid:true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar' //Generar datos para la consulta
						})
					});
					Ext.getCmp('btnGenerarArchivo').enable();
					Ext.getCmp('btnGenerarPDF').enable();
					Ext.getCmp('btnBajarPDF').hide();
					Ext.getCmp('btnBajarArchivo').hide();
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href='15admpymeparam04ext.jsp';
				}
			}
		]
	});

//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	});

	
//-------------------------------- ----------------- -----------------------------------
	
	catalogoTasasData.load();

});