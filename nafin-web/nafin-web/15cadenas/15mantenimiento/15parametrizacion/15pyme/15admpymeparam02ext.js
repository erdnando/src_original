Ext.onReady(function() {
//----------------------------------HANDLERS------------------------------------
	var procesarSuccessFailureGenerarPDF = function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//success == true si la petición de Ajax fue exitosa.
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarPDF.setHandler(function (boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
			});
		}else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
			}
		}
//------------------------------------STORE-------------------------------------
	var consultaData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '15admpymeparam02ext.data.jsp',
		baseParams : {
			informacion : 'Consulta'
		},
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields:[
				{name: 'RAZON_SOCIAL'},
				{name: 'CG_RAZON_SOCIAL'},
				{name: 'CD_NOMBRE'},
				{name: 'CG_BANCO'},
				{name: 'CG_NUMERO_CUENTA'},
				{name: 'CG_SUCURSAL'},
				{name: 'CS_VOBO_IF'}
			]
		}),
		groupField: 'RAZON_SOCIAL',
		sortInfo:{field: 'RAZON_SOCIAL',direction:"ASC"},
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			exception: {
				fn: function(proxy,type,action,optionRequest,response,args)	{
					NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
				}
			}
		}
	});
//------------------------------------------------------------------------------
var grid = new Ext.grid.GridPanel({
		store:consultaData,
		hidden: false,
		columns: [
				{
				header: 'EPO',
				tooltip: 'EPO',
				sortable: true,
				dataIndex: 'RAZON_SOCIAL',
				hidden: true
				},
				{
				header: 'Institución Financiera',
				tooltip: 'Institución Financiera',
				sortable: true,
				dataIndex: 'CG_RAZON_SOCIAL',
				resiazable: true,
				width: 300
				},
				{
				header: 'Moneda',
				tooltip: 'Moneda',
				sortable: true,
				dataIndex: 'CD_NOMBRE',
				resiazable: true,
				align: 'center',
				width: 200
				
				},
				{
				header: 'Banco de Servicio',
				tooltip: 'Banco de Servicio',
				sortable: true,
				dataIndex: 'CG_BANCO',
				resiazable: true,
				width: 200
				},
				{
				header: 'No. de Cuenta',
				tooltip: 'No. de Cuenta',
				sortable: true,
				dataIndex: 'CG_NUMERO_CUENTA',
				width: 160,
				align: 'center'
				},
				{
				header: 'Sucursal',
				tooltip: 'Sucursal',
				sortable: true,
				dataIndex: 'CG_SUCURSAL',
				width: 100,
				align: 'center'
				},
				{
				header: 'Autorización',
				tooltip: 'Autorización',
				sortable: true,
				dataIndex: 'CS_VOBO_IF',
				width: 100,
				align: 'center'
				}],	
			view: new Ext.grid.GroupingView({
					forceFit: true,
					groupTextTpl: '{text}'
			}),
			margins: '20 0 0 0',
			stripeRows: true,
			loadMask: true,
			frame: true,
			style: 'margin:0 auto',
			width: 885,
			height: 400,
			title: 'Cuentas Bancarias',
			bbar: {
				items: [
					'->',
					'-',
					{
						xtype: 'button',
						text: 'Generar PDF',
						id: 'btnGenerarPDF',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
							Ext.Ajax.request({
								url: '15admpymeparam02ext.data.jsp',
								params: {
									informacion: 'ArchivoPDF'
								},
								callback: procesarSuccessFailureGenerarPDF
							});
						}
					},
					{
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDF',
						hidden: true
					},
					'-'
				]
			}
		});
//----------------------------------PRINCIPAL-----------------------------------
// La aplicación se mostrará en un div de cierto ancho, para lo cual se define el siguiente contenedor.
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			grid
		]
	});
});