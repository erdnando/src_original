<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar = "";

com.netro.cadenas.ConsSeleccionIF paginador = new com.netro.cadenas.ConsSeleccionIF();
paginador.setClavePyme(iNoCliente);
CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );

try{
	Registros reg = queryHelper.doSearch();
	infoRegresar =
		"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	}catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
if (informacion.equals("ArchivoPDF")) {
	try
	{
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean (true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	}catch(Throwable e){
		throw new AppException("Error al generar el archivo PDF", e);
	}
}
%>
<%=infoRegresar%>