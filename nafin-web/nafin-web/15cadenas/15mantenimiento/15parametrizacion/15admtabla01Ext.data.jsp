<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.sql.*,java.util.*,java.text.*,netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		java.lang.String,
		com.netro.cadenas.*,
		com.netro.exception.*,
		com.netro.procesos.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.descuento.*,
		org.apache.commons.logging.Log,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoCadAfi,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

ITablasDinamicas tablasDinamicas = ServiceLocator.getInstance().lookup("TablasDinamicasEJB",ITablasDinamicas.class);
Tablaimgtxt timg = new Tablaimgtxt();	//Clase en la que se encuentran los metodos que generan los querys que usan la gen. y mod. de tablas

if(informacion.equals("catalogoTipoPublicacion") ) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_tipo_publicacion");
	cat.setCampoClave("ic_tipo_publicacion");
	cat.setCampoDescripcion("cd_tipo_publicacion");
	cat.setOrden("ic_tipo_publicacion");
	
	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoCadenas")){
	List lista = new ArrayList();
	lista.add("S");
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_epo");
	cat.setCampoClave("IC_EPO");
	cat.setCampoDescripcion("CG_RAZON_SOCIAL");
	cat.setCampoLlave("cs_habilitado");
	cat.setValoresCondicionIn(lista);
	cat.setOrden("CG_RAZON_SOCIAL");
	
	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("catalogoMulti")){
	String cveCad = (request.getParameter("cveCad")==null) ? "" : request.getParameter("cveCad");
	String tipoAfi = (request.getParameter("tipoAfi")==null) ? "" : request.getParameter("tipoAfi");
	CatalogoCadAfi cat = new CatalogoCadAfi();
	cat.setCveCad(cveCad);
	cat.setTipoAfi(tipoAfi);
	cat.setCampoClave("cn.ic_nafin_electronico");
	cat.setCampoDescripcion("t1.CG_RAZON_SOCIAL||' '||t1.CG_APMAT||' '||t1.CG_APPAT||' '||t1.CG_NOMBRE");
	cat.setOrden("DESCRIPCION");
	
	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("ConsulTablas")){
	String cveCad = (request.getParameter("cveCad")==null) ? "" : request.getParameter("cveCad");
			
	Vector mVecListaPub = new Vector();
	mVecListaPub = tablasDinamicas.obtenListaPublicaciones(cveCad);
	
	JSONArray a= new JSONArray();
	for(int i = 0;i < mVecListaPub.size(); i++){
		List aux =(List)mVecListaPub.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NUMPUB",aux.get(0)==null?"":aux.get(0).toString());
		jo.put("NOMPUB",aux.get(1)==null?"":aux.get(1).toString());
		jo.put("FECINI",aux.get(2)==null?"":aux.get(2).toString());
		jo.put("FECFIN",aux.get(3)==null?"":aux.get(3).toString());
		jo.put("NUMCOL",aux.get(4)==null?"":aux.get(4).toString());
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("ConsultaImagenes")){
	String 	cadena = (request.getParameter("cadena")==null) ? "" : request.getParameter("cadena");
	ConImagen cm = new ConImagen();
	ArrayList list = new ArrayList();
	list = cm.conImg(cadena);
	
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size(); i++){
		Vector aux=(Vector)list.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NUMPUBIM",aux.get(0));
		jo.put("NOMPUBIM",aux.get(1));
		jo.put("FECINIIM",aux.get(2));
		jo.put("FECFINIM",aux.get(3));
		jo.put("IMAGEN",aux.get(4));
		jo.put("ARCHIVO",aux.get(5));
		jo.put("CONTENIDO",aux.get(6));
		jo.put("CONTENIDOALL", aux.get(7));
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("ConsulTabIndiv")){
	String 	msNumPublicacion = request.getParameter("nPublicacion"),
		msNombreTabla    = request.getParameter("nom_tabla");
		JSONObject jsonObj = new JSONObject();
		
		if (msNumPublicacion != ""){
			if ( tablasDinamicas.existeTabla( msNumPublicacion, "E" ) )	{
				String 	  msQryEstruc =  "SELECT cg_nombre_atrib, cg_tipo_dato, ig_long_atrib, cs_total, cs_tipo_atrib, " +
								 "	 cs_total, ig_ordenamiento " +
								 "FROM com_tabla_pub_atrib " +
								 "WHERE ic_publicacion = " + msNumPublicacion + " " +
								 "ORDER BY cs_tipo_atrib DESC, ic_atributo";
				ResultSet mCurGen;
				
				String nomCabecera = "";
				List li = new ArrayList();
				int y = 2;
				AccesoDB  lobjConexion = new AccesoDB();
				try{
						lobjConexion.conexionDB();
						mCurGen = lobjConexion.queryDB( msQryEstruc );
						
						while(mCurGen.next()) {
							nomCabecera = mCurGen.getString("cg_nombre_atrib");
							li.add(nomCabecera);
						}
						
				}catch (Exception e){
					e.printStackTrace();
					throw new Exception("SIST0001");
				}finally{
					if(lobjConexion.hayConexionAbierta()) lobjConexion.cierraConexionDB();
					System.out.println("Salida Consulta tabla individualmente(S)");
				}
				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("li", li);
				jsonObj.put("msNombreTabla",msNombreTabla);
			}
			else{
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("publicacion", msNumPublicacion);
			}
		}
		
		infoRegresar = jsonObj.toString();

}else if(informacion.equals("ModificarTabIndiv")){
		String 	msNumPublicacion = request.getParameter("nPublicacion");
		String   msNombreTabla    = request.getParameter("nom_tabla");
		StringBuffer  columnasStore = new StringBuffer();
		StringBuffer  columnasRecords = new StringBuffer();
		JSONObject jsonObj = new JSONObject();
		String lsQryNombres = "SELECT cg_nombre_atrib , ig_long_atrib AS longitud, ig_long_atrib_d AS long_d, cs_total, ig_ordenamiento " +
						   "FROM com_tabla_pub_atrib " +
						  "	WHERE cs_tipo_atrib = 'F' AND ic_publicacion = " + msNumPublicacion + " ORDER BY ic_atributo " ;
						  
		String lsQryTiposD  = "SELECT cg_tipo_dato FROM com_tabla_pub_atrib WHERE cs_tipo_atrib = 'F' AND ic_publicacion = " + msNumPublicacion + " ORDER BY ic_atributo";
		
		String msNombrePub  = tablasDinamicas.obtenNombrePub(Integer.parseInt(msNumPublicacion));
						  
		List liNom = new ArrayList();		
		List lvOrdenCampos = new ArrayList();	
		List liTipo = new ArrayList();
		List lvLongitudes = new ArrayList();
		int tot = 0;
		AccesoDB  lobjConexion = new AccesoDB();
		try{
			lobjConexion.conexionDB();
			ResultSet lCursor = lobjConexion.queryDB( lsQryNombres );
			columnasStore.append(" [  {	name: 'PROVDIST', mapping : 'PROVDIST'	 },");	
			while ( lCursor.next() )	{ //Se obtiene la lista de nombres para la cabecera del grid
				liNom.add(lCursor.getString("cg_nombre_atrib"));
				lvOrdenCampos.add(lCursor.getString("ig_ordenamiento"));
				lvLongitudes.add(lCursor.getString("longitud"));
				columnasStore.append(" {  name: '"+lCursor.getString("cg_nombre_atrib")+"' , mapping : '"+lCursor.getString("cg_nombre_atrib")+"' },");
			}
			System.out.println("\n+++Longitudes de campos: "+lvLongitudes); /////Quitar estas impresiones
			columnasStore.deleteCharAt(columnasStore.length()-1);
			columnasStore.append(" ] ");	
			
			lCursor = lobjConexion.queryDB( lsQryTiposD );
			while ( lCursor.next() )	{ //Se obtiene los tipos de datos de cada columna del grid
				liTipo.add(lCursor.getString("cg_tipo_dato"));
			}
			
			/***********************************************************************************************
			*	ARMANDO SELECT DE TABLA DINAMICA
			***********************************************************************************************/
			String msOrden[]    = {"", "", ""};
			String lsNombreCampoF = "";
			String lsQryValores = "";
			String lsQryTotales = "";
				lsQryValores = "SELECT id, campo0,  ";
				lsQryTotales = "SELECT '0' AS vacio";
				
				for (int i=0; i<liNom.size(); i++)	{
					lsNombreCampoF = "campo" + String.valueOf(i+1);
					if ( liTipo.get(i).equals("fecha") )	{
						lsQryValores += "TO_CHAR(" + lsNombreCampoF + ", 'dd/mm/yyyy') as campo"+String.valueOf(i+1);
						lsQryTotales += ", COUNT(" + lsNombreCampoF + ")";
					}
					else if( liTipo.get(i).equals("hora") )	{
						lsQryValores += "TO_CHAR(" + lsNombreCampoF + ", 'hh24:mi') as campo"+String.valueOf(i+1);
						lsQryTotales += ", COUNT(" + lsNombreCampoF + ")";
					}
					else if( liTipo.get(i).equals("numerico") )	{
						lsQryValores += lsNombreCampoF;
						lsQryTotales += ", NVL( SUM(" + lsNombreCampoF + "), 0) ";
					}
					else {
						lsQryValores += lsNombreCampoF;
						lsQryTotales += ", COUNT(" + lsNombreCampoF + ")";
					}
					
					if ( i < liNom.size()-1 )	lsQryValores += ", ";
					
					/**********
					* DETERMINANDO ORDENAMIENTO
					**********/
					int liPosOrden = Integer.parseInt( (String) lvOrdenCampos.get(i) );
					if ( liPosOrden > 0 )
						msOrden[liPosOrden-1] = lsNombreCampoF;
				}
				
				lsQryValores += " FROM tabla_" + msNumPublicacion + " ORDER BY campo0";
				lsQryTotales += " FROM tabla_" + msNumPublicacion;
				
				for (int i=0; i<=2; i++)	{
					if ( !msOrden[i].equals("") )
						lsQryValores += ", " + msOrden[i];
				}
				
			/***********************************************************************************************
			*	Ejecutando el Query para obtener los valores que se dibujaran en el grid
			***********************************************************************************************/
			lCursor = lobjConexion.queryDB( lsQryValores );
			columnasRecords.append(" [ ");
			while(lCursor.next()){
				tot++;
				columnasRecords.append("{ NOPUBLIC : '"+msNumPublicacion+"', ");
				columnasRecords.append(" ID : "+ lCursor.getString("id") +", ");
				columnasRecords.append(" EDIT : "+"'N'"+", ");
				columnasRecords.append(" PROVDIST : '"+lCursor.getString("campo0")+"', ");
				for(int i=0; i<liNom.size(); i++){
					columnasRecords.append(liNom.get(i) +" : '"+lCursor.getString("campo"+String.valueOf(i+1))+"', ");
				}
				columnasRecords.deleteCharAt(columnasRecords.length()-2);
				columnasRecords.append(" },");  
			}
			//Para generar una fila en blanco
			for (int j=0 ; j<1; j++){
				columnasRecords.append("{ NOPUBLIC : '"+ msNumPublicacion +"', ");
				columnasRecords.append(" ID : "+"''" +", ");
				columnasRecords.append(" EDIT : "+"'S'"+", ");
				columnasRecords.append(" PROVDIST : "+"''"+", ");
				for(int i=0; i<liNom.size(); i++){
					columnasRecords.append(liNom.get(i) +" : "+"''"+", ");
				}
				columnasRecords.deleteCharAt(columnasRecords.length()-2);
			}
			columnasRecords.append(" },");
			
			columnasRecords.deleteCharAt(columnasRecords.length()-1);
			columnasRecords.append(" ] ");
			
		}catch (Exception e){
					e.printStackTrace();
					throw new Exception("SIST0001");
		}finally{
					if(lobjConexion.hayConexionAbierta()) lobjConexion.cierraConexionDB();
					System.out.println("Salida Modifica tabla(S)");
		}
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("msNombreTabla",msNombreTabla);
		jsonObj.put("columnasRecords", "{registros : "+columnasRecords.toString()+",total : "+tot+"}");
		jsonObj.put("liNom", liNom);
		jsonObj.put("liTipo", liTipo);
		jsonObj.put("liLong", lvLongitudes);
		jsonObj.put("total", String.valueOf(tot));
		jsonObj.put("columnasStore", columnasStore.toString());
				
		infoRegresar = jsonObj.toString();
		
}else if(informacion.equals("ObtenTablaDetalle")){
	String campoFijo = (request.getParameter("nom_fijo") == null) ? "" :  request.getParameter("nom_fijo");
	String 	miNumPublicacion = request.getParameter("nPublicacion");
	String msNumDetalle     = (request.getParameter("num_detalle") == null) ? "" :  request.getParameter("num_detalle");
	JSONObject jsonObj = new JSONObject();
	StringBuffer  columnasRegistros = new StringBuffer();
	List listNom = new ArrayList();
	int total = 0;
	List lvOrdenCamposD = new ArrayList();	
	List lvValoresCampo = new ArrayList();
	List lvTipos = new ArrayList();
	List lvLongitudes = new ArrayList();

	boolean mbExisEditable = false;
	String lsQryNombres = "SELECT (SELECT cg_nombre_atrib FROM com_tabla_pub_atrib " +
							 	"		WHERE ic_publicacion = "+miNumPublicacion+" AND ic_atributo = 0) AS nom_col_fij, "+
								"	cg_nombre_atrib, ig_long_atrib AS longitud, ig_long_atrib_d AS long_d, cg_tipo_dato, " +
								"	cs_total, ig_ordenamiento " +
							   "FROM com_tabla_pub_atrib_d " +
							   "WHERE ic_publicacion = " + miNumPublicacion + " " +
							   "ORDER BY ic_atributo_d";
								
	String lsQryTiposD  = "SELECT cg_tipo_dato FROM com_tabla_pub_atrib_d WHERE ic_publicacion = " + miNumPublicacion + " " +
							   "ORDER BY ic_atributo_d";
	
	AccesoDB  lobjConexion = new AccesoDB();
	try{	
	lobjConexion.conexionDB();
	ResultSet lCursor = lobjConexion.queryDB( lsQryNombres );
	if ( lCursor.next() )	{
				do	{
						mbExisEditable = true;
						listNom.add( lCursor.getString("cg_nombre_atrib") );
						lvValoresCampo.add( lCursor.getString("cg_nombre_atrib") );
						lvLongitudes.add( lCursor.getString("longitud") );
						lvOrdenCamposD.add( lCursor.getString("ig_ordenamiento") );
					}	while ( lCursor.next() );
	}
	lCursor.close();
	
	String msOrdenDet[] = {"", "", ""};
	String lsNombreCampoD = "";
	String lsQryValores = "";
	String lsQryTotDet = "";
	if (mbExisEditable)	{
	
		lCursor = lobjConexion.queryDB( lsQryTiposD );
		while ( lCursor.next() )	{
						lvTipos.add( lCursor.getString("cg_tipo_dato") );
		}
	
	/***********************************************************************************************
	*	ARMANDO SELECT DE TABLA DINAMICA DE DETALLE
	***********************************************************************************************/ 
				
				lsQryValores = "SELECT f.campo0, f.campo1, d.id, d.id_d,  ";
				lsQryTotDet  = "SELECT '0' AS vacio ";
						
				for (int i=0; i<lvValoresCampo.size(); i++)	{
					lsNombreCampoD = "d.campod" + i;
							
					if ( lvTipos.get(i).equals("fecha") )	{
						lsQryValores += "TO_CHAR(" + lsNombreCampoD + ", 'dd/mm/yyyy') ";
						lsQryTotDet  += ", COUNT(" + lsNombreCampoD + ")";
					}
					else if( lvTipos.get(i).equals("hora") )	{
						lsQryValores += "TO_CHAR(" + lsNombreCampoD + ", 'hh24:mi') ";
						lsQryTotDet  += ", COUNT(" + lsNombreCampoD + ")";
					}
					else if( lvTipos.get(i).equals("numerico") )	{
						lsQryValores += lsNombreCampoD;
						lsQryTotDet  += ", NVL( SUM(" + lsNombreCampoD + "), 0)";
					}
					else {
						lsQryValores += lsNombreCampoD;
						lsQryTotDet  += ", COUNT(" + lsNombreCampoD + ")";
					}
					
					int liPosOrden = Integer.parseInt(  (String) lvOrdenCamposD.get(i)  );
					if ( liPosOrden > 0 )
						msOrdenDet[ liPosOrden-1 ] = lsNombreCampoD;
							
					if ( i < lvValoresCampo.size()-1 )	lsQryValores += ", ";
				}
				lsQryValores += " FROM tabla_d_" + miNumPublicacion + " d, " +
										"	   tabla_" + miNumPublicacion + " f " +
										" WHERE d.id = " + msNumDetalle +
										"		AND d.id = f.id " +
										" ORDER BY f.campo0";
						
				for (int i=0; i<=2; i++)	{
					if ( !msOrdenDet[i].equals("") )
						lsQryValores += ", " + msOrdenDet[i];
				}
						
				lsQryTotDet += " FROM tabla_d_" + miNumPublicacion + " d, " +
										"	   tabla_" + miNumPublicacion + " f " +
										" WHERE d.id = " + msNumDetalle +
										"		AND d.id = f.id ";
				
				ResultSet lCursor1 = lobjConexion.queryDB( lsQryValores );
				columnasRegistros.append("[ ");
				while(lCursor1.next()){
					total++;
					columnasRegistros.append("{ ");
					columnasRegistros.append(" PROVDISTD : '"+ lCursor1.getString(1) +"', ");
					columnasRegistros.append(" IDDET : '"+ lCursor1.getString("id_d") +"', ");
					columnasRegistros.append(" IDCAMPF : '"+ msNumDetalle +"', ");
					columnasRegistros.append(" CAMPFIJO : '"+ lCursor1.getString(2) +"', ");
					columnasRegistros.append(" EDITD : 'N', ");
					for(int i=5; i<=lvValoresCampo.size()+4 ;i++){
						columnasRegistros.append(lvValoresCampo.get(i-5)+": '"+(lCursor1.getString(i)==null?"":lCursor1.getString(i))+"', ");
					}
					columnasRegistros.append(" },");
				}
				
				for(int j=0; j<1; j++){
					columnasRegistros.append("{ ");
					columnasRegistros.append(" PROVDISTD : '', ");
					columnasRegistros.append(" IDDET : '', ");
					columnasRegistros.append(" IDCAMPF : '"+ msNumDetalle +"', ");
					columnasRegistros.append(" CAMPFIJO : '', ");
					columnasRegistros.append(" EDITD : 'S', ");
					for(int i=0;i<lvValoresCampo.size() ;i++){
						columnasRegistros.append(lvValoresCampo.get(i)+": '',");
					}
					columnasRegistros.append(" },");
				}
				columnasRegistros.deleteCharAt(columnasRegistros.length()-1);
				columnasRegistros.append(" ] ");
		}
				
	}catch (Exception e){
					e.printStackTrace();
					throw new Exception("SIST0001");
	}finally{
					if(lobjConexion.hayConexionAbierta()) lobjConexion.cierraConexionDB();
					System.out.println("Salida obten tabla detalle(S)");
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("listNom", listNom);
	jsonObj.put("lvTipos", lvTipos);
	jsonObj.put("campoFijo", campoFijo);
	jsonObj.put("longDet", lvLongitudes);
	jsonObj.put("columnasRegistros", "{registros : "+columnasRegistros.toString()+",total : "+total+"}");
	jsonObj.put("total", String.valueOf(total));
	infoRegresar = jsonObj.toString();	
	
}else if(informacion.equals("EliminaDatosGridAll")){
	JSONObject jsonObj = new JSONObject();
	String msCaracCampo = request.getParameter("caracter_campo"),
		    msNumPublica = request.getParameter("num_pub"),
		    msNumIdCampo = request.getParameter("num_campo_fijo");
		
	if ( msCaracCampo.equals("F") )	msNumIdCampo = "";

	tablasDinamicas.borraRegistros( msNumPublica, msCaracCampo, msNumIdCampo );
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("num_pub", msNumPublica);
	infoRegresar = jsonObj.toString();	
	
}else if(informacion.equals("AgregaDatFijos") || informacion.equals("SalvarDatFijos") || informacion.equals("EliminarDatFijos")){
	String num_pub 	 = (request.getParameter("num_pub") == null) ? "0" : request.getParameter("num_pub");
	String tipo_campo[] = request.getParameterValues("tipo_campo");
	String id_campo     = request.getParameter("id_campo");
	String 	msNumEPO     = request.getParameter("numEpo");
	String campo[] 	 = request.getParameterValues("campo");
	JSONObject jsonObj = new JSONObject();
	Vector mVQuerys  = new Vector();
	String msQryMaxId   = "";
	String msQry  = "";
	
	if ( campo != null )	 {
	
		if(informacion.equals("AgregaDatFijos")){
			if ( !tablasDinamicas.checaPseudollave(num_pub, campo[0], campo[1]) )	{
				session.setAttribute("sesMensaje", "No se pueden repetir los campos: ( " +campo[0]+ " y " + campo[1] + ")");
			}
			else	{
				msQryMaxId  = "SELECT NVL(MAX(id)+1, 0) AS num_id FROM tabla_" + num_pub;
				msQry = "INSERT INTO tabla_" + num_pub + "( id, ";
				for (int i=0; i<campo.length; i++)	{
					msQry += " campo"+i;
					if (i < campo.length-1 )	msQry += ", ";
				}
				msQry += ")";
				msQry += " VALUES  ( ("+msQryMaxId+"), ";
				
				String lsProveedor = "";
				
				for (int i=0; i<campo.length; i++)	{
					
					if ( i == 0 )	lsProveedor = campo[i];
					
					if (tipo_campo[i].equals("alfanumerico") || tipo_campo[i].equals("seleccion"))
						msQry += "'" + campo[i] + "'";
					else if (tipo_campo[i].equals("numerico") )
						msQry += campo[i];
					else if (tipo_campo[i].equals("fecha") )
						msQry += "TO_DATE('" + campo[i] + "', 'dd/mm/yyyy')";
					else if (tipo_campo[i].equals("hora") )
						msQry += "TO_DATE('" + campo[i] + "', 'hh24:mi')";
						
					if (i < campo.length-1 )	msQry += ", ";
				}
				
				if ( !tablasDinamicas.hayUsuariosPub(msNumEPO, num_pub, lsProveedor) )	{
					tablasDinamicas.insertaUsuariosAPublicacion(msNumEPO, num_pub, lsProveedor);
				}
				
				msQry += ")";
				mVQuerys.addElement( msQry );
			}
		}else if(informacion.equals("SalvarDatFijos")){
			
			String lsNombreCampoF = "",
				   lsProveedor    = "";
			msQry = "UPDATE tabla_" + num_pub + " SET  campo0 = ";
			
			for (int i=0; i<campo.length; i++)	{
				
				if ( i == 0 )	lsProveedor = campo[i];
				
				lsNombreCampoF = "campo" + String.valueOf(i);
				if ( i>0 )	msQry += lsNombreCampoF + " = ";
				
				if (tipo_campo[i].equals("alfanumerico") || tipo_campo[i].equals("seleccion"))
					msQry += "'" + campo[i] + "'";
				else if (tipo_campo[i].equals("numerico") )
					msQry += campo[i];
				else if (tipo_campo[i].equals("fecha") )
					msQry += "TO_DATE('" + campo[i] + "', 'dd/mm/yyyy')";
				else if (tipo_campo[i].equals("hora") )
					msQry += "TO_DATE('" + campo[i] + "', 'hh24:mi')";
					
				if (i < campo.length-1 )	msQry += ", ";
			}
			
			if ( !tablasDinamicas.hayUsuariosPub(msNumEPO, num_pub, lsProveedor) )	{
				tablasDinamicas.insertaUsuariosAPublicacion(msNumEPO, num_pub, lsProveedor);
			}
			msQry += " WHERE id = " + id_campo;
			mVQuerys.addElement( msQry );
			
		}else if(informacion.equals("EliminarDatFijos")){
			if ( tablasDinamicas.existeTabla( num_pub, "D" ) )	{
				msQry = "DELETE FROM tabla_d_"+num_pub+" WHERE id = "+id_campo;
				mVQuerys.addElement( msQry );
			}
			
			if ( tablasDinamicas.existeTabla( num_pub, "E" ) )	{
				msQry = "DELETE FROM tabla_edit_"+num_pub+" WHERE id = "+id_campo;
				mVQuerys.addElement( msQry );
			}
			
			msQry = "DELETE FROM tabla_"+num_pub+" WHERE id = "+id_campo;
			mVQuerys.addElement( msQry );
		}
		/****************************************************
		*	VERIFICANDO ERROR Y REGRESANDO 
		******************************************************/
		
		if ( mVQuerys.size() > 0 ){
			if ( tablasDinamicas.modificaPublicaciones(mVQuerys, Integer.parseInt(num_pub)) )	{
				//response.sendRedirect("15admtabla04.jsp?parNumPub=" + num_pub + "&parEPO=" + msNumEPO);
			}
			else	System.out.println(" FALLOOOOO ");
		}
		else
			System.out.println(" nada");
			//response.sendRedirect("15admtabla04.jsp?parNumPub=" + num_pub + "&parEPO=" + msNumEPO);
		
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("num_pub",num_pub);
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("AgregaDatDetalle") || informacion.equals("SalvarDatDetalle") || informacion.equals("EliminarDatDetalle")){
	String num_pub 	 = (request.getParameter("num_pub") == null) ? "0" : request.getParameter("num_pub");
	String tipo_campo_det[] = request.getParameterValues("tipo_campo_det");
	String num_campo_fijo   = request.getParameter("num_campo_fijo");
	String id_relacion  = request.getParameter("num_detalle");
	String id_campo     = request.getParameter("id_campo");
	String nom_fijo	= "Ninguno";
	String campo_det[]	     = request.getParameterValues("campo_det");
	JSONObject jsonObj = new JSONObject();
	Vector mVQuerys  = new Vector();
	String msQryMaxId   = "";
	String msQry  = "";
	
	if ( campo_det != null )	 {
		if(informacion.equals("AgregaDatDetalle")){
			msQryMaxId  = "SELECT NVL(MAX(id_d)+1, 0) AS num_id FROM tabla_d_" + num_pub;

			
			msQry = "INSERT INTO tabla_d_" + num_pub + " (id, id_d, ";
			for (int i=0; i<campo_det.length; i++)	{
				msQry += " campod"+i;
				if (i < campo_det.length-1 )	msQry += ", ";
			}
			msQry += ") VALUES  ("+num_campo_fijo+", ("+msQryMaxId+"), ";
			
			for (int i=0; i<campo_det.length; i++)	{
				if (tipo_campo_det[i].equals("alfanumerico") || tipo_campo_det[i].equals("seleccion"))
					msQry += "'" + campo_det[i] + "'";
				else if (tipo_campo_det[i].equals("numerico") )
					msQry += campo_det[i];
				else if (tipo_campo_det[i].equals("fecha") )
					msQry += "TO_DATE('" + campo_det[i] + "', 'dd/mm/yyyy')";
				else if (tipo_campo_det[i].equals("hora") )
					msQry += "TO_DATE('" + campo_det[i] + "', 'hh24:mi')";
					
				if (i < campo_det.length-1 )	msQry += ", ";
			}
			
			msQry += ")";
			mVQuerys.addElement( msQry );
			
		}else if(informacion.equals("SalvarDatDetalle")){
			String lsNombreCampoD = "";
			
			msQry = "UPDATE tabla_d_" + num_pub + " SET ";
			
			for (int i=0; i<campo_det.length; i++)	{
				lsNombreCampoD = "campod" + i;
				msQry += lsNombreCampoD + " = ";
				
				if (tipo_campo_det[i].equals("alfanumerico") || tipo_campo_det[i].equals("seleccion"))
					msQry += "'" + campo_det[i] + "'";
				else if (tipo_campo_det[i].equals("numerico") )
					msQry += campo_det[i];
				else if (tipo_campo_det[i].equals("fecha") )
					msQry += "TO_DATE('" + campo_det[i] + "', 'dd/mm/yyyy')";
				else if (tipo_campo_det[i].equals("hora") )
					msQry += "TO_DATE('" + campo_det[i] + "', 'hh24:mi')";
					
				if (i < campo_det.length-1 )	msQry += ", ";
			}
			
			msQry += " WHERE id_d = " + id_campo + " AND id = " + id_relacion;
			mVQuerys.addElement( msQry );
		}else if(informacion.equals("EliminarDatDetalle")){
			msQry = "DELETE FROM tabla_d_"+num_pub+" WHERE id_d = "+id_campo+" AND id = "+id_relacion;
			mVQuerys.addElement( msQry );
		}
		
		if ( tablasDinamicas.modificaPublicaciones(mVQuerys, Integer.parseInt(num_pub)) )
			System.out.println("Regreso exitoso");
		else	System.out.print(" FALLOOOOO ");
	}	
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("num_pub",num_pub);
	jsonObj.put("num_detalle",id_relacion );
	jsonObj.put("nom_fijo", nom_fijo);
	infoRegresar = jsonObj.toString();
	
}else if( informacion.equals("DescargaImagen") ){
		String cveEpo= (request.getParameter("cveEpo")==null) ? "" : request.getParameter("cveEpo");
		String nomImg= (request.getParameter("nomImg")==null) ? "" : request.getParameter("nomImg");
		
		String nombreImagen = "/nafin/00archivos/15cadenas/15archcadenas/pub_epo"+cveEpo+"/imagenes/"+nomImg;
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", nombreImagen);
		infoRegresar = jsonObj.toString();	
		
}else if( informacion.equals("DescargaArchivo") ){
		String cveEpo= (request.getParameter("cveEpo")==null) ? "" : request.getParameter("cveEpo");
		String nomArch= (request.getParameter("nomArch")==null) ? "" : request.getParameter("nomArch");
		
		String nombreArchivo = "/nafin/00archivos/15cadenas/15archcadenas/pub_epo"+cveEpo+"/archivos/"+nomArch;
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", nombreArchivo);
		infoRegresar = jsonObj.toString();		
		
}else if(informacion.equals("EliminarImgTxt")){
	String cveEpo= (request.getParameter("cveEpo")==null) ? "" : request.getParameter("cveEpo");
	String numPub= (request.getParameter("numPub")==null) ? "" : request.getParameter("numPub");
	String nomArch= (request.getParameter("nomArch").equals("")) ? "null" : request.getParameter("nomArch");
	String nomImg= (request.getParameter("nomImg")==null) ? "" : request.getParameter("nomImg");
	if(nomArch.equals(""))
		nomArch = "null";
	String arch_actual= "15archcadenas/pub_epo"+cveEpo+"/archivos/"+nomArch;
	String imgn_actual= "15archcadenas/pub_epo"+cveEpo+"/imagenes/"+nomImg;
	
	String ruta_actualarch = strDirectorioTemp+arch_actual;
	String ruta_actualimgn = strDirectorioTemp + imgn_actual;
	
	ConImagen cm = new ConImagen();
	cm.eliminaImgTxt(numPub, ruta_actualarch, ruta_actualimgn);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("EliminaTabla")){
	
	String cveEpo = (request.getParameter("cveEpo")==null) ? "" : request.getParameter("cveEpo");
	String numPub = (request.getParameter("numPub")==null) ? "" : request.getParameter("numPub");
	
	boolean mbExisteTabEdi = tablasDinamicas.existeTabla( numPub, "E" ),
				mbExisteTabDet = tablasDinamicas.existeTabla( numPub, "D" );
	
	tablasDinamicas.borraPublicacion( cveEpo, numPub, mbExisteTabEdi, mbExisteTabDet );
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("camposFijos")){ //Store para generar el grid de captura de campos fijos
	int num=0;
	int numFilas = Integer.parseInt((request.getParameter("numFijos")==null) ? "" : request.getParameter("numFijos"));
	ArrayList list = new ArrayList();
	Vector vDatos = null;
	
	numFilas = numFilas > 50? 50: numFilas;
	
	for (int j=0; j<numFilas; j++){
		vDatos = new Vector();
		vDatos.add(0,"");
		vDatos.add(1,"alfanumerico");
		vDatos.add(2,"");
		vDatos.add(3,"0");
		vDatos.add(4,"");
		list.add(vDatos);
	}
	
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size(); i++){
		Vector aux=(Vector)list.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOMCAMPOF",aux.get(0));
		jo.put("TIPODATF",aux.get(1));
		jo.put("LONGITUDF",aux.get(2));
		jo.put("ORDENF",aux.get(3));
		jo.put("TOTALESF",aux.get(4));
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("camposEditables")){ //Store para generar el grid de captura de campos editables
	int numFilas = Integer.parseInt((request.getParameter("numEdi")==null) ? "" : request.getParameter("numEdi"));
	ArrayList list = new ArrayList();
	Vector vDatos = null;
	numFilas = numFilas > 5? 5: numFilas;
	
	for (int j=0; j<numFilas; j++){
		vDatos = new Vector();
		vDatos.add(0,"");
		vDatos.add(1,"alfanumerico");
		vDatos.add(2,"");
		vDatos.add(3,"0");
		vDatos.add(4,"");
		list.add(vDatos);
	}
	
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size(); i++){
		Vector aux=(Vector)list.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOMCAMPOE",aux.get(0));
		jo.put("TIPODATE",aux.get(1));
		jo.put("LONGITUDE",aux.get(2));
		jo.put("ORDENE",aux.get(3));
		jo.put("TOTALESE",aux.get(4));
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("camposDetalle")){ //Store para generar el grid de captura de campos de detalle
	int numFilas = Integer.parseInt((request.getParameter("numDet")==null) ? "" : request.getParameter("numDet"));
	ArrayList list = new ArrayList();
	Vector vDatos = null;
	numFilas = numFilas > 10? 10: numFilas;
	
	for (int j=0; j<numFilas; j++){
		vDatos = new Vector();
		vDatos.add(0,"");
		vDatos.add(1,"alfanumerico");
		vDatos.add(2,"");
		vDatos.add(3,"0");
		vDatos.add(4,"");
		list.add(vDatos);
	}
	
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size(); i++){
		Vector aux=(Vector)list.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOMCAMPOD",aux.get(0));
		jo.put("TIPODATD",aux.get(1));
		jo.put("LONGITUDD",aux.get(2));
		jo.put("ORDEND",aux.get(3));
		jo.put("TOTALESD",aux.get(4));
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("camposFijosMod")){
	int numFilas = Integer.parseInt((request.getParameter("numFijosMod")==null) ? "" : request.getParameter("numFijosMod"));
	ArrayList list = new ArrayList();
	Vector vDatos = null;
	
	for (int j=0; j<numFilas; j++){
		vDatos = new Vector();
		vDatos.add(0,"");
		vDatos.add(1,"alfanumerico");
		vDatos.add(2,"");
		vDatos.add(3,"1");
		vDatos.add(4,"");
		list.add(vDatos);
	}
	
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size(); i++){
		Vector aux=(Vector)list.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOMCAMPOF",aux.get(0));
		jo.put("TIPODATF",aux.get(1));
		jo.put("LONGITUDF",aux.get(2));
		jo.put("ORDENF",aux.get(3));
		jo.put("TOTALESF",aux.get(4));
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("camposEditablesMod")){
	int numFilas = Integer.parseInt((request.getParameter("numEdiMod")==null) ? "" : request.getParameter("numEdiMod"));
	int numPub = Integer.parseInt((request.getParameter("numPubTab")==null) ? "" : request.getParameter("numPubTab"));
	int miNumCamposGrabadosEdit = timg.obtenNumeroCampos(numPub, "E");
	int maxnum = 5 - miNumCamposGrabadosEdit;
	ArrayList list = new ArrayList();
	Vector vDatos = null;
	
	if(numFilas>maxnum) numFilas = maxnum;
	
	for (int j=0; j<numFilas; j++){
		vDatos = new Vector();
		vDatos.add(0,"");
		vDatos.add(1,"alfanumerico");
		vDatos.add(2,"");
		vDatos.add(3,"1");
		vDatos.add(4,"");
		list.add(vDatos);
	}
	
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size(); i++){
		Vector aux=(Vector)list.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOMCAMPOE",aux.get(0));
		jo.put("TIPODATE",aux.get(1));
		jo.put("LONGITUDE",aux.get(2));
		jo.put("ORDENE",aux.get(3));
		jo.put("TOTALESE",aux.get(4));
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true));
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	jsonObj.put("maximo", new Boolean(true));
	
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("camposDetalleMod")){
	int numFilas = Integer.parseInt((request.getParameter("numDetMod")==null) ? "" : request.getParameter("numDetMod"));
	int numPub = Integer.parseInt((request.getParameter("numPubTab")==null) ? "" : request.getParameter("numPubTab"));
	int miNumCamposGrabadosDet = timg.obtenNumeroCampos(numPub, "D");
	int maxnum = 10 - miNumCamposGrabadosDet;
	ArrayList list = new ArrayList();
	Vector vDatos = null;
	
	if(numFilas>maxnum) numFilas = maxnum;
	
	for (int j=0; j<numFilas; j++){
		vDatos = new Vector();
		vDatos.add(0,"");
		vDatos.add(1,"alfanumerico");
		vDatos.add(2,"");
		vDatos.add(3,"1");
		vDatos.add(4,"");
		list.add(vDatos);
	}
	
	JSONArray a= new JSONArray();
	for(int i=0;i<list.size(); i++){
		Vector aux=(Vector)list.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOMCAMPOD",aux.get(0));
		jo.put("TIPODATD",aux.get(1));
		jo.put("LONGITUDD",aux.get(2));
		jo.put("ORDEND",aux.get(3));
		jo.put("TOTALESD",aux.get(4));
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("validaNombre")){
	String  cadena         = (request.getParameter("cadena")    == null) ? "" : request.getParameter("cadena"),
			  crear         = (request.getParameter("crear")    == null) ? "" : request.getParameter("crear"),
			  nom_tabla      = (request.getParameter("nom_tabla")   == null) ? ""   : request.getParameter("nom_tabla");
	boolean rep = timg.tablaRepetida(nom_tabla, cadena);
	
	//if(!crear.equals("")){
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(rep));
		jsonObj.put("nombre", nom_tabla);
		infoRegresar = jsonObj.toString();
	//}

}else if(informacion.equals("crearTabla")){	//Se realiza la creacion de las tablas
	System.out.println("***SE CREARAN LAS TABLAS DINAMICAS***");
	
	String  cadena         = (request.getParameter("cadena")    == null) ? "" : request.getParameter("cadena"),
		copia			= (request.getParameter("copia")    == null) ? "" : request.getParameter("copia"),
		accion         = (request.getParameter("accion")     == null) ? ""   : request.getParameter("accion"),
		numpub			= (request.getParameter("numpub")     == null) ? ""   : request.getParameter("numpub"),
		nom_tabla      = (request.getParameter("nom_tabla")   == null) ? ""   : request.getParameter("nom_tabla"),
		fec_ini        = (request.getParameter("fec_ini")     == null) ? ""   : request.getParameter("fec_ini"),
		fec_fin        = (request.getParameter("fec_fin")     == null) ? ""   : request.getParameter("fec_fin"),
		Usr        = (request.getParameter("Usr")     == null) ? ""   : request.getParameter("Usr"),
		tipo_pub       = "1";
		
		Usr = Usr.replaceAll(",","|");	
		
		Vector fec = new Vector();
		String usrs = "";
		
		if(!copia.equals("")){
			System.out.println("Entre a obtener datos para copiar tabla");
			fec = timg.traeFechas(numpub);
			usrs = timg.traeUsuarios(numpub);
			fec_ini = fec.get(0).toString();
			fec_fin = fec.get(1).toString();
			Usr = usrs;
		}
	
	//Datos para campos fijos
	String nombre_campo[]    = request.getParameterValues("arrNomF"),
		tipo_campo[]      = request.getParameterValues("arrTipoF"),
		long_campo_Fijo[] = request.getParameterValues("arrLongF"),
		orden_campo[]     = request.getParameterValues("arrOrdenF"),
		totales_campo[]   = request.getParameterValues("arrTotF");
		
		if(nombre_campo.length==1){
			if(nombre_campo[0].equals(""))
				nombre_campo= null;
		}
		
		if(totales_campo.length==1){
			if(totales_campo[0].equals(""))
				totales_campo= null;
		}

	//Datos para campos editables
	String nombre_campo_edi[]    = request.getParameterValues("arrNomE"),
		tipo_campo_edi[]      = request.getParameterValues("arrTipoE"),
		long_campo_Editable[] = request.getParameterValues("arrLongE"),
		orden_campo_edi[]     = request.getParameterValues("arrOrdenE"),
		totales_campo_edi[]   = request.getParameterValues("arrTotE");
		
		if(nombre_campo_edi.length==1){
			if(nombre_campo_edi[0].equals(""))
				nombre_campo_edi= null;
		}
		
		if(totales_campo_edi.length==1){
			if(totales_campo_edi[0].equals(""))
				totales_campo_edi= null;
		}
		
	//Datos para campos detalle
	String nombre_campo_det[]   = request.getParameterValues("arrNomD"),
		tipo_campo_det[]     = request.getParameterValues("arrTipoD"),
		long_campo_Detalle[] = request.getParameterValues("arrLongD"),
		orden_campo_det[]    = request.getParameterValues("arrOrdenD"),
		totales_campo_det[]  = request.getParameterValues("arrTotD");
		
		if(nombre_campo_det.length==1){
			if(nombre_campo_det[0].equals(""))
				nombre_campo_det = null;
		}
		
		if(totales_campo_det.length==1){
			if(totales_campo_det[0].equals(""))
				totales_campo_det = null;
		}

	int miNumCampos      = 0,
		miNumPublicacion = (request.getParameter("publicacion") == null) ? 0 : Integer.parseInt(request.getParameter("publicacion"));
	boolean lbNoError 		 = true,
		mbExisteTabEdit  = tablasDinamicas.existeTabla( String.valueOf(miNumPublicacion), "E" ),
		mbExisteTabDet   = tablasDinamicas.existeTabla( String.valueOf(miNumPublicacion), "D" );
	
	if ( nombre_campo 	  != null ) miNumCampos += nombre_campo.length;
	if ( nombre_campo_edi != null ) miNumCampos += nombre_campo_edi.length;
	
	/*********************************************************************************************************************
	*	GUARDANDO DATOS DE LA ESTRUCTURA DE LA TABLA   
	*********************************************************************************************************************/
	
	try {
		if (miNumPublicacion == 0){
			System.out.println("Se guardaran datos de tabla dinamica");
			miNumPublicacion = tablasDinamicas.guardaPublicacion(cadena, tipo_pub, nom_tabla, fec_ini, fec_fin, miNumCampos);
		}
		else{
			System.out.println("Se modificaran datos de tabla dinamica");
			tablasDinamicas.modificaPublicacion(cadena,	nom_tabla,	fec_ini, fec_fin, miNumCampos, miNumPublicacion);
		}
	} catch(Exception err) {
			lbNoError = false;
	}
	
	/*********************************************************************************************************************
	*	GUARDANDO USUARIOS                             
	*********************************************************************************************************************/

	if(!accion.equals("Modifica")){
		if ( miNumPublicacion > 0 && Usr != null )	{
			System.out.println("\nSe guardaran Usuarios");
			lbNoError = tablasDinamicas.guardaUsuarios(miNumPublicacion, Usr);
		}
	}
	
	if (miNumPublicacion > 0 && lbNoError)	{
	
		if (nombre_campo != null)	{
		/****************************************************************************
		*	GUARDANDO CAMPOS FIJOS
		****************************************************************************/
	
			if ( tablasDinamicas.guardaCampos(miNumPublicacion, nombre_campo, tipo_campo, long_campo_Fijo, totales_campo, orden_campo, "F") )	{
				System.out.println("SE HAN GUARDADO CAMPOS FIJOS");
				lbNoError = true;
			}
		}
	
		if ( nombre_campo_edi != null )	{
		/****************************************************************************
		*	GUARDANDO CAMPOS EDITABLES
		****************************************************************************/
		if ( tablasDinamicas.guardaCampos(miNumPublicacion, nombre_campo_edi, tipo_campo_edi, long_campo_Editable, totales_campo_edi, orden_campo_edi, "E") )	{
				System.out.println("SE HAN GUARDADO CAMPOS EDITABLES");
				lbNoError = true;
			}
		}
	
		if ( nombre_campo_det != null )	{
		/****************************************************************************
		*	GUARDANDO CAMPOS DE DETALLE
		****************************************************************************/
	
			if ( tablasDinamicas.guardaCampos(miNumPublicacion, nombre_campo_det, tipo_campo_det, long_campo_Detalle, totales_campo_det, orden_campo_det, "D") )	{
				System.out.println("SE HAN GUARDADO CAMPOS DE DETALLE");
				lbNoError = true;
			}
		}
	}
	
	/*********************************************************************************************************************
	*	CREANDO/MODIFICANDO PRIMER TABLA
	*********************************************************************************************************************/
	
	if (lbNoError)	{
		//Tabla de campos fijos
		if (nombre_campo != null)	{
			String lsQryTabla = timg.creaTablaFijos(accion, miNumPublicacion, nombre_campo, tipo_campo, long_campo_Fijo);
			System.out.println("\nQUERY Crea Tabla Fija-> "+lsQryTabla);
			lbNoError = tablasDinamicas.modificaPublicaciones( lsQryTabla );
			System.out.println("\nSE HA CREADO/MODIFICADO TABLA FIJOS");
		}
		
		//Tabla de campos editables
		if ( nombre_campo_edi != null )	{
			String lsQryTabla = timg.creaTablaEditables(accion, miNumPublicacion, nombre_campo_edi, tipo_campo_edi, long_campo_Editable, mbExisteTabEdit);
			lbNoError = tablasDinamicas.modificaPublicaciones( lsQryTabla );
			System.out.println("\nSE HA CREADO/MODIFICADO TABLA EDITABLES");
		}
		
		//Tabla de campos de detalle
		if ( nombre_campo_det != null )	{
			String lsQryTabla = timg.creaTablaDetalles(accion, miNumPublicacion, nombre_campo_det, tipo_campo_det, long_campo_Detalle, mbExisteTabDet);
			lbNoError = tablasDinamicas.modificaPublicaciones( lsQryTabla );
			System.out.println("\nSE HA CREADO/MODIFICADO TABLA DETALLE");
		}
	} //Fin del if CREANDO/MODIFICANDO PRIMER TABLA
	
	if(accion.equals("Modifica")){
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	}else{
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("nom_tabla", nom_tabla);
		jsonObj.put("miNumPublicacion", String.valueOf(miNumPublicacion));
		infoRegresar = jsonObj.toString();
	}
	System.out.println("\ninfoRegresar: "+infoRegresar);
	
}else if(informacion.equals("obtenFijos")){
	String  copiar   = (request.getParameter("copiar")    == null) ? "" : request.getParameter("copiar");
	List li = new ArrayList();
	li = timg.obtenFijos(copiar);
	
	JSONArray a= new JSONArray();
	for(int i=0;i<li.size(); i++){
		Vector aux=(Vector)li.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOMCAMPOF",aux.get(0));
		jo.put("TIPODATF",aux.get(1));
		jo.put("LONGITUDF",aux.get(2));
		jo.put("ORDENF",aux.get(4));
		jo.put("TOTALESF",aux.get(3));
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("obtenEditables")){
	String  copiar   = (request.getParameter("copiar")    == null) ? "" : request.getParameter("copiar");
	List li = new ArrayList();
	li = timg.obtenEditables(copiar);
	
	JSONArray a= new JSONArray();
	for(int i=0;i<li.size(); i++){
		Vector aux=(Vector)li.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOMCAMPOE",aux.get(0));
		jo.put("TIPODATE",aux.get(1));
		jo.put("LONGITUDE",aux.get(2));
		jo.put("ORDENE",aux.get(4));
		jo.put("TOTALESE",aux.get(3));
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("obtenDetalle")){
	String  copiar   = (request.getParameter("copiar")    == null) ? "" : request.getParameter("copiar");
	List li = new ArrayList();
	li = timg.obtenDetalle(copiar);
	
	JSONArray a= new JSONArray();
	for(int i=0;i<li.size(); i++){
		Vector aux=(Vector)li.get(i);
		JSONObject jo=new JSONObject();
		jo.put("NOMCAMPOD",aux.get(0));
		jo.put("TIPODATD",aux.get(1));
		jo.put("LONGITUDD",aux.get(2));
		jo.put("ORDEND",aux.get(4));
		jo.put("TOTALESD",aux.get(3));
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("ArchivoCSV")){	
	String [] arr = request.getParameterValues("arreglo");
	JSONObject jsonObj = new JSONObject();
	List lArchivo = new ArrayList();
	String nomArch = "";
	
	for(int cont = 0;cont < arr.length; cont++){
			List au = new ArrayList();
			String [] ac = arr[cont].split(",");
			for(int i=0; i<ac.length; i++){
				au.add(ac[i]);
			}
			lArchivo.add(au);
	}
	
	nomArch = timg.crearCSV(request, lArchivo, strDirectorioTemp, "CSV");
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArch);
	infoRegresar = jsonObj.toString();

}
	
%>
<%=infoRegresar%>
