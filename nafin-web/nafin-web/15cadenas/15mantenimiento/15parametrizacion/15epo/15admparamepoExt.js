Ext.onReady(function() {
	
	var form = {oldVal:0, agregar:false,	camposAgregar:null}

	function validaCampoLong(long_ant, nvoCampo){
		if(nvoCampo.getValue() < 1){
                    nvoCampo.reset();
				Ext.Msg.show({
					title:	'Longitud',
					msg:		'El valor tiene que ser mayor a cero',
					buttons:	Ext.Msg.OK,
					icon: Ext.MessageBox.WARNING
				});
			return false;
                }
                /*if(nvoCampo.getValue() < long_ant){
			nvoCampo.reset();
				Ext.Msg.show({
					title:	'Longitud',
					msg:		'La longitud no puede ser menor a la anterior',
					buttons:	Ext.Msg.OK,
					icon: Ext.MessageBox.WARNING
				});
			return false;
		}*/	
	}

	function reseteaPanel(id){
		var myPanel = Ext.getCmp(id);
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype == 'panel')	{	//El metodo isValid() no aplica para los tipos de objetos label.
				if(reseteaPanel(panelItem.id)) {
					return;
				}
			}else{
				panelItem.reset();
				return;
			}
		});
		return;
	}
	
	var procesarSaveDinamicos = function(opts, success, response){
		pnl.el.unmask();
                if (success == true || Ext.util.JSON.decode(response.responseText).success == true){
                    Ext.Msg.show({
                            title:	'Guardar',
                            msg:		'�Se han guardado los cambios con �xito!',
                            buttons:	Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                    });
                }
                else{
                    NE.util.mostrarConnError(response,opts);
                }
	}

	var procesarMuestraDinamicos =  function(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('dinaTipoObj').hide();
			Ext.getCmp('dinaObliga').hide();
			Ext.getCmp('idAgregar').hide();
			var infoR = Ext.util.JSON.decode(response.responseText);
			var contador = 0;
			var contador_fin = 0;
			if ( infoR.contador_fin != undefined){
				contador_fin = parseFloat(infoR.contador_fin);
			}
			if (!fpTabla.isVisible()){
				fpTabla.show();
			}
			if ( (infoR.regsAdicionales != undefined && infoR.regsAdicionales.length > 0) || (infoR.regsDetalle != undefined && infoR.regsDetalle.length > 0)){
				var camposDinamicos;
				if ( (infoR.regsAdicionales != undefined && infoR.regsAdicionales.length > 0) ){
					camposDinamicos = infoR.regsAdicionales;
					contador = infoR.regsAdicionales.length;
				}
				if ( (infoR.regsDetalle != undefined && infoR.regsDetalle.length > 0) ){
					camposDinamicos = infoR.regsDetalle;
					contador = infoR.regsDetalle.length;
				}
				var i = 0;
				Ext.each(camposDinamicos, function(item, index, arrItems){

					Ext.getCmp('hdn_bandera').setValue('UPDATE');
					var config =
						{
							xtype:'panel', id:'pnlIndex'+index,	defaults: {	xtype:'panel',	border: false,	frame: false,	width: 187,	layout:'form',	bodyStyle: 'padding:5px 5px 0'	},
							items:[
								{
									defaultType: 'textfield',	width: 210,	labelWidth: 2,	labelSeparator:'',	defaults: {anchor:'-20',	maxLength:30,msgTarget: 'side'},
									items:[	{id:'TxtNombre'+index, name:'TxtNombre'+index,  regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/i , regexText:'El campo debe ser alfanum�rico', value:item.NOMBRE_CAMPO}	]
								},{
									defaultType: 'textfield',	hideLabels:true,	hidden:false,	defaults: {	anchor:'-20',	msgTarget: 'side'	},
									items:[{	xtype: 'combo',	name: '_sel_tipocampo'+index,	id: 'sel_tipocampo'+index,	hiddenName: '_sel_tipocampo'+index, mode: 'local',	editable:false,
												displayField: 'descripcion',	valueField: 'clave',	forceSelection : true, disabled:false,
												triggerAction : 'all',	typeAhead: true,	minChars : 1,	store: catalogoTipoCampoData,	value: item.CG_TIPO_DATO,
												listeners:{
													'select':function(cbo){
																	var ids = cbo.id;
																	var idLong = ids.replace('sel_tipocampo','TxtLongitud');
																	var txtLong = Ext.getCmp(idLong);
																	if (cbo.getValue() === 'A'){
																		txtLong.setMaxValue(4000);
																	}else if (cbo.getValue() === 'N'){
																		txtLong.setMaxValue(38);
																	}
																}
												}
											}]
								},{
									defaultType: 'numberfield',	width:160,	hideLabels:true,	defaults: {	anchor:'-20',	maxLength:4,	msgTarget: 'side'	},
									items:[{id:'TxtLongitud'+index,	name:'TxtLongitud'+index, value:item.IG_LONGITUD, 
												listeners:	{
													'focus':	function(field){
																	var ids = field.id;
																	var idTipo = ids.replace('TxtLongitud','sel_tipocampo');
																	var txtTipo = Ext.getCmp(idTipo);
																	if (txtTipo.getValue() === 'A'){
																		field.setMaxValue(4000);
																	}else if (txtTipo.getValue() === 'N'){
																		field.setMaxValue(38);
																	}
																},
													'blur':	function(field){
																	validaCampoLong(field.originalValue,field);
																}
												}
									}]
								}
							]
						}

					fpTabla.add(config);
					fpTabla.insert(index+1,config);
					fpTabla.doLayout();

					var radios	= {};
					var tipoObj	= {};
					if (opts.params._sel_tipo === "1"){
						var itemsRadio={};
						var itemsTipoObj={};
						if(opts.params._sel_producto === "9"){
							if (item.CS_OBLIGATORIO === 'S'){
								itemsRadio = {xtype: 'radiogroup',	columns: 2, items:[{boxLabel: 'Si', name: 'rad_oblig'+index, id:'rad_obligS'+index, inputValue: 'S',  checked: true},{boxLabel: 'No', name: 'rad_oblig'+index, id:'rad_obligN'+index, inputValue: 'N'}]}
							}else{
								itemsRadio = {xtype: 'radiogroup',	columns: 2,	items:[{boxLabel: 'Si', name: 'rad_oblig'+index, id:'rad_obligS'+index, inputValue: 'S'},{boxLabel: 'No', name: 'rad_oblig'+index, id:'rad_obligN'+index, inputValue: 'N',  checked: true}]}
							}
							if(i>=0 || i<=5){
								itemsTipoObj = {id:'_sel_tipobjeto'+index, name:'sel_tipobjeto'+index,	value:'Texto',	readOnly:true}
								tipoObj=	{	defaultType: 'textfield',	hideLabels:true,	defaults: {	anchor:'-20',	maxLength:30,	msgTarget: 'side'	},	items:[itemsTipoObj]	}
								if ( !Ext.getCmp('dinaTipoObj').isVisible() ){
									Ext.getCmp('dinaTipoObj').show();
								}
								Ext.getCmp('pnlIndex'+index).add(tipoObj);
								Ext.getCmp('pnlIndex'+index).doLayout();
							}
							radios = {	hideLabels:true,	width: 130,	defaults: {	anchor:'-20',	msgTarget: 'side'	},	items:[itemsRadio]	}
							if ( !Ext.getCmp('dinaObliga').isVisible() ){
								Ext.getCmp('dinaObliga').show();
							}
							//*-*-*-*-*	Agrega radios
							Ext.getCmp('pnlIndex'+index).add(radios);
							Ext.getCmp('pnlIndex'+index).doLayout();
						}else{
							if (item.CS_OBLIGATORIO == 'S'){
								itemsRadio = { xtype: 'radiogroup',	columns: 1,	items:[{boxLabel: 'Si', name: 'rad_oblig'+index, id:'rad_obligS'+index, inputValue: 'S',	checked: true,	disabled:true}] }
							}else{
								itemsRadio = {xtype: 'radiogroup',	columns: 1,	items:[{boxLabel: 'No', name: 'rad_oblig'+index, id:'rad_obligN'+index, inputValue: 'N',	disabled:true}] }
							}
							if (opts.params._sel_producto == "1"){
								if(i==0 || i==1){
									itemsTipoObj = {
										xtype: 'combo',
										id:			'_sel_tipobjeto'+index,
										hiddenName:	'sel_tipobjeto'+index,
										name:			'sel_tipobjeto'+index,
										mode: 'local',
										editable:false,
										displayField: 'descripcion',
										valueField: 'clave',										
										forceSelection : true,
										triggerAction : 'all',
										typeAhead: true,
										minChars : 1,
										store: catalogoTipoObjetoData,
										value: item.CG_TIPO_OBJETO
									}
									tipoObj=	{	hideLabels:true,	defaults: {	anchor:'-20',	maxLength:30,	msgTarget: 'side'	},	items:itemsTipoObj	}

								}else{
									itemsTipoObj = {xtype: 'displayfield', value:''}
									tipoObj=	{	hideLabels:true,	defaults: {	anchor:'-20',	maxLength:30,	msgTarget: 'side'	},	items:itemsTipoObj	}
								}

								if ( !Ext.getCmp('dinaTipoObj').isVisible() ){
									Ext.getCmp('dinaTipoObj').show();
								}
								Ext.getCmp('pnlIndex'+index).add(tipoObj);
								Ext.getCmp('pnlIndex'+index).doLayout();
								
								radios = {	hideLabels:true,	width: 130,	defaults: {	anchor:'-20',	msgTarget: 'side'	},	items:itemsRadio	}
								if ( !Ext.getCmp('dinaObliga').isVisible() ){
									Ext.getCmp('dinaObliga').show();
								}
								//*-*-*-*-*	Agrega radios
								Ext.getCmp('pnlIndex'+index).add(radios);
								Ext.getCmp('pnlIndex'+index).doLayout();
							}
						}
					}
					var itemHid = {xtype:'hidden', id:'hdn_numcampo'+index, name:'hdn_numcampo'+index, value: item.IC_NO_CAMPO}
					var pnlHidCampo = {hideLabels:true,	width: 130,	defaults: {	anchor:'-20',	msgTarget: 'side'	},	items:itemHid}
					Ext.getCmp('pnlIndex'+index).add(pnlHidCampo);
					Ext.getCmp('pnlIndex'+index).doLayout();

					if ( (i==0 || i==1) && opts.params._sel_tipo == "1" ){
							Ext.getCmp('TxtNombre'+i).label.update('<div><font color="red">*</font> </div>');
							Ext.getCmp('TxtNombre'+i).allowBlank=false;	
					}
					i ++;
				});

				if ( contador !=  contador_fin){
					Ext.getCmp('idAgregar').show();
					form.camposAgregar = contador_fin - contador;
				}
				
			}else{
				Ext.getCmp('hdn_bandera').setValue('');
				var h=0;
				if(form.agregar){
					for(h=0;h < form.camposAgregar; h++){
						var config =
							{
								xtype:'panel', id:'pnlIndex'+h,	defaults: {	xtype:'panel',	border: false,	frame: false,	width: 187,	layout:'form',	bodyStyle: 'padding:5px 5px 0'	},
								items:[
									{
										defaultType: 'textfield',	width:210,	labelWidth: 2,	labelSeparator:'',	defaults: {anchor:'-20',	maxLength:30,msgTarget: 'side'},
										items:[	{id:'TxtNombre'+h,	name:'TxtNombre'+h,   regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/i , regexText:'El campo debe ser alfanum�rico'  }	]
									},{
										defaultType: 'textfield',	hideLabels:true,	hidden:false,	defaults: {	anchor:'-20',	msgTarget: 'side'	},
										items:[	{	xtype: 'combo',	name: '_sel_tipocampo'+h,	id: 'sel_tipocampo'+h, hiddenName: '_sel_tipocampo'+h,	mode: 'local',	editable:false,
														displayField: 'descripcion',	valueField: 'clave',	forceSelection : true,
														triggerAction : 'all',	typeAhead: true,	minChars : 1,	store: catalogoTipoCampoData,	value: 'A',
														listeners:{
															'select':function(cbo){
																			var ids = cbo.id;
																			var idLong = ids.replace('sel_tipocampo','TxtLongitud');
																			var txtLong = Ext.getCmp(idLong);
																			if (cbo.getValue() === 'A'){
																				txtLong.setMaxValue(4000);
																			}else if (cbo.getValue() === 'N'){
																				txtLong.setMaxValue(38);
																			}
																		}
														}
													}]
									},{
										defaultType: 'numberfield',	width:160,	hideLabels:true,	defaults: {	anchor:'-20',	maxLength:4,	msgTarget: 'side'	},
										items:[	{id:'TxtLongitud'+h,	name:'TxtLongitud'+h,
													listeners:	{
														'focus':	function(field){
																		var ids = field.id;
																		var idTipo = ids.replace('TxtLongitud','sel_tipocampo');
																		var txtTipo = Ext.getCmp(idTipo);
																		if (txtTipo.getValue() === 'A'){
																			field.setMaxValue(4000);
																		}else if (txtTipo.getValue() === 'N'){
																			field.setMaxValue(38);
																		}
																	},
														'blur':	function(field){
																		validaCampoLong(field.originalValue,field);
																	}
													}
										}]
									}
								]
							}
						if (h==0 && form.camposAgregar == 4 && Ext.getCmp("sel_producto").getValue() == "1"){
						/*Modifica HVC 21/03/2013*/
							config.items.push(
								{	hideLabels:true,	defaults: {	anchor:'-20',	maxLength:30,	msgTarget: 'side'	},	
									items:[
										{
											xtype: 'combo',
											name: 'sel_tipobjeto'+h,
											id: '_sel_tipobjeto'+h,
											mode: 'local',
											editable:false,
											displayField: 'descripcion',
											valueField: 'clave',
											hiddenName : 'sel_tipobjeto'+h,
											forceSelection : true,
											triggerAction : 'all',
											typeAhead: true,
											minChars : 1,
											store: catalogoTipoObjetoData,
											value: 'T'
										}
									]
								}
							);
							if ( !Ext.getCmp('dinaTipoObj').isVisible() ){
								Ext.getCmp('dinaTipoObj').show();
							}
						}
						fpTabla.add(config);
						fpTabla.insert(h+1,config);
						fpTabla.doLayout();

					}
				}else{
					for(h=0;h < contador_fin; h++){
						var config =
							{
								xtype:'panel', id:'pnlIndex'+h,	defaults: {	xtype:'panel',	border: false,	frame: false,	width: 187,	layout:'form',	bodyStyle: 'padding:5px 5px 0'	},
								items:[
									{
										defaultType: 'textfield',	width:210,	labelWidth: 2,	labelSeparator:'',	defaults: {anchor:'-20',	maxLength:30,msgTarget: 'side'},
										items:[	{id:'TxtNombre'+h,	name:'TxtNombre'+h,  regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/i , regexText:'El campo debe ser alfanum�rico'  }	]
									},{
										defaultType: 'textfield',	hideLabels:true,	hidden:false,	defaults: {	anchor:'-20',	msgTarget: 'side'	},
										items:[	{	xtype: 'combo',	name: '_sel_tipocampo'+h,	id: 'sel_tipocampo'+h,	hiddenName: '_sel_tipocampo'+h,  mode: 'local',	editable:false,
														displayField: 'descripcion',	valueField: 'clave',	forceSelection : true,
														triggerAction : 'all',	typeAhead: true,	minChars : 1,	store: catalogoTipoCampoData,	value: 'A',
														listeners:{
															'select':function(cbo){
																			var ids = cbo.id;
																			var idLong = ids.replace('sel_tipocampo','TxtLongitud');
																			var txtLong = Ext.getCmp(idLong);
																			if (cbo.getValue() === 'A'){
																				txtLong.setMaxValue(4000);
																			}else if (cbo.getValue() === 'N'){
																				txtLong.setMaxValue(38);
																			}
																		}
														}
													}]
									},{
										defaultType: 'numberfield',	width:160,	hideLabels:true,	defaults: {	anchor:'-20',	maxLength:4,	msgTarget: 'side'	},
										items:[	{id:'TxtLongitud'+h,	name:'TxtLongitud'+h,
													listeners:	{
														'focus':	function(field){
																		var ids = field.id;
																		var idTipo = ids.replace('TxtLongitud','sel_tipocampo');
																		var txtTipo = Ext.getCmp(idTipo);
																		if (txtTipo.getValue() === 'A'){
																			field.setMaxValue(4000);
																		}else if (txtTipo.getValue() === 'N'){
																			field.setMaxValue(38);
																		}
																	},
														'blur':	function(field){
																		validaCampoLong(field.originalValue,field);
																	}
													}
										}]
									}
								]
							}
						fpTabla.add(config);
						fpTabla.insert(h+1,config);
						fpTabla.doLayout();

						var tipoObj	= {};
						if (opts.params._sel_tipo === "1"){
							var itemsTipoObj={};
							if(opts.params._sel_producto === "9"){
								if( h < 5 ){
									itemsTipoObj = {id:'_sel_tipobjeto'+h, name:'sel_tipobjeto'+h,	value:'Texto',	readOnly:true}
									tipoObj=	{	defaultType: 'textfield',	hideLabels:true,	defaults: {	anchor:'-20',	maxLength:30,	msgTarget: 'side'	},	items:[itemsTipoObj]	}
									if ( !Ext.getCmp('dinaTipoObj').isVisible() ){
										Ext.getCmp('dinaTipoObj').show();
									}
									if ( !Ext.getCmp('dinaObliga').isVisible() ){
										Ext.getCmp('dinaObliga').show();
									}
									Ext.getCmp('pnlIndex'+h).add(tipoObj);
									Ext.getCmp('pnlIndex'+h).doLayout();
								}
							}else{
								if (opts.params._sel_producto == "1"){
									if(h==0 || h==1){
										itemsTipoObj = {
											xtype: 'combo',
											name: 'sel_tipobjeto'+h,
											id: '_sel_tipobjeto'+h,
											mode: 'local',
											editable:false,
											displayField: 'descripcion',
											valueField: 'clave',
											hiddenName : 'sel_tipobjeto'+h,
											forceSelection : true,
											triggerAction : 'all',
											typeAhead: true,
											minChars : 1,
											store: catalogoTipoObjetoData,
											value: 'T'
										}
										tipoObj=	{	hideLabels:true,	defaults: {	anchor:'-20',	maxLength:30,	msgTarget: 'side'	},	items:itemsTipoObj	}

									}else{
										itemsTipoObj = {xtype: 'displayfield', value:''}
										tipoObj=	{	hideLabels:true,	defaults: {	anchor:'-20',	maxLength:30,	msgTarget: 'side'	},	items:itemsTipoObj	}
									}
									if ( !Ext.getCmp('dinaTipoObj').isVisible() ){
										Ext.getCmp('dinaTipoObj').show();
									}
									if ( !Ext.getCmp('dinaObliga').isVisible() ){
										Ext.getCmp('dinaObliga').show();
									}
									Ext.getCmp('pnlIndex'+h).add(tipoObj);
									Ext.getCmp('pnlIndex'+h).doLayout();
								}
							}
						}

						if ( (h==0 || h==1) && opts.params._sel_tipo == "1" ){
								Ext.getCmp('TxtNombre'+h).label.update('<div><font color="red">*</font> </div>');
								Ext.getCmp('TxtNombre'+i).allowBlank=false;	
						}
					}
				}
			}
			if ( Ext.getCmp('dinaObliga').isVisible() || Ext.getCmp('dinaTipoObj').isVisible() ){
				fpTabla.setWidth(940);
			}else{
				fpTabla.setWidth(600);
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

///***********--- - - - - -  Store�s  - - - - - - ******************/

	var catalogoTipoCampoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['A','Alfanum�rico'],
			['N','Num�rico']
		 ]
	});

	var catalogoTipoObjetoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['C','Combo'],
			['T','Texto']
		 ]
	});

	var catalogoTipoRefData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['0','Seleccionar Tipo. . .'],
			['1','Adicionales'],
			['2','Detalle']
		 ]
	});

	var catalogoProductoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admparamepoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoProducto'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

///***********--- - - - - -  Componentes  - - - - - - ******************/

	var elementosDinamicos = [
		{	//Indice Cero.		//	0
			xtype:'panel',
			id:	'panIni',
			defaults: {
				xtype:'panel',
				border: false,
				frame: false,
				width: 187,
				layout:'form',
				bodyStyle: 'padding:5px 5px 0'
			},
			items:[
				{
					title:'Nombre',	id:	'dinaNombre',	width: 210
				},{
					title:'Tipo',	id:'dinaTipo'
				},{
					title:'Longitud',	id:'dinaLong',	width: 160
				},{
					title:'Tipo de Objeto',	id:'dinaTipoObj',	hidden: true
				},{
					title:'Obligatorio',	id:'dinaObliga', width:140,	hidden: true
				}
			]
		},{	//Indice Uno ++.		//	1++
			xtype:'panel',
			html:'<div align="center"><br>* Estos ser�n utilizados como criterios de b�squeda.</div>',
			id:	'panMarca',
			width: 600,
			hidden:true,
			border:false
		},{
			xtype:'panel',
			id:	'panFlag',
			hidden:true,
			items:[
				{
					xtype:'hidden', id:'hdn_bandera', name:'hdn_bandera', value: ''
				}
			]
		}
	];

	var fpTabla = new Ext.form.FormPanel({
		id: 'formaTabla',
		title:'Definicion de Tabla',
		width: 940,
		frame: false,
		hidden:true,
		renderHidden:true,
		style: ' margin:0 auto;',
		bodyStyle: 'padding:20px 20px 20px 20px',
		defaults:{
			layout:'hbox',
			width: 935,
			border:false,
			style: 'margin:0 auto;'
		},
		buttons:[
                    {
                            text:'Guardar Tabla',
                            iconCls:'icoGuardar',
                            handler:	function(){

                                if(	!Ext.getCmp('formaTabla').getForm().isValid()	){
                                        return;
                                }
                                pnl.el.mask('Procesando...', 'x-mask-loading');
                                                                                                
                                if (form.agregar){
                                        Ext.Ajax.request({
                                                url: '15admparamepoExt.data.jsp',
                                                params: Ext.apply(fpTabla.getForm().getValues(),fp.getForm().getValues(),
                                                                    {	informacion:'saveDinamicos',
                                                                            operacion:	form.agregar,
                                                                            camposAgregar:form.camposAgregar
                                                                    }
                                                            ),
                                                callback: procesarSaveDinamicos
                                        });
                                }else{
                                        Ext.Ajax.request({
                                                url: '15admparamepoExt.data.jsp',
                                                params: Ext.apply(fpTabla.getForm().getValues(),fp.getForm().getValues(),{	informacion:'saveDinamicos',operacion:	form.agregar}	),
                                                callback: procesarSaveDinamicos
                                        });
                                }
                                                            
                            }
			},{
				text:'Agregar Campos',
				iconCls:	'autorizar',
				id:		'idAgregar',
				hidden: true,
				handler:	function(){
								form.agregar = true;
								fp.hide();
								Ext.getCmp('panMarca').hide();
								Ext.getCmp('idRegresar').show();
								fpTabla.items.each(function(c){
															if(c.id != "panIni" && c.id != "panMarca" && c.id != "panFlag"){
																fpTabla.remove(c);
															}
														})
								pnl.el.mask('Procesando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '15admparamepoExt.data.jsp',
									params: Ext.apply({informacion:"ObtieneDinamicos"	}),
									callback: procesarMuestraDinamicos
								});
							}
			},{
				text:'Limpiar',
				iconCls:'icoLimpiar',
				handler: function(){
								if(form.agregar){
									var y;
									for (y=0; y < form.camposAgregar; y++){
										reseteaPanel('pnlIndex'+y);
									}
								}else{
									window.location = "15admparamepoExt.jsp";
								}
							}
			},{
				text:'Regresar',
				//iconCls:'',
				id:	'idRegresar',
				hidden:true,
				handler: function(){
								form.agregar = false;
								fp.show();
								fpTabla.items.each(function(c){
															if(c.id != "panIni" && c.id != "panMarca" && c.id != "panFlag"){
																fpTabla.remove(c);
															}
														})
								var sel_tipo = Ext.getCmp('sel_tipo');
								var sel_producto = Ext.getCmp('sel_producto');
									if (!sel_producto.isVisible()){
										sel_producto.show();
									}
									if (sel_tipo.getValue() === "1"){
										Ext.getCmp('panMarca').show();
									}else{
										Ext.getCmp('panMarca').hide();
									}
									Ext.getCmp('idRegresar').hide();
									if (	!Ext.isEmpty(sel_tipo.getValue()) && parseFloat(sel_producto.getValue()) > 0){
										pnl.el.mask('Procesando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: '15admparamepoExt.data.jsp',
											params: Ext.apply({informacion:"ObtieneDinamicos",	_sel_tipo: sel_tipo.getValue(),	_sel_producto: sel_producto.getValue() }),
											callback: procesarMuestraDinamicos
										});
									}
							}
			}
		],
		items: elementosDinamicos
	});

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title:'Campos Adicionales',
		width: 500,
		frame: true,
		labelWidth: 170,
		labelSeparator:'',
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults : {
			anchor : '-20'
		},
		items: [
			{
				xtype: 'combo',
				id:	'sel_tipo',
				name: '_sel_tipo',
				hiddenName: '_sel_tipo',
				fieldLabel: 'Tipo de Referencia de Campo',
				emptyText: 'Seleccionar Tipo. . .',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				autoLoad: false,
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store: catalogoTipoRefData,
				listeners:{
					select:{
						fn:	function(cbo){
									fpTabla.items.each(function(c){
																if(c.id != "panIni" && c.id != "panMarca" && c.id != "panFlag"){
																	fpTabla.remove(c);
																}
															})
									var sel_producto = Ext.getCmp('sel_producto');
									if (cbo.getValue() === '0'){
										sel_producto.hide();
										fpTabla.hide();
									}else{
										if (!sel_producto.isVisible()){
											sel_producto.show();
										}
										if (cbo.getValue() === "1"){
											Ext.getCmp('panMarca').show();
										}else{
											Ext.getCmp('panMarca').hide();
										}
										if (	!Ext.isEmpty(cbo.getValue()) && parseFloat(sel_producto.getValue()) > 0){
											pnl.el.mask('Procesando...', 'x-mask-loading');
											Ext.Ajax.request({
												url: '15admparamepoExt.data.jsp',
												params: Ext.apply({informacion:"ObtieneDinamicos",	_sel_tipo: cbo.getValue(),	_sel_producto: sel_producto.getValue() }),
												callback: procesarMuestraDinamicos
											});
										}
									}
								}
					}
				}
			},{
				xtype: 'combo',
				id:	'sel_producto',
				name: '_sel_producto',
				hiddenName:'_sel_producto',
				fieldLabel: 'Producto',
				emptyText: 'Seleccionar Producto. . .',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				forceSelection : false,
				triggerAction : 'all',
				typeAhead: true,
				hidden: true,
				minChars : 1,
				store: catalogoProductoData,
				tpl : NE.util.templateMensajeCargaCombo,
				listeners:{
					select:{
						fn:	function(cbo){
									fpTabla.items.each(function(c){
																if(c.id != "panIni" && c.id != "panMarca" && c.id != "panFlag"){
																	fpTabla.remove(c);
																}
															})
									if (cbo.getValue() != '0'){
										pnl.el.mask('Procesando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: '15admparamepoExt.data.jsp',
											params: Ext.apply({informacion:"ObtieneDinamicos",	_sel_tipo: Ext.getCmp('sel_tipo').getValue(),	_sel_producto: cbo.getValue() }),
											callback: procesarMuestraDinamicos
										});
									}else{
										fpTabla.hide();
									}
								}
					}
				}
			}
		]
	});

	catalogoProductoData.load();

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			fpTabla
		]
	});

});