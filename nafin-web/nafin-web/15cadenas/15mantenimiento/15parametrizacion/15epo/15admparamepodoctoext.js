Ext.onReady(function(){
		Ext.QuickTips.init();
//-------------------------------------HANDLERS----------------------------------
	var procesaActualizacion = function(opts, success, response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.Msg.alert('Mensaje informativo',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){  
					window.location = '15admparamepodoctoext.jsp';	 
			});
		}else{
			Ext.Msg.alert('Mensaje de Error',"�Error al intentar actualizar!",function(btn){  
					window.location = '15admparamepodoctoext.jsp';	 
			});
		}
	}	
	var procesarConsultaData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var grid = Ext.getCmp('grid');
			grid.show();
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
			el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
//--------------------------------------STORES-----------------------------------
var consultaData = new Ext.data.JsonStore({
	root: 'registros',
	url: '15admparamepodoctoext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'CG_NOMBRE_COMERCIAL'},
				{name: 'IC_NOMBRE'},
				{name: 'CD_DESCRIPCION'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
							fn: function(proxy,type,action,optionRequest,response,args){
								NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
								procesarConsultaData(null,null,null);
							}
		}
	}	
});
var catalogoProductoData = new Ext.data.JsonStore({
	id: 'catalogoProductoDataStore',
	root: 'registros',
	fields: ['clave','descripcion','loadMsg'],
	autoDestroy: true,
	url: '15admparamepodoctoext.data.jsp',
	baseParams: {
		informacion: 'CatalogoProducto'
	},
	totalProperty: 'total',
	autoLoad: false,
	listeners: {
		exception: {
			fn: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
			}
		}
	}
});
var catalogoDocumentoData = new Ext.data.JsonStore({
	id: 'catalogoDocumentoDataStore',
	root: 'registros',
	fields : ['clave', 'descripcion','loadMsg'],
	autoDestroy: true,
	url: '15admparamepodoctoext.data.jsp',
	baseParams: {
		informacion: 'CatalogoDocumento'
	},
	totalProperty: 'total',
	autoLoad: false,
	listeners: {
		exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
		}
	}
});
//-------------------------------------ELEMENTOS------------------------------------
	var elementosForma = [{
		xtype: 'container',
		anchor: '100%',
		layout: 'column',
		items: [
									{
										xtype: 'container',
										id: 'panelIzquierdo',
										columnWidth: .5,
										layout: 'form',
										items: [
													{
														xtype: 'combo',
														name: 'cboProd',
														id: 'cmbProb',
														fieldLabel: 'Producto',
														mode: 'local',
														displayField: 'descripcion',
														valueField: 'clave',
														hiddenName: 'cboProd',
														emptyText: 'Seleccione un Producto',
														forceSelection: true,
														triggerAction: 'all',
														typeAhead: true,
														minChars: 1,
														store: catalogoProductoData,
														tpl: NE.util.templateMensajeCargaCombo,
														anchor: '90%'
													}
										]
									},
									{
										xtype: 'container',
										id: 'panelDerecho',
										columnWidth: .5,
										layout: 'form',
										items: [
													{
														xtype: 'combo',
														name: 'cboDocto',
														id: 'cmbDocto',
														fieldLabel: 'Clase de Documento',
														emptyText: 'Seleccione un documento',
														mode: 'local',
														displayField: 'descripcion',
														valueField: 'clave',
														hiddenName: 'cboDocto',
														forceSeleccion: true,
														triggerAction: 'all',
														typeAhead: true,
														minChars: 1,
														store: catalogoDocumentoData,
														tpl: NE.util.templateMensajeCargaCombo,
														anchor: '90%'
													}
										]
									}
			]
	}];
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'grid',
		store: consultaData,
		style: 'margin:0 auto;',
		hidden: true,
		columns: [
						{
							header: 'Cadena Productiva',
							dataIndex: 'CG_NOMBRE_COMERCIAL',
							tooltip: 'Cadena Productiva',
							sortable: true,
							resiazable: false,
							align: 'center',
							width:150
						},
						{
							header: 'Producto',
							dataIndex: 'IC_NOMBRE',
							tooltip: 'Producto',
							sortable: true,
							resiazable: false,
							align: 'center',
							width:180
						},
						{
							header: 'Clase de Documento',
							dataIndex: 'CD_DESCRIPCION',
							tooltip: 'Clase de Documento',
							sortable: true,
							resiazable: false,
							align: 'center',
							width:150
						}
		],
		stripeRows: true,
		loasMask: true,
		height: 200,
		width: 500,
		title:'',
		frame: true
	});
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 885,
		style: 'margin:0 auto;padding:20px;',
		title: '',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 10px',
		labelWidth: 70,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
						{
							text: 'Aceptar',
							id: 'btnAceptar',
							iconCls: 'icoAceptar',
							formBind: true,
							handler: function(boton,evento){
								var grid = Ext.getCmp('grid');
								grid.hide();
								var cmbProb = Ext.getCmp('cmbProb');
								var cmbDocto = Ext.getCmp('cmbDocto');
								if(Ext.isEmpty(cmbProb.getValue())){
									cmbProb.markInvalid('Favor de seleccionar un producto.');
									cmbProb.focus();
									return;
								}
								if(Ext.isEmpty(cmbDocto.getValue())){
									cmbDocto.markInvalid('Favor de seleccionar una Clase de Documento.');
									cmbDocto.focus();
									return;
								}
								Ext.Ajax.request({
									url: '15admparamepodoctoext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'Actualizar'
									}),
									callback: procesaActualizacion
								});
							}
						},
						{
							text: 'Consultar',
							id: 'btnConsultar',
							iconCls: 'icoBuscar',
							formBind: true,
							handler: function(boton,evento){		
								consultaData.load();
							}
						}
		]
	});
	function verificaPanelIzquierda(){
		var Panel = Ext.getCmp('panelIzquierda');
		var valid = true;
		Panel.items.each(function(panelItem, index, totalCount){
			if(!panelItem.isValid()){
				valid = false;
				return false;
			}
		});
		return valid;
	};
	function  verificaPanelDerecha(){
		var Panel = Ext.getCmp('panelDerecha');
		var valid = true;
		Panel.items.each(function(panelItem, index, totalCount){
			if(!panelItem.isValid()){
				valid = false;
				return false;
			}
		});
		return valid;
	};
//----------------------------------PRINCIPAL-----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridConsulta
		]
	});
	catalogoProductoData.load();
	catalogoDocumentoData.load();
});