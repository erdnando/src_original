Ext.onReady(function(){
	Ext.QuickTips.init();
//-------------------------------VARIABLES--------------------------------------
	var jsonValoresIniciales = null;
//-------------------------------FUNCIONES--------------------------------------
	function procesaValoresIniciales(opts, success, response) {
		var txtPorcDescM = Ext.getCmp('txtPorcDescM');
		var txtPorcDescD = Ext.getCmp('txtPorcDescD');
		var chkOperaFactoraje = Ext.getCmp('chkOperaFactoraje');
		var chkOperaFactorajeDistribuido = Ext.getCmp('chkOperaFactorajeDistribuido');
		var txtDiasMin = Ext.getCmp('txtDiasMin');
		var txtDiasMax = Ext.getCmp('txtDiasMax');
		var txtDiasNotif = Ext.getCmp('txtDiasNotif');
		var txtDiasNotifReact = Ext.getCmp('txtDiasNotifReact');
		var radioFactorajeVencido1 = Ext.getCmp('radioFactorajeVencido1');
		var radioFactorajeVencido2 = Ext.getCmp('radioFactorajeVencido2');
                var chkOperaDescAutoEPO = Ext.getCmp('chkOperaDescAutoEPO'); 
		fp.el.mask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
				jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
				if(jsonValoresIniciales != null){
					txtPorcDescM.setValue(jsonValoresIniciales.strPorcientoActual);
					txtPorcDescD.setValue(jsonValoresIniciales.strPorcientoActualDL);
					chkOperaFactoraje.setValue(jsonValoresIniciales.operaFactVenc);
					chkOperaFactorajeDistribuido.setValue(jsonValoresIniciales.operaFactDist);
					txtDiasMin.setValue(jsonValoresIniciales.strDiasMin);
					txtDiasMax.setValue(jsonValoresIniciales.strDiasMax);
					txtDiasNotif.setValue(jsonValoresIniciales.strDiasNotif);
					txtDiasNotifReact.setValue(jsonValoresIniciales.strDiasNotifReact);
					
					if(jsonValoresIniciales.sDesAutoFactVenci=="S"){
						radioFactorajeVencido1.setValue(true);
					}else{
						radioFactorajeVencido2.setValue(true);
					}
                                        
                                      chkOperaDescAutoEPO.setValue(jsonValoresIniciales.operaDescAutoEPO);
                                        
					fp.el.unmask();
				}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------------HANDLERS-------------------------------------
	var procesaActualizacion = function(opts, success, response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.Msg.alert('Mensaje informativo',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){  
					window.location = '15admotrosparamepoext.jsp';	 
			});
		}else{
			Ext.Msg.alert('Mensaje de Error',"�Error al actualizar!",function(btn){  
					window.location = '15admotrosparamepoext.jsp';	 
			});
		}
	}	
	var procesarSuccessClaveCesion = function(opts,success,response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if(Ext.util.JSON.decode(response.responseText).resultValidaCesion=="S"){
				var ventana = Ext.getCmp('winCesionDerechos1');
				if(ventana){
					ventana.hide();
				}
				Ext.Ajax.request({
					url: '15admotrosparamepoext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ActualizarDatos',
						strPorciento: jsonValoresIniciales.strPorciento,
						strPorcientoDL: jsonValoresIniciales.strPorcientoDL
					}),
					callback: procesaActualizacion
				});
			}else{
				Ext.Msg.alert('Mensaje de Error',"El usuario o la contrase�a no son v�lidas, imposible cambiar los datos.",function(btn){  
					return;	 
				});
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------------ELEMENTOS------------------------------------
	var elementosForma = [
									{
										xtype: 'numberfield',
										name: 'diasMin',
										id: 'txtDiasMin',
										fieldLabel: 'D�as Min. para descuento',
										//allowBlank: false,
										allowDecimals: false,
										allowNegative: false,
										decimalPrecision: 0,
										maxLength: 2,
										minValue: 0
									},
									{
										xtype: 'numberfield',
										name: 'diasMax',
										id: 'txtDiasMax',
										fieldLabel: 'D�as Max. para descuento',
										//allowBlank: false,
										allowDecimals: false,
										allowNegative: false,
										decimalPrecision: 0,
										maxLength: 3,
										minValue: 0
									},
									{
										xtype: 'numberfield',
										name: 'porcDescM',
										id: 'txtPorcDescM',
										fieldLabel: 'Porcentaje de descuento M.N.',
										//allowBlank: false,
										allowDecimals: false,
										allowNegative: false,
										decimalPrecision: 0,
										maxLength: 7,
										minValue: 0
									},
									{
										xtype: 'numberfield',
										name: 'porcDescD',
										id: 'txtPorcDescD',
										fieldLabel: 'Porcentaje de descuento Dol',
										//allowBlank: false,
										allowDecimals: false,
										allowNegative: false,
										decimalPrecision: 0,
										maxLength: 7
										//minValue: 0
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Opera Factoraje al Vencimiento',
										combineErrors: false,
										msgTarget: 'side',
										items: [
													{
														xtype: 'checkbox',
														name: 'operaFactVenc',
														id: 'chkOperaFactoraje',
														width: 350,
														height: 20,
														inputValue: 'S',
														msgTarget: 'side',
														handler: function(chk, val){
															var opFact = Ext.getCmp('opFact');
															if(chk.getValue()==true){
																opFact.show();
															}else{
																opFact.hide();
															}
														}
													}													
										]
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Opera Descuento Autom�tico - Factoraje al Vencimiento',
										id: 'opFact',
										combineErrors: false,
										msgTarget: 'side',
										items: [
													{
														xtype: 'radio',
														boxLabel: 'S�',
														name: 'sDesAutoFactVenci',
														id: 'radioFactorajeVencido1',
														inputValue: 'S'
													},
													{
														xtype: 'radio',
														boxLabel: 'No',
														name: 'sDesAutoFactVenci',
														id: 'radioFactorajeVencido2',
														inputValue: 'N'
													}
										]
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Opera Factoraje Distribuido',
										combineErrors: false,
										msgTarget: 'side',
										items: [
													{
														xtype: 'checkbox',
														name: 'operaFactDist',
														id: 'chkOperaFactorajeDistribuido',
														inputValue: 'S',
														width: 350,
														height: 20,
														msgTarget: 'side'
													}
										]
									},
									{
										xtype: 'numberfield',
										name: 'diasNotif',
										id: 'txtDiasNotif',
										fieldLabel: 'D�as l�mite para Notificaci�n (Cesi�n de Derechos)',
										allowBlank: true,
										allowDecimals: false,
										allowNegative: false,
										decimalPrecision: 0,
										maxLength: 3,
										minValue: 0
									},
									{
										xtype: 'numberfield',
										name: 'diasNotifReact',
										id: 'txtDiasNotifReact',
										fieldLabel: 'D�as l�mite para Notificaci�n para Reactivaci�n (Cesi�n de Derechos)',
										allowBlank: true,
										allowDecimals: false,
										allowNegative: false,
										decimalPrecision: 0,
										maxLength: 3,
										minValue: 0
									},
                                                                        {
										xtype: 'compositefield',
										fieldLabel: 'Opera Descuento Autom�tico EPO ',
										combineErrors: false,
										msgTarget: 'side',
										items: [
                                                                                    {
                                                                                        xtype: 'checkbox',
											name: 'operaDescAutoEPO',
											id: 'chkOperaDescAutoEPO',
											inputValue: 'S',
											width: 350,
											height: 20,
											msgTarget: 'side'
                                                                                    }
										]
									}
	];
//
	var fp = new Ext.FormPanel({
		id: 'forma',
		style: 'margin:0px auto;',
		title: '',
		hidden: false,
		frame: true,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		labelWidth: 250,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		width: 700,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		items: elementosForma,
		monitorValid: true,
		buttons: [
						{
							text: 'Aceptar',
							id: 'btnAceptar',
							iconCls: 'icoAceptar',
							formBind: true,
							handler: function(boton, evento){
								var txtDiasMin = Ext.getCmp('txtDiasMin');
								var txtDiasMax = Ext.getCmp('txtDiasMax');
								var txtPorcDescM = Ext.getCmp('txtPorcDescM');
								var txtPorcDescD = Ext.getCmp('txtPorcDescD');
								if(Ext.isEmpty(txtDiasMax.getValue())){
									txtDiasMax.markInvalid('Favor de teclear el No de D�as M�ximo');
									txtDiasMax.focus();
									return;
								}
								if(Ext.isEmpty(txtDiasMin.getValue())){
									txtDiasMin.markInvalid('Favor de teclear el No de D�as M�nimo');
									txtDiasMin.focus();
									return;
								}
								if(txtDiasMin.getValue()>txtDiasMax.getValue()){
									txtDiasMin.markInvalid('Los d�as m�nimos deben ser menor a d�as m�ximos.');
									txtDiasMin.focus();
									return;
								}
								if(txtDiasMin.getValue()<jsonValoresIniciales.diasMinNaf){
									txtDiasMin.markInvalid('Los d�as m�nimos deben ser mayor o igual a los establecidos por nafin:' + jsonValoresIniciales.diasMinNaf);
									txtDiasMin.focus();
									return;
								}
								if(txtDiasMax.getValue()>jsonValoresIniciales.diasMaxNaf){
									txtDiasMax.markInvalid('Los d�as m�ximos deben ser menor o igual a los establecidos por nafin:' + jsonValoresIniciales.diasMaxNaf);
									txtDiasMax.focus();
									return;
								}
								
								if(Ext.isEmpty(txtPorcDescM.getValue())){
									txtPorcDescM.markInvalid('El valor introducido no parecer ser un numero v�lido');
									txtPorcDescM.focus();
									return;
								}
								
								if(txtPorcDescM.getValue()>100){
									txtPorcDescM.markInvalid('El porcentaje de anticipo no puede ser mayor al 100%');
									txtPorcDescM.focus();
									return;
								}
								
								if(Ext.isEmpty(txtPorcDescD.getValue())){
									txtPorcDescD.markInvalid('El valor introducido no parecer ser un numero v�lido');
									txtPorcDescD.focus();
									return;
								}
								
								if(txtPorcDescD.getValue()>100){
									txtPorcDescD.markInvalid('El porcentaje de anticipo en d�lares no puede ser mayor al 100%');
									txtPorcDescD.focus();
									return;
								}
								var ventana = Ext.getCmp('winCesionDerechos1');
								var objFormLogCesion = new NE.cesion.FormLoginCesion();
								if(ventana){
									ventana.show();
								}else{
									new Ext.Window({
										layout: 'fit',
										autoHeight:true,
										x:300,
										y:100,
										width: 260,
										resiazable: false,
										modal: true,
										id: 'winCesionDerechos1',
										title: ' ',
										bodyStyle: 'text.align:center',
										items: [
											objFormLogCesion
										]
									}).show();
								}
								objFormLogCesion.setHandlerBtnAceptar(function(){
				
									Ext.Ajax.request({
										url: '15admotrosparamepoext.data.jsp',
										params: Ext.apply(objFormLogCesion.getForm().getValues(),{
											informacion: 'ConfirmacionClaves'
										}),
										callback: procesarSuccessClaveCesion
									});
								});
			
							}
						},
						{
							text: 'Limpiar',
							id: 'btnLimpiar',
							formBind: true,
							iconCls: 'icoLimpiar',
							handler: function(boton, evento){	
								
							var ventana = Ext.getCmp('winCesionDerechos1');
								var objFormLogCesion = new NE.cesion.FormLoginCesion();
								if(ventana){
									ventana.show();
								}else{
									new Ext.Window({
										layout: 'fit',
										autoHeight:true,
										x:300,
										y:100,
										width: 260,
										resiazable: false,
										modal: true,
										id: 'winCesionDerechos1',
										title: ' ',
										bodyStyle: 'text.align:center',
										items: [
											objFormLogCesion
										]
									}).show();
								}								
								objFormLogCesion.setHandlerBtnAceptar(function(){
				
									var ventana = Ext.getCmp('winCesionDerechos1');
									if(ventana){
										ventana.hide();
									}
				
									var txtPorcDescM = Ext.getCmp('txtPorcDescM');
									var txtPorcDescD = Ext.getCmp('txtPorcDescD');
									var chkOperaFactoraje = Ext.getCmp('chkOperaFactoraje');
									var chkOperaFactorajeDistribuido = Ext.getCmp('chkOperaFactorajeDistribuido');
									var txtDiasMin = Ext.getCmp('txtDiasMin');
									var txtDiasMax = Ext.getCmp('txtDiasMax');
									var txtDiasNotif = Ext.getCmp('txtDiasNotif');
									var txtDiasNotifReact = Ext.getCmp('txtDiasNotifReact');
									var radioFactorajeVencido1 = Ext.getCmp('radioFactorajeVencido1');
									var radioFactorajeVencido2 = Ext.getCmp('radioFactorajeVencido2');
					
									txtPorcDescM.setValue("");
									txtPorcDescD.setValue("");
									chkOperaFactoraje.setValue("");
									chkOperaFactorajeDistribuido.setValue("");
									txtDiasMin.setValue("");
									txtDiasMax.setValue("");
									txtDiasNotif.setValue("");
									txtDiasNotifReact.setValue("");
																
								});
								
							}
						}
		]
	});
//-----------------------------------PRINCIPAL----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
					fp
		]
	});
	
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '15admotrosparamepoext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});
});
