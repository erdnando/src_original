<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*,
		com.netro.exception.*,
		com.netro.afiliacion.*,
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.seguridadbean.*,
		com.netro.model.catalogos.*,
		com.netro.descuento.* "
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar = "";
	
	ParametrosDescuento parametrosBean = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
	
	if(informacion.equals("valoresIniciales")){
		try{
			String diasMinNaf = "";
			String diasMaxNaf = "";
			String porciento = "";
			String strDiasMin = "";
			String strDiasMax = "";
			String strPorcientoActual = "";
			String strPorcientoActualDL = "";
			String operaFactVenc = "";
			String operaFactDist = "";
			String sDesAutoFactVenci = "";
			String strDiasNotif = "";
			String strDiasNotifReact = "";
			String strPorciento = "";
			String strPorcientoDL = "";
			String operaDescAutoEPO ="";
			String flagfvenc = "0";
			
			HashMap map = parametrosBean.datosOtrosParametros (iNoCliente, flagfvenc);
			if(map!=null){
				strPorciento = String.valueOf(map.get("strPorciento"));
				strPorcientoDL = String.valueOf(map.get("strPorcientoDL"));
				diasMinNaf = String.valueOf(map.get("diasMinNaf"));
				diasMaxNaf = String.valueOf(map.get("diasMaxNaf"));
				porciento = String.valueOf(map.get("porciento"));
				strDiasMin = String.valueOf(map.get("strDiasMin"));
				strDiasMax = String.valueOf(map.get("strDiasMax"));
				strPorcientoActual = String.valueOf(map.get("strPorcientoActual"));
				strPorcientoActualDL = String.valueOf(map.get("strPorcientoActualDL"));
				operaFactVenc = String.valueOf(map.get("operaFactVenc"));
				operaFactDist = String.valueOf(map.get("operaFactDist"));
				sDesAutoFactVenci = String.valueOf(map.get("sDesAutoFactVenci"));
				strDiasNotif = String.valueOf(map.get("strDiasNotif"));
				strDiasNotifReact = String.valueOf(map.get("strDiasNotifReact"));
                                operaDescAutoEPO = String.valueOf(map.get("operaDescAutoEPO"));
			}
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("strPorciento", strPorciento);
			jsonObj.put("strPorcientoDL", strPorcientoDL);
			jsonObj.put("diasMinNaf",diasMinNaf);
			jsonObj.put("diasMaxNaf",diasMaxNaf);
			jsonObj.put("porciento",porciento);
			jsonObj.put("strDiasMin",strDiasMin);
			jsonObj.put("strDiasMax",strDiasMax);
			jsonObj.put("strPorcientoActual",strPorcientoActual);
			jsonObj.put("strPorcientoActualDL",strPorcientoActualDL);
			jsonObj.put("operaFactVenc",operaFactVenc);
			jsonObj.put("operaFactDist",operaFactDist);
			jsonObj.put("sDesAutoFactVenci",sDesAutoFactVenci);
			jsonObj.put("strDiasNotif",strDiasNotif);
			jsonObj.put("strDiasNotifReact",strDiasNotifReact);
                        jsonObj.put("operaDescAutoEPO",operaDescAutoEPO);
                        
			infoRegresar = jsonObj.toString();
		}catch(Exception e){
			throw new AppException("Error al intentar mostrar los parámetros");
		}
	}else if(informacion.equals("ActualizarDatos")){
		try{
			HashMap map = new HashMap();
			String porcentajeM = (request.getParameter("porcDescM")!=null)?request.getParameter("porcDescM"):"";
			String porcentajeD = (request.getParameter("porcDescD")!=null)?request.getParameter("porcDescD"):"";
			String operaFactVenc = (request.getParameter("operaFactVenc")!=null)?request.getParameter("operaFactVenc"):"N";
			String operaFactDist = (request.getParameter("operaFactDist")!=null)?request.getParameter("operaFactDist"):"N";
			String diasMin = (request.getParameter("diasMin")!=null)?request.getParameter("diasMin"):"";
			String diasMax = (request.getParameter("diasMax")!=null)?request.getParameter("diasMax"):"";
			String diasNotif = (request.getParameter("diasNotif")!=null)?request.getParameter("diasNotif"):"";
			String diasNotifReact = (request.getParameter("diasNotifReact")!=null)?request.getParameter("diasNotifReact"):"";
			String sDesAutoFactVenci = (request.getParameter("sDesAutoFactVenci")!=null)?request.getParameter("sDesAutoFactVenci"):"N";
			String strPorciento = (request.getParameter("strPorciento")!=null)?request.getParameter("strPorciento"):"--";
			String strPorcientoDL = (request.getParameter("strPorcientoDL")!=null)?request.getParameter("strPorcientoDL"):"--";
                        String operaDescAutoEPO = (request.getParameter("operaDescAutoEPO")!=null)?request.getParameter("operaDescAutoEPO"):"N";
                        
			map = parametrosBean.actualizaOtrosParametros(porcentajeM, porcentajeD, operaFactVenc, operaFactDist, diasMin, diasMax, diasNotif, diasNotifReact, iNoCliente, sDesAutoFactVenci, operaDescAutoEPO);
			String bandera = String.valueOf(map.get("bandera"));
			
			if("0".equals(bandera)){
				infoRegresar = "{\"success\":true,\"mensaje\":\"Error: Los datos no han sido actualizados.\"}";
			}
			else if("1".equals(bandera)){
				String aforo = String.valueOf(map.get("strAforo"));
				String aforoDL = String.valueOf(map.get("strAforoDL"));
				session.setAttribute("strAforo",aforo);
				session.setAttribute("strAforoDL",aforoDL);
				
				infoRegresar = "{\"success\":true,\"mensaje\":\"Los datos han sido actualizados correctamente.\"}";
			}
			else if("2".equals(bandera)){
				infoRegresar = "{\"success\":true,\"mensaje\":\"El porcentaje de anticipo no puede ser mayor al " + strPorciento + "% \"}";
			}
			else if("4".equals(bandera)){
				infoRegresar = "{\"success\":true,\"mensaje\":\"Imposible modificar el Tipo Factoraje. Hay documentos operables.\"}";
			}
			else if("5".equals(bandera)){
				infoRegresar = "{\"success\":true,\"mensaje\":\"El porcentaje de anticipo en dólares no puede ser mayor al " + strPorcientoDL + "% \"}";
			}
		}catch(Exception e){
			throw new AppException("Error al actualizar los campos.",e);
		}
	}
	else if(informacion.equals("ConfirmacionClaves")){
		try{
			com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			
			String cesionUser 	= (request.getParameter("cesionUser")==null)?"":request.getParameter("cesionUser");
			String cesionPassword 	= (request.getParameter("cesionPassword")==null)?"":request.getParameter("cesionPassword");
			try{
				BeanSegFacultad.validarUsuario(strLogin, cesionUser, cesionPassword);
				infoRegresar = infoRegresar = "{\"success\":true,\"resultValidaCesion\":\"S\"}";
			}catch(Exception e){
				infoRegresar = infoRegresar = "{\"success\":true,\"resultValidaCesion\":\"N\"}";
			}	
		}catch(Exception e){
			throw new AppException("Error al validar usuario y contraseña.",e);
		}
	}
%>
<%=infoRegresar%>