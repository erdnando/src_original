	
Ext.onReady(function() {
var ahora=new Date();
var mes = (parseInt(ahora.getMonth()) + 1)<10?0+(parseInt(ahora.getMonth()) + 1).toString():(parseInt(ahora.getMonth()) + 1).toString();
var diaAct=(parseInt(ahora.getDate()) )<10?0+(parseInt(ahora.getDate()) ).toString():(parseInt(ahora.getDate()) ).toString();
//alert(ahora.getDate()+'/'+mes.toString()+'/'+ahora.getFullYear());
//var mes = (parseInt(ahora.getMonth()) + 1)<10?0+(parseInt(ahora.getMonth()) + 1).toString():(parseInt(ahora.getMonth()) + 1).toString();
var paramSubmit;
TaskLocation = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
]);
//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT

	var procesarCatalogo=function(store, arrRegistros, opts)
	{
	if(store.getTotalCount() > 0) {
	
	store.insert(0,new TaskLocation({ 
    clave: "TODAS", 
    descripcion: "Seleccione Todas", 
    loadMsg: ""})); 
	 
	store.commitChanges(); 

	}
	
	}
	var procesarConsultaData = function(store, arrRegistros, opts) {
	
		var fp = Ext.getCmp('forma');
			
		if (arrRegistros != null) {
			
			if (!gridGeneral.isVisible()) {
				gridGeneral.show();				
				}
			var btnGenerarArchivo = Ext.getCmp('btnGenerarCSV');
			var btnBajarArchivo = Ext.getCmp('btnBajarCSV');		

			var el = gridGeneral.getGridEl();
			
			if(store.getTotalCount() > 0) {
			

				btnGenerarArchivo.enable();
				btnBajarArchivo.hide();
				el.unmask();
				
			} else {
				btnGenerarArchivo.disable();
				btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarCSV');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarCSV');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT

	var catalogoProyectoData = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15admparamtasopExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoTasaOper'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
		load: procesarCatalogo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var consultaData =  new Ext.data.GroupingStore({
		root : 'registros',
		url : '15admparamtasopExt.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
	fields: [
			{name: 'CD_NOMBRE'},
			{name: 'IC_TASA'},
			{name: 'FN_VALOR'},
			{name: 'FECHA'}
		]
		}),
		groupField: 'FECHA',
		sortInfo:{field: 'FECHA', direction: "ASC"},
			
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			/*beforeLoad:	{
				fn: function(store, options){
					var noEpo = Ext.getCmp('cmbEpo');
					Ext.apply(options.params, {no_epo: noEpo.value});
				}
			},*/
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
		
	});

 


//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
	var elementosFecha = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_Inicio',
					id: 'df_consultaMin',
					allowBlank: false,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_consultaMax',
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					formBind: true,
					value:diaAct.toString()+'/'+mes.toString()+'/'+ahora.getFullYear()
					},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_Fin',
					id: 'df_consultaMax',
					allowBlank: false,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_consultaMin',
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					value:diaAct.toString()+'/'+mes.toString()+'/'+ahora.getFullYear()

				}
			]
		}
	];

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'idTasa',
			id: 'cmbProyecto',
			hiddenName : 'idTasa',
			fieldLabel: 'Tasa',
			emptyText: 'Seleccione una Tasa',
			store: catalogoProyectoData,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			allowBlank : false,
			minChars : 1,
			editable:false,
			tpl : NE.util.templateMensajeCargaCombo
		}
	];

	var fp = {
		xtype: 'form',
		id: 'forma',
		width: 600,
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[elementosFecha, elementosForma],
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					
					consultaData.load({
							params: Ext.apply(paramSubmit,{
							operacion: 'Consultar'							
						})
					});
				} //fin handler
			}
		]
	};//FIN DE LA FORMA
	
	
	//CREA EL GRID PARA LA OPCION DE VER TASAS 
		var gridGeneral = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridGeneral',
		hidden: true,
		columns: [		
			{
				header: 'Fecha:',
				tooltip: 'Fecha:',
				dataIndex: 'FECHA',
				sortable: true,				
				align: 'left',
				width: 230,
					hidden: true
			},			
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'CD_NOMBRE',//IC_TASA
				sortable: true,
				width: 300,
				resizable: true,
				hidden: false, 	
				align: 'left'	
			},						
			{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'FN_VALOR',
				sortable: true,
				width: 230,
				resizable: true,
				hidden: false,
				align: 'center'	
			}
			
		],	
		view: new Ext.grid.GroupingView({
            forceFit:false,
            groupTextTpl: '{text}'					 
        }),
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 400,
		width: 550,
		title: '',
		frame: true,
			bbar: {
			xtype: 'toolbar',			
			items: [				
				
					{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSV',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						Ext.Ajax.request({
							url: '15admparamtasopExt.data.jsp',
							params: Ext.apply(paramSubmit,{
								tipoArchivo:'CSV',
								informacion: 'GenerarArchivo'								
							}),	
							callback: procesarSuccessFailureGenerarArchivo
						});			
					}
				},
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				}
			]
		}
		
	});//FIN DEL GRID	
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(5),
			gridGeneral
		]
	});
		catalogoProyectoData.load();
	});
