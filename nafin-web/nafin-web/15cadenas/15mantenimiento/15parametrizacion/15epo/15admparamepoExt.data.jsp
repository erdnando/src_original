<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,com.netro.parametrosgrales.*,
		com.netro.model.catalogos.CatalogoSimple"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar = "";

if(informacion.equals("valoresIniciales")){

}else if(informacion.equals("CatalogoProducto")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_producto_nafin");
	cat.setCampoDescripcion("ic_nombre");
	cat.setTabla("comcat_producto_nafin");
	cat.setValoresCondicionNotIn("3", Integer.class);
	cat.setOrden("ic_producto_nafin");
	
	List elementos = cat.getListaElementos();
	JSONArray jsonArr = new JSONArray();
	Iterator it = elementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				String clave = ec.getClave();
				if(sesEpoInternacional.equals("S") && clave.equals("1")){
					ec.setDescripcion("Nafinet");
					jsonArr.add(JSONObject.fromObject(ec));
					break;
				}
				if(!sesEpoInternacional.equals("S")){
					jsonArr.add(JSONObject.fromObject(ec));
				}
		}
	}

	infoRegresar = "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

}else if(informacion.equals("ObtieneDinamicos")){

	JSONObject jsonObj = new JSONObject();
	ParametrosGrales gralesBean = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);

	String sel_tipo = (request.getParameter("_sel_tipo")!=null)?request.getParameter("_sel_tipo"):"";
	String sel_producto = (request.getParameter("_sel_producto")!=null)?request.getParameter("_sel_producto"):"";
	
	if (sel_tipo != null && sel_tipo.equals("1")){

		Registros reg = gralesBean.getCamposAdicionalesEpo(iNoEPO, sel_producto);
		if (reg != null){
			jsonObj.put("regsAdicionales", reg.getJSONData() );
		}
		jsonObj.put("contador_fin", "5");
		jsonObj.put("success", new Boolean(true));
	
	}else if (sel_tipo != null && sel_tipo.equals("2")){
	
		Registros reg = gralesBean.getCamposAdicionalesDetalleEpo(iNoEPO, sel_producto);
		if (reg != null){
			jsonObj.put("regsDetalle", reg.getJSONData() );
		}
		jsonObj.put("contador_fin", "10");
		jsonObj.put("success", new Boolean(true));

	}else {
		jsonObj.put("success", new Boolean(true));
	}
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("saveDinamicos")){

	ParametrosGrales gralesBean = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
	
	Vector vecNombre= new Vector();
	Vector vecValor= new Vector();
	Hashtable datos = new Hashtable();
	int i = 0;
	JSONObject jsonObj = new JSONObject();
	try{

		for(Enumeration e =request.getParameterNames(); e.hasMoreElements();){
			vecNombre.addElement(e.nextElement());
			String nvo_dato=(String)vecNombre.elementAt(i);
			vecValor.addElement(request.getParameter(nvo_dato));
			String valor_dato=(String)vecValor.elementAt(i);
			datos.put(nvo_dato,valor_dato);
			i++;
		}

		gralesBean.actualizaCamposAdicionales(iNoEPO,datos);

		jsonObj.put("success", new Boolean(true));

	} catch(Exception e) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al actualizar campos Adicionales " + e);
	}

	infoRegresar = jsonObj.toString();

}
%>
<%=infoRegresar%>