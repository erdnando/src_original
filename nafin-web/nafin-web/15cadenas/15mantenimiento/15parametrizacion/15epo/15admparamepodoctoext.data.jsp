<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*,
		com.netro.exception.*,
		com.netro.afiliacion.*,
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.parametrosgrales.*,
		com.netro.model.catalogos.*,
		com.netro.descuento.* "
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar = "";
	
	if(informacion.equals("CatalogoDocumento")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_clase_docto");
		cat.setCampoClave("ic_clase_docto");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setOrden("ic_clase_docto"); 
		infoRegresar = cat.getJSONElementos();
	}
	else if(informacion.equals("CatalogoProducto")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_producto_nafin");
		cat.setCampoClave("ic_producto_nafin");
		cat.setCampoDescripcion("ic_nombre");
		infoRegresar = cat.getJSONElementos();
	}
	else if(informacion.equals("Consulta")){
		com.netro.descuento.ConsDocumentoXProducto paginador = new com.netro.descuento.ConsDocumentoXProducto();
		paginador.setClaveEPO(iNoCliente);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		try{
			Registros reg	= queryHelper.doSearch();
			infoRegresar	=
				"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";		
		}catch(Exception e){
			throw new AppException("Error en la consulta.",e);
		}
	}
	else if(informacion.equals("Actualizar")){
		
		ParametrosGrales parametrosBean = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
		String producto = (request.getParameter("cboProd")!=null)?request.getParameter("cboProd"):"";
		String documento = (request.getParameter("cboDocto")!=null)?request.getParameter("cboDocto"):"";
		int tipoAccion = parametrosBean.actualizaDocumentoXProducto(iNoCliente, producto, documento);
		if(tipoAccion == 0){
			infoRegresar = "{\"success\":true,\"mensaje\":\"Error: Los datos no han sido actualizados.\"}";
		}
		else if(tipoAccion == 1){
			infoRegresar = "{\"success\":true,\"mensaje\":\"Se dió de alta el documento y el producto seleccionados.\"}";
		}
		else if(tipoAccion == 2){
			infoRegresar = "{\"success\":true,\"mensaje\":\"Se actualizó el documento y el producto seleccionados.\"}";
		}
	}
%>
<%=infoRegresar%>