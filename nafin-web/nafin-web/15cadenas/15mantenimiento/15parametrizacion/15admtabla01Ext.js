var band = true;
var band2 = true;
var bg = true;

Ext.onReady(function() {

	var fpBotones = new Ext.Container({		//Para el CREAR TABLA
		layout: 'table',
		id: 'fpBotones',	
		hidden: true,
		width:	'300',
		heigth:	'auto',
		style: 'margin: 0 auto auto 542;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: 'Crear Tabla',	
				iconCls:	'icoAceptar',
				id: 'btnCreaTabla',					
				handler: function() {
					accionConsulta("CREARTABLA", null);
				}
			},
			{
				xtype: 'box',
				width: 10
			},
			{
				xtype: 'button',
				text: 'Cancelar',	
				iconCls:	'icoRechazar',
				id: 'btnCancel',					
				handler: function() {
					fp.show();
					gridFijos.hide();
					gridEditables.hide();
					gridDetalle.hide();
					fpBotones.hide();
					accionConsulta("LIMPIAR", null);
				}
			}	
		]
	});	
	
	var fpBotonesModT = new Ext.Container({		//Para el MODIFICAR TABLA
		layout: 'table',
		id: 'fpBotonesMod',	
		hidden: true,
		width:	'300',
		heigth:	'auto',
		style: 'margin: 0 auto auto 470;',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: 'Limpiar',	
				iconCls:	'icoLimpiar',
				id: 'btnLimpiarDatTab',					
				handler: function() {
					//accionConsulta("CREARTABLA", null);
				}
			},
			{
				xtype: 'box',
				width: 8
			},
			{
				xtype: 'button',
				text: 'Cancelar',	
				iconCls:	'icoRechazar',
				id: 'btnCancelModTab',					
				handler: function() {
					fpModTab.getForm().reset();
					fpModTab.hide();
					gridFijosMod.hide();
					gridEditablesMod.hide();
					gridDetalleMod.hide();
					fpBotonesModT.hide();
					grid.show();
				}
			},
			{
				xtype: 'box',
				width: 8
			},
			{
				xtype: 'button',
				text: 'Modificar',	
				iconCls:	'icoAceptar',
				id: 'btnModificarTab',					
				handler: function() {
					accionConsulta("MODIFICATABLA", null);
				}
			}	
		]
	});	

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - - -

	function procesarSuccessFailureDeleteImg(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var gr = Ext.getCmp("gridConsulta2");
			gr.el.unmask();
			accionConsulta("CONSULTAR", null);	
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarSuccessFailureDeleteTabla(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var gr = Ext.getCmp("gridConsulta");
			gr.el.unmask();
			accionConsulta("CONSULTAR", null);	
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarAgregarFilaFijos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridDinEdit.destroy();
			var infoR = Ext.util.JSON.decode(response.responseText);
			var nPublicacion = infoR.num_pub;
			var nom_tabla = "Ninguna";
			Ext.Ajax.request({
				url: '15admtabla01Ext.data.jsp',
				params: 
						{
							informacion: 'ModificarTabIndiv',
							nom_tabla: nom_tabla,
							nPublicacion: nPublicacion
						},				
				callback: procesarModificaDatosTabla											  
			});
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarAgregarFilaDetalle(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridDinEditDet.destroy();
			var infoR = Ext.util.JSON.decode(response.responseText);
			var nPublicacion = infoR.num_pub;
			var num_detalle = infoR.num_detalle ;
			var nom_fijo = infoR.nom_fijo ;
			Ext.Ajax.request({
								url: '15admtabla01Ext.data.jsp',
								params:{
									informacion: 'ObtenTablaDetalle',
									nPublicacion: nPublicacion,
									num_detalle: num_detalle,
									nom_fijo: nom_fijo
								},
								callback: procesarTablaDetalle
							});
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarEliminarFilaDetalle(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridDinEditDet.destroy();
			var infoR = Ext.util.JSON.decode(response.responseText);
			var nPublicacion = infoR.num_pub;
			var num_detalle = infoR.num_detalle ;
			var nom_fijo = infoR.nom_fijo ;
			Ext.Ajax.request({
								url: '15admtabla01Ext.data.jsp',
								params:{
									informacion: 'ObtenTablaDetalle',
									nPublicacion: nPublicacion,
									num_detalle: num_detalle,
									nom_fijo: nom_fijo
								},
								callback: procesarTablaDetalle
							});
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarSalvarFilaDetalle(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridDinEditDet.destroy();
			band2 = true;
			var infoR = Ext.util.JSON.decode(response.responseText);
			var nPublicacion = infoR.num_pub;
			var num_detalle = infoR.num_detalle ;
			var nom_fijo = infoR.nom_fijo ;
			Ext.Ajax.request({
								url: '15admtabla01Ext.data.jsp',
								params:{
									informacion: 'ObtenTablaDetalle',
									nPublicacion: nPublicacion,
									num_detalle: num_detalle,
									nom_fijo: nom_fijo
								},
								callback: procesarTablaDetalle
							});
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarEliminarFilaFijos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridDinEdit.destroy();
			var infoR = Ext.util.JSON.decode(response.responseText);
			var nPublicacion = infoR.num_pub;
			var nom_tabla = "Ninguna";
			Ext.Ajax.request({
				url: '15admtabla01Ext.data.jsp',
				params: 
						{
							informacion: 'ModificarTabIndiv',
							nom_tabla: nom_tabla,
							nPublicacion: nPublicacion
						},				
				callback: procesarModificaDatosTabla											  
			});
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarModificarFilaFijos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridDinEdit.destroy();
			band = true;
			var infoR = Ext.util.JSON.decode(response.responseText);
			var nPublicacion = infoR.num_pub;
			var nom_tabla = " ";
			Ext.Ajax.request({
				url: '15admtabla01Ext.data.jsp',
				params: 
						{
							informacion: 'ModificarTabIndiv',
							nom_tabla: nom_tabla,
							nPublicacion: nPublicacion
						},				
				callback: procesarModificaDatosTabla											  
			});
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarEliminarTodos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridDinEdit.destroy();
			band = true;
			var infoR = Ext.util.JSON.decode(response.responseText);
			var nPublicacion = infoR.num_pub;
			var nom_tabla = " ";
			Ext.Ajax.request({
				url: '15admtabla01Ext.data.jsp',
				params: 
						{
							informacion: 'ModificarTabIndiv',
							nom_tabla: nom_tabla,
							nPublicacion: nPublicacion
						},				
				callback: procesarModificaDatosTabla											  
			});
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarEliminarTodosDetalle(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridDinEditDet.destroy();
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarSuccessFailureCreaTabla(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			gridFijos.hide();
			gridEditables.hide();
			gridDetalle.hide();
			fpBotones.hide();
			var nom_tabla = infoR.nom_tabla;
			var nPublicacion = infoR.miNumPublicacion;
			Ext.Ajax.request({
				url: '15admtabla01Ext.data.jsp',
				params: 
						{
							informacion: 'ModificarTabIndiv',
							nom_tabla: nom_tabla,
							nPublicacion: nPublicacion
						},				
				callback: procesarModificaDatosTabla											  
			});
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarSuccessFailureModificaTabla(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			fpModTab.hide();
			gridFijosMod.hide();
			gridEditablesMod.hide();
			gridDetalleMod.hide();
			fpBotonesModT.hide();
			grid.show();
			var gr = Ext.getCmp("gridConsulta");
			gr.el.unmask();
			accionConsulta("CONSULTAR", null);	
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaRegistros = function(store, registros, opts){
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		forma.hide();
		if (registros != null) {
			var grid  = Ext.getCmp('gridConsulta');
			if (!grid.isVisible()) {
				grid.show();
			}
			Ext.getCmp('barraPaginacion').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				el.unmask();
			} 
		}
	}
	
	var procesarConsultaRegistrosIm = function(store, registros, opts){
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		forma.hide();
		if (registros != null) {
			var grid  = Ext.getCmp('gridConsulta2');
			if (!grid.isVisible()) {
				grid.show();
			}
			Ext.getCmp('barraPaginacion2').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				el.unmask();
			} 
		}
	}

	var myValidFn = function(v) {
		var myRegex = /^.+\.([jJ][pP][gG])$/;
		return myRegex.test(v);
	}
		
	Ext.apply(Ext.form.VTypes, {
		archivojpg 		: myValidFn,
		archivojpgText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): JPG.'
	});
	
	var myValidFnTxt = function(v) {
		var myRegex = /^.+\.([tT][xX][tT])$/;
		return myRegex.test(v);
	}
	
	Ext.apply(Ext.form.VTypes, {
		archivotxt 		: myValidFnTxt,
		archivotxtText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): TXT.'
	});
	
	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);
	
	var procesarCatCadenasData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var claveCad = Ext.getCmp('_cmb_cad');
			
			var newRecord1 = new recordType({
				clave:'',
				descripcion:'Seleccionar...'
			});
			
			store.insert(0,newRecord1);
			store.commitChanges();
			claveCad.setValue('');
				
			var newRecord = new recordType({
				clave:'T',
				descripcion:'Todos'
			});
			
			store.insert(1,newRecord);
			store.commitChanges();
			claveCad.setValue('T');
			
			Ext.getCmp('_cmb_cad').setValue('');

		}
  }
	
	var procesarCatalogoTipoPublicacion = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) {
			Ext.getCmp('_cmb_mant').setValue('1');
		}
	}
	
	//GENERAR ARCHIVO DE CSV DE LOS GRIDS DINAMICOS
	function procesarSuccessFailureGenerarCSV(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	//Procesa el retorno de los datos consultados para la modificacion de campos de tabla
	var procesarModificaDatosTabla = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			grid.hide();
			var tabla = jsonObj.msNombreTabla;
			var nombres = jsonObj.liNom;
			var tipos = jsonObj.liTipo;
			var longitudes = jsonObj.liLong;
			
			var fld = [];
			var cols = {};
			cols['name'] = 'PROVDIST';
			fld.push(cols);
			var cols = {};
			cols['name'] = 'NOPUBLIC';
			fld.push(cols);
			var cols = {};
			cols['name'] = 'ID';
			fld.push(cols);
			var cols = {};
			cols['name'] = 'EDIT';
			fld.push(cols);
			for(var i =0; i<nombres.length; i++){
				var cols = {};
				cols['name'] = nombres[i];
				fld.push(cols);
			}
			
			var consultaTablasIndiv =new Ext.data.JsonStore({
				xtype: 'jsonstore',
				id:'consultaTablasIndiv',
				root: 'registros',
				totalProperty: 'total',
				messageProperty: 'msg',
				autoLoad: false,
				fields: fld
			});
			
			consultaTablasIndiv.loadData(jsonObj.columnasRecords);
			var tot = jsonObj.total;
			
			var col = [];
			var this_col = {};
			this_col['header'] = '';
			this_col['dataIndex'] = 'NOPUBLIC';
			this_col['align'] = 'center';
			this_col['hidden'] = true;		
			col.push(this_col);
			var this_col = {};
			this_col['header'] = '';
			this_col['dataIndex'] = 'ID';
			this_col['align'] = 'center';
			this_col['hidden'] = true;		
			col.push(this_col);
			var this_col = {};
			this_col['header'] = '';
			this_col['dataIndex'] = 'EDIT';
			this_col['align'] = 'center';
			this_col['hidden'] = true;		
			col.push(this_col);
			var this_col = {};
			this_col['header'] = 'Proveedor Distribuidor';
			this_col['width'] = 200;
			this_col['dataIndex'] = 'PROVDIST';
			this_col['align'] = 'center';
			this_col['sortable'] = false;
			this_col['editor'] = {
												xtype: 'textfield',
												maxLength:20	 
											};
			this_col['renderer'] = function(value,metadata,registro, rowIndex, colIndex){
												if(registro.data['EDIT']=="S" || registro.data['EDIT']=="A")
													return NE.util.colorCampoEdit(value,metadata,registro);
												else
													return value;
											};
			col.push(this_col);
			
			for(var i =0; i<nombres.length; i++){		//Se generan las columnas dinamicas del grid
				var this_col = {};
				if(tipos[i]=='seleccion'){
					this_col['xtype'] = 'checkcolumn';
					this_col['header'] = nombres[i];
					this_col['dataIndex'] = nombres[i];
				}else{
					this_col['header'] = nombres[i];
					this_col['width'] = 160;
					this_col['dataIndex'] = nombres[i];
					this_col['sortable'] = false;
					this_col['align'] = 'center';
					if(tipos[i]=='alfanumerico' || tipos[i]=='numerico' || tipos[i]=='hora'){
						if(tipos[i]=='hora'){
							this_col['editor'] = {
														xtype: 'textfield',
														maxLength:5	 
													};
						}
						else{
							this_col['editor'] = {
														xtype: 'textfield',
														maxLength: parseInt(longitudes[i])	 
													};
						}
						this_col['renderer'] = function(value,metadata,registro, rowIndex, colIndex){
												if(registro.data['EDIT']=="S" || registro.data['EDIT']=="A")
													return NE.util.colorCampoEdit(value,metadata,registro);
												else
													return value;
												};
					}else if(tipos[i]=='fecha'){
						this_col['editor'] = {
													xtype: 'datefield',
													name: 'txtFechaAplic',
													allowBlank: false,	
													format:'d/m/Y',
													startDay: 0,
													width: 100,
													maxLength:10,	
													msgTarget: 'side',			
													margins: '0 20 0 0'	 
												};
						this_col['renderer'] = function(value,metadata,registro, rowIndex, colIndex){
													if(registro.data['EDIT']=="S" || registro.data['EDIT']=="A")
														return NE.util.colorCampoEdit(value,metadata,registro);
													else
														return value;
												};
					}
				}
				col.push(this_col);
			}
			
			var this_col = {};
			this_col['xtype'] = 'actioncolumn';
			this_col['header'] = 'Detalle';
			this_col['width'] = 100;
			this_col['align'] = 'center';
			this_col['items'] = [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['EDIT']=="N"){
								this.items[0].tooltip = 'Tabla Detalle';
								return 'icoLupa';		
							}
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
							var registro = gridDinEdit.getStore().getAt(rowIndex);
							if(!bg)
									gridDinEditDet.destroy();
							var nPublicacion = registro.get('NOPUBLIC');
							var num_detalle = registro.get('ID');
							Ext.Ajax.request({
								url: '15admtabla01Ext.data.jsp',
								params:{
									informacion: 'ObtenTablaDetalle',
									nPublicacion: nPublicacion,
									num_detalle: num_detalle,
									nom_fijo: nombres[0]
								},
								callback: procesarTablaDetalle
							});
						}
					}
				]	;
			col.push(this_col);
			
			var this_col = {};
			this_col['xtype'] = 'actioncolumn';
			this_col['header'] = 'Datos';
			this_col['width'] = 100;
			this_col['align'] = 'center';
			this_col['items'] = [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['EDIT']=="S"){
								this.items[0].tooltip = 'Agregar';
								return 'icoAceptar';				
							}
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
							b = true;
							var campo = new Array();
							var tipo_campo = new Array();
							var registro = gridDinEdit.getStore().getAt(rowIndex);
							var numEpo = Ext.getCmp('_cmb_cad').getValue();
							var num_pub = registro.get('NOPUBLIC');
							if(registro.get('PROVDIST')==""){
								Ext.MessageBox.alert("Aviso", " Te hacen falta datos ");
								b = false;
							}
							for(var i =0; i<nombres.length; i++){
								if(registro.get(nombres[i]) == ""){
									Ext.MessageBox.alert("Aviso", " Te hacen falta datos ");
									b=false;
									break;
								}
							}
							if(b){
								campo[0]= registro.get('PROVDIST');
								for(var i =0; i<nombres.length; i++){
									campo[i+1] = registro.get(nombres[i]);
								}
								tipo_campo[0]="alfanumerico";
								for(var j=0; j<tipos.length; j++){
									tipo_campo[j+1] = tipos[j];
								}
								Ext.Ajax.request({
										url: '15admtabla01Ext.data.jsp',
										params: 
											{
													informacion: 'AgregaDatFijos',
													numEpo: numEpo,
													num_pub: num_pub,
													campo: campo,
													tipo_campo: tipo_campo
											},				
										callback: procesarAgregarFilaFijos										  
								});	
							}
						}
					},{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['EDIT']=="N"){
								this.items[1].tooltip = 'Modificar';
								return 'icoModificar';	
							}
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
							var registro = gridDinEdit.getStore().getAt(rowIndex);
							if(band){
								var nomProv = registro.set('EDIT',"A");
								band = false;
								registro.commit();
								var reg = gridDinEdit.getStore().getAt(gridDinEdit.getStore().getTotalCount());
								reg.set('EDIT', "R");
								reg.commit();
							}
						}
					},{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['EDIT']=="N"){
								this.items[2].tooltip = 'Eliminar';
								return 'icoEliminar';			
							}
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
							
							Ext.Msg.confirm('Confirmaci�n', 'Desea borrar todos los registros con sus relaciones?', function(btn){
										if(btn=='yes'){
											var campo = new Array();
											var tipo_campo = new Array();
											var registro = gridDinEdit.getStore().getAt(rowIndex);
											var numEpo = Ext.getCmp('_cmb_cad').getValue();
											var num_pub = registro.get('NOPUBLIC');
											var id_campo = registro.get('ID');
											campo[0]= registro.get('PROVDIST');
											for(var i =0; i<nombres.length; i++){
												campo[i+1] = registro.get(nombres[i]);
											}
											tipo_campo[0]="alfanumerico";
											for(var j=0; j<tipos.length; j++){
												tipo_campo[j+1] = tipos[j];
											}
											Ext.Ajax.request({
														url: '15admtabla01Ext.data.jsp',
														params: 
															{
																	informacion: 'EliminarDatFijos',
																	numEpo: numEpo,
																	num_pub: num_pub,
																	campo: campo,
																	tipo_campo: tipo_campo,
																	id_campo: id_campo
															},				
														callback: procesarEliminarFilaFijos										  
											});
										}
									})
						}
					},{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['EDIT']=="A"){
								this.items[3].tooltip = 'Salvar';
								return 'icoAgregar';			
							}
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
							var registro = gridDinEdit.getStore().getAt(rowIndex);
							b = true;
							var campo = new Array();
							var tipo_campo = new Array();
							var numEpo = Ext.getCmp('_cmb_cad').getValue();
							var num_pub = registro.get('NOPUBLIC');
							if(registro.get('PROVDIST')==""){
								Ext.MessageBox.alert("Aviso", " Te hacen falta datos ");
								b = false;
							}
							for(var i =0; i<nombres.length; i++){
								if(registro.get(nombres[i]) == ""){
									Ext.MessageBox.alert("Aviso", " Te hacen falta datos ");
									b=false;
								}
							}
							if(b){
								var id_campo = registro.get('ID');
								campo[0]= registro.get('PROVDIST');
								for(var i =0; i<nombres.length; i++){
									campo[i+1] = registro.get(nombres[i]);
								}
								tipo_campo[0]="alfanumerico";
								for(var j=0; j<tipos.length; j++){
									tipo_campo[j+1] = tipos[j];
								}
								Ext.Ajax.request({
										url: '15admtabla01Ext.data.jsp',
										params: 
											{
													informacion: 'SalvarDatFijos',
													numEpo: numEpo,
													num_pub: num_pub,
													campo: campo,
													tipo_campo: tipo_campo,
													id_campo: id_campo
											},				
										callback: procesarModificarFilaFijos										  
								});
							}
						}
					}
				];
			col.push(this_col);
			
			gridDinEdit = new Ext.grid.EditorGridPanel({
				title: 'Modificacion de la tabla '+"'"+tabla+"'",
				store: consultaTablasIndiv,
				width: 900,  
				height:280,
				clicksToEdit:1,
				style: 'margin:0 auto;',
				enableDD: false,       
			   enableSort : false,
			   enableHdMenu:false,
			   columns:col,
				listeners: {
						beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
							var record = e.record;
							var gridFijos = e.gridFijos; 
							var campo= e.field;
				
							if(campo == 'PROVDIST') {					
									if(record.data['EDIT']=="S" || record.data['EDIT']=="A") {					
										return true;		
									}				
								else{
									record.commit();	
									return false;		
								}
							}		
							for(var i =0; i<nombres.length; i++){
								if(campo == nombres[i]) {					
									if(record.data['EDIT']=="S" || record.data['EDIT']=="A") {					
										return true;		
									}				
									else{
										record.commit();	
										return false;		
									}
								}	
							}
						},afteredit : function(e){
								var record = e.record;
								var gridEditable = e.gridEditable; 
								var campo= e.field;        
							   for(var i=0; i<tipos.length; i++){
									if(tipos[i]=='fecha'){
										var fecha =Ext.util.Format.date(record.data[nombres[i]],'d/m/Y');
										record.data[nombres[i]] = fecha;
										record.commit();
									}
								}
						}
				},bbar: {
					buttonAlign: 	'right',
					id: 			'barraPaginacion21',
					items: [
						'->','-',
						{
							xtype:	'button',
							text:		'Generar Archivo',
							width:	100,
							iconCls:	'icoXls',
							id: 		'btnGenArchivo21',
							handler: function(boton, evento) {
								//Aqui se obtienen los datos del grid para su impresion en un archivo .csv
								var arreglo = new Array();
								var a = new Array();
								a[0] = "Proveedor Distribuidor";
								for(var c=0; c<nombres.length; c++){
									a[c+1] = nombres[c];
								}
								arreglo[0] = a;
								for(var i=0; i<gridDinEdit.getStore().getTotalCount(); i++){
									var aux = new Array();
									registro = gridDinEdit.getStore().getAt(i);
									aux[0] = registro.get('PROVDIST');
									for(var j =0; j<nombres.length; j++){
												aux[j+1] = registro.get(nombres[j]);
									}
									arreglo[i+1] = aux;
								}
								Ext.Ajax.request({
									url: '15admtabla01Ext.data.jsp',
									params:	
												{
													informacion:'ArchivoCSV',
													arreglo: arreglo
												}
									,
									callback: procesarSuccessFailureGenerarCSV
								});
							}
						},'-',
						{
							xtype:	'button',
							text:		'Importar',
							width:	100,
							iconCls:	'autorizar',
							id: 		'btnImportar',
							handler: function(boton, evento) {
								gridDinEdit.hide();
								formaDatos.hide();
								fpTabFija.show();
							}
						},'-',
						{
							xtype:	'button',
							text:		'Eliminar Todos',
							width:	100,
							iconCls:	'icoCancelar',
							id: 		'btnEliminarAll',
							handler: function(grid, rowIndex, colIndex, item, event) {
								Ext.Msg.confirm('Confirmaci�n', 'Desea borrar todos los registros con sus relaciones?', function(btn){
										if(btn=='yes'){
											var registro = gridDinEdit.getStore().getAt(0);
											var num_pub = registro.get('NOPUBLIC');
											var caracter_campo = "F";
											Ext.Ajax.request({
												url: '15admtabla01Ext.data.jsp',
												params: 
													{
														informacion: 'EliminaDatosGridAll',
														num_pub: num_pub,
														caracter_campo: caracter_campo
													},				
													callback: procesarEliminarTodos									  
											});
										}
									})
							}
						},'-',
						{
							xtype:	'button',
							text:		'Terminar',
							width:	100,
							iconCls:	'icoRegresar',
							id: 		'btnTerminar21',
							handler: function(boton, evento) {
								gridDinEdit.destroy();
								if(!bg)
									gridDinEditDet.destroy();
								formaDatos.hide();
								accionConsulta("CONSULTAR", null);
								grid.show();
								if(!band)
									band = true;
							}
						},'-'
					]
				}
			});
			
			if(tot <= 0)
				Ext.getCmp('btnGenArchivo21').disable();
				
			var formaDatos = Ext.getCmp('formaDatos');		
			if(!formaDatos.isVisible())
				formaDatos.show();
			formaDatos.insert(0,gridDinEdit);
			formaDatos.doLayout(false,true);
			
		}else
			NE.util.mostrarConnError(response,opts);
	}
	
	//Procesa el retorno de los datos consultados para la info de tablas
	var procesarConsultaDatosTabla =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			var tabla = jsonObj.msNombreTabla;
			var nombres = jsonObj.li;
			grid.hide();
			
			var consultaTablasIndiv = {
				xtype: 'jsonstore',
				id:'consultaTablasIndiv',
				root: 'registros',
				totalProperty: 'total',
				messageProperty: 'msg',
				autoLoad: false,
				fields: null,
				data: null
			};
			
			var col = [];
			var this_col = {};
			this_col['header'] = 'Pyme - Usuario';
			this_col['width'] = 190;
			this_col['dataIndex'] = 'PYMEUSR';
			this_col['sortable'] = false;
			col.push(this_col);
			
			for(var i =0; i<nombres.length; i++){
				var this_col = {};
				this_col['header'] = nombres[i];
				this_col['width'] = 160;
				this_col['dataIndex'] = nombres[i];
				this_col['sortable'] = false;
				col.push(this_col);
			}
		
			var this_col = {};
			this_col['header'] = 'Distribuidores';
			this_col['width'] = 200;
			this_col['dataIndex'] = 'DIST';
			this_col['sortable'] = false;
			col.push(this_col);
			
			gridDin = new Ext.grid.GridPanel({
				title: 'Consulta de Tablas '+"'"+tabla+"'",
				store: consultaTablasIndiv,
				width: 900,  
				height:280,
				style: 'margin:0 auto;',
				enableDD: false,       
			   enableSort : false,
			   enableHdMenu:false,
			   columns:col,
				bbar: {
					buttonAlign: 	'right',
					id: 			'barraPaginacion21',
					items: [
						'->','-',
						{
							xtype:	'button',
							text:		'Generar Archivo',
							width:	100,
							iconCls:	'icoXls',
							id: 		'btnGenArchivo',
							handler: function(boton, evento) {
								//�k
							}
						},'-',
						{
							xtype:	'button',
							text:		'Terminar',
							width:	100,
							iconCls:	'icoRegresar',
							id: 		'btnTerminar',
							handler: function(boton, evento) {
								gridDin.destroy();
								formaDatos.hide();
								grid.show();
							}
						},'-'
					]
				}
			});
			
			var formaDatos = Ext.getCmp('formaDatos');		
			if(!formaDatos.isVisible())
				formaDatos.show();
			formaDatos.insert(0,gridDin);
			formaDatos.doLayout(false,true);
			
		} else {
			//NE.util.mostrarConnError(response,opts);
			var resp = 	Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert("Aviso", "No hay campos de edici�n para la publicaci�n "+resp.publicacion);
		}
	}
	
	//Procesa el retorno de los datos consultados para mostrar tabla detalle
	var procesarTablaDetalle = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			bg=false;
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			var nombres = jsonObj.listNom;
			var tip = jsonObj.lvTipos;
			var longd = jsonObj.longDet;
			var nomCampFijo = jsonObj.campoFijo;
			
			var fl = [];
			var cols = {};
			cols['name'] = 'PROVDISTD';
			fl.push(cols);
			var cols = {};
			cols['name'] = 'IDDET';
			fl.push(cols);
			var cols = {};
			cols['name'] = 'IDCAMPF';
			fl.push(cols);
			var cols = {};
			cols['name'] = 'CAMPFIJO';
			fl.push(cols);
			var cols = {};
			cols['name'] = 'EDITD';
			fl.push(cols);
			for(var i =0; i<nombres.length; i++){
				var cols = {};
				cols['name'] = nombres[i];
				fl.push(cols);
			}
			
			var consultaTablaDetalle =new Ext.data.JsonStore({
				xtype: 'jsonstore',
				id:'consultaTablaDetalle',
				root: 'registros',
				totalProperty: 'total',
				messageProperty: 'msg',
				autoLoad: false,
				fields: fl
			});
			
			consultaTablaDetalle.loadData(jsonObj.columnasRegistros);
			var tot = jsonObj.total;
			
			var col = [];
			var this_col = {};
			this_col['header'] = 'Proveedor Distribuidor';
			this_col['width'] = 200;
			this_col['dataIndex'] = 'PROVDISTD';
			this_col['align'] = 'center';
			this_col['sortable'] = false;
			col.push(this_col);
			var this_col = {};
			this_col['header'] = '';
			this_col['dataIndex'] = 'IDDET';
			this_col['align'] = 'center';
			this_col['hidden'] = true;
			col.push(this_col);
			var this_col = {};
			this_col['header'] = '';
			this_col['dataIndex'] = 'IDCAMPF';
			this_col['align'] = 'center';
			this_col['hidden'] = true;
			col.push(this_col);
			var this_col = {};
			this_col['header'] = nomCampFijo;
			this_col['width'] = 200;
			this_col['dataIndex'] = 'CAMPFIJO';
			this_col['align'] = 'center';
			this_col['sortable'] = false;
			col.push(this_col);
			var this_col = {};
			this_col['header'] = '';
			this_col['dataIndex'] = 'EDITD';
			this_col['align'] = 'center';
			this_col['hidden'] = true;		
			col.push(this_col);
			
			for(var i =0; i<nombres.length; i++){		//Se generan las columnas dinamicas del grid
				var this_col = {};
				if(tip[i]=='seleccion'){
					this_col['xtype'] = 'checkcolumn';
					this_col['header'] = nombres[i];
					this_col['dataIndex'] = nombres[i];
				}else{
					this_col['header'] = nombres[i];
					this_col['width'] = 160;
					this_col['dataIndex'] = nombres[i];
					this_col['sortable'] = false;
					this_col['align'] = 'center';
					if(tip[i]=='alfanumerico' || tip[i]=='numerico' || tip[i]=='hora'){
						if(tip[i]=='hora'){
							this_col['editor'] = {
														xtype: 'textfield',
														maxLength:5	 
													};	
						}else{
							this_col['editor'] = {
														xtype: 'textfield',
														maxLength: parseInt(longd[i])		 
													};		
						}
						this_col['renderer'] = function(value,metadata,registro, rowIndex, colIndex){
															if(registro.data['EDITD']=="S" || registro.data['EDITD']=="A")
																return NE.util.colorCampoEdit(value,metadata,registro);
															else
																return value;
													};
					}else if(tip[i]=='fecha'){
						this_col['editor'] = {
												xtype: 'datefield',
												name: 'txtFechaAplicDet',
												allowBlank: false,	
												format:'d/m/Y',
												maxLength:10,
												startDay: 0,
												width: 100,
												msgTarget: 'side',			
												margins: '0 20 0 0'	 
											};
						this_col['renderer'] = function(value,metadata,registro, rowIndex, colIndex){
												if(registro.data['EDITD']=="S" || registro.data['EDITD']=="A")
													return NE.util.colorCampoEdit(value,metadata,registro);
												else
													return value;
											};
					}
				}
				col.push(this_col);
			}
						
			var this_col = {};
			this_col['xtype'] = 'actioncolumn';
			this_col['header'] = 'Datos';
			this_col['width'] = 100;
			this_col['align'] = 'center';
			this_col['items'] = [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['EDITD']=="S"){
								this.items[0].tooltip = 'Agregar';
								return 'icoAceptar';				
							}		
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
							b = true;
							var campo_det = new Array();
							var registro = gridDinEditDet.getStore().getAt(rowIndex);
							var reg = gridDinEdit.getStore().getAt(0);
							var num_pub = reg.get('NOPUBLIC');
							var id_campo = registro.get('IDCAMPF');
							for(var i =0; i<nombres.length; i++){
								if(registro.get(nombres[i]) == ""){
									Ext.MessageBox.alert("Aviso", " Te hacen falta datos ");
									b=false;
									break;
								}
							}
							if(b){
								for(var i =0; i<nombres.length; i++){
									campo_det[i] = registro.get(nombres[i]);
								}
								Ext.Ajax.request({
										url: '15admtabla01Ext.data.jsp',
										params: 
											{
													informacion: 'AgregaDatDetalle',
													num_pub: num_pub,
													campo_det: campo_det,
													tipo_campo_det: tip,
													num_detalle: id_campo,
													num_campo_fijo: id_campo
											},				
										callback: procesarAgregarFilaDetalle								  
								});
							
							}
						}
					},{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['EDITD']=="N"){
								this.items[1].tooltip = 'Modificar';
								return 'icoModificar';				
							}		
						},
						handler: modificaFilaDetalle
					},{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['EDITD']=="N"){
								this.items[2].tooltip = 'Eliminar';
								return 'icoEliminar';				
							}		
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
							Ext.Msg.confirm('Confirmaci�n', 'Desea borrar registros?', function(btn){
										if(btn=='yes'){
											var campo_det = new Array();
											var reg = gridDinEdit.getStore().getAt(0);
											var num_pub = reg.get('NOPUBLIC');
											var registro = gridDinEditDet.getStore().getAt(rowIndex);
											var id_campo = registro.get('IDDET');
											var id_relacion = registro.get('IDCAMPF');
											for(var i =0; i<nombres.length; i++){
												campo_det[i+1] = registro.get(nombres[i]);
											}
											Ext.Ajax.request({
														url: '15admtabla01Ext.data.jsp',
														params: 
															{
																	informacion: 'EliminarDatDetalle',
																	num_pub: num_pub,
																	campo_det: campo_det,
																	tipo_campo_det: tip,
																	id_campo: id_campo,
																	num_detalle: id_relacion,
																	num_campo_fijo: id_relacion
															},				
														callback: procesarEliminarFilaDetalle								  
											});
										}
							})
						}
					},{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['EDITD']=="A"){
								this.items[3].tooltip = 'Salvar';
								return 'icoAgregar';			
							}
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
							b = true;
							var campo_det = new Array();
							var registro = gridDinEditDet.getStore().getAt(rowIndex);
							var reg = gridDinEdit.getStore().getAt(0);
							var num_pub = reg.get('NOPUBLIC');
							var id_relacion = registro.get('IDCAMPF');
							var id_campo = registro.get('IDDET');
							for(var i =0; i<nombres.length; i++){
								if(registro.get(nombres[i]) == ""){
									Ext.MessageBox.alert("Aviso", " Te hacen falta datos ");
									b=false;
									break;
								}
							}
							if(b){
								for(var i =0; i<nombres.length; i++){
									campo_det[i] = registro.get(nombres[i]);
								}
								Ext.Ajax.request({
										url: '15admtabla01Ext.data.jsp',
										params: 
											{
													informacion: 'SalvarDatDetalle',
													num_pub: num_pub,
													campo_det: campo_det,
													tipo_campo_det: tip,
													id_campo: id_campo,
													num_detalle: id_relacion,
													num_campo_fijo: id_relacion
											},				
										callback: procesarSalvarFilaDetalle						  
								});
								
							}
						}
					}
				];
			col.push(this_col);
			
			gridDinEditDet = new Ext.grid.EditorGridPanel({
				title: 'Modificacion de campos de detalle de la tabla ',
				store: consultaTablaDetalle,
				width: 900,  
				height:280,
				clicksToEdit:1,
				style: 'margin:0 auto;',
				enableDD: false,       
			   enableSort : false,
			   enableHdMenu:false,
			   columns:col,
				listeners: {
						beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
							var record = e.record;
							var gridFijos = e.gridFijos; 
							var campo= e.field;
					
							for(var i =0; i<nombres.length; i++){
								if(campo == nombres[i]) {					
									if(record.data['EDITD']=="S" || record.data['EDITD']=="A") {					
										return true;		
									}				
									else{
										record.commit();	
										return false;		
									}
								}	
							}
						},afteredit : function(e){
								var record = e.record;
								var gridEditable = e.gridEditable; 
								var campo= e.field;        
							   for(var i=0; i<tip.length; i++){
									if(tip[i]=='fecha'){
										var fecha =Ext.util.Format.date(record.data[nombres[i]],'d/m/Y');
										record.data[nombres[i]] = fecha;
										record.commit();
									}
								}
						}
				},
				bbar: {
					buttonAlign: 	'right',
					id: 			'barraPaginacion211',
					items: [
						'->','-',
						{
							xtype:	'button',
							text:		'Generar Archivo',
							width:	100,
							iconCls:	'icoXls',
							id: 		'btnGenArchivo211',
							handler: function(boton, evento) {
								//Aqui se obtienen los datos del grid para su impresion en un archivo .csv
								var arreglo = new Array();
								var a = new Array();
								a[0] = "Proveedor Distribuidor";
								a[1] = nomCampFijo;
								for(var c=0; c<nombres.length; c++){
									a[c+2] = nombres[c];
								}
								arreglo[0] = a;
								for(var i=0; i<gridDinEditDet.getStore().getTotalCount(); i++){
									var aux = new Array();
									registro = gridDinEditDet.getStore().getAt(i);
									aux[0] = registro.get('PROVDISTD');
									aux[1] = registro.get('CAMPFIJO');
									for(var j =0; j<nombres.length; j++){
												aux[j+2] = registro.get(nombres[j]);
									}
									arreglo[i+1] = aux;
								}
								Ext.Ajax.request({
									url: '15admtabla01Ext.data.jsp',
									params:	
												{
													informacion:'ArchivoCSV',
													arreglo: arreglo
												}
									,
									callback: procesarSuccessFailureGenerarCSV
								});
							}
						},'-',
						{
							xtype:	'button',
							text:		'Importar',
							width:	100,
							iconCls:	'autorizar',
							id: 		'btnImportar211',
							handler: function(boton, evento) {
								gridDinEdit.hide();
								formaDatos.hide();
								fpTabDetalle.show();
							}
						},'-',
						{
							xtype:	'button',
							text:		'Eliminar Todos',
							width:	100,
							iconCls:	'icoCancelar',
							id: 		'btnEliminarAll211',
							handler: function(boton, evento) {
								Ext.Msg.confirm('Confirmaci�n', 'Desea borrar todos los registros?', function(btn){
										if(btn=='yes'){
											//accionConsulta("MODIFICAR", null);
											var registro = gridDinEdit.getStore().getAt(0);
											var num_pub = registro.get('NOPUBLIC');
											var reg = gridDinEditDet.getStore().getAt(0);
											var num_campo_fijo = reg.get('IDCAMPF');
											var caracter_campo = "D";
											Ext.Ajax.request({
												url: '15admtabla01Ext.data.jsp',
												params: 
													{
														informacion: 'EliminaDatosGridAll',
														num_pub: num_pub,
														caracter_campo: caracter_campo,
														num_campo_fijo: num_campo_fijo
													},				
													callback: procesarEliminarTodosDetalle							  
											});
										}
									})
							}
						},'-',
						{
							xtype:	'button',
							text:		'Terminar',
							width:	100,
							iconCls:	'icoRegresar',
							id: 		'btnTerminar211',
							handler: function(boton, evento) {
								gridDinEdit.destroy();
								gridDinEditDet.destroy();
								formaDatos.hide();
								accionConsulta("CONSULTAR", null);
								grid.show();
								if(!band)
									band = true;
							}
						},'-'
					]
				}
			});
			
			if(tot <= 0)
				Ext.getCmp('btnGenArchivo211').disable();
			
			var formaDatos = Ext.getCmp('formaDatos');		
			if(!formaDatos.isVisible())
				formaDatos.show();
			formaDatos.insert(1,NE.util.getEspaciador(22));
			formaDatos.insert(2,gridDinEditDet);
			formaDatos.doLayout(false,true);
			
		}else
			NE.util.mostrarConnError(response,opts);
	}
		
	//Lanza la consulta para cargar el grid de consulta de una tabla de EPO
	var consultarDatTabla = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var nPublicacion = registro.get('NUMPUB');
		var nomPub = registro.get('NOMPUB');
		Ext.Ajax.request({
				url: '15admtabla01Ext.data.jsp',
				params: 
						{
							informacion: 'ConsulTabIndiv',
							nom_tabla: nomPub,
							nPublicacion: nPublicacion
						},				
				callback: procesarConsultaDatosTabla											  
		});	
	}
	
	//Lanza la modificacion de una tabla
	var modificarDatTabla = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var nPublicacion = registro.get('NUMPUB');
		var nom_tabla = registro.get('NOMPUB');
		Ext.Ajax.request({
				url: '15admtabla01Ext.data.jsp',
				params: 
						{
							informacion: 'ModificarTabIndiv',
							nom_tabla: nom_tabla,
							nPublicacion: nPublicacion
						},				
				callback: procesarModificaDatosTabla											  
		});	
	}

	
	//Se muestran los componentes para la modificacion de las tablas
	var modificarTabla = function(grid, rowIndex, colIndex, item, event){
		fpModTab.show();
		grid.hide();
		var registro = grid.getStore().getAt(rowIndex);
		var numPubTab = registro.get('NUMPUB');
		var nomTab = registro.get('NOMPUB');
		var feciT = registro.get('FECINI');
		var fecfT = registro.get('FECFIN');
		Ext.getCmp('_txt_num_mod_tab').setValue(numPubTab);
		Ext.getCmp('_txt_nom_mod_tab').setValue(nomTab);
		Ext.getCmp('_fecha_carga_ini_mod_tab').setValue(feciT);
		Ext.getCmp('_fecha_carga_fin_mod_tab').setValue(fecfT);
		Ext.StoreMgr.key('camposFijosModDataStore').load({
						params:
								{
									informacion: 'camposFijosMod',
									numFijosMod: 1
								}
		});
		Ext.StoreMgr.key('camposEditablesModDataStore').load({
						params:
								{
									informacion: 'camposEditablesMod',
									numEdiMod: 1,
									numPubTab: numPubTab
								}
		});
		Ext.StoreMgr.key('camposDetalleModDataStore').load({
						params:
								{
									informacion: 'camposDetalleMod',
									numDetMod: 1,
									numPubTab: numPubTab
								}
		});
		gridFijosMod.show();
		gridEditablesMod.show();
		gridDetalleMod.show();
		fpBotonesModT.show();
	}
	
	//muestra el archivo de la columna archivo
	var procesarSuccessFailureArchivo =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	var modificaFilaDetalle = function(grid, rowIndex, colIndex, item, event) {
		var registro = gridDinEditDet.getStore().getAt(rowIndex);
		if(band2){
			var nomProv = registro.set('EDITD',"A");
			band2 = false;
			registro.commit();
			var reg = gridDinEditDet.getStore().getAt(gridDinEditDet.getStore().getTotalCount());
			reg.set('EDITD', "R");
			reg.commit();
		}
	}
	
	//descarga archivo
	var descargaArchivo = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid2.getStore().getAt(rowIndex);
		var cveEpo = Ext.getCmp('_cmb_cad').getValue();
		var nomArch = registro.get('ARCHIVO');		
		
		Ext.Ajax.request({
			url: '15admtabla01Ext.data.jsp',
			params:{
				informacion: 'DescargaArchivo',
				cveEpo: cveEpo,
				nomArch: nomArch
			},
			callback: procesarSuccessFailureArchivo
		});
	}
	
	//muestra la imagen de la columna imagen
	var procesarSuccessFailureImagen =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	//Se valida que no exista una tabla con un nombre repetido
	var procesarValidaNombreTabla =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			accionConsulta("LIMPIAR",null);
			Ext.MessageBox.alert("Aviso","Ya existe una tabla con el nombre "+"'"+resp.nombre+"'");
		} else {		
			fp.hide();
			var numFijos = Ext.getCmp('_txt_camf').getValue();
			var numEdi = Ext.getCmp('_txt_came').getValue();
			var numDet = Ext.getCmp('_txt_camd').getValue();
			//Se genearan los grids para capturar datos de la tabla
			Ext.StoreMgr.key('camposFijosDataStore').load({
				params:
					{
						informacion: 'camposFijos',
						numFijos: numFijos
					}
			});
			Ext.StoreMgr.key('camposEditablesDataStore').load({
				params:
					{
						informacion:'camposEditables',
						numEdi: numEdi
					}
			});
			Ext.StoreMgr.key('camposDetalleDataStore').load({
				params:
					{
						informacion:'camposDetalle',
						numDet: numDet
					}
			});
			gridFijos.show();
			//gridFijos.setTitle('Definici�n de campos fijos de la tabla'+"'"+resp.nombre+"'");
			gridEditables.show();
			gridDetalle.show();
			fpBotones.show();
		}		
	}
	
	var procesarValidaNombreTablaCopia =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			//accionConsulta("LIMPIAR",null);
			Ext.getCmp('_txt_nom_tab_copy').reset();
			Ext.getCmp('winCopiar').hide();
			Ext.MessageBox.alert("Aviso","Ya existe una tabla con el nombre "+"'"+resp.nombre+"'");
		} else {		
			var numPub = Ext.getCmp('_txt_num_pub_copy').getValue();
			Ext.getCmp('winCopiar').hide();
			Ext.StoreMgr.key('camposFijosDataStore').load({
				params:
					{
						informacion: 'obtenFijos',
						copiar: numPub
					}
			});
			Ext.StoreMgr.key('camposEditablesDataStore').load({
				params:
					{
						informacion: 'obtenEditables',
						copiar: numPub
					}
			});
			Ext.StoreMgr.key('camposDetalleDataStore').load({
				params:
					{
						informacion: 'obtenDetalle',
						copiar: numPub
					}
			});
			grid.hide();
			gridFijos.show();
			gridEditables.show();
			gridDetalle.show();
			fpBotones.show();
		}		
	}
	
	//descarga imagen
	var descargaImagen = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid2.getStore().getAt(rowIndex);
		var cveEpo = Ext.getCmp('_cmb_cad').getValue();
		var nomImg = registro.get('IMAGEN');		
		
		Ext.Ajax.request({
			url: '15admtabla01Ext.data.jsp',
			params:{
				informacion: 'DescargaImagen',
				cveEpo: cveEpo,
				nomImg: nomImg
			},
			callback: procesarSuccessFailureImagen
		});
	}
	
	var eliminarTabla = function (grid, rowIndex, colIndex, item, event){
		Ext.Msg.confirm('Confirmaci�n', '�Est� seguro que desea eliminar las tablas?', function(btn){
			if(btn=='yes'){
				var registro = grid.getStore().getAt(rowIndex);
				var cveEpo = Ext.getCmp('_cmb_cad').getValue();
				var numPub = registro.get('NUMPUB');
				Ext.Ajax.request({
					url: '15admtabla01Ext.data.jsp',
					params:{
						informacion: 'EliminaTabla',
						cveEpo: cveEpo,
						numPub: numPub
					},
					callback: procesarSuccessFailureDeleteTabla
				});
			}
		});
	}
	
	var procesarCargaEditables = function(store, registros, opts){
		if(store.getTotalCount()==5){
			Ext.getCmp('btnAgrCampEditable').disable();
		}else
			Ext.getCmp('btnAgrCampEditable').enable();
	}
	
	var procesarCargaDetalle = function(store, registros, opts){
		if(store.getTotalCount()==10){
			Ext.getCmp('btnAgrCampDetalle').disable();
		}else
			Ext.getCmp('btnAgrCampDetalle').enable();
	}
	
	var procesarCargaEditablesMod = function(opts, success, response){
		var resp = 	Ext.util.JSON.decode(response.responseText);
		/*if(resp.maximo != undefined && resp.maximo){
			Ext.getCmp('btnAgrCampEditable').disable();
		}else
			Ext.getCmp('btnAgrCampEditable').enable();*/
	}
	
	var validaNombre = function(){
		Ext.Ajax.request({
				url: '15admtabla01Ext.data.jsp',
				params:{
					informacion: 'validaNombre',
					cadena: Ext.getCmp('_cmb_cad').getValue(),
					nom_tabla: Ext.getCmp('_txt_nom').getValue()	
				},
				callback: procesarValidaNombreTabla
		});
	}
	
	var validaNombreCopia = function(grid, rowIndex, colIndex, item, event){
		Ext.Ajax.request({
				url: '15admtabla01Ext.data.jsp',
				params:{
					informacion: 'validaNombre',
					cadena: Ext.getCmp('_cmb_cad').getValue(),
					nom_tabla: Ext.getCmp('_txt_nom_tab_copy').getValue()	
				},
				callback: procesarValidaNombreTablaCopia
		});
	}
	
//- - - - - - - - - - - - - - - - STORE'S - - - - - - - - - - - - - - - - - -
	
	var storeTipodeDato = new Ext.data.SimpleStore({
    fields: ['clave', 'descripcion'],
    data : [['alfanumerico','Alfanumerico']
				,['numerico','N�merico']
				,['fecha','Fecha']
				,['hora','Hora']
				,['seleccion','Selecci�n']
				]
	});
	
	//Catalogo para el combo de seleccion de Tipo de Publicacion combo de mantenimiento
	var catalogoTipoPublicacionData = new Ext.data.JsonStore({
		id:				'catalogoTipoPublicacionDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admtabla01Ext.data.jsp',
		baseParams:		{	informacion: 'catalogoTipoPublicacion'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load:procesarCatalogoTipoPublicacion,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//Catalogo para el combo de seleccion de Tipo de Publicacion combo de mantenimiento
	var catalogoCadenasData = new Ext.data.JsonStore({
		id:				'catalogoCadenasDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admtabla01Ext.data.jsp',
		baseParams:		{	informacion: 'catalogoCadenas'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			load:procesarCatCadenasData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//Catalogo para el combo de seleccion Multiple
	var catalogoMultipleData = new Ext.data.JsonStore({
		id:				'catalogoMultipleDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'15admtabla01Ext.data.jsp',
		baseParams:		{	informacion: 'catalogoMulti'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeTipoAfiliado = new Ext.data.SimpleStore({
    fields: ['clave', 'descripcion'],
    data : [['1','Proveedores']
				,['2','Distribuidores']
				]
	});
	
	var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'15admtabla01Ext.data.jsp',
			baseParams: {	informacion:	'ConsulTablas'	},
			fields: [
				{ name: 'NUMPUB' },
				{ name: 'NOMPUB'},
				{ name: 'FECINI'},
				{ name: 'FECFIN'},
				{ name: 'NUMCOL'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
					
					}
				},
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null, null);
					}
				}
			}
	});
	
	var registrosConsultados2Data = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultados2DataStore',
			url: 			'15admtabla01Ext.data.jsp',
			baseParams: {	informacion:	'ConsultaImagenes'	},
			fields: [
				{ name: 'NUMPUBIM' },
				{ name: 'NOMPUBIM'},
				{ name: 'FECINIIM'},
				{ name: 'FECFINIM'},
				{ name: 'IMAGEN'},
				{ name: 'ARCHIVO'},
				{ name: 'CONTENIDO'},
				{ name: 'CONTENIDOALL'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
					
					}
				},
				load: 	procesarConsultaRegistrosIm,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistrosIm(null, null, null);
					}
				}
			}
	});
	
	var camposFijosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'camposFijosDataStore',
			url: 			'15admtabla01Ext.data.jsp',
			baseParams: {	informacion:	'camposFijos'	},
			fields: [
				{ name: 'NOMCAMPOF' },
				{ name: 'TIPODATF'},
				{ name: 'LONGITUDF'},
				{ name: 'ORDENF'},
				{ name: 'TOTALESF'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
					
					}
				},
				//load: 	procesarCargaFijos,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						//procesarCargaFijos(null, null, null);
					}
				}
			}
	});
	
	var camposFijosModData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'camposFijosModDataStore',
			url: 			'15admtabla01Ext.data.jsp',
			baseParams: {	informacion:	'camposFijosMod'	},
			fields: [
				{ name: 'NOMCAMPOF' },
				{ name: 'TIPODATF'},
				{ name: 'LONGITUDF'},
				{ name: 'ORDENF'},
				{ name: 'TOTALESF'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
					
					}
				},
				//load: 	procesarCargaFijos,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						//procesarCargaFijos(null, null, null);
					}
				}
			}
	});
	
	var camposEditablesData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'camposEditablesDataStore',
			url: 			'15admtabla01Ext.data.jsp',
			baseParams: {	informacion:	'camposEditables'	},
			fields: [
				{ name: 'NOMCAMPOE' },
				{ name: 'TIPODATE'},
				{ name: 'LONGITUDE'},
				{ name: 'ORDENE'},
				{ name: 'TOTALESE'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
					
					}
				},
				load: 	procesarCargaEditables,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarCargaEditables(null, null, null);
					}
				}
			}
	});
	
	var camposEditablesModData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'camposEditablesModDataStore',
			url: 			'15admtabla01Ext.data.jsp',
			baseParams: {	informacion:	'camposEditablesMod'	},
			fields: [
				{ name: 'NOMCAMPOE' },
				{ name: 'TIPODATE'},
				{ name: 'LONGITUDE'},
				{ name: 'ORDENE'},
				{ name: 'TOTALESE'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
					
					}
				},
				load: 	procesarCargaEditablesMod,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarCargaEditablesMod(null, null, null);
					}
				}
			}
	});
	
	var camposDetalleData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'camposDetalleDataStore',
			url: 			'15admtabla01Ext.data.jsp',
			baseParams: {	informacion:	'camposDetalle'	},
			fields: [
				{ name: 'NOMCAMPOD' },
				{ name: 'TIPODATD'},
				{ name: 'LONGITUDD'},
				{ name: 'ORDEND'},
				{ name: 'TOTALESD'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
					
					}
				},
				load: 	procesarCargaDetalle,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarCargaDetalle(null, null, null);
					}
				}
			}
	});
	
	var camposDetalleModData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'camposDetalleModDataStore',
			url: 			'15admtabla01Ext.data.jsp',
			baseParams: {	informacion:	'camposDetalle'	},
			fields: [
				{ name: 'NOMCAMPOD' },
				{ name: 'TIPODATD'},
				{ name: 'LONGITUDD'},
				{ name: 'ORDEND'},
				{ name: 'TOTALESD'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
					
					}
				},
				//load: 	procesarCargaDetalle,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						//procesarCargaDetalle(null, null, null);
					}
				}
			}
	});
	
	/*function validaFloat(numero)
	{
		res = false;
		if (/^([0-9])*.+[1-2]$/.test(numero)){
			res = true;
		}
		return res;
	}*/
	
	function validaFloat(numero)
	{
		res = false;
		if (/^([1-9])+([0-9])*.{1}[1-2]{1}$/.test(numero)){
			res = true;
			console.log("Es un numero valido")
		}
		return res;
	}
	
	
//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - - -

	var accionConsulta = function(estadoSiguiente, respuesta){
		 if(  estadoSiguiente == "CONSULTAR" ){
			var cad = Ext.getCmp('_cmb_cad').getValue();
			var mant = Ext.getCmp('_cmb_mant').getValue();
			Ext.getCmp('_txt_nom').reset();
			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');
			if(mant=="1"){
				Ext.StoreMgr.key('registrosConsultadosDataStore').load({
					params:
								{
									informacion:'ConsulTablas',
									cveCad: cad
								}
				});
			}else if(mant=="2"){
				Ext.StoreMgr.key('registrosConsultados2DataStore').load({
					params:
								{
									informacion:'ConsultaImagenes',
									cadena: cad
								}
				});	
			}
			
		} else if(	estadoSiguiente == "AGREGAR"){ //Para guardar datos capturados en el form principal (fp)
			var mant = Ext.getCmp('_cmb_mant').getValue();
			if(mant=="1"){
				if(Ext.getCmp('_txt_nom').getValue()=="" || Ext.getCmp('_fecha_carga_ini').getValue()=="" || Ext.getCmp('_fecha_carga_fin').getValue()=="" || Ext.getCmp('_txt_camf').getValue()=="" || Ext.getCmp('_txt_came').getValue()=="" || Ext.getCmp('_txt_camd').getValue()=="" || Ext.getCmp('_cmb_cad').getValue()=="" || Ext.getCmp('_cmb_cad').getValue()=="T" || fp.getForm().findField('_cmb_multi').getValue()=="") {
					if(Ext.getCmp('_txt_nom').getValue()=="")
						Ext.getCmp('_txt_nom').markInvalid('Debe capturar el nombre de la tabla');
					else if(Ext.getCmp('_fecha_carga_ini').getValue()=="")
						Ext.getCmp('_fecha_carga_ini').markInvalid('Debe capturar la fecha de publicaci�n inicial');
					else if(Ext.getCmp('_fecha_carga_fin').getValue()=="")
						Ext.getCmp('_fecha_carga_fin').markInvalid('Debe capturar la fecha de publicaci�n final');
					else if(Ext.getCmp('_txt_camf').getValue()=="")
						Ext.getCmp('_txt_camf').markInvalid('Debe capturar el n�mero de campos Fijos de la tabla');
					else if(Ext.getCmp('_txt_came').getValue()=="")
						Ext.getCmp('_txt_came').markInvalid('Debe capturar el n�mero de campos Editables de la tabla');
					else if(Ext.getCmp('_txt_camd').getValue()=="")
						Ext.getCmp('_txt_camd').markInvalid('Debe capturar el n�mero de campos de Detalle de la tabla');
					else if(Ext.getCmp('_cmb_cad').getValue()=="" || Ext.getCmp('_cmb_cad').getValue()=="T")
						Ext.getCmp('_cmb_cad').markInvalid('Debe seleccionar una cadena');
					else if(fp.getForm().findField('_cmb_multi').getValue()=="")
						Ext.getCmp('_cmb_multi').markInvalid('No hay pymes seleccionadas para enviar...!!!');
				}
				else{
					var bi=true;
					var numFijos = parseInt(Ext.getCmp('_txt_camf').getValue());
					var numEdit = parseInt(Ext.getCmp('_txt_came').getValue());
					var numDet = parseInt(Ext.getCmp('_txt_camd').getValue());
					if(numFijos>50){
						Ext.getCmp('_txt_camf').markInvalid('El n�mero maximo de campos fijos es 50');
						bi=false;
					}else if(numEdit>5){
						Ext.getCmp('_txt_came').markInvalid('El n�mero maximo de campos editables es 5');
						bi=false;
					}
					else if(numDet>10){
						Ext.getCmp('_txt_camd').markInvalid('El n�mero maximo de campos de detalle es 10');
						bi=false;
					}
					//Se debe validar que el nombre de la tabla no este repetido para la EPO
					if(bi)
						validaNombre();
				}
			}
			else if(mant=="2"){
				if(Ext.getCmp('_txt_tit').getValue()=="" || Ext.getCmp('_fecha_carga_ini').getValue()=="" || Ext.getCmp('_fecha_carga_fin').getValue()=="" || Ext.getCmp('_txt_con').getValue()=="" || Ext.getCmp('_imagen').getValue()=="" || Ext.getCmp('_cmb_cad').getValue()=="" || Ext.getCmp('_cmb_cad').getValue()=="T") {
					if(Ext.getCmp('_txt_tit').getValue()=="")
						Ext.getCmp('_txt_tit').markInvalid('Debe capturar el t�tulo de la publicaci�n');
					else if(Ext.getCmp('_cmb_cad').getValue()=="" || Ext.getCmp('_cmb_cad').getValue()=="T")
						Ext.getCmp('_cmb_cad').markInvalid('Debe seleccionar una cadena');
					else if(Ext.getCmp('_fecha_carga_ini').getValue()=="")
						Ext.getCmp('_fecha_carga_ini').markInvalid('Debe capturar la fecha de publicaci�n inicial');
					else if(Ext.getCmp('_fecha_carga_fin').getValue()=="")
						Ext.getCmp('_fecha_carga_fin').markInvalid('Debe capturar la fecha de publicaci�n final');
					else if(Ext.getCmp('_txt_con').getValue()=="")
						Ext.getCmp('_txt_con').markInvalid('Debe capturar el contenido de la publicaci�n');
					else if(Ext.getCmp('_imagen').getValue()=="")
						Ext.getCmp('_imagen').markInvalid('Debe seleccionar una im�gen a importar');
				}else{
						var cadena = Ext.getCmp('_cmb_cad').getValue();
						var titulo = Ext.getCmp('_txt_tit').getValue();	
						var fecIni = Ext.getCmp('_fecha_carga_ini').getRawValue();
						var fecFin = Ext.getCmp('_fecha_carga_fin').getRawValue();
						var cont = Ext.getCmp('_txt_con').getValue();
						var multiS = fp.getForm().findField('_cmb_multi').getValue();
						var opcion = "G";
						//se forma la cadena de parametros a enviar
						var parametros = "cadena="+cadena+"&titulo="+titulo+"&fecIni="+fecIni+"&fecFin="+fecFin+"&cont="+cont+"&multiS="+multiS+"&opcion="+opcion;
						
						fp.getForm().submit({
							url: '../15parametrizacion/15pyme/15admtabla01.ma.jsp?'+parametros,									
							waitMsg: 'Enviando datos...',
							waitTitle :'Por favor, espere',			
							success: function(form, action) {	
								var resp = action.resp;
								var mensaje = action.mensaje;
						
								Ext.MessageBox.alert("Mensaje","El Archivo ha sido Cargado .");					
				
								if(resp=='S') {
									Ext.MessageBox.alert("Mensaje","El Archivo ha sido Cargado .");					
								}else if(resp=='N') {
									Ext.MessageBox.alert("Mensaje",mensaje);
									return;
								}
							}
						});
				}
			}
			
		} else if( estadoSiguiente == "MODIFICAR" ){
			if(Ext.getCmp('_txt_tit_mod_img').getValue()=="" || Ext.getCmp('_fecha_carga_ini_mod_img').getValue()=="" || Ext.getCmp('_fecha_carga_fin_mod_img').getValue()=="" || Ext.getCmp('_txt_con_mod_img').getValue()=="" || Ext.getCmp('imagen_mod_img').getValue()==""){
				if(Ext.getCmp('_txt_tit_mod_img').getValue()=="")
					Ext.getCmp('_txt_tit_mod_img').markInvalid('Debe capturar el t�tulo de la publicaci�n');
				else if(Ext.getCmp('_fecha_carga_ini_mod_img').getValue()=="")
					Ext.getCmp('_fecha_carga_ini_mod_img').markInvalid('Debe capturar la fecha de publicaci�n inicial');
				else if(Ext.getCmp('_fecha_carga_fin_mod_img').getValue()=="")
					Ext.getCmp('_fecha_carga_fin_mod_img').markInvalid('Debe capturar la fecha de publicaci�n final');
				else if(Ext.getCmp('_txt_con_mod_img').getValue()=="")
					Ext.getCmp('_txt_con_mod_img').markInvalid('Debe capturar el contenido de la publicaci�n');
				else if(Ext.getCmp('imagen_mod_img').getValue()=="")
					Ext.getCmp('imagen_mod_img').markInvalid('Debe seleccionar una im�gen a importar');
			}
			else{
				var cadMod = Ext.getCmp('_cmb_cad').getValue();
				var publicacion = Ext.getCmp('_txt_pub_mod_img').getValue();
				var titMod = Ext.getCmp('_txt_tit_mod_img').getValue();
				var fecIniMod = Ext.getCmp('_fecha_carga_ini_mod_img').getRawValue();
				var fecFinMod = Ext.getCmp('_fecha_carga_fin_mod_img').getRawValue();
				var contMod = Ext.getCmp('_txt_con_mod_img').getValue();
				var archMod = Ext.getCmp('archivo_mod_img').getValue();
				var imgMod = Ext.getCmp('imagen_mod_img').getValue();
				var opcion = "A";
				var params = "cadMod="+cadMod+"&publicacion="+publicacion+"&titMod="+titMod+"&fecIniMod="+fecIniMod+"&fecFinMod="+fecFinMod+"&contMod="+contMod+"&archMod="+archMod+"&imgMod="+imgMod+"&opcion="+opcion;
				
				//Se envian los datos para su modificacion
				fpModImg.getForm().submit({
							url: '../15parametrizacion/15pyme/15admtabla01.ma.jsp?'+params,									
							waitMsg: 'Enviando datos...',
							waitTitle :'Por favor, espere',			
							success: function(form, action) {
								//Se tiene que volver a consultar los datos
								fpModImg.hide();
								accionConsulta("CONSULTAR", null);
							}
					});
			}
			
		}else if( estadoSiguiente == "CREARTABLA" ){	//Se obtienen los datps de los grids de tablas para su posterior creacion
		
			var bandera = true;
			var grFijos = Ext.getCmp('gridCamposFijos'); //Store de grid campos fijos
			var arrNomF = new Array();
			var arrTipoF = new Array();
			var arrLongF = new Array();
			var arrOrdenF = new Array();
			var arrTotF = new Array();
			var j = 0;
			var c = 0;
			for(i=0; i<grFijos.getStore().getTotalCount() ;i++ ){
				var registro = grFijos.getStore().getAt(i);
				if(registro.get('NOMCAMPOF')=="" || registro.get('TIPODATF')==""){
						Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
						bandera = false;
						break;
				}else if((registro.get('TIPODATF')=="alfanumerico" || registro.get('TIPODATF')=="numerico") && registro.get('LONGITUDF')==""){
						Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
						bandera = false;
						break;
				}else if(registro.get('TIPODATF')=="numerico"){
						var num = registro.get('LONGITUDF');
						var p = validaFloat(num);
						if(!p){
							Ext.MessageBox.alert("Aviso", "El formato para el campo longitud no es valido, debe ser *.1 � *.2");
							bandera = false;
							break;
						}else{
							arrNomF[i] = registro.get('NOMCAMPOF');
							arrTipoF[i] = registro.get('TIPODATF');
							arrOrdenF[i] = registro.get('ORDENF');
							if(registro.get('TOTALESF')==true){
								arrTotF[j] = i;
								j++;
							}
							var nums = num.toString().split(".");
							arrLongF[c] = nums[0];
							arrLongF[++c] = nums[1];
							c++;
						}
				}else{
					arrNomF[i] = registro.get('NOMCAMPOF');
					arrTipoF[i] = registro.get('TIPODATF');
					arrLongF[c] = registro.get('LONGITUDF')=="" ? "1" : registro.get('LONGITUDF');
					arrOrdenF[i] = registro.get('ORDENF');
					if(registro.get('TOTALESF')==true){
						arrTotF[j] = i;
						j++;
					}
					c++;
				}
			}
			
			var grEditables = Ext.getCmp('gridCamposEditables'); //Store de grid campos editables
			var arrNomE = new Array();
			var arrTipoE = new Array();
			var arrLongE = new Array();
			var arrOrdenE = new Array();
			var arrTotE = new Array();
			var j= 0;
			var c= 0;
			for(i=0; i<grEditables.getStore().getTotalCount() ;i++ ){
				var registro = grEditables.getStore().getAt(i);
				if(registro.get('NOMCAMPOE')=="" || registro.get('TIPODATE')==""){
					Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
					bandera = false;
					break;
				}else if((registro.get('TIPODATE')=="alfanumerico" || registro.get('TIPODATE')=="numerico") && registro.get('LONGITUDE')==""){
					Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
					bandera = false;
					break;
				}else if(registro.get('TIPODATE')=="numerico"){
						var num = registro.get('LONGITUDE');
						var p = validaFloat(num);
						if(!p){
							Ext.MessageBox.alert("Aviso", "El formato para el campo longitud no es valido, debe ser *.1 � *.2");
							bandera = false;
							break;
						}else{
							arrNomE[i] = registro.get('NOMCAMPOE');
							arrTipoE[i] = registro.get('TIPODATE');
							arrOrdenE[i] = registro.get('ORDENE');
							if(registro.get('TOTALESE')==true){
								arrTotE[j] = i;
								j++;
							}
							var nums = num.toString().split(".");
							arrLongE[c] = nums[0];
							arrLongE[++c] = nums[1];
							c++;
						}
				}else{
					arrNomE[i] = registro.get('NOMCAMPOE');
					arrTipoE[i] = registro.get('TIPODATE');
					arrLongE[c] = registro.get('LONGITUDE')==""?"1":registro.get('LONGITUDE');
					arrOrdenE[i] = registro.get('ORDENE');
					if(registro.get('TOTALESE')==true){
						arrTotE[i] = i;
						j++;
					}
					c++;
				}
			}
			
			var grDetalle = Ext.getCmp('gridCamposDetalle'); //Store de grid campos de detalle
			var arrNomD = new Array();
			var arrTipoD = new Array();
			var arrLongD = new Array();
			var arrOrdenD = new Array();
			var arrTotD = new Array();
			var j=0;
			var c=0;
			for(i=0; i<grDetalle.getStore().getTotalCount() ;i++ ){
				var registro = grDetalle.getStore().getAt(i);
				if(registro.get('NOMCAMPOD')=="" || registro.get('TIPODATD')==""){
					Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
					bandera = false;
					break;
				}else if((registro.get('TIPODATD')=="alfanumerico" || registro.get('TIPODATD')=="numerico") && registro.get('LONGITUDD')==""){
					Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
					bandera = false;
					break;
				}else if(registro.get('TIPODATD')=="numerico"){
						var num = registro.get('LONGITUDD');
						var p = validaFloat(num);
						if(!p){
							Ext.MessageBox.alert("Aviso", "El formato para el campo longitud no es valido, debe ser *.1 � *.2");
							bandera = false;
							break;
						}else{
							arrNomD[i] = registro.get('NOMCAMPOD');
							arrTipoD[i] = registro.get('TIPODATD');
							arrOrdenD[i] = registro.get('ORDEND');
							if(registro.get('TOTALESD')==true){
								arrTotD[j] = i;
								j++;
							}
							var nums = num.toString().split(".");
							arrLongD[c] = nums[0];
							arrLongD[++c] = nums[1];
							c++;
						}
				}else{
					arrNomD[i] = registro.get('NOMCAMPOD');
					arrTipoD[i] = registro.get('TIPODATD');
					arrLongD[c] = registro.get('LONGITUDD')==""?"1":registro.get('LONGITUDD');
					arrOrdenD[i] = registro.get('ORDEND');
					if(registro.get('TOTALESD')==true){
						arrTotD[i] = i;
						j++;
					}
					c++;
				}
			}
			
			if(bandera && Ext.getCmp('_txt_nom').getValue()==""){ //Si es el caso de copiar tabla
				var nom_tabla = Ext.getCmp('_txt_nom_tab_copy').getValue();
				Ext.getCmp('_txt_nom_tab_copy').reset();
				var numpub = Ext.getCmp('_txt_num_pub_copy').getValue();
				var cadena = Ext.getCmp('_cmb_cad').getValue();
				var copia = "copia";
				Ext.Ajax.request({
									url: '15admtabla01Ext.data.jsp',
									params: {
										informacion: 'crearTabla',
										cadena : cadena,
										copia: copia,
										numpub: numpub,
										nom_tabla : nom_tabla,
										arrNomF : arrNomF,
										arrTipoF : arrTipoF,
										arrLongF : arrLongF,
										arrOrdenF : arrOrdenF,
										arrTotF : arrTotF,
										arrNomE : arrNomE,
										arrTipoE : arrTipoE,
										arrLongE : arrLongE,
										arrOrdenE : arrOrdenE,
										arrTotE : arrTotE,
										arrNomD : arrNomD,
										arrTipoD : arrTipoD,
										arrLongD : arrLongD,
										arrOrdenD : arrOrdenD,
										arrTotD : arrTotD
										
									},
									callback: procesarSuccessFailureCreaTabla
						});
				
			}else if(bandera && Ext.getCmp('_txt_nom').getValue()!=""){ //Si es el caso de crear tabla nueva
				var nom_tabla = Ext.getCmp('_txt_nom').getValue();
				var cadena = Ext.getCmp('_cmb_cad').getValue();
				var fec_ini = Ext.getCmp('_fecha_carga_ini').getRawValue();
				var fec_fin = Ext.getCmp('_fecha_carga_fin').getRawValue();
				var Usr = fp.getForm().findField('_cmb_multi').getValue()+"|";
				Ext.Ajax.request({
									url: '15admtabla01Ext.data.jsp',
									params: {
										informacion: 'crearTabla',
										cadena : cadena,
										nom_tabla : nom_tabla,
										fec_ini : fec_ini,
										fec_fin : fec_fin,
										Usr : Usr,
										arrNomF : arrNomF,
										arrTipoF : arrTipoF,
										arrLongF : arrLongF,
										arrOrdenF : arrOrdenF,
										arrTotF : arrTotF,
										arrNomE : arrNomE,
										arrTipoE : arrTipoE,
										arrLongE : arrLongE,
										arrOrdenE : arrOrdenE,
										arrTotE : arrTotE,
										arrNomD : arrNomD,
										arrTipoD : arrTipoD,
										arrLongD : arrLongD,
										arrOrdenD : arrOrdenD,
										arrTotD : arrTotD
										
									},
									callback: procesarSuccessFailureCreaTabla
						});
			}
			
		}else if( estadoSiguiente == "MODIFICATABLA"){ //Para modificar datos de la tabla 
			
			var bandera = true;
			var grFijos = Ext.getCmp('gridCamposFijosMod'); //Store de grid campos fijos
			var arrNomF = new Array();
			var arrTipoF = new Array();
			var arrLongF = new Array();
			var arrOrdenF = new Array();
			var arrTotF = new Array();
			var j = 0;
			var c = 0;
			for(i=0; i<grFijos.getStore().getTotalCount() ;i++ ){
				var registro = grFijos.getStore().getAt(i);
				if(registro.get('NOMCAMPOF')=="" || registro.get('TIPODATF')==""){
						Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
						bandera = false;
						break;
				}else if((registro.get('TIPODATF')=="alfanumerico" || registro.get('TIPODATF')=="numerico") && registro.get('LONGITUDF')==""){
						Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
						bandera = false;
						break;
				}else if(registro.get('TIPODATF')=="numerico"){
					var num = registro.get('LONGITUDF');
					var p = validaFloat(num);
					if(!p){
						Ext.MessageBox.alert("Aviso", "El formato para el campo longitud no es valido, debe ser *.1 � *.2");
						bandera = false;
						break;
					}else{
						arrNomF[i] = registro.get('NOMCAMPOF');
						arrTipoF[i] = registro.get('TIPODATF');
						arrOrdenF[i] = registro.get('ORDENF');
						if(registro.get('TOTALESF')==true){
							arrTotF[j] = i;
							j++;
						}
						var nums = num.toString().split(".");
						arrLongF[c] = nums[0];
						arrLongF[++c] = nums[1];
						c++;
					}
				}else{
					arrNomF[i] = registro.get('NOMCAMPOF');
					arrTipoF[i] = registro.get('TIPODATF');
					arrLongF[i] = registro.get('LONGITUDF')=="" ? "1" : registro.get('LONGITUDF');
					arrOrdenF[i] = registro.get('ORDENF');
					if(registro.get('TOTALESF')==true){
						arrTotF[j] = i;
						j++;
					}
					c++;
				}
			}
			
			var grEditables = Ext.getCmp('gridCamposEditablesMod'); //Store de grid campos editables
			var arrNomE = new Array();
			var arrTipoE = new Array();
			var arrLongE = new Array();
			var arrOrdenE = new Array();
			var arrTotE = new Array();
			var j= 0;
			var c= 0;
			for(i=0; i<grEditables.getStore().getTotalCount() ;i++ ){
				var registro = grEditables.getStore().getAt(i);
				if(registro.get('NOMCAMPOE')=="" || registro.get('TIPODATE')==""){
					Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
					bandera = false;
					break;
				}else if((registro.get('TIPODATE')=="alfanumerico" || registro.get('TIPODATE')=="numerico") && registro.get('LONGITUDE')==""){
					Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
					bandera = false;
					break;
				}else if(registro.get('TIPODATE')=="numerico"){
					var num = registro.get('LONGITUDE');
					var p = validaFloat(num);
					if(!p){
						Ext.MessageBox.alert("Aviso", "El formato para el campo longitud no es valido, debe ser *.1 � *.2");
						bandera = false;
						break;
					}else{
						arrNomE[i] = registro.get('NOMCAMPOE');
						arrTipoE[i] = registro.get('TIPODATE');
						arrOrdenE[i] = registro.get('ORDENE');
						if(registro.get('TOTALESE')==true){
							arrTotE[j] = i;
							j++;
						}
						var nums = num.toString().split(".");
						arrLongE[c] = nums[0];
						arrLongE[++c] = nums[1];
						c++;
					}
				}else{
					arrNomE[i] = registro.get('NOMCAMPOE');
					arrTipoE[i] = registro.get('TIPODATE');
					arrLongE[i] = registro.get('LONGITUDE')==""?"1":registro.get('LONGITUDE');
					arrOrdenE[i] = registro.get('ORDENE');
					if(registro.get('TOTALESE')==true){
						arrTotE[i] = i;
						j++;
					}
					c++;
				}
			}
			
			var grDetalle = Ext.getCmp('gridCamposDetalleMod'); //Store de grid campos de detalle
			var arrNomD = new Array();
			var arrTipoD = new Array();
			var arrLongD = new Array();
			var arrOrdenD = new Array();
			var arrTotD = new Array();
			var j=0;
			var c = 0;
			for(i=0; i<grDetalle.getStore().getTotalCount() ;i++ ){
				var registro = grDetalle.getStore().getAt(i);
				if(registro.get('NOMCAMPOD')=="" || registro.get('TIPODATD')==""){
					Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
					bandera = false;
					break;
				}else if((registro.get('TIPODATD')=="alfanumerico" || registro.get('TIPODATD')=="numerico") && registro.get('LONGITUDD')==""){
					Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
					bandera = false;
					break;
				}else if(registro.get('TIPODATD')=="numerico"){
					var num = registro.get('LONGITUDD');
					var p = validaFloat(num);
					if(!p){
						Ext.MessageBox.alert("Aviso", "El formato para el campo longitud no es valido, debe ser *.1 � *.2");
						bandera = false;
						break;
					}else{
						arrNomD[i] = registro.get('NOMCAMPOD');
						arrTipoD[i] = registro.get('TIPODATD');
						arrOrdenD[i] = registro.get('ORDEND');
						if(registro.get('TOTALESD')==true){
							arrTotD[j] = i;
							j++;
						}
						var nums = num.toString().split(".");
						arrLongD[c] = nums[0];
						arrLongD[++c] = nums[1];
						c++;
					}
				}else{
					arrNomD[i] = registro.get('NOMCAMPOD');
					arrTipoD[i] = registro.get('TIPODATD');
					arrLongD[i] = registro.get('LONGITUDD')==""?"1":registro.get('LONGITUDD');
					arrOrdenD[i] = registro.get('ORDEND');
					if(registro.get('TOTALESD')==true){
						arrTotD[i] = i;
						j++;
					}
					c++;
				}
			}
			
			if(Ext.getCmp('_txt_nom_mod_tab').getValue()=="" || Ext.getCmp('_fecha_carga_ini_mod_tab').getValue()=="" || Ext.getCmp('_fecha_carga_fin_mod_tab').getValue()==""){
				Ext.MessageBox.alert("Aviso", "Te hacen falta datos por llenar.");
				bandera = false;
			} 
			
			if(bandera){
				var cadena = Ext.getCmp('_cmb_cad').getValue();
				var accion = "Modifica";
				var publicacion = Ext.getCmp('_txt_num_mod_tab').getValue();
				var nom_tabla = Ext.getCmp('_txt_nom_mod_tab').getValue();
				var fec_ini = Ext.getCmp('_fecha_carga_ini_mod_tab').getRawValue();
				var fec_fin = Ext.getCmp('_fecha_carga_fin_mod_tab').getRawValue();
				Ext.Ajax.request({
									url: '15admtabla01Ext.data.jsp',
									params: {
										informacion: 'crearTabla',
										cadena : cadena,
										accion : accion,
										publicacion : publicacion,
										nom_tabla : nom_tabla,
										fec_ini : fec_ini,
										fec_fin : fec_fin,
										arrNomF : arrNomF,
										arrTipoF : arrTipoF,
										arrLongF : arrLongF,
										arrOrdenF : arrOrdenF,
										arrTotF : arrTotF,
										arrNomE : arrNomE,
										arrTipoE : arrTipoE,
										arrLongE : arrLongE,
										arrOrdenE : arrOrdenE,
										arrTotE : arrTotE,
										arrNomD : arrNomD,
										arrTipoD : arrTipoD,
										arrLongD : arrLongD,
										arrOrdenD : arrOrdenD,
										arrTotD : arrTotD
										
									},
									callback: procesarSuccessFailureModificaTabla
						});
			}
			
		}else if(estadoSiguiente == "COPIAR"){
			validaNombreCopia();
		}else if(	estadoSiguiente == "LIMPIAR"){	//Para borrar los datos capturados en el form principal
			var mant = Ext.getCmp('_cmb_mant').getValue();
			Ext.getCmp('forma').getForm().reset();
			if(mant == "1")
				Ext.getCmp('_cmb_mant').setValue('1');
			else
				Ext.getCmp('_cmb_mant').setValue('2');
		}
	}
	
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - - - - - - - - - -

	//Combo que va dentro del EditorGridPanel de Fijos
	var comboTipoDatoF = new Ext.form.ComboBox({  
	 mode: 'local',
	 id: '_cmb_tipof',	 
	 displayField : 'descripcion',
	 valueField : 'clave',
    triggerAction : 'all',
	 typeAhead: true,
	 minChars : 1,
    allowBlank: true,
	 editable: false,
    store: storeTipodeDato
	});
	
	//Combo que va dentro del EditorGridPanel de Editables
	var comboTipoDatoE = new Ext.form.ComboBox({  
	 mode: 'local',
	 id: '_cmb_tipoe',	 
	 displayField : 'descripcion',
	 valueField : 'clave',
    triggerAction : 'all',
	 typeAhead: true,
	 minChars : 1,
    allowBlank: true,
	 editable: false,
    store: storeTipodeDato 
	});
	
	//Combo que va dentro del EditorGridPanel de Detalle
	var comboTipoDatoD = new Ext.form.ComboBox({  
	 mode: 'local',
	 id: '_cmb_tipod',	 
	 displayField : 'descripcion',
	 valueField : 'clave',
    triggerAction : 'all',
	 typeAhead: true,
	 minChars : 1,
    allowBlank: true,
	 editable: false,
    store: storeTipodeDato 
	});

//----------++++++GRIDS PARA LA GENERACION DE LAS TABLAS+++++++-----------//

	//Elementos del grid para campos fijos
	var gridFijos = new Ext.grid.EditorGridPanel({
		store: 		camposFijosData,
		id:			'gridCamposFijos',
		title: 'Definici�n de campos fijos de la tabla',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		clicksToEdit:1,
		margins:		'20 0 0 0',
		stripeRows: true,
		loadMask: 	true,
		height: 		240,
		width: 		610,
		frame: 		false,
		columns: [
				{
					header: 		'Nombre',
					tooltip: 	'Nombre del campo',
					dataIndex: 	'NOMCAMPOF',
					align:		'center',
					editor: {
						xtype: 'textfield',
						maxLength:25		 
					},
					resizable: 	false,
					width: 		175,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Tipo',
					tooltip: 	'Tipo de dato para el campo',
					dataIndex: 	'TIPODATF',
					align:		'center',
					editor: comboTipoDatoF,
					width: 		133,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro){
						var record = comboTipoDatoF.findRecord(comboTipoDatoF.valueField, value);
						return NE.util.colorCampoEdit(record ? record.get(comboTipoDatoF.displayField) : comboTipoDatoF.valueNotFoundText,metadata,registro);
					}
				},
				{
					header: 'Longitud',
					dataIndex: 'Longitud del campo',
					dataIndex: 	'LONGITUDF',
					align:		'center',
					editor: {
						xtype: 'numberfield',
						maxLength:11		 
					},
					resizable: 	false,
					width: 		95,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Ordenamiento',
					tooltip: 	'Ordenamiento del campo',
					dataIndex: 	'ORDENF',
					align:		'center',
					editor: {
					 xtype: 'numberfield',
					 maxLength:2		 
					},
					resizable: 	false,
					width: 		80,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					xtype: 'checkcolumn',				
					header:'Totales',
					tooltip: 'Totales',
					dataIndex : 'TOTALESF',
					width : 55,
					align: 'center',
					sortable : false			
				 },
				 {
					xtype: 'actioncolumn',
					header: 'Seleccione',
					tooltip: 'Eliminar campo',
					resizable: 	false,
					width: 65,
					align: 'center',					
					items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Eliminar';
							return 'icoEliminar';										
						},	
						handler: function(boton, evento) {
							//Se debe eliminar una fila en el grid
							var g = Ext.getCmp('gridCamposFijos');
							var numFijos = g.getStore().getTotalCount()-1;
							Ext.StoreMgr.key('camposFijosDataStore').load({
							params:
									{
										informacion: 'camposFijos',
										numFijos: numFijos
									}
							});
						}
					}
				]				
			}
		],
		listeners: {
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridFijos = e.gridFijos; 
				var campo= e.field;
				
				if(campo == 'LONGITUDF') {					
					if(record.data['TIPODATF']=="alfanumerico" || record.data['TIPODATF']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDF']='';
						record.commit();	
						return false;		
					}
				}						
			},
			afteredit : function(e){
				var record = e.record;
				var gridFijos = e.gridFijos; 
				var campo= e.field;
				
				if(campo == 'LONGITUDF') {					
					if(record.data['TIPODATF']=="alfanumerico" || record.data['TIPODATF']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDF']='';
						record.commit();	
						return false;		
					}
				}
			}
		},bbar: {
			buttonAlign: 	'right',
			id: 			'barraPaginacionFijos',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Agrega Campo Fijo',
					iconCls:	'icoAgregar',
					id: 		'btnAgrCampFijo',
					handler: function(boton, evento) {
						//Se debe agregar una nueva fila en el grid
						var g = Ext.getCmp('gridCamposFijos');
						var numFijos = g.getStore().getTotalCount()+1;
						Ext.StoreMgr.key('camposFijosDataStore').load({
						params:
								{
									informacion: 'camposFijos',
									numFijos: numFijos
								}
						});
					
					}
				},'-'
			]
		}
	});
	
	var gridFijosMod = new Ext.grid.EditorGridPanel({
		store: 		camposFijosModData,
		id:			'gridCamposFijosMod',
		title: 'Definici�n de campos fijos de la tabla',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		clicksToEdit:1,
		margins:		'20 0 0 0',
		stripeRows: true,
		loadMask: 	true,
		height: 		240,
		width: 		610,
		frame: 		false,
		columns: [
				{
					header: 		'Nombre',
					tooltip: 	'Nombre del campo',
					dataIndex: 	'NOMCAMPOF',
					align:		'center',
					editor: {
						xtype: 'textfield',
						maxLength:25		 
					},
					resizable: 	false,
					width: 		175,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Tipo',
					tooltip: 	'Tipo de dato para el campo',
					dataIndex: 	'TIPODATF',
					align:		'center',
					editor: comboTipoDatoF,
					width: 		133,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro){
						var record = comboTipoDatoF.findRecord(comboTipoDatoF.valueField, value);
						return NE.util.colorCampoEdit(record ? record.get(comboTipoDatoF.displayField) : comboTipoDatoF.valueNotFoundText,metadata,registro);
					}
				},
				{
					header: 'Longitud',
					dataIndex: 'Longitud del campo',
					dataIndex: 	'LONGITUDF',
					align:		'center',
					editor: {
						xtype: 'numberfield',
						maxLength:11	 
					},
					resizable: 	false,
					width: 		95,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Ordenamiento',
					tooltip: 	'Ordenamiento del campo',
					dataIndex: 	'ORDENF',
					align:		'center',
					editor: {
					 xtype: 'numberfield',
					 maxLength:2		 
					},
					resizable: 	false,
					width: 		80,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					xtype: 'checkcolumn',				
					header:'Totales',
					tooltip: 'Totales',
					dataIndex : 'TOTALESF',
					width : 55,
					align: 'center',
					sortable : false			
				 },
				 {
					xtype: 'actioncolumn',
					header: 'Seleccione',
					tooltip: 'Eliminar campo',
					resizable: 	false,
					width: 65,
					align: 'center',					
					items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Eliminar';
							return 'icoEliminar';										
						},	
						handler: function(boton, evento) {
							//Se debe eliminar una fila en el grid
							var g = Ext.getCmp('gridCamposFijosMod');
							var numFijosMod = g.getStore().getTotalCount()-1;
							Ext.StoreMgr.key('camposFijosModDataStore').load({
							params:
									{
										informacion: 'camposFijosMod',
										numFijosMod: numFijosMod
									}
							});
						}
					}
				]				
			}
		],
		listeners: {
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridFijosMod = e.gridFijosMod; 
				var campo= e.field;
				
				if(campo == 'LONGITUDF') {					
					if(record.data['TIPODATF']=="alfanumerico" || record.data['TIPODATF']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDF']='';
						record.commit();	
						return false;		
					}
				}						
			},
			afteredit : function(e){
				var record = e.record;
				var gridFijosMod = e.gridFijosMod; 
				var campo= e.field;
				
				if(campo == 'LONGITUDF') {					
					if(record.data['TIPODATF']=="alfanumerico" || record.data['TIPODATF']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDF']='';
						record.commit();	
						return false;		
					}
				}
			}
		},bbar: {
			buttonAlign: 	'right',
			id: 			'barraPaginacionFijosMod',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Agrega Campo Fijo',
					iconCls:	'icoAgregar',
					id: 		'btnAgrCampFijoMod',
					handler: function(boton, evento) {
						//Se debe agregar una nueva fila en el grid
						var g = Ext.getCmp('gridCamposFijosMod');
						var numFijosMod = g.getStore().getTotalCount()+1;
						Ext.StoreMgr.key('camposFijosModDataStore').load({
						params:
								{
									informacion: 'camposFijosMod',
									numFijosMod: numFijosMod
								}
						});
					
					}
				},'-'
			]
		}
	});
	
	//Elementos del grid para campos editables
	var gridEditables = new Ext.grid.EditorGridPanel({
		store: 		camposEditablesData,
		id:			'gridCamposEditables',
		title: 'Definici�n de campos editables de la tabla',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		clicksToEdit:1,
		margins:		'20 0 0 0',
		stripeRows: true,
		loadMask: 	true,
		height: 		240,
		width: 		610,
		frame: 		false,
		columns: [
				{
					header: 		'Nombre',
					tooltip: 	'Nombre del campo',
					dataIndex: 	'NOMCAMPOE',
					align:		'center',
					editor: {
						xtype: 'textfield',
						maxLength:25		 
					},
					resizable: 	false,
					width: 		175,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Tipo',
					tooltip: 	'Tipo de dato para el campo',
					dataIndex: 	'TIPODATE',
					align:		'center',
					editor: comboTipoDatoE,
					width: 		133,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro){
						var record = comboTipoDatoE.findRecord(comboTipoDatoE.valueField, value);
						return NE.util.colorCampoEdit(record ? record.get(comboTipoDatoE.displayField) : comboTipoDatoE.valueNotFoundText,metadata,registro);
					}
				},
				{
					header: 'Longitud',
					dataIndex: 'Longitud del campo',
					dataIndex: 	'LONGITUDE',
					align:		'center',
					editor: {
						xtype: 'numberfield',
						maxLength:11	 
					},
					resizable: 	false,
					width: 		95,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Ordenamiento',
					tooltip: 	'Ordenamiento del campo',
					dataIndex: 	'ORDENE',
					align:		'center',
					editor: {
					 xtype: 'numberfield',
					 maxLength:2		 
					},
					resizable: 	false,
					width: 		80,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					xtype: 'checkcolumn',				
					header:'Totales',
					tooltip: 'Totales',
					dataIndex : 'TOTALESE',
					width : 55,
					align: 'center',
					sortable : false			
				 },
				 {
					xtype: 'actioncolumn',
					header: 'Seleccione',
					tooltip: 'Eliminar campo',
					resizable: 	false,
					width: 65,
					align: 'center',					
					items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Eliminar';
							return 'icoEliminar';										
						},	
						handler: function(boton, evento) {
						//Se debe eliminar una fila en el grid
						var g = Ext.getCmp('gridCamposEditables');
						var numEdi = g.getStore().getTotalCount()-1;
						Ext.StoreMgr.key('camposEditablesDataStore').load({
						params:
								{
									informacion: 'camposEditables',
									numEdi: numEdi
								}
						});
					
					}
					}
				]				
			}
		],
		listeners: {
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridFijos = e.gridFijos; 
				var campo= e.field;
				
				if(campo == 'LONGITUDE') {					
					if(record.data['TIPODATE']=="alfanumerico" || record.data['TIPODATE']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDE']='';
						record.commit();	
						return false;		
					}
				}						
			},
			afteredit : function(e){
				var record = e.record;
				var gridFijos = e.gridFijos; 
				var campo= e.field;
				
				if(campo == 'LONGITUDE') {					
					if(record.data['TIPODATE']=="alfanumerico" || record.data['TIPODATE']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDE']='';
						record.commit();	
						return false;		
					}
				}
			}
		},bbar: {
			buttonAlign: 	'right',
			id: 			'barraPaginacionEditables',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Agrega Campo Editable',
					iconCls:	'icoAgregar',
					id: 		'btnAgrCampEditable',
					handler: function(boton, evento) {
						//Se debe agregar una nueva fila en el grid
						var g = Ext.getCmp('gridCamposEditables');
						var numEdi = g.getStore().getTotalCount()+1;
						Ext.StoreMgr.key('camposEditablesDataStore').load({
						params:
								{
									informacion: 'camposEditables',
									numEdi: numEdi
								}
						});
					
					}
				},'-'
			]
		}
	});
	
	var gridEditablesMod = new Ext.grid.EditorGridPanel({
		store: 		camposEditablesModData,
		id:			'gridCamposEditablesMod',
		title: 'Definici�n de campos editables de la tabla',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		clicksToEdit:1,
		margins:		'20 0 0 0',
		stripeRows: true,
		loadMask: 	true,
		height: 		240,
		width: 		610,
		frame: 		false,
		columns: [
				{
					header: 		'Nombre',
					tooltip: 	'Nombre del campo',
					dataIndex: 	'NOMCAMPOE',
					align:		'center',
					editor: {
						xtype: 'textfield',
						maxLength:25		 
					},
					resizable: 	false,
					width: 		175,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Tipo',
					tooltip: 	'Tipo de dato para el campo',
					dataIndex: 	'TIPODATE',
					align:		'center',
					editor: comboTipoDatoE,
					width: 		133,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro){
						var record = comboTipoDatoE.findRecord(comboTipoDatoE.valueField, value);
						return NE.util.colorCampoEdit(record ? record.get(comboTipoDatoE.displayField) : comboTipoDatoE.valueNotFoundText,metadata,registro);
					}
				},
				{
					header: 'Longitud',
					dataIndex: 'Longitud del campo',
					dataIndex: 	'LONGITUDE',
					align:		'center',
					editor: {
						xtype: 'numberfield',
						maxLength:11		 
					},
					resizable: 	false,
					width: 		95,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Ordenamiento',
					tooltip: 	'Ordenamiento del campo',
					dataIndex: 	'ORDENE',
					align:		'center',
					editor: {
					 xtype: 'numberfield',
					 maxLength:2		 
					},
					resizable: 	false,
					width: 		80,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					xtype: 'checkcolumn',				
					header:'Totales',
					tooltip: 'Totales',
					dataIndex : 'TOTALESE',
					width : 55,
					align: 'center',
					sortable : false			
				 },
				 {
					xtype: 'actioncolumn',
					header: 'Seleccione',
					tooltip: 'Eliminar campo',
					resizable: 	false,
					width: 65,
					align: 'center',					
					items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Eliminar';
							return 'icoEliminar';										
						},	
						handler: function(grid, rowIndex, colIndex, item, event) {
						//Se debe eliminar una fila en el grid
					/*	var g = Ext.getCmp('gridConsulta');
						var registro = g.getStore().getAt(rowIndex);*/
						var numPubTab = Ext.getCmp('_txt_num_mod_tab').getValue();
						var numEdiMod = grid.getStore().getTotalCount()-1;
						Ext.StoreMgr.key('camposEditablesModDataStore').load({
						params:
								{
									informacion: 'camposEditablesMod',
									numEdiMod: numEdiMod,
									numPubTab: numPubTab
								}
						});
						}
					}
				]				
			}
		],
		listeners: {
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridFijos = e.gridFijos; 
				var campo= e.field;
				
				if(campo == 'LONGITUDE') {					
					if(record.data['TIPODATE']=="alfanumerico" || record.data['TIPODATE']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDE']='';
						record.commit();	
						return false;		
					}
				}						
			},
			afteredit : function(e){
				var record = e.record;
				var gridFijos = e.gridFijos; 
				var campo= e.field;
				
				if(campo == 'LONGITUDE') {					
					if(record.data['TIPODATE']=="alfanumerico" || record.data['TIPODATE']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDE']='';
						record.commit();	
						return false;		
					}
				}
			}
		},bbar: {
			buttonAlign: 	'right',
			id: 			'barraPaginacionEditablesMod',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Agrega Campo Editable',
					iconCls:	'icoAgregar',
					id: 		'btnAgrCampEditableMod',
					handler: function(boton, evento) {
						//Se debe agregar una nueva fila en el grid
						var numPubTab = Ext.getCmp('_txt_num_mod_tab').getValue();
						var g = Ext.getCmp('gridCamposEditablesMod');
						var numEdiMod = g.getStore().getTotalCount()+1;
						Ext.StoreMgr.key('camposEditablesModDataStore').load({
						params:
								{
									informacion: 'camposEditablesMod',
									numEdiMod: numEdiMod,
									numPubTab: numPubTab
								}
						});
					
					}
				},'-'
			]
		}
	});
	
	//Elementos del grid para campos de detalle
	var gridDetalle = new Ext.grid.EditorGridPanel({
		store: 		camposDetalleData,
		id:			'gridCamposDetalle',
		title: 'Definici�n de campos de detalle de la tabla',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		clicksToEdit:1,
		margins:		'20 0 0 0',
		stripeRows: true,
		loadMask: 	true,
		height: 		240,
		width: 		610,
		frame: 		false,
		columns: [
				{
					header: 		'Nombre',
					tooltip: 	'Nombre del campo',
					dataIndex: 	'NOMCAMPOD',
					align:		'center',
					editor: {
						xtype: 'textfield',
						maxLength:25		 
					},
					resizable: 	false,
					width: 		175,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Tipo',
					tooltip: 	'Tipo de dato para el campo',
					dataIndex: 	'TIPODATD',
					align:		'center',
					editor: comboTipoDatoD,
					width: 		133,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro){
						var record = comboTipoDatoD.findRecord(comboTipoDatoD.valueField, value);
						return NE.util.colorCampoEdit(record ? record.get(comboTipoDatoD.displayField) : comboTipoDatoD.valueNotFoundText,metadata,registro);
					}
				},
				{
					header: 'Longitud',
					dataIndex: 'Longitud del campo',
					dataIndex: 	'LONGITUDD',
					align:		'center',
					editor: {
						xtype: 'numberfield',
						maxLength:11		 
					},
					resizable: 	false,
					width: 		95,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Ordenamiento',
					tooltip: 	'Ordenamiento del campo',
					dataIndex: 	'ORDEND',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maskRe:		/[0-9]/,
					 maxLength:2		 
					},
					resizable: 	false,
					width: 		80,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					xtype: 'checkcolumn',				
					header:'Totales',
					tooltip: 'Totales',
					dataIndex : 'TOTALESD',
					width : 55,
					align: 'center',
					sortable : false			
				 },
				 {
					xtype: 'actioncolumn',
					header: 'Seleccione',
					tooltip: 'Eliminar campo',
					resizable: 	false,
					width: 65,
					align: 'center',					
					items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Eliminar';
							return 'icoEliminar';										
						},	
						handler: function(boton, evento) {
							//Se debe eliminar una fila en el grid
							var g = Ext.getCmp('gridCamposDetalle');
							var numDet = g.getStore().getTotalCount()-1;
							Ext.StoreMgr.key('camposDetalleDataStore').load({
							params:
									{
										informacion: 'camposDetalle',
										numDet: numDet
									}
							});
						}
					}
				]				
			}
		],
		listeners: {
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridFijos = e.gridFijos; 
				var campo= e.field;
				
				if(campo == 'LONGITUDD') {					
					if(record.data['TIPODATD']=="alfanumerico" || record.data['TIPODATD']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDD']='';
						record.commit();	
						return false;		
					}
				}						
			},
			afteredit : function(e){
				var record = e.record;
				var gridFijos = e.gridFijos; 
				var campo= e.field;
				
				if(campo == 'LONGITUDD') {					
					if(record.data['TIPODATD']=="alfanumerico" || record.data['TIPODATD']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDD']='';
						record.commit();	
						return false;		
					}
				}
			}
		},bbar: {
			buttonAlign: 	'right',
			id: 			'barraPaginacionDetale',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Agrega Campo Detalle',
					iconCls:	'icoAgregar',
					id: 		'btnAgrCampDetalle',
					handler: function(boton, evento) {
						/*if(g.getStore().getTotalCount()==10)
							Ext.getCmp('btnAgrCampDetalle').disable();*/
						//Se debe agregar una nueva fila en el grid
						var g = Ext.getCmp('gridCamposDetalle');
						var numDet = g.getStore().getTotalCount()+1;
						Ext.StoreMgr.key('camposDetalleDataStore').load({
						params:
								{
									informacion: 'camposDetalle',
									numDet: numDet
								}
						});
					}
				},'-'
			]
		}
	});
	
	var gridDetalleMod = new Ext.grid.EditorGridPanel({
		store: 		camposDetalleModData,
		id:			'gridCamposDetalleMod',
		title: 'Definici�n de campos de detalle de la tabla',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		clicksToEdit:1,
		margins:		'20 0 0 0',
		stripeRows: true,
		loadMask: 	true,
		height: 		240,
		width: 		610,
		frame: 		false,
		columns: [
				{
					header: 		'Nombre',
					tooltip: 	'Nombre del campo',
					dataIndex: 	'NOMCAMPOD',
					align:		'center',
					editor: {
						xtype: 'textfield',
						maxLength:25		 
					},
					resizable: 	false,
					width: 		175,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Tipo',
					tooltip: 	'Tipo de dato para el campo',
					dataIndex: 	'TIPODATD',
					align:		'center',
					editor: comboTipoDatoD,
					width: 		133,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro){
						var record = comboTipoDatoD.findRecord(comboTipoDatoD.valueField, value);
						return NE.util.colorCampoEdit(record ? record.get(comboTipoDatoD.displayField) : comboTipoDatoD.valueNotFoundText,metadata,registro);
					}
				},
				{
					header: 'Longitud',
					dataIndex: 'Longitud del campo',
					dataIndex: 	'LONGITUDD',
					align:		'center',
					editor: {
						xtype: 'textfield',
						maxLength:11		 
					},
					resizable: 	false,
					width: 		95,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					header: 		'Ordenamiento',
					tooltip: 	'Ordenamiento del campo',
					dataIndex: 	'ORDEND',
					align:		'center',
					editor: {
					 xtype: 'textfield',
					 maskRe:		/[0-9]/,
					 maxLength:2		 
					},
					resizable: 	false,
					width: 		80,
					hidden: 		false,
					hideable:	false,
					renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},{
					xtype: 'checkcolumn',				
					header:'Totales',
					tooltip: 'Totales',
					dataIndex : 'TOTALESD',
					width : 55,
					align: 'center',
					sortable : false			
				 },
				 {
					xtype: 'actioncolumn',
					header: 'Seleccione',
					tooltip: 'Eliminar campo',
					resizable: 	false,
					width: 65,
					align: 'center',					
					items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Eliminar';
							return 'icoEliminar';										
						},	
						handler: function(boton, evento) {
							//Se debe eliminar una fila en el grid
							var g = Ext.getCmp('gridCamposDetalleMod');
							var numPubTab = Ext.getCmp('_txt_num_mod_tab').getValue();
							var numDetMod = g.getStore().getTotalCount()-1;
							Ext.StoreMgr.key('camposDetalleModDataStore').load({
							params:
									{
										informacion: 'camposDetalleMod',
										numDetMod: numDetMod,
										numPubTab: numPubTab
									}
							});
						}
					}
				]				
			}
		],
		listeners: {
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridFijos = e.gridFijos; 
				var campo= e.field;
				
				if(campo == 'LONGITUDD') {					
					if(record.data['TIPODATD']=="alfanumerico" || record.data['TIPODATD']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDD']='';
						record.commit();	
						return false;		
					}
				}						
			},
			afteredit : function(e){
				var record = e.record;
				var gridFijos = e.gridFijos; 
				var campo= e.field;
				
				if(campo == 'LONGITUDD') {					
					if(record.data['TIPODATD']=="alfanumerico" || record.data['TIPODATD']=="numerico" ) {					
						return true;		
					}				
					else{
						record.data['LONGITUDD']='';
						record.commit();	
						return false;		
					}
				}
			}
		},bbar: {
			buttonAlign: 	'right',
			id: 			'barraPaginacionDetalleMod',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Agrega Campo Detalle',
					iconCls:	'icoAgregar',
					id: 		'btnAgrCampDetalleMod',
					handler: function(boton, evento) {
						//Se debe agregar una nueva fila en el grid
						var g = Ext.getCmp('gridCamposDetalleMod');
						var numPubTab = Ext.getCmp('_txt_num_mod_tab').getValue();
						var numDetMod = g.getStore().getTotalCount()+1;
						Ext.StoreMgr.key('camposDetalleModDataStore').load({
						params:
								{
									informacion: 'camposDetalleMod',
									numDetMod: numDetMod,
									numPubTab: numPubTab
								}
						});
					}
				},'-'
			]
		}
	});


	//Elementos del grid de la consulta de tablas
		var grid = new Ext.grid.GridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			title: 'Consulta de Tablas',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			stripeRows: true,
			loadMask: 	true,
			height: 		260,
			width: 		828,
			frame: 		false,
			columns: [
				{
					header: 		'N�mero de publicaci�n',
					tooltip: 	'N�mero de publicaci�n',
					dataIndex: 	'NUMPUB',
					sortable: 	true,
					align:		'center',
					resizable: 	false,
					width: 		120,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Nombre',
					tooltip: 	'Nombre de la publicaci�n',
					dataIndex: 	'NOMPUB',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		172,
					hidden: 		false,
					hideable:	false,
					renderer: function(val, meta, record, rowIndex, colIndex, store) {
						return  '<div style="display:inline;color:blue">' + val + '</div>'
					}
				},{
					header: 		'Desde',
					tooltip: 	'Fecha inicial',
					dataIndex: 	'FECINI',
					align:		'center',
					sortable: 	true,
					resizable: 	false,
					width: 		115,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Hasta',
					tooltip: 	'Fecha final',
					dataIndex: 	'FECFIN',
					align:		'center',
					sortable: 	true,
					resizable: 	false,
					width: 		115,
					hideable:	false
				},{
					header: 		'Columnas',
					tooltip: 	'N�mero de columnas',
					dataIndex: 	'NUMCOL',
					align:		'center',
					sortable: 	true,
					resizable: 	false,
					width: 		80,
					hidden: 		false,
					hideable:	false
				},{
					xtype: 'actioncolumn',
					header: 'Datos',
					tooltip: 'Modificar-Consultar',
					resizable: 	false,
					width: 100,
					hidden: 		false,
					hideable:	false,
					align: 'center',					
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Modificar';
									return 'icoModificar';
							},
							handler: modificarDatTabla
						},{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
									this.items[1].tooltip = 'Consultar';
									return 'icoLupa';
							},
							handler: consultarDatTabla
						}
					]				
				},{
					xtype: 'actioncolumn',
					header: 'Estructura',
					tooltip: 'Modificar-Eliminar-Copiar',
					resizable: 	false,
					width: 115,
					hidden: 		false,
					hideable:	false,
					align: 'center',					
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Modificar';
									return 'icoModificar';
							},
							handler: modificarTabla
						},{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
									this.items[1].tooltip = 'Eliminar';
									return 'icoEliminar';
							},
							handler: eliminarTabla
						},{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
									this.items[2].tooltip = 'Copiar';
									return 'icoGuardar';
							},
							//handler: copiarTabla
							handler: function(grid, rowIndex, colIndex, item, event){
								var registro = grid.getStore().getAt(rowIndex);
								var numPub = registro.get('NUMPUB');
								Ext.getCmp('winCopiar').show();
								Ext.getCmp('_txt_num_pub_copy').setValue(numPub);
							}
						}
					]				
				}
			],bbar: {
			buttonAlign: 	'right',
			id: 			'barraPaginacion',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Regresar',
					width:	100,
					iconCls:	'icoRegresar',
					id: 		'btnRegresar',
					handler: function(boton, evento) {
						grid.hide();
						fp.show();
						Ext.getCmp('forma').getForm().reset();
						Ext.getCmp('_cmb_mant').setValue('1');
					}
				},'-'
			]
		}
	});
	
	//Elementos del grid de la consulta de imagenes con texto
		var grid2 = new Ext.grid.GridPanel({
			store: 		registrosConsultados2Data,
			id:			'gridConsulta2',
			title: 'Consulta de textos con imagenes',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			stripeRows: true,
			loadMask: 	true,
			height: 		267,
			width: 		828,
			frame: 		false,
			columns: [
				{
					header: 		'N�mero de publicaci�n',
					tooltip: 	'N�mero de publicaci�n',
					dataIndex: 	'NUMPUBIM',
					sortable: 	true,
					align:		'center',
					resizable: 	false,
					width: 		120,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Nombre',
					tooltip: 	'Nombre de la publicaci�n',
					dataIndex: 	'NOMPUBIM',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		140,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Desde',
					tooltip: 	'Fecha inicial',
					dataIndex: 	'FECINIIM',
					align:		'center',
					sortable: 	true,
					resizable: 	false,
					width: 		115,
					hidden: 		false,
					hideable:	false
				},{
					header: 		'Hasta',
					tooltip: 	'Fecha final',
					dataIndex: 	'FECFINIM',
					align:		'center',
					sortable: 	true,
					resizable: 	false,
					width: 		115,
					hideable:	false
				},{
					xtype: 'actioncolumn',
					header: 'Imagen',
					tooltip: 'Nombre del archivo de imagen',
					dataIndex: 	'IMAGEN',
					resizable: 	true,
					width: 100,
					hidden: 		false,
					hideable:	false,
					align: 'left',	
					renderer:  function (value,metaData,registro,rowIndex,colIndex,store){
										return '<div style="display:inline;color:blue">' +value + ' '+ '</div>';
					},
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Ver';
									return 'icoLupa';
							},
							handler: descargaImagen	
						}
					]				
				},{
					xtype: 'actioncolumn',
					header: 'Archivo',
					tooltip: 'Nombre del archivo',
					dataIndex: 	'ARCHIVO',
					resizable: 	true,
					width: 115,
					hidden: 		false,
					hideable:	false,
					align: 'left',		
					renderer:  function (value,metaData,registro,rowIndex,colIndex,store){
										if(value==""){value="null"}
										return '<div style="display:inline;color:blue">' +value + ' '+ '</div>';
					},
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Ver';
									return 'icoLupa';
							},
							handler: descargaArchivo
						}
					]				
				},{
					header: 		'Contenido',
					tooltip: 	'Contenido',
					dataIndex: 	'CONTENIDO',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		138,
					hidden: 		false,
					hideable:	false
				},{
					xtype: 'actioncolumn',
					header: 'Seleccionar',
					tooltip: 'Modificar-Consultar',
					resizable: 	false,
					width: 95,
					hidden: 		false,
					hideable:	false,
					align: 'center',					
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Modificar';
									return 'icoModificar';
							},
							handler: function(grid, rowIndex, colIndex, item, event){
									grid2.hide();
									fpModImg.setVisible(true);
									var registro = grid2.getStore().getAt(rowIndex);
									var numImg = registro.get('NUMPUBIM');
									var nomImg = registro.get('NOMPUBIM');
									var feci = registro.get('FECINIIM');
									var fecf = registro.get('FECFINIM');
									var cont = registro.get('CONTENIDOALL');
									Ext.getCmp('_txt_pub_mod_img').setValue(numImg);
									Ext.getCmp('_txt_tit_mod_img').setValue(nomImg);
									Ext.getCmp('_fecha_carga_ini_mod_img').setValue(feci);
									Ext.getCmp('_fecha_carga_fin_mod_img').setValue(fecf);
									Ext.getCmp('_txt_con_mod_img').setValue(cont);
								}
						},{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
									this.items[1].tooltip = 'Eliminar';
									return 'icoEliminar';
							},
							handler: function(grid, rowIndex, colIndex, item, event){
									var registro = grid2.getStore().getAt(rowIndex);
									var numPub = registro.get('NUMPUBIM');
									var nomArch = registro.get('ARCHIVO');
									var nomImg = registro.get('IMAGEN');
									var cveEpo = Ext.getCmp('_cmb_cad').getValue();
									Ext.Msg.confirm('Confirmaci�n', '�Est� seguro que desea eliminar este texto con imag�n?', function(btn){
									//Se eliminara la imagen con texto
									if(btn=='yes'){
											var gr = Ext.getCmp("gridConsulta2");
											gr.el.mask('Procesando...','x-mask-loading');
											Ext.Ajax.request({
												url: '15admtabla01Ext.data.jsp',
												params:{
													informacion: 'EliminarImgTxt',
													cveEpo: cveEpo,
													numPub: numPub,
													nomArch: nomArch,
													nomImg: nomImg
												},
												callback: procesarSuccessFailureDeleteImg
											});
										}
									});
								}
						}
					]				
				}
			],bbar: {
			buttonAlign: 	'right',
			id: 			'barraPaginacion2',
			items: [
				 '->','-',
				{
					xtype:	'button',
					text:		'Regresar',
					width:	100,
					iconCls:	'icoRegresar',
					id: 		'btnRegresar2',
					handler: function(boton, evento) {
						grid2.hide();
						fp.show();
						Ext.getCmp('forma').getForm().reset();
						Ext.getCmp('_cmb_mant').setValue('2');
					}
				},'-'
			]
		}
	});
	
	var elementosFormaModificaTabla = [
			{
				xtype:		'textfield',
				fieldLabel: '',
				name:			'txt_num_mod_tab',
				id:			'_txt_num_mod_tab',
				hiddenName:	'txt_num_mod_tab',
				hidden: true,
				allowBlank:	true,
				maxLength:	50,
				anchor:	'95%'
			},{
				xtype:		'textfield',
				fieldLabel: 'Nombre',
				name:			'txt_nom_mod_tab',
				id:			'_txt_nom_mod_tab',
				hiddenName:	'txt_nom_mod_tab',
				allowBlank:	true,
				maxLength:	50,
				anchor:	'95%'
			},{
				xtype: 'compositefield',
				fieldLabel: 'Publicar',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'displayfield',
						value: 'Desde',
						width: 25,
						margins: '0 15 0 0'
					},
					{
						xtype: 'datefield',
						name: 'fecha_carga_ini_mod_tab',
						id: '_fecha_carga_ini_mod_tab',
						allowBlank: false,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						//vtype: 'rangofecha',
						//campoFinFecha: '_fecha_carga_fin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'Hasta',
						width: 25,
						margins: '0 15 0 0'
					},
					{
						xtype: 'datefield',
						name: 'fecha_carga_fin_mod_tab',
						id: '_fecha_carga_fin_mod_tab',
						allowBlank: false,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						//vtype: 'rangofecha',
						//campoInicioFecha: '_fecha_carga_ini',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			}
	];
	
	var fpModTab = new Ext.form.FormPanel({
			id:				'formaModTab',
			width:			550,
			style:			'margin:0 auto;',
			title: 'Modificaci�n de la tabla',
			frame:			true,
			hidden: true,
			bodyStyle:		'padding: 6px',
			labelWidth:		150,
			items:			elementosFormaModificaTabla,
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			monitorValid:	true
		});
	
	var elementosFormaModificarImg = [
			{
				xtype:		'textfield',
				fieldLabel: '',
				name:			'txt_pub_mod_img',
				id:			'_txt_pub_mod_img',
				hiddenName:	'txt_pub_mod_img',
				allowBlank:	true,
				maxLength:	50,
				hidden: true,
				anchor:	'95%'
			},{
				xtype:		'textfield',
				fieldLabel: 'Titulo',
				name:			'txt_tit_mod_img',
				id:			'_txt_tit_mod_img',
				hiddenName:	'txt_tit_mod_img',
				allowBlank:	true,
				maxLength:	50,
				hidden: false,
				anchor:	'95%'
			},{
				xtype: 'compositefield',
				fieldLabel: 'Publicar',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'displayfield',
						value: 'Desde',
						width: 25,
						margins: '0 15 0 0'
					},
					{
						xtype: 'datefield',
						name: 'fecha_carga_ini_mod_img',
						id: '_fecha_carga_ini_mod_img',
						allowBlank: false,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						//vtype: 'rangofecha',
						//campoFinFecha: '_fecha_carga_fin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'Hasta',
						width: 25,
						margins: '0 15 0 0'
					},
					{
						xtype: 'datefield',
						name: 'fecha_carga_fin_mod_img',
						id: '_fecha_carga_fin_mod_img',
						allowBlank: false,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						//vtype: 'rangofecha',
						//campoInicioFecha: '_fecha_carga_ini',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			},{
				xtype:		'textarea',
				fieldLabel: 'Contenido',
				name:			'txt_con_mod_img',
				id:			'_txt_con_mod_img',
				hiddenName:	'txt_con_mod_img',
				allowBlank:	false,
				maxLength:	100,
				hidden: false,
				anchor:	'95%'
			},{
				xtype: 'fileuploadfield',
				id: 'archivo_mod_img',  
				hidden: false,
				width: 343,
				//emptyText: 'Ruta del Archivo',
				fieldLabel: 'Archivo',
				buttonText: 'Examinar...',
				buttonCfg: {
				iconCls: 'upload-icon'
				},
				anchor: '95%'/*,
				vtype: 'archivotxt'*/
			},{
				xtype: 'fileuploadfield',
				id: 'imagen_mod_img',  
				hidden: false,
				width: 343,
				//emptyText: 'Ruta del Archivo',
				fieldLabel: 'Imagen',
				buttonText: 'Examinar...',
				buttonCfg: {
				iconCls: 'upload-icon'
				},
				anchor: '95%'/*,
				vtype: 'archivojpg'*/
			}	
	];
	
	var fpModImg = new Ext.form.FormPanel({
			id:				'formaModImg',
			width:			550,
			style:			'margin:0 auto;',
			title: 'Modificaci�n de Texto con Imag�n',
			frame:			true,
			hidden: false,
			bodyStyle:		'padding: 6px',
			fileUpload: true,
			labelWidth:		150,
			items:			elementosFormaModificarImg,
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			monitorValid:	true,
			buttons: [
				{
					text:		'Modificar',
					id:		'btnModificar',
					iconCls:	'icoModificar',
					handler:	function() {
									Ext.Msg.confirm('Confirmaci�n', '�Est� seguro que desea modificar este texto con im�gen?', function(btn){
										if(btn=='yes'){
											accionConsulta("MODIFICAR", null);
										}
									})
								}
				},{
					text:		'Cancelar',
					id:		'btnCancel',
					iconCls:	'icoCancelar',
					handler:	function() {
									fpModImg.hide();
									grid2.show();
									fpModImg.getForm().reset();
								}
				}
			]
		});
	
	var elementosFormaConsulta = [
			{
				xtype: 'combo',
				name: 'cmb_mant',
				id: '_cmb_mant',
				fieldLabel: 'Mantenimiento',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'cveMant',
				forceSelection : true,
				triggerAction : 'all',
				editable: true,
				typeAhead: true,
				minChars : 1,
				store: 		catalogoTipoPublicacionData,
				anchor:	'95%',
				listeners:{
					select: function(combo){
						Ext.getCmp('_cmb_multi').reset();
						Ext.getCmp('_cmb_cad').reset();
						//Ext.getCmp('_cmb_afi').reset();
						fp.getForm().findField('_cmb_afi').reset();
						var cmbMant = combo.getValue();
						if((cmbMant.toString())=="2"){
							Ext.getCmp('_txt_tit').show();
							Ext.getCmp('_txt_con').show();
							Ext.getCmp('_archivo').show();
							Ext.getCmp('_imagen').show();
							Ext.getCmp('_txt_nom').hide();
							Ext.getCmp('_txt_camf').hide();
							Ext.getCmp('_txt_came').hide();
							Ext.getCmp('_txt_camd').hide();
						}
						else{
							Ext.getCmp('_txt_tit').hide();
							Ext.getCmp('_txt_con').hide();
							Ext.getCmp('_archivo').hide();
							Ext.getCmp('_imagen').hide();
							Ext.getCmp('_txt_nom').show();
							Ext.getCmp('_txt_camf').show();
							Ext.getCmp('_txt_came').show();
							Ext.getCmp('_txt_camd').show();
						}
					}
				}
			},{
				xtype:		'textfield',
				fieldLabel: 'Nombre',
				name:			'txt_nom',
				id:			'_txt_nom',
				hiddenName:	'txt_nom',
				allowBlank:	true,
				maxLength:	50,
				anchor:	'95%'
			},{
				xtype:		'textfield',
				fieldLabel: 'Titulo',
				name:			'txt_tit',
				id:			'_txt_tit',
				hiddenName:	'txt_tit',
				allowBlank:	true,
				maxLength:	50,
				hidden: true,
				anchor:	'95%'
			},{
				xtype: 'compositefield',
				fieldLabel: 'Publicar',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'displayfield',
						value: 'Desde',
						width: 25,
						margins: '0 15 0 0'
					},
					{
						xtype: 'datefield',
						name: 'fecha_carga_ini',
						id: '_fecha_carga_ini',
						hiddenName:	'fecha_carga_ini',
						allowBlank: false,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoFinFecha: '_fecha_carga_fin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'Hasta',
						width: 25,
						margins: '0 15 0 0'
					},
					{
						xtype: 'datefield',
						name: 'fecha_carga_fin',
						id: '_fecha_carga_fin',
						allowBlank: false,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoInicioFecha: '_fecha_carga_ini',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			},{
				xtype:		'textfield',
				fieldLabel: 'N�mero de campos Fijos',
				name:			'txt_camf',
				id:			'_txt_camf',
				hiddenName:		'txt_camf',
				emptyText: 'M�ximo 50',
				maskRe:		/[0-9]/,
				allowBlank:	true,
				anchor:	'50%',
				maxLength:	2
			},{
				xtype:		'textfield',
				fieldLabel: 'N�mero de campos editables',
				name:			'txt_came',
				id:			'_txt_came',
				hiddenName:		'txt_came',
				emptyText: 'M�ximo 5',
				maskRe:		/[0-5]/,
				allowBlank:	true,
				anchor:	'50%',
				maxLength:	1
			},{
				xtype:		'textfield',
				fieldLabel: 'N�mero de campos detalle',
				name:			'txt_camd',
				id:			'_txt_camd',
				emptyText: 'M�ximo 10',
				hiddenName:		'txt_camd',
				maskRe:		/[0-9]/,
				allowBlank:	true,
				anchor:	'50%',
				maxLength:	2
			},NE.util.getEspaciador(20),{
				xtype: 'combo',
				name: 'cmb_cad',
				id: '_cmb_cad',
				fieldLabel: 'Cadenas',
				mode: 'local',
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'cveCad',
				forceSelection : true,
				triggerAction : 'all',
				editable: true,
				typeAhead: true,
				minChars : 1,
				store: 		catalogoCadenasData,
				anchor:	'95%'
			},{
				xtype: 'compositefield',
				fieldLabel: 'Tipo de afiliado',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'combo',
						name: 'cmb_afi',
						id: '_cmb_afi',
						mode: 'local',
						displayField : 'descripcion',
						valueField : 'clave',
						hiddenName : 'cveAfi',
						emptyText: 'Seleccione...',
						forceSelection : true,
						triggerAction : 'all',
						editable: true,
						store: storeTipoAfiliado,
						typeAhead: true,
						margins: '0 20 0 0',
						minChars : 1
					},{
						xtype: 'displayfield',
						value: ' ',
						width: 25
					},{
						xtype: 'button',
						text:		'Buscar',
						id:		'btnBuscar',
						iconCls:	'icoBuscar',
						handler:	function(){
							var cad = Ext.getCmp('_cmb_cad').getValue();
							var tAfi = Ext.getCmp('_cmb_afi').getValue();
							if(cad =="" || tAfi ==""){
								if(cad == ""){
									Ext.getCmp('_cmb_cad').markInvalid('Tiene que hacer una selecci�n...!!!');
								}else if(tAfi == ""){
									Ext.getCmp('_cmb_afi').markInvalid('Tiene que hacer una selecci�n...!!!');
								}
							}else{
								Ext.StoreMgr.key('catalogoMultipleDataStore').load({
								params:
									{
										informacion: 'catalogoMulti',
										cveCad: cad,
										tipoAfi: tAfi
									}
								});
							}
						}
					}
				]
			},{
				xtype: 'multiselect',
				id: '_cmb_multi',
				hiddenName: 'cmb_multi',
				width: '95%',
				height: 100, 
				autoLoad: false,
				allowBlank:false,
				store: catalogoMultipleData,
				displayField: 'descripcion',
				valueField: 'clave'
			},{
				xtype:		'textarea',
				fieldLabel: 'Contenido',
				name:			'txt_con',
				id:			'_txt_con',
				hiddenName:	'txt_con',
				allowBlank:	false,
				maxLength:	900,
				hidden: true,
				anchor:	'95%'
			},{
				xtype: 'fileuploadfield',
				id: '_archivo',  
				hidden: true,
				width: 343,
				//emptyText: 'Ruta del Archivo',
				fieldLabel: 'Archivo',
				buttonText: 'Examinar...',
				buttonCfg: {
				iconCls: 'upload-icon'
				},
				anchor: '95%'/*,
				vtype: 'archivotxt'*/
			},{
				xtype: 'fileuploadfield',
				id: '_imagen',  
				hidden: true,
				width: 343,
				//emptyText: 'Ruta del Archivo',
				fieldLabel: 'Imagen',
				buttonText: 'Examinar...',
				buttonCfg: {
				iconCls: 'upload-icon'
				},
				anchor: '95%'/*,
				vtype: 'archivojpg'*/
			}	
	];
	
	var fp = new Ext.form.FormPanel({
			id:				'forma',
			width:			550,
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			fileUpload: true,
			labelWidth:		150,
			items:			elementosFormaConsulta,
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			monitorValid:	true,
			buttons: [
				{
					text:		'Agregar',
					iconCls:	'icoContinuar',
					handler:	function() {
									accionConsulta("AGREGAR",null);
								}
				},{
					text:		'Deshacer',
					id:		'btnCancelar',
					iconCls:	'icoDeshacer',
					handler:	function() {
									accionConsulta("LIMPIAR",null);
								}
				},{
					text:		'Consultar',
					id:		'btnConsultar',
					iconCls:	'icoBuscar',
					handler:	function() {
									if(Ext.getCmp('_cmb_cad').getValue()=="" || Ext.getCmp('_cmb_cad').getValue()=="T")
										Ext.getCmp('_cmb_cad').markInvalid('Debe seleccionar una cadena');
									else
										accionConsulta("CONSULTAR",null);
								}
				}
			]
		});
		
		var elementosUpTabFija = [
			{
				xtype: 'fileuploadfield',
				id: '_dat_tab_fija',  
				width: 343,
				fieldLabel: 'Archivo a subir',
				buttonText: 'Examinar...',
				buttonCfg: {
				iconCls: 'upload-icon'
				},
				anchor: '95%'
			}
		];
		
		var fpTabFija = new Ext.form.FormPanel({
			id:				'fpTabFija',
			width:			550,
			title: 'Importando datos de tabla Fija',
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			//hidden: true,
			fileUpload: true,
			labelWidth:		150,
			items:			elementosUpTabFija,
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			monitorValid:	true,
			buttons: [
				{
					text:		'Aceptar',
					id:		'btnAcepTabFija',
					iconCls:	'icoAceptar',
					handler:	function() {
								//	accionConsulta("LIMPIAR",null);
									Ext.MessageBox.alert("Aviso","El sistema esta experimentando dificultades tecnicas. Intente mas tarde");
								}
				},{
					text:		'Cancelar',
					id:		'btnCanTabFija',
					iconCls:	'icoCancelar',
					handler:	function() {
									fpTabFija.hide();
									gridDinEdit.show();
									var formaDatos = Ext.getCmp('formaDatos');		
									if(!formaDatos.isVisible())
										formaDatos.show();
									formaDatos.insert(0,gridDinEdit);
									formaDatos.doLayout(false,true);
								}
				}
			]
		});
		
		var elementosUpTabDetalle = [
			{
				xtype: 'fileuploadfield',
				id: '_dat_tab_detalle',  
				width: 343,
				fieldLabel: 'Archivo a subir',
				buttonText: 'Examinar...',
				buttonCfg: {
				iconCls: 'upload-icon'
				},
				anchor: '95%'
			}
		];
		
		var fpTabDetalle = new Ext.form.FormPanel({
			id:				'fpTabDetalle',
			width:			550,
			title: 'Importando datos de tabla de Detalle',
			style:			'margin:0 auto;',
			frame:			true,
			bodyStyle:		'padding: 6px',
			//hidden: true,
			fileUpload: true,
			labelWidth:		150,
			items:			elementosUpTabDetalle,
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			monitorValid:	true,
			buttons: [
				{
					text:		'Aceptar',
					id:		'btnAcepTabDetalle',
					iconCls:	'icoAceptar',
					handler:	function() {
										Ext.MessageBox.alert("Aviso","El sistema esta experimentando dificultades tecnicas. Intente mas tarde");
								}
				},{
					text:		'Cancelar',
					id:		'btnCanTabDetalle',
					iconCls:	'icoCancelar',
					handler:	function() {
									fpTabDetalle.hide();
									gridDinEdit.show();
									var formaDatos = Ext.getCmp('formaDatos');		
									if(!formaDatos.isVisible())
										formaDatos.show();
									formaDatos.insert(0,gridDinEdit);
									formaDatos.doLayout(false,true);
								}
				}
			]
		});
		
	//Ventana emergente para recabar nombre de copia de tabla
	var winCopiar = new Ext.Window ({
		id:'winCopiar',
		height: 139,
		x: 300,
		y: 100,
		width: 580,
		modal: true,
		closeAction: 'hide',
		items:[
			{
				xtype:'form',
				frame: true,
				id:'fpCopiaTabla',
				style: 'margin: 0 auto',
				bodyStyle:'padding:10px',
				monitorValid: true,
				defaults: {
					msgTarget: 'side',
					anchor: '-20'
				},
				labelWidth: 80,
				items:[
					{
						xtype:		'textfield',
						fieldLabel: '',
						id:			'_txt_num_pub_copy',
						allowBlank:	true,
						maxLength:	50,
						hidden: true,
						anchor:	'95%'
					},{
						xtype:		'textfield',
						fieldLabel: 'Nombre de la nueva tabla',
						name:			'txt_nom_tab_copy',
						id:			'_txt_nom_tab_copy',
						hiddenName:	'txt_nom_tab_copy',
						allowBlank:	true,
						maxLength:	50,
						hidden: false,
						anchor:	'95%'
					}
				],
				buttonAlign:'center',
				buttons:[
					{
						text:'Aceptar',
						iconCls:'aceptar',
						formBind:true,
						handler: function(){
							if(Ext.getCmp('_txt_nom_tab_copy').getValue()=="")
								Ext.getCmp('winCopiar').hide();
							else{
								accionConsulta("COPIAR",null);
							}	
						}
					},{
						text:'Cancelar',
						iconCls: 'icoRechazar',
						handler: function() {	
							Ext.getCmp('_txt_nom_tab_copy').reset();
							Ext.getCmp('winCopiar').hide();
						}
					}
				]
			}
		]
	});
	
	var formaDatos = {
		id: 'formaDatos',
		hidden: true,
		xtype: 'panel',
		height: 'auto',
		width: 949,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		frame: true,
		monitorValid: true
	};
	
	
//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	930,
		align: 'center',
		items: 	[
			NE.util.getEspaciador(10),
			formaDatos,
			fpModTab,
			NE.util.getEspaciador(10),
			gridFijosMod,
			NE.util.getEspaciador(10),
			gridEditablesMod,
			NE.util.getEspaciador(10),
			gridDetalleMod,
			NE.util.getEspaciador(10),
			fpBotonesModT,
			fp,
			NE.util.getEspaciador(5),
			fpTabFija,
			fpTabDetalle,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10),
			grid2,
			NE.util.getEspaciador(10),
			fpModImg,
			gridFijos,
			NE.util.getEspaciador(10),
			gridEditables,
			NE.util.getEspaciador(10),
			gridDetalle,
			NE.util.getEspaciador(12),
			fpBotones
		]
	});
	
	fpModImg.hide(); //truco para que se cargue correctamente el form
	fpTabFija.hide();
	fpTabDetalle.hide();
	Ext.StoreMgr.key('catalogoTipoPublicacionDataStore').load();
	Ext.StoreMgr.key('catalogoCadenasDataStore').load();
	
});