<%@ page contentType="application/json;charset=UTF-8" 
	import="
		java.util.*,
		java.text.*,	
		netropology.utilerias.*,	
		com.netro.cadenas.*,
		com.netro.dispersion.*, 
		com.netro.model.catalogos.*,	
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		javax.naming.Context,
		org.apache.commons.logging.Log"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");

String ic_epo	= (request.getParameter("ic_epo")		== null)?"":request.getParameter("ic_epo");
String ic_if	= (request.getParameter("ic_if")		== null)?"":request.getParameter("ic_if");
String ic_convenio	= (request.getParameter("ic_convenio")		== null)?"":request.getParameter("ic_convenio");
String fecha_public_oper_ini	= (request.getParameter("fecha_public_oper_ini")		== null)?"":request.getParameter("fecha_public_oper_ini");
String fecha_public_oper_fin	= (request.getParameter("fecha_public_oper_fin")		== null)?"":request.getParameter("fecha_public_oper_fin");
String tipo	= (request.getParameter("tipo")		== null)?"":request.getParameter("tipo");
String habilitado	= (request.getParameter("habilitado")		== null)?"":request.getParameter("habilitado");



HashMap  datos  = new HashMap();
JSONArray  registros = new JSONArray();
JSONObject jsonObj = new JSONObject();
List parametros_reporte = new ArrayList(); 
List parametros_detalle = new ArrayList();   


String infoRegresar ="", consulta ="";

InteligenciaComercial inteligenciaComercial = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);
    

RepOpPymesDescAutomatico pag =  new RepOpPymesDescAutomatico ();



String cadena_datos_entrada = "1" + ic_epo + ic_if + ic_convenio + fecha_public_oper_ini + fecha_public_oper_fin;
String hashDatosEntrada = Integer.toString(cadena_datos_entrada.hashCode());
     
parametros_reporte.add(hashDatosEntrada);
parametros_reporte.add("1");
parametros_reporte.add(ic_epo);
parametros_reporte.add(ic_if);
parametros_reporte.add(ic_convenio);
parametros_reporte.add(fecha_public_oper_ini);
parametros_reporte.add(fecha_public_oper_fin);

if ( informacion.equals("catalogoEpo") )	{

	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setClave("ic_epo");
	catalogo.setDescripcion("cg_razon_social");
	catalogo.setOrden("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();		
  

}else if ( informacion.equals("catalogoIF") )	{
  
	CatalogoIF catalogo = new CatalogoIF();
	catalogo.setClave("ic_if");
	catalogo.setDescripcion("cg_razon_social");
	catalogo.setOrden("cg_razon_social");
	catalogo.setIc_epo(ic_epo);
	infoRegresar = catalogo.getJSONElementos();		

}else if ( informacion.equals("catConvenio") )	{
		
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_version_convenio");
	catalogo.setCampoDescripcion("cd_version_convenio");
	catalogo.setTabla("comcat_version_convenio");
   catalogo.setOrden("ic_version_convenio");
	infoRegresar = catalogo.getJSONElementos();	

}else if ( informacion.equals("Consultar")  || informacion.equals("Generar_Arch")  )	{

	List reg = inteligenciaComercial.reporteOperacionesDescuentoAutomatico(parametros_reporte);
	
	if ( informacion.equals("Consultar") ){
		if(reg.size()>0)  {
	
			for(int i= 0; i < reg.size(); i++){
				List oper = (List)reg.get(i);
				
				datos  = new HashMap();
				datos.put("NOMBRE_EPO",oper.get(2));
				datos.put("NOMBRE_IF",oper.get(3));
				datos.put("CONVENIO",oper.get(4));					 
				datos.put("TOTAL_PYMES",oper.get(5));
						  
				datos.put("NO_PYMES_DESCACT_HAB",oper.get(6));
				datos.put("NO_DOCTOS_PUBLIC_HAB",oper.get(7));
				datos.put("MONTO_PUBLIC_HAB",oper.get(8));
				datos.put("NO_DOCTOS_OPERADOS_HAB",oper.get(9));
				datos.put("MONTO_OPERADOS_HAB",oper.get(10));
						 
				// operacionesDescuentoAutomatico.get(0) ---operacionesDescuentoAutomatico.get(1)
						
				datos.put("NO_PYMES_DESCACT_NOHAB",oper.get(11));
				datos.put("NO_DOCTOS_PUBLIC_NOHAB",oper.get(12));
				datos.put("MONTO_PUBLIC_NOHAB",oper.get(13));
				datos.put("NO_DOCTOS_OPERADOS_NOHAB",oper.get(14));
				datos.put("MONTO_OPERADOS_NOHAB",oper.get(15));
				//operacionesDescuentoAutomatico.get(0)    operacionesDescuentoAutomatico.get(1)             
				registros.add(datos);
			}
		}
					
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		infoRegresar = jsonObj.toString(); 

	}else  if(informacion.equals("Generar_Arch") )	{
		
		String nombreArchivo = 	pag.getArchivosConst(request, strDirectorioTemp, tipo,  reg, informacion  );
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		infoRegresar = jsonObj.toString(); 
	}	
	
}else if ( informacion.equals("ConDetalle_pyme") ||  informacion.equals("Generar_Arch_Det") )	{
	
	parametros_reporte.add(habilitado);      
	List detalle = inteligenciaComercial.obtenerPymesDescuentoAutomatico(parametros_reporte);
	
	if ( informacion.equals("ConDetalle_pyme")) {
		if(detalle.size()>0) {
			for(int i= 0; i < detalle.size(); i++){
				List reg = (List)detalle.get(i);
				datos  = new HashMap();
				datos.put("NO_ELECTRONICO",reg.get(0) );
				datos.put("NOMBRE_PYME", reg.get(1) );
				datos.put("RFC",reg.get(2) );
				registros.add(datos);
			}
		}
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		infoRegresar = jsonObj.toString(); 
	
	}else  if(informacion.equals("Generar_Arch_Det") )	{
	
		String nombreArchivo = 	pag.getArchivosConst(request, strDirectorioTemp, tipo,  detalle, informacion  );
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		infoRegresar = jsonObj.toString(); 
	
	}
 
}
//log.debug("infoRegresar  "+infoRegresar  ); 
 
 
%>
<%=infoRegresar%> 
