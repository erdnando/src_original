Ext.onReady(function(){

	var opcionReporte  = 'N';
	var mes_detalle    = '';
	var anio_detalle   = '';
	var titulo_detalle = '';

//------------------------------ Handlers ----------------------------
	function limpiar(){
		main.el.mask('Procesando...', 'x-mask-loading');
		window.location  = "15forma03ext.jsp?idMenu=15OPNAMOV"
	}

	function generar(){

		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var campo_ini = 0;
		var campo_fin = 0;

		// Valido la forma principal
		if(!this.up('form').getForm().isValid()){
			return;
		}

		// Comparo las fechas
		campo_ini = parseInt(forma.query('#anio_ini_id')[0].getValue());
		campo_fin = parseInt(forma.query('#anio_fin_id')[0].getValue());
		if(campo_ini > campo_fin){
			Ext.MessageBox.alert('Mensaje', 'Rango de fechas incorrecto, por favor, intente nuevamente.');
			return;
		}
		campo_ini = parseInt(forma.query('#mes_ini_id')[0].getValue());
		campo_fin = parseInt(forma.query('#mes_fin_id')[0].getValue());
		if(campo_ini > campo_fin){
			Ext.MessageBox.alert('Mensaje', 'Rango de fechas incorrecto, por favor, intente nuevamente.');
			return;
		}	

		// Realiza la consulta del grid
		main.el.mask('Procesando...', 'x-mask-loading');
		Ext.ComponentQuery.query('#gridData')[0].hide();
		Ext.ComponentQuery.query('#gridData1')[0].hide();
		if(opcionReporte == 'N' || opcionReporte == 'V'){
			consultaData.load({
				params: Ext.apply({
					mes_ini:    forma.query('#mes_ini_id')[0].getValue(),
					mes_fin:    forma.query('#mes_fin_id')[0].getValue(),
					anio_ini:   forma.query('#anio_ini_id')[0].getValue(),
					anio_fin:   forma.query('#anio_fin_id')[0].getValue(),
					tipo_docto: opcionReporte
				})
			});
		} else{
			consultaData1.load({
				params: Ext.apply({
					mes_ini:    forma.query('#mes_ini_id')[0].getValue(),
					mes_fin:    forma.query('#mes_fin_id')[0].getValue(),
					anio_ini:   forma.query('#anio_ini_id')[0].getValue(),
					anio_fin:   forma.query('#anio_fin_id')[0].getValue(),
					tipo_docto: opcionReporte
				})
			});
		}


	}

	//Genera el archivo
	function generaReporte(){

		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var grid= Ext.ComponentQuery.query('#gridData')[0];
		var boton = grid.query('#btnReporteXLS')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');

		grid= Ext.ComponentQuery.query('#gridData1')[0];
		var boton = grid.query('#btnReporteXLS1')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');

		Ext.Ajax.request({
			url: '15forma03ext.data.jsp',
			params: Ext.apply({
				informacion:      'GENERA_REPORTE',
				mes_ini:    forma.query('#mes_ini_id')[0].getValue(),
				mes_fin:    forma.query('#mes_fin_id')[0].getValue(),
				anio_ini:   forma.query('#anio_ini_id')[0].getValue(),
				anio_fin:   forma.query('#anio_fin_id')[0].getValue(),
				tipo_docto: opcionReporte
			}),
			callback: procesaGeneraArchivo
		});

	}

	//Genera el archivo Detalle
	function generaReporteDetalle(){

		var grid= Ext.ComponentQuery.query('#gridDetalle')[0];
		var boton = grid.query('#btnReporteDetalleXLS')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');

		grid= Ext.ComponentQuery.query('#gridDetalleTotal')[0];
		var store = grid.getStore();
		var datos = '';
		var datar = new Array();

		var records = store.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		datos = Ext.encode(datar);

		Ext.Ajax.request({
			url: '15forma03ext.data.jsp',
			params: Ext.apply({
				informacion:  'VER_DETALLE_ARCHIVO',
				mes_ini:      mes_detalle,
				anio_ini:     anio_detalle,
				tipo_docto:   opcionReporte,
				datos_grid_1: datos
			}),
			callback: procesaGeneraArchivo
		});

	}

	//Genera el archivo Detalle
	function generaReporteDetalle1(){

		var grid= Ext.ComponentQuery.query('#gridDetalle1')[0];
		var boton = grid.query('#btnReporteDetalleXLS_1')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');

		grid = Ext.ComponentQuery.query('#gridDetalleTotal1')[0];
		var store1 = grid.getStore();
		var datos1 = '';
		var datos2 = '';
		var datar = new Array();

		var records = store1.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		datos1 = Ext.encode(datar);

		grid = Ext.ComponentQuery.query('#gridDetalleTotal2')[0];
		var store2 = grid.getStore();
		datar = new Array();
		records = store2.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		datos2 = Ext.encode(datar);

		Ext.Ajax.request({
			url: '15forma03ext.data.jsp',
			params: Ext.apply({
				informacion:  'VER_DETALLE_ARCHIVO_1',
				mes_ini:      mes_detalle,
				anio_ini:     anio_detalle,
				tipo_docto:   opcionReporte,
				datos_grid_1: datos1,
				datos_grid_2: datos2
			}),
			callback: procesaGeneraArchivo
		});

	}

	// Se ocultan los grids de ver detalles, y se muestran los componentes principales
	function regresarDetalle(){

		Ext.ComponentQuery.query('#gridDetalle')[0].hide();
		Ext.ComponentQuery.query('#gridDetalleTotal')[0].hide();
		Ext.ComponentQuery.query('#gridDetalle1')[0].hide();
		Ext.ComponentQuery.query('#gridDetalleTotal1')[0].hide();
		Ext.ComponentQuery.query('#gridDetalleTotal2')[0].hide();
		Ext.ComponentQuery.query('#formaPrincipal')[0].show();
		if(opcionReporte == 'N' || opcionReporte == 'V'){
			Ext.ComponentQuery.query('#gridData')[0].show();
		} else{
			Ext.ComponentQuery.query('#gridData1')[0].show();
		}

	}

//------------------------------ Callback ----------------------------
	// Proceso posterior a la consulta del grid consultaData
	var procesarConsultaData = function(store, arrRegistros, success, opts){

		main.el.unmask();
		var grid = Ext.ComponentQuery.query('#gridData')[0];
		if (arrRegistros != null){
			grid.show();
			if(store.getTotalCount() > 0){
				grid.query('#btnReporteXLS')[0].enable();
			} else{
				grid.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				grid.query('#btnReporteXLS')[0].disable();
			}
		}

	}

	// Proceso posterior a la consulta del grid consultaData
	var procesarConsultaData1 = function(store, arrRegistros, success, opts){

		main.el.unmask();
		var grid = Ext.ComponentQuery.query('#gridData1')[0];
		if (arrRegistros != null){
			grid.show();
			if(store.getTotalCount() > 0){
				//grid.query('#btnReporteXLS')[0].enable();
			} else{
				grid.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				//grid.query('#btnReporteXLS')[0].disable();
			}
		}

	}

	// Proceso posterior a la consulta del grid Detalle
	var procesarDetalle = function(store, arrRegistros, success, opts){

		//var grid = Ext.ComponentQuery.query('#gridDetalle')[0];
		if (arrRegistros != null){

			// Oculto el resto de los componentes
			Ext.ComponentQuery.query('#formaPrincipal')[0].hide();
			Ext.ComponentQuery.query('#gridData')[0].hide();
			Ext.ComponentQuery.query('#gridData1')[0].hide();
			Ext.ComponentQuery.query('#gridDetalle')[0].show();

			// Asigno t�tilo al grid
			Ext.ComponentQuery.query('#gridDetalle')[0].setTitle(titulo_detalle);

			if(store.getTotalCount() > 0){

				// Habilito el bot�n del reporte
				Ext.ComponentQuery.query('#gridDetalle')[0].query('#btnReporteDetalleXLS')[0].enable();
				// Realizo la consulta de los totales
				consultaDetalleTotal.loadPage(1, {
					params: Ext.apply({
						mes_ini:    mes_detalle,
						anio_ini:   anio_detalle,
						tipo_docto: opcionReporte
					})
				});
			} else{
				main.el.unmask();
				Ext.ComponentQuery.query('#gridDetalle')[0].query('#btnReporteDetalleXLS')[0].disable();
				Ext.ComponentQuery.query('#gridDetalle')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
			}
		}

	}

	// Proceso posterior a la consulta del grid Detalle Total
	var procesarDetalleTotal = function(store, arrRegistros, success, opts){
		main.el.unmask();
		var grid = Ext.ComponentQuery.query('#gridDetalleTotal')[0];
		if (arrRegistros != null){
			grid.show();
			if(store.getTotalCount() > 0){
			} else{
				grid.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
			}
		}
	}

	// Proceso posterior a la consulta del grid Detalle 1
	var procesarDetalle1 = function(store, arrRegistros, success, opts){

		if (arrRegistros != null){

			// Oculto el resto de los componentes
			Ext.ComponentQuery.query('#formaPrincipal')[0].hide();
			Ext.ComponentQuery.query('#gridData')[0].hide();
			Ext.ComponentQuery.query('#gridData1')[0].hide();
			Ext.ComponentQuery.query('#gridDetalle1')[0].show();

			// Asigno t�tilo al grid
			Ext.ComponentQuery.query('#gridDetalle1')[0].setTitle(titulo_detalle);

			if(store.getTotalCount() > 0){

				// Habilito el bot�n del reporte
				Ext.ComponentQuery.query('#gridDetalle1')[0].query('#btnReporteDetalleXLS_1')[0].enable();
				main.el.unmask();
				// Realizo la consulta de los totales
				consultaDetalleTotal1.loadPage(1, {
					params: Ext.apply({
						mes_ini:    mes_detalle,
						anio_ini:   anio_detalle,
						tipo_docto: opcionReporte
					})
				});
			} else{
				main.el.unmask();
				Ext.ComponentQuery.query('#gridDetalle1')[0].query('#btnReporteDetalleXLS_1')[0].disable();
				Ext.ComponentQuery.query('#gridDetalle1')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
			}
		}

	}

	// Proceso posterior a la consulta del grid Detalle Total 1
	var procesarDetalleTotal1 = function(store, arrRegistros, success, opts){
		var grid = Ext.ComponentQuery.query('#gridDetalleTotal1')[0];
		if (arrRegistros != null){
			if(store.getTotalCount() > 0){
				grid.show();
				// Realizo la consulta de los totales
				consultaDetalleTotal2.loadPage(1, {
					params: Ext.apply({
						mes_ini:    mes_detalle,
						anio_ini:   anio_detalle,
						tipo_docto: opcionReporte
					})
				});
			} else{
				main.el.unmask();
				grid.hide();
				grid.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
			}
		}
	}

	// Proceso posterior a la consulta del grid Detalle Total 2
	var procesarDetalleTotal2 = function(store, arrRegistros, success, opts){
		var grid = Ext.ComponentQuery.query('#gridDetalleTotal2')[0];
		if (arrRegistros != null){
			if(store.getTotalCount() > 0){
				grid.show();
			} else{
				grid.hide();
				grid.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
			}
		}
		main.el.unmask();
	}

	// Handler que procesa respuesta creacion archivo
	function procesaGeneraArchivo(opts, success, response){

		// PARSEAR RESPUESTA DEL SERVIDOR
		var resp = null;
		try {
			resp = Ext.JSON.decode(response.responseText);
		} catch(error) {
			NE.util.mostrarErrorResponse4(
				{ status: -1 }, // Para que no asuma que es un est�tus devuelto por el servidor
				error.name + " - " + error.message
			);
			return;
		}

		// REALIZAR LA DESCARGA AUTOM�TICA DEL ARCHIVO
		if ( success == true && resp.success == true ) {

			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo';
			forma.method = 'post';
			forma.target = '_self';

			// Suprimir el Context root del url del archivo
			var urlArchivo = resp.urlArchivo;
			nombreArchivo  = urlArchivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');

			// Insertar Nombre Archivo
			var inputNombreArchivo = Ext.DomHelper.insertFirst(
				forma,
				{
					tag:   'input',
					type:  'hidden',
					id:    'nombreArchivo',
					name:  'nombreArchivo',
					value: nombreArchivo
				},
				true
			);
			// Solicitar Archivo Al Servidor
			forma.submit();
			// Remover nodo agregado
			inputNombreArchivo.remove();

		} else {
				NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
		var grid = Ext.ComponentQuery.query('#gridData')[0];
		var boton = grid.query('#btnReporteXLS')[0];
		boton.enable();
		boton.setIconCls('icoXls');

		grid = Ext.ComponentQuery.query('#gridData1')[0];
		boton = grid.query('#btnReporteXLS1')[0];
		boton.enable();
		boton.setIconCls('icoXls');

		grid = Ext.ComponentQuery.query('#gridDetalle')[0];
		boton = grid.query('#btnReporteDetalleXLS')[0];
		boton.enable();
		boton.setIconCls('icoXls');

		grid = Ext.ComponentQuery.query('#gridDetalle1')[0];
		boton = grid.query('#btnReporteDetalleXLS_1')[0];
		boton.enable();
		boton.setIconCls('icoXls');
	}

	// Lleno el catalogo con el valor por default
	var procesarCatalogoMesIni = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			Ext.ComponentQuery.query('#formaPrincipal')[0].query('#mes_ini_id')[0].setValue('0');
		}
	}

	// Lleno el catalogo con el valor por default
	var procesarCatalogoMesFin = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			Ext.ComponentQuery.query('#formaPrincipal')[0].query('#mes_fin_id')[0].setValue('0');
		}
	}

	// Lleno el catalogo con el valor por default
	var procesarCatalogoAnioIni = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma       = Ext.ComponentQuery.query('#formaPrincipal')[0];
			var campo       = forma.query('#anio_ini_id')[0];
			var store_combo = campo.getStore();
			var valor       = store_combo.getRange()[0].data.clave;
			campo.setValue(valor);
		}
	}

	// Lleno el catalogo con el valor por default
	var procesarCatalogoAnioFin = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma       = Ext.ComponentQuery.query('#formaPrincipal')[0];
			var campo       = forma.query('#anio_fin_id')[0];
			var store_combo = campo.getStore();
			var valor       = store_combo.getRange()[0].data.clave;
			campo.setValue(valor);
		}
	}
//------------------------------ Stores ------------------------------
	// Se crea el MODEL para los combos
	Ext.define('ModelCatologo',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);

	// Se crea el MODEL para el grid 'consulta negociable'
	Ext.define('ListaData',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'ANIO_NOTIFICACION'  },
			{name: 'MES_NOTIFICACION'   },
			{name: 'PYMES'              },
			{name: 'DOCTOS_NOTIFICADOS' },
			{name: 'MONTO_NOTIFICADO'   },
			{name: 'NUM_MES'            }
		]
	});

	// Se crea el MODEL para el grid 'consulta negociable'
	Ext.define('ListaData1',{
		extend: 'Ext.data.Model',
		fields: [
			{name: '0' },  {name: '1' },  {name: '2' },  {name: '3' },  {name: '4' },
			{name: '5' },  {name: '6' },  {name: '7' },  {name: '8' },  {name: '9' },
			{name: '10'},  {name: '11' }, {name: '12'},  {name: '13'},  {name: '14'},  {name: '15'}
		]
	});

	// Se crea el MODEL para el grid 'Detalles'
	Ext.define('ListaDetalle',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'NAFIN_ELECTRONICO' },
			{name: 'IC_PYME'           },
			{name: 'RFC'               },
			{name: 'NOMBRE_PYME'       },
			{name: 'NUM_CEL'           },
			{name: 'DOCTOS_NOTIFICADOS'},
			{name: 'MONTO_NOTIFICADO'  },
			{name: 'DOCTOS_OPERADOS'   },
			{name: 'MONTO_OPERADO'     },
			{name: 'DOCTOS_VENCIDOS'   },
			{name: 'MONTO_VENCIDO'     }
		]
	});

	// Se crea el MODEL para el grid 'Total Detalles'
	Ext.define('ListaDetalleTotales',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'NO_PYMES'           },
			{name: 'DOCTOS_NOTIFICADOS' },
			{name: 'MONTO_NOTIFICADO'   },
			{name: 'DOCTOS_OPERADOS'    },
			{name: 'MONTO_OPERADO'      },
			{name: 'DOCTOS_VENCIDOS'    },
			{name: 'MONTO_VENCIDO'      }
		]
	});

	// Se crea el MODEL para el grid 'Detalles1'
	Ext.define('ListaDetalle1',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'NO_NAFIN'       },
			{name: 'PYME'           },
			{name: 'RFC'            },
			{name: 'RAZON_SOCIAL'   },
			{name: 'CELULAR'        },
			{name: 'ESTADO'         },
			{name: 'DOCTOS_OPER'    },
			{name: 'MONTO_OPER'     },
			{name: 'NUM_NOT_OPER'   },
			{name: 'COSTO_NOT_OPER' },
			{name: 'INTERESES'      },
			{name: 'DOCTOS_NEG'     },
			{name: 'MONTO_NEG'      },
			{name: 'NUM_NOT_NEG'    },
			{name: 'COSTO_NOT_NEG'  },
			{name: 'NUM_DOCTOS_VEN' },
			{name: 'MONTO_VEN'      },
			{name: 'NUM_NOT_VEN'    },
			{name: 'COSTO_NOT_VEN'  },
			{name: 'TIPO_EPO'       }
		]
	});

	// Se crea el MODEL para el grid 'Total Detalles1'
	Ext.define('ListaDetalleTotales1',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'DOCTOS_NOTIFICADOS'   },
			{name: 'MONTO_NOTIFICADO'     },
			{name: 'NUM_NOTIFICACIONES'   },
			{name: 'COSTO_NOTIFICACIONES' },
			{name: 'INTERESES_OPERADOS'   }
		]
	});

	// Se crea el MODEL para el grid 'Total Detalles2'
	Ext.define('ListaDetalleTotales2',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'DESCRIPCION' },
			{name: 'DOCTOS_NE'   },
			{name: 'MONTO_NE'    }
		]
	});

	// Se crea el STORE para el cat�logo 'Mes Inicio'
	var catalogoMesIni = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15forma03ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_MES'
			}
		},
		autoLoad:          true,
		listeners: {
			load:           procesarCatalogoMesIni,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'Mes Fin'
	var catalogoMesFin = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15forma03ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_MES'
			}
		},
		autoLoad:          true,
		listeners: {
			load:           procesarCatalogoMesFin,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'A�o Inicio'
	var catalogoAnioIni = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15forma03ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_ANIO'
			}
		},
		autoLoad:          true,
		listeners: {
			load:           procesarCatalogoAnioIni,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'A�o Fin'
	var catalogoAnioFin = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15forma03ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_ANIO'
			}
		},
		autoLoad:          true,
		listeners: {
			load:           procesarCatalogoAnioFin,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE del grid 'Consulta Data'
	var consultaData = Ext.create('Ext.data.Store',{
		storeId:           'consultaData',
		model:             'ListaData',
		proxy: {
			type:           'ajax',
			url:            '15forma03ext.data.jsp',
			reader:  {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CONSULTA_NEGOCIABLE'
			},
			totalProperty:  'total',
			messageProperty:'msg',
			autoLoad:       false,
			listeners: {
				exception:   NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad:          false,
		listeners: {
			load:           procesarConsultaData
		}
	});

	// Se crea el STORE del grid 'Consulta Data1'
	var consultaData1 = Ext.create('Ext.data.Store',{
		storeId:           'consultaData1',
		model:             'ListaData1',
		proxy: {
			type:           'ajax',
			url:            '15forma03ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CONSULTA_NEGOCIABLE'
			},
			totalProperty:  'total',
			messageProperty:'msg',
			autoLoad:       false,
			listeners: {
				exception:   NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad:          false,
		listeners: {
			load:           procesarConsultaData1
		}
	});

	// Se crea el STORE del grid 'Detalles'
	var consultaDetalle = Ext.create('Ext.data.Store',{
		storeId:            'consultaDetalle',
		model:              'ListaDetalle',
		pageSize:           15,
		proxy: {
			type:            'ajax',
			url:             '15forma03ext.data.jsp',
			reader: {
				type:         'json',
				root:         'registros'
			},
			extraParams: {
				informacion:  'VER_DETALLE'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception:    NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad:           false,
		listeners: {
			load:            procesarDetalle
		}
	});

	// Se crea el STORE del grid 'Total Detalles'
	var consultaDetalleTotal = Ext.create('Ext.data.Store',{
		storeId:            'consultaDetalleTotal',
		model:              'ListaDetalleTotales',
		proxy: {
			type:            'ajax',
			url:             '15forma03ext.data.jsp',
			reader: {
				type:         'json',
				root:         'registros'
			},
			extraParams: {
				informacion:  'VER_DETALLE_TOTALES'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception:    NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad:           false,
		listeners: {
			beforeLoad: {fn: function(store, options){
				Ext.apply(options.params, {
					mes_ini:    mes_detalle,
					anio_ini:   anio_detalle
				});
			}},
			load:            procesarDetalleTotal
		}
	});

	// Se crea el STORE del grid 'Detalles 1'
	var consultaDetalle1 = Ext.create('Ext.data.Store',{
		storeId:            'consultaDetalle1',
		model:              'ListaDetalle1',
		pageSize:           15,
		proxy: {
			type:            'ajax',
			url:             '15forma03ext.data.jsp',
			reader: {
				type:         'json',
				root:         'registros'
			},
			extraParams: {
				informacion:  'VER_DETALLE_1'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception:    NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad:           false,
		listeners: {
			load:            procesarDetalle1
		}
	});

	// Se crea el STORE del grid 'Total Detalles 1'
	var consultaDetalleTotal1 = Ext.create('Ext.data.Store',{
		storeId:            'consultaDetalleTotal1',
		model:              'ListaDetalleTotales1',
		proxy: {
			type:            'ajax',
			url:             '15forma03ext.data.jsp',
			reader: {
				type:         'json',
				root:         'registros'
			},
			extraParams: {
				informacion:  'VER_DETALLE_TOTALES_1'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception:    NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad:           false,
		listeners: {
			load:            procesarDetalleTotal1
		}
	});

	var consultaDetalleTotal2 = Ext.create('Ext.data.Store',{
		storeId:            'consultaDetalleTotal2',
		model:              'ListaDetalleTotales2',
		proxy: {
			type:            'ajax',
			url:             '15forma03ext.data.jsp',
			reader: {
				type:         'json',
				root:         'registros'
			},
			extraParams: {
				informacion:  'VER_DETALLE_TOTALES_2'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception:    NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad:           false,
		listeners: {
			load:            procesarDetalleTotal2
		}
	});

//------------------------------ Componentes -------------------------
	// Se crea el grid 'ConsultaData'
	var gridData = Ext.create('Ext.grid.Panel',{
		width:           835,
		height:          430,
		xtype:           'grouped-text-grid',
		itemId:          'gridData',
		style:           'margin:0 auto;',
		bodyStyle:       'padding: 6px',
		store:           Ext.data.StoreManager.lookup('consultaData'),
		hideTitle:       true,
		scroll:          true,
		hidden:          true,
		columns: [{
			text:         'A�o / Mes',
			columns: [{
				width: 120,  dataIndex: 'ANIO_NOTIFICACION',  align: 'center',  text: 'A�o'
			},{
				width: 120,  dataIndex: 'MES_NOTIFICACION',   align: 'left',    text: 'Mes'
			}]
		},{
				width: 150,  dataIndex: 'PYMES',              align: 'center',  text: 'No. de PYMES �nicas'
		},{
				width: 190,  dataIndex: 'DOCTOS_NOTIFICADOS', align: 'center',  text: 'No. de Documentos Notificados'
		},{
				width: 130,  dataIndex: 'MONTO_NOTIFICADO',   align: 'center',  text: 'Monto Notificado', renderer: 'usMoney'
		},{
			width:        90,
			xtype:        'actioncolumn',
			header:       'Ver',
			align:        'center',
			sortable:     false,
			resizable:    false,
			items: [{
				iconCls:    'icoBuscar',
				tooltip:    'Ver formato',
				handler:    function(grid, rowIndex, colIndex){
					var rec        = consultaData.getAt(rowIndex);
					mes_detalle    = rec.get('MES_NOTIFICACION');
					anio_detalle   = rec.get('ANIO_NOTIFICACION');
					titulo_detalle = rec.get('MES_NOTIFICACION') + ' ' + rec.get('ANIO_NOTIFICACION');
					main.el.mask('Procesando...', 'x-mask-loading');
					consultaDetalle.loadPage(1, {
						params: Ext.apply({
							mes_ini:    mes_detalle,
							anio_ini:   anio_detalle,
							tipo_docto: opcionReporte,
							operacion:  'generar',
							start:      0,
							limit:      15
						})
					});
				}
			}]
		}],
		bbar: ['->','-',{
			xtype:        'button',
			itemId:       'btnReporteXLS',
			iconCls:      'icoXls', 
			text:         'Generar Archivo',
			handler:      generaReporte
		}]
	});

	// Se crea el grid 'ConsultaData 1'
	var gridData1 = Ext.create('Ext.grid.Panel',{
		width:           '93%',
		height:          430,
		xtype:           'grouped-text-grid',
		itemId:          'gridData1',
		style:           'margin:0 auto;',
		bodyStyle:       'padding: 6px',
		store:           Ext.data.StoreManager.lookup('consultaData1'),
		hideTitle:       true,
		scroll:          true,
		hidden:          true,
		columns: [{
			text:         'A�o / Mes',
			columns: [{
				width: 120,  dataIndex: '0',   align: 'center', text: 'A�o'
			},{
				width: 120,  dataIndex: '1',   align: 'left',   text: 'Mes'
			}]
		},{
				width: 80,   dataIndex: '2',   align: 'center', text: 'No. de <br> PYMES �nicas'
		},{
			text:           'Notificaciones Operadas',
			columns: [{
				width: 120,  dataIndex: '3',   align: 'center', text: 'No. de <br> Documentos'
			},{
				width: 120,  dataIndex: '4',   align: 'center', text: 'Monto Operado', renderer: 'usMoney'
			},{
				width: 120,  dataIndex: '5',   align: 'center', text: 'No. de <br> Notificaciones'
			},{
				width: 120,  dataIndex: '6',   align: 'center', text: 'Costo de <br> Notificaciones', renderer: 'usMoney'
			},{
				width: 120,  dataIndex: '7',   align: 'center', text: 'Inter�s por <br> Doctos. <br> Operados', renderer: 'usMoney'
			}]
		},{
			text:           'Notificaciones Negociables',
			columns: [{
				width: 120,  dataIndex: '8',   align: 'center', text: 'No. de <br> Documentos'
			},{
				width: 120,  dataIndex: '9',   align: 'center', text: 'Monto Negociable', renderer: 'usMoney'
			},{
				width: 120,  dataIndex: '10',  align: 'center', text: 'No. de <br> Notificaciones'
			},{
				width: 120,  dataIndex: '11',  align: 'center', text: 'Costo de <br> Notificaciones', renderer: 'usMoney'
			}]
		},{
			text:           'Notificaciones Vencidas',
			columns: [{
				width: 120,  dataIndex: '12',  align: 'center', text: 'No. de <br> Documentos'
			},{
				width: 120,  dataIndex: '13',  align: 'center', text: 'Monto Vencido', renderer: 'usMoney'
			},{
				width: 120,  dataIndex: '14',  align: 'center', text: 'No. de <br> Notificaciones'
			},{
				width: 120,  dataIndex: '15',  align: 'center', text: 'Costo de <br> Notificaciones', renderer: 'usMoney'
			}]
		},{
			width:        90,
			xtype:        'actioncolumn',
			header:       'Ver',
			align:        'center',
			sortable:     false,
			resizable:    false,
			items: [{
				iconCls:    'icoBuscar',
				tooltip:    'Ver formato',
				handler:    function(grid, rowIndex, colIndex){
					var rec        = consultaData1.getAt(rowIndex);
					mes_detalle    = rec.get('1');
					anio_detalle   = rec.get('0');
					titulo_detalle = rec.get('1') + ' ' + rec.get('0');
					main.el.mask('Procesando...', 'x-mask-loading');
					consultaDetalle1.loadPage(1, {
						params: Ext.apply({
							mes_ini:    mes_detalle,
							anio_ini:   anio_detalle,
							operacion:  'generar',
							start:      0,
							limit:      15
						})
					});
				}
			}]
		}],
		bbar: ['->','-',{
			xtype:        'button',
			itemId:       'btnReporteXLS1',
			iconCls:      'icoXls', 
			text:         'Generar Archivo',
			handler:      generaReporte
		}]
	});

	// Se crea el grid 'Detalle'
	var gridDetalle = Ext.create('Ext.grid.Panel',{
		height:          440,
		width:           '93%',
		itemId:          'gridDetalle',
		style:           'margin:0 auto;',
		bodyStyle:       'padding: 6px',
		title:           'Consulta',
		store:           Ext.data.StoreManager.lookup('consultaDetalle'),
		stripeRows:      true,
		hidden:          true,
		columns: [{
				width: 75,   dataIndex: 'NAFIN_ELECTRONICO', align: 'center',  text: 'No. <br> N@E'
		},{
				width: 80,   dataIndex: 'IC_PYME',           align: 'center',  text: 'No. <br> PYME'
		},{
				width: 130,  dataIndex: 'RFC',               align: 'center',  text: 'RFC'
		},{
				width: 230,  dataIndex: 'NOMBRE_PYME',       align: 'left',    text: 'Raz�n Social'
		},{
				width: 120,  dataIndex: 'NUM_CEL',           align: 'center',  text: 'N�m. Celular'
		},{
				width: 120,  dataIndex: 'DOCTOS_NOTIFICADOS',align: 'center',  text: 'No. de  <br> Documentos <br> Notificados'
		},{
				width: 150,  dataIndex: 'MONTO_NOTIFICADO',  align: 'center',  text: 'Monto Notificado', renderer: 'usMoney'
		},{
				width: 120,  dataIndex: 'DOCTOS_OPERADOS',   align: 'center',  text: 'No. de <br> Documentos <br> Operados'
		},{
				width: 150,  dataIndex: 'MONTO_OPERADO',     align: 'center',  text: 'Monto Operado', renderer: 'usMoney'
		},{
				width: 120,  dataIndex: 'DOCTOS_VENCIDOS',   align: 'center',  text: 'No. de <br> Documentos <br> Vencidos'
		},{
				width: 150,  dataIndex: 'MONTO_VENCIDO',     align: 'center',  text: 'Monto Vencido', renderer: 'usMoney'
		}],
		bbar: [
			Ext.create('Ext.PagingToolbar',{
			store:        Ext.data.StoreManager.lookup('consultaDetalle'),
			itemId:       'barraPaginacion',
			displayMsg:   '{0} - {1} de {2}',
			emptyMsg:     'No se encontraron registros',
			displayInfo:  true,
			border:       false
		}),'->','-',{
			xtype:        'button',
			itemId:       'btnReporteDetalleXLS',
			iconCls:      'icoXls', 
			text:         'Generar Archivo',
			disabled:     true,
			handler:       generaReporteDetalle
		},'-',{
			xtype:        'button',
			itemId:       'btnRegresarDetalle',
			iconCls:      'icoRegresar', 
			text:         'Regresar',
			handler:       limpiar
		}]
	});

	// Se crea el grid 'Detalle Total'
	var gridDetalleTotal = Ext.create('Ext.grid.Panel',{
		width:           '93%',
		itemId:          'gridDetalleTotal',
		style:           'margin:0 auto;',
		bodyStyle:       'padding: 6px',
		store:           Ext.data.StoreManager.lookup('consultaDetalleTotal'),
		autoHeight:      true,
		hideTitle:       true,
		scroll:          true,
		hidden:          true,
		columns: [{
				width: 115,  dataIndex: 'NO_PYMES',          align: 'center',  text: 'No. <br> PYMES'
		},{
				width: 100,  dataIndex: 'DOCTOS_NOTIFICADOS',align: 'center',  text: 'No. de  <br> Documentos <br> Notificados'
		},{
				width: 150,  dataIndex: 'MONTO_NOTIFICADO',  align: 'center',  text: 'Monto Notificado', renderer: 'usMoney'
		},{
				width: 100,  dataIndex: 'DOCTOS_OPERADOS',   align: 'center',  text: 'No. de <br> Documentos <br> Operados'
		},{
				width: 150,  dataIndex: 'MONTO_OPERADO',     align: 'center',  text: 'Monto Operado', renderer: 'usMoney'
		},{
				width: 100,  dataIndex: 'DOCTOS_VENCIDOS',   align: 'center',  text: 'No. de <br> Documentos <br> Vencidos'
		},{
				width: 150,  dataIndex: 'MONTO_VENCIDO',     align: 'center',  text: 'Monto Vencido', renderer: 'usMoney'
		}]
	});

	// Se crea el grid 'Detalle 1'
	var gridDetalle1 = Ext.create('Ext.grid.Panel',{
		height:          440,
		width:           '93%',
		itemId:          'gridDetalle1',
		style:           'margin:0 auto;',
		bodyStyle:       'padding: 6px',
		title:           'Consulta',
		store:           Ext.data.StoreManager.lookup('consultaDetalle1'),
		stripeRows:      false,
		hidden:          true,
		columns: [{
				width: 70,   dataIndex: 'NO_NAFIN',        align: 'center',  text: 'No. <br> N@E'
		},{
				width: 80,   dataIndex: 'PYME',            align: 'center',  text: 'No. <br> PyME'
		},{
				width: 130,  dataIndex: 'RFC',             align: 'center',  text: 'RFC'
		},{
				width: 230,  dataIndex: 'RAZON_SOCIAL',    align: 'left',    text: 'Raz�n Social'
		},{
				width: 120,  dataIndex: 'CELULAR',         align: 'center',  text: 'N�m. Celular'
		},{
				width: 120,  dataIndex: 'ESTADO',          align: 'center',  text: 'Estado'
		},{
				width: 150,  dataIndex: 'TIPO_EPO',        align: 'center',  text: 'Tipo EPO'
		},{
			text:           'Notificaciones Operadas',
			columns: [{
				width: 120,  dataIndex: 'DOCTOS_OPER',     align: 'center',  text: 'No. de <br> Documentos'
			},{
				width: 120,  dataIndex: 'MONTO_OPER',      align: 'center',  text: 'Monto Operado', renderer: 'usMoney'
			},{
				width: 150,  dataIndex: 'NUM_NOT_OPER',    align: 'center',  text: 'No. de <br> Notificaciones'
			},{
				width: 120,  dataIndex: 'COSTO_NOT_OPER',  align: 'center',  text: 'Costo de <br> Notificaciones', renderer: 'usMoney'
			},{
				width: 150,  dataIndex: 'INTERESES',       align: 'center',  text: 'Inter�s por <br> Doctos. <br> Operados', renderer: 'usMoney'
			}]
		},{
			text:           'Notificaciones Negociables',
			columns: [{
				width: 150,  dataIndex: 'DOCTOS_NEG',      align: 'center',  text: 'No. de <br> Documentos'
			},{
				width: 150,  dataIndex: 'MONTO_NEG',       align: 'center',  text: 'Monto Negociable', renderer: 'usMoney'
			},{
				width: 150,  dataIndex: 'NUM_NOT_NEG',     align: 'center',  text: 'No. de <br> Notificaciones'
			},{
				width: 150,  dataIndex: 'COSTO_NOT_NEG',   align: 'center',  text: 'Costo de <br> Notificaciones', renderer: 'usMoney'
			}]
		},{
			text:           'Notificaciones Vencidas',
			columns: [{
				width: 150,  dataIndex: 'NUM_DOCTOS_VEN',  align: 'center',  text: 'No. de <br> Documentos'
		},{
				width: 150,  dataIndex: 'MONTO_VEN',       align: 'center',  text: 'Monto Vencido', renderer: 'usMoney'
		},{
				width: 150,  dataIndex: 'NUM_NOT_VEN',     align: 'center',  text: 'No. de <br> Notificaciones'
		},{
				width: 150,  dataIndex: 'COSTO_NOT_VEN',   align: 'center',  text: 'Costo de <br> Notificaciones', renderer: 'usMoney'
			}]
		}],
		bbar: [
			Ext.create('Ext.PagingToolbar',{
			store:        Ext.data.StoreManager.lookup('consultaDetalle1'),
			itemId:       'barraPaginacion',
			displayMsg:   '{0} - {1} de {2}',
			emptyMsg:     'No se encontraron registros',
			displayInfo:  true,
			border:       false
		}),'->','-',{
			xtype:        'button',
			itemId:       'btnReporteDetalleXLS_1',
			iconCls:      'icoXls', 
			text:         'Generar Archivo',
			disabled:     true,
			handler:      generaReporteDetalle1
		},'-',{
			xtype:        'button',
			itemId:       'btnRegresarDetalle_1',
			iconCls:      'icoRegresar', 
			text:         'Regresar',
			handler:       regresarDetalle
		}]
	});

	// Se crea el grid 'Detalle Total 1'
	var gridDetalleTotal1 = Ext.create('Ext.grid.Panel',{
		width:           '93%',
		itemId:          'gridDetalleTotal1',
		style:           'margin:0 auto;',
		bodyStyle:       'padding: 6px',
		store:           Ext.data.StoreManager.lookup('consultaDetalleTotal1'),
		autoHeight:      true,
		hideTitle:       true,
		hidden:          true,
		columns: [{
				width: 150,  sortable: false,   dataIndex: 'DOCTOS_NOTIFICADOS',   align: 'center',  text: 'Documentos Notificados'
		},{
				width: 150,  sortable: false,   dataIndex: 'MONTO_NOTIFICADO',     align: 'center',  text: 'Monto Notificado', renderer: 'usMoney'
		},{
				width: 160,  sortable: false,   dataIndex: 'NUM_NOTIFICACIONES',   align: 'center',  text: 'No. de Notificaciones'
		},{
				width: 190,  sortable: false,   dataIndex: 'COSTO_NOTIFICACIONES', align: 'center',  text: 'Costo de las Notificaciones', renderer: 'usMoney'
		},{
				width: 210,  sortable: false,   dataIndex: 'INTERESES_OPERADOS',   align: 'center',  text: 'Inter�ses por Doctos. Operados', renderer: 'usMoney'
		}]
	});

	// Se crea el grid 'Detalle Total 1'
	var gridDetalleTotal2 = Ext.create('Ext.grid.Panel',{
		width:           '93%',
		itemId:          'gridDetalleTotal2',
		style:           'margin:0 auto;',
		bodyStyle:       'padding: 6px',
		store:           Ext.data.StoreManager.lookup('consultaDetalleTotal2'),
		autoHeight:      true,
		hideTitle:       true,
		hidden:          true,
		columns: [{
				width: 260,  sortable: false,  dataIndex: 'DESCRIPCION',  align: 'center',  text: 'Doctos. en N@E'
		},{
				width: 300,  sortable: false,  dataIndex: 'DOCTOS_NE',    align: 'center',  text: 'No. Total de Doctos. en N@E'
		},{
				width: 300,  sortable: false,  dataIndex: 'MONTO_NE',     align: 'center',  text: 'Monto Total de Doctos. en N@E', renderer: 'usMoney'
		}]
	});

	// Se crea el form principal
	var formaPrincipal = Ext.create( 'Ext.form.Panel',{
		width:                  '95%',
		itemId:                 'formaPrincipal',
		title:                  'Documentos Negociables Notificados',
		bodyPadding:            '12 6 12 6',
		style:                  'margin: 0px auto 0px auto;',
		frame:                  true,
		border:                 true,
		fieldDefaults: {
			msgTarget:           'side'
		},
		items: [{
			xtype:               'container',
			layout:              'hbox',
			anchor:              '95%',
			margin:              '0 0 5 0',
			items:[{
				width:            90,
				xtype:            'displayfield',
				hideLabel:        true
			},{
				width:            650,
				xtype:            'radiogroup',
				fieldLabel:       'Auto Layout',
				cls:              'x-check-group-alt',
				id:               'opcion_id',
				name:             'opcion',
				hideLabel:        true,
				items: [{
					boxLabel:      'Negociables',
					name:          'opcion',
					inputValue:    '1',
					checked:       true
				},{
					boxLabel:      'Por Vencer',
					name:          'opcion',
					inputValue:    '2'
				},{
					boxLabel:      'Costo e ingreso de las notificaciones',
					name:          'opcion',
					inputValue:    '3'
				}],
				listeners: {
					change: function (field, newValue, oldValue) {
						Ext.ComponentQuery.query('#gridData')[0].hide();
						Ext.ComponentQuery.query('#gridData1')[0].hide();
						switch (newValue['opcion']){
							case '1':
								Ext.ComponentQuery.query('#formaPrincipal')[0].setTitle('Documentos Negociables Notificados');
								opcionReporte = 'N';
								break;
							case '2':
								Ext.ComponentQuery.query('#formaPrincipal')[0].setTitle('Documentos por Vencer Notificados');
								opcionReporte = 'V';
								break;
							case '3':
								Ext.ComponentQuery.query('#formaPrincipal')[0].setTitle('Documentos Negociables Notificados');
								opcionReporte = 'CI';
								break;
							default:
								opcionReporte ='';
								break;
						}
					}
				}
			}]
		},{
			xtype:               'container',
			layout:              'hbox',
			anchor:              '95%',
			margin:              '0 0 5 0',
			items:[{
				width:            170,
				xtype:            'displayfield',
				value:            'Fecha de Notificaci�n desde ',
				hideLabel:        true
			},{
				width:            150,
				labelWidth:       30,
				xtype:            'combobox',
				itemId:           'mes_ini_id',
				name:             'mes_ini',
				hiddenName:       'mes_ini',
				fieldLabel:       'Mes',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoMesIni
			},{
				width:            120,
				labelWidth:       30,
				xtype:            'combobox',
				itemId:           'anio_ini_id',
				name:             'anio_ini',
				hiddenName:       'anio_ini',
				fieldLabel:       'A�o',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoAnioIni
			},{
				width:            60,
				xtype:            'displayfield',
				value:            'Hasta ',
				hideLabel:        true
			},{
				width:            150,
				labelWidth:       30,
				xtype:            'combobox',
				itemId:           'mes_fin_id',
				name:             'mes_fin',
				hiddenName:       'mes_fin',
				fieldLabel:       'Mes',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoMesFin
			},{
				width:            120,
				labelWidth:       30,
				xtype:            'combobox',
				itemId:           'anio_fin_id',
				name:             'anio_fin',
				hiddenName:       'anio_fin',
				fieldLabel:       'A�o',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoAnioFin
			}]
		}],
		buttons: [{
			text:                'Generar',
			itemId:              'btnGenerar',
			iconCls:             'icoBuscar',
			handler:             generar
		},{
			text:                'Limpiar',
			itemId:              'btnLimpiar',
			iconCls:             'icoLimpiar',
			handler:             limpiar
		}]
	});

	// Contenedor principal
	var main = Ext.create('Ext.container.Container',{
		width:      949,
		minHeight:  650,
		autoHeight: true,
		id:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		style:      'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			NE.util.getEspaciador(10),
			gridData,
			gridData1,
			gridDetalle,
			gridDetalleTotal,
			gridDetalle1,
			gridDetalleTotal1,
			gridDetalleTotal2
		]
	});

});