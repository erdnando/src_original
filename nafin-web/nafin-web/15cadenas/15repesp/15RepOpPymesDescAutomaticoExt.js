Ext.onReady(function() {

	
	
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();							
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	Ext.apply(Ext.form.field.VTypes, {
		rangoFechas: function(val, field) {
			var date = field.parseDate(val);	
			if (!date) {
				return false;
			}
			if (field.startDateField && (!this.rangoFechasMax || (date.getTime() != this.rangoFechasMax.getTime()))) {
				var start = field.up('form').down('#' + field.startDateField);
				start.setMaxValue(date);
				start.validate();
				this.rangoFechasMax = date;
			} else if (field.endDateField && (!this.rangoFechasMin || (date.getTime() != this.rangoFechasMin.getTime()))) {
				var end = field.up('form').down('#' + field.endDateField);
				end.setMinValue(date);
				end.validate();
				this.rangoFechasMin = date;
			}
			return true;
		},
		rangoFechasText: 'La fecha de inicio debe ser menor que la fecha final'
	});

	
	
	//-------------GENERCION DE LA CONSULTA DE  Detalle de PyME's DE Descuento Autom�tico Habilitado
		
	var procesarArchivosXLS_Det = function() {
		var  fp =   Ext.ComponentQuery.query('#forma')[0];		
					
		Ext.Ajax.request({
			url: '15RepOpPymesDescAutomatico.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{					
				informacion:'Generar_Arch_Det',
				tipo:'XLS',
				habilitado:Ext.ComponentQuery.query('#habilitado')[0].getValue()
			}),
			callback: procesarDescargaArchivos
		});
	}
	
	var procesarArchivosPDF_Det = function() {
		var  fp =   Ext.ComponentQuery.query('#forma')[0];		
					
		Ext.Ajax.request({
			url: '15RepOpPymesDescAutomatico.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{					
				informacion:'Generar_Arch_Det',
				tipo:'PDF',
				habilitado:Ext.ComponentQuery.query('#habilitado')[0].getValue()
			}),
			callback: procesarDescargaArchivos
		});
	}
	
		var detallePymes = function (habilitado){
	
			Ext.ComponentQuery.query('#habilitado')[0].setValue(habilitado);
	
			var  fp =   Ext.ComponentQuery.query('#forma')[0];		
				consDetallePymeData.load({
				params: Ext.apply(fp.getForm().getValues(),{ 
					informacion:'ConDetalle_pyme',
					habilitado: habilitado
				})
			});		
		
		new Ext.Window({
			modal: true,
			width: 470,
			resizable: false,
			closable:true,
			id: 'VentanaCheck',
			autoDestroy:false,
			closeAction: 'destroy',
			items: [
				gridDetalle				
			]		
		}).show().setTitle('Detalle de PyMEs');	
			
	}
	
	
	var procesarConsDetallePyme = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			var  forma =   Ext.ComponentQuery.query('#forma')[0];
			forma.el.unmask();
				
			
			if(store.getTotalCount() > 0) {
				Ext.ComponentQuery.query('#btnDescArchDetPyme')[0].enable();	
				Ext.ComponentQuery.query('#btnImprimirDetPyme')[0].enable();
				Ext.ComponentQuery.query('#gridDetalle')[0].getView().setAutoScroll(true);
			}else  {
				Ext.ComponentQuery.query('#gridDetalle')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridDetalle')[0].getView().setAutoScroll(false);
			
				Ext.ComponentQuery.query('#btnDescArchDetPyme')[0].disable();	
				Ext.ComponentQuery.query('#btnImprimirDetPyme')[0].disable();	
			}
		}
	};
	
	Ext.define('LisRegDetalle', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'NO_ELECTRONICO'},
			{ name: 'NOMBRE_PYME'},
			{ name: 'RFC'}				
		 ]
	});
	
	var consDetallePymeData = Ext.create('Ext.data.Store', {
		model: 'LisRegDetalle',
		proxy: {
			type: 'ajax',
			url: '15RepOpPymesDescAutomatico.data.jsp',
			reader: 		 { type: 'json',  root: 'registros' 	},
			extraParams: { informacion: 'ConDestalle_pyme' 	},
			listeners:   { exception: NE.util.mostrarProxyAjaxError 	}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsDetallePyme
		}		
	});
	
	var  gridDetalle = {
		xtype: 'grid',		
		itemId: 'gridDetalle',
		store: consDetallePymeData,
		height: 200,
		width: 440,
		style: 'margin: 10px auto 0px auto;',	     
      frame: true,		
		clicksToEdit: 1,
		columns: [		
			{
				header: 'No. Nafin Electr�nico',
				dataIndex: 'NO_ELECTRONICO',
				sortable: true,
				width: 120,
				align: 'center'
         },	
			{
				header: 'Nombre de la PyME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 200,
				align: 'left'
         },	
			{
				header: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 100,
				align: 'center'
         }			
				
		],		
		bbar:{
			items: [
				'-',
				'->',
				{
					text: 'Descarga Archivo',
					id: 'btnDescArchDetPyme',	
					iconCls: 'icoBotonXLS',
					handler: procesarArchivosXLS_Det				
				},
				{
					text: 'Imprimir',
					id: 'btnImprimirDetPyme',	
					iconCls: 'icoPdf',
					handler: procesarArchivosPDF_Det	
				}
			]
		}		
	}
	
	
	//-------------GENERACI�N DE LA CONSULTA PRINCIPAL ----------------------------
	
	
		
	
	var procesarArchivosXLS = function() {
		var  fp =   Ext.ComponentQuery.query('#forma')[0];		
					
		Ext.Ajax.request({
			url: '15RepOpPymesDescAutomatico.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{					
				informacion:'Generar_Arch',
				tipo:'XLS'
			}),
			callback: procesarDescargaArchivos
		});
	}
	
	
	var procesarArchivosPDF = function() {
		var  fp =   Ext.ComponentQuery.query('#forma')[0];		
							
		Ext.Ajax.request({
			url: '15RepOpPymesDescAutomatico.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{					
				informacion:'Generar_Arch',
				tipo:'PDF'
			}),
			callback: procesarDescargaArchivos
		});
	}
	
	
	
	var procesarConsultar = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			var  forma =   Ext.ComponentQuery.query('#forma')[0];			
			main.el.unmask();
			
			Ext.ComponentQuery.query('#gridConsulta')[0].show();	
			
			if(store.getTotalCount() > 0) {
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(true);
				Ext.ComponentQuery.query('#btnDescArch')[0].enable();
				Ext.ComponentQuery.query('#btnImprimir')[0].enable();
			}else  {
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(false);
				Ext.ComponentQuery.query('#btnDescArch')[0].disable();
				Ext.ComponentQuery.query('#btnImprimir')[0].disable();
				
					
		
		
			}
		}
	};

	
	Ext.define('LisRegistros', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'NOMBRE_EPO'},
			{ name: 'NOMBRE_IF'},
			{ name: 'CONVENIO'},
			{ name: 'TOTAL_PYMES'},
			{ name: 'NO_PYMES_DESCACT_HAB'},
			{ name: 'NO_DOCTOS_PUBLIC_HAB'},
			{ name: 'MONTO_PUBLIC_HAB'},
			{ name: 'NO_DOCTOS_OPERADOS_HAB'},
			{ name: 'MONTO_OPERADOS_HAB'},
			{ name: 'NO_PYMES_DESCACT_NOHAB'},
			{ name: 'NO_DOCTOS_PUBLIC_NOHAB'},
			{ name: 'MONTO_PUBLIC_NOHAB'},
			{ name: 'NO_DOCTOS_OPERADOS_NOHAB'},
			{ name: 'MONTO_OPERADOS_NOHAB'}				
		 ]
	});
	
	var consultaData = Ext.create('Ext.data.Store', {
		model: 'LisRegistros',
		proxy: {
			type: 'ajax',
			url: '15RepOpPymesDescAutomatico.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consultar'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsultar
			}
	});
	
	
	var  gridConsulta = Ext.create('Ext.grid.Panel',{
		itemId: 'gridConsulta',
		store: consultaData,				
	   title: 'Consulta',
		xtype:           'grouped-header-grid',
		style: 'margin: 10px auto 0px auto;',
     	hidden:          true,
		frame: true,
		height: 400,		
		width: 900,		
		columns: [
			{
				header: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				width:        150,
				sortable:     true,
			   resizable:    true,				
			   align:        'center'
         },
			{
				header: 'IF',
				dataIndex: 'NOMBRE_IF',				
				width:        150,
				sortable:     true,
			   resizable:    true,
			   align:        'center'		
         },
			{
				header: 'Convenio',
				dataIndex: 'CONVENIO',				
				width:        150,
				sortable:     true,
			   resizable:    true,
			   align:        'center'		
         },
			{
				header: 'Total de PyMEs',
				dataIndex: 'TOTAL_PYMES',
				width:        150,
				sortable:     true,
			   resizable:    true,
			   align:        'center'		
         },
			{
				text: 'Descuento Autom�tico Habilitado',
				menuDisabled: false,
				columns: [
					{
						header: 'No. de PyMEs con <br>Descuento Autom�tico',
						dataIndex: 'NO_PYMES_DESCACT_HAB',
						sortable: true,
						width: 150,
						align: 'center',
						resizable: true
					},
				  {
						header: 'No. Doctos. Publicados',
						dataIndex: 'NO_DOCTOS_PUBLIC_HAB',
						sortable: true,
						align: 'center',
						width: 150						
					},
					{
						header: 'Monto Publicado',
						dataIndex: 'MONTO_PUBLIC_HAB',
						sortable: true,						
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						header: 'No. Doctos Operados',
						dataIndex: 'NO_DOCTOS_OPERADOS_HAB',
						sortable: true,
						width: 150,
						align: 'center',
						resizable: true
					},
					{
						header: 'Monto Operado',
						dataIndex: 'MONTO_OPERADOS_HAB',
						sortable: true,
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						xtype		: 'actioncolumn',
						header: 'Detalle PyMes',
						tooltip: 'CDetalle PyMes',				
						sortable: true,
						width: 150,			
						resizable: true,				
						align: 'center',
						items		: [
							{
								getClass: function(value,metadata,record,rowIndex,colIndex,store){									
									this.items[0].tooltip = 'Ver';
									return 'icoBuscar';									
								}							
								,handler: function() {
									detallePymes('S');
								}
							}
						]				
					}
				]
			},
			{
				text: 'Descuento Autom�tico No Habilitado',
				menuDisabled: false,
				columns: [
					{
						header: 'No. de PyMEs sin <br> Descuento Autom�tico',
						dataIndex: 'NO_PYMES_DESCACT_NOHAB',
						sortable: true,
						width: 150,
						align: 'center',
						resizable: true
					},			
					{
						header: 'No. Doctos. Publicados',
						dataIndex: 'NO_DOCTOS_PUBLIC_NOHAB',
						sortable: true,
						align: 'center',
						width: 150						
					},
					{
						header: 'Monto Publicado',
						dataIndex: 'MONTO_PUBLIC_NOHAB',
						sortable: true,
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						header: 'No. Doctos Operados',
						dataIndex: 'NO_DOCTOS_OPERADOS_NOHAB',
						sortable: true,
						width: 150,
						align: 'center',
						resizable: true
					},
					{
						header: 'Monto Operado',
						dataIndex: 'MONTO_OPERADOS_NOHAB',
						sortable: true,
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						xtype		: 'actioncolumn',
						header: 'Detalle PyMes',
						tooltip: 'CDetalle PyMes',				
						sortable: true,
						width: 150,			
						resizable: true,				
						align: 'center',
						items		: [
							{
								getClass: function(value,metadata,record,rowIndex,colIndex,store){									
									this.items[0].tooltip = 'Ver';
									return 'icoBuscar';									
								}
								,handler: function() {
									detallePymes('N');
								}
							}
						]				
					}
				]
			}
		],
		bbar: [
			'->','-',
			{
				text: 'Descarga Archivo',
				id: 'btnDescArch',	
				iconCls: 'icoBotonXLS',
				handler: procesarArchivosXLS
				
			},
			{
				text: 'Imprimir',
				id: 'btnImprimir',	
				iconCls: 'icoPdf',
				handler: procesarArchivosPDF
			}
		] 		
	});
	
	
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
		
	
	var  catalogoEpo = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15RepOpPymesDescAutomatico.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoEpo'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	
	var  catalogoIF = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15RepOpPymesDescAutomatico.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoIF'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	
	var  catConvenio = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15RepOpPymesDescAutomatico.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catConvenio'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	
	var elementosForma =  [
		{ xtype: 'textfield',	itemId: 'habilitado', hidden:  true },
		{
			xtype: 'combo',
			fieldLabel: 'EPO',
			itemId: 'ic_epo1',
			name: 'ic_epo',
			hiddenName: 'ic_epo',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			store: catalogoEpo,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',			
			listeners:	{					
				select:	{
					fn: function(combo){					
						
						catalogoIF.load({
							params: 	{
								ic_epo: combo.getValue()
							}
						});
					}					
				}
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'IF',
			itemId: 'ic_if1',
			name: 'ic_if',
			hiddenName: 'ic_if',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			store: catalogoIF,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',			
			listeners:	{					
				select:	{
					fn: function(combo){
						
					}					
				}
			}
		},
				{
			xtype: 'combo',
			fieldLabel: 'Convenio',
			itemId: 'ic_convenio1',
			name: 'ic_convenio',
			hiddenName: 'ic_convenio',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			store: catConvenio,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',			
			listeners:	{					
				select:	{
					fn: function(combo){
						
					}					
				}
			}
		},
		{
			xtype: 'container',			
			layout: 'hbox', 			
			items: [
				{
					xtype: 'datefield',					
					fieldLabel: 'Fecha de Public./Oper. de',
					name: 'fecha_public_oper_ini',
					itemId: 'fecha_public_oper_ini',
					vtype: 'rangoFechas',
					endDateField: 'fecha_public_oper_fin',
					allowBlank: true,
					startDay: 1,					
					msgTarget: 'side',				
					minValue: '01/01/1901',					
					margins: '0 20 0 0'
					
				},
				{
					xtype: 'displayfield',
					value: '&nbsp;al &nbsp;', 
					width: 30
				},
				{
					xtype: 'datefield',					
					labelAlign: 'right',					
					name: 'fecha_public_oper_fin',
					itemId: 'fecha_public_oper_fin',
					vtype: 'rangoFechas',
					startDateField: 'fecha_public_oper_ini',
					allowBlank: true,
					startDay: 1,					
					msgTarget: 'side',				
					minValue: '01/01/1901',					
					margins: '0 20 0 0'					
				}
			]
		}		
	];	
	
	

	var fp = Ext.create('Ext.form.Panel',	{		
		itemId: 'forma',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: 'Operaci�n de PYMEs con Descuento Autom�tico',
      width: 600,		
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 150
		},		
		items:elementosForma, 
		buttons: [		
			{
				text: 'Generar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(){
				
					var  fp =   Ext.ComponentQuery.query('#forma')[0];
					
					var ic_convenio =  fp.query('#ic_convenio1')[0]; 
					
					if(Ext.isEmpty(ic_convenio.getValue())){
						ic_convenio.markInvalid('El valor de el Convenio es requerido');
						ic_convenio.focus();						
						return;
					}
					
						var fecha_public_oper_ini =  fp.query('#fecha_public_oper_ini')[0]; 
						var fecha_public_oper_fin =  fp.query('#fecha_public_oper_fin')[0]; 
							
					if(Ext.isEmpty(fecha_public_oper_ini.getValue()) || Ext.isEmpty(fecha_public_oper_fin.getValue()) ){
						if(Ext.isEmpty(fecha_public_oper_ini.getValue())){
							fecha_public_oper_ini.markInvalid('El valor de la Fecha de Public./Oper. inicial es requerido');
							fecha_public_oper_ini.focus();
							return;
						}
						if(Ext.isEmpty(fecha_public_oper_fin.getValue())){
							fecha_public_oper_fin.markInvalid('El valor de la Fecha de Public./Oper. final es requerido');
							fecha_public_oper_fin.focus();
							return;
						}
					}
					
					main.el.mask('Procesando Consulta...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{   		})
					});							
						
				}	
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',	
				formBind: true,
				handler: function(){
					window.location  = "15RepOpPymesDescAutomaticoExt.jsp"; 	
				
				}
			}
		]
	});
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 900,
		style: 'margin:0 auto;',
		items: [
			fp	,
			gridConsulta
		]
	});
		
	
	catalogoEpo.load();
	catConvenio.load();
	
});
	
	
	