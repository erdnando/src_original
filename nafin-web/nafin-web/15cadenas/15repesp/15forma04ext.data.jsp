<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.SimpleDateFormat,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.xls.*,
		com.netro.xlsx.*,
		com.netro.exception.*,
		com.netro.seguridad.*,
		com.netro.seguridadbean.*,
		com.netro.cadenas.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_comun.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null ? "": (String)request.getParameter("informacion");
String mes         = request.getParameter("mes")         == null ? "": (String)request.getParameter("mes");
String anio        = request.getParameter("anio")        == null ? "": (String)request.getParameter("anio");
String icProceso   = request.getParameter("ic_proceso")  == null ? "": (String)request.getParameter("ic_proceso");

HashMap    datos      = new HashMap();
JSONArray  registros  = new JSONArray();
JSONObject resultado  = new JSONObject();

String mensaje        = "";
String consulta       = "";
String nombreArchivo  = "";
String infoRegresar   = "";
boolean success       = true;

log.info("informacion: <<<<<"    + informacion    + ">>>>>");

if(informacion.equals("CATALOGO_MES")){

	datos = new HashMap(); datos.put("clave", "01"); datos.put("descripcion", "Enero"     ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "02"); datos.put("descripcion", "Febrero"   ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "03"); datos.put("descripcion", "Marzo"     ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "04"); datos.put("descripcion", "Abril"     ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "05"); datos.put("descripcion", "Mayo"      ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "06"); datos.put("descripcion", "Junio"     ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "07"); datos.put("descripcion", "Julio"     ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "08"); datos.put("descripcion", "Agosto"    ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "09"); datos.put("descripcion", "Septiembre"); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "10"); datos.put("descripcion", "Octubre"   ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "11"); datos.put("descripcion", "Noviembre" ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "12"); datos.put("descripcion", "Diciembre" ); registros.add(datos);
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else if(informacion.equals("CATALOGO_ANIO")){

	SimpleDateFormat formato = new SimpleDateFormat("yyyy");
	Calendar calendario = Calendar.getInstance();
	String sAnio = formato.format(calendario.getTime());
	int iAnio = Integer.parseInt(sAnio)+1;

	for (int i = 0; i < 3; i++) {
		datos = new HashMap();
		datos.put("clave",       Integer.toString(iAnio));
		datos.put("descripcion", Integer.toString(iAnio));
		registros.add(datos);
		iAnio -= 1;
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else if(informacion.equals("CARGA_ARCHIVO")){

	ParametrosRequest req       = null;
	String  itemArchivo         = "";
	String  error_tam           = "";
	String  rutaArchivoTemporal = "";
	String  extension           = "";
	int     tamanioMB           = 2;
	int     tamaniArch_1        = 1048576;
	int     tamanio             = 0;
	int     indice              = 0;
	boolean okFile              = false;

	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType                 );
	request.setAttribute("myContentType", myContentType   );

	try{

		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setRepository(new File(strDirectorioTemp));
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(tamanioMB * tamaniArch_1); //MB
		req = new ParametrosRequest(upload.parseRequest(request));

		FileItem fileItem = null;
		fileItem          = (FileItem)req.getFiles().get(0);
		itemArchivo       = (String)fileItem.getName();
		tamanio           = (int)fileItem.getSize();
		indice            = itemArchivo.lastIndexOf('.');
		extension         = itemArchivo.substring((indice+1), itemArchivo.length());

		if(tamanio < 5242880){

			nombreArchivo = Comunes.cadenaAleatoria(16)+ "." + extension;
			rutaArchivoTemporal = strDirectorioTemp + nombreArchivo;
			fileItem.write(new File(rutaArchivoTemporal));
			okFile = true;
			//System.out.println("strDirectorioTemp: " + strDirectorioTemp);
			//System.out.println("rutaArchivoTemporal: " + rutaArchivoTemporal);

		} else{
			okFile = false;
			mensaje = "El archivo excede el tamaño de 2 MB";
		}

	} catch(Exception e){
		okFile = false;
		log.debug("Error al cargar el archivo: " + e);
		mensaje = "Error al cargar el archivo: " + e;
		e.printStackTrace();
	}

	// Se procesan los datos del archivo
	try{

		if(okFile == true){

			InteligenciaComercial inteligenciaComercial = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);

			JSONArray listaSinError  = new JSONArray();
			JSONArray listaConError  = new JSONArray();
			List      lDatos         = new ArrayList();

			if("xls".equals(extension)){
				ComunesXLS xls  = new ComunesXLS();
				lDatos          = xls.leerExcel(rutaArchivoTemporal);
			} else if("xlsx".equals(extension)){
				ComunesXLSX xlsx = new ComunesXLSX();
				lDatos           = xlsx.leerExcel(rutaArchivoTemporal);
			}

			//System.out.println("Lista del archivo:" + lDatos.toString());

			Map    mResultado = inteligenciaComercial.procesarArchivo(lDatos, Integer.parseInt(anio), mes);
			List   lCorrectos = (List)mResultado.get("correctos");
			List   lErrores   = (List)mResultado.get("errores");
			String existeMes  = (String)mResultado.get("existeMes");
			icProceso         = (String)mResultado.get("proceso");

			// Armo los datos de los grids
			if(lCorrectos.size() > 0){
				for(int i = 0; i < lCorrectos.size(); i++){
					Map mDetOk = (Map)lCorrectos.get(i);
					listaSinError.add(mDetOk);
				}
				resultado.put("LISTA_SIN_ERROR", "" + listaSinError.toString());
				resultado.put("TOTAL_SIN_ERROR", "" + lCorrectos.size()       );
			} else{
				resultado.put("LISTA_SIN_ERROR", ""                           );
				resultado.put("TOTAL_SIN_ERROR", "0"                          );
			}
			if(lErrores.size() > 0){
				for(int i = 0; i < lErrores.size(); i++){
					Map mDetErr = (Map)lErrores.get(i);
					listaConError.add(mDetErr);
				}
				resultado.put("LISTA_CON_ERROR", "" + listaConError.toString());
				resultado.put("TOTAL_CON_ERROR", "" + lErrores.size()         );
			} else{
				resultado.put("LISTA_CON_ERROR", ""                           );
				resultado.put("TOTAL_CON_ERROR", "0"                          );
			}

			resultado.put("EXISTE_MES", existeMes);
			resultado.put("IC_PROCESO", icProceso);

		} else{
			success = false;
		}

	} catch(Exception e){
		success = false;
		log.error("Error al leer el archivo: " + e);
		mensaje = "Error al leer el archivo: " + e;
	}

	resultado.put("mensaje", mensaje             );
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("PREACUSE_CARGA_MASIVA")){

	InteligenciaComercial inteligenciaComercial = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);
	List lResultado = inteligenciaComercial.getMensajesCargadosTmp(icProceso);

	registros  = new JSONArray();
	datos      = new HashMap();
	datos.put("ETIQUETA",    "N&uacute;mero total de registros exitosos:");
	datos.put("DESCRIPCION", lResultado.size()                           );
	registros.add(datos);

	JSONArray listaResultados = new JSONArray();
	for(int i=0;i<lResultado.size();i++){
		Map mColumnas = (Map)lResultado.get(i);
		listaResultados.add(mColumnas);
	}

	resultado.put("success",        new Boolean(success)      );
	resultado.put("LISTA_RESUMEN",  registros.toString()      );
	resultado.put("LISTA_PREACUSE", listaResultados.toString());
	infoRegresar = resultado.toString();

} else if(informacion.equals("CARGA_MASIVA")){

	InteligenciaComercial inteligenciaComercial = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);
	List lResultado = inteligenciaComercial.procesarMensajesCargados(icProceso, anio, mes);

	registros  = new JSONArray();
	datos      = new HashMap();
	datos.put("ETIQUETA",    "N&uacute;mero total de registros exitosos:"                     );
	datos.put("DESCRIPCION", lResultado.size()                                                );
	registros.add(datos);
	datos      = new HashMap();
	datos.put("ETIQUETA",    "Fecha:"                                                         );
	datos.put("DESCRIPCION", (new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date())));
	registros.add(datos);

	JSONArray listaResultados = new JSONArray();
	for(int i=0;i<lResultado.size();i++){
		Map mColumnas = (Map)lResultado.get(i);
		listaResultados.add(mColumnas);
	}

	resultado.put("success",        new Boolean(success)      );
	resultado.put("LISTA_RESUMEN",  registros.toString()      );
	resultado.put("LISTA_PREACUSE", listaResultados.toString());
	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>