Ext.onReady(function(){

//------------------------------ Handlers ----------------------------
	// Realiza la consulta para llenar el grid
	function buscar(){

		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		// Valido la forma principal
		if(!this.up('form').getForm().isValid()){
			return;
		}
		// Realiza la consulta del grid
		main.el.mask('Procesando...', 'x-mask-loading');
		Ext.ComponentQuery.query('#gridConsulta')[0].hide();
		consultaData.load({
			params: Ext.apply({
				ic_mes:       forma.query('#ic_mes_id')[0].getValue(),
				ic_anio:      forma.query('#ic_anio_id')[0].getValue()
			})
		});

	}

	//Aqui se define el tipo de archivo y se deshabilitan los botones
	function reportePDF(){
		var gridConsulta = Ext.ComponentQuery.query('#gridConsulta')[0];
		var boton = gridConsulta.query('#btnReportePDF')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		generaReporte('PDF');
	}

	//Aqui se define el tipo de archivo y se deshabilitan los botones
	function reporteXLS(){
		var gridConsulta = Ext.ComponentQuery.query('#gridConsulta')[0];
		var boton = gridConsulta.query('#btnReporteXLS')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		generaReporte('CSV');
	}

	//Genera el archivo
	function generaReporte(tipo_archivo){
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		Ext.Ajax.request({
			url: '15forma02ext.data.jsp',
			params: Ext.apply({
				informacion:  'GENERA_ARCHIVO',
				ic_mes:       forma.query('#ic_mes_id')[0].getValue(),
				ic_anio:      forma.query('#ic_anio_id')[0].getValue(),
				tipo_archivo: tipo_archivo
			}),
			callback: procesaGeneraArchivo
		});
	}

//------------------------------ Callback ----------------------------
	// Proceso posterior a la consulta del grid consultaData
	var procesarConsultaData = function(store, arrRegistros, success, opts){
		main.el.unmask();
		var gridConsulta = Ext.ComponentQuery.query('#gridConsulta')[0];
		if (arrRegistros != null){
			gridConsulta.show();
			if(store.getTotalCount() > 0){
				gridConsulta.query('#btnReporteXLS')[0].enable();
				gridConsulta.query('#btnReportePDF')[0].enable();
			} else{
				gridConsulta.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridConsulta.query('#btnReporteXLS')[0].disable();
				gridConsulta.query('#btnReportePDF')[0].disable();
			}
		}
	}

	// Handler que procesa respuesta creacion archivo
	function procesaGeneraArchivo(opts, success, response){

		var gridConsulta = Ext.ComponentQuery.query('#gridConsulta')[0];
		var boton1 = gridConsulta.query('#btnReportePDF')[0];
		var boton2 = gridConsulta.query('#btnReporteXLS')[0];

		// PARSEAR RESPUESTA DEL SERVIDOR
		var resp = null;
		try {
			resp = Ext.JSON.decode(response.responseText);
		} catch(error) {
			NE.util.mostrarErrorResponse4(
				{ status: -1 }, // Para que no asuma que es un est�tus devuelto por el servidor
				error.name + " - " + error.message
			);
			return;
		}

		// REALIZAR LA DESCARGA AUTOM�TICA DEL ARCHIVO
		if ( success == true && resp.success == true ) {

			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo';
			forma.method = 'post';
			forma.target = '_self';

			// Suprimir el Context root del url del archivo
			var urlArchivo = resp.urlArchivo;
			nombreArchivo  = urlArchivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');

			// Insertar Nombre Archivo
			var inputNombreArchivo = Ext.DomHelper.insertFirst(
				forma,
				{
					tag:   'input',
					type:  'hidden',
					id:    'nombreArchivo',
					name:  'nombreArchivo',
					value: nombreArchivo
				},
				true
			);
			// Solicitar Archivo Al Servidor
			forma.submit();
			// Remover nodo agregado
			inputNombreArchivo.remove();

		} else {
				NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
		boton1.enable();
		boton2.enable();
		boton1.setIconCls('icoPdf');
		boton2.setIconCls('icoXls');

	}

//------------------------------ Stores ------------------------------
	// Se crea el MODEL para los combos
	Ext.define('ModelCatologo',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);


	// Se crea el MODEL para el grid 'consultaData'
	Ext.define('ListaData',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'MES_ANIO'                   },
			{name: 'PYMES_UNICAS_AFILIADAS'     },
			{name: 'PYMES_CON_PUBLICACION'      },
			{name: 'PORC_PYMES_CON_PUBLICACION' },
			{name: 'MONTO_PUBLICADO'            },
			{name: 'DOCUMENTOS_PUBLICADOS'      },
			{name: 'MONTO_OPERADO'              },
			{name: 'DOCUMENTOS_OPERADOS'        }
		]
	});

	// Se crea el STORE para el cat�logo 'A�o'
	var catalogoAnio = Ext.create('Ext.data.Store',{
		model: 'ModelCatologo',
		proxy: {
			type: 'ajax',
			url: '15forma02ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_ANIO'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: true
	});

	// Se crea el DATA STORE para el cat�logo 'Mes' 
	var catalogoMes = Ext.create('Ext.data.Store',{
		fields: ['clave', 'descripcion'],
		data : [
			{'clave': '01', 'descripcion': 'Enero'     },
			{'clave': '02', 'descripcion': 'Febrero'   },
			{'clave': '03', 'descripcion': 'Marzo'     },
			{'clave': '04', 'descripcion': 'Abril'     },
			{'clave': '05', 'descripcion': 'Mayo'      },
			{'clave': '06', 'descripcion': 'Junio'     },
			{'clave': '07', 'descripcion': 'Julio'     },
			{'clave': '08', 'descripcion': 'Agosto'    },
			{'clave': '09', 'descripcion': 'Septiembre'},
			{'clave': '10', 'descripcion': 'Octubre'   },
			{'clave': '11', 'descripcion': 'Noviembre' },
			{'clave': '12', 'descripcion': 'Diciembre' }
		]
	});

	// Se crea el STORE del grid 'Consulta Data'
	var consultaData = Ext.create('Ext.data.Store',{
		storeId:    'consultaData',
		model:      'ListaData',
		proxy: {
			type:    'ajax',
			url:     '15forma02ext.data.jsp',
			reader:  {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion:  'CONSULTA_DATA'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultaData
		}
	});

//------------------------------ Componentes -------------------------
	// Se crea el grid 'ConsultaData'
	var gridConsulta = Ext.create('Ext.grid.Panel',{
		//height:          430,
		width:           '95%',
		xtype:           'grouped-text-grid',
		itemId:          'gridConsulta',
		style:           'margin:0 auto;',
		bodyStyle:       'padding: 6px',
		title:           'Por Mes',
		store:           Ext.data.StoreManager.lookup('consultaData'),
		scroll:          true,
		border:          true,
		frame:           true,
		hidden:          true,
		autoHeight:      true,
		columns: [{
			width:        110,
			dataIndex:    'MES_ANIO',
			text:         'Mes / A�o',
			align:        'center',
			sortable:     true,
			resizable:    true
		},{
			width:        130,
			dataIndex:    'PYMES_UNICAS_AFILIADAS',
			text:         'PYMES �nicas Afiliadas',
			align:        'center',
			sortable:     true,
			resizable:    true
		},{
			width:        80,
			dataIndex:    'PYMES_CON_PUBLICACION',
			text:         'PYMES con <br>Publicaci�n',
			align:        'center',
			sortable:     true,
			resizable:    true
		},{
			width:        120,
			dataIndex:    'PORC_PYMES_CON_PUBLICACION',
			text:         'Porcentaje PYMES <br> con Publicaci�n',
			align:        'center',
			sortable:     true,
			resizable:    true
		},{
			width:        105,
			dataIndex:    'MONTO_PUBLICADO',
			text:         'Monto Publicado',
			align:        'center',
			sortable:     true,
			resizable:    true
		},{
			width:        110,
			dataIndex:    'DOCUMENTOS_PUBLICADOS',
			text:         'No. de Documentos <br> Publicados',
			align:        'center',
			sortable:     true,
			resizable:    true
		},{
			width:        105,
			dataIndex:    'MONTO_OPERADO',
			text:         'Monto Operado',
			align:        'center',
			sortable:     true,
			resizable:    true
		},{
			width:        110,
			dataIndex:    'DOCUMENTOS_OPERADOS',
			text:         'No. de Documentos <br> Operados',
			align:        'center',
			sortable:     true,
			resizable:    true
		}],
		bbar: ['->','-',
		{
				xtype:      'button',
				itemId:     'btnReporteXLS',
				iconCls:    'icoXls', 
				text:       'Generar Archivo',
				tooltip:    'Generar Archivo en formato CSV',
				handler:    reporteXLS
			},'-',{
				xtype:      'button',
				itemId:     'btnReportePDF',
				iconCls:    'icoPdf', 
				text:       'Imprimir PDF',
				tooltip:    'Generar Archivo en formato PDF',
				handler:    reportePDF
			}
		]
	});

	// Se crea el form principal
	var formaPrincipal = Ext.create( 'Ext.form.Panel',{
		width:               '50%',
		itemId:              'formaPrincipal',
		title:               'Operaci�n de PYMES en Cadenas',
		bodyPadding:         '12 6 12 6',
		style:               'margin: 0px auto 0px auto;',
		frame:               true,
		border:              true,
		fieldDefaults: {
			msgTarget:        'side'
		},
		items: [{
			xtype:            'container',
			layout:           'hbox',
			anchor:           '95%',
			margin:           '0 0 5 0',
			items:[{
				width:         150,
				xtype:         'displayfield',
				hideLabel:     true
			},{
				width:         160,
				xtype:         'radiogroup',
				fieldLabel:    'Auto Layout',
				cls:           'x-check-group-alt',
				hideLabel:     true,
				items: [{
					boxLabel:   'Por EPO',
					name:       'opcion',
					inputValue: 1
				},{
					boxLabel:   'Por Mes',
					name:       'opcion',
					inputValue: 2,
					checked:    true
				}],
				listeners: {
					change: function (field, newValue, oldValue) {
						switch (newValue['opcion']){
							case 1:
								window.location.href='15forma01ext.jsp';
								break;
							default:
								break;
						}
					}
				}
			}]
		},{
			labelWidth:       100,
			anchor:           '95%',
			xtype:            'combobox',
			itemId:           'ic_mes_id',
			name:             'ic_mes',
			hiddenName:       'ic_mes',
			fieldLabel:       'Mes',
			emptyText:        'Seleccione...',
			displayField:     'descripcion',
			valueField:       'clave',
			queryMode:        'local',
			triggerAction:    'all',
			listClass:        'x-combo-list-small',
			typeAhead:        true,
			selectOnTab:      true,
			lazyRender:       true,
			forceSelection:   true,
			editable:         true,
			allowBlank:       false,
			store:            catalogoMes
		},{
			labelWidth:       100,
			anchor:           '95%',
			xtype:            'combobox',
			itemId:           'ic_anio_id',
			name:             'ic_anio',
			hiddenName:       'ic_anio',
			fieldLabel:       'A�o',
			emptyText:        'Seleccione...',
			displayField:     'descripcion',
			valueField:       'clave',
			queryMode:        'local',
			triggerAction:    'all',
			listClass:        'x-combo-list-small',
			typeAhead:        true,
			selectOnTab:      true,
			lazyRender:       true,
			forceSelection:   true,
			editable:         true,
			allowBlank:       false,
			store:            catalogoAnio
		}],
		buttons: [{
			text:             'Generar',
			itemId:           'btnGenerar',
			iconCls:          'icoBuscar',
			handler:          buscar
		}]
	});

	// Contenedor principal
	var main = Ext.create('Ext.container.Container',{
		width:      949,
		minHeight:  650,
		autoHeight: true,
		id:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		style:      'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			NE.util.getEspaciador(10),
			gridConsulta
		]
	});

});