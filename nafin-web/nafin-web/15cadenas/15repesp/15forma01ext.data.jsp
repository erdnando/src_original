<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		com.netro.seguridadbean.*,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion    = request.getParameter("informacion")      == null ? "": (String)request.getParameter("informacion");
String icEpo          = request.getParameter("ic_epo")           == null ? "": (String)request.getParameter("ic_epo");
String fechaPublicIni = request.getParameter("fecha_public_ini") == null ? "": (String)request.getParameter("fecha_public_ini");
String fechaPublicFin = request.getParameter("fecha_public_fin") == null ? "": (String)request.getParameter("fecha_public_fin");
String tipoArchivo    = request.getParameter("tipo_archivo")     == null ? "": (String)request.getParameter("tipo_archivo");
String bancoFondeo    = "1";

HashMap datosGrid = new HashMap();
JSONArray regGrid = new JSONArray();

String mensaje        = "";
String consulta       = "";
String nombreArchivo  = "";
String infoRegresar   = "";
JSONObject resultado  = new JSONObject();
boolean success       = true;
int start = 0;
int limit = 0;

if(!fechaPublicIni.equals("") && fechaPublicIni.length() > 10){
	fechaPublicIni = fechaPublicIni.substring(8, 10) + "/" + fechaPublicIni.substring(5, 7) + "/" + fechaPublicIni.substring(0, 4);
}
if(!fechaPublicFin.equals("") && fechaPublicFin.length() > 10){
	fechaPublicFin = fechaPublicFin.substring(8, 10) + "/" + fechaPublicFin.substring(5, 7) + "/" + fechaPublicFin.substring(0, 4);
}

log.info("informacion: <<<<<"    + informacion    + ">>>>>");

if(informacion.equals("CATALOGO_EPO")){

	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setClaveBancoFondeo(bancoFondeo);
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("CONSULTA_DATA") || informacion.equals("GENERA_ARCHIVO")){

	List operacionesCadenasEpo = new ArrayList();
	InteligenciaComercial inteligenciaComercial = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);
	String cadenaDatosEntrada = bancoFondeo + icEpo + fechaPublicIni + fechaPublicFin;
	List reporteOperacionesCadenas = inteligenciaComercial.reporteOperacionesEnCadenas(Integer.toString(cadenaDatosEntrada.hashCode()), bancoFondeo, icEpo, fechaPublicIni, fechaPublicFin);
	if(!reporteOperacionesCadenas.isEmpty()){
		for(int i = 0; i < reporteOperacionesCadenas.size(); i++){
			operacionesCadenasEpo = (List)reporteOperacionesCadenas.get(i);
			datosGrid = new HashMap();
			datosGrid.put("PYMES",                                operacionesCadenasEpo.get(0)                                  );
			datosGrid.put("EPO",                                  operacionesCadenasEpo.get(1)                                  );
			datosGrid.put("TOTAL_PYMES_AFILIADAS_HABILITADAS",    operacionesCadenasEpo.get(2)                                  );
			datosGrid.put("CANTIDAD_PUCP",                        operacionesCadenasEpo.get(3)                                  );//PYMES Únicas Con Publicación
			datosGrid.put("MONTO_PUCP",                           "$" + Comunes.formatoDecimal(operacionesCadenasEpo.get(4), 2) );//PYMES Únicas Con Publicación
			datosGrid.put("PYMES_UNICAS_SIN_OPERACION",           operacionesCadenasEpo.get(5)                                  );
			datosGrid.put("CANTIDAD_PUCO",                        operacionesCadenasEpo.get(6)                                  );//PYMES Únicas Con Operación
			datosGrid.put("MONTO_PUCO",                           "$" + Comunes.formatoDecimal(operacionesCadenasEpo.get(7), 2) );//PYMES Únicas Con Operación
			datosGrid.put("TOTAL_PYMES_AFILIADAS_NO_HABILITADAS", operacionesCadenasEpo.get(8)                                  );
			datosGrid.put("CANTIDAD_PUCP_1",                      operacionesCadenasEpo.get(9)                                  );
			datosGrid.put("MONTO_PUCP_1",                         "$" + Comunes.formatoDecimal(operacionesCadenasEpo.get(10), 2));
			datosGrid.put("PYMES_UNICAS_SIN_PUBLICACION",         operacionesCadenasEpo.get(11)                                 );
			regGrid.add(datosGrid);
		}
	}

	if(informacion.equals("CONSULTA_DATA")){

		try{
			if(!reporteOperacionesCadenas.isEmpty()){
				consulta = "{\"success\": "+new Boolean(success)+", \"total\": \"" + regGrid.size() + "\", \"registros\": " + regGrid.toString() +"}";
			} else{
				consulta = "{\"success\": "+new Boolean(success)+", \"total\": 0, \"registros\": []}";
			}
			resultado = JSONObject.fromObject(consulta);
		} catch(Exception e) {
			success = false;
			throw new AppException("Error al realizar la consulta ", e);
		}

	} else if(informacion.equals("GENERA_ARCHIVO")){

		try{
			OperacionPymesCadenas archivo = new OperacionPymesCadenas();
			archivo.setArrayRegistros(regGrid);
			archivo.setTipoReporte("POR_EPO");
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(archivo);
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Exception e) {
			success = false;
			throw new AppException("Error al generar el archivo ", e);
		}

	}

	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>