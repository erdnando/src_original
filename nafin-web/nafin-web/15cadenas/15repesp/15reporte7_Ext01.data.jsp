<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*, 
		netropology.utilerias.usuarios.*, 
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar="", consulta="",consultaGrid="", mensaje ="";
	System.out.println("informacion *** "+informacion);
	String respuesta="";
	
	JSONObject jsonObj = new JSONObject();
	HashMap datos = new HashMap();
	JSONArray registros1 = new JSONArray();
	JSONObject jsonObjG = new JSONObject();
	
	Calendar cal = Calendar.getInstance();
	int iAnioActual = cal.get(Calendar.YEAR);
	int iMesActual = cal.get(Calendar.MONTH);
	int iDiaActual = cal.get(Calendar.DAY_OF_MONTH);
	int iAnioInicial = cal.get(Calendar.YEAR) - 1;
	int iMesInicial = cal.get(Calendar.MONTH);
	int iDiaInicial = cal.get(Calendar.DAY_OF_MONTH);
	
	if(informacion.equals("Consulta_Mes")){
		JSONArray reg = new JSONArray();
		
		datos = new HashMap();
		datos.put("clave","1");
		datos.put("descripcion","Enero");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","2");
		datos.put("descripcion","Febrero");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","3");
		datos.put("descripcion","Marzo");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","4");
		datos.put("descripcion","Abril");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","5");
		datos.put("descripcion","Mayo");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","6");
		datos.put("descripcion","Junio");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","7");
		datos.put("descripcion","Julio");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","8");
		datos.put("descripcion","Agosto");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","9");
		datos.put("descripcion","Septiembre");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","10");
		datos.put("descripcion","Octubre");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","11");
		datos.put("descripcion","Noviembre");
		reg.add(datos);
		datos = new HashMap();
		datos.put("clave","12");
		datos.put("descripcion","Diciembre");
		reg.add(datos);
		
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("registros",reg);
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("Consulta_Anio")){
		JSONArray reg = new JSONArray();
		
		for (int anio = 2007; anio <= iAnioActual; anio++) {
		
			datos = new HashMap();
			datos.put("clave",Integer.toString(anio));
			datos.put("descripcion",Integer.toString(anio));
			reg.add(datos);
		}
		
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("registros",reg);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("Consulta_Epo")){
		Registros reg = null;
	
		String eposPEF = (request.getParameter("eposPEF")!=null)?request.getParameter("eposPEF"):"";
		String eposGobMun = (request.getParameter("eposGobMun")!=null)?request.getParameter("eposGobMun"):"";
		String eposPrivadas = (request.getParameter("eposPrivadas")!=null)?request.getParameter("eposPrivadas"):"";
		String eposNC = (request.getParameter("eposNC")!=null)?request.getParameter("eposNC"):"";
		String eposCredi = (request.getParameter("eposCredi")!=null)?request.getParameter("eposCredi"):"";
		String sNoEposSelec = (request.getParameter("sNoEposSelec")!=null)?request.getParameter("sNoEposSelec"):"";
		
		String filtroEpo   = request.getParameter("filtroEpo")   == null?"":(String)request.getParameter("filtroEpo");
		
		if(eposPEF.equals("true")){
			eposPEF ="S";
		}else if(eposPEF.equals("false")||eposPEF.equals("")){
			eposPEF ="N";
		}
		if(eposGobMun.equals("true")){
			eposGobMun ="S";
		}else if(eposGobMun.equals("false")||eposGobMun.equals("")){
			eposGobMun ="N";
		}
		if(eposPrivadas.equals("true")){
			eposPrivadas ="S";
		}else if(eposPrivadas.equals("false")||eposPrivadas.equals("")){
			eposPrivadas ="N";
		}
		if(eposNC.equals("true")){
			eposNC ="S";
		}else if(eposNC.equals("false")||eposNC.equals("")){
			eposNC ="N";
		}
		if(eposCredi.equals("true")){
			eposCredi ="S";
		}else if(eposCredi.equals("false")||eposCredi.equals("")){
			eposCredi ="N";
		}
		
		try {
				ConsClientesSusCredi clase = new ConsClientesSusCredi();
				clase.setEposPEF(eposPEF);
				clase.setEposGobMun(eposGobMun);
				clase.setEposPrivadas(eposPrivadas);
				clase.setEposNC(eposNC);	
				clase.setEposCredi(eposCredi);
				clase.setSNoEposSelec(sNoEposSelec);
				clase.setFiltroEpo(filtroEpo);
		
				reg	=	clase.getConsultaDataEpo();
				consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
				jsonObj = JSONObject.fromObject(consulta);
				infoRegresar = jsonObj.toString();
				System.out.println("infoRegresar *** "+infoRegresar);
			} catch(Exception e) {
				throw new AppException("Error al obtener los datos", e);
			}
	}else if(informacion.equals("total_pymes")){
		
		Registros reg = null;
		String sNoEpo = (request.getParameter("sNoEpo")!=null)?request.getParameter("sNoEpo"):"";
		try {
				ConsClientesSusCredi clase = new ConsClientesSusCredi();
				
				clase.setSNoEposSelec(sNoEpo);
				String resultado = clase.getTotalPymes();
				
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("resultado",resultado);
				infoRegresar = jsonObj.toString();
			} catch(Exception e) {
				throw new AppException("Error al obtener los datos", e);
			}
	}else if(informacion.equals("Generar_archivo")){
		Registros reg = null;
		jsonObj = new JSONObject();
		String fase_proc_archi = (String)request.getParameter("fase_proceso");
		
		try {
			if("1".equals(fase_proc_archi)){
				String mesInicial = (request.getParameter("mesInicial")!=null)?request.getParameter("mesInicial"):"";
				String anioInicial = (request.getParameter("anioInicial")!=null)?request.getParameter("anioInicial"):"";
				String mesFinal = (request.getParameter("mesFinal")!=null)?request.getParameter("mesFinal"):"";
				String anioFinal = (request.getParameter("anioFinal")!=null)?request.getParameter("anioFinal"):"";
				String sNoEpos = (request.getParameter("sNoEpos")!=null)?request.getParameter("sNoEpos"):"";
				String sPeriodo1 = (request.getParameter("sPeriodo1")!=null)?request.getParameter("sPeriodo1"):"";
				String sPeriodo2 = (request.getParameter("sPeriodo1")!=null)?request.getParameter("sPeriodo2"):"";
				String sPeriodo3 = (request.getParameter("sPeriodo1")!=null)?request.getParameter("sPeriodo3"):"";
				
				//Eliminar lo que hay en session para no mostrar el Archivo descargado anteriomente
				session.removeAttribute("GeneraArchivoConsultaThread"); 
				//Crear una nueva session para mostrar el nuevo ZIP
				GeneraArchivoConsultaThread generarArchivo = new GeneraArchivoConsultaThread();
				
				generarArchivo.setSNoEposSelec(sNoEpos);
				generarArchivo.setMesInicial(mesInicial);
				generarArchivo.setAnioInicial(anioInicial);
				generarArchivo.setMesFinal(mesFinal);
				generarArchivo.setAnioFinal(anioFinal);
				generarArchivo.setSPeriodo1(sPeriodo1);
				generarArchivo.setSPeriodo2(sPeriodo2);
				generarArchivo.setSPeriodo3(sPeriodo3);
				generarArchivo.setRutaFisica(strDirectorioTemp);
				generarArchivo.setRunning(true);
				new Thread(generarArchivo).start();
				session.setAttribute("GeneraArchivoConsultaThread", generarArchivo);
				jsonObj.put("PROCESO_FASE","2");
			}else if("2".equals(fase_proc_archi)){
				mensaje = "";
				//GeneraArchivoConsultaThread generarArchivo = new GeneraArchivoConsultaThread();
				GeneraArchivoConsultaThread generarArchivo = (GeneraArchivoConsultaThread)session.getAttribute("GeneraArchivoConsultaThread");
				
				if(generarArchivo.hasError()){
					mensaje="Error inesperado al generar archivo";
					jsonObj.put("mensaje", mensaje);
					jsonObj.put("PROCESO_FASE", "3");
				} 
				if(generarArchivo.getNombreArchivo()==null && generarArchivo.isRunning()){
					mensaje="Generando Archivo";	
					jsonObj.put("mensaje", mensaje);
					jsonObj.put("PROCESO_FASE", "2");
				}else if(!generarArchivo.isRunning() ){
					String archivo =strDirecVirtualTemp+generarArchivo.getNombreArchivo();		
					jsonObj.put("PROCESO_FASE", "4");
					jsonObj.put("mensaje", mensaje);
					jsonObj.put("urlArchivo", archivo);
				}	
			}
			
		    jsonObj.put("success", new Boolean(true));
			 infoRegresar = jsonObj.toString();
		} catch(Exception e) {
			throw new AppException("Error al Generar el archivo", e);
		}
	}
	
	%>	

<%= infoRegresar %>


