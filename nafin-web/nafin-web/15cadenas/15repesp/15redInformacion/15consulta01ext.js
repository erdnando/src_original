Ext.onReady(function(){

//------------------------------ Handlers ----------------------------
	// Se define la base de datos a consultar
	function seleccionaOpcion(){

		// Valido la forma principal
		if(!this.up('form').getForm().isValid()){
			return;
		}

		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var campo = forma.query('#opcion_id')[0].getValue();
		forma.hide();

		if(campo.opcion == '1'){

			forma = Ext.ComponentQuery.query('#formaCadenas')[0];
			forma.show();
			catalogoMesIni.load();
			catalogoAnioIni.load();
			catalogoMesFin.load();
			catalogoAnioFin.load();
			catalogoDependencias.load();

		} else if(campo.opcion == '2'){

			main.el.mask('Procesando...', 'x-mask-loading');
			window.location = '15consulta02ext.jsp'

		} else if(campo.opcion == '3'){

			main.el.mask('Procesando...', 'x-mask-loading');
			window.location = '15consulta03ext.jsp'

		}

	}

	// Consulta CADENAS
	function buscarCadenas(){
		// Valido el form
		if(!this.up('form').getForm().isValid()){
			return;
		}
		// Valido que el periodo inicial sea menor al periodo final
		var forma    = Ext.ComponentQuery.query('#formaCadenas')[0];
		var fechaIni = new Date(forma.query('#anio_ini_id')[0].getValue(), forma.query('#mes_ini_id')[0].getValue(), 1);
		var fechaFin = new Date(forma.query('#anio_fin_id')[0].getValue(), forma.query('#mes_fin_id')[0].getValue(), 1);
		if(fechaIni > fechaFin){
			Ext.MessageBox.alert('Mensaje', 'El periodo inicial debe ser menor que el periodo final.');
			return;
		}
		// Si todo es correcto se genera el archivo
		main.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '15consulta01ext.data.jsp',
			params: {
				informacion:      'CONSULTA_CADENAS',
				mes_ini:          forma.query('#mes_ini_id')[0].getValue(),
				anio_ini:         forma.query('#anio_ini_id')[0].getValue(),
				mes_fin:          forma.query('#mes_fin_id')[0].getValue(),
				anio_fin:         forma.query('#anio_fin_id')[0].getValue(),
				dependencias:     forma.query('#dependencias_id')[0].getValue()
			},
			callback: procesaGeneraArchivo
		});
	}

	// Regresa a la forma principal
	function regresar(){
		main.el.mask('Procesando...', 'x-mask-loading');
		window.location  = '15consulta01ext.jsp?idMenu=15REDCONSINF'
	}

//------------------------------ Callback ----------------------------
	// Lleno el catalogo con el valor por default
	var procesarCatalogoMesIni = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma       = Ext.ComponentQuery.query('#formaCadenas')[0];
			var campo       = forma.query('#mes_ini_id')[0];
			var store_combo = campo.getStore();
			var valor       = store_combo.getRange()[0].data.clave;
			campo.setValue(valor);
		}
	}
	// Lleno el catalogo con el valor por default
	var procesarCatalogoAnioIni = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma       = Ext.ComponentQuery.query('#formaCadenas')[0];
			var campo       = forma.query('#anio_ini_id')[0];
			var store_combo = campo.getStore();
			var valor       = store_combo.getRange()[0].data.clave;
			campo.setValue(valor);
		}
	}

	// Lleno el catalogo con el valor por default
	var procesarCatalogoMesFin = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma       = Ext.ComponentQuery.query('#formaCadenas')[0];
			var campo       = forma.query('#mes_fin_id')[0];
			var store_combo = campo.getStore();
			var valor       = store_combo.getRange()[0].data.clave;
			campo.setValue(valor);
		}
	}

	// Lleno el catalogo con el valor por default
	var procesarCatalogoAnioFin = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma       = Ext.ComponentQuery.query('#formaCadenas')[0];
			var campo       = forma.query('#anio_fin_id')[0];
			var store_combo = campo.getStore();
			var valor       = store_combo.getRange()[0].data.clave;
			campo.setValue(valor);
		}
	}

	// Handler que procesa respuesta creacion archivo
	function procesaGeneraArchivo(opts, success, response){

		main.el.unmask();

		// PARSEAR RESPUESTA DEL SERVIDOR
		var resp = null;
		try {
			resp = Ext.JSON.decode(response.responseText);
		} catch(error) {
			NE.util.mostrarErrorResponse4(
				{ status: -1 }, // Para que no asuma que es un est�tus devuelto por el servidor
				error.name + " - " + error.message
			);
			return;
		}

		// REALIZAR LA DESCARGA AUTOM�TICA DEL ARCHIVO
		if ( success == true && resp.success == true ) {

			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo';
			forma.method = 'post';
			forma.target = '_self';

			// Suprimir el Context root del url del archivo
			var urlArchivo = resp.urlArchivo;
			nombreArchivo  = urlArchivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');

			// Insertar Nombre Archivo
			var inputNombreArchivo = Ext.DomHelper.insertFirst(
				forma,
				{
					tag:   'input',
					type:  'hidden',
					id:    'nombreArchivo',
					name:  'nombreArchivo',
					value: nombreArchivo
				},
				true
			);
			// Solicitar Archivo Al Servidor
			forma.submit();
			// Remover nodo agregado
			inputNombreArchivo.remove();

		} else {
				NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}

	}

//------------------------------ Stores ------------------------------
	// Se crea el MODEL para los combos
	Ext.define('ModelCatologo',{
		extend: 'Ext.data.Model',
		fields: [{ name:'clave', type: 'string'}, { name:'descripcion', type:'string'}]
	});

	// Se crea el STORE para el cat�logo 'Mes'
	var catalogoMesIni = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15consulta01ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_MES'
			}
		},
		autoLoad:          false,
		listeners: {
			load:           procesarCatalogoMesIni,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'A�o'
	var catalogoAnioIni = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15consulta01ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_ANIO'
			}
		},
		autoLoad:          false,
		listeners: {
			load:           procesarCatalogoAnioIni,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'Mes'
	var catalogoMesFin = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15consulta01ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_MES'
			}
		},
		autoLoad:          false,
		listeners: {
			load:           procesarCatalogoMesFin,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'A�o'
	var catalogoAnioFin = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15consulta01ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_ANIO'
			}
		},
		autoLoad:          false,
		listeners: {
			load:           procesarCatalogoAnioFin,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'Dependencias'
	var catalogoDependencias = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15consulta01ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_DEPENDENCIAS'
			}
		},
		autoLoad:          false,
		listeners: {
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

//------------------------------ Componentes -------------------------
	// Se crea el form CADENAS
	var formaCadenas = Ext.create( 'Ext.form.Panel',{
		width:                  '70%',
		itemId:                 'formaCadenas',
		title:                  'CADENAS',
		bodyPadding:            '12 6 12 6',
		style:                  'margin: 0px auto 0px auto;',
		frame:                  true,
		border:                 true,
		hidden:                 true,
		fieldDefaults: {
			msgTarget:           'side'
		},
		items: [{
			width:               '90%',
			xtype:               'displayfield',
			value:               '<div align="center"><b> Seleccione el periodo y la clasificaci�n de Dependencia a consultar </b></div>',
			hideLabel:           true
		},{
			xtype:               'container',
			layout:              'hbox',
			anchor:              '95%',
			margin:              '0 0 5 0',
			items:[{
				width:            50,
				xtype:            'displayfield',
				value:            '&nbsp',
				hideLabel:        true
			},{
				width:            170,
				labelWidth:       65,
				xtype:            'combobox',
				itemId:           'mes_ini_id',
				name:             'mes_ini',
				hiddenName:       'mes_ini',
				fieldLabel:       'Periodo de',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoMesIni
			},{
				width:            100,
				xtype:            'combobox',
				itemId:           'anio_ini_id',
				name:             'anio_ini',
				hiddenName:       'anio_ini',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				hideLabel:        true,
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoAnioIni
			},{
				width:            120,
				labelWidth:       15,
				xtype:            'combobox',
				itemId:           'mes_fin_id',
				name:             'mes_fin',
				hiddenName:       'mes_fin',
				fieldLabel:       'a',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoMesFin
			},{
				width:            100,
				xtype:            'combobox',
				itemId:           'anio_fin_id',
				name:             'anio_fin',
				hiddenName:       'anio_fin',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				hideLabel:        true,
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoAnioFin
			}]
		},{
			width:               600,
			labelWidth:          165,
			xtype:               'combobox',
			itemId:              'dependencias_id',
			name:                'dependencias',
			hiddenName:          'dependencias',
			fieldLabel:          'Clasificaci�n de Dependencias',
			emptyText:           'Seleccione...',
			displayField:        'descripcion',
			valueField:          'clave',
			queryMode:           'local',
			triggerAction:       'all',
			listClass:           'x-combo-list-small',
			margins:             '0 20 0 0',
			typeAhead:           true,
			selectOnTab:         true,
			lazyRender:          true,
			forceSelection:      true,
			editable:            true,
			allowBlank:          false,
			store:               catalogoDependencias
		}],
		buttons: [{
			text:                'Consultar',
			itemId:              'btnBuscar',
			iconCls:             'icoXls',
			handler:             buscarCadenas
		},{
			text:                'Regresar',
			itemId:              'btnRegresar',
			iconCls:             'icoRegresar',
			handler:             regresar
		}]
	});

	// Se crea el form principal
	var formaPrincipal = Ext.create( 'Ext.form.Panel',{
		width:                  '50%',
		itemId:                 'formaPrincipal',
		title:                  'Selecciona la base de datos a consultar',
		bodyPadding:            '12 6 12 6',
		style:                  'margin: 0px auto 0px auto;',
		frame:                  true,
		border:                 true,
		fieldDefaults: {
			msgTarget:           'side'
		},
		items: [{
			xtype:               'container',
			layout:              'hbox',
			anchor:              '95%',
			margin:              '0 0 5 0',
			items:[{
				width:            80,
				xtype:            'displayfield',
				hideLabel:        true
			},{
				width:            300,
				xtype:            'radiogroup',
				itemId:           'opcion_id',
				fieldLabel:       'Auto Layout',
				cls:              'x-check-group-alt',
				hideLabel:        true,
				allowBlank:       false,
				items: [
					{  boxLabel:'CADENAS',    name:'opcion',  inputValue:1  },
					{  boxLabel:'COMPRANET',  name:'opcion',  inputValue:2  },
					{  boxLabel:'SIAG',       name:'opcion',  inputValue:3  }
				]
			}]
		}],
		buttons: [{
			text:                'Aceptar',
			itemId:              'btnAceptar',
			iconCls:             'icoAceptar',
			handler:             seleccionaOpcion
		}]
	});

	// Contenedor principal
	var main = Ext.create('Ext.container.Container',{
		width:                  949,
		minHeight:              650,
		autoHeight:             true,
		id:                     'contenedorPrincipal',
		renderTo:               'areaContenido',
		style:                  'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			formaCadenas
		]
	});

});