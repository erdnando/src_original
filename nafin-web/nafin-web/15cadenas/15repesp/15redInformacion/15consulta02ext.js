Ext.onReady(function(){

//------------------------------ Handlers ----------------------------
	function consultaEpo(){
		catalogoEpo.load({
			params: Ext.apply({
				mes_ini:     Ext.ComponentQuery.query('#formaCompranet')[0].query('#mes_ini_id')[0].getValue(),
				anio_ini:    Ext.ComponentQuery.query('#formaCompranet')[0].query('#anio_ini_id')[0].getValue(),
				mes_fin:     Ext.ComponentQuery.query('#formaCompranet')[0].query('#mes_fin_id')[0].getValue(),
				anio_fin:    Ext.ComponentQuery.query('#formaCompranet')[0].query('#anio_fin_id')[0].getValue(),
				federales:   Ext.ComponentQuery.query('#formaCompranet')[0].query('#federales_id')[0].getValue(),
				otros:       Ext.ComponentQuery.query('#formaCompranet')[0].query('#otros_id')[0].getValue(),
				epo:         ''
			})
		});
	}

	// Regresa a la forma principal
	function regresar(){
		main.el.mask('Procesando...', 'x-mask-loading');
		window.location = '15consulta01ext.jsp?idMenu=15REDCONSINF'
	}

	// Consulta COMPRANET
	function buscar(){
		// Valido el form
		if(!this.up('form').getForm().isValid()){
			return;
		}
		// Valido que el periodo inicial sea menor al periodo final
		var forma    = Ext.ComponentQuery.query('#formaCompranet')[0];
		var fechaIni = new Date(forma.query('#anio_ini_id')[0].getValue(), forma.query('#mes_ini_id')[0].getValue(), 1);
		var fechaFin = new Date(forma.query('#anio_fin_id')[0].getValue(), forma.query('#mes_fin_id')[0].getValue(), 1);
		if(fechaIni > fechaFin){
			Ext.MessageBox.alert('Mensaje', 'El periodo inicial debe ser menor que el periodo final.');
			return;
		}

		var cadena = forma.query('#epo_id')[0].getValue();

		// Valido el campo EPO, ya que el mensaje original lo muestra en ingl�s
		if(cadena == null || cadena == ''){
			forma.query('#epo_id')[0].markInvalid('Seleccione al menos una EPO para realizar la consulta.');
			return;
		}

		var str_epo = '';
		for(i=0; i< cadena.length; i++){
			if(i > 0)
				str_epo += ',';
			str_epo += cadena[i];
		}
		// Si todo es correcto se genera el archivo
		main.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '15consulta02ext.data.jsp',
			params: {
				informacion: 'CONSULTA_COMPRANET',
				mes_ini:     forma.query('#mes_ini_id')[0].getValue(),
				anio_ini:    forma.query('#anio_ini_id')[0].getValue(),
				mes_fin:     forma.query('#mes_fin_id')[0].getValue(),
				anio_fin:    forma.query('#anio_fin_id')[0].getValue(),
				federales:   forma.query('#federales_id')[0].getValue(),
				otros:       forma.query('#otros_id')[0].getValue(),
				epo:         str_epo
			},
			callback: procesaGeneraArchivo
		});
	}

//------------------------------ Callback ----------------------------
	// Lleno el catalogo con el valor por default
	var procesarCatalogoMesIni = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma       = Ext.ComponentQuery.query('#formaCompranet')[0];
			var campo       = forma.query('#mes_ini_id')[0];
			var store_combo = campo.getStore();
			var valor       = store_combo.getRange()[0].data.clave;
			campo.setValue(valor);
		}
	}

	// Lleno el catalogo con el valor por default
	var procesarCatalogoAnioIni = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma       = Ext.ComponentQuery.query('#formaCompranet')[0];
			var campo       = forma.query('#anio_ini_id')[0];
			var store_combo = campo.getStore();
			var valor       = store_combo.getRange()[0].data.clave;
			campo.setValue(valor);
		}
	}

	// Lleno el catalogo con el valor por default
	var procesarCatalogoMesFin = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma       = Ext.ComponentQuery.query('#formaCompranet')[0];
			var campo       = forma.query('#mes_fin_id')[0];
			var store_combo = campo.getStore();
			var valor       = store_combo.getRange()[0].data.clave;
			campo.setValue(valor);
		}
	}

	// Lleno el catalogo con el valor por default
	var procesarCatalogoAnioFin = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma       = Ext.ComponentQuery.query('#formaCompranet')[0];
			var campo       = forma.query('#anio_fin_id')[0];
			var store_combo = campo.getStore();
			var valor       = store_combo.getRange()[0].data.clave;
			campo.setValue(valor);
		}
	}

	// Handler que procesa respuesta creacion archivo
	function procesaGeneraArchivo(opts, success, response){

		main.el.unmask();

		// PARSEAR RESPUESTA DEL SERVIDOR
		var resp = null;
		try {
			resp = Ext.JSON.decode(response.responseText);
		} catch(error) {
			NE.util.mostrarErrorResponse4(
				{ status: -1 }, // Para que no asuma que es un est�tus devuelto por el servidor
				error.name + " - " + error.message
			);
			return;
		}

		// REALIZAR LA DESCARGA AUTOM�TICA DEL ARCHIVO
		if ( success == true && resp.success == true ) {

			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo';
			forma.method = 'post';
			forma.target = '_self';

			// Suprimir el Context root del url del archivo
			var urlArchivo = resp.urlArchivo;
			nombreArchivo  = urlArchivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');

			// Insertar Nombre Archivo
			var inputNombreArchivo = Ext.DomHelper.insertFirst(
				forma,
				{
					tag:   'input',
					type:  'hidden',
					id:    'nombreArchivo',
					name:  'nombreArchivo',
					value: nombreArchivo
				},
				true
			);
			// Solicitar Archivo Al Servidor
			forma.submit();
			// Remover nodo agregado
			inputNombreArchivo.remove();

		} else {
				NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}

	}

//------------------------------ Stores ------------------------------
	// Se crea el MODEL para los combos
	Ext.define('ModelCatologo',{
		extend: 'Ext.data.Model',
		fields: [{ name:'clave', type: 'string'}, { name:'descripcion', type:'string'}]
	});

	// Se crea el STORE para el cat�logo 'Mes'
	var catalogoMesIni = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15consulta02ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_MES'
			}
		},
		autoLoad:          true,
		listeners: {
			load:           procesarCatalogoMesIni,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'A�o'
	var catalogoAnioIni = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15consulta02ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_ANIO'
			}
		},
		autoLoad:          true,
		listeners: {
			load:           procesarCatalogoAnioIni,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'Mes'
	var catalogoMesFin = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15consulta02ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_MES'
			}
		},
		autoLoad:          true,
		listeners: {
			load:           procesarCatalogoMesFin,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'A�o'
	var catalogoAnioFin = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15consulta02ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_ANIO'
			}
		},
		autoLoad:          true,
		listeners: {
			load:           procesarCatalogoAnioFin,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'Dependencias'
	var catalogoEpo = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15consulta02ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_EPO'
			}
		},
		autoLoad:          false,
		listeners: {
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

//------------------------------ Componentes -------------------------
	// Se crea el form CADENAS
	var formaCompranet = Ext.create( 'Ext.form.Panel',{
		width:                  '70%',
		itemId:                 'formaCompranet',
		title:                  'COMPRANET',
		bodyPadding:            '12 6 12 6',
		style:                  'margin: 0px auto 0px auto;',
		frame:                  true,
		border:                 true,
		fieldDefaults: {
			msgTarget:           'side'
		},
		items: [{
			xtype:               'container',
			layout:              'hbox',
			anchor:              '95%',
			margin:              '0 0 5 0',
			items:[{
				width:            50,
				xtype:            'displayfield',
				value:            '&nbsp',
				hideLabel:        true
			},{
				width:            170,
				labelWidth:       65,
				xtype:            'combobox',
				itemId:           'mes_ini_id',
				name:             'mes_ini',
				hiddenName:       'mes_ini',
				fieldLabel:       'Periodo de',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoMesIni
			},{
				width:            100,
				xtype:            'combobox',
				itemId:           'anio_ini_id',
				name:             'anio_ini',
				hiddenName:       'anio_ini',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				hideLabel:        true,
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoAnioIni
			},{
				width:            120,
				labelWidth:       15,
				xtype:            'combobox',
				itemId:           'mes_fin_id',
				name:             'mes_fin',
				hiddenName:       'mes_fin',
				fieldLabel:       'a',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoMesFin
			},{
				width:            100,
				xtype:            'combobox',
				itemId:           'anio_fin_id',
				name:             'anio_fin',
				hiddenName:       'anio_fin',
				emptyText:        'Seleccione...',
				displayField:     'descripcion',
				valueField:       'clave',
				queryMode:        'local',
				triggerAction:    'all',
				listClass:        'x-combo-list-small',
				margins:          '0 20 0 0',
				hideLabel:        true,
				typeAhead:        true,
				selectOnTab:      true,
				lazyRender:       true,
				forceSelection:   true,
				editable:         true,
				allowBlank:       false,
				store:            catalogoAnioFin
			}]
		},{
			xtype:               'container',
			layout:              'hbox',
			anchor:              '95%',
			margin:              '0 0 5 0',
			items:[{
				width:            200,
				xtype:            'displayfield',
				value:            '&nbsp',
				hideLabel:        true
			},{
				width:            120,
				xtype:            'checkbox',
				itemId:           'federales_id',
				name:             'federales',
				hiddenName:       'federales',
				boxLabel:         '&nbsp; Federales',
				listeners: {
					change: function (field, newValue, oldValue){
						var tmpEpos = Ext.ComponentQuery.query('#epo_id')[0];
						tmpEpos.clearValue();
						tmpEpos.fromField.store.removeAll();
						consultaEpo();
					}
				}
			},{
				width:            120,
				xtype:            'checkbox',
				itemId:           'otros_id',
				name:             'otros',
				hiddenName:       'otros',
				boxLabel:         '&nbsp; Otros',
				listeners: {
					change: function (field, newValue, oldValue){
						var tmpEpos = Ext.ComponentQuery.query('#epo_id')[0];
						tmpEpos.clearValue();
						tmpEpos.fromField.store.removeAll();
						consultaEpo();
					}
				}
			}]
		},{
			height:              150,
			labelWidth:          50,
			anchor:              '95%',
			xtype:               'itemselector',
			name:                'epo',
			itemId:              'epo_id',
			fieldLabel:          '&nbsp; EPO',
			displayField:        'descripcion',
			valueField:          'clave',
			margins:             '0 20 0 0',
			imagePath:           '../ux/images/',
			buttons:             ['add', 'remove'],
			allowBlank:          true,
			store:               catalogoEpo
		}],
		buttons: [{
			text:                'Consultar',
			itemId:              'btnBuscar',
			iconCls:             'icoXls',
			handler:             buscar
		},{
			text:                'Regresar',
			itemId:              'btnRegresar',
			iconCls:             'icoRegresar',
			handler:             regresar
		}]
	});

	// Contenedor principal
	var main = Ext.create('Ext.container.Container',{
		width:                  949,
		minHeight:              650,
		autoHeight:             true,
		id:                     'contenedorPrincipal',
		renderTo:               'areaContenido',
		style:                  'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaCompranet
		]
	});

	consultaEpo();

});