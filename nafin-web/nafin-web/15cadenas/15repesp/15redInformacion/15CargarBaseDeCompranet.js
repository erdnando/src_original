Ext.onReady(function() {
	var avance100porc = 0;
	var ceroRegProc = 0;

	var fnDoContinuar = function(urlArchivo){
		Ext.Ajax.request({
			url : '15redInformacion/15CargarBaseDeCompranet.data.jsp',
			params : Ext.apply(pnlCargaCadenas.getForm().getValues(),{
				informacion: 'PROCESAR_ARCHIVO',
				reemplazarRegistros: 'true',
				urlArchivo: urlArchivo
			}),
			callback: procesarSuccessDoContinuar
		});
	}
	
	var fnDoProcesando = function(){
			Ext.Ajax.request({
			url : '15redInformacion/15CargarBaseDeCompranet.data.jsp',
			params : {
				informacion: 'PROCESANDO_ARCHIVO'
			},
			callback: procesarSuccessDoContinuar
		});
	}
	
//------------------------------------------------------------------------------------------------
	var procesarSuccessMuestraCatEposCompranet = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			
			pnlCargaCadenas.hide();
			pnlAvanceCarga.hide();
			pnlErroresCarga.hide();
			gridLayout.hide();
			gridCatalogoEpos.show();
			
			Ext.ComponentQuery.query('#btnAceptar')[0].show();
			Ext.ComponentQuery.query('#btnGuardar')[0].hide();
			Ext.ComponentQuery.query('#btnRegresar')[0].hide();
			gridCatalogoEpos.getStore().loadData(resp.registros);
			
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
	
	var procesarConsCatEposCompranet = function(store,  arrRegistros, success, opts) {
		var jsonParams = store.proxy.reader.jsonData;
		if (arrRegistros !== null) {
			if(store.getTotalCount() > 0) {
				if(jsonParams.csConsCat === 'S'){
					Ext.ComponentQuery.query('#btnAceptar')[0].hide();
					Ext.ComponentQuery.query('#btnGuardar')[0].show();
					Ext.ComponentQuery.query('#btnRegresar')[0].show();
				}
				
				pnlCargaCadenas.hide();
				pnlAvanceCarga.hide();
				pnlErroresCarga.hide();
				gridLayout.hide();
				gridCatalogoEpos.show();
			}
		}else{
		
		}
    };
	
	
	var procesarSuccessAceptarCatEpos = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			pnlCargaCadenas.show();
			
			if(!resp.hayRegistrosConError){
				pnlAvanceCarga.hide();
				gridCatalogoEpos.hide();
				pnlErroresCarga.hide();
				Ext.Msg.alert('Aviso', 'La carga se complet� con �xito.');
			}else{
				Ext.ComponentQuery.query('#mensjeErrorCarga')[0].setValue(resp.mensjeErrorCarga);
				pnlAvanceCarga.hide();
				gridCatalogoEpos.hide();
				pnlErroresCarga.show();
			}
			
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
	
	var procesarSuccessGuardarrCatEpos = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			Ext.Msg.alert('Aviso', 'La operaci�n se complet� con �xito.');
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
	
	var procesarSuccessDoContinuar  = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			pnlCargaCadenas.hide();
			pnlErroresCarga.hide();
			gridLayout.hide();
			pnlAvanceCarga.show();
			
			if(resp.inicioProceso===false){
				if(resp.errorProceso === true ){
					Ext.Msg.alert('Aviso','Ocurri� un error en el Proceso de Validaci�n de Datos. La carga se suspende.', function(){
						Ext.ComponentQuery.query('#avanceTR')[0].setValue('0');
						Ext.ComponentQuery.query('#avanceRP')[0].setValue('0');
						Ext.ComponentQuery.query('#avancePA')[0].setValue('0%');
						pnlCargaCadenas.show();
						pnlAvanceCarga.hide();
					});
					
				}else if(Number(resp.numeroTotalRegistros)===0){
	
						Ext.Msg.alert('Aviso','No se encontraron registros para el periodo indicado del a�o en curso. La carga se suspende.', function(){
							Ext.ComponentQuery.query('#avanceTR')[0].setValue('0');
							Ext.ComponentQuery.query('#avanceRP')[0].setValue('0');
							Ext.ComponentQuery.query('#avancePA')[0].setValue('0%');
							pnlCargaCadenas.show();
							pnlAvanceCarga.hide();
						});
				
				}else if(avance100porc <2){
					Ext.ComponentQuery.query('#avanceTR')[0].setValue(resp.numeroTotalRegistros);
					Ext.ComponentQuery.query('#avanceRP')[0].setValue(resp.numeroRegistrosProcesados);
					Ext.ComponentQuery.query('#avancePA')[0].setValue(resp.porcentaje);
					
					if(Number(resp.porcentaje)===100){
						avance100porc++;
					}
					
					setTimeout(function(){ fnDoProcesando(); }, 1000);
				
				}else if(avance100porc >=2){
					
					Ext.Ajax.request({
						url : '15redInformacion/15CargarBaseDeCompranet.data.jsp',
						params : {
							informacion: 'MUESTRA_CAT_EPOS_COMPRANET'
						},
						callback: procesarSuccessMuestraCatEposCompranet
					});
					
				}
			}else{
				setTimeout(function(){ fnDoProcesando(); }, 1000);
			}
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
//-----------------------------------------------------------------------------------------------
	Ext.define('ModCatEposCompranet', {
		extend: 'Ext.data.Model',
		fields: [
			{name: 'numeroEpoOriginal', type: 'string'},
			{name: 'nombreEpo', type: 'string'},
			{name: 'icEpoCompranet', type: 'string'},
			{name: 'nuevo', type: 'string'},
			{name: 'numeroEpo', type: 'string'}
			
		]
	});
		
	var storeCatEposCompranet = Ext.create('Ext.data.Store', {
			itemId: 'storeCatEposCompranet',
			storeId: 'storeCatEposCompranet',
			model: 'ModCatEposCompranet',
			proxy: {
				type: 'ajax',
				url: '15redInformacion/15CargarBaseDeCompranet.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'CONS_CAT_EPOS_COMPRANET'
				},
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			autoLoad: false,
			listeners:{
			    load:procesarConsCatEposCompranet
			}
		});
	
	var gridCatalogoEpos =  Ext.create('Ext.grid.Panel', {
		itemId: 'gridCtrlLimites',
		width: 650,
		height: 300,
		hidden: 'true',
		style: {margin: '0 auto'},
		title: 'Cat�logo de EPO\'s',
		frame: false,
		store: storeCatEposCompranet,
		columnLines: true,
		loadMask: true,
		selType: 'cellmodel',
		plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
			clicksToEdit: 1
		}),'bufferedrenderer'],
		selModel: {
			pruneRemoved: false
		},
		multiSelect: true,
		viewConfig: {
			trackOver: false,
			stripeRows: true,
			deferEmptyText: false,
			emptyText: 'No se encontr� informaci�n',
			getRowClass: function (record) {
				if(record.get('nuevo')==='S'){
					return 'row-nuevo';
				}else{
					return '';
				}
			}
		},
		columns:[
			{
				text: '<CENTER>Clave</CENTER>', dataIndex: 'numeroEpo', hideable: false,
				width: 60, align: 'left', sortable: false, locked: false,
				editor: {
					xtype: 'numberfield',
					allowBlank: false,
					step: 1,
					spinDownEnabled : false,
					spinUpEnabled : false,
					maxLength: 4,
					hideTrigger: true,
					allowDecimals:false,
					enableKeyEvents: true
				}
			},
			{
				text: '<CENTER>Nombre</CENTER>', dataIndex: 'nombreEpo', hideable: false, 
				width: 560, align: 'left', sortable: false, locked: false
			}
		],
		buttonAlign: 'center',
		buttons:[
			{text: 'Aceptar', itemId: 'btnAceptar',
				handler: function(){
					var registrosEnviar =[];
					storeCatEposCompranet.each(function(record){
						registrosEnviar.push(record.data);
					});
					
					Ext.Ajax.request({
						url : '15redInformacion/15CargarBaseDeCompranet.data.jsp',
						params : Ext.apply(pnlCargaCadenas.getForm().getValues(),{
							informacion: 'ACEPTAR_EPOS_COMPRANET',
							registros: Ext.encode(registrosEnviar),
							reemplazarRegistros: 'true'
						}),
						callback: procesarSuccessAceptarCatEpos
					});
				}
			}, {
				text: 'Guardar', itemId: 'btnGuardar',
				handler: function(){
					var registrosEnviar =[];
					storeCatEposCompranet.each(function(record){
						registrosEnviar.push(record.data);
					});
					
					Ext.Ajax.request({
						url : '15redInformacion/15CargarBaseDeCompranet.data.jsp',
						params : Ext.apply(pnlCargaCadenas.getForm().getValues(),{
							informacion: 'GUARDAR_EPOS_COMPRANET',
							registros: Ext.encode(registrosEnviar)
						}),
						callback: procesarSuccessGuardarrCatEpos
					});
				}
			},{
				text: 'Regresar', itemId: 'btnRegresar',
				handler: function(){
					pnlAvanceCarga.hide();
					gridCatalogoEpos.hide();
					pnlErroresCarga.hide();
					gridLayout.hide();
					pnlCargaCadenas.show();
				}
			}
		]
	});
	
	
	var pnlErroresCarga = Ext.create('Ext.panel.Panel',{
		itemId: 'pnlErroresCarga',
		title: 'Errores en el archivo',
		width: 500,
		hidden: true,
		frame: true,
		layout: 'form',
		items: [
			{
				xtype: 'textarea', itemId: 'mensjeErrorCarga', height: 300
			}
		]
	});
	
	var gridLayout = Ext.create('Ext.grid.Panel', {
		itemId: 'gridLayout',
		width: 600,
		height: 250,
		hidden: 'true',
		style: {margin: '0 auto'},
		store:  Ext.create('Ext.data.ArrayStore', {
			fields: [
			   {name: 'NUMERO'},
			   {name: 'DESCRIPCION'},
			   {name: 'TIPO_DATO'},
			   {name: 'LONGITUD'},
			   {name: 'OBLIGATORIO'}
			],
			data:[
				['1','Nombre de EPO', 'Texto', '150', 'Si'],['2','Licitaci�n', 'Texto', '16', 'Si'],
				['3','Unidad', 'Texto', '200', 'Si'],['4','Tipo de Contrataci�n', 'Texto', '40', 'Si'],
				['5','Fecha de Emisi�n de Fallo', 'Fecha', 'dd/mm/aaaa', 'Si'],['6','No. de la Partida', 'Num�rico', '5', 'Si'],
				['7','Importe sin IVA', 'Num�rico', '15,2', 'Si'],['8','Raz�n Social', 'Texto', '200', 'Si'],
				['9','RFC', 'Alfanum�rico', '15', 'Si'],['10','Clasificaci�n de la Dependencia', 'Num�rico', '1', 'Si']
			]
		}),
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'No. de Campo',
				dataIndex : 'NUMERO',
				width : 90,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex : 'DESCRIPCION',
				width : 180,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Tipo de Dato',
				tooltip: 'Tipo de Dato',
				dataIndex : 'TIPO_DATO',
				width : 90,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Longitud',
				tooltip: 'Longitud',
				dataIndex : 'LONGITUD',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 100,
				sortable : true,
				align: 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		frame: true
	});
	
	var pnlAvanceCarga = Ext.create('Ext.panel.Panel',{
		itemId: 'pnlAvanceCarga',
		title: 'Estatus de la Carga',
		width: 500,
		hidden: true,
		frame: true,
		layout: 'form',
		items: [
			{xtype: 'displayfield', itemId: 'avanceTR', labelWidth: 300, labelAlign: 'right', fieldLabel: 'Total de Registros', value:'0'},
			{xtype: 'displayfield', itemId: 'avanceRP', labelWidth: 300, labelAlign: 'right', fieldLabel: 'Registros Procesados', value:'0'},
			{xtype: 'displayfield', itemId: 'avancePA', labelWidth: 300, labelAlign: 'right', fieldLabel: 'Porcentaje de Avance', value:'0%'}
		]
	});
	
	
	var pnlCargaCadenas = Ext.create('Ext.form.Panel',{
		title: '<CENTER>COMPRANET</CENTER>',
		frame: true,
		width: 600,
		items:[
			{xtype: 'hidden', itemId: 'noProceso', name:'noProceso'},
			{xtype: 'hidden', itemId: 'meses', name:'meses'},
			{xtype: 'hidden', itemId: 'mesesG', name:'mesesG'},
			{
				xtype: 'container', width:350, layout:'hbox',defaults:{margins:'10 10 10 10'},style: {margin: '0 auto'},
				items: [
					{ xtype: 'displayfield', value: 'Periodo: ' },
					{
						xtype: 'combo', itemId: 'mesInicial', name: 'mesInicial', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15redInformacion/15CargarBaseDeCompranet.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MES'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
											var index = store.getCount();
											var valor = store.getAt(index-1).get('clave');
											var mesInicial = Ext.ComponentQuery.query('#mesInicial')[0];
											mesInicial.setValue(valor);
										
									}
								}
						}
					},
					{ xtype: 'displayfield', value: 'a' },
					{
						xtype: 'combo', itemId: 'mesFinal', name: 'mesFinal', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15redInformacion/15CargarBaseDeCompranet.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MES'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
											var index = store.getCount();
											var valor = store.getAt(index-1).get('clave');
											var mesFinal = Ext.ComponentQuery.query('#mesFinal')[0];
											mesFinal.setValue(valor);
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					}
				]
			},
			{xtype: 'displayfield', value: 'Seleccione la ruta del archivo a cargar:'},
			{xtype: 'container', layout:'hbox',
			items:[
				{xtype:'button', iconCls:'icoAyuda',
					handler: function(){
						if(gridLayout.isVisible()){
							gridLayout.hide();
						}else{
							gridLayout.show();
						}
					}
				},
				{
					xtype: 'filefield',
					id: 'idArchivoActulizaLG',
					itemId: 'idArchivoActulizaLG',
					emptyText: 'Selecciona archivo CSV',
					fieldLabel: 'Carga de Archivo',
					allowBlank: false,
					width: 560,
					name: 'archivoAutLG',
					regex: /^.*\.(csv|CSV)$/,
					regexText: 'Favor de ingresar un archivo con formato .csv',
					buttonText: 'Examinar',
					buttonConfig: {
						iconCls: 'upload-icon'
					}
				}
			]}
		],
		buttonAlign: 'center',
		buttons: [
			{
				text: 'Continuar',
				iconCls: 'icoContinuar',
				formBind: true,
				handler: function(){
					var form = this.up('form').getForm();
					if(form.isValid()){
						form.submit({
							url: '15redInformacion/15CargarBaseDeCompranetFile.jsp',
							waitMsg: 'Cargando archivo... ',
							success: function(fp, o) {
								var resp = Ext.JSON.decode(o.response.responseText);
								var objArchivoAut = Ext.ComponentQuery.query('#idArchivoActulizaLG')[0];
								if(resp.success){
									fnDoContinuar(resp.urlArchivo);
								}else{
									Ext.Msg.alert('Aviso', 'Error en la carga del archivo');
								}
				
							},
							failure: function() {
								Ext.Msg.alert("Error", Ext.JSON.decode(this.response.responseText).msgErrorPdf);
							}
						});
					}
				}
			},
			{
				text: 'Cat�logo de Epo\'s',
				iconCls: 'icoTxt',
				handler: function(){
					storeCatEposCompranet.load();
				}
			},
			{
				text: 'Regresar',
				iconCls: 'icoRegresar',
				handler: function(){
					window.location = '15redInfoProvCargarInfoExt.jsp';
				}
			}
		]
	});
	
	Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(20),
			pnlCargaCadenas,
			pnlAvanceCarga,
			gridCatalogoEpos,
			NE.util.getEspaciador(20),
			gridLayout,
			pnlErroresCarga
		]
	});
	
	

});