Ext.onReady(function() {
	var avance100porc = 0;

	var fnDoContinuar = function(urlArchivo){
		Ext.Ajax.request({
			url : '15redInformacion/15cargarBaseDeSiag.data.jsp',
			params : Ext.apply(pnlCargaInfoSiag.getForm().getValues(),{
				informacion: 'PROCESAR_ARCHIVO',
				reemplazarRegistros: 'true',
				urlArchivo: urlArchivo
			}),
			callback: procesarSuccessDoContinuar
		});
	}
	
	var fnDoProcesando = function(){
			Ext.Ajax.request({
			url : '15redInformacion/15cargarBaseDeSiag.data.jsp',
			params : {
				informacion: 'PROCESANDO_ARCHIVO'
			},
			callback: procesarSuccessDoContinuar
		});
	}
	
//------------------------------------------------------------------------------------------------
	var procesarSuccessValidIniciales = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
	
	var procesarSuccessCamposAdicionales = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			
			Ext.create('Ext.window.Window', {
				title: 'Nombrar Campos Adicionales',
				itemId: 'winGuardarCamposAdic',
				width: 400,
				layout: 'form',
				items: [{
						xtype: 'textarea',
						itemId: 'camposAdicionales',
						fieldLabel: 'Capturar el Nombre de los Campos Adicionales seguido de comas(,)',
						labelAlign: 'top',
						value: resp.camposAdicionales,
						style: {margin: '0 auto'},
						width: 300,
						height: 150,
						maxLength: 1536,
						enableKeyEvents: true,
						listeners: {
							keyup: function(obj, e, opt){
								var objContador = Ext.ComponentQuery.query('#contadorCaract')[0];
								objContador.setValue(1536 - Number(obj.getValue().length));
							}
						}
					},{
						xtype: 'displayfield',  itemId: 'contadorCaract', value:1536,
						listeners:{
							afterrender: function(obj){
								var objAdic = Ext.ComponentQuery.query('#camposAdicionales')[0];
								obj.setValue(1536 - Number(objAdic.getValue().length));
							}
						}
					}
				],
				buttonAlign: 'center',
				buttons:[
					{text:'Guardar', handler: function(){
							Ext.Ajax.request({
								url : '15redInformacion/15cargarBaseDeSiag.data.jsp',
								params : {
									informacion: 'GUARDAR_CAMPOS_ADICIONALES',
									camposAdicionales: Ext.ComponentQuery.query('#camposAdicionales')[0].getValue()
								},
								callback: procesarSuccessGuardarCamposAdic
							});
						}
					},
					{text:'Cancelar',  handler: function(){
						Ext.ComponentQuery.query('#winGuardarCamposAdic')[0].close();
					}}
				],
				modal: true,
				closeAction: 'destroy'
			}).show();
			
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
	
	var procesarSuccessGuardarCamposAdic = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			Ext.Msg.alert('Aviso', 'La descripci�n de los Campos Adicionales se actualiz� con �xito.');
			Ext.ComponentQuery.query('#winGuardarCamposAdic')[0].close();
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
	
	var procesarSuccessMuestraResumenCargaSiag = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			pnlCargaInfoSiag.show();
			
			if(!resp.hayRegistrosConError){
				pnlAvanceCarga.hide();
				pnlErroresCarga.hide();
				Ext.Msg.alert('Aviso', 'La carga se complet� con �xito.');
			}else{
				Ext.ComponentQuery.query('#mensjeErrorCarga')[0].setValue(resp.mensjeErrorCarga);
				pnlAvanceCarga.hide();
				pnlErroresCarga.show();
			}
			
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
	
	var procesarSuccessDoContinuar  = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			pnlCargaInfoSiag.hide();
			pnlErroresCarga.hide();
			gridLayout.hide();
			pnlAvanceCarga.show();
			
			if(resp.inicioProceso===false){
				if(resp.errorProceso === true ){
					Ext.Msg.alert('Aviso','Ocurri� un error en el Proceso de Validaci�n de Datos. La carga se suspende.', function(){
						Ext.ComponentQuery.query('#avanceTR')[0].setValue('0');
						Ext.ComponentQuery.query('#avanceRP')[0].setValue('0');
						Ext.ComponentQuery.query('#avancePA')[0].setValue('0%');
						pnlCargaInfoSiag.show();
						pnlAvanceCarga.hide();
					});
					
				}else if(Number(resp.numeroTotalRegistros)===0){
	
						Ext.Msg.alert('Aviso','No se encontraron registros para el periodo indicado del a�o en curso. La carga se suspende.', function(){
							Ext.ComponentQuery.query('#avanceTR')[0].setValue('0');
							Ext.ComponentQuery.query('#avanceRP')[0].setValue('0');
							Ext.ComponentQuery.query('#avancePA')[0].setValue('0%');
							pnlCargaInfoSiag.show();
							pnlAvanceCarga.hide();
						});
				
				}else if(avance100porc <2){
					Ext.ComponentQuery.query('#avanceTR')[0].setValue(resp.numeroTotalRegistros);
					Ext.ComponentQuery.query('#avanceRP')[0].setValue(resp.numeroRegistrosProcesados);
					Ext.ComponentQuery.query('#avancePA')[0].setValue(resp.porcentaje);
					
					if(Number(resp.porcentaje)===100){
						avance100porc++;
					}
					
					setTimeout(function(){ fnDoProcesando(); }, 1000);
				
				}else if(avance100porc >=2){
					
					Ext.Ajax.request({
						url : '15redInformacion/15cargarBaseDeSiag.data.jsp',
						params : Ext.apply(pnlCargaInfoSiag.getForm().getValues(),{
							informacion: 'MUESTRA_RESUMEN_CARGA_SIAG',
							reemplazarRegistros: 'true'
						}),
						callback: procesarSuccessMuestraResumenCargaSiag
					});
					
				}
			}else{
				setTimeout(function(){ fnDoProcesando(); }, 1000);
			}
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
//-----------------------------------------------------------------------------------------------
	
	
	var pnlErroresCarga = Ext.create('Ext.panel.Panel',{
		itemId: 'pnlErroresCarga',
		title: 'Errores en el archivo',
		width: 500,
		hidden: true,
		frame: true,
		layout: 'form',
		items: [
			{
				xtype: 'textarea', itemId: 'mensjeErrorCarga', height: 300
			}
		]
	});
	
	var gridLayout = Ext.create('Ext.grid.Panel', {
		itemId: 'gridLayout',
		width: 600,
		height: 340,
		hidden: 'true',
		style: {margin: '0 auto'},
		store:  Ext.create('Ext.data.ArrayStore', {
			fields: [
			   {name: 'NUMERO'},
			   {name: 'DESCRIPCION'},
			   {name: 'TIPO_DATO'},
			   {name: 'LONGITUD'},
			   {name: 'OBLIGATORIO'}
			],
			data:[
				['1','Raz�n Social', 'Texto', '90', 'Si'],['2','RFC', 'Alfanum�rico', '15', 'Si'],
				['3','Estado', 'Texto', '50', 'Si'],['4','Monto Actual del Cr�dito', 'Num�rico', '15,2', 'Si'],
				['5','Plazo', 'Num�rico', '3', 'Si'],['6','Intermediario', 'Texto', '5', 'Si'],
				['7','Estrato Inicial', 'Num�rico', '3', 'Si'],['8','Nuevo Estrato', 'Numerico', '3', 'Si'],
				['9','Sector', 'Num�rico', '3', 'Si'],['10','Fecha de Registro', 'Fecha', 'dd/mm/aaaa', 'Si'],
				['11','Descripci�n del Portafolio', 'Texto', '30', 'Si'],['12','Clave CONREC', 'Numerico', '3', 'Si'],
				['13','Campos Adicionales', 'Texto', '4000', 'No'],['13.1','param3', 'Texto', 'variable', 'No']
			]
		}),
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'No. de Campo',
				dataIndex : 'NUMERO',
				width : 90,
				sortable : true,
				align: 'center'
			},
			{
				header : '<CENTER>Descripci�n</CENTER>',
				tooltip: 'Descripci�n',
				dataIndex : 'DESCRIPCION',
				width : 200,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Tipo de Dato',
				tooltip: 'Tipo de Dato',
				dataIndex : 'TIPO_DATO',
				width : 90,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Longitud',
				tooltip: 'Longitud',
				dataIndex : 'LONGITUD',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 100,
				sortable : true,
				align: 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		frame: true
	});
	
	var pnlAvanceCarga = Ext.create('Ext.panel.Panel',{
		itemId: 'pnlAvanceCarga',
		title: 'Estatus de la Carga',
		width: 500,
		hidden: true,
		frame: true,
		layout: 'form',
		items: [
			{xtype: 'displayfield', itemId: 'avanceTR', labelWidth: 300, labelAlign: 'right', fieldLabel: 'Total de Registros', value:'0'},
			{xtype: 'displayfield', itemId: 'avanceRP', labelWidth: 300, labelAlign: 'right', fieldLabel: 'Registros Procesados', value:'0'},
			{xtype: 'displayfield', itemId: 'avancePA', labelWidth: 300, labelAlign: 'right', fieldLabel: 'Porcentaje de Avance', value:'0%'}
		]
	});
	
	
	var pnlCargaInfoSiag = Ext.create('Ext.form.Panel',{
		title: '<CENTER>SIAG</CENTER>',
		frame: true,
		width: 600,
		items:[
			{xtype: 'hidden', itemId: 'noProceso', name:'noProceso'},
			{xtype: 'hidden', itemId: 'meses', name:'meses'},
			{xtype: 'hidden', itemId: 'mesesG', name:'mesesG'},
			{
				xtype: 'container', width:350, layout:'hbox',defaults:{margins:'10 10 10 10'},style: {margin: '0 auto'},
				items: [
					{ xtype: 'displayfield', value: 'Periodo: ' },
					{
						xtype: 'combo', itemId: 'mesInicial', name: 'mesInicial', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15redInformacion/15cargarBaseDeSiag.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MES'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
											var index = store.getCount();
											var valor = store.getAt(index-1).get('clave');
											var mesInicial = Ext.ComponentQuery.query('#mesInicial')[0];
											mesInicial.setValue(valor);
										
									}
								}
						}
					},
					{ xtype: 'displayfield', value: 'a' },
					{
						xtype: 'combo', itemId: 'mesFinal', name: 'mesFinal', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15redInformacion/15cargarBaseDeSiag.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MES'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
											var index = store.getCount();
											var valor = store.getAt(index-1).get('clave');
											var mesFinal = Ext.ComponentQuery.query('#mesFinal')[0];
											mesFinal.setValue(valor);
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					}
				]
			},
			{xtype: 'displayfield', value: 'Seleccione la ruta del archivo a cargar:'},
			{xtype: 'container', layout:'hbox',
			items:[
				{xtype:'button', iconCls:'icoAyuda',
					handler: function(){
						if(gridLayout.isVisible()){
							gridLayout.hide();
						}else{
							gridLayout.show();
						}
					}
				},
				{
					xtype: 'filefield',
					id: 'idArchivoActulizaLG',
					itemId: 'idArchivoActulizaLG',
					emptyText: 'Selecciona archivo CSV',
					fieldLabel: 'Carga de Archivo',
					allowBlank: false,
					width: 560,
					name: 'archivoAutLG',
					regex: /^.*\.(csv|CSV)$/,
					regexText: 'Favor de ingresar un archivo con formato .csv',
					buttonText: 'Examinar',
					buttonConfig: {
						iconCls: 'upload-icon'
					}
				}
			]}
		],
		buttonAlign: 'center',
		buttons: [
			{
				text: 'Continuar',
				iconCls: 'icoContinuar',
				formBind: true,
				handler: function(){
					var form = this.up('form').getForm();
					if(form.isValid()){
						form.submit({
							url: '15redInformacion/15cargarBaseDeSiagFile.jsp',
							waitMsg: 'Cargando archivo... ',
							success: function(fp, o) {
								var resp = Ext.JSON.decode(o.response.responseText);
								var objArchivoAut = Ext.ComponentQuery.query('#idArchivoActulizaLG')[0];
								if(resp.success){
									fnDoContinuar(resp.urlArchivo);
								}else{
									Ext.Msg.alert('Aviso', 'Error en la carga del archivo');
								}
				
							},
							failure: function() {
								Ext.Msg.alert("Error", Ext.JSON.decode(this.response.responseText).msgErrorPdf);
							}
						});
					}
				}
			},
			{
				text: 'Nombrar Campos Adiconales',
				iconCls: 'icoTxt',
				handler: function(){
					Ext.Ajax.request({
						url : '15redInformacion/15cargarBaseDeSiag.data.jsp',
						params : {
							informacion: 'CAMPOS_ADICIONALES'
						},
						callback: procesarSuccessCamposAdicionales
					});
				}
			},
			{
				text: 'Regresar',
				iconCls: 'icoRegresar',
				handler: function(){
					window.location = '15redInfoProvCargarInfoExt.jsp';
				}
			}
		]
	});
	
	Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(20),
			pnlCargaInfoSiag,
			pnlAvanceCarga,
			NE.util.getEspaciador(20),
			gridLayout,
			pnlErroresCarga
		]
	});
	
	Ext.Ajax.request({
		url : '15redInformacion/15cargarBaseDeSiag.data.jsp',
		params : {
			informacion: 'VALORES_INICIALES'
		},
		callback: procesarSuccessValidIniciales
	});

});