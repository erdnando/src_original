<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.SimpleDateFormat,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.xls.*,
		com.netro.xlsx.*,
		com.netro.exception.*,
		com.netro.seguridad.*,
		com.netro.seguridadbean.*,
		com.netro.cadenas.*,
		com.netro.model.catalogos.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_comun.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion    = request.getParameter("informacion")     == null ? "": (String)request.getParameter("informacion");
String mesIni         = request.getParameter("mes_ini")         == null ? "": (String)request.getParameter("mes_ini");
String anioIni        = request.getParameter("anio_ini")        == null ? "": (String)request.getParameter("anio_ini");
String mesFin         = request.getParameter("mes_fin")         == null ? "": (String)request.getParameter("mes_fin");
String anioFin        = request.getParameter("anio_fin")        == null ? "": (String)request.getParameter("anio_fin");
String dependencias   = request.getParameter("dependencias")    == null ? "": (String)request.getParameter("dependencias");

HashMap    datos      = new HashMap();
JSONArray  registros  = new JSONArray();
JSONObject resultado  = new JSONObject();

String  mensaje       = "";
String  consulta      = "";
String  nombreArchivo = "";
String  infoRegresar  = "";
boolean success       = true;

log.info("informacion: <<<<<"    + informacion    + ">>>>>");

if(informacion.equals("CATALOGO_MES")){

	Calendar cal = Calendar.getInstance();
	int sAnioIni = cal.get(Calendar.YEAR);

	CatalogoRedInformacionMes cat = new CatalogoRedInformacionMes();
	cat.setTabla("COM_CRUCE_CADENAS");
	cat.setNombreTabla("COM_CRUCE_CADENAS");
	cat.setAnioIni(sAnioIni);
	cat.setCampoClave("DISTINCT SUBSTR (CG_MES, 1, 3)");
	cat.setCampoDescripcion("CG_MES");
	cat.setOrden("CG_MES");
	List elementos = cat.getListaElementos();

	Iterator it = elementos.iterator();
	String descripcion = "";
	String clave = "";

	while(it.hasNext()){
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			clave = ec.getClave();
			descripcion = ec.getDescripcion();
			System.err.println(clave + " - " + descripcion);
			if("ENE".equals(clave)) {  clave ="01";  descripcion = "Enero";       }
			if("FEB".equals(clave)) {  clave ="02";  descripcion = "Febrero";     }
			if("MAR".equals(clave)) {  clave ="03";  descripcion = "Marzo";       }
			if("ABR".equals(clave)) {  clave ="04";  descripcion = "Abril";       }
			if("MAY".equals(clave)) {  clave ="05";  descripcion = "Mayo";        }
			if("JUN".equals(clave)) {  clave ="06";  descripcion = "Junio";       }
			if("JUL".equals(clave)) {  clave ="07";  descripcion = "Julio";       }
			if("AGO".equals(clave)) {  clave ="08";  descripcion = "Agosto";      }
			if("SEP".equals(clave)) {  clave ="09";  descripcion = "Septiembre";  }
			if("OCT".equals(clave)) {  clave ="10";  descripcion = "Octubre";     }
			if("NOV".equals(clave)) {  clave ="11";  descripcion = "Noviembre";   }
			if("DIC".equals(clave)) {  clave ="12";  descripcion = "Diciembre";   }
			datos = new HashMap();
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);
		}
	}

	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else if(informacion.equals("CATALOGO_ANIO")){

	Calendar cal = Calendar.getInstance();
	int sAnioIni = cal.get(Calendar.YEAR);

	datos = new HashMap();
	datos.put("clave",       Integer.toString(sAnioIni));
	datos.put("descripcion", Integer.toString(sAnioIni));
	registros.add(datos);
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else if(informacion.equals("CATALOGO_DEPENDENCIAS")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_DEPENDENCIA");
	catalogo.setCampoDescripcion("CG_DESCRIPCION");
	catalogo.setTabla("COMCAT_DEPEN_CADENAS");
	catalogo.setOrden("CG_DESCRIPCION");
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("CONSULTA_CADENAS")){

	String meses1    = "";
	String mesesC    = "";
	String mesesCon  = "";
	String mesesConT = "";
	int    tamanio   = 0;
	int    tamanioC  = 0;

	ArrayList alMes      = new ArrayList();
	ArrayList alAnio     = new ArrayList();
	ArrayList datosmeses = new ArrayList();

	if(!mesIni.equals(mesFin))
		tamanio = Integer.parseInt(mesFin) - Integer.parseInt(mesIni) + 1;
	else
		tamanio = Integer.parseInt(mesFin);

	for (int e = Integer.parseInt(mesIni); e <= tamanio; e++){
		if(e==1) { mesesC = "ENE";}
		if(e==2) { mesesC = "FEB";}
		if(e==3) { mesesC = "MAR";}
		if(e==4) { mesesC = "ABR";}
		if(e==5) { mesesC = "MAY";}
		if(e==6) { mesesC = "JUN";}
		if(e==7) { mesesC = "JUL";}
		if(e==8) { mesesC = "AGO";}
		if(e==9) { mesesC = "SEP";}
		if(e==10){ mesesC = "OCT";}
		if(e==11){ mesesC = "NOV";}
		if(e==12){ mesesC = "DIC";}
		mesesCon += "'" + mesesC + "-" + anioFin + "',";
		alMes.add(mesesC);
		alAnio.add(anioFin);
		datosmeses.add(mesesC+anioFin);
	}

	tamanioC  = mesesCon.length()-1;
	mesesConT = mesesCon.substring(0, tamanioC);

	ConsultaRedInformacionCadenas paginacion = new ConsultaRedInformacionCadenas();
	paginacion.setAlMes(alMes);
	paginacion.setAlAnio(alAnio);
	paginacion.setDatosmeses(datosmeses);
	paginacion.setMesesConT(mesesConT);
	paginacion.setClasificacion(dependencias);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginacion);

	try {
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
		resultado.put("success", new Boolean(true));
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e){
		throw new AppException("Error al generar el archivo CSV", e);
	}

	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>