<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.SimpleDateFormat,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.exception.*,
		com.netro.seguridad.*,
		com.netro.seguridadbean.*,
		com.netro.cadenas.*,
		com.netro.model.catalogos.*,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_comun.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion    = request.getParameter("informacion")     == null ? "": (String)request.getParameter("informacion");
String mesIni         = request.getParameter("mes_ini")         == null ? "": (String)request.getParameter("mes_ini");
String anioIni        = request.getParameter("anio_ini")        == null ? "": (String)request.getParameter("anio_ini");
String mesFin         = request.getParameter("mes_fin")         == null ? "": (String)request.getParameter("mes_fin");
String anioFin        = request.getParameter("anio_fin")        == null ? "": (String)request.getParameter("anio_fin");
String portafolio     = request.getParameter("portafolio")      == null ? "": (String)request.getParameter("portafolio");

HashMap    datos      = new HashMap();
JSONArray  registros  = new JSONArray();
JSONObject resultado  = new JSONObject();

String  mensaje       = "";
String  consulta      = "";
String  nombreArchivo = "";
String  infoRegresar  = "";
boolean success       = true;

log.info("informacion: <<<<<"    + informacion    + ">>>>>");

if(informacion.equals("CATALOGO_MES")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COM_CRUCE_SIAG");
	cat.setCampoClave("DISTINCT TO_CHAR(DF_FECHA_REGISTRO,'MM')");
	cat.setCampoDescripcion("TO_CHAR(DF_FECHA_REGISTRO,'MM')");
	cat.setOrden("CLAVE");
	List elementos = cat.getListaElementos();

	Iterator it = elementos.iterator();
	String descripcion = "";
	String clave = "";

	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			clave = ec.getClave();
			descripcion = ec.getDescripcion();
			if("01".equals(clave)) {  clave ="01";  descripcion = "Enero";       }
			if("02".equals(clave)) {  clave ="02";  descripcion = "Febrero";     }
			if("03".equals(clave)) {  clave ="03";  descripcion = "Marzo";       }
			if("04".equals(clave)) {  clave ="04";  descripcion = "Abril";       }
			if("05".equals(clave)) {  clave ="05";  descripcion = "Mayo";        }
			if("06".equals(clave)) {  clave ="06";  descripcion = "Junio";       }
			if("07".equals(clave)) {  clave ="07";  descripcion = "Julio";       }
			if("08".equals(clave)) {  clave ="08";  descripcion = "Agosto";      }
			if("09".equals(clave)) {  clave ="09";  descripcion = "Septiembre";  }
			if("10".equals(clave)) {  clave ="10";  descripcion = "Octubre";     }
			if("11".equals(clave)) {  clave ="11";  descripcion = "Noviembre";   }
			if("12".equals(clave)) {  clave ="12";  descripcion = "Diciembre";   }
			datos = new HashMap();
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);
		}
	}

	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else if(informacion.equals("CATALOGO_ANIO")){

	Calendar cal = Calendar.getInstance();
	int sAnioIni = cal.get(Calendar.YEAR);

	datos = new HashMap();
	datos.put("clave",       Integer.toString(sAnioIni));
	datos.put("descripcion", Integer.toString(sAnioIni));
	registros.add(datos);
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else if(informacion.equals("CATALOGO_PORTAFOLIO")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("DISTINCT CG_PORTAFOLIO");
	catalogo.setCampoDescripcion("CG_PORTAFOLIO");
	catalogo.setTabla("COM_CRUCE_SIAG");
	catalogo.setOrden("CG_PORTAFOLIO");
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("CONSULTA_SIAG")){

	String fechaInicial = "";
	String fechaFinal   = "";

	fechaInicial = "01/" + mesIni + "/" + anioIni;
	if( mesFin.equals("02")){
		fechaFinal =  "28/" + mesFin + "/" + anioFin;
	} else{
		fechaFinal =  "30/" + mesFin + "/" + anioFin;
	}

	ConsultaRedInformacionSiag paginacion = new ConsultaRedInformacionSiag();
	paginacion.setFechaInicial(fechaInicial);
	paginacion.setFechaFinal(fechaFinal);
	paginacion.setPortafolio(portafolio);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginacion);

	try {
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
		resultado.put("success", new Boolean(true));
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e){
		throw new AppException("Error al generar el archivo CSV", e);
	}

	infoRegresar = resultado.toString();
}

%>
<%=infoRegresar%>