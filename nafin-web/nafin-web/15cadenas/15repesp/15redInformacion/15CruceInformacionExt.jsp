<!DOCTYPE html>
<%@ page import="
		java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/14seguridad/14secsession.jspf" %>

<%
	String cgCI = request.getParameter("cgCI")==null?"":request.getParameter("cgCI");
%>

<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs4.jspf" %>
<%@ include file="/01principal/menu.jspf"%>

<style>
.row-nuevo .x-grid-cell {
	color: red;
}

.ext-el-mask-msg div { cursor: default !important; }
</style>
<% if("CadCompranet".equals(cgCI)){%>
<script type="text/javascript" src="15CadenasCompranetExt.js?<%=session.getId()%>"></script>
<% }else if("CompranetSiag".equals(cgCI)){%>
<script type="text/javascript" src="15cruceCompranetSiagExt.js?<%=session.getId()%>"></script>
<% }else if("CadCompranetSiag".equals(cgCI)){%>
<script type="text/javascript" src="15cruceCadenasCompranetSiagExt.js?<%=session.getId()%>"></script>
<% }else {%>
<script type="text/javascript" src="15CruceInformacionExt.js?<%=session.getId()%>"></script>
<% }%>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<div id="areaContenido"></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>

</html>