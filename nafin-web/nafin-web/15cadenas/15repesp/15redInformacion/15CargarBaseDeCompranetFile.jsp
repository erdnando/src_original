<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		java.sql.*,
		java.util.regex.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	JSONObject archivoCSV = new JSONObject();
	JSONObject archivoPDF = new JSONObject();
	JSONObject registroAcuse = new JSONObject();
	JSONArray arrAcuse = new JSONArray();
	boolean flag = true;
	boolean flagExt = true;
	boolean flagReg = true;
	ParametrosRequest req = null;
	String rutaArchivo = "";
	String PATH_FILE	=	strDirectorioTemp;
	String itemArchivo = "";
	String extension = "";
	String folio = "";
	String fechaHora = "";
	String msgError = "";
	
	int tamanio = 0;
	
	String fechaHoy	= "";
	String horaActual	= "";
	String nombreArch = "";
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		extension = (req.getParameter("hidExtension") == null)? "" : req.getParameter("hidExtension");

		try{
			int tamaPer = 0;
			if (extension.equals("csv")){
				upload.setSizeMax(200 * 1024);	//700 Kb
				tamaPer = (200 * 1024);
			}

			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName();
			InputStream archivoZip = fItem.getInputStream();
			tamanio			= (int)fItem.getSize();

			rutaArchivo = PATH_FILE+itemArchivo;
			
			File file = new File(rutaArchivo);
			fItem.write(file);
			
			System.out.println("tamanio=="+tamanio);
			System.out.println("tamaPer=="+tamaPer);
			System.out.println("itemArchivo=="+itemArchivo);
			System.out.println("strDirectorioPublicacion=="+strDirectorioPublicacion.replaceAll("/",""));
			System.out.println("rutaArchivo=="+rutaArchivo);
			
			
			if (tamanio > tamaPer){
				flag = false;
				msgError = "El tamanio del archivo TXT es mayor al permitido";
			}
			if (flag){

				fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				try{
					nombreArch = itemArchivo;
					System.out.println("ARCHIVO:: "+nombreArch);
				}catch (Exception e){
						throw new AppException("Ocurrio un error al generar el archivo", e);
				}
			
			}
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}
	
%>	

{
	"success": true,
	"urlArchivo": '<%=itemArchivo%>',
	"fechaHoy": '<%=fechaHoy%>',
	"horaActual": '<%=horaActual%>',
	"usuario": '<%=strNombreUsuario%>'
}
