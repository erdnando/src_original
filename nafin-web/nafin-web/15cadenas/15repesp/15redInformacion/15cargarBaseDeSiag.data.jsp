<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		com.netro.informacion.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		com.netro.seguridadbean.*,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion    = request.getParameter("informacion")      == null ? "": (String)request.getParameter("informacion");
JSONObject jsonObj  = new JSONObject();
String infoRegresar   = "";

//variables para combos para el rango de periodos
String sMeses[] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
RepEspecialesProv repEspecialesProv = ServiceLocator.getInstance().lookup("RepEspecialesProvEJB", RepEspecialesProv.class);


if(informacion.equals("VALORES_INICIALES")){
	ValidaDatosSiagThread validaDatos = (ValidaDatosSiagThread) request.getSession().getAttribute("ValidaDatosSiag");
		if(validaDatos != null){
			if(validaDatos.isRunning()) validaDatos.setRunning(false);
			request.getSession().removeAttribute("ValidaDatosSiag");
		}
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("CONS_CAT_MES")){
	
	List catalogoMeses 		= repEspecialesProv.getCatalogoMesesHastaMesActual();
	
	jsonObj.put("registros", JSONArray.fromObject(catalogoMeses));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("PROCESAR_ARCHIVO")){
	
	String mesInicio = request.getParameter("mesInicial")==null?"":request.getParameter("mesInicial");
	String mesFin = request.getParameter("mesFinal")==null?"":request.getParameter("mesFinal");
	String reemReg = request.getParameter("reemplazarRegistros")==null?"":request.getParameter("reemplazarRegistros");
	String urlArchivo = request.getParameter("urlArchivo")==null?"":request.getParameter("urlArchivo");
	
	boolean reemplazarRegistros  = "true".equals(reemReg)?true:false;
	
	// Obtener numero de proceso
	int numeroProceso = Integer.parseInt(repEspecialesProv.getIdCargaSiag());
	
	// Insertar el archivo
	FiltroFechaSiag filtroFechas = new FiltroFechaSiag(Integer.parseInt(mesInicio),Integer.parseInt(mesFin),reemplazarRegistros);	
	repEspecialesProv.insertaRegistrosTmpSiag(strDirectorioTemp+urlArchivo, numeroProceso, filtroFechas);
	
	// "Lanzar" thread
	ValidaDatosSiagThread validaDatos = new ValidaDatosSiagThread();
	request.getSession().setAttribute("ValidaDatosSiag",validaDatos);
	validaDatos.setProcessID(numeroProceso);
	validaDatos.setRunning(true);
	new Thread(validaDatos).start();
	
	jsonObj.put("inicioProceso", new Boolean(true));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("PROCESANDO_ARCHIVO")){
	ValidaDatosSiagThread validaDatos = (ValidaDatosSiagThread) request.getSession().getAttribute("ValidaDatosSiag");
	
	if(validaDatos == null){
		jsonObj.put("numeroTotalRegistros", "0");
		jsonObj.put("numeroRegistrosProcesados", "0");
		jsonObj.put("porcentaje", "0.00");
		jsonObj.put("errorProceso", "true");
	}else{
	
		int		totalNumberOfRegisters  = validaDatos.getNumberOfRegisters();
		int		processedRegisters 		= validaDatos.getProcessedRegisters();
		float 	percent 						= validaDatos.getPercent();
		String   errorProceso				= String.valueOf(validaDatos.hasError());
	
		// Establecer resultado
		jsonObj.put("numeroTotalRegistros",			Comunes.formatoDecimal(totalNumberOfRegisters,0,true));
		jsonObj.put("numeroRegistrosProcesados",	Comunes.formatoDecimal(processedRegisters,0,true));
		jsonObj.put("porcentaje",						Comunes.formatoDecimal(percent,2,true));
		jsonObj.put("errorProceso",						errorProceso);
	}
	
	jsonObj.put("inicioProceso", new Boolean(false));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CAMPOS_ADICIONALES")){
	
	String            camposAdicionales = null;
	
	// Si hay errores en los datos enviados, no hacer nada
	camposAdicionales = repEspecialesProv.getCamposAdicionalesSiag();
	camposAdicionales = camposAdicionales == null?"":camposAdicionales;
	String charCounter = String.valueOf(1536-camposAdicionales.length());
	
	jsonObj.put("charCounter", charCounter);
	jsonObj.put("camposAdicionales", camposAdicionales);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("GUARDAR_CAMPOS_ADICIONALES")){
	String camposAdicionales = request.getParameter("camposAdicionales")==null?"":request.getParameter("camposAdicionales");
	
	camposAdicionales = camposAdicionales == null?"":camposAdicionales;
	// Filtrar saltos de linea
	camposAdicionales = camposAdicionales.replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\r", " ");
	camposAdicionales = camposAdicionales.replaceAll("\"","");
	// Guardar la nueva descripcion de los campos adicionales
	repEspecialesProv.guardarCamposAdicionalesSiag(camposAdicionales);
	// Consultar los cambios hechos
	camposAdicionales = repEspecialesProv.getCamposAdicionalesSiag();
	camposAdicionales = camposAdicionales == null?"":camposAdicionales;
	
	jsonObj.put("camposAdicionales", camposAdicionales);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("MUESTRA_RESUMEN_CARGA_SIAG")){
	String mesInicio = request.getParameter("mesInicial")==null?"":request.getParameter("mesInicial");
	String mesFin = request.getParameter("mesFinal")==null?"":request.getParameter("mesFinal");
	String mensjeErrorCarga = "";
	
	
	boolean  hayRegistrosConError = false;
	boolean  reemplazarRegistros  = true;
	boolean pantallaInicio = false;
	
	int numeroProceso = 0;
	
	try{
		// Obtener Numero de Proceso de Carga
		ValidaDatosSiagThread validaDatos 	= (ValidaDatosSiagThread) request.getSession().getAttribute("ValidaDatosSiag");
		if(validaDatos == null) {
			pantallaInicio = true;
		}
		numeroProceso 	= validaDatos.getProcessID();
	
		// Si existe un thread previo y se esta ejecutando, detenerlo y luego removerlo de sesion
		if(validaDatos != null){
			if(validaDatos.isRunning()) validaDatos.setRunning(false);
			request.getSession().removeAttribute("ValidaDatosSiag");
		}
		
		// Revisar si en la carga se presentaron errores
		hayRegistrosConError = repEspecialesProv.hayErrorCargaSiag(numeroProceso);
		reemplazarRegistros  = true;
		int      mesInicial           = Integer.parseInt(mesInicio);
		int      mesFinal             = Integer.parseInt(mesFin);
		if(!hayRegistrosConError){
			// Insertar registros cargados en COM_CRUCE_SIAG
			repEspecialesProv.insertaRegistrosEnCruceSiag(reemplazarRegistros, numeroProceso,mesInicial,mesFinal);
			// Enviar mensaje indicando que la operacion de carga fue exitosa
			
		}else{
			// Obtener mensajes de error
			mensjeErrorCarga =  repEspecialesProv.getMensajeErrorCargaSiag(numeroProceso).toString();
		}
	
	}catch(Exception e){
		log.error("doMostrarResumenCargaSiag(Exception)");
		log.error("doMostrarResumenCargaSiag(Input Parameters)");
		log.error("Ocurrió un error al mostrar la pantalla de resumen de la Carga de la Base de Datos de SIAG. Se canceló la operación.");
		e.printStackTrace();
		
	}finally{

		 // Borrar registros cargados en la tabla temporal
		 try {
			if(numeroProceso != 0){
			   repEspecialesProv.borrarRegistrosTmpCargaSiag(numeroProceso,1);
			}
		 }catch(Exception e){ e.printStackTrace(); }

		log.info("doMostrarResumenCargaSiag(S)");
	}
		
	jsonObj.put("hayRegistrosConError", new Boolean(hayRegistrosConError));
	jsonObj.put("mensjeErrorCarga", mensjeErrorCarga);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>