Ext.onReady(function() {
	var accion = 'G';
	var noClasificacion = 0;
	
	var  validaDiasPeriodo = function() {
		var mesInicial = Ext.ComponentQuery.query('#mesInicial')[0].getValue();
		var mesFinal = Ext.ComponentQuery.query('#mesFinal')[0].getValue();
		
		var anioInicial = Ext.ComponentQuery.query('#anioInicial')[0].getValue();
		var anioFinal = Ext.ComponentQuery.query('#anioFinal')[0].getValue();
		
		if (parseInt(mesInicial) < 10) {mesInicial = "0" + mesInicial} else {mesInicial = mesInicial};
		if (parseInt(mesFinal) < 10) {mesFinal = "0" + mesFinal} else {mesFinal = mesFinal};
		
		var fechaInicial = "01/" + mesInicial + "/" + anioInicial;
		var fechaFinal = "01/" + mesFinal + "/" + anioFinal;
		
		if (comparaFechas(fechaInicial, fechaFinal)) {
			var diasPeriodo = mtdCalculaPlazo(fechaInicial, fechaFinal);
			if (esAnioBisiesto(parseInt(anioInicial)) && 2 >= parseInt(mesInicial)) {
				diasPeriodo--;
			}
			if (diasPeriodo == 365) {
				var arrayPeriodo = document.forms[0].sPeriodo;					
				for (var i = 0; i < arrayPeriodo.length; i++) {
					document.forms[0].sPeriodo[i].disabled = false;
				}
				document.forms[0].sPeriodo[arrayPeriodo.length - 1].checked = false;
			} else {
				var arrayPeriodo = document.forms[0].sPeriodo;
				for (var i = 0; i < arrayPeriodo.length - 1; i++) {
					document.forms[0].sPeriodo[i].disabled = true;
				}
				document.forms[0].sPeriodo[arrayPeriodo.length - 1].disabled = false;
				document.forms[0].sPeriodo[arrayPeriodo.length - 1].checked = true;
			}			
		} else {
				var arrayPeriodo = document.forms[0].sPeriodo;
				document.forms[0].sPeriodo[arrayPeriodo.length - 1].checked = false;
				for (var i = 0; i < arrayPeriodo.length; i++) {
					document.forms[0].sPeriodo[i].disabled = true;
				}
			alert("El periodo inicial debe ser menor que el periodo final.");
			return;
		}
		
	};
	
	var fnGetNumPymes = function(){
		var numTotPyme = 0;
		var tmpEpos = Ext.ComponentQuery.query('#sNoEpoSelec')[0];
		var store = tmpEpos.toField.store
		store.each(function(record){
			numTotPyme += Number(record.get('NUMPYMES'));
		});
		console.log('numTotPyme === '+numTotPyme);
		return 'Total de pymes: <b>'+numTotPyme+'</b>';
	};
//------------------------------------------------------------------------------------------------
	var procesarSuccessValidIniciales = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			Ext.ComponentQuery.query('#noProceso')[0].setValue(resp.noProceso);
			Ext.ComponentQuery.query('#mesesG')[0].setValue(resp.mesesG);
			Ext.ComponentQuery.query('#meses')[0].setValue(resp.meses);
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
	
	var procesarSuccessCatClasif = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			if(resp.accion === 'G'){
				Ext.Msg.alert('Aviso', '<table width=250><tr><td align="center">El registro fue guardado</td></tr></table>');
			}else if(resp.accion === 'M'){
				Ext.Msg.alert('Aviso', '<table width=250><tr><td align="center">El registro fue modificado</td></tr></table>');
			}else if(resp.accion === 'E'){
				Ext.Msg.alert('Aviso', '<table width=250><tr><td align="center">El registro fue eliminado</td></tr></table>');
			}
			
			Ext.ComponentQuery.query('#nombreClasif')[0].setValue('');
			accion= 'G';
			
			var store = Ext.data.StoreManager.lookup('storeCatClasif');
			store.load();
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	var proccessConsMultiSelectEpos = function(store,  arrRegistros, success, opts) {
			var jsonParams = store.proxy.reader.jsonData;
			var sNoEpoSelec = Ext.ComponentQuery.query('#sNoEpoSelec')[0];
			setTimeout(function(){ 
				sNoEpoSelec.setValue(jsonParams.txtCombo2); 
				Ext.ComponentQuery.query('#cNumPymes')[0].setValue(fnGetNumPymes());
			}, 1000);
			
	};
//-----------------------------------------------------------------------------------------------
	var fnNuevaClasificacion = function(){
		Ext.define('ModCatClasif', {
			extend: 'Ext.data.Model',
			fields: [
				'IC_CLASIFICACION',
				'NOMBRE_CLASIFICACION'
			]
		});
		
		var storeCatClasif= Ext.create('Ext.data.Store', {
			itemId: 'storeCatClasif',
			id: 'storeCatClasif',
			model: 'ModCatClasif',
			proxy: {
				type: 'ajax',
				url : '15redInformacion/15CargaCadenasNExt.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'CONS_CAT_CLASIF'
				},
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			autoLoad: true
		});
		
		
		var gridCatClasif = Ext.create('Ext.grid.Panel', {
			anchor: '100%',
			height: 300,
			frame: true,
			store: storeCatClasif,
			columnLines: true,
			loadMask: true,
			multiSelect: true,
			selModel: {
				pruneRemoved: false
			},
			viewConfig: {
				trackOver: false,
				stripeRows: true,
				deferEmptyText: false,
				emptyText: 'No se encontr� informaci�n'
			},
			columns:[
				{
					text: 'Clasificaci�n',
					dataIndex: 'NOMBRE_CLASIFICACION',
					width: 250,
					align: 'left',
					sortable: false
				},
				{
					text: '<CENTER>Seleccionar</CENTER>', hideable: false,
					width: 100, align: 'center', sortable: false, locked: false,
					xtype: 'actioncolumn',
					items: [
						{
							getClass: function(v, meta, reg) {
								return 'icoModificar';
							},
							getTip: function(v, meta, reg) {
								return 'Modificar';
							},
							handler: function(grid, rowIndex, colIndex) {
								var record = grid.getStore().getAt(rowIndex);
								noClasificacion = record.get('IC_CLASIFICACION');
								Ext.ComponentQuery.query('#nombreClasif')[0].setValue(record.get('NOMBRE_CLASIFICACION'));
								accion = 'M';
							}
						},
						{
							getClass: function(v, meta, reg) {
								return 'icoEliminar';
							},
							getTip: function(v, meta, reg) {
								return 'Eliminar';
							},
							handler: function(grid, rowIndex, colIndex) {
								Ext.MessageBox.show({
									title: 'Mensaje',
									msg: '�Esta seguro de querer elliminar el registro?',
									buttons: Ext.MessageBox.YESNO,
									buttonText:{
										yes: "Aceptar",
										no: "Cancelar"
									},
									fn: function(btn){
										var record = grid.getStore().getAt(rowIndex);
										noClasificacion = record.get('IC_CLASIFICACION');
										Ext.Ajax.request({
											url : '15redInformacion/15CargaCadenasNExt.data.jsp',
											params : {
												informacion: 'GUARDAR_MODIF_CLASIFICACION',
												noClasificacion: noClasificacion,
												accion: 'E'
											},
											callback: procesarSuccessCatClasif
										});
									}
								});
							}
						}
					]
				}
			],
			buttonAlign: 'center',
			buttons: [ {
					text:'Guardar',
					handler: function(){
						var nombreClasif = Ext.ComponentQuery.query('#nombreClasif')[0];
						
						if(nombreClasif.getValue()===''){
							Ext.Msg.alert('Aviso', '<table width=250><tr><td align="center">Debe de capturar un valor</td></tr></table>');
						}else{
							Ext.Ajax.request({
								url : '15redInformacion/15CargaCadenasNExt.data.jsp',
								params : {
									informacion: 'GUARDAR_MODIF_CLASIFICACION',
									nombreClasif: nombreClasif.getValue(),
									noClasificacion: noClasificacion,
									accion: accion
								},
								callback: procesarSuccessCatClasif
							});
						}
					}
				},{
					text:'Cerrar',
					handler: function(){
						Ext.ComponentQuery.query('#winCatClasif')[0].close();
					}
				}
			]
		});
		
		Ext.create('Ext.window.Window', {
			name: 'winCatClasif',
			itemId: 'winCatClasif',
			title: 'Agregar una Clasificaci�n',
			width: 380,
			layout: 'anchor',
			items: [{xtype: 'textfield', fieldLabel: 'Nombre', 
				name: 'nombreClasif', itemId: 'nombreClasif'},gridCatClasif],
			modal: true,
			closeAction: 'destroy',
			listeners:{
				close: function(panel, opt){
					var store = Ext.ComponentQuery.query('#clasificacion')[0].getStore();
					store.load();
				}
			}
		}).show();
	}
	
	var pnlCargaCadenas = Ext.create('Ext.form.Panel',{
		title: '<CENTER>CADENAS</CENTER>',
		frame: true,
		width: 850,
		items:[
			{xtype: 'hidden', itemId: 'noProceso', name:'noProceso'},
			{xtype: 'hidden', itemId: 'meses', name:'meses'},
			{xtype: 'hidden', itemId: 'mesesG', name:'mesesG'},
			{
				xtype: 'container', layout: 'hbox', defaults:{margins:'10 10 10 10'},
				width: 550, style: {margin: '0 auto'},
				items: [
					{ xtype: 'displayfield', value: 'Periodo de' },
					{
						xtype: 'combo', itemId: 'mesInicial', name: 'mesInicial', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15redInformacion/15CargaCadenasNExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MES_INI'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
											var jsonParams = store.proxy.reader.jsonData;
											var mesInicial = Ext.ComponentQuery.query('#mesInicial')[0];
											mesInicial.setValue(jsonParams.iMesActual);
										
									}
								}
						}
					},{
						xtype: 'combo', itemId: 'anioInicial', name: 'anioInicial', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 80,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15redInformacion/15CargaCadenasNExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_ANIO_INI_FIN'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
											var jsonParams = store.proxy.reader.jsonData;
											var anioInicial = Ext.ComponentQuery.query('#anioInicial')[0];
											anioInicial.setValue(jsonParams.iAnioInicial);
										
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					},
					{ xtype: 'displayfield', value: 'a' },
					{
						xtype: 'combo', itemId: 'mesFinal', name: 'mesFinal', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15redInformacion/15CargaCadenasNExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MES_FIN'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
											var jsonParams = store.proxy.reader.jsonData;
											var mesFinal = Ext.ComponentQuery.query('#mesFinal')[0];
											mesFinal.setValue(jsonParams.iMesActual);
										
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					},
					{
						xtype: 'combo', itemId: 'anioFinal', name: 'anioFinal', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 80,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15redInformacion/15CargaCadenasNExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_ANIO_INI_FIN'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
											var jsonParams = store.proxy.reader.jsonData;
											var anioFinal = Ext.ComponentQuery.query('#anioFinal')[0];
											anioFinal.setValue(jsonParams.iAnioActual);
										
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					}
				]
			},
			{xtype: 'container', layout: 'hbox', style: {margin: '0 auto'}, width: 450, items: [
				{
					xtype: 'combo', itemId: 'clasificacion', name: 'clasificacion', forceSelection: true,
					valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
					fieldLabel: 'Clasificaci�n de Dependencias', allowBlank: false, anchor:'70%', labelWidth: 200,
					store: {
							fields: [ 'clave', 'descripcion' ],
							autoLoad: true,
							proxy: {
								type: 'ajax',
								url: '15redInformacion/15CargaCadenasNExt.data.jsp',
								reader: {
									type: 'json',
									root: 'registros'
								},
								extraParams: {
									informacion: 'CONS_CAT_CLASIF_DEP'
								}
							}
					},
					listeners:{
						select: function(combo, records, e){
							var tmpEpos = Ext.ComponentQuery.query('#sNoEpoSelec')[0];
							tmpEpos.clearValue();
							tmpEpos.fromField.store.removeAll();
							
							var storeItemSelect = Ext.ComponentQuery.query('#sNoEpoSelec')[0].getStore();
							storeItemSelect.load({
								params: Ext.apply(pnlCargaCadenas.getForm().getValues())
							});
						}
					}
				},{
					xtype: 'button', text: 'Nueva Clasificaci�n',
					handler: function(){
						fnNuevaClasificacion();
					}
				}]
			},
			{
				xtype: 'checkboxgroup',
				itemId: 'cgClasifEpos',
				anchor: '100%',
				fieldLabel: '',
				style: {margin: '0 auto'},
				columns: 1,
				items: [
					{boxLabel: 'PEF', name: 'eposPEF', inputValue: 'S'},
					{boxLabel: 'Gobiernos y Municipios', name: 'eposGobMun', inputValue: 'S'},
					{boxLabel: 'Privadas', name: 'eposPrivadas', inputValue: 'S'},
					{boxLabel: 'EPO\'s que pertenecen a Credicadenas', name: 'eposCredi', inputValue: 'S'},
					{boxLabel: 'EPO\'s No Clasificadas', name: 'eposNC', inputValue: 'S'}
				],
				listeners:{
					change: function(obj, nVal, oVal, opt){
						var tmpEpos = Ext.ComponentQuery.query('#sNoEpoSelec')[0];
						tmpEpos.clearValue();
						tmpEpos.fromField.store.removeAll();
						
						var storeItemSelect = Ext.ComponentQuery.query('#sNoEpoSelec')[0].getStore();
						storeItemSelect.load({
							params: Ext.apply(pnlCargaCadenas.getForm().getValues())
						});
					}
				}
			},
			{
				xtype: 'itemselector',
				name: 'sNoEpoSelec',
				id: 'sNoEpoSelec',
				itemId: 'sNoEpoSelec',
				anchor: '100%',
				height: 150,
				labelWidth: 60,
				fieldLabel: 'EPO\'s',
				imagePath: '../ux/images/',
				buttons:['add', 'remove'],
				store: {
					fields: [ 'CLAVE', 'DESCRIPCION', 'NUMPYMES' ],
					autoLoad: true,
					itemId: 'storeEpoSelec',
					proxy: {
						type: 'ajax',
						url: '15redInformacion/15CargaCadenasNExt.data.jsp',
						reader: {
							type: 'json',
							root: 'llenaCombo1'
						},
						extraParams: {
							informacion: 'CONS_MULTISELECT_EPOS'
						}
					},
					listeners:{
						load: proccessConsMultiSelectEpos
					}
				},
				displayField: 'DESCRIPCION',
				valueField: 'CLAVE',
				allowBlank: false,
				msgTarget: 'side',
				listeners:{
					change: function(obj, nVal,oVal, opt){
						Ext.ComponentQuery.query('#cNumPymes')[0].setValue(fnGetNumPymes());
					}
				}
			},
			{xtype: 'displayfield', labelSeparator:'',  fieldLabel: ' ', labelWidth:465, itemId: 'cNumPymes', value:'0'}, 
			{
				xtype: 'checkboxgroup',
				itemId: 'cgInfoPresent',
				anchor: '100%',
				allowBlank: false,
				labelWidth: 200,
				labelAlign: 'top',
				fieldLabel: 'Informaci�n a Presentar',
				style: {margin: '0 auto'},
				columns: 1,
				items: [
					{boxLabel: '�ltimos seis meses consecutivos', name: 'sPeriodo', inputValue: '6'},
					{boxLabel: 'Ocho de Doce', name: 'sPeriodo', inputValue: '8'},
					{boxLabel: 'Todos', name: 'sPeriodo', inputValue: 'T'}
				]
			}
		],
		buttonAlign: 'center',
		buttons:[{
			text: 'Regresar', 
			handler: function(){
				window.location = '15redInfoProvCargarInfoExt.jsp';
			}
		},{
			text: 'Generar',
			formBind : true,
			handler: function(){
				Ext.Ajax.request({
					url : '15redInformacion/15CargaCadenasNExt.data.jsp',
					params : Ext.apply(pnlCargaCadenas.getForm().getValues(),{
						informacion: 'GENERAR_ARCHIVO'
					}),
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							Ext.ComponentQuery.query('#btnGSistema')[0].show();
							var resp = Ext.JSON.decode(response.responseText);
							var forma = Ext.getDom('formAux');
							forma.action 		= resp.urlArchivo;
							forma.method 		= 'post';
							forma.target 		= '_self';
							forma.submit();
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});
			}
		},{
			text: 'Guardar en Sistema', 
			hidden: true,
			itemId: 'btnGSistema', 
			handler: function(btn){
				Ext.Ajax.request({
					url : '15redInformacion/15CargaCadenasNExt.data.jsp',
					params : Ext.apply(pnlCargaCadenas.getForm().getValues(),{
						informacion: 'GUARDAR_EN_SISTEMA'
					}),
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							btn.hide();
							Ext.Msg.alert('Aviso', '<table width=250><tr><td align="center">Se realiz� la carga con �xito</td></tr></table>');
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});
			}
		}]
	});
	
	Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(20),
			pnlCargaCadenas,
			NE.util.getEspaciador(20)
		]
	});
	
	Ext.Ajax.request({
		url : '15redInformacion/15CargaCadenasNExt.data.jsp',
		params : {
			informacion: 'VALORES_INICIALES'
		},
		callback: procesarSuccessValidIniciales
	});

});