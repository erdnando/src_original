Ext.onReady(function() {

//------------------------------------------------------------------------------------------------

	var proccessConsMultiSelectEpos = function(store,  arrRegistros, success, opts) {
			var jsonParams = store.proxy.reader.jsonData;
			//var sNoEpoSelec = Ext.ComponentQuery.query('#sNoEpoSelec')[0];
			//setTimeout(function(){ sNoEpoSelec.setValue(jsonParams.txtCombo2); }, 1000);
			//Ext.ComponentQuery.query('#cgClasifEpos')[0].setValue(jsonParams.clasifEpos);
			//Ext.ComponentQuery.query('#cgClasifEpos')[0].setValue([true]);
			
			
	};
//-----------------------------------------------------------------------------------------------
	var pnlCadCompranetIni = Ext.create('Ext.form.Panel',{
		title: '<CENTER>CADENAS - COMPRANET</CENTER>',
		frame: true,
		width: 700,
		items:[
			{xtype: 'hidden', itemId: 'noProceso', name:'noProceso'},
			{xtype: 'hidden', itemId: 'meses', name:'meses'},
			{xtype: 'hidden', itemId: 'mesesG', name:'mesesG'},
			{ xtype: 'displayfield', style: {margin: '0 auto'},
				value: 'Seleccione el periodo y la clasificaci�n de Dependencia de CADENAS con el que se generar� los RFC\'s �nicos'
			},
			{
				xtype: 'container', layout: 'hbox', defaults:{margins:'10 10 10 10'}, style: {margin: '0 auto'},width:550,
				items: [
					{ xtype: 'displayfield', value: 'Periodo de' },
					{
						xtype: 'combo', itemId: 'mesInicial', name: 'mesInicial', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15CadenasCompranetExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var record = store.getAt(0);                     
										Ext.ComponentQuery.query('#mesInicial')[0].setValue(record.get('clave'));
									}
								}
						}
					},{
						xtype: 'combo', itemId: 'anioInicial', name: 'anioInicial', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 80,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15CadenasCompranetExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_ANIOS'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var record = store.getAt(0);                     
										Ext.ComponentQuery.query('#anioInicial')[0].setValue(record.get('clave'));
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					},
					{ xtype: 'displayfield', value: 'a' },
					{
						xtype: 'combo', itemId: 'mesFinal', name: 'mesFinal', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15CadenasCompranetExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var record = store.getAt(0);                     
										Ext.ComponentQuery.query('#mesFinal')[0].setValue(record.get('clave'));
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					},
					{
						xtype: 'combo', itemId: 'anioFinal', name: 'anioFinal', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 80,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15CadenasCompranetExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_ANIOS'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var record = store.getAt(0);                     
										Ext.ComponentQuery.query('#anioFinal')[0].setValue(record.get('clave'));
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					}
				]
			},
			{
				xtype: 'combo', itemId: 'clasificacion', name: 'clasificacion', forceSelection: true,
				valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local', emptyText: 'Seleccione...',
				fieldLabel: 'Clasificaci�n de Dependencias', allowBlank: false, anchor:'70%', labelWidth: 200,
				store: {
						fields: [ 'clave', 'descripcion' ],
						autoLoad: true,
						proxy: {
							type: 'ajax',
							url: '15CadenasCompranetExt.data.jsp',
							reader: {
								type: 'json',
								root: 'registros'
							},
							extraParams: {
								informacion: 'CONS_CAT_CLASIF_DEP'
							}
						}
				},
				listeners:{
					select: function(combo, records, e){
						/*Ext.ComponentQuery.query('#sNoEpoSelec')[0].reset();
						var storeItemSelect = Ext.ComponentQuery.query('#sNoEpoSelec')[0].getStore();
						storeItemSelect.load({
							params: Ext.apply(pnlCadCompranetIni.getForm().getValues())
						});*/
					}
				}
			}
		],
		buttonAlign: 'center',
		buttons:[{
			text: 'Siguiente',
			formBind : true,
			handler: function(){
				pnlCadCompranetIni.hide();
				pnlCadCompranetSig.show();
				
				var tmpEpos = Ext.ComponentQuery.query('#sNoEpoSelec')[0];
				tmpEpos.clearValue();
				tmpEpos.fromField.store.removeAll();
				storeEpos.load({
					params : Ext.apply(pnlCadCompranetIni.getForm().getValues())
				});
			}
		},{
			text: 'Regresar', 
			handler: function(){
				window.location = '15CruceInformacionExt.jsp';
			}
		}]
	});
	
	
	var storeEpos = Ext.create('Ext.data.Store', {
			fields: [ 'CLAVE', 'DESCRIPCION' ],
			autoLoad: false,
			proxy: {
				type: 'ajax',
				url: '15CadenasCompranetExt.data.jsp',
				reader: {
					type: 'json',
					root: 'llenaCombo1'
				},
				extraParams: {
					informacion: 'SIGUIENTE_CADENAS_COMPRANET'
				}
			},
			listeners:{
				load: proccessConsMultiSelectEpos
			}
		});
		
		
	var pnlCadCompranetSig = Ext.create('Ext.form.Panel',{
		title: '<CENTER>CADENAS - COMPRANET</CENTER>',
		frame: true,
		width: 850,
		hidden: true,
		items:[
			{
				xtype: 'container', layout: 'hbox', defaults:{margins:'10 10 10 10'}, width: 550, style: {margin: '0 auto'},
				items: [
					{ xtype: 'displayfield', value: 'Periodo de' },
					{
						xtype: 'combo', itemId: 'mesInicialCom', name: 'mesInicialCom', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15CadenasCompranetExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES_COMPRANET'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var record = store.getAt(0);                     
										Ext.ComponentQuery.query('#mesInicialCom')[0].setValue(record.get('clave'));
									}
								}
						}
					},{
						xtype: 'combo', itemId: 'anioInicialCom', name: 'anioInicialCom', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 80,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15CadenasCompranetExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_ANIOS'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var record = store.getAt(0);                     
										Ext.ComponentQuery.query('#anioInicialCom')[0].setValue(record.get('clave'));
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					},
					{ xtype: 'displayfield', value: 'a' },
					{
						xtype: 'combo', itemId: 'mesFinalCom', name: 'mesFinalCom', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15CadenasCompranetExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES_COMPRANET'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var record = store.getAt(0);                     
										Ext.ComponentQuery.query('#mesFinalCom')[0].setValue(record.get('clave'));
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					},
					{
						xtype: 'combo', itemId: 'anioFinalCom', name: 'anioFinalCom', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 80,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15CadenasCompranetExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_ANIOS'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var record = store.getAt(0);                     
										Ext.ComponentQuery.query('#anioFinalCom')[0].setValue(record.get('clave'));
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					}
				]
			},
			{
				xtype: 'checkboxgroup',
				itemId: 'cgClasifEpos',
				anchor: '100%',
				fieldLabel: '',
				style: {margin: '0 auto'},
				columns: 1,
				items: [
					{boxLabel: 'Federales', name: 'federales', inputValue: '1'},
					{boxLabel: 'Otros', name: 'otros', inputValue: '2'}
				],
				listeners:{
					change: function(obj, nVal, oVal, opt){
						var tmpEpos = Ext.ComponentQuery.query('#sNoEpoSelec')[0];
						tmpEpos.clearValue();
						tmpEpos.fromField.store.removeAll();
						
						storeEpos.load({
							params: Ext.apply(pnlCadCompranetIni.getForm().getValues(),pnlCadCompranetSig.getForm().getValues())
						});
						
						
					}
				}
			},
			{
				xtype: 'itemselector',
				name: 'sNoEpoSelec',
				id: 'sNoEpoSelec',
				itemId: 'sNoEpoSelec',
				anchor: '100%',
				height: 150,
				labelWidth: 60,
				fieldLabel: 'EPO\'s',
				imagePath: '../ux/images/',
				buttons:['add', 'remove'],
				store: storeEpos,
				displayField: 'DESCRIPCION',
				valueField: 'CLAVE',
				allowBlank: false,
				msgTarget: 'side'
			}
		],
		buttonAlign: 'center',
		buttons:[{
			text: 'Generar RFC �nicos',
			formBind : true,
			handler: function(){
				
				Ext.Ajax.request({
					url : '15CadenasCompranetExt.data.jsp',
					params : Ext.apply(pnlCadCompranetIni.getForm().getValues(),pnlCadCompranetSig.getForm().getValues(),{
						informacion: 'GENERAR_RFC_UNICOS'
					}),
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							var resp = Ext.JSON.decode(response.responseText);
							var forma = Ext.getDom('formAux');
							forma.action 		= resp.urlArchivo;
							forma.method 		= 'post';
							forma.target 		= '_self';
							forma.submit();
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});
			
			}
		},{
			text: 'Regresar', 
			handler: function(){
				pnlCadCompranetIni.show();
				pnlCadCompranetSig.hide();
			}
		},{
			text: 'Teminar', 
			handler: function(){
				window.location = '15CruceInformacionExt.jsp';
			}
		}]
	});
	
	
	Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(20),
			pnlCadCompranetIni,
			pnlCadCompranetSig,
			NE.util.getEspaciador(20)
		]
	});
	
	
	
});