Ext.onReady(function() {

//------------------------------------------------------------------------------------------------

	var proccessConsMultiSelectEpos = function(store,  arrRegistros, success, opts) {
		var jsonParams = store.proxy.reader.jsonData;
	};
//-----------------------------------------------------------------------------------------------
	
	var storeEpos = Ext.create('Ext.data.Store', {
		fields: [ 'clave', 'descripcion' ],
		autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '15cruceCompranetSiagExt.data.jsp',
			reader: {
				type: 'json',
				root: 'llenaCombo1'
			},
			extraParams: {
				informacion: 'CONS_CADENAS_COMPRANET'
			}
		},
		listeners:{
			load: proccessConsMultiSelectEpos
		}
	});
	
	var storeCatPortafolio = Ext.create('Ext.data.Store', {
		fields: [ 'clave', 'descripcion' ],
		autoLoad: false,
		proxy: {
			type: 'ajax',
			url: '15cruceCompranetSiagExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CONS_CAT_PORTAFOLIO'
			}
		},
		listeners:{
			load: proccessConsMultiSelectEpos
		}
	});
	
	var pnlCompranetSiagIni = Ext.create('Ext.form.Panel',{
		title: '<CENTER>COMPRANET - SIAG</CENTER>',
		frame: true,
		width: 700,
		items:[
			{ xtype: 'displayfield', style: {margin: '0 auto'},
				value: 'Seleccione los criterios de b�squeda de COMPRANET con los que se realizar� la consulta'
			},
			{
				xtype: 'container', layout: 'hbox', defaults:{margins:'10 10 10 10'}, style: {margin: '0 auto'},width:380,
				items: [
					{ xtype: 'displayfield', value: 'Periodo de' },
					{
						xtype: 'combo', itemId: 'mesInicial', name: 'mesInicial', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15cruceCompranetSiagExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var jsonParams = store.proxy.reader.jsonData;
										Ext.ComponentQuery.query('#mesInicial')[0].setValue(jsonParams.mesInicial);
									}
								}
						}
					},
					{ xtype: 'displayfield', value: 'a' },
					{
						xtype: 'combo', itemId: 'mesFinal', name: 'mesFinal', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15cruceCompranetSiagExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var jsonParams = store.proxy.reader.jsonData;
										Ext.ComponentQuery.query('#mesFinal')[0].setValue(jsonParams.mesFinal);
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					}
				]
			},
			{
				xtype: 'checkboxgroup',
				itemId: 'cgClasifEpos',
				anchor: '100%',
				fieldLabel: '',
				style: {margin: '0 auto'},
				columns: 1,
				items: [
					{boxLabel: 'Federales', name: 'federales', inputValue: '1'},
					{boxLabel: 'Otros', name: 'otros', inputValue: '2'}
				],
				listeners:{
					change: function(obj, nVal, oVal, opt){
						var tmpEpos = Ext.ComponentQuery.query('#sNoEpoSelec')[0];
						tmpEpos.clearValue();
						tmpEpos.fromField.store.removeAll();
						storeEpos.load({
							params: Ext.apply(pnlCompranetSiagIni.getForm().getValues())
						});
						
					}
				}
			},
			{
				xtype: 'itemselector',
				name: 'sNoEpoSelec',
				id: 'sNoEpoSelec',
				itemId: 'sNoEpoSelec',
				anchor: '100%',
				height: 150,
				labelWidth: 60,
				fieldLabel: 'EPO\'s',
				imagePath: '../ux/images/',
				buttons:['add', 'remove'],
				store: storeEpos,
				displayField: 'descripcion',
				valueField: 'clave',
				allowBlank: false,
				msgTarget: 'side'
			}
		],
		buttonAlign: 'center',
		buttons:[{
			text: 'Siguiente',
			formBind : true,
			handler: function(){
				pnlCompranetSiagIni.hide();
				pnlCompranetSiagSig.show();
				
				storeCatPortafolio.load({
					params : Ext.apply(pnlCompranetSiagIni.getForm().getValues())
				});
			}
		},{
			text: 'Regresar', 
			handler: function(){
				window.location = '15CruceInformacionExt.jsp';
			}
		}]
	});
		
	var pnlCompranetSiagSig = Ext.create('Ext.form.Panel',{
		title: '<CENTER>CADENAS - COMPRANET</CENTER>',
		frame: true,
		width: 850,
		hidden: true,
		items:[
			NE.util.getEspaciador(20),
			{ xtype: 'displayfield', style: {margin: '0 auto'}, 
				value: 'Seleccione el Portafolio de SIAG con el que se realizar� el cruce de informaci�n.' 
			},
			{
				anchor: '70%',
				xtype: 'multiselect',
				msgTarget: 'side',
				fieldLabel: 'Portafolio',
				height: 200,
				style: {margin: '0 auto'},
				name: 'portafolio',
				id: 'portafolio',
				itemId: 'portafolio',
				allowBlank: false,
				store: storeCatPortafolio,
				valueField: 'clave',
				displayField: 'descripcion'
			},
			NE.util.getEspaciador(20),
			{ xtype: 'displayfield', style: {margin: '0 auto'}, 
				value: 'Seleccione el periodo del a�o en curso del que se extraer� la informaci�n.' },
			{
				xtype: 'container', layout: 'hbox', defaults:{margins:'10 10 10 10'},
				style: {margin: '0 auto'}, width:380,
				items: [
					{ xtype: 'displayfield', value: 'Periodo: ' },
					{
						xtype: 'combo', itemId: 'mesInicialSiag', name: 'mesInicialSiag', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15cruceCompranetSiagExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES_SIAG'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var jsonParams = store.proxy.reader.jsonData;
										Ext.ComponentQuery.query('#mesInicialSiag')[0].setValue(jsonParams.mesInicial);
									}
								}
						}
					},
					{ xtype: 'displayfield', value: 'hasta' },
					{
						xtype: 'combo', itemId: 'mesFinalSiag', name: 'mesFinalSiag', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15cruceCompranetSiagExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES_SIAG'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {										
										var jsonParams = store.proxy.reader.jsonData;
										Ext.ComponentQuery.query('#mesFinalSiag')[0].setValue(jsonParams.mesFinal);
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					}
				]
			},
			NE.util.getEspaciador(20)
		],
		buttonAlign: 'center',
		buttons:[{
			text: 'Generar RFC\'s con Garant�as',
			formBind : true,
			handler: function(){
				
				Ext.Ajax.request({
					url : '15cruceCompranetSiagExt.data.jsp',
					params : Ext.apply(pnlCompranetSiagIni.getForm().getValues(),pnlCompranetSiagSig.getForm().getValues(),{
						informacion: 'GENERAR_RFC_CON_GARANT'
					}),
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							var resp = Ext.JSON.decode(response.responseText);
							var forma = Ext.getDom('formAux');
							forma.action 		= resp.urlArchivo;
							forma.method 		= 'post';
							forma.target 		= '_self';
							forma.submit();
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});
			
			}
		},{
			text: 'Generar RFC\'s con/sin Garant�as',
			formBind : true,
			handler: function(){
				Ext.Ajax.request({
					url : '15cruceCompranetSiagExt.data.jsp',
					params : Ext.apply(pnlCompranetSiagIni.getForm().getValues(),pnlCompranetSiagSig.getForm().getValues(),{
						informacion: 'GENERAR_RFC_CON_SIN_GARANT'
					}),
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							var resp = Ext.JSON.decode(response.responseText);
							var forma = Ext.getDom('formAux');
							forma.action 		= resp.urlArchivo;
							forma.method 		= 'post';
							forma.target 		= '_self';
							forma.submit();
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});
			}
		
		},{
			text: 'Regresar', 
			handler: function(){
				pnlCompranetSiagSig.hide();
				pnlCompranetSiagSig.getForm().reset();
				pnlCompranetSiagIni.show();
			}
		},{
			text: 'Terminar', 
			handler: function(){
				window.location = '15CruceInformacionExt.jsp';
			}
		}]
	});
	
	
	Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(20),
			pnlCompranetSiagIni,
			pnlCompranetSiagSig,
			NE.util.getEspaciador(20)
		]
	});
	
	
	
});