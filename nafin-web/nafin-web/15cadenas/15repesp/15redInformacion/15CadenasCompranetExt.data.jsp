<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		com.netro.informacion.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		com.netro.seguridadbean.*,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion    = request.getParameter("informacion")      == null ? "": (String)request.getParameter("informacion");
JSONObject jsonObj  = new JSONObject();

String mensaje        = "";
String consulta       = "";
String infoRegresar   = "";

//variables para combos para el rango de periodos
String sMeses[] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
Calendar cal = Calendar.getInstance();
int iAnioActual = cal.get(Calendar.YEAR);
int iMesActual = cal.get(Calendar.MONTH);
int iDiaActual = cal.get(Calendar.DAY_OF_MONTH);
int iAnioInicial = cal.get(Calendar.YEAR) - 1;
int iMesInicial = cal.get(Calendar.MONTH);
int iDiaInicial = cal.get(Calendar.DAY_OF_MONTH);

Calendar gcFechaIniSigMes = new GregorianCalendar(iAnioActual, iMesActual+1, 1);
gcFechaIniSigMes.add(Calendar.DATE, -1);
int iUltimoDia = gcFechaIniSigMes.get(Calendar.DAY_OF_MONTH);

RepEspecialesProv repEspecialesProv = ServiceLocator.getInstance().lookup("RepEspecialesProvEJB", RepEspecialesProv.class);

if(informacion.equals("CONS_CAT_ANIOS")){
	int sAnioIni = cal.get(Calendar.YEAR);
	List lstAnioIni = new ArrayList();
	
	Map hm = new HashMap();	
	hm = new HashMap();
	hm.put("clave",String.valueOf(sAnioIni));
	hm.put("descripcion", String.valueOf(sAnioIni));
	lstAnioIni.add(hm);

	jsonObj.put("registros", JSONArray.fromObject(lstAnioIni));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_MESES")){
	
	List lstMeses = new ArrayList();
	lstMeses = repEspecialesProv.getCatMesesCadenas();

	jsonObj.put("registros", JSONArray.fromObject(lstMeses));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();


}else if(informacion.equals("CONS_CAT_MESES_COMPRANET")){
	
	List lstMeses = new ArrayList();
	lstMeses = repEspecialesProv.getCatMesesCompranet();

	jsonObj.put("registros", JSONArray.fromObject(lstMeses));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_CLASIF_DEP")){
		
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_dependencia");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("COMCAT_DEPEN_CADENAS");
		catalogo.setOrden("cg_descripcion");
		infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("SIGUIENTE_CADENAS_COMPRANET")){
	
	String clasificacion = request.getParameter("clasificacion")==null?"":request.getParameter("clasificacion");
	String sNoEpoSelec = request.getParameter("sNoEpoSelec")==null?"":request.getParameter("sNoEpoSelec");
	
	String federales = request.getParameter("federales")==null?"":request.getParameter("federales");
	String otros = request.getParameter("otros")==null?"":request.getParameter("otros");
	
	String 	clasifEpos = "";
	
	List llenaCombo1  = new ArrayList();
	List llenaCombo2  = new ArrayList();
	
	if(clasificacion.equals("1")){
		clasifEpos = "1";
		federales = "1";
	}else if(clasificacion.equals("2")){
		clasifEpos = "2";
		otros = "2";
	}
	
	//metodo que llena combos izquierdo
	llenaCombo1 = repEspecialesProv.llenaComboCom1( federales, otros, ""  );

	//metodo que llena combo derecho
	if (!sNoEpoSelec.equals("") ) {
	 llenaCombo2 = repEspecialesProv.llenaComboCom2( sNoEpoSelec);
	}
	
	List lstCombo1 = new ArrayList();
	String txtCombo2 = "";
	Map hm = new HashMap();
	if(llenaCombo1.size()>0){
		for(int x=0; x<llenaCombo1.size(); x++){
			hm = new HashMap();
			List datos = (List)llenaCombo1.get(x);
			hm.put("CLAVE", (String)datos.get(0));
			hm.put("DESCRIPCION", (String)datos.get(1));
			lstCombo1.add(hm);
		}
	}
	
	if(llenaCombo2.size()>0){
		for(int x=0; x<llenaCombo2.size(); x++){
			List datos = (List)llenaCombo2.get(x);
			txtCombo2+= ("".equals(txtCombo2))?(String)datos.get(0):(","+(String)datos.get(0));
		}
	}

	
	jsonObj.put("clasifEpos", clasifEpos);
	jsonObj.put("llenaCombo1", JSONArray.fromObject(lstCombo1));
	jsonObj.put("txtCombo2", txtCombo2);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
		
}else if(informacion.equals("GENERAR_RFC_UNICOS")){
	
	Map hm = new HashMap();
	hm.put("mesInicial",request.getParameter("mesInicial")==null?"":request.getParameter("mesInicial"));
	hm.put("mesFinal",request.getParameter("mesFinal")==null?"":request.getParameter("mesFinal"));
	hm.put("anioInicial",request.getParameter("anioInicial")==null?"":request.getParameter("anioInicial"));
	hm.put("anioFinal",request.getParameter("anioFinal")==null?"":request.getParameter("anioFinal"));
	
	hm.put("mesInicialCom",request.getParameter("mesInicialCom")==null?"":request.getParameter("mesInicialCom"));
	hm.put("mesFinalCom",request.getParameter("mesFinalCom")==null?"":request.getParameter("mesFinalCom"));
	
	hm.put("clasificacion",request.getParameter("clasificacion")==null?"":request.getParameter("clasificacion"));
	hm.put("federales",request.getParameter("federales")==null?null:request.getParameter("federales"));
	hm.put("otros",request.getParameter("otros")==null?null:request.getParameter("otros"));
	hm.put("seleciona",request.getParameter("sNoEpoSelec")==null?"":request.getParameter("sNoEpoSelec"));
	
	hm.put("directorio_publicacion", strDirectorioPublicacion);
	hm.put("directorio_plantillas", strDirPlantillas);
	
	String nombreArchivo = repEspecialesProv.getGenerarRFCunicosCadCompranet(hm);
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	

}

%>
<%=infoRegresar%>