Ext.onReady(function() {

	var pnlSeleccionPrincipal = Ext.create('Ext.panel.Panel',{
		title: '<CENTER>Seleccione las bases de datos de las que se requiere generar el reporte</CENTER>',
		frame: true,
		width: 500,
		style: {margin: '0 auto'},
		items:[
			{
				xtype: 'checkboxgroup',
				columns: 1,
				itemId: 'cgCruceInfo',
				style: {margin: '0 auto'},
				items: [
					{boxLabel: 'CADENAS', name: 'cbCadenas', inputValue: 'S'},
					{boxLabel: 'COMPRANET', name: 'cbCompras', inputValue: 'S'},
					{boxLabel: 'SIAG', name: 'cbSiag', inputValue: 'S'}
				]
			}
		],
		buttonAlign: 'center',
		buttons:[{
				xtype:'button',
				text: 'Aceptar',
				width: 60,
				handler: function(){
					var cgCruceInfo = Ext.ComponentQuery.query('#cgCruceInfo')[0];
					var tipoCruce = '';

					if(cgCruceInfo.getValue().cbCadenas === 'S' && cgCruceInfo.getValue().cbSiag === 'S' &&
							cgCruceInfo.getValue().cbCompras !== 'S'){
						Ext.Msg.alert('Aviso', ' No es posible hacer el cruce de información CADENAS-SIAG');

					}else {
						
						if(cgCruceInfo.getValue().cbCadenas === 'S' && cgCruceInfo.getValue().cbCompras === 'S' &&
								cgCruceInfo.getValue().cbSiag === 'S'){
							tipoCruce='CadCompranetSiag';
						}else if(cgCruceInfo.getValue().cbCadenas === 'S' && cgCruceInfo.getValue().cbCompras === 'S'){
							tipoCruce='CadCompranet';
						}else if(cgCruceInfo.getValue().cbCompras === 'S' && cgCruceInfo.getValue().cbSiag === 'S'){
							tipoCruce='CompranetSiag';
						}
						
						if(tipoCruce===''){
							Ext.Msg.alert('Aviso', 'Es necesario seleccionar al menos dos opción');
						}else{
							window.location = '15CruceInformacionExt.jsp?cgCI='+tipoCruce;
						}
					}
				}
			}]
	});

	Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			pnlSeleccionPrincipal,
			NE.util.getEspaciador(20)
		]
	});

});