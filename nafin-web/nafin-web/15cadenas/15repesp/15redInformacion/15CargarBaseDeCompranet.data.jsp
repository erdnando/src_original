<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		com.netro.informacion.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		com.netro.seguridadbean.*,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion    = request.getParameter("informacion")      == null ? "": (String)request.getParameter("informacion");
JSONObject jsonObj  = new JSONObject();
String infoRegresar   = "";

//variables para combos para el rango de periodos
String sMeses[] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
RepEspecialesProv repEspecialesProv = ServiceLocator.getInstance().lookup("RepEspecialesProvEJB", RepEspecialesProv.class);


if(informacion.equals("VALORES_INICIALES")){
	ValidaDatosCompranetThread validaDatos = (ValidaDatosCompranetThread) request.getSession().getAttribute("ValidaDatosCompranet");
	if(validaDatos != null){
		if(validaDatos.isRunning()) validaDatos.setRunning(false);
		request.getSession().removeAttribute("ValidaDatosCompranet");
	}
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("CONS_CAT_MES")){
	
	List catalogoMeses 		= repEspecialesProv.getCatalogoMesesHastaMesActual();
	
	jsonObj.put("registros", JSONArray.fromObject(catalogoMeses));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("PROCESAR_ARCHIVO")){
	
	String mesInicio = request.getParameter("mesInicial")==null?"":request.getParameter("mesInicial");
	String mesFin = request.getParameter("mesFinal")==null?"":request.getParameter("mesFinal");
	String reemReg = request.getParameter("reemplazarRegistros")==null?"":request.getParameter("reemplazarRegistros");
	String urlArchivo = request.getParameter("urlArchivo")==null?"":request.getParameter("urlArchivo");
	
	boolean reemplazarRegistros  = "true".equals(reemReg)?true:false;
	
	int numeroProceso = Integer.parseInt(repEspecialesProv.getIdCargaCompranet());
	
	FiltroFechaCompranet filtroFechas = new FiltroFechaCompranet(Integer.parseInt(mesInicio),Integer.parseInt(mesFin),reemplazarRegistros);
	
	repEspecialesProv.insertaRegistrosTmpCompranet(strDirectorioTemp+urlArchivo, numeroProceso, filtroFechas);
	
	ValidaDatosCompranetThread validaDatos = new ValidaDatosCompranetThread();
	request.getSession().setAttribute("ValidaDatosCompranet",validaDatos);
	validaDatos.setProcessID(numeroProceso);
	validaDatos.setRunning(true);
	new Thread(validaDatos).start();
	
	jsonObj.put("inicioProceso", new Boolean(true));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("PROCESANDO_ARCHIVO")){
	ValidaDatosCompranetThread validaDatos = (ValidaDatosCompranetThread) request.getSession().getAttribute("ValidaDatosCompranet");
	
	if(validaDatos == null){
		jsonObj.put("numeroTotalRegistros", "0");
		jsonObj.put("numeroRegistrosProcesados", "0");
		jsonObj.put("porcentaje", "0.00");
		jsonObj.put("errorProceso", "true");
	}else{
	
		int		totalNumberOfRegisters  = validaDatos.getNumberOfRegisters();
		int		processedRegisters 		= validaDatos.getProcessedRegisters();
		float 	percent 						= validaDatos.getPercent();
		String   errorProceso				= String.valueOf(validaDatos.hasError());
	
		// Establecer resultado
		jsonObj.put("numeroTotalRegistros",			Comunes.formatoDecimal(totalNumberOfRegisters,0,true));
		jsonObj.put("numeroRegistrosProcesados",	Comunes.formatoDecimal(processedRegisters,0,true));
		jsonObj.put("porcentaje",						Comunes.formatoDecimal(percent,2,true));
		jsonObj.put("errorProceso",						errorProceso);
	}
	
	jsonObj.put("inicioProceso", new Boolean(false));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_EPOS_COMPRANET")){
	
	List listaCatalogo = repEspecialesProv.getCatalogoEposCompranet("no_nuevos");
	
	jsonObj.put("csConsCat", "S");
	jsonObj.put("registros", JSONArray.fromObject(listaCatalogo));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("GUARDAR_EPOS_COMPRANET")){
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String numeroEpoNuevo = "";
	String numeroEpoOriginal = "";
	String icEpoCompranet = "";
	
	List arrRegistros = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistros.iterator();
	
	while (itReg.hasNext()) {
		JSONObject reg = (JSONObject)itReg.next();
		Map hm = new HashMap();
		String strKey = "";
		for ( Object key : reg.keySet() ) {
			strKey = (String)key;
			hm.put(strKey,reg.getString(strKey));
		}
		
		numeroEpoNuevo = (String)hm.get("numeroEpo");
		numeroEpoOriginal = (String)hm.get("numeroEpoOriginal");
		icEpoCompranet = (String)hm.get("icEpoCompranet");
		
		if(!numeroEpoNuevo.equals(numeroEpoOriginal)){
			repEspecialesProv.updateNumeroEpoCatalogoCompranet(icEpoCompranet,numeroEpoNuevo);
		}
	}
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("MUESTRA_CAT_EPOS_COMPRANET")){
	
	// Obtener Numero de Proceso de Carga
	ValidaDatosCompranetThread validaDatos 	= (ValidaDatosCompranetThread) request.getSession().getAttribute("ValidaDatosCompranet");
	int numeroProceso 	= validaDatos.getProcessID();

	// Borrar las EPOs Nuevas o EPOs sin Clasificacion que pudieran haber quedado registradas por causa de una
	// carga previa sin finalizar.
	repEspecialesProv.borraEposNuevasOSinClasificacion();
	// Registrar las nuevas EPOs encontradas en el catalogo de EPOs de Compranet
	repEspecialesProv.insertaEposNuevasEnCatalogoCompranet(numeroProceso);
	// Insertar las nuevas PYMES encontradas en el catalogo de PYMES de Compranet
	repEspecialesProv.insertaPymesNuevasEnCatalogoCompranet(numeroProceso);
	// Asignar numeros de EPO a las EPOs recien insertadas
	repEspecialesProv.asignaNumeroEPOCatalogoCOMPRANET();
	// Obtener Catalogo de Epos Compranet
	List listaCatalogo = repEspecialesProv.getCatalogoEposCompranet();
	
	jsonObj.put("csConsCat", "N");
	jsonObj.put("registros", JSONArray.fromObject(listaCatalogo));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("ACEPTAR_EPOS_COMPRANET")){
	String mesInicio = request.getParameter("mesInicial")==null?"":request.getParameter("mesInicial");
	String mesFin = request.getParameter("mesFinal")==null?"":request.getParameter("mesFinal");
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String mensjeErrorCarga = "";
	
	boolean  hayRegistrosConError = false;
	boolean  reemplazarRegistros  = true;
	boolean pantallaInicio = false;
	
	int numeroProceso = 0;
	
	try{
		ValidaDatosCompranetThread validaDatos 	= (ValidaDatosCompranetThread) request.getSession().getAttribute("ValidaDatosCompranet");
		if(validaDatos == null) {
			pantallaInicio = true;
		}
		
		// Obtener Numero de Proceso de Carga
		numeroProceso = validaDatos.getProcessID();
		
		// Actualizar el Numbero Epo de aquellas Epos cuyo numero fue cambiado
		String numeroEpoNuevo 		= "";
		String numeroEpoOriginal 	= "";
		String icEpoCompranet		= "";
		
		// Obtener la Lista del Catalogo de Epos
		List arrRegistros = JSONArray.fromObject(jsonRegistros);
		Iterator itReg = arrRegistros.iterator();
		
		while (itReg.hasNext()) {
			JSONObject reg = (JSONObject)itReg.next();
			Map hm = new HashMap();
			String strKey = "";
			for ( Object key : reg.keySet() ) {
				strKey = (String)key;
				hm.put(strKey,reg.getString(strKey));
			}
			
			numeroEpoNuevo = (String)hm.get("numeroEpo");
			numeroEpoOriginal = (String)hm.get("numeroEpoOriginal");
			icEpoCompranet = (String)hm.get("icEpoCompranet");
			
			if(!numeroEpoNuevo.equals(numeroEpoOriginal)){
				repEspecialesProv.updateNumeroEpoCatalogoCompranet(icEpoCompranet,numeroEpoNuevo);
			}
		}
		
		// Si existe un thread previo y se esta ejecutando, detenerlo y luego removerlo de sesion
		if(validaDatos != null){
			if(validaDatos.isRunning()) validaDatos.setRunning(false);
			request.getSession().removeAttribute("ValidaDatosCompranet");
		}
		
		// Revisar si en la carga se presentaron errores
		hayRegistrosConError = repEspecialesProv.hayErrorCargaCompranet(numeroProceso);
		reemplazarRegistros  = true;
		int      mesInicial           = Integer.parseInt(mesInicio);
		int      mesFinal             = Integer.parseInt(mesFin);
		if(!hayRegistrosConError){
		
			// Una vez aprobadas las asignaciones al catalogo de EPOs por parte del usuario, quitar el estatus nuevos de las EPOs
			// recien registradas
			repEspecialesProv.doSuprimeEstatusNuevoEposCompranet();
			// Asignar ID a las PYMES
			repEspecialesProv.asignaIdPymeCompranet(numeroProceso);
			// Asignar ID a las EPOS
			repEspecialesProv.asignaIdEpoCompranet(numeroProceso);
			// Asignar ID Correspondiente a la Clasificacion de la Dependencia
			repEspecialesProv.asignaClasificacionDependenciaEpoCompranet(numeroProceso);
		
			// Insertar registros cargados en COM_CRUCE_COMPRANET
			repEspecialesProv.insertaRegistrosEnCruceCompranet(reemplazarRegistros, numeroProceso,mesInicial,mesFinal);
			// Enviar mensaje indicando que la operacion de carga fue exitosa  (exito)
			
		
		}else{
			// Obtener mensajes de error
			mensjeErrorCarga = repEspecialesProv.getMensajeErrorCargaCompranet(numeroProceso).toString();
			// Mostrar Botones de Resumen para Carga con Error (error)
		}
		
	}catch(Exception e){
			log.error("doMostrarResumenCargaCompranet(Exception)");
			log.error("doMostrarResumenCargaCompranet(Input Parameters)");
			log.error("Ocurrió un error al mostrar la pantalla de resumen de la Carga de la Base de Datos de COMPRANET. Se canceló la operación.");
			e.printStackTrace();
			
	}finally{
	 // Borrar registros cargados en la tabla temporal
		try {
			if(numeroProceso != 0){
				repEspecialesProv.borrarRegistrosTmpCargaCompranet(numeroProceso,1);
			}
		}catch(Exception e){ e.printStackTrace(); }
		
		log.info("doMostrarResumenCargaCompranet(S)");
	}
	
	jsonObj.put("hayRegistrosConError", new Boolean(hayRegistrosConError));
	jsonObj.put("mensjeErrorCarga", mensjeErrorCarga);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>