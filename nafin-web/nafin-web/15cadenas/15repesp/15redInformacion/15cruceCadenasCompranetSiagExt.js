Ext.onReady(function() {

var mostararBtnGuardado = false;
var idProcesoCruce = '';

//-----------------------------------------------------------------------------------------------

	var fnGetValuesForms = function(){
		var values = Ext.apply(pnlCadenas.getForm().getValues(), pnlCompranet.getForm().getValues());
		values = Ext.apply(values,pnlSiag.getForm().getValues());
		return values;
	};
//------------------------------------------------------------------------------------------------
	var procesarSuccessValidIniciales = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			mostararBtnGuardado = resp.showGuardarRFCsParaSIAG;
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
    };
	
	var proccessConsMultiSelectEpos = function(store,  arrRegistros, success, opts) {
			var jsonParams = store.proxy.reader.jsonData;
	};
//-----------------------------------------------------------------------------------------------
	var pnlCadenas = Ext.create('Ext.form.Panel',{
		title: '<CENTER>CADENAS - COMPRANET - SIAG </CENTER>',
		frame: true,
		width: 700,
		items:[
			{ xtype: 'displayfield', style: {margin: '0 auto'}, 
				value: 'Seleccione el periodo y la clasificaci�n de Dependencia de CADENAS con el que se generar�n los RFC\'s �nicos.'
			},
			{
				xtype: 'container', layout: 'hbox', defaults:{margins:'10 10 10 10'}, style: {margin: '0 auto'},width:380,
				items: [
					{ xtype: 'displayfield', value: 'Periodo de' },
					{
						xtype: 'combo', itemId: 'mesInicialCadenas', name: 'mesInicialCadenas', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15cruceCadenasCompranetSiagExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES_CAD'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var jsonParams = store.proxy.reader.jsonData;
										Ext.ComponentQuery.query('#mesInicialCadenas')[0].setValue(jsonParams.mesInicial);
									}
								}
						}
					},
					{ xtype: 'displayfield', value: 'a' },
					{
						xtype: 'combo', itemId: 'mesFinalCadenas', name: 'mesFinalCadenas', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15cruceCadenasCompranetSiagExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES_CAD'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var jsonParams = store.proxy.reader.jsonData;
										Ext.ComponentQuery.query('#mesFinalCadenas')[0].setValue(jsonParams.mesFinal);
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					}
				]
			},
			{
				xtype: 'combo', itemId: 'clasificacionDependencia', name: 'clasificacionDependencia', forceSelection: true,
				valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local', emptyText: 'Seleccione...',
				fieldLabel: 'Clasificaci�n de Dependencias', allowBlank: false, anchor:'70%', labelWidth: 200,
				store: {
						fields: [ 'clave', 'descripcion' ],
						autoLoad: true,
						proxy: {
							type: 'ajax',
							url: '15cruceCadenasCompranetSiagExt.data.jsp',
							reader: {
								type: 'json',
								root: 'registros'
							},
							extraParams: {
								informacion: 'CONS_CAT_CLASIF_DEP'
							}
						}
				},
				listeners:{
					select: function(combo, records, e){
					
					}
				}
			}
		],
		buttonAlign: 'center',
		buttons:[{
			text: 'Siguiente',
			formBind : true,
			handler: function(){
				pnlCadenas.hide();
				pnlCompranet.show();
				
				var tmpEpos = Ext.ComponentQuery.query('#sNoEpoSelec')[0];
				tmpEpos.clearValue();
				tmpEpos.fromField.store.removeAll();
				storeEpos.load({ 
					params : Ext.apply(pnlCompranet.getForm().getValues())
				});
			}
		},{
			text: 'Regresar', 
			handler: function(){
				window.location = '15CruceInformacionExt.jsp';
			}
		}]
	});
	
	
	var storeEpos = Ext.create('Ext.data.Store', {
			fields: [ 'clave', 'descripcion' ],
			autoLoad: false,
			proxy: {
				type: 'ajax',
				url: '15cruceCadenasCompranetSiagExt.data.jsp',
				reader: {
					type: 'json',
					root: 'llenaCombo1'
				},
				extraParams: {
					informacion: 'CONS_CADENAS_COMPRANET'
				}
			},
			listeners:{
				load: proccessConsMultiSelectEpos
			}
		});
	
	var storeCatPortafolio = Ext.create('Ext.data.Store', {
		fields: [ 'clave', 'descripcion' ],
		autoLoad: false,
		proxy: {
			type: 'ajax',
			url: '15cruceCadenasCompranetSiagExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CONS_CAT_PORTAFOLIO'
			}
		},
		listeners:{
			load: proccessConsMultiSelectEpos
		}
	});
		
	var pnlCompranet = Ext.create('Ext.form.Panel',{
		title: '<CENTER>CADENAS - COMPRANET - SIAG</CENTER>',
		frame: true,
		width: 700,
		hidden:true,
		items:[
			{
				xtype: 'container', layout: 'hbox', defaults:{margins:'10 10 10 10'}, style: {margin: '0 auto'},width:380,
				items: [
					{ xtype: 'displayfield', value: 'Periodo de' },
					{
						xtype: 'combo', itemId: 'mesInicial', name: 'mesInicial', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15cruceCadenasCompranetSiagExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES_COMPRANET'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var jsonParams = store.proxy.reader.jsonData;
										Ext.ComponentQuery.query('#mesInicial')[0].setValue(jsonParams.mesInicial);
									}
								}
						}
					},
					{ xtype: 'displayfield', value: 'a' },
					{
						xtype: 'combo', itemId: 'mesFinal', name: 'mesFinal', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15cruceCadenasCompranetSiagExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES_COMPRANET'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var jsonParams = store.proxy.reader.jsonData;
										Ext.ComponentQuery.query('#mesFinal')[0].setValue(jsonParams.mesFinal);
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					}
				]
			},
			{
				xtype: 'checkboxgroup',
				itemId: 'cgClasifEpos',
				anchor: '100%',
				fieldLabel: '',
				style: {margin: '0 auto'},
				columns: 1,
				items: [
					{boxLabel: 'Federales', name: 'federales', inputValue: '1'},
					{boxLabel: 'Otros', name: 'otros', inputValue: '2'}
				],
				listeners:{
					change: function(obj, nVal, oVal, opt){
						var tmpEpos = Ext.ComponentQuery.query('#sNoEpoSelec')[0];
						tmpEpos.clearValue();
						tmpEpos.fromField.store.removeAll();
						storeEpos.load({
							params: Ext.apply(pnlCompranet.getForm().getValues())
						});
						
					}
				}
			},
			{
				xtype: 'itemselector',
				name: 'sNoEpoSelec',
				id: 'sNoEpoSelec',
				itemId: 'sNoEpoSelec',
				anchor: '100%',
				height: 150,
				labelWidth: 60,
				fieldLabel: 'EPO\'s',
				imagePath: '../ux/images/',
				buttons:['add', 'remove'],
				store: storeEpos,
				displayField: 'descripcion',
				valueField: 'clave',
				allowBlank: false,
				msgTarget: 'side'
			}
		],
		buttonAlign: 'center',
		buttons:[{
			text: 'Realizar Cruce',
			formBind : true,
			handler: function(){
				Ext.Ajax.request({
					url : '15cruceCadenasCompranetSiagExt.data.jsp',
					params : Ext.apply(pnlCadenas.getForm().getValues(),pnlCompranet.getForm().getValues(),{
						informacion: 'REALIZAR_CRUCE_INI'
					}),
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							var resp = Ext.JSON.decode(response.responseText);
							idProcesoCruce = resp.idProcesoCruce;
							
							Ext.ComponentQuery.query('#btnDescargarComp')[0].show();
							Ext.ComponentQuery.query('#btnSiguienteComp')[0].show();
							
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});
			}
		},{
			text: 'Descargar RFC\s �nicos', 
			itemId: 'btnDescargarComp',
			hidden: true,
			handler: function(){
				Ext.Ajax.request({
					url : '15cruceCadenasCompranetSiagExt.data.jsp',
					params :{
						informacion: 'GENERAR_RFC_UNICOS',
						idProcesoCruce: idProcesoCruce
					},
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							var resp = Ext.JSON.decode(response.responseText);
							var forma = Ext.getDom('formAux');
							forma.action 		= resp.urlArchivo;
							forma.method 		= 'post';
							forma.target 		= '_self';
							forma.submit();
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});
			}
		},{
			text: 'Siguiente',
			itemId: 'btnSiguienteComp',
			hidden: true,
			handler: function(){
				pnlCompranet.hide();
				pnlSiag.show();
				storeCatPortafolio.load();
			}
		},{
			text: 'Regresar', 
			handler: function(){
				
				pnlCompranet.hide();
				pnlCompranet.getForm().reset();
				Ext.ComponentQuery.query('#btnDescargarComp')[0].hide();
				Ext.ComponentQuery.query('#btnSiguienteComp')[0].hide();
				pnlCadenas.show();
				
			}
		},{
			text: 'Terminar', 
			handler: function(){
				window.location = '15CruceInformacionExt.jsp';
			}
		}]
	});
	
	var pnlSiag = Ext.create('Ext.form.Panel',{
		title: '<CENTER>CADENAS - COMPRANET - SIAG</CENTER>',
		frame: true,
		width: 850,
		hidden: true,
		items:[
			NE.util.getEspaciador(20),
			{ xtype: 'displayfield', style: {margin: '0 auto'}, 
				value: 'Seleccione el Portafolio de SIAG con el que se realizar� el cruce de informaci�n de proveedores.' 
			},
			{
				anchor: '70%',
				xtype: 'multiselect',
				msgTarget: 'side',
				fieldLabel: 'Portafolio',
				height: 200,
				style: {margin: '0 auto'},
				name: 'portafolio',
				id: 'portafolio',
				itemId: 'portafolio',
				allowBlank: false,
				store: storeCatPortafolio,
				valueField: 'clave',
				displayField: 'descripcion'
			},
			NE.util.getEspaciador(20),
			{ xtype: 'displayfield', style: {margin: '0 auto'}, 
				value: 'Seleccione el periodo del a�o en curso del que se extraer� la informaci�n.' },
			{
				xtype: 'container', layout: 'hbox', defaults:{margins:'10 10 10 10'},
				style: {margin: '0 auto'}, width:380,
				items: [
					{ xtype: 'displayfield', value: 'Periodo: ' },
					{
						xtype: 'combo', itemId: 'mesInicialSiag', name: 'mesInicialSiag', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15cruceCadenasCompranetSiagExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES_SIAG'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {
										var jsonParams = store.proxy.reader.jsonData;
										Ext.ComponentQuery.query('#mesInicialSiag')[0].setValue(jsonParams.mesInicial);
									}
								}
						}
					},
					{ xtype: 'displayfield', value: 'hasta' },
					{
						xtype: 'combo', itemId: 'mesFinalSiag', name: 'mesFinalSiag', forceSelection: true,
						valueField: 'clave', displayField: 'descripcion', typeAhead: true, queryMode: 'local',
						fieldLabel: '', allowBlank: false, width: 100,
						store: {
								fields: [ 'clave', 'descripcion' ],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: '15cruceCadenasCompranetSiagExt.data.jsp',
									reader: {
										type: 'json',
										root: 'registros'
									},
									extraParams: {
										informacion: 'CONS_CAT_MESES_SIAG'
									}
								},
								listeners:{
									load: function(store,  arrRegistros, success, opts) {										
										var jsonParams = store.proxy.reader.jsonData;
										Ext.ComponentQuery.query('#mesFinalSiag')[0].setValue(jsonParams.mesFinal);
									}
								}
						},
						listeners:{
							select: function(combo, records, e){
								
							}
						}
					}
				]
			},
			NE.util.getEspaciador(20)
		],
		buttonAlign: 'center',
		buttons:[{
			text: 'Realizar Cruce',
			formBind : true,
			handler: function(){
				Ext.Ajax.request({
					url : '15cruceCadenasCompranetSiagExt.data.jsp',
					params : Ext.apply(fnGetValuesForms(),{
						informacion: 'REALIZAR_CRUCE_SIAG',
						idProcesoCruce: idProcesoCruce
					}),
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							var resp = Ext.JSON.decode(response.responseText);
							Ext.ComponentQuery.query('#btnSiagGenerarRFCconGarant')[0].show();
							Ext.ComponentQuery.query('#btnSiagGenerarRFCsinGarant')[0].show();
							if(mostararBtnGuardado){
								Ext.ComponentQuery.query('#btnSiagGuardarRFCparaSiag')[0].show();
							}
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});		
			}
		},{
			text: 'Generar RFC\'s con Garant�as',
			formBind : true,
			hidden: true,
			itemId: 'btnSiagGenerarRFCconGarant',
			handler: function(){
				
				Ext.Ajax.request({
					url : '15cruceCadenasCompranetSiagExt.data.jsp',
					params : Ext.apply(fnGetValuesForms(),{
						informacion: 'GENERAR_RFC_UNICOS_CON_GARANT',
						idProcesoCruce: idProcesoCruce
					}),
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							var resp = Ext.JSON.decode(response.responseText);
							var forma = Ext.getDom('formAux');
							forma.action 		= resp.urlArchivo;
							forma.method 		= 'post';
							forma.target 		= '_self';
							forma.submit();
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});
			
			}
		},{
			text: 'Generar RFC\'s con/sin Garant�as',
			formBind : true,
			hidden: true,
			itemId: 'btnSiagGenerarRFCsinGarant',
			handler: function(){
				Ext.Ajax.request({
					url : '15cruceCadenasCompranetSiagExt.data.jsp',
					params : Ext.apply(fnGetValuesForms(),{
						informacion: 'GENERAR_RFC_UNICOS_SIN_GARANT',
						idProcesoCruce: idProcesoCruce
					}),
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							var resp = Ext.JSON.decode(response.responseText);
							var forma = Ext.getDom('formAux');
							forma.action 		= resp.urlArchivo;
							forma.method 		= 'post';
							forma.target 		= '_self';
							forma.submit();
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});
			}
		
		},{
			text: 'Guardar RFC\'s para SIAG',
			hidden: true,
			itemId: 'btnSiagGuardarRFCparaSiag', 
			handler: function(){
				Ext.Ajax.request({
					url : '15cruceCadenasCompranetSiagExt.data.jsp',
					params : {
						informacion: 'GUARDAR_RFC_PARA_SIAG',
						idProcesoCruce: idProcesoCruce
					},
					callback: function(options, success, response) {
						if (success === true && Ext.JSON.decode(response.responseText).success === true) {
							Ext.Msg.alert('Aviso','La informaci�n se insert� con �xito.');
						} else {
							NE.util.mostrarErrorPeticion(response);
						}
					}
				});
			}
		},{
			text: 'Regresar', 
			handler: function(){
				pnlCadenas.hide();
				pnlSiag.hide();
				Ext.ComponentQuery.query('#btnSiagGenerarRFCconGarant')[0].hide();
				Ext.ComponentQuery.query('#btnSiagGenerarRFCsinGarant')[0].hide();
				Ext.ComponentQuery.query('#btnSiagGuardarRFCparaSiag')[0].hide();
				pnlCompranet.show();
			}
		},{
			text: 'Terminar', 
			handler: function(){
				window.location = '15CruceInformacionExt.jsp';
			}
		}]
	});
	
	Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(20),
			pnlCadenas,
			pnlCompranet,
			pnlSiag,
			NE.util.getEspaciador(20)
		]
	});
	
	Ext.Ajax.request({
		url : '15cruceCadenasCompranetSiagExt.data.jsp',
		params : {
			informacion: 'VALORES_INICIALES'
		},
		callback: procesarSuccessValidIniciales
	});	
	
});