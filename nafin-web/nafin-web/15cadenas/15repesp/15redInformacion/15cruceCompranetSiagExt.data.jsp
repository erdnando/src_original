<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		com.netro.informacion.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		com.netro.seguridadbean.*,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion    = request.getParameter("informacion")      == null ? "": (String)request.getParameter("informacion");
JSONObject jsonObj  = new JSONObject();

String mensaje        = "";
String consulta       = "";
String infoRegresar   = "";

//variables para combos para el rango de periodos
String sMeses[] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
Calendar cal = Calendar.getInstance();
int iAnioActual = cal.get(Calendar.YEAR);
int iMesActual = cal.get(Calendar.MONTH);
int iDiaActual = cal.get(Calendar.DAY_OF_MONTH);
int iAnioInicial = cal.get(Calendar.YEAR) - 1;
int iMesInicial = cal.get(Calendar.MONTH);
int iDiaInicial = cal.get(Calendar.DAY_OF_MONTH);

Calendar gcFechaIniSigMes = new GregorianCalendar(iAnioActual, iMesActual+1, 1);
gcFechaIniSigMes.add(Calendar.DATE, -1);
int iUltimoDia = gcFechaIniSigMes.get(Calendar.DAY_OF_MONTH);

RepEspecialesProv repEspecialesProv = ServiceLocator.getInstance().lookup("RepEspecialesProvEJB", RepEspecialesProv.class);

if(informacion.equals("VALORES_INICIALES")){
	String mesInicial = String.valueOf(Fecha.getMesActual());
	String mesFinal = String.valueOf(Fecha.getMesActual());
	
	jsonObj.put("mesInicial", mesInicial);
	jsonObj.put("mesFinal", mesFinal);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("CONS_CAT_MESES")){

	// Obtener Catalogo de Meses
	List catalogoMeses 		= repEspecialesProv.getCatalogoMesesHastaUltimoMes("COM_CRUCE_COMPRANET");

	String mesInicial = String.valueOf(Fecha.getMesActual());
	String mesFinal = String.valueOf(Fecha.getMesActual());
	
	jsonObj.put("mesInicial", mesInicial);
	jsonObj.put("mesFinal", mesFinal);
	jsonObj.put("registros", JSONArray.fromObject(catalogoMeses));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_MESES_SIAG")){

	// Obtener Catalogo de Meses
	List catalogoMeses 		= repEspecialesProv.getCatalogoMesesHastaUltimoMes("COM_CRUCE_SIAG");

	String mesInicial = String.valueOf(Fecha.getMesActual());
	String mesFinal = String.valueOf(Fecha.getMesActual());
	
	jsonObj.put("mesInicial", mesInicial);
	jsonObj.put("mesFinal", mesFinal);
	jsonObj.put("registros", JSONArray.fromObject(catalogoMeses));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();


}else if(informacion.equals("CONS_CADENAS_COMPRANET")){
	
	String sNoEpoSelec = request.getParameter("sNoEpoSelec")==null?"":request.getParameter("sNoEpoSelec");
	String federales = request.getParameter("federales")==null?"":request.getParameter("federales");
	String otros = request.getParameter("otros")==null?"":request.getParameter("otros");
	
	List catalogoEpos 		= new ArrayList();
	
	// Construir parametros de consulta
	List tipoClasificacion = new ArrayList();
	if("1".equals(federales)) tipoClasificacion.add("1");
	if("2".equals(otros)) 		tipoClasificacion.add("2");
	
	String eposSeleccionadas 		= sNoEpoSelec;
		List listaEposSeleccionadas 	= null;
		if(eposSeleccionadas != null && !eposSeleccionadas.trim().equals("")){
			String []lista = eposSeleccionadas.split(",");
			listaEposSeleccionadas = new ArrayList(Arrays.asList(lista));
		}
	
	if("1".equals(federales) || "2".equals(otros) || listaEposSeleccionadas!=null && listaEposSeleccionadas.size()>0) {
		catalogoEpos 	= repEspecialesProv.getCatalogoEposCompranet(tipoClasificacion,listaEposSeleccionadas);
		catalogoEpos = catalogoEpos == null?new ArrayList():catalogoEpos;
	}else{
		catalogoEpos 		= repEspecialesProv.getCatalogoEposCompranet(null,null);
	}
	
	jsonObj.put("llenaCombo1", JSONArray.fromObject(catalogoEpos));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_PORTAFOLIO")){

	// Obtener Catalogo de Meses
	List catalogoPortafolio = repEspecialesProv.getCatalogoPortafolioSiag();
	
	jsonObj.put("registros", JSONArray.fromObject(catalogoPortafolio));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
		
}else if(informacion.equals("GENERAR_RFC_CON_GARANT")){
	
	Map hm = new HashMap();
	hm.put("mesInicial",request.getParameter("mesInicial")==null?"":request.getParameter("mesInicial"));
	hm.put("mesFinal",request.getParameter("mesFinal")==null?"":request.getParameter("mesFinal"));
	
	hm.put("mesInicialSiag",request.getParameter("mesInicialSiag")==null?"":request.getParameter("mesInicialSiag"));
	hm.put("mesFinalSiag",request.getParameter("mesFinalSiag")==null?"":request.getParameter("mesFinalSiag"));
	
	hm.put("federales",request.getParameter("federales")==null?null:request.getParameter("federales"));
	hm.put("otros",request.getParameter("otros")==null?null:request.getParameter("otros"));
	hm.put("eposSeleccionadas",request.getParameter("sNoEpoSelec")==null?"":request.getParameter("sNoEpoSelec"));
	hm.put("portafolio",request.getParameter("portafolio")==null?"":request.getParameter("portafolio"));
	
	hm.put("directorioPublicacion", strDirectorioPublicacion);
	hm.put("directorioPlantillas", strDirPlantillas);
	
	String nombreArchivo = repEspecialesProv.getGenerarRFCconGarant(hm);
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("GENERAR_RFC_CON_SIN_GARANT")){
	
	Map hm = new HashMap();
	hm.put("mesInicial",request.getParameter("mesInicial")==null?"":request.getParameter("mesInicial"));
	hm.put("mesFinal",request.getParameter("mesFinal")==null?"":request.getParameter("mesFinal"));
	
	hm.put("mesInicialSiag",request.getParameter("mesInicialSiag")==null?"":request.getParameter("mesInicialSiag"));
	hm.put("mesFinalSiag",request.getParameter("mesFinalSiag")==null?"":request.getParameter("mesFinalSiag"));
	
	hm.put("federales",request.getParameter("federales")==null?null:request.getParameter("federales"));
	hm.put("otros",request.getParameter("otros")==null?null:request.getParameter("otros"));
	hm.put("eposSeleccionadas",request.getParameter("sNoEpoSelec")==null?"":request.getParameter("sNoEpoSelec"));
	hm.put("portafolio",request.getParameter("portafolio")==null?"":request.getParameter("portafolio"));
	
	hm.put("directorioPublicacion", strDirectorioPublicacion);
	hm.put("directorioPlantillas", strDirPlantillas);
	
	String nombreArchivo = repEspecialesProv.getGenerarRFCconSinGarant(hm);
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();	

}

%>
<%=infoRegresar%>