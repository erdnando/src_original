<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		com.netro.informacion.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		com.netro.seguridadbean.*,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion    = request.getParameter("informacion")      == null ? "": (String)request.getParameter("informacion");
JSONObject jsonObj  = new JSONObject();

String mensaje        = "";
String consulta       = "";
String infoRegresar   = "";

//variables para combos para el rango de periodos
String sMeses[] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
Calendar cal = Calendar.getInstance();
int iAnioActual = cal.get(Calendar.YEAR);
int iMesActual = cal.get(Calendar.MONTH);
int iDiaActual = cal.get(Calendar.DAY_OF_MONTH);
int iAnioInicial = cal.get(Calendar.YEAR) - 1;
int iMesInicial = cal.get(Calendar.MONTH);
int iDiaInicial = cal.get(Calendar.DAY_OF_MONTH);

Calendar gcFechaIniSigMes = new GregorianCalendar(iAnioActual, iMesActual+1, 1);
gcFechaIniSigMes.add(Calendar.DATE, -1);
int iUltimoDia = gcFechaIniSigMes.get(Calendar.DAY_OF_MONTH);

RepEspecialesProv repEspecialesProv = ServiceLocator.getInstance().lookup("RepEspecialesProvEJB", RepEspecialesProv.class);


if(informacion.equals("VALORES_INICIALES")){
	String noProceso = repEspecialesProv.setnoProceso();
	
	jsonObj.put("noProceso", noProceso);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("CONS_CAT_MES_INI")){
	String anioInicial = request.getParameter("anioInicial")==null?String.valueOf(iAnioInicial):request.getParameter("anioInicial");
	int iMes = (iAnioActual == Integer.parseInt(anioInicial))?(iDiaActual==iUltimoDia?iMesInicial:iMesInicial):11;
	
	List lstMesIni = new ArrayList();
	Map hm = new HashMap();
	for (int i = 0; i <= iMes; i++) {
		hm = new HashMap();
		hm.put("clave",String.valueOf(i+1));
		hm.put("descripcion", sMeses[i]);
		lstMesIni.add(hm);
	} 
	
	jsonObj.put("registros", JSONArray.fromObject(lstMesIni));
	jsonObj.put("iMesActual", String.valueOf(iMesActual));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("CONS_CAT_ANIO_INI_FIN")){
	
	List lstAnioIni = new ArrayList();
	Map hm = new HashMap();
	
	for (int anio = 2007; anio <= iAnioActual; anio++) {
		hm = new HashMap();
		hm.put("clave",String.valueOf(anio));
		hm.put("descripcion", String.valueOf(anio));
		lstAnioIni.add(hm);
	} 

	jsonObj.put("registros", JSONArray.fromObject(lstAnioIni));
	jsonObj.put("iAnioInicial", String.valueOf(iAnioInicial));
	jsonObj.put("iAnioActual", String.valueOf(iAnioActual));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_MES_FIN")){
	String anioFinal = request.getParameter("anioFinal")==null?String.valueOf(iAnioActual):request.getParameter("anioFinal");
	int iMes = (iAnioActual == Integer.parseInt(anioFinal))?(iDiaActual==iUltimoDia?iMesActual:iMesActual):11;
	
	List lstMesFin = new ArrayList();
	Map hm = new HashMap();
	
	for (int i = 0; i <= iMes; i++) {
		hm = new HashMap();
		hm.put("clave",String.valueOf(i+1));
		hm.put("descripcion", sMeses[i]);
		lstMesFin.add(hm);
	} 

	jsonObj.put("registros", JSONArray.fromObject(lstMesFin));
	jsonObj.put("iMesActual", String.valueOf(iMesActual));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_CLASIF_DEP")){
		
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_dependencia");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("COMCAT_DEPEN_CADENAS");
		catalogo.setOrden("cg_descripcion");
		infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("CONS_CAT_CLASIF")){
	List	catalogo =  repEspecialesProv.consultaCatClasificacion();
	List	catalogoB = new ArrayList();
	for(int i=0; i<catalogo.size();i++){
		Map hm = new HashMap();
		List cat = (List)catalogo.get(i);
		hm.put("IC_CLASIFICACION",(String)cat.get(0));
		hm.put("NOMBRE_CLASIFICACION",(String)cat.get(1));
		catalogoB.add(hm);
		
	}
		
	jsonObj.put("registros", JSONArray.fromObject(catalogoB));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("GUARDAR_MODIF_CLASIFICACION")){
	String descripcion = request.getParameter("nombreClasif")==null?"":request.getParameter("nombreClasif");
	String noClasificacion = request.getParameter("noClasificacion")==null?"":request.getParameter("noClasificacion");
	String accion = request.getParameter("accion")==null?"":request.getParameter("accion");
	
	if("G".equals(accion)){
		repEspecialesProv.setGuardaClasificacion(descripcion);
	}else if("M".equals(accion)){
		repEspecialesProv.setModificarClasificacion(noClasificacion, descripcion);
	}else if("E".equals(accion)){
		String respuesta =  repEspecialesProv.setEliminarClasificacion(noClasificacion);
	}
		
	jsonObj.put("accion", accion);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_MULTISELECT_EPOS")){
	String mesInicial = request.getParameter("mesInicial")==null?"":request.getParameter("mesInicial");
	String mesFinal = request.getParameter("mesFinal")==null?"":request.getParameter("mesFinal");
	String anioInicial = request.getParameter("anioInicial")==null?"":request.getParameter("anioInicial");
	String anioFinal = request.getParameter("anioFinal")==null?"":request.getParameter("anioFinal");
	
	String clasificacion = request.getParameter("clasificacion")==null?"":request.getParameter("clasificacion");
	String sNoEpoSelec = request.getParameter("sNoEpoSelec")==null?"":request.getParameter("sNoEpoSelec");
	String noSeleciona = request.getParameter("noSeleciona")==null?"":request.getParameter("noSeleciona");
	
	String eposPEF = request.getParameter("eposPEF")==null?"":request.getParameter("eposPEF");
	String eposGobMun = request.getParameter("eposGobMun")==null?"":request.getParameter("eposGobMun");
	String eposPrivadas = request.getParameter("eposPrivadas")==null?"":request.getParameter("eposPrivadas");
	String eposNC = request.getParameter("eposNC")==null?"":request.getParameter("eposNC");
	String eposCredi = request.getParameter("eposCredi")==null?"":request.getParameter("eposCredi");
	
	List llenaCombo1  = new ArrayList();
	List llenaCombo2  = new ArrayList();
	if (!sNoEpoSelec.equals("") || !clasificacion.equals("")) {
		llenaCombo2 = repEspecialesProv.llenaCombo2( sNoEpoSelec, clasificacion, noSeleciona);
	}
	llenaCombo1  = repEspecialesProv.llenaCombo1( eposPEF,	eposGobMun,  eposPrivadas, eposNC, eposCredi,  "" );
	
	List lstCombo1 = new ArrayList();
	String txtCombo2 = "";
	Map hm = new HashMap();
	if(llenaCombo1.size()>0){
		for(int x=0; x<llenaCombo1.size(); x++){
			hm = new HashMap();
			List datos = (List)llenaCombo1.get(x);
			hm.put("CLAVE", (String)datos.get(0));
			hm.put("DESCRIPCION", (String)datos.get(1)+" - pymes("+ (String)datos.get(2)+")");
			hm.put("NUMPYMES", (String)datos.get(2));
			lstCombo1.add(hm);
		}
	}
	
	if(llenaCombo2.size()>0){
		for(int x=0; x<llenaCombo2.size(); x++){
			List datos = (List)llenaCombo2.get(x);
			txtCombo2+= ("".equals(txtCombo2))?(String)datos.get(0):(","+(String)datos.get(0));
		}
	}
	
	//valida los meses ya capturados
	String  mesesG = "" ,  meses = "";
	
	if(!sNoEpoSelec.equals("")){
		List mesesGP  = repEspecialesProv.validaMeses( anioInicial, anioFinal, mesInicial,   mesFinal,  sNoEpoSelec, clasificacion);
		if(mesesGP.size()>0){
			mesesG = (String) mesesGP.get(0);
			meses = (String) mesesGP.get(1);
		}
	}
	
	request.setAttribute("meses", meses );
	request.setAttribute("mesesG", mesesG );
	
	jsonObj.put("meses", meses);
	jsonObj.put("mesesG", mesesG);
	jsonObj.put("llenaCombo1", JSONArray.fromObject(lstCombo1));
	jsonObj.put("txtCombo2", txtCombo2);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
		
}else if(informacion.equals("GENERAR_ARCHIVO")){
	String periodosP = "";
	String [] periodosPa = request.getParameterValues("sPeriodo");
	for(int i=0;i<periodosPa.length;i++)
	{
	   periodosP+=("".equals(periodosP))?periodosPa[i]:(","+periodosPa[i]);
	}
	
	Map hm = new HashMap();
	hm.put("mesInicial",request.getParameter("mesInicial")==null?"":request.getParameter("mesInicial"));
	hm.put("mesFinal",request.getParameter("mesFinal")==null?"":request.getParameter("mesFinal"));
	hm.put("anioInicial",request.getParameter("anioInicial")==null?"":request.getParameter("anioInicial"));
	hm.put("anioFinal",request.getParameter("anioFinal")==null?"":request.getParameter("anioFinal"));
	
	hm.put("clasificacion",request.getParameter("clasificacion")==null?"":request.getParameter("clasificacion"));
	hm.put("eposPEF",request.getParameter("eposPEF")==null?"":request.getParameter("eposPEF"));
	hm.put("eposGobMun",request.getParameter("eposGobMun")==null?"":request.getParameter("eposGobMun"));
	hm.put("eposPrivadas",request.getParameter("eposPrivadas")==null?"":request.getParameter("eposPrivadas"));
	hm.put("eposNC",request.getParameter("eposNC")==null?"":request.getParameter("eposNC"));
	hm.put("eposCredi",request.getParameter("eposCredi")==null?"":request.getParameter("eposCredi"));
	hm.put("sNoEpoSelec",request.getParameter("sNoEpoSelec")==null?"":request.getParameter("sNoEpoSelec"));
	hm.put("periodosP",periodosP);
	hm.put("noProceso",request.getParameter("noProceso")==null?"":request.getParameter("noProceso"));
	
	hm.put("directorio_publicacion", strDirectorioPublicacion);
	hm.put("directorio_plantillas", strDirPlantillas);
	
	String nombreArchivo = repEspecialesProv.setGenerarArchivoCargaInfo(hm);
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("GUARDAR_EN_SISTEMA")){
	String siguarda = "S";
	String periodosP = "";
	String [] periodosPa = request.getParameterValues("sPeriodo");
	for(int i=0;i<periodosPa.length;i++)
	{
	   periodosP+=("".equals(periodosP))?periodosPa[i]:(","+periodosPa[i]);
	}
	
	Map hm = new HashMap();
	String mesInicial = request.getParameter("mesInicial")==null?"":request.getParameter("mesInicial");
	String mesFinal = request.getParameter("mesFinal")==null?"":request.getParameter("mesFinal");
	String anioInicial = request.getParameter("anioInicial")==null?"":request.getParameter("anioInicial");
	String anioFinal = request.getParameter("anioFinal")==null?"":request.getParameter("anioFinal");
	
	String clasificacion = request.getParameter("clasificacion")==null?"":request.getParameter("clasificacion");
	String sNoEpoSelec = request.getParameter("sNoEpoSelec")==null?"":request.getParameter("sNoEpoSelec");	
	String eposPEF = request.getParameter("eposPEF")==null?"":request.getParameter("eposPEF");
	String eposGobMun = request.getParameter("eposGobMun")==null?"":request.getParameter("eposGobMun");
	String eposPrivadas = request.getParameter("eposPrivadas")==null?"":request.getParameter("eposPrivadas");
	String eposNC = request.getParameter("eposNC")==null?"":request.getParameter("eposNC");
	String eposCredi = request.getParameter("eposCredi")==null?"":request.getParameter("eposCredi");
	String noProceso = request.getParameter("noProceso")==null?"":request.getParameter("noProceso");
	String mesesG = request.getParameter("mesesG")==null?"":request.getParameter("mesesG");
	
	String  meses = "";
	if(!sNoEpoSelec.equals("")){
		List mesesGP  = repEspecialesProv.validaMeses( anioInicial, anioFinal, mesInicial,   mesFinal,  sNoEpoSelec, clasificacion);
		if(mesesGP.size()>0){
			 mesesG = (String) mesesGP.get(0);
			 meses = (String) mesesGP.get(1);
		}
	}
	
	//inserta los datos correspondientes a la tabla de  COM_CRUCE_CADENAS
	String seguardo = repEspecialesProv.setGuardaSistema( anioInicial, anioFinal, mesInicial,   mesFinal,  sNoEpoSelec, clasificacion, mesesG,siguarda,  noProceso);
	
	jsonObj.put("meses", meses);
	jsonObj.put("mesesG", mesesG);
	jsonObj.put("seguardo", seguardo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>