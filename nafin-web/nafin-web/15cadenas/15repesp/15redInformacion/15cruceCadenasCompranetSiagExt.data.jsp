<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		com.netro.informacion.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		com.netro.seguridadbean.*,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion    = request.getParameter("informacion")      == null ? "": (String)request.getParameter("informacion");
JSONObject jsonObj  = new JSONObject();

String mensaje        = "sss";
String consulta       = "";
String infoRegresar   = "";

//variables para combos para el rango de periodos
String sMeses[] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
Calendar cal = Calendar.getInstance();
int iAnioActual = cal.get(Calendar.YEAR);
int iMesActual = cal.get(Calendar.MONTH);
int iDiaActual = cal.get(Calendar.DAY_OF_MONTH);
int iAnioInicial = cal.get(Calendar.YEAR) - 1;
int iMesInicial = cal.get(Calendar.MONTH);
int iDiaInicial = cal.get(Calendar.DAY_OF_MONTH);

Calendar gcFechaIniSigMes = new GregorianCalendar(iAnioActual, iMesActual+1, 1);
gcFechaIniSigMes.add(Calendar.DATE, -1);
int iUltimoDia = gcFechaIniSigMes.get(Calendar.DAY_OF_MONTH);

RepEspecialesProv repEspecialesProv = ServiceLocator.getInstance().lookup("RepEspecialesProvEJB", RepEspecialesProv.class);

if(informacion.equals("VALORES_INICIALES")){
	boolean showGuardarRFCsParaSIAG = false;
	if ("ADMIN NAFIN".equals(strPerfil)){
		showGuardarRFCsParaSIAG = true;
	}
	
	jsonObj.put("showGuardarRFCsParaSIAG", new Boolean(showGuardarRFCsParaSIAG));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("CONS_CAT_MESES_CAD")){
	
	List catalogoMeses = repEspecialesProv.getCatalogoMesesHastaUltimoMes("COM_CRUCE_CADENAS");

	String mesInicial = String.valueOf(Fecha.getMesActual());
	String mesFinal = String.valueOf(Fecha.getMesActual());
	
	jsonObj.put("mesInicial", mesInicial);
	jsonObj.put("mesFinal", mesFinal);
	jsonObj.put("registros", JSONArray.fromObject(catalogoMeses));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_MESES_COMPRANET")){
	
	List catalogoMeses 		= repEspecialesProv.getCatalogoMesesHastaUltimoMes("COM_CRUCE_COMPRANET");
	
	String mesInicial = String.valueOf(Fecha.getMesActual());
	String mesFinal = String.valueOf(Fecha.getMesActual());
	
	jsonObj.put("mesInicial", mesInicial);
	jsonObj.put("mesFinal", mesFinal);
	jsonObj.put("registros", JSONArray.fromObject(catalogoMeses));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_MESES_SIAG")){
	
	List catalogoMeses 		= repEspecialesProv.getCatalogoMesesHastaUltimoMes("COM_CRUCE_SIAG");
	
	String mesInicial = String.valueOf(Fecha.getMesActual());
	String mesFinal = String.valueOf(Fecha.getMesActual());
	
	jsonObj.put("mesInicial", mesInicial);
	jsonObj.put("mesFinal", mesFinal);
	jsonObj.put("registros", JSONArray.fromObject(catalogoMeses));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_CLASIF_DEP")){
		
	List catalogoClasificacionDependencia 	= repEspecialesProv.getCatalogoClasificacionDependencia();
	
	jsonObj.put("registros", JSONArray.fromObject(catalogoClasificacionDependencia));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CADENAS_COMPRANET")){
	
	String sNoEpoSelec = request.getParameter("sNoEpoSelec")==null?"":request.getParameter("sNoEpoSelec");
	String federales = request.getParameter("federales")==null?"":request.getParameter("federales");
	String otros = request.getParameter("otros")==null?"":request.getParameter("otros");
	
	List catalogoEpos 		= new ArrayList();
	
	// Construir parametros de consulta
	List tipoClasificacion = new ArrayList();
	if("1".equals(federales)) tipoClasificacion.add("1");
	if("2".equals(otros)) 		tipoClasificacion.add("2");
	
	String eposSeleccionadas 		= sNoEpoSelec;
		List listaEposSeleccionadas 	= null;
		if(eposSeleccionadas != null && !eposSeleccionadas.trim().equals("")){
			String []lista = eposSeleccionadas.split(",");
			listaEposSeleccionadas = new ArrayList(Arrays.asList(lista));
		}
	
	if("1".equals(federales) || "2".equals(otros) || listaEposSeleccionadas!=null && listaEposSeleccionadas.size()>0) {
		catalogoEpos 	= repEspecialesProv.getCatalogoEposCompranet(tipoClasificacion,listaEposSeleccionadas);
		catalogoEpos = catalogoEpos == null?new ArrayList():catalogoEpos;
	}else{
		catalogoEpos 		= repEspecialesProv.getCatalogoEposCompranet(null,null);
	}
	
	jsonObj.put("llenaCombo1", JSONArray.fromObject(catalogoEpos));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("CONS_CAT_PORTAFOLIO")){

	// Obtener Catalogo de Meses
	List catalogoPortafolio = repEspecialesProv.getCatalogoPortafolioSiag();
	
	jsonObj.put("registros", JSONArray.fromObject(catalogoPortafolio));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("REALIZAR_CRUCE_INI")){
	
	// Obtener Id del Proceso de Cruce
	String idProcesoCruce = request.getParameter("idProcesoCruce");
	if(idProcesoCruce == null || "".equals(idProcesoCruce)){
		idProcesoCruce = repEspecialesProv.getIdCruceCadenasCompranet();
	}
	// Registrar lista de EPOs seleccionadas
	String eposSeleccionadas 		= request.getParameter("sNoEpoSelec");
	List listaEposSeleccionadas 	= null;
	if(eposSeleccionadas != null && !eposSeleccionadas.trim().equals("")){
		String []lista = eposSeleccionadas.split(",");
		listaEposSeleccionadas = new ArrayList(Arrays.asList(lista));
	}
	repEspecialesProv.registraListaEposCompranetSeleccionadas(idProcesoCruce,listaEposSeleccionadas);
	// Realizar cruce de informacion
	int 			   anioActual 		= Fecha.getAnioActual();
	String fechaInicialCadenas		= repEspecialesProv.getPrimerDiaDelMes(anioActual,Integer.parseInt(request.getParameter("mesInicialCadenas")));
	String fechaFinalCadenas		= repEspecialesProv.getUltimoDiaDelMes(anioActual,Integer.parseInt(request.getParameter("mesFinalCadenas")));
	String fechaInicialCompranet 	= repEspecialesProv.getPrimerDiaDelMes(anioActual,Integer.parseInt(request.getParameter("mesInicial")));
	String fechaFinalCompranet		= repEspecialesProv.getUltimoDiaDelMes(anioActual,Integer.parseInt(request.getParameter("mesFinal")));
	
	repEspecialesProv.cruzaProveedoresCadenasConCompranet(
		idProcesoCruce,
		fechaInicialCadenas,
		fechaFinalCadenas,
		request.getParameter("clasificacionDependencia"),
		fechaInicialCompranet,
		fechaFinalCompranet
	);

	
	jsonObj.put("mesInicial",request.getParameter("mesInicial"));
	jsonObj.put("mesFinal",request.getParameter("mesFinal"));
	jsonObj.put("idProcesoCruce",idProcesoCruce);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("REALIZAR_CRUCE_SIAG")){

	// Obtener Id del Proceso de Cruce
	String idProcesoCruce = request.getParameter("idProcesoCruce");
	String portafolio = request.getParameter("portafolio")==null?"": request.getParameter("portafolio");

	// Actualizar el Portafolio Seleccionado
	repEspecialesProv.registraPortafoliosSeleccionados(idProcesoCruce,portafolio.split(","));

	// Realizar cruce de informacion
	int 	 anioActual 			= Fecha.getAnioActual();
	String fechaInicialSiag		= repEspecialesProv.getPrimerDiaDelMes(anioActual,Integer.parseInt(request.getParameter("mesInicialSiag")));
	String fechaFinalSiag		= repEspecialesProv.getUltimoDiaDelMes(anioActual,Integer.parseInt(request.getParameter("mesFinalSiag")));
	repEspecialesProv.cruzarRFCsUnicosConSIAG(idProcesoCruce, fechaInicialSiag, fechaFinalSiag );
	
	jsonObj.put("mesInicial",request.getParameter("mesInicial"));
	jsonObj.put("mesFinal",request.getParameter("mesFinal"));
	jsonObj.put("idProcesoCruce",idProcesoCruce);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("GENERAR_RFC_UNICOS")){
	
	Map hm = new HashMap();
	
	hm.put("idProcesoCruce",request.getParameter("idProcesoCruce")==null?"":request.getParameter("idProcesoCruce"));
	hm.put("directorioPublicacion", strDirectorioPublicacion);
	hm.put("directorioPlantillas", strDirPlantillas);
	
	String nombreArchivo = repEspecialesProv.getGenerarRFCunicosCadCompSiag(hm);
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("GENERAR_RFC_UNICOS_CON_GARANT")){
	
	Map hm = new HashMap();
	
	hm.put("idProcesoCruce",request.getParameter("idProcesoCruce")==null?"":request.getParameter("idProcesoCruce"));
	hm.put("mesInicialSiag",request.getParameter("mesInicialSiag")==null?"":request.getParameter("mesInicialSiag"));
	hm.put("mesFinalSiag",request.getParameter("mesFinalSiag")==null?"":request.getParameter("mesFinalSiag"));
	hm.put("portafolio",request.getParameter("portafolio")==null?"":request.getParameter("portafolio"));
	hm.put("directorioPublicacion", strDirectorioPublicacion);
	hm.put("directorioPlantillas", strDirPlantillas);
	
	String nombreArchivo = repEspecialesProv.getDescargarRFCunicosConGarant(hm);
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("GENERAR_RFC_UNICOS_SIN_GARANT")){
	
	Map hm = new HashMap();
	
	hm.put("idProcesoCruce",request.getParameter("idProcesoCruce")==null?"":request.getParameter("idProcesoCruce"));
	hm.put("mesInicialSiag",request.getParameter("mesInicialSiag")==null?"":request.getParameter("mesInicialSiag"));
	hm.put("mesFinalSiag",request.getParameter("mesFinalSiag")==null?"":request.getParameter("mesFinalSiag"));
	hm.put("portafolio",request.getParameter("portafolio")==null?"":request.getParameter("portafolio"));
	hm.put("directorioPublicacion", strDirectorioPublicacion);
	hm.put("directorioPlantillas", strDirPlantillas);
	
	String nombreArchivo = repEspecialesProv.getDescargarRFCunicosSinGarant(hm);
	
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("GUARDAR_RFC_PARA_SIAG")){

	// Obtener Id del Proceso de Cruce
	String idProcesoCruce = request.getParameter("idProcesoCruce")==null?"":request.getParameter("idProcesoCruce");
	
	// Insertar registros
	repEspecialesProv.insertarCruceEnRedinfSiag(idProcesoCruce);
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>