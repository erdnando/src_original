<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.math.BigDecimal,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*, 
		netropology.utilerias.usuarios.*, 
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject, 
		java.sql.PreparedStatement,
		java.sql.ResultSet,
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion  = request.getParameter("operacion")== null?"":(String)request.getParameter("operacion");
	String infoRegresar="", consulta="",consultaGrid="", mensaje ="";
	
	String respuesta="";
	int start = 0;
	int limit = 0;
	
	JSONObject resultado = new JSONObject();
	HashMap datos = new HashMap();
	JSONObject jsonObj = new JSONObject();
	JSONObject jsonObjG = new JSONObject();
	if(informacion.equals("Consulta_Mes")){
		List reg = this.getCatalogoMesI("");
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar = jsonObj.toString();
		
	}if(informacion.equals("Consulta_MesF")){
		List reg = this.getCatalogoMesF("");
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("Consulta_Anio")){
		List reg = this.getCatalogoAnioI();
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar = jsonObj.toString();
		System.out.println("infoRegresar ** inicial "+infoRegresar);
	}else if(informacion.equals("Consulta_AnioF")){
		List reg = this.getCatalogoAnioF();
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("Consulta_info")){
		JSONArray reg = new JSONArray();
		
		String claveMesI = (request.getParameter("claveMesI")!=null)?request.getParameter("claveMesI"):"";
		String claveMesF = (request.getParameter("claveMesF")!=null)?request.getParameter("claveMesF"):"";
		String claveMesC = claveMesI;
		String claveAnioI = (request.getParameter("claveAnioI")!=null)?request.getParameter("claveAnioI"):"";
		String claveAnioF = (request.getParameter("claveAnioF")!=null)?request.getParameter("claveAnioF"):"";
		String claveAnioC = claveAnioI;
	
		String num_nae = iNoCliente; 
		BigDecimal numActivas = new BigDecimal("0");
								
		BigDecimal Aci = null;
		BigDecimal Ali = null;
		BigDecimal Abi = null;
		BigDecimal Acn = null;
		BigDecimal Aln = null;
		BigDecimal Abn = null;
		
		ConsClieRegMovil paginador = new ConsClieRegMovil();
		paginador.setNum_nae(num_nae);
		paginador.setClaveAnioI(claveAnioI);
		paginador.setClaveAnioF(claveAnioF);
		paginador.setClaveMesI(claveMesI);
		paginador.setClaveMesF(claveMesF);
		paginador.setClaveAnioC(claveAnioC);
		paginador.setClaveMesC(claveMesC);//setTipoConsulta
		paginador.setTipoConsulta("CONSULTA_GENERAL");
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		if(informacion.equals("Consulta_info")){		
			try{     		   
				try {
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));
					
				
				} catch(Exception e) {
					throw new AppException("Error en los parametros recibidos (star y limit)", e);
				}
				try {
					if (operacion.equals("generar")){
						queryHelper.executePKQuery(request);
					}
					
					Registros registros = queryHelper.getPageResultSet(request,start,limit);
					int i =0;
					while (registros.next()){
						numActivas = new BigDecimal("0");
						
						String activas = registros.getString("ALTAS").toString()==null||"".equals(registros.getString("ALTAS").toString())?"0":registros.getString("ALTAS").toString();
						String altas = registros.getString("ALTAS").toString()==null||"".equals(registros.getString("ALTAS").toString())?"0":registros.getString("ALTAS").toString();
						String bajas = registros.getString("INACTIVAS").toString()==null||"".equals(registros.getString("INACTIVAS").toString())?"0":registros.getString("INACTIVAS").toString();
						
						if(i==0){
							Acn = new BigDecimal (activas);
							Aln = new BigDecimal (altas);
							Abn = new BigDecimal (bajas);
						}else{										
							Aln = new BigDecimal (altas);
							Abn = new BigDecimal (bajas);								
							Acn = Acn.add(Aln).subtract(Abn);									
						}	
						numActivas = Acn;
							registros.setObject("ALTAS",Aln.toString());
							registros.setObject("ACTIVAS",numActivas.setScale(0, BigDecimal.ROUND_HALF_UP).toString());
							registros.setObject("BAJAS",Abn.toString());
					}
					
					
					consulta = "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
					resultado = JSONObject.fromObject(consulta);
					resultado.put("perfil", strPerfil);
				} catch(Exception e) {
				throw new AppException(" Error en la paginacion", e);
			}
				
			} catch(Exception e) {
				throw new AppException("Error al generar la consulta ", e);
			}
		}
		resultado.put("success", new Boolean(true));
		
		infoRegresar = resultado.toString();
		System.out.println("infoRegresar "+infoRegresar);
	}else if(informacion.equals("Generar_Arch")){
		String num_nae = iNoCliente;
		String claveMesI = (request.getParameter("claveMesI")!=null)?request.getParameter("claveMesI"):"";
		String claveMesF = (request.getParameter("claveMesF")!=null)?request.getParameter("claveMesF"):"";
		String claveMesC = (request.getParameter("claveMesC")!=null)?request.getParameter("claveMesC"):"";
		String claveAnioI = (request.getParameter("claveAnioI")!=null)?request.getParameter("claveAnioI"):"";
		String claveAnioF = (request.getParameter("claveAnioF")!=null)?request.getParameter("claveAnioF"):"";
		String claveAnioC = (request.getParameter("claveAnioC")!=null)?request.getParameter("claveAnioC"):"";
		
		try{
		
			InteligenciaComercial inteligenciaComercial = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);
		

			String nombre = inteligenciaComercial.generarArchNafinsaMovil(strDirectorioPublicacion,strDirectorioTemp,num_nae,claveAnioI,claveAnioF,claveMesI,claveMesF ,claveAnioC,claveMesC);
			
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombre);
			infoRegresar = jsonObj.toString();
		} catch(Exception e) {
			throw new AppException("Error al generar la consulta ", e);
		}
		
	}else if(informacion.equals("Generar_Arch_Detalle")){
		String anioDetalle = (request.getParameter("anioDetalle")!=null)?request.getParameter("anioDetalle"):"";
		String numMes = (request.getParameter("numMes")!=null)?request.getParameter("numMes"):"";
		String mesDetalle = (request.getParameter("mesDetalle")!=null)?request.getParameter("mesDetalle"):"";
		try{
		
			InteligenciaComercial inteligenciaComercial = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);
		

			String nombre = inteligenciaComercial.generarArchNafinsaMovilDetalle(strDirectorioPublicacion,strDirectorioTemp,anioDetalle,numMes,mesDetalle);
			
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombre);
			infoRegresar = jsonObj.toString();
		} catch(Exception e) {
			throw new AppException("Error al generar la consulta ", e);
		}
		
	}else if(informacion.equals("Consulta_info_detalle")){
	
		
		String anioDetalle = (request.getParameter("anioDetalle")!=null)?request.getParameter("anioDetalle"):"";	
		String numMes = (request.getParameter("numMes")!=null)?request.getParameter("numMes"):"";	
		String mesDetalle = (request.getParameter("mesDetalle")!=null)?request.getParameter("mesDetalle"):"";
		JSONArray reg = new JSONArray();
		StringBuffer strSQL1 = new StringBuffer();
		List varBind1 = new ArrayList();
		
		AccesoDB conexBD = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean lbSinError = true;
		try{     	
			
			ConsClieRegMovil paginador = new ConsClieRegMovil();
			paginador.setAnioDetalle(anioDetalle);
			paginador.setNumMes(numMes);
			paginador.setTipoConsulta("CONSULTA_DETALLE");
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
			try{ 
				try {
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));
					
				
				} catch(Exception e) {
					throw new AppException("Error en los parametros recibidos (star y limit)", e);
				}
				try {
					conexBD.conexionDB();
					if (operacion.equals("generar")){
						queryHelper.executePKQuery(request);
					}
					Registros registros = queryHelper.getPageResultSet(request,start,limit);
					while (registros.next()){
							strSQL1 = new StringBuffer();
							varBind1 = new ArrayList();
							strSQL1.append("SELECT distinct ");
							strSQL1.append("    CASE ");
							strSQL1.append("        WHEN ce.ic_tipo_epo IN(1,2,3,4) THEN 'AMBAS' ");
							strSQL1.append("        WHEN ce.ic_tipo_epo IN(1,2) THEN 'PUBLICAS' ");
							strSQL1.append("        WHEN ce.ic_tipo_epo IN(3,4) THEN 'PRIVADAS' END AS tipoEPO ");
							strSQL1.append("  FROM comcat_epo ce, comrel_pyme_epo cpe ");
							strSQL1.append(" WHERE cpe.ic_epo = ce.ic_epo ");
							strSQL1.append("   AND ce.ic_tipo_epo is not null ");
							strSQL1.append("   AND cpe.cs_aceptacion = ? ");
							strSQL1.append("   AND cpe.cs_habilitado = ? ");
							strSQL1.append("   AND cpe.ic_pyme = ? ");
			
							varBind1.add("H");
							varBind1.add("S");
							varBind1.add(registros.getString("NO_PYME"));
							ps = conexBD.queryPrecompilado(strSQL1.toString(), varBind1);
							rs = ps.executeQuery();
							if (rs.next()) {
								registros.setObject("TIPOEPO", rs.getString("TIPOEPO").toString());
							}
							rs.close();
							ps.close();
					}
					consulta = "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
					resultado = JSONObject.fromObject(consulta);
				resultado.put("anioDetalle",anioDetalle);
				resultado.put("mesDetalle",mesDetalle);
				infoRegresar =resultado.toString();
				} catch(Exception e) {
					lbSinError = false;
					throw new AppException(" Error en la paginacion", e);
				}finally {
					try {
						if (rs != null)
							rs.close();
						if (ps != null)
							ps.close();
						
						
					} catch (Exception t) {
						log.error("guardaPreguntaGral():: Error al cerrar Recursos: " + t.getMessage());
					}
					conexBD.terminaTransaccion(lbSinError);
					if (conexBD.hayConexionAbierta()) {
						conexBD.cierraConexionDB();
					}
					log.debug("Error el generar la consulta ");
				}	
			} catch(Exception e) {
				throw new AppException("Error al generar la consulta ", e);
			}
			
			
		} catch(Exception e) {
			throw new AppException("Error al generar la detalle ", e);
		}
		
	}
	
	%>	

<%= infoRegresar %>
<%!
	public List getCatalogoMesI(String claveAnioI) {				
		List catalogoMesI = new ArrayList();				
		String sMeses[] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };						
		
		for(int i=0; i<=11; i++) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(String.valueOf(i));
			elementoCatalogo.setDescripcion(sMeses[i]);
			catalogoMesI.add(elementoCatalogo);			
		}		
		
		return catalogoMesI;
	}
	
	public List getCatalogoMesF(String claveAnioF) {				
		List catalogoMesF = new ArrayList();				
		String sMeses[] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };				
		
		for(int i=0; i<=11; i++) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(String.valueOf(i));
			elementoCatalogo.setDescripcion(sMeses[i]);
			catalogoMesF.add(elementoCatalogo);			
		}
		
		return catalogoMesF;
	}	
	public List getCatalogoAnioI() {	
		List catalogoAnioI = new ArrayList();				
		Calendar cal = Calendar.getInstance();	
		int iAnioActual = cal.get(Calendar.YEAR);				
		
		for(int anio=2008; anio<=iAnioActual; anio++) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(String.valueOf(anio));
			elementoCatalogo.setDescripcion(String.valueOf(anio));
			catalogoAnioI.add(elementoCatalogo);			
		}
		
		return catalogoAnioI;
	}
	public List getCatalogoAnioF() {	
		List catalogoAnioF = new ArrayList();					
		Calendar cal = Calendar.getInstance();	
		int iAnioActual = cal.get(Calendar.YEAR);				
		
		for(int anio=2008; anio<=iAnioActual; anio++) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(String.valueOf(anio));
			elementoCatalogo.setDescripcion(String.valueOf(anio));
			catalogoAnioF.add(elementoCatalogo);			
		}
		
		return catalogoAnioF;
	}

%>


