<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.SimpleDateFormat,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		com.netro.descuento.*,
		net.sf.json.JSONObject,
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		com.netro.seguridadbean.*,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion")  == null ? "": (String)request.getParameter("informacion");
String icMes       = request.getParameter("ic_mes")       == null ? "": (String)request.getParameter("ic_mes");
String icAnio      = request.getParameter("ic_anio")      == null ? "": (String)request.getParameter("ic_anio");
String tipoArchivo = request.getParameter("tipo_archivo") == null ? "": (String)request.getParameter("tipo_archivo");
String bancoFondeo = "1";

HashMap    datos      = new HashMap();
JSONArray  registros  = new JSONArray();
JSONObject resultado  = new JSONObject();

String mensaje        = "";
String consulta       = "";
String nombreArchivo  = "";
String infoRegresar   = "";
boolean success       = true;

log.info("informacion: <<<<<"    + informacion    + ">>>>>");

if(informacion.equals("CATALOGO_ANIO")){

	SimpleDateFormat formato = new SimpleDateFormat("yyyy");
	Calendar calendario = Calendar.getInstance();
	String sAnio = formato.format(calendario.getTime());
	int iAnio = Integer.parseInt(sAnio);

	for (int i = 0; i < 3; i++) {
		datos = new HashMap();
		datos.put("clave",       Integer.toString(iAnio));
		datos.put("descripcion", Integer.toString(iAnio));
		registros.add(datos);
		iAnio -= 1;
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else if(informacion.equals("CONSULTA_DATA") || informacion.equals("GENERA_ARCHIVO")){

	HashMap datosConsulta = new HashMap();
	datosConsulta.put("banco_fondeo", bancoFondeo);
	datosConsulta.put("mesOperacion", icMes      );
	datosConsulta.put("anioOperacion", icAnio    );

	InteligenciaComercial inteligenciaComercial = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);

	HashMap reporteOperacionPymesCadenasMes = inteligenciaComercial.generaReporteOperacionPymesCadenasMes(datosConsulta);
	int numeroRegistros = Integer.parseInt(reporteOperacionPymesCadenasMes.get("numeroRegistros").toString());
	registros = new JSONArray();

	for(int i = 0; i < numeroRegistros; i++){
		HashMap operacionPorMes = (HashMap)reporteOperacionPymesCadenasMes.get("operacionPorMes" + i);
		datos = new HashMap(); // Se tiene que pasar nuevamente a un HashMap para darle formato
		datos.put("MES_ANIO",                   Comunes.ucFirst(Fecha.getNombreDelMes(Integer.parseInt(icMes))) + "/" + icAnio);
		datos.put("PYMES_UNICAS_AFILIADAS",     Comunes.formatoDecimal(operacionPorMes.get("pymesUnicasAfiliadas"), 0)        );
		datos.put("PYMES_CON_PUBLICACION",      Comunes.formatoDecimal(operacionPorMes.get("pymesConPublicacion"), 0)         );
		datos.put("PORC_PYMES_CON_PUBLICACION", Comunes.formatoDecimal(operacionPorMes.get("pcPymesConPub"), 4) + " %"        );
		datos.put("MONTO_PUBLICADO",      "$" + Comunes.formatoDecimal(operacionPorMes.get("montoPublicado"), 2)              );
		datos.put("DOCUMENTOS_PUBLICADOS",      Comunes.formatoDecimal(operacionPorMes.get("numeroPoctosPub"), 0)             );
		datos.put("MONTO_OPERADO",        "$" + Comunes.formatoDecimal(operacionPorMes.get("montoOperado"), 2)                );
		datos.put("DOCUMENTOS_OPERADOS",        Comunes.formatoDecimal(operacionPorMes.get("numeroDoctosOpe"), 0)             );
		registros.add(datos);
	}

	if(informacion.equals("CONSULTA_DATA")){

		try{
			if(numeroRegistros > 0){
				consulta = "{\"success\": "+new Boolean(success)+", \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString() +"}";
			} else{
				consulta = "{\"success\": "+new Boolean(success)+", \"total\": 0, \"registros\": []}";
			}
			resultado = JSONObject.fromObject(consulta);
		} catch(Exception e) {
			success = false;
			throw new AppException("Error al realizar la consulta ", e);
		}

	} else if(informacion.equals("GENERA_ARCHIVO")){

		try{
			OperacionPymesCadenas archivo = new OperacionPymesCadenas();
			archivo.setArrayRegistros(registros);
			archivo.setTipoReporte("POR_MES");
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(archivo);
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Exception e) {
			success = false;
			throw new AppException("Error al generar el archivo ", e);
		}

	}

	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>