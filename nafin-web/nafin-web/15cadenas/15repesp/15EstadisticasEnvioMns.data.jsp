<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*, 
		netropology.utilerias.usuarios.*, 
		com.netro.xls.ComunesXLS,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar="", consulta="",consultaGrid="", mensaje ="";
	
	String respuesta="";
	
	JSONObject jsonObj = new JSONObject();
	JSONArray registros1 = new JSONArray();
	HashMap datos = new HashMap();
	
	try{ 
		
		
		InteligenciaComercial beanInt = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);	
		if(informacion.equals("Consulta_Anio")){
			String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
			String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";
			JSONArray reg = new JSONArray();
			if(anio.equals("")){
				anio	= new SimpleDateFormat("yyyy").format(new java.util.Date());
			}
			
			if(mes.equals("")){
				mes	= new SimpleDateFormat("MM").format(new java.util.Date());
			}
			
			Map		mAniosMeses = beanInt.consultaMesesAnios(anio,mes);
			List	lAnios		= (List)mAniosMeses.get("anios");
			for(int i = 0; i < lAnios.size(); i++){
				datos = new HashMap();
				datos.put("clave",lAnios.get(i));
				datos.put("descripcion",lAnios.get(i));
				reg.add(datos);	
			}
			
		
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("registros",reg);
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("Consulta_Mes")){
			String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
			String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";
			JSONArray reg = new JSONArray();
			if(anio.equals("")){
				anio	= new SimpleDateFormat("yyyy").format(new java.util.Date());
			}
			
			if(mes.equals("")){
				mes	= new SimpleDateFormat("MM").format(new java.util.Date());
			}
			
			
			Map		mAniosMeses = beanInt.consultaMesesAnios(anio,mes);
			List	lMeses		= (List)mAniosMeses.get("meses");
			Map		mMeses 		= new HashMap();
			mMeses.put("01","Enero");
			mMeses.put("02","Febrero");
			mMeses.put("03","Marzo");
			mMeses.put("04","Abril");
			mMeses.put("05","Mayo");
			mMeses.put("06","Junio");
			mMeses.put("07","Julio");
			mMeses.put("08","Agosto");
			mMeses.put("09","Septiembre");
			mMeses.put("10","Octubre");
			mMeses.put("11","Noviembre");
			mMeses.put("12","Diciembre");
			
			for(int i = 0; i < lMeses.size(); i++){
				datos = new HashMap();
				datos.put("clave",lMeses.get(i));
				datos.put("descripcion",mMeses.get(lMeses.get(i)));
				reg.add(datos);	
			}
			
		
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("registros",reg);
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("Consulta_Epos")){
			JSONArray reg = new JSONArray();
			List lEpos = beanInt.consultaEposMensajes();
			datos = new HashMap();
			datos.put("clave","");
			datos.put("descripcion","Todas las EPOs");
			reg.add(datos);
			for(int i=0;i<lEpos.size();i++){
				Map		me	= (Map)lEpos.get(i);
				datos = new HashMap();
				datos.put("clave",me.get("IC_EPO"));
				datos.put("descripcion",me.get("NOMBRE"));
				reg.add(datos);		
			}
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("registros",reg);
			infoRegresar = jsonObj.toString();
			
		}else if(informacion.equals("Consulta_info")){
			String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
			String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";
			String estatusEnvio = (request.getParameter("estatusEnvio")!=null)?request.getParameter("estatusEnvio"):"";
			String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
			JSONArray reg = new JSONArray();
	
			List lEstadisticas = beanInt.consultaEstadisticas(anio,mes,estatusEnvio,ic_epo);
			int numRadios =0;
			String radio_hide="S";
			if(lEstadisticas.size()>0){
				numRadios = 2;
				String anioAct = new SimpleDateFormat("yyyy").format(new java.util.Date());
				String mesAct	= new SimpleDateFormat("MM").format(new java.util.Date());
				if(anio.equals(anioAct)&&mes.equals(mesAct)){
					numRadios = 4;
					radio_hide="N";
				}
				
				for(int i=0;i<lEstadisticas.size();i++){
					Map mColumnas = (Map)lEstadisticas.get(i);
					datos = new HashMap();
					datos.put("NOMBRE_EPO",mColumnas.get("NOMBRE_EPO"));
					datos.put("NUM_PYMES",mColumnas.get("NUM_PYMES"));
					datos.put("NUM_PYMES_MSG",mColumnas.get("NUM_PYMES_MSG"));
					datos.put("NUM_RESP_SI",mColumnas.get("NUM_RESP_SI"));
					datos.put("NUM_RESP_NO",mColumnas.get("NUM_RESP_NO"));
					datos.put("SIN_RESPONDER",mColumnas.get("SIN_RESPONDER"));
					datos.put("PYMES_SIN_MSG",mColumnas.get("PYMES_SIN_MSG"));
					datos.put("ESTATUS_ENVIO",mColumnas.get("ESTATUS_ENVIO"));
					datos.put("IC_EPO",mColumnas.get("IC_EPO"));
					reg.add(datos);	
				}
		}
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("numRadios",String.valueOf(numRadios));
		jsonObj.put("radio_hide",radio_hide);
		infoRegresar =jsonObj.toString();
		System.out.println("infoRegresar ** tpsss "+infoRegresar);
	}else if(informacion.equals("Consulta_info_total")){
			String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
			String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";
			String estatusEnvio = (request.getParameter("estatusEnvio")!=null)?request.getParameter("estatusEnvio"):"";
			String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
			JSONArray reg = new JSONArray();
			List lEstadisticas = beanInt.consultaEstadisticas(anio,mes,estatusEnvio,ic_epo);
			if(lEstadisticas.size()>0){
				Map mColumnas = (Map)lEstadisticas.get((lEstadisticas.size()-1));
				datos = new HashMap();
				datos.put("DESCRICPION","TOTAL : ");
				datos.put("TOT_PYMES",mColumnas.get("TOT_PYMES"));
				datos.put("TOT_PYMES_MSG",mColumnas.get("TOT_PYMES_MSG"));
				datos.put("TOT_PYMES_SI",mColumnas.get("TOT_PYMES_SI"));
				datos.put("TOT_PYMES_NO",mColumnas.get("TOT_PYMES_NO"));
				datos.put("TOT_SIN_RESP",mColumnas.get("TOT_SIN_RESP"));
				datos.put("TOT_SIN_MSG",mColumnas.get("TOT_SIN_MSG"));
				reg.add(datos);	
		}
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
		
	}else if(informacion.equals("Consulta_info_detalle")){
		String epoDetalle = (request.getParameter("epoDetalle")!=null)?request.getParameter("epoDetalle"):"";
		
		String resp = (request.getParameter("respuesta")!=null)?request.getParameter("respuesta"):"";
		String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";
		String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
	
		List	lDetalle		= beanInt.consultaDetallePorEpo(epoDetalle,resp,anio,mes);	
		JSONArray reg = new JSONArray();
		
		for(int i=0;i<lDetalle.size();i++){
			Map mColumnas = (Map)lDetalle.get(i);
			datos = new HashMap();
			datos.put("IC_PYME",mColumnas.get("IC_PYME"));
			datos.put("NUMERO_SIRAC",mColumnas.get("NUMERO_SIRAC"));
			datos.put("NOMBRE_PYME",mColumnas.get("NOMBRE_PYME"));
			datos.put("CREDCAD",mColumnas.get("CREDCAD"));
			datos.put("ESTADO",mColumnas.get("ESTADO"));
			datos.put("CP",mColumnas.get("CP"));
			datos.put("TELEFONO",mColumnas.get("TELEFONO"));
			datos.put("MONTO_CREDITO",Comunes.formatoDecimal((String)mColumnas.get("MONTO_CREDITO"),2));
			datos.put("MONTO_CONSOLIDADO",Comunes.formatoDecimal((String)mColumnas.get("MONTO_CONSOLIDADO"),2));
			datos.put("RESPUESTA",mColumnas.get("RESPUESTA"));
			datos.put("FECHA_RESPUESTA",mColumnas.get("FECHA_RESPUESTA"));
			reg.add(datos);	
		}
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
		
	}else if(informacion.equals("Consulta_total_detalle")){
		String epoDetalle = (request.getParameter("epoDetalle")!=null)?request.getParameter("epoDetalle"):"";
		
		String resp = (request.getParameter("respuesta")!=null)?request.getParameter("respuesta"):"";
		String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";
		String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
		List	lResumen = beanInt.consultaResumenEst("","","",epoDetalle);	
		JSONArray reg = new JSONArray();
		if(lResumen!=null){
			Map mColumnas = (Map)lResumen.get(0);
			datos = new HashMap();
			datos.put("DESCRIPCION","Total");
			datos.put("NUM_RESP_SI",mColumnas.get("NUM_RESP_SI"));
			datos.put("NUM_RESP_NO",mColumnas.get("NUM_RESP_NO"));
			datos.put("SIN_RESPONDER",mColumnas.get("SIN_RESPONDER"));
			datos.put("NUM_PYMES_MSG",mColumnas.get("NUM_PYMES_MSG"));
			reg.add(datos);	
			
			String  MONTO_RESP_SI = (String)mColumnas.get("MONTO_RESP_SI");
			String  MONTO_RESP_NO = (String)mColumnas.get("MONTO_RESP_NO");
			String  MONTO_SIN_RESPONDER = (String)mColumnas.get("MONTO_SIN_RESPONDER");
			
			datos = new HashMap();
			datos.put("DESCRIPCION","Total Monto Crédito Ventas 3 meses");
			datos.put("NUM_RESP_SI",(MONTO_RESP_SI.equals("0.00"))?"$ 0.00":"$ "+MONTO_RESP_SI);
			datos.put("NUM_RESP_NO",(MONTO_RESP_NO.equals("0.00"))?"$ 0.00":"$ "+MONTO_RESP_NO);
			datos.put("SIN_RESPONDER",(MONTO_SIN_RESPONDER.equals("0.00"))?"$ 0.00":"$ "+MONTO_SIN_RESPONDER);
			datos.put("NUM_PYMES_MSG","");
			reg.add(datos);	
		}	
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
	}else if(informacion.equals("Generar_Arch_Detalle")){
		String epoDetalle = (request.getParameter("epoDetalle")!=null)?request.getParameter("epoDetalle"):"";
		String resp = (request.getParameter("respuesta")!=null)?request.getParameter("respuesta"):"";
		String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";
		String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
		String 	nombreEpo		= "";
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = archivo.nombreArchivo()+".xls";
		ComunesXLS xlsDoc = new ComunesXLS();
			
		String encabezados[] = {"","NUM PYME","NUM SIRAC","PYME","CRED. CAD.","ESTADO","C.P.","TELEFONO","MONTO CREDITO VENTAS 3 MESES","MONTO LINEA CONSOLIDADO","RESPUESTA","FECHA DE RESPUESTA"};
		
		List lFilas = new ArrayList();
		List lColumnas = null;
		
		lColumnas = new ArrayList();

		List	lDetalle		= beanInt.consultaDetallePorEpo(epoDetalle,respuesta,anio,mes);
		List	lResumen		= null;
		
		for(int i=0;i<lDetalle.size();i++){
			Map mColumnas = (Map)lDetalle.get(i);
			lColumnas = new ArrayList();
			lColumnas.add("");
			lColumnas.add(mColumnas.get("IC_PYME"));
			lColumnas.add(mColumnas.get("NUMERO_SIRAC"));
			lColumnas.add(mColumnas.get("NOMBRE_PYME"));
			lColumnas.add(mColumnas.get("CREDCAD"));
			lColumnas.add(mColumnas.get("ESTADO"));
			lColumnas.add(mColumnas.get("CP"));
			lColumnas.add(mColumnas.get("TELEFONO"));
			lColumnas.add(Comunes.formatoDecimal(mColumnas.get("MONTO_CREDITO").toString(),2));
			lColumnas.add(Comunes.formatoDecimal(mColumnas.get("MONTO_CONSOLIDADO").toString(),2));
			lColumnas.add(mColumnas.get("RESPUESTA"));
			lColumnas.add(mColumnas.get("FECHA_RESPUESTA"));
			nombreEpo = mColumnas.get("NOMBRE_EPO").toString();
			lFilas.add(lColumnas);
		} 
		
		lResumen = beanInt.consultaResumenEst("","","",epoDetalle);
		Map mColumnas = (Map)lResumen.get(0);
		
		lColumnas = new ArrayList();
		for(int i=0;i<12;i++){
			lColumnas.add("");
		}
		lFilas.add(lColumnas);
		lFilas.add(lColumnas);
		lFilas.add(lColumnas);
		
		lColumnas = new ArrayList();
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("PYMES SI INTERESADAS");
		lColumnas.add("PYMES NO INTERESADAS");
		lColumnas.add("PYMES SIN CONTESTAR");
		lColumnas.add("TOTAL");
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lFilas.add(lColumnas);
		
		lColumnas = new ArrayList();
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("TOTAL");
		lColumnas.add(mColumnas.get("NUM_RESP_SI"));
		lColumnas.add(mColumnas.get("NUM_RESP_NO"));
		lColumnas.add(mColumnas.get("SIN_RESPONDER"));
		lColumnas.add(mColumnas.get("NUM_PYMES_MSG"));
		
		lColumnas.add("");
		
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lFilas.add(lColumnas);
		
		lColumnas = new ArrayList();
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("TOTAL MONTO CREDITO VENTA 3 MESES");
		lColumnas.add(mColumnas.get("MONTO_RESP_SI"));
		lColumnas.add(mColumnas.get("MONTO_RESP_NO"));
		lColumnas.add(mColumnas.get("MONTO_SIN_RESPONDER"));
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lColumnas.add("");
		lFilas.add(lColumnas);
		
		xlsDoc.generarXLS("HOJA1",nombreEpo,encabezados,lFilas,strDirectorioTemp+nombreArchivo);	
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("accion_aceptar")){
		String icEpos[] = request.getParameterValues("sel");
		String accion = (request.getParameter("accion")!=null)?request.getParameter("accion"):"";
		String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
		String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";
		String estatusEnvio = (request.getParameter("estatusEnvio")!=null)?request.getParameter("estatusEnvio"):"";
		String mns = "";
		String nombreArchivo  = "";
		if(accion.equals("D")||accion.equals("E")){
			beanInt.cambiarEstatusEnvio(icEpos,accion);
			mns= "La actualización se llevó a cabo con éxito";
		}
		if(accion.equals("GA")||accion.equals("GD")){
			CreaArchivo archivo = new CreaArchivo();
			nombreArchivo = archivo.nombreArchivo()+".xls";
			ComunesXLS xlsDoc = new ComunesXLS();
		
			if("GA".equals(accion)){
				String encabezados[] = {"","EPO","Total de Pymes","Total de Pymes a quienes se les envio mensaje","Total de Pymes que respondieron que SI","Total de Pymes que respondieron que no","Total de Pymes que no han contestado","Total de Pymes que no se les envio mensaje"};
				List lEstadisticas = beanInt.consultaEstadisticas(anio,mes,estatusEnvio,icEpos);
				List lFilas = new ArrayList();
				List lColumnas = null;
				
				for(int i=0;i<lEstadisticas.size();i++){
					Map  mColumnas = (Map)lEstadisticas.get(i);
					lColumnas = new ArrayList();
					lColumnas.add("");
					lColumnas.add(mColumnas.get("NOMBRE_EPO"));
					lColumnas.add(mColumnas.get("NUM_PYMES"));
					lColumnas.add(mColumnas.get("NUM_PYMES_MSG"));
					lColumnas.add(mColumnas.get("NUM_RESP_SI"));
					lColumnas.add(mColumnas.get("NUM_RESP_NO"));
					lColumnas.add(mColumnas.get("SIN_RESPONDER"));
					lColumnas.add(mColumnas.get("PYMES_SIN_MSG"));
					lColumnas.add(mColumnas.get("ESTATUS_ENVIO"));
					lFilas.add(lColumnas);
				}
				xlsDoc.generarXLS("HOJA1","ESTADISTICOS ENVIO DE MENSAJES",encabezados,lFilas,strDirectorioTemp+nombreArchivo);			
			}else{
			
				String encabezados[] = {"","NOMBRE_EPO","NUM PYME","NUM SIRAC","PYME","CRED. CAD.","ESTADO","C.P.","TELEFONO","MONTO CREDITO VENTAS 3 MESES","MONTO LINEA CONSOLIDADO","RESPUESTA","FECHA DE RESPUESTA"};
				List	lDetalle		= beanInt.consultaDetallePorEpo(icEpos,"",anio,mes);
				List	lResumen		= null;
				List lFilas = new ArrayList();
				List lColumnas = null;
				
				for(int i=0;i<lDetalle.size();i++){
					Map mColumnas = (Map)lDetalle.get(i);
					lColumnas = new ArrayList();
					lColumnas.add("");
					lColumnas.add(mColumnas.get("NOMBRE_EPO"));
					lColumnas.add(mColumnas.get("IC_PYME"));
					lColumnas.add(mColumnas.get("NUMERO_SIRAC"));
					lColumnas.add(mColumnas.get("NOMBRE_PYME"));
					lColumnas.add(mColumnas.get("CREDCAD"));
					lColumnas.add(mColumnas.get("ESTADO"));
					lColumnas.add(mColumnas.get("CP"));
					lColumnas.add(mColumnas.get("TELEFONO"));
					lColumnas.add(Comunes.formatoDecimal(mColumnas.get("MONTO_CREDITO").toString(),2));
					lColumnas.add(Comunes.formatoDecimal(mColumnas.get("MONTO_CONSOLIDADO").toString(),2));
					lColumnas.add(mColumnas.get("RESPUESTA"));
					lColumnas.add(mColumnas.get("FECHA_RESPUESTA"));
					lFilas.add(lColumnas);
				} 
				
				lResumen = beanInt.consultaResumenEst("","","",icEpos);
				Map mColumnas = (Map)lResumen.get(0);
				
				lColumnas = new ArrayList();
				for(int i=0;i<13;i++){
					lColumnas.add("");
				}
				lFilas.add(lColumnas);
				lFilas.add(lColumnas);
				lFilas.add(lColumnas);
				
				lColumnas = new ArrayList();
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("PYMES SI INTERESADAS");
				lColumnas.add("PYMES NO INTERESADAS");
				lColumnas.add("PYMES SIN CONTESTAR");
				lColumnas.add("TOTAL");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lFilas.add(lColumnas);
				
				lColumnas = new ArrayList();
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("TOTAL");
				lColumnas.add(mColumnas.get("NUM_RESP_SI"));
				lColumnas.add(mColumnas.get("NUM_RESP_NO"));
				lColumnas.add(mColumnas.get("SIN_RESPONDER"));
				lColumnas.add(mColumnas.get("NUM_PYMES_MSG"));
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lFilas.add(lColumnas);
				
				lColumnas = new ArrayList();
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("TOTAL MONTO CREDITO VENTA 3 MESES");
				lColumnas.add(mColumnas.get("MONTO_RESP_SI"));
				lColumnas.add(mColumnas.get("MONTO_RESP_NO"));
				lColumnas.add(mColumnas.get("MONTO_SIN_RESPONDER"));
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lColumnas.add("");
				lFilas.add(lColumnas);
				
				xlsDoc.generarXLS("HOJA1","DETALLE DE PYMES",encabezados,lFilas,strDirectorioTemp+nombreArchivo);		
			}
		}
		
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("mns",mns);
		jsonObj.put("accion",accion);
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
		
	}
} catch(Exception e) {
	throw new AppException("Error al generar la consulta ", e);
}
%>	

<%= infoRegresar %>


