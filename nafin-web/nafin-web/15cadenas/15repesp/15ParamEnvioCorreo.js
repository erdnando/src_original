	var	origen =[];
	var   ic_envio_clientes = [];
	var 	cg_email = [];
	var 	cs_envio_mensual = [];
	var 	cs_envio_diario =[];
	var inicializaVariables= function(){
		origen =[];
		ic_envio_clientes = [];
		cg_email = [];
	   cs_envio_mensual = [];
	   cs_envio_diario =[];
	}
function cambioEnvioDiario(check,rowIndex,colIds){
	var gridConsulta = Ext.ComponentQuery.query('grid')[0];	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	if(check.checked == true)  {
			reg.set('cs_envio_diario','S');
		}else{
			reg.set('cs_envio_diario','N');
		}	
}
function cambioEnvioMensual(check,rowIndex,colIds){
	var gridConsulta = Ext.ComponentQuery.query('grid')[0];	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	if(check.checked == true)  {
			reg.set('cs_envio_mensual','S');
		}else{
			reg.set('cs_envio_mensual','N');
		}	
}

Ext.onReady(function(){
	var procesarConsultaData = function(store, arrRegistros, success, opts){
		if (arrRegistros != null){
			var jsonData = store.proxy.reader.jsonData;
			var cs_envio_mensual_clientes = jsonData.cs_envio_mensual_clientes;
			var cs_envio_diario_clientes = jsonData.cs_envio_diario_clientes;
			var hidAction = jsonData.hidAction;
			Ext.ComponentQuery.query('#cg_email_agrega')[0].reset();
			if(hidAction=='A'){//
				Ext.ComponentQuery.query('#btnAddCorreo')[0].setIconCls('icoAgregar');
				Ext.ComponentQuery.query('#btnAddCorreo')[0].setDisabled(false);
			}/*else if(hidAction=='E'){
				consultaData.load();
			}*/
			var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
			
			var eMCA = Ext.ComponentQuery.query('#eMCA')[0];
			var eMCD = Ext.ComponentQuery.query('#eMCD')[0];
			
			var eDCA = Ext.ComponentQuery.query('#eDCA')[0];
			var eDCD = Ext.ComponentQuery.query('#eDCD')[0];
			
			if(cs_envio_mensual_clientes=='S'){
				eMCA.setValue(true);
			}else{
				eMCD.setValue(true);
			}
			
			if(cs_envio_diario_clientes=='S'){
				eDCA.setValue(true);
			}else{
				eDCD.setValue(true);
			}
			
			if(store.getTotalCount() > 0){
				gridConsulta.getView().setAutoScroll(true);
			} else{
				gridConsulta.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridConsulta.getView().setAutoScroll(false);
				
			}
		}
	}
	var procesarAcepatar = function(opts, success, response) {
		Ext.ComponentQuery.query('#cg_email_agrega')[0].reset();
		Ext.ComponentQuery.query('#btnAceptar')[0].setIconCls('icoAceptar');
		Ext.ComponentQuery.query('#btnAceptar')[0].setDisabled(false);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			 var json=Ext.JSON.decode(response.responseText);
			 var mns = json.mns;
			 Ext.Msg.alert('Avisar','<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>'+mns+'</center></td><td></td></table> </div>');	
			consultaData.load();
		} else {
			
			NE.util.mostrarErrorPeticion(response);
		}
		
	}
	var eliminaReg  = function (grid, rowIndex, colIndex, item, event){
		var  fp =   Ext.ComponentQuery.query('#forma')[0];
		var registro = grid.getStore().getAt(rowIndex);
		var indice = registro.get('indice');  
		var val_origen = registro.get('origen');  
		if(val_origen=='agregar'){
			registro.set('origen','nodespliega');
		}else{
			registro.set('origen','eliminar');
		}
		var numRegistros=0;
		var jsonData = grid.getStore().getRange();
		inicializaVariables();
		Ext.each(jsonData, function (item) {
			origen.push(item.data.origen);	
			ic_envio_clientes.push(item.data.ic_envio_clientes);
			cg_email.push(item.data.cg_email);
			cs_envio_mensual.push(item.data.cs_envio_mensual);
			cs_envio_diario.push(item.data.cs_envio_diario);
			numRegistros++;
		});
		consultaData.load({
			params: Ext.apply(
				{
					informacion: 'Consulta_info',
					origen:   origen,
					ic_envio_clientes:    ic_envio_clientes,
					cg_email:    cg_email,
					cs_envio_mensual:   cs_envio_mensual,
					cs_envio_diario:    cs_envio_diario,
					numRegistros:    numRegistros,
					cg_email_agrega:    Ext.ComponentQuery.query('#cg_email_agrega')[0].getValue(),
					hidAction:    ''
					
				})
			});
	}
	Ext.define('ListaRegistros',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'ic_envio_clientes'     },
				{name: 'cg_email'     },
				{name: 'cs_envio_diario'     },
				{name: 'cs_envio_mensual'     },
				{name: 'origen'     },
				{name: 'indice'     }
			]
	});
	var consultaData = Ext.create('Ext.data.Store',{
		storeId: 'consultaData',
		model: 'ListaRegistros',
		pageSize: 15,
		proxy: {
			type: 'ajax',
			url:  '15ParamEnvioCorreo.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_info'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			listeners: {
				
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad:        true,
		listeners: {
			load: procesarConsultaData
		}
	});
	function procesarAceptar(boton){
		Ext.Msg.confirm('Avisar','Los cambios se guardaran en base de datos. �Desea continuar?',
			function(btnText){
				if(btnText === "yes"){
					var numRegistros=0;
					var grid = Ext.ComponentQuery.query('#grid')[0];
					var jsonData = grid.getStore().getRange();
					var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
					inicializaVariables();
					Ext.each(jsonData, function (item) {
						origen.push(item.data.origen);	
						ic_envio_clientes.push(item.data.ic_envio_clientes);
						cg_email.push(item.data.cg_email);
						cs_envio_mensual.push(item.data.cs_envio_mensual);
						cs_envio_diario.push(item.data.cs_envio_diario);
						numRegistros++;
					});
				boton.setDisabled(true);
				boton.setIconCls('x-mask-msg-text');	
				Ext.Ajax.request({
					url: '15ParamEnvioCorreo.data.jsp',
					params: Ext.apply(forma.getForm().getValues(),{					
						informacion:'guardar_datos',
						origen:   origen,
						ic_envio_clientes:    ic_envio_clientes,
						cg_email:    cg_email,
						cs_envio_mensual:   cs_envio_mensual,
						cs_envio_diario:    cs_envio_diario,
						numRegistros:    numRegistros,
						hidAction:    'G'
					}),
					callback: procesarAcepatar
				});
				
					
				}else{
				}
			}
			
		);
		
	}
	
	var elementosDetalle = [
		{
				xtype:'label',
					style:
                {
                    'font-size' : '12px'
                },
				width:	750,
				html:	'<table width="420" cellpadding="0" cellspacing="0" border="0">'+
							'<tr>'+
							'<td valign="top">'+
							'<table cellpadding="0" cellspacing="0" border="0">'+
								'<tr>'+
								'<td valign="top" align="center"><br>'+
								'<table width="420" cellpadding="0" cellspacing="1" border="0" class="formas">'+
									'<tr>'+
									'<td class="formas" align="center"><b>CLIENTES SUSCEPTIBLES DE CREDICADENAS</b></td>'+
				
								'</tr>'+
				
								'<tr>'+
				
									'<td class="formas">'+
				
										'<i><b>Objetivo:</b></i><br>'+
				
										'Emitir una base de datos de los clientes de Factoraje Electr&oacute;nico que cumplen las condiciones establecidas por riesgos para participar en el programa de Credicadenas.<br>'+
				
										'<br><i><b>Periodicidad:</b></i><br>'+
				
										'Mensual.<br>'+
				
										'<br><i><b>Destinatario:</b></i><br>'+
				
										'&nbsp;* Subdirecci�n de Productos de Financiamiento.<br>'+
				
										'&nbsp;* Rafael Velasco.<br>'+
				
										'&nbsp;* Alejandro Aguirre.<br>'+
				
										'<br><i><b>Fuente:</b></i><br>'+
				
										'Base de datos de  Nafin-electr&oacute;nico.<br>'+
				
										'<br><i><b>Formato:</b></i><br>'+
				
										'Excel (.csv)<br>'+
				
										'<br><i><b>Instrucciones:</b></i><br>'+
				
										'&nbsp;- Seleccionar el mismo mes en ambos criterios.<br>'+
				
										'<br><i><b>En excel:</i></b><br>'+
				
										'&nbsp;- Realizar las adecuaciones necesarias.<br>'+
				
										'&nbsp;- Aplicar el formato deseado y guardar el reporte.<br>'+
				
										'<br><i><b>Contactos:</i></b><br>'+
				
										'Rafael Velasco Posada - Subdirecci�n de Productos Electr&oacute;nicos y Financiamiento.<br>'+
				
										'Alejandro Aguirre - Credicadenas.<br>'+
				
										'<br><i><b>Observaciones:</i></b><br>'+
				
										'Las condiciones para estos clientes son:<br>'+
				
										"&nbsp;- Pertenecer a las Epo's seleccionadas.<br>"+
				
										'&nbsp;- Tener publicaciones en: 6 meses consecutivos &oacute;<br>'+
				
										'&nbsp;- Por lo menos en  8 de 12 meses.<br>'+
				
									'</td>'+
				
								'</tr>'+
				
								'</table>'+
				
							'</td>'+
				
						'</tr>'+
				
						'</table>'+
						'</td>'+
				
				'</tr>'+
				
				'</table>'

		}
	]
	var fpDetalle = {
		xtype: 'form',
		itemId					: 'fpDetalle',	
		width				: 500,
		height: 550,
		style				: ' margin:0 auto;',
		frame				: false,
		hidden			: false,
		fileUpload: true,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 200,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items: elementosDetalle	
	};
	function verDetalle(){
		var winDetalle =Ext.ComponentQuery.query('winDetalle')[0];
		if(winDetalle){
			
			winDetalle.show();
		} else{
					new Ext.Window({
								layout: 'fit',
								modal: true,
								width: 500,
								frame:       false,
								constrain:   true,
								height: 550,
								resizable: false,
								closable: true,
								x: 450,
								itemId: 'winDetalle',
								autoDestroy:false,
								closeAction: 'destroy',
								items: [
									fpDetalle
								],
								title: ''
						}).show();	
		}
		
	}
	
	var elementosForma = [
		{
			xtype: 'fieldcontainer',
			labelWidth: 250,
			layout: 'hbox',
			width:600, 
			fieldLabel:          'Env�o Mensual de Clientes Potenciales',
			items: [
				{
					xtype      : 'radiofield',
					identificador:'opcion_radio',
					boxLabel:      'Activar',
					width:100, 
					itemId:'eMCA',
					name:          'cs_envio_mensual_clientes',
					inputValue:    'S'
				},
				{ 	
					width:20, 
					xtype: 'displayfield'	
				},
				{
					xtype      : 'radiofield',
					identificador:'opcion_radio',
					boxLabel:      'Desactivar',
					width:100, 
					itemId:'eMCD',
					name:          'cs_envio_mensual_clientes',
					inputValue:    'N'
				}
			]
		},
		{
			xtype: 'fieldcontainer',
			labelWidth: 250,
			layout: 'hbox',
			width:600, 
			fieldLabel:          'Env�o Diario de Clientes Interesados',
			items: [
				{
					xtype      : 'radiofield',
					identificador:'opcion_radio',
					boxLabel:      'Activar',
					width:100, 
					itemId:'eDCA',
					name:          'cs_envio_diario_clientes',
					inputValue:    'S'
				},
				{ 	
					width:20, 
					xtype: 'displayfield'	
				},
				{
					xtype      : 'radiofield',
					identificador:'opcion_radio',
					boxLabel:      'Desactivar',
					width:100, 
					itemId:'eDCD',
					name:          'cs_envio_diario_clientes',
					inputValue:    'N'
				}
			]
		},
		{
			  xtype: 'textfield',
			  name: 'cg_email_agrega',
			  width:550,
			  labelWidth: 250,
			  vtype: 'email',
			  allowBlank: false,
			  itemId: 'cg_email_agrega',
			  fieldLabel: 'Ingreso de Correos Electr�nicos',
			  emptyText:'Correo'
		 }
		
	]
	var formaPrincipal = new Ext.form.FormPanel({
		width:                     600,
		height :'auto',
		itemId:                    'formaPrincipal',
		title:                     'Clientes Susceptibles de Credicadenas',
		bodyPadding:               '0 0 0 0',
		style:                     'margin: 10px auto 10px auto;',
		frame:                     true,
		border:                    true,
		fieldDefaults: {
			msgTarget:              'side',
			labelWidth:             70
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text:                   'Agregar Correo',
				itemId:                 'btnAddCorreo',
				formBind: true,
				iconCls:                'icoAgregar',
				handler: function(boton, evento) {
					var cg_email_agrega = Ext.ComponentQuery.query('#cg_email_agrega')[0];
					if(Ext.isEmpty(cg_email_agrega.getValue())){
						cg_email_agrega.markInvalid("Favor de capturar la direccion de correo electr�nico");
						cg_email_agrega.focus();						
						return;
					}
					var numRegistros=0;
					var  grid =   Ext.ComponentQuery.query('#grid')[0];
					var store = grid.getStore();
					var jsonData = grid.getStore().getRange();
					inicializaVariables();
					Ext.each(jsonData, function (item) {
						origen.push(item.data.origen);	
						ic_envio_clientes.push(item.data.ic_envio_clientes);
						cg_email.push(item.data.cg_email);
						cs_envio_mensual.push(item.data.cs_envio_mensual);
						cs_envio_diario.push(item.data.cs_envio_diario);
						numRegistros++;
					});
					boton.setDisabled(true);
					boton.setIconCls('x-mask-msg-text');
					consultaData.load({
						params: Ext.apply(
							{
								informacion: 'Consulta_info',
								origen:   origen,
								ic_envio_clientes:    ic_envio_clientes,
								cg_email:    cg_email,
								cs_envio_mensual:   cs_envio_mensual,
								cs_envio_diario:    cs_envio_diario,
								numRegistros:    numRegistros,
								cg_email_agrega:    Ext.ComponentQuery.query('#cg_email_agrega')[0].getValue(),
								hidAction:    'A'
								
							})
						});	
				}
			},
			{
				xtype: 'button',
				text:       '<center>Detalle</center>',
				itemId:     'btnDetalle',
				handler:    verDetalle
			}
		]
	});
	var  grid = Ext.create('Ext.grid.Panel',{
		itemId: 'grid',
		store:            Ext.data.StoreManager.lookup('consultaData'),
	   title: 'Consulta',
		xtype:           'grouped-header-grid',
		style: 'margin: 10px auto 0px auto;',
     	hidden:  false,
		frame: true,
		height: 400,		
		width: 730,
		columns: [
			{
				
				dataIndex: 'ic_envio_clientes',
				sortable: true,
				width: 200,
				hidden:true,
				align: 'center',
				resizable: true
			},
			{
				
				dataIndex: 'cg_email',
				sortable: true,
				width: 200,
				hidden:true,
				align: 'center',
				resizable: true
			},
			{
				
				dataIndex: 'cs_envio_diario',
				sortable: true,
				width: 200,
				hidden:true,
				align: 'center',
				resizable: true
			},
			{
				
				dataIndex: 'cs_envio_mensual',
				sortable: true,
				width: 200,
				hidden:true,
				align: 'center',
				resizable: true
			},
			{
				
				dataIndex: 'origen',
				sortable: true,
				width: 200,
				hidden:true,
				align: 'center',
				resizable: true
			},
			{
				header: 'Correo Electr�nico',
				tooltip: 'Correo Electr�nico',	
				dataIndex: 'cg_email',
				sortable: true,
				width: 200,
				align: 'center',
				resizable: true
			},
			{
				header: 'Env�o Diario de Clientes Interesados',
				tooltip: 'Env�o Diario de Clientes Interesados',	
				dataIndex: 'cs_envio_diario',
				sortable: true,
				width: 200,
				align: 'center',
				resizable: true,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['cs_envio_diario']=='S' ){
							return '<input  id="chkCredito" type="checkbox" checked  onclick="cambioEnvioDiario(this, '+rowIndex +','+colIndex+');" />';
					}else{
							return '<input  id="chkCredito" type="checkbox"  onclick="cambioEnvioDiario(this, '+rowIndex +','+colIndex+');" />';
					}
				}
			},
			{
				header: 'Env�o Mensual de Clientes Potenciales',
				tooltip: 'Env�o Mensual de Clientes Potenciales',	
				dataIndex: 'cs_envio_mensual',
				sortable: true,
				width: 200,
				align: 'center',
				resizable: true,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){			
					if(record.data['cs_envio_mensual']=='S' ){
							return '<input  id="chkCredito" type="checkbox" checked  onclick="cambioEnvioMensual(this, '+rowIndex +','+colIndex+');" />';
					}else{
							return '<input  id="chkCredito" type="checkbox"  onclick="cambioEnvioMensual(this, '+rowIndex +','+colIndex+');" />';
					}
				}
			},
			{
				width:         100,
				xtype:         'actioncolumn',
				header:        'Eliminar',
				align:         'center',
				sortable:      false,
				resizable:     false,
				menuDisabled:  true,
				items: [
					{
						getClass:   function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Eliminar';
							return 'icoEliminar';
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
							var registro = grid.getStore().getAt(rowIndex);
							var origen = registro.get('origen');
							if(origen!='eliminar'){
								eliminaReg(grid, rowIndex, colIndex, item, event);
							}
						},
						isDisabled: function(view, rowIndex, colIndex, item, record) {
							 if(record.data['origen']=='eliminar' ){
								return !record.get('editable');
							 }
							 
						}
					}
				]
			}
		],
		bbar: [
			'->',
			{
				text: 'Aceptar',
				id: 'btnAceptar',	
				iconCls: 'icoAceptar',
				handler: procesarAceptar
				
			}
			
			
		]
	});

	var main = Ext.create('Ext.container.Container',{
		itemId:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		width:      949,
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			grid
		]
	});
	
	
	
});