Ext.onReady(function(){
	var _OPERACION_="I";
	var _mess = "";
	var evento = "";
	
	
	function procesaGenerarArchivo(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var resp=Ext.JSON.decode(response.responseText);
			if(resp.PROCESO_FASE=='2') {
				Ext.Ajax.request({
					url:'15reporte7_Ext01.data.jsp',
					params:{
						informacion:'Generar_archivo',
						fase_proceso: resp.PROCESO_FASE
					},
					callback: procesaGenerarArchivo
				});
			}else if(resp.PROCESO_FASE=='3') {	
				Ext.Msg.alert('Aviso','<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>'+resp.mensaje+'</center></td></tr> </table> </div>');
										
			}else if(resp.PROCESO_FASE=='4') {

				var boton = Ext.ComponentQuery.query('#btnGenerar')[0];
				
				boton.setIconCls('icoGenerar');
				boton.enable();
				
				var archivo = resp.urlArchivo;				
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};	
				var forma  	= Ext.getDom('formAux'); 
				forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				forma.method 		= 'post';
				forma.target 		= '_self'; 	
				forma.submit();	
			}
			 
		} else {
			
			NE.util.mostrarErrorPeticion(response);
		}
	
	}
	function generar(boton) {
		
		
		var epoSele = Ext.ComponentQuery.query('#sNoEpo')[0];
		
		var sPeriodo1 = Ext.ComponentQuery.query('#sPeriodo1')[0];
		var sPeriodo2 = Ext.ComponentQuery.query('#sPeriodo2')[0];
		var sPeriodo3 = Ext.ComponentQuery.query('#sPeriodo3')[0];
		
		var mesInicial = Ext.ComponentQuery.query('#mesInicial')[0];
		var anioInicial = Ext.ComponentQuery.query('#anioInicial')[0];
		var mesFinal = Ext.ComponentQuery.query('#mesFinal')[0];
		var anioFinal = Ext.ComponentQuery.query('#anioFinal')[0];
		if(epoSele.getValue() == '') {
			epoSele.markInvalid('Debe de seleccionar y pasar las epos para la generaci�n del archivo.');
			epoSele.focus();
			return false;
		}else if(sPeriodo1.getValue() == false&&sPeriodo2.getValue() == false&&sPeriodo3.getValue() == false) {
				Ext.Msg.alert('Aviso','<div > <table width="300" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>Debe de seleccionar por lo menos un criterio para presentar la informaci�n.</center></td></tr> </table> </div>');
			return false;
		}
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15reporte7_Ext01.data.jsp',
			params: Ext.apply({
				informacion:  'Generar_archivo',
				mesInicial: Ext.ComponentQuery.query('#mesInicial')[0].getValue(),
				anioInicial:    Ext.ComponentQuery.query('#anioInicial')[0].getValue(),
				mesFinal:      Ext.ComponentQuery.query('#mesFinal')[0].getValue(),
				anioFinal:    Ext.ComponentQuery.query('#anioFinal')[0].getValue(),
				sNoEpos:    epoSele.getValue(),
				sPeriodo1:    Ext.ComponentQuery.query('#sPeriodo1')[0].getValue(),
				sPeriodo2:      Ext.ComponentQuery.query('#sPeriodo2')[0].getValue(),
				sPeriodo3: Ext.ComponentQuery.query('#sPeriodo3')[0].getValue(),
				fase_proceso: '1'
			}),
			callback: procesaGenerarArchivo
		});
			
	}
	
	function validarEnvio(f) {
		var iTotalEpos = f.sNoEpoSelec.length;
		if(iTotalEpos == 0) {
			alert('Debe de seleccionar y pasar las epos para la generaci�n del archivo.');
			f.sNoEpo.focus();
			return false;
		}
		var bHayPeriodoSelec = false;
		for(p=0; p<f.sPeriodo.length; p++) {
			if(f.sPeriodo[p].checked)
				bHayPeriodoSelec = true;
		}
		if(!bHayPeriodoSelec) {
			alert('Debe de seleccionar por lo menos un criter�o para presentar la informaci�n.');
			//f.sPeriodo.focus();
			return false;
		}
	return true;
	}
	
	function validaDiasPeriodo() {
			var mesInicial = "";
			var mesFinal = "";
			if (parseInt(Ext.ComponentQuery.query('#mesInicial')[0].getValue()) < 10) {mesInicial = "0" + Ext.ComponentQuery.query('#mesInicial')[0].getValue()} else {mesInicial = Ext.ComponentQuery.query('#mesInicial')[0].getValue()};
			if (parseInt(Ext.ComponentQuery.query('#mesFinal')[0].getValue()) < 10) {mesFinal = "0" + Ext.ComponentQuery.query('#mesFinal')[0].getValue()} else {mesFinal = Ext.ComponentQuery.query('#mesFinal')[0].getValue()};
			
			var fechaInicial = "01/" + mesInicial + "/" + Ext.ComponentQuery.query('#anioInicial')[0].getValue();
			var fechaFinal = "01/" + mesFinal + "/" + Ext.ComponentQuery.query('#anioFinal')[0].getValue()
			
			if (comparaFechas(fechaInicial, fechaFinal)) {
				
				var diasPeriodo = mtdCalculaPlazo(fechaInicial, fechaFinal);
				
				if (esAnioBisiesto(parseInt(Ext.ComponentQuery.query('#anioInicial')[0].getValue())) && 2 >= parseInt(mesInicial)) {
					diasPeriodo--;
				}
			
				if (diasPeriodo == 365) {
					var arrayPeriodo = Ext.ComponentQuery.query('#formaPrincipal checkboxfield[identificador=periodo]');
					for (var i = 0; i < arrayPeriodo.length; i++) {
						arrayPeriodo[i].setDisabled(false);
					}
					arrayPeriodo[arrayPeriodo.length - 1].setValue(false);
				} else {
					var arrayPeriodo = Ext.ComponentQuery.query('#formaPrincipal checkboxfield[identificador=periodo]');
					for (var i = 0; i < arrayPeriodo.length - 1; i++) {
						var peri = arrayPeriodo[i];
						peri.setDisabled(true);
					}
					
					arrayPeriodo[arrayPeriodo.length - 1].setDisabled(false);
					arrayPeriodo[arrayPeriodo.length - 1].setValue(true);
				}			
			} else {
					var arrayPeriodo = Ext.ComponentQuery.query('#formaPrincipal checkboxfield[identificador=periodo]');
					arrayPeriodo[arrayPeriodo.length - 1].setValue(false);
					for (var i = 0; i < arrayPeriodo.length; i++) {
						arrayPeriodo[i].setDisabled(true);
					}
				alert("El periodo inicial debe ser menor que el periodo final.");
				return;
			}
		}
	function enviaAnio() {
		var forma    = Ext.getDom('formAux');
		forma.action = '15reporte7_Ext01.js';
		forma.submit();
	}
	var elementosDetalle = [
		{
				xtype:'label',
					style:
                {
                    'font-size' : '12px'
                },
				width:	750,
				html:	'<table width="420" cellpadding="0" cellspacing="0" border="0">'+
							'<tr>'+
							'<td valign="top">'+
							'<table cellpadding="0" cellspacing="0" border="0">'+
								'<tr>'+
								'<td valign="top" align="center"><br>'+
								'<table width="420" cellpadding="0" cellspacing="1" border="0" class="formas">'+
									'<tr>'+
									'<td class="formas" align="center"><b>CLIENTES SUSCEPTIBLES DE CREDICADENAS</b></td>'+
				
								'</tr>'+
				
								'<tr>'+
				
									'<td class="formas">'+
				
										'<i><b>Objetivo:</b></i><br>'+
				
										'Emitir una base de datos de los clientes de Factoraje Electr&oacute;nico que cumplen las condiciones establecidas por riesgos para participar en el programa de Credicadenas.<br>'+
				
										'<br><i><b>Periodicidad:</b></i><br>'+
				
										'Mensual.<br>'+
				
										'<br><i><b>Destinatario:</b></i><br>'+
				
										'&nbsp;* Subdirecci�n de Productos de Financiamiento.<br>'+
				
										'&nbsp;* Rafael Velasco.<br>'+
				
										'&nbsp;* Alejandro Aguirre.<br>'+
				
										'<br><i><b>Fuente:</b></i><br>'+
				
										'Base de datos de  Nafin-electr&oacute;nico.<br>'+
				
										'<br><i><b>Formato:</b></i><br>'+
				
										'Excel (.csv)<br>'+
				
										'<br><i><b>Instrucciones:</b></i><br>'+
				
										'&nbsp;- Seleccionar el mismo mes en ambos criterios.<br>'+
				
										'<br><i><b>En excel:</i></b><br>'+
				
										'&nbsp;- Realizar las adecuaciones necesarias.<br>'+
				
										'&nbsp;- Aplicar el formato deseado y guardar el reporte.<br>'+
				
										'<br><i><b>Contactos:</i></b><br>'+
				
										'Rafael Velasco Posada - Subdirecci�n de Productos Electr&oacute;nicos y Financiamiento.<br>'+
				
										'Alejandro Aguirre - Credicadenas.<br>'+
				
										'<br><i><b>Observaciones:</i></b><br>'+
				
										'Las condiciones para estos clientes son:<br>'+
				
										"&nbsp;- Pertenecer a las Epo's seleccionadas.<br>"+
				
										'&nbsp;- Tener publicaciones en: 6 meses consecutivos &oacute;<br>'+
				
										'&nbsp;- Por lo menos en  8 de 12 meses.<br>'+
				
									'</td>'+
				
								'</tr>'+
				
								'</table>'+
				
							'</td>'+
				
						'</tr>'+
				
						'</table>'+
						'</td>'+
				
				'</tr>'+
				
				'</table>'

		}
	]
	var fpDetalle = {
		xtype: 'form',
		itemId					: 'fpDetalle',	
		width				: 500,
		height: 550,
		style				: ' margin:0 auto;',
		frame				: false,
		hidden			: false,
		fileUpload: true,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 200,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items: elementosDetalle	
	};
	function verDetalle(){
		var winDetalle =Ext.ComponentQuery.query('winDetalle')[0];
		if(winDetalle){
			
			winDetalle.show();
		} else{
					new Ext.Window({
								layout: 'fit',
								modal: true,
								width: 500,
								frame:       false,
								constrain:   true,
								height: 550,
								resizable: false,
								closable: true,
								x: 450,
								itemId: 'winDetalle',
								autoDestroy:false,
								closeAction: 'destroy',
								items: [
									fpDetalle
								],
								title: ''
						}).show();	
		}
		
	}
	
	

	var procesarGetPyme = function(opts, success, response) {
		
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			 var json=Ext.JSON.decode(response.responseText);
			 var resultado = parseInt(json.resultado);
			 if(resultado>0){
				Ext.ComponentQuery.query('#ttlPymes')[0].setValue(resultado);
			 }else{
				Ext.ComponentQuery.query('#ttlPymes')[0].setValue('0');
			 }
			 
		} else {
			
			NE.util.mostrarErrorPeticion(response);
		}
		
	}
	// Se crea el MODEL para el cat�logo 'Raz�n Social'
	Ext.define('ModelAnio',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	Ext.define('ModelMes',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	
	Ext.define('ModelEpo',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'CLAVE',       type: 'string'},
				{ name: 'DESCRIPCION', type: 'string'}
			]
		}
	);

	var procesarMes = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#mesInicial')[0].setValue('1');
		}
	}
	var procesarMesFin = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#mesFinal')[0].setValue('1');
		}
	}
	
	var procesarAnioIni = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#anioInicial')[0].setValue(arrRegistros[(arrRegistros.length-2)]);
		}
	}
	var procesarAnioFin = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#anioFinal')[0].setValue(arrRegistros[(arrRegistros.length-1)]);
		}
	}
	
	
	var storeMes = Ext.create('Ext.data.Store',{
		model: 'ModelMes',
		proxy: {
			type: 'ajax',
			url: '15reporte7_Ext01.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Mes'
			},
			autoLoad: true,
			listeners: {
				load : procesarMes,
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		 autoLoad: true
	});
	var storeAnio = Ext.create('Ext.data.Store',{
		model: 'ModelAnio',
		proxy: {
			type: 'ajax',
			url: '15reporte7_Ext01.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Anio'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		 autoLoad: true
	});
	var procesarStoreData = function(store, arrRegistros, success, opts){
		main.el.unmask();
	}
	var storeData = Ext.create('Ext.data.Store',{
		model: 'ModelEpo',
		itemId:'storeData',
		proxy: {
			type: 'ajax',
			url: '15reporte7_Ext01.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Epo',
				filtroEpo:''
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		 autoLoad: true,
		 listeners: {
			load: procesarStoreData
		}
	});
	
	var elementosForma = [
		{
			xtype:'panel',
			frame: true,
			width:                     590,				 
			border	: false,
			cls: 'panel-sin-borde',
			layout:'table',
			items:[
				{
					xtype: 'combo',
					itemId:  'mesInicial',
					fieldLabel: 'Per�odo de',
					name:  'mesInicial',
					hiddenName: 'mesInicial',
					forceSelection:true,
					allowBlank: false,
					emptyText: 'Seleccione...',
					queryMode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					typeAhead:		true,
					triggerAction:	'all',
					width:  210,
					store: Ext.create('Ext.data.Store', {
						model:'ModelMes',
						autoLoad: true,
						proxy: {
							type: 'ajax',
							url: '15reporte7_Ext01.data.jsp',
							reader: {
								type: 'json',
								root: 'registros'
							},
							extraParams: {
								informacion: 'Consulta_Mes'
							},
								totalProperty:  'total'
							},
							listeners: {
								load : procesarMes,
								exception: NE.util.mostrarProxyAjaxError
							}
					}),
					listeners: {
						select: function(combo, record, index) {
									validaDiasPeriodo();					 
							}
					}
				},
				{
					xtype: 'displayfield',
					value: ' ',
					width:  10
				},
				{
					xtype: 'combo',
					itemId:  'anioInicial',
					name:  'anioInicial',
					hiddenName: 'anioInicial',
					forceSelection:true,
					allowBlank: false,
					labelWidth: 110,
					emptyText: 'Seleccione...',
					queryMode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					typeAhead:		true,
					triggerAction:	'all',
					width:  105,
					store: Ext.create('Ext.data.Store', {
						model:'ModelMes',
						autoLoad: true,
						proxy: {
							type: 'ajax',
							url: '15reporte7_Ext01.data.jsp',
							reader: {
								type: 'json',
								root: 'registros'
							},
							extraParams: {
								informacion: 'Consulta_Anio'
							},
								totalProperty:  'total'
							},
							listeners: {
								load : procesarAnioIni,
								exception: NE.util.mostrarProxyAjaxError
							}
					}),
					listeners: {
						select: function(combo, record, index) {
									validaDiasPeriodo();					 
							}
					}
				},
				{
					xtype: 'displayfield',
					value: '<center>  a  </center>',
					width:  20
				},
				{
					xtype: 'combo',
					itemId:  'mesFinal',
					name:  'mesFinal',
					hiddenName: 'mesFinal',
					forceSelection:true,
					allowBlank: false,
					labelWidth: 105,
					emptyText: 'Seleccione...',
					queryMode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					typeAhead:		true,
					triggerAction:	'all',
					width:  110,
					store: Ext.create('Ext.data.Store', {
						model:'ModelMes',
						autoLoad: true,
						proxy: {
							type: 'ajax',
							url: '15reporte7_Ext01.data.jsp',
							reader: {
								type: 'json',
								root: 'registros'
							},
							extraParams: {
								informacion: 'Consulta_Mes'
							},
								totalProperty:  'total'
							},
							listeners: {
								load : procesarMesFin,
								exception: NE.util.mostrarProxyAjaxError
							}
					}),
					listeners: {
						select: function(combo, record, index) {
									validaDiasPeriodo();					 
							}
					}
				},
				{
					xtype: 'displayfield',
					value: ' ',
					width:  10
				},
				{
					xtype: 'combo',
					itemId:  'anioFinal',
					name:  'anioFinal',
					hiddenName: 'anioFinal',
					forceSelection:true,
					allowBlank: false,
					labelWidth: 100,
					emptyText: 'Seleccione...',
					queryMode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					typeAhead:		true,
					triggerAction:	'all',
					width:  110,
					store: Ext.create('Ext.data.Store', {
						model:'ModelMes',
						autoLoad: true,
						proxy: {
							type: 'ajax',
							url: '15reporte7_Ext01.data.jsp',
							reader: {
								type: 'json',
								root: 'registros'
							},
							extraParams: {
								informacion: 'Consulta_Anio'
							},
								totalProperty:  'total'
							},
							listeners: {
								load : procesarAnioFin,
								exception: NE.util.mostrarProxyAjaxError
							}
					}),
					listeners: {
						select: function(combo, record, index) {
									validaDiasPeriodo();					 
							}
					}
				}
			]
		},
		{
			xtype:'panel',
			frame: true,
			width:                     590,				 
			border	: false,
			cls: 'panel-sin-borde',
			layout:'table',
			items:[
				{
					xtype: 'displayfield',
					value: ' ',
					width:  320
				},
				{
						xtype: 'fieldcontainer',
						defaultType: 'checkboxfield',
						items: [
							 {
								  boxLabel  : 'PEF',
								  name      : 'eposPEF',
								  inputValue: 'S',
								  itemId        : 'eposPEF',
								  listeners: {
										change : {
														fn : function(checkbox, newValue, oldValue, eOpts) {
														
															
															//if(newValue==true){
																storeData.removeAll();
																Ext.ComponentQuery.query('#sNoEpo')[0].reset();
																main.el.mask('Procesando...', 'x-mask-loading');
																storeData.load({
																	params: {
																		informacion : 'Consulta_Epo',
																		filtroEpo:'',
																		eposPEF:Ext.ComponentQuery.query('#eposPEF')[0].getValue(),
																		eposGobMun:Ext.ComponentQuery.query('#eposGobMun')[0].getValue(),
																		eposPrivadas:Ext.ComponentQuery.query('#eposPrivadas')[0].getValue(),
																		eposCredi:Ext.ComponentQuery.query('#eposCredi')[0].getValue(),
																		eposNC:Ext.ComponentQuery.query('#eposNC')[0].getValue()
																			 	  
																	}
																});
																
														//	}
														}
													}
								  }
							 }, {
								  boxLabel  : 'Gobiernos y Municipios',
								  name      : 'eposGobMun',
								  inputValue: 'S',
								  itemId        : 'eposGobMun',
								  listeners: {
										change : {
														fn : function(checkbox, checked){
															//if(checked==true){
																storeData.removeAll();
																Ext.ComponentQuery.query('#sNoEpo')[0].reset();
																main.el.mask('Procesando...', 'x-mask-loading');
																storeData.load({
																	params: {	
																		filtroEpo:'',
																		eposPEF:Ext.ComponentQuery.query('#eposPEF')[0].getValue(),
																		eposGobMun:Ext.ComponentQuery.query('#eposGobMun')[0].getValue(),
																		eposPrivadas:Ext.ComponentQuery.query('#eposPrivadas')[0].getValue(),
																		eposCredi:Ext.ComponentQuery.query('#eposCredi')[0].getValue(),
																		eposNC:Ext.ComponentQuery.query('#eposNC')[0].getValue()
																			 	  
																	}
																});
															//}
														}
													}
								  }
							 }, {
								  boxLabel  : 'Privadas',
								  name      : 'eposPrivadas',
								  inputValue: 'S',
								  itemId        : 'eposPrivadas',
								  listeners: {
										change : {
														fn : function(checkbox, checked){
															//if(checked==true){
																storeData.removeAll();
																Ext.ComponentQuery.query('#sNoEpo')[0].reset();
																main.el.mask('Procesando...', 'x-mask-loading');
																storeData.load({
																	params: {
																		filtroEpo:'',
																		eposPEF:Ext.ComponentQuery.query('#eposPEF')[0].getValue(),
																		eposGobMun:Ext.ComponentQuery.query('#eposGobMun')[0].getValue(),
																		eposPrivadas:Ext.ComponentQuery.query('#eposPrivadas')[0].getValue(),
																		eposCredi:Ext.ComponentQuery.query('#eposCredi')[0].getValue(),
																		eposNC:Ext.ComponentQuery.query('#eposNC')[0].getValue()
																			 	  
																	}
																});
															//}
														}
													}
								  }
							 }, {
								  boxLabel  : "EPO's que pertenecen a Credicadenas",
								  name      : 'eposCredi',
								  inputValue: 'S',
								  itemId        : 'eposCredi',
								  listeners: {
										change : {
														fn : function(checkbox, checked){
															//if(checked==true){
																storeData.removeAll();
																Ext.ComponentQuery.query('#sNoEpo')[0].reset();
																main.el.mask('Procesando...', 'x-mask-loading');
																storeData.load({
																	params: {
																		filtroEpo:'',
																		eposPEF:Ext.ComponentQuery.query('#eposPEF')[0].getValue(),
																		eposGobMun:Ext.ComponentQuery.query('#eposGobMun')[0].getValue(),
																		eposPrivadas:Ext.ComponentQuery.query('#eposPrivadas')[0].getValue(),
																		eposCredi:Ext.ComponentQuery.query('#eposCredi')[0].getValue(),
																		eposNC:Ext.ComponentQuery.query('#eposNC')[0].getValue()
																			 	  
																	}
																});
															//}
														}
													}
								  }
							 }, {
								  boxLabel  : "EPO's No Clasificadas",
								  name      : 'eposNC',
								  inputValue: 'S',
								  itemId        : 'eposNC',
								  listeners: {
										change : {
														fn : function(checkbox, checked){
															//if(checked==true){
																storeData.removeAll();
																Ext.ComponentQuery.query('#sNoEpo')[0].reset();
																main.el.mask('Procesando...', 'x-mask-loading');
																storeData.load({
																	params: {
																		filtroEpo:'',
																		eposPEF:Ext.ComponentQuery.query('#eposPEF')[0].getValue(),
																		eposGobMun:Ext.ComponentQuery.query('#eposGobMun')[0].getValue(),
																		eposPrivadas:Ext.ComponentQuery.query('#eposPrivadas')[0].getValue(),
																		eposCredi:Ext.ComponentQuery.query('#eposCredi')[0].getValue(),
																		eposNC:Ext.ComponentQuery.query('#eposNC')[0].getValue()
																			 	  
																	}
																});
															//}
														}
													}
								  }
							 }
						]
				  }
				  
			]
		},
		{
			xtype: 'itemselector',
         name: 'sNoEpo',
         itemId: 'sNoEpo',
         fieldLabel: 'Epo',
         imagePath: '/nafin/00utils/extjs/ux/images/',
         store: storeData,
         displayField: 'DESCRIPCION',
         valueField: 'CLAVE',
			width: 580,
			height: 200,
         msgTarget: 'side',
			buttons: [ 'add', 'remove'],
			listeners:{
				change: function (f,newV,oldV,opts){
					Ext.ComponentQuery.query('#eposCredi')[0].setValue(false);
					if(newV!=''){
						
						 Ext.Ajax.request({
							url: '15reporte7_Ext01.data.jsp',
							params: Ext.apply({
								informacion: 'total_pymes',
								sNoEpo:String(newV)
							}),
							callback: procesarGetPyme
						});
					}else{
						Ext.ComponentQuery.query('#ttlPymes')[0].setValue('0');
					}
						
            }
			}
      },
		
		{
			xtype:'panel',
			frame: true,
			width:                     590,				 
			border	: false,
			layout:'table',
			cls: 'panel-sin-borde',
			items:[
				{
					xtype: 'displayfield',
					value: ' ',
					width:  400
				},
				{
					xtype: 'displayfield',
					fieldLabel: 'Total de Pymes',
					itemId: 'ttlPymes',
					name: 'ttlPymes',
					width:  250,
					value:'0'
				}
			]
		},
		{
			xtype:'panel',
			frame: true,
			width:                     590,				 
			border	: false,
			layout:'table',
			itemId:'pnlPeriodo',
			cls: 'panel-sin-borde',
			items:[
				{
					xtype: 'displayfield',
					value: ' ',
					width:  240
				},
				{
						xtype: 'fieldcontainer',
						defaultType: 'checkboxfield',
						fieldLabel:  'Informaci&oacute;n a Presentar',
						items: [
							 {
								  boxLabel  : '�ltimos seis meses consecutivos',
								  name      : 'sPeriodo',
								  inputValue: '6',
								  identificador:'periodo',
								  itemId        : 'sPeriodo1'
							 }, {
								  boxLabel  : 'Ocho de Doce',
								  name      : 'sPeriodo',
								  inputValue: '7',
								  identificador:'periodo',
								  itemId        : 'sPeriodo2'
							 }, {
								  boxLabel  : 'Todos',
								  name      : 'sPeriodo',
								  inputValue: 'T',
								  identificador:'periodo',
								  itemId        : 'sPeriodo3'
							 }
						]
				  }
			]
		}
	]
	var formaPrincipal = new Ext.form.FormPanel({
		width:                     600,
		height :'auto',
		itemId:                    'formaPrincipal',
		title:                     'Clientes Susceptibles de Credicadenas',
		bodyPadding:               '0 0 0 0',
		style:                     'margin: 10px auto 10px auto;',
		frame:                     true,
		border:                    true,
		fieldDefaults: {
			msgTarget:              'side',
			labelWidth:             70
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text:                   'Regresar',
				itemId:                 'btnRegresar',
				iconCls:                'icoRegresar',
				handler: function(boton, evento) {
					window.location.href='15reporte7_Ext01.jsp';
				}
			},
			{
				text:                   '<center>Generar</center>',
				itemId:                 'btnGenerar',
				iconCls:                'icoGenerar',
				handler:                generar
			},
			{
				xtype: 'button',
				text:       '<center>Detalle</center>',
				itemId:     'btnDetalle',
				handler:    verDetalle
			}
		]
	});

	var main = Ext.create('Ext.container.Container',{
		itemId:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		width:      949,
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal
		]
	});
	
	
	
});