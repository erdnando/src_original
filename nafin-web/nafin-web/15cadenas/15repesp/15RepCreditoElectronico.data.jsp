<%@ page contentType="application/json;charset=UTF-8" 
	import="
		java.util.*,
		java.text.*,	
		netropology.utilerias.*,	
		com.netro.cadenas.*,
		java.math.*,
		com.netro.model.catalogos.*,	
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		javax.naming.Context,
		org.apache.commons.logging.Log"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion	= (request.getParameter("informacion")		== null)?"":request.getParameter("informacion");

String reporte_credito	= (request.getParameter("reporte_credito")		== null)?"":request.getParameter("reporte_credito");
String base_operacion	= (request.getParameter("base_operacion")		== null)?"":request.getParameter("base_operacion");
String clave_if	= (request.getParameter("clave_if")		== null)?"":request.getParameter("clave_if");
String tipo_credito	= (request.getParameter("tipo_credito")		== null)?"":request.getParameter("tipo_credito");
String clave_estado	= (request.getParameter("clave_estado")		== null)?"":request.getParameter("clave_estado");
String fecha_public_oper_ini	= (request.getParameter("fecha_public_oper_ini")		== null)?"":request.getParameter("fecha_public_oper_ini");
String fecha_public_oper_fin	= (request.getParameter("fecha_public_oper_fin")		== null)?"":request.getParameter("fecha_public_oper_fin");
String tipo	= (request.getParameter("tipo")		== null)?"":request.getParameter("tipo");
String persona	= (request.getParameter("persona")		== null)?"":request.getParameter("persona");
String titulo	= (request.getParameter("titulo")		== null)?"":request.getParameter("titulo");


String infoRegresar ="", consulta ="";
String  periodo ="Período del "+fecha_public_oper_ini+" al "+fecha_public_oper_fin;

List catalogo = new ArrayList();
JSONArray jsObjArray = new JSONArray();
JSONArray  registros = new JSONArray();
JSONObject jsonObj = new JSONObject();

HashMap datos  = new HashMap();


//Instanciación para uso del EJB
InteligenciaComercial inteligenciaComercial = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);
 

log.debug ("informacion  "+informacion); 
log.debug ("base_operacion  "+base_operacion); 	
log.debug ("clave_if  "+clave_if); 
log.debug ("tipo_credito  "+tipo_credito); 
log.debug ("clave_estado  "+clave_estado); 
log.debug ("fecha_public_oper_ini  "+fecha_public_oper_ini); 
log.debug ("fecha_public_oper_fin  "+fecha_public_oper_fin); 
log.debug ("reporte_credito  "+reporte_credito); 
log.debug ("persona  "+persona); 
log.debug ("titulo  "+titulo); 
log.debug ("periodo  "+periodo); 
log.debug ("tipo  "+tipo); 

List parametros = new ArrayList();      
 

RepCreditoElectronico pag = new RepCreditoElectronico();
pag.setInformacion(informacion);
pag.setPeriodo(periodo);
pag.setReporte_credito(reporte_credito);
pag.setPath(strDirectorioTemp);
pag.setTipo(tipo);
pag.setTitulo(titulo); 

if ( informacion.equals("catalogoBaseOperacion") )	{

	catalogo = new ArrayList();
	jsObjArray = new JSONArray();
	
	if(reporte_credito.equals("INT_FIN") && !clave_if.equals("")  ){
	
		catalogo = inteligenciaComercial.obtenerCatalogoBaseOperacion(clave_if);
		
	}else if(reporte_credito.equals("BASE_OPER") || reporte_credito.equals("") ){
	
	 catalogo = inteligenciaComercial.obtenerCatalogoBaseOperacion("");	 
	 
	 }
	 
	jsObjArray = JSONArray.fromObject(catalogo);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if ( informacion.equals("catalogoIF") )	{

	catalogo = new ArrayList();
	jsObjArray = new JSONArray();
	
	if(reporte_credito.equals("BASE_OPER") || reporte_credito.equals("") ){
	
		catalogo = inteligenciaComercial.obtenerCatalogoIF(base_operacion);
		
	}else if(reporte_credito.equals("INT_FIN")){
	
		catalogo = inteligenciaComercial.obtenerCatalogoIF("");
		
	}
	
	
	jsObjArray = JSONArray.fromObject(catalogo);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if ( informacion.equals("catalogoTipoCredito") )	{

	catalogo = new ArrayList();
	jsObjArray = new JSONArray();
	
	if(reporte_credito.equals("BASE_OPER") || reporte_credito.equals("") ){
		catalogo = inteligenciaComercial.obtenerCatalogoTipoCredito(clave_if, "");
		
	}else if(reporte_credito.equals("INT_FIN")  && !clave_if.equals("")   &&  !base_operacion.equals("")   ){
		catalogo = inteligenciaComercial.obtenerCatalogoTipoCredito(clave_if, base_operacion);
	}
	
	jsObjArray = JSONArray.fromObject(catalogo);	
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if ( informacion.equals("catalogoEstado") )	{

	CatalogoEstado cat = new CatalogoEstado();
   cat.setCampoClave("ic_estado");
   cat.setCampoDescripcion("cd_nombre");
   cat.setClavePais("24");
	infoRegresar = cat.getJSONElementos();		

}else if ( informacion.equals("Consultar") || informacion.equals("Generar_Arch")  )	{

		parametros.add(base_operacion);
		parametros.add(clave_if);
		parametros.add(tipo_credito);
		parametros.add(clave_estado);
		parametros.add(fecha_public_oper_ini);
		parametros.add(fecha_public_oper_fin);
		parametros.add(reporte_credito);	

		List regRepor = inteligenciaComercial.reporteCreditoElectronicoBO(parametros);
		
	
	if ( informacion.equals("Consultar") )	{
		int total_registros = 0;
		BigDecimal total_monto_solic = new BigDecimal("0.00");
		BigDecimal total_monto_desc = new BigDecimal("0.00");
		int total_personas_f = 0;
		int total_personas_m = 0;
		for(int i= 0; i < regRepor.size(); i++){
			List registroReporteCredito = (List)regRepor.get(i);
				total_registros += Integer.parseInt((String)registroReporteCredito.get(8));
				total_monto_solic = total_monto_solic.add(new BigDecimal((String)registroReporteCredito.get(3)));
				total_monto_desc = total_monto_desc.add(new BigDecimal((String)registroReporteCredito.get(4)));
				total_personas_f += Integer.parseInt((String)registroReporteCredito.get(6));
				total_personas_m += Integer.parseInt((String)registroReporteCredito.get(7));
				
				datos  = new HashMap();
				
				datos.put("BASE_OPERACION",registroReporteCredito.get(0));
				datos.put("NOMBRE_IF",registroReporteCredito.get(1));							
				datos.put("TIPO_DE_CREDITO",registroReporteCredito.get(2));
				datos.put("TOTAL_REGISTOS",registroReporteCredito.get(8));
				datos.put("MONTO_SOLICITADO",registroReporteCredito.get(3));
				datos.put("MONTO_DESCUENTO",registroReporteCredito.get(4));
				datos.put("ESTADO_ACREDITADO",registroReporteCredito.get(5));
				datos.put("TOTAL_PFISICAS",registroReporteCredito.get(6));
				datos.put("TOTAL_PMORALES",registroReporteCredito.get(7));    
				
				datos.put("IC_BASE_OPERACION",registroReporteCredito.get(9)); 
				datos.put("CLAVE_IF",registroReporteCredito.get(10)); 
				datos.put("TIPO_CREDITO",registroReporteCredito.get(11)); 
				datos.put("CLAVE_ESTADO",registroReporteCredito.get(12)); 
				
				registros.add(datos);
				
		}
				
			
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		
		jsonObj.put("TOTAL_REGISTOS", String.valueOf(total_registros));
		jsonObj.put("TOTAL_MONTO_SOLICITADO", total_monto_solic);	
		jsonObj.put("TOTAL_MONTO_DESCUENTO", total_monto_desc);	
		jsonObj.put("TOTAL_PFISICAS", String.valueOf(total_personas_f));	
		jsonObj.put("TOTAL_PMORALES", String.valueOf(total_personas_m));	
		jsonObj.put("periodo", periodo);	
		jsonObj.put("reporte_credito", reporte_credito);	
		
		infoRegresar = jsonObj.toString(); 
	
	}else  if(informacion.equals("Generar_Arch") )	{
		
		String nombreArchivo = 	pag.getArchivosConst(request, regRepor );
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		infoRegresar = jsonObj.toString(); 
	}	
		
      
}else if ( informacion.equals("consDetPersonas") || informacion.equals("Generar_Arch_Det")  )	{

		parametros.add(base_operacion);
		parametros.add(clave_if);
		parametros.add(tipo_credito);
		parametros.add(clave_estado);
		parametros.add(fecha_public_oper_ini);
		parametros.add(fecha_public_oper_fin);		
		parametros.add(persona);  

	List regDetalle = inteligenciaComercial.obtenerDetalleCreditoElectronico(parametros);

	 if ( informacion.equals("consDetPersonas")  )	{
 
		for(int i= 0; i < regDetalle.size(); i++){
			List registroReporteCredito = (List)regDetalle.get(i);
			datos  = new HashMap();
			datos.put("NO_SIRAC",registroReporteCredito.get(0));
			datos.put("RFC",registroReporteCredito.get(1));
			datos.put("RAZON_SOCIAL",registroReporteCredito.get(2));
			datos.put("ESTADO",registroReporteCredito.get(3));
			datos.put("CIUDAD",registroReporteCredito.get(4));
			registros.add(datos);     
		}
			
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString(); 
		
	}else  if (  informacion.equals("Generar_Arch_Det")  )	{
	
		String nombreArchivo = 	pag.getArchivosConst(request,   regDetalle );
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		infoRegresar = jsonObj.toString(); 
	
	}

}


%>
<%=infoRegresar%> 