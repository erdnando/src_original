Ext.onReady(function(){

	var existe_mes = '';
	var ic_proceso = '';

	// Se arma el contenido de la ventana Detalles
	var elementosDetalle = [{
		width:	750,
		xtype:'label',
		style:{
			'font-size' : '12px'
		},
		html:	'<table width="420" cellpadding="0" cellspacing="0" border="0">'+
				'<tr>'+
				'<td valign="top">'+
				'<table cellpadding="0" cellspacing="0" border="0">'+
					'<tr>'+
					'<td valign="top" align="center"><br>'+
						'<table width="420" cellpadding="0" cellspacing="1" border="0" class="formas">'+
						'<tr>'+
							'<td class="formas" align="center"><b>CLIENTES SUSCEPTIBLES DE CREDICADENAS</b></td>'+
						'</tr>'+
						'<tr>'+
							'<td class="formas">'+
								'<i><b>Objetivo:</b></i><br>'+
									'Emitir una base de datos de los clientes de Factoraje Electr&oacute;nico que cumplen las condiciones establecidas por riesgos para participar en el programa de Credicadenas.<br>'+
								'<br><i><b>Periodicidad:</b></i><br>'+
									'Mensual.<br>'+
								'<br><i><b>Destinatario:</b></i><br>'+
									'&nbsp;* Subdirecci�n de Productos de Financiamiento.<br>'+
									'&nbsp;* Rafael Velasco.<br>'+
									'&nbsp;* Alejandro Aguirre.<br>'+
								'<br><i><b>Fuente:</b></i><br>'+
									'Base de datos de  Nafin-electr&oacute;nico.<br>'+
								'<br><i><b>Formato:</b></i><br>'+
									'Excel (.csv)<br>'+
								'<br><i><b>Instrucciones:</b></i><br>'+
									'&nbsp;- Seleccionar el mismo mes en ambos criterios.<br>'+
								'<br><i><b>En excel:</i></b><br>'+
									'&nbsp;- Realizar las adecuaciones necesarias.<br>'+
									'&nbsp;- Aplicar el formato deseado y guardar el reporte.<br>'+
								'<br><i><b>Contactos:</i></b><br>'+
									'Rafael Velasco Posada - Subdirecci�n de Productos Electr&oacute;nicos y Financiamiento.<br>'+
									'Alejandro Aguirre - Credicadenas.<br>'+
								'<br><i><b>Observaciones:</i></b><br>'+
									'Las condiciones para estos clientes son:<br>'+
									"&nbsp;- Pertenecer a las Epo's seleccionadas.<br>"+
									'&nbsp;- Tener publicaciones en: 6 meses consecutivos &oacute;<br>'+
									'&nbsp;- Por lo menos en  8 de 12 meses.<br>'+
							'</td>'+
						'</tr>'+
					'</table>'+
					'</td>'+
					'</tr>'+
				'</table>'+
					'</td>'+
					'</tr>'+
			'</table>'
	}]

//------------------------------ Handlers ----------------------------
	// Defino el tipo de archivo que se puede subir
	Ext.apply(Ext.form.VTypes, {
		archivoXls: function(v) {
			var bandera = false;
			var myRegex1 = /^.+\.([Xx][Ll][Ss])$/;
			var myRegex2 = /^.+\.([Xx][Ll][Ss][Xx])$/;
			if(myRegex1.test(v) || myRegex2.test(v)){
				bandera = true;
			}
			return bandera;
		},
		archivoXlsText: 'El formato del archivo de origen no es el correcto.<br> Formatos soportado: xls y xlsx'
	});

	function renderMoneda(value, metaData, record, rowIndex, colIndex, store){
		if(value != '') return '$' + value;
		else return value;
		
	}

	// Vuelve a cargar la p�gina
	function cancelar(){
		main.el.mask('Procesando...', 'x-mask-loading');
		window.location  = '15forma04ext.jsp?idMenu=15SUPCARARC'
	}

	// Muestra la ventana de Detalles
	function verDetalle(){
		var winDetalle =Ext.ComponentQuery.query('winDetalle')[0];
		if(winDetalle){
			winDetalle.show();
		} else{
			new Ext.Window({
				width:       500,
				height:      550,
				x:           450,
				layout:      'fit',
				itemId:      'winDetalle',
				closeAction: 'destroy',
				modal:       true,
				frame:       false,
				constrain:   true,
				resizable:   false,
				closable:    true,
				autoDestroy: false,
				items: [     fpDetalle]
			}).show();	
		}
	}

	// Una vez que se seleccionaron los campos y el archivo se procesa el archivo
	function continuar(){

		// Valido los campos obligatorios
		if(!this.up('form').getForm().isValid()){
			return;
		}

		var mes  = Ext.getCmp('mes_id').getValue();
		var anio = Ext.getCmp('anio_id').getValue();

		main.el.mask('Procesando...', 'x-mask-loading');

		Ext.ComponentQuery.query('#formaSolicitudes')[0].hide();
		Ext.ComponentQuery.query('#formaResumen')[0].hide();
		existe_mes = '';
		ic_proceso = '';

		Ext.getCmp('formaPrincipal').getForm().submit({
			clientValidation: true,
			url: '15forma04ext.data.jsp?informacion=CARGA_ARCHIVO&mes=' + mes + '&anio=' +  anio,
			success: function(form, action) {
				procesaContinuar(null, true, action.response );
			},
			failure: function(form, action) {
				action.response.status = 200;
				procesaContinuar(null, false, action.response );
			}
		});

	}

	// Procesa las solicitudes
	function preacuse(){

		var actualizar = false;
		if(existe_mes == 'S'){
			Ext.Msg.confirm('Confirmaci�n', 'El archivo correspondiente a este mes ya existe.<br>�Desea actualizarlo?',function(botonConf){
				if (botonConf == 'ok' || botonConf == 'yes'){
					main.el.mask('Procesando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '15forma04ext.data.jsp',
						params: {
							informacion: 'PREACUSE_CARGA_MASIVA',
							ic_proceso: ic_proceso
						},
						callback: procesarPreacuse
					});
				}
			});
		} else{
			main.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15forma04ext.data.jsp',
				params: {
					informacion: 'PREACUSE_CARGA_MASIVA',
					ic_proceso: ic_proceso
				},
				callback: procesarPreacuse
			});
		}
	}

	// Finaliza el procesamiento
	function cargaMasiva(){
		main.el.mask('Procesando...', 'x-mask-loading');
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		Ext.Ajax.request({
			url: '15forma04ext.data.jsp',
			params: {
				informacion: 'CARGA_MASIVA',
				ic_proceso:  ic_proceso,
				mes:         forma.query('#mes_id')[0].getValue(),
				anio:        forma.query('#anio_id')[0].getValue()
			},
			callback: procesarCargaMasiva
		});
	}

//------------------------------ Callback ----------------------------

	// Lleno el catalogo con el valor por default
	var procesarCatalogoMes = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
			var campo = forma.query('#mes_id')[0];
			var fecha = Ext.Date.format(Ext.getCmp('fecha_id').getValue(), 'm');
			campo.setValue(fecha);
		}
	}

	// Lleno el catalogo con el valor por default
	var procesarCatalogoAnio = function(store, arrRegistros, success, opts){
		if(store.getTotalCount() > 0){
			var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
			var campo = forma.query('#anio_id')[0];
			var fecha = Ext.Date.format(Ext.getCmp('fecha_id').getValue(), 'Y');
			campo.setValue(fecha);
		}
	}

	// Procesa la respuesta de la carga del archivo
	var procesaContinuar = function (opts, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true){

			var resp = Ext.JSON.decode(response.responseText);

			existe_mes = resp.EXISTE_MES;
			ic_proceso = resp.IC_PROCESO;

			// Cargo los datos en los grids
			datosSinError.loadData(resp.LISTA_SIN_ERROR);
			datosConError.loadData(resp.LISTA_CON_ERROR);

			var forma = Ext.ComponentQuery.query('#formaSolicitudes')[0];

			// Oculto o muestro el bot�n de procesar
			if(resp.TOTAL_SIN_ERROR != '0'){
				forma.query('#btnProcesar')[0].show();
			} else{
				forma.query('#btnProcesar')[0].hide();
			}

			// Muestro los totales
			forma.query('#mensaje1_id')[0].setValue(resp.TOTAL_SIN_ERROR);
			forma.query('#mensaje2_id')[0].setValue(resp.TOTAL_CON_ERROR);
			forma.show();

		} else{
			Ext.MessageBox.alert('Error', Ext.JSON.decode(response.responseText).mensaje);
		}
		main.el.unmask();
	}

	// Obtengo los resultados del preacuse
	var procesarPreacuse = function (opts, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true){

			var resp = Ext.JSON.decode(response.responseText);

			// Cargo los datos en los grids
			datosResumenCargaMasiva.loadData(resp.LISTA_RESUMEN);
			datosPreacuse.loadData(resp.LISTA_PREACUSE);

			var forma = Ext.ComponentQuery.query('#formaSolicitudes')[0];
			forma.hide();
			forma = Ext.ComponentQuery.query('#formaResumen')[0];
			if(resp.LISTA_PREACUSE != ''){
				forma.query('#btnContinuarCarga')[0].show();
				forma.query('#btnCancelar')[0].show();
				forma.query('#btnRegresar')[0].hide();
			} else{
				forma.query('#btnContinuarCarga')[0].hide();
				forma.query('#btnCancelar')[0].hide();
				forma.query('#btnRegresar')[0].show();
			}
			forma.show();

		} else{
			Ext.MessageBox.alert('Error', 'Error inesperado.');
		}
		main.el.unmask();
	}

	// Finalizo el procesamiento del archivo
	var procesarCargaMasiva = function (opts, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true){

			var resp = Ext.JSON.decode(response.responseText);

			// Cargo los datos en los grids
			datosResumenCargaMasiva.loadData(resp.LISTA_RESUMEN);
			datosPreacuse.loadData(resp.LISTA_PREACUSE);

			var forma = Ext.ComponentQuery.query('#formaResumen')[0];
			forma.setTitle('Los registros se han dado de alta con �xito');
				forma.query('#btnContinuarCarga')[0].hide();
				forma.query('#btnCancelar')[0].hide();
				forma.query('#btnRegresar')[0].show();
			forma.show();

		} else{
			Ext.MessageBox.alert('Error', 'Error inesperado.');
		}
		main.el.unmask();
	}

//------------------------------ Stores ------------------------------
	// Se crea el MODEL para los combos
	Ext.define('ModelCatologo',{
		extend: 'Ext.data.Model',
		fields: [{ name:'clave', type: 'string'}, { name:'descripcion', type:'string'}]
	});

	// Se crea el STORE para el cat�logo 'Mes'
	var catalogoMes = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15forma04ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_MES'
			}
		},
		autoLoad:          true,
		listeners: {
			load:           procesarCatalogoMes,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE para el cat�logo 'A�o'
	var catalogoAnio = Ext.create('Ext.data.Store',{
		model:             'ModelCatologo',
		proxy: {
			type:           'ajax',
			url:            '15forma04ext.data.jsp',
			reader: {
				type:        'json',
				root:        'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_ANIO'
			}
		},
		autoLoad:          true,
		listeners: {
			load:           procesarCatalogoAnio,
			exception:      NE.util.mostrarProxyAjaxError
		}
	});

	// Se crea el STORE del grid 'Solicitudes sin error'
	var datosSinError = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosSinError',
		fields:   [{name:'linea', mapping:0}, {name:'numPyme', mapping:1}],
		autoLoad: false
	});

	// Se crea el STORE del grid 'Solicitudes con error'
	var datosConError = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosConError',
		fields:   [{name:'linea', mapping:0}, {name:'error', mapping:1}, {name:'solucion', mapping:2}],
		autoLoad: false
	});

	// Se crea el STORE para el grid preacuse
	var datosPreacuse = Ext.create('Ext.data.Store',{
		storeId:  'datosPreacuse',
		fields: [
			{ name: 'IC_EPO',            mapping:0 },
			{ name: 'IC_PYME',           mapping:1 },
			{ name: 'MONTO_CREDITO',     mapping:2 },
			{ name: 'MONTO_CONSOLIDADO', mapping:3 },
			{ name: 'EJECUTIVO',         mapping:4 },
			{ name: 'TELEFONO',          mapping:5 },
			{ name: 'CORREO',            mapping:6 }
		],
		autoLoad: false
	});

	// Se crea el STORE para el grid Resumen Carga Masiva
	var datosResumenCargaMasiva = Ext.create('Ext.data.Store',{
		storeId:  'datosResumenCargaMasiva',
		fields:   [{ name:'ETIQUETA', mapping:0 },{ name:'DESCRIPCION', mapping:1 }],
		autoLoad: false
	});

//------------------------------ Componentes -------------------------
	// Se crea el grid 'Solicitudes sin error'
	var gridDatosSinError = Ext.create('Ext.grid.Panel',{
		anchor:      '95%',
		height:      180,
		itemId:      'gridDatosSinError',
		title:       'Solicitudes sin error',
		store:       Ext.data.StoreManager.lookup('datosSinError'),
		columns: [
			{width:'50%',   sortable:false,  resizable:false,  align:'center',  dataIndex:'linea',    text:'L�nea'     },
			{width:'50%',   sortable:false,  resizable:false,  align:'left',    dataIndex:'numPyme',  text:'N�m. PYME' }
		]
	});

	// Se crea el grid 'Solicitudes con error'
	var gridDatosConError = Ext.create('Ext.grid.Panel',{
		anchor:      '95%',
		height:      180,
		itemId:      'gridDatosConError',
		title:       'Solicitudes con error',
		store:       Ext.data.StoreManager.lookup('datosConError'),
		columns: [
			{width:90,   sortable:false,  resizable:false,  align:'center',  dataIndex:'linea',     text:'L�nea'                    },
			{width:250,  sortable:false,  resizable:true,   align:'left',    dataIndex:'error',     text:'Error',    tdCls: 'wrap'  },
			{width:250,  sortable:false,  resizable:true,   align:'left',    dataIndex:'solucion',  text:'Soluci�n', tdCls: 'wrap'  }
		]
	});

	// Se crea el grid 'Resumen Carga Masiva'
	var gridResumenCargaMasiva = Ext.create('Ext.grid.Panel',{
		width:       400,
		minHeight:   65,
		itemId:      'gridResumenCargaMasiva',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		title:       'Resumen de la carga masiva',
		store:       Ext.data.StoreManager.lookup('datosResumenCargaMasiva'),
		autoHeight:  true,
		columnLines: true,
		hideHeaders: true,
		columns: [
			{width:192,  sortable:false,  resizable:false,   align:'right',  dataIndex:'ETIQUETA'     },
			{width:190,  sortable:false,  resizable:false,   align:'center', dataIndex:'DESCRIPCION'  }
		]
	});

	// Se crea el grid 'Preacuse'
	var gridPreacuse = Ext.create('Ext.grid.Panel',{
		width:       780,
		height:      150,
		itemId:      'gridPreacuse',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		hideTitle:   true,
		store:       Ext.data.StoreManager.lookup('datosPreacuse'),
		columnLines: false,
		columns: [
			{width:'7%',   sortable:false,  resizable:false,  align:'center',  dataIndex:'IC_EPO',             text:'IC EPO'                        },
			{width:'10%',  sortable:false,  resizable:false,  align:'center',  dataIndex:'IC_PYME',            text:'NUM_PYME'                      },
			{width:'20%',  sortable:false,  resizable:true,   align:'center',  dataIndex:'MONTO_CREDITO',      text:'Monto Cr�dito Ventas 3 meses',  renderer: renderMoneda },
			{width:'18%',  sortable:false,  resizable:true,   align:'center',  dataIndex:'MONTO_CONSOLIDADO',  text:'Monto Consolidado Ajustado',    renderer: renderMoneda },
			{width:'20%',  sortable:false,  resizable:true,   align:'left',    dataIndex:'EJECUTIVO',          text:'Ejecutivo Promoci�n'           },
			{width:'15%',  sortable:false,  resizable:true,   align:'center',  dataIndex:'TELEFONO',           text:'Tel�fono'                      },
			{width:'15%',  sortable:false,  resizable:true,   align:'left',    dataIndex:'CORREO',             text:'Correo'                        }
		]
	});

	// Se crea el form Resumen de proceso
	var formaResumen = Ext.create( 'Ext.form.Panel',{
		width:                 800,
		itemId:                'formaResumen',
		title:                 'Preacuse de Carga',
		bodyPadding:           '12 6 12 6',
		style:                 'margin: 0px auto 0px auto;',
		frame:                 true,
		border:                true,
		hidden:                true,
		items: [
			gridResumenCargaMasiva, 
			gridPreacuse
		],
		buttons: [{
			text:               'Continuar',
			itemId:             'btnContinuarCarga',
			iconCls:            'icoAceptar',
			handler:            cargaMasiva
		},{
			text:               'Cancelar',
			itemId:             'btnCancelar',
			iconCls:            'icoCancelar',
			handler:            cancelar
		},{
			text:               'Regresar',
			itemId:             'btnRegresar',
			iconCls:            'icoRegresar',
			handler:            cancelar
		}]
	});

	// Se crea el form de solicitudes
	var formaSolicitudes = Ext.create( 'Ext.form.Panel',{
		width:                 850,
		itemId:                'formaSolicitudes',
		title:                 'Resultado Solicitudes',
		bodyPadding:           '12 6 12 6',
		style:                 'margin: 0px auto 0px auto;',
		frame:                 true,
		border:                true,
		hidden:                true,
		defaults: {
			anchor:             '100%'
		},
		items: [{
			xtype:              'container',
			layout:             'hbox',
			items:[{
				xtype:           'container',
				flex:            1,
				border:          false,
				layout:          'anchor',
				items: [
					gridDatosSinError,
				{
					anchor:       '95%',
					xtype:        'displayfield',
					name:         'mensaje1',
					itemId:       'mensaje1_id',
					fieldLabel:   'Total sin errores',
					value:        '0'
				}]
			},{
				xtype:           'container',
				flex:            1,
				border:          false,
				layout:          'anchor',
				items: [
					gridDatosConError,
				{
					anchor:       '95%',
					xtype:        'displayfield',
					name:         'mensaje2',
					itemId:       'mensaje2_id',
					fieldLabel:   'Total con errores',
					value:        '0'
				}]
			}]
		}],
		buttons: [{
			text:               'Procesar',
			itemId:             'btnProcesar',
			iconCls:            'icoAceptar',
			handler:            preacuse
		}]
	});


	// Se crea el form de Detalles
	var fpDetalle = {
		width:                 500,
		height:                550,
		labelWidth:            200,
		xtype:                 'form',
		itemId:                'fpDetalle',
		style:                 'margin:0 auto;',
		bodyStyle:             'padding: 8px',
		defaults:              { msgTarget: 'side',anchor: '-20' },
		frame:                 false,
		items:                 elementosDetalle
	};

	// Se crea el form principal
	var formaPrincipal = Ext.create( 'Ext.form.Panel',{
		width:                 640,
		id:                    'formaPrincipal',
		title:                 'Clientes Susceptibles de Credicadenas',
		bodyPadding:           '12 6 12 6',
		style:                 'margin: 0px auto 0px auto;',
		frame:                 true,
		border:                true,
		fileUpload:            true,
		fieldDefaults: {
			msgTarget:          'side'
		},
		items: [{
			xtype:              'container',
			layout:             'hbox',
			anchor:             '95%',
			margin:             '0 0 5 0',
			items:[{
				width:           250,
				xtype:           'displayfield',
				value:           '<div align="center"><b>Seleccione mes y a�o de la publicaci�n:</b></div>',
				hideLabel:       true
			},{
				width:           10,
				xtype:           'displayfield',
				value:           '&nbsp',
				hideLabel:       true
			},{
				width:           150,
				labelWidth:      30,
				xtype:           'combobox',
				id:              'mes_id',
				name:            'mes',
				hiddenName:      'mes',
				fieldLabel:      'Mes',
				emptyText:       'Seleccione...',
				displayField:    'descripcion',
				valueField:      'clave',
				queryMode:       'local',
				triggerAction:   'all',
				listClass:       'x-combo-list-small',
				margins:         '0 20 0 0',
				typeAhead:       true,
				selectOnTab:     true,
				lazyRender:      true,
				forceSelection:  true,
				editable:        true,
				allowBlank:      false,
				store:           catalogoMes
			},{
				width:           150,
				labelWidth:      30,
				xtype:           'combobox',
				id:              'anio_id',
				name:            'anio',
				hiddenName:      'anio',
				fieldLabel:      'A�o',
				emptyText:       'Seleccione...',
				displayField:    'descripcion',
				valueField:      'clave',
				queryMode:       'local',
				triggerAction:   'all',
				listClass:       'x-combo-list-small',
				margins:         '0 20 0 0',
				typeAhead:       true,
				selectOnTab:     true,
				lazyRender:      true,
				forceSelection:  true,
				editable:        true,
				allowBlank:      false,
				store:           catalogoAnio
			}]
		},{
				width:           580,
				labelWidth:      130,
				xtype:           'filefield',
				vtype:           'archivoXls',
				itemId:          'archivo_id',
				name:            'archivo',
				emptyText:       'Seleccione...',
				fieldLabel:      '&nbsp Ruta del Archivo',
				margins:         '0 20 0 0',
				buttonText:      'Examinar',
				allowBlank:      false
		},{
				xtype:           'datefield',
				id:              'fecha_id',
				value:           new Date(),
				hidden:          true
		}],
		buttons: [{
			text:               'Continuar',
			itemId:             'btnContinuar',
			iconCls:            'icoAceptar',
			handler:            continuar
		},{
			text:               'Detalle',
			itemId:             'btnDetalle',
			handler:            verDetalle
		}]
	});

// Contenedor principal
	var main = Ext.create('Ext.container.Container',{
		width:      949,
		minHeight:  650,
		autoHeight: true,
		id:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		style:      'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			NE.util.getEspaciador(15),
			formaSolicitudes,
			formaResumen
		]
	});

});