Ext.onReady(function(){

//------------------------------ Handlers ----------------------------
	// Realiza la consulta para llenar el grid
	function buscar(){

		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var campo_ini = '';
		var campo_fin = '';

		// Valido la forma principal
		if(!this.up('form').getForm().isValid()){
			return;
		}

		// Validar la fecha de publicaci�n
		campo_ini = forma.query('#fecha_public_ini_id')[0].getValue();
		campo_fin = forma.query('#fecha_public_fin_id')[0].getValue();
		if(campo_ini == null)
			campo_ini = '';
		if(campo_fin == null)
			campo_fin = '';
		if(campo_ini == '' && campo_fin != ''){
			forma.query('#fecha_public_ini_id')[0].markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			return;
		}
		if(campo_fin == '' && campo_ini != ''){
			forma.query('#fecha_public_fin_id')[0].markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			return;
		}

		// Realiza la consulta del grid
		main.el.mask('Procesando...', 'x-mask-loading');
		Ext.ComponentQuery.query('#gridConsulta')[0].hide();
		consultaData.load({
			params: Ext.apply({
				ic_epo:           forma.query('#ic_epo_id')[0].getValue(),
				fecha_public_ini: forma.query('#fecha_public_ini_id')[0].getValue(),
				fecha_public_fin: forma.query('#fecha_public_fin_id')[0].getValue()
			})
		});

	}

	//Aqui se define el tipo de archivo y se deshabilitan los botones
	function reportePDF(){
		var gridConsulta = Ext.ComponentQuery.query('#gridConsulta')[0];
		var boton = gridConsulta.query('#btnReportePDF')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		generaReporte('PDF');
	}

	//Aqui se define el tipo de archivo y se deshabilitan los botones
	function reporteXLS(){
		var gridConsulta = Ext.ComponentQuery.query('#gridConsulta')[0];
		var boton = gridConsulta.query('#btnReporteXLS')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		generaReporte('CSV');
	}

	//Genera el archivo
	function generaReporte(tipo_archivo){
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		Ext.Ajax.request({
			url: '15forma01ext.data.jsp',
			params: Ext.apply({
				informacion:      'GENERA_ARCHIVO',
				ic_epo:           forma.query('#ic_epo_id')[0].getValue(),
				fecha_public_ini: forma.query('#fecha_public_ini_id')[0].getValue(),
				fecha_public_fin: forma.query('#fecha_public_fin_id')[0].getValue(),
				tipo_archivo:     tipo_archivo
			}),
			callback: procesaGeneraArchivo
		});
	}

//------------------------------ Callback ----------------------------
	// Proceso posterior a la consulta del grid consultaData
	var procesarConsultaData = function(store, arrRegistros, success, opts){
		main.el.unmask();
		var gridConsulta = Ext.ComponentQuery.query('#gridConsulta')[0];
		if (arrRegistros != null){
			gridConsulta.show();
			if(store.getTotalCount() > 0){
				gridConsulta.query('#btnReporteXLS')[0].enable();
				gridConsulta.query('#btnReportePDF')[0].enable();
			} else{
				gridConsulta.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridConsulta.query('#btnReporteXLS')[0].disable();
				gridConsulta.query('#btnReportePDF')[0].disable();
			}
		}
	}

	// Handler que procesa respuesta creacion archivo
	function procesaGeneraArchivo(opts, success, response){

		var gridConsulta = Ext.ComponentQuery.query('#gridConsulta')[0];
		var boton1 = gridConsulta.query('#btnReportePDF')[0];
		var boton2 = gridConsulta.query('#btnReporteXLS')[0];

		// PARSEAR RESPUESTA DEL SERVIDOR
		var resp = null;
		try {
			resp = Ext.JSON.decode(response.responseText);
		} catch(error) {
			NE.util.mostrarErrorResponse4(
				{ status: -1 }, // Para que no asuma que es un est�tus devuelto por el servidor
				error.name + " - " + error.message
			);
			return;
		}

		// REALIZAR LA DESCARGA AUTOM�TICA DEL ARCHIVO
		if ( success == true && resp.success == true ) {

			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo';
			forma.method = 'post';
			forma.target = '_self';

			// Suprimir el Context root del url del archivo
			var urlArchivo = resp.urlArchivo;
			nombreArchivo  = urlArchivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');

			// Insertar Nombre Archivo
			var inputNombreArchivo = Ext.DomHelper.insertFirst(
				forma,
				{
					tag:   'input',
					type:  'hidden',
					id:    'nombreArchivo',
					name:  'nombreArchivo',
					value: nombreArchivo
				},
				true
			);
			// Solicitar Archivo Al Servidor
			forma.submit();
			// Remover nodo agregado
			inputNombreArchivo.remove();

		} else {
				NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
		boton1.enable();
		boton2.enable();
		boton1.setIconCls('icoPdf');
		boton2.setIconCls('icoXls');

	}

//------------------------------ Stores ------------------------------
	// Se crea el MODEL para los combos
	Ext.define('ModelCatologo',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);

	// Se crea el MODEL para el grid 'consultaData'
	Ext.define('ListaData',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'PYMES'                               },
			{name: 'EPO'                                 },
			{name: 'TOTAL_PYMES_AFILIADAS_HABILITADAS'   },
			{name: 'CANTIDAD_PUCP'                       },
			{name: 'MONTO_PUCP'                          },
			{name: 'PYMES_UNICAS_SIN_OPERACION'          },
			{name: 'CANTIDAD_PUCO'                       },
			{name: 'MONTO_PUCO'                          },
			{name: 'TOTAL_PYMES_AFILIADAS_NO_HABILITADAS'},
			{name: 'CANTIDAD_PUCP_1'                     },
			{name: 'MONTO_PUCP_1'                        },
			{name: 'PYMES_UNICAS_SIN_PUBLICACION'        }
		]
	});

	// Se crea el STORE para el cat�logo 'EPO'
	var catalogoEPO = Ext.create('Ext.data.Store',{
		model: 'ModelCatologo',
		proxy: {
			type: 'ajax',
			url: '15forma01ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_EPO'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: true
	});

	// Se crea el STORE del grid 'Consulta Data'
	var consultaData = Ext.create('Ext.data.Store',{
		storeId:    'consultaData',
		model:      'ListaData',
		proxy: {
			type:    'ajax',
			url:     '15forma01ext.data.jsp',
			reader:  {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion:  'CONSULTA_DATA'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultaData
		}
	});

//------------------------------ Componentes -------------------------
	// Se crea el grid 'ConsultaData'
	var gridConsulta = Ext.create('Ext.grid.Panel',{
		height:          430,
		width:           '95%',
		xtype:           'grouped-text-grid',
		itemId:          'gridConsulta',
		style:           'margin:0 auto;',
		bodyStyle:       'padding: 6px',
		title:           'Por EPO',
		store:           Ext.data.StoreManager.lookup('consultaData'),
		scroll:          true,
		border:          true,
		frame:           true,
		hidden:          true,
		columns: [{
			width:        60,
			dataIndex:    'PYMES',
			text:         'PYMES',
			align:        'center',
			sortable:     true,
			resizable:    true
		},{
			width:        200,
			dataIndex:    'EPO',
			text:         '<div align="center"> EPO </div>',
			align:        'left',
			sortable:     true,
			resizable:    true
		},{
			width:        120,
			dataIndex:    'TOTAL_PYMES_AFILIADAS_HABILITADAS',
			text:         'Total de PYMEs <br>Afiliadas Habilitadas',
			align:        'center',
			sortable:     true,
			resizable:    true
		},{
			text:         'PYMES �nicas Con Publicaci�n',
			columns: [{
				width:     75,
				dataIndex: 'CANTIDAD_PUCP',
				text:      'Cantidad',
				align:     'center',
				sortable:  true,
				resizable: true
			},{
				width:     125,
				dataIndex: 'MONTO_PUCP',
				text:      'Monto',
				align:     'center',
				sortable:  true,
				resizable: true
			}]
		},{
			text:         'PYMES �nicas <br>Sin Publicaci�n',
			columns: [{
				width:     100,
				dataIndex: 'PYMES_UNICAS_SIN_OPERACION',
				text:      'Cantidad',
				align:     'center',
				sortable:  true,
				resizable: true
			}]
		},{
			text:         'PYMES �nicas Con Operaci�n',
			columns: [{
				width:     75,
				dataIndex: 'CANTIDAD_PUCO',
				text:      'Cantidad',
				align:     'center',
				sortable:  true,
				resizable: true
			},{
				width:     125,
				dataIndex: 'MONTO_PUCO',
				text:      'Monto',
				align:     'center',
				sortable:  true,
				resizable: true
			}]
		},{
			width:        120,
			dataIndex:    'TOTAL_PYMES_AFILIADAS_NO_HABILITADAS',
			text:         'Total PYMES <br> Afiliadas No Habilitadas',
			align:        'center',
			sortable:     true,
			resizable:    true
		},{
			text:         'PYMES �nicas Con Publicaci�n',
			columns: [{
				width:     75,
				dataIndex: 'CANTIDAD_PUCP_1',
				text:      'Cantidad',
				align:     'center',
				sortable:  true,
				resizable: true
			},{
				width:     125,
				dataIndex: 'MONTO_PUCP_1',
				text:      'Monto',
				align:     'center',
				sortable:  true,
				resizable: true
			}]
		},{
			text:         'PYMES �nicas <br> Sin Publicaci�n',
			columns: [{
				width:     120,
				dataIndex: 'PYMES_UNICAS_SIN_PUBLICACION',
				text:      'Cantidad',
				align:     'center',
				sortable:  true,
				resizable: true
			}]
		}],
		bbar: ['->','-',
		{
				xtype:      'button',
				itemId:     'btnReporteXLS',
				iconCls:    'icoXls', 
				text:       'Generar Archivo',
				tooltip:    'Generar Archivo en formato CSV',
				handler:    reporteXLS
			},'-',{
				xtype:      'button',
				itemId:     'btnReportePDF',
				iconCls:    'icoPdf', 
				text:       'Imprimir PDF',
				tooltip:    'Generar Archivo en formato PDF',
				handler:    reportePDF
			}
		]
	});

	// Se crea el form principal
	var formaPrincipal = Ext.create( 'Ext.form.Panel',{
		width:                  '80%',
		itemId:                 'formaPrincipal',
		title:                  'Operaci�n de PYMES en Cadenas',
		bodyPadding:            '12 6 12 6',
		style:                  'margin: 0px auto 0px auto;',
		frame:                  true,
		border:                 true,
		fieldDefaults: {
			msgTarget:           'side'
		},
		items: [{
			xtype:               'container',
			layout:              'hbox',
			anchor:              '95%',
			margin:              '0 0 5 0',
			items:[{
				width:            280,
				xtype:            'displayfield',
				hideLabel:        true
			},{
				width:            160,
				xtype:            'radiogroup',
				itemId:           'opcion_id',
				fieldLabel:       'Auto Layout',
				cls:              'x-check-group-alt',
				hideLabel:        true,
				items: [{
					boxLabel:      'Por EPO',
					name:          'opcion',
					inputValue:    1,
					checked:       true
				},{
					boxLabel:      'Por Mes',
					name:          'opcion',
					inputValue:    2
				}],
				listeners: {
					change: function (field, newValue, oldValue) {
						switch (newValue['opcion']){
							case 2:
								window.location.href='15forma02ext.jsp';
								break;
							default:
								break;
						}
					}
				}
			}]
		},{
			labelWidth:          140,
			anchor:              '95%',
			xtype:               'combobox',
			itemId:              'ic_epo_id',
			name:                'ic_epo',
			hiddenName:          'ic_epo',
			fieldLabel:          'EPO',
			emptyText:           'Seleccione...',
			displayField:        'descripcion',
			valueField:          'clave',
			queryMode:           'local',
			triggerAction:       'all',
			listClass:           'x-combo-list-small',
			typeAhead:           true,
			selectOnTab:         true,
			lazyRender:          true,
			forceSelection:      true,
			editable:            true,
			allowBlank:          false,
			store:               catalogoEPO
		},{
			xtype:               'container',
			layout:              'hbox',
			anchor:              '95%',
			margin:              '0 0 5 0',
			items:[{
				width:            250,
				labelWidth:       140,
				xtype:            'datefield',
				id:               'fecha_public_ini_id',
				name:             'fecha_public_ini',
				hiddenName:       'fecha_public_ini',
				fieldLabel:       'Fecha de Public/Oper de',
				vtype:            'rangofecha',
				minValue:         '01/01/1901',
				campoFinFecha:    'fecha_public_fin_id',
				margins:          '0 20 0 0',
				allowBlank:       false,
				startDay:         0
			},{
				width:            158,
				labelWidth:       35,
				xtype:            'datefield',
				id:               'fecha_public_fin_id',
				name:             'fecha_public_fin',
				hiddenName:       'fecha_public_fin',
				fieldLabel:       'a',
				vtype:            'rangofecha',
				minValue:         '01/01/1901',
				campoInicioFecha: 'fecha_public_ini_id',
				margins:          '0 20 0 0',
				allowBlank:       false,
				startDay:         1
			}]
		}],
		buttons: [{
			text:                'Generar',
			itemId:              'btnGenerar',
			iconCls:             'icoBuscar',
			handler:             buscar
		}]
	});

	// Contenedor principal
	var main = Ext.create('Ext.container.Container',{
		width:      949,
		minHeight:  650,
		autoHeight: true,
		id:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		style:      'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			NE.util.getEspaciador(10),
			gridConsulta
		]
	});

});