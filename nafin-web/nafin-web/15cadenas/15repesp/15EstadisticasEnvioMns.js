function setMessage() {
	var mns = '<table width="300"  ><tr><td><div><p style=�text-align: justify;� >VALORES POSIBLES EN EL ESTATUS DE ENVIO<BR>Activo:Se envian los mensajes promocionales a las pymes que pertenecen a la EPO.<br>Suspendido:Se detiene el envio de mensajes<br>No Aplica:La informaci&oacute;n consultada pertenece a publicaciones de meses anteriores.<br></p></td></tr></table><br>';
	Ext.Msg.alert('Aviso',mns
	);
}
Ext.onReady(function(){
	
	function consultarInfo(boton) {
		var fp = Ext.ComponentQuery.query('#formaPrincipal')[0];
		if(Ext.isEmpty(fp.query('#mes')[0].getValue())){
			fp.query('#mes')[0].markInvalid('Favor de seleccionar el a�o.');
			fp.query('#mes')[0].focus();						
			return;
		}
		if(Ext.isEmpty(fp.query('#anio')[0].getValue())){
			fp.query('#anio')[0].markInvalid('Favor de seleccionar el mes.');
			fp.query('#anio')[0].focus();						
			return;
		}
		
		boton.setDisabled(true);
		boton.setIconCls('x-mask-msg-text');
		consultaData.load({
			params: Ext.apply(
				{
					informacion: 'Consulta_info',
					anio:    fp.query('#anio')[0].getValue(),
					mes:    fp.query('#mes')[0].getValue(),
					estatusEnvio:    fp.query('#estatusEnvio')[0].getValue(),
					ic_epo:    fp.query('#ic_epo')[0].getValue()
				})
			});
	}
	function limpiar(boton) {
		var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
		var gridConsultaDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
		if(gridConsulta.isVisible()){
			gridConsulta.hide();
		}
		if(gridConsultaDetalle.isVisible()){
			gridConsultaDetalle.hide();
		}
	}
	function procesarDescargaArchivos(opts, success, response) {
		Ext.ComponentQuery.query('#btnDescArch')[0].setIconCls('icoBotonXLS');
		Ext.ComponentQuery.query('#btnDescArch')[0].setDisabled(false);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();							
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesarDescargaArchivosDetalle(opts, success, response) {
		Ext.ComponentQuery.query('#btnArchivoXLSDetalle')[0].setIconCls('icoBotonXLS');
		Ext.ComponentQuery.query('#btnArchivoXLSDetalle')[0].setDisabled(false);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();							
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesarAceptar(opts, success, response){
		Ext.ComponentQuery.query('#btnAcepatar')[0].setIconCls('icoAceptar');
		Ext.ComponentQuery.query('#btnAcepatar')[0].setDisabled(false);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var accion = infoR.accion;	
			var archivo = infoR.urlArchivo;		
			var mns = infoR.mns;		
			if(accion=='D'||accion=='E'){
				Ext.Msg.alert('Avisar',mns,
					function(){
							window.location  = "15EstadisticasEnvioMns.jsp"; 	
					}
				);
				
			}else if(accion=='GA'||accion=='GD'){
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};	
				var forma  	= Ext.getDom('formAux'); 
				forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				forma.method 		= 'post';
				forma.target 		= '_self'; 	
				forma.submit();	
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function aceptar(boton){
		var  grid =   Ext.ComponentQuery.query('#grid')[0];
		var filaSel = grid.getSelectionModel().getSelection();
		var opcRadios = "";
		var selected = [];
		Ext.each(filaSel, function (item) {
		  selected.push(item.data.IC_EPO);
		});
		
		if(selected.length<=0){
			Ext.Msg.alert('Avisar','Favor de seleccionar la(s) cadena(s) a procesar');
			return;
		}
		var contTrue = 0;
		var arrayPeriodo = Ext.ComponentQuery.query('#formaGrid radiofield[identificador=opcion_radio]');
		for (var i = 0; i < arrayPeriodo.length; i++) {
			if(arrayPeriodo[i].getValue()==true){
				contTrue++;
				opcRadios = arrayPeriodo[i].inputValue;
			}	
		}
		if(contTrue==0){
			Ext.Msg.alert('Avisar','Favor de seleccionar la acci�n que desea realizar');
			return;
		}else if(contTrue>1){
			Ext.Msg.alert('Avisar','Seleccionar solo una opci�n');
			return;
		}
		boton.setDisabled(true);
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15EstadisticasEnvioMns.data.jsp',
			params: Ext.apply({					
				informacion:'accion_aceptar',
				estatusEnvio:  Ext.ComponentQuery.query('#estatusEnvio')[0].getValue(),
				anio:    Ext.ComponentQuery.query('#anio')[0].getValue(),
				mes: Ext.ComponentQuery.query('#mes')[0].getValue(),
				accion:opcRadios,
				sel: selected
			}),
			callback: procesarAceptar
		});
		
	}
	var procesarAnio = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#anio')[0].setValue(arrRegistros[0]);
		}
	}
	var procesarMes = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#mes')[0].setValue(arrRegistros[(arrRegistros.length-1)]);
		}
	}
	var procesarEpo = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#ic_epo')[0].setValue('');
		}
	}
	var detalleClientes  = function (grid, rowIndex, colIndex, item, event){
		var  fp =   Ext.ComponentQuery.query('#forma')[0];
		var  fpDetalle =   Ext.ComponentQuery.query('#formaDetalle')[0];
		var registro = grid.getStore().getAt(rowIndex);
		var epoDetalle = registro.get('IC_EPO');  
		Ext.ComponentQuery.query('#epo_detalle')[0].setValue(epoDetalle);
		consultaDataDetalle.load({
			params: Ext.apply(
				{
					informacion: 'Consulta_info_detalle',
					epoDetalle: epoDetalle,
					mes:    Ext.ComponentQuery.query('#mes')[0].getValue(),
					anio:    Ext.ComponentQuery.query('#anio')[0].getValue(),
					respuesta: Ext.ComponentQuery.query('#respuesta')[0].getValue()
				})
			});
		consTotalDataDetalle.load({
			params: Ext.apply(
				{
					informacion: 'Consulta_total_detalle',
					epoDetalle: epoDetalle,
					mes:    Ext.ComponentQuery.query('#mes')[0].getValue(),
					anio:    Ext.ComponentQuery.query('#anio')[0].getValue(),
					respuesta: Ext.ComponentQuery.query('#respuesta')[0].getValue()
				})
			});
	}
	
	var procesarArchivosXLS = function(boton) {
		var  forma =   Ext.ComponentQuery.query('#formaPrincipal')[0];
		var fecha1 = forma.query('#claveMesI')[0].getValue() + '/1/' + forma.query('#claveAnioI')[0].getValue(); 
		var fecha2 = forma.query('#claveMesF')[0].getValue() + '/1/' + forma.query('#claveAnioF')[0].getValue(); 
		var f1=new Date(fecha1); 
		var f2=new Date(fecha2); 
		if(f1 > f2){
			Ext.Msg.alert('Aviso','<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Rango de fechas incorrecto, por favor intente nuevamente.</center></td><td></td></table> </div>');
			forma.query('#claveMesI')[0].focus();
			return;
		}
		boton.setDisabled(true);
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15EstadisticasEnvioMns.data.jsp',
			params: Ext.apply(forma.getForm().getValues(),{					
				informacion:'Generar_Arch'
			}),
			callback: procesarDescargaArchivos
		});
	}
	var procesarArchivosXLSDetalle = function(boton) {
		var  fp =   Ext.ComponentQuery.query('#formaPrincipal')[0];
		var epoDetalle = Ext.ComponentQuery.query('#epo_detalle')[0].getValue(); 
		boton.setDisabled(true);
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15EstadisticasEnvioMns.data.jsp',
			params: Ext.apply({					
				informacion:'Generar_Arch_Detalle',
				epoDetalle:epoDetalle,
				respuesta:  Ext.ComponentQuery.query('#respuesta')[0].getValue(),
				anio:    Ext.ComponentQuery.query('#anio')[0].getValue(),
				mes: Ext.ComponentQuery.query('#mes')[0].getValue()
			}),
			callback: procesarDescargaArchivosDetalle
		});
	}
	var procesarConsultaData = function(store, arrRegistros, success, opts){
		Ext.ComponentQuery.query('#btnConsultar')[0].setIconCls('icoBuscar');
		Ext.ComponentQuery.query('#btnConsultar')[0].setDisabled(false);
		var jsonData = store.proxy.reader.jsonData;
		var numRadios = jsonData.numRadios;
		var radio_hide = jsonData.radio_hide;
		var fp = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
		var gridTotal = Ext.ComponentQuery.query('#gridTotal')[0];
		if(!gridConsulta.isVisible()){
			gridConsulta.show();
			
		}
		Ext.ComponentQuery.query('#numRadios')[0].setValue(numRadios);
		if (arrRegistros != null){
			if(store.getTotalCount() > 0){
				Ext.ComponentQuery.query('#hidElementos')[0].setValue(store.getTotalCount());
				consTotalData.load({
					params: Ext.apply(
						{
							informacion: 'Consulta_info_total',
							anio:    fp.query('#anio')[0].getValue(),
							mes:    fp.query('#mes')[0].getValue(),
							estatusEnvio:    fp.query('#estatusEnvio')[0].getValue(),
							ic_epo:    fp.query('#ic_epo')[0].getValue()
						})
					});	
				Ext.ComponentQuery.query('#formaGrid')[0].show();
				
				if(radio_hide!='N'){
					Ext.ComponentQuery.query('#rdDDMP')[0].hide();
					Ext.ComponentQuery.query('#rdEMP')[0].hide();
				}else{
					Ext.ComponentQuery.query('#rdDDMP')[0].show();
					Ext.ComponentQuery.query('#rdEMP')[0].show();
				}
				gridConsulta.getView().setAutoScroll(true);
			} else{
				
				gridTotal.hide();
				Ext.ComponentQuery.query('#formaGrid')[0].hide();
				gridConsulta.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridConsulta.getView().setAutoScroll(false);
				
			}
		}
	}
	var procesarConsultaTotalData = function(store, arrRegistros, success, opts){
		
		var gridConsulta = Ext.ComponentQuery.query('#gridTotal')[0];
		if(!gridConsulta.isVisible()){
			gridConsulta.show();
		}
		if (arrRegistros != null){
			if(store.getTotalCount() > 0){
				
			} else{
				gridConsulta.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridConsulta.getView().setAutoScroll(false);
				
			}
		}
	}
	var procesarConsultaDataDetalle = function(store, arrRegistros, success, opts){
		if (arrRegistros != null){
			var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
			var gridTotal = Ext.ComponentQuery.query('#gridTotal')[0];
			var formGrid = Ext.ComponentQuery.query('#formaGrid')[0];
			var formaPrincipal = Ext.ComponentQuery.query('#formaPrincipal')[0];
			var gridConsultaDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
			var gridTotalDetalle = Ext.ComponentQuery.query('#gridTotalDetalle')[0];
			var formaDetalle = Ext.ComponentQuery.query('#formaDetalle')[0];
			if(!gridConsultaDetalle.isVisible()){
				gridConsultaDetalle.show();
				formaDetalle.show();
				if(gridConsulta.isVisible()){
					gridConsulta.hide();
				}
				if(gridTotal.isVisible()){
					gridTotal.hide();
				}
				if(formGrid.isVisible()){
					formGrid.hide();
				}
				if(formaPrincipal.isVisible()){
					formaPrincipal.hide();
				}
				if(gridTotalDetalle.isVisible()){
					gridTotalDetalle.hide();
				}
			}
			if(store.getTotalCount() > 0){
				Ext.ComponentQuery.query('#btnArchivoXLSDetalle')[0].setDisabled(false);
				gridConsultaDetalle.getView().setAutoScroll(true);
				var epoDetalle = Ext.ComponentQuery.query('#epo_detalle')[0].getValue();  
			} else{
				Ext.ComponentQuery.query('#btnArchivoXLSDetalle')[0].setDisabled(true);
				gridConsultaDetalle.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridConsultaDetalle.getView().setAutoScroll(false);
			}
		}
	}
	var procesarConsultaTotalDetalle = function(store, arrRegistros, success, opts){
		if (arrRegistros != null){
			var anioDetalle ="";
			var mesDetalle ="";
			var gridTotalDetalle = Ext.ComponentQuery.query('#gridTotalDetalle')[0];
			if(!gridTotalDetalle.isVisible()){
				gridTotalDetalle.show();
			}
			if(store.getTotalCount() > 0){
				gridTotalDetalle.getView().setAutoScroll(true);
			} else{
				gridTotalDetalle.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridTotalDetalle.getView().setAutoScroll(false);
				
			}
		}
	}
	Ext.define('ListaRegistros',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'NOMBRE_EPO'     },
				{name: 'NUM_PYMES'     },
				{name: 'NUM_PYMES_MSG'     },
				{name: 'NUM_RESP_SI'     },
				{name: 'NUM_RESP_NO'     },
				{name: 'SIN_RESPONDER'     },
				{name: 'PYMES_SIN_MSG'     },
				{name: 'ESTATUS_ENVIO'     },
				{name: 'IC_EPO'     }
			]
	});
	Ext.define('ListaRegistrosTotal',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'TOT_PYMES'     },
				{name: 'TOT_PYMES_MSG'     },
				{name: 'TOT_PYMES_SI'     },
				{name: 'TOT_PYMES_NO'     },
				{name: 'TOT_SIN_RESP'     },
				{name: 'TOT_SIN_MSG'     },
				{name: 'DESCRICPION'     }
			]
	});
	Ext.define('ListaRegistrosDetalle',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'IC_PYME'     },
				{name: 'NUMERO_SIRAC'     },
				{name: 'NOMBRE_PYME'     },
				{name: 'CREDCAD'     },
				{name: 'ESTADO'     },
				{name: 'CP'     },
				{name: 'TELEFONO'     },
				{name: 'MONTO_CREDITO'     },
				{name: 'MONTO_CONSOLIDADO'     },
				{name: 'RESPUESTA'     },
				{name: 'FECHA_RESPUESTA'     }
			]
	});
	Ext.define('ListRegTotalDetalle',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'DESCRIPCION'     },
				{name: 'NUM_RESP_SI'     },
				{name: 'NUM_RESP_NO'     },
				{name: 'SIN_RESPONDER'     },
				{name: 'NUM_PYMES_MSG'     }
			]
	});
	var consultaData = Ext.create('Ext.data.Store',{
		storeId: 'consultaData',
		model: 'ListaRegistros',
		pageSize: 15,
		proxy: {
			type: 'ajax',
			url:  '15EstadisticasEnvioMns.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_info'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		
		listeners: {
			load: procesarConsultaData
		}
	});
	var consTotalData = Ext.create('Ext.data.Store',{
		storeId: 'consultaTotalData',
		model: 'ListaRegistrosTotal',
		pageSize: 15,
		proxy: {
			type: 'ajax',
			url:  '15EstadisticasEnvioMns.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_info_total'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		
		listeners: {
			load: procesarConsultaTotalData
		}
	});
	
	var consultaDataDetalle = Ext.create('Ext.data.Store',{
		storeId: 'consultaDataDetalle',
		model: 'ListaRegistrosDetalle',
		proxy: {
			type: 'ajax',
			url:  '15EstadisticasEnvioMns.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_info_detalle'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		
		listeners: {
			load: procesarConsultaDataDetalle
		}
	});
	var consTotalDataDetalle = Ext.create('Ext.data.Store',{
		storeId: 'consTotalDataDetalle',
		model: 'ListRegTotalDetalle',
		proxy: {
			type: 'ajax',
			url:  '15EstadisticasEnvioMns.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_total_detalle'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		
		listeners: {
			load: procesarConsultaTotalDetalle
		}
	});
	Ext.define('ModelMes',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	Ext.define('ModelMesF',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	Ext.define('ModelAnio',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	Ext.define('ModelAnioF',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	
	var storeMes = Ext.create('Ext.data.Store',{
		model:'ModelMes',
		autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '15EstadisticasEnvioMns.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Mes'
			},
				totalProperty:  'total'
		},
		listeners: {
			load:procesarMes,
			exception: NE.util.mostrarProxyAjaxError
		}
	});
	var elementosDetalle = [
		{
				xtype:'label',
					style:
                {
                    'font-size' : '12px'
                },
				width:	750,
				html:	'<table width="420" cellpadding="0" cellspacing="0" border="0">'+
							'<tr>'+
							'<td valign="top">'+
							'<table cellpadding="0" cellspacing="0" border="0">'+
								'<tr>'+
								'<td valign="top" align="center"><br>'+
								'<table width="420" cellpadding="0" cellspacing="1" border="0" class="formas">'+
									'<tr>'+
									'<td class="formas" align="center"><b>CLIENTES SUSCEPTIBLES DE CREDICADENAS</b></td>'+
				
								'</tr>'+
				
								'<tr>'+
				
									'<td class="formas">'+
				
										'<i><b>Objetivo:</b></i><br>'+
				
										'Emitir una base de datos de los clientes de Factoraje Electr&oacute;nico que cumplen las condiciones establecidas por riesgos para participar en el programa de Credicadenas.<br>'+
				
										'<br><i><b>Periodicidad:</b></i><br>'+
				
										'Mensual.<br>'+
				
										'<br><i><b>Destinatario:</b></i><br>'+
				
										'&nbsp;* Subdirecci�n de Productos de Financiamiento.<br>'+
				
										'&nbsp;* Rafael Velasco.<br>'+
				
										'&nbsp;* Alejandro Aguirre.<br>'+
				
										'<br><i><b>Fuente:</b></i><br>'+
				
										'Base de datos de  Nafin-electr&oacute;nico.<br>'+
				
										'<br><i><b>Formato:</b></i><br>'+
				
										'Excel (.csv)<br>'+
				
										'<br><i><b>Instrucciones:</b></i><br>'+
				
										'&nbsp;- Seleccionar el mismo mes en ambos criterios.<br>'+
				
										'<br><i><b>En excel:</i></b><br>'+
				
										'&nbsp;- Realizar las adecuaciones necesarias.<br>'+
				
										'&nbsp;- Aplicar el formato deseado y guardar el reporte.<br>'+
				
										'<br><i><b>Contactos:</i></b><br>'+
				
										'Rafael Velasco Posada - Subdirecci�n de Productos Electr&oacute;nicos y Financiamiento.<br>'+
				
										'Alejandro Aguirre - Credicadenas.<br>'+
				
										'<br><i><b>Observaciones:</i></b><br>'+
				
										'Las condiciones para estos clientes son:<br>'+
				
										"&nbsp;- Pertenecer a las Epo's seleccionadas.<br>"+
				
										'&nbsp;- Tener publicaciones en: 6 meses consecutivos &oacute;<br>'+
				
										'&nbsp;- Por lo menos en  8 de 12 meses.<br>'+
				
									'</td>'+
				
								'</tr>'+
				
								'</table>'+
				
							'</td>'+
				
						'</tr>'+
				
						'</table>'+
						'</td>'+
				
				'</tr>'+
				
				'</table>'

		}
	]
	var fpDetalle = {
		xtype: 'form',
		itemId					: 'fpDetalle',	
		width				: 500,
		height: 550,
		style				: ' margin:0 auto;',
		frame				: false,
		hidden			: false,
		fileUpload: true,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 200,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items: elementosDetalle	
	};
	function verDetalle(){
		var winDetalle =Ext.ComponentQuery.query('winDetalle')[0];
		if(winDetalle){
			
			winDetalle.show();
		} else{
					new Ext.Window({
								layout: 'fit',
								modal: true,
								width: 500,
								frame:       false,
								constrain:   true,
								height: 550,
								resizable: false,
								closable: true,
								x: 450,
								itemId: 'winDetalle',
								autoDestroy:false,
								closeAction: 'destroy',
								items: [
									fpDetalle
								],
								title: ''
						}).show();	
		}
		
	}
	var elementosForma = [
		{
			xtype: 'combo',
			itemId:  'anio',
			name:  'anio',
			fieldLabel: 'A�o',
			hiddenName: 'anio',
			forceSelection:true,
			allowBlank: false,
			labelWidth: 100,
			emptyText: 'Seleccione...',
			queryMode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			typeAhead:		true,
			triggerAction:	'all',
			width:  450,
			value:2016,
			store: Ext.create('Ext.data.Store', {
				model:'ModelAnio',
				autoLoad: true,
				proxy: {
					type: 'ajax',
					url: '15EstadisticasEnvioMns.data.jsp',
					reader: {
						type: 'json',
						root: 'registros'
					},
					extraParams: {
						informacion: 'Consulta_Anio'
					},
						totalProperty:  'total'
					},
					listeners: {
						load : procesarAnio,
						exception: NE.util.mostrarProxyAjaxError
					}
			}),
			listeners: {
				select: function(combo, record, index) {
					var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
					storeMes.load({
						params: Ext.apply({
							mes: forma.query('#mes')[0].getValue(),
							anio: forma.query('#anio')[0].getValue()
							
						})
					});
				}
			}
		},
		{
			xtype: 'combo',
			itemId:  'mes',
			fieldLabel: 'Mes',
			name:  'mes',
			hiddenName: 'mes',
			forceSelection:true,
			allowBlank: false,
			emptyText: 'Seleccione...',
			queryMode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			typeAhead:		true,
			labelWidth: 100,
			triggerAction:	'all',
			width:  450,
			store:  storeMes
		},
		{
			xtype: 'combo',
			itemId:  'estatusEnvio',
			
			fieldLabel: 'Estatus de Env�o',
			name:  'estatusEnvio',
			hiddenName: 'estatusEnvio',
			forceSelection:true,
			allowBlank: false,
			emptyText: 'Seleccione...',
			queryMode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			typeAhead:		true,
			labelWidth: 100,
			triggerAction:	'all',
			value: '',
			width:  450,
			store: new Ext.data.ArrayStore(
				{
					fields: [
						'clave',
						'descripcion'
					],
					data: [
						['', 'Todos'], 
						['A', 'Activo'], 
						['N', 'Suspendido'],
						['NA','No Aplica']
					]
				}
															
			)
		},
		{
			xtype: 'combo',
			itemId:  'ic_epo',
			fieldLabel: 'EPO',
			name:  'ic_epo',
			hiddenName: 'ic_epo',
			forceSelection:true,
			allowBlank: false,
			emptyText: 'Seleccione...',
			queryMode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			typeAhead:		true,
			labelWidth: 100,
			triggerAction:	'all',
			width:  450,
			store: Ext.create('Ext.data.Store', {
				model:'ModelMes',
				autoLoad: true,
				proxy: {
					type: 'ajax',
					url: '15EstadisticasEnvioMns.data.jsp',
					reader: {
						type: 'json',
						root: 'registros'
					},
					extraParams: {
						informacion: 'Consulta_Epos'
					},
						totalProperty:  'total'
					},
					listeners: {
						load : procesarEpo,
						exception: NE.util.mostrarProxyAjaxError
					}
			})
		},
		{
			xtype:'hidden',
			itemId:'numRadios',
			name:'numRadios'
		},
		{
			xtype:'hidden',
			itemId:'hidElementos',
			name:'hidElementos'
		}
		
	]
	var fpAyuda = new Ext.form.FormPanel({
			itemId			: 'fpAyuda',	
			layout			: 'form',
			width				: 390,			
			height			: 100,
			style				: ' margin:0 auto;',
			frame				: false,
			hidden			: false,
			collapsible		: false,
			titleCollapse	: false	,
			bodyStyle		: 'padding: 8px',
			defaults			: { msgTarget: 'side',anchor: '-20' },
			items: [
				{
					xtype:'label',
					style: {  'font-size' : '12px'  },
					html:'<table width="390"  ><tr><td><div><p style=�text-align: justify;� >El estatus de envio adquirir&aacute; el valor de ACTIVO, y las PYMES que su respuesta haya sido NO, regresar&aacute;n al Estatus inicial que es SIN CONTESTAR.<br></p></td></tr></table><br>'
				}
			]
		});
	var ventanaAyuda = new Ext.Window({
		modal: true,
		resizable: false,
		layout: 'form',
		x: 400,
		width: 400,
		height : 100,
		itemId: 'winVistaPrev',
		closable: true,
		closeAction: 'hide',
		items: fpAyuda
			
	});
	var fpAyuda1 = new Ext.form.FormPanel({
			itemId			: 'fpAyuda',	
			layout			: 'form',
			width				: 390,			
			height			: 100,
			style				: ' margin:0 auto;',
			frame				: false,
			hidden			: false,
			collapsible		: false,
			titleCollapse	: false	,
			bodyStyle		: 'padding: 8px',
			defaults			: { msgTarget: 'side',anchor: '-20' },
			items: [
				{
					xtype:'label',
					style: {  'font-size' : '12px'  },
					html:'<table width="390"  ><tr><td><div><p style=�text-align: justify;� >El estatus de envio adquirir&aacute; el valor de SUSPENDIDO<br></p></td></tr></table><br>'
				}
			]
		});
	var ventanaAyuda1 = new Ext.Window({
		modal: true,
		resizable: false,
		layout: 'form',
		x: 400,
		width: 400,
		height : 100,
		itemId: 'winVistaPrev1',
		closable: true,
		closeAction: 'hide',
		items: fpAyuda1
			
	});
	var elementosGrid = [
		{
			xtype:'panel',
			frame: true,
			layout:'table',
			border	: false,
			itemId:'rdEMP',
			
			cls: 'panel-sin-borde',
			width:300,
			items: [
				{	
					xtype: 'button',	
					itemId: 'btnAyuda',	
					iconCls: 'icoAyuda',
					width:20, 
					handler: function(obj){
						ventanaAyuda.setVisible(true);
					}
				},
				{ 	
					width:10, 
					xtype: 'displayfield'	
				},
				{
					xtype      : 'radiofield',
					identificador:'opcion_radio',
					boxLabel:      'Enviar Mensaje',
					itemId:'rdEM',
					name:          'rdAction',
					inputValue:    'E'
				}
			]
		},
		{
			xtype:'panel',
			frame: true,
			layout:'table',
			itemId:'rdDDMP',
			identificador:'opcion_radio',
			cls: 'panel-sin-borde',
			border	: false,
			width:300,
			items: [
				{	
					xtype: 'button',	
					itemId: 'btnAyuda1',	
					iconCls: 'icoAyuda',
					width:20, 
					handler: function(obj){
						ventanaAyuda1.setVisible(true);
					}
				},
				{ 	
					width:10, 
					xtype: 'displayfield'	
				},
				{
					xtype      : 'radiofield',
					identificador:'opcion_radio',
					boxLabel:      'Dejar de Enviar Mensaje',
					itemId:'rdDDM',
					name:          'rdAction',
					inputValue:    'D'
				}
			]
		},
		{
			xtype:'panel',
			frame: true,
			layout:'table',
			identificador:'opcion_radio',
			cls: 'panel-sin-borde',
			border	: false,
			width:300,
			items: [
				{ 	
					width:30, 
					xtype: 'displayfield'	
				},
				{
					xtype      : 'radiofield',
					identificador:'opcion_radio',
					boxLabel:      'Generar Archivo',
					inputValue:    'GA',
					itemId:'rdGA',
					name:          'rdAction',
					checked:       true
				}
			]
		},
		{
			xtype:'panel',
			frame: true,
			layout:'table',
			identificador:'opcion_radio',
			cls: 'panel-sin-borde',
			border	: false,
			width:300,
			items: [
				{ 	
					width:30, 
					xtype: 'displayfield'	
				},
				{
					xtype      : 'radiofield',
					identificador:'opcion_radio',
					boxLabel:      'Generar Archivo con Detalle',
					itemId:'rdGAD',
					name:          'rdAction',
					inputValue:    'GD'
				}
			]
		}
		
		
	]
	var elementosDetalle = [
		{
			xtype: 'combo',
			itemId:  'respuesta',
			fieldLabel: 'Respuesta',
			name:  'respuesta',
			hiddenName: 'respuesta',
			forceSelection:true,
			allowBlank: false,
			emptyText: 'Seleccione...',
			queryMode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			typeAhead:		true,
			labelWidth: 100,
			triggerAction:	'all',
			width:  450,
			store: new Ext.data.ArrayStore(
				{
					fields: [
						'clave',
						'descripcion'
					],
					data: [
						['S', 'Si'], 
						['R', 'No'],
						['N','No Contestada']
					]
				}
															
			)
		},
		{
			xtype: 'hidden',
			itemId:'epo_detalle',
			name : 'epo_detalle'
		}
	]
	var formaPrincipal = new Ext.form.FormPanel({
		width:                     500,
		height :'auto',
		itemId:                    'formaPrincipal',
		title:                     'Clientes Susceptibles de Credicadenas',
		bodyPadding:               '0 0 0 0',
		style:                     'margin: 10px auto 10px auto;',
		frame:                     true,
		border:                    true,
		fieldDefaults: {
			msgTarget:              'side',
			labelWidth:             70
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			
			{
				text:                   '<center>Consultar</center>',
				itemId:                 'btnConsultar',
				iconCls:                'icoBuscar',
				handler:                consultarInfo
			},
			{
				xtype: 'button',
				text:       '<center>Detalle</center>',
				itemId:     'btnDetalle',
				handler:    verDetalle
			}
		]
	});
	
	var formaGrid = new Ext.form.FormPanel({
		width:                     500,
		height :'auto',
		itemId:                    'formaGrid',
		bodyPadding:               '0 0 0 0',
		style:                     'margin: 10px auto 10px auto;',
		frame:                     true,
		hidden:  true,
		border:                    true,
		fieldDefaults: {
			msgTarget:              'side',
			labelWidth:             70
		},
		items: elementosGrid,
		monitorValid: true,
		buttons: [
			
			{
				text:                   '<center>Aceptar</center>',
				itemId:                 'btnAcepatar',
				iconCls:                'icoAceptar',
				handler:                aceptar
			}
		]
	});
	var formaDetalle = new Ext.form.FormPanel({
		width:                     500,
		height :'auto',
		title:'Detalle',
		itemId:                    'formaDetalle',
		bodyPadding:               '0 0 0 0',
		style:                     'margin: 10px auto 10px auto;',
		frame:                     true,
		hidden:  true,
		border:                    true,
		fieldDefaults: {
			msgTarget:              'side',
			labelWidth:             70
		},
		items: elementosDetalle,
		monitorValid: true,
		buttons: [
			
			{
				text:                   '<center>Consultar</center>',
				itemId:                 'btnConsultar',
				iconCls:                'icoBuscar',
				handler: function(grid, rowIndex, colIndex, item, event) {
					
					var epoDetalle = Ext.ComponentQuery.query('#epo_detalle')[0].getValue();  
					consultaDataDetalle.load({
						params: Ext.apply(
							{
								informacion: 'Consulta_info_detalle',
								epoDetalle: epoDetalle,
								mes:    Ext.ComponentQuery.query('#mes')[0].getValue(),
								anio:    Ext.ComponentQuery.query('#anio')[0].getValue(),
								respuesta: Ext.ComponentQuery.query('#respuesta')[0].getValue()
							})
						});
					
				}
			}
		]
	});
	
	var sm = Ext.create('Ext.selection.CheckboxModel');
	var  grid = Ext.create('Ext.grid.Panel',{
		itemId: 'grid',
		store:            Ext.data.StoreManager.lookup('consultaData'),
	   title: 'Consulta',
		xtype:           'grouped-header-grid',
		style: 'margin: 10px auto 0px auto;',
     	hidden:  true,
		frame: true,
		height: 400,		
		width: 815,
		selModel: sm,
		columns: [
			{
				header: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 200,
				align: 'center',
				resizable: true
			},
			{
				header: 'Total de Pymes',
				dataIndex: 'NUM_PYMES',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			},
			{
				header: '<div align="center"> Total de Pymes<br>a quienes se les<br>envio mensaje </div>',
				tooltip: ' Total de Pymes a quienes se les envio mensaje',	
				dataIndex: 'NUM_PYMES_MSG',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			},
			{
				header: 'Total de Pymes<br>que respondieron<br>que si',
				tooltip: 'Total de Pymes que respondieron que si',	
				dataIndex: 'NUM_RESP_SI',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			},
			{
				header: 'Total de Pymes<br>que respondieron<br>que no',
				tooltip: 'Total de Pymes que respondieron que no',	
				dataIndex: 'NUM_RESP_NO',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			},
			{
				header: 'Total de Pymes<br>que no han<br>contestado',
				tooltip: 'Total de Pymes que no han contestado',	
				dataIndex: 'SIN_RESPONDER',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			},
			{
				header: 'Total de Pymes<br>que no se les<br>envio mensaje',
				tooltip: 'Total de Pymes que no se les envio mensaje',	
				dataIndex: 'PYMES_SIN_MSG',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			},
			{
				header:'<a href="JavaScript:setMessage();"><img src="/nafin/00utils/gif/ihelp.gif" border="0" width="15"></a>&nbsp;Estatus de<br>Env&iacute;o',
				dataIndex: 'ESTATUS_ENVIO',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			},
			{
				width:         75,
				xtype:         'actioncolumn',
				header:        'Ver detalle',
				align:         'center',
				sortable:      false,
				resizable:     false,
				menuDisabled:  true,
				items: [
					{
						getClass:   function(value,metadata,record,rowIndex,colIndex,store){
								this.items[0].tooltip = 'Ver';
								return 'icoBuscar';	
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
									detalleClientes(grid, rowIndex, colIndex, item, event);
						}
					}
				]
			}
		]
	});
	var  gridTotal = Ext.create('Ext.grid.Panel',{
		itemId: 'gridTotal',
		store: consTotalData,				
	   title: '',	
		style: 'margin: 10px auto 0px auto;',
     	hidden:          true,
		frame: true,
		height: 'auto',		
		width: 815,
		height: 100,	
		columns: [
			
			{
				header: '',
				dataIndex: 'DESCRICPION',
				width:        200,
				sortable:     true,
			   resizable:    true,				
			   align:        'center'
         },			
			{
				header: 'Total de Pymes',
				dataIndex: 'TOT_PYMES',
				width:        100,
				sortable:     true,
			   resizable:    true,
			   align:        'center'		
         },
			{
				header: '<div align="center"> Total de Pymes<br>a quienes se les<br>envio mensaje </div>',
				dataIndex: 'TOT_PYMES_MSG',
				width:        100,
				sortable:     true,
			   resizable:    true,
			   align:        'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
         },
			{
				header: '<div align="center"> Total de Pymes<br>que respondieron<br>que si </div>',
				dataIndex: 'TOT_PYMES_SI',
				width:        100,
				sortable:     true,
			   resizable:    true,
			   align:        'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
         },	
			{
				header: '<div align="center"> Total de Pymes<br>que respondieron<br>que no </div>',
				dataIndex: 'TOT_PYMES_NO',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			},				  
			{
				header:'<div align="center"> Total de Pymes<br>que no han<br>contestado </div>',
				dataIndex: 'TOT_SIN_RESP',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			},
			{
				header:'<div align="center"> Total de Pymes<br>que no se les<br>envio mensaje </div>',
				dataIndex: 'TOT_SIN_MSG',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			}
		]
	});
	var  gridDetalle = Ext.create('Ext.grid.Panel',{
		itemId: 'gridDetalle',
		store:            Ext.data.StoreManager.lookup('consultaDataDetalle'),
	   title: 'Consulta detalle',
		xtype:           'grouped-header-grid',
		style: 'margin: 10px auto 0px auto;',
     	hidden:  true,
		frame: true,
		height: 300,		
		width: 815,
		columns: [
					{
						header: 'N�m. Pyme',
						sortable: true,
						tooltip:   'Num. Pyme',
						width: 100,
						dataIndex: 'IC_PYME',
						align: 'center',
						resizable: true
					},				  
					{
						header: 'N�m. Sirac',
						sortable: true,
						tooltip:   'Num Sirac',
						dataIndex: 'NUMERO_SIRAC',
						width: 100,
						align: 'center',
						resizable: true
					},
					{
						header: 'PYME',
						sortable: true,
						tooltip:   'PYME',
						width: 100,
						dataIndex: 'NOMBRE_PYME',
						align: 'center',
						resizable: true
					},
					{
						header: 'Cred. Cad.',
						sortable: true,
						tooltip:   'Cred. Cad.',
						width: 100,
						align: 'center',
						dataIndex: 'CREDCAD',
						resizable: true
					},
					{
						header: 'Estado',
						sortable: true,
						tooltip:   'Estado',
						width: 100,
						align: 'center',
						dataIndex: 'ESTADO',
						resizable: true
					},
					{
						header: 'C.P',
						sortable: true,
						tooltip:   'C.P',
						width: 100,
						dataIndex: 'CP',
						align: 'center',
						resizable: true
					},
					{
						header: 'Tel�fono',
						sortable: true,
						tooltip:   'Tel�fono',
						dataIndex: 'TELEFONO',
						width: 100,
						align: 'center',
						resizable: true
					},
					{
						header: 'Monto Cr�dito<br>Ventas 3 meses',
						sortable: true,
						width: 100,
						tooltip:   'Monto Cr�dito Ventas 3 meses',
						align: 'center',
						dataIndex: 'MONTO_CREDITO',
						resizable: true
					},
					{
						header: 'Monto L�nea<br>Consolidado',
						sortable: true,
						width: 100,
						tooltip:   'Monto L�nea<br>Consolidado',
						align: 'center',
						dataIndex: 'MONTO_CONSOLIDADO',
						resizable: true
					},
					{
						header: 'Respuesta',
						sortable: true,
						width: 100,
						tooltip:   'Respuesta',
						dataIndex: 'RESPUESTA',
						align: 'center',
						resizable: true
					},
					{
						header: 'Fecha de<br>Respuesta',
						sortable: true,
						tooltip:   'Fecha de<br>Respuesta',
						dataIndex: 'FECHA_RESPUESTA',
						width: 100,
						align: 'center',
						resizable: true
					}
		],
		bbar: [
			'->','-',
			
			{
				text: 'Generar Archivo',
				id: 'btnArchivoXLSDetalle',	
				iconCls: 'icoBotonXLS',
				handler: procesarArchivosXLSDetalle
				
			},
			{
				text: 'Regresar',
				id: 'btnRegresar',	
				iconCls: 'icoRegresar',
				handler: function(){
					var gridDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
					var gridTotalDetalle = Ext.ComponentQuery.query('#gridTotalDetalle')[0];
					var formaDetalle = Ext.ComponentQuery.query('#formaDetalle')[0];
					
					var formaPrincipal = Ext.ComponentQuery.query('#formaPrincipal')[0];
					var grid = Ext.ComponentQuery.query('#grid')[0];
					var gridTotal = Ext.ComponentQuery.query('#gridTotal')[0];
					var formaGrid = Ext.ComponentQuery.query('#formaGrid')[0];
					formaDetalle.getForm().reset();
					gridDetalle.hide();
					gridTotalDetalle.hide();
					formaDetalle.hide();
					
					formaPrincipal.show();
					grid.show();
					gridTotal.show();
					formaGrid.show();
					
				}
				
			}
			
		] 				
	});
	var  gridTotalDetalle = Ext.create('Ext.grid.Panel',{
		itemId: 'gridTotalDetalle',
		store: consTotalDataDetalle,				
	   title: 'Resumen',	
		style: 'margin: 10px auto 0px auto;',
     	hidden:          true,
		frame: true,
		height: 'auto',		
		width: 615,
		height: 130,	
		columns: [
			
			{
				header: '',
				dataIndex: 'DESCRIPCION',
				width:        200,
				sortable:     true,
			   resizable:    true,				
			   align:        'center'
         },			
			{
				header: '<div align="center"> PYMES Si<br>Interesadas </div>',
				dataIndex: 'NUM_RESP_SI',
				width:        100,
				sortable:     true,
			   resizable:    true,
			   align:        'center'		
         },
			{
				header: '<div align="center"> PYMES No<br>Interesadas </div>',
				dataIndex: 'NUM_RESP_NO',
				width:        100,
				sortable:     true,
			   resizable:    true,
			   align:        'center'
         },
			{
				header: '<div align="center"> PYMES Sin<br>Contestar </div>',
				dataIndex: 'SIN_RESPONDER',
				width:        100,
				sortable:     true,
			   resizable:    true,
			   align:        'center'
         },	
			{
				header: '<div align="center"> Total </div>',
				dataIndex: 'NUM_PYMES_MSG',
				sortable: true,
				width: 100,
				align: 'center',
				resizable: true
			}
		]
	});
	var main = Ext.create('Ext.container.Container',{
		itemId:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		width:      949,
		items: [
			formaPrincipal,
			formaDetalle,
			NE.util.getEspaciador(10),
			grid,
			gridTotal,
			formaGrid,
			gridDetalle,
			gridTotalDetalle
		]
	});
	
});