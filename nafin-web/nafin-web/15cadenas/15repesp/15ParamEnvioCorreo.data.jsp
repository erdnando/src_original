<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*, 
		netropology.utilerias.usuarios.*, 
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.*"		
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar="", consulta="",consultaGrid="", mensaje ="";
	System.out.println("informacion *** "+informacion);
	String respuesta="";
	
	JSONObject jsonObj = new JSONObject();
	HashMap datos = new HashMap();
	JSONArray registros1 = new JSONArray();
	JSONObject jsonObjG = new JSONObject();
	try{   
	
		InteligenciaComercial beanInt = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);
		
		String ic_envio_clientes[] =request.getParameterValues("ic_envio_clientes");
		String origen[] = request.getParameterValues("origen");  
		String cg_email[] = request.getParameterValues("cg_email");
		String cs_envio_mensual[] = request.getParameterValues("cs_envio_mensual");
		String cs_envio_diario[] = request.getParameterValues("cs_envio_diario");
		String mailAgrega = (request.getParameter("cg_email_agrega")!=null)?request.getParameter("cg_email_agrega"):"";
		String hidAction = (request.getParameter("hidAction")!=null)?request.getParameter("hidAction"):"";
		
		if(informacion.equals("Consulta_info")){
			JSONArray reg = new JSONArray();
			List	lCorreos	= null;
			Map		mParamEnvio	= null;
			mParamEnvio = beanInt.consultaParamEnvio(hidAction
						, mailAgrega
						, ic_envio_clientes
						, origen
						, cg_email
						, cs_envio_mensual
						, cs_envio_diario
						, 5);
			lCorreos	= (List)mParamEnvio.get("correos");
			if(lCorreos.size()>0){
				
				for(int i = 0; i<lCorreos.size();i++){
					Map mColumnas = (Map)lCorreos.get(i);
					//if(!"eliminar".equals(mColumnas.get("origen").toString())){
						datos = new HashMap();
						datos.put("ic_envio_clientes",mColumnas.get("ic_envio_clientes"));
						datos.put("cg_email",mColumnas.get("cg_email"));
						datos.put("cs_envio_diario",mColumnas.get("cs_envio_diario"));
						datos.put("cs_envio_mensual",mColumnas.get("cs_envio_mensual"));
						datos.put("cs_envio_mensual",mColumnas.get("cs_envio_mensual"));
						datos.put("origen",mColumnas.get("origen"));
						datos.put("indice",String.valueOf(i));
						
						reg.add(datos);
					//}
				}
			}
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("registros",reg);
			jsonObj.put("cs_envio_mensual_clientes",mParamEnvio.get("cs_envio_mensual_clientes"));
			jsonObj.put("cs_envio_diario_clientes",mParamEnvio.get("cs_envio_diario_clientes"));
			jsonObj.put("hidAction",hidAction);
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("guardar_datos")){
			String cs_envio_mensual_clientes = (request.getParameter("cs_envio_mensual_clientes")!=null)?request.getParameter("cs_envio_mensual_clientes"):"";
			String cs_envio_diario_clientes = (request.getParameter("cs_envio_diario_clientes")!=null)?request.getParameter("cs_envio_diario_clientes"):"";
			beanInt.guardarParamEnvio(
				cs_envio_mensual_clientes
				,cs_envio_diario_clientes
				,ic_envio_clientes
				,origen
				,cg_email
				,cs_envio_mensual
				,cs_envio_diario
				,5);	
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("mns","La información ha sido guardada con éxito");
			infoRegresar = jsonObj.toString();
		}
	} catch(Exception e) {
		throw new AppException("Error al generar la consulta ", e);
	}
	
	%>	

<%= infoRegresar %>


