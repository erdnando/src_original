Ext.onReady(function() {

	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();							
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	Ext.apply(Ext.form.field.VTypes, {
		rangoFechas: function(val, field) {
			var date = field.parseDate(val);	
			if (!date) {
				return false;
			}
			if (field.startDateField && (!this.rangoFechasMax || (date.getTime() != this.rangoFechasMax.getTime()))) {
				var start = field.up('form').down('#' + field.startDateField);
				start.setMaxValue(date);
				start.validate();
				this.rangoFechasMax = date;
			} else if (field.endDateField && (!this.rangoFechasMin || (date.getTime() != this.rangoFechasMin.getTime()))) {
				var end = field.up('form').down('#' + field.endDateField);
				end.setMinValue(date);
				end.validate();
				this.rangoFechasMin = date;
			}
			return true;
		},
		rangoFechasText: 'La fecha de inicio debe ser menor que la fecha final'
	});


	function cambiosRenderer(val){
    return '<div style="white-space:normal !important;">'+ val +'</div>';
	}
	
	//vista  de Personas 
	var detallePersonas  = function (persona, grid, rowIndex, colIndex, item, event){
	
		var  fp =   Ext.ComponentQuery.query('#forma')[0];
		var titulo;
		
		var registro = grid.getStore().getAt(rowIndex);
		var base_operacion = registro.get('IC_BASE_OPERACION');  
		var clave_if = registro.get('CLAVE_IF'); 
		var tipo_credito = registro.get('TIPO_CREDITO'); 
		var clave_estado = registro.get('CLAVE_ESTADO'); 
		
		if(persona=='F') { 
			titulo= "Detalle Personas F�sicas";
		}else if(persona=='M') { 
			titulo= "Detalle Personas Morales";
		}	
			
		consDetalleData.load({
			params: Ext.apply(fp.getForm().getValues(),{ 
				informacion:'consDetPersonas',
				base_operacion:base_operacion,
				clave_if:clave_if,
				tipo_credito:tipo_credito,
				clave_estado:clave_estado,
				persona: persona
			})
		});		
						
		new Ext.Window({
			modal: true,
			width: 770,
			resizable: false,
			closable:true,
			id: 'VentanaPersona',
			autoDestroy:false,
			closeAction: 'destroy',
			items: [
				gridDetalle				
			],
			bbar:{
				items: [
					'-',
					'->',
					{
						text: 'Descarga Archivo',
						id: 'btnDescArchDetPyme',	
						iconCls: 'icoBotonXLS',
						disabled:  true,
						handler: function(){ 
						
							Ext.Ajax.request({
								url: '15RepCreditoElectronico.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{					
									informacion:'Generar_Arch_Det',
									tipo:'XLS',
									base_operacion:base_operacion,
									clave_if:clave_if,
									tipo_credito:tipo_credito,
									clave_estado:clave_estado,
									persona: persona,
									titulo:titulo
								}),
								callback: procesarDescargaArchivos
							});						
						}
					},
					{
						text: 'Imprimir',
						id: 'btnImprimirDetPyme',	
						iconCls: 'icoPdf',
						disabled:  true,
						handler: function(){
						
							Ext.Ajax.request({
								url: '15RepCreditoElectronico.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{					
									informacion:'Generar_Arch_Det',
									tipo:'PDF',
									base_operacion:base_operacion,
									clave_if:clave_if,
									tipo_credito:tipo_credito,
									clave_estado:clave_estado,
									persona: persona,
									titulo:titulo
								}),
								callback: procesarDescargaArchivos
							});									
						}
					}
				]
			}		
		}).show().setTitle(titulo);			
	}
	
	var procesarConsDetallePyme = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			var  forma =   Ext.ComponentQuery.query('#forma')[0];
			forma.el.unmask();
				
			
			if(store.getTotalCount() > 0) {
				Ext.ComponentQuery.query('#btnDescArchDetPyme')[0].enable();	
				Ext.ComponentQuery.query('#btnImprimirDetPyme')[0].enable();
				Ext.ComponentQuery.query('#gridDetalle')[0].getView().setAutoScroll(true);
			}else  {
				Ext.ComponentQuery.query('#gridDetalle')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridDetalle')[0].getView().setAutoScroll(false);
			
				Ext.ComponentQuery.query('#btnDescArchDetPyme')[0].disable();	
				Ext.ComponentQuery.query('#btnImprimirDetPyme')[0].disable();	
			}
		}
	};
	
	Ext.define('LisRegDetalle', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'NO_SIRAC'},
			{ name: 'RFC'},
			{ name: 'RAZON_SOCIAL'},
			{ name: 'ESTADO'}	,
			{ name: 'CIUDAD'}	
		 ]
	});
	
	
	var consDetalleData = Ext.create('Ext.data.Store', {
		model: 'LisRegDetalle',
		proxy: {
			type: 'ajax',
			url: '15RepCreditoElectronico.data.jsp',
			reader: 		 { type: 'json',  root: 'registros' 	},
			extraParams: { informacion: 'consDetPersonas' 	},
			listeners:   { exception: NE.util.mostrarProxyAjaxError 	}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsDetallePyme
		}		
	});
	
	var  gridDetalle = {
		xtype: 'grid',		
		itemId: 'gridDetalle',
		store: consDetalleData,
		height: 200,
		width: 760,
		style: 'margin: 10px auto 0px auto;',	     
      frame: true,		
		clicksToEdit: 1,
		columns: [		
			{
				header: 'N�mero SIRAC',
				dataIndex: 'NO_SIRAC',
				sortable: true,
				width: 150,
				align: 'center'
         },	
			{
				header: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 150,
				align: 'center'
         },	
			{
				header: 'Nombre o Raz�n Social',
				dataIndex: 'RAZON_SOCIAL',
				sortable: true,
				width: 150,
				align: 'left',
				renderer:cambiosRenderer	
         },
			{
				header: 'Estado',
				dataIndex: 'ESTADO',
				sortable: true,
				width: 150,
				align: 'left',
				renderer:cambiosRenderer	
         },
			{
				header: 'Ciudad',
				dataIndex: 'CIUDAD',
				sortable: true,
				width: 150,
				align: 'left',
				renderer:cambiosRenderer	
         }					
		]
	}
	
	//-------------GENERACI�N DE LA CONSULTA PRINCIPAL ----------------------------
	
	
	
	var procesarArchivosXLS = function() {
		var  fp =   Ext.ComponentQuery.query('#forma')[0];		
		var  clave_if;
		if( Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']=='BASE_OPER' ){	
			clave_if = fp.query('#clave_if1')[0].getValue();								
		}else  if( Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']=='INT_FIN' ){		
			clave_if = fp.query('#clave_if_2_1')[0].getValue();
		}			
		Ext.Ajax.request({
			url: '15RepCreditoElectronico.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{					
				informacion:'Generar_Arch',
				tipo:'XLS',
				clave_if:clave_if
			}),
			callback: procesarDescargaArchivos
		});
	}
	
	var procesarArchivosPDF = function() {
		var  fp =   Ext.ComponentQuery.query('#forma')[0];		
		var  clave_if;
		if( Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']=='BASE_OPER' ){	
			clave_if = fp.query('#clave_if1')[0].getValue();								
		}else  if( Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']=='INT_FIN' ){		
			clave_if = fp.query('#clave_if_2_1')[0].getValue();
		}

		Ext.Ajax.request({
			url: '15RepCreditoElectronico.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{					
				informacion:'Generar_Arch',
				tipo:'PDF',
				clave_if:clave_if
			}),
			callback: procesarDescargaArchivos
		});
	}
	
	
	var procesarConsultar = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			var  forma =   Ext.ComponentQuery.query('#forma')[0];			
			main.el.unmask();
			
			Ext.ComponentQuery.query('#gridConsulta')[0].show();	
			
			if( json.reporte_credito=='BASE_OPER' ){	
			
				Ext.ComponentQuery.query('#BASE_OPERACION')[0].setText('Base Operaci�n (SIRAC');
				Ext.ComponentQuery.query('#NOMBRE_IF')[0].setText('Intermediario Financiero');
				
			}else  if( json.reporte_credito=='INT_FIN' ){	
			
				Ext.ComponentQuery.query('#BASE_OPERACION')[0].setText('Intermediario Financiero');
				Ext.ComponentQuery.query('#NOMBRE_IF')[0].setText('Base Operaci�n (SIRAC');	
				
			}
			
			if(store.getTotalCount() > 0) {
			
				Ext.ComponentQuery.query('#gridConsulta')[0].setTitle(json.periodo);
			
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(true);
				Ext.ComponentQuery.query('#btnDescArch')[0].enable();
				Ext.ComponentQuery.query('#btnImprimir')[0].enable();
				Ext.ComponentQuery.query('#gridTotal')[0].show();	
				
		
			
			
			var storeTotales = [
				['Total General',json.TOTAL_REGISTOS, json.TOTAL_MONTO_SOLICITADO, json.TOTAL_MONTO_DESCUENTO ,json.TOTAL_PFISICAS ,json.TOTAL_PMORALES  ]
			];
			
		
			consTotalData.loadData(storeTotales);	
		
			}else  {
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(false);
				Ext.ComponentQuery.query('#btnDescArch')[0].disable();
				Ext.ComponentQuery.query('#btnImprimir')[0].disable();
				Ext.ComponentQuery.query('#gridTotal')[0].hide();	
			}
		}
	};

	
	var consTotalData = new Ext.data.ArrayStore({
		  fields: [
			{ name: 'DESCRICPION'},
			{ name: 'TOTAL_REGISTOS'},			
			{ name: 'TOTAL_MONTO_SOLICITADO'},
			{ name: 'TOTAL_MONTO_DESCUENTO'},
			{ name: 'TOTAL_PFISICAS'},
			{ name: 'TOTAL_PMORALES'}	
		  ]
	 });
	 
	 
	var  gridTotal = Ext.create('Ext.grid.Panel',{
		itemId: 'gridTotal',
		store: consTotalData,				
	   title: '',	
		style: 'margin: 10px auto 0px auto;',
     	hidden:          true,
		frame: true,
		height: 'auto',		
		width: 900,
		columns: [
			{
				header: '',
				dataIndex: 'DESCRICPION',
				width:        100,
				sortable:     true,
			   resizable:    true,				
			   align:        'center'
         },			
			{
				header: 'Total de Registros',
				dataIndex: 'TOTAL_REGISTOS',
				width:        150,
				sortable:     true,
			   resizable:    true,
			   align:        'center'		
         },
			{
				header: 'Total Monto Solicitado',
				dataIndex: 'TOTAL_MONTO_SOLICITADO',
				width:        150,
				sortable:     true,
			   resizable:    true,
			   align:        'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
         },
			{
				header: 'Total Monto del Descuento',
				dataIndex: 'TOTAL_MONTO_DESCUENTO',
				width:        160,
				sortable:     true,
			   resizable:    true,
			   align:        'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
         },	
			{
				header: 'Total Personas F�sicas',
				dataIndex: 'TOTAL_PFISICAS',
				sortable: true,
				width: 160,
				align: 'center',
				resizable: true
			},				  
			{
				header:'Total Personas Morales',
				dataIndex: 'TOTAL_PMORALES',
				sortable: true,
				width: 160,
				align: 'center',
				resizable: true
			}
		]
	});
			
		
	Ext.define('LisRegistros', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'BASE_OPERACION'},
			{ name: 'NOMBRE_IF'},
			{ name: 'TIPO_DE_CREDITO'},
			{ name: 'TOTAL_REGISTOS'},
			{ name: 'MONTO_SOLICITADO'},
			{ name: 'MONTO_DESCUENTO'},
			{ name: 'ESTADO_ACREDITADO'},
			{ name: 'TOTAL_PFISICAS'},
			{ name: 'TOTAL_PMORALES'},			
			{ name: 'IC_BASE_OPERACION'},
			{ name: 'CLAVE_IF'},
			{ name: 'TIPO_CREDITO'},
			{ name: 'CLAVE_ESTADO'}
		 ]
	});
	
	var consultaData = Ext.create('Ext.data.Store', {
		model: 'LisRegistros',
		proxy: {
			type: 'ajax',
			url: '15RepCreditoElectronico.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consultar'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsultar
			}
	});
	
	
	var  gridConsulta = Ext.create('Ext.grid.Panel',{
		itemId: 'gridConsulta',
		store: consultaData,				
	   title: 'Consulta',
		xtype:           'grouped-header-grid',
		style: 'margin: 10px auto 0px auto;',
     	hidden:  true,
		frame: true,
		height: 400,		
		width: 900,
		columns: [
			{
				header: 'Base Operaci�n (SIRAC)',
				dataIndex: 'BASE_OPERACION',
				itemId:'BASE_OPERACION',
				width:        200,
				sortable:     true,
			   resizable:    true,				
			   align:        'left',
				renderer:cambiosRenderer	
         },
			{
				header: 'Intermediario Financiero',
				dataIndex: 'NOMBRE_IF',	
				itemId:'NOMBRE_IF',
				width:        200,
				sortable:     true,
			   resizable:    true,
			   align:        'left',				
				renderer:cambiosRenderer	
         },
			{
				header: 'Tipo de Cr�dito <br>(Destino del Cr�dito)',
				dataIndex: 'TIPO_DE_CREDITO',				
				width:        150,
				sortable:     true,
			   resizable:    true,
			   align:        'left'		
         },
			{
				header: 'Total de Registros',
				dataIndex: 'TOTAL_REGISTOS',
				width:        100,
				sortable:     true,
			   resizable:    true,
			   align:        'center'		
         },
			{
				header: 'Monto Solicitado',
				dataIndex: 'MONTO_SOLICITADO',
				width:        150,
				sortable:     true,
			   resizable:    true,
			   align:        'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
         },
			{
				header: 'Monto del Descuento',
				dataIndex: 'MONTO_DESCUENTO',
				width:        150,
				sortable:     true,
			   resizable:    true,
			   align:        'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
         },
			{
				header: 'Estado del Acreditado',
				dataIndex: 'ESTADO_ACREDITADO',
				width:        150,
				sortable:     true,
			   resizable:    true,
			   align:        'left'		
         },
			
			{
				text: 'Personas F�sicas',
				menuDisabled: false,
				columns: [
					{
						header: 'Total',
						dataIndex: 'TOTAL_PFISICAS',
						sortable: true,
						width: 100,
						align: 'center',
						resizable: true
					},				  
					{
						xtype		: 'actioncolumn',
						header: 'Detalle ',
						tooltip: 'Detalle',				
						sortable: true,
						width: 100,			
						resizable: true,				
						align: 'center',
						items		: [
							{
								getClass: function(value,metadata,record,rowIndex,colIndex,store){									
									this.items[0].tooltip = 'Ver';
									return 'icoBuscar';									
								}							
								,handler: function(grid, rowIndex, colIndex, item, event) {
									detallePersonas('F', grid, rowIndex, colIndex, item, event);
								}
							}
						]				
					}
				]
			},
			{
				text: 'Personas Morales',
				menuDisabled: false,
				columns: [
					{
						header: 'Total',
						dataIndex: 'TOTAL_PMORALES',
						sortable: true,
						width: 100,
						align: 'center',
						resizable: true
					},	
					{
						xtype		: 'actioncolumn',
						header: 'Detalle',
						tooltip: 'Detalle',				
						sortable: true,
						width: 100,			
						resizable: true,				
						align: 'center',
						items		: [
							{
								getClass: function(value,metadata,record,rowIndex,colIndex,store){									
									this.items[0].tooltip = 'Ver';
									return 'icoBuscar';									
								}
								,handler: function(grid, rowIndex, colIndex, item, event) {
									detallePersonas('M', grid, rowIndex, colIndex, item, event);
								}
							}
						]				
					}
				]
			}
		],
		bbar: [
			'->','-',
			{
				text: 'Descarga Archivo',
				id: 'btnDescArch',	
				iconCls: 'icoBotonXLS',
				handler: procesarArchivosXLS
				
			},
			{
				text: 'Imprimir',
				id: 'btnImprimir',	
				iconCls: 'icoPdf',
				handler: procesarArchivosPDF
			}
		] 		
	});
	
	
	var limpiar = function() { 
		Ext.ComponentQuery.query('#base_operacion1')[0].setValue('');
		Ext.ComponentQuery.query('#clave_if1')[0].setValue('');
		Ext.ComponentQuery.query('#tipo_credito1')[0].setValue('');
		Ext.ComponentQuery.query('#clave_estado1')[0].setValue('');
		Ext.ComponentQuery.query('#fecha_public_oper_ini')[0].setValue('');
		Ext.ComponentQuery.query('#fecha_public_oper_fin')[0].setValue('');
		Ext.ComponentQuery.query('#clave_if_2_1')[0].setValue(''); 
		
		Ext.ComponentQuery.query('#gridConsulta')[0].hide();	
		Ext.ComponentQuery.query('#gridTotal')[0].hide();	

	}


								
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
	

	var  catalogoBaseOperacion = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15RepCreditoElectronico.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoBaseOperacion'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var  catalogoIF = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15RepCreditoElectronico.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoIF'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var  catalogoTipoCredito = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15RepCreditoElectronico.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoTipoCredito'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	
	var  catalogoEstado = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '15RepCreditoElectronico.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catalogoEstado'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	
	var elementosForma =[	
		{
			xtype:               'container',
			layout:              'hbox',
			anchor:              '95%',
			margin:              '0 0 0 0',
			items:[
				{
					width:            150,
					xtype:            'displayfield',
					hideLabel:        true
				},
				{
					width:            300,
					xtype:            'radiogroup',
					itemId:           'reporte_credito1',
					fieldLabel:       'Auto Layout',
					cls:              'x-check-group-alt',
					hideLabel:        true,
					items: [
						{
							boxLabel:      'Base de Operaci�n ',
							name:          'reporte_credito',
							inputValue:    'BASE_OPER',
							checked:       true
						},
						{
							boxLabel:      'IF',
							name:          'reporte_credito',
							inputValue:    'INT_FIN'
						}
					],
					listeners: {
						change: function (field, newValue, oldValue) {
							
								limpiar();
							if(newValue['reporte_credito']=='BASE_OPER') {	
							
								Ext.ComponentQuery.query('#clave_if_2_1')[0].hide();
								Ext.ComponentQuery.query('#clave_if1')[0].show();									
								catalogoBaseOperacion.load();
								
							}else  if(newValue['reporte_credito']=='INT_FIN') {
								
								Ext.ComponentQuery.query('#clave_if_2_1')[0].show();
								Ext.ComponentQuery.query('#clave_if1')[0].hide();
								
								catalogoIF.load({
									params: 	{
										reporte_credito: newValue['reporte_credito']
									}
								});
								
								catalogoBaseOperacion.load({
									params: 	{
										reporte_credito: newValue['reporte_credito']
									}
								});	
								
								catalogoTipoCredito.load({
									params: 	{
										reporte_credito: newValue['reporte_credito']
									}
								});
								
							}							
						}
					}
				}
			]
		},		
		{
			xtype: 'combo',
			fieldLabel: 'IF',
			itemId: 'clave_if_2_1',
			name: 'clave_if_2',
			hiddenName: 'clave_if_2',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			store: catalogoIF,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',	
			hidden:  true,
			listeners:	{					
				select:	{
					fn: function(combo){					
					
						catalogoBaseOperacion.load({
							params: 	{
								clave_if: combo.getValue(),
								reporte_credito:Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']
							}
						});
						
						
					}					
				}
			}
		},			
		{
			xtype: 'combo',
			fieldLabel: 'Base de Operaci�n',
			itemId: 'base_operacion1',
			name: 'base_operacion',
			hiddenName: 'base_operacion',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			store: catalogoBaseOperacion,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',			
			listeners:	{					
				select:	{
					fn: function(combo){	
					
						if( Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']=='BASE_OPER' ){					
							catalogoIF.load({
								params: 	{
									base_operacion: combo.getValue(),
									reporte_credito:Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']
								}
							});
						}else if( Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']=='INT_FIN' ){				
							
							catalogoTipoCredito.load({
								params: 	{
									clave_if: Ext.ComponentQuery.query('#clave_if_2_1')[0].getValue(),
									base_operacion: combo.getValue(),
									reporte_credito:Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']
								}
							});						
						}
					}					
				}
			}
		},
		{
			xtype: 'combo',
			fieldLabel: 'IF',
			itemId: 'clave_if1',
			name: 'clave_if',
			hiddenName: 'clave_if',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			store: catalogoIF,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave',			
			listeners:	{					
				select:	{
					fn: function(combo){					
						
						catalogoTipoCredito.load({
							params: 	{
								clave_if: combo.getValue(),
								reporte_credito:Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']
							}
						});
					}					
				}
			}
		},	
		{
			xtype: 'combo',
			fieldLabel: 'Tipo de Cr�dito',
			itemId: 'tipo_credito1',
			name: 'tipo_credito',
			hiddenName: 'tipo_credito',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			store: catalogoTipoCredito,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave'			
		},
		
			{
			xtype: 'combo',
			fieldLabel: 'Estado',
			itemId: 'clave_estado1',
			name: 'clave_estado',
			hiddenName: 'clave_estado',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			store: catalogoEstado,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave'			
		},
			{
			xtype: 'container',			
			layout: 'hbox', 			
			items: [
				{
					xtype: 'datefield',					
					fieldLabel: 'Per�odo',
					name: 'fecha_public_oper_ini',
					itemId: 'fecha_public_oper_ini',
					vtype: 'rangoFechas',
					endDateField: 'fecha_public_oper_fin',
					allowBlank: true,
					startDay: 1,					
					msgTarget: 'side',				
					minValue: '01/01/1901',					
					margins: '0 20 0 0'
					
				},
				{
					xtype: 'displayfield',
					value: '&nbsp;al &nbsp;', 
					width: 30
				},
				{
					xtype: 'datefield',					
					labelAlign: 'right',					
					name: 'fecha_public_oper_fin',
					itemId: 'fecha_public_oper_fin',
					vtype: 'rangoFechas',
					startDateField: 'fecha_public_oper_ini',
					allowBlank: true,
					startDay: 1,					
					msgTarget: 'side',				
					minValue: '01/01/1901',					
					margins: '0 20 0 0'					
				}
			]
		}
	];
	
	
	
	var fp = Ext.create('Ext.form.Panel',	{		
		itemId: 'forma',
      frame: true,
		border: true,     
		title: 'Cr�dito Electr�nico',
      width: 600,		
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 120
		},		
		items:elementosForma, 
		buttons: [		
			{
				text: 'Generar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(){
				
					var  fp =   Ext.ComponentQuery.query('#forma')[0];
					if( Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']=='BASE_OPER' ){	
					
						if(Ext.isEmpty(fp.query('#base_operacion1')[0].getValue())){
							fp.query('#base_operacion1')[0].markInvalid('El valor de la Base de Operaci�n es requerido.');
							fp.query('#base_operacion1')[0].focus();						
							return;
						}
						
						clave_if = fp.query('#clave_if1')[0].getValue();
						
					}else  if( Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']=='INT_FIN' ){		
					
						if(Ext.isEmpty(fp.query('#clave_if_2_1')[0].getValue())){
							fp.query('#clave_if_2_1')[0].markInvalid('El valor de el Intermediario Financiero es requerido.');
							fp.query('#clave_if_2_1')[0].focus();						
							return;
						}
					
						clave_if = fp.query('#clave_if_2_1')[0].getValue();
					
					}
					
					var fecha_public_oper_ini =  fp.query('#fecha_public_oper_ini')[0]; 
					var fecha_public_oper_fin =  fp.query('#fecha_public_oper_fin')[0]; 
							
					if(Ext.isEmpty(fecha_public_oper_ini.getValue()) || Ext.isEmpty(fecha_public_oper_fin.getValue()) ){
						if(Ext.isEmpty(fecha_public_oper_ini.getValue())){
							fecha_public_oper_ini.markInvalid('El valor de la Fecha Inicial es requerido.');
							fecha_public_oper_ini.focus();
							return;
						}
						if(Ext.isEmpty(fecha_public_oper_fin.getValue())){
							fecha_public_oper_fin.markInvalid('El valor de la Fecha Final es requerido.');
							fecha_public_oper_fin.focus();
							return;
						}
					}	
					
					main.el.mask('Procesando Consulta...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						clave_if:clave_if						
						})
					});
					
					
				}	
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',					
				formBind: true,
				handler: function(){
					window.location  = "15RepCreditoElectronicoExt.jsp"; 	
				
				}
			}
		]
	});
	
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 900,
		style: 'margin:0 auto;',
		items: [
			fp,
			NE.util.getEspaciador(10),
			gridConsulta,
			NE.util.getEspaciador(10),
			gridTotal
		]
	});
	
	
	catalogoEstado.load();
	catalogoBaseOperacion.load({
		params: 	{
			reporte_credito:Ext.ComponentQuery.query('#reporte_credito1')[0].getValue()['reporte_credito']
		}
	});
	
	
});
	
	
	