Ext.onReady(function(){
	
	function generar(boton) {
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var fecha1 = forma.query('#claveMesI')[0].getValue() + '/1/' + forma.query('#claveAnioI')[0].getValue(); 
		var fecha2 = forma.query('#claveMesF')[0].getValue() + '/1/' + forma.query('#claveAnioF')[0].getValue(); 
		var f1=new Date(fecha1); 
		var f2=new Date(fecha2); 
				
		if(f1 > f2){
			Ext.Msg.alert('Aviso','<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Rango de fechas incorrecto, por favor intente nuevamente.</center></td><td></td></table> </div>');
			forma.query('#claveMesI')[0].focus();
			return;
		}
		
		boton.setDisabled(true);
		boton.setIconCls('x-mask-msg-text');
	
		consultaData.load({
			params:
				{
					informacion: 'Consulta_info',
					operacion:    'generar',
					claveMesI:    forma.query('#claveMesI')[0].getValue(),
					claveMesF:    forma.query('#claveMesF')[0].getValue(),
					claveMesC:    forma.query('#claveMesC')[0].getValue(),
					claveAnioI:    forma.query('#claveAnioI')[0].getValue(),
					claveAnioF:    forma.query('#claveAnioF')[0].getValue(),
					claveAnioC:    forma.query('#claveAnioC')[0].getValue(),
					start:        0,
					limit:        15
				}
			});	
	}
	function limpiar(boton) {
		var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
		var gridConsultaDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
		if(gridConsulta.isVisible()){
			gridConsulta.hide();
		}
		if(gridConsultaDetalle.isVisible()){
			gridConsultaDetalle.hide();
		}
	}
	function procesarDescargaArchivos(opts, success, response) {
		Ext.ComponentQuery.query('#btnDescArch')[0].setIconCls('icoBotonXLS');
		Ext.ComponentQuery.query('#btnDescArch')[0].setDisabled(false);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();							
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesarDescargaArchivosDetalle(opts, success, response) {
		Ext.ComponentQuery.query('#btnArchivoXLSDetalle')[0].setIconCls('icoBotonXLS');
		Ext.ComponentQuery.query('#btnArchivoXLSDetalle')[0].setDisabled(false);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();							
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var detallePersonas  = function (grid, rowIndex, colIndex, item, event){
		var  fp =   Ext.ComponentQuery.query('#forma')[0];
		var registro = grid.getStore().getAt(rowIndex);
		var ANIO = registro.get('ANIO');  
		var NUMMES = registro.get('NUMMES');
		var MES = registro.get('MES');
		Ext.ComponentQuery.query('#anioDetalle')[0].setValue(ANIO);
		Ext.ComponentQuery.query('#numMes')[0].setValue(NUMMES);
		Ext.ComponentQuery.query('#mesDetalle')[0].setValue(MES);
		consultaDataDetalle.load({
			params: Ext.apply(
				{
					informacion: 'Consulta_info_detalle',
					operacion:'generar',
					anioDetalle: ANIO,
					numMes: NUMMES,
					mesDetalle:  MES,
					start:        0,
					limit:        15
				})
			});
	}
	
	var procesarArchivosXLS = function(boton) {
		var  forma =   Ext.ComponentQuery.query('#formaPrincipal')[0];
		var fecha1 = forma.query('#claveMesI')[0].getValue() + '/1/' + forma.query('#claveAnioI')[0].getValue(); 
		var fecha2 = forma.query('#claveMesF')[0].getValue() + '/1/' + forma.query('#claveAnioF')[0].getValue(); 
		var f1=new Date(fecha1); 
		var f2=new Date(fecha2); 
		if(f1 > f2){
			Ext.Msg.alert('Aviso','<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Rango de fechas incorrecto, por favor intente nuevamente.</center></td><td></td></table> </div>');
			forma.query('#claveMesI')[0].focus();
			return;
		}
		
		boton.setDisabled(true);
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15ClieRegNafinMivilIni.data.jsp',
			params: Ext.apply(forma.getForm().getValues(),{					
				informacion:'Generar_Arch'
			}),
			callback: procesarDescargaArchivos
		});
	}
	var procesarArchivosXLSDetalle = function(boton) {
		var  fp =   Ext.ComponentQuery.query('#formaPrincipal')[0];
		
		boton.setDisabled(true);
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '15ClieRegNafinMivilIni.data.jsp',
			params: Ext.apply({					
				informacion:'Generar_Arch_Detalle',
				anioDetalle:    fp.query('#anioDetalle')[0].getValue(),
				numMes:    fp.query('#numMes')[0].getValue(),
				mesDetalle:    fp.query('#mesDetalle')[0].getValue()
			}),
			callback: procesarDescargaArchivosDetalle
		});
	}
	
	var procesarConsultaData = function(store, arrRegistros, success, opts){
		Ext.ComponentQuery.query('#btnGenerar')[0].setIconCls('icoGenerar');
		Ext.ComponentQuery.query('#btnGenerar')[0].setDisabled(false);
		
		var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
		var gridConsultaDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
		if(!gridConsulta.isVisible()){
			gridConsulta.show();
			gridConsultaDetalle.hide();
		}
		if (arrRegistros != null){
			if(store.getTotalCount() > 0){
				gridConsultaDetalle.hide();
				Ext.ComponentQuery.query('#btnDescArch')[0].setDisabled(false);//
				gridConsulta.getView().setAutoScroll(true);
			} else{
				Ext.ComponentQuery.query('#btnDescArch')[0].setDisabled(true);
				gridConsulta.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridConsulta.getView().setAutoScroll(false);
				
			}
		}
	}
	var procesarConsultaDataDetalle = function(store, arrRegistros, success, opts){
		
		if (arrRegistros != null){
			var anioDetalle ="";
			var mesDetalle ="";
			var jsonData = store.proxy.reader.jsonData;
			anioDetalle = jsonData.anioDetalle;
			mesDetalle = jsonData.mesDetalle;
			
			var gridConsultaDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
			var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
			if(!gridConsultaDetalle.isVisible()){
				Ext.ComponentQuery.query('#mes_anio')[0].setText(mesDetalle+' '+anioDetalle);
				gridConsultaDetalle.show();
				if(gridConsulta.isVisible()){
					gridConsulta.hide();
				}
			}
			if(store.getTotalCount() > 0){
				Ext.ComponentQuery.query('#btnArchivoXLSDetalle')[0].setDisabled(false);
				gridConsultaDetalle.getView().setAutoScroll(true);
			} else{
				Ext.ComponentQuery.query('#btnArchivoXLSDetalle')[0].setDisabled(true);
				gridConsultaDetalle.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridConsultaDetalle.getView().setAutoScroll(false);
				
			}
		}
	}
	Ext.define('ListaRegistros',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'ANIO'     },
				{name: 'MES'     },
				{name: 'AFILIADAS'     },
				{name: 'HABILITADAS'     },
				{name: 'ALTAS'     },
				{name: 'ACTIVAS'     },
				{name: 'BAJAS'     },
				{name: 'NUMMES'     }
			]
	});
	Ext.define('ListaRegistrosDetalle',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'NO_NAFIN'     },
				{name: 'NO_PYME'     },
				{name: 'RAZON_SOCIAL'     },
				{name: 'EMAIL'     },
				{name: 'NO_CELULAR'     },
				{name: 'FECHA_REGISTRO'     },
				{name: 'FECHA_DESACTIVACION'     },
				{name: 'FECHA_ACTIVACION'     },
				{name: 'EPOS_AFILIADAS'     },
				{name: 'EPOS_NO_AFILIADAS'     },
				{name: 'ESTADO'     },
				{name: 'ESTATUS'     },
				{name: 'TIPOEPO'     }
			]
	});
	var consultaData = Ext.create('Ext.data.Store',{
		storeId: 'consultaData',
		model: 'ListaRegistros',
		pageSize: 15,
		proxy: {
			type: 'ajax',
			url:  '15ClieRegNafinMivilIni.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_info'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			beforeload: function(){
				var  forma =   Ext.ComponentQuery.query('#formaPrincipal')[0];
							Ext.apply(this.proxy.extraParams,forma.getForm().getValues());
			},
			load: procesarConsultaData
		}
	});
	var consultaDataDetalle = Ext.create('Ext.data.Store',{
		storeId: 'consultaDataDetalle',
		model: 'ListaRegistrosDetalle',
		pageSize: 15,
		proxy: {
			type: 'ajax',
			url:  '15ClieRegNafinMivilIni.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_info_detalle'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		
		listeners: {
			beforeload: function(){
				var  forma =   Ext.ComponentQuery.query('#formaPrincipal')[0];
							Ext.apply(this.proxy.extraParams,forma.getForm().getValues());
			},
			load: procesarConsultaDataDetalle
		}
	});
	Ext.define('ModelMes',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	Ext.define('ModelMesF',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	Ext.define('ModelAnio',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	Ext.define('ModelAnioF',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);
	

	var procesarMes = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#claveMesI')[0].setValue('0');
		}
	}
	var procesarMesFin = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#claveMesF')[0].setValue('0');
		}
	}
	
	var procesarAnioIni = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#claveAnioI')[0].setValue(arrRegistros[0]);
		}
	}
	var procesarAnioFin = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.ComponentQuery.query('#claveAnioF')[0].setValue(arrRegistros[0]);
		}
	}
	
	
	var elementosForma = [
		{
			xtype: 'hidden',
			name: 'tipo_reporte',
			itemId: 'tipo_reporte',
			value:'clientes_registrados'
		},
		{
			xtype: 'hidden',
			name: 'claveMesC',
			itemId: 'claveMesC',
			value:0
		},
		{
			xtype: 'hidden',
			name: 'claveAnioC',
			itemId: 'claveAnioC',
			value:2008
		},
		{
			xtype: 'hidden',
			name: 'anioDetalle',
			itemId: 'anioDetalle',
			value:2008
		},
		{
			xtype: 'hidden',
			name: 'numMes',
			itemId: 'numMes',
			value:2008
		},
		{
			xtype: 'hidden',
			name: 'mesDetalle',
			itemId: 'mesDetalle',
			value:2008
		},
		{
			xtype:'panel',
			frame: true,
			width:  800,				 
			border	: false,
			cls: 'panel-sin-borde',
			layout:'table',
			items:[
				{
					xtype: 'combo',
					itemId:  'claveMesI',
					fieldLabel: 'Fecha de registro desde Mes',
					name:  'claveMesI',
					hiddenName: 'claveMesI',
					forceSelection:true,
					allowBlank: false,
					emptyText: 'Seleccione...',
					queryMode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					typeAhead:		true,
					labelWidth: 160,
					triggerAction:	'all',
					width:  300,
					store: Ext.create('Ext.data.Store', {
						model:'ModelMes',
						autoLoad: true,
						proxy: {
							type: 'ajax',
							url: '15ClieRegNafinMivilIni.data.jsp',
							reader: {
								type: 'json',
								root: 'registros'
							},
							extraParams: {
								informacion: 'Consulta_Mes'
							},
								totalProperty:  'total'
							},
							listeners: {
								load : procesarMes,
								exception: NE.util.mostrarProxyAjaxError
							}
					})
				},
				{
					xtype: 'displayfield',
					value: ' ',
					width:  10
				},
				{
					xtype: 'combo',
					itemId:  'claveAnioI',
					name:  'claveAnioI',
					fieldLabel: 'A�o',
					hiddenName: 'claveAnioI',
					forceSelection:true,
					allowBlank: false,
					labelWidth: 30,
					emptyText: 'Seleccione...',
					queryMode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					typeAhead:		true,
					triggerAction:	'all',
					width:  130,
					store: Ext.create('Ext.data.Store', {
						model:'ModelAnio',
						autoLoad: true,
						proxy: {
							type: 'ajax',
							url: '15ClieRegNafinMivilIni.data.jsp',
							reader: {
								type: 'json',
								root: 'registros'
							},
							extraParams: {
								informacion: 'Consulta_Anio'
							},
								totalProperty:  'total'
							},
							listeners: {
								load : procesarAnioIni,
								exception: NE.util.mostrarProxyAjaxError
							}
					})
				},
				{
					xtype: 'displayfield',
					value: '<center>  hasta  </center>',
					width:  50
				},
				{
					xtype: 'combo',
					itemId:  'claveMesF',
					name:  'claveMesF',
					fieldLabel: 'Mes',
					hiddenName: 'claveMesF',
					forceSelection:true,
					allowBlank: false,
					labelWidth: 30,
					emptyText: 'Seleccione...',
					queryMode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					typeAhead:		true,
					triggerAction:	'all',
					width:  130,
					store: Ext.create('Ext.data.Store', {
						model:'ModelMesF',
						autoLoad: true,
						proxy: {
							type: 'ajax',
							url: '15ClieRegNafinMivilIni.data.jsp',
							reader: {
								type: 'json',
								root: 'registros'
							},
							extraParams: {
								informacion: 'Consulta_MesF'
							},
								totalProperty:  'total'
							},
							listeners: {
								load : procesarMesFin,
								exception: NE.util.mostrarProxyAjaxError
							}
					})
				},
				{
					xtype: 'displayfield',
					value: ' ',
					width:  10
				},
				{
					xtype: 'combo',
					itemId:  'claveAnioF',
					name:  'claveAnioF',
					hiddenName: 'claveAnioF',
					fieldLabel: 'A�o',
					forceSelection:true,
					allowBlank: false,
					labelWidth: 30,
					emptyText: 'Seleccione...',
					queryMode:				'local',
					displayField:	'descripcion',
					valueField:		'clave',
					typeAhead:		true,
					triggerAction:	'all',
					width:  130,
					store: Ext.create('Ext.data.Store', {
						model:'ModelAnioF',
						autoLoad: true,
						proxy: {
							type: 'ajax',
							url: '15ClieRegNafinMivilIni.data.jsp',
							reader: {
								type: 'json',
								root: 'registros'
							},
							extraParams: {
								informacion: 'Consulta_AnioF'
							},
								totalProperty:  'total'
							},
							listeners: {
								load : procesarAnioFin,
								exception: NE.util.mostrarProxyAjaxError
							}
					})
				}
			]
		}
	]
	var formaPrincipal = new Ext.form.FormPanel({
		width:                     800,
		height :'auto',
		itemId:                    'formaPrincipal',
		title:                     'Clientes Registrados a Nafinsa M�vil',
		bodyPadding:               '0 0 0 0',
		style:                     'margin: 10px auto 10px auto;',
		frame:                     true,
		border:                    true,
		fieldDefaults: {
			msgTarget:              'side',
			labelWidth:             70
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text:                   'Regresar',
				itemId:                 'btnRegresar',
				iconCls:                'icoRegresar',
				handler: function(boton, evento) {
					window.location.href='15ClieRegNafinMivilIni.jsp';
				}
			},
			{
				text:                   '<center>Generar</center>',
				itemId:                 'btnGenerar',
				iconCls:                'icoGenerar',
				handler:                generar
			},
			{
				text:                   '<center>Limpiar</center>',
				itemId:                 'btnLimpiar',
				iconCls:                'icoLimpiar',
				handler:                limpiar
			}
		]
	});
	
	var  grid = Ext.create('Ext.grid.Panel',{
		itemId: 'grid',
		store:Ext.data.StoreManager.lookup('consultaData'),
	   title: 'Consulta',
		xtype:           'grouped-header-grid',
		style: 'margin: 10px auto 0px auto;',
     	hidden:  true,
		frame: true,
		height: 400,		
		width: 815,
		columns: [
			{
				text: 'A�o/Mes',
				menuDisabled: false,
				columns: [
					{
						dataIndex: 'ANIO',
						sortable: true,
						width: 60,
						align: 'center',
						resizable: true
					},				  
					{
						dataIndex: 'MES',
						sortable: true,
						width: 100,
						align: 'center',
						resizable: true
					}
				]
			},{
				text: 'PyMES en Cadenas',
				menuDisabled: false,
				columns: [
					{
						header: 'Afiliadas',
						dataIndex: 'AFILIADAS',
						sortable: true,
						width: 100,
						align: 'center',
						resizable: true
					},				  
					{
						header: 'Afiliadas Habilitadas',
						dataIndex: 'HABILITADAS',
						sortable: true,
						width: 150,
						align: 'center',
						resizable: true
					}
				]
			},{
				text: 'PyMES en Nafinsa M�vil',
				menuDisabled: false,
				columns: [
					{
						header: 'Altas',
						dataIndex: 'ALTAS',
						sortable: true,
						width: 100,
						align: 'center',
						resizable: true
					},				  
					{
						header: 'Activas',
						dataIndex: 'ACTIVAS',
						sortable: true,
						width: 100,
						align: 'center',
						resizable: true
					},
					{
						header: 'Bajas',
						dataIndex: 'BAJAS',
						sortable: true,
						width: 100,
						align: 'center',
						resizable: true
					}
				]
			},
			{
				width:         75,
				xtype:         'actioncolumn',
				header:        'Ver',
				align:         'center',
				sortable:      false,
				resizable:     false,
				menuDisabled:  true,
				items: [
					{
						getClass:   function(value,metadata,record,rowIndex,colIndex,store){
								this.items[0].tooltip = 'Ver';
								return 'icoBuscar';	
						},
						handler: function(grid, rowIndex, colIndex, item, event) {
									detallePersonas(grid, rowIndex, colIndex, item, event);
						}
					}
				]
			}
		],
		bbar: [
			Ext.create('Ext.PagingToolbar',{
				store:Ext.data.StoreManager.lookup('consultaData'),
				itemId:     'barraPaginacion',
				displayMsg: '{0} - {1} de {2}',
				emptyMsg:   'No se encontraron registros',
				displayInfo:true
				
			}),
			'->','-',
			{
				text: 'Descarga Archivo',
				id: 'btnDescArch',	
				iconCls: 'icoBotonXLS',
				handler: procesarArchivosXLS
				
			}
		] 				
	});
	var  gridDetalle = Ext.create('Ext.grid.Panel',{
		itemId: 'gridDetalle',
		store:            Ext.data.StoreManager.lookup('consultaDataDetalle'),
	   title: 'Clientes Registrados a Nafinsa M�vil -Detalle ',
		xtype:           'grouped-header-grid',
		style: 'margin: 10px auto 0px auto;',
     	hidden:  true,
		frame: true,
		height: 400,		
		width: 949,
		columns: [
			{
				text: 'Septiembre 2019',
				menuDisabled: false,
				align: 'center',
				itemId: 'mes_anio',
				columns: [
					{
						header: 'No. N@E',
						sortable: true,
						tooltip:   'No. N@E',
						width: 60,
						dataIndex: 'NO_NAFIN',
						align: 'center',
						resizable: true
					},				  
					{
						header: 'No. PyME',
						sortable: true,
						tooltip:   'No PyME',
						dataIndex: 'NO_PYME',
						width: 100,
						align: 'center',
						resizable: true
					},
					{
						header: 'Raz�n Social',
						sortable: true,
						tooltip:   'Raz�n Social',
						width: 100,
						dataIndex: 'RAZON_SOCIAL',
						align: 'center',
						resizable: true
					},
					{
						header: 'Email',
						sortable: true,
						tooltip:   'Email',
						width: 100,
						align: 'center',
						dataIndex: 'EMAIL',
						resizable: true
					},
					{
						header: 'No. Celular',
						sortable: true,
						tooltip:   'No celular',
						width: 100,
						align: 'center',
						dataIndex: 'NO_CELULAR',
						resizable: true
					},
					{
						header: 'Fecha de Registro',
						sortable: true,
						tooltip:   'Fecha de Registro',
						width: 100,
						dataIndex: 'FECHA_REGISTRO',
						align: 'center',
						resizable: true
					},
					{
						header: 'Fecha de �ltima desactivaci�n',
						sortable: true,
						tooltip:   'Fecha de �ltima desactivaci�n',
						dataIndex: 'FECHA_DESACTIVACION',
						width: 100,
						align: 'center',
						resizable: true
					},
					{
						header: 'Fecha de �ltima activaci�n',
						sortable: true,
						width: 100,
						tooltip:   'Fecha de �ltima activaci�n',
						align: 'center',
						dataIndex: 'FECHA_ACTIVACION',
						resizable: true
					},
					{
						header: 'EPOS Afiliadas',
						sortable: true,
						width: 100,
						tooltip:   'EPOS Afiliadas',
						align: 'center',
						dataIndex: 'EPOS_AFILIADAS',
						resizable: true
					},
					{
						header: 'EPOS por Afiliar',
						sortable: true,
						width: 100,
						tooltip:   'EPOS por Afiliar',
						dataIndex: 'EPOS_NO_AFILIADAS',
						align: 'center',
						resizable: true
					},
					{
						header: 'Tipo de EPO',
						sortable: true,
						tooltip:   'Tipo de EPO',
						dataIndex: 'TIPOEPO',
						width: 100,
						align: 'center',
						resizable: true
					},
					{
						header: 'Estado',
						sortable: true,
						tooltip:   'Estado',
						dataIndex: 'ESTADO',
						width: 100,
						align: 'center',
						resizable: true
					},
					{
						header: 'Estatus en Nafinsa M�vil',
						sortable: true,
						tooltip:   'Estatus en Nafinsa M�vil',
						width: 100,
						dataIndex: 'ESTATUS',
						align: 'center',
						resizable: true
					}
				]
			}
		],
		bbar: [
			Ext.create('Ext.PagingToolbar',{
				store:Ext.data.StoreManager.lookup('consultaDataDetalle'),
				itemId:     'barraPaginacionD',
				displayMsg: '{0} - {1} de {2}',
				emptyMsg:   'No se encontraron registros',
				displayInfo:true
				
			}),
			'->','-',
			{
				text: 'Regresar',
				id: 'btnRegresar',	
				iconCls: 'icoRegresar',
				handler: function(){
					var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
					var gridConsultaDetalle = Ext.ComponentQuery.query('#gridDetalle')[0];
					gridConsulta.show();
					gridConsultaDetalle.hide();
				}
				
			},
			{
				text: 'Descarga Archivo',
				id: 'btnArchivoXLSDetalle',	
				iconCls: 'icoBotonXLS',
				handler: procesarArchivosXLSDetalle
				
			}
			
		] 				
	});
	var main = Ext.create('Ext.container.Container',{
		itemId:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		width:      949,
		items: [
			formaPrincipal,
			NE.util.getEspaciador(10),
			grid,
			gridDetalle
		]
	});
	
});