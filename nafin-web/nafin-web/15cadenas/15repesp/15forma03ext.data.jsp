<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.SimpleDateFormat,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.seguridad.*,
		com.netro.seguridadbean.SeguException,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.cadenas.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.Usuario,
		com.netro.seguridadbean.*,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null ? "": (String)request.getParameter("informacion");
String operacion   = request.getParameter("operacion")   == null ? "": (String)request.getParameter("operacion");
String mesIni      = request.getParameter("mes_ini")     == null ? "": (String)request.getParameter("mes_ini");
String mesFin      = request.getParameter("mes_fin")     == null ? "": (String)request.getParameter("mes_fin");
String anioIni     = request.getParameter("anio_ini")    == null ? "": (String)request.getParameter("anio_ini");
String anioFin     = request.getParameter("anio_fin")    == null ? "": (String)request.getParameter("anio_fin");
String tipoDocto   = request.getParameter("tipo_docto")  == null ? "": (String)request.getParameter("tipo_docto");
String datosGrid1  = request.getParameter("datos_grid_1")== null ? "": (String)request.getParameter("datos_grid_1");
String datosGrid2  = request.getParameter("datos_grid_2")== null ? "": (String)request.getParameter("datos_grid_2");

HashMap    datos      = new HashMap();
JSONArray  registros  = new JSONArray();
JSONObject resultado  = new JSONObject();

String mensaje        = "";
String consulta       = "";
String nombreArchivo  = "";
String infoRegresar   = "";
boolean success       = true;

int start = 0;
int limit = 0;

log.info("informacion: <<<<<"    + informacion    + ">>>>>");
log.info("mesIni: <<<<<"    + mesIni    + ">>>>>");
log.info("mesFin: <<<<<"    + mesFin    + ">>>>>");
log.info("anioIni: <<<<<"    + anioIni    + ">>>>>");
log.info("anioFin: <<<<<"    + anioFin    + ">>>>>");
log.info("tipoDocto: <<<<<"    + tipoDocto    + ">>>>>");

if(informacion.equals("CATALOGO_MES")){

	datos = new HashMap(); datos.put("clave", "0");  datos.put("descripcion", "Enero"     ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "2");  datos.put("descripcion", "Febrero"   ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "2");  datos.put("descripcion", "Marzo"     ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "3");  datos.put("descripcion", "Abril"     ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "4");  datos.put("descripcion", "Mayo"      ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "5");  datos.put("descripcion", "Junio"     ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "6");  datos.put("descripcion", "Julio"     ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "7");  datos.put("descripcion", "Agosto"    ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "8");  datos.put("descripcion", "Septiembre"); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "9");  datos.put("descripcion", "Octubre"   ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "10"); datos.put("descripcion", "Noviembre" ); registros.add(datos);
	datos = new HashMap(); datos.put("clave", "11"); datos.put("descripcion", "Diciembre" ); registros.add(datos);
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else if(informacion.equals("CATALOGO_ANIO")){

	SimpleDateFormat formato = new SimpleDateFormat("yyyy");
	Calendar cal = Calendar.getInstance();	
	int iAnioActual = cal.get(Calendar.YEAR);	

	for(int anio = 2008; anio <= iAnioActual; anio++){
		datos = new HashMap();
		datos.put("clave",       Integer.toString(anio));
		datos.put("descripcion", Integer.toString(anio));
		registros.add(datos);
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else if(informacion.equals("CONSULTA_NEGOCIABLE") || informacion.equals("GENERA_REPORTE")){

	InteligenciaComercial inteligenciaComercial = ServiceLocator.getInstance().lookup("InteligenciaComercialEJB", InteligenciaComercial.class);

	List reporte = new ArrayList();
	//System.out.println("claveAnioI: " + anioIni + ", claveAnioF: " + anioFin + ", claveMesI: " + mesIni + ", claveMesF: " + mesFin + ", tipo_docto: " + tipoDocto);
	if("N".equals(tipoDocto) || "V".equals(tipoDocto)){
		reporte = inteligenciaComercial.reporteNegociablesNafinsaMovil(tipoDocto, anioIni, anioFin, mesIni, mesFin);
		for(int i = 0; i < reporte.size(); i++){
			List registroReporte = (List)reporte.get(i);
			datos = new HashMap();
			datos.put("ANIO_NOTIFICACION",  registroReporte.get(0));
			datos.put("MES_NOTIFICACION",   registroReporte.get(1));
			datos.put("PYMES",              registroReporte.get(2));
			datos.put("DOCTOS_NOTIFICADOS", registroReporte.get(3));
			datos.put("MONTO_NOTIFICADO",   registroReporte.get(4));
			datos.put("NUM_MES",            registroReporte.get(5));
			registros.add(datos);
		}
	} else{
		reporte = inteligenciaComercial.reporteCostoEIngresoNafinsaMovil(anioIni, anioFin, mesIni, mesFin);
		for(int i = 0; i < reporte.size(); i++){
			List registroReporte = (List)reporte.get(i);
			datos = new HashMap();
			datos.put("0", registroReporte.get(0));
			datos.put("1", registroReporte.get(1));
			datos.put("2", registroReporte.get(2));
			datos.put("3", registroReporte.get(3));
			datos.put("4", registroReporte.get(4));
			datos.put("5", registroReporte.get(5));
			datos.put("6", registroReporte.get(6));
			datos.put("7", registroReporte.get(7));
			datos.put("8", registroReporte.get(8));
			datos.put("9", registroReporte.get(9));
			datos.put("10", registroReporte.get(10));
			datos.put("11", registroReporte.get(11));
			datos.put("12", registroReporte.get(12));
			datos.put("13", registroReporte.get(13));
			datos.put("14", registroReporte.get(14));
			datos.put("15", registroReporte.get(15));
			registros.add(datos);
		}
	}

	if(informacion.equals("CONSULTA_NEGOCIABLE")){
		consulta = "{\"success\": "+new Boolean(success)+", \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString() +"}";
		resultado.put("success", new Boolean(success));
		resultado = JSONObject.fromObject(consulta);
	} else if(informacion.equals("GENERA_REPORTE")){

		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();

		if("N".equals(tipoDocto) || "V".equals(tipoDocto)){
			contenidoArchivo.append("Año / Mes,"                    );
			contenidoArchivo.append(" ,"                            );
			contenidoArchivo.append("No. de PyMES únicas,"          );
			contenidoArchivo.append("No. de Documentos Notificados,");
			contenidoArchivo.append("Monto Notificado\n"            );
			for(int i = 0; i < reporte.size(); i++){
				List registroReporte = (List)reporte.get(i);
				contenidoArchivo.append(                             ((String) registroReporte.get(0)).replaceAll(",","")  + "," );
				contenidoArchivo.append(                             ((String) registroReporte.get(1)).replaceAll(",","")  + "," );
				contenidoArchivo.append(                             ((String) registroReporte.get(2)).replaceAll(",","")  + "," );
				contenidoArchivo.append(                             ((String) registroReporte.get(3)).replaceAll(",","")  + "," );
				contenidoArchivo.append("$" + Comunes.formatoDecimal((String) registroReporte.get(4),2).replaceAll(",","") + "\n");
			}
		} else{
			contenidoArchivo.append(", , ,"                            );
			contenidoArchivo.append("Notificaciones Operadas,  , , , ,");
			contenidoArchivo.append("Notificaciones Negociables, , , ,");
			contenidoArchivo.append("Notificaciones Vencidas\n"        );

			contenidoArchivo.append("Año / Mes, ,"                                                                                                   );
			contenidoArchivo.append("No. de PyMES únicas,"                                                                                           );
			contenidoArchivo.append("No. de Documentos, Monto Operado, No. de Notificaciones, Costo de Notificaciones, Interés por Doctos. Operados,");
			contenidoArchivo.append("No. de Documentos, Monto Negociable, No. de Notificaciones, Costo de Notificaciones,"                           );
			contenidoArchivo.append("No. de Documentos, Monto Vencido, No. de Notificaciones, Costo de Notificaciones\n"                             );

			for(int i = 0; i < reporte.size(); i++){
				List registroReporte = (List)reporte.get(i);
				contenidoArchivo.append(                            ((String) registroReporte.get(0)).replaceAll(",","")    + "," );
				contenidoArchivo.append(                            ((String) registroReporte.get(1)).replaceAll(",","")    + "," );
				contenidoArchivo.append(                            ((String) registroReporte.get(2)).replaceAll(",","")    + "," );
				contenidoArchivo.append(                            ((String) registroReporte.get(3)).replaceAll(",","")    + "," );
				contenidoArchivo.append("$" + Comunes.formatoDecimal((String) registroReporte.get(4),2).replaceAll(",","")  + "," );
				contenidoArchivo.append(                            ((String) registroReporte.get(5)).replaceAll(",","")    + "," );
				contenidoArchivo.append("$" + Comunes.formatoDecimal((String) registroReporte.get(6),2).replaceAll(",","")  + "," );
				contenidoArchivo.append("$" + Comunes.formatoDecimal((String) registroReporte.get(7),2).replaceAll(",","")  + "," );
				contenidoArchivo.append(                            ((String) registroReporte.get(8)).replaceAll(",","")    + "," );
				contenidoArchivo.append("$" + Comunes.formatoDecimal((String) registroReporte.get(9),2).replaceAll(",","")  + "," );
				contenidoArchivo.append(                            ((String) registroReporte.get(10)).replaceAll(",","")   + "," );
				contenidoArchivo.append("$" + Comunes.formatoDecimal((String) registroReporte.get(11),2).replaceAll(",","") + "," );
				contenidoArchivo.append(                            ((String) registroReporte.get(12)).replaceAll(",","")   + "," );
				contenidoArchivo.append("$" + Comunes.formatoDecimal((String) registroReporte.get(13),2).replaceAll(",","") + "," );
				contenidoArchivo.append(                            ((String) registroReporte.get(14)).replaceAll(",","")   + "," );
				contenidoArchivo.append("$" + Comunes.formatoDecimal((String) registroReporte.get(15),2).replaceAll(",","") + "\n");
			}
		}
		if(archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
			nombreArchivo = archivo.nombre;
		}
		resultado.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);

	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("VER_DETALLE") || informacion.equals("VER_DETALLE_TOTALES") || informacion.equals("VER_DETALLE_ARCHIVO")){

	if("ENERO".equals(mesIni))           mesIni = "01";
	else if("FEBRERO".equals(mesIni))    mesIni = "02";
	else if("MARZO".equals(mesIni))      mesIni = "03";
	else if("ABRIL".equals(mesIni))      mesIni = "04";
	else if("MAYO".equals(mesIni))       mesIni = "05";
	else if("JUNIO".equals(mesIni))      mesIni = "06";
	else if("JULIO".equals(mesIni))      mesIni = "07";
	else if("AGOSTO".equals(mesIni))     mesIni = "08";
	else if("SEPTIEMBRE".equals(mesIni)) mesIni = "09";
	else if("OCTUBRE".equals(mesIni))    mesIni = "10";
	else if("NOVIEMBRE".equals(mesIni))  mesIni = "11";
	else if("DICIEMBRE".equals(mesIni))  mesIni = "12";

	ReporteNafinsaMovilDetalle paginacion = new ReporteNafinsaMovilDetalle();
	paginacion.setTipoDocto(tipoDocto);
	paginacion.setAnioDetalle(anioIni);
	paginacion.setNumMes(mesIni);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginacion);

	if(informacion.equals("VER_DETALLE") || informacion.equals("VER_DETALLE_TOTALES")){

		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		if(informacion.equals("VER_DETALLE")){

			try {
				if (operacion.equals("generar")) { //Nueva consulta
					queryHelper.executePKQuery(request);
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);
			} catch(Exception e) {
				success = false;
				throw new AppException("Error en la paginacion", e);
			}
			resultado = JSONObject.fromObject(consulta);

		} else if(informacion.equals("VER_DETALLE_TOTALES")){
			try{
				consulta = queryHelper.getJSONResultCount(request);
				resultado = JSONObject.fromObject(consulta);
			} catch(Exception e){
				success = false;
				throw new AppException("Error al obtener los totales", e);
			}
		}

	} else if(informacion.equals("VER_DETALLE_ARCHIVO")){

		HashMap mapaGrid1 = new HashMap();
		List listaGrid1 = new ArrayList();
		JSONArray arrayRegistros = JSONArray.fromObject(datosGrid1);
		for(int i = 0; i < arrayRegistros.size(); i++){
			JSONObject auxiliar = arrayRegistros.getJSONObject(i);
			mapaGrid1 = new HashMap();
			mapaGrid1.put("NO_PYMES",           auxiliar.getString("NO_PYMES")           );
			mapaGrid1.put("DOCTOS_NOTIFICADOS", auxiliar.getString("DOCTOS_NOTIFICADOS") );
			mapaGrid1.put("MONTO_NOTIFICADO",   auxiliar.getString("MONTO_NOTIFICADO")   );
			mapaGrid1.put("DOCTOS_OPERADOS",    auxiliar.getString("DOCTOS_OPERADOS")    );
			mapaGrid1.put("MONTO_OPERADO",      auxiliar.getString("MONTO_OPERADO")      );
			mapaGrid1.put("DOCTOS_VENCIDOS",    auxiliar.getString("DOCTOS_VENCIDOS")    );
			mapaGrid1.put("MONTO_VENCIDO",      auxiliar.getString("MONTO_VENCIDO")      );
			listaGrid1.add(mapaGrid1);
		}
		paginacion.setGridTotales(listaGrid1);
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			throw new AppException("Error al generar el archivo CSV", e);
		}

	}

	infoRegresar = resultado.toString();

} else if(informacion.equals("VER_DETALLE_1") || informacion.equals("VER_DETALLE_TOTALES_1") || informacion.equals("VER_DETALLE_TOTALES_2") || informacion.equals("VER_DETALLE_ARCHIVO_1")){

	if("ENERO".equals(mesIni))           mesIni = "01";
	else if("FEBRERO".equals(mesIni))    mesIni = "02";
	else if("MARZO".equals(mesIni))      mesIni = "03";
	else if("ABRIL".equals(mesIni))      mesIni = "04";
	else if("MAYO".equals(mesIni))       mesIni = "05";
	else if("JUNIO".equals(mesIni))      mesIni = "06";
	else if("JULIO".equals(mesIni))      mesIni = "07";
	else if("AGOSTO".equals(mesIni))     mesIni = "08";
	else if("SEPTIEMBRE".equals(mesIni)) mesIni = "09";
	else if("OCTUBRE".equals(mesIni))    mesIni = "10";
	else if("NOVIEMBRE".equals(mesIni))  mesIni = "11";
	else if("DICIEMBRE".equals(mesIni))  mesIni = "12";

	ReporteCostoEIngresoDetalle paginacion = new ReporteCostoEIngresoDetalle();
	paginacion.setAnioDetalle(anioIni);
	paginacion.setNumMes(mesIni);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginacion);

	if(informacion.equals("VER_DETALLE_1") || informacion.equals("VER_DETALLE_TOTALES_1") || informacion.equals("VER_DETALLE_TOTALES_2")){

		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		if(informacion.equals("VER_DETALLE_1")){

			try {
				if (operacion.equals("generar")) { //Nueva consulta
					queryHelper.executePKQuery(request);
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);
			} catch(Exception e) {
				success = false;
				throw new AppException("Error en la paginacion", e);
			}
			resultado = JSONObject.fromObject(consulta);

		} else if(informacion.equals("VER_DETALLE_TOTALES_1")){

			try{
				consulta = queryHelper.getJSONResultCount(request);
				resultado = JSONObject.fromObject(consulta);
			} catch(Exception e){
				success = false;
				throw new AppException("Error al obtener los totales", e);
			}

		} else if(informacion.equals("VER_DETALLE_TOTALES_2")){

			try{
				List listaTotales = paginacion.getTotales();
				registros = new JSONArray();
				datos = new HashMap();
				datos.put("DESCRIPCION", "Negociables"              );
				datos.put("DOCTOS_NE",   (String)listaTotales.get(0));
				datos.put("MONTO_NE",    (String)listaTotales.get(1));
				registros.add(datos);
				datos = new HashMap();
				datos.put("DESCRIPCION", "Operados"                 );
				datos.put("DOCTOS_NE",   (String)listaTotales.get(2));
				datos.put("MONTO_NE",    (String)listaTotales.get(3));
				registros.add(datos);
				datos = new HashMap();
				datos.put("DESCRIPCION", "Vencidos sin Operar"      );
				datos.put("DOCTOS_NE",   (String)listaTotales.get(4));
				datos.put("MONTO_NE",    (String)listaTotales.get(5));
				registros.add(datos);

				consulta = "{\"success\": "+new Boolean(success)+", \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString() +"}";
				resultado = JSONObject.fromObject(consulta);
			} catch(Exception e){
				success = false;
				throw new AppException("Error al obtener los totales", e);
			}

		}
	} else if(informacion.equals("VER_DETALLE_ARCHIVO_1")){

		HashMap mapaGrid1 = new HashMap();
		List listaGrid1 = new ArrayList();
		JSONArray arrayRegistros = JSONArray.fromObject(datosGrid1);

		if(arrayRegistros.size() > 0){
			for(int i = 0; i < arrayRegistros.size(); i++){
				JSONObject auxiliar = arrayRegistros.getJSONObject(i);
				mapaGrid1 = new HashMap();
				mapaGrid1.put("DOCTOS_NOTIFICADOS",   auxiliar.getString("DOCTOS_NOTIFICADOS")   );
				mapaGrid1.put("MONTO_NOTIFICADO",     auxiliar.getString("MONTO_NOTIFICADO")     );
				mapaGrid1.put("NUM_NOTIFICACIONES",   auxiliar.getString("NUM_NOTIFICACIONES")   );
				mapaGrid1.put("COSTO_NOTIFICACIONES", auxiliar.getString("COSTO_NOTIFICACIONES") );
				mapaGrid1.put("INTERESES_OPERADOS",   auxiliar.getString("INTERESES_OPERADOS")   );
				listaGrid1.add(mapaGrid1);
			}
		}
		paginacion.setGridTotales1(listaGrid1);
		mapaGrid1 = new HashMap();
		listaGrid1 = new ArrayList();
		arrayRegistros = JSONArray.fromObject(datosGrid2);
		if(arrayRegistros.size() > 0){
			for(int i = 0; i < arrayRegistros.size(); i++){
				JSONObject auxiliar = arrayRegistros.getJSONObject(i);
				mapaGrid1 = new HashMap();
				mapaGrid1.put("DESCRIPCION",  auxiliar.getString("DESCRIPCION"));
				mapaGrid1.put("DOCTOS_NE",    auxiliar.getString("DOCTOS_NE")  );
				mapaGrid1.put("MONTO_NE",     auxiliar.getString("MONTO_NE")   );
				listaGrid1.add(mapaGrid1);
			}
		}
		paginacion.setGridTotales2(listaGrid1);

		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			throw new AppException("Error al generar el archivo CSV", e);
		}

	}

	infoRegresar = resultado.toString();

}
%>
<%=infoRegresar%>