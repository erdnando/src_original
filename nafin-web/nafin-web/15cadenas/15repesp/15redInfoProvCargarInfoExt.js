Ext.onReady(function() {

	var pnlSeleccionPrincipal = Ext.create('Ext.panel.Panel',{
		title: '<CENTER>Seleccione la base de datos a cargar información</CENTER>',
		frame: true,
		width: 500,
		style: {margin: '0 auto'},
		items:[
			{
				xtype: 'radiogroup',
				columns: 1,
				itemId: 'rgBAseDatos',
				style: {margin: '0 auto'},
				items: [
					{boxLabel: 'CADENAS', name: 'rgBD', inputValue: 'CADENAS'},
					{boxLabel: 'COMPRANET', name: 'rgBD', inputValue: 'COMPRANET'},
					{boxLabel: 'SIAG', name: 'rgBD', inputValue: 'SIAG'}
				]
			}
		],
		buttonAlign: 'center',
		buttons:[{
				xtype:'button',
				text: 'Aceptar',
				width: 60,
				handler: function(){
					console.log(Ext.ComponentQuery.query('#rgBAseDatos')[0].getValue().rgBD);
					window.location = '15redInfoProvCargarInfoExt.jsp?rgBD='+Ext.ComponentQuery.query('#rgBAseDatos')[0].getValue().rgBD;
				}
			}]
	});
	
	Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			pnlSeleccionPrincipal,
			NE.util.getEspaciador(20)
		]
	});

});