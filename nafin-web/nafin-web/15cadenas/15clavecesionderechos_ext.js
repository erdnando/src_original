Ext.onReady(function() {
var gralCorreo = '';
var hidiNoUsuario = Ext.getDom('hidiNoUsuario').value;
var hidStrNombreUsuario = Ext.getDom('hidStrNombreUsuario').value;
var csCveCeDer = 'S';



//-------------------------------FUNCIONES--------------------------------------

var funcHidePanels = function(){
	fpConfirmacion.hide();
	fpGeneraClaveCesion.hide();
	pnlGeneracionClaveCesion.hide();
}

var procesarSuccessValidaFolioSeguridad = function(opts, success, response) {
	pnl.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
		if(resp.validacionExitosa){
			csCveCeDer = resp.csCveCeDer;
			funcHidePanels();
			pnlGeneracionClaveCesion.show(); 
		}else{
			Ext.Msg.alert('Aviso', 'Folio de Seguridad Incorrecto');
		}
		
	}else{
		NE.util.mostrarConnError(response,opts);
	}
}

var procesarSuccessAltaClaveCesion = function(opts, success, response) {
	pnl.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
		Ext.Msg.alert('Aviso','Se registr� con �xito la Clave de Cesi�n de Derechos.', function(){
			window.location.href='15clavecesionderechos_ext.jsp';
		});
		
	}else{
		NE.util.mostrarConnError(response,opts);
	}
}

var procesarSuccessValidaExisteFolio  = function(opts, success, response) {
	pnl.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
		var opcion = resp.opcion;
		var folio = resp.folio;
		var texto1 = resp.existeCveCesion?'Revocar':'Generar';
		var texto2 = resp.existeCveCesion?'Revocaci�n':'Generaci�n';
		var pnlGeneraClaveCesion = Ext.getCmp('pnlGeneraClaveCesion');
		fpGeneraClaveCesion.setTitle(texto1+ ' Clave de Cesi�n de Derechos');
		Ext.getCmp('fpCorreoConfirm1').setText(resp.mail);
		if(folio==''){
			pnlGeneraClaveCesion.update('<table border=1 bordercolor="black" style="border-collapse:collapse;" ><tr><td style="background-color:#F2F2F2; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">' +
					  '<p style="text-align: justify;" >"Usted no cuenta con Folio de Seguridad para '+texto1+' la Clave de Cesi�n de Derechos, de clic en <u>Generar Folio</u> para que se env�e a su correo electr�nico y pueda continuar".<br>' +
					  '</p></td></tr></table>');
					  
			Ext.getCmp('btnGenerarFolio').setText('Generar Folio');
			Ext.getCmp('btnContinuar').hide();
		
		}else{
			pnlGeneraClaveCesion.update('<table border=1 bordercolor="black" style="border-collapse:collapse;" ><tr><td style="background-color:#F2F2F2; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">' +
					  '<p style="text-align: justify;" >"Ya solicit� la '+texto2+' de su Clave de Cesi�n de Derechos y se le envi� un folio de seguridad a su correo electr�nico. Para continuar favor de dar clic en el bot�n <u>Continuar</u>".<br><br> ' +
					  'Si el folio de seguridad no lo recuerda por favor de clic en el bot�n <u>Generar Nuevo Folio</u> para que se le env�e un nuevo folio de seguridad y pueda continuar</u> ' +
					  '</p></td></tr></table>');
			
			Ext.getCmp('btnGenerarFolio').setText('Generar Nuevo Folio');
			Ext.getCmp('btnContinuar').show();
		
		}
		
		if(resp.existeCveCesion){
			fpHistClaveCesion.show();
			consultaData.load();
		}
		
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

	

var handlerGenerarFolio = function(btn){
		pnl.el.mask('Enviando Folio...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '15clavecesionderechos_ext.data.jsp',
			params: {
				informacion: 'generaFolioSeguridad'
			},
			callback: procesarSuccessGenerarFolio
		});  
}

var procesarSuccessGenerarFolio = function(opts, success, response) {
	pnl.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
		
		if(resp.msgError != ''){
			Ext.Msg.alert('Fallo', resp.msgError);
		}else{
			Ext.Msg.alert('Aviso', 'El folio de seguridad se env�o exitosamente al Sujeto de Apoyo', function(){
				window.location.href='15clavecesionderechos_ext.jsp';
			});
		}
		
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}


var procesarSuccessGuardarHistorial  = function(opts, success, response) {
	pnl.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
		consultaData.load();
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}


var handlerContinuar = function(){
	funcHidePanels();
	fpConfirmacion.show();
	
}

//-------------------------------PANELES----------------------------------------
//PANEL PARA OPCION VIGENCIA



var fpGeneraClaveCesion = new Ext.FormPanel({
		name: 'fpGeneraClaveCesion',
		id: 'fpGeneraClaveCesion1',
		hidden: false,
		height: 'auto',
		width: 550,
		labelWidth: 200,
		title: 'Generar Clave de Cesi�n de Derechos',
		style: 'margin: 0 auto',
		frame: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{ xtype: 'label', fieldLabel: '<b>Login</b>', html: hidiNoUsuario, align: 'center', name: 'fpLogin' },
			{ xtype: 'label', allowBlank: false, fieldLabel: '<b>Nombre Usuario</b>', text: hidStrNombreUsuario, name: 'fpNombreUsuario' },
			{ xtype: 'label', id:'fpCorreoConfirm1', fieldLabel: '<b>Correo Electr�nico</b>', name: 'fpCorreoConfirm'},
			{
				xtype:'panel', id:'pnlGeneraClaveCesion', bodyBorder: false, style: 'margin: 0 auto', border: false,
				html:'',
				buttonAlign: 'center',
				buttons:[
					{
						text:'Generar Nuevo Folio',
						id: 'btnGenerarFolio',
						handler: handlerGenerarFolio
					},
					{
						text:'Continuar',
						id: 'btnContinuar',
						handler: handlerContinuar
					}
				]
			}
			
		]
	});

var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		var grid = Ext.getCmp('gridHistorial');
		Ext.getCmp('cmbSolicCDer1').setValue(jsonData.csSolicCveCDer);
		
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}	
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {				
				el.unmask();
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}				
		}			
	}

var catalogoParamCDer = new Ext.data.ArrayStore({
		id: 'catalogoParamCDerStore',
		fields : ['clave', 'descripcion'],
		data:[['S','Si'],['N','No'],['X','Ninguna']]
	});

var consultaData = new Ext.data.JsonStore({
	root : 'registros',
	url : '15clavecesionderechos_ext.data.jsp',
	baseParams: {
		informacion: 'ConsultaHistorial'
	},
	fields: [
		{name: 'CG_MOVIMIENTO'},
		{name: 'DF_CAMBIO'},
		{name: 'IC_USUARIO'}
	],
	totalProperty : 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
			}
		}
	}
});	
	
var fpHistClaveCesion = new Ext.FormPanel({
		name: 'fpHistClaveCesion',
		id: 'fpHistClaveCesion1',
		hidden: true,
		height: 'auto',
		width: 550,
		labelWidth: 350,
		title: 'Solicitar Clave de Cesi�n de Derechos',
		style: 'margin: 0 auto',
		frame: true,
		items: [
			{
				xtype: 'container',
				layout: {
					type: 'table',
					columns: 2
				},
				items:[{xtype:'container',layout:'form', width: 450,
						items:[{xtype: 'combo',
						name: 'cmbSolicCDer',
						id: 'cmbSolicCDer1',
						fieldLabel: 'Solicitar Clave de Cesi�n de Derechos en la operaci�n Telef�nica',
						mode: 'local', 
						displayField : 'descripcion',
						valueField : 'clave',
						hiddenName : 'cmbSolicCDer',
						emptyText: 'Seleccione...',					
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						width: 80,
						//minChars : 1,
						allowBlank: true,
						store : catalogoParamCDer
						}]
					},
					{xtype:'panel', layout:'form',
						items:[{xtype:'button',
						iconCls: 'icoGuardar',
						handler: function(){
							Ext.Ajax.request({
								url: '15clavecesionderechos_ext.data.jsp',
								params: {
									informacion: 'GuardarHistorial',
									csSolicCveCDer: Ext.getCmp('cmbSolicCDer1').getValue()
								},
								callback: procesarSuccessGuardarHistorial
							});
						}
						}]
					}
				]
			},
			{
				xtype:'fieldset',
				title: 'Ver Hist�tico de cambios',
				items:[{
				xtype: 'grid',
				id: 'gridHistorial',
				store: consultaData,
				hidden: false,
				margins: '20 0 0 0',
				style: 'margin: 0 auto',
				title: '',
				stripeRows: true,
				loadMask: true,
				height: 200,
				width: 500,		
				frame: false,
				columns: [
					{
						header: 'Movimiento',
						tooltip: 'Movimiento',
						dataIndex: 'CG_MOVIMIENTO',
						sortable: false,
						resizable: false	,
						width: 130,				
						align: 'left',
						renderer: function(value, metadata, registro, rowindex, colindex, store) {
							var valor = '';
							if(value == 'S'){
								valor = 'Si';
							}else if(value == 'N'){
								valor = 'No';
							}else if(value == 'X'){
								valor = 'Ninguna';
							}
							
							return valor;
						}
					},
					{
						header: 'Fecha y Hora',
						tooltip: 'Fecha y Hora',
						dataIndex: 'DF_CAMBIO',
						sortable: false,
						resizable: false	,
						width: 150,				
						align: 'left'
					},
					{
						header: 'Usuario',
						tooltip: 'Usuario',
						dataIndex: 'IC_USUARIO',
						sortable: false,
						resizable: false,
						width: 210,				
						align: 'left'
					}
				]
				}]
			}
			
		]
	});


var fpConfirmacion = new Ext.FormPanel({
		hidden: true,
		height: 'auto',
		width: 300,
		labelWidth: 130,
		title: 'Ingresar Folio de Seguridad',
		style: 'margin: 0 auto',
		frame: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: [
			{
				xtype: 'textfield',
				allowBlank: false,
				fieldLabel: 'Folio de Seguridad',
				id: 'folioSeguridad1',
				inputType: 'password',
				minLength: 4,
				maxLength: 128,
				name: 'folioSeguridad'
			},
			{
				xtype: 'textfield',
				allowBlank: false,
				id: 'folioSeguridadConfirmacion1',
				name: 'folioSeguridadConfirmacion',
				fieldLabel: 'Confirmar Folio de Seguridad',
				inputType: 'password',
				minLength: 4,
				maxLength: 128,
				enableKeyEvents: true,
				listeners:{
					keydown:function(obj, e){
						var c = e.keyCode;
						if(e.ctrlKey && c==86){
						e.stopEvent();
						}
					}
				}
			},
			{
				xtype:'fieldset',
				labelWidth:180,
				items:[
					{
						xtype: 'radiogroup',
						id: 'gcsCveCeDer',
						fieldLabel: '<div style="text-align:justify;">�Desea que en la selecci�n de documentos v�a Operaci�n Telef�nica se solicite su Clave de Cesi�n de Derechos?</div>',
						labelSeparator: '',
						itemCls: 'x-check-group-alt',
						columns: 1,
						items: [
							 {boxLabel: 'Si', name: 'csCveCeDer', inputValue: 'S'},
							 {boxLabel: 'No', name: 'csCveCeDer', inputValue: 'N', checked: true}
						]
				  }
				]
			}
		],
		buttons: [
			{
				text: 'Aceptar',
				formBind: true,
				handler: function(boton, evt) {
					var basicForm = fpConfirmacion.getForm();
					var cmpFolioSeguridad = Ext.getCmp("folioSeguridad1");
					var cmpFolioSeguridadConfirm = Ext.getCmp("folioSeguridadConfirmacion1");
					if (cmpFolioSeguridad.getValue() != cmpFolioSeguridadConfirm.getValue()) {
						cmpFolioSeguridadConfirm.markInvalid('La confirmaci�n del Folio de seguridad no es correcta');
						cmpFolioSeguridadConfirm.focus();
						return;
					}
					
					pnl.el.mask('Validando Folio...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '15clavecesionderechos_ext.data.jsp',
						params: Ext.apply(fpConfirmacion.getForm().getValues(),{
							informacion: 'validaFolioSeguridad'
						}),
						callback: procesarSuccessValidaFolioSeguridad
					});
					
					
				}
			},
			{
				text:'Cancelar',
				handler: function(btn){
					window.location.href='15clavecesionderechos_ext.jsp';
				}
			}
		]
	});
	
	var fpAltaCveCesion = new Ext.FormPanel({
		id: 'formAltaCveCesion1',
		width: 250,
		height:150,
		title: 'Clave cesi�n de derechos',
		frame: true,   
		border: true, 
		monitorValid: true,
		bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:center',  
		defaultType: 'textfield',
		style: 'margin: 0 auto',
		buttonAlign: 'center',
		defaults: {
			msgTarget: 'side',
			anchor: '-30'
		},
		items : [
			{   
					xtype:'textfield',
					fieldLabel: 'Clave',
					name:'cesionCve',
					id: 'cesionCve1', //campo clave cesion
					inputType: 'password', 
					style:'padding:4px 3px;',
					allowBlank: false,
					maxLength : 8,
					minLength : 8,
					vtype:'alphanum',
					listeners:{
						blur: function(obj){
							var InString = obj.getValue();
							var f = document.form1;	
							var RefString="1234567890";
							var inc = 0;
							var Count = 0;
							var TempChar;
							var auxChar;
							if(InString != ''){
								for(Count=0;Count < InString.length;Count++) {
									TempChar = InString.substring(Count,Count+1);
									 if(RefString.indexOf(TempChar,0)==-1) 
										inc++;
								}
								if(inc == 0 || inc == 8){
									Ext.MessageBox.alert('Aviso','La clave debe contener tanto n�meros como letras. Por ejemplo nafe2009');
									obj.setValue("");
									obj.focus();
									return;
								}
								inc = 0;
								for(Count=0;Count < InString.length;Count++){
									TempChar = InString.substring(Count,Count+1);
									if(Count > 0){
										if(auxChar == TempChar){
											inc++;	
										}else{
											inc = 0;
										}
									}
									auxChar = TempChar;
									if(inc == 2){
										break;
									}
								}
								if(inc == 2){
									if(isdigit(auxChar)){
										Ext.MessageBox.alert('Aviso','La clave debe contener mas de 2 n�meros iguales consecutivos. (Ejemplo:111md5es)');
										obj.setValue("");
										obj.focus();
									}else{
										Ext.MessageBox.alert('Aviso','La clave debe contener mas de 2 letras iguales en forma consecutiva. (Ejemplo:fffmf23a)');
										obj.setValue("");
										obj.focus();
									}
									return;
								}
							}
						}
					}
		  },    
		  {   
				xtype:'textfield',
				fieldLabel: 'Confirmar Clave',
				name: 'cesionCveConfirm',
				id: 'cesionCveConfirm1', //campo clave de confirmacion
				inputType: 'password', 
				style:'padding:4px 3px;',   
				blankText: 'Digite su contrase�a',
				allowBlank: false,
				maxLength : 8,
				minLength : 8,
				vtype:'alphanum'
		  }
		],
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAltaAcept',
				formBind: true,
				handler:function(){
					if(Ext.getCmp('cesionCve1').getValue() != Ext.getCmp('cesionCveConfirm1').getValue()){
						Ext.MessageBox.alert('Aviso','La confirmaci�n de la clave no coincide, por favor verif�quelo');
						Ext.getCmp('cesionCveConfirm1').setValue('');
						Ext.getCmp('cesionCveConfirm1').focus();
					}else{
						pnl.el.mask('Procesando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '15clavecesionderechos_ext.data.jsp',
							params: Ext.apply(fpAltaCveCesion.getForm().getValues(),{
								informacion: 'altaClaveCesionDerecho',
								csCveCeDer: csCveCeDer
							}),
							callback: procesarSuccessAltaClaveCesion
						});
					}
				}
			},
			{
				text:'Cancelar',
				handler: function(btn){
					window.location.href='15clavecesionderechos_ext.jsp';
				}
			}
		]
	});

	
	var pnlGeneracionClaveCesion = new Ext.Panel({
		id: 'pnlGeneracionClaveCesion1',
		hidden: true,
		height: 'auto',
		width: 550,
		labelWidth: 200,
		title: 'Generaci�n de Clave de Confirmaci�n de Operaciones',
		style: 'margin: 0 auto',
		frame: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype:'panel', bodyBorder: false, style: 'margin: 0 auto', border: false,
				html:'<table class="formas" ><tr><td style="text-align:center"><b>Pol�ticas de Contrase�a.</b></td></tr>'+
								'<tr><td>'+
								'<tr><td>* Debe contener tanto n�meros como letras, y ser igual a <b>8</b> posiciones </td></tr>'+
								'<tr><td>* Se permite el uso de letras may�sculas y min�sculas. </td></tr>'+
								'<tr><td>* No se permite el uso de letras acentuadas, ni caracteres especiales como �, $, etc. </td></tr>'+
								'<tr><td>* El Usuario o parte del Usuario no debe estar contenido en el Password. </td></tr>'+
								'<tr><td>* No debe contener m�s de 2 n�meros iguales consecutivos. (ej. 12223445) </td></tr>'+
								'<tr><td>* No debe contener m�s de 2 letras iguales en forma consecutiva. (ej. bbbccd) </td></tr>'+
								'<table>'
			},
			fpAltaCveCesion			
		]
	});

//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			NE.util.getEspaciador(25),
			fpGeneraClaveCesion,
			fpConfirmacion,
			pnlGeneracionClaveCesion,
			NE.util.getEspaciador(20),
			fpHistClaveCesion
		]
	});
	
	pnl.el.mask('Cargando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '15clavecesionderechos_ext.data.jsp',
		params: {
			informacion: 'validaExisteFolio'
		},
		callback: procesarSuccessValidaExisteFolio
	});


});