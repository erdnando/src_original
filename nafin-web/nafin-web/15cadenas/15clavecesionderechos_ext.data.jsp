<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,
		netropology.utilerias.*,
		com.netro.seguridadbean.*,
		netropology.utilerias.usuarios.*,
		com.netro.pdf.*,
		com.netro.afiliacion.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
JSONObject jsonObj = new JSONObject();
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";


UtilUsr utilUsr = new UtilUsr();
com.netro.seguridadbean.Seguridad seguridad = ServiceLocator.getInstance().lookup(
		"SeguridadEJB",com.netro.seguridadbean.Seguridad.class);	


if (informacion.equals("validaExisteFolio")){
	Usuario usuario = utilUsr.getUsuario(iNoUsuario);
	String mail = usuario.getEmail();
	String vFolio = seguridad.getFolioClaveCesion(iNoUsuario);
	boolean existeCveCesion = seguridad.vvalidarCveCesion(iNoUsuario);
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("folio", vFolio==null?"":vFolio);
	jsonObj.put("existeCveCesion", new Boolean(existeCveCesion));
	jsonObj.put("mail", mail==null?"":mail);
	infoRegresar = jsonObj.toString();

}else  if(informacion.equals("validaFolioSeguridad")){
			String folioSeguridad 		= request.getParameter("folioSeguridad");
			String csCveCeDer  =  request.getParameter("csCveCeDer");
			try{
				String vFolio = seguridad.getFolioClaveCesion(iNoUsuario);
				if(folioSeguridad.equals(vFolio)){
					jsonObj.put("validacionExitosa", new Boolean(true));
				}else{
					jsonObj.put("validacionExitosa", new Boolean(false));
				}
				
			}catch(Exception ne){
				ne.printStackTrace();
				throw new AppException("Error al validar Folio de Seguridad ", ne);
			}
			
			
			jsonObj.put("csCveCeDer", csCveCeDer);
			jsonObj.put("success", new Boolean(true));
			infoRegresar = jsonObj.toString();

}else  if(informacion.equals("generaFolioSeguridad")){
			String msgError="";
			try{
				seguridad.generaFolioClaveCesion(iNoUsuario);
			}catch(Throwable ne){
				System.out.println("Exception: BeanSegFacultad.generaFolioClaveCesion ");// Debug info
				ne.printStackTrace();
				msgError = ne.getMessage();
			}
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("msgError", msgError);
			infoRegresar = jsonObj.toString();
			
}else if(informacion.equals("altaClaveCesionDerecho")){
	
	String cesionCve  =  request.getParameter("cesionCve");
	String csCveCeDer  =  request.getParameter("csCveCeDer");
	
	try{
		seguridad.eliminaCesion(iNoUsuario);
		seguridad.guardarCveCesEncriptada(iNoUsuario,cesionCve, csCveCeDer);
	}catch(Exception nee){
		throw new AppException("Error al dar de alta la clave de cesion para la pyme ", nee);
	}
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("ConsultaParamSolicCveCder")){
	
	String csSolicCveCDer   = seguridad.consultaParamValidCveCder(iNoUsuario);
	
	jsonObj.put("csSolicCveCDer", csSolicCveCDer);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();


}else if(informacion.equals("ConsultaHistorial")){
	List lstReg = seguridad.consultaHistCveCDer(iNoUsuario);
	String csSolicCveCDer = seguridad.consultaParamValidCveCder(iNoUsuario);
	
	JSONArray registros = JSONArray.fromObject(lstReg);
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+", \"csSolicCveCDer\": \""+csSolicCveCDer+"\"}";
	
}else if(informacion.equals("GuardarHistorial")){
	String csSolicCveCDer  =  request.getParameter("csSolicCveCDer");
	
	seguridad.guardarHistCveCDer(iNoUsuario, csSolicCveCDer);
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}

%>

<%=infoRegresar%>