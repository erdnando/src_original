Ext.onReady(function() {

//--------------------------------- HANDLERS -------------------------------

	var procesarSuccessFailureCambiarPassword = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.show({
				title: 'Estatus del cambio de contraseña',
				msg: jsonObj.msg,
				buttons: Ext.MessageBox.OK,
				fn: function(){
					fp.getForm().reset(); 
				},
				icon: Ext.MessageBox.INFO
			});

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//-------------------------------- STORES -----------------------------------

//-------------------------------------------------------------------
	var fp = new Ext.FormPanel({
		hidden: false,
		height: 'auto',
		width: 300,
		labelWidth: 130,
		title: 'Cambio de Contraseña',
		style: 'margin: 0 auto',
		frame: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: [
			{
				xtype: 'textfield',
				allowBlank: false,
				fieldLabel: 'Contraseña anterior',
				inputType: 'password',
				minLength: 6,
				maxLength: 128,
				name: 'password_anterior'
			},
						{
				xtype: 'textfield',
				allowBlank: false,
				fieldLabel: 'Nueva contraseña',
				id: 'passwordNuevo',
				inputType: 'password',
				minLength: 6,
				maxLength: 128,
				name: 'password_nuevo'
			},
			{
				xtype: 'textfield',
				allowBlank: false,
				id: 'passwordNuevoConfirmacion',
				fieldLabel: 'Confirmar contraseña',
				inputType: 'password',
				minLength: 6,
				maxLength: 128
			}
		],
		buttons: [
			{
				text: 'Enviar',
				formBind: true,
				handler: function(boton, evt) {
					var basicForm = fp.getForm();
					var cmpPassNuevo = Ext.getCmp("passwordNuevo");
					var cmpPassNuevoConfirmacion = Ext.getCmp("passwordNuevoConfirmacion");
					if (cmpPassNuevo.getValue() != cmpPassNuevoConfirmacion.getValue()) {
						cmpPassNuevoConfirmacion.markInvalid('La confirmación de la contraseña no es correcta');
						cmpPassNuevoConfirmacion.focus();
						return;
					}
					
					fp.el.mask('Procesando...', 'x-mask-loading');
					
					Ext.Ajax.request({
						url: '15forma02ext.data.jsp',
						params: Ext.apply(basicForm.getValues(),{
							informacion: "CambiarPassword"
						}),
						callback: procesarSuccessFailureCambiarPassword
					});
				}
			}
		]
	});


	var panel = new Ext.Panel({
		xtype: 'panel',
		hidden: false,
		title: 'Politicas para cambio de contraseña',
		html: Ext.getDom('politicas').innerHTML,
		style: 'margin: 0 auto',
		frame: true,
		width: 600,
		height: 'auto'
	});
//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			fp,
			NE.util.getEspaciador(25,700),
			panel
		]
	});

//-------------------------------- ----------------- -----------------------------------

});