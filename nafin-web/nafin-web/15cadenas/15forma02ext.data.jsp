<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.sql.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar = "";
JSONObject jsonObj = new JSONObject();

if (informacion.equals("CambiarPassword")) {
	String sClave = (String) session.getAttribute("sesCveUsuario");
	String sContrasena = request.getParameter("password_anterior");
	String sContraNueva = request.getParameter("password_nuevo");
	if(sContrasena == null || sContrasena.equals("") || 
			sContraNueva == null || sContraNueva.equals("")){
		throw new AppException("Error en los parametros recibidos");
	} else {
		UtilUsr util = new UtilUsr();
		boolean valida = false;
		valida = util.esUsuarioValido(sClave,sContrasena);
		if(valida){
			String respuesta = util.cambiarPassword(sClave,sContrasena,sContraNueva);
			if(respuesta == null || respuesta.equals("")) {
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("msg", "Cambio de contraseña realizado");
			} else {
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "Error en el cambio de contraseña");
				jsonObj.put("stackTrace", respuesta);
			}
		} else {
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("msg", "Error en el cambio de contraseña. Los datos proporcionados son incorrectos");
		}
	}
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>
