Ext.onReady(function() {
var gralCorreo = '';
var hidiNoUsuario = Ext.getDom('hidiNoUsuario').value;
var hidStrNombreUsuario = Ext.getDom('hidStrNombreUsuario').value;
var hidStrSerial = Ext.getDom('hidStrSerial').value;
var hidOpcion = Ext.getDom('hidOpcion').value;
var instal = Ext.getDom('instal').value;



//-------------------------------FUNCIONES--------------------------------------

var funcHidePanels = function(){
	pnlInstalCert.hide();
	pnlCertRevocado.hide();
	fpConfirmacion.hide();
	pnlAvisoFolio.hide();
	fpRevocacionConfirm.hide();
	fpRevocacion.hide();
	fpVigencia.hide();
	pnlRevocadoNoCert.hide();
	pnlCertYaInst.hide();
	pnlVigNoCert.hide();
	fpCerKey.hide();
}

var funcInfoOpcion = function(opcion, rgroup){
	Ext.Ajax.request({
		url: '15certificadodigital_ext.data.jsp',
		params: {
			informacion: 'obtieneSerialCert',
			opcion: opcion
		},
		callback: procesarSuccessSerialCert
	});
}


var procesarSuccessSerialCert  = function(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
		var opcion = resp.opcion;
		hidStrSerial = resp.strSerialAct;

		if(hidStrSerial=='' && opcion=='1'){
			funcHidePanels();
			pnlVigNoCert.show();
		}else if(hidStrSerial=='' && opcion=='2'){
			funcHidePanels();
			pnlRevocadoNoCert.show();
		}else if(hidStrSerial!='' && opcion=='3'){
			funcHidePanels();
			pnlCertYaInst.show();
		}else if(opcion=='4'){
			funcHidePanels();
			fpCerKey.show();
			Ext.getCmp('fsArchivosDesc').hide();
			fpCerKey.getForm().reset();
			pnl.add(fpCerKey);
			pnl.doLayout();
		}else{
			Ext.Ajax.request({
				url: '15certificadodigital_ext.data.jsp',
				params: {
					informacion: 'obtieneInfoCertificado',
					opcion: opcion
				},
				callback: procesarSuccessInfo
			});
		}

	} else {
		NE.util.mostrarConnError(response,opts);
	}
}



var procesarSuccessInfo = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			funcHidePanels();
			if(resp.opcion=='1'){
				Ext.getCmp('lbFecVig').setText(resp.fecVigenciaCert);
				fpVigencia.show();
				if(!pnl.findById('fpVigencia1')){
					pnl.add(fpVigencia);
					pnl.doLayout();

				}
			}else if(resp.opcion=='2'){
				Ext.getCmp('fpCorreo1').setText(resp.mailUsuario);
				Ext.getCmp('fpCorreoConfirm1').setText(resp.mailUsuario);
				gralCorreo = resp.mailUsuario;

				if(resp.bfolioVigente){
					fpRevocacionConfirm.show();
					if(!pnl.findById('fpRevocacionConfirm1')){
						fpRevocacionConfirm.show();
						pnl.add(fpRevocacionConfirm);
						pnl.doLayout();
					}
				}else{
					fpRevocacion.show();
					if(!pnl.findById('fpRevocacion1')){
						fpRevocacion.show();
						pnl.add(fpRevocacion);
						pnl.doLayout();
					}
				}
			}else if(resp.opcion=='3'){

				pnlInstalCert.show();
				pnl.add(pnlInstalCert);
				pnl.doLayout();

			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

var handlerRevocacion = function(btn){
		pnl.el.mask('Enviando Folio...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '15certificadodigital_ext.data.jsp',
			params: {
				informacion: 'solicitaRevocacion'
			},
			callback: procesarSuccessSolicRevocacion
		});
}

var procesarSuccessSolicRevocacion = function(opts, success, response) {
	pnl.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);

		funcHidePanels();
		pnlAvisoFolio.show();
		pnl.add(pnlAvisoFolio);
		pnl.doLayout();

		var msg = '<p align="center">El Folio de Seguridad fue enviado al correo electr�nico<br>'+gralCorreo+'</p>';
		//var msg = gralCorreo;
		//alert(msg);
		Ext.getCmp('pnlMsgEnvCorreo').update(msg);
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

var handlerAceptaAviso = function(btn){
	funcHidePanels();
	fpRevocacionConfirm.show();
	pnl.add(fpRevocacionConfirm);
	pnl.doLayout();

}

var handlerConfirmaRev = function(){
	pnl.el.mask('Procesando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '15certificadodigital_ext.data.jsp',
		params: {
			informacion: 'validaVigRevocacion'
		},
		callback: procesarSuccessVvalidaVigRevocacion
	});

}

var procesarSuccessVvalidaVigRevocacion = function(opts, success, response) {
	pnl.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);

		if(!resp.bfolioVigente){
			Ext.Msg.alert('Aviso', 'El Folio de Seguridad ha expirado, favor de generar otro para continuar con la revocaci�n del certificado');
		}else{
			funcHidePanels();
			fpConfirmacion.show();
			pnl.add(fpConfirmacion);
			pnl.doLayout()
		}
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

var procesarSuccessFailureConfirmaPassword = function(opts, success, response) {
	pnl.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);

		if(resp.bFolio){
			funcHidePanels();
			pnlCertRevocado.show();
			pnl.add(pnlCertRevocado);
			pnl.doLayout();
			//window.location.href='/nafin/20secure/index.jsp';
			window.location.href='15certificadodigital_ext.jsp?opcion=3';
		}else{
			Ext.Msg.alert('Aviso', 'Folio de Seguridad Incorrecto');
		}
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

//-------------------------------PANELES----------------------------------------
//PANEL PARA OPCION VIGENCIA
var fpVigencia = new Ext.FormPanel({
		name: 'fpVigencia',
		id: 'fpVigencia1',
		hidden: false,
		height: 'auto',
		width: 450,
		labelWidth: 200,
		title: 'Vigencia - Certificado Digital',
		style: 'margin: 0 auto',
		frame: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'label',
				fieldLabel: '<b>Login</b>',
				html: hidiNoUsuario,
				align: 'center',
				name: 'fpLogin'
			},{
				xtype: 'label',
				allowBlank: false,
				fieldLabel: '<b>Nombre Usuario</b>',
				text: hidStrNombreUsuario,
				name: 'fpNombreUsuario'
			},{
				xtype: 'label',
				id:'lbFecVig',
				fieldLabel: '<b>Fecha Vigencia</b>',
				name: 'fpFecVigencia'
			}
		]
	});


var fpRevocacion = new Ext.FormPanel({
		name: 'fpRevocacion',
		id: 'fpRevocacion1',
		hidden: false,
		height: 'auto',
		width: 450,
		labelWidth: 200,
		title: 'Revocaci�n - Certificado Digital',
		style: 'margin: 0 auto',
		frame: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{ xtype: 'label', fieldLabel: '<b>Login</b>', html: hidiNoUsuario, align: 'center', name: 'fpLogin' },
			{ xtype: 'label', allowBlank: false, fieldLabel: '<b>Nombre Usuario</b>', text: hidStrNombreUsuario, name: 'fpNombreUsuario' },
			{ xtype: 'label', id:'fpCorreo1', fieldLabel: '<b>Correo Electr�nico</b>', name: 'fpCorreo'},
			{xtype:'panel', id: 'pnlInternoBtnRevocar', height:30, style: 'margin: 0 auto',
				items:[
					{
						xtype: 'button',
						style: 'margin: 0 auto',
						name: 'revocar',
						text: '<b>Revocar</b>',
						width:100,
						handler: handlerRevocacion
					}
				]
			},
			{
				xtype:'panel', id:'pnlInternoMsgRevocacion', bodyBorder: false, style: 'margin: 0 auto', border: false,
				html:'<table border=1 bordercolor="black" style="border-collapse:collapse;" ><tr><td style="background-color:#F2F2F2; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">' +
					  '<p style="text-align: justify;" >Se enviar� un Folio de Seguridad al correo electr�nico indicado en la parte superior, ' +
					  'el cual se utilizar� para la revocaci�n del Certificado, en caso de que el correo electr�nico ' +
					  'no sea el correcto por favor comun�quese al Centro de Atenci�n a Clientes al tel�fono 01-800-CADENAS (01-800-2233627).</p></td></tr></table>'
			}

		]
	});


var fpRevocacionConfirm = new Ext.FormPanel({
		name: 'fpRevocacionConfirm',
		id: 'fpRevocacionConfirm1',
		hidden: false,
		height: 'auto',
		width: 450,
		labelWidth: 200,
		title: 'Revocaci�n - Certificado Digital',
		style: 'margin: 0 auto',
		frame: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{ xtype: 'label', fieldLabel: '<b>Login</b>', html: hidiNoUsuario, align: 'center', name: 'fpLogin' },
			{ xtype: 'label', allowBlank: false, fieldLabel: '<b>Nombre Usuario</b>', text: hidStrNombreUsuario, name: 'fpNombreUsuario' },
			{ xtype: 'label', id:'fpCorreoConfirm1', fieldLabel: '<b>Correo Electr�nico</b>', name: 'fpCorreoConfirm'},
			{
				xtype:'panel', id:'pnlInternoConfirmRev', bodyBorder: false, style: 'margin: 0 auto', border: false,
				html:'<table border=1 bordercolor="black" style="border-collapse:collapse;" ><tr><td style="background-color:#F2F2F2; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">' +
					  '<p style="text-align: justify;" >"Ya solicit&oacute; la revocaci�n del Certificado Digital. Para continuar favor de dar clic en el bot�n <u>Continuar Revocaci�n</u>".<br><br> ' +
					  'Si el folio de Seguridad ha expirado de clic en el bot�n <u>Generar Nuevo Folio</u> ' +
					  '</p></td></tr></table>',
				buttonAlign: 'center',
				buttons:[
					{
						text:'Generar Nuevo Folio',
						handler: handlerRevocacion
					},
					{
						text:'Continuar Revocaci�n',
						handler: handlerConfirmaRev
					}
				]
			}

		]
	});

var pnlAvisoFolio = new Ext.Panel({
	name: 'pnlAvisoFolio',
	id: 'pnlAvisoFolio1',
	width: 400,
	title: 'Aviso',
	style: 'margin: 0 auto',
	frame:true,
	items: [
		{
			xtype: 'panel',
			id: 'pnlMsgEnvCorreo',
			style: 'margin: 0 auto',
			bodyBorder: false,
			border: false,
			width: 300,
			height: 50
		},
		{
			xtype:'panel',
			width: 300,
			height: 80,
			bodyBorder: false,
			style: 'margin: 0 auto',
			border: false,
			html:'<table border=1 bordercolor="black" style="border-collapse:collapse;" ><tr><td style="background-color:#F2F2F2; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;"> ' +
				  '<p style="text-align: justify;" >Despu�s de un plazo de 24 hrs el Folio de Seguridad expirar� y deber� solicitar otro para continuar con la revocaci�n del Certificado Digital.</p>' +
				  '</td></tr></table>'
		},
		{
			xtype: 'panel',
			width: 120,
			style: 'margin: 0 auto',
			bodyBorder: false,
			border: false,
			items:[{xtype:'button', width:100, text:'Aceptar', handler: handlerAceptaAviso}]
		}
	]
});


var fpConfirmacion = new Ext.FormPanel({
		hidden: false,
		height: 'auto',
		width: 300,
		labelWidth: 130,
		title: 'Revocar Certificado - Folio',
		style: 'margin: 0 auto',
		frame: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: [
			{
				xtype: 'textfield',
				allowBlank: false,
				fieldLabel: 'Folio de Seguridad',
				id: 'passwordNuevo',
				inputType: 'password',
				minLength: 4,
				maxLength: 128,
				name: 'password_nuevo'
			},
			{
				xtype: 'textfield',
				allowBlank: false,
				id: 'passwordNuevoConfirmacion',
				fieldLabel: 'Confirmar Folio de Seguridad',
				inputType: 'password',
				minLength: 4,
				maxLength: 128
			}
		],
		buttons: [
			{
				text: 'Aceptar',
				formBind: true,
				handler: function(boton, evt) {
					var basicForm = fpConfirmacion.getForm();
					var cmpPassNuevo = Ext.getCmp("passwordNuevo");
					var cmpPassNuevoConfirmacion = Ext.getCmp("passwordNuevoConfirmacion");
					if (cmpPassNuevo.getValue() != cmpPassNuevoConfirmacion.getValue()) {
						cmpPassNuevoConfirmacion.markInvalid('La confirmaci�n del Folio de seguridad no es correcta');
						cmpPassNuevoConfirmacion.focus();
						return;
					}

					pnl.el.mask('Revocando Certificado...', 'x-mask-loading');

					Ext.Ajax.request({
						url: '15certificadodigital_ext.data.jsp',
						params: Ext.apply(basicForm.getValues(),{
							informacion: "confirmaRevocacion"
						}),
						callback: procesarSuccessFailureConfirmaPassword
					});
				}
			},
			{
				text:'Cancelar',
				handler: function(btn){
					window.location.href='15certificadodigital_ext.jsp?opcion=2';
				}
			}
		]
	});


var pnlCertRevocado = new Ext.Panel({
	name:'pnlCertRevocado',
	width: '400',
	style: 'margin: 0 auto',
	bodyBorder: false,
	border: false,
	html:'<table border=1 bordercolor="black" style="border-collapse:collapse;" ><tr><td style="background-color:#F2F2F2; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">' +
					  '<p style="text-align: justify;" >El Certificado Digital ha sido revocado exitosamente. Para Instalar un nuevo Certificado Digital, favor de dar <a href="15certificadodigital_ext.jsp?opcion=3">clic</a>.<br><br> ' +
					  '</p></td></tr></table>'
});


var pnlMsgInstalacion = new Ext.Panel({
	name:'pnlMsgInstalacion',
	width: '550',
	style: 'margin: 0 auto',
	frame: true,
	bodyBorder: false,
	border: false,
	html:'<table border=1 bordercolor="black" style="border-collapse:collapse;" ><tr><td style="background-color:#F2F2F2; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">' +
					  '<p style="text-align: justify;" ><b>NOTA:</b> Si ha realizado el proceso de revocar certificado, antes de realizar la instalaci�n deber� eliminar el Certificado instalado en su equipo en: ' +
					  'Herramientas - Opciones Internet - Contenido - Certificados - Seleccionar Certificado y dar clic en "Quitar". ' +
					  '</p></td></tr></table>'
});

var pnlInstalCert = new Ext.FormPanel({
	name:'pnlInstalCert',
	id:'pnlInstalCert1',
	width: '550',
	frame: false,
	border: false,
	style: 'margin: 0 auto',
	items:[
		pnlMsgInstalacion,
		NE.util.getEspaciador(25),
		{
			xtype: 'panel',
			title: 'Certificado Digital',
			frame: true,
			html:'<table ><tr><td style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">' +
							  '<hr><p align="center"><b>IMPORTANTE</b><br> ' +
							  'Una vez generado e instalado el certificado, es necesario cerrar el navegador y volver a ingresar al sistema.' +
							  '</p></td></tr></table>',
			buttonAlign: 'right',
			buttons: [
				{
					text:'Generar Certificado',
					handler: function(){
						window.location.href='/nafin/20secure/20generarCertificadoExt.jsp';
					}
				}
			]
		},
		{
			xtype: 'panel',
			frame: true,
			html:'<hr>',
			buttonAlign: 'right',
			buttons: [
				{
					text:'Manual de Instalaci�n',
					iconCls: 'icoPdf',
					handler: function(){

						var params = {nombreArchivo: '16archivos/cadenas/Guia_Oper_Insta_Elimin_Certificados.pdf'};
						var urlArchivo =  NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
						var fp = Ext.getCmp('pnlInstalCert1');
						fp.getForm().getEl().dom.action = urlArchivo;
						fp.getForm().getEl().dom.submit();
					}
				}
			]
		}
	]
});

var pnlRevocadoNoCert = new Ext.Panel({
	name:'pnlRevocadoNoCert',
	width: '400',
	style: 'margin: 0 auto',
	bodyBorder: false,
	border: false,
	hidden: true,
	html:'<table border=1 bordercolor="black" style="border-collapse:collapse;" ><tr><td style="background-color:#F2F2F2; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">' +
					  '<p style="text-align: justify;" >No existe Certificado Digital Instalado para efectuar la revocaci�n.<br><br> ' +
					  '</p></td></tr></table>'
});

var pnlCertYaInst = new Ext.Panel({
	name:'pnlCertYaInst',
	width: '400',
	style: 'margin: 0 auto',
	bodyBorder: false,
	border: false,
	hidden: true,
	html:'<table border=1 bordercolor="black" style="border-collapse:collapse;" ><tr><td style="background-color:#F2F2F2; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">' +
					  '<p style="text-align: justify;" >Ya cuenta con el Certificado Digital Instalado.<br><br> ' +
					  '</p></td></tr></table>'
});


var pnlVigNoCert = new Ext.Panel({
	name:'pnlVigNoCert',
	width: '400',
	style: 'margin: 0 auto',
	bodyBorder: false,
	border: false,
	hidden: true,
	html:'<table border=1 bordercolor="black" style="border-collapse:collapse;" ><tr><td style="background-color:#F2F2F2; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">' +
					  '<p style="text-align: justify;" >No existe Certificado Digital instalado para consultar la Vigencia.<br><br> ' +
					  '</p></td></tr></table>'
});

var pkcs12Buffer = null;
var fpCerKey = new Ext.FormPanel({
		name: 'fpCerKey',
		id: 'fpCerKey1',
		hidden: false,
		height: 'auto',
		width: 550,
		labelWidth: 200,
		title: 'Obtener archivos CER y KEY',
		style: 'margin: 0 auto',
		frame: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'fileuploadfield',
				id: 'pfx_file',
				emptyText: 'Ruta de certificado',
				fieldLabel: 'Seleccione certificado (pfx)',
				name: 'pfx_file',
				buttonText: 'Examinar...',
				width: 320,
				listeners: {
					fileselected: function(fb, v){
						//objSelectFirma.setHandlerFileUpload(fb.fileInput, v);
						var temp_reader = new FileReader();

						var current_files = fb.fileInput.dom.files;

						temp_reader.onload =
						function(event)
						{
							pkcs12Buffer = event.target.result;
						};

						temp_reader.readAsArrayBuffer(current_files[0]);
					}
				},
				regex: /^.*\.(pfx|PFX)$/,
				regexText:'Solo se admiten archivos con extensi�n *.pfx'
			},
			{
				fieldLabel: 'Contrase�a certificado (pfx))',
				xtype: 'textfield',
				name: 'passwordPfx',
				id: 'passwordPfx',
				inputType: 'password',
				style:'padding:4px 3px;',
				allowBlank: false,
				margins: '0 20 0 0'
			},
			{
				xtype: 'fieldset',
				title: 'Establezca contrase�a para los archivos a generar',
				style: 'margin:0 auto',
				defaults: {
					msgTarget: 'side',
					anchor: '-20'
				},
				name: 'lbCer',
				items:[{
							fieldLabel: 'Contrase�a de llave privada',
							xtype: 'textfield',
							name: 'passwordCer',
							id: 'passwordCer',
							inputType: 'password',
							style:'padding:4px 3px;',
							allowBlank: false,
							margins: '0 20 0 0'
						},
						{
							fieldLabel: 'Confirmar contrase�a de llave privada',
							xtype: 'textfield',
							name: 'passwordKey',
							id: 'passwordKey',
							inputType: 'password',
							style:'padding:4px 3px;',
							allowBlank: false,
							margins: '0 20 0 0'
						}]
			},
			{
				xtype: 'fieldset',
				id: 'fsArchivosDesc',
				title: 'Archivos generados',
				style: 'margin:0 auto',
				hidden: true,
				defaults: {
					msgTarget: 'side',
					anchor: '-20'
				},
				name: 'lbCer',
				items:[
					{
						xtype: 'panel',
						html: '<ol id="results"></ol>',
						style: 'margin:0 auto'
					}
				]
			}


		],
		buttons: [
			{
				text:'Generar Certificado(CER) y Llave privada(KEY)',
				handler: function(){

					var formaValida = true;
					if(pkcs12Buffer===null || pkcs12Buffer.byteLength === 0) {
						Ext.getCmp("pfx_file").markInvalid('Debe seleccionar el certificado *.PFX');
						formaValida = false;
					}
					var passwordPfx =  Ext.getCmp("passwordPfx").getValue();
					if(passwordPfx === '') {
						Ext.getCmp("passwordPfx").markInvalid('Debe ingresar la contrase\u00f1a');
						formaValida = false;
					}
					var passwordCer =  Ext.getCmp("passwordCer").getValue();
					if(passwordCer === '') {
						Ext.getCmp("passwordCer").markInvalid('Debe ingresar la contrase\u00f1a');
						formaValida = false;
					}
					var passwordKey =  Ext.getCmp("passwordKey").getValue();
					if(passwordKey === '') {
						Ext.getCmp("passwordKey").markInvalid('Debe confirmar la contrase\u00f1a');
						formaValida = false;
					}
					if(passwordCer !== passwordKey) {
						Ext.getCmp("passwordCer").markInvalid('No coinciden las contrase\u00f1as');
						formaValida = false;
					}

					var node = document.getElementById("results");
					while(node.firstChild) {
						node.removeChild(node.firstChild);
					}
					Ext.getCmp('fsArchivosDesc').hide();

					if(formaValida && fpCerKey.getForm().isValid()){

						try {
							openPKCS12(pkcs12Buffer, passwordPfx, passwordCer, function(error, data) {
								if(error) {

									if(error == 'Error: Unable to decrypt PKCS#8 ShroudedKeyBag, wrong password?'){
										Ext.MessageBox.alert("Mensaje","Error: No fue posible decifrar el certificado, contrase�a incorrecta.");
									}else{
										Ext.MessageBox.alert("Mensaje",error);
									}

								}
							  else
							  {
								 var idx=0;

								 // create download link for private key
								 var base64String = btoa(String.fromCharCode.apply(null, new Uint8Array(data.privateKey.encoding)));
								 var aKey = document.createElement('a');
								 aKey.download = data.privateKey.label;
								 aKey.setAttribute('href', 'data:application/pkcs8;base64,' + base64String);
								 aKey.appendChild(document.createTextNode(data.privateKey.label));
								 var li = document.createElement('li');
								 li.appendChild(aKey);
								 document.getElementById('results').appendChild(li);

								 for(; idx < data.certificates.length; idx++) {
									base64String = btoa(String.fromCharCode.apply(null, new Uint8Array(data.certificates[idx].encoding)));
									// create download link for certificate
									var a2 = document.createElement('a');
									a2.download = data.certificates[idx].label;
									a2.setAttribute('href', 'data:application/pkix-cert;base64,' + base64String);
									a2.appendChild(document.createTextNode(data.certificates[idx].label));

									var li_ = document.createElement('li');
									li_.appendChild(a2);
									document.getElementById('results').appendChild(li_);
									Ext.getCmp('fsArchivosDesc').show();
									fpCerKey.getForm().reset();
									pkcs12Buffer = null;
								 }
							  }
						   });
						} catch(myExc) {
							Ext.MessageBox.alert("Error",'Se ha presentado una excepci�n: ' + myExc);
						}
					}
				}
			}
		]
	});

//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			//fp,
			NE.util.getEspaciador(25),
			{
				xtype: 'radiogroup',
				//fieldLabel: 'Auto Layout',
				width:500,
				style: 'margin: 0 auto',
				items: [
					 {boxLabel: 'Vigencia', name: 'rb-auto', inputValue: 1, checked: (hidOpcion=='1'?true:false)},
					 {boxLabel: 'Revocaci�n', name: 'rb-auto', inputValue: 2, checked: (hidOpcion=='2'?true:false)},
					 {boxLabel: 'Instalaci�n', name: 'rb-auto', inputValue: 3, checked: (hidOpcion=='3'?true:false)},
					 {boxLabel: 'Obtener CER y KEY', name: 'rb-auto', inputValue: 4, checked: (hidOpcion=='4'?true:false)}
				],
				listeners:{
					afterrender:function(rgroup){
						//if(rgroup.getValue().getValue()){
							funcInfoOpcion(rgroup.getValue().inputValue, rgroup);
						//}
					},
					change: function(rgroup, radio){
						funcInfoOpcion(radio.inputValue, rgroup);
					}
				}
			},
			NE.util.getEspaciador(10),
			pnlRevocadoNoCert,
			pnlCertYaInst,
			pnlVigNoCert
		]
	});


});