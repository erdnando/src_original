var VerDetalles;

Ext. onReady(function(){
	
	var jsonValoresIniciales = null;
	var registrosEnviar = [];
	var publicacion;
	var camposEditables;
	var noCampos;
		
	var cancelar =  function() {  				
		registrosEnviar = [];		
		publicacion;
	}
	var  procesarSuccessFailureGuarda =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			window.location = '15info01ext.jsp';		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function confirmacionClaves(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var bConfirma;
			jsondeConfirma = Ext.util.JSON.decode(response.responseText);
			if (jsondeConfirma != null){
				bConfirma= jsondeConfirma.bConfirma;									
			}		
			if(bConfirma ==false) {
				Ext.MessageBox.alert('Mensaje','La confirmación es incorrecta, intente de nuevo');				
			} else  if(bConfirma ==true) {
				var Comfirmacionclave = Ext.getCmp('Comfirmacionclave');
				if (Comfirmacionclave) {		
					Comfirmacionclave.destroy();		
				}					
				registrosEnviar = Ext.encode(registrosEnviar);				
				Ext.Ajax.request({
					url : '15info01ext.data.jsp',
					params : {
						informacion: 'GuardarRespuesta',					
						registros:registrosEnviar,
						publicacion:publicacion
					},
					callback: procesarSuccessFailureGuarda
				});	
			}
		} else {		
			NE.util.mostrarConnError(response,opts);
		}	
	}
		
	var procesarGuardar = function() {
		var gridDatos = Ext.getCmp('gridDatos');
		var store = gridDatos.getStore();	
		cancelar(); //limpia las variables				
		
		store.each(function(record) {					
			if( record.data['SELECCIONAR'] ==true){	
				var arregloNombre=(camposEditables.split(","));				
				for(i = 0; i < arregloNombre.length; i++) {
					var  temp = arregloNombre[i];
					if(Ext.isEmpty(record.data[temp]) ==true){						
						Ext.MessageBox.alert('Mensaje','Te hacen falta datos.');						
						return ;
					}
				}				
				
				registrosEnviar.push(record.data);				
				publicacion  =  record.data['IC_PUBLICACION'];								
			}					
		});			
		
				
		if (registrosEnviar.length !=noCampos) {		
			Ext.MessageBox.alert('Mensaje','Debe seleccionar por lo menos un registro para continuar');			
			return false;	
		}	
		
		if (registrosEnviar.length > 0) {	
			var btnGuardar = Ext.getCmp('btnGuardar');			
			btnGuardar.setHandler(function(boton, evento) {	
				var  fLogCesion = new  NE.cesion.FormLoginCesion();
				fLogCesion.setHandlerBtnAceptar(function(){
					Ext.Ajax.request({
						url: '15info01ext.data.jsp',
						params: Ext.apply(fLogCesion.getForm().getValues(),{
							informacion: 'ConfirmacionClaves'
						}),
						callback: confirmacionClaves
					});					
				});
				//se crea ventana de Confirmacion de Clave 
				var	ventanaConfir =  Ext.getCmp('Comfirmacionclave');	
				if (ventanaConfir) {
					ventanaConfir.show();				
				} else {					
					new Ext.Window({
						layout: 'fit',
						width: 400,
						height: 200,			
						id: 'Comfirmacionclave',
						modal:true,
						closeAction: 'hide',
						items: [					
							fLogCesion
						],
						title: ''				
					}).show();
				}
			});
		}
	}
	
		
	// ver Detalles Informacion
	VerDetalles= function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var num_pub = registro.get('IC_PUBLICACION');	
		var num_id = registro.get('NID');			
				
		var ventana = Ext.getCmp('VerDetallesInfo');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				layout: 'fit',
				width: 500,
				height: 300,		
				style: 'margin:0 auto;',
				id: 'VerDetallesInfo',
				closeAction: 'hide',
				items: [					
					PanelDetallesInfo
				],
				title: 'Ver Detalles de Información Diversos'					
			}).show();
		}	
		var pabelBody = Ext.getCmp('PanelDetallesInfo').body;
		var mgr = pabelBody.getUpdater();
		mgr.on('failure', 
			function(el, response) {
				pabelBody.update('');
				NE.util.mostrarErrorResponse(response);
			});		
		mgr.update({
			url: '15info06ext.jsp',
			params: {
				num_pub:num_pub,
				num_id:num_id
			},
			scripts: true,			
			indicatorText: 'Cargando Ver Detalles Información Diversos '
		});					
	}
	
	var PanelDetallesInfo = new Ext.Panel({
		id: 'PanelDetallesInfo',
		width: 400,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	});
	
	var procesarSuccessFailureTotales =  function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText)
			
			consultaDataTotales.fields =  jsonObj.columnasStoreTotales;
			consultaDataTotales.data =  jsonObj.columnasRecordsTotales;
			gridTotales.columns= jsonObj.columnasGridTotales;			
			
			formaDatos.add(gridTotales);
			formaDatos.el.unmask();
			formaDatos.doLayout();	
		}	
	}
	
	var procesarSuccessFailureEditables =  function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText)
							
			grid.hide();	
			var formaDatos = Ext.getCmp('formaDatos');		
			
			if(!formaDatos.isVisible()) 	{
				formaDatos.show();
			}			
		   publicacion  = jsonObj.publicacion;	
			camposEditables  = jsonObj.camposEditables;	
			consultaDataEditables.fields =  jsonObj.columnasStore;
			consultaDataEditables.data =  jsonObj.columnasRecords;
			gridDatos.columns = jsonObj.columnasGrid;	
			noCampos =  jsonObj.noCampos;	
			campoFechas = jsonObj.campoFechas;							 
			
			formaDatos.add(gridDatos);
			formaDatos.el.unmask();
			formaDatos.doLayout();	
			
			var noEditables  = jsonObj.noEditables;	
			if(noEditables>0){
			Ext.getCmp('btnGuardar').hide();
			}
			
			//esto es para formar el grid de los totales
			Ext.Ajax.request({
				url: '15info01ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'DATOSTOTALES',
					publicacion: publicacion
				}),
				callback: procesarSuccessFailureTotales
			});	
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	 
	var consultaDataEditables = {
		xtype: 'jsonstore',
		id:'consultaDataEditables',
		root: 'registros',
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		fields: null,
		data: null
	};
	
	var gridDatos = {
		xtype: 'editorgrid',	
		id:'gridDatos',
		store:consultaDataEditables,		
		columns: [],		
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		frame: true,
		height: 400,
		width: 900,	
		style: 'margin:0 auto;',
		title: '',
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 							
				var record = e.record;
				var gridDatos = e.gridDatos; 
				var campo= e.field;				
				var arregloNombre=(camposEditables.split(","));				
				for(i = 0; i < arregloNombre.length; i++) {
					var  temp = arregloNombre[i];
					if(campo == temp){
						var accion=record.data[temp];						
						if(accion!=='' ){						
							return false;
						}else {
							return true;
						}
					}				
				}
			},
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
				var record = e.record;
				var gridDatos = e.gridDatos; 
				var campo= e.field;
				
				var arregloNombre=(campoFechas.split(","));				
				for(i = 0; i < arregloNombre.length; i++) {
					var  temp = arregloNombre[i];
					if(campo == temp){
						var accion=record.data[temp];							
						if(accion!=='' ){													
							record.data[temp] =  Ext.util.Format.date(record.data[temp],'d/m/Y'); 
							record.commit();								
						}
						
					}				
				}
			}
		},				
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Guardar',
					id: 'btnGuardar',
					handler: procesarGuardar
				},
				'-',
				{
					text: 'Cancelar',
					xtype: 'button',
					id: 'btnCancelar',
					handler: function() {						
						window.location = '15info01ext.jsp';
					}
				}			
			]
		}
	};

	//para el grid de los totales 
	var consultaDataTotales = {
		xtype: 'jsonstore',
		id:'consultaDataTotales',
		root: 'registros',
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		fields: null,
		data: null
	};
	
	var gridTotales = {
		xtype: 'grid',	
		store:consultaDataTotales,		
		columns: [],		
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		frame: true,
		height: 100,
		width: 900,	
		style: 'margin:0 auto;',
		title: 'Totales'					
		};

	//para procesar los datos editables 
	var VerdatosT = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var publicacion = registro.get('IC_PUBLICACION');
		Ext.Ajax.request({
			url: '15info01ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'DATOSEDITABLES',
				publicacion: publicacion
			}),
			callback: procesarSuccessFailureEditables
		});	
	}
	
	var elementosFormaT = [
		{
			xtype: 'textarea',
			name: 'comentarios',
			id: 'comentarios1',
			fieldLabel: 'Comentarios',
			allowBlank: true,
			maxLength: 50,	
			hidden: true,			
			width: 50,
			msgTarget: 'side',
			margins: '0 20 0 0'  
		}	
	];
	
	// Verdatos
	var Verdatos = function(grid, rowIndex, colIndex, item, event) {
		var store = grid.getStore();	
		var registro = grid.getStore().getAt(rowIndex);
		var publicacion =registro.get('IC_PUBLICACION');
		var CadenasEpos =registro.get('IC_TIPO_PUBLICACION');
		var ventana = Ext.getCmp('VerdatosInfo');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				layout: 'fit',
				width: 500,
				height: 300,		
				style: 'margin:0 auto;',
				id: 'VerdatosInfo',
				closeAction: 'hide',
				items: [					
					PaneldatosInfo
				],
				title: 'Ver Informacion Diversos'					
			}).show();
		}	
		var pabelBody = Ext.getCmp('PaneldatosInfo').body;
		var mgr = pabelBody.getUpdater();
		mgr.on('failure', function(el, response) {
			pabelBody.update('');
			NE.util.mostrarErrorResponse(response);
		});		
		mgr.update({
			url: '15info04ext.jsp',
			params: {
				publicacion:publicacion,
				CadenasEpos:CadenasEpos
			},
			scripts: true,			
			indicatorText: 'Cargando Ver Informacion Diversos '
		});					
	}
	
	var PaneldatosInfo = new Ext.Panel({
		id: 'PaneldatosInfo',
		width: 400,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	});
	
	function procesaValoresIniciales (opts, success, response) {
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null) {
				if(jsonValoresIniciales.Usuario == 'NAFIN') 	{
					catalogoSimpleData.load();
					if(!fp.isVisible()) 	{
						fp.show();
					}
				}	else 	{
					consultaData.load();
					if(fp.isVisible()) {
						fp.hide();
					}
				}
			}
		}	else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//-----------------------------------HANDLERS-----------------------------------
	var procesarConsultaData = function (store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if(!grid.isVisible()) {
				grid.show();
			}
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				btnGenerarPDF.enable();
				btnBajarPDF.hide();
				
				if(!btnBajarPDF.isVisible()){
					btnGenerarPDF.enable();
				} else {
					btnGenerarPDF.disable();
				}
				el.unmask();
			}else {
				btnGenerarPDF.disable();
				el.mask('No se encontró ningún registro','x-mask');
			}
		}
	}
	
	var procesarSuccessFailureGenerarPDF = function(opts,success,response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if(success = true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton, evento){
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
//------------------------------------STORES------------------------------------
	var catalogoSimpleData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '15info01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoSimple'
		},
		totalProperty: 'total',
		autoload: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var consultaData= new Ext.data.JsonStore({
		root: 'registros',
		url: '15info01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'IC_PUBLICACION'},
			{name: 'IC_TIPO_PUBLICACION'},
			{name: 'CG_NOMBRE'},
			{name: 'FCH_INI'},
			{name: 'FCH_FIN'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});

	var grid = new Ext.grid.GridPanel({
		store:consultaData,		
		columns: [
			{
				xtype: 'actioncolumn',
				header: 'Nombre',
				tooltip: 'Nombre',
				sortable: true,
				dataIndex: 'CG_NOMBRE' ,			
				width: 300,				
				align: 'left',
				hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {				 	
					return value;									
				},
				items: [
					//cuando el tipo publicacion es diferente de 1
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							var tipoPublicacion =registro.get('IC_TIPO_PUBLICACION');
							if(tipoPublicacion !=1) {
									this.items[0].tooltip = 'Ver';
									return 'icoContinuar';	
							}
						},	
						handler: Verdatos
					},
					//cuando el tipo publicacion es igual a 1
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							var tipoPublicacion =registro.get('IC_TIPO_PUBLICACION');
							if(tipoPublicacion ==1) {						
									this.items[0].tooltip = 'Ver';
									return 'icoContinuar';			
							}
						},	
						handler: VerdatosT
					}
				]				
			},
			{
				header: 'Vigencia de', 
				tooltip: 'Inicio Vegencia',
				sortable: true,
				dataIndex: 'FCH_INI',
				width: 200,	
				align: 'center',
				hidden: false
			},
			{
				header: 'Vigencia hasta',
				tooltip: 'Final Vigencia',
				sortable: true,
				dataIndex: 'FCH_FIN',
				hidden: false,
				width: 200,	
				align: 'center',
				editor: { 
				 xtype : 'numberfield', 
				 maxValue : 999999999999.99, 
				 allowBlank: false 
				   }    
			}			
		],
	margins: '20 0 0 0',
	stripeRows: true,
	loadMask: true,
	frame: true,
	width: 700,
	height: 400,
	style: 'margin:0 auto;',
	title: 'Información para proveedores',
	hidden: false,
	bbar: {
		items: [
			'->',
			'-',
			{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnGenerarPDF',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					var cboEpo = Ext.getCmp("cboEpo");
					Ext.Ajax.request({
						url: '15info01ext.data.jsp',
						params: {
							informacion: 'ArchivoPDF',
							ic_epo: cboEpo.getValue()
						},
						callback: procesarSuccessFailureGenerarPDF
					});
				}
			},
			{
				xtype: 'button',
				text: 'Bajar PDF',
				id: 'btnBajarPDF',
				hidden: true
			},
			'-'
		]
	}
	});
	

	var elementosForma = [
	{
		xtype: 'combo',
		name: 'ic_epo',
		id: 'cboEpo',
		allowBlank: false,
		fieldLabel: 'Nombre de la EPO',
		mode: 'local',
		displayField: 'descripcion',
		valueField: 'clave',
		hiddenName: 'ic_epo',
		emptyText: 'Seleccione un EPO',
		forceSeleccion: true,
		triggerAction: 'all',
		typeAhead: true,
		minChars: 1,
		store: catalogoSimpleData,
		tpl: NE.util.templateMensajeCargaCombo
	}
	];
//----------------------------PRINCIPAL--------------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 885, 
		title: 'Información ',
		style: 'margin:0 auto;',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarjet: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid:true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoConsultar',
				formBind: true,
				handler: function(boton, evento) {
					fp.el.mask('Enviando...','x-msk-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(), {
							operacion: 'Generar',
							start: 0,
							limit: 5
						})
					});
					Ext.getCmp('btnGenerarPDF').enable();
					Ext.getCmp('btnBajarPDF').hide();
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					fp.getForm().reset();
					grid.hide();
				}
			}
		]	
	});
	
	
	var formaDatos = new Ext.form.FormPanel({
		id: 'formaDatos',
		hidden: true,
		height: 'auto',
		width: 949,
		title: 'Información Diversos',
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		frame: true,
		monitorValid: true
	});
	
	
	var pnl = new Ext.Container ({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			fp,
			formaDatos, 
			NE.util.getEspaciador(20),
			grid				
		]
	});
	
	//Petición para obtener valores iniciales
	Ext.Ajax.request({
		url: '15info01ext.data.jsp',
		params: {
			informacion: "ValoresIniciales"
		},
		callback: procesaValoresIniciales
	});
//--------------------------------------------------------------------------
});