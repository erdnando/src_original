<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		java.io.*,
		javax.naming.*,	
		java.util.Date.*,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.UtilUsr,		
		com.netro.cadenas.*,
		net.sf.json.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
	<%@ include file="/appComun.jspf"%>
	<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
	
	<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String cboEpo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	String infoRegresar="";
	
	if(informacion.equals("ValoresIniciales"))
	{
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("Usuario", strTipoUsuario);
		infoRegresar = jsonObj.toString();
	}
	else if(informacion.equals("CatalogoSimple")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_epo");
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		infoRegresar = cat.getJSONElementos();
		}
	else if (informacion.equals("Consulta") ||
					 informacion.equals("ArchivoPDF")){
		
		com.netro.cadenas.ConsInfoDiversa paginador = new com.netro.cadenas.ConsInfoDiversa();
		
		if (strTipoUsuario.equals("NAFIN"))
			{paginador.setClaveEPO(cboEpo);}
		else
			{paginador.setClavePyme(iNoCliente);}
		
		paginador.setTipoUsuario(strTipoUsuario);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
			
		if(informacion.equals("Consulta")){
			try {
				Registros reg	=	queryHelper.doSearch();
				infoRegresar	=	
					"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
			}
		}
		else if (informacion.equals("ArchivoPDF")){
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
				}catch(Throwable e){
					throw new AppException("Error al generar el archivo PDF",e);
				}
			}
		}else  if (informacion.equals("DATOSEDITABLES")  ||   informacion.equals("DATOSTOTALES")  ){		
		
			String publicacion = (request.getParameter("publicacion")!=null)?request.getParameter("publicacion"):"";
			
			StringBuffer 	qrySentencia	= new StringBuffer();
			AccesoDB con		= new AccesoDB();
			ResultSet rs		= null;
			PreparedStatement ps	= null;
			ResultSet rsE		= null;
			PreparedStatement psE	= null;
			ResultSet rsD		= null;
			PreparedStatement psD	= null;			

			int x =0;
			boolean mbOpcionSalvar = false,
			msExistEditab  = false;
					
			//esta es para saber si la tabla existe 
			CImportaReg impTab       = new CImportaReg();
			msExistEditab = impTab.existeTabla( publicacion, "E" );			
			String tiposdDato ="", nombreCampo ="", tipoDato ="", tipo_atrib ="",
			tipoAtributo="", longitud ="", ordenamiento ="", cs_total =""; 
			
			StringBuffer  columnasStore = new StringBuffer();
			StringBuffer 	columnasRecords = new StringBuffer();
			StringBuffer  columnasGridX = new StringBuffer();			
			 //aqui se obtienen los registeros 			
			StringBuffer  msQryTotDatos = new StringBuffer();
			StringBuffer  msQryDatos = new StringBuffer();
			msQryTotDatos.append("SELECT ");
			msQryDatos.append("SELECT F.id, F.campo0");							
			String lsNombreCampo  =""; 
			boolean lbCambioCampoEdi = false;
			int  miNumCampo  = 0,	  miNumCampoTot = 0;				
			
			List tipoAtributoL = new ArrayList();
			List nombreCampoL = new ArrayList();
			List tipoDatoL = new ArrayList();
			List longitudL = new ArrayList();
			List tipo_atribL = new ArrayList();			
			List totalesL = new ArrayList();	
			List nomCampoL = new ArrayList();				
			StringBuffer  camposEditables = new StringBuffer();
			StringBuffer  camposFechas = new StringBuffer();
			//para los totales 
			StringBuffer  columnasGridTotales = new StringBuffer();
			StringBuffer  columnasStoreTotales = new StringBuffer();
			StringBuffer 	columnasRecordsTotales = new StringBuffer();
			int camp =0;
			try{
				con.conexionDB();
				
				/********************************************************/
				/* Se Agregran los Titulos */
				/********************************************************/
				qrySentencia.append( " SELECT cg_nombre_atrib, cg_tipo_dato,  cs_tipo_atrib," );
				qrySentencia.append( " ig_long_atrib , ig_long_atrib_d ," );
				qrySentencia.append( " ig_ordenamiento, cs_total");
				qrySentencia.append( " FROM com_tabla_pub_atrib ");
				qrySentencia.append( " WHERE ic_publicacion = "+publicacion);
				qrySentencia.append( " ORDER BY cs_tipo_atrib DESC, ic_atributo ");
				
				System.out.println("qrySentencia.toString() "+qrySentencia.toString());
				
				ps = con.queryPrecompilado(qrySentencia.toString());
				rs = ps.executeQuery();	
			
				columnasGridX.append("	[  { ");
				columnasGridX.append("	xtype: 'checkcolumn', ");
				columnasGridX.append("	header: 'Contestar', ");
				columnasGridX.append("	tooltip: 'Contestar', ");
				columnasGridX.append("	dataIndex: 'SELECCIONAR', ");
				columnasGridX.append("	sortable: true, ");
				columnasGridX.append("	resizable: true	, ");
				columnasGridX.append("	width: 130,		 ");		
				columnasGridX.append("	align: 'center' ");
				columnasGridX.append("	},  ");	
				
				columnasGridX.append("	{ 	header: 'Proveedor / Distribuidor', ");
				columnasGridX.append("		tooltip: 'Proveedor / Distribuidor', ");
				columnasGridX.append("		dataIndex: 'PROVEEDOR', ");
				columnasGridX.append("		sortable: true, ");
				columnasGridX.append("		resizable: true	, ");
				columnasGridX.append("		width: 130,		 ");		
				columnasGridX.append("		align: 'left' ");
				columnasGridX.append("	}, ");	
				

				columnasGridX.append("	{ ");
				columnasGridX.append("		header: 'IC_PUBLICACION', ");
				columnasGridX.append("		tooltip: 'IC_PUBLICACION', ");
				columnasGridX.append("		dataIndex: 'IC_PUBLICACION', ");
				columnasGridX.append("		sortable: true, ");
				columnasGridX.append("		resizable: true	, ");
				columnasGridX.append(" 		hidden: true,	 ");
				columnasGridX.append("		width: 130,		 ");		
				columnasGridX.append("		align: 'center' ");
				columnasGridX.append("	},  ");	
				

				columnasGridX.append("	 { ");
				columnasGridX.append("		header: 'NID', ");
				columnasGridX.append("		tooltip: 'NID', ");
				columnasGridX.append("		dataIndex: 'NID', ");
				columnasGridX.append("		sortable: true, ");
				columnasGridX.append("		resizable: true	, ");
				columnasGridX.append("		width: 130,		 ");	
				columnasGridX.append(" 		hidden: true,	 ");
				columnasGridX.append("		align: 'left' ");
				columnasGridX.append("	}, ");	
					
					
				columnasStore.append(" [  {	name: 'SELECCIONAR', mapping : 'SELECCIONAR'	 },");						
				columnasStore.append(" {  name: 'PROVEEDOR' , mapping : 'PROVEEDOR' },");
				columnasStore.append(" {  name: 'IC_PUBLICACION' , mapping : 'IC_PUBLICACION' },");
				columnasStore.append(" {  name: 'NID' , mapping : 'NID' },");
													
				while(rs.next()) {	
					x++;	
					tipoAtributo = rs.getString("cs_tipo_atrib");
					nombreCampo = rs.getString("cg_nombre_atrib");
					tipoDato = rs.getString("cg_tipo_dato");
					longitud = rs.getString("ig_long_atrib");	
					tipo_atrib = rs.getString("cs_tipo_atrib");	
					cs_total = rs.getString("cs_total");	
					String nomCampo = "CAMPO"+x;
					
					if(tipo_atrib.equals("E") && !tipoDato.equals("seleccion") ){
						camposEditables.append(nomCampo+",");								
					}
										
					if(tipo_atrib.equals("F"))  {  //no editable 
						tiposdDato = "label"; 
						lsNombreCampo = "F.campo";
					}
					if(tipo_atrib.equals("E"))  {   //editable 
						lsNombreCampo = "E.campoe";
						if ( !lbCambioCampoEdi )	{
							miNumCampo = -1;
							lbCambioCampoEdi = true;
						}
						
						
						if(tipoDato.equals("alfanumerico"))  {  tiposdDato = "textfield"; }
						if(tipoDato.equals("numerico"))  {  tiposdDato = "numberfield"; }
						if(tipoDato.equals("hora"))  {  tiposdDato = "Time"; }
						if(tipoDato.equals("fecha"))  {  tiposdDato = "datefield"; }
						if(tipoDato.equals("seleccion"))  {  tiposdDato = "checkcolumn"; }
						
					}
					if(!tipoDato.equals("seleccion")){
						columnasGridX.append("	{ ");
						columnasGridX.append("		header: '"+nombreCampo+"', ");
						columnasGridX.append("		tooltip: '"+nombreCampo+"', ");
						columnasGridX.append("		dataIndex: '"+nomCampo+"', ");
						columnasGridX.append("		sortable: true, ");
						columnasGridX.append("		resizable: true	, ");
						columnasGridX.append("		width: 130,		 ");		
						columnasGridX.append("		align: 'left' ");							
					
						if(tipo_atrib.equals("E") && tipoDato.equals("fecha")  )  {   //editable 							
							columnasGridX.append(" , editor: { ");
							columnasGridX.append(" xtype : 'datefield', ");						
							columnasGridX.append(" startDay: 0, ");							
							columnasGridX.append(" format: 'm/d/Y'  ");
							columnasGridX.append(" } ");
							//columnasGridX.append(" , renderer: Ext.util.Format.dateRenderer('d/m/Y') ");
							camposFechas.append(nomCampo+",");							
						}					
						
						if(tipo_atrib.equals("E") && tipoDato.equals("hora")  )  {   //editable 
							columnasGridX.append(" , editor: { ");
							columnasGridX.append("  xtype : 'timefield', ");						
							columnasGridX.append("  minValue: '1:00',   ");							
							columnasGridX.append("  maxValue: '23:00',  ");
							columnasGridX.append("  increment: 60  ");
							columnasGridX.append(" } ");							
						}					
						
						if(tipo_atrib.equals("E") && tipoDato.equals("alfanumerico")  )  {   //editable 
							columnasGridX.append(" , editor: { ");
							columnasGridX.append(" xtype : 'textfield', ");						
							columnasGridX.append(" allowBlank: false,  ");
							columnasGridX.append(" maxLength: "+longitud);	
							columnasGridX.append(" } ");
						}
						if(tipo_atrib.equals("E") && tipoDato.equals("numerico")  )  {   //editable 
							columnasGridX.append(" , editor: { ");
							columnasGridX.append(" xtype : 'numberfield', ");
							columnasGridX.append(" maxValue : 999999999999.99, ");
							columnasGridX.append(" allowBlank: false,  ");
							columnasGridX.append(" maxLength: "+longitud);	
							columnasGridX.append(" } ");
						}												
						columnasGridX.append(" },");	
						
					}else if(tipoDato.equals("seleccion")) { //para cuando es un check el campo 
					
						columnasGridX.append("	{ ");
						columnasGridX.append("	 xtype: 'checkcolumn', ");
						columnasGridX.append(" header : '"+nombreCampo+"',");
						columnasGridX.append(" tooltip: '"+nombreCampo+"',");
						columnasGridX.append(" dataIndex : '"+nomCampo+"',");
						columnasGridX.append(" width : 100,");
						columnasGridX.append("	align: 'center',  ");
						columnasGridX.append(" sortable : false");
						columnasGridX.append("		},");
				
					}
					columnasStore.append(" {  name: '"+nomCampo+"',  mapping : '"+nomCampo+"' },");
					
					//esto se usara posteriormente para  mostrar los datos y totales	
					tipoAtributoL.add(tipoAtributo);
					nombreCampoL.add(nombreCampo);
					tipoDatoL.add(tiposdDato);
					longitudL.add(longitud);
					tipo_atribL.add(tipo_atrib);					
					totalesL.add(cs_total);
					nomCampoL.add(nomCampo);
						
					lsNombreCampo += String.valueOf(miNumCampo+1);
						
					//esta parte es para los  obtener los totales y registros a mostrar
					//if ( rs.getInt("ig_ordenamiento") > 0 ) 
					
									
					
					if ( rs.getString("cg_tipo_dato").equals("hora") )	{
						msQryDatos.append(", TO_CHAR("+ lsNombreCampo + ", 'hh24:mm')");
						msQryTotDatos.append("COUNT(" + lsNombreCampo + "), ");
					}
					else if ( rs.getString("cg_tipo_dato").equals("fecha") )	{
						msQryDatos.append(", TO_CHAR("+ lsNombreCampo + ", 'dd/mm/yyyy')");
						msQryTotDatos.append("COUNT(" + lsNombreCampo + "), ");
					}
					else if ( rs.getString("cg_tipo_dato").equals("numerico") )	{
						msQryDatos.append(", "+ lsNombreCampo);
						msQryTotDatos.append("NVL( SUM(" + lsNombreCampo + "), 0), ");
					}
					else	{
						msQryDatos.append(", "+ lsNombreCampo);
						msQryTotDatos.append("COUNT(" + lsNombreCampo + "), ");
					}
					
					miNumCampo++;
					miNumCampoTot++;		
				
				} //termina while de los titulos 
				rs.close();
				ps.close();

				columnasGridX.append(" 	{  ");
				columnasGridX.append(" xtype: 'actioncolumn', ");
				columnasGridX.append(" header: 'DETALLES', ");
				columnasGridX.append(" tooltip: 'DETALLES', ");						
				columnasGridX.append(" width: 130, ");
				columnasGridX.append(" align: 'center' ");					
				columnasGridX.append(" , items: [ ");
				columnasGridX.append("	{ ");
				columnasGridX.append("		getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {");
				columnasGridX.append("			this.items[0].tooltip = 'Ver'; ");
				columnasGridX.append("			return 'iconoLupa';		");			 					
				columnasGridX.append("		}	");
				columnasGridX.append(" , handler:  VerDetalles  ");											
				columnasGridX.append(" }	");
				columnasGridX.append(" ]		");		
				columnasGridX.append(" } ");				
				columnasStore.append(" {  name: 'DETALLES' , mapping : 'DETALLES' } ");					
				columnasStore.append("	] ");			
				
				columnasGridX.append("	] ");			
				
				//quieres para obtener los datos a mostrar y para los totales 
				if ( msExistEditab )	{						
					msQryDatos.append( ", E.id AS id_edit " +
								  "	FROM tabla_" + publicacion + " F, " +
								  "		tabla_edit_" + publicacion + " E " +
								  " WHERE F.id = E.id(+) " +
								  "		AND F.campo0 = (SELECT rel.cg_pyme_epo_interno " +
								  "						FROM comrel_pyme_epo rel " +
								  "						WHERE rel.ic_epo = "+iNoEPO+"  "+
								  "						      AND rel.ic_pyme = " + iNoCliente + ") " +
								  "		AND E.ic_pyme(+) = " + iNoCliente +  " ");
					
					msQryTotDatos.append( "0 AS ultimo FROM tabla_" + publicacion + " F, " +
							  "		tabla_edit_" + publicacion + " E " +
							  " WHERE F.id = E.id(+) " +
							  "		AND F.campo0 = (SELECT rel.cg_pyme_epo_interno " +
							  "						FROM comrel_pyme_epo rel " +
							  "						WHERE rel.ic_epo = "+iNoEPO+"  "+
							  "						      AND rel.ic_pyme = " + iNoCliente + ") " +
							  "		AND E.ic_pyme(+) = " + iNoCliente +  " ");
				}	else	{					
					msQryDatos.append(" " +
								  "	FROM tabla_" + publicacion + " F " +
								  " WHERE 	F.campo0 = (SELECT rel.cg_pyme_epo_interno " +
								  "						FROM comrel_pyme_epo rel " +
								  "						WHERE rel.ic_epo = "+iNoEPO+"  "+
								  "						      AND rel.ic_pyme = " + iNoCliente + ")");
					
					
					msQryTotDatos.append( "0 AS ultimo FROM tabla_" + publicacion + " F " +
							  " WHERE F.campo0 = (SELECT rel.cg_pyme_epo_interno " +
							  "						FROM comrel_pyme_epo rel " +
							  "						WHERE rel.ic_epo = "+iNoEPO+"  "+
							  "						      AND rel.ic_pyme = " + iNoCliente + ") ");
				}
			
			
			
				
				/********************************************************/
				/* Se Agregran los  valores de los registros editables y  no editables  */
				/********************************************************/
				System.out.println("msQryDatos.toString() "+msQryDatos.toString());
				
				psD = con.queryPrecompilado(msQryDatos.toString());
				rsD = psD.executeQuery();	
				int y = 0;
				int totalNumeros = x+3;		
				List NoEditables  = new ArrayList();			
				int v =0;
				columnasRecords.append("  [ ");
				while(rsD.next()) {	
					y++;
					String valore =  rsD.getString(2);
					String NId =  rsD.getString(1);					
					
					columnasRecords.append("{ SELECCIONAR :false ,  ");
					columnasRecords.append("  PROVEEDOR :'"+valore+"' ,  ");
					columnasRecords.append("  NID :'"+NId+"' ,  ");
					columnasRecords.append(" IC_PUBLICACION :'"+publicacion+"' ,  ");
					
					v =0;
					for (int i=0; i<x; i++)	{
						v++;
						String tipdDato = (String)tipoDatoL.get(y);
						String nombCampo = (String)nombreCampoL.get(i);
						String longi = (String)longitudL.get(i);
						String atributos  = (String)tipo_atribL.get(i);						
						String value =  rsD.getString(i+3);						
						String nomCampo = "CAMPO"+v;					
						if(value==null) value ="";
						columnasRecords.append(nomCampo+":'"+value+"' ,  "); //aqui agrego el valor de regitros 
						
											
						if(atributos.equals("E")  && !value.equals("")) {
							NoEditables.add(value);
						}
							
								
					}//termina el for 
					
					columnasRecords.append(" DETALLES :'DETALLES'  }  ,"); //
				}
				rsD.close();
				psD.close();
				
				columnasRecords.deleteCharAt(columnasRecords.length()-1);
				columnasRecords.append(" ] ");	
			
						
						
							
				/********************************************************/
				/* Se Agregran los totales */
				/********************************************************/
							
				columnasGridTotales.append("	[  { ");
				columnasGridTotales.append("		header: '', ");
				columnasGridTotales.append("		tooltip: '', ");
				columnasGridTotales.append("		dataIndex: 'TOTALES', ");
				columnasGridTotales.append("		sortable: true, ");
				columnasGridTotales.append("		resizable: true	, ");
				columnasGridTotales.append("		width: 130,		 ");		
				columnasGridTotales.append("		align: 'left' ");
				columnasGridTotales.append("		},");	
	
				columnasStoreTotales.append(" [ {  name: 'TOTALES' , mapping : 'TOTALES' },");
				
				columnasRecordsTotales.append(" [  { TOTALES :'Totales' ,  ");
				
				System.out.println("msQryTotDatos.toString() "+msQryTotDatos.toString());
				psE = con.queryPrecompilado(msQryTotDatos.toString());
				rsE = psE.executeQuery();	
				String datotales  = "";				
				while(rsE.next()) {	
					for (int i=0; i<totalesL.size(); i++)	{
						String valorT= (String)totalesL.get(i);
						boolean lbTotal = ((valorT).equals("S")) ? true : false;
						 datotales  = "";
						String 	nomCampoL1 = (String)nomCampoL.get(i); 
						
						columnasGridTotales.append("	{ ");
						columnasGridTotales.append("		header: '', ");
						columnasGridTotales.append("		tooltip: '', ");
						columnasGridTotales.append("		dataIndex: '"+nomCampoL1+"', ");
						columnasGridTotales.append("		sortable: true, ");
						columnasGridTotales.append("		resizable: true	, ");
						columnasGridTotales.append("		width: 130,		 ");		
						columnasGridTotales.append("		align: 'left' ");
						columnasGridTotales.append("		},");
					
						columnasStoreTotales.append(" {  name: '"+nomCampoL1+"' , mapping : '"+nomCampoL1+"' },");
					
						if(lbTotal==true) {
							datotales  = rsE.getString(i+1);							
							columnasRecordsTotales.append(nomCampoL1+":'"+datotales+"' ,"); 						
							
						}else {			
							columnasRecordsTotales.append(nomCampoL1+":'' ,"); 
						
						}
					}
				}
				rsE.close();
				psE.close();
			
			
			
			columnasGridTotales.deleteCharAt(columnasGridTotales.length()-1);
			columnasGridTotales.append(" ] ");	
			
			columnasStoreTotales.deleteCharAt(columnasStoreTotales.length()-1);
			columnasStoreTotales.append(" ] ");	
			
			
			columnasRecordsTotales.deleteCharAt(columnasRecordsTotales.length()-1);
			columnasRecordsTotales.append("  }  ] ");	
			
			String campos ="'"+camposEditables+"'";	 
			String campoFechas ="'"+camposFechas+"'";	 
			
			System.out.println("camposEditables "+camposEditables);
			System.out.println("camposFechas "+camposFechas);
			
			if(!camposEditables.toString().equals("")){
			camposEditables.deleteCharAt(camposEditables.length()-1);
			 campos="'"+camposEditables+"'";		
			}
			
			if(!camposFechas.toString().equals("")){
				camposFechas.deleteCharAt(camposFechas.length()-1);
				campoFechas =  campos="'"+camposFechas+"'";	
			}
			

				
		if (informacion.equals("DATOSEDITABLES")  ){				
					
				infoRegresar += "{\"success\": true, \"columnasGrid\": " + columnasGridX.toString() + 
				", \"columnasStore\": " + columnasStore.toString() +
				", \"publicacion\": " + publicacion +									
				", \"camposEditables\": " + campos +
				", \"campoFechas\": " + campoFechas +	
				", \"noEditables\": " + NoEditables.size() +				
				", \"noCampos\": " + y +				
				", \"columnasRecords\": {\"registros\": " + columnasRecords.toString() +", \"total\":"+y+" }  }";		
				
				
		
		}else if ( informacion.equals("DATOSTOTALES")  ){	
			infoRegresar += "{\"success\": true, \"columnasGridTotales\": " + columnasGridTotales.toString() + 
				", \"columnasStoreTotales\": " + columnasStoreTotales.toString() +						
				", \"columnasRecordsTotales\": {\"registros\": " + columnasRecordsTotales.toString() +", \"total\":1 }  }";								
		}
		
			}catch (Exception e){
				e.printStackTrace();
				throw new Exception("SIST0001");
			}finally{
				if(con.hayConexionAbierta()) con.cierraConexionDB();
				System.out.println("getInfoDiversa(S)");
			}		
			
			
	}else  if (informacion.equals("ConfirmacionClaves") ) {

	String msUsuario 	= (request.getParameter("cesionUser")==null)?"":request.getParameter("cesionUser");
	String msContrasena 	= (request.getParameter("cesionPassword")==null)?"":request.getParameter("cesionPassword");
	boolean		bConfirma = false;	

	
	if(!msUsuario.equals("") && !msContrasena.equals("")) {
		UtilUsr utils = new UtilUsr();
		boolean usaurioOK = utils.esUsuarioValido(msUsuario, msContrasena);
		if(usaurioOK) {bConfirma = true; }							
		if(!usaurioOK) {bConfirma = false; }	
			
	}
			
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("bConfirma", new Boolean(bConfirma));	
	infoRegresar = jsonObj.toString();	
	
	
	}else  if (informacion.equals("GuardarRespuesta") ) {
	ITablasDinamicas tablas = ServiceLocator.getInstance().lookup("TablasDinamicasEJB",ITablasDinamicas.class);
	
	
	String publicacion = (request.getParameter("publicacion")!=null)?request.getParameter("publicacion"):"";
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");

	List arrRegistrosModificados = JSONArray.fromObject(jsonRegistros);
	
	
	
	StringBuffer 	qrySentencia	= new StringBuffer();
	AccesoDB con		= new AccesoDB();
	ResultSet rs		= null;
	PreparedStatement ps	= null;
	String msQuerysX[]	= new String[arrRegistrosModificados.size()];
	String msNumIdsX[]	= new String[arrRegistrosModificados.size()];
	StringBuffer 	msQuerys	= new StringBuffer();
	StringBuffer 	msNumIds	= new StringBuffer();

	
	List nomCampoL = new ArrayList();
	List tipoDatoL = new ArrayList();
	
	
	qrySentencia.append( " SELECT cg_nombre_atrib, cg_tipo_dato,  cs_tipo_atrib," );
	qrySentencia.append( " ig_long_atrib , ig_long_atrib_d ," );
	qrySentencia.append( " ig_ordenamiento, cs_total");
	qrySentencia.append( " FROM com_tabla_pub_atrib ");
	qrySentencia.append( " WHERE ic_publicacion = "+publicacion);
	qrySentencia.append( " ORDER BY cs_tipo_atrib DESC, ic_atributo ");
		
	try{
		con.conexionDB();			
		ps = con.queryPrecompilado(qrySentencia.toString());
		rs = ps.executeQuery();	
		int  x =0, index = 0;
	
		
		while(rs.next()) {	
			x++;					
			String	tipoDato = rs.getString("cg_tipo_dato");
			String	tipo_atrib = rs.getString("cs_tipo_atrib");						
			if(tipo_atrib.equals("E"))  {   
			String	 nomCampo  = "CAMPO"+x;					
				nomCampoL.add(nomCampo);
				tipoDatoL.add(tipoDato);
			}				
		}		
		rs.close();
		ps.close();
	
		
		Iterator itReg = arrRegistrosModificados.iterator();
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();
			
			String  ids = registro.getString("NID");
			msNumIds.append(ids);	
			
			msQuerys	= new StringBuffer();
			
			for (int j=0; j<nomCampoL.size(); j++ )	{
				String 	nomCampo = (String)nomCampoL.get(j);
				String 	tipoDato = (String)tipoDatoL.get(j);
				
				String  valor = registro.getString(nomCampo);
								
				if(tipoDato.equals("seleccion") && valor.equals("true") )  {					
					valor = "S";													
				}else if(tipoDato.equals("seleccion") && ( valor.equals("false") ||  valor.equals("") ) )  {					
					valor = "N";							
				}
				
				if(tipoDato.equals("seleccion")  )  { 
					msQuerys.append("'" + valor + "',");
				}				
				if(tipoDato.equals("alfanumerico")  )  { 
					msQuerys.append( "'" + valor + "',");
				}
				if(tipoDato.equals("numerico"))  { 
					msQuerys.append(valor+",");  
				}
				if(tipoDato.equals("hora"))  { 
					valor = 	valor.replaceAll("AM", "").replaceAll("PM", "");
					msQuerys.append( " TO_DATE('"+valor+"', 'hh24:mi') ,");
				}
				if(tipoDato.equals("fecha"))  {  
									
					msQuerys.append( "TO_DATE('"+valor+"', 'dd/mm/yyyy') ,");	
				}	
		
			}//for 
						
			msQuerysX [index] =  msQuerys.toString();
			msNumIdsX [index] =  ids;
			index ++;
		}//while
			
		
			
					
	}catch (Exception e){
		e.printStackTrace();
		throw new Exception("SIST0001");
	}finally{
		if(con.hayConexionAbierta()) con.cierraConexionDB();
		System.out.println("getInfoDiversa(S)");
	}
		
		
				 
	tablas.guardaRespuestaPymeExt(msQuerysX, msNumIdsX, iNoCliente, publicacion);
	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));	
	infoRegresar = jsonObj.toString();	
	
	
	}	
	%>
	<%=infoRegresar%>	