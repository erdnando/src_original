<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		java.io.*,
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../015secsession.jspf" %>
<%
	String publicacion = (request.getParameter("publicacion")!=null)?request.getParameter("publicacion"):"";
	String CadenasEpos = (request.getParameter("CadenasEpos") == null) ? "" : request.getParameter("CadenasEpos");
	String  contenido = "", rutaImagen = "", rutaArchivo ="";
	
	System.out.println(" publicacion ---->"+publicacion);
	System.out.println(" CadenasEpos ---->"+CadenasEpos);	
	
	List datos = new ArrayList();
	InformacionTOIC informacionTOICBean = ServiceLocator.getInstance().lookup("InformacionTOICEJB",InformacionTOIC.class);
			
	
	datos.add(strTipoUsuario);
	datos.add(CadenasEpos);
	datos.add(iNoEPO);
	datos.add(String.valueOf(iNoCliente) );
	datos.add(String.valueOf(publicacion));
	
	List resultado = informacionTOICBean.getInfoDiversa(datos);		
	if(resultado.size() >0) {
				contenido  = (String)resultado.get(0);
				rutaImagen = (String)resultado.get(1);
				rutaArchivo = (String)resultado.get(2);		
	}
	String ruta = strDirecVirtualTrabajo +rutaImagen;

	System.out.println(" contenido ---->"+contenido);
	System.out.println(" rutaImagen ---->"+rutaImagen);
	System.out.println(" rutaArchivo ---->"+rutaArchivo);
%>
	<script language="JavaScript">	
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}
	</script>	
	<table width="400" cellpadding="0" cellspacing="0" border="0">	
		<tr>
			<td valign="top" align="left">
				<table cellpadding="0" cellspacing="0" border="0">		
					<tr>
						<td align="center" class="formas"><br>
							<table width="400" cellpadding="3" cellspacing="0" border="0" class="formas">
								<tr>	
									<td align="center" class="formas"><%=contenido%></td>
									<td align="center" class="formas"><img src="<%=ruta%>" border="0" alt=""></td>
								</tr>
							</table>
						</td>
					</tr>
					<%if(!rutaArchivo.equals("")){ %>
					<tr>
						<td align="center"><br>
							<table cellpadding="1" cellspacing="2" border="0">
								<tr>
									<td align="center" class="celda02" height="25" width="50"><a href="<%=(strDirecVirtualTrabajo + rutaArchivo)%>" target="_new">Bajar Archivo</a></td>
								</tr>
							</table> 
						</td>
					</tr>		
					<%}%>
				</table>	
			</td>
		</tr>
	</table>
	