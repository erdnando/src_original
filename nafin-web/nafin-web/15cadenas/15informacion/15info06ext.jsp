<%@ page import="java.sql.*,java.io.*,netropology.utilerias.*,java.util.*"%>
<%@ include file="../015secsession.jspf" %>
<%
String 	msNumPublicacion = request.getParameter("num_pub"),
msNumIdPub       = request.getParameter("num_id"),
msOrdenCampDet[] = {"", "", ""};
ResultSet mCurGen;
AccesoDB  lobjConexion = new AccesoDB();
int miNumReg = 0;

System.out.println("msNumPublicacion ===="+msNumPublicacion);
System.out.println("msNumIdPub ===="+msNumIdPub);

try{
	lobjConexion.conexionDB();
%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=strDirecVirtualCSS%>css/<%=strClase%>">
<center>
<%
//	PIDIENDO TABLA DE DETALLE
if ( msNumIdPub != null )	{
				
	String 	lsCabecerasDetalle =""+
	" SELECT (SELECT cg_nombre_atrib FROM com_tabla_pub_atrib " +
	"	WHERE ic_publicacion = "+msNumPublicacion+" AND ic_atributo = 0) AS nom_col_fij, " +
	"	cg_nombre_atrib, cg_tipo_dato, cs_total, ig_ordenamiento " +
	" FROM com_tabla_pub_atrib_d " +
	" WHERE ic_publicacion = " + msNumPublicacion + " ORDER BY ic_atributo_d ",
	lsDatosDetalle     = "SELECT f.campo0, f.campo1, d.id, d.id_d",
	lsTotalesDetalle   = "SELECT ";
				
	Vector	lVecTipoDato   = new Vector(),
	lVecTotalesDet = new Vector();
	int		liNumcamposDet     = 0;
	
	mCurGen = lobjConexion.queryDB( lsCabecerasDetalle );
	
	if ( mCurGen.next() )	{%>
	<br><br>
	<span class="titulos" id="titulo_det">Consulta de <em>Campos de Detalle</em></span>
	<br><br>
	<table border="0"> 
		<tr> 
			<td>
			<table border="0">
			<!-- 
			/********************************************************************************************
			*	PINTANDO CABECERA DE DETALLES
			*   Y ARMADO QUERY DE VALORES
			******************************************************************************************** -->
				<tr>
					<th class="celda01" bgcolor="silver" align="center" width="50px">	N�</th>
					<th class="celda01" bgcolor="silver" align="center" width="50px">	Proveedor / Distribuidor</th>
					<th class="celda01" bgcolor="silver" align="center" width="50px">	<%=mCurGen.getString("nom_col_fij")%>	</th>
					<%do {%>
					<th class="celda01" bgcolor="silver" align="center"><%=mCurGen.getString("cg_nombre_atrib")%>	</th>
					<%
						String lsTipoDatoTmp    = mCurGen.getString("cg_tipo_dato");
						String lsNombreCampoTmp = "d.campod" + liNumcamposDet;
						
						lVecTipoDato.addElement( mCurGen.getString("cg_tipo_dato") );
						lVecTotalesDet.addElement( mCurGen.getString("cs_total") );
						
						if (  mCurGen.getInt("ig_ordenamiento") > 0 )
							msOrdenCampDet[ mCurGen.getInt("ig_ordenamiento")-1 ] = lsNombreCampoTmp;
						
						
						if ( lsTipoDatoTmp.equals("hora") )	{
							lsDatosDetalle   += ", TO_CHAR(" + lsNombreCampoTmp + ", 'hh24:mi') ";
							lsTotalesDetalle += "COUNT("+lsNombreCampoTmp+"), ";
						}
						else if ( lsTipoDatoTmp.equals("fecha") )	{
							lsDatosDetalle += ", TO_CHAR(" + lsNombreCampoTmp + ", 'dd/mm/yyyy') ";
							lsTotalesDetalle += "COUNT("+lsNombreCampoTmp+"), ";
						}
						else if ( lsTipoDatoTmp.equals("numerico") )	{
							lsDatosDetalle += ", " + lsNombreCampoTmp;
							lsTotalesDetalle += "NVL(SUM("+lsNombreCampoTmp+"), 0), ";
						}
						else	{
							lsDatosDetalle += ", " + lsNombreCampoTmp;
							lsTotalesDetalle += "COUNT("+lsNombreCampoTmp+"), ";
						}
						liNumcamposDet++;
					} while ( mCurGen.next() );
					
					lsDatosDetalle += " FROM tabla_d_" + msNumPublicacion + " d, " +
									  "		 tabla_" + msNumPublicacion + " f " +
									  " WHERE d.id = " + msNumIdPub + " " +
									  "		  AND f.id = d.id " +
									  " ORDER BY f.campo0";
					
					for (int i=0; i<=2; i++)
						if ( !msOrdenCampDet[i].equals("") )
							lsDatosDetalle += ", " + msOrdenCampDet[i];
					
					
					lsTotalesDetalle += "'0' AS vacio FROM tabla_d_" + msNumPublicacion + " d, " +
									  "		 tabla_" + msNumPublicacion + " f " +
									  " WHERE d.id = " + msNumIdPub + " " +
									  "		  AND f.id = d.id";
					
					%>
				</tr>
				<!-- 
				/********************************************************************************************
				*	PINTANDO DATOS DE LA TABLA
				******************************************************************************************** -->
				<%mCurGen = lobjConexion.queryDB( lsDatosDetalle );
				if ( mCurGen.next() )	{
					do {
						miNumReg = mCurGen.getRow();%>
				<tr>
					<td class="formas" align="center"> <%=miNumReg%> </td>
					<td class="celda01" align="center"> <%=mCurGen.getString(1)%> </td>
					<td class="celda01" align="center"> <%=mCurGen.getString(2)%> </td>
						<%for (int i=0; i<liNumcamposDet; i++)	{
								String 	lsDatoTmp  = mCurGen.getString(i+5),
								lsTipoDato = (String) lVecTipoDato.elementAt(i);
								if ( lsTipoDato.equals("seleccion") )
									if ( lsDatoTmp.equals("S") )
										lsDatoTmp = "<img src='/nafin/00utils/gif/chk_h.gif' border='0'>";
										else
										lsDatoTmp = "<img src='/nafin/00utils/gif/chk_d.gif' border='0'>";
									%>
						<td class="formas" align="center">&nbsp;&nbsp; <%=lsDatoTmp%> &nbsp;&nbsp;</td>
							<%}%>
						</tr>
						<%
						} while (mCurGen.next());
					}	else	{	// SI NO HAY REGISTROS%>
					<tr>
						<td colspan="<%=liNumcamposDet+3%>"><a> &nbsp;&nbsp;&nbsp;&nbsp; NO HAY REGISTROS DE DETALLE PARA ESTE CAMPO &nbsp;&nbsp;&nbsp;&nbsp; </a></td>
					</tr>
					<%}%>
	
					<!-- 
					/********************************************************************************************
					*	PINTANDO TOTALES
					******************************************************************************************** -->
					<tr>
						<td colspan="<%=liNumcamposDet+3%>"><hr></td>
					</tr>	
					<tr>
						<td class="formas" align="center"><strong>Totales</strong></td>
						<td class="formas" align="center">&nbsp;</td>
						<td class="formas" align="center">&nbsp;</td>
						<% mCurGen = lobjConexion.queryDB( lsTotalesDetalle );
							if ( mCurGen.next() )	{
								for (int i=0; i<liNumcamposDet; i++)	{
									boolean lbSePinta = ((String)lVecTotalesDet.elementAt(i)).equals("S") ? true : false;
									 %>
								<td class="formas" align="center"><strong> <%= lbSePinta ? mCurGen.getString(i+1) : "&nbsp;"%> </strong></td>
							<%	}
							}
						mCurGen.close();%>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<!--<td height="60px" valign="bottom" align="right" colspan="<%=liNumcamposDet+1%>"></td>-->
			</tr>
		</table>
		<%}else	{
					out.print("<a> NO HAY TABLA DE DETALLES. </a>");
		}
		mCurGen.close();		
	}	// IF DE ID DE DETALLE
	%>
	</center>

<%
}
catch(Exception err)	{
	out.print("ERROR: " + err.getMessage() );
}
finally	{
	lobjConexion.cierraConexionDB();
}
%>