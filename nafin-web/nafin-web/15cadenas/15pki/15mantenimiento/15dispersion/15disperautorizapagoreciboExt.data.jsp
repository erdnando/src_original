<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.math.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.dispersion.*, 
		com.netro.descuento.*,
		com.netro.exception.*,
		javax.naming.*,
		net.sf.json.*,
		com.netro.pdf.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%@ include file="/15cadenas/15pki/certificado.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String msgError = "";
try{

	if(informacion.equals("TransmitirDoctos")){
		
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		
		session.removeAttribute("15DISPERPAGORESUMEN_JSP_INSERTED");
		session.removeAttribute("15DISPERPAGORESUMEN_JSP_NUMERO_DE_RECIBO");
		session.removeAttribute("15DISPERPAGORESUMEN_JSP_ACUSE");
		session.removeAttribute("15DISPERPAGORESUMEN_JSP_HORA_AUTORIZACION");
		
		JSONObject jsonObj				=  new JSONObject();
		String objGral						= (request.getParameter("objGral") == null)?"":request.getParameter("objGral");
		String TextoFirmado				= (request.getParameter("TextoFirmado") == null)?"":request.getParameter("TextoFirmado");
		String Pkcs7 						= (request.getParameter("Pkcs7") == null)?"":request.getParameter("Pkcs7");
		String fechaDeVencimiento		= (request.getParameter("fechaDeVencimiento") == null)?"":request.getParameter("fechaDeVencimiento");
		
		boolean	seHanInsertadoDatos	= (session.getAttribute("15DISPERPAGORESUMEN_JSP_INSERTED") != null && ((String)session.getAttribute("15DISPERPAGORESUMEN_JSP_INSERTED")).equals("true"))?true:false;
		String 	numeroDeRecibo 		= !seHanInsertadoDatos?"":(String)session.getAttribute("15DISPERPAGORESUMEN_JSP_NUMERO_DE_RECIBO");
		String	numeroDeAcuse			= !seHanInsertadoDatos?"":(String)session.getAttribute("15DISPERPAGORESUMEN_JSP_ACUSE");
		String	horaDeAutorizacion	= !seHanInsertadoDatos?"":(String)session.getAttribute("15DISPERPAGORESUMEN_JSP_HORA_AUTORIZACION");
		JSONObject jsObjGral				= JSONObject.fromObject(objGral);
		
		
		HashMap 	resultado			= new HashMap();
		String	serial				= strSerial;
		String 	claveCliente		= iNoCliente;
		
		String 	mensajeError		= "";
		String 	tipoFactoraje 		= "Normal";
		String 	fechaVencimiento 	= fechaDeVencimiento;
		String 	fechaEmision 		= jsObjGral.getString("fechaDeAutorizacion");
		String 	nombreDeDocumento = jsObjGral.getString("numeroDeFolio");
		String 	claveMoneda			= jsObjGral.getString("claveMoneda");
		String 	claveMonedaUSD		= jsObjGral.getString("claveMonedaUSD");
		String 	mesReferencia		= jsObjGral.getString("nombreDelMes");
		String 	monto					= jsObjGral.getString("importe");
		String 	montoUSD				= jsObjGral.getString("importeUSD");
		String nombreProveedor		= jsObjGral.getString("nombreEPO");
		String moneda					= jsObjGral.getString("descripcionMonedaNacional");
		String monedaUSD				= jsObjGral.getString("descripcionDolaresAmericanos");
		
		boolean 	montoMNEsCero						= jsObjGral.getBoolean("montoMNEsCero");
		boolean 	montoUSDEsCero						= jsObjGral.getBoolean("montoUSDEsCero");
		boolean	hayAutentificacionExitosa 		= false; 
		boolean estaParametrizadaDispersionMN	= jsObjGral.getBoolean("hayTarifaDispersionMonedaNacional");
		boolean estaParametrizadaDispersionUSD	= jsObjGral.getBoolean("hayTarifaDispersionDolaresAmericanos");
		
		String	fechaDeAutorizacion			= fechaEmision;
		String	usuario							= strLogin + " - " + strNombreUsuario;
		String 	archivoPDF 						= null; 
		String   claveEPO							= iNoCliente;
		HashMap 	documento 						= null; 
		HashMap	acuse								= null;
		String   claveUsuario					= iNoUsuario;

		// Realizar autenticacion
		if(!seHanInsertadoDatos){
			try{
				resultado 						= dispersion.autenticaFirmaDigitalv2(Pkcs7, TextoFirmado, serial, claveCliente);
				hayAutentificacionExitosa 	= ((String)resultado.get("VERIFICACION_EXITOSA")).equals("true")?true:false;
			}catch(Throwable t ){
				hayAutentificacionExitosa = false;
			}
		}else{
			hayAutentificacionExitosa  = true;
		}
		
		// Extraer numero de recibo, numero de acuse
		if(!seHanInsertadoDatos){
			if(hayAutentificacionExitosa){
				java.util.Date date = new java.util.Date();
				numeroDeRecibo 		= (String) resultado.get("RECIBO");
				numeroDeAcuse			= (String) resultado.get("FOLIO");
				horaDeAutorizacion	= (new SimpleDateFormat ("HH:mm:ss")).format(date);
				
				session.setAttribute("15DISPERPAGORESUMEN_JSP_INSERTED",					"true");
				session.setAttribute("15DISPERPAGORESUMEN_JSP_NUMERO_DE_RECIBO",		numeroDeRecibo);
				session.setAttribute("15DISPERPAGORESUMEN_JSP_ACUSE",						numeroDeAcuse);
				session.setAttribute("15DISPERPAGORESUMEN_JSP_HORA_AUTORIZACION",		horaDeAutorizacion);
				
			}else{ 
				mensajeError	= resultado.get("MENSAJE_ERROR")==null?"Error en el certificado seleccionado":(String) resultado.get("MENSAJE_ERROR");
			}
		}
		
		// Insertar documento y acuse 
		if(!seHanInsertadoDatos && hayAutentificacionExitosa ){
			
			documento = new HashMap();
		
			documento.put("IC_EPO",					claveEPO);
			documento.put("CC_ACUSE",				numeroDeAcuse);
			documento.put("IG_NUMERO_DOCTO", 	nombreDeDocumento);	
			documento.put("DF_FECHA_DOCTO",		fechaEmision + " " + horaDeAutorizacion);
			documento.put("DF_FECHA_VENC",		fechaVencimiento );
			documento.put("IC_MONEDA",				claveMoneda);
			documento.put("IC_MONEDA_USD",		claveMonedaUSD);
			documento.put("CS_DSCTO_ESPECIAL", 	"N");	
			documento.put("IC_ESTATUS_DOCTO", 	"2"); // Vencido	
			documento.put("CT_REFERENCIA", 		"Cargo por servicio de dispersion en moneda nacional del mes de " + mesReferencia );
			documento.put("CT_REFERENCIA_USD", 	"Cargo por servicio de dispersion en dolares americanos del mes de " + mesReferencia );
			documento.put("DF_ALTA", 				fechaEmision + " " + horaDeAutorizacion ); 
			documento.put("CS_CAMBIO_IMPORTE", 	"N");	
			if(estaParametrizadaDispersionMN && !montoMNEsCero){
				documento.put("FN_MONTO",				monto);//quitaFormatoNumerico(monto)
				documento.put("INSERTA_DOCTO_MN",	"true");
			}else{
				documento.put("FN_MONTO",				"0.00");
				documento.put("INSERTA_DOCTO_MN",	"false");
			}
			if(estaParametrizadaDispersionUSD && !montoUSDEsCero){
				documento.put("FN_MONTO_USD",	montoUSD); //quitaFormatoNumerico(montoUSD)
				documento.put("INSERTA_DOCTO_USD",	"true");
			}else{
				documento.put("FN_MONTO_USD",			"0.00");
				documento.put("INSERTA_DOCTO_USD",	"false");
			}
	
			acuse		= new HashMap();
			
			acuse.put("NUMERO_DE_ACUSE",		numeroDeAcuse);
			if(estaParametrizadaDispersionMN && !montoMNEsCero){
				acuse.put("NUMERO_DOCTOS_MN",		"1");
				acuse.put("MONTO_TOTAL_MN",		monto);//quitaFormatoNumerico(monto)
			}else{
				acuse.put("NUMERO_DOCTOS_MN",		"0");
				acuse.put("MONTO_TOTAL_MN",		"0.00");
			}
			if(estaParametrizadaDispersionUSD && !montoUSDEsCero){
				acuse.put("NUMERO_DOCTOS_DL",		"1");
				acuse.put("MONTO_TOTAL_DL",		montoUSD);//quitaFormatoNumerico(montoUSD)
			}else{
				acuse.put("NUMERO_DOCTOS_DL",		"0");
				acuse.put("MONTO_TOTAL_DL",		"0.00");	
			}
			acuse.put("CLAVE_USUARIO",		   claveUsuario);
			acuse.put("NUMERO_DE_RECIBO",		numeroDeRecibo);
			
			String clavePYME 	= dispersion.getClaveDelProveedorDeDispersion(claveEPO);
			
			dispersion.generaDocumentoParaCobroDelServicioDeDispersion(documento, acuse, clavePYME);
		}
		
		// Generar archivo PDF
		if(hayAutentificacionExitosa){
		
			HashMap cabecera = new HashMap();
			
			cabecera.put("PAIS",								(String)	session.getAttribute("strPais"));
			cabecera.put("NUM_NAFIN_ELECTRONICO",	   session.getAttribute("iNoNafinElectronico").toString());//(session.getAttribute("iNoNafinElectronico")==null?"":(String) session.getAttribute("iNoNafinElectronico")));
			cabecera.put("EXTERNO",							(String)	session.getAttribute("sesExterno"));
			cabecera.put("NOMBRE",							(String) session.getAttribute("strNombre"));
			cabecera.put("NOMBRE_USUARIO",				(String) session.getAttribute("strNombreUsuario"));
			cabecera.put("LOGO",								(String)	session.getAttribute("strLogo"));
			cabecera.put("DIRECTORIO_PUBLICACION",		strDirectorioPublicacion );
			
			List	  lista	  = new ArrayList();
			HashMap registro = null;
			
			if(estaParametrizadaDispersionMN && !montoMNEsCero){
				registro = new HashMap();
				registro.put("NOMBRE_PROVEEDOR",			nombreProveedor);
				registro.put("NOMBRE_DOCUMENTO",			nombreDeDocumento);
				registro.put("FECHA_EMISION",				fechaEmision);
				registro.put("FECHA_VENCIMIENTO",		fechaVencimiento);
				registro.put("MONEDA",						moneda);
				registro.put("TIPO_FACTORAJE",			tipoFactoraje);
				registro.put("MONTO",						monto);
				registro.put("REFERENCIA",					"Cargo por servicio de dispersión en moneda nacional del mes de " + mesReferencia);
				
				lista.add(registro);
			}
			
			if(estaParametrizadaDispersionUSD && !montoUSDEsCero){
				registro = new HashMap();
				registro.put("NOMBRE_PROVEEDOR",			nombreProveedor);
				registro.put("NOMBRE_DOCUMENTO",			nombreDeDocumento);
				registro.put("FECHA_EMISION",				fechaEmision);
				registro.put("FECHA_VENCIMIENTO",		fechaVencimiento);
				registro.put("MONEDA",						monedaUSD);
				registro.put("TIPO_FACTORAJE",			tipoFactoraje);
				registro.put("MONTO",						montoUSD);
				registro.put("REFERENCIA",					"Cargo por servicio de dispersión en dólares americanos del mes de " + mesReferencia);
				
				lista.add(registro);
			}
			
			HashMap recibo	= new HashMap();
			
			recibo.put("NUMERO_RECIBO",		numeroDeRecibo);
			recibo.put("NUMERO_ACUSE",			numeroDeAcuse);
			recibo.put("FECHA_EMISION",		fechaEmision);
			recibo.put("HORA_AUTORIZACION",	horaDeAutorizacion);
			recibo.put("USUARIO",				usuario);
		
			archivoPDF = getArchivoPDF(strDirectorioTemp, cabecera, lista, recibo);
			System.out.println("archivoPDF::"+archivoPDF);
			
		}

		jsonObj.put("numeroDeRecibo", numeroDeRecibo);
		jsonObj.put("numeroDeAcuse", numeroDeAcuse);
		jsonObj.put("horaDeAutorizacion", horaDeAutorizacion);
		jsonObj.put("fechaDeAutorizacion", fechaDeAutorizacion);
		jsonObj.put("usuario", usuario);
		jsonObj.put("mensajeError", mensajeError);
		jsonObj.put("urlArchivo", strDirecVirtualTemp+archivoPDF);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	}

}catch(Throwable t){
	t.printStackTrace();
	throw new AppException("Error en la peticion",t);
}finally{
	System.out.println("infoRegresar = " + infoRegresar);
}
%>

<%=infoRegresar%>

<%!
	public String getArchivoPDF(String directorio, HashMap cabecera, List registros, HashMap recibo)
		throws NafinException{
		
		System.out.println("15disperpagoresumen.jsp::getArchivoPDF(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	documentoPdf 	= null;
		
		try {
			archivo 			= new CreaArchivo();
			nombreArchivo 	= archivo.nombreArchivo()+".pdf";
			documentoPdf 	= new ComunesPDF(2,directorio+nombreArchivo, "", false, true, true);

			// Encabezado			
		documentoPdf.setEncabezado((String)	cabecera.get("PAIS"),
											(String) cabecera.get("NUM_NAFIN_ELECTRONICO"),
											(String) cabecera.get("EXTERNO"),
											(String) cabecera.get("NOMBRE"),
											(String) cabecera.get("NOMBRE_USUARIO"),
											(String) cabecera.get("LOGO"),
											(String) cabecera.get("DIRECTORIO_PUBLICACION"),
											"");
			
			// Encabezado con detalles de la autentificacion
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			
			if(recibo != null ){
				
				documentoPdf.addText("La autentificación se llevó a cabo con éxito","formasG",ComunesPDF.CENTER);
				documentoPdf.addText("Recibo: "+((String)recibo.get("NUMERO_RECIBO")),"formasG",ComunesPDF.CENTER);
			
				// Detalles del Acuse
				documentoPdf.setLTable(2,50,new float[]{25.00f,25.00f});
				documentoPdf.setLCell(" ",													"celda01rep",	ComunesPDF.LEFT,2,1,1);
				documentoPdf.setLCell("No. de Acuse",									"formasrep",	ComunesPDF.LEFT,1,1,1);
				documentoPdf.setLCell((String)recibo.get("NUMERO_ACUSE"),		"formasrep",	ComunesPDF.LEFT,1,1,1);
				documentoPdf.setLCell("Fecha de Autorización",						"formasrep",	ComunesPDF.LEFT,1,1,1);
				documentoPdf.setLCell((String)recibo.get("FECHA_EMISION"),		"formasrep",	ComunesPDF.LEFT,1,1,1);
				documentoPdf.setLCell("Hora de Autorización",						"formasrep",	ComunesPDF.LEFT,1,1,1);
				documentoPdf.setLCell((String)recibo.get("HORA_AUTORIZACION"),	"formasrep",	ComunesPDF.LEFT,1,1,1);
				documentoPdf.setLCell("Usuario",											"formasrep",	ComunesPDF.LEFT,1,1,1);
				documentoPdf.setLCell((String)recibo.get("USUARIO"),				"formasrep",	ComunesPDF.LEFT,1,1,1);
				documentoPdf.setLCell("Al transmitir este MENSAJE DE DATOS, "   +
										 	 "usted está bajo su responsabilidad "     +
											 "haciendo negociables los DOCUMENTOS "    +
											 "que constan en el mismo, aceptando de "  +
											 "esta forma la cesión de los derechos "   +
											 "de cobro de las PYMES al INTERMEDIARIO " +
											 "FINANCIERO, dicha transmisión tendrá "   +
											 "validez para todos los efectos legales.",	
										 													"formasrep",	ComunesPDF.JUSTIFIED,2,1,1);
				documentoPdf.addLTable();
			}
			
			// Agregar linea Vacia
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			
			documentoPdf.setLTable(8,100,new float[]{12.50f,12.50f,12.50f,12.50f,12.50f,12.50f,12.50f,12.50f});
			// Agregar encabezado
			documentoPdf.setLCell("Nombre\nProveedor",			"celda01rep",ComunesPDF.CENTER,1,1,1);
			documentoPdf.setLCell("Número de\nDocumento",		"celda01rep",ComunesPDF.CENTER,1,1,1);
			documentoPdf.setLCell("Fecha\nEmisión",				"celda01rep",ComunesPDF.CENTER,1,1,1);
			documentoPdf.setLCell("Fecha\nVencimiento",			"celda01rep",ComunesPDF.CENTER,1,1,1);
			documentoPdf.setLCell("Moneda",							"celda01rep",ComunesPDF.CENTER,1,1,1);
			documentoPdf.setLCell("Tipo\nFactoraje",				"celda01rep",ComunesPDF.CENTER,1,1,1);
			documentoPdf.setLCell("Monto",							"celda01rep",ComunesPDF.CENTER,1,1,1);
			documentoPdf.setLCell("Referencia",						"celda01rep",ComunesPDF.CENTER,1,1,1);
				
			// Llenar tabla
			int i = 0;
			if(registros != null && registros.size() > 0){
				for(i=0;i<registros.size();i++){
			
					HashMap registro = (HashMap) registros.get(i);
					
					int longNombreProveedor = ((String) registro.get("NOMBRE_PROVEEDOR")).length();					
					
					documentoPdf.setLCell((String) registro.get("NOMBRE_PROVEEDOR"),		"formasrep",((longNombreProveedor < 17)?ComunesPDF.CENTER:ComunesPDF.JUSTIFIED),1,1,1);
					documentoPdf.setLCell((String) registro.get("NOMBRE_DOCUMENTO"),		"formasrep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setLCell((String) registro.get("FECHA_EMISION"),			"formasrep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setLCell((String) registro.get("FECHA_VENCIMIENTO"),		"formasrep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setLCell((String) registro.get("MONEDA"),					"formasrep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setLCell((String) registro.get("TIPO_FACTORAJE"),			"formasrep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setLCell((String) registro.get("MONTO"),						"formasrep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setLCell((String) registro.get("REFERENCIA"),				"formasrep",ComunesPDF.JUSTIFIED,1,1,1);
				}
			}else{
					documentoPdf.setLCell(" \nNo se encontró ningún registro.\n ",	   	"formasmenB",ComunesPDF.CENTER,8,1,1);
			}
			documentoPdf.addLTable();
			
			// Finalizar documento
			documentoPdf.endDocument();
			
		}catch(Exception e){
			System.out.println("15disperpagoresumen.jsp::getArchivoPDF(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}
		System.out.println("15disperpagoresumen.jsp::getArchivoPDF(S)");
		
		return nombreArchivo;	
	}
%>