<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.math.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.dispersion.*, 
		com.netro.descuento.*,
		com.netro.exception.*,
		javax.naming.*,
		net.sf.json.*,
		com.netro.pdf.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%@ include file="/15cadenas/15pki/certificado.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String msgError = "";
try{


	if (informacion.equals("DatosIniciales")) {
		JSONObject jsonObj = new JSONObject();
		JSONArray jsObjArrayMeses = new JSONArray();
		JSONArray jsObjArrayAnios = new JSONArray();
		String	_mes 			= (request.getParameter("_mes")			== null)?Integer.toString(Fecha.getMesActual()):request.getParameter("_mes").trim();
		String	_anio 		= (request.getParameter("_anio")			== null)?Integer.toString(Fecha.getAnioActual()):request.getParameter("_anio").trim();
		
		String mesConFormato = _mes.length() == 1?"0"+_mes:_mes;
		
		List listaMeses = getListaMeses(Integer.parseInt(_anio));
		List listaAnios = getListaAnios();
		
		jsObjArrayMeses = JSONArray.fromObject(listaMeses);
		jsObjArrayAnios = JSONArray.fromObject(listaAnios);
		
		jsonObj.put("cboMes",mesConFormato);
		jsonObj.put("cboAnio",_anio);
		jsonObj.put("registrosMeses", jsObjArrayMeses.toString());
		jsonObj.put("registrosAnios", jsObjArrayAnios.toString());
		jsonObj.put("success",new Boolean(true));
		
		infoRegresar = jsonObj.toString();
	
	
	}else if (informacion.equals("ConsultarMovimientos")) {
		
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		
		CargaDocumento cargaDocumento = ServiceLocator.getInstance().lookup("CargaDocumentoEJB",CargaDocumento.class);
		
		String diasInhabiles = cargaDocumento.getDiasInhabiles(iNoCliente);
		
		SimpleDateFormat 	sdf 			= new SimpleDateFormat("dd/MM/yyyy");
		String 				sFechaHoy 	= sdf.format(new java.util.Date());
		
		String 	fechaVencDe	= "";
		String	fechaVencA	= "";
		
		JSONObject jsonObj = new JSONObject();
		String 		icEPO 							= iNoCliente;
		String 		nombreEPO						= strNombre;
		boolean 		hayTarifasParametrizadas 	= false;	
		boolean		hayTarifaA						= false;
		boolean		hayTarifaB						= false;
		boolean		hayTarifaC						= false;
		String 		diasTarifaA						= null;
		String 		diasTarifaBinicio				= null;
		String 		diasTarifaBfin					= null;
		String 		diasTarifaC						= null;
		List 			listaDeTarifas 				= null;
		Hashtable 	datosFactura 					= null;
		Vector 		documentosPorIntermediario = null;
		Vector 		documentosNoOperados 		= null;
		double		tarifaA							= 0.0;
		double		tarifaB							= 0.0;
		double		tarifaC							= 0.0;
		
		String		numeroDeFolio								= "";
		String		nombreDelMes								= "";
		String 		importe										= "";
		String		fechaDeAutorizacion						= "";
		String		claveMoneda									= "";
		String		claveMonedaUSD								= "";
		boolean		hayDocumentosOperados					= true;
		boolean		hayDocumentosNoOperados					= true;
		String		estatus										= null;
		boolean		hayAutorizacion							= false;
		boolean 		montoCero 									= false; 
		boolean		hayProveedorDelServicioDeDispersion = false;
		
		
		// Dolares
		boolean 		hayTarifasParametrizadasUSD 	= false;	
		boolean		hayTarifaAUSD						= false;
		boolean		hayTarifaBUSD						= false;
		boolean		hayTarifaCUSD						= false;
		List 			listaDeTarifasUSD					= null;
		String 		diasTarifaAUSD						= null;
		String 		diasTarifaBinicioUSD				= null;
		String 		diasTarifaBfinUSD					= null;
		String 		diasTarifaCUSD						= null;
		double		tarifaAUSD							= 0.0;
		double		tarifaBUSD							= 0.0;
		double		tarifaCUSD							= 0.0;
		Hashtable 	datosFacturaUSD 					= null;
		Vector 		documentosPorIntermediarioUSD = null;
		Vector 		documentosNoOperadosUSD 		= null;
		
		boolean 		hayDocumentosOperadosMN 	= true;
		boolean 		hayDocumentosOperadosUSD	= true;
		
		boolean		hayDocumentosNoOperadosMN	= true;
		boolean		hayDocumentosNoOperadosUSD	= true;
		
		ArrayList 	listaDocumentosOperados 	= null;
		ArrayList 	listaDocumentosOperadosTot = new ArrayList();
		
		HashMap  	registro 						= null;
		String 		rowSpan 							= "1";
		String 		hayDocumentosMN 				= "";
		String 		hayDocumentosUSD 				= ""; 
		String 		nombre_intermediario			= "";
		String   	monto_mn 						= "";
		String   	numero_mn 						= "";
		String 		monto_usd 						= "";
		String   	numero_usd 						= "";
		
		int 			iTotOper 						= 0;
		BigDecimal 	bMtoOper 						= new BigDecimal(0);
	
		int 			iTotOperUSD 					= 0;
		BigDecimal 	bMtoOperUSD 					= new BigDecimal(0);
		
		String 		descripcionMonedaNacional 		= "";
		String 		descripcionDolaresAmericanos 	= "";
		
		String 		importeUSD							= "";
		
		String	_mes 			= (request.getParameter("cboMes")			== null)?Integer.toString(Fecha.getMesActual()):request.getParameter("cboMes").trim();
		String	_anio 		= (request.getParameter("cboAnio")			== null)?Integer.toString(Fecha.getAnioActual()):request.getParameter("cboAnio").trim();
		double		valorIva							= 0.0;
		double		valorIvaT							= 0.0;
		
		//-- Obtener la descripcion de las monedas --
		descripcionMonedaNacional 		= dispersion.getDescripcionMoneda("1");
		descripcionDolaresAmericanos 	= dispersion.getDescripcionMoneda("54");
		
		//Verficar si hay algun proveedor registrado para el servicio de dispersion
		hayProveedorDelServicioDeDispersion = dispersion.hayPYMEParametrizadaParaElServicioDeDispersion(icEPO);
		
		String mesConFormato = _mes.length() == 1?"0"+_mes:_mes;
		
		fechaVencDe 	= "01/" + mesConFormato + "/" + _anio ;
		fechaVencA  	= Fecha.getUltimoDiaDelMes(fechaVencDe,"dd/MM/yyyy");
		System.out.println("fechaVencDe==="+fechaVencDe);
		System.out.println("fechaVencA==="+fechaVencA);
		
		claveMoneda 	= "1";
		claveMonedaUSD = "54";
		
		//-- Obtener Lista de Tarifas en Moneda Nacional con su Costo --
		
		
		boolean	hayTarifaDispersionMonedaNacional		= dispersion.hasTarifaDispersionMonedaNacional(icEPO);
		boolean 	hayTarifaDispersionDolaresAmericanos	= dispersion.hasTarifaDispersionDolaresAmericanos(icEPO);
		
		boolean  montoMNEsCero 	= true;
		boolean  montoUSDEsCero = true;

		if(dispersion.hasTarifaDispersionMonedaNacional(icEPO)){

			listaDeTarifas 				= dispersion.getTarifaEpo(icEPO);

			if( listaDeTarifas.size() >  0 ){
				diasTarifaA 					= ((List) listaDeTarifas.get(0)).get(0).toString();
				tarifaA 							= Double.parseDouble(((List) listaDeTarifas.get(0)).get(1).toString());

				if(diasTarifaA != null && !diasTarifaA.equals("")){
					hayTarifaA 						= true;

				}
			}
			if( listaDeTarifas.size() >  1 && hayTarifaA )	{
				diasTarifaBinicio				= Integer.toString(Integer.parseInt(diasTarifaA)+1);
				diasTarifaBfin 				= ((List) listaDeTarifas.get(1)).get(0).toString();
				tarifaB 							= Double.parseDouble(((List) listaDeTarifas.get(1)).get(1).toString());

				if(diasTarifaBfin != null && !diasTarifaBfin.equals("")){
					hayTarifaB						= true;
					hayTarifasParametrizadas 	= true;
				}
			}
			if( listaDeTarifas.size() >  2 && hayTarifaB )	{
				diasTarifaC						= Integer.toString(Integer.parseInt(diasTarifaBfin)+1);
				tarifaC 							= Double.parseDouble(((List) listaDeTarifas.get(2)).get(1).toString());
				hayTarifaC 						= true;
				hayTarifasParametrizadas 	= true;
			}
		}
		
		//-- Obtener Lista de Tarifas en Dolares Americanos con su Costo --
		if(dispersion.hasTarifaDispersionDolaresAmericanos(icEPO)){

			listaDeTarifasUSD 				= dispersion.getTarifaEpoUSD(icEPO);

			if( listaDeTarifasUSD.size() >  0 ){
				diasTarifaAUSD					= ((List) listaDeTarifasUSD.get(0)).get(0).toString();
				tarifaAUSD 						= Double.parseDouble(((List) listaDeTarifasUSD.get(0)).get(1).toString());

				if(diasTarifaAUSD != null && !diasTarifaAUSD.equals("")){
					hayTarifaAUSD 						= true;
					//hayTarifasParametrizadasUSD 	= true;
				}
			}
			if( listaDeTarifasUSD.size() >  1 && hayTarifaAUSD )	{
				diasTarifaBinicioUSD			= Integer.toString(Integer.parseInt(diasTarifaAUSD)+1);
				diasTarifaBfinUSD 			= ((List) listaDeTarifasUSD.get(1)).get(0).toString();
				tarifaBUSD 						= Double.parseDouble(((List) listaDeTarifasUSD.get(1)).get(1).toString());

				if(diasTarifaBfinUSD != null && !diasTarifaBfinUSD.equals("")){
					hayTarifaBUSD						= true;
					hayTarifasParametrizadasUSD 	= true;
				}
			}
			if( listaDeTarifasUSD.size() >  2 && hayTarifaBUSD )	{
				diasTarifaCUSD						= Integer.toString(Integer.parseInt(diasTarifaBfinUSD)+1);
				tarifaCUSD 							= Double.parseDouble(((List) listaDeTarifasUSD.get(2)).get(1).toString());
				hayTarifaCUSD 						= true;
				hayTarifasParametrizadasUSD 	= true;
			}
		}
		
		//-- Generar Detalle de Documentos Operados M.N. por Intermediario Financiero --
		if(hayTarifasParametrizadas) {
			System.out.println("DATos enviados::::");
			System.out.println("icEPO::::"+icEPO);
			System.out.println("fechaVencDe::::"+fechaVencDe);
			System.out.println("fechaVencA::::"+fechaVencA);
			System.out.println("diasTarifaA::::"+diasTarifaA);
			
			datosFactura 					= dispersion.getOperacionesDispersadasEpos(icEPO, fechaVencDe, fechaVencA, String.valueOf(Integer.parseInt(diasTarifaA)+1), String.valueOf(Integer.parseInt(diasTarifaBfin)+1));
			documentosPorIntermediario = (Vector)datosFactura.get("vOperyOperPag");
			documentosNoOperados 		= (Vector)datosFactura.get("vVencSinOperSumRes");
			
			

			documentosPorIntermediario	= documentosPorIntermediario 	!= null?documentosPorIntermediario	:new Vector();
			documentosNoOperados			= documentosNoOperados 			!= null?documentosNoOperados			:new Vector();

		}

		//-- Generar Detalle de Documentos Operados USD por Intermediario Financiero --
		if(hayTarifasParametrizadasUSD) {

			datosFacturaUSD 					= dispersion.getOperacionesDispersadasEposUSD(icEPO, fechaVencDe, fechaVencA, String.valueOf(Integer.parseInt(diasTarifaAUSD)+1), String.valueOf(Integer.parseInt(diasTarifaBfinUSD)+1));
			documentosPorIntermediarioUSD = (Vector)datosFacturaUSD.get("vOperyOperPagUSD");
			documentosNoOperadosUSD 		= (Vector)datosFacturaUSD.get("vVencSinOperSumResUSD");

			documentosPorIntermediarioUSD	= documentosPorIntermediarioUSD		!= null?documentosPorIntermediarioUSD	:new Vector();
			documentosNoOperadosUSD			= documentosNoOperadosUSD 				!= null?documentosNoOperadosUSD			:new Vector();

		}

		//-- Ordenar los documentos --
		hayDocumentosOperadosMN 	= ( (documentosPorIntermediario		!= null) && (documentosPorIntermediario.size()		>0) )?true:false;
		hayDocumentosOperadosUSD	= ( (documentosPorIntermediarioUSD	!= null) && (documentosPorIntermediarioUSD.size()	>0) )?true:false;

		listaDocumentosOperados = dispersion.ordenaRegistros(hayDocumentosOperadosMN?documentosPorIntermediario:null,hayDocumentosOperadosUSD?documentosPorIntermediarioUSD:null);
		
		//Generar parametro con los que se generara el documento --
		numeroDeFolio					= "DISP" + mesConFormato + _anio;
		nombreDelMes					= getNombreDelMes(Integer.parseInt(_mes)) + " de " + _anio;
		importe							= "0.00 MN";
		importeUSD						= "0.00 USD";
		fechaDeAutorizacion			= sFechaHoy;

		//-- Obtener Estatus --
		System.out.println("numeroDeFolio = " + numeroDeFolio + ", icEPO = " + icEPO);
		if(dispersion.estaAutorizadoDocumentoParaPagoDelServicioDeDispersion(numeroDeFolio, icEPO)){
			estatus 						= "AUTORIZADO";
			hayAutorizacion			= true;
		}else{
			estatus = "POR AUTORIZAR";
		}
		
		
		if(listaDocumentosOperados!=null && listaDocumentosOperados.size()>0){
			int doctosMN = 0;
			BigDecimal bdMontoMN = new BigDecimal("0.0");
			int doctosUSD = 0;
			BigDecimal bdMontoUSD = new BigDecimal("0.0");
			for(int i = 0; i<listaDocumentosOperados.size(); i++){
				HashMap mpDataReg = (HashMap)listaDocumentosOperados.get(i);
				String moneda = (String)mpDataReg.get("MONEDA");
				String monto = (String)mpDataReg.get("MONTO");
				String numero = (String)mpDataReg.get("NUMERO");
				
				if("1".equals(moneda)){
					doctosMN += Integer.parseInt(numero);
					bdMontoMN = bdMontoMN.add(new BigDecimal(monto));
				}
				if("54".equals(moneda)){
					doctosUSD += Integer.parseInt(numero);
					bdMontoUSD = bdMontoUSD.add(new BigDecimal(monto));
				}
			}
			HashMap mpTotales = new HashMap();
			mpTotales.put("TITULO_MONEDA","Total MN: " );
			mpTotales.put("TOTAL_DOCTOS", String.valueOf(doctosMN));
			mpTotales.put("MONTO_DOCTOS", bdMontoMN.toPlainString());
			listaDocumentosOperadosTot.add(mpTotales);
			mpTotales = new HashMap();
			mpTotales.put("TITULO_MONEDA","Total USD: " );
			mpTotales.put("TOTAL_DOCTOS", String.valueOf(doctosUSD));
			mpTotales.put("MONTO_DOCTOS", bdMontoUSD.toPlainString());
			listaDocumentosOperadosTot.add(mpTotales);
		}

		
		String fecha1   = "01/"+ _mes + "/" + _anio; // formato fecha: DD/MM/YYYY
		String porcentajeIva   = Iva.getPorcentaje(fecha1);
		valorIva = ((BigDecimal)Iva.getValor(fecha1)).doubleValue();
		valorIvaT = 1 + ((BigDecimal)Iva.getValor(fecha1)).doubleValue();
		
		List lstDoctoNoOperados = new ArrayList();
		List lstDoctoNoOperadosUSD = new ArrayList();
		
		

		System.out.println("**********Descarga *****************************::  ");
		System.out.println("fecha::  "+fecha1);
		System.out.println("porcentajeIva::  "+porcentajeIva);
		System.out.println(valorIva  +"  "+valorIvaT);
		System.out.println("***************************************::  ");
		
		JSONArray jsObjArray = new JSONArray();
		JSONArray jsObjArrayTotales = new JSONArray();
		JSONArray jsObjArrayB = new JSONArray();
		JSONArray jsObjArrayBtotales = new JSONArray();
		JSONArray jsObjArrayC = new JSONArray();
		JSONArray jsObjArrayCtotales = new JSONArray();
		
		jsObjArray = JSONArray.fromObject(listaDocumentosOperados);
		jsObjArrayTotales = JSONArray.fromObject(listaDocumentosOperadosTot);
		
		jsonObj.put("hayTarifasParametrizadas",  new Boolean(hayTarifasParametrizadas));
		jsonObj.put("hayTarifasParametrizadasUSD",  new Boolean(hayTarifasParametrizadasUSD));
		jsonObj.put("hayDocumentosOperadosMN",  new Boolean(hayDocumentosOperadosMN));
		jsonObj.put("hayDocumentosOperadosUSD",  new Boolean(hayDocumentosOperadosUSD));	
		jsonObj.put("descripcionMonedaNacional",  descripcionMonedaNacional);
		jsonObj.put("descripcionDolaresAmericanos",  descripcionDolaresAmericanos);
		jsonObj.put("hayTarifaDispersionMonedaNacional",  new Boolean(hayTarifaDispersionMonedaNacional));	
		jsonObj.put("hayTarifaDispersionDolaresAmericanos",  new Boolean(hayTarifaDispersionDolaresAmericanos));
		jsonObj.put("hayProveedorDelServicioDeDispersion",  new Boolean(hayProveedorDelServicioDeDispersion));
		jsonObj.put("hayAutorizacion",  new Boolean(hayAutorizacion));
		
		if(hayTarifasParametrizadas){
			jsonObj.put("hayTarifaA",  new Boolean(hayTarifaA));
			jsonObj.put("diasTarifaA",  "Hasta "+diasTarifaA+" días inclusive.");	
			jsonObj.put("hayTarifaB",  new Boolean(hayTarifaB));
			jsonObj.put("diasTarifaB",  "De los "+diasTarifaBinicio+" a los "+ diasTarifaBfin + " días.");
			jsonObj.put("hayTarifaC",  new Boolean(hayTarifaC));
			jsonObj.put("diasTarifaC",  "De los "+ diasTarifaC + " en adelante.");
			
			lstDoctoNoOperados = dispersion.getOperDispEposDoctosNoOperJS(documentosNoOperados, tarifaA, tarifaB, tarifaC, valorIva, valorIvaT, "autorizacion");
			
			if(lstDoctoNoOperados!=null && lstDoctoNoOperados.size()>0){
				List lstDostosMN = lstDoctoNoOperados.size()>=1?(ArrayList)lstDoctoNoOperados.get(0):new ArrayList();
				List lstTotalesMN = lstDoctoNoOperados.size()>=2?(ArrayList)lstDoctoNoOperados.get(1):new ArrayList();
				//HashMap mapTotal = lstDoctoNoOperados.size()>=2?(HashMap)lstDoctoNoOperados.get(1):new HashMap();
				//HashMap mapDataExtra = lstDoctoNoOperados.size()>=3?(HashMap)lstDoctoNoOperados.get(2):new HashMap();
				HashMap mpTotComision = (HashMap)lstTotalesMN.get(4);
				importe = (String)mpTotComision.get("iTotPymePV1");
				
				montoMNEsCero 	= !hayTarifaDispersionMonedaNacional?		true:(esCero(quitaFormatoNumerico(importe))	?true:false);

				jsObjArrayB = JSONArray.fromObject(lstDostosMN);
				jsObjArrayBtotales = JSONArray.fromObject(lstTotalesMN);
			}
			
		}
		
		if(hayTarifasParametrizadasUSD){
			jsonObj.put("hayTarifaAUSD",  new Boolean(hayTarifaAUSD));
			jsonObj.put("diasTarifaAUSD",  "Hasta "+diasTarifaAUSD+" días inclusive.");	
			jsonObj.put("hayTarifaBUSD",  new Boolean(hayTarifaBUSD));
			jsonObj.put("diasTarifaBUSD",  "De los "+diasTarifaBinicioUSD+" a los "+ diasTarifaBfinUSD + " días.");
			jsonObj.put("hayTarifaCUSD",  new Boolean(hayTarifaCUSD));
			jsonObj.put("diasTarifaCUSD",  "De los "+ diasTarifaCUSD + " en adelante.");
			
			lstDoctoNoOperadosUSD = dispersion.getOperDispEposDoctosNoOperUSDJS(documentosNoOperadosUSD, tarifaAUSD, tarifaBUSD, tarifaCUSD, valorIva, valorIvaT, "autorizacion");
			
			if(lstDoctoNoOperadosUSD!=null && lstDoctoNoOperadosUSD.size()>0){
				List lstDostosUSD = lstDoctoNoOperadosUSD.size()>=1?(ArrayList)lstDoctoNoOperadosUSD.get(0):new ArrayList();
				List lstTotalesUSD = lstDoctoNoOperadosUSD.size()>=2?(ArrayList)lstDoctoNoOperadosUSD.get(1):new ArrayList();
				//HashMap mapTotal = lstDoctoNoOperadosUSD.size()>=2?(HashMap)lstDoctoNoOperadosUSD.get(1):new HashMap();
				//HashMap mapDataExtra = lstDoctoNoOperadosUSD.size()>=3?(HashMap)lstDoctoNoOperadosUSD.get(2):new HashMap();
				HashMap mpTotComision = (HashMap)lstTotalesUSD.get(4);
				importeUSD = (String)mpTotComision.get("iTotPymePV1");
				montoUSDEsCero = !hayTarifaDispersionDolaresAmericanos?	true:(esCero(quitaFormatoNumerico(importeUSD))?true:false);
				
				jsObjArrayC = JSONArray.fromObject(lstDostosUSD);
				jsObjArrayCtotales = JSONArray.fromObject(lstTotalesUSD);
				
			}
		}
		
		// Verificar si hay algun monto por el cual cobrar documentos
		BigDecimal suma = new BigDecimal("0");
		BigDecimal zero = new BigDecimal("0");
		
		String numero = "";
		if(importe != null && !importe.trim().equals("")){
			numero = quitaFormatoNumerico(importe);
			suma = suma.add(new BigDecimal(numero));
		}
		if(importe != null && !importe.trim().equals("")){
			numero = quitaFormatoNumerico(importeUSD);
			suma = suma.add(new BigDecimal(numero));
		}

		montoCero = (zero.compareTo(suma) == 0)?true:false;
		
		
		System.err.println("---> hayTarifaDispersionMonedaNacional = <"+hayTarifaDispersionMonedaNacional+">"); // Debug info
		System.err.println("---> hayTarifaDispersionDolaresAmericanos = <"+hayTarifaDispersionDolaresAmericanos+">"); // Debug info
		System.err.println("---> importe 			= <"+importe+">"); // Debug info
		System.err.println("---> importeUSD 		= <"+importeUSD+">"); // Debug info
		System.err.println("---> montoMNEsCero 	= <"+montoMNEsCero+">"); // Debug info
		System.err.println("---> montoUSDEsCero 	= <"+montoUSDEsCero+">"); // Debug info
		
		jsonObj.put("diasInhabiles",diasInhabiles);
		jsonObj.put("estatus",estatus);
		jsonObj.put("numeroDeFolio",numeroDeFolio);
		jsonObj.put("nombreDelMes",nombreDelMes);
		jsonObj.put("importe",importe);
		jsonObj.put("importeUSD",importeUSD);
		jsonObj.put("montoCero", new Boolean(montoCero));
		jsonObj.put("montoMNEsCero",new Boolean(montoMNEsCero));
		jsonObj.put("montoUSDEsCero",new Boolean(montoUSDEsCero));
		
		jsonObj.put("claveMoneda",claveMoneda);
		jsonObj.put("claveMonedaUSD",claveMonedaUSD);
		jsonObj.put("sFechaHoy",sFechaHoy);//fechaDeAutorizacion
		jsonObj.put("fechaVencDe",fechaVencDe);
		jsonObj.put("fechaVencA",fechaVencA);
		jsonObj.put("nombreEPO",nombreEPO);
		jsonObj.put("registrosB", jsObjArrayB.toString());
		jsonObj.put("registrosBTot", jsObjArrayBtotales.toString());
		jsonObj.put("registrosCTot", jsObjArrayCtotales.toString());
		jsonObj.put("registrosC", jsObjArrayC.toString());
		jsonObj.put("registros", jsObjArray.toString());
		jsonObj.put("registrosTotales", jsObjArrayTotales.toString());
		jsonObj.put("success",  new Boolean(true));
		jsonObj.put("msgError", msgError);
		infoRegresar= jsonObj.toString();
	
	}else if (informacion.equals("GenerarArchivoCSV")) {
		JSONObject jsonObj = new JSONObject();
		String objGral = (request.getParameter("objGral") == null)?"":request.getParameter("objGral");
		String arrJsDoctoOperGral = (request.getParameter("arrDoctoOperGral") == null)?"":request.getParameter("arrDoctoOperGral");
		String arrJsDoctoOperTotalGral = (request.getParameter("arrDoctoOperTotalGral") == null)?"":request.getParameter("arrDoctoOperTotalGral");
		String arrJsDoctoNoOperMn = (request.getParameter("arrDoctoNoOperMn") == null)?"":request.getParameter("arrDoctoNoOperMn");
		String arrJsDoctoNoOperMnTot = (request.getParameter("arrDoctoNoOperMnTot") == null)?"":request.getParameter("arrDoctoNoOperMnTot");
		String arrJsDoctoNoOperUsd = (request.getParameter("arrDoctoNoOperUsd") == null)?"":request.getParameter("arrDoctoNoOperUsd");
		String arrJsDoctoNoOperUsdTot = (request.getParameter("arrDoctoNoOperUsdTot") == null)?"":request.getParameter("arrDoctoNoOperUsdTot");
		
		JSONObject jsObjGral = JSONObject.fromObject(objGral);
		List lstArrJsDoctoOperGral = JSONArray.fromObject(arrJsDoctoOperGral);
		List lstArrJsDoctoOperTotalGral = JSONArray.fromObject(arrJsDoctoOperTotalGral);
		List lstArrJsDoctoNoOperMn = JSONArray.fromObject(arrJsDoctoNoOperMn);
		List lstArrJsDoctoNoOperMnTot = JSONArray.fromObject(arrJsDoctoNoOperMnTot);
		List lstArrJsDoctoNoOperUsd = JSONArray.fromObject(arrJsDoctoNoOperUsd);
		List lstArrJsDoctoNoOperUsdTot = JSONArray.fromObject(arrJsDoctoNoOperUsdTot);
		
		Iterator itRegJsDoctoOperGral = lstArrJsDoctoOperGral.iterator();
		Iterator itRegJsDoctoOperTotalGral = lstArrJsDoctoOperTotalGral.iterator();
		Iterator itRegJsDoctoNoOperMn = lstArrJsDoctoNoOperMn.iterator();
		Iterator itRegJsDoctoNoOperMnTot = lstArrJsDoctoNoOperMnTot.iterator();
		Iterator itRegJsDoctoNoOperUsd = lstArrJsDoctoNoOperUsd.iterator();
		Iterator itRegJsDoctoNoOperUsdTot = lstArrJsDoctoNoOperUsdTot.iterator();
		
		
		StringBuffer sbContenidoF = new StringBuffer(",,,,,,,,RESUMEN DISPERSIÓN EPO\r\n");
		sbContenidoF.append(",,,,,,,,Fecha de Emisión:"+jsObjGral.getString("sFechaHoy")+"\r\n");
		sbContenidoF.append("Nombre EPO:"+jsObjGral.getString("nombreEPO")+",,,,,,,,\r\n");
		sbContenidoF.append(",,Fecha de Inicio,,,Fecha de Termino,,,\r\n");
		sbContenidoF.append(",,"+jsObjGral.getString("fechaVencDe")+",,,"+jsObjGral.getString("fechaVencA")+",,,\r\n");
		sbContenidoF.append("\r\n");
		sbContenidoF.append("Detalle Documentos Operados por Intermediario Financiero,,,,,,,,\r\n");
		sbContenidoF.append("Intermediario Financiero,Moneda,Número,Monto,,,,,,\r\n");
		
		String intermediario = "";
		while (itRegJsDoctoOperGral.hasNext()) {
			JSONObject registro = (JSONObject)itRegJsDoctoOperGral.next();
			if(!intermediario.equals(registro.getString("INTERMEDIARIO")))
				sbContenidoF.append("\""+registro.getString("INTERMEDIARIO")+"\",");
			else sbContenidoF.append(",");
			sbContenidoF.append("\""+ (("1".equals(registro.getString("MONEDA")))?jsObjGral.getString("descripcionMonedaNacional"):jsObjGral.getString("descripcionDolaresAmericanos")) +"\",");
			sbContenidoF.append("\""+registro.getString("NUMERO")+"\",");
			sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("MONTO"),2,SIN_COMAS)+"\",");
			sbContenidoF.append("\r\n");
			
			intermediario = registro.getString("INTERMEDIARIO");
			
		}//fin while()
		
		while(itRegJsDoctoOperTotalGral.hasNext()){
			JSONObject registro = (JSONObject)itRegJsDoctoOperTotalGral.next();
			sbContenidoF.append("\""+registro.getString("TITULO_MONEDA")+"\",,");
			sbContenidoF.append("\""+registro.getString("TOTAL_DOCTOS")+"\",");
			sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("MONTO_DOCTOS"),2,SIN_COMAS)+"\",");
			sbContenidoF.append("\r\n");
		}
		
		sbContenidoF.append("\r\n");
		
		//faltan totales de la primea tabla
		
		sbContenidoF.append("\"Detalle de Documentos no Operados M.N. por Periodo de Vigencia*\"");
		sbContenidoF.append("\r\n");
		
		sbContenidoF.append("\"\",");							
		if( jsObjGral.getString("diasTarifaA") != null){
			sbContenidoF.append("\""+jsObjGral.getString("diasTarifaA")+"\",,");
		}else{
			sbContenidoF.append("\"Falta parametrizar tarifa\",,");
		}
		if( jsObjGral.getString("diasTarifaB") != null){
			sbContenidoF.append("\""+jsObjGral.getString("diasTarifaB")+"\",,");
		}else{
			sbContenidoF.append("\"Falta parametrizar tarifa\",,");
		}
		if( jsObjGral.getString("diasTarifaC") != null){
			sbContenidoF.append("\""+jsObjGral.getString("diasTarifaC")+"\",,");
		}else{
			sbContenidoF.append("\"Falta parametrizar tarifa\",,");
		}
		sbContenidoF.append("\"Total No Operados\",");
		sbContenidoF.append("\r\n");
		
		sbContenidoF.append("\"Fecha de Vencimiento\",");	
		sbContenidoF.append("\"Número de Proveedores\",");
		sbContenidoF.append("\"Monto\",");
		sbContenidoF.append("\"Número de Proveedores\",");
		sbContenidoF.append("\"Monto\",");
		sbContenidoF.append("\"Número de Proveedores\",");
		sbContenidoF.append("\"Monto\",");
		sbContenidoF.append("\"Número de Proveedores\",");
		sbContenidoF.append("\"Monto\",");
		sbContenidoF.append("\r\n");
		
		if(lstArrJsDoctoNoOperMn!=null && lstArrJsDoctoNoOperMn.size()>0){
			
			while (itRegJsDoctoNoOperMn.hasNext()) {
				JSONObject registro = (JSONObject)itRegJsDoctoNoOperMn.next();
				sbContenidoF.append("\""+registro.getString("fechaVencimiento")+"\",");
				sbContenidoF.append("\""+registro.getString("iNoPymePV1")+"\",");
				sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoPV1"),2,SIN_COMAS)+"\",");
				sbContenidoF.append("\""+registro.getString("iNoPymePV2")+"\",");
				sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoPV2"),2,SIN_COMAS)+"\",");
				sbContenidoF.append("\""+registro.getString("iNoPymePV3")+"\",");
				sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoPV3"),2,SIN_COMAS)+"\",");
				sbContenidoF.append("\""+registro.getString("iTotPymes")+"\",");
				sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bSumTotMtoV"),2,SIN_COMAS)+"\",");
				sbContenidoF.append("\r\n");
			}
			
			int i = 1;
			
			while (itRegJsDoctoNoOperMnTot.hasNext()) {
				JSONObject registro = (JSONObject)itRegJsDoctoNoOperMnTot.next();
				
				sbContenidoF.append("\""+registro.getString("titulo")+"\",");
				if(i>1)
					sbContenidoF.append("\"$"+Comunes.formatoDecimal(registro.getString("iTotPymePV1"),2,SIN_COMAS)+"\",");
				else
					sbContenidoF.append("\""+registro.getString("iTotPymePV1")+"\",");
				
				if(i<2){				
					sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoTotPV1"),2,SIN_COMAS)+"\",");
					sbContenidoF.append("\""+registro.getString("iTotPymePV2")+"\",");
					sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoTotPV2"),2,SIN_COMAS)+"\",");
					sbContenidoF.append("\""+registro.getString("iTotPymePV3")+"\",");
					sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoTotPV3"),2,SIN_COMAS)+"\",");
					sbContenidoF.append("\""+registro.getString("iSumTotPymes")+"\",");
					sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bSumTotMtoH"),2,SIN_COMAS)+"\",");
				}else if(i==2){
					sbContenidoF.append(",");
					sbContenidoF.append("\"$"+Comunes.formatoDecimal(registro.getString("iTotPymePV2"),2,SIN_COMAS)+"\",");
					sbContenidoF.append(",");
					sbContenidoF.append("\"$"+Comunes.formatoDecimal(registro.getString("iTotPymePV3"),2,SIN_COMAS)+"\"");
				}
				sbContenidoF.append("\r\n");
				i++;
			}
		}else{ 
			sbContenidoF.append("\"No se encontraron registros.\",");
			sbContenidoF.append("\r\n");
		}
		
		sbContenidoF.append("\r\n");
			
		sbContenidoF.append("\"Detalle de Documentos no Operados USD por Periodo de Vigencia*\"");
		sbContenidoF.append("\r\n");
		
		sbContenidoF.append("\"\",");							
		if( jsObjGral.getString("diasTarifaAUSD") != null){
			sbContenidoF.append("\""+jsObjGral.getString("diasTarifaAUSD")+"\",,");
		}else{
			sbContenidoF.append("\"Falta parametrizar tarifa\",,");
		}
		if( jsObjGral.getString("diasTarifaBUSD") != null){
			sbContenidoF.append("\""+jsObjGral.getString("diasTarifaBUSD")+"\",,");
		}else{
			sbContenidoF.append("\"Falta parametrizar tarifa\",,");
		}
		if( jsObjGral.getString("diasTarifaCUSD") != null){
			sbContenidoF.append("\""+jsObjGral.getString("diasTarifaCUSD")+"\",,");
		}else{
			sbContenidoF.append("\"Falta parametrizar tarifa\",,");
		}
		sbContenidoF.append("\"Total No Operados\",");
		sbContenidoF.append("\r\n");
		
		sbContenidoF.append("\"Fecha de Vencimiento\",");	
		sbContenidoF.append("\"Número de Proveedores\",");
		sbContenidoF.append("\"Monto\",");
		sbContenidoF.append("\"Número de Proveedores\",");
		sbContenidoF.append("\"Monto\",");
		sbContenidoF.append("\"Número de Proveedores\",");
		sbContenidoF.append("\"Monto\",");
		sbContenidoF.append("\"Número de Proveedores\",");
		sbContenidoF.append("\"Monto\",");
		sbContenidoF.append("\r\n");
		
		if(lstArrJsDoctoNoOperUsd!=null && lstArrJsDoctoNoOperUsd.size()>0){
			while (itRegJsDoctoNoOperUsd.hasNext()) {
				JSONObject registro = (JSONObject)itRegJsDoctoNoOperUsd.next();
				sbContenidoF.append("\""+registro.getString("fechaVencimiento")+"\",");
				sbContenidoF.append("\""+registro.getString("iNoPymePV1")+"\",");
				sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoPV1"),2,SIN_COMAS)+"\",");
				sbContenidoF.append("\""+registro.getString("iNoPymePV2")+"\",");
				sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoPV2"),2,SIN_COMAS)+"\",");
				sbContenidoF.append("\""+registro.getString("iNoPymePV3")+"\",");
				sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoPV3"),2,SIN_COMAS)+"\",");
				sbContenidoF.append("\""+registro.getString("iTotPymes")+"\",");
				sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bSumTotMtoV"),2,SIN_COMAS)+"\",");
				sbContenidoF.append("\r\n");
			}
			
			int i = 1;
			while (itRegJsDoctoNoOperUsdTot.hasNext()) {
				JSONObject registro = (JSONObject)itRegJsDoctoNoOperUsdTot.next();
				
				sbContenidoF.append("\""+registro.getString("titulo")+"\",");
				if(i>1)
					sbContenidoF.append("\"$"+Comunes.formatoDecimal(registro.getString("iTotPymePV1"),2,SIN_COMAS)+"\",");
				else
					sbContenidoF.append("\""+registro.getString("iTotPymePV1")+"\",");
				
				if(i<2){				
					sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoTotPV1"),2,SIN_COMAS)+"\",");
					sbContenidoF.append("\""+registro.getString("iTotPymePV2")+"\",");
					sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoTotPV2"),2,SIN_COMAS)+"\",");
					sbContenidoF.append("\""+registro.getString("iTotPymePV3")+"\",");
					sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bMtoTotPV3"),2,SIN_COMAS)+"\",");
					sbContenidoF.append("\""+registro.getString("iSumTotPymes")+"\",");
					sbContenidoF.append("\""+Comunes.formatoDecimal(registro.getString("bSumTotMtoH"),2,SIN_COMAS)+"\",");
				}else if(i==2){
					sbContenidoF.append(",");
					sbContenidoF.append("\"$"+Comunes.formatoDecimal(registro.getString("iTotPymePV2"),2,SIN_COMAS)+"\",");
					sbContenidoF.append(",");
					sbContenidoF.append("\"$"+Comunes.formatoDecimal(registro.getString("iTotPymePV3"),2,SIN_COMAS)+"\"");
				}
				sbContenidoF.append("\r\n");
				i++;
			}
		}else{ 
			sbContenidoF.append("\"No se encontraron registros.\",");
			sbContenidoF.append("\r\n");	
		}
		
		sbContenidoF.append("\r\n");
			
		
		sbContenidoF.append("\"* Para determinar el período de vigencia de los documentos, se "); 
		sbContenidoF.append("calculará la diferiencia en días entre la fecha de vencimiento y "); 
		sbContenidoF.append("la fecha de alta de los mismos en Cadenas Productivas. En el caso de "); 
		sbContenidoF.append("existir más de un documento con la misma fecha de vencimiento ");
		sbContenidoF.append("para el mismo proveedor, se tomará como periodo de vigencia el ");
		sbContenidoF.append("mínimo del grupo de documentos.\",");
		sbContenidoF.append("\r\n");
		
		String nombreArchivo 	= "";
		CreaArchivo archivo = new CreaArchivo();
		if (archivo.make(sbContenidoF.toString(), strDirectorioTemp, ".csv"))
			nombreArchivo = archivo.nombre;	
			
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
		
		}else if (informacion.equals("ImprimirPDF")) {
		
		JSONObject jsonObj = new JSONObject();
		String nombreArchivo = "";
		String objGral = (request.getParameter("objGral") == null)?"":request.getParameter("objGral");
		String arrJsDoctoOperGral = (request.getParameter("arrDoctoOperGral") == null)?"":request.getParameter("arrDoctoOperGral");
		String arrJsDoctoNoOperMn = (request.getParameter("arrDoctoNoOperMn") == null)?"":request.getParameter("arrDoctoNoOperMn");
		String arrJsDoctoNoOperMnTot = (request.getParameter("arrDoctoNoOperMnTot") == null)?"":request.getParameter("arrDoctoNoOperMnTot");
		String arrJsDoctoNoOperUsd = (request.getParameter("arrDoctoNoOperUsd") == null)?"":request.getParameter("arrDoctoNoOperUsd");
		String arrJsDoctoNoOperUsdTot = (request.getParameter("arrDoctoNoOperUsdTot") == null)?"":request.getParameter("arrDoctoNoOperUsdTot");
		
		JSONObject jsObjGral = JSONObject.fromObject(objGral);
		List lstArrJsDoctoOperGral = JSONArray.fromObject(arrJsDoctoOperGral);
		List lstArrJsDoctoNoOperMn = JSONArray.fromObject(arrJsDoctoNoOperMn);
		List lstArrJsDoctoNoOperMnTot = JSONArray.fromObject(arrJsDoctoNoOperMnTot);
		List lstArrJsDoctoNoOperUsd = JSONArray.fromObject(arrJsDoctoNoOperUsd);
		List lstArrJsDoctoNoOperUsdTot = JSONArray.fromObject(arrJsDoctoNoOperUsdTot);
		
		Iterator itRegJsDoctoOperGral = lstArrJsDoctoOperGral.iterator();
		Iterator itRegJsDoctoNoOperMn = lstArrJsDoctoNoOperMn.iterator();
		Iterator itRegJsDoctoNoOperMnTot = lstArrJsDoctoNoOperMnTot.iterator();
		Iterator itRegJsDoctoNoOperUsd = lstArrJsDoctoNoOperUsd.iterator();
		Iterator itRegJsDoctoNoOperUsdTot = lstArrJsDoctoNoOperUsdTot.iterator();
		
		CreaArchivo archivo = new CreaArchivo();
		nombreArchivo = archivo.nombreArchivo()+".pdf";	
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
												session.getAttribute("iNoNafinElectronico").toString(),
												(String)session.getAttribute("sesExterno"),
												(String) session.getAttribute("strNombre"),
												(String) session.getAttribute("strNombreUsuario"),
												(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
		
		StringBuffer sbContenidoF = new StringBuffer();
		
		//sbContenidoF.append("Nombre EPO:"+jsObjGral.getString("nombreEPO")+",,,,,,,,\r\n");
		sbContenidoF.append("Fecha de Inicio                                                           Fecha de Término\n");
		sbContenidoF.append(jsObjGral.getString("fechaVencDe")+"                                                                     "+jsObjGral.getString("fechaVencA"));

		pdfDoc.addText("\n\nFecha de Emisión:"+jsObjGral.getString("sFechaHoy"),"formas",ComunesPDF.RIGHT);
		pdfDoc.addText("Nombre EPO:"+jsObjGral.getString("nombreEPO"),"formas",ComunesPDF.LEFT);
		pdfDoc.addText(sbContenidoF.toString(),"formas",ComunesPDF.CENTER);
		
		String tarifaA = (jsObjGral.getString("diasTarifaA")!=null && !"".equals(jsObjGral.getString("diasTarifaA")))?jsObjGral.getString("diasTarifaA"):"Falta parametrizar tarifa";
		String tarifaB = (jsObjGral.getString("diasTarifaB")!=null && !"".equals(jsObjGral.getString("diasTarifaB")))?jsObjGral.getString("diasTarifaB"):"Falta parametrizar tarifa";
		String tarifaC = (jsObjGral.getString("diasTarifaC")!=null && !"".equals(jsObjGral.getString("diasTarifaC")))?jsObjGral.getString("diasTarifaC"):"Falta parametrizar tarifa";
		
		String tarifaAUSD = (jsObjGral.getString("diasTarifaAUSD")!=null && !"".equals(jsObjGral.getString("diasTarifaAUSD")))?jsObjGral.getString("diasTarifaAUSD"):"Falta parametrizar tarifa";
		String tarifaBUSD = (jsObjGral.getString("diasTarifaBUSD")!=null && !"".equals(jsObjGral.getString("diasTarifaBUSD")))?jsObjGral.getString("diasTarifaBUSD"):"Falta parametrizar tarifa";
		String tarifaCUSD = (jsObjGral.getString("diasTarifaCUSD")!=null && !"".equals(jsObjGral.getString("diasTarifaCUSD")))?jsObjGral.getString("diasTarifaCUSD"):"Falta parametrizar tarifa";
		
		String textoDoctos = "Clasificación de Documentos de Acuerdo a su Vencimiento\n"+
							"Moneda Nacional                  Dolares Americanos\n "+
							"a)"+tarifaA+"            a)"+tarifaAUSD+"\n"+
							"b)"+tarifaB+"            b)"+tarifaBUSD+"\n"+
							"c)"+tarifaC+"            c)"+tarifaCUSD;
		
		int rowspan = lstArrJsDoctoOperGral.size()+3;
		float widths[] = {.125f,.125f,.125f,.125f,.50f};
		pdfDoc.setTable(5,100, widths);
		pdfDoc.setCell("Detalle Documentos Operados por Intermediario Financiero", "formas",ComunesPDF.LEFT,4,1);
		pdfDoc.setCell(textoDoctos,"celda01",ComunesPDF.CENTER,1,rowspan);
		pdfDoc.setCell("Intermediario Financiero","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Documentos Operados ","formas",ComunesPDF.CENTER,3,1);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Número","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,1);
		
		
		String intermediario = "";
		while (itRegJsDoctoOperGral.hasNext()) {
			JSONObject registro = (JSONObject)itRegJsDoctoOperGral.next();
			if(!intermediario.equals(registro.getString("INTERMEDIARIO")))
				pdfDoc.setCell(registro.getString("INTERMEDIARIO"),"formas",ComunesPDF.CENTER,1,1);
			else pdfDoc.setCell("","formas",ComunesPDF.CENTER,1,1);
			pdfDoc.setCell((("1".equals(registro.getString("MONEDA")))?jsObjGral.getString("descripcionMonedaNacional"):jsObjGral.getString("descripcionDolaresAmericanos")),"formas",ComunesPDF.CENTER,1,1);
			pdfDoc.setCell(registro.getString("NUMERO"),"formas",ComunesPDF.CENTER,1,1);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("MONTO"),2,true),"formas",ComunesPDF.CENTER,1,1);
			
			intermediario = registro.getString("INTERMEDIARIO");
			
			rowspan++;
		}//fin while()
		
		
		
		pdfDoc.addTable();
		
		
		
		pdfDoc.setTable(9,100);
		pdfDoc.setCell("Detalle de Documentos no Operados M.N. por Periodo de Vigencia*", "formas",ComunesPDF.LEFT,9,1);
		pdfDoc.setCell("Fecha de Vencimiento","formas",ComunesPDF.CENTER,1,2);
						
		if( jsObjGral.getString("diasTarifaA") != null && !"".equals(jsObjGral.getString("diasTarifaA"))){
			pdfDoc.setCell(jsObjGral.getString("diasTarifaA"),"celda01",ComunesPDF.CENTER,2,1);
		}else{
			pdfDoc.setCell("Falta parametrizar tarifa","celda01",ComunesPDF.CENTER,2,1);
		}
		if( jsObjGral.getString("diasTarifaB") != null && !"".equals(jsObjGral.getString("diasTarifaB"))){
			pdfDoc.setCell(jsObjGral.getString("diasTarifaB"),"celda01",ComunesPDF.CENTER,2,1);
		}else{
			pdfDoc.setCell("Falta parametrizar tarifa","celda01",ComunesPDF.CENTER,2,1);
		}
		if( jsObjGral.getString("diasTarifaC") != null && !"".equals(jsObjGral.getString("diasTarifaC"))){
			pdfDoc.setCell(jsObjGral.getString("diasTarifaC"),"celda01",ComunesPDF.CENTER,2,1);
		}else{
			pdfDoc.setCell("Falta parametrizar tarifa","celda01",ComunesPDF.CENTER,2,1);
		}
		pdfDoc.setCell("Total No Operados","celda01",ComunesPDF.CENTER,2,1);
		
		
		pdfDoc.setCell("Número de Proveedores","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Número de Proveedores","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Número de Proveedores","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Número de Proveedores","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,1);

		
		if(lstArrJsDoctoNoOperMn!=null && lstArrJsDoctoNoOperMn.size()>0){
			
			while (itRegJsDoctoNoOperMn.hasNext()) {
				JSONObject registro = (JSONObject)itRegJsDoctoNoOperMn.next();
				pdfDoc.setCell(registro.getString("fechaVencimiento"),"formas",ComunesPDF.CENTER,1,1); 
				pdfDoc.setCell(registro.getString("iNoPymePV1"),"formas",ComunesPDF.CENTER,1,1); 
				pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bMtoPV1"),2),"formas",ComunesPDF.RIGHT,1,1); 
				pdfDoc.setCell(registro.getString("iNoPymePV2"),"formas",ComunesPDF.CENTER,1,1); 
				pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bMtoPV2"),2),"formas",ComunesPDF.RIGHT,1,1); 
				pdfDoc.setCell(registro.getString("iNoPymePV3"),"formas",ComunesPDF.CENTER,1,1); 
				pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bMtoPV3"),2),"formas",ComunesPDF.RIGHT,1,1); 
				pdfDoc.setCell(registro.getString("iTotPymes"),"formas",ComunesPDF.CENTER,1,1); 
				pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bSumTotMtoV"),2),"formas",ComunesPDF.RIGHT,1,1); 

			}
			
			int i = 1;
			
			while (itRegJsDoctoNoOperMnTot.hasNext()) {
				JSONObject registro = (JSONObject)itRegJsDoctoNoOperMnTot.next();
				
				pdfDoc.setCell(registro.getString("titulo"),"celda01",ComunesPDF.CENTER,1,1); 
				if(i>2)
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("iTotPymePV1"),2)+" MN","formas",ComunesPDF.CENTER,2,1); 
				else if(i>1)
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("iTotPymePV1"),2),"formas",ComunesPDF.CENTER,1,1); 
				else
					pdfDoc.setCell(registro.getString("iTotPymePV1"),"formas",ComunesPDF.CENTER,1,1); 
				
				if(i<2){				
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bMtoTotPV1"),2),"formas",ComunesPDF.RIGHT,1,1); 
					pdfDoc.setCell(registro.getString("iTotPymePV2"),"formas",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bMtoTotPV2"),2),"formas",ComunesPDF.RIGHT,1,1); 
					pdfDoc.setCell(registro.getString("iTotPymePV3"),"formas",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bMtoTotPV3"),2),"formas",ComunesPDF.RIGHT,1,1); 
					pdfDoc.setCell(registro.getString("iSumTotPymes"),"formas",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bSumTotMtoH"),2),"formas",ComunesPDF.RIGHT,1,1); 

				}else if(i==2){
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("$"+registro.getString("iTotPymePV2"),"formas",ComunesPDF.CENTER,1,1);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("$"+registro.getString("iTotPymePV3"),"formas",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER,3,1);
				}else if(i>2){
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER,6,1); 
				}
				i++;
			}
					
			pdfDoc.addTable();
			
		}else{ 
			pdfDoc.setCell("No se encontraron registros.","celda01",ComunesPDF.CENTER,9,1); 
			pdfDoc.addTable();
		}
		
		
		pdfDoc.setTable(9,100);
		pdfDoc.setCell("Detalle de Documentos no Operados USD por Periodo de Vigencia*", "formas",ComunesPDF.LEFT,9,1);
		pdfDoc.setCell("Fecha de Vencimiento","formas",ComunesPDF.CENTER,1,2);
						
		if( jsObjGral.getString("diasTarifaAUSD") != null && !"".equals(jsObjGral.getString("diasTarifaAUSD"))){
			pdfDoc.setCell(jsObjGral.getString("diasTarifaAUSD"),"celda01",ComunesPDF.CENTER,2,1);
		}else{
			pdfDoc.setCell("Falta parametrizar tarifa","celda01",ComunesPDF.CENTER,2,1);
		}
		if( jsObjGral.getString("diasTarifaBUSD") != null && !"".equals(jsObjGral.getString("diasTarifaBUSD"))){
			pdfDoc.setCell(jsObjGral.getString("diasTarifaBUSD"),"celda01",ComunesPDF.CENTER,2,1);
		}else{
			pdfDoc.setCell("Falta parametrizar tarifa","celda01",ComunesPDF.CENTER,2,1);
		}
		if( jsObjGral.getString("diasTarifaCUSD") != null && !"".equals(jsObjGral.getString("diasTarifaCUSD"))){
			pdfDoc.setCell(jsObjGral.getString("diasTarifaCUSD"),"celda01",ComunesPDF.CENTER,2,1);
		}else{
			pdfDoc.setCell("Falta parametrizar tarifa","celda01",ComunesPDF.CENTER,2,1);
		}
		pdfDoc.setCell("Total No Operados","celda01",ComunesPDF.CENTER,2,1);
		
		
		pdfDoc.setCell("Número de Proveedores","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Número de Proveedores","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Número de Proveedores","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Número de Proveedores","celda01",ComunesPDF.CENTER,1,1);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,1);

		
		if(lstArrJsDoctoNoOperUsd!=null && lstArrJsDoctoNoOperUsd.size()>0){
			
			while (itRegJsDoctoNoOperUsd.hasNext()) {
				JSONObject registro = (JSONObject)itRegJsDoctoNoOperUsd.next();
				pdfDoc.setCell(registro.getString("fechaVencimiento"),"formas",ComunesPDF.CENTER,1,1); 
				pdfDoc.setCell(registro.getString("iNoPymePV1"),"formas",ComunesPDF.CENTER,1,1); 
				pdfDoc.setCell(Comunes.formatoDecimal(registro.getString("bMtoPV1"),2),"formas",ComunesPDF.RIGHT,1,1); 
				pdfDoc.setCell(registro.getString("iNoPymePV2"),"formas",ComunesPDF.CENTER,1,1); 
				pdfDoc.setCell(Comunes.formatoDecimal(registro.getString("bMtoPV2"),2),"formas",ComunesPDF.RIGHT,1,1); 
				pdfDoc.setCell(registro.getString("iNoPymePV3"),"formas",ComunesPDF.CENTER,1,1); 
				pdfDoc.setCell(Comunes.formatoDecimal(registro.getString("bMtoPV3"),2),"formas",ComunesPDF.RIGHT,1,1); 
				pdfDoc.setCell(registro.getString("iTotPymes"),"formas",ComunesPDF.CENTER,1,1); 
				pdfDoc.setCell(Comunes.formatoDecimal(registro.getString("bSumTotMtoV"),2),"formas",ComunesPDF.RIGHT,1,1); 

			}
			
			int i = 1;
			
			while (itRegJsDoctoNoOperUsdTot.hasNext()) {
				JSONObject registro = (JSONObject)itRegJsDoctoNoOperUsdTot.next();
				
				pdfDoc.setCell(registro.getString("titulo"),"celda01",ComunesPDF.CENTER,1,1); 
				if(i>2)
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("iTotPymePV1"),2)+" USD","formas",ComunesPDF.CENTER,2,1); 
				else if(i>1)
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("iTotPymePV1"),2),"formas",ComunesPDF.CENTER,1,1); 
				else
					pdfDoc.setCell(registro.getString("iTotPymePV1"),"formas",ComunesPDF.CENTER,1,1); 
				
				if(i<2){				
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bMtoTotPV1"),2),"formas",ComunesPDF.RIGHT,1,1); 
					pdfDoc.setCell(registro.getString("iTotPymePV2"),"formas",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bMtoTotPV2"),2),"formas",ComunesPDF.RIGHT,1,1); 
					pdfDoc.setCell(registro.getString("iTotPymePV3"),"formas",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bMtoTotPV3"),2),"formas",ComunesPDF.RIGHT,1,1); 
					pdfDoc.setCell(registro.getString("iSumTotPymes"),"formas",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("$"+Comunes.formatoDecimal(registro.getString("bSumTotMtoH"),2),"formas",ComunesPDF.RIGHT,1,1); 

				}else if(i==2){
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("$"+registro.getString("iTotPymePV2"),"formas",ComunesPDF.CENTER,1,1);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("$"+registro.getString("iTotPymePV3"),"formas",ComunesPDF.CENTER,1,1); 
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER,3,1);
				}else if(i>2){
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER,6,1); 
				}
				i++;
			}
					
			pdfDoc.addTable();
			
		}else{ 
			pdfDoc.setCell("No se encontraron registros.","celda01",ComunesPDF.CENTER,9,1); 
			pdfDoc.addTable();
		}
		
		
		sbContenidoF = new StringBuffer();
		sbContenidoF.append("\"* Para determinar el período de vigencia de los documentos, se "); 
		sbContenidoF.append("calculará la diferiencia en días entre la fecha de vencimiento y "); 
		sbContenidoF.append("la fecha de alta de los mismos en Cadenas Productivas. En el caso de "); 
		sbContenidoF.append("existir más de un documento con la misma fecha de vencimiento ");
		sbContenidoF.append("para el mismo proveedor, se tomará como periodo de vigencia el ");
		sbContenidoF.append("mínimo del grupo de documentos.\",");
		
		pdfDoc.addText(sbContenidoF.toString());
		
		pdfDoc.endDocument();
	
			
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
		
	}else if (informacion.equals("DetalleDoctos")){
		
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		JSONObject jsonObj =  new JSONObject();
		JSONArray jsObjArrayDisp = new JSONArray();
		JSONArray jsObjArrayNoDisp = new JSONArray();
		
		String icEPO = iNoCliente;
		String 	fechaVencDe	= (request.getParameter("txtFechaVencDe")	== null)?"":request.getParameter("txtFechaVencDe").trim();
		String	fechaVencA	= (request.getParameter("txtFechaVencA")	== null)?"":request.getParameter("txtFechaVencA").trim();
		
		List lstDoctoDisp = dispersion.getDetalleDocumentosDispersados(icEPO, fechaVencDe, fechaVencA );
		List lstDoctoNoDisp = dispersion.getDetalleDocumentosNoDispersados(icEPO, fechaVencDe, fechaVencA );
		
		jsObjArrayDisp = JSONArray.fromObject(lstDoctoDisp);
		jsObjArrayNoDisp = JSONArray.fromObject(lstDoctoNoDisp);
		
		jsonObj.put("registrosDisp",jsObjArrayDisp.toString());
		jsonObj.put("totalDisp",jsObjArrayDisp.size()+"");
		jsonObj.put("registrosNoDisp",jsObjArrayNoDisp.toString());
		jsonObj.put("totalNoDisp",jsObjArrayNoDisp.size()+"");
		jsonObj.put("success", new Boolean(true));
		
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("GeneraPopUpDetalle")){
		
		Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
		JSONObject jsonObj =  new JSONObject();
		JSONArray jsObjArrayDetalle = new JSONArray();
		
		String claveFlujoFondos = (request.getParameter("clave") == null)?"":request.getParameter("clave").trim();
		String tipoDocumento = (request.getParameter("tipoDocto") == null)?"":request.getParameter("tipoDocto").trim();
		
		List lista	= new ArrayList();
		if(tipoDocumento.equals("dispersado")){
			lista	= dispersion.getDetalleFlujoFondos(claveFlujoFondos);
		}
		if(tipoDocumento.equals("no_dispersado")){
			lista = dispersion.getDetalleFlujoFondosConError(claveFlujoFondos);// Falta agregar metodo
		}
		
		
		jsObjArrayDetalle = JSONArray.fromObject(lista);
		
		jsonObj.put("registros",jsObjArrayDetalle.toString());
		jsonObj.put("total",jsObjArrayDetalle.size()+"");		
		jsonObj.put("success", new Boolean(true));
		
		infoRegresar = jsonObj.toString();
	
	}
	
	
}catch(Throwable t){
	t.printStackTrace();
	throw new AppException("Error en la peticion",t);
}finally{
	System.out.println("infoRegresar = " + infoRegresar);
}
%>

<%=infoRegresar%>

<%!
	public ArrayList getListaMeses(int anio)throws NafinException{		
		ArrayList meses 	= new ArrayList();

		int anioActual		= Fecha.getAnioActual();
		
		if(anio == 0) 		return meses;
		
		for(int i=1; i<=10;i++){
			String mesConFormato = String.valueOf(i).length() == 1?"0"+i:""+i;
			HashMap mpMeses = new HashMap();
			mpMeses.put("clave",mesConFormato);
			mpMeses.put("descripcion",getNombreDelMes(i));
			meses.add(mpMeses);
		}

		return meses;
	}

	public ArrayList getListaAnios()throws NafinException{		
		ArrayList anios 	= new ArrayList();
		
		int anioInicial = 1998;
		int anioFinal	= Fecha.getAnioActual();
		
		for(int i=anioInicial;i<=anioFinal;i++){
			
			HashMap mpAnios = new HashMap();
			mpAnios.put("clave",Integer.toString(i));
			mpAnios.put("descripcion",Integer.toString(i));
			anios.add(mpAnios);
		}
		
		return anios;
	}
	
	public String getNombreDelMes(int mes){
	
		String nombre = "";
		switch(mes){
			case 1: 	nombre="Enero"; 		break;
			case 2: 	nombre="Febrero"; 	break;
			case 3: 	nombre="Marzo"; 		break;
			case 4: 	nombre="Abril"; 		break;
			case 5: 	nombre="Mayo"; 		break;
			case 6: 	nombre="Junio"; 		break;
			case 7: 	nombre="Julio"; 		break;
			case 8: 	nombre="Agosto"; 		break;
			case 9: 	nombre="Septiembre"; break;
			case 10: nombre="Octubre"; 	break;
			case 11: nombre="Noviembre"; 	break;
			case 12: nombre="Diciembre"; 	break;
		}
		return nombre;
	}
	
	public String quitaFormatoNumerico(String numero){
		
		StringBuffer resultado 				= new StringBuffer();
		String 		 numerosPermitidos	= "0123456789.";
		
		if (numero == null || numero.equals("") ) return "";
		
		for (int i=0;i<numero.length();i++){
			char digito = numero.charAt(i);
			if( numerosPermitidos.indexOf(digito)!= -1 ){
				resultado.append(digito);
			}
		}
		
		return resultado.toString();
	}
	
	public boolean esCero(String monto){
		
		BigDecimal zero 	= new BigDecimal("0");
		BigDecimal numero = new BigDecimal(monto);
											
		boolean esMontoCero = (zero.compareTo(numero) == 0)?true:false;
		return esMontoCero;
	}
 
%>