Ext.onReady(function() {

    form = {nRow: null, textoF: null, _acuse: null, acuse: null, fechaC: null, horaC: null};
    var banderaInicio = true;

    //---------------------------descargaInfo------------------------------------
    function descargaInfo(opts, success, response) {

        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
            var infoR = Ext.util.JSON.decode(response.responseText);
            var recibo = infoR.miRecibo; 
            var folio = infoR.miFolio;
            var fecha = infoR.miFecha;
            var hora = infoR.miHora;
                  var claUser = infoR.registros[0].usuario0;
                  var nomUser = infoR.registros[0].nombre_usuario0;
                  var nomComp = claUser + " - " + nomUser;  
           
           var miData = new Array();
           var registro = [];
           registro.push("Recibo:");
           registro.push(recibo);
           miData.push(registro);
           
           var registro = [];
           registro.push("N�mero de Acuse:");
           registro.push(folio);
           miData.push(registro);
           
           var registro = [];
           registro.push("Fecha:");
           registro.push(fecha);
           miData.push(registro);
           
           var registro = [];
           registro.push("Hora:");
           registro.push(hora);
           miData.push(registro);
           
           var registro = [];
           registro.push("Usuario:");
           registro.push(nomComp);
           miData.push(registro);
           
        var grid = Ext.getCmp('gridDetalle');
        var store = grid.getStore();
        store.loadData(miData);
 
            Ext.getCmp('formaDetalle').hide();
            Ext.getCmp('grid').hide();
            
            Ext.getCmp('formaautenticacion').show();
            Ext.getCmp('gridNafin').show();

            var grid = Ext.getCmp('gridNafin');
            var store = grid.getStore();
            var registrosEnviar = [];
            var existeInfo = true;
            
            store.each(function(record) {
                TaskLocation = Ext.data.Record.create([
                    {name: 'IC_EPO'},
                    {name: 'IC_NAFIN_ELECTRONICO'},
                    {name: 'CG_RAZON_SOCIAL'},
                    {name: 'CG_RFC'},
                    {name: 'CS_BLOQUEADO'},
                    {name: 'DF_FECHA'},
                    {name: 'CG_NOMBRE_USUARIO'},
                    {name: 'CG_CAUSA'}
                ]);
                var newRecord = new TaskLocation({
                    clave: Ext.id(),
                    IC_NAFIN_ELECTRONICO: record.data['IC_NAFIN_ELECTRONICO'],
                    CG_RAZON_SOCIAL: record.data['CG_RAZON_SOCIAL'],
                    CG_RFC: record.data['CG_RFC'],
                    CS_BLOQUEADO: record.data['CS_BLOQUEADO'],
                    DF_FECHA: fecha + " - " + hora, 
                    CG_NOMBRE_USUARIO: nomComp, 
                    CG_CAUSA: record.data['CG_CAUSA']
                });
                store.add(newRecord);
                store.remove(record);
                store.commitChanges();
                
                registrosEnviar.push(record.data);
            });

        } else {
                NE.util.mostrarConnError(response, opts);
        }
    }
//---------------------------fin descargaArchivo--------------------------------

//-----------------------------procesarConsultaData-----------------------------
    var procesarConsultaData = function(store, arrRegistros, opts) {
        var grid = Ext.getCmp('grid');
        if (arrRegistros !== null) {
            var el = grid.getGridEl();
            if (store.getTotalCount() > 0) {
                el.unmask();
                Ext.getCmp('btnConfirmar').enable();
                Ext.getCmp('btnCancelar').enable();
                Ext.getCmp('btnBuscar').enable();
                banderaInicio = false;
            } else {
                el.mask('No se encontr� ning�n registro', 'x-mask');
                Ext.getCmp('btnConfirmar').disable();
                Ext.getCmp('btnCancelar').disable();
                Ext.getCmp('btnBuscar').enable();
            }
        }
    };
//--------------------------Fin procesarConsultaData----------------------------

//-Store detalle---------------------------------
	var detalle = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'detalle',
     idIndex: 0, 
		 fields		: [
              {name : 'clave' },
              {name : 'descripcion'}              
              ]
		});
//Fin Store detalle----------------------------


//-------------------------------ConsultaData-----------------------------------	
    var consultaData = new Ext.data.JsonStore({
		  //autoDestroy: true,
		  
        root: 'registros',
        url: '15BloqServCaptEXT.data.jsp',
        //baseParams: {
        //informacion: 'consultaGeneral'
        //},
        fields: [
            {name: 'IC_PRODUCTO_NAFIN'},
            {name: 'IC_EPO'},
            {name: 'IC_NAFIN_ELECTRONICO'},
            {name: 'CG_RAZON_SOCIAL'},
            {name: 'CG_RFC'},
            {name: 'CS_BLOQUEADO'},
				{name: 'aux_bloq'},
				{name: 'b1'},
            {name: 'DF_FECHA'},
            {name: 'CG_NOMBRE_USUARIO'},
            {name: 'CG_CAUSA'},
				{name: 'aux_causa'},
				{name: 'estatusBloqueoActual'},
				{name: 'editable'},
				{name: 'fila'}
				
        ],
        totalProperty: 'total',
        messageProperty: 'msg',
        autoLoad: false,		  
        listeners: {
            load: procesarConsultaData,
            exception: {
                fn: function(proxy, type, action, optionsRequest, response, args) {
                            NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
                }
            }
        }
    });
    //----------------------------Fin ConsultaData----------------------------------

    //------------------------------Catalogo EPO------------------------------------
    var catalogoEpo = new Ext.data.JsonStore({
        id: 'catalogoEpo',
        root: 'registros',
        fields: ['clave', 'descripcion', 'loadMsg'],
        url: '15BloqServCaptEXT.data.jsp',
        baseParams: {
            informacion: 'catalogoepo'
        },
        totalProperty: 'total',
        autoLoad: false,
        listeners: {
                 beforeload: NE.util.initMensajeCargaCombo,
                exception: NE.util.mostrarDataProxyError
        }
    });
    //----------------------------Fin Catalogo EPO----------------------------------

    //-------------------------------Elementos Forma--------------------------------
    var elementosForma =
            [{
                    xtype: 'compositefield',
                    items: [
                        {
                            xtype: 'displayfield',
                            value: ' ',
                            width: 100
                        },
                        {
                            xtype: 'button',
                            text: 'Captura',
                            id: 'btnCaptura',
                            hidden: false,
                            iconCls: 'icoTxt',
                            height: 15,
                            width: 25,
                            handler: function(boton, evento) {
                                window.location = "/nafin/15cadenas/15pki/15mantenimiento/15dispersion/15BloqServCaptEXT.jsp";
                            }
                        }, {
                            xtype: 'displayfield',
                            value: ' ',
                            width: 50
                        }, {
                            xtype: 'button',
                            text: 'Consulta',
                            id: 'btnConsulta',
                            hidden: false,
                            iconCls: 'icoBuscar',
                            handler: function(boton, evento) {
                                window.location = "/nafin/15cadenas/15mantenimiento/15dispersion/15BloqServConsultaEXT.jsp";
                            }
                        }
                    ]}, {
                    xtype: 'combo',
                    fieldLabel: 'EPO',
                    name: 'cmb_epo',
                    hiddenName: '_cmb_epo',
                    id: 'cmbepo',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    emptyText: 'Seleccionar ...',
                    forceSelection: true,
                    triggerAction: 'all',
                    typeAhead: true,
                    minChars: 1,
                    store: catalogoEpo,
                    mode: 'local',
                    tpl: NE.util.templateMensajeCargaComboConDescripcionCompleta,
                    width: 120,
                    listeners: {
                        select: {
                            fn: function(combo) {
                                Ext.getCmp('grid').hide();
											 var gridConsulta = Ext.getCmp('gridNafin');	
											 var store = gridConsulta.getStore();
											 var filas=0;
											 store.each(function(record) {
												filas++
												});
											for(var i = 0;i< filas; i++){
													var rec = store.getAt(0); 
													store.remove(rec);
													//store.commitChanges();
												}
                            }
                        }
                    }
                }
            ];
    //-----------------------------Fin Elementos Forma------------------------------

    var confirm = function(pkcs7, textoFirmar, vcN, vcE, vst, vca){
	
        if (Ext.isEmpty(pkcs7)) {
            return;
        } else {
               
                
            Ext.Ajax.request({
                url: '15BloqServCaptEXT.data.jsp',
                params: {
                    informacion: 'Confirmar',
                    Pkcs7: pkcs7,
                    TextoFirmado: textoFirmar,
                    prodNafin: vcN,
                    epo: vcE,
                    check: vst,
                    causa: vca
                },
                callback: descargaInfo 
            });
        }
	}
    
//*******************************************************  procesa confirnmar
    var procesarConfirmar = function() {
	 
        var grid = Ext.getCmp('grid');
        var columnModelGrid = grid.getColumnModel();
        var store = grid.getStore();
        /* Pedimos al store que nos devuelva los registros modificados  	 */
        var modifiedRecords = store.getModifiedRecords();
        /* Creamos un array con los datos totales de los registros que han cambiado */
        var changes = new Array();
        /* Creamos un array para cada columna */
        var cN = new Array();
        var cE = new Array();
        var nE = new Array();
        var rS = new Array();
        var rFc = new Array();
        var st = new Array();
        var fh = new Array();
        var nu = new Array();
        var ca = new Array();
		  var lock = new Array();
		  var unlock = new Array();
		  var numReg=0;
		  consultaData.each(function(record) {
				if(record.data['editable']=='S'){
					numReg++;
					changes.push(record.data);
				}
			}); 
         var rowIndex = 0;
			var confirma=true;
			for (var i = 0; i < changes.length; i++) {
			
				if ( changes[i].CG_CAUSA != ""){
				} else if ( changes[i].CS_BLOQUEADO == "S" ){
					confirma=false;
					rowIndex = changes[i].fila;
					Ext.MessageBox.alert("Alerta", "Favor de escribir la causa de bloqueo.",
					function() {
						grid.startEditing(rowIndex, columnModelGrid.findColumnIndex('CG_CAUSA'));
					});
				} else if ( changes[i].CS_BLOQUEADO == "N" ){
					confirma=false;
					rowIndex = changes[i].fila;
					Ext.MessageBox.alert("Alerta", "Favor de escribir la causa de desbloqueo.",
					function() {
						grid.startEditing(rowIndex, columnModelGrid.findColumnIndex('CG_CAUSA'));
					});		
				}
			}
			
			
  var clavesIF = new Array();
  var layoutsCompletoVenc = new Array();
  var layoutsCompletoOper = new Array();
  var layoutsDoctosVenc = new Array();
  
  if (confirma){
			for (var i = 0; i < changes.length; i++) {
				
				cN.push(changes[i].IC_PRODUCTO_NAFIN);
				cE.push(changes[i].IC_EPO);
				nE.push(changes[i].IC_NAFIN_ELECTRONICO);
				rS.push(changes[i].CG_RAZON_SOCIAL);
				rFc.push(changes[i].CG_RFC);
				st.push(changes[i].CS_BLOQUEADO);
				fh.push(changes[i].DF_FECHA);
				nu.push(changes[i].CG_NOMBRE_USUARIO);
				ca.push(changes[i].CG_CAUSA);
				layoutsCompletoVenc.push(changes[i].CG_RAZON_SOCIAL);
				layoutsCompletoOper.push(changes[i].CS_BLOQUEADO);
				layoutsDoctosVenc.push(changes[i].CG_CAUSA);
			}
		}

  if (confirma){
    form.textoF = "| EPO | \t | Bloqueado | \t | Causa |\n";
    for (var j = 0; j < numReg; j++) {
        form.textoF += "| " + layoutsCompletoVenc[j] + " | " + layoutsCompletoOper[j] + " | " + layoutsDoctosVenc[j] + "\n";
    }
	 
  }     
          if (confirma){  
            NE.util.obtenerPKCS7(confirm, form.textoF, cN, cE, st, ca);     
			 var gridCrea = Ext.getCmp('gridNafin');
           var storeCrea = gridCrea.getStore();
                for (var i = 0; i < changes.length; i++) {
                TaskLocation = Ext.data.Record.create([
                {name: 'IC_EPO'},
                {name: 'IC_NAFIN_ELECTRONICO'},
                {name: 'CG_RAZON_SOCIAL'},
                {name: 'CG_RFC'},
                {name: 'CS_BLOQUEADO'},
                {name: 'DF_FECHA'},
                {name: 'CG_NOMBRE_USUARIO'},
                {name: 'CG_CAUSA'}
            ]);
                 var record = new TaskLocation({
                clave: Ext.id(),
                IC_NAFIN_ELECTRONICO: changes[i].IC_NAFIN_ELECTRONICO,
                CG_RAZON_SOCIAL: changes[i].CG_RAZON_SOCIAL,
                CG_RFC: changes[i].CG_RFC,
                CS_BLOQUEADO: changes[i].CS_BLOQUEADO,
                DF_FECHA: changes[i].DF_FECHA,
                CG_NOMBRE_USUARIO: changes[i].CG_NOMBRE_USUARIO,
                CG_CAUSA: changes[i].CG_CAUSA
            });
                 if(changes[i].CG_CAUSA != ''){
                    storeCrea.add(record);
                 }
                }
           
          }//fin 
    };
//******************************************************* Fin procesa confirnmar


//******************************************************* procesarCausaVacia
    var procesarCausaVacia = function() {
        var grid = Ext.getCmp('grid');
        var columnModelGrid = grid.getColumnModel();
        var store = grid.getStore();
        var errorValidacion = false;
        var b = "";
        store.each(function(record) {
            numRegistro = store.indexOf(record);

            if ((record.data['estatusBloqueoActual'] === 'B' && record.data['CS_BLOQUEADO'] !== 'B') || (record.data['CS_BLOQUEADO'] === 'B' && record.data['estatusBloqueoActual'] === '')) {
                if ((record.data['CS_BLOQUEADO'] === 'B' && record.data['estatusBloqueoActual'] === ''))
                    b = "desbloqueo";
                else
                    b = "bloqueo";
                if (record.data['CG_CAUSA'] === '') {
                    errorValidacion = true;
                    Ext.MessageBox.alert('Error de validaci�n', ' Favor de escribir la causa de ' + b,
                            function() {
                                //Ext.getCmp('btnConfirmar').enable();
                                Ext.getCmp('txtCausa').setDisabled(false);
                                Ext.getCmp('txtCausa').setReadOnly(false);
                                grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('CG_CAUSA'));
                            }
                    );
                    return false;
                }
                Ext.getCmp('txtCausa').setReadOnly(true);
                Ext.getCmp('txtCausa').setDisabled(true);


            } else {
                if ((record.data['CS_BLOQUEADO'] === 'N' && record.data['estatusBloqueoActual'] === ''))
                    b = "desbloqueo";
                else
                    b = "bloqueo";
                if (record.data['CG_CAUSA'] === '') {
                    errorValidacion = true;
                    Ext.MessageBox.alert('Error de validaci�n', ' Favor de escribir la causa de ' + b,
                            function() {
                                Ext.getCmp('txtCausa').setDisabled(false);
                                Ext.getCmp('txtCausa').setReadOnly(false);
                                grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('CG_CAUSA'));
                            }
                    );
                    return false;
                }
                Ext.getCmp('txtCausa').setReadOnly(true);
                Ext.getCmp('txtCausa').setDisabled(true);
            }

            Ext.getCmp('txtCausa').setReadOnly(true);
            Ext.getCmp('txtCausa').setDisabled(true);
            ;
        });

        if (errorValidacion) {
            return;
        } else {
            Ext.getCmp('txtCausa').setReadOnly(true);
            Ext.getCmp('txtCausa').setDisabled(true);
        }

        Ext.getCmp('txtCausa').setReadOnly(true);
        Ext.getCmp('txtCausa').setDisabled(true);
        store.commitChanges();
    };
//******************************************************* Fin procesarCausaVacia
var x;
//******************************************************* sm
    var sm = new Ext.grid.CheckboxSelectionModel({
        header: 'Bloq',
        checkOnly: true,
        id: 'chk',
        tooltip: 'Bloqueada',
        width: 32,
		  moveEditorOnEnter :false,
        singleSelect: false,
        listeners: {
            rowselect: function(sm, rowIndex, record) {
                Ext.getCmp('fila').setValue(rowIndex);
                record.data['estatusBloqueoActual'] = 'B';
					 record.data['fila']=rowIndex;
					
                x=rowIndex;
                var gridConsulta = Ext.getCmp('grid');
                var store = gridConsulta.getStore();
                var reg = gridConsulta.getStore().getAt(rowIndex);
                var columnModelGrid = gridConsulta.getColumnModel();
                Ext.getCmp('texto').setValue(reg.get('CG_CAUSA'));
					
					
					 
					 if (record.data['b1'] === record.data['CS_BLOQUEADO'] ){
						 if (record.data['CS_BLOQUEADO'] === 'N') {
							  record.data['CG_CAUSA'] = '';
								record.data['editable'] = 'S'; 
								
									Ext.getCmp('txtCausa').setDisabled(false);
									Ext.getCmp('txtCausa').setReadOnly(false);
									gridConsulta.startEditing(rowIndex, columnModelGrid.findColumnIndex('CG_CAUSA'));
						  
						 } else {
							
						 }
					 } else if ( record.data['b1'] == '' ){
							
							var bloq= record.data['CS_BLOQUEADO'];
							record.data['b1']= bloq;
							
							if (record.data['CS_BLOQUEADO'] == 'N'  ){
								var cau = record.data['CG_CAUSA'];
								record.data['aux_causa']= cau; 
								
								var bloq= record.data['CS_BLOQUEADO'];
								record.data['aux_bloq']= bloq;
								record.data['CG_CAUSA'] = '';
								
						  	record.data['editable'] = 'S';
							Ext.getCmp('txtCausa').setDisabled(false);
							Ext.getCmp('txtCausa').setReadOnly(false);
							//var rowIndex=record.data['fila'];
							}
					}
					 else {
						var rowIndex=record.data['fila'];
						record.data['editable'] = 'N';
						var caus = record.data['aux_causa'];
						if (caus !=''){
							record.data['CG_CAUSA'] = record.data['aux_causa'];
							}
					 }
                reg.set('CS_BLOQUEADO', 'S');
				
				
					 
            },
            rowdeselect: function(sm, rowIndex, record) {
                Ext.getCmp('fila').setValue(rowIndex);
                record.data['estatusBloqueoActual'] = '';
                record.data['fila']=rowIndex;
					
					
                var gridConsulta = Ext.getCmp('grid');
                var store = gridConsulta.getStore();
                var reg = gridConsulta.getStore().getAt(rowIndex);
                var columnModelGrid = gridConsulta.getColumnModel();
                Ext.getCmp('texto').setValue(reg.get('CG_CAUSA'));

				
					 if ( record.data['aux_bloq'] != record.data['CS_BLOQUEADO'] ){
							var bloq= record.data['CS_BLOQUEADO'];
							record.data['b1']= bloq;
						 if (record.data['CS_BLOQUEADO'] == 'S')  {
						  if (record.data['aux_bloq'] == 'N'){
								record.data['editable'] = 'N';
						     
							   record.data['CG_CAUSA'] = record.data['aux_causa'];
								record.data['b1'] = '';
								
						  } else {
							var cau = record.data['CG_CAUSA'];
							record.data['aux_causa']= cau;
							 record.data['CG_CAUSA'] = '';
								record.data['editable'] = 'S';
									Ext.getCmp('txtCausa').setDisabled(false);
									Ext.getCmp('txtCausa').setReadOnly(false);
									gridConsulta.startEditing(rowIndex, columnModelGrid.findColumnIndex('CG_CAUSA'));
							 }
						 } 
                }
					 reg.set('CS_BLOQUEADO', 'N');
					 
            }
        },
        renderer: function(value, metadata, record, rowindex, colindex, store) {
				
            if (record.data['CS_BLOQUEADO'] !== 'S') {	
                return '<div class="x-grid3-row-checker" >&#160</div>';
            } else {
                if (banderaInicio)
                    grid.getSelectionModel().selectRow(rowindex, true);
                return '<div class="x-grid3-row-checker" >&#160</div>';
            }
        }
    });
//******************************************************* Fin sm


//-------gridDetalle 
var gridDetalle = new Ext.grid.GridPanel({
		id: 'gridDetalle',
		store: detalle,
		height: 125,
		width: 70,
    frame: true,
    stripeRows: true,
		columns: [
				{
				width : 217,
				dataIndex: 'clave',
				align: 'left'
			},{
				width : 217,
				dataIndex: 'descripcion',
				align: 'center'
			}
		]
	});
//-------fin gridDetalle 

//--------------------------------Grid Consulta---------------------------------	
    var grid = new Ext.grid.EditorGridPanel({
        store: consultaData,
        title: '<center><b>Bloqueo de Servicio Captura.',
		  sortable: true,
        clicksToEdit: 1,
        hidden: true,
        id: 'grid',
        sm: sm,
        columns: [
            {
                header: 'N@E',
                tooltip: 'N@E',
                dataIndex: 'IC_NAFIN_ELECTRONICO',
                align: 'center',
					 resizable: true,
                width: 100
            }, {
                header: 'Raz�n Social',
                tooltip: 'Raz�n Social',
                dataIndex: 'CG_RAZON_SOCIAL',
					 resizable: true,
                width: 230,
                align: 'center',
                renderer: function(value) {
                    return "<div align='left'>" + value + "</div>";
                }
            }, {
                header: 'RFC',
                tooltip: 'RFC',
                dataIndex: 'CG_RFC',
					 resizable: true,
                width: 150,
                align: 'center',
                renderer: function(value) {
                    return "<div align='left'>" + value + "</div>";
                }
            }, sm, {
                header: 'Fecha/Hora <br>Bloqueo/Desbloqueo',
                tooltip: 'Fecha/Hora <br> Bloqueo/Desbloqueo',
                dataIndex: 'DF_FECHA',
					 resizable: true,
                width: 150,
                align: 'center'
            }, {
                header: 'Nombre del Usuario',
                tooltip: 'Nombre del Usuario',
                dataIndex: 'CG_NOMBRE_USUARIO',
					 resizable: true,
                width: 200,
                align: 'center',
                renderer: function(value) {
                    return "<div align='left'>" + value + "</div>";
                }
            }, {
                header: '<center>Causa</center>',
                tooltip: 'Causa',
                dataIndex: 'CG_CAUSA',
                align: 'left',
					 resizable: true,
					 width: 200,
                editor: {
                    xtype: 'textfield',
                    height: 10,
                    id: 'txtCausa',
                    readOnly: true,
                    disabled: true,
                    allowBlank	: false,
                    maxLength: 50,
                    listeners: {
                        invalid: function (field,sms){//sin perder el focus
                        
                              if (field.getValue() === "") {
                                
                              }else { 
                            }
                        },
                        blur: function(field) {//al perder el focus
						
                            //numFocus++;
                            /*if (!Ext.getCmp('txtCausa').isValid()){
                              if (field.getValue() === "") {
                                
                              }else {
                                
                              }
                            }  else  if (field.getValue() === "") {
                                //procesarCausaVacia();
                                //sm.lock( );
                                //deselectRow( row, [preventViewNotify] )
                                //sm.deselectRow(x);
                                //selectRow( row, [keepExisting], [preventViewNotify] )
                                if (Ext.getCmp('texto').getValue()== ''){
                                  sm.deselectRow(x);
                                }
                            } else {*/

//    Ext.getCmp('txtCausa').setReadOnly(false);
//    Ext.getCmp('txtCausa').setDisabled(false);
                          //  }
                        },
                        specialkey: function(field, e) {
                            if (e.getKey() === e.ENTER) {
                                if (field.getValue() === "") {
                                    //procesarCausaVacia();
                                } else
                                    Ext.getCmp('txtCausa').setReadOnly(false);
                                Ext.getCmp('txtCausa').setDisabled(false);
                            }
                        }
                    }//fin listener
                },
                renderer: function(value, metadata, registro, rowIndex, colIndex) {
                    metadata.attr = 'style="border: thin solid #3399CC;  color: black;"';
                    return value;
                },
                width: 100
            }],
        loadMask: true,
        style: 'margin:0 auto;',
        height: 300,
		  listeners : {
				cellclick: function(grid, rowIndex, columnIndex, e) {

            },
				beforeedit: function(e){
					if( e.record.data['editable'] =='S' ){
						
					}else {
						e.cancel=true;
					}
					//	grid.stopEditing(true);
					
				}
		  },
        bbar: {
            autoScroll: true,
            id: 'barra',
            displayInfo: true,
            items: ['->', '-',
                {
                    xtype: 'button',
                    id: 'btnConfirmar',
                    text: 'Confirmar',
                    iconCls: 'icoAceptar',
                    width: 50,
                    weight: 70,
                    style: 'margin:0 auto;',
                    handler: function(boton, evento) {
							  var numReg=0;
							  consultaData.each(function(record) {
									if(record.data['editable']=='S'){
										numReg++;
									}
								});
								if(numReg !=0){
								procesarConfirmar();
								} else {
								
								}
                    },
                    align: 'rigth'
                }, {
                    xtype: 'button',
                    id: 'btnCancelar',
                    text: 'Cancelar',
                    iconCls: 'icoRechazar',
                    width: 50,
                    handler: function(boton, evento) {
								 var gridConsulta = Ext.getCmp('gridNafin');	
								 var store = gridConsulta.getStore();
								 var filas=0;
								 store.each(function(record) {
									filas++
									});
								for(var i = 0;i< filas; i++){
										var rec = store.getAt(0); 
										store.remove(rec);
										//store.commitChanges();
									}
                        window.location = "/nafin/15cadenas/15pki/15mantenimiento/15dispersion/15BloqServCaptEXT.jsp";
                    },
                    weight: 70,
                    style: 'margin:0 auto;',
                    align: 'rigth'
                }]
        },
        width: 830,
        frame: true
    });
//-----------------------------Fin Grid Consulta--------------------------------

//-------------------------Store correoNafin---------------------------------
    var correoNafin = new Ext.data.ArrayStore({
        autoDestroy: true,
        storeId: 'myStore',
        idIndex: 0,
        fields: [
            {name: 'IC_PRODUCTO_NAFIN'},
            {name: 'IC_EPO'},
            {name: 'IC_NAFIN_ELECTRONICO'},
            {name: 'CG_RAZON_SOCIAL'},
            {name: 'CG_RFC'},
            {name: 'CS_BLOQUEADO'},
            {name: 'DF_FECHA'},
            {name: 'CG_NOMBRE_USUARIO'},
            {name: 'CG_CAUSA'}
        ]
    });
//--------------------------Fin Store correoNafin----------------------------

//---------------------------------gridNafin---------------------------------------------
    var gridNafin = new Ext.grid.GridPanel({
        id: 'gridNafin',
        title: '<center><b>Los cambios se realizaron con �xito',
        sortable: true,
        store: correoNafin,
        style: 'margin:0 auto;',
        frame: true,
        hidden:true,
        height: 250,
        width: 900,
        columns: [
            {
                header: 'N@E',
                tooltip: 'N@E',
                dataIndex: 'IC_NAFIN_ELECTRONICO',
                align: 'center',
                sortable: true,
                resizable: true,
                width: 180
            }, {
                header: 'Raz�n Social',
                tooltip: 'Raz�n Social',
                dataIndex: 'CG_RAZON_SOCIAL',
                width: 200,
                align: 'center',
                renderer: function(value) {
                    return "<div align='left'>" + value + "</div>";
                }
            }, {
                header: 'RFC',
                tooltip: 'RFC',
                dataIndex: 'CG_RFC',
                width: 120,
                align: 'center',
                renderer: function(value) {
                    return "<div align='left'>" + value + "</div>";
                }
            }, {
                header: 'Bloqueado',
                tooltip: 'Bloqueado',
                dataIndex: 'CS_BLOQUEADO',
                width: 120,
                align: 'center',
                renderer: function(value) {
                    if (value === "S")
                        return "<div align='center'>" + "S�" + "</div>";
                    if (value === "N")
                        return "<div align='center'>" + " No" + "</div>";

                }
            }, {
                header: 'Fecha/Hora <br>Bloqueo/Desbloqueo',
                tooltip: 'Fecha/Hora <br> Bloqueo/Desbloqueo',
                dataIndex: 'DF_FECHA',
                width: 120,
                align: 'center'
            }, {
                header: 'Nombre del Usuario',
                tooltip: 'Nombre del Usuario',
                dataIndex: 'CG_NOMBRE_USUARIO',
                width: 120,
                align: 'center',
                renderer: function(value) {
                    return "<div align='left'>" + value + "</div>";
                }
            }, {
                header: '<center>Causa</center>',
                tooltip: 'Causa',
                dataIndex: 'CG_CAUSA',
                align: 'left',
                width: 100
            }
        ],
        bbar: {
            autoScroll: true,
            id: 'barraCrear',
            displayInfo: true,
            items: ['->', '-',
                {
                    xtype: 'button',
                    id: 'btnBack',
                    text: 'Regresar',
                    iconCls: 'icoRegresar',
                    width: 50,
                    weight: 70,
                    style: 'margin:0 auto;',
                    handler: function(boton, evento) {
                        window.location = "/nafin/15cadenas/15pki/15mantenimiento/15dispersion/15BloqServCaptEXT.jsp";
                    },
                    align: 'rigth'
                }]
        }//FIN barra 
    });
//---------------------------------Fin gridNafin------------------------------------

//-------------------------------formaautenticacion---------------------------------
    var fa = new Ext.form.FormPanel({
        id: 'formaautenticacion',
        title: 'La autenticaci�n se llev� a cabo con �xito.',
        style: ' margin:0 auto;',
        hidden: true,
        frame: true,
        width: '500',
        labelWidth: 1,
        bodyStyle: 'padding: 6px',
        defaults: {
            anchor: '-20'
        },
        items: [
            gridDetalle
        ]
    });
//------------------------------Fin formaautenticacion---------------------------------

//-------------------------------Panel Consulta---------------------------------
    var fp = new Ext.form.FormPanel({
        id: 'formaDetalle',
        title: 'Bloqueo de Servicio Captura',
        style: ' margin:0 auto;',
        hidden: false,
        frame: true,
        bodyStyle: 'padding: 6px',
		  labelWidth		:25,
        defaults: {
            anchor: '-20'
        },
        items: elementosForma,
        buttons: [{
               xtype		: 'textfield',
               id       : 'fila',
               hidden   :true
              },{
               xtype		: 'textfield',
               id       : 'texto',
               hidden   :true
              },{
                xtype: 'button',
                id: 'btnBuscar',
                text: 'Buscar',
                iconCls: 'icoBuscar',
                width: 50,
                weight: 70,
                handler: function(boton, evento) {
                    banderaInicio = true;
                    var params = (fp) ? fp.getForm().getValues() : {};
                    consultaData.load({
                        params: Ext.apply(fp.getForm().getValues(), {
                            informacion: 'Consultar',
                            operacion: 'Generar',
                            start: 0,
                            limit: 15
                        })
                    });
                    grid.show();
                },
                style: 'margin:0 auto;',
                align: 'rigth'
            }, {
                xtype: 'button',
                id: 'btnCancel',
                text: 'Limpiar',
                iconCls: 'icoLimpiar',
                width: 50,
                weight: 70,
                handler: function(boton, evento) {
                    Ext.getCmp('formaDetalle').getForm().reset();
                    Ext.getCmp('grid').hide();
						    var gridConsulta = Ext.getCmp('gridNafin');	
							 var store = gridConsulta.getStore();
							 var filas=0;
							 store.each(function(record) {
								filas++
								});
							for(var i = 0;i< filas; i++){
									var rec = store.getAt(0); 
									store.remove(rec);
									//store.commitChanges();
								}
                },
                style: 'margin:0 auto;'
            }],
        height: 'auto',
        width: 500
    });
    //------------------------------Fin Panel Consulta------------------------------

    //----------------------------Contenedor Principal------------------------------
    var pnl = new Ext.Container({
        id: 'contenedorPrincipal',
        applyTo: 'areaContenido',
        width: '940',
        height: 'auto',
        frame: true,
        items: [
           NE.util.getEspaciador(10),
            fp,
           NE.util.getEspaciador(10),
            grid,
            NE.util.getEspaciador(10),
            fa,
            NE.util.getEspaciador(10),
            gridNafin

        ]
    });
//-----------------------------Fin Contenedor Principal-------------------------
    catalogoEpo.load();
});//-----------------------------------------------Fin Ext.onReady(function(){}