<%@ page
	contentType=
		"application/json;charset=UTF-8"
	import="
		net.sf.json.JSONArray,net.sf.json.JSONObject, netropology.utilerias.*, netropology.utilerias.usuarios.*, com.netro.cadenas.*, com.netro.exception.*, com.netro.dispersion.*,
		java.util.*, javax.naming.*, com.netro.model.catalogos.*, org.apache.commons.logging.Log, java.util.Arrays "
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%@ include file = "../../../15pki/certificado.jspf" %>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
String noEpo					= (request.getParameter("_cmb_epo") 	!=null)	?	request.getParameter("_cmb_epo")		:	"";
String informacion			= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String operacion 				= (request.getParameter("operacion") 	!= null) ? 	request.getParameter("operacion")	: 	"";
String infoRegresar			= "", consulta="";

strTipoUsuario = (session.getAttribute("strTipoUsuario")==null)?"":(String)session.getAttribute("strTipoUsuario");
iNoUsuario = (session.getAttribute("iNoUsuario")==null)?"":(String)session.getAttribute("iNoUsuario");
iNoCliente = (session.getAttribute("iNoCliente")==null)?"":(String)session.getAttribute("iNoCliente");
int start=0, limit=15;

HashMap 						datos 				= new HashMap();
List 							regdata			= new ArrayList();

if (informacion.equals("catalogoepo") )	{
	JSONObject 					jsonObj 	= new JSONObject();
	CatalogoEpoDispersion 	catEpo 	= new CatalogoEpoDispersion();
	catEpo.setCampoClave("ic_epo");
	catEpo.setCampoDescripcion("cg_razon_social");
	List lis = catEpo.getListaElementos();
	jsonObj.put("registros", lis);
  infoRegresar = jsonObj.toString();

} else if (informacion.equals("Consultar")){

	JSONObject 					jsonObj 	= new JSONObject();
	BloqueoServicio  paginadorext	= new BloqueoServicio();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginadorext);
	paginadorext.setIcIf(noEpo);
	try {
		if (operacion.equals("Generar")) {
			queryHelper.executePKQuery(request);
		}
	consulta = queryHelper.getJSONPageResultSet(request,start,limit);
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Confirmar")){
  String numeroCliente    	= (String) request.getSession().getAttribute("iNoCliente");
  String cveUsuario    			= (String) request.getSession().getAttribute("Clave_usuario");
	JSONObject jsonObj = new JSONObject();
	
	Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
	HashMap 						datosFirma			= new HashMap();
	HashMap						  datosConsulta		= new HashMap();
	List 							  registros		    = new ArrayList();
  
  UtilUsr utilUsr 			= new UtilUsr();
  Usuario usuario 			= utilUsr.getUsuario(cveUsuario);
  String NombreUsuario 	= usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
  String ClaveUsuario 	= usuario.getLogin();
  
	String pkcs7 	= request.getParameter("Pkcs7");
	String texto	= request.getParameter("TextoFirmado");
	
	String[] proNaf	  = request.getParameterValues("prodNafin");	
	String[] ePo		  = request.getParameterValues("epo");
  String[] check		= request.getParameterValues("check");
	String[] causa		= request.getParameterValues("causa");

for (int r=0; r<check.length ; r++){
  String ck = check[r];
  if(ck.equals("N")){
    check[r]=null;
  }
}

  List listProdNaf 	= Arrays.asList(proNaf);
	List listEpo 		= Arrays.asList(ePo);
	List listCheck 	= Arrays.asList(check);
	List listCausa 	= Arrays.asList(causa);
  //HashMap resultado = dispersion.autenticaFirmaDigital(pkcs7, textoFirmado, serial, numeroCliente);
	datosFirma=dispersion.autenticaFirmaDigital(pkcs7,texto,_serial,iNoCliente);
	String miRecibo	= (String)datosFirma.get("RECIBO");
	String miFolio 	= (String)datosFirma.get("FOLIO");

	String dispActualizacionBloqueos="";
	//dispActualizacionBloqueos=dispersion.DispActualizacionBloqueos(listCausa,listCheck,listEpo,listProdNaf,strTipoUsuario,iNoUsuario,miFolio,miRecibo);
	dispActualizacionBloqueos=dispersion.DispActualizacionBloqueos(listCausa,listCheck,listEpo,listProdNaf,NombreUsuario,iNoUsuario,miFolio,miRecibo);
  
	datosConsulta=dispersion.DispCaptBloqServsFin(listCausa, listEpo);
	String newFecha=(String)datosConsulta.get("fecha0");
 
	String[] fechaArray = newFecha.split(" ");
	String miFecha ="";
	String miHora  ="";
	for (int z=0; z<fechaArray.length; z++){
		if (z==0)			miFecha = fechaArray[z];
		if (z==1)			miHora  = fechaArray[z];
	}
  
	registros.add(datosConsulta);

	jsonObj.put("success", new Boolean (true));
	jsonObj.put("miRecibo",miRecibo);
	jsonObj.put("miFolio",miFolio);
	jsonObj.put("miFecha",miFecha);
	jsonObj.put("miHora",miHora);
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();

} 
%>
<%=    infoRegresar%>