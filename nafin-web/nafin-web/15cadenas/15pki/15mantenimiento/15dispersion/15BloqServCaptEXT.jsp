<!DOCTYPE  HTML>
<%@ page 
	contentType="text/html;charset=windows-1252" 
	import="java.util.*, netropology.utilerias.*"
	errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf"%>
<%@ include file = "../../../15pki/certificado.jspf" %>

<% String version = (String)session.getAttribute("version"); %>

<html>
  <head>
		<%@ include file="/extjs.jspf" %>
		<%if(version!=null){%>
		<%@ include file="/01principal/menu.jspf"%>
		<%}%>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
		<script type="text/javascript" src="15BloqServCaptEXT.js?<%=session.getId()%>"></script>
		<%@ include file="/00utils/componente_firma.jspf" %>
    <title>ADMIN NAFIN / Administracion / Dispersion/ Bloqueo de Servicio</title>
	 <link rel="stylesheet" href="/nafin/14seguridad/Seguridad/css/nafin.css"/>
  </head>
  
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%if(version!=null){%>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<%}%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%if(version!=null){%>
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<%}%>
			<div align="center" id="areaContenido"><div style="height:230px"></div></div>                                                                                     
		</div>
	</div>
	<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<%}%>
	<form id='formAux' name="formAux" target='_new'></form>
	</body>
</html>
