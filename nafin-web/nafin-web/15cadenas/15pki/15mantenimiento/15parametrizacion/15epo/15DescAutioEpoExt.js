Ext.onReady(function() {


    function procesarGuardar(opts, success, response) {
        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
            var  jsonData = Ext.util.JSON.decode(response.responseText);
            if (jsonData !== null){	
            
                Ext.MessageBox.alert("Mensaje","La fecha para el Descuento autom�tico EPO, se dio de alta correctamente.");
            }      
            
        } else {
            NE.util.mostrarConnError(response,opts);
	}
    };
    
    var confirmar = function(pkcs7, vtextoPlano){
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("Aceptar").enable();	
			return;
		}else  {                    
			Ext.getCmp("Aceptar").disable();	                
			Ext.Ajax.request({
			url: '15DescAutioEpoExt.data.jsp',
			params: Ext.apply({
				pkcs7:pkcs7,
				informacion: 'Guardar',
				textoPlano:vtextoPlano,				
				fechaDescuento: Ext.getCmp('fechaDescuento').getValue().format('d/m/Y')
			}),
			callback: procesarGuardar
		});  
		}
	}
     function procesarValidaDiaHabil(opts, success, response) {
        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
            var  jsonData = Ext.util.JSON.decode(response.responseText);
            if (jsonData !== null){	
             
                if(jsonData.diaHabil==='N'){
                
                    Ext.MessageBox.alert("Mensaje","La fecha es un d�a inh�bil.");
                
                
                }else  if(jsonData.diaHabil==='S'){
                
                    var  textoPlano ='Fecha de Descuento'+Ext.getCmp('fechaDescuento').getValue().format('d/m/Y');
		
                    NE.util.obtenerPKCS7(confirmar, textoPlano );	
                    
                }
            }
            
        } else {
            NE.util.mostrarConnError(response,opts);
	}
    };    
    
    
    var procesarConsultaData = function(store, arrRegistros, opts) 	{
        var fp = Ext.getCmp('formaCons');
	fp.el.unmask();
	var gridConsulta = Ext.getCmp('gridConsulta');	
        var el = gridConsulta.getGridEl();	
	if (arrRegistros !== null) {
            if (!gridConsulta.isVisible()) {
                gridConsulta.show();
            }	
		
            if(store.getTotalCount() > 0) {			
                el.unmask();
            } else {	
		el.mask('No existe fecha dada de alta para el Descuento Autom�tico EPO', 'x-mask');				
            }
	}
    };
        
    var consultaData = new Ext.data.JsonStore({
        root: 'registros',
	url: '15DescAutioEpoExt.data.jsp',
	baseParams: {
            informacion: 'Consulta'
	},
	fields: [
            {name: 'FECHA_DESCUENTO'},
            {name: 'FECHA_PARAMETRIZACION'},
            {name: 'USUARIO'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
            load: procesarConsultaData,
            exception: {
                fn: function(proxy,type,action,optionsRequest,response,args){
                    NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
                    procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
		}
            }
	}
    });

    var texto1 = ['En el d�a parametrizado se realizar� el descuento de los documentos de los Proveedores que hayan entregado su carta firmada con la aceptaci�n de dicho descuento. '];

    var ctexto1 = new Ext.form.FormPanel({		
        id: 'forma3',
	width: 580,			
	frame: true,		
	titleCollapse: false,
	hidden: false,
	bodyStyle: 'padding: 6px',	
	style: 'margin:0 auto;',
	defaultType: 'textfield',
	html: texto1.join('')			
    });
    
    
		

    var gridConsulta = new Ext.grid.GridPanel({
        id: 'gridConsulta',
        hidden: false,
	header:true,
	title: ' ',
	store: consultaData,
	style: 'margin:0 auto;',
	columns:[
            {
                header:'<center>Fecha de Descuento</center>',
		tooltip: 'Fecha de Descuento',
		sortable: true,
		dataIndex: 'FECHA_DESCUENTO',
		width: 150,
		align: 'center'
            },
            {
                header:'<center>Fecha Parametrizaci�n</center>',
		tooltip: 'Fecha Parametrizaci�n',
		sortable: true,
		dataIndex: 'FECHA_PARAMETRIZACION',
		width: 150,
		align: 'center'
            },
            {
                header:'<center>Usuario</center>',
		tooltip: 'Usuario',
		sortable: true,
		dataIndex: 'USUARIO',
		width: 260,
		align: 'left'
            }
        ],
        stripeRows: true,
        loadMask: true,
	height: 300,
	width: 580,	
	frame: true
    });
  

    var fpCons = new Ext.form.FormPanel({
        id: 'formaCons',
	width: 450,
	monitorValid: true,
	title: 'Criterios de b�squeda',
	frame: true,		
	titleCollapse: false,
	style: 'margin:0 auto;',
	bodyStyle: 'padding: 6px',
	labelWidth: 150,	
	defaultType: 'textfield',
	defaults: {
            msgTarget: 'side',
            anchor: '-20'
	},
	items: [
             {
                xtype: 'compositefield',
                fieldLabel: 'Fecha de Descuento',
                combineErrors: false,
                msgTarget: 'side',
                items: [                    
                    {
                        xtype: 'datefield',
			name: 'fechaDescuentoIni',
			id: 'fechaDescuentoIni',
			allowBlank: true,
			startDay: 0,
			width: 100,
			msgTarget: 'side',
			vtype: 'rangofecha', 
			campoFinFecha: 'fechaDescuentoFin',
			margins: '0 20 0 0'
                    },
                    {
                        xtype: 'displayfield',
                        value: 'a',
			width: 25
                    },
                    {
                        xtype: 'datefield',
			name: 'fechaDescuentoFin',
			id: 'fechaDescuentoFin',
			allowBlank: true,
			startDay: 1,
			width: 100,
			msgTarget: 'side',
			vtype: 'rangofecha',
			campoInicioFecha: 'fechaDescuentoIni',
			margins: '0 20 0 0'
                    }                                
                ]
            }
        ],		
	buttons: [
             {
                text: 'Consultar',
		id: 'Consultar',
		iconCls: 'icoBuscar',
		formBind: true,				
		handler: function(boton, evento) {
                
                    if (!Ext.isEmpty(Ext.getCmp("fechaDescuentoIni").getValue()) || !Ext.isEmpty(Ext.getCmp("fechaDescuentoFin").getValue()) ) {
                        if(Ext.isEmpty(Ext.getCmp("fechaDescuentoIni").getValue()))	{
                            Ext.getCmp("fechaDescuentoIni").markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
                            Ext.getCmp("fechaDescuentoIni").focus();
                            return;
                        }else if (Ext.isEmpty(Ext.getCmp("fechaDescuentoFin").getValue())){
                            Ext.getCmp("fechaDescuentoFin").markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
                            Ext.getCmp("fechaDescuentoFin").focus();
                            return;
                        }
                    }                
                
                    fpCons.el.mask('Enviando...', 'x-mask-loading');
                    consultaData.load({
                        params: Ext.apply(fpCons.getForm().getValues(),{
                        })
                    });	
                }
            }
		
	]
    });





    var fp = new Ext.form.FormPanel({
        id: 'forma',
	width: 300,
	monitorValid: true,
	title: 'Descuento Autom�tico EPO',
	frame: true,		
	titleCollapse: false,
	style: 'margin:0 auto;',
	bodyStyle: 'padding: 6px',
	labelWidth: 150,	
	defaultType: 'textfield',
	defaults: {
            msgTarget: 'side',
            anchor: '-20'
	},
	items: [
             {
                xtype: 'compositefield',
                fieldLabel: 'Fecha de Descuento',
                combineErrors: false,
                msgTarget: 'side',
                items: [
                    {
                        xtype: 'datefield',
                        name: 'fechaDescuento',
                        id: 'fechaDescuento',
                        allowBlank: true,
                        startDay: 0,
                        width: 100,
                        msgTarget: 'side',
                        vtype: 'rangofecha', 
                        minValue: '01/01/1901',                    
                        margins: '0 20 0 0'  
                    }
                ]
            }
        ],		
	buttons: [		
            {
                text: 'Aceptar',
		id: 'Aceptar',
		iconCls: 'icoAceptar',
		formBind: true,				
		handler: function(boton, evento) {
                    
                    var fechaDescuento = Ext.getCmp('fechaDescuento');
                   
                    if(Ext.isEmpty(fechaDescuento.getValue())){
                        fechaDescuento.markInvalid('Debe capturar la fehca de descuento ');
                        fechaDescuento.focus();
                        return;
                    }
                    
                     Ext.Ajax.request({
                        url: '15DescAutioEpoExt.data.jsp',
			params: Ext.apply({
                            informacion: 'validaDiaHabil',
                            fechaDescuento: fechaDescuento.getValue().format('d/m/Y')
                        }),
			callback: procesarValidaDiaHabil
                    });    
                                               
                }
            }
	]
    });


    var paneles = new Ext.TabPanel({
        id: 'paneles',
	activeTab:0,
	width:940,
	plain: 	true,
	defaults:{autoHeight: true},
	items:[
            {
                title: 'Captura',
		id: 'tabCaptura',
		style: 'margin:0 auto;',
		items: [
                    NE.util.getEspaciador(20),
                    fp,
                    NE.util.getEspaciador(20),
                    ctexto1,
                    NE.util.getEspaciador(20)
                ]
            },
            {
                title: 'Consulta Historial',
		id: 'tabConsulta',
		style: 'margin:0 auto;',
		items: [
                    NE.util.getEspaciador(20),
                    fpCons,
                    NE.util.getEspaciador(20),
                    gridConsulta,
                    NE.util.getEspaciador(20)
                ]
            }  
        ],
        listeners:{
            tabchange:	function(tab, panel){
            
                if (panel.getItemId() === 'tabConsulta') {
                
                    fpCons.el.mask('Enviando...', 'x-mask-loading');
                        consultaData.load({
                            params: Ext.apply(fpCons.getForm().getValues(),{
                        })
                    });	
                }
            }
        }
    });

  new Ext.Container({
        id: 'contenedorPrincipal',
	applyTo: 'areaContenido',
	width: 949,		
	style: 'margin:0 auto;',
	items: [
            NE.util.getEspaciador(20),
            paneles,            
            NE.util.getEspaciador(20)            
        ]
    });
        
                                        
        
});