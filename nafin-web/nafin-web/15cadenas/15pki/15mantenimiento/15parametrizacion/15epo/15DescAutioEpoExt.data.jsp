<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,		 
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
                com.netro.descuento.*,
                com.netro.cadenas.*,"		
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<%@ include file="../../../certificado.jspf" %>
<%
String informacion	= (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String fechaDescuento	= (request.getParameter("fechaDescuento")!=null)?request.getParameter("fechaDescuento"):"";
String fechaDescuentoIni  = (request.getParameter("fechaDescuentoIni")!=null)?request.getParameter("fechaDescuentoIni"):"";
String fechaDescuentoFin  = (request.getParameter("fechaDescuentoFin")!=null)?request.getParameter("fechaDescuentoFin"):"";
String infoRegresar = "";
JSONObject jsonObj = new JSONObject();

ConsDescAutoEPO paginador = new ConsDescAutoEPO();
paginador.setIdclaveEpo(iNoCliente);
paginador.setFechaDescuentoIni(fechaDescuentoIni);
paginador.setFechaDescuentoFin(fechaDescuentoFin);

CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

if ("validaDiaHabil".equals(informacion)  ) {

    boolean bFechaInhabil = false;
    bFechaInhabil =  paginador.getDiaHabil(fechaDescuento, iNoCliente);
    
    if(bFechaInhabil==false) {
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("diaHabil", "S");
        
    }else  if(bFechaInhabil==true) {
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("diaHabil", "N");
    }

    infoRegresar = jsonObj.toString(); 
 
}else  if ("Guardar".equals(informacion)  ) {  

    String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
    String _acuse = "", serial = "", folioCert = "", mensajeAuto="";
    String externContent = (request.getParameter("textoPlano")==null)?"":request.getParameter("textoPlano");
    char getReceipt = 'Y';
   
    try{
            
        if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
        
            folioCert = iNoCliente;
            Seguridad s = new Seguridad();
                
            if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {			
                _acuse = s.getAcuse();					
                            
                String nombreUsuario = iNoUsuario+" - "+strNombreUsuario;       
                   
                paginador.capturaFecha(iNoCliente, fechaDescuento,  nombreUsuario, _acuse  );
                
                jsonObj.put("success", new Boolean(true));
                jsonObj.put("mensaje", "La fecha para el Descuento automático EPO, se dio de alta correctamente.");
                    
            } else {	//autenticacion fallida
                String _error = s.mostrarError();
                jsonObj.put("success", new Boolean(false));
                jsonObj.put("msg", "La autentificaci\u00F3n no se llev\u00F3 a cabo. " + _error + ".<br>Proceso CANCELADO");
            }
            
        }
            
    }catch(Exception e){
        jsonObj.put("success", new Boolean(false));
        throw new Exception("Error al dar de alta la fecha .",e);
    }    
    
    infoRegresar = jsonObj.toString(); 

}else  if ("Consulta".equals(informacion)  ) {

    Registros reg = queryHelper.doSearch();  
    
    String consulta	=	"{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
    jsonObj = JSONObject.fromObject(consulta);	
    infoRegresar = jsonObj.toString();  		

}

%>
<%=infoRegresar%>