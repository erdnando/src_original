<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.text.SimpleDateFormat,
		com.netro.cadenas.*, 
		netropology.utilerias.usuarios.*, 
		com.netro.model.catalogos.*,
		com.netro.descuento.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<jsp:useBean id="BeanParamDescuento" scope="page" class="com.netro.descuento.ParametrosDescuentoBean" />
<%@ include file="/15cadenas/015secsession.jspf"%>
<%@ include file="../../../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String numeroProceso = (request.getParameter("numeroProceso")!=null)?request.getParameter("numeroProceso"):"";
String acuse = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
String fechaCarga = (request.getParameter("fechaCarga")!=null)?request.getParameter("fechaCarga"):"";
String horaCarga = (request.getParameter("horaCarga")!=null)?request.getParameter("horaCarga"):"";
String recibo = (request.getParameter("recibo")!=null)?request.getParameter("recibo"):"";
String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
	
String infoRegresar	=	"";
JSONObject jsonObj = new JSONObject();
HashMap	info = new HashMap();
JSONArray registros = new JSONArray();	
HashMap hmResCarga  = new HashMap();

ParametrosDescuento parametrosDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);


if(informacion.equals("consultaValidos")  ||  informacion.equals("consultaInValidos") ){
	
	hmResCarga = parametrosDescuento.getResLiberMasivaCtasPymes(numeroProceso);
	List lstRegOk = (List)hmResCarga.get("correctos");
	List lstRegX = (List)hmResCarga.get("errores");

	if(informacion.equals("consultaValidos") ){
		if(lstRegOk!=null && lstRegOk.size()>0){
			for(int x=0;x<lstRegOk.size();x++){
				List lstResult = (List)lstRegOk.get(x);				
				info = new HashMap();
				info.put("NO_LINEA","Linea "+(String)lstResult.get(0));
				info.put("PYME",(String)lstResult.get(1));
				info.put("IC_EPO",(String)lstResult.get(2));
				info.put("MONEDAS",(String)lstResult.get(3));
				registros.add(info);
			}
		}		
		infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

	}else if(informacion.equals("consultaInValidos") ){
	
		if(lstRegX!=null && lstRegX.size()>0){
			for(int x=0;x<lstRegX.size();x++){
				List lstResult = (List)lstRegX.get(x);				
				info = new HashMap();
				info.put("NO_LINEA","Linea "+(String)lstResult.get(0));
				info.put("ERROR",(String)lstResult.get(4));
				registros.add(info);
			}
		}		
		infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

	}
}else  if(informacion.equals("ArchivoErrores")  ){

	String nombreArchivo = 	parametrosDescuento.generarArchError(numeroProceso,  strDirectorioTemp );			
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();

}else  if(informacion.equals("ConsultarPreAcuse")  ){

	List lstResPreAcuse = parametrosDescuento.getPreAcuseLiberMasivaPymes(numeroProceso);
	if(lstResPreAcuse!=null && lstResPreAcuse.size()>0){
		for(int x=0;x<lstResPreAcuse.size();x++){
			List lstRegProc = (List)lstResPreAcuse.get(x);
			info = new HashMap();
			info.put("NOMBRE_EPO",lstRegProc.get(3)==null?"":(String)lstRegProc.get(3));
			info.put("NOMBRE_PYME",lstRegProc.get(4)==null?"":(String)lstRegProc.get(4));
			info.put("MONEDA",lstRegProc.get(5)==null?"":(String)lstRegProc.get(5));
			info.put("MOVIMIENTO",lstRegProc.get(6)==null?"":(String)lstRegProc.get(6));
			info.put("DOMICILIO",lstRegProc.get(7)==null?"":(String)lstRegProc.get(7));
			info.put("ALTA_HACIENDA",lstRegProc.get(8)==null?"":(String)lstRegProc.get(8));
			info.put("CEDULA_RFC",lstRegProc.get(9)==null?"":(String)lstRegProc.get(9));
			info.put("IDENTIFICACION",lstRegProc.get(10)==null?"":(String)lstRegProc.get(10));
			info.put("ESTADO_CUENTA",lstRegProc.get(11)==null?"":(String)lstRegProc.get(11));
			info.put("CONVENIO_PYME_NAFIN",lstRegProc.get(12)==null?"":(String)lstRegProc.get(12));
			info.put("OTROS_DOCTOS_FACTORAJE",lstRegProc.get(13)==null?"":(String)lstRegProc.get(13));
			info.put("CONTRATO_IF",lstRegProc.get(14)==null?"":(String)lstRegProc.get(14));
			info.put("NOTAS","".equals((String)lstRegProc.get(15))?"":(String)lstRegProc.get(15));
			info.put("RECHAZO",lstRegProc.get(16)==null?"":(String)lstRegProc.get(16));			
			registros.add(info);					
		}
		infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	}

}else  if(informacion.equals("GenerarAcuse")  ){

	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
	String texto_plano = (request.getParameter("texto_plano")!=null)?request.getParameter("texto_plano"):"";
	String _acuse = "500", serial = "", folioCert = "", mensajeAuto="";
	String externContent = (request.getParameter("texto_plano")==null)?"":request.getParameter("texto_plano");
	char getReceipt = 'Y';
	fechaCarga = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
	horaCarga = (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());	
	String usuario =iNoUsuario+" "+strNombreUsuario;
	
	
	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		folioCert = "LIBERMASIVA"+numeroProceso;
		Seguridad s = new Seguridad();
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {			
			_acuse = s.getAcuse();		
			String cveIf = iNoCliente;
			BeanParamDescuento.confirmaLiberMasivaCtasPyme(numeroProceso, cveIf, strLogin, String.valueOf(iNoNafinElectronico), strNombreUsuario, strNombre);
			mensajeAuto=" Acuse de Liberación Masiva   La autentificación se llevó a cabo con éxito Recibo:"+_acuse;				
		}  else { 
			mensajeAuto ="La autentificación no se llevó  a cabo: "+s.mostrarError();			
			_acuse ="";
		}
	}	
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensajeAuto", mensajeAuto);	
	jsonObj.put("recibo", _acuse);
	jsonObj.put("fechaCarga", fechaCarga);
	jsonObj.put("horaCarga", horaCarga);
	jsonObj.put("acuse", numeroProceso);
	jsonObj.put("numeroProceso", numeroProceso);
	jsonObj.put("usuario",usuario);	
	infoRegresar = jsonObj.toString();
	
}else  if(informacion.equals("ConsultaAcuse")  ){
	
	
	List lstAcuseLibMasiva = parametrosDescuento.getAcuseLiberMasivaPymes(numeroProceso);
	if(lstAcuseLibMasiva!=null && lstAcuseLibMasiva.size()>0){
		for(int x=0;x<lstAcuseLibMasiva.size();x++){
			List lstRegProc = (List)lstAcuseLibMasiva.get(x);
			info = new HashMap();
			info.put("NOMBRE_EPO",lstRegProc.get(3)==null?"":(String)lstRegProc.get(3));
			info.put("NOMBRE_PYME",lstRegProc.get(4)==null?"":(String)lstRegProc.get(4));
			info.put("MONEDA",lstRegProc.get(5)==null?"":(String)lstRegProc.get(5));
			info.put("MOVIMIENTO",lstRegProc.get(6)==null?"":(String)lstRegProc.get(6));
			info.put("DOMICILIO",lstRegProc.get(7)==null?"":(String)lstRegProc.get(7));
			info.put("ALTA_HACIENDA",lstRegProc.get(8)==null?"":(String)lstRegProc.get(8));
			info.put("CEDULA_RFC",lstRegProc.get(9)==null?"":(String)lstRegProc.get(9));
			info.put("IDENTIFICACION",lstRegProc.get(10)==null?"":(String)lstRegProc.get(10));
			info.put("ESTADO_CUENTA",lstRegProc.get(11)==null?"":(String)lstRegProc.get(11));
			info.put("CONVENIO_PYME_NAFIN",lstRegProc.get(12)==null?"":(String)lstRegProc.get(12));
			info.put("OTROS_DOCTOS_FACTORAJE",lstRegProc.get(13)==null?"":(String)lstRegProc.get(13));
			info.put("CONTRATO_IF",lstRegProc.get(14)==null?"":(String)lstRegProc.get(14));
			info.put("NOTAS","".equals((String)lstRegProc.get(15))?"":(String)lstRegProc.get(15));
			info.put("RECHAZO",lstRegProc.get(16)==null?"":(String)lstRegProc.get(16));			
			registros.add(info);					
		}		
	}
	
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
}else  if(informacion.equals("GenerarPDFAcuse")  || informacion.equals("GenerarCSVAcuse")    ){

	List parametros = new ArrayList();
	parametros.add(numeroProceso);
	parametros.add(recibo);
	parametros.add(acuse);
	parametros.add(fechaCarga);
	parametros.add(horaCarga);	
	parametros.add( iNoUsuario+" "+strNombreUsuario);
	parametros.add(strDirectorioTemp);	
	parametros.add((String)session.getAttribute("strPais"));
	parametros.add(strNombre);
	parametros.add(strNombreUsuario);
	parametros.add(strLogo);
	parametros.add(strDirectorioPublicacion);	
	parametros.add(tipoArchivo);	
	parametros.add(iNoCliente);
	
	String nombreArchivo = parametrosDescuento.archivAcuseCSVyPDF(parametros);
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>
