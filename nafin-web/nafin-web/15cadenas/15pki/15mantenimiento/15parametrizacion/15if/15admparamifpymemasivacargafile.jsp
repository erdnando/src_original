<%@ page contentType="text/html; charset=UTF-8"
	import="
		javax.naming.*,		
		java.sql.*,
		com.netro.exception.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*, 	
		netropology.utilerias.*, 
		java.util.*,
		java.io.*,				
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,				
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String rutaArchivoTemporal  ="";
	ParametrosRequest req = null;
	String itemArchivo	="",  rutaArchivo ="",   error_tam =""; 
	String PATH_FILE	=	strDirectorioTemp;
	String nombreArchivo= "";
	System.out.println("ServletFileUpload.isMultipartContent(request)=="+ServletFileUpload.isMultipartContent(request));
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
	
		req = new ParametrosRequest(upload.parseRequest(request));
			 
		FileItem fItem = (FileItem)req.getFiles().get(0);
		itemArchivo		= (String)fItem.getName();
		InputStream archivo = fItem.getInputStream();
		rutaArchivo = PATH_FILE+itemArchivo;
		int tamanio			= (int)fItem.getSize();
		
		nombreArchivo= Comunes.cadenaAleatoria(16)+ ".txt";
		rutaArchivoTemporal = PATH_FILE + "/" + nombreArchivo;
		fItem.write(new File(rutaArchivoTemporal));
		
		if(tamanio>1048576){
			error_tam ="El Archivo es muy Grande, excede el Límite que es de 1 MB.";
		}		
	}
	
	// metodo para validar el archivo
	List  datos = this.validaCargaArchivo( strDirectorioTemp,  rutaArchivoTemporal,  iNoCliente );
		
	error_tam=(String)datos.get(0);
	String numeroProceso=(String)datos.get(1);
	String noErrores = (String)datos.get(2);
	String regValidos = (String)datos.get(3);

	if(!error_tam.equals("")){
		numeroProceso="";
	}
	
%>
{
	"success": true,
	"numeroProceso":	'<%=numeroProceso%>',
	"error_tam":	'<%=error_tam%>',
	"noErrores":	'<%=noErrores%>',
	"regValidos":	'<%=regValidos%>'	
}

<%!
	public List  validaCargaArchivo(String strDirectorioTemp, String ruta, String iNoCliente ) throws NafinException {
		
		AccesoDB con =new AccesoDB();
		String 		numeroProceso ="", linea = "", mensaje ="", noErrores = "0", regValidos ="0";
		BufferedReader 	brt =  null;
		BufferedReader 	br						= 	null;
		int totaldoctos =	0, numMaxRegistros  =	2500;
		List informacion = new ArrayList();
		
		
		try{	
			
			ParametrosDescuento parametrosDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

			ServletFileUpload myUpload = new ServletFileUpload();
			
			con.conexionDB();
			java.io.File ft = new java.io.File(ruta);
			System.out.println("ft");
			brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
			while((linea=brt.readLine())!=null)
				totaldoctos++;
			if( totaldoctos > numMaxRegistros  || totaldoctos <= 1 ){
				if(totaldoctos <= 1) {
					mensaje = "El archivo proporcionado está vacío.";
				}else{ 
					mensaje = "	El archivo proporcionado tiene"+totaldoctos+" registros, por lo que excede el límite de registros permitidos, que es "+numMaxRegistros+"registros.";
				}								
			}else {
				String name	="",  value ="",  cboTipoAfiliacion 	="",  Epo	="";
				
				VectorTokenizer 	vtd	 		 = null;
				Vector 				vecdet 		 = null;
				int iExistePD	 = 0, resultado = 0, tipo_cliente = 0, numero_sirac = 0, iTotalPymes  = 0,iTotalReg    = 0;
				StringBuffer error = new StringBuffer();
				StringBuffer bien = new StringBuffer();
				StringBuffer lineabien = new StringBuffer();
				StringBuffer lineaerror = new StringBuffer();
				String 				tipoclient	 ="1";		
				java.io.File f=new java.io.File(ruta);
				br		=	new BufferedReader(new InputStreamReader(new FileInputStream(f)));
				
			// Obtener numero unico que identifica al proceso de carga de datos
				
				String 		qry 						= "SELECT SEQ_COMTMP_CTASBANC_PYMES_MAS.NEXTVAL FROM DUAL";
				ResultSet	rsNumeroProceso		= con.queryDB(qry);
				if( rsNumeroProceso.next()){
					numeroProceso	= rsNumeroProceso.getString(1);					
				}
				rsNumeroProceso.close();
				con.cierraStatement();
			
				// Insertar datos en la tabla COMTMP_AFILIA_PYME_MASIVA
				parametrosDescuento.setDatosLiberMasivaPyme(ruta,numeroProceso,iNoCliente);
				parametrosDescuento.validaCargaLiberMasiva(numeroProceso,iNoCliente);			
			}
			
			if(brt != null ) brt.close();
			if(br  != null ) br.close();
			
			//esto es para saber la cantidad de errores que hay  y poder mostrar el boton de descarga de errores 
			
			if(!numeroProceso.equals("")){
				HashMap hmResCarga  = new HashMap();				
				hmResCarga = parametrosDescuento.getResLiberMasivaCtasPymes(numeroProceso);
				List lstRegX = (List)hmResCarga.get("errores");
				List lstRegOk = (List)hmResCarga.get("correctos");
				if(lstRegX!=null && lstRegX.size()>0){
					noErrores = String.valueOf( lstRegX.size());
				}
				if(lstRegOk!=null && lstRegOk.size()>0){
					regValidos = String.valueOf( lstRegOk.size());
				}
			}
			
			informacion.add(mensaje);
			informacion.add(numeroProceso);
			informacion.add(noErrores);
			informacion.add(regValidos);			
			
			
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {				
			
			if(con.hayConexionAbierta())  con.cierraConexionDB();	
		}
			return informacion;
	}
	
%>
