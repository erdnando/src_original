<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<%
   String num_naf_elect = request.getParameter("num_naf_elect")==null?"":request.getParameter("num_naf_elect");	
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS)  { %>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="../../../certificado.jspf" %>

<script language="JavaScript1.2" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>

<script type="text/javascript" src="15admparamifpymeext.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%if(esEsquemaExtJS)  { %>
<%@ include file="/01principal/01if/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01if/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'>
      <input type="hidden" name="num_naf_elect" id="num_naf_elect" value="<%=num_naf_elect%>" />
   </form>
	
<%	}else  { %>
	
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
   </div>
	</div>
	<form id='formAux' name="formAux" target='_new'>
	 <input type="hidden" name="num_naf_elect" id="num_naf_elect" value="<%=num_naf_elect%>" />
	</form>
<% }%>
</body>
</html>