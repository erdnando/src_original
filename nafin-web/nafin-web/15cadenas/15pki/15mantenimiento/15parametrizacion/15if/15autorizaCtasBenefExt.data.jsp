<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.text.SimpleDateFormat,
		com.netro.model.catalogos.*,
		javax.naming.Context,
		com.netro.descuento.*,
		net.sf.json.JSONArray,
		com.netro.factdistribuido.*,
		com.netro.seguridadbean.SeguException,
		org.apache.commons.logging.Log,
		net.sf.json.JSONObject,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<%@ include file="../../../certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
int start = 0;
int limit = 0;
String infoRegresar = "";
JSONObject jsonObj  = new JSONObject();
String informacion  = (request.getParameter("informacion")    !=null)?request.getParameter("informacion")    :"";
String nombrePyme   = (request.getParameter("nombrePyme")     !=null)?request.getParameter("nombrePyme")     :"";
String rfcPyme      = (request.getParameter("rfcPyme")        !=null)?request.getParameter("rfcPyme")        :"";
String numNE        = (request.getParameter("num_ne_pyme")    !=null)?request.getParameter("num_ne_pyme")    :"";
String icEpo        = (request.getParameter("ic_epo")         !=null)?request.getParameter("ic_epo")         :"";
String icBenef      = (request.getParameter("ic_benef")       !=null)?request.getParameter("ic_benef")       :"";
String fechaAltaIni = (request.getParameter("fecha_alta_ini") !=null)?request.getParameter("fecha_alta_ini") :"";
String fechaAltaFin = (request.getParameter("fecha_alta_fin") !=null)?request.getParameter("fecha_alta_fin") :"";
String cambioCuenta = (request.getParameter("cambio_cuenta")  !=null)?request.getParameter("cambio_cuenta")  :"";
String operacion    = (request.getParameter("operacion")      !=null)?request.getParameter("operacion")      :"";
String pkcs7        = (request.getParameter("pkcs7")          !=null)?request.getParameter("pkcs7")          :"";
String externContent= (request.getParameter("textoFirmado")   !=null)?request.getParameter("textoFirmado")   :"";
String numAcuse     = (request.getParameter("acuse")          !=null)?request.getParameter("acuse")          :"";
String fecha        = (request.getParameter("fecha")          !=null)?request.getParameter("fecha")          :"";
String usuario      = (request.getParameter("usuario")        !=null)?request.getParameter("usuario")        :"";
String numRecibo    = (request.getParameter("num_recibo")     !=null)?request.getParameter("num_recibo")     :"";
String consecutivo  = (request.getParameter("consecutivo")    !=null)?request.getParameter("consecutivo")    :"";
String[] autorizados= request.getParameterValues("autorizados");

ParametrosDescuento paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

if("CONSULTA_CAMBIOS_CUENTAS".equals(informacion)){

	infoRegresar = paramDsctoEJB.obtenerJSONCambioCuentasPorAutorizar(iNoNafinElectronico);

} else if("CATALOGO_EPO".equals(informacion)) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setDistinc(true);
	catalogo.setCampoClave("A.IC_EPO");
	catalogo.setCampoDescripcion("A.CG_RAZON_SOCIAL");
	catalogo.setTabla("COMCAT_EPO A, COMREL_PRODUCTO_EPO B");
	catalogo.setCondicionesAdicionales("A.IC_EPO=B.IC_EPO");
	catalogo.setCondicionesAdicionales("B.CS_FACTORAJE_DISTRIBUIDO='S'");
	catalogo.setOrden("A.CG_RAZON_SOCIAL");
	infoRegresar = catalogo.getJSONElementos();	

} else if("CATALOGO_BENEFICIARIO".equals(informacion)) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setDistinc(true);
	catalogo.setCampoClave("A.IC_IF");
	catalogo.setCampoDescripcion("A.CG_RAZON_SOCIAL");
	catalogo.setOrden("A.CG_RAZON_SOCIAL");
	catalogo.setTabla("COMCAT_IF A, COMREL_IF_EPO B");
	catalogo.setCondicionesAdicionales("A.IC_IF=B.IC_IF");
	catalogo.setCondicionesAdicionales("B.CS_FACTORAJE_DISTRIBUIDO='S'");
	catalogo.setCondicionesAdicionales("B.CS_BENEFICIARIO='S'");
	if(!"".equals(icEpo)) {
		catalogo.setCondicionesAdicionales("B.IC_EPO="+icEpo);
	}
	infoRegresar = catalogo.getJSONElementos();

} else if("CONSULTA_NOMBRE_PYME".equals(informacion)) {

	String icPyme    = "";
	String txtNombre = "";
	List datosPymes = paramDsctoEJB.datosPymes(numNE);
	if(datosPymes.size()>0){
		icPyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("ic_pyme", icPyme);
	jsonObj.put("txtNombre", txtNombre);
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("BUSQUEDA_AVANZADA")) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setDistinc(true);
	catalogo.setCampoClave("N.IC_NAFIN_ELECTRONICO");
	catalogo.setCampoDescripcion("P.CG_RAZON_SOCIAL");
	catalogo.setTabla("COMREL_NAFIN N, COMCAT_PYME P");
	catalogo.setCondicionesAdicionales("N.IC_EPO_PYME_IF = P.IC_PYME AND P.CS_FACT_DISTRIBUIDO='S'");
	if(!"".equals(rfcPyme)) {
		catalogo.setCondicionesAdicionales("P.CG_RFC LIKE NVL (REPLACE (UPPER ('"+rfcPyme+"'), '*', '%'), '%')");
	}
	if(!"".equals(nombrePyme)) {
		catalogo.setCondicionesAdicionales("P.CG_RAZON_SOCIAL LIKE NVL (REPLACE (UPPER ('"+nombrePyme+"'), '*', '%'), '%')");
	}
	catalogo.setOrden("P.CG_RAZON_SOCIAL");
	infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("CONSULTAR_GRID") || informacion.equals("GENERAR_CSV") || informacion.equals("GENERAR_PDF")) {

	ConsultaAutorizacionBenef paginador = new ConsultaAutorizacionBenef();
	paginador.setIcNafinElectronico(Long.parseLong("".equals(numNE)?"0":numNE));
	paginador.setIcEpo(Long.parseLong("".equals(icEpo)  ?"0":icEpo));
	paginador.setIcBeneficiario(Long.parseLong("".equals(icBenef)?"0":icBenef));
	paginador.setFechaAltaIni(!"".equals(fechaAltaIni)?fechaAltaIni.substring(0, 10):"");
	paginador.setFechaAltaFin(!"".equals(fechaAltaFin)?fechaAltaFin.substring(0, 10):"");
	paginador.setCambioCta("true".equals(cambioCuenta)?true:false);
	paginador.setAcuse(numAcuse);
	paginador.setFecha(fecha);
	paginador.setUsuario(usuario);
	paginador.setRecibo(numRecibo);
	paginador.setIcIf(iNoNafinElectronico);
	paginador.setIcConsecutivo(Long.parseLong("".equals(consecutivo)  ?"0":consecutivo));
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	if (informacion.equals("CONSULTAR_GRID")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try{
			if("GENERAR".equals(operacion)){
				queryHelper.executePKQuery(request);
			}
			String consulta = queryHelper.getJSONPageResultSet(request,start,limit);
			jsonObj = JSONObject.fromObject(consulta);
		}catch(Exception e){
			throw new AppException("Error en la paginación",e);
		}
	} else if (informacion.equals("GENERAR_CSV") || informacion.equals("GENERAR_PDF")) {
		String tipoArchivo = "PDF";
		try {
			if (informacion.equals("GENERAR_CSV")){
				tipoArchivo = "CSV";
			}
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo " + tipoArchivo, e);
		}
	}
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("AUTORIZAR")) {
	for(int i=0;i<autorizados.length;i++) {
		System.out.println("..." + autorizados[i] + "...");
	}

	Acuse   acuse        = new Acuse(Acuse.ACUSE_IF, "1");
	boolean exito        = false;
	char    getReceipt   = 'Y';
	String  folioCert    = "";
	String  _acuse       = "";
	String  recibo       = "1";
	String  mensajeError = "";
	String fechaAutorizacion = "";

	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		folioCert = acuse.toString();
		Seguridad s = new Seguridad();
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			try {
				_acuse = s.getAcuse();
				exito = true;
			} catch (Throwable e) {
				exito = false;
				jsonObj.put("success", new Boolean(false));
				throw new AppException("Error al procesar la firma digital. ", e);
			}
		}
	} else{
		exito = false;
		mensajeError = "No se pudo procesar la firma digital. El serial, texto a firmar o la firma digital son incorrectas.";
	}

	//Si se procesó correctamente la firma digital, se manda a actualizar los campos correspondientes
	if(exito == true) {
		try {
			Long icIf = 19L;
			fechaAutorizacion = paramDsctoEJB.autorizarCuentasBeneficiarios(autorizados, folioCert, _acuse, iNoUsuario, iNoNafinElectronico);
			jsonObj.put("success", new Boolean(true));
		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al descargar el archivo. ", e);
		}
	}
	jsonObj.put("acuse", folioCert);
	jsonObj.put("recibo", _acuse);
	jsonObj.put("fechaAutorizacion", fechaAutorizacion);
	jsonObj.put("usuario", iNoUsuario + " - " + strNombreUsuario);
	jsonObj.put("mensajeErr", mensajeError);//Se manda en caso de que exista un mensaje de error
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("CONSULTAR_ACUSE_DETALLE")) {

	infoRegresar = paramDsctoEJB.obtenerJSONCuentasAutorizadas(numAcuse);

}
%>
<%=infoRegresar%>