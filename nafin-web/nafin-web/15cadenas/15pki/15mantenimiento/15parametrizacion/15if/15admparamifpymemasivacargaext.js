Ext.onReady(function() {


	// ***********************Mostrar Acuse ********************

	//generaci�n del CSV del Acuse
	var procesarGenerarCSVAcuse =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarCSVAcuse');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarCSVAcuse');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//generaci�n del PDF del Acuse
	var procesarGenerarPDFAcuse =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarPDFAcuse');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarPDFAcuse');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarAcuseData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridAcuse = Ext.getCmp('gridAcuse');	
	
		if (arrRegistros != null) {
			if (!gridAcuse.isVisible()) {
				gridAcuse.show();
			}						
			//edito el titulo de la columna
			var el = gridAcuse.getGridEl();
			var cm = gridAcuse.getColumnModel();
			var jsonData = store.reader.jsonData;	
							
			if(store.getTotalCount() > 0) {								
				Ext.getCmp("gridPreAcuse").hide();												
				el.unmask();					
			} else {	
				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaAcuseData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15admparamifpymemasivacarga.data.jsp',
		baseParams: {
			informacion: 'ConsultaAcuse'
		},		
		fields: [			
			{name: 'NOMBRE_EPO'},	
			{name: 'NOMBRE_PYME'},	
			{name: 'MONEDA'},	
			{name: 'MOVIMIENTO'},	
			{name: 'DOMICILIO'},	
			{name: 'ALTA_HACIENDA'},	
			{name: 'CEDULA_RFC'},	
			{name: 'IDENTIFICACION'},	
			{name: 'ESTADO_CUENTA'},	
			{name: 'CONVENIO_PYME_NAFIN'},	
			{name: 'OTROS_DOCTOS_FACTORAJE'},	
			{name: 'CONTRATO_IF'},	
			{name: 'NOTAS'},	
			{name: 'RECHAZO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarAcuseData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarAcuseData(null, null, null);					
				}
			}
		}		
	});
	var errorRenderer = function( value, metadata, record, rowIndex, colIndex, store){
	 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; text-align: left;" ';
		return value;
			
	}
	var gridAcuse = new Ext.grid.GridPanel({
		store: consultaAcuseData,
		id: 'gridAcuse',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Acuse Liberaci�n Masiva',
		columns: [	
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'MONEDA',
				tooltip: 'MONEDA',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'MOVIMIENTO',
				tooltip: 'MOVIMIENTO',
				dataIndex: 'MOVIMIENTO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'COMPROBANTE DOMICILIO',
				tooltip: 'COMPROBANTE DOMICILIO',
				dataIndex: 'DOMICILIO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'COMPROBANTE ALTA HACIENDA',
				tooltip: 'COMPROBANTE ALTA HACIENDA',
				dataIndex: 'ALTA_HACIENDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'CEDULA_RFC',
				tooltip: 'CEDULA_RFC',
				dataIndex: 'CEDULA_RFC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'IDENTIFICACI�N OFICIAL VIGENTE',
				tooltip: 'IDENTIFICACI�N OFICIAL VIGENTE',
				dataIndex: 'IDENTIFICACION',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'ESTADO CUENTA CHEQUES',
				tooltip: 'ESTADO CUENTA CHEQUES',
				dataIndex: 'ESTADO_CUENTA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'CONVENIO PYME NAFIN',
				tooltip: 'CONVENIO PYME NAFIN',
				dataIndex: 'CONVENIO_PYME_NAFIN',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'OTROS DOCTOS FACTORAJE',
				tooltip: 'OTROS DOCTOS FACTORAJE',
				dataIndex: 'OTROS_DOCTOS_FACTORAJE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'CONTRATO IF',
				tooltip: 'CONTRATO IF',
				dataIndex:'CONTRATO_IF',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'NOTAS',
				tooltip: 'NOTAS',
				dataIndex:'NOTAS',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAUSA DE RECHAZO',
				tooltip: 'CAUSA DE RECHAZO',
				dataIndex:'RECHAZO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {		
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generaci�n PDF',
					tooltip:	'Generaci�n PDF',					
					id: 'btnGenerarPDFAcuse',
					handler: function(boton, evento) {
						var numeroProceso = Ext.getCmp('numeroProceso').getValue();
						var  fechaCarga = Ext.getCmp('fechaCarga').getValue();						
						var  horaCarga =  Ext.getCmp('horaCarga').getValue();
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15admparamifpymemasivacarga.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GenerarPDFAcuse',
								numeroProceso:numeroProceso,
								fechaCarga:fechaCarga,
								horaCarga:horaCarga,
								acuse:numeroProceso,
								tipoArchivo:'PDF'
							}),
							callback: procesarGenerarPDFAcuse
						});	
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFAcuse',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generaci�n CSV',
					tooltip:	'Generaci�n CSV',					
					id: 'btnGenerarCSVAcuse',
					handler: function(boton, evento) {
						var numeroProceso = Ext.getCmp('numeroProceso').getValue();
						var  fechaCarga = Ext.getCmp('fechaCarga').getValue();						
						var  horaCarga =  Ext.getCmp('horaCarga').getValue();
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15admparamifpymemasivacarga.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GenerarCSVAcuse',
								numeroProceso:numeroProceso,
								fechaCarga:fechaCarga,
								horaCarga:horaCarga,
								acuse:numeroProceso,
								tipoArchivo:'CSV'
							}),
							callback: procesarGenerarCSVAcuse
						});	
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVAcuse',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Salir',
					tooltip:	'Salir',
					iconCls: 'icoLimpiar',
					id: 'btnSalir',
					handler: function() {
						window.location = '/nafin/15cadenas/15mantenimiento/15parametrizacion/15if/15admparamifpymemasivaext.jsp?origenPantalla=PKI';
					}
				}	
			]
		}
	});

	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 150,
		style: 'margin:0 auto;',		
		frame: true
	});
	
	var mensajeAutorizacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutorizacion',							
		width:	'250',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 	xtype: 'displayfield',  id: 'mensajeAuto', 	value: '' }			
		]
	});
					
	function procesarSuccessAcuse(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
				
				if(jsonData.recibo !='') {				
					var acuseCifras = [
						['N�mero de Acuse', jsonData.acuse],
						['Fecha de Carga ', jsonData.fechaCarga],
						['Hora de Carga', jsonData.horaCarga],
						['Usuario de Captura ', jsonData.usuario]
					];
					
					storeCifrasData.loadData(acuseCifras);	
					Ext.getCmp('gridCifrasControl').show();
					Ext.getCmp('gridAcuse').show();
					
					consultaAcuseData.load({
						params:{
							informacion:'ConsultaAcuse',
							numeroProceso:jsonData.numeroProceso
						}
					});	
					
				}else {
					Ext.getCmp("fpBotones").show();	
				}
				
				Ext.getCmp("mensajeAuto").setValue(jsonData.mensajeAuto);					
				Ext.getCmp('mensajeAutorizacion').show();
				Ext.getCmp("gridPreAcuse").hide();	
				Ext.getCmp('fechaCarga').setValue(jsonData.fechaCarga);
				Ext.getCmp('horaCarga').setValue(jsonData.fechaCarga);
				
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}		
		
	var confirmar = function(pkcs7, texto_plano , numeroProceso){
				
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else  {
		
			Ext.Ajax.request({
				url : '15admparamifpymemasivacarga.data.jsp',
				params : {
					informacion: 'GenerarAcuse',
					pkcs7:pkcs7,
					texto_plano:texto_plano,
					numeroProceso:numeroProceso
				}
				,callback: procesarSuccessAcuse
			});	
		}
		
		
	}
	var procesarAceptar = function() 	{
		var  texto_plano ='El Proceso Autorizar� la cuenta Bancaria de la PYME �Desea Continuar?';
		var numeroProceso = Ext.getCmp('numeroProceso').getValue();
		
		
		NE.util.obtenerPKCS7(confirmar, texto_plano, numeroProceso );
		
	}
	
	
		// ***********************Mostrar Pre Acuse ********************
	var procesarPreAcuseData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridPreAcuse = Ext.getCmp('gridPreAcuse');	
	
		if (arrRegistros != null) {
			if (!gridPreAcuse.isVisible()) {
				gridPreAcuse.show();
			}						
			//edito el titulo de la columna
			var el = gridPreAcuse.getGridEl();
			var cm = gridPreAcuse.getColumnModel();
			var jsonData = store.reader.jsonData;	
							
			if(store.getTotalCount() > 0) {								
				Ext.getCmp("mensajeError").hide();
				Ext.getCmp("fpBotones").hide();	
				Ext.getCmp("forma").hide();
				Ext.getCmp("gridLayout").hide();	
				Ext.getCmp("contenedorGrids").hide();										
				el.unmask();					
			} else {	
				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaPreAcuseData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15admparamifpymemasivacarga.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreAcuse'
		},		
		fields: [			
			{name: 'NOMBRE_EPO'},	
			{name: 'NOMBRE_PYME'},	
			{name: 'MONEDA'},	
			{name: 'MOVIMIENTO'},	
			{name: 'DOMICILIO'},	
			{name: 'ALTA_HACIENDA'},	
			{name: 'CEDULA_RFC'},	
			{name: 'IDENTIFICACION'},	
			{name: 'ESTADO_CUENTA'},	
			{name: 'CONVENIO_PYME_NAFIN'},	
			{name: 'OTROS_DOCTOS_FACTORAJE'},	
			{name: 'CONTRATO_IF'},	
			{name: 'NOTAS'},	
			{name: 'RECHAZO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarPreAcuseData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarPreAcuseData(null, null, null);					
				}
			}
		}		
	});
	
	var gridPreAcuse = new Ext.grid.GridPanel({
		store: consultaPreAcuseData,
		id: 'gridPreAcuse',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Pre Acuse Liberaci�n Masiva',
		columns: [	
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'MONEDA',
				tooltip: 'MONEDA',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'MOVIMIENTO',
				tooltip: 'MOVIMIENTO',
				dataIndex: 'MOVIMIENTO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'COMPROBANTE DOMICILIO',
				tooltip: 'COMPROBANTE DOMICILIO',
				dataIndex: 'DOMICILIO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'COMPROBANTE ALTA HACIENDA',
				tooltip: 'COMPROBANTE ALTA HACIENDA',
				dataIndex: 'ALTA_HACIENDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'CEDULA_RFC',
				tooltip: 'CEDULA_RFC',
				dataIndex: 'CEDULA_RFC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'IDENTIFICACI�N OFICIAL VIGENTE',
				tooltip: 'IDENTIFICACI�N OFICIAL VIGENTE',
				dataIndex: 'IDENTIFICACION',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'ESTADO CUENTA CHEQUES',
				tooltip: 'ESTADO CUENTA CHEQUES',
				dataIndex: 'ESTADO_CUENTA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'CONVENIO PYME NAFIN',
				tooltip: 'CONVENIO PYME NAFIN',
				dataIndex: 'CONVENIO_PYME_NAFIN',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'OTROS DOCTOS FACTORAJE',
				tooltip: 'OTROS DOCTOS FACTORAJE',
				dataIndex: 'OTROS_DOCTOS_FACTORAJE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'CONTRATO IF',
				tooltip: 'CONTRATO IF',
				dataIndex:'CONTRATO_IF',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'NOTAS',
				tooltip: 'NOTAS',
				dataIndex:'NOTAS',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAUSA DE RECHAZO',
				tooltip: 'CAUSA DE RECHAZO',
				dataIndex:'RECHAZO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {		
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Aceptar',
					tooltip:	'Aceptar',
					iconCls: 'icoAceptar',
					id: 'btnAceptar',
					handler:procesarAceptar
				},
				'-',
				{
					xtype: 'button',
					text: 'Regresar',
					tooltip:	'Regresar',
					iconCls: 'icoLimpiar',
					id: 'btnCancelarPreA',
					handler: function() {
						window.location = '15admparamifpymemasivacargaext.jsp';
					}
				}	
			]
		}
	});
	
	
	// ***********************Mostrar los Grid de Registros Validos e Invalidos********************
	
	// para generar el archivo de errores en formato xsv
	function procesarGenerarArchivoErrores(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	// registros Validos
	var registrosValidosData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15admparamifpymemasivacarga.data.jsp',
		baseParams: {
			informacion: 'consultaValidos'
		},
		fields: [
			{name: 'NO_LINEA'},
			{name: 'PYME'},
			{name: 'IC_EPO'},
			{name: 'MONEDAS'}	
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {					
				exception: {
					fn: function(proxy,type,action,optionRequest,response,args){
						NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);						
					}
				}
			}
	});
		
	var gridValidos = {
		xtype: 'grid',
		store: registrosValidosData,
		id: 'gridValidos',
		columns: [
			{
				header: 'N�mero de L�nea',
				tooltip: 'N�mero de L�nea',
				dataIndex: 'NO_LINEA',
				sortable: false,
				width: 100,				
				align: 'center'
			},
			{
				header: 'Pyme',
				tooltip: 'Pyme',
				dataIndex: 'PYME',
				sortable: false,				
				width: 120,
				align: 'center'
			},
			{				
				header : 'IC_EPO',
				tooltip: 'IC_EPO',
				dataIndex : 'IC_EPO',
				width: 80,
				align: 'left',
				sortable : false,
				align: 'center'
				
			},
			{				
				header : 'Monedas',
				tooltip: 'Monedas',
				dataIndex : 'MONEDAS',
				width: 80,
				align: 'left',
				sortable : false,
				align: 'center'
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 260,
		width: 400,
		title: 'Registros sin Errores',
		frame: true		
	};
	
	// registros In Validos
	var registrosInValidosData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15admparamifpymemasivacarga.data.jsp',
		baseParams: {
			informacion: 'consultaInValidos'
		},
		fields: [
			{name: 'NO_LINEA'},
			{name: 'ERROR'}			
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {					
				exception: {
					fn: function(proxy,type,action,optionRequest,response,args){
						NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);						
					}
				}
			}
	});
		
	var gridInvalidos = {
		xtype: 'grid',
		store: registrosInValidosData,
		id: 'gridInvalidos',
		columns: [
			{
				header: 'N�mero de L�nea',
				tooltip: 'N�mero de L�nea',
				dataIndex: 'NO_LINEA',
				sortable: false,
				width: 100,				
				align: 'center'
			},
			{
				header: 'Error',
				tooltip: 'Error',
				dataIndex: 'ERROR',
				sortable: false,				
				width: 280,
				align: 'center',
				renderer:	errorRenderer
			}		
		],
		stripeRows: true,
		loadMask: true,
		height: 260,
		width: 400,
		title: 'Registros con Errores',
		frame: true		
	};
		
	
	var contenedorGrids = {
		xtype: 'panel',
		id: 'contenedorGrids',
		hidden: true,
		title: 'Resultado del Proceso de Carga',		
		width: 850,
		frame: true,
		margins: '20 0 0 0',
		layout: 'hbox',
		layoutConfig: {
			pack: 'center',
			align: 'center'
		},
		style: 'margin:0 auto;',
		items: [
			 gridValidos,
			gridInvalidos
		],
		buttons: [	
			{
				xtype: 'button',
				text: 'Generar Archivo de Registros con Errores',					
				tooltip:	'Generar Archivo de Registros con Errores',
				iconCls: 'icoAceptar',
				hidden: true,
				id: 'btnArchiConError',
				handler: function(boton, evento) {
					var numeroProceso = Ext.getCmp('numeroProceso').getValue();
					Ext.Ajax.request({
						url: '15admparamifpymemasivacarga.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoErrores',
							numeroProceso:numeroProceso														
						})
						,callback: procesarGenerarArchivoErrores
					});
				}				
			},
			{
				xtype: 'button',
				text: 'Continuar Carga',					
				tooltip:	'Continuar Carga',
				iconCls: 'icoAceptar',
				id: 'btnContinuarC',
				hidden: true,
				handler: function(boton, evento) {
					var numeroProceso = Ext.getCmp('numeroProceso').getValue();
					
					consultaPreAcuseData.load({
						params:{
							informacion:'ConsultarPreAcuse',
							numeroProceso:numeroProceso
						}
					});	
				}				
			},
			{
				xtype: 'button',
				text: 'Cancelar',
				tooltip:	'Cancelar',
				iconCls: 'icoLimpiar',
				id: 'btnCancelar',
				handler: function() {
					window.location = '15admparamifpymemasivacargaext.jsp';
				}
			}	
		]
	};
	
	// ***********************Forma Principal********************
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'150',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{
				xtype: 'button',
				text: 'Regresar',			
				id: 'btnRegresar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15admparamifpymemasivacargaext.jsp';
				}
			}	
		]
	});
	
	var mensajeError = new Ext.Container({
		layout: 'table',		
		id: 'mensajeError',							
		width:	'750',
		heigth:	'auto',
		align: 'left',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
		{ 
			xtype: 'displayfield', 
			align: 'left',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			id: 'mensaje', 	
			value: '' }				
		]
	});
	
	//STORES////////////////////////////////////////////////////////////////////////
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [			
			{name: 'NUMCAMPO'},
			{name: 'NOMBRE_CAMPO'},
			{name: 'TIPODATO'},
			{name: 'LONGITUD'},
			{name: 'OBLIGATORIO'},
			{name: 'EJEMPLO'}			
	  ]
	});	
 
 	//HANDLERS PARA OBJETOS BUTTON/////////////////////////////////////////////////
	var infoLayout = [
		['1','EPO(IC_EPO)', 'Num�rico','8','Si','157'],
		['2','PYME (RFC)','Num�rico','15','Si', 'NET-010101-EEE' ],		
		['3','Tipo Moneda','Num�rico','2','Si', '1= pesos 54= D�lares'],		
		['4','Movimiento','Alfanum�rico','1','Si', 'A= Autorizaci�n R= Rechazo'],
	
		['5','COMPROBANTE_DOMICILIO_FISCAL','Alfanum�rico','1','Si', 'S'],
		['6','COMPROBANTE_ALTA_HACIENDA','Alfanum�rico','1','Si', 'N'],
		['7','CEDULA_RFC','Alfanum�rico','1','Si', 'N'],
		['8','IDENTIFICACION_OFICIAL_VIGENTE','Alfanum�rico','1','Si', 'N'],
		['9','ESTADO_CUENTA_CHEQUES','Alfanum�rico','1','Si', 'N'],
		['10','CONVENIO_PYME_NAFIN','Alfanum�rico','1','Si', 'N'],
		['11','OTROS_DOCTOS_FACTORAJE','Alfanum�rico','1','Si', 'N'],
		['12','CONTRATO_IF','Alfanum�rico','1','Si', 'N' ],
		['13','NOTAS','Alfanum�rico','1000','No', 'El comprobante de domicilio es incorrecto']		
	];

   storeLayoutData.loadData(infoLayout); 	  
 
	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title:'Se deber� cargar un archivo de texto (.txt) con las columnas separada por pipes "|".',
		columns: [
			{
				header : 'No. de Campo',
				dataIndex : 'NUMCAMPO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Nombre del Campo',
				tooltip: 'Nombre del Campo',
				dataIndex : 'NOMBRE_CAMPO',
				width : 200,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Tipo de Dato',
				dataIndex : 'TIPODATO',
				width : 100,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Longitud',
				dataIndex : 'LONGITUD',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Ejemplo de dato esperado',
				dataIndex : 'EJEMPLO',
				width : 230,
				sortable : true,
				align: 'center'
			}			
		],		
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 850,
		height: 400,
		frame: true,
		bbar: {		
			items: [
				'<b>Nota: A partir del No. de Campo 5 al 13  es para CAUSAS DE RECHAZO DE UN PROVEEDOR </b>'
			]
		}
	});
	
	var myValidFn = function(v) {
			var myRegex = /^.+\.([tT][xX][tT])$/;
			return myRegex.test(v);
		}
		Ext.apply(Ext.form.VTypes, {
			archivotxt 		: myValidFn,
			archivotxtText 	: 'Es necesario ingresar un archivo con formato .txt'
		});
		
	var elementosForma = [	
		{	xtype: 'textfield',	hidden: true,  id: 'numeroProceso' },
		{	xtype: 'textfield',	hidden: true, id: 'noErrores' },
		{	xtype: 'textfield',	hidden: true, id: 'regValidos' },		
		{	xtype: 'textfield',	hidden: true, id: 'fechaCarga' },
		{	xtype: 'textfield',	hidden: true, id: 'horaCarga' },
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){						
						if (!gridLayout.isVisible()) {
							gridLayout.show();
						}else{
							gridLayout.hide();
						}					
					}
				},
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 150,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo',
							combineErrors: false,
							msgTarget: 'side',
							width: 400,
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									emptyText: 'Nombre del Archivo',
									fieldLabel: 'Nombre del Archivo',
									name: 'archivoFormalizacion',   
									buttonText: 'Examinar',
									width: 320,	 									
									vtype: 'archivotxt'
								}						
							]
						}
					]
				}	
			]
		}	
	];	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Liberaci�n Masiva',
		frame: true,
		fileUpload: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 50,
		defaultType: 'textfield',
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Continuar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton, evento) {	
					
					var archivo = Ext.getCmp("archivo");
					if (Ext.isEmpty(archivo.getValue()) ) {
						archivo.markInvalid('El valor de la Ruta es requerida.');	
						return;		
					}else {
						var forma = fp.getForm();									
						forma.submit({
							url: '15admparamifpymemasivacargafile.jsp',
							waitMsg: 'Enviando datos...',
							waitTitle :'Por favor, espere',		
							success: function(form, action) {
								var resp = action.result;
								var numeroProceso =resp.numeroProceso;
								var error_tam = resp.error_tam;	
								var noErrores = resp.noErrores;	
								var regValidos = resp.regValidos;
								Ext.getCmp('numeroProceso').setValue(numeroProceso);
								Ext.getCmp('noErrores').setValue(noErrores);
																					
								if(noErrores!='0') {
									Ext.getCmp('btnArchiConError').show();									
								}else {
									Ext.getCmp('btnArchiConError').hide();	
								}
								
								if(regValidos!='0') {
									Ext.getCmp('btnContinuarC').show();	
								}else {
									Ext.getCmp('btnContinuarC').hide();
								}
								
								if(numeroProceso=='') {
									Ext.getCmp("mensaje").setValue(error_tam);	
									Ext.getCmp("mensajeError").show();
									Ext.getCmp("fpBotones").show();	
									Ext.getCmp("forma").hide();
									Ext.getCmp("gridLayout").hide();	
									Ext.getCmp("contenedorGrids").hide();										
								}else {
									Ext.getCmp("gridLayout").hide();		
									Ext.getCmp("contenedorGrids").show();	
									registrosValidosData.load({
										params:{
											informacion: 'consultaValidos',
											numeroProceso:numeroProceso
										}
									});	
									registrosInValidosData.load({
										params:{
											informacion:'consultaInValidos',
											numeroProceso:numeroProceso
										}
									});									
								}				
							},
							failure: NE.util.mostrarSubmitError
						});
					}			
				}
			},
			{
				text: 'Regresar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '/nafin/15cadenas/15mantenimiento/15parametrizacion/15if/15admparamifpymemasivaext.jsp?origenPantalla=PKI';
				}
			}
		]
	});



	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,		
		items: [	
			NE.util.getEspaciador(20),
			mensajeAutorizacion,
			NE.util.getEspaciador(20),
			fp,
			gridCifrasControl,			
			NE.util.getEspaciador(20),
			contenedorGrids,	
			gridPreAcuse,
			gridLayout,
			mensajeError,
			fpBotones,	
			gridAcuse,
			NE.util.getEspaciador(20)
		]
	});
	
});
