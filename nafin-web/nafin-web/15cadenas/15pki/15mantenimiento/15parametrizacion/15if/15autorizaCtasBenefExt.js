Ext.onReady(function() {

	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//-------------------- HANDLERS --------------------
	//Regresa la pantalla a su condici�n inicial
	function limpiar() {
		//Limpio los campos
		Ext.getCmp('num_ne_pyme').setValue('');
		Ext.getCmp('txtNombre').setValue('');
		Ext.getCmp('ic_epo').setValue('');
		Ext.getCmp('ic_benef').setValue('');
		Ext.getCmp('fecha_alta_ini').setValue('');
		Ext.getCmp('fecha_alta_fin').setValue('');
		Ext.getCmp('cambio_cuenta').setValue('');
		Ext.getCmp('num_recibo').setValue('');
		Ext.getCmp('num_acuse').setValue('');
		Ext.getCmp('fecha_autoriza').setValue('');
		Ext.getCmp('usuario_autoriza').setValue('');
		Ext.getCmp('consecutivo').setValue('');
		//Oculto los componentes
		Ext.getCmp('gridAcuseDetalle').hide();
		Ext.getCmp('gridAcuse').hide();
		Ext.getCmp('fpAcuse').hide();
		Ext.getCmp('gridConsulta').hide();
		Ext.getCmp('fpBusqueda').show();
		//Refresco el catalogo de beneficiario
		catalogo_benef.load();
	}


	var confirma = function(pkcs7, textoFirmar, cuentasAutorizadas ){
	
		
		if (Ext.isEmpty(pkcs7)) {
			Ext.MessageBox.alert('Error', 'Error en la firma. Termina el proceso');
			return; //Error en la firma. Termina el proceso...
		} else {
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15autorizaCtasBenefExt.data.jsp',
				params : {
					informacion:  'AUTORIZAR',
					pkcs7:        pkcs7,
					textoFirmado: textoFirmar,
					autorizados:  cuentasAutorizadas
				},
				callback: procesoConcluido
			});
		}
	
	}
	//Antes de actualizar la autorizaci�n, se tiene que obtener el folio del certificado digital
	function procesaFirma(cuentasAutorizadas) {
		var textoFirmar = 'Nombre del Beneficiario|No. de cuenta|Moneda|Proveedor asociado|No. Nafin electronico|Banco de servicio|EPO|Plaza No. cuenta CLABE|No. cuenta Spid| SWIFT|ABA|Cambio de cuenta|\n';
		var store = Ext.getCmp('gridConsulta').getStore();
		store.each(function(record) {
			if(record.data['CG_AUTORIZA_IF']===true){
				textoFirmar +=
					record.data['NOMBRE_BENEFICIARIO']  +'|'+
					record.data['CG_NUMERO_CUENTA']     +'|'+
					record.data['NOMBRE_MONEDA']        +'|'+
					record.data['NOMBRE_PYME']          +'|'+
					record.data['IC_NAFIN_ELECTRONICO'] +'|'+
					record.data['BANCO_SERVICIO']       +'|'+
					record.data['NOMBRE_EPO']           +'|'+
					record.data['CG_PLAZA']             +'|'+
					record.data['CG_CUENTA_CLABE']      +'|'+
					record.data['CG_CUENTA_SPID']       +'|'+
					record.data['CG_SWIFT']             +'|'+
					record.data['CG_ABA']               +'|'+
					record.data['CG_CAMBIO_CUENTA']     +'|\n';
			}
		});
		
				
		NE.util.obtenerPKCS7(confirma, textoFirmar,   cuentasAutorizadas  );
		
	}

	//Funci�n para descargar archivos
	function descargaArchivo(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
			Ext.getCmp('imprimirPDF').setIconClass('icoPdf');
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fpBusqueda.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpBusqueda.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//Realiza la consulta para el grid
	function consultaGrid() {
		pnl.el.mask('Enviando...', 'x-mask-loading');
		Ext.getCmp('gridConsulta').hide();
		consultaData.load({
			params: Ext.apply({
				informacion:    'CONSULTAR_GRID',
				operacion:      'GENERAR',
				start:          0,
				limit:          15,
				consecutivo:    Ext.getCmp('consecutivo').getValue(),
				num_ne_pyme:    Ext.getCmp('num_ne_pyme').getValue(),
				ic_epo:         Ext.getCmp('ic_epo').getValue(),
				ic_benef:       Ext.getCmp('ic_benef').getValue(),
				fecha_alta_ini: Ext.getCmp('fecha_alta_ini').getValue(),
				fecha_alta_fin: Ext.getCmp('fecha_alta_fin').getValue(),
				cambio_cuenta:  Ext.getCmp('cambio_cuenta').getValue()
			})
		});
	}

	// Proceso de autorizaci�n concluido
	var procesoConcluido = function(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			if(jsonObj.mensajeErr==='') {
				//Guardo los datos del acuse para cuando quiera generar el pdf
				Ext.getCmp('pnl_recibo').setValue('<b>La autentificaci�n se llev� a cabo con �xito. Recibo: ' + jsonObj.recibo + '<\/b>');
				Ext.getCmp('num_recibo').setValue(jsonObj.recibo);
				Ext.getCmp('num_acuse').setValue(jsonObj.acuse);
				Ext.getCmp('fecha_autoriza').setValue(jsonObj.fechaAutorizacion);
				Ext.getCmp('usuario_autoriza').setValue(jsonObj.usuario);
				//Proceso el grid de acuse
				var acuseCifras = [
					['N�mero de Acuse', jsonObj.acuse],
					['Fecha y Hora', jsonObj.fechaAutorizacion],
					['Usuario', jsonObj.usuario]
				];
				consultaAcuse.loadData(acuseCifras);
				consultaAcuseDetalle.load({
					params: Ext.apply({
						informacion: 'CONSULTAR_ACUSE_DETALLE',
						acuse:       jsonObj.acuse
					})
				});
			} else {
				pnl.el.unmask();
				Ext.MessageBox.alert('Mensaje de la p�gina web',jsonObj.mensajeErr);
			}
		} else {
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	};

	//Proceso para la b�squeda del campo N�m. NE y PyME.
	var successAjaxFn = function(opts, success, response) {
		pnl.el.unmask();
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			if(jsonObj.txtNombre===''){
				Ext.MessageBox.alert('Mensaje de la p�gina web','N�mero de Nafin Electr�nico incorrecto.');
				Ext.getCmp('txtNombre').setValue('');
				Ext.getCmp('num_ne_pyme').setValue('');
			} else {
				Ext.getCmp('txtNombre').setValue(jsonObj.txtNombre);
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

	//Proceso posterior a la consulta del grid de acuse detalle de las cuentas agregadas.
	var procesarConsultaAcuseDetalle = function(store, arrRegistros, opts) {
		pnl.el.unmask();
		var el = Ext.getCmp('gridAcuseDetalle').getGridEl();
		if (arrRegistros !== null) {
			if(store.getTotalCount() > 0) {
				//Proceso los grids de acuse
				Ext.getCmp('fpAcuse').show();
				Ext.getCmp('gridAcuse').show();
				Ext.getCmp('gridAcuseDetalle').show();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('fpBusqueda').hide();
				Ext.getCmp('imprimirPDF').enable();
				el.unmask();
			} else {
				Ext.getCmp('fpAcuse').hide();
				Ext.getCmp('gridAcuse').hide();
				Ext.getCmp('gridAcuseDetalle').hide();
			}
		}
	};

	//Proceso posterior a la consulta del grid.
	var procesarConsultaData = function(store, arrRegistros, opts) {
		pnl.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();
		if (arrRegistros !== null) {
			if(store.getTotalCount() > 0) {
				gridConsulta.show();
				el.unmask();
				Ext.getCmp('btnGenerarCSV').enable();
				Ext.getCmp('btnAutorizar').enable();
			} else {
				gridConsulta.show();
				Ext.getCmp('btnGenerarCSV').disable();
				Ext.getCmp('btnAutorizar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

	//Proceso posterior a la consulta del grid de notificaci�n de cuentas modificadas pendientes de autorizar.
	var procesarConsultaNotifcacion = function(store, arrRegistros, opts) {
		pnl.el.unmask();
		var gridNot = Ext.getCmp('gridNotificacion');
		var el = gridNot.getGridEl();
		if (arrRegistros !== null) {
			if(store.getTotalCount() > 0) {
				Ext.getCmp('winNotificacion').show();
				gridNot.show();
				el.unmask();
			} else {
				gridNot.hide();
				Ext.getCmp('winNotificacion').hide();
				Ext.getCmp('fpBusqueda').show();
			}
		}
	};

	//Procesamiento del combo: Nombre de la EPO.
	var procesa_catalogo_epo = function(store, arrRegistros, opts) {
		Ext.getCmp('ic_epo').setValue('');
	};

	//Procesamiento del combo: Beneficiario.
	var procesa_catalogo_benef = function(store, arrRegistros, opts) {
		if(Ext.getCmp('ic_benef').getValue()!=='') {
			Ext.getCmp('ic_benef').setValue(Ext.getCmp('ic_benef').getValue());
		}
	};

//-------------------- STORES --------------------
	//Store del grid de acuse detalle.
	var consultaAcuseDetalle = new Ext.data.JsonStore({
		root:            'registros',
		id:              'consultaAcuseDetalle',
		url:             '15autorizaCtasBenefExt.data.jsp',
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		baseParams:      {
			informacion: 'CONSULTAR_ACUSE_DETALLE'
		},
		fields: [
			{name: 'NOMBRE_EPO'              },
			{name: 'NOMBRE_PYME'             },
			{name: 'IC_NAFIN_ELECTRONICO'    },
			{name: 'BANCO_SERVICIO'          },
			{name: 'NOMBRE_BENEFICIARIO'     },
			{name: 'NOMBRE_MONEDA'           },
			{name: 'CG_NUMERO_CUENTA'        },
			{name: 'CG_CUENTA_CLABE'         },
			{name: 'CG_CUENTA_SPID'          },
			{name: 'CG_SWIFT'                },
			{name: 'CG_ABA'                  },
			{name: 'CG_PLAZA'                },
			{name: 'CG_CAMBIO_CUENTA'        },
			{name: 'IC_CONSECUTIVO'          }
		],
		listeners: {
			load: procesarConsultaAcuseDetalle,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaAcuseDetalle(null, null, null);
				}
			}
		}
	});
	
	//Store del grid de acuse
	var consultaAcuse = new Ext.data.ArrayStore({
		fields: [
			{name: 'etiqueta'},
			{name: 'informacion'}
		]
	});

	//Store del grid de consulta.
	var consultaData = new Ext.data.JsonStore({
		root:            'registros',
		id:              'consultaData',
		url:             '15autorizaCtasBenefExt.data.jsp',
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		baseParams:      {
			informacion: 'CONSULTAR_GRID'
		},
		fields: [
			{name: 'NOMBRE_EPO'              },
			{name: 'NOMBRE_PYME'             },
			{name: 'IC_NAFIN_ELECTRONICO'    },
			{name: 'BANCO_SERVICIO'          },
			{name: 'NOMBRE_BENEFICIARIO'     },
			{name: 'NOMBRE_MONEDA'           },
			{name: 'CG_NUMERO_CUENTA'        },
			{name: 'CG_CUENTA_CLABE'         },
			{name: 'CG_CUENTA_SPID'          },
			{name: 'CG_SWIFT'                },
			{name: 'CG_ABA'                  },
			{name: 'CG_PLAZA'                },
			{name: 'CG_AUTORIZA_IF'          },
			{name: 'CG_CAMBIO_CUENTA'        },
			{name: 'COLOR'                   },
			{name: 'IC_CONSECUTIVO'          },
			{name: 'IC_EPO'                  },
			{name: 'IC_PYME'                 },
			{name: 'IC_BENEFICIARIO'         },
			{name: 'IC_MONEDA'               },
			{name: 'IC_BANCOS_TEF'           },
			{name: 'IC_PLAZA'                },
			{name: 'DF_FECHA_ALTA'           }
		],
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	//Store del grid de notificaci�n de cuentas modificadas pendientes de autorizar.
	var consultaNotifcacion = new Ext.data.JsonStore({
		root:            'registros',
		id:              'consultaNotifcacion',
		url:             '15autorizaCtasBenefExt.data.jsp',
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		baseParams:      {
			informacion: 'CONSULTA_CAMBIOS_CUENTAS'
		},
		fields: [
			{name: 'IC_CONSECUTIVO'       },
			{name: 'IC_NAFIN_ELECTRONICO' },
			{name: 'IC_EPO'               },
			{name: 'IC_BENEFICIARIO'      },
			{name: 'CG_CAMBIO_CUENTA'     },
			{name: 'CG_RFC'               },
			{name: 'NOMBRE_PYME'          },
			{name: 'NOMBRE_EPO'           },
			{name: 'NOMBRE_BENEFICIARIO'  },
			{name: 'NOMBRE_MONEDA'        }
		],
		listeners: {
			load: procesarConsultaNotifcacion,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaNotifcacion(null, null, null);
				}
			}
		}
	});

	//Store del form: Busqueda Avanzada.
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id:              'storeBusqAvanzPyme',
		root:            'registros',
		url:             '15autorizaCtasBenefExt.data.jsp',
		fields:          ['clave', 'descripcion','loadMsg'],
		baseParams:      {
			informacion: 'BUSQUEDA_AVANZADA'
		},
		totalProperty:   'total',
		autoLoad:        false,
		listeners:       {
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	//Store del combo: Nombre de la EPO.
	var catalogo_epo = new Ext.data.JsonStore({
		id:              'catalogo_epo',
		root:            'registros',
		totalProperty:   'total',
		url:             '15autorizaCtasBenefExt.data.jsp',
		fields:          ['clave', 'descripcion', 'loadMsg'],
		baseParams:      {
			informacion: 'CATALOGO_EPO'
		},
		autoLoad:        true,
		listeners:       {
			load:        procesa_catalogo_epo,
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	//Store del combo: Beneficiario.
	var catalogo_benef = new Ext.data.JsonStore({
		id:              'catalogo_benef',
		root:            'registros',
		totalProperty:   'total',
		url:             '15autorizaCtasBenefExt.data.jsp',
		fields:          ['clave', 'descripcion', 'loadMsg'],
		baseParams:      {
			informacion: 'CATALOGO_BENEFICIARIO'
		},
		autoLoad:        true,
		listeners:       {
			load:        procesa_catalogo_benef,
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

//-------------------- FORMS --------------------
	//Grid de consulta del grid de acuse detalle de las cuentas agregadas.
	var gridAcuseDetalle = new Ext.grid.GridPanel({
		id:                  'gridAcuseDetalle',
		style:               'margin: 0 auto; padding-top:10px;',
		emptyMsg:            'No hay registros.',
		margins:             '20 0 0 0',
		stripeRows:          true,
		loadMask:            true,
		frame:               true,
		hidden:              true,
		displayInfo:         true,
		height:              200,
		width:               930,
		store:               consultaAcuseDetalle,
		title:               'Acuse de recibo de autorizaci�n de cuentas bancarias Proveedor',
		columns:             [{
			header:          'Nombre del Beneficiario',
			dataIndex:       'NOMBRE_BENEFICIARIO',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           200
		},{
			header:          'No. de cuenta',
			dataIndex:       'CG_NUMERO_CUENTA',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           100
		},{
			header:          'Moneda',
			dataIndex:       'NOMBRE_MONEDA',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           150
		},{
			header:          'Proveedor asociado',
			dataIndex:       'NOMBRE_PYME',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           200
		},{
			header:          'N�m. Nafin Electr�nico',
			dataIndex:       'IC_NAFIN_ELECTRONICO',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           150
		},{
			header:          'Banco de servicio',
			dataIndex:       'BANCO_SERVICIO',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           200
		},{
			header:          'EPO',
			dataIndex:       'NOMBRE_EPO',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           200
		},{
			header:          'Plaza',
			dataIndex:       'CG_PLAZA',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           200
		},{
			header:          'No. cuenta CLABE',
			dataIndex:       'CG_CUENTA_CLABE',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           200
		},{
			header:          'No. cuenta Spid',
			dataIndex:       'CG_CUENTA_SPID',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           200
		},{
			header:          'SWIFT',
			dataIndex:       'CG_SWIFT',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           200
		},{
			header:          'ABA',
			dataIndex:       'CG_ABA',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           200
		},{
			header:          'Cambio de cuenta',
			dataIndex:       'CG_CAMBIO_CUENTA',
			sortable:        true,
			resizable:       true,
			menuDisabled:    true,
			width:           120
		}],
		bbar: {
			items: [
				'->','-',
				{
					xtype:   'button',
					text:    'Imprimir',
					iconCls: 'icoPdf',
					id:      'imprimirPDF',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15autorizaCtasBenefExt.data.jsp',
							params: Ext.apply({
								informacion: 'GENERAR_PDF',
								acuse:       Ext.getCmp('num_acuse').getValue(),
								fecha:       Ext.getCmp('fecha_autoriza').getValue(),
								usuario:     Ext.getCmp('usuario_autoriza').getValue(),
								num_recibo:  Ext.getCmp('num_recibo').getValue()
							}),
							callback: descargaArchivo
						});
					}
				},'-',
				{
					xtype:   'button',
					text:    'Regresar',
					iconCls: 'icoRegresar',
					id:      'regresar',
					handler: function(boton, evento) {
						limpiar();
					}
				}
			]
		}
	});

	//Grid que muestra el acuse de las cuentas agregadas.
	var gridAcuse = {
		xtype:         'grid',
		id:            'gridAcuse',
		title:         'Acuse',
		style:         'margin: 0 auto; padding-top:10px;',
		margins:       '20 0 0 0',
		hidden:        true,
		stripeRows:    true,
		loadMask:      true,
		frame:         false,
		height:        130,
		width:         300,
		store:         consultaAcuse,
		columns: [{
			dataIndex: 'etiqueta',
			align:     'left',
			width:     100
		},{
			dataIndex: 'informacion',
			align:     'left',
			width:     195
		}]
	};

	//Grid de consulta
	var gridConsulta = new Ext.grid.GridPanel({
		id:               'gridConsulta',
		style:            'margin: 0 auto; padding-top:10px;',
		emptyMsg:         'No hay registros.',
		margins:          '20 0 0 0',
		stripeRows:       true,
		loadMask:         true,
		frame:            true,
		hidden:           true,
		displayInfo:      true,
		height:           400,
		width:            930,
		store:            consultaData,
		title:            undefined,
		columns: [{
			header:       'Nombre del Beneficiario',
			dataIndex:    'NOMBRE_BENEFICIARIO',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'No. de cuenta',
			dataIndex:    'CG_NUMERO_CUENTA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        100,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Moneda',
			dataIndex:    'NOMBRE_MONEDA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        150,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Proveedor asociado',
			dataIndex:    'NOMBRE_PYME',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'N�m. Nafin Electr�nico',
			dataIndex:    'IC_NAFIN_ELECTRONICO',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        150,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Banco de servicio',
			dataIndex:    'BANCO_SERVICIO',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'EPO',
			dataIndex:    'NOMBRE_EPO',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Plaza',
			dataIndex:    'CG_PLAZA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'No. cuenta CLABE',
			dataIndex:    'CG_CUENTA_CLABE',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'No. cuenta Spid',
			dataIndex:    'CG_CUENTA_SPID',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'SWIFT',
			dataIndex:    'CG_SWIFT',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'ABA',
			dataIndex:    'CG_ABA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        200,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			width:       120,
			xtype:       'checkcolumn',
			header:      'Autorizaci�n IF',
			dataIndex:   'CG_AUTORIZA_IF',
			align:       'center',
			sortable:    false,
			resizable:   false,
			listeners:   {
				'checkchange': function(grid, rowindex, isChecked){
				}
			}
		},{
			header:       'Cambio de cuenta',
			dataIndex:    'CG_CAMBIO_CUENTA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        120,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		},{
			header:       'Fecha de alta',
			dataIndex:    'DF_FECHA_ALTA',
			sortable:     true,
			resizable:    true,
			menuDisabled: true,
			width:        100,
			renderer:     function(value, metadata, registro) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				var color = registro.data['COLOR'];	
				if(color !=='') {
					return '<span style="color:red;">' + value + '<\/span>';
				}else {
					return  value;
				}
			}
		}],
		bbar: {
			xtype:           'paging',
			buttonAlign:     'left',
			id:              'barraPaginacion',
			displayMsg:      '{0} - {1} de {2}',
			emptyMsg:        'No hay registros.',
			displayInfo:     true,
			store:           consultaData,
			pageSize:        15,
			items: [
				'->','-',
				{
					xtype:   'button',
					text:    'Generar Archivo',
					iconCls: 'icoXls',
					id:      'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15autorizaCtasBenefExt.data.jsp',
							params: Ext.apply({
								informacion:    'GENERAR_CSV',
								num_ne_pyme:    Ext.getCmp('num_ne_pyme').getValue(),
								ic_epo:         Ext.getCmp('ic_epo').getValue(),
								ic_benef:       Ext.getCmp('ic_benef').getValue(),
								fecha_alta_ini: Ext.getCmp('fecha_alta_ini').getValue(),
								fecha_alta_fin: Ext.getCmp('fecha_alta_fin').getValue(),
								cambio_cuenta:  Ext.getCmp('cambio_cuenta').getValue()
							}),
							callback: descargaArchivo
						});
					}
				},'-',
				{
					xtype:   'button',
					text:    'Autorizar',
					iconCls: 'icoAceptar',
					id:      'btnAutorizar',
					handler: function(boton, evento) {
						var jsonData = consultaData.data.items;
						var autorizados = [];
						var contador = 0;
						Ext.each(jsonData, function(item,index,arrItem) {
							if(item.data.CG_AUTORIZA_IF===true) {
								autorizados.push(item.data.IC_CONSECUTIVO);
								contador++;
							}
						});
						if(contador===0) {
							Ext.MessageBox.alert('Mensaje de la p�gina web','Debe seleccionar al menos un registro.');
							return;
						} else {
							procesaFirma(autorizados);
						}
					}
				}
			]
		}
	});

	//Grid de notificaci�n de cuentas modificadas pendientes de autorizar.
	var gridNotificacion = new Ext.grid.GridPanel({
		id:               'gridNotificacion',
		stripeRows:       true,
		loadMask:         true,
		frame:            false,
		hidden:           false,
		displayInfo:      true,
		height:           400,
		width:            930,
		store:            consultaNotifcacion,
		columns: [{
			header:       'RFC',
			dataIndex:    'CG_RFC',
			resizable:    true,
			menuDisabled: true,
			width:        100
		},{
			header:       'Proveedor',
			dataIndex:    'NOMBRE_PYME',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'EPO',
			dataIndex:    'NOMBRE_EPO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Beneficiario',
			dataIndex:    'NOMBRE_BENEFICIARIO',
			resizable:    true,
			menuDisabled: true,
			width:        200
		},{
			header:       'Moneda',
			dataIndex:    'NOMBRE_MONEDA',
			resizable:    true,
			menuDisabled: true,
			width:        150
		},{
			xtype:        'actioncolumn',
			header:       'Por autorizar',
			align:        'center',
			width:        100,
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
					return 'iconoLupa';
				},
				handler: function(grid, rowIndex, colIndex, item, event) {
					var registro = grid.getStore().getAt(rowIndex);
					Ext.getCmp('num_ne_pyme').setValue(registro.get('IC_NAFIN_ELECTRONICO'));
					Ext.getCmp('ic_epo').setValue(registro.get('IC_EPO'));
					Ext.getCmp('ic_benef').setValue(registro.get('IC_BENEFICIARIO'));
					Ext.getCmp('cambio_cuenta').setValue(registro.get('CG_CAMBIO_CUENTA')==='N'?false:true);
					Ext.getCmp('consecutivo').setValue(registro.get('IC_CONSECUTIVO'));
					Ext.getCmp('winNotificacion').hide();
					Ext.getCmp('fpBusqueda').show();
					Ext.Ajax.request({
						url: '15autorizaCtasBenefExt.data.jsp',
						method: 'POST',
						callback: successAjaxFn,
						params: {
							informacion: 'CONSULTA_NOMBRE_PYME' ,
							num_ne_pyme: Ext.getCmp('num_ne_pyme').getValue()
						}
					});
					consultaGrid();
				}
			}]
		}]
	});

	//Ventana donde se aloja el grid de notificaciones de cuantas con cambios.
	var winNotificacion = new Ext.Window({
		title:       'EXISTEN CAMBIOS DE CUENTAS BANCARIAS BENEFICIARIO PENDIENTES DE AUTORIZACI�N',
		buttonAlign: 'right',
		id:          'winNotificacion',
		style:       'margin:0 auto;',
		layout:      'fit',
		closeAction: 'hide',
		closable:    true,
		modal:       true,
		width:       635,
		height:      205,
		minWidth:    635,
		minHeight:   205,
		y:           100,
		items:       gridNotificacion		
	});

	//Form que muestra el recibo de acuse.
	var fpAcuse = new Ext.form.FormPanel({
		id:            'fpAcuse',
		style:         'margin:0 auto;',
		bodyStyle:     'padding-left:20px;,padding-top:10px;,text-align:left',
		defaults: {
			msgTarget: 'side',
			anchor:    '-20'
		},
		border:        false,
		hidden:        true,
		labelWidth:    60,
		width:         500,
		items: [{
			xtype:     'displayfield',
			id:        'pnl_recibo'
		}]
	});

	//Form de B�squeda Avanzada
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id:                'fpBusqAvanzada',
		bodyStyle:         'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:            'form',
		defaults:          {
			xtype:         'textfield',
			msgTarget:     'side',
			anchor:        '-20'
		},
		frame:             true,
		monitorValid:      true,
		labelWidth:        57,
		items:             [{
			xtype:         'textfield',
			name:          'nombrePyme',
			id:            'nombrePyme1',
			fieldLabel:    'Nombre',
			msgTarget:     'side',
			margins:       '0 20 0 0',
			allowBlank:    true,
			maxLength:     100,
			width:         80
		},{
			xtype:         'textfield',
			name:          'rfcPyme',
			id:            'rfcPyme1',
			fieldLabel:    'RFC',
			msgTarget:     'side',
			margins:       '0 20 0 0',
			allowBlank:    true,
			maxLength:     20,
			width:         80
		},{ 
			xtype:         'label',
			html:          'Utilice el * para realizar una b�squeda gen�rica.',
			cls:           'x-form-item',
			style:         {
				width:     '100%',
				textAlign: 'center'
			} 
		},{
			xtype:         'panel',
			bodyStyle:     'padding: 10px',
			layout:        {
				type:      'hbox',
				pack:      'center',
				align:     'middle'
			},
			items:         [{
				xtype:     'button',
				text:      'Buscar',
				id:        'btnBuscar',
				iconCls:   'icoBuscar',
				width:     75,
				handler:   function(boton, evento) {
					var pymeComboCmp = Ext.getCmp('cbPyme1');
					pymeComboCmp.setValue('');
					pymeComboCmp.setDisabled(false);
					storeBusqAvanzPyme.load({
						params: {
							informacion: 'BUSQUEDA_AVANZADA',
							nombrePyme:  Ext.getCmp('nombrePyme1').getValue(),
							rfcPyme:     Ext.getCmp('rfcPyme1').getValue()
						}
					});
				},
				style: {
					marginBottom: '10px'
				}
			}]
		},{
			xtype:         'combo',
			name:          'cbPyme',
			id:            'cbPyme1',
			mode:          'local',
			displayField:  'descripcion',
			emptyText:     'Seleccione...',
			valueField:    'clave',
			hiddenName:    'cbPyme',
			fieldLabel:    'Nombre',
			triggerAction: 'all',
			typeAhead:     true,
			autoLoad:      false,
			disabled:      true,
			forceSelection:true,
			minChars:      1,
			store:         storeBusqAvanzPyme,
			tpl:           '<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}<\/div>'+
				'<\/tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}<\/div>' +
				'<\/div><\/tpl><\/tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		}],
		buttons:           [{
			text:          'Aceptar',
			iconCls:       'icoAceptar',
			formBind:      true,
			disabled:      true,
			handler:       function(boton, evento) {
				var cmbPyme= Ext.getCmp('cbPyme1');
				var ventana = Ext.getCmp('winBusqAvan');
				if (Ext.isEmpty(cmbPyme.getValue())) {
					cmbPyme.markInvalid('Seleccione Proveedor');
					return;
				}
				var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
				record = record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
				var a = [];
				a = record.split(" ",1);
				var nombre = record.substring(a[0].length,record.length);
				Ext.getCmp('num_ne_pyme').setValue(cmbPyme.getValue());
				Ext.getCmp('txtNombre').setValue(nombre);
				Ext.Ajax.request({
					url: '15autorizaCtasBenefExt.data.jsp',
					method: 'POST',
					callback: successAjaxFn,
					params: {
						informacion: 'CONSULTA_NOMBRE_PYME',
						num_ne_pyme: Ext.getCmp('num_ne_pyme').getValue()
					}
				});
				fpBusqAvanzada.getForm().reset();
				ventana.hide();
			}
		},{
			text:          'Cancelar',
			iconCls:       'icoLimpiar',
			handler:       function() {
				var ventana = Ext.getCmp('winBusqAvan');
				fpBusqAvanzada.getForm().reset();
				ventana.hide();
			}
		}]
	});

	//Form: Criterios de b�squeda.
	var fpBusqueda = new Ext.form.FormPanel({
		id:                       'fpBusqueda',
		title:                    'Criterios de B�squeda',
		style:                    'margin:0 auto;',
		bodyStyle:                'padding-left:20px;,padding-top:10px;,text-align:left',
		defaults: {
			msgTarget:            'side',
			anchor:               '-20'
		},
		collapsible:              false,
		titleCollapse:            false,
		frame:                    true,		
		width:                    700,
		labelWidth:               150,
		items:                    [{
			xtype:                'compositefield',
			fieldLabel:           'N�m. NE Proveedor',
			msgTarget:            'side',
			combineErrors:        false,
			items: [{
				xtype:            'numberfield',
				name:             'num_ne_pyme',
				id:               'num_ne_pyme',
				msgTarget:        'side',
				margins:          '0 20 0 0',
				allowBlank:       true,
				maxLength:        38,
				width:            55,
				regex:            /^[0-9]*$/,
				listeners:        {
					'blur':       function(){
						if(!Ext.getCmp('fpBusqueda').getForm().isValid()){
							return;
						}
						if(!Ext.isEmpty(Ext.getCmp('num_ne_pyme').getValue())){
							Ext.Ajax.request({
								url: '15autorizaCtasBenefExt.data.jsp',
								method: 'POST',
								callback: successAjaxFn,
								params: {
									informacion: 'CONSULTA_NOMBRE_PYME' ,
									num_ne_pyme: Ext.getCmp('num_ne_pyme').getValue()
								}
							});
						}
					}
				}
			},{
				xtype:            'textfield',
				name:             'txtNombre',
				id:               'txtNombre',
				msgTarget:        'side',
				margins:          '0 20 0 0',
				allowBlank:       true,
				disabled:         true,
				maxLength:        100,
				width:            270
			},{
				xtype:            'button',
				text:             'B�squeda Avanzada',
				iconCls:          'icoBuscar',
				id:               'btnBusqAv',
				handler:          function(boton, evento) {
					var pymeComboCmp = Ext.getCmp('cbPyme1');
					pymeComboCmp.setValue('');
					pymeComboCmp.setDisabled(true);
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
							title:          'B�squeda Avanzada',
							buttonAlign:    'center',
							id:             'winBusqAvan',
							closeAction:    'hide',
							layout:         'fit',
							width:          400,
							height:         300,
							minWidth:       400,
							minHeight:      300,
							items:          fpBusqAvanzada
						}).show();
					}
				}
			}]
		},{
			xtype:                'combo',
			id:                   'ic_epo',
			name:                 'ic_epo',
			hiddenName:           'ic_epo',
			fieldLabel:           'Nombre de la EPO',
			emptyText:            'Seleccione...',
			displayField:         'descripcion',
			valueField:           'clave',
			triggerAction:        'all',
			mode:                 'local',
			forceSelection:       true,
			typeAhead:            false,
			allowBlank:           true,
			minChars:             1,
			store:                catalogo_epo,
			tpl:                  NE.util.templateMensajeCargaCombo,
			style: {
				boder:            '0px'
			},
			listeners:            {
				'select': function(cbo){
					if(!Ext.isEmpty(cbo.getValue())) {
						Ext.getCmp('ic_benef').setValue('');
						Ext.getCmp('ic_benef').store.removeAll();
						catalogo_benef.load({
							params: {
								ic_epo: Ext.getCmp('ic_epo').getValue()
							}
						});
					}
				}
			}
		},{
			xtype:                'combo',
			id:                   'ic_benef',
			name:                 'ic_benef',
			hiddenName:           'ic_benef',
			fieldLabel:           'Beneficiario',
			emptyText:            'Seleccione...',
			displayField:         'descripcion',
			valueField:           'clave',
			triggerAction:        'all',
			mode:                 'local',
			forceSelection:       true,
			typeAhead:            false,
			allowBlank:           true,
			minChars:             1,
			store:                catalogo_benef,
			tpl:                  NE.util.templateMensajeCargaCombo,
			style: {
				boder:            '0px'
			}
		},{
			xtype:                'compositefield',
			id:                   'cfFechaAlta',
			fieldLabel:           'Fecha de Alta',
			msgTarget:            'side',
			combineErrors:        false,
			items: [{
				xtype:            'datefield',
				name:             'fecha_alta_ini',
				id:               'fecha_alta_ini',
				msgTarget:        'side',
				vtype:            'rangofecha',
				campoFinFecha:    'fecha_alta_fin',
				margins:          '0 20 0 0',
				allowBlank:       true,
				startDay:         0,
				width:            150
			},{
				xtype:            'displayfield',
				value:            'al',
				width:            25
			},{
				xtype:            'datefield',
				name:             'fecha_alta_fin',
				id:               'fecha_alta_fin',
				msgTarget:        'side',
				vtype:            'rangofecha',
				campoInicioFecha: 'fecha_alta_ini',
				margins:          '0 20 0 0',
				allowBlank:       true,
				startDay:         1,
				width:            150
			}]
		},{
			xtype:                'checkbox',
			fieldLabel:           'Cambio de cuenta',
			name:                 'cambio_cuenta',
			id:                   'cambio_cuenta'
		},{
			xtype:                'textfield',
			name:                 'consecutivo',
			id:                   'consecutivo',
			hidden:               true
		},{
			xtype:                'textfield',
			name:                 'num_recibo',
			id:                   'num_recibo',
			hidden:               true
		},{
			xtype:                'textfield',
			name:                 'num_acuse',
			id:                   'num_acuse',
			hidden:               true
		},{
			xtype:                'textfield',
			name:                 'fecha_autoriza',
			id:                   'fecha_autoriza',
			hidden:               true
		},{
			xtype:                'textfield',
			name:                 'usuario_autoriza',
			id:                   'usuario_autoriza',
			hidden:               true
		}],
		buttons: [{
			width:                100,
			text:                 'Buscar',
			id:                   'btnConsultar',
			iconCls:              'icoBuscar',
			handler:              function(boton, evento) {
				//Valida longitud y tipo de los campos
				if(!Ext.getCmp('fpBusqueda').getForm().isValid()){
					return;
				} else if(  (Ext.getCmp('fecha_alta_ini').getValue()==='' && Ext.getCmp('fecha_alta_fin').getValue()!=='') ||
							(Ext.getCmp('fecha_alta_ini').getValue()!=='' && Ext.getCmp('fecha_alta_fin').getValue()==='')) {
					Ext.MessageBox.alert('Mensaje de la p�gina web','Ingrese Fecha Inicial y/o Ingrese Fecha Final.');
					return;
				} else {
					Ext.getCmp('consecutivo').setValue('');
					consultaGrid();
				}
			}
		},{
			width:                100,
			text:                 'Limpiar',
			id:                   'btnLimpiar',
			iconCls:              'icoLimpiar',
			handler:              function(boton, evento) {
				limpiar();
			}
		}]
	});

//-------------------- CONTENEDOR PRINCIPAL -------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		height: 'auto',
		frame: true,
		width: 940,
		items: [
			NE.util.getEspaciador(10),
			fpBusqueda,
			fpAcuse,
			gridAcuse,
			winNotificacion,
			NE.util.getEspaciador(10),
			gridConsulta,
			gridAcuseDetalle,
			NE.util.getEspaciador(10)
		]

	});

//-------------------- LLAMA A LA VENTANA EMERGENTE --------------------
	consultaNotifcacion.load({
		params: Ext.apply({
			informacion: 'CONSULTA_CAMBIOS_CUENTAS'
		})
	});
});