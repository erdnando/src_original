<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.text.SimpleDateFormat,		
		com.netro.model.catalogos.*,
		com.netro.descuento.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<%@ include file="../../../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
String num_electronico = (request.getParameter("num_electronico") == null)?"":request.getParameter("num_electronico");
String chkPymesConvenio = (request.getParameter("chkPymesConvenio") == null)?"N":request.getParameter("chkPymesConvenio");
String fechaparamCU_ini = (request.getParameter("fechaparamCU_ini") == null)?"":request.getParameter("fechaparamCU_ini");
String fechaparamCU_fin = (request.getParameter("fechaparamCU_fin") == null)?"":request.getParameter("fechaparamCU_fin");
String expediente_digital = (request.getParameter("expediente_digital") == null)?"":request.getParameter("expediente_digital");
String txtNombre = (request.getParameter("txtNombre") == null)?"":request.getParameter("txtNombre");
String cboEPO = (request.getParameter("cboEPO") == null)?"":request.getParameter("cboEPO");
String clave_pyme = (request.getParameter("clave_pyme") == null)?"":request.getParameter("clave_pyme");
if(clave_pyme.equals("0")) {  clave_pyme =""; }

String infoRegresar	=	"", ic_pyme ="", consulta ="", sIfConvenioUnico ="";
int start =0, limit =0;
JSONObject jsonObj = new JSONObject();
List parametros= new ArrayList();

ParametrosDescuento parametrosDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

sIfConvenioUnico =parametrosDescuento.sIfConvenioUnico(iNoCliente);

// al inicar la pantalla se 
if(informacion.equals("valIniciales") ){
	int numeroCuentasPendientes = parametrosDescuento.verifCuentasPendResul(iNoCliente);	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("sIfConvenioUnico", sIfConvenioUnico);	
	jsonObj.put("numeroCuentasPendientes", String.valueOf(numeroCuentasPendientes));		
	infoRegresar = jsonObj.toString();

	
}else if(informacion.equals("pymeNombre") ) {
	
	List datosPymes =  parametrosDescuento.datosPymes(num_electronico, "" ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);
	infoRegresar = jsonObj.toString();		

}else  if (informacion.equals("busquedaAvanzada") ) {

	String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
		
	CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");	
	if(!rfcPyme.equals(""))
	cat.setRfc_pyme(rfcPyme);	
	if(!nombrePyme.equals(""))
	cat.setNombre_pyme(nombrePyme);	
	cat.setNafin_electronico("");
	cat.setPantalla("SoliAfi");
	cat.setOrden("p.cg_razon_social");		
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");	
	cat.setClaveIf(iNoCliente);  
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoPYME")){

	infoRegresar = parametrosDescuento.catalogoPyme(cboEPO, iNoCliente);

}else if(informacion.equals("consultaCuentas")){

	consulta  = parametrosDescuento.cuentasPedientes(iNoCliente);
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();	

}else if(informacion.equals("Consultar") ){
	
	sIfConvenioUnico =parametrosDescuento.sIfConvenioUnico(iNoCliente);
	
	parametros= new ArrayList();
	parametros.add(iNoCliente);
	parametros.add(chkPymesConvenio);
	parametros.add(num_electronico);
	parametros.add(cboEPO);
	parametros.add(clave_pyme);
	parametros.add(fechaparamCU_ini);
	parametros.add(fechaparamCU_fin);
			
	consulta = parametrosDescuento.consAutotizacion(parametros);

	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("sIfConvenioUnico", sIfConvenioUnico);	
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("ArchivoCSV")){
	
	sIfConvenioUnico =parametrosDescuento.sIfConvenioUnico(iNoCliente);
	
	parametros= new ArrayList();
	parametros.add(iNoCliente);
	parametros.add(chkPymesConvenio);
	parametros.add(num_electronico);
	parametros.add(cboEPO);
	parametros.add(clave_pyme);
	parametros.add(fechaparamCU_ini);
	parametros.add(fechaparamCU_fin);
	parametros.add(sIfConvenioUnico);
	parametros.add(strDirectorioTemp);
	
	String nombreArchivo = parametrosDescuento.archivoConsAutotizacion(parametros );
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();		


}else if(informacion.equals("Autorizar")){
		
	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
	String _acuse = "", serial = "", folioCert = "", mensajeAuto="";
	String externContent = (request.getParameter("texto_plano")==null)?"":request.getParameter("texto_plano");
	char getReceipt = 'Y';
	
	
	
	sIfConvenioUnico =parametrosDescuento.sIfConvenioUnico(iNoCliente);
	
	String	chkCtaBanc[]		=	request.getParameterValues("chkCtaBanc");
	String iPyme  ="", iEPO  ="",  iCta  ="";
	VectorTokenizer vtCtaEPOPyme = null;
	Vector vCtaEPOPyme = null;
	for(int i=0;i<chkCtaBanc.length;i++) {
		vtCtaEPOPyme = new VectorTokenizer(chkCtaBanc[i],"|");
		vCtaEPOPyme = vtCtaEPOPyme.getValuesVector();
		try{
			iCta += (String)vCtaEPOPyme.get(0)+",";
			iEPO += (String)vCtaEPOPyme.get(1)+",";
			iPyme += (String)vCtaEPOPyme.get(2)+",";			
		}catch(ArrayIndexOutOfBoundsException aiobe){
			out.println(aiobe);
		}
	}
		
	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		
		folioCert = "LIBERINDIV"+iNoCliente;
		Seguridad s = new Seguridad();
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {			
			_acuse = s.getAcuse();					
			
			consulta =  parametrosDescuento.autorizacionCuenta( chkCtaBanc , sIfConvenioUnico, iNoCliente, strLogin , iNoNafinElectronico.toString(), strNombreUsuario,  strNombre);
			jsonObj = JSONObject.fromObject(consulta);	
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("iCta", iCta);	
			jsonObj.put("iEPO", iEPO);	
			jsonObj.put("iPyme", iPyme);	
			jsonObj.put("sIfConvenioUnico", sIfConvenioUnico);	
			
		} else {	//autenticacion fallida
			String _error = s.mostrarError();
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "La autentificaci\u00F3n no se llev\u00F3 a cabo. " + _error + ".<br>Proceso CANCELADO");
		}
	}	
	
	infoRegresar = jsonObj.toString();		

}else if(informacion.equals("ArchivoPDFAcuse")){
	
	sIfConvenioUnico =parametrosDescuento.sIfConvenioUnico(iNoCliente);
	String iPyme = (request.getParameter("iPyme") == null)?"":request.getParameter("iPyme");
	String iEPO = (request.getParameter("iEPO") == null)?"":request.getParameter("iEPO");
	String iCta = (request.getParameter("iCta") == null)?"":request.getParameter("iCta");
		
	if(!iPyme.equals("")){
		int tamanio1 =	iPyme.length();
		iPyme =iPyme.substring(0,tamanio1-1);
	}
	if(!iEPO.equals("")){
		int tamanio1 =	iEPO.length();
		iEPO =iEPO.substring(0,tamanio1-1);
	}
	if(!iCta.equals("")){
		int tamanio1 =	iCta.length();
		iCta =iCta.substring(0,tamanio1-1);
	}
		
	parametros= new ArrayList();
	parametros.add(iNoCliente);
	parametros.add(iPyme);
	parametros.add(iEPO);
	parametros.add(iCta);
	parametros.add(strNombreUsuario);
	parametros.add(strDirectorioTemp);
	parametros.add(sIfConvenioUnico);
	parametros.add((String)session.getAttribute("strPais"));
	parametros.add(strNombre);
	parametros.add(strNombreUsuario);
	parametros.add(strLogo);
	parametros.add(strDirectorioPublicacion);
		
	String nombreArchivo = parametrosDescuento.archivoAcuseAutorizacion(parametros);

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();		

}	
%>
<%=infoRegresar%>