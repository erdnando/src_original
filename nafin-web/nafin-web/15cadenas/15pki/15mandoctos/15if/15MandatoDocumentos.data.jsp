<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,	
		java.text.*, 	
		java.sql.*, 
		java.math.*,
		netropology.utilerias.*,	
		com.netro.afiliacion.*,
		com.netro.cadenas.*,		
		com.netro.mandatodoc.*,
		com.netro.exception.*,
		com.netro.descuento.*,
		com.netro.model.catalogos.*,		
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%@ include file="../../certificado.jspf" %>

<%
InstruccionIrrevocable InstIrrevocable = new InstruccionIrrevocable();
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String claveTipoCadena = (request.getParameter("claveTipoCadena")!=null)?request.getParameter("claveTipoCadena"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String num_electronico = (request.getParameter("num_electronico")!=null)?request.getParameter("num_electronico"):"";
String clave_estatus = (request.getParameter("clave_estatus")!=null)?request.getParameter("clave_estatus"):"";
String fecha_solicitud_ini = (request.getParameter("fecha_solicitud_ini")!=null)?request.getParameter("fecha_solicitud_ini"):"";
String fecha_solicitud_fin = (request.getParameter("fecha_solicitud_fin")!=null)?request.getParameter("fecha_solicitud_fin"):"";
String fecha_aceptacion_ini = (request.getParameter("fecha_aceptacion_ini")!=null)?request.getParameter("fecha_aceptacion_ini"):"";
String fecha_aceptacion_fin = (request.getParameter("fecha_aceptacion_fin")!=null)?request.getParameter("fecha_aceptacion_fin"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String nafin_electronico = (request.getParameter("nafin_electronico")!=null)?request.getParameter("nafin_electronico"):"";
String nombre_pyme = (request.getParameter("nombre_pyme")!=null)?request.getParameter("nombre_pyme"):"";
String nombre_epo = (request.getParameter("nombre_epo")!=null)?request.getParameter("nombre_epo"):"";
String nombre_if = (request.getParameter("nombre_if")!=null)?request.getParameter("nombre_if"):"";
String nombre_descontante = (request.getParameter("nombre_descontante")!=null)?request.getParameter("nombre_descontante"):"";
String fecha_solicitud = (request.getParameter("fecha_solicitud")!=null)?request.getParameter("fecha_solicitud"):"";
String folio_instruccion = (request.getParameter("folio_instruccion")!=null)?request.getParameter("folio_instruccion"):"";
String fecha_aceptacion_if = (request.getParameter("fecha_aceptacion_if")!=null)?request.getParameter("fecha_aceptacion_if"):"";
String monto_credito = (request.getParameter("monto_credito")!=null)?request.getParameter("monto_credito"):"";
String fecha_vencimiento = (request.getParameter("fecha_vencimiento")!=null)?request.getParameter("fecha_vencimiento"):"";
String banco_servicio = (request.getParameter("banco_servicio")!=null)?request.getParameter("banco_servicio"):"";
String cuenta_bancaria = (request.getParameter("cuenta_bancaria")!=null)?request.getParameter("cuenta_bancaria"):"";
String tipo_cuenta = (request.getParameter("tipo_cuenta")!=null)?request.getParameter("tipo_cuenta"):"";
String numero_credito = (request.getParameter("numero_credito")!=null)?request.getParameter("numero_credito"):"";
String moneda = (request.getParameter("moneda")!=null)?request.getParameter("moneda"):"";
String moneda_descuento = (request.getParameter("moneda_descuento")!=null)?request.getParameter("moneda_descuento"):"";
String observaciones = (request.getParameter("observaciones")!=null)?request.getParameter("observaciones"):"";
String estatus_solicitud = (request.getParameter("estatus_solicitud")!=null)?request.getParameter("estatus_solicitud"):"";
String nueva_fecha_vencimiento  = (request.getParameter("nueva_fecha_vencimiento")!=null)?request.getParameter("nueva_fecha_vencimiento"):"";

String fechaCarga = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
String horaCarga = (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());	
List datosPymes = new ArrayList();	
StringBuffer 	columnasGrid	= new StringBuffer();
StringBuffer 	columnasStore	= new StringBuffer();
StringBuffer 	columnasRecords	= new StringBuffer();

int start = 0, 	limit = 0;
String consulta ="", habilitaCamposInstruccion ="N", tipoCadenaInstruccion ="", infoRegresar ="", respuesta ="N";
if (strPerfil.equals("ADMIN IF")) {
	habilitaCamposInstruccion = "S";
} else if (strPerfil.equals("DESCONT IF")) {
	habilitaCamposInstruccion = "S";
} else {
	habilitaCamposInstruccion = "N";
}

//habilitaCamposInstruccion = "N";


JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

Afiliacion 		BeanAfiliacion 		= ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
MandatoDocumentos 		mandatoDocumentos 		= ServiceLocator.getInstance().lookup("MandatoDocumentosEJB",MandatoDocumentos.class);
ParametrosDescuento 		parametrosDescuento 		= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);


if(!folio_instruccion.equals("")){
	tipoCadenaInstruccion = mandatoDocumentos.obtenerTipoCadenaInstruccion(folio_instruccion);
}


	columnasGrid	= new StringBuffer();
	columnasStore	= new StringBuffer();
	columnasRecords	= new StringBuffer();
	num_electronico = (request.getParameter("num_electronico")!=null)?request.getParameter("num_electronico"):"";
	String txtRFC = (request.getParameter("txtRFC")!=null)?request.getParameter("txtRFC"):"";
	String ic_pyme ="";
	Calendar calendario = Calendar.getInstance();
	int anioActual = calendario.get(Calendar.YEAR);
	int mesActual = calendario.get(Calendar.MONTH) + 1;
	int ultimoDiaMesActual = calendario.getActualMaximum(Calendar.DAY_OF_MONTH);
	registros = new JSONArray();
	HashMap estadisticas = new HashMap();
	HashMap informacionPorEpo = new HashMap();
	int  numeroEpos =0;
	BigDecimal doctosPubPorEpo = new BigDecimal("0");
	BigDecimal doctosPubTotal = new BigDecimal("0");
	BigDecimal doctosPubPorMes = new BigDecimal("0");
	BigDecimal montoPubPorMes = new BigDecimal("0");
	BigDecimal montoPubTotal = new BigDecimal("0");
	BigDecimal montoPubPorEpo = new BigDecimal("0");
	if(!txtRFC.equals("")) {
		num_electronico = obtieneIC_Pyme(txtRFC);
	}
	if(!num_electronico.equals("")) {
		datosPymes =  parametrosDescuento.datosPymes(num_electronico, "" ); //para obtener 
		if(datosPymes.size()>0){
			ic_pyme= (String)datosPymes.get(0);	
		}		
	}
	//para obtener los años y Meses 
	HashMap aniosOperacion = InstIrrevocable.generaEncabezadoTabla(mesActual, anioActual);
	int numeroAnios = Integer.parseInt(aniosOperacion.get("numeroAnios").toString());
	
if (informacion.equals("catalogoTipoCadena")  ) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_epo");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setTabla("comcat_tipo_epo");
	cat.setOrden("ic_tipo_epo");
	infoRegresar = cat.getJSONElementos();	
	
}else if (informacion.equals("catalogoEstatus")  ) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_man_doc");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setTabla("comcat_estatus_man_doc");
	cat.setOrden("ic_estatus_man_doc");
	infoRegresar = cat.getJSONElementos();	

}else if (informacion.equals("CatalogoEPO")  ) {

	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setTipoCadena(claveTipoCadena);
	cat.setClaveIf(iNoCliente);
	cat.setMandatoDocumentos("S");
	infoRegresar = cat.getJSONElementos();		


} else if (informacion.equals("busquedaAvanzada") ) {

	String nombrePyme = (request.getParameter("nombrePyme")!=null)?request.getParameter("nombrePyme"):"";
	String rfcPyme = (request.getParameter("rfcPyme")!=null)?request.getParameter("rfcPyme"):"";
	
	CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");	
	if(!rfcPyme.equals(""))
	cat.setRfc_pyme(rfcPyme);
	if(!nombrePyme.equals(""))
	cat.setNombre_pyme(nombrePyme);
	cat.setPantalla("SoliAfi");		
	cat.setOrden("p.cg_razon_social");		
	infoRegresar = cat.getJSONElementos();

		
} else if(informacion.equals("pymeNombre") ) {
	 ic_pyme= "";
	String txtNombre ="";
	
	datosPymes =  parametrosDescuento.datosPymes(num_electronico, "" ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);	
	jsonObj.put("ic_pyme", ic_pyme);
	infoRegresar = jsonObj.toString();	

}else  if(informacion.equals("CatBancoServicio")  ) {

	CatalogoBancoServicio cat = new CatalogoBancoServicio();
	cat.setCampoClave("cif.ic_if ||'|'|| cif.cg_razon_social");
	//cat.setCampoClave("cif.cg_razon_social");
	cat.setCampoDescripcion("cif.cg_razon_social");
	cat.setTipoFinanciera("1,4,5,6,7,10,23");
	cat.setOrden("cif.cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else  if(informacion.equals("CatTipoCuenta")  ) {
 
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("cc_tipo_cuenta ||'|'|| cg_descripcion");
	//cat.setCampoClave("cc_tipo_cuenta");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setTabla("comcat_tipo_cta_man_doc");
	cat.setOrden("cc_tipo_cuenta");
	infoRegresar = cat.getJSONElementos();	
	
 }else  if(informacion.equals("CatMoneda")  ) {
 
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setOrden("ic_moneda");
	cat.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = cat.getJSONElementos();	

 }else  if(informacion.equals("CatMonedaDescuento")  ) {
 
	JSONArray registros2 = new JSONArray();
	HashMap info = new HashMap();
	for(int i=0; i<3; i++){
		info = new HashMap();
		if(i==0){
			info.put("clave", "0");
			info.put("descripcion", "AMBAS");			
		}
		if(i==1){
			info.put("clave", "1");
			info.put("descripcion", "MONEDA NACIONAL");			
		}
		if(i==2){
			info.put("clave", "54");
			info.put("descripcion", "DOLAR AMERICANO");			
		}
		registros2.add(info);
	}

infoRegresar =  "{\"success\": true, \"total\": \"" + registros2.size() + "\", \"registros\": " + registros2.toString()+"}";

		
} else if (informacion.equals("Consultar")){


	ConsultaMandatoDocumentos paginador = new ConsultaMandatoDocumentos();
	
	paginador.setPerfil_usuario(strPerfil);
	paginador.setClaveTipoCadena(claveTipoCadena);
	paginador.setClave_epo(clave_epo);
	paginador.setClave_if(iNoCliente);
	paginador.setNumero_nafele(num_electronico);
	paginador.setClave_estatus(clave_estatus);
	paginador.setFecha_solicitud_ini(fecha_solicitud_ini);
	paginador.setFecha_solicitud_fin(fecha_solicitud_fin);	
	paginador.setFecha_aceptacion_ini(fecha_aceptacion_ini);
	paginador.setFecha_aceptacion_fin(fecha_aceptacion_fin);
	paginador.setPerfil_usuario(strPerfil);	
	paginador.setRegistrosAux(null);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
		
			try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		Registros registros1  = new Registros();
		Registros registrosEpos  = new Registros();
		registros1 = queryHelper.getPageResultSet(request,start,limit);
		registrosEpos = queryHelper.getPageResultSet(request,start,limit);
		
		List reg = new ArrayList();
		StringBuffer epos = new StringBuffer();
		String folioAnterior = "";
		String folioActual = "";
		int rowspan = 0;
		while (registros1.next()){
			folioActual = registros1.getString("folio_instruccion");
			if (!folioActual.equals(folioAnterior)) {
				 epos = new StringBuffer();
				//rowspan = 0;
				while (registrosEpos.next()) {
					if (folioActual.equals(registrosEpos.getString("folio_instruccion"))) {
						epos.append(registrosEpos.getString("nombre_epo")+"    ");		
					}					
				}				
				
				HashMap hash = new HashMap();
				hash.put("FOLIO_INSTRUCCION",	registros1.getString("folio_instruccion"));
				hash.put("NAFIN_ELECTRONICO",	registros1.getString("NAFIN_ELECTRONICO"));
				hash.put("NOMBRE_PYME",	registros1.getString("NOMBRE_PYME"));
				hash.put("IC_PYME",	registros1.getString("ic_pyme"));
				hash.put("IC_EPO",	registros1.getString("ic_epo"));
				hash.put("NOMBRE_EPO",	epos.toString());
				hash.put("IC_IF",	registros1.getString("ic_if"));
				hash.put("NOMBRE_IF",	registros1.getString("nombre_if"));
				hash.put("NOMBRE_DESCONTANTE",	registros1.getString("nombre_descontante"));
				hash.put("FECHA_SOLICITUD",	registros1.getString("fecha_solicitud"));
				hash.put("FECHA_ACEPTACION_IF",	registros1.getString("fecha_autorizacion_if"));
				hash.put("MONTO_CREDITO",	registros1.getString("monto_credito"));
				hash.put("ANTERIOR_MONTO_CREDITO",	registros1.getString("monto_credito"));
				hash.put("FECHA_VENCIMIENTO",	registros1.getString("fecha_vencimiento"));
				hash.put("CUENTA_BANCARIA",	registros1.getString("cuenta_bancaria"));
				String ctaMas_banco = (registros1.getString("banco_servicio")).equals("")?"":registros1.getString("clave_banco_servicio")+"|"+registros1.getString("banco_servicio");
				hash.put("BANCO_SERVICIO",	ctaMas_banco);
				String ctaMas_tipo = (registros1.getString("tipo_cuenta")).equals("")?"":registros1.getString("clave_tipo_cuenta")+"|"+registros1.getString("tipo_cuenta");
				hash.put("TIPO_CUENTA",	ctaMas_tipo);
				hash.put("NUMERO_CREDITO",	registros1.getString("numero_credito"));
				hash.put("MONEDA",	registros1.getString("moneda"));
				hash.put("MONEDA_DESCUENTO",	registros1.getString("moneda_descuento"));
				hash.put("OBSERVACIONES",	registros1.getString("observaciones").replaceAll("\r", " "));
				hash.put("NUEVA_FECHA_VENCIMIENTO",	!registros1.getString("nueva_fecha_vencimiento").trim().equals("")	?registros1.getString("nueva_fecha_vencimiento")	:"");
				hash.put("ANTERIOR_FECHA_VENCIMIENTO",	!registros1.getString("nueva_fecha_vencimiento").trim().equals("")	?registros1.getString("nueva_fecha_vencimiento")	:"");
				hash.put("FECHA_MODIFICACION",	!registros1.getString("fecha_modificacion").trim().equals("")		?registros1.getString("fecha_modificacion")		:"NA");
				hash.put("ESTATUS_SOLICITUD",	registros1.getString("estatus_solicitud"));
				hash.put("DESCAUT",	registros1.getString("fechaDescAut"));
				hash.put("OBSERVACIONES",	registros1.getString("OBSERVACIONES"));
				hash.put("CLAVE_ESTATUS",	registros1.getString("CLAVE_ESTATUS"));
				hash.put("HABILITACAMPOS",	habilitaCamposInstruccion);
				hash.put("FECHA_ACTUAL",fechaCarga);	
				reg.add(hash);
				folioAnterior = folioActual;
			}
		}

		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(reg);
		consulta = "{\"success\": true, \"total\": "+ queryHelper.getIdsSize() +" , \"registros\": " + jsObjArray.toString() + " }";

		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("HABILITACAMPOS",habilitaCamposInstruccion);					
	   infoRegresar = jsonObj.toString();

	} catch(Exception e) {	
		throw new AppException("Error en la paginacion", e);
	}

} else if (informacion.equals("AcusePyme")) {

	try{
	
		String nafinElectronicoPyme = "", nombrePyme = "", nombreEpo = "", nombreIf = "", nombreDescontante = "",  fechaSolicitud = "", 	horaSolicitud = "", usuarioSolicitud = "";
		HashMap informacionSolicitud = mandatoDocumentos.obtenerInformacionInstruccion(folio_instruccion);
	
	  
		int numeroRegistros = Integer.parseInt((String)informacionSolicitud.get("numeroRegistros"));
		
		if (numeroRegistros > 0) {
			for (int i = 0; i < numeroRegistros; i++) {
				HashMap solicitud = (HashMap)informacionSolicitud.get("solicitud"+i);
				if (i == 0) {
					nafinElectronicoPyme = (String)solicitud.get("nafinElectronicoPyme");
					nombrePyme = (String)solicitud.get("nombrePyme");							
					nombreIf = (String)solicitud.get("nombreIf");
					nombreDescontante = (String)solicitud.get("nombreDescontante");
					fechaSolicitud = (String)solicitud.get("fechaSolicitud");
					horaSolicitud = (String)solicitud.get("horaSolicitud");
					usuarioSolicitud = (String)solicitud.get("usuarioSolicitud");
				}
				nombreEpo += (String)solicitud.get("nombreEpo");
				if (numeroRegistros > 1) {
					nombreEpo += "\n";
				}
			}
		}

		ConsultaMandatoDocumentos pag = new ConsultaMandatoDocumentos();
		pag.setClaveTipoCadena(tipoCadenaInstruccion);
		pag.setNombreEpo(nombreEpo);
		pag.setNombreIf(nombreIf);
		pag.setNombreDescontante(nombreDescontante);
		pag.setFolioInstruccion(folio_instruccion);
		pag.setFechaAct(fechaSolicitud);
		pag.setHoraAct(horaSolicitud);
		pag.setUsuarioCarga(usuarioSolicitud);
		pag.setPerfil_usuario(strPerfil);
		String nombreArchivo = pag.crearPageCustomFile(request, null,strDirectorioTemp, "AcusePDF");

		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	
	} catch (Exception e) {
		throw new AppException("Error al generar el archivo", e);
	}	finally {	
	}
	
}else if (informacion.equals("ArchivoPDF") ||  informacion.equals("ArchivoCSV") ){

	String nombreArchivo ="";
	try{
		
		HashMap parametrosConsulta = new HashMap();	
		parametrosConsulta.put("claveTipoCadena", claveTipoCadena);
		parametrosConsulta.put("claveEpo", clave_epo);
		parametrosConsulta.put("claveIf", iNoCliente);
		parametrosConsulta.put("numeroNafele", num_electronico);
		parametrosConsulta.put("claveEstatus", clave_estatus);
		parametrosConsulta.put("fechaSolicitudIni", fecha_solicitud_ini);
		parametrosConsulta.put("fechaSolicitudFin", fecha_solicitud_fin);
		parametrosConsulta.put("fechaAceptacionIni", fecha_aceptacion_ini);
		parametrosConsulta.put("fechaAceptacionFin", fecha_aceptacion_fin);
		parametrosConsulta.put("perfilUsuario", strPerfil);
		Registros registros1 = mandatoDocumentos.consultarIntruccionesIrrevocables(parametrosConsulta);
		Registros registrosEpos = mandatoDocumentos.consultarIntruccionesIrrevocables(parametrosConsulta);

		ConsultaMandatoDocumentos pag = new ConsultaMandatoDocumentos();
		pag.setRegistrosAux(registrosEpos);
		pag.setPerfil_usuario(strPerfil);
		
		if (informacion.equals("ArchivoPDF") ) {
			 nombreArchivo = pag.crearPageCustomFile(request, registros1, strDirectorioTemp, "ConsultaPDF");
		}
		if (informacion.equals("ArchivoCSV") ) {
			 nombreArchivo = pag.crearCustomFiles(request, registros1,  strDirectorioTemp, "ConsultaCSV");		 
		}
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	
	} catch (Exception e) {
		throw new AppException("Error al generar el archivo", e);
	}	finally {
		}	


}else if (informacion.equals("ConsultarPreacuse1")  ){

	
	datos = new HashMap();
	datos.put("FOLIO_INSTRUCCION", folio_instruccion);
	datos.put("NAFIN_ELECTRONICO", nafin_electronico);
	datos.put("NOMBRE_PYME", nombre_pyme);
	datos.put("NOMBRE_EPO", nombre_epo);
	datos.put("NOMBRE_IF", nombre_if);
	datos.put("NOMBRE_DESCONTANTE", nombre_descontante);
	datos.put("FECHA_SOLICITUD", fecha_solicitud);
	registros.add(datos);	
		
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

}else if (informacion.equals("ConsultarPreacuse2")  ){

	if(!banco_servicio.equals("")) {
		banco_servicio = banco_servicio.substring(banco_servicio.lastIndexOf("|")+1, banco_servicio.length());	
	}
	tipo_cuenta = tipo_cuenta.substring(tipo_cuenta.lastIndexOf("|")+1, tipo_cuenta.length());
	fecha_aceptacion_if =fechaCarga;
	
	datos = new HashMap();
	datos.put("FECHA_ACEPTACION_IF", fecha_aceptacion_if);
	datos.put("MONTO_CREDITO", monto_credito);
	datos.put("FECHA_VENCIMIENTO", fecha_vencimiento);
	datos.put("BANCO_SERVICIO", banco_servicio);
	datos.put("CUENTA_BANCARIA", cuenta_bancaria);
	datos.put("TIPO_CUENTA", tipo_cuenta);
	datos.put("NUMERO_CREDITO", numero_credito);
	registros.add(datos);	
		
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	

}else if (informacion.equals("ConsultarPreacuse3")  ){

	
	if(moneda.equals("1")) moneda  = "MONEDA NACIONAL";
	if(moneda.equals("54")) moneda  = "DOLAR AMERICADO";
	if(moneda_descuento.equals("0")) moneda_descuento  = "AMBAS";
	if(moneda_descuento.equals("1")) moneda_descuento  =  "MONEDA NACIONAL";
	if(moneda_descuento.equals("54")) moneda_descuento  = "DOLAR AMERICADO";
		
	datos = new HashMap();
	datos.put("MONEDA", moneda);
	datos.put("MONEDA_DESCUENTO", moneda_descuento);
	datos.put("OBSERVACIONES", observaciones);
	datos.put("ESTATUS_SOLICITUD", estatus_solicitud);	
	registros.add(datos);	
		
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

				 
}else if (informacion.equals("PreAcuse")  ){

StringBuffer mensaje = new StringBuffer();
StringBuffer texto_plano = new StringBuffer();
String texto = (request.getParameter("texto")!=null)?request.getParameter("texto"):"";

texto_plano.append(texto+"\n \n ");

mensaje.append(" "+
"<table border='0' width='900' cellpadding='0' cellspacing='0'> "+
"<tr>"+
	"<td align='center'>"+
	"<table align='center' border='1' width='900'>"+
		"<tr>"+
		"<td class='formas' align='left'>");
		if (tipoCadenaInstruccion.equals("1")) {
			mensaje.append(" <b>Aceptación del IF</b><br/><br/> "+	
						"Al aceptar el mensaje de datos que contiene la <b>INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b> que me otorga el <b>SUJETO DE APOYO</b>,"+
						"	en este acto manifiesto que al  <b>SUJETO DE APOYO</b> le otorgué un <b>CRÉDITO</b>  para el financiamiento de los <b>CONTRATOS</b> que le fueron adjudicados "+
						"	por la <b>DEPENDENCIA O ENTIDAD</b> que se indica, y me comprometo a cancelar la <b>INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b> una "+
						"	vez que se encuentre totalmente pagado el crédito y sus accesorios, en el entendido de que la <b>INSTRUCCIÓN IRREVOCABLE NAFIN</b> quedará cancelada automáticamente.<br/><br/>"+
						"   <b>Tratándose del DESCONTANTE</b><br/><br/> "+
						" Al aceptar el mensaje de datos que contiene la <b>INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b> que me otorga el <b>SUJETO DE APOYO</b>, "+
						"	en este acto manifiesto que el  <b>SUJETO DE APOYO</b> hizo de mi conocimiento que se le otorgó un <b>CRÉDITO</b> por parte del <b>INTERMEDIARIO FINANCIERO</b> "+
						"	para el financiamiento de los <b>CONTRATOS</b> que le fueron adjudicados por la <b>DEPENDENCIA O ENTIDAD</b> que se indica, y me comprometo a cancelar la "+
						"	<b>INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b> una vez que reciba del <b>INTERMEDIARIO FINANCIERO</b> la confirmación de que el crédito"+
						"	se encuentra totalmente pagado así como de sus accesorios, en el entendido de que la <b>INSTRUCCIÓN IRREVOCABLE NAFIN</b> quedará cancelada automáticamente.");
		
						texto_plano.append("Aceptación del IF \n \n ");
						texto_plano.append("Al aceptar el mensaje de datos que contiene la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE que me otorga el SUJETO DE APOYO, ");
						texto_plano.append("en este acto manifiesto que al SUJETO DE APOYO le otorgué un CRÉDITO para el financiamiento de los CONTRATOS que le fueron adjudicados por la DEPENDENCIA O ENTIDAD ");
						texto_plano.append("que se indica, y me comprometo a cancelar la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE una vez que se encuentre totalmente pagado ");
						texto_plano.append("el crédito y sus accesorios, en el entendido de que la INSTRUCCIÓN IRREVOCABLE NAFIN quedará cancelada automáticamente. \n");
							
						texto_plano.append("Tratándose del DESCONTANTE \n");
						texto_plano.append("Al aceptar el mensaje de datos que contiene la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE que me otorga el SUJETO DE APOYO, ");
						texto_plano.append("en este acto manifiesto que el SUJETO DE APOYO hizo de mi conocimiento que se le otorgó un CRÉDITO por parte del INTERMEDIARIO FINANCIERO para el financiamiento de los CONTRATOS ");
						texto_plano.append("que le fueron adjudicados por la DEPENDENCIA O ENTIDAD que se indica, y me comprometo a cancelar la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE ");
						texto_plano.append("una vez que reciba del INTERMEDIARIO FINANCIERO la confirmación de que el crédito se encuentra totalmente pagado así como de sus accesorios, en el entendido de ");
						texto_plano.append("que la INSTRUCCIÓN IRREVOCABLE NAFIN quedará cancelada automáticamente.");
		
		} else if (tipoCadenaInstruccion.equals("2")) {
			mensaje.append(" <b>Aceptación del IF</b><br/><br/>	"+						
            " Al aceptar el mensaje de datos que contiene la <b>INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b> que me otorga el <b>SUJETO DE APOYO</b>, "+
				" en este acto manifiesto que al  <b>SUJETO DE APOYO</b> le otorgué un <b>CRÉDITO</b>, y me comprometo a cancelar la <b>INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b> una  "+
				" vez que se encuentre totalmente pagado el crédito y sus accesorios, en el entendido de que la <b>INSTRUCCIÓN IRREVOCABLE NAFIN</b> quedará cancelada automáticamente.<br/><br/> "+
            " <b>Tratándose del DESCONTANTE</b><br/><br/> "+
            " Al aceptar el mensaje de datos que contiene la <b>INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b> que me otorga el <b>SUJETO DE APOYO</b>, "+
				" en este acto manifiesto que el  <b>SUJETO DE APOYO</b> hizo de mi conocimiento que se le otorgó un <b>CRÉDITO</b> por parte del <b>INTERMEDIARIO FINANCIERO</b> que se indica, y me comprometo a cancelar la "+
				" <b>INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b> una vez que reciba del <b>INTERMEDIARIO FINANCIERO</b> la confirmación de que el crédito"+
				" se encuentra totalmente pagado así como de sus accesorios, en el entendido de que la <b>INSTRUCCIÓN IRREVOCABLE NAFIN</b> quedará cancelada automáticamente.");
			
			texto_plano.append("Aceptación del IF \n \n");
			texto_plano.append("Al aceptar el mensaje de datos que contiene la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE que me otorga el SUJETO DE APOYO, ");
			texto_plano.append("en este acto manifiesto que al SUJETO DE APOYO le otorgué un CRÉDITO, y me comprometo a cancelar la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE una vez que se encuentre totalmente pagado ");
			texto_plano.append("el crédito y sus accesorios, en el entendido de que la INSTRUCCIÓN IRREVOCABLE NAFIN quedará cancelada automáticamente.\\n\\n");
			texto_plano.append("Tratándose del DESCONTANTE  \n");
			texto_plano.append("Al aceptar el mensaje de datos que contiene la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE que me otorga el SUJETO DE APOYO, ");
			texto_plano.append("en este acto manifiesto que el SUJETO DE APOYO hizo de mi conocimiento que se le otorgó un CRÉDITO por parte del INTERMEDIARIO FINANCIERO que se indica, y me comprometo a cancelar la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE ");
			texto_plano.append("una vez que reciba del INTERMEDIARIO FINANCIERO la confirmación de que el crédito se encuentra totalmente pagado así como de sus accesorios, en el entendido de ");
			texto_plano.append("que la INSTRUCCIÓN IRREVOCABLE NAFIN quedará cancelada automáticamente.");
			
			}
mensaje.append("</td>"+
					"</tr>"+
				"</table>"+
			"</td>"+
		"</tr>"+
	"</table>");
	
	
	
	if(!banco_servicio.equals("")) {
		banco_servicio  = banco_servicio.substring(0,banco_servicio.indexOf('|'));	
	}
	tipo_cuenta  = tipo_cuenta.substring(0,tipo_cuenta.indexOf('|'));
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", mensaje.toString());
	jsonObj.put("texto_plano", texto_plano.toString());
	jsonObj.put("folio_instruccion", folio_instruccion);
	jsonObj.put("monto_credito", monto_credito);
	jsonObj.put("fecha_vencimiento", fecha_vencimiento);
	jsonObj.put("cuenta_bancaria", cuenta_bancaria);
	jsonObj.put("tipo_cuenta", tipo_cuenta);
	jsonObj.put("numero_credito", numero_credito);
	jsonObj.put("moneda", moneda);
	jsonObj.put("observaciones", observaciones);
	jsonObj.put("observaciones", observaciones);
	jsonObj.put("banco_servicio", banco_servicio);
	jsonObj.put("moneda_descuento", moneda_descuento);
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("GenerarAcuse")  ){

	String externContent = (request.getParameter("textoFirmar")!=null)?request.getParameter("textoFirmar"):"";
	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";	
	String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
	String folioCert = request.getAttribute("folioCert")==null?"ABCDE123456789":(String)request.getAttribute("folioCert");
	String   mensajeAutent ="", acuse ="", recibo ="", usuario =iNoUsuario+"-"+strNombreUsuario;
	boolean errorUsuario = true;
	HashMap informacionSolicitud = new HashMap();
	HashMap eposInstruccion = new HashMap();
	numeroEpos = 0;
	
	char getReceipt = 'Y';
	Seguridad seguridad = new Seguridad();
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		errorUsuario = false;
		if(errorUsuario == false && seguridad.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)){
		
			List informacion_solicitud = new ArrayList();
			informacion_solicitud.add(folio_instruccion);
			informacion_solicitud.add(monto_credito);
			informacion_solicitud.add(fecha_vencimiento);
			informacion_solicitud.add(cuenta_bancaria);
			informacion_solicitud.add(tipo_cuenta);
			informacion_solicitud.add(numero_credito);
			informacion_solicitud.add(moneda);
			informacion_solicitud.add(observaciones);
			informacion_solicitud.add(banco_servicio);
			informacion_solicitud.add(moneda_descuento);		
						
			try {
				//Se crea la instancia del EJB para su utilización.
       
				mandatoDocumentos.aceptarSolicitudMandatoDoctos(informacion_solicitud);
				
				informacionSolicitud = mandatoDocumentos.obtenerSolicitudMandatoDoctos(folio_instruccion);//FODEA 033 - 2010 ACF
			
				eposInstruccion = (HashMap)informacionSolicitud.get("eposInstruccion");
				numeroEpos = Integer.parseInt((String)eposInstruccion.get("numeroEpos"));
				
			} catch(Exception e) {
				e.printStackTrace();
				throw new AppException("ERROR AL ACEPTAR LA INSTRUCCION IRREVOCABLE", e);
			}
			
			recibo = seguridad.getAcuse();
			acuse = folioCert;
			mensajeAutent="<b>La autentificación se llevó a cabo con éxito <br/> Recibo: "+acuse+"</b>";
		}else {
			mensajeAutent	="<b>La autentificación no se llevó a cabo.PROCESO CANCELADO</b>";
		}
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("fechaCarga", fechaCarga);
	jsonObj.put("horaCarga", horaCarga);
	jsonObj.put("mensajeA", mensaje);	
	jsonObj.put("mensajeAutent", mensajeAutent);	
	jsonObj.put("acuse", acuse);	
	jsonObj.put("recibo", recibo);	
	jsonObj.put("usuario", usuario);
	
	infoRegresar = jsonObj.toString();

 } else if (informacion .equals("ArchivoPDFAcuse")  ) {	
 
 	String acuse = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
	String recibo = (request.getParameter("recibo")!=null)?request.getParameter("recibo"):"";
	String hora_carga = (request.getParameter("hora_carga")!=null)?request.getParameter("hora_carga"):"";
			 
	List parametros = new ArrayList();	
	parametros.add("ACUSE");// tipoArchivo
	parametros.add(strLogin);
	parametros.add(strNombreUsuario);
	parametros.add(recibo);
	parametros.add(acuse);	
	parametros.add(fechaCarga);
	parametros.add(hora_carga);
	parametros.add(folio_instruccion);	
	parametros.add(tipoCadenaInstruccion);
	parametros.add(strDirectorioTemp);
	parametros.add((String)session.getAttribute("strPais"));
	parametros.add(iNoCliente);	
	parametros.add(strNombre);
	parametros.add(strNombreUsuario);
	parametros.add(strLogo);
	parametros.add(strDirectorioPublicacion);
	
	String nombreArchivo = InstIrrevocable.archivAcusePDF(parametros );		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();	


}else if (informacion.equals("GenerarRechazo")  ){

	List parametros = new ArrayList();	
	parametros.add(folio_instruccion);
	parametros.add(observaciones);
		
	try {
		mandatoDocumentos.rechazarSolicitudMandatoDoctos(parametros);		
		respuesta="OK";
	} catch(Exception e) {
		 respuesta="N";
		e.printStackTrace();
		throw new AppException("ERROR AL ACEPTAR LA INSTRUCCION IRREVOCABLE", e);
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", respuesta);	
	infoRegresar = jsonObj.toString();	
	

}else if (informacion.equals("AmpliarMonto")  ){
	
	List informacion_nueva = new ArrayList();
	informacion_nueva.add(folio_instruccion);					
	informacion_nueva.add(observaciones); 						
	informacion_nueva.add(monto_credito);						
	informacion_nueva.add(nueva_fecha_vencimiento);			
	try {
		mandatoDocumentos.ampliarMonto(informacion_nueva);	
		respuesta="OK";
	} catch(Exception e) {
		 respuesta="N";
		e.printStackTrace();
		throw new AppException("ERROR AL ACEPTAR LA INSTRUCCION IRREVOCABLE", e);
	}	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", respuesta);	
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("Cancelar")  ){
	
	List informacion_solicitud = new ArrayList();
	informacion_solicitud.add(folio_instruccion);					
	informacion_solicitud.add(observaciones); 						
			
	try {
		mandatoDocumentos.cancelarSolicitudMandatoDoctos(informacion_solicitud);	
		respuesta="OK";
	} catch(Exception e) {
		 respuesta="N";
		e.printStackTrace();
		throw new AppException("ERROR AL ACEPTAR LA INSTRUCCION IRREVOCABLE", e);
	}	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", respuesta);	
	infoRegresar = jsonObj.toString();	
	

}else if (informacion.equals("ActivarDesc")  ||  informacion.equals("Desactivar")   ){

	String csDescAut  = (request.getParameter("csDescAut")!=null)?request.getParameter("csDescAut"):"";
	
	try {
		mandatoDocumentos.activaDesactivaDescAut(folio_instruccion,csDescAut);
		respuesta="OK";
	} catch(Exception e) {
		 respuesta="N";
		e.printStackTrace();
		throw new AppException("ERROR AL ACEPTAR LA INSTRUCCION IRREVOCABLE", e);
	}	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", respuesta);	
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("ConsultaOperacion")  ){

MandatoDocumentos 		beanMandatoDocumento 		= ServiceLocator.getInstance().lookup("MandatoDocumentosEJB",MandatoDocumentos.class);

	AccesoDB con = new AccesoDB();
	try{
	
		con.conexionDB();
		String clave_pyme ="",  clave_if="", fecha_de ="", fecha_a ="",  num_sirac ="", nombrepyme ="", num_documento = "",  fecha_emision = "", fecha_venci = "", nombre_moneda = "", tipo_factoraje = "",
				 monto = "", porc_descuento ="", recursogarantia ="", monto_interes ="",  monto_descuento = "", monto_a_operar ="", tasa = "",  plazo = "", num_proveedor = "", fecha_cambio = "";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		StringBuffer operadosQry 	= new StringBuffer();
		
		// Metodo para obtener los rangos	
		List rangos =  InstIrrevocable.rangos(folio_instruccion);			
		for (int j = 0; j < rangos.size(); j++) {
			List estatus = (List)rangos.get(j);
			String tipo_estatus = "";
			clave_epo 	 = (String)estatus.get(0);
			clave_pyme 	 = (String)estatus.get(1);
			clave_if 	 = (String)estatus.get(2);
			tipo_estatus = (String)estatus.get(4);
			fecha_de 	 = (String)((List)rangos.get(j)).get(3);
			if(rangos.size()-1 == j){
				fecha_a = "";
			}else{
				fecha_a  = (String)((List)rangos.get(j + 1)).get(3);
			}
			
			operadosQry = InstIrrevocable.queryDoctosOperados(fecha_a); 
			System.out.println("operadosQry ********* "+operadosQry);	
			varBind = new ArrayList();
			varBind.add(clave_epo);
			varBind.add(clave_pyme);
			varBind.add(clave_if);
			varBind.add(fecha_de);
			if(!fecha_a.equals("")){varBind.add(fecha_a);}
			
			ps = con.queryPrecompilado(operadosQry.toString(), varBind);
			rs = ps.executeQuery(); 
				
			while (rs.next()) {
			
				num_sirac = (rs.getString("num_sirac")==null)?"":rs.getString("num_sirac");
				nombrepyme 	= (rs.getString("nombrepyme")==null)?"":rs.getString("nombrepyme");
				num_documento = (rs.getString("num_documento")==null)?"":rs.getString("num_documento");
				fecha_emision = (rs.getString("fecha_emision")==null)?"":rs.getString("fecha_emision");
				fecha_venci = (rs.getString("fecha_vencimiento")==null)?"":rs.getString("fecha_vencimiento"); 
				nombre_moneda = (rs.getString("nombre_moneda")==null)?"":rs.getString("nombre_moneda");
				tipo_factoraje = (rs.getString("tipo_factoraje")==null)?"":rs.getString("tipo_factoraje");
				monto = (rs.getString("monto")==null)?"":rs.getString("monto");
				porc_descuento = (rs.getString("porc_descuento")==null)?"":rs.getString("porc_descuento");
				recursogarantia = (rs.getString("recursogarantia")==null)?"":rs.getString("recursogarantia");
				monto_interes = (rs.getString("monto_interes")==null)?"":rs.getString("monto_interes");
				monto_descuento = (rs.getString("monto_descuento")==null)?"":rs.getString("monto_descuento");
				monto_a_operar = (rs.getString("monto_a_operar")==null)?"":rs.getString("monto_a_operar");
				tasa = (rs.getString("tasa")==null)?"":rs.getString("tasa");
				plazo = (rs.getString("plazo")==null)?"":rs.getString("plazo");
				num_proveedor = (rs.getString("num_proveedor")==null)?"":rs.getString("num_proveedor");
				
				datos = new HashMap();
				datos.put("ESTATUS", tipo_estatus);
				datos.put("FECHA_CAMBIO", fecha_de);
				datos.put("NO_CLIENTE",num_sirac );
				datos.put("PROVEEDOR",nombrepyme );
				datos.put("NO_DOCUMENTO", num_documento);
				datos.put("FECHA_EMISION", fecha_emision);
				datos.put("FECHA_VENCIMIENTO", fecha_venci);
				datos.put("MONEDA", nombre_moneda);
				datos.put("TIPO_FACTORAJE", tipo_factoraje);
				datos.put("MONTO",monto );
				datos.put("PORCE_DESCUENTO",porc_descuento );
				datos.put("RECUERSO_GARANTIA",monto_interes );
				datos.put("MONTO_DESC",monto_descuento);
				datos.put("MONTO_INTERES", monto_interes);
				datos.put("MONTO_OPERAR", monto_a_operar);
				datos.put("TASA", tasa);
				datos.put("PLAZO", plazo);
				datos.put("NO_PROVEEDOR", num_proveedor);				
				registros.add(datos);		
			}
			if(rs!=null) rs.close();
			if(ps!=null) ps.close();
			
		}//for rangos

			infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

		con.cierraConexionDB();
	} catch(Exception e) {
		 out.println(e);
		e.printStackTrace();
	} finally {
		if (con.hayConexionAbierta() == true)
		con.cierraConexionDB();
	}

} else if (informacion.equals("ImpOperadosMan")   ||   informacion.equals("ImpOperadosManPDF")     ) {	

	String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";


	List parametros = new ArrayList();	
	parametros.add(folio_instruccion);					
	parametros.add(tipoArchivo);
	parametros.add(strLogin);
	parametros.add(strDirectorioTemp);
	parametros.add((String)session.getAttribute("strPais"));
	parametros.add(iNoCliente);	
	parametros.add(strNombre);
	parametros.add(strNombreUsuario);
	parametros.add(strLogo);
	parametros.add(strDirectorioPublicacion);
		
	String nombreArchivo = InstIrrevocable.archivVerOperados(parametros);	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
	infoRegresar = jsonObj.toString();		

	
} else if(informacion.equals("ConsultarEstadistica")  ) {

	List infoPyme = InstIrrevocable.datosPyme( txtRFC, num_electronico );
	String tituloG= " No. de Nafin Electrónico:  "+ infoPyme.get(0).toString().replaceAll(",", "") +"  Nombre de la Pyme:  "+ infoPyme.get(1).toString().replaceAll(",", "") +"    RFC:  " +infoPyme.get(2).toString();	
	
	//para obtener los registros de las estadisticas por mes	 
	List info  = InstIrrevocable.consEstadistica(ic_pyme , mesActual, anioActual, ultimoDiaMesActual );		
 
	if(info.size()>0){
		informacionPorEpo = (HashMap)info.get(0);
		estadisticas = (HashMap)info.get(1);
		numeroEpos = Integer.parseInt(info.get(2).toString()); 
	}
		
	if (numeroEpos > 0) {

		columnasGrid.append(" [  ");
		columnasGrid.append("  {  ");
		columnasGrid.append(" header: ' EPO', ");
		columnasGrid.append(" tooltip: 'EPO', ");						
		columnasGrid.append(" width: 130, ");
		columnasGrid.append(" align: 'left', ");					
		columnasGrid.append(" dataIndex: 'NOMBRE_EPO' ");
		columnasGrid.append(" }, ");							
		columnasStore.append("  [  ");	
		columnasStore.append("  {  name: 'NOMBRE_EPO',  mapping : 'NOMBRE_EPO' }, ");

		
		for (int i = 0; i < numeroAnios; i++) {
			int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+i)).toString());
				String  anio  = aniosOperacion.get("anio"+i).toString();			
			
			HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+i));
			for (int j = 0; j < mesesPorAnio; j++) {
				String  mes  = meses.get("nombreMes"+j).toString();
				String  indiceMes  ="MES"+j+anio;
				String  indiceMonto  ="MONTO"+j+anio;				
				columnasGrid.append("  {  ");
				columnasGrid.append(" header: 'No. de Doctos "+mes+" "+anio+"', ");
				columnasGrid.append(" tooltip: '"+mes+" "+anio+"', ");						
				columnasGrid.append(" width: 130, ");
				columnasGrid.append(" align: 'center', ");					
				columnasGrid.append(" dataIndex: '"+indiceMes+"' ");
				columnasGrid.append(" },");
			
				columnasGrid.append("  {  ");
				columnasGrid.append(" header: ' Monto de Doctos "+mes+" "+anio+"', ");
				columnasGrid.append(" tooltip: '"+mes+" "+anio+"', ");						
				columnasGrid.append(" width: 130, ");
				columnasGrid.append(" align: 'center', ");					
				columnasGrid.append(" dataIndex: '"+indiceMonto+"' ");
				columnasGrid.append(" },");
				
				columnasStore.append("  {  name: '"+indiceMes+"',  mapping : '"+indiceMes+"' },");	
				
				columnasStore.append("  {  name: '"+indiceMonto+"',  mapping : '"+indiceMonto+"' },");	
						
			}
		}		
		
		columnasGrid.append("  {  ");
		columnasGrid.append(" header: 'TOTAL DOCTOS POR EPO', ");
		columnasGrid.append(" tooltip: 'TOTAL DOCTOS POR EPO', ");						
		columnasGrid.append(" width: 130, ");
		columnasGrid.append(" align: 'center', ");					
		columnasGrid.append(" dataIndex: 'TOTAL_DOCTOS_XEPO' ");
		columnasGrid.append(" },  ");
		
		columnasGrid.append("  {  ");
		columnasGrid.append(" header: 'TOTAL MONTOS POR EPO', ");
		columnasGrid.append(" tooltip: 'TOTAL MONTOS POR EPO', ");						
		columnasGrid.append(" width: 130, ");
		columnasGrid.append(" align: 'center', ");					
		columnasGrid.append(" dataIndex: 'TOTAL_MONTOS_XEPO' ");
		columnasGrid.append(" } ");
		
		columnasGrid.append("	] ");		
		
		
				
		columnasStore.append("  {  name: 'TOTAL_DOCTOS_XEPO',  mapping : 'TOTAL_DOCTOS_XEPO' }, ");
		columnasStore.append("  {  name: 'TOTAL_MONTOS_XEPO',  mapping : 'TOTAL_MONTOS_XEPO' } ");
		
		columnasStore.append("	] ");
	
		//esta parte es para los datos 
		columnasRecords.append("  [   ");
		for (int i = 0; i < numeroEpos; i++) {
			
			informacionPorEpo = (HashMap)estadisticas.get("informacionPorEpo"+i);			
			String nombreEPO= informacionPorEpo.get("nombreEpo").toString();
			columnasRecords.append(" { ");
			columnasRecords.append("NOMBRE_EPO"+":'"+nombreEPO+"',");
					
			for (int j = 0; j < numeroAnios; j++) {
				int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+j)).toString());
				HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+j));
				String  anio  = aniosOperacion.get("anio"+j).toString();
					
				for (int k = 0; k < mesesPorAnio; k++) {
				  String informa  =informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j)).toString();
				  String monto = informacionPorEpo.get("montopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":informacionPorEpo.get("montopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j)).toString();
				  
				   doctosPubPorEpo = doctosPubPorEpo.add(new BigDecimal(informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
					montoPubPorEpo = montoPubPorEpo.add(new BigDecimal(informacionPorEpo.get("montopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("montopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
					
					String  indiceMes  ="MES"+k+anio;
					String  indiceMonto  ="MONTO"+k+anio;	
										
					columnasRecords.append("'"+indiceMes+"'"+":'"+informa+"',");
					columnasRecords.append("'"+indiceMonto+"'"+":'"+Comunes.formatoDecimal(monto,2)+"',");					
					
				}
			}
		
			columnasRecords.append("TOTAL_DOCTOS_XEPO"+":'"+doctosPubPorEpo+"',");
			columnasRecords.append("TOTAL_MONTOS_XEPO"+":'"+montoPubPorEpo+"'},");
			
			String totalXepo =  doctosPubPorEpo.setScale(0, BigDecimal.ROUND_HALF_UP).toString();
			doctosPubPorEpo = new BigDecimal("0");		
		}			
		
		columnasRecords.deleteCharAt(columnasRecords.length()-1);
		columnasRecords.append(" ] ");
		
	}//if (numeroEpos > 0) {

	if(numeroEpos!=0) {
		consulta = "{\"success\": true, \"columnasGrid\": " + columnasGrid.toString() + 
				", \"columnasStore\": " + columnasStore.toString() +	
				", \"numeroEpos\": " + numeroEpos +				
				", \"num_electronico\": " + num_electronico +								
				", \"columnasRecords\": {\"registros\": " + columnasRecords.toString() +			
				", \"total\":"+numeroEpos+" }  }";
				
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("tituloG", tituloG);	
	infoRegresar = jsonObj.toString();	
	
	}else if(numeroEpos==0) {
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("numeroEpos", String.valueOf(numeroEpos));	
		infoRegresar = jsonObj.toString();	
	} 


} else if(informacion.equals("GenerarCSV")  ) {

	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = "";
	StringBuffer contenidoArchivo = new StringBuffer();

	//obtener los datos de la Pyme 
	List infoPyme = InstIrrevocable.datosPyme( txtRFC, num_electronico );
	String NoNafin = infoPyme.get(0).toString();
	String NOM_PYME = infoPyme.get(1).toString();
	String RFC = infoPyme.get(2).toString();
		
	//para obtener los registros de las estadisticas por mes
	List info  = InstIrrevocable.consEstadistica(ic_pyme , mesActual, anioActual, ultimoDiaMesActual );
	
	if(info.size()>0){
		informacionPorEpo = (HashMap)info.get(0);
		estadisticas = (HashMap)info.get(1);
		numeroEpos = Integer.parseInt(info.get(2).toString()); 
	}
	
	if (numeroEpos > 0) {
		contenidoArchivo.append("Número de Nafin Electrónico, Nombre de la Pyme, RFC,\n");
		contenidoArchivo.append(NoNafin  + ",");
		contenidoArchivo.append(NOM_PYME.replaceAll(",", "") + ",");
		contenidoArchivo.append(RFC + ",\n");
		contenidoArchivo.append("EPO,");
				
		for (int i = 0; i < numeroAnios; i++) {
			int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+i)).toString());
			HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+i));
			for (int j = 0; j < mesesPorAnio; j++) {
			contenidoArchivo.append("No. de Documentos " + meses.get("nombreMes"+j) + " " + aniosOperacion.get("anio"+i) + ",");	
			contenidoArchivo.append("Monto de Documentos " + meses.get("nombreMes"+j) + " " + aniosOperacion.get("anio"+i) + ",");		
			}
		}
		contenidoArchivo.append("TOTAL POR EPO,TOTAL MONTOS POR EPO,\n");
		
		for (int i = 0; i < numeroEpos; i++) {
			informacionPorEpo = (HashMap)estadisticas.get("informacionPorEpo"+i);			
			contenidoArchivo.append(informacionPorEpo.get("nombreEpo").toString().replaceAll(",", "") + ",");
			
			for (int j = 0; j < numeroAnios; j++) {
				int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+j)).toString());
				HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+j));
				String  anio  = aniosOperacion.get("anio"+j).toString();
					
				for (int k = 0; k < mesesPorAnio; k++) {
				  	doctosPubPorEpo = doctosPubPorEpo.add(new BigDecimal(informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
					montoPubPorEpo = montoPubPorEpo.add(new BigDecimal(informacionPorEpo.get("montopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("montopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
					
					contenidoArchivo.append((informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))) + ",");							
					contenidoArchivo.append((informacionPorEpo.get("montopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":informacionPorEpo.get("montopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))) + ",");
					
				}
			}			
			contenidoArchivo.append(doctosPubPorEpo.setScale(0, BigDecimal.ROUND_HALF_UP).toString() + ",");		
			contenidoArchivo.append(montoPubPorEpo.setScale(0, BigDecimal.ROUND_HALF_UP).toString() + ",");
		
			doctosPubTotal = doctosPubTotal.add(doctosPubPorEpo);
			montoPubTotal = montoPubTotal.add(montoPubPorEpo);
			doctosPubPorEpo = new BigDecimal("0");		
			montoPubPorEpo = new BigDecimal("0");	
			contenidoArchivo.append("\n");	
		}
		contenidoArchivo.append("TOTAL POR MES,");
		for (int j = 0; j < numeroAnios; j++) {
			HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+j));
			int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+j)).toString());
			for (int k = 0; k < mesesPorAnio; k++) {
				for (int i = 0; i < numeroEpos; i++) {
					informacionPorEpo = (HashMap)estadisticas.get("informacionPorEpo"+i);
					doctosPubPorMes = doctosPubPorMes.add(new BigDecimal(informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
					montoPubPorMes = montoPubPorMes.add(new BigDecimal(informacionPorEpo.get("montopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("montopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
					
				}
				contenidoArchivo.append(doctosPubPorMes.setScale(0, BigDecimal.ROUND_HALF_UP).toString() + ",");
				contenidoArchivo.append(montoPubPorMes.setScale(0, BigDecimal.ROUND_HALF_UP).toString() + ",");
					
				doctosPubPorMes = new BigDecimal("0");
				montoPubPorMes = new BigDecimal("0");
			}
		}
		contenidoArchivo.append(doctosPubTotal.setScale(0, BigDecimal.ROUND_HALF_UP).toString() + ",");
		contenidoArchivo.append(montoPubTotal.setScale(0, BigDecimal.ROUND_HALF_UP).toString() + ",");
			
		contenidoArchivo.append("\n");

	}

	archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv");
	nombreArchivo = archivo.getNombre();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();  		

}else  if(informacion.equals("obtenParametrosPymeIf")  ) {

	String claveIf = (request.getParameter("claveIf")!=null)?request.getParameter("claveIf"):"";
	String clavePyme = (request.getParameter("clavePyme")!=null)?request.getParameter("clavePyme"):"";
	
	Registros reg = obtenerParamPymeIf(clave_epo, claveIf, moneda, clavePyme);
	
	if(reg.next()){
		jsonObj.put("claveBanco",		reg.getString("clave_banco")==null?"":reg.getString("clave_banco")			);
		jsonObj.put("bancoServicio",	reg.getString("banco_servicio")==null?"":reg.getString("banco_servicio")	);
		jsonObj.put("cuentaBancaria",	reg.getString("cuenta_bancaria")==null?"":reg.getString("cuenta_bancaria")	);
	}
	
	jsonObj.put("success", new Boolean(true));

	infoRegresar = jsonObj.toString();

}



%>
<%=infoRegresar%>


<%!
	public String  obtieneIC_Pyme(String  rfc ){
		System.out.println("obtieneIC_Pyme  (E)");
		StringBuffer 	strSQL 	= new StringBuffer();
		PreparedStatement 	ps	= null;
		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		String  ic_pyme = "";

		try{
			con.conexionDB();
			//strSQL.append(" SELECT ic_pyme AS pyme FROM comcat_pyme WHERE cg_rfc =  ? ");
			
			 strSQL.append(" SELECT p.cg_razon_social AS pyme, "+
				"  p.cg_rfc AS rfc_pyme, "+
				 " na.ic_nafin_electronico AS nonafin "+
				"  FROM comcat_pyme p "+
				" , comrel_nafin na "+
				 " WHERE na.ic_epo_pyme_if = p.ic_pyme "+
				" AND na.cg_tipo = 'P' "+
				" and p.cg_rfc = ? ");
 
 

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1,rfc);
			rs = ps.executeQuery();
			if(rs.next()){
				ic_pyme = (rs.getString("nonafin")==null)?"":rs.getString("nonafin");
        }
			rs.close();
			ps.close();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("obtieneIC_Pyme (S)");
		}
			return ic_pyme;
	}
	
	private Registros obtenerParamPymeIf(String ic_epo, String ic_if, String ic_moneda, String ic_pyme){
		AccesoDB con	= new AccesoDB();
		List lVarBind	= new ArrayList();
		Registros reg	= new Registros();
		try{
			con.conexionDB();
			
			StringBuffer qrySentencia = new StringBuffer(
				"SELECT nvl(cb.cg_cuenta_clabe, cb.cg_numero_cuenta) as cuenta_bancaria, cb.cg_banco AS banco_servicio, i.ic_if AS clave_banco "+
				"  FROM comrel_pyme_if pi, comrel_cuenta_bancaria cb, comcat_if i"+
				" WHERE cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria"+
				"	AND pi.cs_borrado = ?	"+
				"	AND cb.cs_borrado = ?	"+
				"	AND pi.cs_vobo_if = ?	"+
				"	AND pi.cs_opera_descuento = ?	"+
				"	AND pi.ic_epo = ?	"+
				"	AND pi.ic_if = ?	"+
				"	AND cb.ic_moneda= ?	"+
				"	AND cb.ic_pyme = ?	"+
				"	AND cb.cg_banco = i.cg_razon_social (+)	"+
				"	AND rownum = ?	"
			);
			lVarBind.add("N");
			lVarBind.add("N");
			lVarBind.add("S");
			lVarBind.add("S");
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer(ic_if));
			lVarBind.add(new Integer(ic_moneda));
			lVarBind.add(new Integer(ic_pyme));
			lVarBind.add(new Integer(1));

			reg = con.consultarDB(qrySentencia.toString(),lVarBind);

		}catch(Exception e){
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();		
		}
		return reg;
	}


%>
