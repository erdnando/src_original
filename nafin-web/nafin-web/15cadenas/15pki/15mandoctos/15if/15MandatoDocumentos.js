Ext.onReady(function() {

	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});

	var procesarObtenParametrosPymeIf = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			var record = opts.params.record;
			if(Ext.isDefined(jsonObj.claveBanco) && Ext.isDefined(jsonObj.bancoServicio)){
				var claveConcatenada = jsonObj.claveBanco +"|"+jsonObj.bancoServicio;
				var dato = CatBancoServicio.findExact("clave", claveConcatenada);
				if (dato != -1 ) {
					record.set('BANCO_SERVICIO',	claveConcatenada);
				}else{
					var dato1 = CatBancoServicio.findExact("descripcion", jsonObj.bancoServicio);
					if (dato1 != -1 ) {
						var reg = CatBancoServicio.getAt(dato1);
						record.set('BANCO_SERVICIO',	reg.get('clave')+"|"+jsonObj.bancoServicio);
					}
				}
			}
			if(Ext.isDefined(jsonObj.cuentaBancaria)){
				record.set('CUENTA_BANCARIA',jsonObj.cuentaBancaria);
			}
			record.commit;
			Ext.getCmp('gridConsulta').getGridEl().unmask();
		}else{
			NE.util.mostrarConnError(response, opts);
		}
	}

	var procesarGenerarCSVEstadistica =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarCSVEsta');
		btnGenerar.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarCSVEsta');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessConsultaEstadistica =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonObj = Ext.util.JSON.decode(response.responseText)
			if(jsonObj.numeroEpos!='0'){
				consultaDataEstadisticas.fields =  jsonObj.columnasStore;
				consultaDataEstadisticas.data =  jsonObj.columnasRecords;
				gridEstadisticas.columns = jsonObj.columnasGrid;		
			}	
			
			var num_electronico = jsonObj.num_electronico;
			var tituloG = jsonObj.tituloG;
			var ventana = Ext.getCmp('ventana');		
			if(ventana){
				ventana.show();
			}else{
				new Ext.Window({
					layout: 'fit',
					modal: true,
					width: 800,
					height: 400,
					id: 'ventana',					
					closeAction: 'hide',
					autoDestroy:true,
					closable:false,
					title:tituloG,
					items: [	
						gridEstadisticas
					],
					bbar: {
						xtype: 'toolbar',	
						buttons: [
							'-',
							'->',
							{
								xtype: 'button', 	buttonAlign:'right', 	text: 'Cerrar',id: 'btnCerrarDe', 
								handler: function(){
									Ext.getCmp('gridEstadisticas').hide();
									Ext.getCmp('ventana').destroy();
								} 
							},
							'-',
							{
								xtype: 'button',
								text: 'Generar Archivo',
								iconCls:	'icoXls',
								id: 'btnGenerarCSVEsta',
								handler: function(boton, evento) {
									boton.disable();
									boton.setIconClass('loading-indicator');
									Ext.Ajax.request({
										url: '15MandatoDocumentos.data.jsp',
										params:{
											informacion: 'GenerarCSV',
											num_electronico:num_electronico
										},
										callback: procesarGenerarCSVEstadistica
									});
								}
							},
							{
								xtype: 'button',
								text: 'Bajar Archivo',
								id: 'btnBajarCSVEsta',
								hidden: true
							}
						]
					}
				}).show();
			}	
			var grid = Ext.getCmp('gridEstadisticas');
			var el = grid.getGridEl();	
			if(jsonObj.numeroEpos!='0'){
				
				Ext.getCmp('btnGenerarCSVEsta').enable();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnGenerarCSVEsta').disable();
			}
				
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}

	var VerEstadisticas = function(grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var num_electronico = registro.get('NAFIN_ELECTRONICO');
		var el = grid.getGridEl();	
							
		Ext.Ajax.request({
			url: '15MandatoDocumentos.data.jsp',
			params: {
				informacion: 'ConsultarEstadistica',
				num_electronico:num_electronico
			}
			,callback: procesarSuccessConsultaEstadistica
		});
		
	}
	
	var consultaDataEstadisticas = {
		xtype: 'jsonstore',
		id:'consultaDataEstadisticas',
		root: 'registros',
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		fields: null,
		data: null
	};

	var gridEstadisticas = {		
		xtype: 'grid',	
		id:'gridEstadisticas',	
		store:consultaDataEstadisticas,			
		columns: [],		
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		frame: true,
		height: 400,
		width: 900,	
		style: 'margin:0 auto;',
		title: ''			
	};

	 var procesarSuccessImpOperadosManXLS=  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnArchivoXLS');
		btnGenerar.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarXLS');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessImpOperadosManPDF =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnArchivoPDF');
		btnGenerar.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarPDF1');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// ****************************Ver Documentos Operados *****************
	
	var VerDoctosOperados = function(grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var folio_instruccion = registro.get('FOLIO_INSTRUCCION');
		var el = grid.getGridEl();	
		
		var ventana = Ext.getCmp('ventana');		
		if(ventana){
			ventana.setTitle(titulo);
			ventana.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 800,
				height: 400,
				id: 'ventana',					
				closeAction: 'hide',
				autoDestroy:true,
				closable:false,
				title:'Documentos Operados',
				items: [						
					gridOperados
				],
				bbar: {
					xtype: 'toolbar',	
					buttons: [
						'-',
						'->',
						{
							xtype: 'button', 	buttonAlign:'right', 	text: 'Cerrar',id: 'btnCerrarDe', 
							handler: function(){
								Ext.getCmp('gridOperados').hide();
								Ext.getCmp('ventana').destroy();
							} 
						},
						'-',
						{
							xtype: 'button',
							text: 'Generar Archivo',
							iconCls:	'icoXls',
							tooltip:	'Generar Archivo',
							id: 'btnArchivoXLS',	
							disabled: true,
							handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								var barraPaginacion = Ext.getCmp("barraPaginacion");
								Ext.Ajax.request({
									url: '15MandatoDocumentos.data.jsp',
									params:	{
										informacion:'ImpOperadosMan',										
										folio_instruccion: folio_instruccion,
										tipoArchivo:'XLS'										
									}					
									,callback: procesarSuccessImpOperadosManXLS
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar Archivo',
							tooltip:	'Bajar Archivo',
							id: 'btnBajarXLS',
							hidden: true
						},
						'-',
						{
							xtype: 'button',
							text: 'Generar PDF',
							disabled: true,
							iconCls:	'icoPdf',
							tooltip:	'Generar PDF',
							id: 'btnArchivoPDF',	
							handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								
								Ext.Ajax.request({
									url: '15MandatoDocumentos.data.jsp',
									params:	{
										informacion:'ImpOperadosManPDF',										
										folio_instruccion: folio_instruccion,
										tipoArchivo:'PDF'											
									}					
									,callback: procesarSuccessImpOperadosManPDF
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar Archivo',
							tooltip:	'Bajar Archivo',
							id: 'btnBajarPDF1',
							hidden: true
						}
					]
				}						
			}).show();
		}
		consultaOperaData.load({
			params:{
				folio_instruccion: folio_instruccion					
			}
		});
		
	}
	
	var procesarConsultaOperaData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridOperados = Ext.getCmp('gridOperados');
			var el = gridOperados.getGridEl();
			var store = consultaOperaData;
			 Ext.getCmp('btnBajarPDF1').hide();
			  Ext.getCmp('btnBajarXLS').hide();
			
			if(store.getTotalCount()>0){			
				Ext.getCmp('btnArchivoPDF').enable();
			   Ext.getCmp('btnArchivoXLS').enable();				
				el.unmask();
			}else {
				Ext.getCmp('btnArchivoPDF').disable();
			   Ext.getCmp('btnArchivoXLS').disable();		
				el.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}
		
	var consultaOperaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'ConsultaOperacion'
		},
		fields: [
			{name: 'ESTATUS'},
			{name: 'FECHA_CAMBIO'},	
			{name: 'NO_CLIENTE'},	
			{name: 'PROVEEDOR'},	
			{name: 'NO_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO'},
			{name: 'PORCE_DESCUENTO'},
			{name: 'RECUERSO_GARANTIA'},
			{name: 'MONTO_DESC'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'TASA'},
			{name: 'PLAZO'},
			{name: 'NO_PROVEEDOR'}				
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaOperaData,
			exception: {
				fn: function(proxy,type,action,optionRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
					procesarConsultaOperaData(null,null,null);
				}
			}
		}
	});
	
	var gridOperados = {
		xtype: 'grid',
		store: consultaOperaData,
		id: 'gridOperados',
		columns: [
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				align: 'left',
				width: 130
			},
			{
				header: 'Fecha de Cambio',
				tooltip: 'Fecha de Cambio',
				dataIndex: 'FECHA_CAMBIO',
				align: 'left',
				width: 130
			},
			{
				header: 'No. Cliente',
				tooltip: 'No. Cliente',
				dataIndex: 'NO_CLIENTE',
				align: 'left',
				width: 130
			},
			{
				header: 'Proveedor',
				tooltip: 'Proveedor',
				dataIndex: 'PROVEEDOR',
				align: 'left',
				width: 130
			},
			{
				header: 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex: 'NO_DOCUMENTO',
				align: 'center',
				width: 130
			},
			{
				header: 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex: 'FECHA_EMISION',
				align: 'center',
				width: 130
			},
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				align: 'center',
				width: 130
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				align: 'left',
				width: 130
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				align: 'left',
				width: 130
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				align: 'right',
				width: 130,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'PORCE_DESCUENTO',
				align: 'center',
				width: 130,
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recuerdo en Garantia',
				tooltip: 'Recuerdo en Garantia',
				dataIndex: 'RECUERSO_GARANTIA',
				align: 'left',
				width: 130
			},
			{
				header: 'Monto Descuento',
				tooltip: 'Monto Descuento',
				dataIndex: 'MONTO_DESC',
				align: 'right',
				width: 130,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Inter�s',
				tooltip: 'Monto Inter�s',
				dataIndex: 'MONTO_INTERES',
				align: 'right',
				width: 130,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPERAR',
				align: 'right',
				width: 130,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				align: 'left',
				width: 130
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				align: 'center',
				width: 130
			},
			{
				header: 'No. Proveedor',
				tooltip: 'No. Proveedor',
				dataIndex: 'NO_PROVEEDOR',
				align: 'center',
				width: 130
			}	
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 885,
		title: '',
		frame: true,
		bbar: {			
		}
	}

	// ****************************Desactivar  Descento Automatico  *****************
		function procesarSuccessDesactivar(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
				if(jsonData.respuesta =='OK') {		
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15						
						})
					});		
				}			
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Desactivar  Descento Automatico 
	var desactivarDescAuto = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var folio_instruccion = registro.get('FOLIO_INSTRUCCION');
		var el = grid.getGridEl();		
		
		Ext.MessageBox.confirm('Confirmaci�n','�Est� seguro de Desactivar el Descuento Autom�tico para la Instrucci�n Irrevocable? Esto afectar� a todas las EPO �s incluidas', function(resp){
			if(resp =='yes'){			
				Ext.Ajax.request({
					url : '15MandatoDocumentos.data.jsp',
					params : {
						informacion:"ActivarDesc",				
						folio_instruccion:folio_instruccion,
						csDescAut:'N'
					}
					,callback: procesarSuccessDesactivar
				});
			}
		});		
	}	
	
		// ****************************Activar Descuento Automatico *****************
	function procesarSuccessActivar(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
				if(jsonData.respuesta =='OK') {		
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15						
						})
					});		
				}			
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//Activar Descento Automatico 
	var activaDescAuto = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var folio_instruccion = registro.get('FOLIO_INSTRUCCION');		
		var el = grid.getGridEl();		
		
		Ext.MessageBox.confirm('Confirmaci�n','Est� seguro de Activar el Descuento Autom�tico para la Instrucci�n Irrevocable? Esto afectar� a todas las EPO �s incluidas', function(resp){
			if(resp =='yes'){					
				Ext.Ajax.request({
					url : '15MandatoDocumentos.data.jsp',
					params : {
						informacion:"ActivarDesc",				
						folio_instruccion:folio_instruccion,
						csDescAut:'S'
					}
					,callback: procesarSuccessActivar
				});
			}
		});		
	}	

	// ****************************procesar Cancelar *****************
	function procesarSuccessCancelar(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
				if(jsonData.respuesta =='OK') {		
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15						
						})
					});		
				}			
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// procesar Cancelar 
	var procesoCancelar = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var folio_instruccion = registro.get('FOLIO_INSTRUCCION');
		var  observaciones = registro.get('OBSERVACIONES');
		var el = grid.getGridEl();		
				
		Ext.Ajax.request({
			url : '15MandatoDocumentos.data.jsp',
			params : {
				informacion:"Cancelar",				
				folio_instruccion:folio_instruccion,
				observaciones:observaciones				
			}
			,callback: procesarSuccessCancelar
		});
	}	
	
	// ****************************procesar Ampliar MONTO *****************
	function procesarSuccessAmpliarMonto(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
			if(jsonData.respuesta =='OK') {				
					Ext.MessageBox.alert('Mensaje','La actualizaci�n se realiz� con �xito');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15						
						})
					});		
				}			
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	// procesar Rechazo 
	var procesoAmpliarMonto = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var folio_instruccion = registro.get('FOLIO_INSTRUCCION');
		var  observaciones = registro.get('OBSERVACIONES');
		var  monto_credito = registro.get('MONTO_CREDITO');
		var  nueva_fecha_vencimiento = registro.get('NUEVA_FECHA_VENCIMIENTO');
		var  anterior_fecha_vencimiento = registro.get('ANTERIOR_FECHA_VENCIMIENTO');
		var  ante_monto_credito = registro.get('ANTERIOR_MONTO_CREDITO');

		var el = grid.getGridEl();		
		
		/*if(nueva_fecha_vencimiento==''){
			Ext.MessageBox.alert('Mensaje','La ampliaci�n del monto se realiz� con �xito');
			return;
		}*/
		/*if(nueva_fecha_vencimiento!=''){
			if(anterior_fecha_vencimiento!=''){
				//2 si la segunda mayor que la primera
				var resultado = datecomp(Ext.util.Format.date(nueva_fecha_vencimiento ,'d/m/Y'), anterior_fecha_vencimiento	);
				if (resultado == 2){
					Ext.MessageBox.alert('Mensaje','La nueva Fecha no puede ser menor a la Fecha inicial:'+anterior_fecha_vencimiento);
					return;
				}
			}
		}

		if(monto_credito!=''){
			if(  (  parseFloat(monto_credito) < parseFloat(ante_monto_credito) ) ||  (  parseFloat(monto_credito) == parseFloat(ante_monto_credito) ))  {			
				Ext.MessageBox.alert('Mensaje','El nuevo Monto del Cr�dito debe ser mayor al actual.');
				return;
			}
		}*/

		if(	!Ext.isEmpty(nueva_fecha_vencimiento)	){
			if(		!Ext.isEmpty(anterior_fecha_vencimiento)	){
				if(	nueva_fecha_vencimiento != anterior_fecha_vencimiento	){
					if(	Ext.isEmpty(observaciones)	){
						Ext.MessageBox.alert('Mensaje','Se debe capurar las observaciones.');	
						return;
					}
				}
			}
		}
		
		if(	!Ext.isEmpty(monto_credito)	){
			if(		!Ext.isEmpty(ante_monto_credito)	){
				if(	monto_credito != ante_monto_credito	){
					if(	Ext.isEmpty(observaciones)	){
						Ext.MessageBox.alert('Mensaje','Se debe capurar las observaciones.');
						return;
					}
				}
			}
		}

		Ext.Ajax.request({
			url : '15MandatoDocumentos.data.jsp',
			params : {
				informacion:"AmpliarMonto",				
				folio_instruccion:folio_instruccion,
				observaciones:observaciones,
				monto_credito:monto_credito,
				nueva_fecha_vencimiento: Ext.util.Format.date(nueva_fecha_vencimiento,'d/m/Y')
			}
			,callback: procesarSuccessAmpliarMonto
		});
	}
	
	
	// ****************************procesar Rechazo *****************
	function procesarSuccessFailureRechazo(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
			if(jsonData.respuesta =='OK') {
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15						
						})
					});		
				}
			
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// procesar Rechazo 
	var procesoRechazar = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var folio_instruccion = registro.get('FOLIO_INSTRUCCION');
		var  observaciones = registro.get('OBSERVACIONES');
		var el = grid.getGridEl();		
		
		
		Ext.Ajax.request({
			url : '15MandatoDocumentos.data.jsp',
			params : {
				informacion:"GenerarRechazo",				
				folio_instruccion:folio_instruccion,
				observaciones:observaciones
			}
			,callback: procesarSuccessFailureRechazo
		});
	}
	// **************** Acuse ****************************
	
	// para generar el PDF de Acuse	
	var procesarGenerarPDFAcuse =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('ArchivoPDFAcuse');
		//btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarPDFAcuse');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarSuccessFailureAcuse(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
				
				if(jsonData.recibo !='') {
				
					var acuseCifras = [
						['N�mero de Acuse', jsonData.acuse],
						['Fecha de Carga ', jsonData.fechaCarga],
						['Hora de Carga', jsonData.horaCarga],
						['Usuario de Captura ', jsonData.usuario]
					];
					storeCifrasData.loadData(acuseCifras);									
					Ext.getCmp("acuse").setValue(jsonData.acuse);
					Ext.getCmp("recibo").setValue(jsonData.recibo);
					Ext.getCmp("hora_carga").setValue(jsonData.horaCarga);
			
					Ext.getCmp("mensajeA").setValue(jsonData.mensajeA);	
					Ext.getCmp("mensajeAcuse").show();					
					
					Ext.getCmp("gridPreAcuse1").show();
					Ext.getCmp("gridPreAcuse2").show();	
					Ext.getCmp("gridPreAcuse3").show();							
					Ext.getCmp("gridConsulta").hide();	
					Ext.getCmp("forma").hide();	
					Ext.getCmp("fpBotones").hide();	
					
					Ext.getCmp("btnGenerarPDFAcuse").show();						
					Ext.getCmp("gridCifrasControl").show();						
				
				} else {
					Ext.getCmp("mensajeAcuse").hide();					
					Ext.getCmp("mensajePreAcuse").hide();
					Ext.getCmp("gridPreAcuse1").hide();
					Ext.getCmp("gridPreAcuse2").hide();	
					Ext.getCmp("gridPreAcuse3").hide();						
					Ext.getCmp("btnGenerarPDFAcuse").hide();																	
				
				}
				Ext.getCmp("forma").hide();	
				Ext.getCmp("fpBotones").hide();	
				Ext.getCmp("mensajePreAcuse").hide();
				Ext.getCmp("gridConsulta").hide();				
				Ext.getCmp("mensajeAutent").setValue(jsonData.mensajeAutent);	
				Ext.getCmp("mensajeAutentificacion").show();	
				Ext.getCmp("btnSalir").show();
				Ext.getCmp("fpBotonesImp").show();
					
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	var confirmar = function(pkcs7, textoFirmar){

		var mensaje  = Ext.getCmp("mensaje").getValue();
				
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else  {
		
			var folio_instruccion = Ext.getCmp("folio_instruccion").getValue();			
			var monto_credito = 	Ext.getCmp("monto_credito").getValue();			
			var fecha_vencimiento = Ext.getCmp("fecha_vencimiento").getValue();			
			var cuenta_bancaria = Ext.getCmp("cuenta_bancaria").getValue();			
			var tipo_cuenta = Ext.getCmp("tipo_cuenta").getValue();			
			var numero_credito = Ext.getCmp("numero_credito").getValue();			
			var moneda = Ext.getCmp("moneda").getValue();			
			var observaciones = Ext.getCmp("observaciones").getValue();	
			var banco_servicio = Ext.getCmp("banco_servicio").getValue();	
			var moneda_descuento = Ext.getCmp("moneda_descuento").getValue();	
			
			Ext.Ajax.request({
				url : '15MandatoDocumentos.data.jsp',
				params : {
					informacion:"GenerarAcuse",
					textoFirmar:textoFirmar,	
					pkcs7:pkcs7,
					mensaje:mensaje,
					folio_instruccion:folio_instruccion,
					monto_credito:monto_credito,
					fecha_vencimiento:fecha_vencimiento,
					cuenta_bancaria:cuenta_bancaria,
					tipo_cuenta:tipo_cuenta,
					numero_credito:numero_credito,
					moneda:moneda,
					observaciones:observaciones,
					banco_servicio:banco_servicio,
					moneda_descuento:moneda_descuento
				}
				,callback: procesarSuccessFailureAcuse
			});
		
		}
		
	}
	// function apra el botn de continuar
	var procesarAcuse = function()	{
		
		var textoFirmar  = Ext.getCmp("texto_plano").getValue();
		
		NE.util.obtenerPKCS7(confirmar, textoFirmar );		
		
	}
	
	var mensajeAutentificacion= new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutentificacion',							
		width:	'400',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 	xtype: 'displayfield',  id: 'mensajeAutent', 	value: '' }			
		]
	});
	
	
	var mensajeAcuse = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAcuse',							
		width:	'900',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 	xtype: 'displayfield',  id: 'mensajeA', 	value: '' }			
		]
	});

	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 150,
		style: 'margin:0 auto;',		
		frame: true
	});
	// **************** PreAcuse ****************************
	
	function procesarSuccessFailurePreAcuse(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
				
				Ext.getCmp("mensaje").setValue(jsonData.mensaje);	
				Ext.getCmp("texto_plano").setValue(jsonData.texto_plano);	
				
				Ext.getCmp("folio_instruccion").setValue(jsonData.folio_instruccion);			
				Ext.getCmp("monto_credito").setValue(jsonData.monto_credito);			
				Ext.getCmp("fecha_vencimiento").setValue(jsonData.fecha_vencimiento);			
				Ext.getCmp("cuenta_bancaria").setValue(jsonData.cuenta_bancaria);	
				Ext.getCmp("tipo_cuenta").setValue(jsonData.tipo_cuenta);	
				Ext.getCmp("numero_credito").setValue(jsonData.numero_credito);			
				Ext.getCmp("moneda").setValue(jsonData.moneda);
				Ext.getCmp("observaciones").setValue(jsonData.observaciones);
				Ext.getCmp("banco_servicio").setValue(jsonData.banco_servicio);
				Ext.getCmp("moneda_descuento").setValue(jsonData.moneda_descuento);
				
				Ext.getCmp("mensajePreAcuse").show();
				Ext.getCmp("gridPreAcuse1").show();
				Ext.getCmp("gridPreAcuse2").show();	
				Ext.getCmp("gridPreAcuse3").show();							
				Ext.getCmp("gridConsulta").hide();	
				Ext.getCmp("forma").hide();					
				Ext.getCmp("fpBotones").show();	
								
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesoAceptar = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var no_folio = registro.get('FOLIO_INSTRUCCION');
		var el = grid.getGridEl();		
		
		var folio_instruccion;
		var nafin_electronico;
		var nombre_pyme;
		var nombre_epo;
		var nombre_if;
		var nombre_descontante;
		var fecha_solicitud;
		var fecha_aceptacion_if;
		var monto_credito;
		var fecha_vencimiento;
		var banco_servicio;
		var cuenta_bancaria;
		var tipo_cuenta;
		var numero_credito;
		var moneda;	
		var moneda_descuento;
		var observaciones;
		var estatus_solicitud;
		var fechaActual;
		var texto= 'Folio|N@E PyME|PyME|EPO|IF que tramita el cr�dito|Descontante|Fecha de Solicitud|Fecha de Aceptaci�n|Monto del Cr�dito|Fecha de Vencimiento del Cr�dito|Banco de Servicio|Cuenta Bancaria|Tipo de Cuenta|N�mero de Cr�dito|Moneda de Cr�dito|Moneda para Descuento|Observaciones|Estatus \n';
		
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();		
		store.each(function(record) {
		folio_instruccion = record.data['FOLIO_INSTRUCCION'];		
		if(no_folio ==folio_instruccion) {	
		texto += record.get('FOLIO_INSTRUCCION')+'|'+record.get('NAFIN_ELECTRONICO')+'|'+record.get('NOMBRE_PYME')+'|'+record.get('NOMBRE_EPO')+'|'+record.get('NOMBRE_IF')+'|'+
					record.get('NOMBRE_DESCONTANTE')+'|'+record.get('FECHA_SOLICITUD')+'|'+record.get('FECHA_ACEPTACION_IF')+'|'+record.get('MONTO_CREDITO')+'|'+record.get('FECHA_VENCIMIENTO')+'|'+
					record.get('BANCO_SERVICIO')+'|'+record.get('CUENTA_BANCARIA')+'|'+record.get('TIPO_CUENTA')+'|'+record.get('NUMERO_CREDITO')+'|'+record.get('MONEDA')+'|'+record.get('MONEDA_DESCUENTO')+'|'+
					record.get('OBSERVACIONES')+'|'+record.get('ESTATUS_SOLICITUD')+'\n';
				
				folio_instruccion = record.data['FOLIO_INSTRUCCION'];	
				nafin_electronico = record.data['NAFIN_ELECTRONICO'];
				nombre_pyme = record.data['NOMBRE_PYME'];
				nombre_epo = record.data['NOMBRE_EPO'];
				nombre_if = record.data['NOMBRE_IF'];
				nombre_descontante= record.data['NOMBRE_DESCONTANTE'];
				fecha_solicitud = record.data['FECHA_SOLICITUD'];				
				fecha_aceptacion_if = record.data['FECHA_ACEPTACION_IF'];
				monto_credito = record.data['MONTO_CREDITO'];
				fecha_vencimiento = record.data['FECHA_VENCIMIENTO'];
				banco_servicio = record.data['BANCO_SERVICIO'];
				cuenta_bancaria = record.data['CUENTA_BANCARIA'];
				tipo_cuenta = record.data['TIPO_CUENTA'];
				numero_credito = record.data['NUMERO_CREDITO'];	
				
				moneda = record.data['MONEDA'];		
				moneda_descuento = record.data['MONEDA_DESCUENTO'];		
				observaciones = record.data['OBSERVACIONES'];
				estatus_solicitud = record.data['ESTATUS_SOLICITUD'];	
				fechaActual = record.data['FECHA_ACTUAL'];	
			}		
		});
		
		if(monto_credito ==''){
			Ext.MessageBox.alert('Mensaje','El valor de el Monto del Cr�dito es requerido');
			return;
		}
		if(fecha_vencimiento ==''){
			Ext.MessageBox.alert('Mensaje','El valor de la Fecha de Vencimiento es requerido');
			return;
		}		
		if(fecha_vencimiento !=''){		
			var resultado = datecomp(Ext.util.Format.date(fecha_vencimiento ,'d/m/Y'),fechaActual	);
			if (resultado == 2){
				Ext.MessageBox.alert('Mensaje','El valor de la Fecha de Vencimiento debe ser mayor que la Fecha Actual.');
				return;
			}
		}
		
		if(cuenta_bancaria ==''){
			Ext.MessageBox.alert('Mensaje','El valor de la Cuenta Bancaria es requerido.');
			return;
		}
		if(tipo_cuenta ==''){
			Ext.MessageBox.alert('Mensaje','El valor de el Tipo de Cuenta es requerido.');
			return;
		}
		if(moneda ==''){
			Ext.MessageBox.alert('Mensaje','El valor de la Moneda es requerido.');
			return;
		}
		
		if(moneda_descuento !='0' &&  moneda_descuento !='1' && moneda_descuento !='54' ){
			Ext.MessageBox.alert('Mensaje','El valor de la Moneda para Descuento es requerido.');
			return;
		}

		Ext.Ajax.request({
			url : '15MandatoDocumentos.data.jsp',
			params : {
				informacion:"PreAcuse",
				texto:texto,
				folio_instruccion:no_folio,
				monto_credito:monto_credito,
				fecha_vencimiento:Ext.util.Format.date(fecha_vencimiento ,'d/m/Y') ,
				cuenta_bancaria:cuenta_bancaria,
				tipo_cuenta:tipo_cuenta,
				numero_credito:numero_credito,
				moneda:moneda,
				observaciones:observaciones,
				banco_servicio:banco_servicio,
				moneda_descuento:moneda_descuento				
			}
			,callback: procesarSuccessFailurePreAcuse
		});

		consultaPreAcuseData1.load({
			params: {				
				nafin_electronico:nafin_electronico,
				nombre_pyme:nombre_pyme,
				nombre_epo:nombre_epo,
				nombre_if:nombre_if,
				nombre_descontante:nombre_descontante,
				fecha_solicitud:fecha_solicitud,				
				folio_instruccion:folio_instruccion				
			}
		});

		consultaPreAcuseData2.load({
			params: {
				 fecha_aceptacion_if:fecha_aceptacion_if,
				 monto_credito:monto_credito,
				 fecha_vencimiento:Ext.util.Format.date(fecha_vencimiento ,'d/m/Y') ,
				 banco_servicio:banco_servicio,
				 cuenta_bancaria:cuenta_bancaria,
				 tipo_cuenta:tipo_cuenta,
				 numero_credito:numero_credito
			}
		});

		consultaPreAcuseData3.load({
			params: {
				 moneda:moneda,
				 moneda_descuento:moneda_descuento,
				 observaciones:observaciones,
				 estatus_solicitud:estatus_solicitud				 
			}
		});
	}

	var consultaPreAcuseData1   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreacuse1'			
		},		
		fields: [			
			{name: 'FOLIO_INSTRUCCION'},			
			{name: 'NAFIN_ELECTRONICO'},
			{name: 'NOMBRE_PYME'},
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_DESCONTANTE'},
			{name: 'FECHA_SOLICITUD'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);				
				}
			}
		}		
	});
	
	var gridPreAcuse1 = new Ext.grid.GridPanel({
		store: consultaPreAcuseData1,
		id: 'gridPreAcuse1',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '',		
		columns: [	
			{
				header: 'FOLIO',
				tooltip: 'FOLIO',
				dataIndex: 'FOLIO_INSTRUCCION',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'N@E PyME',
				tooltip: 'N@E PyME',
				dataIndex: 'NAFIN_ELECTRONICO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'PyME',
				tooltip: 'PyME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'IF que tramita el cr�dito',
				tooltip: 'IF que tramita el cr�dito',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Descontante',
				tooltip: 'Descontante',
				dataIndex: 'NOMBRE_DESCONTANTE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Fecha de Solicitud',
				tooltip: 'Fecha de Solicitud',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			}	
		],
		stripeRows: true,
		loadMask: true,
		height: 100,
		width: 900,		
		frame: true
	});

	var consultaPreAcuseData2   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreacuse2'			
		},		
		fields: [			
			{name: 'FECHA_ACEPTACION_IF'},
			{name: 'MONTO_CREDITO'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'BANCO_SERVICIO'},
			{name: 'CUENTA_BANCARIA'},
			{name: 'TIPO_CUENTA'},
			{name: 'NUMERO_CREDITO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);				
				}
			}
		}		
	});
	
	var gridPreAcuse2 = new Ext.grid.GridPanel({
		store: consultaPreAcuseData2,
		id: 'gridPreAcuse2',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '',		
		columns: [	
			{
				header: 'Fecha de Aceptaci�n',
				tooltip: 'Fecha de Aceptaci�n',
				dataIndex: 'FECHA_ACEPTACION_IF',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Monto del Cr�dito',
				header: 'Monto del Cr�dito',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Vencimiento del Cr�dito',
				tooltip: 'Fecha de Vencimiento del Cr�dito',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: ' Banco de Servicio',
				tooltip: ' Banco de Servicio',
				dataIndex: 'BANCO_SERVICIO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Cuenta Bancaria ',
				tooltip: 'Cuenta Bancaria ',
				dataIndex: 'CUENTA_BANCARIA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Tipo de Cuenta',
				tooltip: 'Tipo de Cuenta',
				dataIndex: 'TIPO_CUENTA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'N�mero de Cr�dito ',
				tooltip: 'N�mero de Cr�dito ',
				dataIndex: 'NUMERO_CREDITO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			}	
		],
		stripeRows: true,
		loadMask: true,
		height: 100,
		width: 900,		
		frame: true
	});

	var consultaPreAcuseData3   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreacuse3'			
		},		
		fields: [			
			{name: 'MONEDA'},
			{name: 'MONEDA_DESCUENTO'},
			{name: 'OBSERVACIONES'},
			{name: 'ESTATUS_SOLICITUD'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);				
				}
			}
		}		
	});

	var gridPreAcuse3 = new Ext.grid.GridPanel({
		store: consultaPreAcuseData3,
		id: 'gridPreAcuse3',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '',		
		columns: [	
			{
				header: 'Moneda de Cr�dito',
				tooltip: 'Moneda de Cr�dito',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 200,				
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Moneda para Descuento ',
				header: 'Moneda para Descuento ',
				dataIndex: 'MONEDA_DESCUENTO',
				sortable: true,
				width: 200,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Observaciones',
				tooltip: 'Observaciones',
				dataIndex: 'OBSERVACIONES',
				sortable: true,
				width: 200,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS_SOLICITUD',
				sortable: true,
				width: 200,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: '',
				tooltip: '',				
				sortable: true,
				width: 110,				
				resizable: true,				
				align: 'center'	
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 100,
		width: 900,		
		frame: true
	});

	var mensajePreAcuse = new Ext.Container({
		layout: 'table',		
		id: 'mensajePreAcuse',							
		width:	'930',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 	xtype: 'displayfield',  id: 'mensaje', 	value: '' },
			{ 	xtype: 'textarea',  id: 'texto_plano', hidden: true, 	value: '' },
			{ 	xtype: 'textfield',  id: 'folio_instruccion',  hidden: true,	value: '' },
			{ 	xtype: 'textfield',  id: 'monto_credito', hidden: true, 	value: '' },
			{ 	xtype: 'textfield',  id: 'fecha_vencimiento',hidden: true,  	value: '' },
			{ 	xtype: 'textfield',  id: 'banco_servicio', hidden: true,	value: '' },
			{ 	xtype: 'textfield',  id: 'cuenta_bancaria',  hidden: true,	value: '' },
			{ 	xtype: 'textfield',  id: 'tipo_cuenta', hidden: true, 	value: '' },
			{ 	xtype: 'textfield',  id: 'numero_credito', hidden: true, 	value: '' },
			{ 	xtype: 'textfield',  id: 'moneda', hidden: true,	value: '' },
			{ 	xtype: 'textfield',  id: 'observaciones', hidden: true, 	value: '' },			
			{ 	xtype: 'textfield',  id: 'moneda_descuento', hidden: true, 	value: '' },
			{ 	xtype: 'textfield',  id: 'acuse', hidden: true, 	value: '' },
			{ 	xtype: 'textfield',  id: 'recibo',  hidden: true,	value: '' },
			{ 	xtype: 'textfield',  id: 'hora_carga', hidden: true, 	value: '' }
		]
	});

		//Contenedor de Botones de Impresion
	var fpBotones = new Ext.Container({
		layout: 'table',		
		style: 'margin:0 auto;',
		id: 'fpBotones',	
		hidden: true,	
		width:	'300',
		heigth:	'auto',		
		items: [
			{
				xtype:	'button',
				text:		'Cancelar',
				tooltip:	'Cancelar',					
				iconCls:	'icoLimpiar',
				id:		'btnCancelar',
				width:	'100',
				handler: function(boton, evento) {
					window.location = '15MandatoDocumentosext.jsp';	
				}
			},{
				xtype:	'box',
				width:	'10'
			},{
				xtype:	'button',
				text:		'Continuar',					
				tooltip:	'Continuar',
				id:		'btnContinuar',
				iconCls:	'icoAceptar',
				width:	'100',
				handler:procesarAcuse
			}		
		]
	});

 //Contenedor de Botones de Impresion
	var fpBotonesImp = new Ext.Container({
		layout: 'table',		
		style: 'margin:0 auto;',
		id: 'fpBotonesImp',	
		hidden: true,	
		width:	'300',
		heigth:	'auto',		
		items: [
			{
				xtype: 'button',
				text: 'Generar PDF',
				iconCls:	'icoPdf',
				id: 'btnGenerarPDFAcuse',
				hidden: true,
				handler: function(boton, evento) {
					
					var  acuse  = Ext.getCmp("acuse").getValue();
					var  recibo  = Ext.getCmp("recibo").getValue();
					var  hora_carga  = Ext.getCmp("hora_carga").getValue();
					var  folio_instruccion  = Ext.getCmp("folio_instruccion").getValue();				
					
					Ext.Ajax.request({
						url: '15MandatoDocumentos.data.jsp',
						params: {
							informacion: 'ArchivoPDFAcuse',
							folio_instruccion:folio_instruccion,
							acuse:acuse,
							recibo:recibo, 
							hora_carga:hora_carga							
						},
						callback: procesarGenerarPDFAcuse
					});
				}
			},			
			{
				xtype: 'button',
				text: 'Bajar PDF',
				hidden: true,
				id: 'btnBajarPDFAcuse'					
			},
			{
				xtype: 'button',
				text: 'Salir',
				hidden: true,
				tooltip:	'Salir',
				iconCls: 'icoLimpiar',
				id: 'btnSalir',			
				handler: function(boton, evento) {
					window.location = '15MandatoDocumentosext.jsp';	
				}
			}
		]
	});

	// **************** para mostrar el acuse  de la Pyme
	function procesarMuestraAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var VerAcuse = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var no_folio = registro.get('FOLIO_INSTRUCCION');
		var el = grid.getGridEl();	
	
		
		Ext.Ajax.request({
			url: '15MandatoDocumentos.data.jsp',
			params: Ext.apply({
				informacion: "AcusePyme",	
				folio_instruccion: no_folio
			}),
			callback: procesarMuestraAcuse
		});
	}

	// ************Para generar los combos del Grid 
	var CatMonedaDescuento = new Ext.data.JsonStore({
		id: 'CatMonedaDescuento',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'CatMonedaDescuento'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var CatMoneda = new Ext.data.JsonStore({
		id: 'CatMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'CatMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var CatBancoServicio = new Ext.data.JsonStore({
		id: 'CatBancoServicio',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'CatBancoServicio'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var CatTipoCuenta = new Ext.data.JsonStore({
		id: 'CatTipoCuenta',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'CatTipoCuenta'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	// para el combo de la columna  Moneda para Descuento
	var comboMonedaDesc = new Ext.form.ComboBox({  
		triggerAction : 'all',  
		displayField : 'descripcion',  
		valueField : 'clave', 
		typeAhead: true,
		width: 250,
		minChars : 1,
		allowBlank: true,
		 forceSelection: true,
		store :CatMonedaDescuento 
	});

	Ext.util.Format.comboRendererMonDes = function(comboMonedaDesc){	
		return function(value,metadata,registro,rowIndex,colIndex,store){			
			if(registro.get('CLAVE_ESTATUS')==1){
				var record = comboMonedaDesc.findRecord(comboMonedaDesc.valueField, value);
				value = record ? record.get(comboMonedaDesc.displayField) : comboMonedaDesc.valueNotFoundText;
				return NE.util.colorCampoEdit(value,metadata,registro);
			} else {
				return  value;			
			}
		}
	}

	var comboMoneda = new Ext.form.ComboBox({  
		triggerAction : 'all',  
		displayField : 'descripcion',  
		valueField : 'clave', 
		typeAhead: true,
		width: 250,
		minChars : 1,
		allowBlank: true,
		store :CatMoneda 
	});

	Ext.util.Format.comboRendererMon = function(comboMoneda){
		return function(value,metadata,registro,rowIndex,colIndex,store){
			if(registro.get('CLAVE_ESTATUS')==1 ){
				var record = comboMoneda.findRecord(comboMoneda.valueField, value);
				value = record ? record.get(comboMoneda.displayField) : comboMoneda.valueNotFoundText;
				return NE.util.colorCampoEdit(value,metadata,registro);
			}else {
				return value;
			}
		}
	}

		//para la creacion del combo en el grid
	var comboTipoCuenta = new Ext.form.ComboBox({  
		triggerAction:		'all',
		forceSelection:	true,
		displayField:		'descripcion',  
		valueField:			'clave', 
		typeAhead:			true,
		width:				250,
		minChars:			1,
		allowBlank:			true,
		mode:					'local',
		store:				CatTipoCuenta
	});

	Ext.util.Format.comboRendererCue = function(comboTipoCuenta){
		return function(value,metadata,registro,rowIndex,colIndex,store){
			var record = comboTipoCuenta.findRecord(comboTipoCuenta.valueField, value);
			value = record ? record.get(comboTipoCuenta.displayField) : "";
			if(registro.get('CLAVE_ESTATUS')==1 ){
				value = record ? record.get(comboTipoCuenta.displayField) : comboTipoCuenta.valueNotFoundText;
				return NE.util.colorCampoEdit(value,metadata,registro);
			}else{
				return value;
			}
		}
	}

	//para la creacion del combo en el grid
	var comboBancoServicio = new Ext.form.ComboBox({
		triggerAction : 'all',
		mode:		'local',
		displayField : 'descripcion',  
		valueField : 'clave', 
		typeAhead: true,
		width: 250,
		minChars : 1,
		allowBlank: true,
		store :CatBancoServicio 
	});

	Ext.util.Format.comboRendererBan = function(comboBancoServicio){
		return function(value,metadata,registro,rowIndex,colIndex,store){
			var record = comboBancoServicio.findRecord(comboBancoServicio.valueField, value);
			value = record ? record.get(comboBancoServicio.displayField) : "";
			if(registro.get('CLAVE_ESTATUS')==1){
				value = record ? record.get(comboBancoServicio.displayField) : comboBancoServicio.valueNotFoundText;
				return NE.util.colorCampoEdit(value,metadata,registro);
			}else{
				return value;
			}
		} 
	}

	var observaciones = new Ext.form.TextField({
		name: 'observaciones',
		id:'txtobservaciones',
		fieldLabel: '',
		allowBlank: true,
		maxLength: 100,
		width: 80,
		msgTarget: 'side',
		margins: '0 20 0 0'//Necesario para mostrar el icono de error
	});

	// para generar el CSV de la consulta 	
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarCSV');
		btnGenerar.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarCSV');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

		// para generar el PDF de la consulta 	
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarPDF');
		btnGenerar.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarPDF');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	// Generaci�n de Consulta de Grid **************************************************	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;				
			
			if(jsonData.HABILITACAMPOS=='S'){
				gridConsulta.setTitle('Al definir el tipo de moneda en Monto de Cr�dito y Descuento faculta a la Pyme para seleccionar el IF de descuento en caso de que la operaci�n se encuentre fuera de par�metros definidos, fuera de la Instrucci�n Irrevocable suscrita.');
			}
			
			 Ext.getCmp('btnBajarPDF').hide();
			  Ext.getCmp('btnBajarCSV').hide();
			  
			if(store.getTotalCount() > 0) {	
				Ext.getCmp('btnGenerarPDF').enable();
			   Ext.getCmp('btnGenerarCSV').enable();			
				el.unmask();					
			} else {	
				Ext.getCmp('btnGenerarPDF').disable();
			   Ext.getCmp('btnGenerarCSV').disable();		
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'Consultar',
			operacion: 'Generar'
		},		
		fields: [
			{name: 'FOLIO_INSTRUCCION'},			
			{name: 'NAFIN_ELECTRONICO'},
			{name: 'IC_PYME'},
			{name: 'NOMBRE_PYME'},
			{name: 'NOMBRE_EPO'},
			{name: 'IC_EPO'},
			{name: 'IC_IF'},
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_DESCONTANTE'},
			{name: 'FECHA_SOLICITUD'},
			{name: 'FECHA_ACEPTACION_IF'},
			{name: 'MONTO_CREDITO'},
			{name: 'ANTERIOR_MONTO_CREDITO'},			
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'CLAVE_BANCO_SERVICIO'},
			{name: 'BANCO_SERVICIO'},
			{name: 'CUENTA_BANCARIA'},
			{name: 'TIPO_CUENTA'},
			{name: 'NUMERO_CREDITO'},
			{name: 'MONEDA'},
			{name: 'MONEDA_DESCUENTO'},
			{name: 'OBSERVACIONES'},
			{name: 'NUEVA_FECHA_VENCIMIENTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'ANTERIOR_FECHA_VENCIMIENTO', type: 'date', dateFormat: 'd/m/Y'},			
			{name: 'FECHA_MODIFICACION'},
			{name: 'ESTATUS_SOLICITUD'},
			{name: 'DESCAUT'},
			{name: 'OBSERVACIONES'},
			{name: 'CLAVE_ESTATUS'},			
			{name: 'HABILITACAMPOS'},
			{name: 'FECHA_ACTUAL'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{
				fn: function(store, options){
					Ext.apply(options.params, {
						num_electronico:	Ext.getCmp('num_electronico1').getValue(),
						claveTipoCadena:	Ext.getCmp('claveTipoCadena1').getValue(),
						clave_epo:	Ext.getCmp('clave_epo1').getValue(),
						clave_estatus:	Ext.getCmp('clave_estatus1').getValue(),
						fecha_solicitud_ini:	Ext.util.Format.date(Ext.getCmp('fecha_solicitud_ini').getValue(),'d/m/Y'),
						fecha_solicitud_fin:	Ext.util.Format.date(Ext.getCmp('fecha_solicitud_fin').getValue(),'d/m/Y'),
						fecha_aceptacion_ini:	Ext.util.Format.date(Ext.getCmp('fecha_aceptacion_ini').getValue(),'d/m/Y'),
						fecha_aceptacion_fin:	Ext.util.Format.date(Ext.getCmp('fecha_aceptacion_fin').getValue(),'d/m/Y')
					});
				}		
			},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		hidden: true,
		title:'Instrucci�n Irrevocable',
		store: consultaData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		columns: [		
			{
				header: 'FOLIO',
				tooltip: 'FOLIO',
				dataIndex: 'FOLIO_INSTRUCCION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'N@E PyME',
				tooltip: 'N@E PyME',
				dataIndex: 'NAFIN_ELECTRONICO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'PyME',
				tooltip: 'PyME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'left'
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'IF que tramita el cr�dito',
				tooltip: 'IF que tramita el cr�dito',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Descontante',
				tooltip: 'Descontante',
				dataIndex: 'NOMBRE_DESCONTANTE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Fecha de Solicitud',
				tooltip: 'Fecha de Solicitud',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Aceptaci�n',
				tooltip: 'Fecha de Aceptaci�n',
				dataIndex: 'FECHA_ACEPTACION_IF',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},			
			{	
				header : 'Monto del Cr�dito',
				tooltip: 'Monto del Cr�dito',
				dataIndex : 'MONTO_CREDITO',
				width : 150,
				sortable : false,				
				align: 'center',					
				editor: {
					xtype : 'numberfield',
					maxValue : 9999999999999999.99,
					allowBlank: true
				},
				renderer:function(value,metadata,registro){
					if( ( registro.data['CLAVE_ESTATUS']==1 ||  registro.data['CLAVE_ESTATUS']==2  ) && registro.data['HABILITACAMPOS'] =='S'  ){			
						return NE.util.colorCampoEdit(Ext.util.Format.number(value,'$0,0.00'),metadata,registro);	
					}else if(registro.data['CLAVE_ESTATUS']==1  && registro.data['HABILITACAMPOS'] =='N'  ){	
						return 'NA';	
					}else if(registro.data['CLAVE_ESTATUS']==2  && registro.data['HABILITACAMPOS'] =='N'  ){
						return Ext.util.Format.number(value,'$0,0.00');	
					}else if(registro.data['CLAVE_ESTATUS']==3  || registro.data['CLAVE_ESTATUS'] ==5  ){
						return Ext.util.Format.number(value,'$0,0.00');						
					}
				}
			},		
			{				
				header: 'Fecha de Vencimiento del Cr�dito',
				tooltip:'Fecha de Vencimiento del Cr�dito',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: campoFechas,				
				renderer:function(value,metadata,registro){
					if(registro.data['CLAVE_ESTATUS']==1  && registro.data['HABILITACAMPOS'] =='S'  ){	
						return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);						
					}else if(registro.data['CLAVE_ESTATUS']==1  && registro.data['HABILITACAMPOS'] =='N'  ){	
						return 'NA';
					}else {
						return value;	
					}
				}	
			},
			{
				header: 'Moneda de Cr�dito',
				tooltip: 'Moneda de Cr�dito',
				dataIndex: 'MONEDA',
				width : 300,
				align: 'center',
				sortable : true,
				resizable: true,
				editor:comboMoneda,
				renderer: Ext.util.Format.comboRendererMon(comboMoneda)
			},
			{							
				header: 'Tipo de Cuenta',
				tooltip: 'Tipo de Cuenta',
				dataIndex: 'TIPO_CUENTA',
				width : 200,
				align: 'center',
				sortable : true,
				resizable: true,
				editor:comboTipoCuenta,	
				renderer: Ext.util.Format.comboRendererCue(comboTipoCuenta)	
			},
			{							
				header : 'Banco de Servicio',
				tooltip: 'Banco de Servicio',
				dataIndex : 'BANCO_SERVICIO',
				width : 300,
				align: 'center',
				sortable : true,
				resizable: true,
				editor:comboBancoServicio,	
				renderer: Ext.util.Format.comboRendererBan(comboBancoServicio)	
			},			
			{
				header: 'Cuenta Bancaria',
				tooltip: 'Cuenta Bancaria',
				dataIndex: 'CUENTA_BANCARIA',
				width : 150,
				sortable : false,				
				align: 'center',					
				editor: {
					xtype : 'textfield',					
					maxLength: 18,
					allowBlank: true			
				},
				renderer:function(value,metadata,registro){
					if(registro.data['CLAVE_ESTATUS']==1  && registro.data['HABILITACAMPOS'] =='S'  ){	
						return NE.util.colorCampoEdit(value,metadata,registro);		
					}else if(registro.data['CLAVE_ESTATUS']==1  && registro.data['HABILITACAMPOS'] =='N'  ){	
						return 'NA';
					}else {
						return value;	
					}
				}
			},
			{
				header: 'N�mero de Cr�dito',
				tooltip: 'N�mero de Cr�dito',
				dataIndex: 'NUMERO_CREDITO',
				width : 150,
				sortable : false,				
				align: 'center',					
				editor: {
					xtype : 'numberfield',
					maxValue : 99999999999,
					allowBlank: true
				},
				renderer:function(value,metadata,registro){
					if(registro.data['CLAVE_ESTATUS']==1 && registro.data['HABILITACAMPOS'] =='S'  ){						
						return NE.util.colorCampoEdit(value,metadata,registro);	
					}else if(registro.data['CLAVE_ESTATUS']==1  && registro.data['HABILITACAMPOS'] =='N'  ){	
						return 'NA';
					}	else {						
						return value;	
					}
				}
			},
			{							
				header: 'Moneda para Descuento',
				tooltip: 'Moneda para Descuento',
				dataIndex: 'MONEDA_DESCUENTO',
				width : 300,
				align: 'center',
				sortable : true,
				resizable: true,
				editor:comboMonedaDesc,	
				renderer: Ext.util.Format.comboRendererMonDes(comboMonedaDesc)	
			},		
			
			{
				header: 'Observaciones', 
				tooltip: 'Observaciones',
				dataIndex: 'OBSERVACIONES',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left',				
				editor: 	{ 
					xtype:'textarea', id:'obsevacion',enableKeyEvents: true,
					listeners:{
						keyup:	function(txtA){
							if (	!Ext.isEmpty(txtA.getValue())	){
								var numero = (txtA.getValue().length);
								if (numero > 250){
									var cadena = (txtA.getValue()).substring(0,250);
									txtA.setValue(cadena);
									Ext.Msg.alert('Comentarios','No puedes ingresar m�s de 250 caracteres.');
									txtA.focus();
									return;
								}
							}
						}
					}
				},
				renderer:function(value,metadata,registro){
					if( (registro.data['CLAVE_ESTATUS']==1 ||  registro.data['CLAVE_ESTATUS']==2)  && registro.data['HABILITACAMPOS'] =='S'  ){		
						return NE.util.colorCampoEdit(value,metadata,registro);
					}else if(registro.data['CLAVE_ESTATUS']==1  && registro.data['HABILITACAMPOS'] =='N'  ){	
						return 'NA';
					}else {
						return value;	
					}
				}				
			},
			{
				header: 'Nueva Fecha de Vencimiento del Cr�dito',
				tooltip: 'Nueva Fecha de Vencimiento del Cr�dito',
				dataIndex: 'NUEVA_FECHA_VENCIMIENTO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center',
				editor: campoFechas,	
				renderer:function(value,metadata,registro){
					var valor =value
					if(value=='' ){	valor ='N/A'  }
					if(registro.data['CLAVE_ESTATUS']==2  && registro.data['HABILITACAMPOS'] =='S'  ){							
						return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);																				
					}else {
						return valor
					}	
				}
			},
			{
				header: 'Fecha de Modificaci�n de Monto',
				tooltip: 'Fecha de Modificaci�n de Monto',
				dataIndex: 'FECHA_MODIFICACION',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'
				
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS_SOLICITUD',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},			
			{
				xtype: 'actioncolumn',
				header: 'Acci�n',
				tooltip: 'Acci�n',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if( (registro.data['CLAVE_ESTATUS']==1 || registro.data['CLAVE_ESTATUS']==2)   && registro.data['HABILITACAMPOS'] =='N'  ){	
						return 'NA';
					}else if(registro.data['CLAVE_ESTATUS']==3  || registro.data['CLAVE_ESTATUS'] ==5  ){				
						return 'NA';
					}
				},
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
						if(registro.data['CLAVE_ESTATUS']==1  && registro.data['HABILITACAMPOS'] =='S'  ){
							this.items[0].tooltip = 'Aceptar';
								return 'icoAceptar';
							}
						},
						handler: procesoAceptar
					},
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(registro.data['CLAVE_ESTATUS']==1  && registro.data['HABILITACAMPOS'] =='S'  ){
								this.items[1].tooltip = 'Rechazar';
								return 'icoRechazar';
							}
						}
						,handler: procesoRechazar
					},
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(registro.data['CLAVE_ESTATUS']==2  && registro.data['HABILITACAMPOS'] =='S'  ){
								//this.items[2].tooltip = 'Ampliar Monto';
								this.items[2].tooltip = 'Modificar <br>Monto / Plazo';
								return 'icoAceptar';
							}
						}
						,handler: procesoAmpliarMonto
					},
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(registro.data['CLAVE_ESTATUS']==2  && registro.data['HABILITACAMPOS'] =='S'  ){
								this.items[3].tooltip = 'Cancelar';
								return 'icoRechazar';
							}
						}
						,handler: procesoCancelar
					}
				]
			},
			{
				xtype: 'actioncolumn',
				header: 'Descuento Autom�tico',
				tooltip: 'Descuento Autom�tico',
				dataIndex: 'DESCAUT',
				width: 220,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.data['CLAVE_ESTATUS']==1  && ( registro.data['HABILITACAMPOS'] =='S'  ||  registro.data['HABILITACAMPOS'] =='N')   ){
						return 'NA ';	
					}else if(registro.data['CLAVE_ESTATUS']==3  || registro.data['CLAVE_ESTATUS'] ==5  ){		
						return 'NA ';
					}else if(registro.data['CLAVE_ESTATUS']==2 &&  registro.data['HABILITACAMPOS'] =='N' ){	
						return value;
					}else{
						return value;
					}
				},
				items: [					
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(registro.data['CLAVE_ESTATUS']==2  && registro.data['HABILITACAMPOS'] =='S' ){
								this.items[0].tooltip = 'Activar';
								return 'aceptar';
							}
						}
						,handler: activaDescAuto
					},
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(registro.data['CLAVE_ESTATUS']==2  && registro.data['HABILITACAMPOS'] =='S' ){
								this.items[1].tooltip = 'Desactivar';
								return 'borrar';
							}
						}
						,handler: desactivarDescAuto
					}
				]
			},		
			{
				xtype: 'actioncolumn',
				header: 'Documentos Operados',
				tooltip: 'Documentos Operados',				
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.data['CLAVE_ESTATUS']==1  && ( registro.data['HABILITACAMPOS'] =='S'  ||  registro.data['HABILITACAMPOS'] =='N')   ){
						return 'N/A ';					
					} else  if(registro.data['CLAVE_ESTATUS']==5  ){
						return 'N/A ';
					}
				},
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(registro.data['CLAVE_ESTATUS']==2  ||  registro.data['CLAVE_ESTATUS']==3 ){
								this.items[0].tooltip = 'Ver';
								return 'icoBuscar';
							}
						}
						,handler: VerDoctosOperados
					}
				]
			},
			{
				xtype: 'actioncolumn',
				header: 'Estad�sticas ',
				tooltip: 'Estad�sticas',
				width: 130,
				align: 'center',				
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'icoBuscar';							
						}
						,handler: VerEstadisticas
					}					
				]	
			},
			{
				xtype: 'actioncolumn',
				header: 'Acuse ',
				tooltip: 'Acuse',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'icoBuscar';						
						},
						handler: VerAcuse
					}					
				]	
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		displayInfo: true,		
		clicksToEdit: 1, 		
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
					var record = e.record;
					var gridConsulta = e.gridConsulta; 
					var campo= e.field;
					
					if( ( campo == 'MONTO_CREDITO' ||  campo == 'FECHA_VENCIMIENTO' 
						|| campo =='BANCO_SERVICIO' || campo =='CUENTA_BANCARIA' 
						|| campo =='TIPO_CUENTA' || campo =='TIPO_CUENTA'
						|| campo =='NUMERO_CREDITO'  || campo =='MONEDA' 
						|| campo =='MONEDA_DESCUENTO' || campo =='OBSERVACIONES' )   && record.data['CLAVE_ESTATUS'] =='1' &&   record.data['HABILITACAMPOS'] =='S' ){
						return true; // es editable				
					
					}else if( ( campo == 'MONTO_CREDITO' ||  campo =='OBSERVACIONES'  || campo =='NUEVA_FECHA_VENCIMIENTO' ) &&   record.data['HABILITACAMPOS'] =='S'  &&  record.data['CLAVE_ESTATUS']==2  ){						
						return true; // es editable				

					}else {						
						return false; // no es editable 	
					}
			},
			validateedit : function(e){// se dispara para calular datos que al caputar los campos editables 

						//record.commit();
					if (e.field === 'TIPO_CUENTA'){
						if((e.value).toUpperCase() == 'P|PYME'){
							if(	!Ext.isEmpty(e.record.data['MONEDA'])	){
								Ext.getCmp('gridConsulta').getGridEl().mask('Procesando', 'x-mask');
								Ext.Ajax.request({
									url:		'15MandatoDocumentos.data.jsp',
									params:	{
													informacion:'obtenParametrosPymeIf',
													record:		e.record,
													clave_epo:	e.record.data['IC_EPO'],
													claveIf:		e.record.data['IC_IF'],
													moneda:		e.record.data['MONEDA'],
													clavePyme:	e.record.data['IC_PYME']
												},
									 callback: procesarObtenParametrosPymeIf
								});
							}
						}else{
							e.record.data['CUENTA_BANCARIA'] = "";
							e.record.data['BANCO_SERVICIO'] = "";
						}
					}else if(e.field === 'MONEDA'){
						if(	(e.record.data['TIPO_CUENTA']).toUpperCase() == 'P|PYME'	){
								Ext.getCmp('gridConsulta').getGridEl().mask('Procesando', 'x-mask');
								Ext.Ajax.request({
									url:		'15MandatoDocumentos.data.jsp',
									params:	{
													informacion:'obtenParametrosPymeIf',
													record:		e.record,
													clave_epo:	e.record.data['IC_EPO'],
													claveIf:		e.record.data['IC_IF'],
													moneda:		e.record.data['MONEDA'],
													clavePyme:	e.record.data['IC_PYME']
												},
									 callback: procesarObtenParametrosPymeIf
								});
						}else{
							e.record.data['CUENTA_BANCARIA'] = "";
							e.record.data['BANCO_SERVICIO'] = "";
						}
					}else if (e.field === 'MONTO_CREDITO'){
						if(e.value <= e.originalValue){
							e.cancel = true;
							Ext.MessageBox.alert('Mensaje','El nuevo Monto del Cr�dito debe ser mayor al actual.');
						}
					}else if(e.field === 'NUEVA_FECHA_VENCIMIENTO'){
						var resultado = datecomp(Ext.util.Format.date(e.value,'d/m/Y'),Ext.util.Format.date(e.originalValue,'d/m/Y'));
						if (resultado == 2){
							e.cancel = true;
							Ext.MessageBox.alert('Mensaje','La nueva Fecha no puede ser menor a la Fecha inicial: '+Ext.util.Format.date(e.originalValue,'d/m/Y'));
						}
					}
			}
		},
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros",		
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					iconCls:	'icoPdf',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '15MandatoDocumentos.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',																
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarGenerarPDF
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls:	'icoXls',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15MandatoDocumentos.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',								
								tipoArchivo:'CSV'
							}),
							callback: procesarGenerarCSV
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSV',
					hidden: true
				}				
			]
		}		
	});

// *************************Forma de Criterios de Busqueda *************************

	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoTipoCadena = new Ext.data.JsonStore({
		id: 'catalogoTipoCadena',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'catalogoTipoCadena'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15MandatoDocumentos.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},		
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbPyme1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);									
							
							storeBusqAvanzPyme.load({
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
														
									})																	
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});
	
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);				
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var  elementosForma =[
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de Cadena',
			combineErrors: false,
			msgTarget: 'side',			
			items: [			
				{
					xtype: 'combo',
					fieldLabel: 'Tipo de Cadena',
					name: 'claveTipoCadena',
					id: 'claveTipoCadena1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'claveTipoCadena',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					width: 250,
					store: catalogoTipoCadena,
					tpl:	NE.util.templateMensajeCargaCombo,
					listeners: {
						select: {
							fn: function(combo) {
								var cmbclave_epo = Ext.getCmp('clave_epo1');
								cmbclave_epo.setValue('');
								cmbclave_epo.setDisabled(false);
								cmbclave_epo.store.load({
									params: {
										claveTipoCadena:combo.getValue()
									}
								});
							}
						}
					}			
				}
			]
		},
		{
			xtype: 'combo',
			fieldLabel: 'EPO',
			name: 'clave_epo',
			id: 'clave_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'clave_epo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO,
			tpl:'<tpl for=".">' +
					'<tpl if="!Ext.isEmpty(loadMsg)">'+
					'<div class="loading-indicator">{loadMsg}</div>'+
					'</tpl>'+
					'<tpl if="Ext.isEmpty(loadMsg)">'+
					'<div class="x-combo-list-item">' +
					'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
					'</div></tpl></tpl>'
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�m. Elect. PyME',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'N�mero Nafin Electr�nico',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){
							// Petici�n b�sica  
							Ext.Ajax.request({  
								url: '15MandatoDocumentos.data.jsp',  
								method: 'POST',  
								callback: successAjaxFn,  
									params: {  
									 informacion: 'pymeNombre' ,
									 num_electronico: Ext.getCmp('num_electronico1').getValue()
								}  
							}); 
						}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 270,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
				xtype: 'button',
				text: 'B�squeda Avanzada...',
				iconCls: 'icoBuscar',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
					}
				}
			}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Estatus',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Estatus',
					name: 'clave_estatus',
					id: 'clave_estatus1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'clave_estatus',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoEstatus,
					tpl:	NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_solicitud_ini',
					id: 'fecha_solicitud_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_solicitud_fin',
					margins: '0 20 0 0' ,
					formBind: true},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'fecha_solicitud_fin',
					id: 'fecha_solicitud_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_solicitud_ini',
					margins: '0 20 0 0',
					formBind: true
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Aceptaci�n IF',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_aceptacion_ini',
					id: 'fecha_aceptacion_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_aceptacion_fin',
					margins: '0 20 0 0' 
					},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'fecha_aceptacion_fin',
					id: 'fecha_aceptacion_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_aceptacion_ini',
					margins: '0 20 0 0'					
				}
			]
		}		
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 740,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 140,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
				
					var fecha_aceptacion_ini = Ext.getCmp('fecha_aceptacion_ini');
					var fecha_aceptacion_fin = Ext.getCmp('fecha_aceptacion_fin');
					var fecha_solicitud_ini = Ext.getCmp('fecha_solicitud_ini');
					var fecha_solicitud_fin = Ext.getCmp('fecha_solicitud_fin');
							
					if(  ( !Ext.isEmpty(fecha_aceptacion_ini.getValue()) &&  Ext.isEmpty(fecha_aceptacion_fin.getValue()))  || (Ext.isEmpty(fecha_aceptacion_ini.getValue()) && !Ext.isEmpty(fecha_aceptacion_fin.getValue()) )    ) {											
						Ext.MessageBox.alert('Mensaje','Debe de capturar ambas Fechas o dejarlas en Blanco');
						return;						
					}	
					if(  ( !Ext.isEmpty(fecha_solicitud_ini.getValue()) &&  Ext.isEmpty(fecha_solicitud_fin.getValue()))  || (Ext.isEmpty(fecha_solicitud_ini.getValue()) &&  !Ext.isEmpty(fecha_solicitud_fin.getValue()) )    ) {											
						Ext.MessageBox.alert('Mensaje','Debe de capturar ambas Fechas o dejarlas en Blanco');
						return;						
					}	
				
				
					fp.el.mask('Enviando...', 'x-mask-loading');	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar',
							start:0,
							limit:15						
						})
					});
				
				
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				formBind: true,
				handler: function(boton, evento) {	
					window.location = '15MandatoDocumentosext.jsp';
				}
			},
			{
				text: 'Estad�sticas',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton, evento) {	
					window.location = '15EstaMandatoDocext.jsp';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			mensajeAutentificacion,
			NE.util.getEspaciador(10),
			fp,
			gridCifrasControl,
			NE.util.getEspaciador(5),
			mensajeAcuse,			
			NE.util.getEspaciador(5),
			fpBotonesImp,
			NE.util.getEspaciador(5),
			gridConsulta,
			gridPreAcuse1,
			gridPreAcuse2,
			gridPreAcuse3,
			NE.util.getEspaciador(5),
			fpBotones,
			NE.util.getEspaciador(5),
			mensajePreAcuse,
			NE.util.getEspaciador(5)
		]
	});

	catalogoTipoCadena.load();
	catalogoEstatus.load();
	CatTipoCuenta.load();
	CatBancoServicio.load();

});