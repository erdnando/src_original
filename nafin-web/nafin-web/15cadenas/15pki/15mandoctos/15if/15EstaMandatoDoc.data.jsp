<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,	
		java.text.*,
		java.sql.*,
		java.math.*,
		netropology.utilerias.*,	
		com.netro.afiliacion.*,
		com.netro.cadenas.*,		
		com.netro.mandatodoc.*,
		com.netro.exception.*,
		com.netro.model.catalogos.*,
		com.netro.descuento.*,
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<jsp:useBean id="InstIrrevocable" scope="page" class="com.netro.mandatodoc.InstruccionIrrevocable" />
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String num_electronico = (request.getParameter("num_electronico")!=null)?request.getParameter("num_electronico"):"";
String txtRFC = (request.getParameter("txtRFC")!=null)?request.getParameter("txtRFC"):"";

String infoRegresar ="", txtNombre ="", ic_pyme ="", consulta ="";
JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
HashMap estadisticas = new HashMap();
HashMap informacionPorEpo = new HashMap();
BigDecimal doctosPubPorEpo = new BigDecimal("0");
BigDecimal doctosPubTotal = new BigDecimal("0");
BigDecimal doctosPubPorMes = new BigDecimal("0");
int  numeroEpos =0;
Calendar calendario = Calendar.getInstance();
int anioActual = calendario.get(Calendar.YEAR);
int mesActual = calendario.get(Calendar.MONTH) + 1;
int ultimoDiaMesActual = calendario.getActualMaximum(Calendar.DAY_OF_MONTH);
List datosPymes = new ArrayList();	
StringBuffer 	columnasGrid	= new StringBuffer();
StringBuffer 	columnasStore	= new StringBuffer();
StringBuffer 	columnasRecords	= new StringBuffer();

ParametrosDescuento 		parametrosDescuento 		= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

if(!txtRFC.equals("")) {
	num_electronico = obtieneIC_Pyme(txtRFC);
}
if(!num_electronico.equals("")) {
	datosPymes =  parametrosDescuento.datosPymes(num_electronico, "" ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);	
	}
	
}
//para obtener los años y Meses 
HashMap aniosOperacion = InstIrrevocable.generaEncabezadoTabla(mesActual, anioActual);
int numeroAnios = Integer.parseInt(aniosOperacion.get("numeroAnios").toString());

	
if (informacion.equals("busquedaAvanzada") ) {

	String nombrePyme = (request.getParameter("nombrePyme")!=null)?request.getParameter("nombrePyme"):"";
	String rfcPyme = (request.getParameter("rfcPyme")!=null)?request.getParameter("rfcPyme"):"";
	
	CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");	
	if(!rfcPyme.equals(""))
	cat.setRfc_pyme(rfcPyme);
	if(!nombrePyme.equals(""))
	cat.setNombre_pyme(nombrePyme);
	cat.setPantalla("SoliAfi");		
	cat.setOrden("p.cg_razon_social");		
	infoRegresar = cat.getJSONElementos();
		
} else if(informacion.equals("pymeNombre") ) {
	
	datosPymes =  parametrosDescuento.datosPymes(num_electronico, "" ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);	
	jsonObj.put("ic_pyme", ic_pyme);
	infoRegresar = jsonObj.toString();	
	
}  else if(informacion.equals("ConsultarPyme") ) {
	List info = InstIrrevocable.datosPyme( txtRFC, num_electronico );
	List infoEpo  = InstIrrevocable.consEstadistica(ic_pyme , mesActual, anioActual, ultimoDiaMesActual );		
 
	if(infoEpo.size()>0){
		numeroEpos = Integer.parseInt(infoEpo.get(2).toString()); 
	}
	
	if(info.size()>0&&numeroEpos>0){
		datos = new HashMap();		
		datos.put("NAFIN_ELECTRONICO", info.get(0).toString());
		datos.put("NOMBRE_PYME", info.get(1).toString());
		datos.put("RFC", info.get(2).toString());	
		registros.add(datos);	
	}	
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	

}  else if(informacion.equals("ConsultarEstadistica")  ) {
	
	columnasGrid	= new StringBuffer();
	columnasStore	= new StringBuffer();
	columnasRecords	= new StringBuffer();

	//para obtener los registros de las estadisticas por mes	 
	List info  = InstIrrevocable.consEstadistica(ic_pyme , mesActual, anioActual, ultimoDiaMesActual );		
 
	if(info.size()>0){
		informacionPorEpo = (HashMap)info.get(0);
		estadisticas = (HashMap)info.get(1);
		numeroEpos = Integer.parseInt(info.get(2).toString()); 
	}
		
	if (numeroEpos > 0) {

		columnasGrid.append(" [  ");
		columnasGrid.append("  {  ");
		columnasGrid.append(" header: ' EPO', ");
		columnasGrid.append(" tooltip: 'EPO', ");						
		columnasGrid.append(" width: 130, ");
		columnasGrid.append(" align: 'left', ");					
		columnasGrid.append(" dataIndex: 'NOMBRE_EPO' ");
		columnasGrid.append(" }, ");							
		columnasStore.append("  [  ");	
		columnasStore.append("  {  name: 'NOMBRE_EPO',  mapping : 'NOMBRE_EPO' }, ");

		for (int i = 0; i < numeroAnios; i++) {
			int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+i)).toString());
				String  anio  = aniosOperacion.get("anio"+i).toString();			
			
			HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+i));
			for (int j = 0; j < mesesPorAnio; j++) {
				String  mes  = meses.get("nombreMes"+j).toString();
				String  indiceMes  ="MES"+j+anio;
			
				columnasGrid.append("  {  ");
				columnasGrid.append(" header: '"+mes+" "+anio+"', ");
				columnasGrid.append(" tooltip: '"+mes+" "+anio+"', ");						
				columnasGrid.append(" width: 130, ");
				columnasGrid.append(" align: 'center', ");					
				columnasGrid.append(" dataIndex: '"+indiceMes+"' ");
				columnasGrid.append(" },");
			
				columnasStore.append("  {  name: '"+indiceMes+"',  mapping : '"+indiceMes+"' },");	
						
			}
		}		
		
		columnasGrid.append("  {  ");
		columnasGrid.append(" header: 'TOTAL POR EPO', ");
		columnasGrid.append(" tooltip: 'TOTAL POR EPO', ");						
		columnasGrid.append(" width: 130, ");
		columnasGrid.append(" align: 'center', ");					
		columnasGrid.append(" dataIndex: 'TOTAL_XEPO' ");
		columnasGrid.append(" } ");		
		columnasGrid.append("	] ");		
				
		columnasStore.append("  {  name: 'TOTAL_XEPO',  mapping : 'TOTAL_XEPO' } ");		
		columnasStore.append("	] ");
		
		//esta parte es para los datos 
		columnasRecords.append("  [   ");
		for (int i = 0; i < numeroEpos; i++) {
			
			informacionPorEpo = (HashMap)estadisticas.get("informacionPorEpo"+i);			
			String nombreEPO= informacionPorEpo.get("nombreEpo").toString();
			columnasRecords.append(" { ");
			columnasRecords.append("NOMBRE_EPO"+":'"+nombreEPO+"',");
				
			for (int j = 0; j < numeroAnios; j++) {
				int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+j)).toString());
				HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+j));
				String  anio  = aniosOperacion.get("anio"+j).toString();
					
				for (int k = 0; k < mesesPorAnio; k++) {
				  String informa  =informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j)).toString();
					doctosPubPorEpo = doctosPubPorEpo.add(new BigDecimal(informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
					String  indiceMes  ="MES"+k+anio;
					columnasRecords.append("'"+indiceMes+"'"+":'"+informa+"',");
				}
			}
			
			String totalXepo =  doctosPubPorEpo.setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString();
			doctosPubPorEpo = new BigDecimal("0");		
			columnasRecords.append("TOTAL_XEPO"+":'"+totalXepo+"'},");
			
		}							
		columnasRecords.deleteCharAt(columnasRecords.length()-1);
		columnasRecords.append(" ] ");

		
	}//if (numeroEpos > 0) {

	if(numeroEpos!=0) {
		infoRegresar = "{\"success\": true, \"columnasGrid\": " + columnasGrid.toString() + 
				", \"columnasStore\": " + columnasStore.toString() +	
				", \"numeroEpos\": " + numeroEpos +	
				", \"num_electronico\": " + num_electronico +					
				", \"columnasRecords\": {\"registros\": " + columnasRecords.toString() +			
				", \"total\":"+numeroEpos+" }  }";
	}else if(numeroEpos==0) {
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("numeroEpos", String.valueOf(numeroEpos));	
		infoRegresar = jsonObj.toString();	
	} 


} else if( informacion.equals("consultaDataTotales")  ) { 
		
	columnasGrid	= new StringBuffer();
	columnasStore	= new StringBuffer();
	columnasRecords	= new StringBuffer();
		
	//para obtener los registros de las estadisticas por mes
	List info  = InstIrrevocable.consEstadistica(ic_pyme , mesActual, anioActual, ultimoDiaMesActual );
	
	if(info.size()>0){
		informacionPorEpo = (HashMap)info.get(0);
		estadisticas = (HashMap)info.get(1);
		numeroEpos = Integer.parseInt(info.get(2).toString()); 
	}
	
	if (numeroEpos > 0) {

		columnasGrid.append(" [  ");
		columnasGrid.append("  {  ");
		columnasGrid.append(" header: ' ', ");
		columnasGrid.append(" tooltip: '', ");						
		columnasGrid.append(" width: 130, ");
		columnasGrid.append(" align: 'left', ");					
		columnasGrid.append(" dataIndex: 'TOTAL_X_MES' ");
		columnasGrid.append(" }, ");							
		columnasStore.append("  [  ");	
		columnasStore.append("  {  name: 'TOTAL_X_MES',  mapping : 'TOTAL_X_MES' }, ");

		for (int i = 0; i < numeroAnios; i++) {
			int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+i)).toString());
				String  anio  = aniosOperacion.get("anio"+i).toString();			
			
			HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+i));
			for (int j = 0; j < mesesPorAnio; j++) {
				String  mes  = meses.get("nombreMes"+j).toString();
				String  indiceMes  ="MES"+j+anio;
			
				columnasGrid.append("  {  ");
				columnasGrid.append(" header: '"+mes+" "+anio+"', ");
				columnasGrid.append(" tooltip: '"+mes+" "+anio+"', ");						
				columnasGrid.append(" width: 130, ");
				columnasGrid.append(" align: 'center', ");					
				columnasGrid.append(" dataIndex: '"+indiceMes+"' ");
				columnasGrid.append(" },");
			
				columnasStore.append("  {  name: '"+indiceMes+"',  mapping : '"+indiceMes+"' },");	
						
			}
		}		
		
		columnasGrid.append("  {  ");
		columnasGrid.append(" header: 'TOTAL POR EPO', ");
		columnasGrid.append(" tooltip: 'TOTAL POR EPO', ");						
		columnasGrid.append(" width: 130, ");
		columnasGrid.append(" align: 'center', ");					
		columnasGrid.append(" dataIndex: 'TOTAL_XEPO' ");
		columnasGrid.append(" } ");		
		columnasGrid.append("	] ");		
				
		columnasStore.append("  {  name: 'TOTAL_XEPO',  mapping : 'TOTAL_XEPO' } ");		
		columnasStore.append("	] ");  
	
		
		//Para los Totales 
		doctosPubPorMes = new BigDecimal("0");
		columnasRecords.append("  [   ");
		columnasRecords.append(" { ");
		columnasRecords.append("TOTAL_X_MES"+":'TOTAL POR MES' ,");
		
		for (int j = 0; j < numeroAnios; j++) {
			HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+j));
			int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+j)).toString());
			String  anio  = aniosOperacion.get("anio"+j).toString();
			for (int k = 0; k < mesesPorAnio; k++) {
				for (int i = 0; i < numeroEpos; i++) {
					informacionPorEpo = (HashMap)estadisticas.get("informacionPorEpo"+i);
					doctosPubPorMes = doctosPubPorMes.add(new BigDecimal(informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
					doctosPubPorEpo = doctosPubPorEpo.add(new BigDecimal(informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
					
				}
				String  indiceMes  ="MES"+k+anio;
				columnasRecords.append("'"+indiceMes+"'"+":'"+doctosPubPorMes.setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString()+"',");
				doctosPubPorMes = new BigDecimal("0");							
			}
		}
		doctosPubTotal = doctosPubTotal.add(doctosPubPorEpo);
		columnasRecords.append("TOTAL_XEPO"+":'"+doctosPubTotal.setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString()+"'},");
		columnasRecords.deleteCharAt(columnasRecords.length()-1);
		columnasRecords.append(" ] ");
	
		
	}//if (numeroEpos > 0) {

	if(numeroEpos!=0) {
		infoRegresar = "{\"success\": true, \"columnasGridT\": " + columnasGrid.toString() + 
				", \"columnasStoreT\": " + columnasStore.toString() +	
				", \"numeroEpos\": " + numeroEpos +	
				", \"num_electronico\": " + num_electronico +					
				", \"columnasRecordsT\": {\"registros\": " + columnasRecords.toString() +			
				", \"total\":"+1+" }  }";
	}else if(numeroEpos==0) {
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("numeroEpos", String.valueOf(numeroEpos));	
		infoRegresar = jsonObj.toString();	
	}


} else if(informacion.equals("GenerarCSV")  ) {

	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = "";
	StringBuffer contenidoArchivo = new StringBuffer();

	//obtener los datos de la Pyme 
	List infoPyme = InstIrrevocable.datosPyme( txtRFC, num_electronico );
	String NoNafin = infoPyme.get(0).toString();
	String NOM_PYME = infoPyme.get(1).toString();
	String RFC = infoPyme.get(2).toString();
		
	//para obtener los registros de las estadisticas por mes
	List info  = InstIrrevocable.consEstadistica(ic_pyme , mesActual, anioActual, ultimoDiaMesActual );
	
	if(info.size()>0){
		informacionPorEpo = (HashMap)info.get(0);
		estadisticas = (HashMap)info.get(1);
		numeroEpos = Integer.parseInt(info.get(2).toString()); 
	}
	
	if (numeroEpos > 0) {
		contenidoArchivo.append("Número de Nafin Electrónico, Nombre de la Pyme, RFC,\n");
		contenidoArchivo.append(NoNafin  + ",");
		contenidoArchivo.append(NOM_PYME.replaceAll(",", "") + ",");
		contenidoArchivo.append(RFC + ",\n");
		contenidoArchivo.append("EPO,");
				
		for (int i = 0; i < numeroAnios; i++) {
			int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+i)).toString());
			HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+i));
			for (int j = 0; j < mesesPorAnio; j++) {
			contenidoArchivo.append("No. de Documentos " + meses.get("nombreMes"+j) + " " + aniosOperacion.get("anio"+i) + ",");						
			}
		}
		contenidoArchivo.append("TOTAL POR EPO,\n");
		
		for (int i = 0; i < numeroEpos; i++) {
			informacionPorEpo = (HashMap)estadisticas.get("informacionPorEpo"+i);			
			contenidoArchivo.append(informacionPorEpo.get("nombreEpo").toString().replaceAll(",", "") + ",");
			
			for (int j = 0; j < numeroAnios; j++) {
				int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+j)).toString());
				HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+j));
				String  anio  = aniosOperacion.get("anio"+j).toString();
					
				for (int k = 0; k < mesesPorAnio; k++) {
				  	doctosPubPorEpo = doctosPubPorEpo.add(new BigDecimal(informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
					contenidoArchivo.append((informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))) + ",");							
				}
			}			
			contenidoArchivo.append(doctosPubPorEpo.setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString() + ",");		
			doctosPubTotal = doctosPubTotal.add(doctosPubPorEpo);
			doctosPubPorEpo = new BigDecimal("0");			
			contenidoArchivo.append("\n");	
		}
		contenidoArchivo.append("TOTAL POR MES,");
		for (int j = 0; j < numeroAnios; j++) {
			HashMap meses = (HashMap)aniosOperacion.get("meses"+aniosOperacion.get("anio"+j));
			int mesesPorAnio = Integer.parseInt(aniosOperacion.get("numeroMeses"+aniosOperacion.get("anio"+j)).toString());
			for (int k = 0; k < mesesPorAnio; k++) {
				for (int i = 0; i < numeroEpos; i++) {
					informacionPorEpo = (HashMap)estadisticas.get("informacionPorEpo"+i);
					doctosPubPorMes = doctosPubPorMes.add(new BigDecimal(informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))==null?"0":(String)informacionPorEpo.get("doctopub_" + meses.get("numeroMes"+k) + "/" + aniosOperacion.get("anio"+j))));
				}
				contenidoArchivo.append(doctosPubPorMes.setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString() + ",");
				doctosPubPorMes = new BigDecimal("0");
			}
		}
		contenidoArchivo.append(doctosPubTotal.setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString() + ",");
		contenidoArchivo.append("\n");

	}

	archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv");
	nombreArchivo = archivo.getNombre();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();  		
}

%>
<%=infoRegresar%>


<%!
	public String  obtieneIC_Pyme(String  rfc ){
		System.out.println("obtieneIC_Pyme  (E)");
		StringBuffer 	strSQL 	= new StringBuffer();
		PreparedStatement 	ps	= null;
		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		String  ic_pyme = "";

		try{
			con.conexionDB();
			//strSQL.append(" SELECT ic_pyme AS pyme FROM comcat_pyme WHERE cg_rfc =  ? ");
			
			 strSQL.append(" SELECT p.cg_razon_social AS pyme, "+
				"  p.cg_rfc AS rfc_pyme, "+
				 " na.ic_nafin_electronico AS nonafin "+
				"  FROM comcat_pyme p "+
				" , comrel_nafin na "+
				 " WHERE na.ic_epo_pyme_if = p.ic_pyme "+
				" AND na.cg_tipo = 'P' "+
				" and p.cg_rfc = ? ");
 
 

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1,rfc);
			rs = ps.executeQuery();
			if(rs.next()){
				ic_pyme = (rs.getString("nonafin")==null)?"":rs.getString("nonafin");
        }
			rs.close();
			ps.close();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("obtieneIC_Pyme (S)");
		}
			return ic_pyme;
	}

%>
