Ext.onReady(function() {

	var numeroEpos = 0;
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarCSV');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarCSV');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	// GRID DE LOS TOTALES  
	var procesarSuccessConsultaTotaes =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonObj = Ext.util.JSON.decode(response.responseText)
			var contenedorPrincipal = Ext.getCmp('contenedorPrincipal');
			
				
			if(jsonObj.numeroEpos!='0'){
				consultaDataTotales.fields =  jsonObj.columnasStoreT;
				consultaDataTotales.data =  jsonObj.columnasRecordsT;
				gridTotales.columns = jsonObj.columnasGridT;	
							
				contenedorPrincipal.remove("gridTotales");			
				contenedorPrincipal.add(gridTotales);
				numeroEpos = 1;
				
				
				
			}else if(jsonObj.numeroEpos=='0'){
				contenedorPrincipal.remove("gridTotales");	
				contenedorPrincipal.remove("mensajeL");
			}
			
					
			if(numeroEpos==1){
				contenedorPrincipal.remove("mensajeL");
				contenedorPrincipal.add(mensajeL);
			}
			
			contenedorPrincipal.el.unmask();
			contenedorPrincipal.doLayout();	
		
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	
// GRID QUE MUESTRA LAS ESTADISTICAS 
	var procesarSuccessConsultaEstadistica =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonObj = Ext.util.JSON.decode(response.responseText)
			var contenedorPrincipal = Ext.getCmp('contenedorPrincipal');			
			
			if(jsonObj.numeroEpos!='0'){
				consultaDataEstadisticas.fields =  jsonObj.columnasStore;
				consultaDataEstadisticas.data =  jsonObj.columnasRecords;
				gridEstadisticas.columns = jsonObj.columnasGrid;	
				contenedorPrincipal.remove("gridEstadisticas");			
				contenedorPrincipal.add(gridEstadisticas);	
				//Totales					
					Ext.Ajax.request({
						url: '15EstaMandatoDoc.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'consultaDataTotales'						
						})					
						,callback: procesarSuccessConsultaTotaes
					});
					
							
			}else if(jsonObj.numeroEpos=='0'){
				contenedorPrincipal.remove("gridEstadisticas");
				contenedorPrincipal.remove("gridTotales");
				contenedorPrincipal.remove("mensajeL");	
				Ext.getCmp('gridPyme').hide();
			}
				
				contenedorPrincipal.el.unmask();
				contenedorPrincipal.doLayout();	
				
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	

	//para Motrar los registros de estadisticas 
	var consultaDataEstadisticas = {
		xtype: 'jsonstore',
		id:'consultaDataEstadisticas',
		root: 'registros',
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		fields: null,
		data: null
	};
	
	
	var gridEstadisticas = {		
		xtype: 'grid',	
		id:'gridEstadisticas',	
		store:consultaDataEstadisticas,			
		columns: [],		
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		frame: true,
		height: 400,
		width: 900,	
		style: 'margin:0 auto;',
		title: 'No. de Documentos',
		bbar: {
			xtype: 'toolbar',
			id: 'barraPaginacion',
			items: [
				'->',	
				'-',				
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15EstaMandatoDoc.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GenerarCSV'																
							}),
							callback: procesarGenerarCSV
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				}	
			]
		}		
	};
	
 // para mostrar los totales por Meses 
	
	var consultaDataTotales = {
		xtype: 'jsonstore',
		id:'consultaDataTotales',
		root: 'registros',
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		fields: null,
		data: null
	};
	
	var gridTotales = {		
		xtype: 'grid',	
		id:'gridTotales',	
		store:consultaDataTotales,			
		columns: [],		
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		frame: true,
		height: 120,
		width: 900,	
		style: 'margin:0 auto;',
		title: ''	
	};
	
	
	var link = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td>&nbsp;</td></tr>'+ 	
		'<tr><td class="formas" style="text-align: justify;"  colspan="3"><b>Para mayor informaci�n, consultar: <a href="http://www.compranet.gob.mx" target="_blank">www.compranet.gob.mx</a></b></td></tr>'+ 
		'<tr><td>&nbsp;</td></tr>'+ 	
		'</table>';
	
	 var mensajeL = {
		layout: 'table',		
		id: 'mensajeL',							
		width:	'330',
		heigth:	'auto',
		style: 'margin:0 auto;',	
		bodyStyle: 'padding: 6px',
		items: [					
			{ 
				xtype:   'label',  
				html:		link, 
				cls:		'x-form-item', 
				style: { 
					width:   		'700%', 
					textAlign: 		'left'
				} 
			}	
		]
	};
	
	
	
// GRID QUE MUESTRA LOS DATOS DE LA PYME  
	var procesarConsultaPymeData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();							
		var gridPyme = Ext.getCmp('gridPyme');		
		if (arrRegistros != null) {
			if (!gridPyme.isVisible()) {
				gridPyme.show();
			}	
			var el = gridPyme.getGridEl();
			
			if(store.getTotalCount() > 0) {				
				el.unmask();					
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaPymeData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '15EstaMandatoDoc.data.jsp',
		baseParams: {
			informacion: 'ConsultarPyme'
		},		
		fields: [			
			{name: 'NAFIN_ELECTRONICO'},
			{name: 'NOMBRE_PYME'},
			{name: 'RFC'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaPymeData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaPymeData(null, null, null);					
				}
			}
		}		
	});
	
	
	var gridPyme = new Ext.grid.GridPanel({
		store: consultaPymeData,
		id: 'gridPyme',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '',		
		columns: [	
			{
				header: 'N�mero de Nafin Electr�nico',
				tooltip: 'N�mero de Nafin Electr�nico',
				dataIndex: 'NAFIN_ELECTRONICO',
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: '<center>Nombre de la Pyme</center>',
				tooltip: 'Nombre de la Pyme',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 300,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		width: 600,
		height: 100,
		frame: true
	});
						
// *************************Forma de Criterios de Busqueda *************************		
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);				
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15EstaMandatoDoc.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},		
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 		'panel',
			bodyStyle: 	'padding: 10px',
			layout: {
			type: 	'hbox',
				pack: 	'center',
				align: 	'middle'
			},
			items: [
				{
					xtype: 'button',
					text: 'Buscar',
					id: 'btnBuscar',
					iconCls: 'icoBuscar',
					width: 	75,
					handler: function(boton, evento) {
						var pymeComboCmp = Ext.getCmp('cbPyme1');
						pymeComboCmp.setValue('');
						pymeComboCmp.setDisabled(false);									
						storeBusqAvanzPyme.load({
							params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{		})																	
						});				
					},
					style: { 
						marginBottom:  '10px' 
					} 
				}
			]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
			xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});
	
	var  elementosForma =[			
		{
			xtype: 'compositefield',
			fieldLabel: 'RFC',
			combineErrors: false,
			msgTarget: 'side',	
			items: [
				{
					xtype: 'textfield',
					name: 'txtRFC',
					id: 'txtRFC1',
					allowBlank: true,
					maxLength: 100,
					width: 380,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				}
			]
		},				
		{
			xtype: 'compositefield',
			fieldLabel: 'NE de la PyME',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'N�mero Nafin Electr�nico',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){
							// Petici�n b�sica  
							Ext.Ajax.request({  
								url: '15EstaMandatoDoc.data.jsp',  
								method: 'POST',  
								callback: successAjaxFn,  
									params: {  
									 informacion: 'pymeNombre' ,
									 num_electronico: Ext.getCmp('num_electronico1').getValue()
								}  
							}); 
						}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 280,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
					xtype: 'button',
					text: 'B�squeda Avanzada...',
					iconCls: 'icoBuscar',
					id: 'btnBusqAv',
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winBusqAvan');
						if (ventana) {
							ventana.show();
						} else {
							new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
						}
					}
				}
			]
		}		
	];


	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 680,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 90,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
					var txtRFC =  Ext.getCmp("txtRFC1");
					var num_electronico =  Ext.getCmp("num_electronico1");
				
					if (Ext.isEmpty(txtRFC.getValue())  && Ext.isEmpty(num_electronico.getValue())  ){
						Ext.MessageBox.alert('Mensaje','Debes de capturar una opci�n de b�squeda');								
						return;
					}
					if (!Ext.isEmpty(txtRFC.getValue())  && !Ext.isEmpty(num_electronico.getValue())  ){
						Ext.MessageBox.alert('Mensaje','Captura solo una opci�n de b�squeda');	
						fp.getForm().reset()
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');	
					consultaPymeData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarPyme'				
						})
					});
					//Consulta Registros
					Ext.Ajax.request({
						url: '15EstaMandatoDoc.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ConsultarEstadistica'						
						})					
						,callback: procesarSuccessConsultaEstadistica
					});
					
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				formBind: true,
				handler: function(boton, evento) {	
					window.location = '15EstaMandatoDocext.jsp';
				}
			},
			{
				text: 'Regresar',
				iconCls: 'cancelar',
				formBind: true,
				handler: function(boton, evento) {	
					window.location = '15MandatoDocumentosext.jsp';
				}
			}
		]
	});
	


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 960,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),			
			fp,
			NE.util.getEspaciador(20),
			gridPyme,
			NE.util.getEspaciador(20)
		]
	});

});