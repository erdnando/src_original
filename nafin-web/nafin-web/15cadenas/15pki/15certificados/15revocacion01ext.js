Ext.require('Ext.data.Store');
Ext.onReady(function() {

	var idMenu =  Ext.getDom('idMenu').value;
	
		//----------------------------- "MAQUINA DE ESTADO" -------------------------------
		var procesaRevocacionCertificadoDigital = function(opts, success, response) {
			
			var resp = 	Ext.JSON.decode(response.responseText); 
			
			if (success == true && Ext.JSON.decode(response.responseText).success == true) {
				
				var resp = 	Ext.JSON.decode(response.responseText);
				
				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							revocacionCertificadoDigital(resp.estadoSiguiente,resp);
						}
					).setZIndex(1000000);
				} else {
					revocacionCertificadoDigital(resp.estadoSiguiente,resp);
				}
				
			} else {
				
				// Mostrar mensaje de error
				NE.util.mostrarErrorResponse4( 
					response,
					opts, 
					function(){

						var formaConsulta 				= Ext.ComponentQuery.query('#formaConsulta')[0];
						if( formaConsulta.getEl().isMasked() ){
							formaConsulta.getEl().unmask();
						}

						var formaDetalleCertificado 	= Ext.ComponentQuery.query('#formaDetalleCertificado')[0];
						if( formaDetalleCertificado.getEl().isMasked() ){
							formaDetalleCertificado.getEl().unmask();
						}

					} 

				);  

			}

		}

		var fnRevocarCertCallback = function(vpkcs7, vtextoFirmar, vrespUsuario){
			var textoFirmado2	= Ext.isEmpty(vpkcs7)?"":vtextoFirmar;
			
			// RevocarCertificado si cumple los requisitos
			Ext.Ajax.request({
				url: 						'15revocacion01ext.data.jsp',
				params: 	{
					informacion:		'RevocacionCertificadoDigital.revocarCertificado',
					usuario:			vrespUsuario,
					pkcs7:				vpkcs7,
					textoFirmado:		textoFirmado2
				},
				callback: 				procesaRevocacionCertificadoDigital
			});
		}
		
		var revocacionCertificadoDigital = function( estado, respuesta ){
		
			if(					estado == "INICIALIZACION"			){
					
				// Determinar el estado siguiente
				Ext.Ajax.request({
					url: 						'15revocacion01ext.data.jsp',
					params: 	{
						informacion:		'RevocacionCertificadoDigital.inicializacion',
						idMenuP:idMenu
					},
					callback: 				procesaRevocacionCertificadoDigital
				});
				
			} else if(			estado == "MOSTRAR_AVISO"         ){
	
				if(!Ext.isEmpty(respuesta.aviso)){
			
					var panelAvisos = Ext.ComponentQuery.query('#panelAvisos')[0];
					var labelAviso  = panelAvisos.query('#labelAviso')[0];
					
					labelAviso.setText(respuesta.aviso,false);
					panelAvisos.show();
			
				}
				
			} else if(			estado == "ESPERAR_DECISION"      				){
			
			} else if(			estado == "PRESENTAR_FORMA_CONSULTA"        	){
				
				var formaConsulta 				= Ext.ComponentQuery.query('#formaConsulta')[0];
				var formaDetalleCertificado 	= Ext.ComponentQuery.query('#formaDetalleCertificado')[0];
				
				var botonRevocarCertificado 	= formaDetalleCertificado.query("#botonRevocarCertificado")[0];
				
				var conservarParametros			= false;
				if( respuesta != null && Ext.isDefined(respuesta.conservarParametros) && respuesta.conservarParametros === true ){
					conservarParametros			= true;
				}
				
				// Ocultar m�scara
				if( formaConsulta.getEl().isMasked() ){
					formaConsulta.getEl().unmask();
				}
				// Ocultar m�scara
				if( formaDetalleCertificado.getEl().isMasked() ){
					formaDetalleCertificado.getEl().unmask();
				}
				
				// OCULTAR FORMA DETALLE CERTIFICADO
				
				// Ocultar forma de detalle
				formaDetalleCertificado.hide();
				// Poner boton Revocar Certificado en su estado original
				botonRevocarCertificado.enable();
				// Resetear elementos de la forma
				formaDetalleCertificado.getForm().reset();
				
				// MOSTRAR FORMA CONSULTA
				
				// Resetear elementos de la forma
				if( conservarParametros === false ){
					formaConsulta.getForm().reset();
				}
				// Mostrar forma de consulta
				formaConsulta.show();
				
			} else if(			estado == "CONSULTAR_CERTIFICADO"      		){
				
				var formaConsulta = Ext.ComponentQuery.query('#formaConsulta')[0];
				formaConsulta.getEl().mask('Realizando consulta...','x-mask-loading');		
				
				Ext.Ajax.request({
					url: 							'15revocacion01ext.data.jsp',
					params:
						Ext.apply(
							{ 
								informacion:	'RevocacionCertificadoDigital.consultarCertificado' 
							},
							respuesta
						),
					callback: 					procesaRevocacionCertificadoDigital
				});
				
			} else if(			estado == "PRESENTAR_DETALLE_CERTIFICADO"  	){
				
				var formaConsulta 				= Ext.ComponentQuery.query('#formaConsulta')[0];
				var formaDetalleCertificado 	= Ext.ComponentQuery.query('#formaDetalleCertificado')[0];

				var botonRevocarCertificado 	= formaDetalleCertificado.query("#botonRevocarCertificado")[0];
				var fechaVigencia					= formaDetalleCertificado.query("#textFechaVigencia")[0];
				
				// Si se especific� el detalle del certificado, mostrarlo
				if( Ext.isDefined(respuesta.detalle)){
					
					// Cargar detalle del certificado
					var detalle = respuesta.detalle;
					
					formaDetalleCertificado.query("#textRazonSocialDependencia")[0].setValue(detalle.razonSocialDependencia);
					formaDetalleCertificado.query("#textUsuario")[0].setValue(detalle.usuario);
					formaDetalleCertificado.query("#textNombreUsuario")[0].setValue(detalle.nombreUsuario);
					formaDetalleCertificado.query("#textCorreo")[0].setValue(detalle.correo);
					formaDetalleCertificado.query("#textFechaVigencia")[0].setValue(detalle.fechaVigencia);
					
					// En caso de no tener certificado, mostrar descripcion de 
					// campo: fechaVigencia en negritas
					if( respuesta.detalle.tieneCertificado === true ){
						fechaVigencia.removeCls("negritas");
					} else {
						fechaVigencia.addCls("negritas");
					}
					
					// Agregar l�gica para ocultar, mostrar bot�n: Revocar Certificado 
					if( respuesta.detalle.tieneCertificado === true ){
						botonRevocarCertificado.enable();
					} else {
						botonRevocarCertificado.disable();
					}
					
				}
				
				// Ocultar m�scara
				if( formaConsulta.getEl().isMasked() ){
					formaConsulta.getEl().unmask();
				}
				// Ocultar m�scara
				if( formaDetalleCertificado.getEl().isMasked() ){
					formaDetalleCertificado.getEl().unmask();
				}
				
				// Ocultar forma de consulta
				formaConsulta.hide();
				// Mostrar forma de detalle
				formaDetalleCertificado.show();
					
			} else if(			estado == "PREPARAR_REVOCACION_CERTIFICADO"        ){
				
				var formaDetalleCertificado = Ext.ComponentQuery.query('#formaDetalleCertificado')[0];
				formaDetalleCertificado.getEl().mask('Realizando revocaci�n...','x-mask-loading');		
				
				Ext.Ajax.request({
					url: 							'15revocacion01ext.data.jsp',
					params:
						Ext.apply(
							{ 
								informacion:	'RevocacionCertificadoDigital.prepararRevocacionCertificado' 
							},
							respuesta
						),
					callback: 					procesaRevocacionCertificadoDigital
				});
				
			} else if(			estado == "REVOCAR_CERTIFICADO"        						){
				
				var textoFirmar = respuesta.textoFirmar;
				
				// Suprimir acentos
				textoFirmar = textoFirmar.replace(/�/g, "a").replace(/�/g, "e").replace(/�/g, "i").replace(/�/g, "o").replace(/�/g, "u");
				textoFirmar = textoFirmar.replace(/�/g, "A").replace(/�/g, "E").replace(/�/g, "I").replace(/�/g, "O").replace(/�/g, "U");
										
				// Firmar texto
				NE.util.obtenerPKCS7(fnRevocarCertCallback, textoFirmar, respuesta.usuario);
				
				
			}

	};
	
	//------------------------- FORMA -DETALLE- REVOCACION CERTIFICADO DIGITAL ---------------------------
	
	var formaDetalleCertificado = Ext.create(
		'Ext.form.Panel',
		{
			layout: 			'form',
			itemId:			'formaDetalleCertificado',
			frame: 			true,
			title: 			'Revocaci&oacute;n - Certificado Digital',
			bodyPadding:	'12 6 12 6',
			width: 			450,
			style:			'margin: 0px auto 0px auto;',
			hidden:			true,
			fieldDefaults: {
					msgTarget: 'side',
					labelWidth: 180
			},
			items: [
				{
					xtype: 			'displayfield',
					fieldLabel:		'Raz&oacute;n Social de la Dependencia',
					itemId:			'textRazonSocialDependencia',
					name: 			'razonSocialDependencia',
					submitValue: 	true
				},
				{
					xtype: 			'displayfield',
					fieldLabel:		'Usuario',
					itemId:			'textUsuario',
					name: 			'usuario',
					submitValue: 	true
				},
				{
					xtype: 			'displayfield',
					fieldLabel:		'Nombre Usuario',
					itemId:			'textNombreUsuario',
					name: 			'nombreUsuario',
					submitValue: 	true
				},
				{
					xtype: 			'displayfield',
					fieldLabel:		'Correo',
					itemId:			'textCorreo',
					name: 			'correo',
					submitValue: 	true
				},
				{
					xtype: 			'displayfield',
					fieldLabel:		'Fecha de Vigencia',
					itemId:			'textFechaVigencia',
					name: 			'fechaVigencia',
					submitValue: 	true
				}
			],
			buttonAlign: 'center',
			buttons: [
				{
					text: 	'Revocar Certificado',
					itemId:	'botonRevocarCertificado',
					iconCls:	'icoContinuar',
					handler: function(){
						
						var forma		= this.up('form').getForm();
						
						Ext.Msg.confirm(
							'Mensaje',
							'�Est� seguro de revocar el certificado digital?',
							function(boton){
								if(boton === 'yes'){
									var respuesta = forma.getValues();
									revocacionCertificadoDigital("PREPARAR_REVOCACION_CERTIFICADO",respuesta);
								}
							}
						);
						
					}
				},
				{
					text: 	'Cancelar',
					itemId:	'botonCancelar',
					iconCls: 'icoCancelar',
					handler: function() {
						revocacionCertificadoDigital("PRESENTAR_FORMA_CONSULTA",null);
					}
				}
			]
		}
	);
	
	//------------------------- FORMA -CONSULTA- REVOCACION CERTIFICADO DIGITAL ---------------------------
	
	var formaConsulta = Ext.create(
		'Ext.form.Panel',
		{
			
			layout: 			'form',
			itemId:			'formaConsulta',
			frame: 			true,
			title: 			'Revocaci\u00F3n - Certificado Digital',
			bodyPadding:	'12 6 12 6',
			width: 			300,
			style:			'margin: 0px auto 0px auto;',
			hidden:			true,
			fieldDefaults: {
					msgTarget: 'side',
					labelWidth: 90
			},
			items: [
				{
					xtype: 			'textfield',
					itemId: 			'textUsuario',
					fieldLabel:		'Usuario',
					name: 			'usuario',
					hidden:			false,
					allowBlank: 	false,
					maxLength: 		8,
					vtype:			'login',
					maskRe:			/[0-9]/ // Filtrar caracteres no numericos
				}
			],
			buttonAlign: 'center',
			buttons: [
				{
					text: 	'Consultar',
					itemId:	'botonConsultar',
					iconCls:	'icoBuscar',
					handler: function(){
						var forma = this.up('form').getForm();
						if( forma.isValid() ){
							var respuesta = forma.getValues();
							revocacionCertificadoDigital("CONSULTAR_CERTIFICADO",respuesta);
						}
					}
				},
				{
					text: 	'Limpiar',
					itemId:	'botonLimpiar',
					iconCls: 'icoLimpiar',
					handler: function() {
						var forma = this.up('form').getForm();
						forma.reset();
					}
				}
			]
		}
	);
	
	//------------------------------------ 1. PANEL AVISOS ---------------------------------------

	var elementosPanelAvisos = [
		{
			xtype:  'label',
			itemId: 'labelAviso',
			width:	'100%',
			style:  'font-weight:bold;text-align:center;margin:15px;color:black;'
		}
	];

	var panelAvisos = Ext.create(
		'Ext.panel.Panel',
		{
			xtype:		'panel',
			itemId:		'panelAvisos',
			hidden:		true,
			width:		450,
			title:		'Avisos',
			frame:		true,
			style:		'margin: 0 auto',
			bodyStyle:	'padding:10px',
			layout: {
				type: 'hbox',
				align: 'middle'
			},
			items:		elementosPanelAvisos
		}
	);

	//---------------------------------- CONTENEDOR PRINCIPAL ---------------------------------
	
	var main = Ext.create(
		'Ext.container.Container', 
		{
			id:		 	'contenedorPrincipal',
			renderTo: 	'areaContenido',
			//width: 		700,
			height: 		'auto',
			defaults: {
				margins: '0 5 5 0' // pad the layout from the window edges
			},
			items: [
				NE.util.getEspaciador(20),
				formaConsulta,
				formaDetalleCertificado
			]
		}
	);
	
	//-------------------------------- INICIALIZACION -----------------------------------
	revocacionCertificadoDigital( "INICIALIZACION", null );
	
});
