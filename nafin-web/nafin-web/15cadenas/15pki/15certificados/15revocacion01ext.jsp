<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<%@ include file="/15cadenas/15pki/certificado.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<%@ include file="/extjs4.jspf" %>
<%if(esEsquemaExtJS) {%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<%-- El jsp que contiene el dise�o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<%
String idMenuP =  request.getParameter("idMenu")==null?"":request.getParameter("idMenu");  
%>
<script language="JavaScript" src="/nafin/00utils/valida.js"></script>
<script type="text/javascript" src="15revocacion01ext.js"></script>

</head>

<% if(esEsquemaExtJS) { %>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<div id="_ObjFirma" style="display:none;">
	<%@ include file="/00utils/componente_firma4.jspf" %>
	</div>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">	
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
				<div id="areaContenido"></div>                                                                                     
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
		<form id='formParametros' name="formParametros">		
			<input type="hidden" id="idMenu" name="idMenu" value="<%=idMenuP%>"/>	
		</form>	
	</body>
<% } else { %>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<div id="_ObjFirma" style="display:none;">
	<%@ include file="/00utils/componente_firma4.jspf" %>
	</div>
				<div id="areaContenido"><div style="height:190px"></div></div>
	
	<form id='formAux' name="formAux" target='_new'></form>
	<form id='formParametros' name="formParametros">		
		<input type="hidden" id="idMenu" name="idMenu" value="<%=idMenuP%>"/>	
	</form>
<%}%>
</body>
</html>