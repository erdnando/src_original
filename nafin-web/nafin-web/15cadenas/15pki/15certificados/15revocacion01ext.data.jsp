<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.exception.*, 
		javax.naming.Context,
		com.netro.seguridadbean.*,
		com.netro.afiliacion.AfiliadoInfo"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%@ include file="/15cadenas/15pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

final	String 	EPO	= "E";
final String 	IF		= "I";
final String 	PYME	= "P";
	
if (        		informacion.equals("RevocacionCertificadoDigital.inicializacion")							)	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		estadoSiguiente 	= null;
	
	//** Fodea 021-2015 (E);
	com.netro.seguridadbean.Seguridad seguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			
	String idMenuP   = request.getParameter("idMenuP")==null?"":request.getParameter("idMenuP");	
	
		
	String  [] permisosSolicitados = { "AC_PAN_REVOCACIO" };       		
	List permisos =  seguridadBean.getPermisosPorMenu( iTipoPerfil, strPerfil, idMenuP, permisosSolicitados );  
	String msg =  "";
	
	if(permisos.size()==0)  {  
		msg = "No está permitido consultar este usuario, favor de revisar."; 
	} else {
		estadoSiguiente = "PRESENTAR_FORMA_CONSULTA";
	}
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente     	);
	resultado.put("msg", 	msg     	);
	infoRegresar = resultado.toString();
	
} else if (        informacion.equals("RevocacionCertificadoDigital.consultarCertificado")				)	{
	
	JSONObject		resultado			= new JSONObject();
	boolean			success				= true;
	String 			estadoSiguiente 	= null;
	String 			msg					= null;
	
	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
		
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){
 
		log.error("RevocacionCertificadoDigital.consultarCertificado(Exception): Ocurrió un error al obtener instancia del EJB de Seguridad.",e);
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}

	// Leer Parametros especificos
	String 		usuarioRevocador	= iNoUsuario;
	String 		usuario 				= (request.getParameter("usuario") == null)?"":request.getParameter("usuario");

	// VALIDACIONES
	String[] detalleError = new String[1];
	if( Comunes.esVacio(usuario) ){
		throw new AppException("La clave de usuario es requerida.");
	} else if( !Comunes.esLoginValido(usuario,detalleError) ){
		throw new AppException(detalleError[0]);
	}

	// REGISTRAR EN EL LOG QUIEN REALIZA LA CONSULTA
	
	log.info(
		"RevocacionCertificadoDigital.consultarCertificado: " +
			"Usuario: "              + usuarioRevocador + 
			" consulto detalle de "  + usuario          +  
			"."
	);
	
	// REALIZAR CONSULTA
	
	// Detalle de la consulta
	String 		nombreUsuario				= null;
	String 		correo						= null;
	String 		fechaVigencia				= null;
	Boolean 		tieneCertificado			= Boolean.FALSE;
	String 		razonSocialDependencia	= null;
	// Variables auxiliares
	boolean		existeUsuario				= false;
	boolean 		tieneFechaVigencia		= false;
	String 		claveAfiliado				= null;
	String 		tipoAfiliado				= null;
	
	// Obtener datos del usuario
	UtilUsr 		utilUsr 						= new UtilUsr();
	Usuario 		datosUsuario 				= utilUsr.getUsuario(usuario);
	
	// Revisar si existe el usuario
	if( datosUsuario == null || Comunes.esVacio(datosUsuario.getLogin()) ){
		existeUsuario  = false; 
	} else {
		existeUsuario  = true;
	}
	
	if( existeUsuario ){

		nombreUsuario 	= datosUsuario.getNombreCompleto();
		correo 			= datosUsuario.getEmail();
		claveAfiliado 	= datosUsuario.getClaveAfiliado();
		tipoAfiliado 	= datosUsuario.getTipoAfiliado();
		
		if( !Comunes.esNumeroEnteroPositivo(claveAfiliado) ){
			claveAfiliado 	= null;
		}
		
		if( Comunes.esVacio(tipoAfiliado) ){
			tipoAfiliado 	= null;
		}
		
	}

	// Consultar Fecha de Vigencia del Certificado
	if( existeUsuario ){
		fechaVigencia 			= seguridad.getFecVigenciaCertificado(usuario);
		tieneFechaVigencia	= Comunes.esVacio(fechaVigencia)?false:true;
		// En caso de que no cuente con fecha de vigencia desplegaró la leyenda: 
		// No cuenta con Certificado Digital
		if( !tieneFechaVigencia ){
			fechaVigencia = "No cuenta con Certificado Digital";
		}
	}

	// Revisar si tiene certificado
	if( tieneFechaVigencia ){
		tieneCertificado = Boolean.TRUE;
	}

	// Obtener razon social de la dependencia
	if( !Comunes.esVacio(claveAfiliado) && !Comunes.esVacio(tipoAfiliado) ){
		razonSocialDependencia = AfiliadoInfo.getNombreAfiliado(Integer.parseInt(claveAfiliado), tipoAfiliado);
	}
		
	// ENVIAR DETALLE DE LA CONSULTA
	
	// Validar que el usuario ingresado este registrado en el OID
	if( !existeUsuario ){
		
		msg 					= "El usuario no existe.";
		resultado.put("conservarParametros",Boolean.TRUE);
		
		estadoSiguiente 	= "PRESENTAR_FORMA_CONSULTA";
		
	// Validar que el usuario ingresado tenga un perfil PYME o EPO o IF
	} else if(
		!EPO.equals(tipoAfiliado)
			&&
		!IF.equals(tipoAfiliado)
			&&
		!PYME.equals(tipoAfiliado)
	){
	
		msg 					= "No está permitido consultar este usuario, favor de revisar.";
		resultado.put("conservarParametros",Boolean.TRUE);
		
		estadoSiguiente 	= "PRESENTAR_FORMA_CONSULTA";
		
	} else {
		
		JSONObject detalle = new JSONObject();
		detalle.put("usuario",						usuario						); 			
		detalle.put("nombreUsuario",				nombreUsuario				);			
		detalle.put("correo",						correo						);					
		detalle.put("fechaVigencia",				fechaVigencia				);			
		detalle.put("tieneCertificado",			tieneCertificado			);		
		detalle.put("razonSocialDependencia",	razonSocialDependencia	);
		resultado.put("detalle",detalle);
		
		estadoSiguiente = "PRESENTAR_DETALLE_CERTIFICADO";
		
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente     	);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if (        informacion.equals("RevocacionCertificadoDigital.prepararRevocacionCertificado")	)	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		estadoSiguiente 	= null;
	
	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
		
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){
 
		log.error("RevocacionCertificadoDigital.prepararRevocacionCertificado(Exception): Ocurrió un error al obtener instancia del EJB de Seguridad.",e);
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}

	// Leer Parametros especificos
	String 		usuarioRevocador			= iNoUsuario;
	String 		nombreUsuarioRevocador	= strNombreUsuario;
	String 		usuario 						= (request.getParameter("usuario") 						== null)?"":request.getParameter("usuario");
	String 		razonSocialDependencia 	= (request.getParameter("razonSocialDependencia")	== null)?"":request.getParameter("razonSocialDependencia");
	String 		nombreUsuario 				= (request.getParameter("nombreUsuario")				== null)?"":request.getParameter("nombreUsuario");
	String 		correo 						= (request.getParameter("correo")						== null)?"":request.getParameter("correo");
	String 		fechaVigencia 				= (request.getParameter("fechaVigencia")				== null)?"":request.getParameter("fechaVigencia");
	
	// Preparar texto a firmar
	StringBuffer textoFirmar				= new StringBuffer(256);
	textoFirmar.append(
		"Datos del usuario que solicitó la revocación:\n"  +
		"\n"  +
		"Razón Social de la Dependencia: " + razonSocialDependencia + "\n"  +
		"Usuario: " + usuario + "\n"  +
		"Nombre Usuario: " + nombreUsuario + "\n"  +
		"Correo: " + correo + "\n"  +
		"Fecha de Vigencia: " + fechaVigencia + "\n"  +
		"\n"  +
		"Datos del usuario que realizó la revocación:\n"  +
		"\n"  +
		"Usuario: " + usuarioRevocador + "\n"  +
		"Nombre Usuario: " + nombreUsuarioRevocador
	);
	resultado.put("textoFirmar",	textoFirmar.toString()	);
	resultado.put("usuario",		usuario						);
	estadoSiguiente = "REVOCAR_CERTIFICADO";

	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente     	);
	infoRegresar = resultado.toString();
	
} else if (        informacion.equals("RevocacionCertificadoDigital.revocarCertificado")					)	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	String 		estadoSiguiente 	= null;
	String 		msg					= null;
	
	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad = null;
	try {
		
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			
	}catch(Exception e){

		log.error("RevocacionCertificadoDigital.revocarCertificado(Exception): Ocurrió un error al obtener instancia del EJB de Seguridad.",e);
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
	}
	
	// OBTENER PARAMETROS
	
	// Obtener datos del usuario que realiza la acción
	String 		usuarioRevocador			= iNoUsuario;
	String 		nombreUsuarioRevocador	= strNombreUsuario;
	// Leer Parametros especificos
	String 		usuario 						= (request.getParameter("usuario") 			== null)?"":request.getParameter("usuario");
	String 		textoFirmado 				= (request.getParameter("textoFirmado")	== null)?"":request.getParameter("textoFirmado");
	String 		pkcs7 						= (request.getParameter("pkcs7")				== null)?"":request.getParameter("pkcs7");
	// Variables auxiliar
	String 		externContent 				= textoFirmado;
	char 			getReceipt 					= 'Y';
	String 		folioCert   				= null;
	String 		acuseFirmaDigital			= null;
	boolean 		doRevocarCertificado		= false;
	boolean		exitoOperacion				= false;

	// VALIDAR PARAMETROS PROPORCIONADOS
	
	// Obtener datos del usuario
	UtilUsr 		utilUsr 						= new UtilUsr();
	Usuario 		datosUsuario 				= utilUsr.getUsuario(usuario);
	String 		tipoAfiliado 				= null;
	
	// Validar que el usuario exista
	if( datosUsuario == null || Comunes.esVacio(datosUsuario.getLogin()) ){
		// Registrar en el log intento de revocación no permitido
		log.info(
			"RevocacionCertificadoDigital.revocarCertificado: " +
			"Usuario: "                                             + usuarioRevocador  + 
			" intentó revocar certificado de usuario inexistente "  + usuario           +
			"."
		);
		throw new AppException("El usuario proporcionado no es válido.");
	}
	// Obtener datos de usuario revocador
	HashMap 		datosRevocador				= new HashMap();
	datosRevocador.put("usuario",			usuarioRevocador);
	datosRevocador.put("nombreUsuario",	nombreUsuarioRevocador);
	// Validar que el usuario ingresado tenga un perfil PYME o EPO o IF
	tipoAfiliado = datosUsuario.getTipoAfiliado();
	if(
		!EPO.equals(tipoAfiliado)
			&&
		!IF.equals(tipoAfiliado)
			&&
		!PYME.equals(tipoAfiliado)
	){
		// Registrar en el log intento de revocación no permitido
		log.info(
			"RevocacionCertificadoDigital.revocarCertificado: " +
			"Usuario: "                                             + usuarioRevocador  + 
			" intentó revocar certificado de usuario no permitido " + usuario           +
			"."
		);
		throw new AppException("No está permitido revocar este usuario.");
	}
	
	// AUTENTICAR FIRMA DIGITAL
	
	// Cancelar la operación si el usuario canceló la firma digital
	if (
			!Comunes.esVacio(_serial)	
				&&
			(
				Comunes.esVacio(pkcs7)	
					||
				Comunes.esVacio(externContent)
			)
	){
		
		msg					= "";
		exitoOperacion		= false;
		
	// Para poder revocar el certificado, primero se deberá autenticar firma la digital
	} else if ( 

		!Comunes.esVacio(pkcs7)	
			&&
		!Comunes.esVacio(externContent)
			&&
		!Comunes.esVacio(_serial)	
		
	) {
 
		folioCert   								= "R"+usuarioRevocador+new SimpleDateFormat("ddMMyyHHmm").format(new java.util.Date());
		netropology.utilerias.Seguridad 	s	= new netropology.utilerias.Seguridad();

		try {
			
			// Autenticar texto firmado
			if (!s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {	
				
				msg 						= "La autentificación no se llevó a cabo: \n"+s.mostrarError();
				exitoOperacion			= false;
				
			} 	else {
				
				// Obtener acuse de la firma digital
				acuseFirmaDigital 	= s.getAcuse();
				// Realizar revocación del certificado
				doRevocarCertificado	= true;

			}

		} catch(Exception e) {
			
			log.error("RevocacionCertificadoDigital.revocarCertificado(Exception)",e);
			msg					= "Ocurrió un error inesperado: " + e.getMessage();
			exitoOperacion		= false;
		}
		
	// Lanzar error si no se cumplen los parametros requeridos para realizar revocación
	} else {
	
		try {
			throw new NafinException("GRAL0021");	
		}catch(Exception e){
			msg					= e.getMessage();
			exitoOperacion		= false;
		}
		
	}

	// REVOCAR CERTIFICADO
	
	// Realizar revocación del certificado
	if( doRevocarCertificado ){
		seguridad.revocarCertificadoDigital(datosUsuario,datosRevocador);
		// Registrar en el log quien realiza la revocación:
		log.info(
			"RevocacionCertificadoDigital.revocarCertificado: " +
			"Usuario: "               + usuarioRevocador  + 
			" revocó certificado de " + usuario           +  
			"; folioCert: "           + folioCert         +
			"; acuseFirmaDigital: "   + acuseFirmaDigital + 
			"."
		);
		msg = "El Certificado Digital ha sido revocado exitosamente";
		exitoOperacion = true;
	}
	
	// Determinar estado siguiente
	if( exitoOperacion ){
		estadoSiguiente = "PRESENTAR_FORMA_CONSULTA";
	} else {
		estadoSiguiente = "PRESENTAR_DETALLE_CERTIFICADO";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente     	);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();

} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%><%=infoRegresar%>