var showPanelLayoutDeCarga;

Ext.onReady(function() {
	
	//-------------------------------- VALIDACIONES -----------------------------------
	
	Ext.apply(Ext.form.VTypes, {
		archivotxt: function(v) {
			var myRegex = /^.+\.([tT][xX][tT])$/;
			return myRegex.test(v);
		},
		archivotxtText: 'El formato del archivo de origen no es el correcto. Formato Soportado: TXT.'
	});
	
	//----------------------------------- HANDLERS ------------------------------------
	
	var procesaValoresIniciales = function(opts, success, response) {
		
		if ( success == true && Ext.util.JSON.decode(response.responseText).success == true ) {
 
			var resp 		= Ext.util.JSON.decode(response.responseText);
			
			// Desbloquear la pantalla para que pueda ser consultada por el usuario.
			if(resp.permisoAgregarCuentas){
				Ext.getCmp("contenedorPrincipal").enable();
			// No hay permiso para agregar las cuentas, bloquear la pantalla
			} else {
				Ext.getCmp('cmbEpo').setValue('');
				var msg = Ext.isEmpty(resp.msg)?"Ocurri� un error inesperado al validar acceso a la pantalla.":resp.msg;	
				Ext.Msg.alert("Aviso", msg );
			}
 
		} else {
 
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
 
		}
		
	}
	function mostrarArchivoTXT(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			panelFormaCargaArhivo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			panelFormaCargaArhivo.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesaCargaMasiva = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaMasiva(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaMasiva(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
 
	 var confirmar = function(pkcs7, textoFirmar, proceso ,  totalRegistros , cveEPO){
	 
	 if (Ext.isEmpty(pkcs7)) {			
			return;
		}else  {			
						
			Ext.Ajax.request({
				url: 						'15cmasiva01ext.data.jsp',
				params: {	
					accion:				'CargaMasiva.insertarCuentas',
					proceso:				proceso,
					totalRegistros:	totalRegistros,
					isEmptyPkcs7: 		Ext.isEmpty(pkcs7),
					pkcs7: 				pkcs7,
					textoFirmado: 		textoFirmar,
					claveEpo: cveEPO
				},
				callback: 	procesaCargaMasiva
			});
	 
		}
	 
	 }
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	
	var cargaMasiva = function(estado, respuesta ){
		var cveEPO = Ext.getCmp('cmbEpo').getValue();
 
		if(			estado == "SUBIR_ARCHIVO"					){
			
			Ext.getCmp("panelFormaCargaArhivo").getForm().submit({
				clientValidation: 	true,
				url: 						'15cmasiva01ext.data.jsp?accion=CargaMasiva.subirArchivo',
				waitMsg:   				'Subiendo archivo...',
				waitTitle: 				'Por favor espere',
				success: function(form, action) {
					 procesaCargaMasiva(null,  true,  action.response );
				},
				failure: function(form, action) {
				 	 action.response.status = 200;
					 procesaCargaMasiva(null,  false, action.response );
				}
			});
			
		} else if(	estado == "VALIDA_CARACTERES_CONTROL"				){
	      var proceso	= respuesta.proceso;
			var totalRegistros	= respuesta.totalRegistros;
			var nombreArchivo	= respuesta.nombreArchivo;
			Ext.getCmp('guardaProceso').setValue(proceso);
			Ext.getCmp('guardaTotalRegistros').setValue(totalRegistros);
			Ext.Ajax.request({
				url: 		'15cmasiva01ext.data.jsp',
				params: 	{
					accion:				'CargaMasiva.validaCaracteresControl',
					nombreArchivo: 			nombreArchivo
				},
				callback: 				procesaCargaMasiva
			});
		
		} else if(	estado == "MOSTRAR_AVANCE_THREAD"				){
			Ext.Ajax.request({
				url: 		'15cmasiva01ext.data.jsp',
				params: 	{
					accion:				'CargaMasiva.avanceThreadCaracteres'
				},
				callback: 				procesaCargaMasiva
			});
		
		}else if(	estado == "HAY_CARACTERES_CONTROL"				){
				 Ext.getCmp("panelFormaCargaArhivo").hide();
				var mensaje	= respuesta.mns;
				var nombreArchivoCaractEsp	= respuesta.nombreArchivoCaractEsp;
				Ext.getCmp('nombArchivoDetalle').setValue(nombreArchivoCaractEsp);
				panelFormaCargaArhivo.hide();
				caracteresEspeciales.show();
				caracteresEspeciales.setMensaje(mensaje);
		}else if(	estado == "ERROR_THREAD_CARACTERES"				){
				var mensaje	= respuesta.mns;
				Ext.Msg.alert("Error", mensaje );
				return;
		}else if(	estado == "DESCARGAR_ARCHIVO"				){
		
			var archivo = Ext.getCmp('nombArchivoDetalle').getValue();
			Ext.Ajax.request({
				url: 		'15cmasiva01ext.data.jsp',
				params: 	{
					accion:				'descargaArchivoDetalle',
					archivo : archivo
				},
				callback: 				mostrarArchivoTXT
			});
		}else if(	estado == "REVISAR_CUENTAS"				){
			
		   
			Ext.Ajax.request({
				url: 		'15cmasiva01ext.data.jsp',
				params: 	{
					accion:				'CargaMasiva.revisarCuentas',
					proceso: 			Ext.getCmp('guardaProceso').getValue(),
					totalRegistros:	Ext.getCmp('guardaTotalRegistros').getValue(),
					claveEpo: cveEPO
				},
				callback: 				procesaCargaMasiva
			});
		
		} else if(	estado == "MOSTRAR_AVANCE_REVISION"		){
			
			var proceso				= respuesta.proceso;
         var totalRegistros	= respuesta.totalRegistros;
         
         showWindowAvanceValidacion(totalRegistros);
         
			Ext.Ajax.request({
				url: 		'15cmasiva01ext.data.jsp',
				params: 	{
					accion:				'CargaMasiva.mostrarAvanceRevision',
					proceso: 			proceso,
					totalRegistros:	totalRegistros
				},
				callback: 				procesaCargaMasiva
			});
			
		} else if(	estado == "ACTUALIZAR_AVANCE_REVISION"	){
			
			var proceso							= respuesta.proceso;
         var totalRegistros				= respuesta.totalRegistros;
         var avance							= respuesta.avance;
         var textoPorcentajeAvance		= respuesta.textoPorcentajeAvance;
         var registrosProcesados			= respuesta.registrosProcesados;
         var completado						= respuesta.completado;
         
         var barrarAvanceValidacion		= Ext.getCmp("barrarAvanceValidacion");
			var valorRegistrosProcesados	= Ext.getCmp("valorRegistrosProcesados");
 
			barrarAvanceValidacion.updateProgress( Number(avance), textoPorcentajeAvance, true);
			valorRegistrosProcesados.setText(registrosProcesados);
			
			setTimeout(
				function(){
					Ext.Ajax.request({
						url: 		'15cmasiva01ext.data.jsp',
						params: 	{
							accion:				'CargaMasiva.actualizarAvanceRevision',
							proceso: 			proceso,
							totalRegistros:	totalRegistros,
							completado:			completado,
							claveEpo: cveEPO
						},
						callback: 				procesaCargaMasiva
					});
					return;
				},
				1250
			);
			
		} else if(	estado == "MOSTRAR_RESULTADO_REVISION"	){
			
			var proceso							= respuesta.proceso;
         var totalRegistros				= respuesta.totalRegistros;
         
         var totalSolicitudesCorrectas	= respuesta.totalSolicitudesCorrectas;
         var totalSolicitudesConError	= respuesta.totalSolicitudesConError;
         var mostrarBotonContinuar		= respuesta.mostrarBotonContinuar;
         
         // Recordar numero de proceso y el total de registros
         Ext.getCmp("panelResultadosValidacion.proceso").setValue(proceso);
			Ext.getCmp("panelResultadosValidacion.totalRegistros").setValue(totalRegistros);
			
         // Ocultar ventana de avance validacion
         var ventanaAvanceValidacion = Ext.getCmp('windowAvanceValidacion');
         if (ventanaAvanceValidacion) {
         	ventanaAvanceValidacion.hide();
         }
         
         // Ocutar forma de carga de archivo
         Ext.getCmp("panelFormaCargaArhivo").hide();
         
         // Ocutar layout de ayuda
         hidePanelLayoutDeCarga();
         
         // Inicializar panel de resultados
         Ext.getCmp('totalSolicitudesCorrectas').setValue(totalSolicitudesCorrectas);
         Ext.getCmp('totalSolicitudesConError').setValue(totalSolicitudesConError);
         Ext.getCmp('botonGenerarArchivoTXTErrores').enable();
         Ext.getCmp('botonBajarArchivoTXTErrores').hide();
         Ext.getCmp('totalSolicitudesProcesadas').setValue(totalRegistros);
         
         if(mostrarBotonContinuar){
         	Ext.getCmp('botonContinuarInsercion').enable();
         }else{
         	Ext.getCmp('botonContinuarInsercion').disable();
         }
         
         // Remover contenido anterior de los grids
         Ext.StoreMgr.key('cuentasCorrectasDataStore').removeAll();
         Ext.StoreMgr.key('cuentasConErroresDataStore').removeAll();
 
         // Mostrar Panel de resultados
         Ext.getCmp('panelResultadosValidacion').show();
         
         // Actualizar contenidos de los grids
         var tabPanelResultadosValidacion = Ext.getCmp('tabPanelResultadosValidacion');
         tabPanelResultadosValidacion.setActiveTab(1);
         Ext.StoreMgr.key('cuentasConErroresDataStore').load({
         	params: 	{
					proceso: 			proceso,
					totalRegistros:	totalRegistros
				}
         }); 
         tabPanelResultadosValidacion.setActiveTab(0);
         Ext.StoreMgr.key('cuentasCorrectasDataStore').load({
         	params: 	{
					proceso: 			proceso,
					totalRegistros:	totalRegistros
				}
         });
 
         // Determinar el estado siguiente
         Ext.Ajax.request({
				url: 		'15cmasiva01ext.data.jsp',
				params: 	{
					accion:				'CargaMasiva.mostrarResultadoRevision',
					proceso: 			proceso,
					totalRegistros:	totalRegistros
				},
				callback: 				procesaCargaMasiva
			});
		  
      } else if(  estado == "ESPERAR_DECISION"  			){
      	
      	// Este es el unico estado que no pasa por el jsp, para determinar
      	// el estado siguiente... se hace as� para reducir la complejidad del 
      	// codigo
      	return;
      	
		} else if(  estado == "GENERAR_TEXTO_FIRMAR" 		){
			
			var proceso 			= Ext.getCmp("panelResultadosValidacion.proceso").getValue();
			var totalRegistros 	= Ext.getCmp("panelResultadosValidacion.totalRegistros").getValue();
			
			// Determinar el estado siguiente
         Ext.Ajax.request({
				url: 						'15cmasiva01ext.data.jsp',
				params: 	{
					accion:				'CargaMasiva.generarTextoFirmar',
					proceso: 			proceso,
					totalRegistros:	totalRegistros
				},
				callback: 				procesaCargaMasiva
			});
			
		} else if(	estado == "INSERTAR_CUENTAS"				){
			
			var proceso							= respuesta.proceso;
         var totalRegistros				= respuesta.totalRegistros;
         
         var textoFirmar					= respuesta.textoFirmar;
		 		
			
		NE.util.obtenerPKCS7(confirmar, textoFirmar, proceso ,  totalRegistros , cveEPO   );
			
         
		} else if(	estado == "MOSTRAR_CUENTAS_INSERTADAS"	){
 
			var proceso							= respuesta.proceso;
         var totalRegistros				= respuesta.totalRegistros;
         
         var insercionExitosa				= respuesta.insercionExitosa;
         var autentificacion				= respuesta.autentificacion;
         
         // Inicializar campos
         var labelAutentificacion 		= Ext.getCmp('labelAutentificacion');
         var gridCuentasAgregadas 		= Ext.getCmp('gridCuentasAgregadas');
         var cuentasAgregadasDataStore	= Ext.StoreMgr.key("cuentasAgregadasDataStore");
         if(insercionExitosa){
 
         	labelAutentificacion.setText(autentificacion,false);
         	
         	// Actualizar el contenido del grid de cuentas agregadas
         	cuentasAgregadasDataStore.removeAll();
         	gridCuentasAgregadas.show(); 
         	cuentasAgregadasDataStore.load({
         			params: {
         				accion: 				'ConsultaCuentasInsertadas',
         				proceso: 			proceso,
         				totalRegistros: 	totalRegistros
         			}
         	});
         	
         } else {
         	 
         	labelAutentificacion.setText("<span style=\"color:red\">" + autentificacion + "</span>",false);
         	
         	// Remover cualquier contenido que pudiera tener el grid de cuentas agregadas
         	cuentasAgregadasDataStore.removeAll();
         	gridCuentasAgregadas.hide();
         	
         }
         
         // Mostrar Panel de resultados
         Ext.getCmp('panelResultadosValidacion').hide();
  
         // Mostrar Panel con el Acuse de cuentas cargadas
         Ext.getCmp('panelAcuseCargaCuentas').show();
         
         // Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'15cmasiva01ext.data.jsp',
				params: {	
					accion:				'CargaMasiva.mostrarCuentasInsertadas',
					proceso:				proceso,
					totalRegistros:	totalRegistros
				},
				callback: 				procesaCargaMasiva
			});
			
		} else if(	estado == "FIN"								){
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "15cmasiva01ext.jsp?accion=CargaMasiva.fin";
			forma.target	= "_self";
			forma.submit();
 
		}else if(	estado == "MENSAJE_CODIFICACION"								){
			new NE.cespcial.AvisoCodificacionArchivo({codificacion:respuesta.codificacionArchivo}).show();
 
		}
		
		return;
		
	}
	
	//-------------------------- PANEL ACUSE CARGA CUENTAS ----------------------------
	
	var procesarConsultaCuentasAgregadas = function(store, registros, opts){

		var gridCuentasAgregadas 					= Ext.getCmp('gridCuentasAgregadas');
		
		if (registros != null) {
			
			var el 										= gridCuentasAgregadas.getGridEl();
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}

	}
	
	var cuentasAgregadasData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'cuentasAgregadasDataStore',
		url: 		'15cmasiva01ext.data.jsp',
		baseParams: {
			accion: 'ConsultaCuentasAgregadas'
		},
		fields: [
			{name: 'NOMBRE_PYME'  	},
			{name: 'CUENTA' 			},
			{name: 'NOMBRE_MONEDA' 	},
			{name: 'ESTATUS'			}
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarConsultaCuentasAgregadas,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCuentasAgregadas(null, null, null);						
				}
			}
		}
		
	});
	
	var elementosAcuseCargaCuentas = [
		{
			xtype: 	'label',
			id:	 	'labelAutentificacion',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:15px;',
			html:  	'La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br>Recibo: 347374872'
		},
		{
			store: 		cuentasAgregadasData,
			xtype: 		'grid',
			id:			'gridCuentasAgregadas',
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			style: {
				borderWidth: 1
			},
			columns: [
				{
					header: 		'PyME',
					tooltip: 	'PyME',
					dataIndex: 	'NOMBRE_PYME',
					sortable: 	true,
					resizable: 	true,
					width: 		305,
					hidden: 		false
				},
				{
					header: 		'Cuenta CLABE / Swift',
					tooltip: 	'Cuenta CLABE / Swift',
					dataIndex: 	'CUENTA',
					sortable: 	true,
					resizable: 	true,
					width: 		150,
					hidden: 		false
				},
				{
					header: 		'Moneda',
					tooltip: 	'Moneda',
					dataIndex: 	'NOMBRE_MONEDA',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					hidden: 		false
				},
				{
					header: 		'Estatus',
					tooltip: 	'Estatus',
					dataIndex: 	'ESTATUS',
					sortable: 	true,
					resizable: 	true,
					width: 		100,
					hidden: 		false
				}
			]
		},
		{
			xtype: 		'panel',
			width: 		'100%',
			style: 		'margin: 8px;',
			layout: {
				type: 'hbox',
				pack: 'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		80,
					text: 		'Aceptar',
					iconCls: 	'icoAceptar',
					id: 			'botonAceptar',
					handler:    function(boton, evento) {
						cargaMasiva("FIN",null);
					}
				}
			] 
		}
	];
	
	var panelAcuseCargaCuentas = {
		title:			'Acuse de la Carga Masiva de Cuentas',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelAcuseCargaCuentas',
		width: 			700, //949,
		frame: 			true,
		style: 			'margin: 0 auto',
		items: 			elementosAcuseCargaCuentas
	}
	
	//------------------------- PANEL RESULTADOS VALIDACION ---------------------------
	
	var procesarSuccessFailureGenerarArchivoTXTErrores = function(opts, success, response){
		
		var botonGenerarArchivoTXTErrores 	= Ext.getCmp('botonGenerarArchivoTXTErrores');
		botonGenerarArchivoTXTErrores.setIconClass('');
		
		var botonBajarArchivoTXTErrores 		= Ext.getCmp('botonBajarArchivoTXTErrores');
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if ( success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			botonBajarArchivoTXTErrores.show();
			botonBajarArchivoTXTErrores.enable();
			botonBajarArchivoTXTErrores.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			botonBajarArchivoTXTErrores.setHandler( function(boton, evento) {
				//var forma 		= Ext.getDom('formAux');
				//forma.action 	= Ext.util.JSON.decode(response.responseText).urlArchivo;
				//forma.submit();
				var params = {nombreArchivo: Ext.util.JSON.decode(response.responseText).urlArchivo};
				var urlArchivo =  NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				var fp = Ext.getCmp('panelFormaCargaArhivo');
				fp.getForm().getEl().dom.action = urlArchivo;
				fp.getForm().getEl().dom.submit();
				
			});
			
		} else {
			
			botonGenerarArchivoTXTErrores.enable();
			botonBajarArchivoTXTErrores.disable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarConsultaCuentasCorrectas 	= function(store, registros, opts){
		
		var gridCuentasCorrectas 						= Ext.getCmp('gridCuentasCorrectas');
		
		if (registros != null) {
			
			var el 							= gridCuentasCorrectas.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var procesarConsultaCuentasConErrores 	= function(store, registros, opts){
		
		var gridCuentasConErrores 					= Ext.getCmp('gridCuentasConErrores');
		
		if (registros != null) {

			var botonGenerarArchivoTXTErrores 	= Ext.getCmp('botonGenerarArchivoTXTErrores');
			var botonBajarArchivoTXTErrores 		= Ext.getCmp('botonBajarArchivoTXTErrores');
			
			var el 										= gridCuentasConErrores.getGridEl();
			
			if(store.getTotalCount() > 0) {
				
				botonGenerarArchivoTXTErrores.enable();
				botonBajarArchivoTXTErrores.disable();
				
				el.unmask();
				
			} else {
				
				botonGenerarArchivoTXTErrores.disable();
				botonBajarArchivoTXTErrores.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var cuentasCorrectasData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'cuentasCorrectasDataStore',
		url: 		'15cmasiva01ext.data.jsp',
		baseParams: {
			accion: 'ConsultaCuentasCorrectas'
		},
		fields: [
			{name: 'LINEA',  	convert: function(value, record){ return Number(value); } },
			{name: 'CUENTA' }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarConsultaCuentasCorrectas,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCuentasCorrectas(null, null, null);						
				}
			}
		}
		
	});
	
	var cuentasConErroresData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'cuentasConErroresDataStore',
		url: 		'15cmasiva01ext.data.jsp',
		baseParams: {
			accion: 'ConsultaCuentasConErrores'
		},
		fields: [
			{name: 'LINEA',  				convert: function(value, record){ return Number(value); } },
			{name: 'DESCRIPCION_ERROR' }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			load: 	procesarConsultaCuentasConErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaCuentasConErrores(null, null, null);						
				}
			}
		}
		
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 			'catalogoEPODataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'15cmasiva01ext.data.jsp',
		baseParams: {
			accion: 'CatalogoEPO'
		},
		totalProperty: 'total',
		autoLoad: 		true
	});
	
	var elementosResultadosValidacion = [
		
		{ 
			xtype: 			'tabpanel',
			id:				'tabPanelResultadosValidacion',
			activeTab:		0, //0,
			plain:			true,
			defaults:{ 
				layout:		'fit',
				border:		false,
				autoScroll: true
			},
			items: [
				{
					title: 		'Solicitudes Correctas',
					store: 		cuentasCorrectasData,
					xtype: 		'grid',
					id:			'gridCuentasCorrectas',
					stripeRows: true,
					loadMask: 	true,
					height: 		400,
					width: 		'100%',//943,		
					frame: 		false,
					columns: [
						{
							header: 		'L�nea',
							tooltip: 	'N�mero de L�nea',
							dataIndex: 	'LINEA',
							sortable: 	true,
							resizable: 	true,
							width: 		50,
							hidden: 		false
						},
						{
							header: 		'Cuenta',
							tooltip: 	'Cuenta',
							dataIndex: 	'CUENTA',
							sortable: 	true,
							resizable: 	true,
							width: 	600,
							hidden: 		false
						}
					],
					bbar: {
						xtype: 'toolbar',
						buttonAlign: 'left',
						items: [
							{
								xtype: 	'label',
								html: 	'Total',
								style: 	'padding: 4px'
							},	
							{
								xtype: 		'textfield',
								readOnly:  	true,
								style:		'text-align: right;',
								id:			'totalSolicitudesCorrectas',
								width:		'75',
								value: 		'0'
							}
						]
					}
				},
				{
					title: 		'Solicitudes con Errores',
					store: 		cuentasConErroresData,
					xtype: 		'grid',
					id:			'gridCuentasConErrores',
					stripeRows: true,
					loadMask: 	true,
					height: 		400,
					width: 		'100%',//943,		
					frame: 		false,
					columns: [
						{
							header: 		'L�nea',
							tooltip: 	'N�mero de L�nea',
							dataIndex: 	'LINEA',
							sortable: 	true,
							resizable: 	true,
							width: 		50,
							hidden: 		false
						},
						{
							header: 		'Descripci�n del Error',
							tooltip: 	'Descripci�n del Error',
							dataIndex: 	'DESCRIPCION_ERROR',
							sortable: 	true,
							resizable: 	true,
							width: 		850,
							hidden: 		false
						}
					],
					bbar: {
						xtype: 'toolbar',
						buttonAlign: 'left',
						items: [
							{
								xtype: 	'label',
								html: 	'Total',
								style: 	'padding: 4px'
							},	
							{
								xtype: 		'textfield',
								readOnly:  	true,
								style:		'text-align: right;',
								id:			'totalSolicitudesConError',
								width:		'75',
								value: 		'0'
							},
							'->',
							'-',
							{
								xtype: 	'button',
								text: 	'Generar Archivo',
								id: 		'botonGenerarArchivoTXTErrores',
								handler: function(boton, evento) {
								
									boton.disable();
									boton.setIconClass('loading-indicator');
									
									// Copiar los parametros base
									var proceso 		 = Ext.getCmp("panelResultadosValidacion.proceso").getValue();
									var totalRegistros = Ext.getCmp("panelResultadosValidacion.totalRegistros").getValue();
									
									// Generar Archivo TXT
									Ext.Ajax.request({
										url: 			'15cmasiva01ext.data.jsp',
										params: 		{ 
											accion: 				'GenerarArchivoTXTErrores',
											proceso:				proceso,
											totalRegistros: 	totalRegistros
										},
										callback: 	procesarSuccessFailureGenerarArchivoTXTErrores
									});
							
								}
							},
							{
								xtype: 	'button',
								text: 	'Bajar Archivo',
								id: 		'botonBajarArchivoTXTErrores',
								hidden: 	true
							}
						]
					}
				}
			]
		},
		{
			xtype: 		'panel',
			width: 		'100%',
			style: 		'margin:  3px; margin-left: 0px; margin-top: 5px;',
			layout: {
				type: 'hbox',
				pack: 'start'
			},
			items: [
				{
					xtype: 	'label',
					html: 	'Solicitudes Procesadas Totales',
					style: 	'padding: 4px; padding-left: 0px;'
				},	
				{
					xtype: 		'textfield',
					readOnly:  	true,
					style:		'text-align: right;',
					id:			'totalSolicitudesProcesadas',
					width:		'75',
					value: 		'0'
				}
			]
		},
		{
			xtype: 		'panel',
			width: 		'100%',
			style: 		'margin: 8px;',
			layout: {
				type: 'hbox',
				pack: 'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		80,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonCancelarInsercion',
					margins: 	' 3',
					handler:    function(boton, evento) {
						cargaMasiva("FIN",null);
					}
				},
				{
					xtype: 		'button',
					width: 		80,
					text: 		'Continuar',
					iconCls: 	'icoContinuar',
					id: 			'botonContinuarInsercion',
					margins: 	' 3',
					handler:    function(boton, evento) {
						cargaMasiva("GENERAR_TEXTO_FIRMAR",null);
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.proceso',
        id: 	'panelResultadosValidacion.proceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.totalRegistros',
        id: 	'panelResultadosValidacion.totalRegistros'
      }
 
	];
	
	var panelResultadosValidacion = {
		title:			'Resultado de la Validaci�n',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosValidacion',
		width: 			700, //949,
		frame: 			true,
		style: 			'margin:  0 auto',
		bodyStyle:		'padding: 0px',
		items: 			elementosResultadosValidacion,
		tools: [
			{
				id:			'ayuda1',
				handler:		 function(event, toolEl, panel){
					showPanelLayoutDeCarga();
				}
			}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-ayudaLayout">&#160</div>',
        '</tpl>'
      )
	}
	
	//--------------------------- PANEL AVANCE VALIDACION -----------------------------
	var elementosAvanceValidacion = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
			text: 	'Revisando cuentas...'
		},
		{
			xtype: 'progress',
			id:	 'barrarAvanceValidacion',
			name:	 'barrarAvanceValidacion',
			cls:	 'center-align',
			value: 0.3756,
			text:  '37.56 %'
		},
		{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				pack: 'start'
			},
			items: [
				{
					xtype: 	'label',
					width: 	'130',
					cls: 		'x-form-item',
					text: 	'Registros Procesados:'
				},
				{
					xtype: 	'label',
					id:		'valorRegistrosProcesados',
					name:		'valorRegistrosProcesados',
					cls: 		'x-form-item',
					style:	{
						textAlign: 'right'
					}
				}
			]
		},
		{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				pack: 'start'
			},
			items: [
				{
					xtype: 	'label',
					width: 	'130',
					cls: 		'x-form-item',
					text: 	'Total de Registros:'
				},
				{
					xtype: 	'label',
					id:		'valorTotalRegistros',
					name:		'valorTotalRegistros',
					cls: 		'x-form-item'
				}
			]
		}
	];
	
	var panelAvanceValidacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelAvanceValidacion',
		frame: 			true,
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		items: 			elementosAvanceValidacion
	});
	
	var showWindowAvanceValidacion = function(numeroRegistros){
		
		var barrarAvanceValidacion		= Ext.getCmp("barrarAvanceValidacion");
		var valorRegistrosProcesados	= Ext.getCmp("valorRegistrosProcesados");
		var valorTotalRegistros			= Ext.getCmp("valorTotalRegistros");
 
		barrarAvanceValidacion.updateProgress(0.00,"0.00 %",true);
		valorTotalRegistros.setText(numeroRegistros); 
		valorRegistrosProcesados.setText("0");
			
		var ventana = Ext.getCmp('windowAvanceValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				resizable:		false,
				id: 				'windowAvanceValidacion',
				modal:			true,
				closable: 		false,
				listeners: 		{
					show: function(component){
						var valorRegistrosProcesados	= Ext.getCmp("valorRegistrosProcesados");
						var valorTotalRegistros			= Ext.getCmp("valorTotalRegistros");
						valorRegistrosProcesados.setWidth(valorTotalRegistros.getWidth());
					}
				},
				items: [
					Ext.getCmp("panelAvanceValidacion")
				]
			}).show();
			
		}
		
	}	
	
	//---------------------------- PANEL LAYOUT DE CARGA ------------------------------
	var hidePanelLayoutDeCarga = function(){
		Ext.getCmp('panelLayoutDeCarga').hide();
		Ext.getCmp('espaciadorPanelLayoutDeCarga').hide();
	}
	
	showPanelLayoutDeCarga = function(){
		Ext.getCmp('panelLayoutDeCarga').show();
		Ext.getCmp('espaciadorPanelLayoutDeCarga').show();
	}
	
	                             
	var elementosLayoutDeCarga = [
		{
			xtype: 	'label',
			html: 	"<table>"  +
						"<tr>"  +
						"	<td class=\"titulos\" align=\"center\">"  +
						"		<span class=\"titulos\">"  +
						"			Estructura de Archivo<br>Carga masiva de cuentas CLABE y Swift"  +
						"		</span>"  +
						"	</td>"  +
						"</tr>"  +
						"<tr>"  +
						"	<td>&nbsp;</td>"  +
						"</tr>"  +
						"<tr>"  +
						"	<td colspan=\"2\" align=\"center\">"  +
						"		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" >"  +
						"		<tr>"  +
						"			<td class=\"celda01\" align=\"center\" colspan=\"10\"  style=\"height:30px;\" >"  +
						"				Layout para cargar cuentas CLABE a PROVEEDORES de la EPO<br>"  +
						"				Archivo de extensi&oacute;n TXT y los campos deben de estar separados por pipes (\"|\")"  +
						"			</td>"  +
						"		</tr>"  +
						"		<tr>"  +
						"			<td class=\"celda01\" align=\"center\" style=\"height:30px;\" >Num.</td>"  +
						"			<td class=\"celda01\" align=\"center\">Campo</td>"  +
						"			<td class=\"celda01\" align=\"center\">Tipo</td>"  +
						"			<td class=\"celda01\" align=\"center\">Longitud</td>"  +
						"			<td class=\"celda01\" align=\"center\">Descripci&oacute;n</td>"  +
						"		</tr> "  +
						"		<tr>"  +
						"			<td class=\"formas\" align=\"center\" style=\"height:30px;\" >1</td>"  +
						"			<td class=\"formas\" align=\"left\">RFC</td>"  +
						"			<td class=\"formas\" align=\"center\">Alfanum&eacute;rico</td>"  +
						"			<td class=\"formas\" align=\"center\">15</td>"  +
						"			<td class=\"formas\" align=\"left\">RFC del Proveedor al que se le cargar&aacute; la cuenta</td>"  +
						"		</tr>"  +
						"		<tr>"  +
						"			<td class=\"formas\" align=\"center\" style=\"height:30px;\" >2</td>"  +
						"			<td class=\"formas\" align=\"left\">Moneda</td>"  +
						"			<td class=\"formas\" align=\"center\">Num&eacute;rico</td>"  +
						"			<td class=\"formas\" align=\"center\">2</td>"  +
						"			<td class=\"formas\" align=\"left\">Clave de la Moneda que manejar&aacute; la cuenta. Valores permitidos: 1 (Moneda Nacional) y 54 (D&oacute;lares)</td>"  +
						"		</tr>"  +
						"		<tr>"  +
						"			<td class=\"formas\" align=\"center\" style=\"height:30px;\" >3</td>"  +
						"			<td class=\"formas\" align=\"left\">Tipo Cuenta</td>"  +
						"			<td class=\"formas\" align=\"center\">Num&eacute;rico</td>"  +
						"			<td class=\"formas\" align=\"center\">2</td>"  +
						"			<td class=\"formas\" align=\"left\">Identificador del tipo de cuenta a registrar. Valores permitidos: 40 (Clabe) y 50 (Swift)</td>"  +
						"		</tr>"  +
						"		<tr>"  +
						"			<td class=\"formas\" align=\"center\" style=\"height:30px;\" >4</td>"  +
						"			<td class=\"formas\" align=\"left\">Cuenta</td>"  +
						"			<td class=\"formas\" align=\"center\">Num&eacute;rico<br>Alfanum&eacute;rico</td>"  +
						"			<td class=\"formas\" align=\"center\">18&nbsp;CLABE<br>20&nbsp;Swift&nbsp;&nbsp;</td>"  +
						"			<td class=\"formas\" align=\"left\">Cuenta a registrar. Si es cuenta Swift, esta puede aceptar como m&aacute;ximo 20 caracteres alfanum&eacute;ricos.</td>"  +
						"		</tr>"  +
						"		<tr>"  +
						"			<td class=\"formas\" align=\"center\" style=\"height:30px;\" >5</td>"  +
						"			<td class=\"formas\" align=\"left\">Banco de Servicio</td>"  +
						"			<td class=\"formas\" align=\"center\">Num&eacute;rico</td>"  +
						"			<td class=\"formas\" align=\"center\">3</td>"  +
						"			<td class=\"formas\" align=\"left\">Clave del Banco de Servicio que se le asociara a la Cuenta del Proveedor </td>"  +
						"		</tr>"  +
						"		<tr>"  +
						"			<td class=\"formas\" align=\"center\" style=\"height:30px;\" >6</td>"  +
						"			<td class=\"formas\" align=\"left\">Plaza Swift</td>"  +
						"			<td class=\"formas\" align=\"center\">Alfanum&eacute;rico</td>"  +
						"			<td class=\"formas\" align=\"center\">20</td>"  +
						"			<td class=\"formas\" align=\"left\">Plaza de la cuenta Swift.<br>No se env&iacute;a para cuenta CLABE.</td>"  +
						"		</tr>"  +
						"		<tr>"  +
						"			<td class=\"formas\" align=\"center\" style=\"height:30px;\" >7</td>"  +
						"			<td class=\"formas\" align=\"left\">Sucursal Swift</td>"  +
						"			<td class=\"formas\" align=\"center\">Alfanum&eacute;rico</td>"  +
						"			<td class=\"formas\" align=\"center\">20</td>"  +
						"			<td class=\"formas\" align=\"left\">Sucursal de la cuenta Swift.<br>No se env&iacute;a para cuenta CLABE.</td>"  +
						"		</tr>"  +
						"		</table>"  +
						"	</td>"  +
						"</tr>"  +
						"<tr>"  +
						"	<td>&nbsp;</td>"  +
						"</tr>"  +
						"</table>" ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginBottom: 	'10px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		}
	];
	var panelLayoutDeCarga = {
		xtype:			'panel',
		id: 				'panelLayoutDeCarga',
		hidden:			true,
		width: 			700,
		title: 			'Descripci�n del Layout de Carga',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosLayoutDeCarga,
		tools: [
			{
				id:			'close',
				handler: function(event, toolEl, panel){
					hidePanelLayoutDeCarga();
				}
    		}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      )
	}
	
	var espaciadorPanelLayoutDeCarga = {
		xtype: 	'box',
		id:		'espaciadorPanelLayoutDeCarga',
		hidden:	true,
		height: 	10,
		width: 	800
	}
	
	//-------------------------- FORMA DE CARGA DE ARCHIVO ---------------------------
	var getIconoAyuda2 = function(){
		var iconoAyuda = '<div class="x-tool x-tool-ayudaLayout x-btn-icon" style="float:left;width:15px;"  onclick="Ext.getCmp(\'panelLayoutDeCarga\').show();" >&#160</div>'
		return iconoAyuda;
	}
 
	var getIconoAyuda = function(){
		var iconoAyuda =
		"<img class=\"x-btn x-btn-icon\" src=\"/nafin/00utils/gif/ayudaLayout.png\" border=\"0\" width=\"15\" style=\"padding-right:10px; outline: 0;\" onclick=\"javascript:showPanelLayoutDeCarga();\" >";
		return iconoAyuda;
	}
	
	var elementosFormaCargaArhivo = [
		{
			xtype: 				'combo',
			name: 				'claveEpo',
			id: 					'cmbEpo',
			fieldLabel: 		'EPO',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEpo',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			tpl: 					NE.util.templateMensajeCargaCombo,
			anchor:				'95%',
			listeners: {
				change: function(obj, newVal, oldVal){
					Ext.Ajax.request({
						url: '15cmasiva01ext.data.jsp',
						params: {
							accion: "valoresIniciales",
							claveEpo: newVal
						},
						callback: procesaValoresIniciales
					});
				}
			}
		},
		{
		  xtype: 		'fileuploadfield',
		  id: 			'archivo',
		  name: 			'archivo',
		  emptyText: 	'Seleccione...',
		  fieldLabel: 	getIconoAyuda() + "Nombre del Archivo", 
		  buttonText: 	null,
		  buttonCfg: {
			  iconCls: 	'upload-icon'
		  },
		  anchor: 		'95%',
		  vtype: 		'archivotxt'
		},
		{  
        xtype:	'hidden',  
        name:	'guardaProceso',
        id: 	'guardaProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'guardaTotalRegistros',
        id: 	'guardaTotalRegistros'
      },
		{  
		  xtype:	'hidden',  
		  name:	'nombArchivoDetalle',
		  id: 	'nombArchivoDetalle'
		}
	];
	
	var panelFormaCargaArhivo = new Ext.form.FormPanel({
		id: 				'panelFormaCargaArhivo',
		width: 			700,
		title: 			'Ubicaci�n del Archivo de Datos',
		frame: 			true,
		collapsible: 	true,
		fileUpload: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	150,
		defaultType: 	'textfield',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementosFormaCargaArhivo,
		monitorValid: 	false,
		buttons: [
			{
				text: 		'Continuar',
				id:			'botonContinuarCarga',
				hidden: 		false,
				iconCls: 	'icoContinuar',
				handler: 	function() {
					
					// Revisar si la forma es invalida
					var forma = Ext.getCmp("panelFormaCargaArhivo").getForm();
					if(!forma.isValid()){
						return;
					}
 
					// Validar que se haya especificado una EPO
					var comboEpo = Ext.getCmp('cmbEpo')
					if( Ext.isEmpty(comboEpo.getValue() ) ){
						comboEpo.markInvalid("'Debe seleccionar una EPO");
						return;
					}
					// Validar que se haya especificado un archivo
					var archivo = Ext.getCmp("archivo");
					if( Ext.isEmpty(archivo.getValue() ) ){
						archivo.markInvalid("'Debe especificar un archivo");
						return;
					}
					
					// Enviar archivo para su procesamiento
					cargaMasiva("SUBIR_ARCHIVO", null);
					
				}
			}
		]
	});
	var caracteresEspeciales = new NE.cespcial.CaracteresEspeciales(
		{
		hidden: true
		}
	);
	
	caracteresEspeciales.setHandlerAceptar(function(){
		caracteresEspeciales.hide(),
		panelFormaCargaArhivo.show();
	});
	
	caracteresEspeciales.setHandlerDescargarArchivo(function(){
		 cargaMasiva("DESCARGAR_ARCHIVO", null);
	});
	
	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		//disabled: true,
		items: 	[
			NE.util.getEspaciador(10),
			panelFormaCargaArhivo,
			NE.util.getEspaciador(10),
			panelLayoutDeCarga,
			espaciadorPanelLayoutDeCarga,
			panelResultadosValidacion,
			panelAcuseCargaCuentas,
			caracteresEspeciales
			
		]
	});
	
	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------

});