<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		netropology.utilerias.caracterescontrol.*,
		com.netro.cadenas.*,
		com.netro.parametrosgrales.*,
		com.netro.afiliacion.ConsCargaProvEcon,
		com.netro.afiliacion.CargaArchivoPyme,
		com.netro.pdf.*,
		com.netro.zip.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEpoCargaArchivos,
		org.apache.commons.logging.Log,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		com.netro.dispersion.*,
		javax.naming.Context,
		java.io.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%@ include file="/15cadenas/15pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String accion 			= (request.getParameter("accion")		== null)?"":request.getParameter("accion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("accion = <"+accion+">");

if (        accion.equals("valoresIniciales") )	{
	
	JSONObject	resultado					= new JSONObject();
	boolean		success						= true;
	boolean		permisoAgregarCuentas 	= true;
	
	// 1. Obtener instancia de EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
				
	}catch(Exception e){
 
		String msg	= "Ocurrió un error al obtener instancia del EJB de Dispersión";
		log.error(msg);
		e.printStackTrace();
		
		success	= false;
		resultado.put( "msg", msg);
		
	}
	
	// 2. Determinar si la EPO es susceptible de dispersion
	if( success ){
		
		String claveEPO	= iNoCliente;
		try {
			
			dispersion.getSuceptibleDispersion(claveEPO);
			
		} catch(NafinException neError){
			
			String msg	= neError.getMsgError();
			log.error(msg);
			neError.printStackTrace();
			
			permisoAgregarCuentas	= false;
			resultado.put("msg", msg);
			
		}
		
	}
	
	// 3. Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success));
	resultado.put("permisoAgregarCuentas", new Boolean(permisoAgregarCuentas));
	
	infoRegresar = resultado.toString();

} else if (        	accion.equals("CargaMasiva.subirArchivo") 				)	{	
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String nombreArchivo = "";
	String itemArchivo	="",  rutaArchivo ="",   error_tam =""; 
	String PATH_FILE	=	strDirectorioTemp;
	String rutaArchivoTemporal ="";
	ParametrosRequest req = null;
	
	
 
	// Obtener instancia del EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("CargaMasiva.subirArchivo(Exception): Obtener instancia del EJB de Dispersion");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
	 
	}
	if (ServletFileUpload.isMultipartContent(request)) {
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		req = new ParametrosRequest(upload.parseRequest(request));
		
		FileItem fItem = (FileItem)req.getFiles().get(0);
		itemArchivo		= (String)fItem.getName();
		InputStream archivo = fItem.getInputStream();
		rutaArchivo = PATH_FILE+itemArchivo;
		int tamanio			= (int)fItem.getSize();
		nombreArchivo= Comunes.cadenaAleatoria(16)+ ".txt";
		rutaArchivoTemporal = PATH_FILE + nombreArchivo;
		fItem.write(new File(rutaArchivoTemporal));
	}
	if(!error_tam.equals("")){
		nombreArchivo="";
	}	
	 ParametrosGrales paramGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
	boolean continua = true;
	String codificacionArchivo ="";
	//boolean resul_cod
	if(paramGrales.validaCodificacionArchivoHabilitado()){
		String[]  	CODIFICACION	= {""};
		CodificacionArchivo  valida_codificacion= new CodificacionArchivo();
		valida_codificacion.setProcessTxt(true);
			//codificaArchivo.setProcessZip(true);
		if(valida_codificacion.esCharsetNoSoportado(rutaArchivoTemporal,CODIFICACION)){
				codificacionArchivo = CODIFICACION[0];
				continua = false;
			
		}
	}
	
	if(continua==true){  
		// Abrir archivo 
		BufferedReader 				bufferedreader 	= null;
		try {
			java.io.File 					file 					= 	new java.io.File(strDirectorioTemp+nombreArchivo);
			bufferedreader 	= 	new BufferedReader(
											new InputStreamReader(
												new FileInputStream(file)
											)
										);
				
		}catch(Exception e){
				
			success		= false;
			log.error("CargaMasiva.subirArchivo(Exception): Abrir archivo");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al abrir el archivo");
				
		} finally {
				
			if( !success && bufferedreader != null ){ try{ bufferedreader.close(); }catch(Exception e){} }
		}
			
		// Insertar en la base de datos el contenido del archivo
		String	ic_proc_carga 	= "0";
		int 		numReg 			= 0;
		try {
				
			String	line		= null;
			while((line = bufferedreader.readLine())!=null) {
					
				line 				= line.replace('\'', ' ').replace('\"', ' ').replace('\\', ' ').trim();
				List list 		= Comunes.getListTokenizer(line, "|");
				log.debug("lISTA DE DATOS"+list);
				ic_proc_carga 	= dispersion.setDatosTmpCuentas(ic_proc_carga, list);
				numReg++;
					
			}
				
		} catch(Exception e){
				
			success		= false;
			log.error("CargaMasiva.subirArchivo(Exception): Insertar en la base de datos el contenido del archivo");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al insertar contenido del archivo la base de datos");
				
		} finally {
				
			if( bufferedreader != null ){ try{ bufferedreader.close(); }catch(Exception e){} }
				
		}
			
		// Enviar numero de proceso
		resultado.put("proceso", 			ic_proc_carga 			);
		// Enviar numero total de registros insertados en la base de datos
		resultado.put("totalRegistros",	String.valueOf(numReg));
		resultado.put("nombreArchivo", 			nombreArchivo 			);
		// Especificar el estado siguiente
		resultado.put("estadoSiguiente", "VALIDA_CARACTERES_CONTROL"		);
	}else{
		resultado.put("estadoSiguiente", "MENSAJE_CODIFICACION"		);
		resultado.put("codificacionArchivo",codificacionArchivo	);
	}
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
} else if (    accion.equals("CargaMasiva.validaCaracteresControl") 				)	{
		JSONObject	resultado	= new JSONObject();
		String nombreArchivo 	= (request.getParameter("nombreArchivo")	== null)?"":request.getParameter("nombreArchivo");
		session.removeAttribute("ValidaCaracteresControl");
		BuscaCaracteresControlThread ValidaCaractCon= new BuscaCaracteresControlThread();
		ResumenBusqueda resCaractCon = new ResumenBusqueda();
		
		ValidaCaractCon.setRutaArchivo(strDirectorioTemp+nombreArchivo);
		ValidaCaractCon.setRegistrador(resCaractCon);
		ValidaCaractCon.setCreaArchivoDetalle(true);
		ValidaCaractCon.setDirectorioTemporal(strDirectorioTemp);
		ValidaCaractCon.setSeparadorCampo("|");
		new Thread(ValidaCaractCon).start();
		session.setAttribute("ValidaCaracteresControl", ValidaCaractCon);
	//ValidaCaractCon.getMensaje();
		resultado.put("success",new Boolean(true));
		resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_THREAD"		);
		infoRegresar = resultado.toString();
	System.out.println("TERMINO Y EMPEZO EL THREAD ");
	
	
}else if (    accion.equals("CargaMasiva.avanceThreadCaracteres") 				)	{
		JSONObject	resultado	= new JSONObject();
		BuscaCaracteresControlThread ValidaCaractCon = (BuscaCaracteresControlThread)session.getAttribute("ValidaCaracteresControl");
		String[] mensa = {""};
		ResumenBusqueda resCaractCon = new ResumenBusqueda(); 
		int estatusThread = ValidaCaractCon.getStatus(mensa);
		System.out.println("ESTATUS "+estatusThread);
		if(estatusThread==200){
			resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_THREAD");	
		}else if(estatusThread==300 ){
			resCaractCon = (ResumenBusqueda)ValidaCaractCon.getRegistrador();
			boolean hayCaracteres = resCaractCon.hayCaracteresControl();
			if(hayCaracteres==true){
				resultado.put("estadoSiguiente", "HAY_CARACTERES_CONTROL");
				resultado.put("mns",resCaractCon.getMensaje());
				resultado.put("nombreArchivoCaractEsp",resCaractCon.getNombreArchivoDetalle());
				
			}else{
				resultado.put("estadoSiguiente", "REVISAR_CUENTAS");
			}
		}else if(estatusThread==400 ){
			resultado.put("estadoSiguiente", "ERROR_THREAD_CARACTERES");	
			resultado.put("mns", "Error en el archivo, favor de verificarlo");	
		}
		resultado.put("success",new Boolean(true));
		infoRegresar = resultado.toString();
		System.out.println("TERMINO Y EMPEZO EL THREAD "+infoRegresar);
	
	
}else if (accion.equals("descargaArchivoDetalle")	){
		String nombreArchivo 	= (request.getParameter("archivo")	== null)?"":request.getParameter("archivo");
		JSONObject jsonObj3 = new JSONObject();
		jsonObj3.put("success", new Boolean(true));
		jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj3.toString();
}else if (    accion.equals("CargaMasiva.revisarCuentas") 				)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	
	String proceso 			= (request.getParameter("proceso")			== null)?"0":request.getParameter("proceso");
	String totalRegistros 	= (request.getParameter("totalRegistros")	== null)?"0":request.getParameter("totalRegistros");
	String claveEPO			= iNoCliente;
	
	List lVarBind = new ArrayList();
	lVarBind.add(new Integer(proceso)	);
	lVarBind.add(new Integer(claveEPO)	);
	new CicloBackGroundSP("SP_CARGA_MASIVA_CUENTAS", lVarBind).start();
	
	// Enviar numero de proceso
	resultado.put("proceso", 			proceso 			);
	// Enviar numero total de registros insertados en la base de datos
	resultado.put("totalRegistros",	totalRegistros	);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "MOSTRAR_AVANCE_REVISION"		);
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    accion.equals("CargaMasiva.mostrarAvanceRevision") 	)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	
	String proceso 			= (request.getParameter("proceso")			 == null)?"0"		:request.getParameter("proceso");
	String totalRegistros 	= (request.getParameter("totalRegistros")	 == null)?"0"		:request.getParameter("totalRegistros");
	
	// Enviar el valor del avance
	resultado.put("avance",							"0"									);
	// Enviar el texto con el porcentaje de avance
	resultado.put("textoPorcentajeAvance",		"0.00 %"								);
	// Enviar el numero de registros procesados
	resultado.put("registrosProcesados",		"0"									);
	// Enviar numero de proceso
	resultado.put("proceso", 						proceso 								);
	// Enviar numero total de registros insertados en la base de datos
	resultado.put("totalRegistros",				totalRegistros						);
	// Especificar el estado siguiente
	resultado.put("completado",					"false");
	resultado.put("estadoSiguiente", 			"ACTUALIZAR_AVANCE_REVISION"	);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
 
	infoRegresar = resultado.toString();
	
} else if (    accion.equals("CargaMasiva.actualizarAvanceRevision") )	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	
	String proceso 			= (request.getParameter("proceso")			 == null)?"0"		:request.getParameter("proceso");
	String totalRegistros 	= (request.getParameter("totalRegistros")	 == null)?"0"		:request.getParameter("totalRegistros");
	String completado 		= (request.getParameter("completado") 		 == null)?"false"	:request.getParameter("completado");
	String claveEPO			= iNoCliente;
	
	// Obtener instancia del EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("CargaMasiva.subirArchivo(Exception): Obtener instancia del EJB de Dispersion");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
	 
	}
	
	// Consultar avance
	String		estatusProceso 		= "PR";
	int			registrosProcesados  = 0;
		
	Registros 	registros 				= dispersion.getEstatusCargaCuentas(proceso);
	if(registros.next()) {
		estatusProceso 					= registros.getString("cg_estatus_proc");
		registrosProcesados  			= Integer.parseInt(registros.getString("ig_num_procesados"));
	}

	// Calcular avance
	double 	avance 						= (double) ( registrosProcesados ) / Double.parseDouble(totalRegistros) ;
	double 	porcentajeAvance			= avance*100;
	String 	textoPorcentajeAvance 	= Comunes.formatoDecimal(porcentajeAvance,2,false) + " %";
 
	// Enver el valor del avance
	resultado.put("avance",							String.valueOf(avance)					);
	// Enviar el texto con el porcentaje de avance
	resultado.put("textoPorcentajeAvance",		textoPorcentajeAvance					);
	// Enviar el numero de registros procesados
	resultado.put("registrosProcesados",		String.valueOf(registrosProcesados)	);
	// Enviar numero de proceso
	resultado.put("proceso", 						proceso 										);
	// Enviar numero total de registros insertados en la base de datos
	resultado.put("totalRegistros",				totalRegistros								);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)						);
	// Especificar el estado siguiente
	System.err.println("estatusProceso = <" + estatusProceso+ ">");
	if ("PR".equals(estatusProceso)) {
		resultado.put("completado",				"false");
		resultado.put("estadoSiguiente", 		"ACTUALIZAR_AVANCE_REVISION"			);
	}else if( "OK".equals(estatusProceso) || "ER".equals(estatusProceso) ){
		
		if( "true".equals(completado) ){
			
			// Obtener numero de registros correctos
			registros 										= dispersion.getRegistrosValidados(proceso, 				"S");
			int			totalSolicitudesCorrectas	= registros.getNumeroRegistros();
			// Obtener numero de registros con error
			registros 										= dispersion.getRegistrosConCuentasValidadas(proceso, "N");
			int			totalSolicitudesConError	= registros.getNumeroRegistros();
			// Revisar si es necesario habilitar el boton continuar
			boolean 	   mostrarBotonContinuar		= totalSolicitudesCorrectas > 0?true:false;
			
			resultado.put("totalSolicitudesCorrectas",String.valueOf(totalSolicitudesCorrectas)	);
			resultado.put("totalSolicitudesConError",	String.valueOf(totalSolicitudesConError)	);
			resultado.put("mostrarBotonContinuar",		new Boolean(mostrarBotonContinuar)			);
			resultado.put("completado",					"true"												);
			resultado.put("estadoSiguiente", 			"MOSTRAR_RESULTADO_REVISION"					);
			
		} else { // Para permitir que el usuario pueda ver cuando se llegue al 100%
			
			resultado.put("completado",					"true");
			resultado.put("estadoSiguiente", 			"ACTUALIZAR_AVANCE_REVISION"					);
			
		}
	}
	
	infoRegresar = resultado.toString();
	
} else if (    accion.equals("CargaMasiva.mostrarResultadoRevision") )	{
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	
	String 		proceso 			= (request.getParameter("proceso")			 == null)?"0"		:request.getParameter("proceso");
	String 		totalRegistros = (request.getParameter("totalRegistros")	 == null)?"0"		:request.getParameter("totalRegistros");
	
	// Enviar numero de proceso
	resultado.put("proceso", 						proceso 								);
	// Enviar numero total de registros insertados en la base de datos
	resultado.put("totalRegistros",				totalRegistros						);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 			"ESPERAR_DECISION"				);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
	
} else if (    accion.equals("CargaMasiva.generarTextoFirmar") 		)	{
	
	JSONObject		resultado		= new JSONObject();
	boolean			success			= true;
	
	String 			proceso 			= (request.getParameter("proceso")			 == null)?"0"		:request.getParameter("proceso");
	String 			totalRegistros = (request.getParameter("totalRegistros")	 == null)?"0"		:request.getParameter("totalRegistros");
	String			nombreEPO		= (String) session.getAttribute("strNombre");
	
	// Obtener instancia del EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("CargaMasiva.generarTextoFirmar(Exception): Obtener instancia del EJB de Dispersion");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
	 
	}
	
	// Generar Texto a Firmar
	StringBuffer	textoFirmar		= new StringBuffer();
	textoFirmar.append("Carga Masiva de Cuentas \n");
	textoFirmar.append("Para proveedores de la EPO:");
	textoFirmar.append(nombreEPO);
	textoFirmar.append("\n");
	
	// Agregar cuentas correctas
	Registros registros 						= dispersion.getRegistrosValidados(proceso, "S");			
	int		 numeroRegistrosCorrectos	= registros.getNumeroRegistros();
	while(registros.next()) {
		
		String claveCuenta	= registros.getString("cg_cuenta");
		String claveRfc 		= registros.getString("cg_rfc");
		
		textoFirmar.append("Cuenta: ");
		textoFirmar.append(claveCuenta); 
		textoFirmar.append(" RFC Proveedor: ");
		textoFirmar.append(claveRfc);
		textoFirmar.append("\n");
		
	}
	
	int numeroSolicitudesProcesadas	= Integer.parseInt(totalRegistros);
	int numeroRegistrosConError		= numeroSolicitudesProcesadas - numeroRegistrosCorrectos;
	
	textoFirmar.append("\nSolicitudes Correctas: ");			textoFirmar.append(numeroRegistrosCorrectos);		textoFirmar.append("\n");
	textoFirmar.append("Solicitudes con Error: ");				textoFirmar.append(numeroRegistrosConError);			textoFirmar.append("\n");
	textoFirmar.append("Solicitudes Procesadas Totales: ");	textoFirmar.append(numeroSolicitudesProcesadas);	textoFirmar.append("\n");
 
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	// Enviar numero de proceso
	resultado.put("proceso", 						proceso 								);
	// Enviar numero total de registros insertados en la base de datos
	resultado.put("totalRegistros",				totalRegistros						);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 			"INSERTAR_CUENTAS"				);
	// Enviar texto a firmar
	resultado.put("textoFirmar", 					textoFirmar.toString()			);
	
	infoRegresar = resultado.toString();
	
} else if (    accion.equals("CargaMasiva.insertarCuentas") 			)	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String 		proceso 				= (request.getParameter("proceso")			 == null)?"0"		:request.getParameter("proceso");
	String 		totalRegistros 	= (request.getParameter("totalRegistros")	 == null)?"0"		:request.getParameter("totalRegistros");
	String 		isEmptyPkcs7 		= (request.getParameter("isEmptyPkcs7")	 == null)?"false"	:request.getParameter("isEmptyPkcs7");
	String		textoFirmado		= (request.getParameter("textoFirmado")	 == null)?""		:request.getParameter("textoFirmado");
	String		pkcs7					= (request.getParameter("pkcs7")			 	 == null)?""		:request.getParameter("pkcs7");
 
	String		estadoSiguiente 	= "";
	// Realizar insercion
	if( "true".equals(isEmptyPkcs7) 	 || "".equals(pkcs7) 						){
		estadoSiguiente = "ESPERAR_DECISION";
	} else {
		
		// Obtener instancia del EJB de Dispersion
		Dispersion dispersion = null;
		try {
					
			dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
						
		}catch(Exception e){
		 
			success		= false;
			log.error("CargaMasiva.insertarCuentas(Exception): Obtener instancia del EJB de Dispersion");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
		 
		}
	
		String		externContent		= textoFirmado;
		String 		folioCert 			= "";
		char 			getReceipt 			= 'Y';
		String 		recibo_e 			= "500";
		String 		claveEPO				= iNoCliente;
		String 		nombreEPO			= strNombre;
		String		loginUsuario		= iNoUsuario;
		
		boolean		insercionExitosa	= false;
		String		autentificacion	= "";
		Seguridad 	s 						= null;
		if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
			
			folioCert 	= claveEPO + new SimpleDateFormat("MMyyyyhhmmss").format(new java.util.Date());
			s 				= new Seguridad();
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
				
				recibo_e 					= s.getAcuse();
				
				Acuse 		acuse 		= new Acuse(3, "5");
				String 		cc_acuse 	= "E"+acuse.toString();
		
				Registros	registros	= dispersion.getRegistrosConCuentasValidadas(proceso, "S");
				
				HashMap parametrosAdicionales = new HashMap();
				parametrosAdicionales.put("DESCRIPCION_ALTA",	"ALTA\n" + nombreEPO 	);
				parametrosAdicionales.put("PANTALLA_ORIGEN", 	"REGMASCTAS" 				);
				
				dispersion.agregaCuentas(registros, claveEPO, loginUsuario, parametrosAdicionales);
				
				insercionExitosa = true;
				autentificacion  = "La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br>Recibo: "+recibo_e;
				
			}
		
		}
		
		if( !insercionExitosa ){
			autentificacion  = s == null?"La autentificaci&oacute;n no se llev&oacute; a cabo.":"La autentificaci&oacute;n no se llev&oacute; a cabo: <br>"+s.mostrarError();
		}
		
		// Enviar numero de proceso
		resultado.put("autentificacion",			autentificacion 					);
		// Enviar numero de proceso
		resultado.put("insercionExitosa", 		new Boolean(insercionExitosa)	);
		
		estadoSiguiente = "MOSTRAR_CUENTAS_INSERTADAS";
		
	}
	
	// Enviar numero de proceso
	resultado.put("proceso", 						proceso 								);
	// Enviar numero total de registros insertados en la base de datos
	resultado.put("totalRegistros",				totalRegistros						);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 			estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
	
} else if (    accion.equals("CargaMasiva.mostrarCuentasInsertadas") )	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String 		proceso 				= (request.getParameter("proceso")			 == null)?"0"		:request.getParameter("proceso");
	String 		totalRegistros 	= (request.getParameter("totalRegistros")	 == null)?"0"		:request.getParameter("totalRegistros");
	
	// Enviar numero de proceso
	resultado.put("proceso", 						proceso 								);
	// Enviar numero total de registros insertados en la base de datos
	resultado.put("totalRegistros",				totalRegistros						);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 			"ESPERAR_DECISION"			 	);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
	
} else if (    accion.equals("CargaMasiva.fin") 							)	{
	
	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("15cuenta02ext.jsp"); 
	
} else if (    accion.equals("ConsultaCuentasCorrectas")					)	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String 		proceso 				= (request.getParameter("proceso")			 == null)?"0"		:request.getParameter("proceso");
	String 		totalRegistros 	= (request.getParameter("totalRegistros")	 == null)?"0"		:request.getParameter("totalRegistros");
	
	// Obtener instancia del EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("ConsultaCuentasCorrectas(Exception): Obtener instancia del EJB de Dispersion");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
	 
	}
	
	Registros 		cuentasCorrectas 		= dispersion.getRegistrosValidados(proceso, "S");
	JSONArray		registros				= new JSONArray();
	while(cuentasCorrectas.next()) {
		
		String 		linea 	= cuentasCorrectas.getString("ic_cuenta");
		String 		cuenta 	= cuentasCorrectas.getString("cg_cuenta");
		
		JSONObject	registro = new JSONObject();
		registro.put("LINEA", 	linea);
		registro.put("CUENTA",  cuenta);
							
		registros.add(registro);
		
	}
	
	resultado.put("total",    String.valueOf(registros.size()) );
	resultado.put("registros",registros			 );
	
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
						
} else if (    accion.equals("ConsultaCuentasConErrores")				)	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String 		proceso 				= (request.getParameter("proceso")			 == null)?"0"		:request.getParameter("proceso");
	String 		totalRegistros 	= (request.getParameter("totalRegistros")	 == null)?"0"		:request.getParameter("totalRegistros");
	
	// Obtener instancia del EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("ConsultaCuentasConErrores(Exception): Obtener instancia del EJB de Dispersion");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
	 
	}
	
	Registros 		cuentasConErrores 	= dispersion.getRegistrosConCuentasValidadas(proceso, "N");
	JSONArray		registros				= new JSONArray();
	while(cuentasConErrores.next()) {
		
		String 		linea 					= cuentasConErrores.getString("ic_cuenta");
		String 		descripcionError 		= cuentasConErrores.getString("cg_error");
		
		JSONObject	registro = new JSONObject();
		registro.put("LINEA", 					linea);
		registro.put("DESCRIPCION_ERROR",	descripcionError);
							
		registros.add(registro);
		
	}
	
	resultado.put("total",    String.valueOf(registros.size()) );
	resultado.put("registros",registros			 );
	
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
	
} else if (    accion.equals("GenerarArchivoTXTErrores")	)	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String 		proceso 				= (request.getParameter("proceso")			 == null)?"0"		:request.getParameter("proceso");
	String 		totalRegistros 	= (request.getParameter("totalRegistros")	 == null)?"0"		:request.getParameter("totalRegistros");
	
	// Obtener instancia del EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("GenerarArchivoTXTErrores(Exception): Obtener instancia del EJB de Dispersion");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
	 
	}
 
	OutputStreamWriter 	writer 			= null;
	BufferedWriter 		buffer 			= null;
	String 					nombreArchivo 	= null;
	
	try {
		
		// Crear archivo TXT
		nombreArchivo 	= Comunes.cadenaAleatoria(16) + ".txt";
		writer 			= new OutputStreamWriter(new FileOutputStream(strDirectorioTemp + nombreArchivo, true), "Cp1252");
		buffer 			= new BufferedWriter(writer);
					
		// Crear buffer
		Registros 		registros 		= dispersion.getRegistrosConCuentasValidadas(proceso, "N");
		int 				numeroLinea 	= 0;
		while(registros.next()) {
			
			numeroLinea++;
			
			buffer.write(registros.getString("cg_rfc")); 			buffer.write("|");
			buffer.write(registros.getString("ic_moneda")); 		buffer.write("|");
			buffer.write(registros.getString("ic_tipo_cuenta")); 	buffer.write("|");
			buffer.write(registros.getString("cg_cuenta")); 		buffer.write("|");
			buffer.write(registros.getString("ic_bancos_tef")); 	buffer.write("\n");
			
			if(numeroLinea % 100 == 0){
				numeroLinea = 0;
				buffer.flush();
			}
			
		}
		buffer.flush();
	
	} catch(Exception e) {
		
		log.error("GenerarArchivoTXTErrores(Exception)");
		log.error("GenerarArchivoTXTErrores.proceso 			= <" + proceso				+ ">");
		log.error("GenerarArchivoTXTErrores.totalRegistros = <" + totalRegistros	+ ">");
		e.printStackTrace();
			
		throw new AppException("Error al generar el archivo", e);
			
	} finally {
		
		if(buffer 	!= null ){ try { buffer.close(); } catch(Exception e) {} }
		log.info("crearCustomFile(S)");
		
	}
		
	resultado.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo	);
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)						);
	
	infoRegresar = resultado.toString();
							
} else if (    accion.equals("ConsultaCuentasInsertadas")				)	{
	
	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	String 		proceso 				= (request.getParameter("proceso")			 == null)?"0"		:request.getParameter("proceso");
	String 		totalRegistros 	= (request.getParameter("totalRegistros")	 == null)?"0"		:request.getParameter("totalRegistros");
	
	// Obtener instancia del EJB de Dispersion
	Dispersion dispersion = null;
	try {
				
		dispersion = ServiceLocator.getInstance().lookup("DispersionEJB",Dispersion.class);
					
	}catch(Exception e){
	 
		success		= false;
		log.error("ConsultaCuentasInsertadas(Exception): Obtener instancia del EJB de Dispersion");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Dispersión.");
	 
	}
	
	HashMap		catalogoMonedas 	= dispersion.getCatalogoMonedas();
	
	Registros	cuentasAgregadas	= dispersion.getRegistrosConCuentasValidadas(proceso, "S");
	JSONArray	registros			= new JSONArray();
	while(cuentasAgregadas.next()) {
		
		String 	auxEstatus 		= cuentasAgregadas.getString("ic_estatus_cecoban");
		boolean 	esCuentaSwift 	= "54".equals(cuentasAgregadas.getString("ic_moneda")) && "50".equals(cuentasAgregadas.getString("ic_tipo_cuenta"));
		if(esCuentaSwift && "99".equals(auxEstatus)){ 
			auxEstatus = "Cuenta&nbsp;Correcta";
		}else if(!"99".equals(auxEstatus)){
			auxEstatus = "Sin Validar";
		}
		
		JSONObject registro = new JSONObject();
		
		registro.put("NOMBRE_PYME",		dispersion.getNombrePyme(cuentasAgregadas.getString("cg_rfc"))								);
		registro.put("CUENTA",				cuentasAgregadas.getString("cg_cuenta")															);
		registro.put("NOMBRE_MONEDA",		dispersion.getNombreMoneda(cuentasAgregadas.getString("ic_moneda"),catalogoMonedas)	);
		registro.put("ESTATUS",				auxEstatus																									);

		registros.add(registro);
	}

	resultado.put("total",    	String.valueOf(registros.size()) );
	resultado.put("registros",	registros			 					);
	
	// Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)		);
	
	infoRegresar = resultado.toString();
	
} else {
	
	throw new AppException("La acción: "+ accion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>