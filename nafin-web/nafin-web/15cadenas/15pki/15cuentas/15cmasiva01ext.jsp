<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<%@ include file="/15cadenas/15pki/certificado.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script type="text/javascript" src="15cmasiva01ext.js?<%=session.getId()%>"></script>
<%-- El jsp que contiene el dise�o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<%@ include file="/00utils/componente_firma.jspf" %>
<script language="JavaScript1.2" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS){%>
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
<%if(esEsquemaExtJS){%>
	<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
<%}%>
	<div id="areaContenido"></div>						
	</div>
	</div>
<%if(esEsquemaExtJS){%>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>