
Ext.onReady(function() {
	var emergente =  Ext.getDom('emergente').value;	
	var txtCadProductiva  = [];
	var fecha  = [];
	var regCveProv  = [];
	var regNumProv  = [];
	var regChkElimina  = [];
	var regCausa  = [];
	
	var cancelar =  function() { 
		txtCadProductiva  = [];
		fecha  = [];
		regCveProv  = [];
		regNumProv  = [];
		regChkElimina  = [];
		regCausa  = [];
	}
	
		
	function procesarSuccessFailureContinuar(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonObj = Ext.util.JSON.decode(response.responseText);
			
			window.location = jsonObj.urlPagina;
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGuardar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			if (jsondeAcuse != null){
				var msg_error= jsondeAcuse.msg_error;	
				var  gridConsulta = Ext.getCmp('gridConsulta');	
				if(msg_error =='') {
					Ext.MessageBox.alert("Mensaje","Guardado con �xito");
					if(emergente=='S') {
						Ext.Ajax.request({
							url: '/nafin/13descuento/13epo/13pymeSinNumProv_popExt.data.jsp',
							params: {
								informacion: "Continuar"
							},
							callback: procesarSuccessFailureContinuar
						});
					}
					gridConsulta.hide();
				}else {
					Ext.MessageBox.alert("Mensaje",msg_error);
					gridConsulta.hide();				
				}
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var confirmar = function(pkcs7, vtextoFirmar, vtxtCadProductiva, vfecha, vregCveProv, vregNumProv, vregChkElimina, vregCausa){
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else {
			Ext.Ajax.request({
				url : '15consSinNumProvExt.data.jsp',
				params : {
					pkcs7: pkcs7,
					textoFirmado: vtextoFirmar,
					informacion: 'Guardar',
					txtCadProductiva:vtxtCadProductiva,
					fecha:vfecha,
					regCveProv:vregCveProv,
					regNumProv:vregNumProv,
					regChkElimina:vregChkElimina,
					regCausa:vregCausa			
				},
			callback: procesarSuccessFailureGuardar
			});			
		}
	}
	
	var procesarGuardar = function(store, arrRegistros, opts) 	{
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		var modificados = store.getModifiedRecords();
		var columnModelGrid = gridConsulta.getColumnModel();
		var textoFirmar  ='';
		var eliminar ='N';
		var opcionEliminar = true;
		cancelar(); // Limpia las variables a enviar 
		
		Ext.each(modificados, function(record) {												
			if( (record.data['ELIMINAR'] == true  && record.data['CAUSA']=='')  || (record.data['ELIMINAR'] == 'false'  && record.data['CAUSA']!='') ){
				Ext.MessageBox.alert("Mensaje","Capture el n�mero o la causa de la eliminaci�n del proveedor");
				opcionEliminar =false; 
				return false;
			}										
		});
					
		store.each(function(record) {					
			if(record.data['SELECCIONADO']=='S'){	
				textoFirmar += record.data['NUM_ELECTRONICO']+"|"+record.data['FECHA_AFILIACION']+"|"
										+record.data['FECHA_ALTA']+"|"+record.data['RFC']+"|"
										+record.data['NOMBRE_PROVEEDOR']+"|"+record.data['NUM_PROVEEDOR']+"|"
										+record.data['CAUSA']+"\n";											
												
					txtCadProductiva.push(record.data['NUM_EPO']);
					fecha.push(record.data['FECHA_ALTA']);
					regCveProv.push(record.data['NUM_PYME']);
					regNumProv.push(record.data['NUM_PROVEEDOR']);
					if(record.data['ELIMINAR']==true)  eliminar ='S';
					regChkElimina.push(eliminar);
					regCausa.push(record.data['CAUSA']);					
				}
			});
		
		
		if(txtCadProductiva =='') {
			Ext.MessageBox.alert('Mensaje','No hay registros modificados');
			return false;	
		}		
			
			if(opcionEliminar ==true){
				NE.util.obtenerPKCS7(confirmar, textoFirmar, txtCadProductiva, fecha, regCveProv, regNumProv, regChkElimina, regCausa );
				
								
			}
	//	}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
		}					
		var el = gridConsulta.getGridEl();	
		var cm = gridConsulta.getColumnModel();	
		var jsonData = store.reader.jsonData;	
				
		if(jsonData.emergente=='S'){
			gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CONVENIO_UNICO'), false);
			Ext.getCmp('btnSalir').show();
		}
		
		if(store.getTotalCount() > 0) {
			Ext.getCmp('btnGuardar').enable();				
			el.unmask();			
		} else {		
		Ext.getCmp('btnGuardar').disable();	
			el.mask('No se encontr� ning�n registro', 'x-mask');				
		}	
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15consSinNumProvExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NUM_ELECTRONICO'},
			{name: 'FECHA_AFILIACION'},
			{name: 'FECHA_ALTA'},
			{name: 'RFC'},
			{name: 'NOMBRE_PROVEEDOR'},
			{name: 'NUM_PROVEEDOR'},
			{name: 'ELIMINAR'},
			{name: 'CAUSA'},
			{name: 'NUM_EPO'},
			{name: 'NUM_PYME'},
			{name: 'SELECCIONADO'},
			{name: 'CONVENIO_UNICO'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	
		

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		title: 'Pymes sin n�mero de Proveedor',
		hidden: true,
		clicksToEdit: 1,
		store: consultaData,
		columns: [		
			{							
				header : 'N�mero Electr�nico',
				tooltip: 'N�mero Electr�nico',
				dataIndex : 'NUM_ELECTRONICO',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true	
			},
			{							
				header : 'Fecha Afiliaci�n',
				tooltip: 'Fecha Afiliaci�n',
				dataIndex : 'FECHA_AFILIACION',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true	
			},
			{							
				header : 'Fecha Alta N�m. Proveedor',
				tooltip: 'Fecha Alta N�m. Proveedor',
				dataIndex : 'FECHA_ALTA',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true	
			},
			{							
				header : 'R.F.C',
				tooltip: 'R.F.C',
				dataIndex : 'RFC',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true	
			},
			{							
				header : 'Nombre',
				tooltip: 'Nombre',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true	
			},	
			{							
				header : 'Convenio �nico',
				tooltip: 'Convenio �nico',
				dataIndex : 'CONVENIO_UNICO',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				hidden: true
			},				
			{							
				header : 'N�mero de Proveedor',
				tooltip: 'N�mero de Proveedor',
				dataIndex : 'NUM_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				editor: { 
					xtype : 'textfield', 
					allowBlank: true,  
					maxLength: 24
				},
				renderer: NE.util.colorCampoEdit
			},
			{
				xtype: 'checkcolumn',
				header: 'Eliminar de la Cadena',  
				tooltip: 'Eliminar de la Cadena',
				dataIndex: 'ELIMINAR',
				sortable: false,
				resizable: false	,
				width: 130,			
				align: 'center'			
			},					
			{
				header: 'Causa',  
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				resizable: true	,				
				width: 130,			
				align: 'left',
				editor: { 
					xtype : 'textarea', 
					allowBlank: true,  
					maxLength: 150
				},
				renderer: NE.util.colorCampoEdit
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		align: 'center',
		frame: false,	
		listeners: {	
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
				
				var eliminar = record.data['ELIMINAR'];
				var causa = record.data['CAUSA'];
				var noProveedor = record.data['NUM_PROVEEDOR'];
		
				if(campo=='NUM_PROVEEDOR'  &&  noProveedor !='') { 				
					causa ="";
					eliminar =false;
					record.data['ELIMINAR']=eliminar;			
					record.data['CAUSA']=causa;
					record.data['SELECCIONADO']='S'; 
				}			
				if(campo=='CAUSA' && causa!='' ) { 	 
					noProveedor ="";
					eliminar =true;
					record.data['ELIMINAR']=eliminar;
					record.data['NUM_PROVEEDOR']='';
					record.data['SELECCIONADO']='S'; 
				}
				
				if(campo=='ELIMINAR' && ( eliminar=='true' || eliminar==true)) { 	
					noProveedor ="";
					record.data['NUM_PROVEEDOR']=noProveedor;
					record.data['SELECCIONADO']='S'; 
				}
				if(causa=='' &&( eliminar=='false' || eliminar==false)  && noProveedor =='' ){
					record.data['SELECCIONADO']='N'; 
				}
				
			record.commit();		
			}
		},
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',
				'-',		
				{
					xtype: 'button',
					id: 'btnSalir',
					text: 'Continuar',
					iconCls: 'icoContinuar',
					hidden: true,
					handler: function(boton, evento) {
						Ext.Ajax.request({
						url: '/nafin/13descuento/13epo/13pymeSinNumProv_popExt.data.jsp',
						params: {
							informacion: "Continuar"
						},
						callback: procesarSuccessFailureContinuar
					});
					
					}					
				},
				{
					xtype: 'button',
					id: 'btnGuardar',
					iconCls: 'icoAceptar',
					text: 'Guardar',
					handler: procesarGuardar					
				}
			]
		}
	});
		
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15consSinNumProvExt.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'noPyme',
			id: 'noPyme1',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbPyme1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);							
							storeBusqAvanzPyme.load({
									params: Ext.apply(fpBusqAvanzada.getForm().getValues())
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}
		
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					num_electronico1.setValue(cmbPyme.getValue());
					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
				
			}
		]
	});				
	var elementosForma = [	
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero Nafin Electr�nico',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'N�mero Nafin Electr�nico',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
				xtype: 'button',
				text: 'B�squeda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
					}
				}
			}
			]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de captura desde',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'fec_ini',
					id: 'fec_ini',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fec_fin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fec_fin',
					id: 'fec_fin',
					allowBlank: true,
					startDay: 1,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fec_ini',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Opera Convenio �nico',
			combineErrors: false,
			msgTarget: 'side',
			id: 'chkConvenio1',
			hidden: true,
			items: [
				{
					xtype: 'checkbox',
					name: 'chkConvenio',
					id: 'chkConvenio',
					fieldLabel: '',
					allowBlank: true,
					hidden: true,
					startDay: 0,
					width: 100,
					height:20,					
					msgTarget: 'side',
					margins: '0 20 0 0'
				}				
			]
		}
		];
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,				
		buttons: [
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton, evento) {	
					fp.el.mask('Enviando...', 'x-mask-loading');	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							emergente:emergente
						})
					});				
				}
			},			
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15consSinNumProvExt.jsp';
				}
			}
		]
	})
	
		var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	
	if(emergente=='S'){
		Ext.getCmp('chkConvenio1').show();
		Ext.getCmp('chkConvenio').show();	
		fp.el.mask('Enviando...', 'x-mask-loading');	
		consultaData.load({
			params: Ext.apply(fp.getForm().getValues(),{
				operacion: 'Generar',
				emergente:emergente
			})
		});
	}
	
});