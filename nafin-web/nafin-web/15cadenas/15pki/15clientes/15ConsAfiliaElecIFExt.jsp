<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>

<% 
String pantalla	= (request.getParameter("pantalla")==null)?"A":request.getParameter("pantalla"); 
String ic_if	= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if"); 
String ic_epo	= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo"); 
String ic_cuenta	= (request.getParameter("ic_cuenta")==null)?"":request.getParameter("ic_cuenta"); 
String origenPantalla	= (request.getParameter("origenPantalla")==null)?"":request.getParameter("origenPantalla"); 
	
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="../certificado.jspf" %>

<script language="JavaScript1.2" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<% if(pantalla.equals("A")){%>
<script type="text/javascript" src="15ConsAfiliaElecIFExt.js?<%=session.getId()%>"></script>
<%}else if(pantalla.equals("C")){%>
<script type="text/javascript" src="15ConsAfiliaElecIFCausaExt.js?<%=session.getId()%>"></script>
<%}%>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01if/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01if/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
<form id='formParametros' name="formParametros">
	<input type="hidden" id="ic_if" name="ic_if" value="<%=ic_if%>"/>
	<input type="hidden" id="ic_epo" name="ic_epo" value="<%=ic_epo%>"/>	
	<input type="hidden" id="ic_cuenta" name="ic_cuenta" value="<%=ic_cuenta%>"/>
	<input type="hidden" id="origenPantalla" name="origenPantalla" value="<%=origenPantalla%>"/>		
</form>	

</body>
</html>