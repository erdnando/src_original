<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		java.text.*, 	
		java.net.*,
		java.io.*,		
		netropology.utilerias.*,	
		com.netro.afiliacion.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,		
		com.netro.exception.*,
		com.netro.model.catalogos.*,
		com.netro.seguridadbean.SeguException,
		com.netro.exception.NafinException,	
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%@ include file="../certificado.jspf" %>

<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String ic_epo = (request.getParameter("ic_epo") != null) ? request.getParameter("ic_epo") : "";
String ic_if = (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") : "";
String txt_fecha_sol_de = (request.getParameter("txt_fecha_sol_de") != null) ? request.getParameter("txt_fecha_sol_de") : "";
String txt_fecha_sol_a = (request.getParameter("txt_fecha_sol_a") != null) ? request.getParameter("txt_fecha_sol_a") : "";
String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
String rfc = (request.getParameter("rfc") != null) ? request.getParameter("rfc") : "";
String pagina = (request.getParameter("pagina") != null) ? request.getParameter("pagina") : "A";
String ic_cuenta = (request.getParameter("ic_cuenta")!=null)?request.getParameter("ic_cuenta"):"";	
String num_electronico = (request.getParameter("num_electronico")!=null)?request.getParameter("num_electronico"):"";	
String txtNombre = (request.getParameter("txtNombre")!=null)?request.getParameter("txtNombre"):"";	
String origenPantalla	= (request.getParameter("origenPantalla")==null)?"":request.getParameter("origenPantalla"); 

JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

if(strTipoUsuario.equals("IF"))
	ic_if = iNoCliente;

String infoRegresar = "";

Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);


if(informacion.equals("pymeNombre")  ) {
	
	List datosPymes =  BeanAfiliacion.datosPymes(num_electronico, ic_epo ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);
	infoRegresar = jsonObj.toString();		


}  else  if(informacion.equals("CatalogoEPO")){

	CatalogoEPOS cat = new CatalogoEPOS();
	cat.setCampoClave("E.ic_epo");
	cat.setCampoDescripcion("E.cg_razon_social");		
	cat.setIntermediario(iNoCliente);
	cat.setAceptacion("H");
	cat.setOrden("E.cg_razon_social");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CatalogoCausa")){

	HashMap info = new HashMap();	
	String id_docto = (request.getParameter("id_docto")!=null)?request.getParameter("id_docto"):"";	
	
	ArrayList ListCausas = BeanAfiliacion.getCausas(id_docto,25);
		info.put("clave", "0");
		info.put("descripcion", "Selecciona una causa ");
		registros.add(info);
	for(int j = 0; j < ListCausas.size(); j++) {
		List listCausa = (ArrayList) ListCausas.get(j);
		String id_causa = listCausa.get(0).toString();
		String descripcion_causa = listCausa.get(1).toString();		
		info = new HashMap();
		info.put("clave", id_causa);
		info.put("descripcion", descripcion_causa);	
		registros.add(info);
	}
	
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";				
					
} else if (informacion.equals("busquedaAvanzada") ) {

	String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
	
		
	CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");	
	if(!rfcPyme.equals(""))
	cat.setRfc_pyme(rfcPyme);
	if(!nombrePyme.equals(""))
	cat.setNombre_pyme(nombrePyme);
	if(!ic_epo.equals(""))
	cat.setIc_epo(ic_epo);
	cat.setPantalla("SoliAfi");
		
	cat.setOrden("p.cg_razon_social");	
	
	infoRegresar = cat.getJSONElementos();

} else if (informacion.equals("Consultar") ||  informacion.equals("ArchivoCSV") )  {
			
	ConsAfiliaElectronica paginador = new ConsAfiliaElectronica();
	
	if(!"".equals(num_electronico) && "".equals(ic_pyme)) {
		List datosPymes =  BeanAfiliacion.datosPymes(num_electronico, ic_epo ); //para obtener el No de la Pyme 
		if(datosPymes.size()>0){
			ic_pyme= (String)datosPymes.get(0);
		}
	}
	
	paginador.setIc_if(ic_if);
	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setTxt_fecha_sol_de(txt_fecha_sol_de);
	paginador.setTxt_fecha_sol_a(txt_fecha_sol_a);
	paginador.setTipoConsulta(pagina);
	
		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
	String consulta2 ="";
	if (informacion.equals("Consultar") ){		
		if (operacion.equals("Generar") ){
			try {
				Registros reg	=	queryHelper.doSearch();
				consulta2	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			}	catch(Exception e) {
				throw new AppException("Error en la paginación", e);
			}	
		}
		
		jsonObj = JSONObject.fromObject(consulta2);		
		jsonObj.put("pagina",pagina);					
		infoRegresar = jsonObj.toString();
		
	}	else if (informacion.equals("ArchivoCSV")){
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
				
		}catch(Throwable e){
			throw new AppException("Error al generar el archivo CSV",e);
		}  
	}

}	else if (informacion.equals("ConsultarPreAcuse") ){


		String cuentas[] = request.getParameterValues("cuentas");
		StringBuffer  cuentasBancarias = new StringBuffer();
		for(int i=0;i<cuentas.length;i++) {			
			cuentasBancarias.append(cuentas[i]+",");
		}		
		
		String cuentaB= 	cuentasBancarias.deleteCharAt(cuentasBancarias.length()-1).toString();		
				
		List  regis =  BeanAfiliacion.consultaSolicAficiacion( cuentaB, iNoCliente, "PreAcuse");
		
		for(int i = 0; i < regis.size(); i++){
			List info =(List)regis.get(i);
			
			String nombrePyme  =info.get(0).toString();
			String  fecha_electronica =info.get(1).toString();
			String nombreIF =info.get(2).toString();
			String nombreEPO =info.get(3).toString();
			String fecha_convenio =info.get(4).toString();
			String datos_cuenta =info.get(5).toString();
			String ic_pyme1 =info.get(9).toString();
			String ic_epo1 =info.get(10).toString();
			String ic_cuenta_bancaria =info.get(12).toString();	
			
			datos_cuenta = "Cuenta No. "+info.get(5).toString()+"  "+info.get(6).toString()+" Sucursal "+info.get(7).toString()+
								"Plaza "+info.get(8).toString();
			datos = new HashMap();
			datos.put("NOMBRE_PYME", nombrePyme);
			datos.put("FECHA_ELECTRONICA", fecha_electronica);
			datos.put("NOMBRE_IF", nombreIF);
			datos.put("NOMBRE_EPO", nombreEPO);
			datos.put("FECHA_CONVENIO", fecha_convenio);
			datos.put("DATOS_CUETA", datos_cuenta);
			datos.put("IC_PYME", ic_pyme1);
			datos.put("IC_EPO", ic_epo1);
			datos.put("IC_CUENTA_BANCARIA", ic_cuenta_bancaria);		
			registros.add(datos);		
			
		}//for
		
		String  consulta2 =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta2);
		jsonObj.put("cuentasBancarias",cuentaB);		
		infoRegresar = jsonObj.toString();
		

}	else if (informacion.equals("ConfirmaAcuse")){


		String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
		String textoFirmar = (request.getParameter("textoFirmar")!=null)?request.getParameter("textoFirmar"):"";
		String cg_ip 				=(request.getRemoteAddr());
		String	cuentas[]		=	request.getParameterValues("cuentas");
		String	epos[]		=	request.getParameterValues("epos");
		String	pymes[]		=	request.getParameterValues("pymes");
				
		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formatoCert = new SimpleDateFormat("ddMMyyyyHHmmss");
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formatoHora = new SimpleDateFormat("hh:mm aa");
		String folioCert = formatoCert.format(calendario.getTime());
		String fechaCarga = formatoFecha.format(calendario.getTime());
		String horaCarga = formatoHora.format(calendario.getTime());	
		
		Acuse acuse = null;
		acuse = new Acuse(Acuse.ACUSE_EPO, "1");
		String ses_ic_usuario = iNoUsuario;
		boolean bInsertaAcuse = false;	
		String mensajeError="";
		String _acuse  ="";
	
		char getReceipt = 'Y';	
		if (!_serial.equals("") && !textoFirmar.equals("") && !pkcs7.equals("")) {
			folioCert = acuse.toString();			
			Seguridad s = new Seguridad();
				
			if (s.autenticar(folioCert, _serial, pkcs7, textoFirmar, getReceipt)) {
				_acuse = s.getAcuse();
				
				System.out.println(" folioCert = " + folioCert + " , _acuse = " + _acuse);// Debug info
				
				bInsertaAcuse = BeanAfiliacion.autorizacionIF( ic_if,  epos, pymes,  cuentas ,  strLogin ,   iNoNafinElectronico.toString(),  cg_ip);
			}	
			
			if(bInsertaAcuse ==false ||  _acuse.equals("") ) {
				mensajeError = "La autentificación no se llevo acabo con éxito"+s.mostrarError();			
			}
			
		}	
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("acuse",_acuse);
		jsonObj.put("mensajeError",mensajeError);
		infoRegresar = jsonObj.toString();	
	
	System.out.println(" infoRegresar = " + infoRegresar  );
	
}	else if (informacion.equals("ArchivoAcuse")  ){

	String acuse2 = (request.getParameter("acuse2")!=null)?request.getParameter("acuse2"):"";
	String cuentasBancarias = (request.getParameter("cuentasBancarias")!=null)?request.getParameter("cuentasBancarias"):"";
	
		
	List datosA = new ArrayList();
	datosA.add(strDirectorioTemp);
	datosA.add(strDirectorioPublicacion);
	datosA.add(acuse2);
	datosA.add(cuentasBancarias);
	datosA.add(ic_if);
	datosA.add((String)session.getAttribute("strPais"));
	datosA.add((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString());
	datosA.add((String)session.getAttribute("sesExterno"));
	datosA.add((String)session.getAttribute("strNombre"));
	datosA.add((String)session.getAttribute("strNombreUsuario"));
	datosA.add((String)session.getAttribute("strLogo"));	
	
	String nombreArchivo= BeanAfiliacion.AcusePDFAuto(datosA);


	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();	

}	else if (informacion.equals("ConsultarCausa") ){
	
	ArrayList alIds = new ArrayList();
	alIds.add("1");
	alIds.add("2");
	alIds.add("3");
	alIds.add("4");
	alIds.add("5");
	alIds.add("6");
	alIds.add("7");
	alIds.add("8");
	alIds.add("9");
	alIds.add("10");
	alIds.add("11");
	alIds.add("12");
	alIds.add("13");
	alIds.add("14");
	alIds.add("21");
	alIds.add("22");
	alIds.add("37");
	alIds.add("38");
	
	// metodos que me dan  los datos capturados			
	HashMap map = BeanAfiliacion.getDoctosCausas(ic_cuenta,ic_if,ic_epo);
	String notaAuxIf = BeanAfiliacion.getNota(ic_cuenta,ic_if,ic_epo);
	
	HashMap datosPyme = BeanAfiliacion.getPymedeCuentaBanc(ic_cuenta,ic_if,ic_epo);
	String sTipoPersona = (String)datosPyme.get("tipo_persona");		
	
	ArrayList ListDoctos = BeanAfiliacion.getDoctos(alIds,sTipoPersona);
	int tam = ListDoctos.size();	

	for(int i = 0; i < ListDoctos.size(); i++){
	
		List listDocto =(List)ListDoctos.get(i);		
		
		String id_docto = listDocto.get(0).toString();
		String DOCUMENTO = listDocto.get(1).toString();	
		String tipo_Persona = (listDocto.get(2).toString().equals("F")? "FISICA":"MORAL");	
		boolean rechaza  = (map.containsKey(id_docto));
		String rechazo ="", causa="0", descripCausa="";
		if(rechaza){ rechazo ="true"; 
		 causa = map.get(id_docto).toString();
			//consulto el la descripcion de la causa		 
		  descripCausa  = BeanAfiliacion.descripCausa(id_docto , causa );
		 }
				
		datos = new HashMap();
		datos.put("IC_DOCUMENTO", id_docto);
		datos.put("DOCUMENTO", DOCUMENTO);		
		datos.put("RECHAZAR", rechazo);
		datos.put("CAUSA", causa);
		datos.put("DESCRIPCION_CAUSA", descripCausa);
		registros.add(datos);		
			
		}//for
		
		String  consulta2 =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta2);	
		jsonObj.put("ic_cuenta",ic_cuenta);
		jsonObj.put("ic_epo",ic_epo);	
		jsonObj.put("notaAuxIf",notaAuxIf);	
		jsonObj.put("origenPantalla",origenPantalla);	
		infoRegresar = jsonObj.toString();
		
}	else if (informacion.equals("GuardarCausaRechazo") ){

		String mensaje ="", causas2="";
		String notaIF = (request.getParameter("notaIF")!=null)?request.getParameter("notaIF"):"";			
		String	causas[]		=	request.getParameterValues("causas");
		for(int i=0;i<causas.length;i++){
			if(!causas[i].equals("0")){
				causas2 +=causas[i]+",";	
			}
		}		
		//se realizo con exito la actualizacion de las causas		
		if (BeanAfiliacion.realizaRechazo(ic_cuenta,ic_if,ic_epo,notaIF,causas2.toString())){
			mensaje = "La actualización se realizó con éxito";		
		}else{
			mensaje= "La actualización no se realizó con éxito Debido a problemas tecnicos";	
		}
		
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("mensaje",mensaje);	
		jsonObj.put("origenPantalla",origenPantalla);		
		infoRegresar = jsonObj.toString();	
}


%>
<%=infoRegresar%>

