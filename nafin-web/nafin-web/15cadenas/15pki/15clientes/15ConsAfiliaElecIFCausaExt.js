
Ext.onReady(function() {
	
	
	var ic_if  = Ext.getDom('ic_if').value;
	var ic_epo  = Ext.getDom('ic_epo').value;
	var ic_cuenta  = Ext.getDom('ic_cuenta').value;
	var origenPantalla  = Ext.getDom('origenPantalla').value;
	var documentos = [];
	var causas = [];
	var cancelar =  function() {  
	 documentos = [];	
	 causas = [];
	}
	
	var  mensaje = '<table width="650" cellpadding="3" cellspacing="1" border="0">'+	
						 '<tr><td class="formas" align="left" colspan="3"><B> �IMPORTANTE :</B></td></tr>'+
						 '<tr><td class="formas" style="text-align: justify;" colspan="3">Favor de seleccionar las causas por cada documento.</td></tr>'+
						 '<tr><td class="formas" style="text-align: justify;" colspan="3"> &nbsp;&nbsp;</td></tr>'+ 
						 '</table>';
						
	var mensajeCausa = new Ext.Container({
		layout: 'table',			
		id: 'mensajeCausa',							
		width:	'650',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		mensaje				
			}			
		]
	});
	
	var procesarSuccessFailureAcuse =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){
								 
				var  mensajeCausa2 = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+	
						 '<tr><td class="formas" align="left" colspan="3"><B> '+jsondeAcuse.mensaje+'</B></td></tr>'+
						 '</table>';					
				
				var mostrarMensaje = new Ext.Container({
					layout: 'table',					
					id: 'mostrarMensaje',							
					width:	'250',
					heigth:	'auto',
					style: 'margin:0 auto;',
					items: [			
						{ 
							xtype:   'label',  
							html:		mensajeCausa2				
						}			
					]
				});
				
				
			//Contenedor para los botones de imprimir y Salir 
			var fpBotones = new Ext.Container({
				layout: 'table',
				id: 'fpBotones',			
				width:	'250',
				heigth:	'auto',
				style: 'margin:0 auto;',
				items: [		
				{
					xtype: 'button',
					text: 'Salir',			
					id: 'btnSalir',	
					hidden: true,
					handler: function() {
						window.location = "15ConsAfiliaElecIFExt.jsp";
					}
				},
				{
					xtype: 'button',
					text: 'Salir',			
					id: 'btnSalirOrigen',	
					hidden: true,
					handler: function() {
						window.location = "/nafin/15cadenas/15pki/15mantenimiento/15parametrizacion/15if/15admparamifpymeext.jsp";
					}
				}				
			]
			});
					
			if(jsondeAcuse.origenPantalla=='15admparamifpymeExt.jsp'){
				Ext.getCmp('btnSalirOrigen').show();
			}else {
				Ext.getCmp('btnSalir').show();
			}		
				var mensajeCausa = Ext.getCmp('mensajeCausa');
				var gridConsulta = Ext.getCmp('gridConsulta');
				var fp = Ext.getCmp('forma');
				mensajeCausa.hide();
				gridConsulta.hide();
				fp.hide();							
							
				var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
				 contenedorPrincipalCmp.add(mostrarMensaje);
				 contenedorPrincipalCmp.add(fpBotones);
				 contenedorPrincipalCmp.doLayout();
 
			}	
						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	
	
	var procesarCausaRechazo = function() {
	
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		cancelar();	
	
		store.each(function(record) {			
					
			if( record.data['RECHAZAR'] ==true ||  record.data['RECHAZAR'] =='true'){			
				documentos.push(record.data['IC_DOCUMENTO']);			
				causas.push(record.data['CAUSA']);				
			}	
			
		});		
		if(documentos =='' || causas=='' ) {
			Ext.MessageBox.alert('Mensaje','Por favor debe seleccionar una causa de rechazo');
			return false;	 
		}
		
		var notaIF = Ext.getCmp('notaIF');
		if( Ext.isEmpty(notaIF.getValue() )  ) {				
			Ext.MessageBox.alert('Mensaje','Por favor debe seleccionar una nota');
			return false;	
		}
				
		var origenPantalla = Ext.getCmp('origenPantalla').getValue();
		Ext.Ajax.request({
			url : '15ConsAfiliaElecIF.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'GuardarCausaRechazo',
				ic_cuenta: ic_cuenta,
				ic_if: ic_if,
				ic_epo: ic_epo,
				causas:causas,
				origenPantalla:origenPantalla
			}),
			callback: procesarSuccessFailureAcuse
		});		
	}		
	
	var CatalogoCausasRechazo = new Ext.data.JsonStore({
		id: 'storeCausasRechazo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15ConsAfiliaElecIF.data.jsp',
		baseParams: {
			informacion: 'CatalogoCausa'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});	
	
	//para la creacion del combo en el grid
	var comboCausasRechazo = new Ext.form.ComboBox({  
		triggerAction : 'all',  
		displayField : 'descripcion',  
		valueField : 'clave', 
		typeAhead: true,
		width: 250,
		minChars : 1,
		allowBlank: true,
		store :CatalogoCausasRechazo 
	});
	
	Ext.util.Format.comboRenderer = function(comboCausasRechazo){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var valor = registro.data['DESCRIPCION_CAUSA'];					
			if(value!='0'&& valor ==''){
				var record = comboCausasRechazo.findRecord(comboCausasRechazo.valueField, value);
				return record ? record.get(comboCausasRechazo.displayField) : comboCausasRechazo.valueNotFoundText;
			} 
			if(value!='0'&& valor !=''){
				return valor;
			}
		}
	}	
	
	
		
	var procesarConsultaData = function(store, arrRegistros, opts) 	{		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
		}	
		var el = gridConsulta.getGridEl();
		var jsonData = store.reader.jsonData;			
		ic_cuenta =jsonData.ic_cuenta;
		ic_epo =jsonData.ic_epo;		
		Ext.getCmp("notaIF").setValue(jsonData.notaAuxIf);  
		Ext.getCmp("origenPantalla").setValue(jsonData.origenPantalla);  		
		
		if(jsonData.origenPantalla=='15admparamifpymeExt.jsp'){
			Ext.getCmp('btnCancelarOri').show();
		}else {
			Ext.getCmp('btnCancelar').show();
		}				
		
		if(store.getTotalCount() > 0) {			
			el.unmask();			
		} else {				
			el.mask('No se encontr� ning�n registro', 'x-mask');				
		}	
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15ConsAfiliaElecIF.data.jsp',
		baseParams: {
			informacion: 'ConsultarCausa'
		},
		fields: [
			{name: 'IC_DOCUMENTO'},
			{name: 'DOCUMENTO'},
			{name: 'RECHAZAR'},
			{name: 'CAUSA'},
			{name: 'DESCRIPCION_CAUSA'}				
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	})
	
		//Inicializa validaciones
	var actualizaRechazo =  function( valor, metadata, registro, info) { 
		var rechazo= registro.data['RECHAZAR'];	
		//alert(rechazo);
		if(rechazo=='true') {	
			registro.data['RECHAZAR']==true;
			registro.commit();			
		}
	}
	
	 var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		title: 'Documentos para Validar',		
		clicksToEdit: 1,
		store: consultaData,	
		columns: [						
			{							
				header : 'Documento a Validar',
				tooltip: 'Documento a Validar',
				dataIndex : 'DOCUMENTO',
				width : 150,
				align: 'left',
				sortable : true,
				resizable: true	
			},
			{
				xtype: 'checkcolumn',
				header : 'Rechazar',
				tooltip: 'Rechazar',
				width : 150,
				align: 'center',
				dataIndex : 'RECHAZAR',				
				sortable : false			
			},		
			{							
				header : 'Causa del Rechazo',
				tooltip: 'Causa del Rechazo',
				dataIndex : 'CAUSA',
				width : 300,
				align: 'center',
				sortable : true,
				resizable: true,
				editor:comboCausasRechazo,				
				renderer: Ext.util.Format.comboRenderer(comboCausasRechazo)					
			}			
		],
		isplayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 650,
		style: 'margin:0 auto;',
		frame: false,
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
				var id_docto = record.data['IC_DOCUMENTO'];	
				
				if( record.data['RECHAZAR'] ==true ||  record.data['RECHAZAR'] =='true'){	
					record.data['RECHAZAR']=true;
					record.commit();
					CatalogoCausasRechazo.load({
						params: {  id_docto: id_docto 	}
					});	
					return true; // es editable				
				}else {						
					return false; // no es editable 	
				}		
				
			},
			validateedit : function(e){// se dispara para calular datos que al caputar los campos editables 
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;				
				if( record.data['RECHAZAR'] ==''){	
					record.data['CAUSA']='';										
				}					
				if( record.data['CAUSA'] !=''){	
					record.data['DESCRIPCION_CAUSA']='';
				}
				record.commit();	
			}			
		}
	});
			
	var elementosForma = [	
		{	xtype: 'textfield',	hidden: true, id: 'origenPantalla' },
		{
			xtype: 'displayfield',
			value: 'Notas:'		
		},
		{
			xtype: 'textarea',
			name: 'notaIF',
			id: 'notaIF',
			fieldLabel: '',
			msgTarget: 'side',			
			maxLength: 260,
			maxLengthText: 'La nota no puede superar los 260 caracteres. Por favor escriba una nota mas corta.',			
			width: 500,
			colspan: 2
		}		
	]
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,	
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		items: elementosForma,			
		monitorValid: true,
		buttons: [
		{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: procesarCausaRechazo
			},					
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				id: 'btnCancelar',
				hidden: true,
				handler: function() {
					window.location = '15ConsAfiliaElecIFExt.jsp';
				}
			},
			{
				text: 'Cancelar',
				id: 'btnCancelarOri',
				iconCls: 'icoLimpiar',
				hidden: true,
				handler: function() {
					window.location = '/nafin/15cadenas/15pki/15mantenimiento/15parametrizacion/15if/15admparamifpymeext.jsp';
				}
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),			
			mensajeCausa,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),
			fp, 
			NE.util.getEspaciador(20)
		]
	});
	
	consultaData.load({
		params: {
			ic_if: ic_if,
			ic_epo:ic_epo,
			ic_cuenta:ic_cuenta,
			origenPantalla:origenPantalla
		}
	});
	
	CatalogoCausasRechazo.load();
	
	
});