
Ext.onReady(function() {
	
	var cuentas = [];
	var pymes = [];
	var epos = [];
	var mensaje;
	var cuentasBancarias;
	var acuse;
	
	var cancelar =  function() {  
	 cuentas = [];
	 pymes = [];
	 epos = [];
	}
					
	//GENERAR ARCHIVO  PDF
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnImprimir = Ext.getCmp('btnImprimir');
		btnImprimir.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnImprimir.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureAcuse =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			
			if (jsondeAcuse != null){
			
				acuse =jsondeAcuse.acuse;				
				var btnImprimir = Ext.getCmp('btnImprimir');
				var btnBajarPDF = Ext.getCmp('btnBajarPDF');
				var btnAutorizarP = Ext.getCmp('btnAutorizarP');
				var gridPreAcuse = Ext.getCmp('gridPreAcuse');
				var mensajePreAcuse = Ext.getCmp('mensajePreAcuse');
				var mensajeAcuse2 = Ext.getCmp('mensajeAcuse2');
				
				var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
				//var mensajeAcuse;
				mensajePreAcuse.hide();	
				btnBajarPDF.hide();	
								
				if(acuse !=''){					 	
					btnAutorizarP.hide();
					btnImprimir.show();console.log('### '+acuse);
					Ext.getCmp('mensajeAc').setValue('<table width="500" cellpadding="3" cellspacing="1" border="0">'+	
						'<tr><td class="formas"  align="center" colspan="3"><B> Acuse de recibo de Autorizaci�n de Cuenta(s) Bancaria(s) PYME</B></td></tr>'+
						 '<tr><td class="formas"  align="center" colspan="3"><B> No. '+acuse+'</B></td></tr>'+
						 '<tr><td class="formas" style="text-align: justify;" colspan="3"> &nbsp;&nbsp;</td></tr>'+ 		
						 '<tr><td class="formas" style="text-align: justify;" colspan="3"><B>	CONFIRMACI�N DE LA ACEPTACI�N Y ADHESI�N ELECTR�NICAMENTE POR UN PROVEEDOR O "PYME", RESPECTO DEL "CONVENIO PARA EL INGRESO A LAS CADENAS PRODUCTIVAS PARA EL DESARROLLO DE PROVEEDORES POR MEDIOS ELECTRONICOS", CELEBRADO ENTRE EL INTERMEDIARIO FINANCIERO ("IF") Y NACIONAL FINANCIERA, S.N.C. (EN LO SUCESIVO "NAFIN"), ASI COMO RESPECTO DE LA CORRESPONDIENTE "CARTA DE ACEPTACI�N O INGRESO", FORMALIZADA ENTRE ESE MISMO "IF" Y LA EMPRESA DE PRIMER ORDEN ("EPO").</B></td></tr>'+
						 '</table>');
					 
	
					mensajeAcuse2.show();
					
				} else {
					gridPreAcuse.hide();
					var  mensaje = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+	
							 '<tr><td class="formas" align="left" colspan="3"><B> '+jsondeAcuse.mensajeError+'</B></td></tr>'+
							 '</table>';					
					var mostrarMensaje = new Ext.Container({
						layout: 'table',					
						id: 'mostrarMensaje',							
						width:	'500',
						heigth:	'auto',
						style: 'margin:0 auto;',
						items: [			
							{ 
								xtype:   'label',  
								html:		mensaje				
							}			
						]
					});
					//Contenedor para los botones de imprimir y Salir 
					var fpBotones = new Ext.Container({
						layout: 'table',
						id: 'fpBotones',			
						width:	'250',
						heigth:	'auto',
						style: 'margin:0 auto;',
						items: [		
						{
							xtype: 'button',
							text: 'Salir',			
							id: 'btnSalir',		 
							handler: function() {
								window.location = "15ConsAfiliaElecIFExt.jsp";
							}
						}	
					]
				});
				
				var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
				 contenedorPrincipalCmp.add(mostrarMensaje);
				 contenedorPrincipalCmp.add(NE.util.getEspaciador(20));
				 contenedorPrincipalCmp.add(fpBotones);			 
				 contenedorPrincipalCmp.doLayout();
				
			}
				
				
				/*
				if(jsondeAcuse.mensajeError !=''){							
					Ext.MessageBox.alert("Mensaje",jsondeAcuse.mensajeError);
					document.location.href = "15ConsAfiliaElecIFExt.jsp";	
				}	
				*/
				
				
				
				
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var confirmar = function(pkcs7, vtextoFirmar, vcuentas, vepos, vpymes){
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}
		
		Ext.Ajax.request({
			url : '15ConsAfiliaElecIF.data.jsp',
			params : {
				pkcs7: pkcs7,
				textoFirmar: vtextoFirmar,
				informacion: 'ConfirmaAcuse',
				cuentas:vcuentas,
				epos:vepos,
				pymes:vpymes				
			},
			callback: procesarSuccessFailureAcuse
		});		
	}	
						
	var procesarAcuse =  function(opts, success, response) {	
		var gridPreAcuse = Ext.getCmp('gridPreAcuse');
		var textoFirmar = " CONFIRMACI�N DE LA ACEPTACI�N Y ADHESI�N ELECTRONICAMENTE POR UN PROVEEDOR O PYME , RESPECTO DEL CONVENIO PARA EL INGRESO A LAS CADENAS PRODUCTIVAS PARA EL DESARROLLO DE PROVEEDORES POR MEDIOS ELECTRONICOS , CELEBRADO ENTRE EL INTERMEDIARIO FINANCIERO (IF) Y NACIONAL FINANCIERA, S.N.C. (EN LO SUCESIVO NAFIN ), ASI COMO RESPECTO DE LA CORRESPONDIENTE "+
									" CARTA DE ACEPTACI�N O INGRESO , FORMALIZADA ENTRE ESE MISMO IF Y LA EMPRESA DE PRIMER ORDEN (EPO).\n"+
									" Nombre o Raz�n Social de la PYME,Fecha de la Adhesi�n Electr�nica,IF,EPO,Fecha del Convenio para el ingreso a las cadenas productivas para el desarrollo de proveedores por medios electr�nicos,Datos de la cuenta bancaria de la PYME en la que solicit� se le hagan los dep�sitos derivados de las operaciones de factoraje financiero electr�nico que se celebren a trav�s del Sistema de Cadenas Productivas de Nafin \n";

		var store = gridPreAcuse.getStore();
		cancelar();	
		store.each(function(record) {
			textoFirmar += record.data['NOMBRE_PYME']+", "+ record.data['FECHA_ELECTRONICA']+", "+ record.data['NOMBRE_IF']+
								record.data['NOMBRE_EPO']+", "+ record.data['FECHA_CONVENIO']+", "+ record.data['DATOS_CUETA'];
			
			cuentas.push(record.data['IC_CUENTA_BANCARIA']);	
			epos.push(record.data['IC_EPO']);	
			pymes.push(record.data['pymes']);	
		});		
		
		NE.util.obtenerPKCS7(confirmar, textoFirmar,  cuentas, epos, pymes);	
			
	}	
	
	var procesarConsultaDataPre = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridPreAcuse.isVisible()) {
				gridPreAcuse.show();
			}	
		}
		var jsonData = store.reader.jsonData;	
		
		cuentasBancarias =jsonData.cuentasBancarias;
		
		var el = gridPreAcuse.getGridEl();		
		var mensajePreAcuse = Ext.getCmp('mensajePreAcuse');
		var gridConsulta = Ext.getCmp('gridConsulta');
		var btnImprimir = Ext.getCmp('btnImprimir');
		var btnBajarPDF = Ext.getCmp('btnBajarPDF');
		var btnAutorizarP = Ext.getCmp('btnAutorizarP');
		btnAutorizarP.show();
		btnImprimir.hide();
		btnBajarPDF.hide();				
		mensajePreAcuse.show();
		fp.hide();
		gridConsulta.hide();
					
		if(store.getTotalCount() > 0) {		
			el.unmask();			
		} else {					
			el.mask('No se encontr� ning�n registro', 'x-mask');				
		}	
	}		
	mensaje = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+	
						 '<tr><td class="formas"  align="center" colspan="3"><B> Pre-acuse de recibo de Autorizaci�n de Cuenta(s) Bancaria(s) PYME</B></td></tr>'+
						 '<tr><td class="formas" style="text-align: justify;" colspan="3"> &nbsp;&nbsp;</td></tr>'+ 		
						 '<tr><td class="formas" style="text-align: justify;" colspan="3"><B>	CONFIRMACI�N DE LA ACEPTACI�N Y ADHESI�N ELECTR�NICAMENTE POR UN PROVEEDOR O "PYME", RESPECTO DEL "CONVENIO PARA EL INGRESO A LAS CADENAS PRODUCTIVAS PARA EL DESARROLLO DE PROVEEDORES POR MEDIOS ELECTRONICOS", CELEBRADO ENTRE EL INTERMEDIARIO FINANCIERO ("IF") Y NACIONAL FINANCIERA, S.N.C. (EN LO SUCESIVO "NAFIN"), ASI COMO RESPECTO DE LA CORRESPONDIENTE "CARTA DE ACEPTACI�N O INGRESO",  FORMALIZADA ENTRE ESE MISMO "IF" Y LA EMPRESA DE PRIMER ORDEN ("EPO").</B></td></tr>'+
						 '</table>';
						
	var mensajePreAcuse = new Ext.Container({
		layout: 'table',	
		hidden: true,
		id: 'mensajePreAcuse',							
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		mensaje				
			}			
		]
	});
	
	var consultaDataPreAcuse = new Ext.data.JsonStore({
		root : 'registros',
		url : '15ConsAfiliaElecIF.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreAcuse'
		},
		fields: [
			{name: 'NOMBRE_PYME'},
			{name: 'FECHA_ELECTRONICA'},
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_EPO'},
			{name: 'FECHA_CONVENIO'},
			{name: 'DATOS_CUETA'},	
			{name: 'IC_PYME'},			
			{name: 'IC_EPO'},
			{name: 'IC_CUENTA_BANCARIA'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataPre,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataPre(null, null, null);						
				}
			}
		}
	});	
	
	
	var gridPreAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridPreAcuse',
		title: '',
		hidden: true,
		clicksToEdit: 1,
		store: consultaDataPreAcuse,		
		columns: [							
			{							
				header : 'Nombre o Raz�n Social de la PYME',
				tooltip: 'Nombre o Raz�n Social de la PYME',
				dataIndex : 'NOMBRE_PYME',
				width : 150,
				align: 'left',
				sortable : true,
				resizable: true	
			},			
			{							
				header : 'Fecha de la Adhesi�n Electr�nica',
				tooltip: 'Fecha de la Adhesi�n Electr�nica',
				dataIndex : 'FECHA_ELECTRONICA',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true	
			},
			{							
				header : 'IF',
				tooltip: 'IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				align: 'left',
				sortable : true,
				resizable: true	
			},
			{							
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'NOMBRE_EPO',
				width : 150,
				align: 'left',
				sortable : true,
				resizable: true	
			},
			{							
				header : 'Fecha del "Convenio para el ingreso a las cadenas productivas para el desarrollo de proveedores por medios electr�nicos',
				tooltip: 'Fecha del "Convenio para el ingreso a las cadenas productivas para el desarrollo de proveedores por medios electr�nicos',
				dataIndex : 'FECHA_CONVENIO',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true	
			},
			{							
				header : 'Datos de la cuenta bancaria de la PYME en la que solicit� se le hagan los dep�sitos derivados de las operaciones de factoraje financiero electr�nico que se celebren a trav�s del Sistema de "Cadenas Productivas" de Nafin',
				tooltip: 'Datos de la cuenta bancaria de la PYME en la que solicit� se le hagan los dep�sitos derivados de las operaciones de factoraje financiero electr�nico que se celebren a trav�s del Sistema de "Cadenas Productivas" de Nafin',
				dataIndex : 'DATOS_CUETA',
				width : 150,
				align: 'left',
				sortable : true,
				resizable: true	
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 900,
		style: 'margin:0 auto;',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',
				'-',				
				{
					xtype: 'button',
					id: 'btnAutorizarP',
					text: 'Autorizar Solicitudes'
					,handler: procesarAcuse					
				},				
				{
					xtype: 'button',
					id: 'btnImprimir',
					text: 'Imprimir PDF',
					hidden: true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15ConsAfiliaElecIF.data.jsp',
							params: {
								informacion:'ArchivoAcuse',
								cuentasBancarias:cuentasBancarias,
								acuse2:acuse
							},
							callback: procesarSuccessFailurePDF
						});
					}					
				},
				
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar Archivo',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-',	
				{
					text: 'Regresar',
					id: 'btnRegresar',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '15ConsAfiliaElecIFExt.jsp';
					}	
				}
			]
		}
	});
	
		
	
	
	var mensajeAcuse2 = new Ext.Container({
		layout: 'table',	
		hidden: true,
		id: 'mensajeAcuse2',							
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items:[{
			xtype: 'displayfield',
			id: 'mensajeAc',
			value: ''
			
			}
		]
		
	});
	
	
	
			
	var procesarGuardar = function() {
	
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		cancelar();	
	
		store.each(function(record) {					
			if( record.data['SELECCIONAR'] ==true){			
				cuentas.push(record.data['IC_CUENTA_BANCARIA']);		
			}		
		});
		
		if(cuentas =='') {
			Ext.MessageBox.alert('Mensaje','Debe seleccionar al menos un registro');
			return false;	
		}
		
		consultaDataPreAcuse.load({
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ConsultarPreAcuse',
				cuentas:cuentas
			})
		});	
	}

	var procesoRechazo = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var ic_cuenta = registro.get('IC_CUENTA_BANCARIA');
		var ic_if = registro.get('IC_IF');
		var ic_epo = registro.get('IC_EPO');	
		
		var parametros = "pantalla=C"+"&ic_if="+ic_if+"&ic_epo="+ic_epo+"&ic_cuenta="+ic_cuenta;								
														
		document.location.href = "15ConsAfiliaElecIFExt.jsp?"+parametros;	
		
	}	
	
	var descargaArchivoExpediente1 = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var rfc = registro.get('RFC');		
		fp.getForm().getEl().dom.action = '/nafin/15cadenas/15mantenimiento/15clientes/popupexpediente.jsp?rfc='+escape(rfc)+'&version=ext';
		fp.getForm().getEl().dom.submit();
	}

	var descargaArchivoExpediente = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var rfc = registro.get('RFC');
		fp.getForm().getEl().dom.action = '/nafin/15cadenas/15mantenimiento/15clientes/popupexpediente.jsp?rfc='+escape(rfc)+'&version=ext';
		fp.getForm().getEl().dom.submit();
	}
	
	
	var procesarSuccessFailureArchivoConsulta1 =  function(opts, success, response) {
		var btnGenerarInf1 = Ext.getCmp('btnGenerarInf1');
		btnGenerarInf1.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV1 = Ext.getCmp('btnBajarCSV1');
			btnBajarCSV1.show();
			btnBajarCSV1.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV1.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarInf1.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var procesarSuccessFailureArchivoConsulta =  function(opts, success, response) {
		var btnGenerarInf = Ext.getCmp('btnGenerarInf');
		btnGenerarInf.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarInf.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
  	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
		}		
		var el = gridConsulta.getGridEl();
					
		var jsonData = store.reader.jsonData;	
		var  pagina = jsonData.pagina;	
		var btnAutorizar = Ext.getCmp('btnAutorizar');
		var btnGenerarInf = Ext.getCmp('btnGenerarInf');
		var btnBajarCSV = Ext.getCmp('btnBajarCSV');	
		var gridConsultaC = Ext.getCmp('gridConsultaC');
		btnBajarCSV.hide();
		gridConsultaC.hide();	
				
				
		if(store.getTotalCount() > 0) {			
			btnGenerarInf.enable();
			btnAutorizar.enable();
			el.unmask();			
		} else {			
			btnAutorizar.disable();
			btnGenerarInf.disable();
			el.mask('No se encontr� ning�n registro', 'x-mask');				
		}	
	}
	
  	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15ConsAfiliaElecIF.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'RAZON_SOCIAL'},
			{name: 'RFC'},
			{name: 'DOMICILIO'},
			{name: 'FECHA_SOLICITUD'},
			{name: 'MONEDA'},
			{name: 'IC_CUENTA_BANCARIA'},
			{name: 'CUENTA_BANCARIA'},
			{name: 'BANCO_SERVICIO'},
			{name: 'SUCURSAL'},
			{name: 'PLAZA'},
			{name: 'ESTATUS'},
			{name: 'SELECCIONAR'},			
			{name: 'IC_EPO'},
			{name: 'IC_IF'},
			{name: 'IC_PYME'}				
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	
	
	//esto esta en duda como manejarlo
	var gruposConsulta = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: 'Datos de la PyME', colspan: 5, align: 'center'},
				{	header: 'Datos de la Cuenta Bancaria', colspan: 5, align: 'center'},	
				{	header: ' ', colspan: 4, align: 'center'}
			]
		]
	});
	
  var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		title: 'Solicitud de Afiliaci�n Electr�nica',
		hidden: true,
		clicksToEdit: 1,
		store: consultaData,
		plugins: gruposConsulta,	
		columns: [
			{							
				header : 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex : 'NOMBRE_EPO',
				width : 150,				
				align: 'left',
				sortable : true,
				resizable: true,				
				renderer:function(value,metadata,registro){                                
				return '<span style="color:red;">' + value + '</span>';
				}
			},
			{							
				header : 'Nombre o Raz�n Social',
				tooltip: 'Nombre o Raz�n Social',
				dataIndex : 'RAZON_SOCIAL',
				width : 150,
				align: 'left',
				sortable : true,
				resizable: true,
				renderer:function(value,metadata,registro){                                
				return '<span style="color:red;">' + value + '</span>';
				}
			},
			{							
				header : 'RFC',
				tooltip: 'RFC',
				dataIndex : 'RFC',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				renderer:function(value,metadata,registro){                                
					return '<span style="color:red;">' + value + '</span>';
				}
			},
			{							
				header : 'Domicilio',
				tooltip: 'Domicilio',
				dataIndex : 'DOMICILIO',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				renderer:function(value,metadata,registro){                                
					return '<span style="color:red;">' + value + '</span>';
				}
			},
			{							
				header : 'Fecha de Solicitud',
				tooltip: 'Fecha de Solicitud',
				dataIndex : 'FECHA_SOLICITUD',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				renderer:function(value,metadata,registro){                                
					return '<span style="color:red;">' + value + '</span>';
				}
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				renderer:function(value,metadata,registro){                                
					return '<span style="color:red;">' + value + '</span>';
				}
			},
			{							
				header : 'Cuenta Bancaria',
				tooltip: 'Cuenta Bancaria',
				dataIndex : 'CUENTA_BANCARIA',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				renderer:function(value,metadata,registro){                                
					return '<span style="color:red;">' + value + '</span>';
				}
			},
			{							
				header : 'Banco de Servicio',
				tooltip: 'Banco de Servicio',
				dataIndex : 'BANCO_SERVICIO',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				renderer:function(value,metadata,registro){                                
					return '<span style="color:red;">' + value + '</span>';
				}
			},
			{							
				header : 'Sucursal',
				tooltip: 'Sucursal',
				dataIndex : 'SUCURSAL',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				renderer:function(value,metadata,registro){                                
					return '<span style="color:red;">' + value + '</span>';
				}
			},
			{							
				header : 'Plaza',
				tooltip: 'Plaza',
				dataIndex : 'PLAZA',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				renderer:function(value,metadata,registro){                                
					return '<span style="color:red;">' + value + '</span>';
				}
			},
			{							
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true,
				renderer:function(value,metadata,registro){                                
					return '<span style="color:red;">' + value + '</span>';
				}
			},	
			{
				xtype: 'checkcolumn',
				header : 'Seleccione',
				tooltip: 'Seleccione',
				dataIndex : 'SELECCIONAR',
				width : 150,
				align: 'center',
				sortable : false				 
			},						
			{
				xtype: 'actioncolumn',
				header: 'Expediente',
				tooltip: 'Expediente',
		      width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Buscar';
							return 'iconoLupa';										
						},	
						handler: descargaArchivoExpediente
					}
				]				
			},	
			{
				xtype: 'actioncolumn',
				header: 'Rechazo',
				tooltip: 'Rechazo',
				dataIndex : 'RECHAZO',
		      width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Rechazo';
							return 'borrar';										
						},	
						handler: procesoRechazo
					}
				]				
			}			
		],
		isplayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 900,
		style: 'margin:0 auto;',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',
				'-',				
				{
					xtype: 'button',
					id: 'btnAutorizar',
					text: 'Autorizar Solicitudes',
					handler: procesarGuardar					
				},
				'-',	
				{
					xtype: 'button',
					id: 'btnGenerarInf',
					text: 'Generar Informaci�n',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15ConsAfiliaElecIF.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoCSV'							
							})	,
							callback: procesarSuccessFailureArchivoConsulta
						});
					}					
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				}
				
			]
		}
	});
		
	// para la consulta
	var procesarConsultaDataC = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsultaC.isVisible()) {
				gridConsultaC.show();
			}	
		}		
		var el = gridConsultaC.getGridEl();	
			
		var jsonData = store.reader.jsonData;	
		var  pagina = jsonData.pagina;			
		var btnGenerarInf1 = Ext.getCmp('btnGenerarInf1');
		var btnBajarCSV1 = Ext.getCmp('btnBajarCSV1');
		var gridConsulta = Ext.getCmp('gridConsulta');
		btnBajarCSV1.hide();
		gridConsulta.hide();
				
		if(store.getTotalCount() > 0) {			
			btnGenerarInf1.enable();
			el.unmask();			
		} else {		
		
			btnGenerarInf1.disable();
			el.mask('No se encontr� ning�n registro', 'x-mask');				
		}	
	}
	
  	var consultaDataC = new Ext.data.JsonStore({
		root : 'registros',
		url : '15ConsAfiliaElecIF.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_EPO'},
			{name: 'RAZON_SOCIAL'},
			{name: 'RFC'},
			{name: 'DOMICILIO'},
			{name: 'FECHA_SOLICITUD'},
			{name: 'MONEDA'},
			{name: 'IC_CUENTA_BANCARIA'},
			{name: 'CUENTA_BANCARIA'},
			{name: 'BANCO_SERVICIO'},
			{name: 'SUCURSAL'},
			{name: 'PLAZA'},
			{name: 'ESTATUS'},
			{name: 'SELECCIONAR'},			
			{name: 'IC_EPO'},
			{name: 'IC_IF'},
			{name: 'IC_PYME'}				
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataC,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataC(null, null, null);						
				}
			}
		}
	});	
	
	
	var gruposConsultaC = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: 'Datos de la PyME', colspan: 5, align: 'center'},
				{	header: 'Datos de la Cuenta Bancaria', colspan: 5, align: 'center'},	
				{	header: ' ', colspan: 1, align: 'center'}
			]
		]
	});
	
  var gridConsultaC = new Ext.grid.EditorGridPanel({
		id: 'gridConsultaC',
		title: 'Solicitud de Afiliaci�n Electr�nica',
		hidden: true,
		clicksToEdit: 1,
		store: consultaDataC,
		plugins: gruposConsultaC,	
		columns: [
			{							
				header : 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex : 'NOMBRE_EPO',
				width : 150,				
				align: 'left',
				sortable : true,
				resizable: true				
			},				
			{							
				header : 'Nombre o Raz�n Social',
				tooltip: 'Nombre o Raz�n Social',
				dataIndex : 'RAZON_SOCIAL',
				width : 150,
				align: 'left',
				sortable : true,
				resizable: true				
			},
			{							
				header : 'RFC',
				tooltip: 'RFC',
				dataIndex : 'RFC',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true				
			},
			{							
				header : 'Domicilio',
				tooltip: 'Domicilio',
				dataIndex : 'DOMICILIO',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true				
			},
			{							
				header : 'Fecha de Solicitud',
				tooltip: 'Fecha de Solicitud',
				dataIndex : 'FECHA_SOLICITUD',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true				
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true				
			},
			{							
				header : 'Cuenta Bancaria',
				tooltip: 'Cuenta Bancaria',
				dataIndex : 'CUENTA_BANCARIA',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true				
			},
			{							
				header : 'Banco de Servicio',
				tooltip: 'Banco de Servicio',
				dataIndex : 'BANCO_SERVICIO',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true				
			},
			{							
				header : 'Sucursal',
				tooltip: 'Sucursal',
				dataIndex : 'SUCURSAL',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true				
			},
			{							
				header : 'Plaza',
				tooltip: 'Plaza',
				dataIndex : 'PLAZA',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true				
			},								
			{
				xtype: 'actioncolumn',
				header: 'Expediente',
				tooltip: 'Expediente',
		      width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Buscar';
							return 'iconoLupa';										
						},	
						handler: descargaArchivoExpediente1
					}
				]				
			}			
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 900,
		style: 'margin:0 auto;',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [				
				'->',				
				{
					xtype: 'button',
					id: 'btnGenerarInf1',
					text: 'Generar Informaci�n',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15ConsAfiliaElecIF.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoCSV'							
							})	,
							callback: procesarSuccessFailureArchivoConsulta1
						});
					}					
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					tooltip:	'Descargar Archivo',
					id: 'btnBajarCSV1',
					hidden: true
				}
				
			]
		}
	});
	
	
	
	
	
	
	var storeCatalogoEPO = new Ext.data.JsonStore({
		id: 'storeCatalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15ConsAfiliaElecIF.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
		var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '15ConsAfiliaElecIF.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},		
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbPyme1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);		
							var ic_epo = Ext.getCmp("ic_epo1").getValue();	
							
							storeBusqAvanzPyme.load({
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
										ic_epo:ic_epo						
									})																	
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});
	
	var successAjaxFn = function(opts, success, response) { 

	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);	
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
}

	var elementosForma = [	
		{  
			xtype:  'radiogroup',  			 
			name: 'pagina',   
			id: 'pagina3',  
			value: 'A',  	
			width: 50,
			columns: 2,			
			items: [         
				{ 
					boxLabel:    "Autorizaci�n",             
					name:        'pagina', 
					id: 'pagina1',
					inputValue:  "A" ,
					value: 'A',
					checked: true,
					width: 20
				},         
				{           
					boxLabel: "Consulta",             
					name: 'pagina', 
					id: 'pagina2',
					inputValue:  "C", 
					value: 'N',
					width: 20	
				}   
			],   
			style: {      
				paddingLeft: '10px'   
			}		
		},
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			emptyText: 'Seleccionar...',
			valueField: 'clave',
			hiddenName : 'ic_epo',
			fieldLabel: 'Nombre de la EPO:',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeCatalogoEPO
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero Nafin Electr�nico',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'N�mero Nafin Electr�nico',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){
						 // Petici�n b�sica  
							Ext.Ajax.request({  
								url: '15ConsAfiliaElecIF.data.jsp',  
								method: 'POST',  
								callback: successAjaxFn,  
								params: {  
									 informacion: 'pymeNombre' ,
									 num_electronico: Ext.getCmp('num_electronico1').getValue()
								}  
						}); 				}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 300,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
				xtype: 'button',
				text: 'B�squeda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
					}
				}
			}
			]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de captura desde',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'txt_fecha_sol_de',
					id: 'txt_fecha_sol_de',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txt_fecha_sol_a',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txt_fecha_sol_a',
					id: 'txt_fecha_sol_a',
					allowBlank: true,
					startDay: 1,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txt_fecha_sol_de',
					margins: '0 20 0 0'  
				}
			]
		}
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 800,
		title: 'Solicitud de Afiliaci�n Electr�nica',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
		{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton, evento) {	
					
														
					var txt_fecha_sol_de =  Ext.getCmp("txt_fecha_sol_de");
					var txt_fecha_sol_a =  Ext.getCmp("txt_fecha_sol_a");
										
					if ( ( !Ext.isEmpty(txt_fecha_sol_de.getValue())  &&   Ext.isEmpty(txt_fecha_sol_a.getValue()) )
						||  ( Ext.isEmpty(txt_fecha_sol_de.getValue())  &&   !Ext.isEmpty(txt_fecha_sol_a.getValue()) ) ){
						txt_fecha_sol_de.markInvalid('Debe capturar ambas Fechas o dejarlas en blanco');
						txt_fecha_sol_a.markInvalid('Debe capturar ambas Fechas o dejarlas en blanco');
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');	
					var pagina1 = Ext.getCmp("pagina1").getValue();
					var pagina2 = Ext.getCmp("pagina2").getValue();					
					
					if(pagina1==true)	{	
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar'							
							})
						});
					}
					
					if(pagina2==true)	{	
						consultaDataC.load({
							params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar'							
							})
						});
					}
					
				}
			},					
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15ConsAfiliaElecIFExt.jsp';
				}
			}
		]
	});
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			fp,
			mensajePreAcuse,
			mensajeAcuse2,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridConsultaC,
			gridPreAcuse,
			NE.util.getEspaciador(20)
		]
	});
	
	storeCatalogoEPO.load();
	
});