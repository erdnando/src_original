<%@ page contentType="application/json;charset=UTF-8" import="		
		org.apache.commons.logging.Log,
		javax.naming.*,
		java.util.*,
		java.sql.*,
		java.text.*, 	
		java.net.*,
		java.io.*,		
		netropology.utilerias.*,	
		com.netro.afiliacion.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,		
		com.netro.exception.*,
		com.netro.model.catalogos.*,
		com.netro.seguridadbean.SeguException,
		com.netro.exception.NafinException,	
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%@ include file="../certificado.jspf"%>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
String informacion    = request.getParameter("informacion")   == null?"": (String)request.getParameter("informacion");
String pymesFloating  = request.getParameter("pymesFloating") == null?"": (String)request.getParameter("pymesFloating");
String regValidos     = request.getParameter("regValidos")    == null?"": (String)request.getParameter("regValidos");
String totalReg       =  request.getParameter("totalReg")     == null?"": (String)request.getParameter("totalReg"); 
String pymesNOFloating  = request.getParameter("pymesNOFloating") == null?"": (String)request.getParameter("pymesNOFloating");


String infoRegresar ="";
String usuario =iNoUsuario + " - " +strNombreUsuario;
JSONObject 	jsonObj	= new JSONObject();

CargaPymeFloating  carga = new  CargaPymeFloating();

if (informacion.equals("GenerarAcuse")) {
	
	String folioCert = ""; 
	String _acuse = "";
	String mensajeAutentificacion ="";	
	String pkcs7 = request.getParameter("pkcs7");	
	String externContent = request.getParameter("textoFirmar");
	char getReceipt = 'Y';
	String fechaHora  = (new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss")).format(new java.util.Date());
	Seguridad s = new Seguridad();
	Acuse acuse = null;	
	String exitoso ="S";
		
	carga = new  CargaPymeFloating();

	 log.debug("_serial "+_serial+" \n externContent=== "+ externContent+" \n pkcs7 "+ pkcs7); 	 
	
	if (!"".equals(_serial) && !"".equals(externContent) && !"".equals(pkcs7)){
		acuse = new Acuse(Acuse.ACUSE_IF,"1");
		folioCert = acuse.toString();		
		
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			_acuse = s.getAcuse();			
			
			Map<String, Object> datos = new HashMap<>();
			datos.put("pymesFloating", pymesFloating);
			datos.put("acuse", acuse.toString());
			datos.put("recibo", _acuse);
			datos.put("usuario", usuario);
			datos.put("fechaHora", fechaHora);
			datos.put("regValidos", regValidos);
			datos.put("totalReg", totalReg);
			datos.put("pymesNOFloating", pymesNOFloating);
			
			try {
				
				carga.paramPymeFloating(datos );		
				
				mensajeAutentificacion = "<b>La carga se llevó a cabo con éxito. Recibo Número: "+_acuse;
				
			} catch (NafinException ne){
			  log.error("Error en la carga  "+ ne.getMsgError());
			  mensajeAutentificacion = "<b>La carga no se llevó  a cabo </b>"+s.mostrarError();
			  exitoso ="N";
			}	
					   
		}else { //autenticación fallida
			mensajeAutentificacion = "<b>La carga no se llevó  a cabo </b>"+s.mostrarError();
			exitoso ="N";
		}
	} else { //autenticación fallida
		mensajeAutentificacion = "<b>La carga no se llevó a cabo </b>";
		exitoso ="N";
	}
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensajeAutentificacion",mensajeAutentificacion);
	jsonObj.put("recibo",_acuse);
	jsonObj.put("acuse",acuse.toString());
	jsonObj.put("fechaHora",fechaHora);
	jsonObj.put("usuario",usuario);
	jsonObj.put("regValidos",regValidos);	
	jsonObj.put("totalReg",totalReg);
	jsonObj.put("exitoso",exitoso);
	
	infoRegresar  = jsonObj.toString();

}else  if (informacion.equals("Imprimir")) {

	String [] datosPdf	  = request.getParameterValues("datosPdf"); 
	String [] datosNoSucpPdf = request.getParameterValues("datosNoSucpPdf"); 
	
	String recibo       = request.getParameter("recibo")   == null?"": (String)request.getParameter("recibo");
  	String acuse        = request.getParameter("acuse")   == null?"": (String)request.getParameter("acuse");
  	String fechaHora    = request.getParameter("fechaHora")   == null?"": (String)request.getParameter("fechaHora");
  
  
	Map<String, Object> infoDatos = new HashMap<>();		
	infoDatos.put("recibo",recibo);
	infoDatos.put("acuse",acuse);
	infoDatos.put("fechaHora",fechaHora);
	infoDatos.put("usuario",usuario);
	infoDatos.put("regValidos",regValidos);	
	infoDatos.put("totalReg",totalReg);
				  
	jsonObj = new JSONObject();
	String nombreArchivo = carga.imprimirDatos( request,  strDirectorioTemp,  datosPdf, infoDatos, datosNoSucpPdf  );
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
	infoRegresar = jsonObj.toString();
}
	
%>
<%=infoRegresar%>