Ext.onReady(function(){

	var regValidos = "";	
	var totalReg = "";	
	var pymesFloating ="";
   var acuse ="";
	var recibo ="";
	var fechaHora ="";
	var pymesNOFloating = "";
	

	function procesarDescargaArchivos(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton = Ext.getCmp('btnImprimir');
			boton.enable();
			var boton2 = Ext.getCmp('btnImprimir');
			boton2.enable();		
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function procesarAcuse(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp("autentificac").setValue(jsonData.mensajeAutentificacion);
			
			if(jsonData.exitoso ==='S'){
			
				var acuseCifras = [
					['N�mero de Acuse', jsonData.acuse],
					['Fecha y Hora ', jsonData.fechaHora],
					['Usuario', jsonData.usuario],
					['Registros Validos ', jsonData.regValidos],				
					['Total de Registros ', jsonData.totalReg]
				];
				
				acuse = jsonData.acuse;
				recibo =jsonData.recibo;
				fechaHora =jsonData.fechaHora;
				regValidos = jsonData.regValidos;				
				totalReg = jsonData.totalReg;	
			
				storeCifrasData.loadData(acuseCifras);		
				
				Ext.getCmp('gridCifrasControl').setTitle("Acuse ");
				Ext.getCmp('gridCifrasControl').show();	
				Ext.getCmp('gridCargaValida').show();	
				Ext.getCmp('btnImprimir').show();	
				
			}else  if(jsonData.exitoso ==='N'){
				Ext.getCmp('gridNOSucepFloating').hide();
				
			}
			
				Ext.getCmp('mensajeAutentificacion').show();	
				Ext.getCmp('contenedorGrids').hide();	
				Ext.getCmp('forma').hide();	
				
				Ext.getCmp('btnSalir').show();	
				Ext.getCmp('btnRegresar').hide();
				Ext.getCmp('btnContinuarC').hide();	
			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

	var confirmar = function(pkcs7, vtextoFirmar, vregValidos, vtotalReg, vpymesFloating, vpymesNOFloating){
	
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else {
			Ext.Ajax.request({
				url: '15CargaProveedoresFloating.data.jsp',
				params: {
					pkcs7: pkcs7,
					textoFirmar:vtextoFirmar,
					informacion:'GenerarAcuse',
					regValidos :vregValidos,
					totalReg :vtotalReg,								
					pymesFloating:vpymesFloating,
					pymesNOFloating:vpymesNOFloating							}					
				,callback: procesarAcuse
			});	
		}
	}
		
	var storeCifrasData = new Ext.data.ArrayStore({
		fields: [
			{	name: 'etiqueta'},
			{	name: 'informacion'}
		]
	});
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Recibo N�mero',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',  dataIndex : 'etiqueta', 	width : 200, 	sortable : true 	},
			{
				header : 'Informacion',	 dataIndex : 'informacion', width : 300, 	sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 520,
		height: 180,
		style: 'margin:0 auto;',		
		frame: true
	});
	
	
	var mensajeAutentificacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutentificacion',							
		width:	350,
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		columns: 1,
		items: [					
			{
				xtype: 'displayfield',
				id:	'autentificac',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}		
			}			
		]
	});

	 //Registros de Pymes que dejaran de ser Suceptibles a Floating 
	var registrosNoFloatingData = new Ext.data.JsonStore({
		root : 'registrosNoFloating',
		fields : ['IC_PYME', 'NOMBRE_PYME', 'RFC' , 'NAFIN_ELECTRONICO', 'PARAMETRIZACION'],
		autoDestroy : true,
		autoLoad: false
	});	
	
	var gridNOSucepFloating = {
		xtype: 'grid',
		id: 'gridNOSucepFloating',	
		store: registrosNoFloatingData,
		margins: '10 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		columns: [			
			{
				header: 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex: 'NOMBRE_PYME',
				sortable: false,
				width: 350,
				resizable: true,
				hidden: false
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: false,				
				width: 150,
				align: 'left'
			},
			{
				header: 'Nafin Electr�nico',
				tooltip: 'Nafin Electr�nico',
				dataIndex: 'NAFIN_ELECTRONICO',
				sortable: false,				
				width: 150,
				align: 'center'
			},
			{
				header: 'Parametrizaci�n',
				tooltip: 'Parametrizaci�n',
				dataIndex: 'PARAMETRIZACION',
				sortable: false,				
				width: 150,
				align: 'center'
			}
		],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		autoHeight: true,		
		width: 800,
		title: 'Proveedores que dejar�an de ser Susceptibles a Floating',
		frame: true,
		bbar: ''
	};

	// registros sin errores 
	var registrosCargaValidosData = new Ext.data.JsonStore({
		root : 'registrosCargaValidos',
		fields : ['IC_PYME', 'NOMBRE_PYME', 'RFC' , 'NAFIN_ELECTRONICO', 'PARAMETRIZACION'],
		autoDestroy : true,
		autoLoad: false
	});	
	
	var gridCargaValida = {
		xtype: 'grid',
		id: 'gridCargaValida',	
		store: registrosCargaValidosData,
		margins: '10 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		columns: [			
			{
				header: 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex: 'NOMBRE_PYME',
				sortable: false,
				width: 350,
				resizable: true,
				hidden: false
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: false,				
				width: 150,
				align: 'left'
			},
			{
				header: 'Nafin Electr�nico',
				tooltip: 'Nafin Electr�nico',
				dataIndex: 'NAFIN_ELECTRONICO',
				sortable: false,				
				width: 150,
				align: 'center'
			},
			{
				header: 'Parametrizaci�n',
				tooltip: 'Parametrizaci�n',
				dataIndex: 'PARAMETRIZACION',
				sortable: false,				
				width: 100,
				align: 'center'
			}
		],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		autoHeight: true,		
		width: 800,
		title: 'Carga de Proveedores Susceptible a Floating',
		frame: true,
		bbar: ''
	};
	
	
	var registrosInvalidosData = new Ext.data.JsonStore({
		root : 'registrosInvalidos',
		fields : ['LINEA', 'CAMPO', 'OBSERVACION'],
		autoDestroy : true,
		autoLoad: false
	});


	var gridInvalidos = {
		xtype: 'grid',
		id: 'gridInvalidos',
		store: registrosInvalidosData,
		margins: '0 0 0 0',
		stripeRows: true,
		loadMask:   true,
		frame:      true,		
		header:     true,
		columns: [
			{
				header: 'L�nea',
				tooltip: 'L�nea',
				dataIndex: 'LINEA',
				
				width: 50,				
				align: 'center'
			},
			{
				header: 'Campo',
				tooltip: 'Nombre del Campo',
				dataIndex: 'CAMPO',				
				width: 125,
				align: 'left'
			},
			{
				
				header : 'Observaci�n',
				tooltip: 'Observaciones',
				dataIndex : 'OBSERVACION',
				width: 350,
				align: 'left',		
				resizable: true,
				renderer:  function (observacion, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(observacion) + '"';
						return observacion;
				}
			}
		],	
		width: 450,		
		title: 'Detalle con Errores',		
		bbar: {			
			displayInfo: true,				
			height: 30,
			items: [	
			'Total Registros Inv�lidos: ',
				{	xtype: 'displayfield', 	text: '-', 	id: 'totalRegistrosInvalidos'	}
			]
		}
	};

	var registrosValidosData = new Ext.data.JsonStore({
		root : 'registrosValidos',
		fields : ['LINEA', 'CAMPO', 'OBSERVACION'],
		autoDestroy : true,
		autoLoad: false
	});
	
	
	var gridValidos = {
		xtype: 'grid',
		id: 'gridValidos',	
		store: registrosValidosData,
		margins: '0 5 0 0',
		stripeRows: true,
		loadMask:   true,
	//	frame:      true,		
//		header:     true,
		columns: [
			{
				header: 'L�nea',
				tooltip: 'L�nea',
				dataIndex: 'LINEA',
				sortable: false,
				width: 50,				
				align: 'center'
			},
			{
				header: 'Dato',
				tooltip: 'Nombre del Campo',
				dataIndex: 'CAMPO',				
				width: 125,
				align: 'left'
			},
			{			
				header : 'Observaci�n',
				tooltip: 'Observaciones',
				dataIndex : 'OBSERVACION',
		      sortable : false,
				hideable : false,
				width: 130,
				align: 'left'				
			}
		],		
		width: 330,		
		title: 'Detalle sin Error',		
		bbar: {			
			displayInfo: true,	
			height: 30,
			items: [		
			'Total Registros Validos: ',
				{	xtype: 'displayfield', 	text: '-', 	id: 'totalRegistrosValidos'	}
			]			
		}
	};
		
	var contenedorGrids = {
		xtype: 'panel',
		id: 'contenedorGrids',
		hidden: true,
		title: ' ',		
		width: 800,	
		frame: true,
		margins: '20 0 0 0',
		layout: 'hbox',
		layoutConfig: {
			pack: 'center',
			align: 'center'			
		},
		
		style: 'margin:0 auto;',
		items: [
			gridValidos,
			gridInvalidos		
		]
	};
		
	var fpBotones = new Ext.Container({
		layout: 'table',
		align: 'center',
		style: 'margin:0 auto;',
		id: 'fpBotones',	
		hidden: true,
	   width: '100',
		heigth:'auto',		
		items: [
			{
				xtype: 'button',
				text: 'Regresar',
				tooltip:	'Regresar',
				iconCls: 'icoLimpiar',
				id: 'btnRegresar',
				handler: function(boton, evento) {
					window.location = '15CargaProveedoresFloating.jsp';		
				}
			},	
			{
				xtype: 'button',
				text: 'Imprimir',
				tooltip:	'Salir',
				iconCls: 'icoPdf',
				id: 'btnImprimir',
				hidden: true,
				handler: function(boton, evento) {	
				
				  var gridpdf = Ext.getCmp('gridCargaValida');
				  var store = gridpdf.getStore();	
				  var datosPdf =[];
				  store.each(function(record) {
						datosPdf.push(record.data['NOMBRE_PYME']+'|'+record.data['RFC']+'|'+record.data['NAFIN_ELECTRONICO']+'|'+record.data['PARAMETRIZACION']);
					});
					
					
				  var gridNOpdf = Ext.getCmp('gridNOSucepFloating');
				  var storeN = gridNOpdf.getStore();	
				  var datosNoSucpPdf =[];
				  storeN.each(function(record) {
						datosNoSucpPdf.push(record.data['NOMBRE_PYME']+'|'+record.data['RFC']+'|'+record.data['NAFIN_ELECTRONICO']+'|'+record.data['PARAMETRIZACION']);
					});
									 
					Ext.Ajax.request({
						url: '15CargaProveedoresFloating.data.jsp',
						params: {								
							informacion:'Imprimir',
							datosPdf:datosPdf,
							datosNoSucpPdf: datosNoSucpPdf,
							acuse:acuse,
							recibo:recibo,
							fechaHora:fechaHora,
							regValidos :regValidos,							
							totalReg:totalReg
						}					
						,callback: procesarDescargaArchivos
					});						
				}
			},
			{ 	xtype: 'displayfield', 	value: ' ', width: 10	},
			{
				xtype: 'button',
				text: 'Continuar',					
				tooltip:	'Continuar',
				iconCls: 'icoAceptar',
				id: 'btnContinuarC',
				handler: function(boton, evento) {
				
					var textoFirmar = 	"L�nea | Campo | Observaci�n\n";
					
					var  gridValidos = Ext.getCmp('gridValidos');
					var store = gridValidos.getStore();
					store.each(function(record) {	
						textoFirmar += 	record.data['LINEA']+"|"+ record.data['CAMPO']+"|"+ record.data['OBSERVACION'] +"\n";
						
					});					
					NE.util.obtenerPKCS7(confirmar, textoFirmar, regValidos, totalReg, pymesFloating, pymesNOFloating);
					
					
				}
			},				
			{
				xtype: 'button',
				text: 'Salir',
				tooltip:	'Salir',
				iconCls: 'icoLimpiar',
				hidden: true,
				id: 'btnSalir',
				handler: function(boton, evento) {
					window.location = '15CargaProveedoresFloating.jsp';		
				}
			}				
		]
	});
	
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMCAMPO'},
			{name: 'CAMPO'},
			{name: 'TIPODATO'},
			{name: 'LONGITUD'},
			{name: 'OBLIGATORIO'}			
		]
	});
	
	var infoLayout = [
		['1','R.F.C. ','Alfanum�rico','15', 'Si'],
		['2','Nafin Electr�nico','Num�rico','10','Si']
	];
	
	storeLayoutData.loadData(infoLayout);	
	 
	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title: 'Descripci�n de campos del archivo .txt',
		columns: [
			{
				header : 'NO.', dataIndex : 'NUMCAMPO', width : 100,	sortable : true,	align: 'center'},
			{
				header : 'Campo',				
				dataIndex : 'CAMPO',
				width : 120,
				align: 'left',
				sortable : true
			},
			{
				header : 'Tipo Dato',
				dataIndex : 'TIPODATO',
				width : 120,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Long. Max',
				dataIndex : 'LONGITUD',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 100,
				sortable : true,
				align: 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 110,
		frame: true
	});

	var myValidFn = function(v) {
		var myRegex = /^.+\.([tT][xX][tT])$/;
		return myRegex.test(v);
	};
	
	Ext.apply(Ext.form.VTypes, {
		archivotxt 		: myValidFn,
		archivotxtText 	: 'El formato del archivo de origen no es el correcto. Debe de tener extensi�n .txt'
	});

		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Carga Proveedores Susceptible a Floating',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},		
		items: [
			{
				xtype:	'panel',
				layout:	'column',
				width: 700,
				anchor: '100%',
				id:'cargaArchivo1',
				defaults: {
					bodyStyle:'padding:4px'
				},
				items:	[
					{
						xtype: 'button',
						id: 'btnAyuda',
						columnWidth: .05,
						autoWidth: true,
						autoHeight: true,
						iconCls: 'icoAyuda',
						handler: function(){						
							if (!gridLayout.isVisible()) {
								gridLayout.show();
							}else{
								gridLayout.hide();
							}									
						}
					},				
					{
						xtype: 'panel',
						id:		'pnlArchivo',
						columnWidth: .95,
						anchor: '100%',
						layout: 'form',
						fileUpload: true,
						labelWidth: 175,
						defaults: {
							bodyStyle:'padding:5px',
							msgTarget: 'side',
							anchor: '-20'
						},
						items: [
							{
								xtype: 'compositefield',
								fieldLabel: '',
								id: 'cargaArchivo',
								combineErrors: false,
								msgTarget: 'side',
								items: [
									{
										xtype: 'fileuploadfield',
										id: 'archivo',
										emptyText: 'Nombre del Archivo',
										fieldLabel: 'Nombre del Archivo',
										name: 'archivoFormalizacion',   
										buttonText: ' ',
										width: 300,	  
										buttonCfg: {
											iconCls: 'upload-icon'
										},
										anchor: '100%',
										vtype: 'archivotxt'
									}						
								]
							}
						]
					}
				]
			}			
		],			
		monitorValid: true,
		buttons: [
			{
				text: 'Procesar',
				id: 'btnContinuar',
				iconCls: 'icoAceptar',
				formBind: true,		
				handler: function(boton, evento) {
				
					var archivo = Ext.getCmp("archivo");										
					if (Ext.isEmpty(archivo.getValue()) ){
						archivo.markInvalid('Favor de capturar una ruta valida');
						return;
					}		
					
					var forma = fp.getForm();									
					forma.submit({
						url: '15CargaProveedoresFloatingFile.data.jsp',
						waitMsg: 'Enviando datos...',
						waitTitle :'Por favor, espere',		
						success: function(form, action) {
						
							registrosValidosData.loadData(action.result);
							registrosInvalidosData.loadData(action.result);
							registrosCargaValidosData.loadData(action.result);		
							registrosNoFloatingData.loadData(action.result);																		
							var valoresResultadoProceso = action.result.valoresResultadoProceso;							
														
							Ext.getCmp('contenedorGrids').show();
							Ext.getCmp('gridNOSucepFloating').show();
							Ext.getCmp('gridLayout').hide();		
							Ext.getCmp('fpBotones').show();							
						
							Ext.getCmp('gridValidos').show();
							Ext.getCmp('gridInvalidos').show();							
							
							Ext.getCmp('totalRegistrosInvalidos').update(valoresResultadoProceso.totalRegistrosInvalidos.toString());								
							Ext.getCmp('totalRegistrosValidos').update(valoresResultadoProceso.totalRegistrosValidos.toString());								
						
							
						
							var elV = Ext.getCmp('gridValidos').getGridEl();	
							if(registrosValidosData.getTotalCount()===0){
								elV.mask('No hay registros', 'x-mask');		
							}else  {
								elV.unmask();	
							}
							
							var elI = Ext.getCmp('gridInvalidos').getGridEl();	
							if(registrosInvalidosData.getTotalCount()===0){
								elI.mask('No hay registros', 'x-mask');		
							}else  {
								elI.unmask();	
							}
														
							var el = Ext.getCmp('gridNOSucepFloating').getGridEl();	
							if(registrosNoFloatingData.getTotalCount()===0){
								el.mask('No hay registros', 'x-mask');		
							}else  {
								el.unmask();	
							}
							
							if(valoresResultadoProceso.btnContinuar ==='N') {
								Ext.getCmp('btnContinuarC').hide();
							}else {
								Ext.getCmp('btnContinuarC').show();
							}
							
							//datos para generar el acuse 
							regValidos  =valoresResultadoProceso.regValidos.toString();								
							totalReg  =valoresResultadoProceso.totalReg.toString();									
							pymesFloating   =valoresResultadoProceso.pymesFloating.toString();	
							pymesNOFloating   =valoresResultadoProceso.pymesNOFloating.toString();	
								
						}
					});
				}
			}
		]
	});
			

	new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
		   NE.util.getEspaciador(20),
			fp,
			mensajeAutentificacion,
			NE.util.getEspaciador(20),
			gridCifrasControl,
			gridLayout,
			NE.util.getEspaciador(20),
			gridCargaValida,
			contenedorGrids,
			NE.util.getEspaciador(20),
			gridNOSucepFloating,
			NE.util.getEspaciador(20),
			fpBotones
		]
	});

});

