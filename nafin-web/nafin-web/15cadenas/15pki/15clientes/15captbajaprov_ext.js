
Ext.onReady(function() {
var numProcesoGbl = '';
var nombrePDF = '';
var nombreZIP = '';
//HANDLERS------------------------------------------------------------------------------
var procesarSuccessDatCboIf = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
      fp.getForm().getEl().dom.action =  resp.urlArchivo;
      fp.getForm().getEl().dom.submit();
			fp.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

var procesarSuccessValidaCaracteresEspeciales = function(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
      
		if(resp.statusThread != '300' && resp.statusThread!== '400'){
			Ext.Ajax.request({
				url: '15captbajaprov_ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
				  informacion: 'ValidaEstatusHiloCaracteresEspeciales',
				  numProceso: resp.numProceso
				}),
				callback: procesarSuccessValidaCaracteresEspeciales
				});
		}else if(resp.statusThread == '300' || resp.statusThread == '400'){
			if(resp.hayCaractCtrl){
				fp.hide();
				var caracteresEspeciales = new NE.cespcial.CaracteresEspeciales({mensaje:resp.mensaje});
				
				caracteresEspeciales.setHandlerAceptar(function(){
					window.location = '15captbajaprov_ext.jsp'
				});
				
				caracteresEspeciales.setHandlerDescargarArchivo(function(){
					
					var archivo = resp.urlArchivo
					archivo = archivo.replace('/nafin','');
					var params = {nombreArchivo: archivo};
					
					fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					fp.getForm().getEl().dom.submit();
				});
				
				pnl.add(caracteresEspeciales);
				pnl.doLayout();
				
			}else{
				Ext.Ajax.request({
				url: '15captbajaprov_ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
				  informacion: 'ValidaCargaArchivo',
				  numProceso: resp.numProceso
				}),
				callback: procesarSuccessValidaCarga
				});
			}
		}
		
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

var procesarSuccessValidaCarga = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			fp.hide();
      Ext.getCmp('tbPanelResult').show();
      storeRegCorrectData.loadData(resp);
      storeRegErrorData.loadData(resp);
      
      if(storeRegCorrectData.getTotalCount()<=0){
        Ext.getCmp('btnContinuarValid').disable();
      }else{
        Ext.getCmp('btnContinuarValid').enable();
      }
      
      if(storeRegErrorData.getTotalCount()<=0){
        Ext.getCmp('btnDescargaError').disable();
      }else{
        Ext.getCmp('btnDescargaError').enable();
      }
     
      
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

var procesarSuccessDescargaErrores = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var archivo = resp.urlArchivo
      archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
      
      fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
      fp.getForm().getEl().dom.submit();
      
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}


var procesarSuccessDescargaPdf = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var archivo = resp.urlArchivo
      archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
      
      fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
      fp.getForm().getEl().dom.submit();
      
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

var procesarConsultaValidaCorrect = function(store, arrRegistros, opts) 	{
		var el = gridRegCorrectos.getGridEl();
    var totRegCorrect = Ext.getCmp('totRegCorrect1');
		if(store.getTotalCount() > 0) {
			el.unmask();	
      totRegCorrect.setValue(store.getTotalCount());
		} else {		
      totRegCorrect.setValue(0);
      //Ext.getCmp('btnGuardar').disable();	
			//el.mask('No hay registros sin error', 'x-mask');				
		}	
	}
  
var procesarConsultaValidaError = function(store, arrRegistros, opts) 	{
		var grid = Ext.getCmp('gridRegErroneos1');
    var totRegError = Ext.getCmp('totRegError1');
    //var el = Ext.getCmp('gridRegErroneos1').getGridEl();
    grid.show();
		if (arrRegistros != null) {
			if (!gridRegErroneos.isVisible()) {
				//gridConsulta.show();
			}	
		}					
		
		if(store.getTotalCount() > 0) {
			//grid.el.unmask();
      totRegError.setValue(store.getTotalCount());
		} else {
      totRegError.setValue(0);
      //Ext.getCmp('btnGuardar').disable();	
			//grid.el.mask('No hay registros con error', 'x-mask');				
		}	
	}
  
var procesarSuccessBajaProveedores = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			//alert('delta');
      panelPreAcuse.hide();
      panelAcuse.show();
      panelAcuse.setTitle('<center>Su solicitud fue recibida con el n�mero de folio: '+resp.folioAcuse+'<br>Cifras de Control</center>');
      storeAcuseData.loadData([['No. Total de registros Eliminados', Ext.getCmp('totRegCorrect1').getValue()],
                               ['No. Total de Registros que no se Eliminaron',Ext.getCmp('totRegError1').getValue()],
                               ['Fecha de Carga', resp.fechaHoy],
                               ['Hora de Carga', resp.horaCarga],
                               ['Usuario', resp.captUser]
                            ]);
      
     
      
      Ext.getCmp('btnImprimirPdfAcuse').setHandler(function(){
        var archivo = resp.urlArchivo
        var archivo = archivo.replace('/nafin','');
        var params = {nombreArchivo: archivo};
        
        fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
        fp.getForm().getEl().dom.submit();
      });
      
      
      
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//FUNCTIONS-------------------------------------------------------------------------------
var fnContinuarCapt = function(boton, evento){
  //fp.el.mask('Descargando Archivo...', 'x-mask-loading');
  var cboEpo = Ext.getCmp('cboEpo1');
  if (cboEpo.getValue()=='' || !cboEpo.isValid()){
    cboEpo.markInvalid('El campo EPO es requerido');
    cboEpo.focus();
    return;
  }
  
  var cargaArchivoPDF = Ext.getCmp('archivoPDF');
  if (cargaArchivoPDF.getValue()=='' || !cargaArchivoPDF.isValid()){
    cargaArchivoPDF.markInvalid('Selecciona un archivo PDF');
    cargaArchivoPDF.focus();
    return;
  }

  var ifile = Ext.util.Format.lowercase(cargaArchivoPDF.getValue());
  var extArchivoPDF = Ext.getCmp('hidExtensionPDF');
  /*var numTotal = Ext.getCmp('hidNumTotalPDF');
  var totalMonto = Ext.getCmp('hidTotalMontoPDF');*/
  var objMessage = Ext.getCmp('pnlMsgValid1');
  
  objMessage.hide();
  
  if (/^.*\.(pdf)$/.test(ifile)){
    extArchivoPDF.setValue('pdf');
  }
  
  //
  var cargaArchivoZIP = Ext.getCmp('archivoZIP');
  if (cargaArchivoZIP.getValue()=='' || !cargaArchivoZIP.isValid()){
    cargaArchivoZIP.markInvalid('Selecciona un archivo PDF');
    cargaArchivoZIP.focus();
    return;
  }

  var ifile = Ext.util.Format.lowercase(cargaArchivoZIP.getValue());
  var extArchivoZIP = Ext.getCmp('hidExtensionZIP');
  
  if (/^.*\.(zip)$/.test(ifile)){
    extArchivoZIP.setValue('zip');
  }
  
  

    fp.getForm().submit({
      url: '15captbajaprovfile_ext.data.jsp',
      waitMsg: 'Enviando datos...',
      success: function(form, action) {
        var resp = action.result;
        var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
        var objMsg = Ext.getCmp('pnlMsgValid1');

		if(!resp.codificacionValida){
			new NE.cespcial.AvisoCodificacionArchivo({codificacion:resp.codificacionArchivo}).show();
        }else if(!resp.flagPdf){
          Ext.getCmp('archivoPDF').markInvalid(resp.msgErrorPdf);
        }else if(!resp.flag){
          //SE MOSTRARAN LEYENDAS DE VALIDACIONES
          Ext.getCmp('archivoZIP').markInvalid(resp.msgError);
        }else{
				numProcesoGbl = resp.numProceso;
				nombrePDF = resp.nombrePDF;
				nombreZIP = resp.nombreZIP;
				gridLayout.hide();
				Ext.Ajax.request({
				url: '15captbajaprov_ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
				  informacion: 'ValidaCaracteresEspeciales',
				  numProceso: resp.numProceso,
				  rutaTxt: resp.nombreZIP
				}),
				callback: procesarSuccessValidaCaracteresEspeciales
				});
            
        }

      },
      failure: NE.util.mostrarSubmitError
    });
}

var fnDescargaErroresCapt = function(btn){
  var cboEpo = Ext.getCmp('cboEpo1');
  var record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());		
	var nombreEpo = record.get(cboEpo.displayField);
  
  Ext.Ajax.request({
    url: '15captbajaprov_ext.data.jsp',
    params: Ext.apply(fp.getForm().getValues(),{
      informacion: 'DescargaArchivoErrores',
      numProceso: numProcesoGbl,
      nombreEpo: nombreEpo
    }),
    callback: procesarSuccessDescargaErrores
  });
}

var fnDescargaAcusePdf = function(btn){
  Ext.Ajax.request({
    url: '15captbajaprov_ext.data.jsp',
    params: Ext.apply(fp.getForm().getValues(),{
      informacion: 'imprimirPDF',
      numProceso: numProcesoGbl
    }),
    callback: procesarSuccessDescargaPdf
  });
}

//STORES----------------------------------------------------------------------------------
var storeCatEpoData = new Ext.data.JsonStore({
		id: 'catalogoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15captbajaprov_ext.data.jsp',
		baseParams: {
			informacion: 'getCatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, option){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
          
					}
			}
		}
	});

var storeLayoutData = new Ext.data.ArrayStore({
	  fields: [
		  {name: 'NUMCAMPO'},
		  {name: 'DESC'},
		  {name: 'TIPODATO'}
	  ],
    data:[
            ['1','N�mero de Proveedor','Alfanum�rico'],
            ['2','Nombre del Proveedor','Alfanum�rico'],
            ['3','RFC del Proveedor','Alfanum�rico'],
            ['4','IC_PYME','Num�rico']
        ]
 });
 
var storePreAcuseData = new Ext.data.ArrayStore({
	  fields: [
		  {name: 'NOMBRE'},
		  {name: 'VALOR'}
	  ]
 });
 
var storeAcuseData = new Ext.data.ArrayStore({
	  fields: [
		  {name: 'NOMBRE'},
		  {name: 'VALOR'}
	  ]
 });

var storeRegCorrectData = new Ext.data.JsonStore({
		root : 'registrosCorrect',
		//url : '04detreqmensual_01.data.jsp',
		//baseParams: {
			//informacion: 'infoContratosSinBO'
		//},
		fields: [
			{name: 'LINEA'},
			{name: 'DESCRIPCION'}
		],	
		autoLoad: false,
		listeners: {
			load: procesarConsultaValidaCorrect,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaValidaCorrect(null, null, null);
				}
			}
		}
		
	});

var storeRegErrorData = new Ext.data.JsonStore({
		root : 'registrosError',
		//url : '04detreqmensual_01.data.jsp',
		//baseParams: {
		//	informacion: 'infoContratosSinBO'
		//},
		fields: [
			{name: 'LINEA'},
			{name: 'DESCRIPCION'}
		],	
		autoLoad: false,
		listeners: {
			load: procesarConsultaValidaError,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaValidaError(null, null, null);
				}
			}
		}
		
	});
//OBJETOS---------------------------------------------------------------------------------------
  var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
    style: 'margin:0 auto;',
		margins: '20 0 0 0',
		columns: [
			{ header : 'No. de Campo', dataIndex : 'NUMCAMPO', width : 100, sortable : true	},
			{ header : 'Descripci�n', dataIndex : 'DESC', width : 190, sortable : true	},
      { header : 'Tipo de Dato', dataIndex : 'TIPODATO', width : 140, sortable : true	}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 460,
		height: 160,
    hidden: true,
		//title: '<div align="left">Nota:<br>Se deber� cargar un archivo de texto(.txt) comprimido (.zip), el cual deber� contener un layout con los datos de los registros que se ajustar�n; los datos deben estar separados por pipe  "|"</div>',
		frame: true,
    bbar: {
			xtype: 'toolbar', items:[{
        xtype:'panel',
        id:'pnlMsgLayout',
        style: 'margin:0 auto;',
        width:450,
        hidden: true,
        html: '<b>Nota</b>: la consulta se realizar� por el IC_PYME los dem�s campos son solo informativos,<br>no tendran ninguna validaci�n.'
      }
    ]
    }
	});
	
	var elementosForma = [	
    {
      xtype: 'combo',
      name: 'cboEpo',
      id: 'cboEpo1',
      hidden: false,
      fieldLabel: 'Nombre de la EPO',
      mode: 'local', 
      displayField : 'descripcion',
      valueField : 'clave',
      hiddenName : 'cboEpo',
      emptyText: 'Seleccionar',
      width: 580,
      forceSelection : true,
      triggerAction : 'all',
      typeAhead: true,
      minChars : 1,
      store : storeCatEpoData,
      listeners:{
        select : function(cboIf, record, index ) {
          pnl.el.unmask();
          
        }
      },
      tpl : NE.util.templateMensajeCargaCombo
    },
    {
      xtype: 'compositefield',
      fieldLabel: '',
      id: 'cargaArchivo1',
      combineErrors: false,
      msgTarget: 'side',
      items: [
        {
          xtype: 'fileuploadfield',
          id: 'archivoPDF',
          width: 380,
          emptyText: 'Ruta del Archivo',
          fieldLabel: 'Carga de Solicitud Baja de Proveedores',
          name: 'archivoPDF',
          buttonText: '',
          buttonCfg: {
            iconCls: 'upload-icon'
          },
          //anchor: '95%',
          regex: /^.*\.(pdf|PDF)$/,
          regexText:'Solo se admiten archivos PDF'
        },
        {
          xtype: 'hidden',
          id:	'hidExtensionPDF',
          name:	'hidExtensionPDF',
          value:	''
        },
        {
          xtype: 'hidden',
          id:	'hidNumTotalPDF',
          name:	'hidNumTotalPDF',
          value:	''
        },
        {
          xtype: 'hidden',
          id:	'hidTotalMontoPDF',
          name:	'hidTotalMontoPDF',
          value:	''
        }/*
        {
          xtype: 	'button',
          text: 	'Continuar',
          id: 		'btnContinuar',
          iconCls: 'icoContinuar',
          style: {
              marginBottom:  '10px'
          },
          handler: function(){
            var cargaArchivo = Ext.getCmp('archivo');
            if (!cargaArchivo.isValid()){
              cargaArchivo.focus();
              return;
            }
            var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
            var extArchivo = Ext.getCmp('hidExtension');
            var numTotal = Ext.getCmp('hidNumTotal');
            var totalMonto = Ext.getCmp('hidTotalMonto');
            var objMessage = Ext.getCmp('pnlMsgValid1');


            objMessage.hide();
            //numTotal.setValue(Ext.getCmp('numtotalvenc1').getValue());
            //totalMonto.setValue(Ext.getCmp('montototalvenc1').getValue());

            if (/^.*\.(zip)$/.test(ifile)){
              extArchivo.setValue('zip');
            }

            fpCarga.getForm().submit({
              url: '33ajustefondojr_file.data.jsp',
              waitMsg: 'Enviando datos...',
              success: function(form, action) {
                var resp = action.result;
                var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
                var objMsg = Ext.getCmp('pnlMsgValid1');

                if(!resp.flag){
                  //SE MOSTRARAN LEYENDAS DE VALIDACIONES
                  objMsg.show();
                  //objMsg.body.update('El tama�o del archivo ZIP es mayor al permitido');
                  objMsg.body.update(resp.msgError);
                }else if(!resp.flagExt){
                  objMsg.show();
                  objMsg.body.update('El archivo contenido dentro del ZIP no es un archivo TXT');
                }else if(!resp.flagReg){
                  objMsg.show();
                  objMsg.body.update('El archivo TXT no puede contener mas de 30000 registros');
                }else{
                  ObjGral.numProc = resp.numProceso;
                  ajaxPeticion(resp.numProceso, '1');

                }

              },
              failure: NE.util.mostrarSubmitError
            })
          }
        }*/
      ]
    },
    {
      xtype: 'compositefield',
      fieldLabel: '',
      id: 'cargaArchivo2',
      combineErrors: false,
      msgTarget: 'side',
      items: [
        {
          xtype: 'fileuploadfield',
          id: 'archivoZIP',
          width: 380,
          emptyText: 'Ruta del Archivo',
          fieldLabel: 'Carga Proveedores Baja',
          name: 'archivoZIP',
          buttonText: '',
          buttonCfg: {
            iconCls: 'upload-icon'
          },
          //anchor: '95%',
          regex: /^.*\.(zip|ZIP)$/,
          regexText:'Solo se admiten archivos ZIP'
        },
        {
          xtype: 'hidden',
          id:	'hidExtensionZIP',
          name:	'hidExtensionZIP',
          value:	''
        },
        {
          xtype: 'hidden',
          id:	'hidNumTotalZIP',
          name:	'hidNumTotalZIP',
          value:	''
        },
        {
          xtype: 'hidden',
          id:	'hidTotalMontoZIP',
          name:	'hidTotalMontoZIP',
          value:	''
        }
      ]
    },
    {
      xtype: 'panel',
      name: 'pnlMsgValid',
      id: 'pnlMsgValid1',
      width: 500,
      style: 'margin:0 auto;',
      frame: true,
      hidden: true

    },
    {
      xtype: 'panel',
      layout: 'hbox',
      bodyStyle: 'padding: 6px',
      items:[ {
          xtype:'button',
          iconCls: 'icoAyuda', id: 'btnAyuda',
          handler: function(boton, evento) {	
            if (!gridLayout.isVisible()) {
              gridLayout.show();
              Ext.getCmp('pnlMsgLayout').show();
            }else{
              gridLayout.hide();
              Ext.getCmp('pnlMsgLayout').hide();
            }
          }
        },{
          xtype:'box',
          width:355,
          html: 'Lay-out de ayuda'
        },{
          xtype:'button',
          text: 'Continuar', id: 'btnContinuar', iconCls: 'icoTxt', formBind: true,
          handler: fnContinuarCapt
        },			
        {
          xtype:'button',
          text: 'Cancelar', iconCls: 'icoLimpiar', handler: function() {
            window.location = '15captbajaprov_ext.jsp';
          }
        }]
    },
    {
      xtype:'panel',
      html: 'Nota: La depuraci�n de Registros solo considerar� a los Proveedores con estatus �S� en la Afiliaci�n y que no tenga ning�n documento publicado en factoraje.'
    }
  ];


//CONTENEDORES, GRIDS------------------------------------------------------------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Captura Proveedores Baja',
		frame: true,
		collapsible: true,
		titleCollapse: false,
    fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true
	});
  

var gridRegCorrectos= new Ext.grid.GridPanel({
	height:230,
	width: 560,
	collapsible: false,
	hidden:false,
  frame: true,
	style: ' margin:0 auto;',
	title:'',
	id: 'gridRegCorrectos',
	store: storeRegCorrectData,
	loadMask: true,
	// grid columns
	columns:[
    {header: "Linea",dataIndex: 'LINEA',width:150},
    {header: "Nombre Proveedor",dataIndex: 'DESCRIPCION',width:400, align:'LEFT'}
	]        
});

var gridRegErroneos= new Ext.grid.GridPanel({
	height:230,
	width: 560,
	collapsible: false,
  frame: true,
	hidden:false,
	style: ' margin:0 auto;',
	title:'',
	id: 'gridRegErroneos1',
	store: storeRegErrorData,
	loadMask: true,
	// grid columns
	columns:[
    {header: "Linea",dataIndex: 'LINEA',width:150},
    {header: "Nombre Proveedor",dataIndex: 'DESCRIPCION',width:400, align:'LEFT'}
	]        
});

  //TAB UNO
var tabDetRegCorrectos = new Ext.Panel({
	title:'Registros sin Error',
  frame:true,
	id:'tabDetRegCorrectos',
	height: 260,
	items:[NE.util.getEspaciador(20),gridRegCorrectos,NE.util.getEspaciador(20)]
});

//TAB DOS
var tabDetRegErroneos = new Ext.Panel({
	title:'Registros con Errores',
	frame: true,
	id:'tabDetRegErroneos',
	height: 260,
	items:[NE.util.getEspaciador(20),gridRegErroneos,NE.util.getEspaciador(20)]
});

  //TAB PRINCIPAL
var tabResultValid = new Ext.TabPanel({
    id: 'tabResultValid',
    border: true,
    //hidden:true,
    frame: false,
    width:600,
    height: 280,
    activeTab: 0,
    style: 'margin:0 auto;',
    items:[ tabDetRegCorrectos, tabDetRegErroneos],
    listeners:{
      tabchange: function(ObjTab, newTabPanel){			
        //tabActual=newTabPanel.getId();
      }
    }
  });

var gridPreAcuse = new Ext.grid.GridPanel({
		id: 'gridPreAcuse',
		store: storePreAcuseData,
    style: 'margin:0 auto;',
    title: '<center>Cifras de Control</center>',
		margins: '20 0 0 0',
		columns: [
			{ header : '', dataIndex : 'NOMBRE', width : 350, sortable : true	},
			{ header : '', dataIndex : 'VALOR', width : 100, sortable : true	}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 480,
		height: 120,
    hidden: false,
		frame: true
  });
  
	  var confirmar = function(pkcs7, textoFirmar){
	  
	  
		if (Ext.isEmpty(pkcs7)) {
            btn.enable();
            return;	
          }else{
             Ext.Ajax.request({
              url: '15captbajaprov_ext.data.jsp',
              params: Ext.apply(fp.getForm().getValues(),{
                informacion: 'realizaBajaProveedores',
                numProceso: numProcesoGbl,
                nombrePDF: nombrePDF,
                nombreZIP: nombreZIP,
                Pkcs7: pkcs7,
                TextoFirmado: textoFirmar,
                totRegCorrect: Ext.getCmp('totRegCorrect1').getValue(),
                totRegError: Ext.getCmp('totRegError1').getValue()
                
              }),
              callback: procesarSuccessBajaProveedores
            });
          }	  
	  }
	  
	  
  
var panelPreAcuse = new Ext.Panel({
		name: 'pnPreAcuse',
		id: 'pnPreAcuse1',
    title: 'Pre Acuse',
		width: 500,
		style: 'margin:0 auto;',
    items:[gridPreAcuse],
		frame: true,
    hidden: true,
    buttonAlign: 'center',
    buttons:[
      {
        text: 'Cancelar', id: 'btnCancelPreAcuse', iconCls: 'icoCancelar', handler: function(btn){
          window.location = '15captbajaprov_ext.jsp';
        }
      },
      {
        text: 'Continuar', id: 'btnCancelContinuar', iconCls: 'icoContinuar', handler: function(btn){
          var intCont = 0;
          var textoFirmar = "Baja de Proveedores.\n";
          textoFirmar += 'Total de Registros a Eliminar: '+Ext.getCmp('totRegCorrect1').getValue()+'\n';
          
          storeRegCorrectData.each(function(record){
            textoFirmar+= record.get('DESCRIPCION')+', ';
            if(++intCont==3){
              textoFirmar+='\n';
              intCont=0;
            }
          });
          
          
			NE.util.obtenerPKCS7(confirmar, textoFirmar);			  
		  
        }
      }
    ]
	});
  
  var gridAcuse = new Ext.grid.GridPanel({
		id: 'gridAcuse',
		store: storeAcuseData,
    style: 'margin:0 auto;',
    //title: '<center>Cifras de Control</center>',
		margins: '20 0 0 0',
		columns: [
			{ header : '', dataIndex : 'NOMBRE', width : 350, sortable : true	},
			{ header : '', dataIndex : 'VALOR', width : 100, sortable : true	}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 480,
		height: 120,
    hidden: false,
		frame: true
  });
  
  var panelAcuse = new Ext.Panel({
		name: 'panelAcuse',
		id: 'panelAcuse1',
    title: 'Acuse de la Carga',
		width: 500,
		style: 'margin:0 auto;',
    items:[gridAcuse],
		frame: true,
    hidden: true,
    buttonAlign: 'center',
    buttons:[
      {
        text: 'Imprimir PDF', id: 'btnImprimirPdfAcuse', iconCls: 'icoImprimir' //, handler: fnDescargaAcusePdf
      },
      {
        text: 'Salir', id: 'btnSalirAcuse', iconCls: 'icoCancelar', handler: function(btn){
          window.location = '15captbajaprov_ext.jsp';
        }
      }
    ]
	});
  
  var pnl = new Ext.Container({
    id: 'contenedorPrincipal',
    applyTo: 'areaContenido',
    style: 'margin:0 auto;',
    width: 949,
    items: [
      NE.util.getEspaciador(20),
      fp,
      {
        xtype:'panel',
        layout: 'form',
        frame: true,
        title:'Resultado del Proceso de Depuraci�n de Proveedores',
        id: 'tbPanelResult',
        labelWidth: 250,
        hidden: true,
        width:600,
        style: 'margin:0 auto;',
        buttonAlign: 'center',
        items:[tabResultValid,
          {
            xtype: 'textfield', name: 'totRegCorrect', id: 'totRegCorrect1', fieldLabel: 'Total de Registros a Eliminar', readOnly: true,
            allowBlank: true, maxLength: 100,	 width: 80, msgTarget: 'side', margins: '0 20 0 0'  //necesario para mostrar el icono de error
          },
          {
            xtype: 'textfield', name: 'totRegError', id: 'totRegError1', fieldLabel: 'Total de Registros que no se Eliminar�n', readOnly: true,
            allowBlank: true, maxLength: 100,	 width: 80, msgTarget: 'side', margins: '0 20 0 0'  //necesario para mostrar el icono de error
          }
        ],
        buttons:[
          {
            iconCls: 'icoCancelar', text: 'Cancelar', id: 'btnCancelavalid',
            handler: function(boton, evento) {	
               window.location = '15captbajaprov_ext.jsp';
            }
          },
          {
            text: 'Descargar Errores', id: 'btnDescargaError', iconCls: 'icoXls', formBind: true,
            handler: fnDescargaErroresCapt
          },			
          {
            text: 'Continuar Carga', iconCls: 'icoContinuar', id: 'btnContinuarValid', handler: function() {
              Ext.getCmp('tbPanelResult').hide();
              panelPreAcuse.show();
              
              
              storePreAcuseData.loadData([['No. Total de registros a Eliminar',Ext.getCmp('totRegCorrect1').getValue()],
                                          ['No. Total de Registros que no se Eliminar�n',Ext.getCmp('totRegError1').getValue()]
                                        ]);
            }
          }
        ]
      },
      panelPreAcuse,
      panelAcuse,
      NE.util.getEspaciador(20),
      gridLayout
    ]
  });
  
  storeCatEpoData.load();
	
});