<%@ page contentType="text/html; charset=UTF-8"
	import="
		javax.naming.*,	
		java.text.*,			
		com.netro.exception.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,		
		netropology.utilerias.*,	
		java.util.*,
		java.io.*,
		java.text.*,		
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,	
		com.netro.cadenas.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String rutaArchivoTemporal  ="";
	ParametrosRequest req = null;
	String itemArchivo	="",  rutaArchivo ="",   error_tam =""; 
	String PATH_FILE	=	strDirectorioTemp;
	String nombreArchivo= "";	
	String btnContinuar  ="N";
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
	
		req = new ParametrosRequest(upload.parseRequest(request));
			 
		FileItem fItem = (FileItem)req.getFiles().get(0);
		itemArchivo		= (String)fItem.getName();
		InputStream archivo = fItem.getInputStream();
		rutaArchivo = PATH_FILE+itemArchivo;
		int tamanio			= (int)fItem.getSize();
		
		nombreArchivo= Comunes.cadenaAleatoria(16)+ ".txt";
		rutaArchivoTemporal = PATH_FILE + "/" + nombreArchivo;
		fItem.write(new File(rutaArchivoTemporal));
		
		if(tamanio>2097152){
			error_tam ="El Archivo es muy Grande, excede el Límite que es de 2 MB.";
		}		
	}
	if(!error_tam.equals("")){
		nombreArchivo="";
	}
		
	String [] mensajeEstatus = {""};
	String mensajeResumen = "";
	int status = 0;
	boolean hayCaractCtrl = false;
	String nombreArchivoCaractEsp = "";
	boolean continuar = true;

	JSONArray arrRegistrosValidos = new JSONArray();
	JSONArray arrRegistrosError = new JSONArray();
	JSONArray registrosCargaValidos = new JSONArray();
	JSONObject valoresResultadoProceso = new JSONObject();
    JSONArray registrosNoFloating = new JSONArray();
	 
	if(continuar){
		
		CargaPymeFloating datos = new CargaPymeFloating();
		Map<String, Object> registros = new HashMap<>();
		registros  =  datos.procesarCargaPymeFloating( rutaArchivoTemporal);
		
		List error =(List)registros.get("ERROR");
		List exito =(List)registros.get("EXITO");
		List carga =(List)registros.get("CARGA");	
		List noFloating =(List)registros.get("NOFLOATING");	
				
		int regValidos =0;
		int regInvalidos =0;
		int totalReg =0;	
		StringBuilder pymesFloating = new StringBuilder();
		StringBuilder pymesNOFloating = new StringBuilder();
		
		if(error !=null){	
			regInvalidos = error.size();
			for (int i = 0; i <error.size(); i++) {
				HashMap dat = (HashMap)error.get(i);				 
				JSONObject registro = new JSONObject();
				registro.put("LINEA",  dat.get("LINEA").toString() );
				registro.put("CAMPO",dat.get("CAMPO").toString());
				registro.put("OBSERVACION",dat.get("OBSERVACION").toString());
				arrRegistrosError.add(registro);		
			}
		}
		
		if(exito !=null){	
			regValidos =exito.size();
			for (int i = 0; i <exito.size(); i++) {
				HashMap dat = (HashMap)exito.get(i);				 
				JSONObject registro = new JSONObject();
				registro.put("LINEA",  dat.get("LINEA").toString() );
				registro.put("CAMPO",dat.get("CAMPO").toString());
				registro.put("OBSERVACION",dat.get("OBSERVACION").toString());
				arrRegistrosValidos.add(registro);		
			}
			if(regValidos>0) {
				btnContinuar  ="S";
			}
		}
		
		if(carga !=null){		
			for (int i = 0; i <carga.size(); i++) {
				HashMap dat = (HashMap)carga.get(i);				 
				JSONObject registro = new JSONObject();
				registro.put("IC_PYME",  dat.get("IC_PYME").toString() );
				registro.put("NOMBRE_PYME",dat.get("NOMBRE_PYME").toString());
				registro.put("RFC",dat.get("RFC").toString());
				registro.put("NAFIN_ELECTRONICO",dat.get("NAFIN_ELECTRONICO").toString());
				registro.put("PARAMETRIZACION",dat.get("PARAMETRIZACION").toString());
				registrosCargaValidos.add(registro);					
				pymesFloating.append(pymesFloating.length() > 0 ? "|" :"").append(dat.get("IC_PYME").toString());	
			}		
		}
		
		//System.out.println ("noFloating------>"+noFloating);
		if(noFloating !=null){		
			for (int y = 0; y <noFloating.size(); y++) {
				HashMap datN = (HashMap)noFloating.get(y);				 
				JSONObject registroN = new JSONObject();
				registroN.put("IC_PYME",  datN.get("IC_PYME").toString() );
				registroN.put("NOMBRE_PYME",datN.get("NOMBRE_PYME").toString());
				registroN.put("RFC",datN.get("RFC").toString());
				registroN.put("NAFIN_ELECTRONICO",datN.get("NAFIN_ELECTRONICO").toString());
				registroN.put("PARAMETRIZACION",datN.get("PARAMETRIZACION").toString());
				registrosNoFloating.add(registroN);	
				pymesNOFloating.append(pymesNOFloating.length() > 0 ? "|" :"").append(datN.get("IC_PYME").toString());
			}		
		}
		
		
		totalReg = regValidos +regInvalidos;
		
		valoresResultadoProceso.put("totalRegistrosValidos", new Integer(regValidos));
		valoresResultadoProceso.put("totalRegistrosInvalidos", new Integer(regInvalidos));
		valoresResultadoProceso.put("btnContinuar", btnContinuar);	
		valoresResultadoProceso.put("regValidos", regValidos);			
		valoresResultadoProceso.put("totalReg", totalReg);	
		valoresResultadoProceso.put("pymesFloating", pymesFloating.toString());	
		valoresResultadoProceso.put("pymesNOFloating", pymesNOFloating.toString());	
	}

	
	
%>
({
	"success": true,
	"registrosValidos": <%=arrRegistrosValidos%>,
	"registrosInvalidos": <%=arrRegistrosError%>,
	"registrosCargaValidos": <%=registrosCargaValidos%>,
	"registrosNoFloating": <%=registrosNoFloating%>,
	"valoresResultadoProceso": <%=valoresResultadoProceso%> })


