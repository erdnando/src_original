
<%@ page contentType="application/json;charset=UTF-8"
	import=" 
		javax.naming.*,
		java.util.*,
		java.sql.*,
		java.text.*, 	
		java.net.*,
		netropology.utilerias.*,	
		com.netro.afiliacion.*,
		com.netro.cadenas.*,		
		com.netro.exception.*,
		com.netro.model.catalogos.*,
		com.netro.seguridadbean.SeguException,
		com.netro.exception.NafinException,	
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%@ include file="../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String num_electronico = (request.getParameter("num_electronico") != null) ? request.getParameter("num_electronico") : "";
String fec_ini = (request.getParameter("fec_ini") != null) ? request.getParameter("fec_ini") : "";
String fec_fin = (request.getParameter("fec_fin") != null) ? request.getParameter("fec_fin") : "";
String emergente = (request.getParameter("emergente") != null) ? request.getParameter("emergente") : "";
String chkConvenio = (request.getParameter("chkConvenio") != null) ? request.getParameter("chkConvenio") : "";
if(chkConvenio.equals("on"))  { chkConvenio ="S"; }


String txtCadProductiva  =iNoCliente;
String infoRegresar ="", consulta ="";
JSONObject 	jsonObj	= new JSONObject();

if (informacion.equals("Consultar") ) {
	
	ConsSinNumProv paginador = new ConsSinNumProv();
	paginador.setTxtCadProductiva(txtCadProductiva);
	paginador.setNum_electronico(num_electronico);
	paginador.setFec_ini(fec_ini);
	paginador.setFec_fin(fec_fin);
	paginador.setConvenio_unico(chkConvenio);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
	
	if (operacion.equals("Generar") ){
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}				
	}	
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("emergente",emergente);
	infoRegresar= jsonObj.toString();	
		
		
} else if (informacion.equals("busquedaAvanzada") ) {

	String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
	String noPyme = request.getParameter("noPyme")==null?"":request.getParameter("noPyme");
	
	
	CatalogoPymeBus cat = new CatalogoPymeBus();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");
	cat.setOrden("p.cg_razon_social");
	cat.setClaveEpo(iNoCliente);
	cat.setNombrePyme(nombrePyme);
	cat.setRfcPyme(rfcPyme);
	cat.setClavePyme(noPyme);	
	
	infoRegresar = cat.getJSONElementos();


} else if (informacion.equals("Guardar") ) {
	
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
	
	Calendar fecActual = Calendar.getInstance();
	String dia = Integer.toString(fecActual.get(Calendar.DATE));
	String mes = Integer.toString(fecActual.get(Calendar.MONTH)+1);
	String anio = Integer.toString(fecActual.get(Calendar.YEAR));

	String 	fecha = dia+"/"+mes+"/"+anio;
	List lstDatosTabla = new ArrayList();
	String msg_error ="";	
	
	String txtCadProductivas []	=	request.getParameterValues("txtCadProductiva");
	String fechas []	=	request.getParameterValues("fecha");
	String regCveProv []	=	request.getParameterValues("regCveProv");
	String regNumProv []	=	request.getParameterValues("regNumProv");
	String regChkElimina []	=	request.getParameterValues("regChkElimina");
	String regCausa []	=	request.getParameterValues("regCausa");
	String externContent = (request.getParameter("textoFirmado") != null) ? request.getParameter("textoFirmado") : "";
	String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
	
								
	for(int i=0;i<txtCadProductivas.length;i++){
	
		List lstRegNumProv = new ArrayList();
		
		lstRegNumProv.add(txtCadProductivas[i]);
		lstRegNumProv.add(fechas[i]);
		lstRegNumProv.add(regCveProv[i]);
		lstRegNumProv.add(regNumProv[i]);
		lstRegNumProv.add(regChkElimina[i]);
		lstRegNumProv.add(regCausa[i]);
		
		lstDatosTabla.add(lstRegNumProv);
	}
	
	Seguridad s = new Seguridad();	
	String		_acuse	= "";
	String folioCert = "";
	char getReceipt = 'Y';
	AccesoDB con = new AccesoDB();

	try {
		con.conexionDB();
		if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			folioCert = "CA"+txtCadProductiva;
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{			
				msg_error =  afiliacion.actualizaProvSinNumero(lstDatosTabla,iNoUsuario);
			}else {
				msg_error ="Ocurrió un error durante el Proceso de Firma";
			}
		}
			
	}catch(NafinException ne){
		msg_error ="Error Ya existe Numero de Proveedor asociado a la EPO";

	}catch(Exception e){
		e.printStackTrace();
		msg_error ="Error en el proceso de Guardado";
	}finally{
		if(con.hayConexionAbierta())
		con.cierraConexionDB();
	}

	
	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("msg_error",msg_error);			
	infoRegresar = jsonObj.toString();	
	
}

%>

<%=infoRegresar%>
