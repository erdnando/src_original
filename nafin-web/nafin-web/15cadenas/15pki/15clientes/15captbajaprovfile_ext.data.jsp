<%@ page
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.parametrosgrales.*,
		com.netro.zip.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="text/html; charset=UTF-8"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>

<%
    String nombreZIP = "";
    String nombrePDF = "";
    JSONObject registroAcuse = new JSONObject();
    JSONArray arrAcuse = new JSONArray();
    boolean flag = true;
    boolean flagPdf = true;
    boolean flagReg = true;
    ParametrosRequest req = null;
    String rutaArchivo = "";
    String PATH_FILE	=	strDirectorioTemp;
    String itemArchivo = "";
    String extensionPDF = "";
    String extensionZIP = "";
    String folio = "";
    String fechaHora = "";
    String numProceso = "";
    String numTotal = "";
    String totalMonto = "";
    String msgError = "";
    String msgErrorPdf = "";
    String nombreTxtZip = "";
    int totalRegistros = 0;
    int tamanio = 0;
	boolean codificacionValida = true;
	String codificacionArchivo = "";
	
    BigDecimal montoTotal = new BigDecimal("0.0");
    
    IMantenimiento beanMantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB",IMantenimiento.class);
	
	if (ServletFileUpload.isMultipartContent(request)) {
    
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		extensionPDF = (req.getParameter("hidExtensionPDF") == null)? "" : req.getParameter("hidExtensionPDF");
		extensionZIP = (req.getParameter("hidExtensionZIP") == null)? "" : req.getParameter("hidExtensionZIP");
		
		System.out.println("extensionPDF=="+extensionPDF);
		System.out.println("extensionZIP=="+extensionZIP);
		try{
            int tamaPer = 0;
            
            if (extensionPDF.equals("pdf")){
                upload.setSizeMax(5000 * 1024);	//5 Mb
                tamaPer = (5000 * 1024);
            }
            
            
            FileItem fItem = (FileItem)req.getFiles().get(0);
            itemArchivo		= (String)fItem.getName();
            InputStream archivoPDF = fItem.getInputStream();
            tamanio			= (int)fItem.getSize();
            nombrePDF = itemArchivo;
            System.out.println("tamanio=="+tamanio);
            System.out.println("tamaPer=="+tamaPer);
            System.out.println("itemArchivo=="+itemArchivo);
            
            java.io.File ftPdf = new java.io.File(PATH_FILE + nombrePDF);
            fItem.write(ftPdf);
            
            if (tamanio > tamaPer){
                flagPdf = false;
                msgErrorPdf = "El tamaño del archivo .PDF no debe de ser mayor a 5 MB.";
            }
            
            if (extensionZIP.equals("zip")){
                upload.setSizeMax(700 * 1024);	//700 Kb
                tamaPer = (700 * 1024);
            }
            
            fItem = (FileItem)req.getFiles().get(1);
            itemArchivo		= (String)fItem.getName();
            InputStream archivoZip = fItem.getInputStream();
            tamanio			= (int)fItem.getSize();
            nombreZIP = itemArchivo;
            java.io.File ftZip = new java.io.File(PATH_FILE + nombreZIP);
            fItem.write(ftZip);
    
            rutaArchivo = PATH_FILE+itemArchivo;
            
            System.out.println("tamanio=="+tamanio);
            System.out.println("tamaPer=="+tamaPer);
            System.out.println("itemArchivo=="+itemArchivo);
            
            if (tamanio > tamaPer){
                flag = false;
                msgError = "El tamaño del archivo ZIP es mayor al permitido";
            }
			
			
			ParametrosGrales paramGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
			
			if(flag && paramGrales.validaCodificacionArchivoHabilitado()){
				String codificacionDetectada[] = {""};
				CodificacionArchivo codificaArchivo = new CodificacionArchivo();
				codificaArchivo.setProcessTxt(true);
				codificaArchivo.setProcessZip(true);
				
				if(codificaArchivo.esCharsetNoSoportado(PATH_FILE + nombreZIP,codificacionDetectada)){
					codificacionArchivo = codificacionDetectada[0];
					codificacionValida = false;
					flag = false;
				}
			}
			
            if (flag){
    
                fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
                    
				try{
                
                    List lArchivos = ComunesZIP.descomprimir(archivoZip, PATH_FILE);
                    if(lArchivos.size()>1){
                        flag = false;
                        msgError = "El archivo .zip, contiene más de un archivo.";
                    }else if(lArchivos.size()<=0){
						flag = false;
						msgError = "El archivo .zip, no trae archivo.";
					}else{
                
						for(int i=0;i<lArchivos.size();i++) {
							String nombre 	= lArchivos.get(i).toString();
							String ext 	= nombre.substring(nombre.lastIndexOf('.') + 1, nombre.length()).toUpperCase();
							String rutaTxt = PATH_FILE+nombre;
							nombreTxtZip = nombre;
							if(!("TXT".equals(ext))) {
								//throw new Exception("Error, el formato del Archivo no es el correcto. ("+nombre+") ");
								flag = false;
								msgError = "El archivo contenido dentro del ZIP no es un archivo TXT";
							}else{
								BufferedReader brt=  null;
								String linea = "";
						
								java.io.File ft = new java.io.File(rutaTxt);
								brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
								
								String prestamo	= "";
								String cuota		= "";
								String totalVenc	= "";
								String fechaVenc	= "";
								
								VectorTokenizer vtd = null;
								Vector vecdet = null;
								
								while((linea=brt.readLine())!=null){
									System.out.println("LINEA=========="+linea);
									vtd		= new VectorTokenizer(linea,"|");
									vecdet	= vtd.getValuesVector();
									totalRegistros++;
								}
						
								if(totalRegistros>0 && totalRegistros<=30000){
									rutaArchivo = rutaTxt;
									numProceso = beanMantenimiento.cargaDatosTmpBajaProveedores(rutaTxt,"");
									//flag = false;
									//msgError = "El número de registros no corresponde con el número de vencimientos capturado";
						
								}else{
									flag = false;
									msgError = "'El archivo TXT no puede contener mas de 30000 registros";
								}
							}
						}
					}
				}catch (Exception e){
					flag = false;
					msgError = "El archivo .zip, dentro de su estructura, posee al menos un directorio.";
					//throw new AppException("Ocurrio un error al generar el archivo", e);
				}
			}
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}
	
%>	
{
    "success": true,
    "flag":	<%=(flag)?"true":"false"%>,
    "flagPdf":	<%=(flagPdf)?"true":"false"%>,
    "nombreZIP":	'<%=(nombreZIP)%>',
    "nombrePDF":	'<%=(nombrePDF)%>',
    "numProceso":	'<%=numProceso%>',
    "totalRegistros":	'<%=totalRegistros%>',
    "msgErrorPdf":	'<%=msgErrorPdf%>',
    "msgError":	'<%=msgError%>',
    "rutaTxt": '<%=nombreTxtZip%>',
	"codificacionValida": <%=(codificacionValida)?"true":"false"%>,
	"codificacionArchivo": '<%=codificacionArchivo%>'
}