
<%@ page contentType="application/json;charset=UTF-8"
	import=" 
		javax.naming.*,
		java.util.*,
		java.sql.*,
		java.text.*, 	
		java.net.*,
    com.netro.pdf.*,
		netropology.utilerias.*,	
		netropology.utilerias.caracterescontrol.*,
		com.netro.descuento.*,
		com.netro.cadenas.*,		
		com.netro.exception.*,
		com.netro.model.catalogos.*,
		com.netro.seguridadbean.SeguException,
		com.netro.exception.NafinException,	
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%@ include file="../certificado.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
JSONObject 	jsonObj	= new JSONObject();
JSONArray jsObjArray = new JSONArray();
IMantenimiento beanMantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB",IMantenimiento.class);
try{
  if (informacion.equals("getCatalogoEPO") ) {
    
    CatalogoEPO cat = new CatalogoEPO();
    cat.setCampoClave("ic_epo");
    cat.setCampoDescripcion("cg_razon_social");
    infoRegresar = cat.getJSONElementos();

    } else if (informacion.equals("ValidaCaracteresEspeciales") ) {
            String rutaTxt = request.getParameter("rutaTxt")==null?"":request.getParameter("rutaTxt");
            String numProceso = request.getParameter("numProceso")==null?"":request.getParameter("numProceso");
            
            session.removeAttribute("15captbajaprovbuscaCCtrlThread");
            ResumenBusqueda resumenBusqueda = new ResumenBusqueda();
            BuscaCaracteresControlThread buscaCCtrlThread = new BuscaCaracteresControlThread();
            buscaCCtrlThread.setRutaArchivo(strDirectorioTemp+rutaTxt);
            buscaCCtrlThread.setRegistrador(resumenBusqueda);
            buscaCCtrlThread.setProcessZip(true);
            buscaCCtrlThread.setCreaArchivoDetalle(true);
            buscaCCtrlThread.setDirectorioTemporal(strDirectorioTemp);
            buscaCCtrlThread.setSeparadorCampo("|");
            buscaCCtrlThread.start();
            session.setAttribute("15captbajaprovbuscaCCtrlThread", buscaCCtrlThread);
            
            jsonObj.put("success", new Boolean(true));
            jsonObj.put("numProceso", numProceso);
            infoRegresar = jsonObj.toString();	
    } else if (informacion.equals("ValidaEstatusHiloCaracteresEspeciales")) {
            String numProceso = request.getParameter("numProceso")==null?"":request.getParameter("numProceso");
            String[] mensajeEstatus = {""};
            
            BuscaCaracteresControlThread buscaCCtrlThread = (BuscaCaracteresControlThread)session.getAttribute("15captbajaprovbuscaCCtrlThread");
            int status = buscaCCtrlThread.getStatus(mensajeEstatus);
            ResumenBusqueda resumen = (ResumenBusqueda)buscaCCtrlThread.getRegistrador();
            boolean hayCaractCtrl = resumen.hayCaracteresControl();
            String mensaje = resumen.getMensaje();
            String nombreArchivo = resumen.getNombreArchivoDetalle();
            
            
            jsonObj.put("success", new Boolean(true));
            jsonObj.put("numProceso", numProceso);
            jsonObj.put("statusThread", String.valueOf(status));
            jsonObj.put("mensajeEstatus", mensajeEstatus);
            jsonObj.put("mensaje", mensaje);
            jsonObj.put("hayCaractCtrl", new Boolean(hayCaractCtrl));
            jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
            infoRegresar = jsonObj.toString();	
    } else if (informacion.equals("ValidaCargaArchivo") ) {

    //String textoFirmado = request.getParameter("TextoFirmado")==null?"":request.getParameter("TextoFirmado");
    //String pkcs7 = request.getParameter("Pkcs7")==null?"":request.getParameter("Pkcs7");
    String numProceso = request.getParameter("numProceso")==null?"":request.getParameter("numProceso");
    String cboEpo = request.getParameter("cboEpo")==null?"":request.getParameter("cboEpo");
    
    System.out.println("numProceso ===== "+numProceso);
    beanMantenimiento.validaCargaProveedores( numProceso, cboEpo);
    List lstRegValidados = beanMantenimiento.getListBajaProvValidados(numProceso);
    //String nombreArchivo = beanMantenimiento.setBajaProveedores( numProceso, pkcs7, textoFirmado, _serial);
    List lstCorrect = (List)lstRegValidados.get(0);
    List lstErrores = (List)lstRegValidados.get(1);
    
    jsObjArray = JSONArray.fromObject(lstCorrect);
    jsonObj.put("registrosCorrect",jsObjArray.toString() );
    
    jsObjArray = JSONArray.fromObject(lstErrores);
    jsonObj.put("registrosError",jsObjArray.toString() );
    
    jsonObj.put("msg_error","");
    jsonObj.put("success", new Boolean(true));
    //jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
    infoRegresar = jsonObj.toString();	
   } else if (informacion.equals("DescargaArchivoErrores") ) {
    CreaArchivo archivo = new CreaArchivo();
    
    String numProceso = request.getParameter("numProceso")==null?"":request.getParameter("numProceso");
    String cboEpo = request.getParameter("cboEpo")==null?"":request.getParameter("cboEpo");
    String nombreEpo = request.getParameter("nombreEpo")==null?"":request.getParameter("nombreEpo");
    StringBuffer strArchivo = new StringBuffer();
    System.out.println("numProceso ===== "+numProceso);
    beanMantenimiento.validaCargaProveedores( numProceso, cboEpo);
    List lstRegValidados = beanMantenimiento.getListBajaProvValidados(numProceso);
    //String nombreArchivo = beanMantenimiento.setBajaProveedores( numProceso, pkcs7, textoFirmado, _serial);
    List lstCorrect = (List)lstRegValidados.get(0);
    List lstErrores = (List)lstRegValidados.get(1);
    
    strArchivo.append(nombreEpo.replaceAll(","," ")+"\n");
    strArchivo.append("No. Proveedor, Nombre Proveedor, RFC, IC_PYME, Descripcion Error\n");
    for(int x=0; x<lstErrores.size(); x++){
      HashMap hm = (HashMap)lstErrores.get(x);
      strArchivo.append((String)hm.get("CG_PYME_EPO_INTERNO")+",");
      strArchivo.append(((String)hm.get("CG_RAZON_SOCIAL")).replaceAll(","," ")+",");
      strArchivo.append((String)hm.get("CG_RFC")+",");
      strArchivo.append((String)hm.get("IC_PYME")+",");
      strArchivo.append((String)hm.get("DESCRIPCION")+"\n");
    }
    
    String nombreArchivo = "";
    if (!archivo.make(strArchivo.toString(), strDirectorioTemp, ".csv")) {
      jsonObj.put("success", new Boolean(false));
      jsonObj.put("msg_error", "Error al generar el archivo CSV ");
    } else {
      nombreArchivo = archivo.nombre;		
      jsonObj.put("success", new Boolean(true));
      jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
    }

    infoRegresar = jsonObj.toString();	
       
  } else if (informacion.equals("realizaBajaProveedores") ) {
    
    String textoFirmado = request.getParameter("TextoFirmado")==null?"":request.getParameter("TextoFirmado");
    String pkcs7 = request.getParameter("Pkcs7")==null?"":request.getParameter("Pkcs7");
    String numProceso = request.getParameter("numProceso")==null?"":request.getParameter("numProceso");
    String cboEpo = request.getParameter("cboEpo")==null?"":request.getParameter("cboEpo");
    String nombrePDF  = request.getParameter("nombrePDF")==null?"":request.getParameter("nombrePDF");
    String nombreZIP  = request.getParameter("nombreZIP")==null?"":request.getParameter("nombreZIP");
    String totRegCorrect = request.getParameter("totRegCorrect")==null?"":request.getParameter("totRegCorrect");
    String totRegError = request.getParameter("totRegError")==null?"":request.getParameter("totRegError");
    
    String _acuse = "";
    String folioCert = "";
    folioCert = "PROVBAJA"+iNoUsuario;//acuse.toString();

    
    String folioAcuse = beanMantenimiento.setBajaProveedores(numProceso, cboEpo,  strDirectorioTemp+nombreZIP, strDirectorioTemp+nombrePDF, pkcs7, textoFirmado, _serial , folioCert, iNoUsuario);
    String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String horaCarga = new java.text.SimpleDateFormat("HH:mm").format(new java.util.Date());
    
    
    CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo 	 = archivo.nombreArchivo()+".pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
    
    String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
    String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
    String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
    String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
    String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
    
    pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String) application.getAttribute("strDirectorioPublicacion"), "");
    pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	
    pdfDoc.addText("Su solicitud fue recibida con el número de folio: "+folioAcuse,"formas",ComunesPDF.CENTER); 
    pdfDoc.addText(" ","formas",ComunesPDF.CENTER);	
    
    pdfDoc.setTable(2, 80);
    pdfDoc.setCell("CIFRAS CONTROL","celda01",ComunesPDF.CENTER,2);
    pdfDoc.setCell("No. Total de Registros Eliminados","celda01",ComunesPDF.CENTER);	
    pdfDoc.setCell(totRegCorrect,"formas",ComunesPDF.CENTER);
    pdfDoc.setCell("No. Total de Registros que no se Eliminaron","celda01",ComunesPDF.CENTER);
    pdfDoc.setCell(totRegError,"formas",ComunesPDF.CENTER);
    pdfDoc.setCell("Fecha de Carga","celda01",ComunesPDF.CENTER);
    pdfDoc.setCell(fechaHoy,"formas",ComunesPDF.CENTER);
    pdfDoc.setCell("Hora de Carga","celda01",ComunesPDF.CENTER);
    pdfDoc.setCell(horaCarga,"formas",ComunesPDF.CENTER);
    pdfDoc.setCell("Usuario","celda01",ComunesPDF.CENTER);
    pdfDoc.setCell(iNoUsuario+" "+strNombreUsuario,"formas",ComunesPDF.CENTER);
    
    pdfDoc.addTable();
    pdfDoc.endDocument();
    
    jsonObj.put("folioAcuse", folioAcuse);
    jsonObj.put("fechaHoy", fechaHoy);
    jsonObj.put("horaCarga", horaCarga);
    jsonObj.put("captUser",iNoUsuario+" "+strNombreUsuario);	
    jsonObj.put("success", new Boolean(true));
    
    jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
    infoRegresar = jsonObj.toString();	
    
  }
}catch(Throwable t){
  t.printStackTrace();
  jsonObj.put("msg_error", "error inesperado");
  jsonObj.put("success", new Boolean(false));
	infoRegresar = jsonObj.toString();	
}

%>

<%=infoRegresar%>
