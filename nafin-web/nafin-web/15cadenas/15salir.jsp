<%@ page language="java" import="oracle.security.sso.enabler.*" %>
<jsp:useBean id="ssoObj" scope="application" class="com.chermansolutions.oracle.sso.partnerapp.beans.SSOEnablerJspBean" />
<%
	// Set the Expires and Cache Control Headers
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "Thu, 29 Oct 1969 17:04:19 GMT");

	// Set request and response type
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	//Cambios login-logout 20060903.
	//  String urlLogout = (String)session.getAttribute("urlLogout");
	String urlLogout = (String)application.getAttribute("urlLogout");
	String username = (String)session.getAttribute("sesCveUsuario");
	//FIN Cambios login-logout 20060903.
	
	try {
		if (username != null) {
			ssoObj.removeSession(username);
			ssoObj.removeJspAppCookies(request,response);
			System.out.println("Se remueve la cookie de la aplicacion");
		}
		
		System.out.println("Se redirige a: " + urlLogout);
		response.sendRedirect(urlLogout);
	} catch(Exception e) {
		e.printStackTrace();
		System.out.println(e);
		return;
	} finally {
		session.invalidate();
	}
%>  

