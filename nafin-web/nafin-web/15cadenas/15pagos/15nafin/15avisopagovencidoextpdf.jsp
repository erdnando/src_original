<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.exception.NafinException,
		com.netro.pdf.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
System.out.println("15avisosext.data.jsp (E)"); 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String txt_num_prestamo = (request.getParameter("txt_num_prestamo")!=null)?request.getParameter("txt_num_prestamo"):"";
EstadoCuentaPyme BeanEstadoCuenta = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB",EstadoCuentaPyme.class);

JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
List encabezado 	= new ArrayList();
try {

	HashMap avisoDePagoVencido = null;
	// Realizar consulta
	avisoDePagoVencido = BeanEstadoCuenta.getAvisoDePagoVencido(txt_num_prestamo);


	String mensaje1 = "IMPORTANTE: Los montos y cifras que en esta patalla aparecen son una mera indicación del estado de cuenta que presenta la PYME, sin que deban considerarse como definitivas, debido a que puede haber montos correspondientes a capital, intereses, comisiones, gastos, abonos, etc., que pueden no estar reflejados en las cifras que se presentan en esta pantalla, por lo que realizar el pago por las cantidades que ahí se indican no los libera de otros adeudos.";
	String mensaje2 = "EN CASO DE TENER ALGUNA DUDA SOBRE ESTOS DATOS, LA CUENTA O EL PROCEDIMIENTO DE PAGO, CONTACTE TELEFÓNICAMENTE AL C.P. PEDRO MACEDO TEL. 53 25 68 72 O AL LIC. FERNANDO SÁNCHEZ TEL. 53 25 64 81.";
	

	ComunesPDF pdfDoc = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
		
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	java.text.SimpleDateFormat formatoHora2 = new java.text.SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a"); 
	String hoyFecha = (formatoHora2.format(new java.util.Date()));
		
		
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);		
	pdfDoc.addText("Usuario:	"+strNombreUsuario,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText("Fecha:		"+hoyFecha,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);		
						
	pdfDoc.addText("AVISO DE PAGO VENCIDO","formas", ComunesPDF.CENTER);	
	pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
	pdfDoc.addText(mensaje1+"", "formas", ComunesPDF.JUSTIFIED);	
	pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
	
	pdfDoc.setTable(2, 100);
	pdfDoc.setCell("DATOS DEL CLIENTE ","celda01",ComunesPDF.CENTER,2);		
	pdfDoc.setCell("Número de Cliente:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(avisoDePagoVencido.get("COD_CLIENTE").toString()+"","formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Nombre del Cliente: ","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(avisoDePagoVencido.get("NOMBRE").toString()+"","formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Nombre del Programa: ","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(avisoDePagoVencido.get("NOMBRE_DEL_PROGRAMA").toString()+"","formas",ComunesPDF.CENTER);
	
	pdfDoc.setCell("DATOS DEL CRÉDITO","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de Préstamo:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(avisoDePagoVencido.get("NUM_PRESTAMO").toString()+"","formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de Operación: ","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(avisoDePagoVencido.get("FECHA_OPERACION").toString()+"","formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Moneda: ","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(avisoDePagoVencido.get("MONEDA").toString()+"","formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto del Crédito Solicitado: ","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+avisoDePagoVencido.get("MONTO_CREDITO_SOLICITADO").toString()+"","formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de Corte: ","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(avisoDePagoVencido.get("FECHA_CORTE").toString()+"","formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Adeudo Total al Corte: ","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+avisoDePagoVencido.get("SALDO_AL_CORTE").toString()+"","formas",ComunesPDF.CENTER);
	
	
	pdfDoc.setCell("DATOS DE VENCIMIENTOS","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de amortizaciones vencidas: ","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(avisoDePagoVencido.get("NUMERO_AMORTIZACIONES_VENCIDAS").toString()+"","formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de último pago realizado:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(avisoDePagoVencido.get("FECHA_ULTIMO_PAGO").toString()+"","formas",ComunesPDF.CENTER);
	
	pdfDoc.setCell("DATOS PARA DEL PAGO","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Capital a Pagar:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+avisoDePagoVencido.get("CAPITAL_A_PAGAR").toString()+"","formas",ComunesPDF.CENTER);	
	pdfDoc.setCell("Interés a Pagar:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+avisoDePagoVencido.get("INTERES_A_PAGAR").toString()+"","formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Intereses Moratorios a Pagar:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("$"+avisoDePagoVencido.get("INTERESES_MORATORIOS_A_PAGAR").toString()+"","formas",ComunesPDF.CENTER);

	pdfDoc.setCell("MONTO TOTAL DE PAGO:","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("$"+avisoDePagoVencido.get("MONTO_TOTAL").toString()+"","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("("+avisoDePagoVencido.get("MONTO_TOTAL_LEYENDA").toString()+")","celda01",ComunesPDF.CENTER,2);
	
	pdfDoc.setCell("FECHA LÍMITE DE PAGO ","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("INMEDIATO","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(" REFERENCIA DE PAGO","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(avisoDePagoVencido.get("REFERENCIA_PAGO").toString()+"","celda01",ComunesPDF.CENTER);
	pdfDoc.addTable();
	
	pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
	pdfDoc.addText("NOTA:", "formas", ComunesPDF.JUSTIFIED);
	pdfDoc.addText(mensaje2,"formas", ComunesPDF.JUSTIFIED);	
		
	
	

	 




	
	
	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>

<%System.out.println("15avisosext.data.jsp (S)"); %>

