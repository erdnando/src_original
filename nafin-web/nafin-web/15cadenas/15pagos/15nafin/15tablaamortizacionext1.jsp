<%@ page 
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.exception.NafinException,
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%

String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String txt_num_prestamo = (request.getParameter("txt_num_prestamo")!=null)?request.getParameter("txt_num_prestamo"):"";

EstadoCuentaPyme BeanEstadoCuenta = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB",EstadoCuentaPyme.class);

System.out.println("txt_num_prestamo ------->"+txt_num_prestamo);

HashMap tablaAmortizacion  = null;
	
	// Realizar consulta
	tablaAmortizacion  = BeanEstadoCuenta.getDatosTablaAmortizacion(txt_num_prestamo);

	String plazoCredito= tablaAmortizacion.get("PLAZOCREDITO").toString(); 
	String subaplica =tablaAmortizacion.get("SUBAPLICA").toString(); 
	String codigoCliente=tablaAmortizacion.get("CODIGOCLIENTE").toString(); 
	String primerPagoCarp =tablaAmortizacion.get("PRIMERPAGOCAP").toString(); 
	String plazogracia=tablaAmortizacion.get("PLAZOGRACIA").toString(); 
	String primerPagoCom = tablaAmortizacion.get("PRIMERPAGOCOM").toString();
	String fechaDisp = tablaAmortizacion.get("FECHADISP").toString(); 
	String totalpagoCapital = tablaAmortizacion.get("TOTALPAGOCAPITAL").toString(); ///
	String totalMontoMensual = tablaAmortizacion.get("TOTALMONTOMENSUAL").toString(); //
	String diaPago = tablaAmortizacion.get("DIAPAGO").toString();
	String tasaAnual = tablaAmortizacion.get("TASAANUAL").toString()+" %"; 
	String baseOP = tablaAmortizacion.get("BASEOP").toString();  
	String banco = tablaAmortizacion.get("BANCO").toString();   
	String referenciaPago = tablaAmortizacion.get("REFERENCIA_PAGO").toString();    
	String totalPagoInteres = tablaAmortizacion.get("TOTALPAGOINTERES").toString(); //
	String moneda= tablaAmortizacion.get("MONEDA").toString();   
	String montoDispuesto = "$ "+tablaAmortizacion.get("MONTODISPUESTO").toString();  
	String empresa =tablaAmortizacion.get("EMPRESA").toString();    	
	
	ArrayList 	pagos = BeanEstadoCuenta.getCalendarioPagos(txt_num_prestamo);



%>
	

<table width="670">
	<tr>
		<td class="titulos" style="padding-right: 18px; padding-left: 17px; " width="100%">
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
			  <tr>
				 <td width="18%">
					<img src="/nafin/00utils/gif/nafin-tu-brazo-derecho-105x62.gif" width="105" height="62"/>
				 </td>
				 <td width="82%">
					<DIV align="center">
					  <table cellspacing="0" cellpadding="0" border="0" width="402">
						 <tr>
							<td class="titulos" style="padding-right: 18px; padding-left: 17px; ">
							  <span>
								 <p>NACIONAL FINANCIERA S.N.C</p>
							  </span>
							</td>
						 </tr>
						 <tr>
							<td class="titulos" style="padding-right: 18px; padding-left: 17px; "><%=baseOP%>
							</td>
						 </tr>
						 <tr>
							<td class="titulos" style="padding-right: 18px; padding-left: 17px; "><%=subaplica%>							  
							</td>
						 </tr>
					  </table>
					</DIV>
				 </td>
			  </tr>
			</table>
			<hr>
		</td>
	</tr>
	<tr>
		<td height="40">
			<table cellspacing="0" cellpadding="0" border="0">
			  <tr>
				 <td class="formas" style="width: 140px; text-align: left;  padding-left: 2px;">
					<b>Nombre de la Empresa:</b>
				 </td>
				 <td class="formas" style="word-wrap: break-word; padding-right: 2px;"><%=empresa%>				
				 </td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="formas">
			<p><b>Estimado Cliente:</b>
		</td>
	</tr>
	<tr>
		<td class="formas">
		  <P>Por medio del presente nos permitimos informarle que su disposici&oacute;n se ha llevado a cabo de acuerdo a las siguientes condiciones, adem&aacute;s le presentamos la tabla de amortizaci&oacute;n con el prop&oacute;sito de recibir oportunamente su pago.</P>
		</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>Fecha de Disposici&oacute;n:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left"><%=fechaDisp%>  </DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>Monto del Cr&eacute;dito Dispuesto:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left"><%=montoDispuesto%>  </DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>Moneda:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left"><%=moneda%>			  </DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>Tasa de Inter&eacute;s Anual:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left"><%=tasaAnual%></DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>D&iacute;a de Pago:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left"><%=diaPago%> de cada mes </DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>Fecha Primer Pago Intereses:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left"><%=primerPagoCom%>
					   
					  </DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>Fecha Primer Pago Capital:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left"><%=primerPagoCarp%>					    
					  </DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>Plazo de Gracia para Capital:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left"><%=plazogracia%> meses </DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>Plazo Total de Cr&eacute;dito:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left"><%=plazoCredito%>  meses  </DIV>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	</table>	


<tr>
		<td align="center">
		  <table cellspacing="0" cellpadding="0" border="1" align="center">
			<tr>
			<td colspan="6"  height="30" class="titulos" style="background-color: #E3E5E6;" align="center">
			  <DIV align="center">
			    <b>CALENDARIO DE PAGOS</b>
			  </DIV>
			</td>
			</tr>
		    <tr>
		      <td height="40" width="80" class="celda02" style="background-color: #E3E5E6;" align="center">N�mero de Pago</td>
		      <td height="40" width="80" class="celda02" style="background-color: #E3E5E6;" align="center">Fecha de Pago</td>
		      <td height="40" width="80" class="celda02" style="background-color: #E3E5E6;" align="center">Pago de Capital</td>
		      <td height="40" width="80" class="celda02" style="background-color: #E3E5E6;" align="center">Intereses Generados</td>
		      <td height="40" width="80" class="celda02" style="background-color: #E3E5E6;" align="center">Monto Pago Mensual</td>
		      <td height="40" width="80" class="celda02" style="background-color: #E3E5E6;" align="center">Saldo de Capital</td>
		    </tr>
			 
			 	<% 	for( int i=0; i<pagos.size(); i++) {
							HashMap registro = (HashMap) pagos.get(i);											
				%>		
				 <tr>
					<td class="formas" align="center"><%=registro.get("NUMEROPAGO").toString()%>	</td>
					<td class="formas" align="center"><%=registro.get("FECHAPAGO").toString()%></td>
					<td class="formas" align="RIGHT"><%=registro.get("PAGOCAPITAL").toString()%></td>
					<td class="formas" align="RIGHT"><%=registro.get("PAGOINTERES").toString()%></td>
					<td class="formas" align="RIGHT"><%=registro.get("MONTOMENSUAL").toString()%></td>
					<td class="formas" align="RIGHT"><%=registro.get("SALDOMENSUAL").toString()%></td>
				 </tr>
				<%}%>
				<tr>
					<td class="formas" align="right" colspan="2"><b>Totales</b></td>
					<td class="formas">
					  <DIV align="RIGHT">
					    <%=tablaAmortizacion.get("TOTALPAGOCAPITAL").toString()%>							
					  </DIV>
					</td>
					<td class="formas">
					  <DIV align="RIGHT">
						 <%=tablaAmortizacion.get("TOTALPAGOINTERES").toString()%>					    
					  </DIV>
					</td>
					<td class="formas">
					  <DIV align="RIGHT">
						<%=tablaAmortizacion.get("TOTALMONTOMENSUAL").toString()%>					 
					  </DIV>
					</td>
					<td class="formas">&nbsp;</td>
				</tr>
			
		  </table>
			
	<table width="670">
		<tr>
		<td class="formas">
		  <P>*Nota: Esta informaci�n corresponde �nicamente a la disposici�n realiza y no considera otras disposiciones que haya efectuado con anterioridad. Asimismo, es una herramienta de apoyo lo cual es �nicamente para efectos ilustrativos, por lo que la informaci�n obtenida no implica asunci�n de obligaci�n al compromiso por parte del acreditado.</P>
		</td>
	</tr>
	<tr>
		<td>
			<table align="center" width="500">
				<tr>
					<td class="formas" style="background-color: #E3E5E6;" >
					  <DIV align="left">&nbsp;
					  <b>
					    <center>CUENTA BANCARIA PARA QUE NAFIN RECIBA EL DEP�SITO DEL PAGO DE SU CR�DITO </center>
					  </b>&nbsp;</DIV>
					  </td>
				</tr>
				<tr>
					<td class="formas">
					A continuaci&oacute;n le indicamos el n&uacute;mero de Cuenta Bancaria donde usted (MIPYME) deber&aacute; cubrir las obligaciones con NAFIN derivadas del contrato de cr&eacute;dito que tiene celebrado con dicha instituci&oacute;n.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>Beneficiario:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left">Nacional Financiera, S.N.C.</DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>Nombre del Banco:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left">BBVA Bancomer</DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>N&uacute;mero de Convenio:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left">616117</DIV>
					</td>
				</tr>
				<tr>
					<td width="180" class="formas" align="right">
					  <DIV align="left">
					    <b>N&uacute;mero de Referencia:</b>
					  </DIV>
					</td>
					<td width="200" class="formas" align="right">
					  <DIV align="left"><%=referenciaPago%></DIV>
					</td>
				</tr>
			</table>
			<tr>
				<td>
					<table align="center" width="500">
						<tr>
							<td class="formas">
								<P>
								  <EM><b><U>O tambi&eacute;n:</U></b></EM>
								</P>
							</td>
						</tr>
				</td>
			</tr>
			<table>
			  <tr>
			    <td width="180" class="formas" align="right">
			      <DIV align="left">
			        <b>Beneficiario:</b> 
			      </DIV>
			    </td>
			    <td width="200" class="formas" align="right">
			      <DIV align="left">Nacional Financiera, S.N.C.</DIV>
			    </td>
			  </tr>
			  <tr>
			    <td width="180" class="formas" align="right">
			      <DIV align="left">
			        <b>Nombre del Banco:</b> 
			      </DIV>
			    </td>
			    <td width="200" class="formas" align="right">
			      <DIV align="left">Banamex</DIV>
			    </td>
			  </tr>
			  <tr>
			    <td width="180" class="formas" align="right">
			      <DIV align="left">
			        <b>Sucursal:</b> 
			      </DIV>
			    </td>
			    <td width="200" class="formas" align="right">
			      <DIV align="left">870</DIV>
			    </td>
			  </tr>
			  <tr>
			    <td width="180" class="formas" align="right">
			      <DIV align="left"><b>Cuenta: </b></DIV>
			    </td>
			    <td width="200" class="texto" align="right">
			      <DIV align="left">8269 - 6</DIV>
			    </td>
			  </tr>
			  <tr>
			    <td width="180" class="formas" align="right">
			      <DIV align="left">
			        <b>N&uacute;mero de Referencia:</b> 
			      </DIV>
			    </td>
			    <td width="200" class="formas" align="right">
			      <DIV align="left"><%=referenciaPago%>
			      </DIV>
			    </td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table align="center" width="500">
				<tr>
					<td class="celda01">
					&nbsp;<b><center>PAGOS ANTICIPADOS</center></b>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="formas">
					Usted (MIPYME) podr&aacute; realizar pagos anticipados a cuenta del CREDITO, sin premio ni castigo.
					<br/>Deber&aacute; avisar por escrito a NAFIN, con 2(dos) d&iacute;as h&aacute;biles de anticipaci&oacute;n a la fecha en que desea llevar a cabo el pago a NAFIN.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table align="center" width="500">
				<tr>
					<td class="celda01">
					&nbsp;<b><center>VENCIMIENTO EN DIA INHABIL</center></b>&nbsp;
					</td>
				</tr>
				<tr>
					<td class="formas">
					Cualquier pago deber&aacute; realizarse el d�a de su vencimiento o bien, el d&iacute;a h&aacute;bil inmediato siguiente si el d�a de vencimiento resulta inh&aacute;bil, sin que esto le ocasione intereses adicionales.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table align="center" width="500">
				<tr>
					<td class="formas">
					<br/>
					<br/>
					<b>Atentamente</b>
					<br/>Nacional Financiera S.N.C.
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	</table>
		</td>
	</tr>
	</table>


