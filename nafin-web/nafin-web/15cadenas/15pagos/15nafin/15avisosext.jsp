<!DOCTYPE html> 
<%@page contentType="text/html;charset=windows-1252"%>
<%@ page import="java.util.*" errorPage="/00utils/error.jsp"%>
 <script language="JavaScript" src="../../../00utils/valida.js?<%=session.getId()%>"></script>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS) {%>	
	<%@ include file="/01principal/menu.jspf"%>
<%}%>

<script type="text/javascript" src="15avisosext.js?<%=session.getId()%>"></script>

</head>

<%if(esEsquemaExtJS) {%>
	<%if("PYME".equals(strTipoUsuario) ) { %>	
	
		<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
			<%@ include file="/01principal/01pyme/cabeza.jspf"%>
			<div id="_menuApp"></div>
				<div id="Contcentral">	
					<%@ include file="/01principal/01pyme/menuLateralFlotante.jspf"%>	
					<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
				</div>
			</div>
			<%@ include file="/01principal/01pyme/pie.jspf"%>
			<form id='formAux' name="formAux" target='_new'></form>
					
		</body>
		
	
	<% } else if("NAFIN".equals(strTipoUsuario) ) { %>	
	
	
		<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
			<%@ include file="/01principal/01nafin/cabeza.jspf"%>
			<div id="_menuApp"></div>
				<div id="Contcentral">	
					<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
					<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
				</div>
			</div>
			<%@ include file="/01principal/01nafin/pie.jspf"%>
			<form id='formAux' name="formAux" target='_new'></form>
					
		</body>
		
	<%}%>	 
		 
<%}else{%>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<div id='areaContenido' style="margin-left: 3px; margin-top: 3px;"></div>
		<form id='formAux' name="formAux" target='_new'></form>
		
	</body>	
	
<%}%>

</html>