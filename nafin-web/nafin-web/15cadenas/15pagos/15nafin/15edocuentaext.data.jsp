<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.cadenas.EdoCuentaConsolNafinCA,
		com.netro.cadenas.EstadoCuentaPyme,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion	= (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String numPrestamo	= (request.getParameter("ig_numero_prestamo")!=null)?request.getParameter("ig_numero_prestamo"):"";
	String txt_cliente	= (request.getParameter("txt_cliente")!=null)?request.getParameter("txt_cliente"):"";
	String nombreCliente	= (request.getParameter("nombreCliente")!=null)?request.getParameter("nombreCliente"):"";
	
	JSONObject jsonObj = new JSONObject();	 
	
	String consultar ="",  fechaHoy		="",  numeroSIRAC	= "",  RFC	 ="",  calle	= "",  colonia	 =  "", 
	municipio	= "",  ciudad	= "", codigoPostal	= "";
	StringBuffer html1 = new StringBuffer();
	StringBuffer html2 = new StringBuffer();
	
	int start 				= 0;
	int limit 				= 0;
	String infoRegresar	= "";
	HashMap direccionCliente = new HashMap();
	
	try{
		fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	}catch(Exception e){
		fechaHoy = "";
	}
	
	EstadoCuentaPyme estadoCuentaPyme = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB",EstadoCuentaPyme.class);
	List meses = estadoCuentaPyme.getFechasAviso();
	
	EdoCuentaConsolNafinCA paginador = new EdoCuentaConsolNafinCA();
	paginador.setTxt_cliente(numeroSIRAC);
	paginador.setTxt_num_prestamo(numPrestamo);
	paginador.setTxt_cliente(txt_cliente);	
	paginador.setNombreCliente(nombreCliente);	
	
		
		
	if (informacion.equals("valoresIniciales")){
			
		jsonObj = new JSONObject();	 
		jsonObj.put("success",new Boolean(true));		
		jsonObj.put("fechaHoy",fechaHoy);
		jsonObj.put("strTipoUsuario",strTipoUsuario);
		
		infoRegresar = jsonObj.toString();
	}
	else if (informacion.equals("Consulta")||informacion.equals("ArchivoPaginaPDF")||informacion.equals("ArchivoTotalPDF")) {
		
		String tipoUsuario = (String) request.getSession().getAttribute("strTipoUsuario");
		if(tipoUsuario.equals("PYME")) {
			numeroSIRAC = estadoCuentaPyme.getNumeroSIRAC(iNoCliente);
			direccionCliente = estadoCuentaPyme.getDireccionCliente(numeroSIRAC);
			nombreCliente = estadoCuentaPyme.getNombreCliente(numeroSIRAC,numPrestamo);
		}else if(tipoUsuario.equals("NAFIN")) {
			direccionCliente = estadoCuentaPyme.getDireccionCliente(txt_cliente);
		}		
		
		RFC = (((String)direccionCliente.get("RFC"))!=null)?((String)direccionCliente.get("RFC")):"";
		calle = (((String)direccionCliente.get("CALLE"))!=null)?((String)direccionCliente.get("CALLE")):"";
		colonia = (((String)direccionCliente.get("COLONIA"))!=null)?((String)direccionCliente.get("COLONIA")):"";
		municipio = (((String)direccionCliente.get("MUNICIPIO"))!=null)?((String)direccionCliente.get("MUNICIPIO")):"";
		ciudad = (((String)direccionCliente.get("CIUDAD"))!=null)?((String)direccionCliente.get("CIUDAD")):"";
		codigoPostal = (((String)direccionCliente.get("CODIGO_POSTAL"))!=null)?((String)direccionCliente.get("CODIGO_POSTAL")):"";
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if(informacion.equals("Consulta")){
		
			String operacion = (request.getParameter("operacion")== null)?"":request.getParameter("operacion");
			try {
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
					throw new AppException("Error en los parámetros recibidos",e);
			}
			try {
					if (operacion.equals("Generar")) {
						queryHelper.executePKQuery(request);
					}
					
					//consultar = queryHelper.getJSONPageResultSet(request,start,limit);	
					Registros reg = queryHelper.getPageResultSet(request,start,limit);		
					while(reg.next()){
						numeroSIRAC = reg.getString("ig_codigo_cliente");
						nombreCliente = reg.getString("cg_desc_cliente");
					}
					
					consultar =  "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + reg.getJSONData()+"}";
			
						
			} catch(Exception e){
					throw new AppException("Error en la paginación",e);
			}
			
			
			html1.append(			
			"<table width='900'  class='collapse'>"+
			"<tr>"+
				"<td class='titulo' colspan='1' align= 'center'>Estado de Cuenta Consolidado al "+fechaHoy+"</td>"+
			"</tr>"+
			"<tr>"+
				"<td align='justify' colspan='1' class='aviso' style='padding-right: 18px; padding-left: 18px;'>"+
					"<i><font face='Arial' size='1px'>"+
					"Importante: Los montos y cifras que en esta pantalla aparecen son una mera indicación del estado de cuenta "+
					"que le presenta Nacional Financiera, sin que deban considerarse como definitivas, debido a que puede haber "+
					"montos correspondientes a capital, intereses, comisiones, gastos, abonos, etc., que pueden no estar reflejados "+
					"en las cifras que se presentan en esta pantalla, por lo que al realizar el pago por las cantidades que ahí se indican "+
					"no los libera de otros adeudos.</font></i>"+ 
				"</td>"+
			"</tr>"+
			"</table>");
			
			
			html2.append(			
			"<table width='900'  border='1' class='collapse' >"+
			"<tr>"+
				"<td align='left' ><b>Cliente:</b><br>"+	numeroSIRAC+"</td>"+
				"<td align='left' colspan='7' ><b>Nombre:</b><br>"+nombreCliente+"</td>"+
				"<td align='left' colspan='13'  ><b>RFC:</b><br>"+RFC+"</font></td>"+
			"</tr>"+
			
			"<tr>"+	
				"<td align='left'  colspan='8' >"+
					"<b>Domicilio:</b><br>"+
					"<strong>Calle/N&uacute;mero:</strong>&nbsp;"+calle+"<br>"+
					"<strong>Colonia:</strong>&nbsp;"+colonia+"<br>"+
					"<strong>Municipio/Delegaci&oacute;n/Estado:</strong>&nbsp;"+municipio+",&nbsp;"+ciudad+"<br>"+
					"<strong>C&oacute;digo&nbsp;Postal:</strong>&nbsp;"+codigoPostal+"<br>"+"</font>"+
				"</td>"+
				"<td colspan='1' class='resultado' style='padding-top: 6px; vertical-align: text-top;'>"+
					"<font face='Arial' size='2px'><b>Fecha Emisi&oacute;n:</b>&nbsp;"+fechaHoy+"</font>"+
				"</td>"+
			"</tr>"+
			"</table> <br>"); 			
			
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consultar);	
			jsonObj.put("html1",html1.toString());
			jsonObj.put("html2",html2.toString());
			jsonObj.put("nombreCliente",nombreCliente);
			
			infoRegresar = jsonObj.toString();		
		
		} else if (informacion.equals("ArchivoPaginaPDF")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp, "PDF");
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
			}
		} else if (informacion.equals("ArchivoTotalPDF")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
			}
		}	
	} 
	else if (informacion.equals("ResumenTotales")){
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//Toma el valor de sesión
		infoRegresar = queryHelper.getJSONResultCount(request);
	}
%>
<%=infoRegresar%>