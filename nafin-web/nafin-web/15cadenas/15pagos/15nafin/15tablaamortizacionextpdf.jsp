<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.exception.NafinException,
		com.netro.pdf.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
System.out.println("15avisosext.data.jsp (E)"); 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String txt_num_prestamo = (request.getParameter("txt_num_prestamo")!=null)?request.getParameter("txt_num_prestamo"):"";

EstadoCuentaPyme BeanEstadoCuenta = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB",EstadoCuentaPyme.class);

JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
List encabezado 	= new ArrayList();
try {
	HashMap tablaAmortizacion  = null;	
	// Realizar consulta
	tablaAmortizacion  = BeanEstadoCuenta.getDatosTablaAmortizacion(txt_num_prestamo);

	String plazoCredito= tablaAmortizacion.get("PLAZOCREDITO").toString()+" meses"; 
	String subaplica =tablaAmortizacion.get("SUBAPLICA").toString(); 
	String codigoCliente=tablaAmortizacion.get("CODIGOCLIENTE").toString(); 
	String primerPagoCarp =tablaAmortizacion.get("PRIMERPAGOCAP").toString(); 
	String plazogracia=tablaAmortizacion.get("PLAZOGRACIA").toString()+" meses"; 
	String primerPagoCom = tablaAmortizacion.get("PRIMERPAGOCOM").toString();
	String fechaDisp = tablaAmortizacion.get("FECHADISP").toString(); 
	String totalpagoCapital = tablaAmortizacion.get("TOTALPAGOCAPITAL").toString(); ///
	String totalMontoMensual = tablaAmortizacion.get("TOTALMONTOMENSUAL").toString(); //
	String diaPago = tablaAmortizacion.get("DIAPAGO").toString()+" de cada mes";
	String tasaAnual = tablaAmortizacion.get("TASAANUAL").toString()+" %"; 
	String baseOP = tablaAmortizacion.get("BASEOP").toString();  
	String banco = tablaAmortizacion.get("BANCO").toString();   
	String referenciaPago = tablaAmortizacion.get("REFERENCIA_PAGO").toString();    
	String totalPagoInteres = tablaAmortizacion.get("TOTALPAGOINTERES").toString(); //
	String moneda= tablaAmortizacion.get("MONEDA").toString();   
	String montoDispuesto = "$ "+tablaAmortizacion.get("MONTODISPUESTO").toString();  
	String empresa =tablaAmortizacion.get("EMPRESA").toString();    	
	String mensaje1 ="Por medio del presente nos permitimos informarle que su disposición se ha llevado a cabo de acuerdo a las siguientes condiciones, además le presentamos la tabla de amortización con el propósito de recibir oportunamente su pago.";
	String mensaje2 ="*Nota: Esta información corresponde únicamente a la disposición realiza y no considera otras disposiciones que haya efectuado con anterioridad. Asimismo, es una herramienta de apoyo lo cual es únicamente para efectos ilustrativos, por lo que la información obtenida no implica asunción de obligación al compromiso por parte del acreditado";
	String mensaje3 = "A continuación le indicamos el número de Cuenta Bancaria donde usted (MIPYME) deberá cubrir las obligaciones con NAFIN derivadas del contrato de crédito que tiene celebrado con dicha institución.";
	String mensaje4 = " Usted (MIPYME) podrá realizar pagos anticipados a cuenta del CREDITO, sin premio ni castigo.Deberá avisar por escrito a NAFIN, con 2(dos) días hábiles de anticipación a la fecha en que desea llevar a cabo el pago a NAFIN. ";
	String mensaje5 ="Cualquier pago deberá realizarse el día de su vencimiento o bien, el día hábil inmediato siguiente si el día de vencimiento resulta inhábil, sin que esto le ocasione intereses adicionales.";
	
	ComunesPDF pdfDoc = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
		
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	java.text.SimpleDateFormat formatoHora2 = new java.text.SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a"); 
	String hoyFecha = (formatoHora2.format(new java.util.Date()));
		
		
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);		
	pdfDoc.addText("Usuario:	"+strNombreUsuario,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText("Fecha:		"+hoyFecha,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);		
						
	pdfDoc.addText("NACIONAL FINANCIERA, S.N.C.","formas", ComunesPDF.CENTER);	
	pdfDoc.addText(baseOP+"", "formas", ComunesPDF.CENTER);	
	pdfDoc.addText(subaplica+"", "formas", ComunesPDF.CENTER);				
	pdfDoc.addText(" ", "formas", ComunesPDF.CENTER);		
	pdfDoc.addText(" ", "formas", ComunesPDF.CENTER);	
		
	pdfDoc.addText("Nombre de la Empresa: "+empresa+"", "formas", ComunesPDF.LEFT);
	pdfDoc.addText(" ", "formas", ComunesPDF.CENTER);	
	pdfDoc.addText("Estimado Cliente: ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText(mensaje1+"", "formas", ComunesPDF.JUSTIFIED);				
	pdfDoc.addText(" ", "formas", ComunesPDF.CENTER);	
	


	pdfDoc.addText("Fecha de Disposición:  		     	      	     "+fechaDisp+"", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Monto del Crédito Dispuesto:  		       "+montoDispuesto+"", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Moneda: 									                                 "+moneda+"", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Tasa de Interés Anual: 			                 "+tasaAnual+"", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Día de Pago:  							                           "+diaPago+"", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Fecha Primer Pago Intereses:  		     "+primerPagoCom+"", "formas", ComunesPDF.LEFT);		
	pdfDoc.addText("Fecha Primer Pago Capital:             "+primerPagoCarp+"", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Plazo de Gracia para Capital:          "+plazogracia+"", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Plazo Total de Crédito:                    "+plazoCredito+"", "formas", ComunesPDF.LEFT);		
	
	
	pdfDoc.setTable(6, 100);
	pdfDoc.setCell("Numero de Pago ","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de Pago","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Pago de Capital","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Intereses Generados","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto Pago Mensual","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Saldo de Capital","celda01",ComunesPDF.CENTER);
	
	ArrayList 	pagos = BeanEstadoCuenta.getCalendarioPagos(txt_num_prestamo);
	
	for( int i=0; i<pagos.size(); i++) {
	
		HashMap registro = (HashMap) pagos.get(i);	
		
		pdfDoc.setCell(registro.get("NUMEROPAGO").toString(),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(registro.get("FECHAPAGO").toString(),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(registro.get("PAGOCAPITAL").toString(),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell(registro.get("PAGOINTERES").toString(),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell(registro.get("MONTOMENSUAL").toString(),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell(registro.get("SALDOMENSUAL").toString(),"formas",ComunesPDF.RIGHT);
			
	}
	
	//para obtener los totales 
	
	pdfDoc.setCell("Totales","formas",ComunesPDF.CENTER,2);
	pdfDoc.setCell(tablaAmortizacion.get("TOTALPAGOCAPITAL").toString(),"formas",ComunesPDF.RIGHT,1);
	pdfDoc.setCell(tablaAmortizacion.get("TOTALPAGOINTERES").toString(),"formas",ComunesPDF.RIGHT,1);
	pdfDoc.setCell(tablaAmortizacion.get("TOTALMONTOMENSUAL").toString(),"formas",ComunesPDF.RIGHT,1);	
	pdfDoc.setCell("","formas",ComunesPDF.CENTER);	
	pdfDoc.addTable();

	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText(mensaje2, "formas", ComunesPDF.JUSTIFIED);	
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("CUENTA BANCARIA PARA QUE NAFIN RECIBA EL DEPOSITO DEL PAGO DE SU CREDITO", "celda01", ComunesPDF.CENTER);		
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText(mensaje3+"	\n ", "formas", ComunesPDF.JUSTIFIED);	
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("PAGOS ANTICIPADOS", "celda01", ComunesPDF.CENTER);		
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText(mensaje4, "formas", ComunesPDF.JUSTIFIED);		
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("VENCIMIENTO EN DIA INHABIL", "celda01", ComunesPDF.CENTER);
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText(mensaje5, "formas", ComunesPDF.JUSTIFIED);		
	
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	
	pdfDoc.addText("Beneficiario:           					      	Nacional Financiera, S.N.C.", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Nombre del Banco::       		  BBVA Bancomer ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Número de Convenio:       	616117", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Número de Referencia:      "+referenciaPago+"", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);		
	pdfDoc.addText("O también:  ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);		
	pdfDoc.addText("Beneficiario:                       Nacional Financiera, S.N.C.", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Nombre del Banco:            Banamex", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Sucursal:                            870", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Cuenta:                              8269 - 6", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Número de Referencia:     "+referenciaPago+"", "formas", ComunesPDF.LEFT);	
	
	
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	
	pdfDoc.addText(" PAGOS ANTICIPADOS ", "celda01", ComunesPDF.CENTER);	
	pdfDoc.addText(" Usted (MI PYME) podrá realizar pagos anticipados a cuenta del CREDITO, sin premio ni castigo.\n Deberá avisar por escrito a NAFIN, con 2(dos) días hábiles de anticipación a la fecha en que desea llevar a cabo el pago a NAFIN.    ", "formas", ComunesPDF.JUSTIFIED);	
	
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText(" VENCIMIENTO EN DIA INHABIL", "celda01", ComunesPDF.CENTER);	
	pdfDoc.addText(" Cualquier pago deberá realizarse el día de su vencimiento o bien, el día hábil inmediato siguiente si el día de vencimiento resulta inhábil, sin que esto le ocasione intereses adicionales.", "formas", ComunesPDF.JUSTIFIED);	
	
	pdfDoc.addText("     ", "formas", ComunesPDF.LEFT);	
	pdfDoc.addText("Atentamente", "celda01", ComunesPDF.LEFT);	
	pdfDoc.addText("Nacional Financiera S.N.C.", "formas", ComunesPDF.LEFT);	
	
	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>

<%System.out.println("15avisosext.data.jsp (S)"); %>

