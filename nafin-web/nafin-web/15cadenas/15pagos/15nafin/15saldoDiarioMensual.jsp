<!DOCTYPE html> 
<%@page contentType="text/html;charset=windows-1252"%>
<%@ page import="java.util.*" errorPage="/00utils/error.jsp"%>
 <script language="JavaScript" src="../../../00utils/valida.js?<%=session.getId()%>"></script>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession.jspf" %>
<html>
<head>
<title>Nafinet</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS) {%>	
	<%@ include file="/01principal/menu.jspf"%>
<%}%>

<script type="text/javascript" src="15saldoDiarioMensual.js?<%=session.getId()%>"></script>

</head>

<%if(esEsquemaExtJS) {%>
	<% if("NAFIN".equals(strTipoUsuario) ) { %>	
		<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
			<%@ include file="/01principal/01nafin/cabeza.jspf"%>
			<div id="_menuApp"></div>
				<div id="Contcentral">	
					<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
					<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
				</div>
			</div>
			
		
			<%@ include file="/01principal/01nafin/pie.jspf"%>
			<form id='formAux' name="formAux" target='_new'></form>
			<form id='formParametros' name="formParametros">
			<input type="text" id="auxStrTipoUsuario" name="auxStrTipoUsuario" value="<%=strTipoUsuario%>"/>
      <input type="hidden" id="strPerfil" name="strPerfil" value="<%=strPerfil%>"/>	
		</body>
	<% } else if("IF".equals(strTipoUsuario ) || "CLIENTE".equals(strTipoUsuario )) { %>	
		<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
			<%@ include file="/01principal/01if/cabeza.jspf"%>
			<div id="_menuApp"></div>
				<div id="Contcentral">	
					<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>	
					<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
				</div>
			</div>
			<%@ include file="/01principal/01if/pie.jspf"%>
			<form id='formAux' name="formAux" target='_new'></form>
			<form id='formParametros' name="formParametros">
			<input type="hidden" id="auxStrTipoUsuario" name="auxStrTipoUsuario" value="<%=strTipoUsuario%>"/>
			<input type="hidden" id="auxINoCliente" name="auxINoCliente" value="<%=iNoCliente%>"/>
      <input type="hidden" id="strPerfil" name="strPerfil" value="<%=strPerfil%>"/>	
		</body>
	<%}%>	
		 
<%}else{%>
	<% if("NAFIN".equals(strTipoUsuario) ) { %>	
		<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<div id='areaContenido' style="margin-left: 3px; margin-top: 3px;"></div>
		<form id='formAux' name="formAux" target='_new'></form>
		<form id='formParametros' name="formParametros">
			<input type="hidden" id="auxStrTipoUsuario" name="auxStrTipoUsuario" value="<%=strTipoUsuario%>"/>
	</body>	
	<% } else if("IF".equals(strTipoUsuario) ) { %>	
		<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<div id='areaContenido' style="margin-left: 3px; margin-top: 3px;"></div>
		<form id='formAux' name="formAux" target='_new'></form>
		<form id='formParametros' name="formParametros">
			<input type="hidden" id="auxStrTipoUsuario" name="auxStrTipoUsuario" value="<%=strTipoUsuario%>"/>
			<input type="hidden" id="auxINoCliente" name="auxINoCliente" value="<%=iNoCliente%>"/>
	</body>	
	<%}%>	
	
<%}%>

</html>