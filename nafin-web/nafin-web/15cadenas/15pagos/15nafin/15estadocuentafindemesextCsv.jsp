<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.exception.NafinException,
		com.netro.pdf.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
System.out.println("15avisosext.data.jsp (E)"); 
String fecha = (request.getParameter("fecha")!=null)?request.getParameter("fecha"):"";
String txt_cliente = (request.getParameter("txt_cliente")!=null)?request.getParameter("txt_cliente"):"";
String txt_num_prestamo = (request.getParameter("txt_num_prestamo")!=null)?request.getParameter("txt_num_prestamo"):"";
String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";

JSONObject jsonObj = new JSONObject();


try {

	String 		nombreArchivo 	= null;	
	StringBuffer contenidoArchivo = new StringBuffer();
	CreaArchivo archivo 				= null;
	archivo 			         = new CreaArchivo();
		
	HashMap estadoCuentaFinDeMes = null;
	HashMap direccionCliente = null;
	ArrayList disposicion= new ArrayList();
	ArrayList recuperado= new ArrayList();
	ArrayList pie = new ArrayList();
	
	EstadoCuentaPyme estadoCuentaPyme = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB",EstadoCuentaPyme.class);
	
	//Establecer la fecha del ultimo dia de los ultimos 3 meses
	List fechasUltimoDiaFinMes 	= estadoCuentaPyme.getFechasAviso();

	
	if (fechasUltimoDiaFinMes != null ){
		List fechaUltimoDia 				= (List)fechasUltimoDiaFinMes.get(1);				
		if(mes.equals("ultimo")){
			fecha = fechaUltimoDia.get(2).toString();
		}
			
		fechaUltimoDia 					= (List)fechasUltimoDiaFinMes.get(2);				
		if(mes.equals("penultimo")){
			fecha = fechaUltimoDia.get(2).toString();
		}
		fechaUltimoDia 					= (List)fechasUltimoDiaFinMes.get(3);
		if(mes.equals("antepenultimo")){
			fecha =fechaUltimoDia.get(2).toString();
		}
	}
		
	System.out.println("fecha "+fecha );		
	System.out.println("txt_cliente "+txt_cliente );		
	System.out.println("txt_num_prestamo "+txt_num_prestamo );
		
	// Realizar consulta
	estadoCuentaFinDeMes = estadoCuentaPyme.getEstadoCuentaFinDeMes(txt_cliente, fecha);
	
	//Obtener los pie de pagina
	pie = estadoCuentaPyme.getPieEstadoCuenta();
	
	//Obtener la direccion del cliente
	direccionCliente = estadoCuentaPyme.getDireccionCliente(txt_cliente);
	
 //Obtener la(s) disposicion(es)
	disposicion = estadoCuentaPyme.getDisposicionEstadoCuenta(txt_num_prestamo, txt_cliente, fecha);
	
	// Obtener la(s) disposicion(es)
	recuperado = estadoCuentaPyme.getRecuperadoEstadoCuenta(txt_num_prestamo, txt_cliente, fecha);
	
	

			contenidoArchivo.append(",,NACIONAL FINANCIERA S.N.CNACIONAL FINANCIERA S.N.C.\n");
			contenidoArchivo.append(",,Registro Federal de Causantes: NFI-340630-5TO\n");
			contenidoArchivo.append(",,Avenida Insurgentes Sur 1971 Colonia Guadalupe Inn\n");
			contenidoArchivo.append(",,Código Postal 01020 México D.F. teléfono 53 25 60 00\n,\n,,ESTADO DE CUENTA POR PRESTAMO\n");
			
			//datos generales
			contenidoArchivo.append("Cliente,"+(String)estadoCuentaFinDeMes.get("CODIG_CLIENTE")+"\n");
			contenidoArchivo.append("Nombre,"+(String)estadoCuentaFinDeMes.get("NOMBRE_RAZON_SOCIAL")+"\n");
			contenidoArchivo.append("R.F.C,"+(String)direccionCliente.get("RFC")+"\n");
			contenidoArchivo.append("Fecha Emisión,"+(String)estadoCuentaFinDeMes.get("FECHA_EMISION")+"\nDIRECCION\n");
			contenidoArchivo.append(",Calle/Número,"+(String)direccionCliente.get("CALLE")+"\n");
			contenidoArchivo.append(",Colonia,"+(String)direccionCliente.get("COLONIA")+"\n");
			contenidoArchivo.append(",Municipio/Delegación/Estado,"+(String)direccionCliente.get("MUNICIPIO")+" - "+(String)direccionCliente.get("CIUDAD")+"\n");
			contenidoArchivo.append(",Código Postal,"+(String)direccionCliente.get("CODIGO_POSTAL")+"\n\n");
			
			//detalle
			contenidoArchivo.append("\n,\nDETALLE\n");
			contenidoArchivo.append("Saldo Inicial Capital,Monto Dispuesto,Monto Recuperado Capital,Saldo Insoluto Capital,Total Comisiones Cobradas\n");
			contenidoArchivo.append("$"+((String)estadoCuentaFinDeMes.get("SALDO_ANTERIOR")).replace(',',' ')+",");
			contenidoArchivo.append("$"+((String)estadoCuentaFinDeMes.get("MONTO_OPERADO")).replace(',',' ')+",");
			contenidoArchivo.append("$"+((String)estadoCuentaFinDeMes.get("CAPITAL_RECUPERADO")).replace(',',' ')+",");
			contenidoArchivo.append("$"+((String)estadoCuentaFinDeMes.get("SALDO_INSOLUTO")).replace(',',' ')+",");
			contenidoArchivo.append("$"+((String)estadoCuentaFinDeMes.get("COMISION")).replace(',',' ')+"\n\n,");
			
			contenidoArchivo.append("\nMovimientos del "+(String)estadoCuentaFinDeMes.get("PERIODO")+"\n,");
			
			//disposiciones
			if(disposicion.size()>0)
			{
				contenidoArchivo.append("\nDISPOSICIONES\n");
				Iterator disp = disposicion.iterator();
				while(disp.hasNext()){
					HashMap hashdisp = new HashMap();
					hashdisp = (HashMap)disp.next();
					contenidoArchivo.append("Número de Préstamo,");
					contenidoArchivo.append("Fecha de Disposición,");
					contenidoArchivo.append("Monto Dispuesto,");
					contenidoArchivo.append("Monto Total Depositado,\n");
					contenidoArchivo.append(((String)hashdisp.get("NUM_PRESTAMO")).replace(',',' ')+",");
					contenidoArchivo.append(((String)hashdisp.get("FECHA_DISPOSICION")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashdisp.get("MONTO_DISPUESTO")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashdisp.get("MONTO_TOTAL")).replace(',',' ')+",");
							
					//comisiones
					contenidoArchivo.append("\nComisiones\n");
					
					contenidoArchivo.append("Descripción de Comisión,");
					contenidoArchivo.append("Fecha de Pago Comisión,");
					contenidoArchivo.append("Monto de la Línea Autorizada,");
					contenidoArchivo.append("Porcentaje Comisión,");	
					contenidoArchivo.append("Monto Comisión Pagada,");
					contenidoArchivo.append("IVA,");
					contenidoArchivo.append("Total Pagado\n");
					
					contenidoArchivo.append(((String)hashdisp.get("DESCRIPCION_COM")).replace(',',' ')+",");
					contenidoArchivo.append(((String)hashdisp.get("FECHA_PAGO_COMISION")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashdisp.get("IMPORTE_LINEA")).replace(',',' ')+",");
					contenidoArchivo.append(((String)hashdisp.get("PORC_COMISION")).replace(',',' ')+" %"+",");
					contenidoArchivo.append("$"+((String)hashdisp.get("IMPORTE_COMISION")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashdisp.get("IVA")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashdisp.get("TOTAL_PAGADO")).replace(',',' ')+",\n");
				}//while(disp.hasNext()){			
			}//if(disposicion.size()>0)
			
			//recuperado
			if(recuperado.size()>0)
			{
			contenidoArchivo.append(",\nRECUPERADO\n");
				
				Iterator recu = recuperado.iterator();
				while(recu.hasNext()){
					HashMap hashrecu = new HashMap();
					hashrecu = (HashMap)recu.next();

					contenidoArchivo.append("Número de Préstamo,");
					contenidoArchivo.append("Fecha de Pago,");
					contenidoArchivo.append("Capital,");
					contenidoArchivo.append("Prepagos Capital,");
					contenidoArchivo.append("Interés Ordinario,");
					contenidoArchivo.append("Interés Moratorio,");
					contenidoArchivo.append("Comisión por Prepago,");
					contenidoArchivo.append("IVA,");
					contenidoArchivo.append("Subsidio,");
					contenidoArchivo.append("Total Pagado\n");
					contenidoArchivo.append(((String)hashrecu.get("NUM_PRESTAMO")).replace(',',' ')+",");
					contenidoArchivo.append(((String)hashrecu.get("FECHA_PAGO")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashrecu.get("CAPITAL")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashrecu.get("PREPAGO_CAPITAL")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashrecu.get("INT_ORDINARIO")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashrecu.get("INT_MORATORIO")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashrecu.get("COMISIONES")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashrecu.get("IVA")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashrecu.get("SUBSIDIO")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashrecu.get("TOTAL_PAGADO")).replace(',',' ')+",");

					contenidoArchivo.append("\nImporte para Cálculo Interés Ordinario,");
					contenidoArchivo.append("Importe para Cálculo Interés Moratorio,");
					contenidoArchivo.append("Tasa Interés Ordinaria Anual,");
					contenidoArchivo.append("Esquema tasa Ordinaria,");
					contenidoArchivo.append("Tasa Interés Moratoria Anual,");
					contenidoArchivo.append("Esquema tasa Moratoria,");
					contenidoArchivo.append("Pagos en cartera vencida,");
					contenidoArchivo.append("Pagos por emitir\n");
					contenidoArchivo.append("$"+((String)hashrecu.get("CAPITAL_VIGENTE")).replace(',',' ')+",");
					contenidoArchivo.append("$"+((String)hashrecu.get("CAPITAL_VENCIDO")).replace(',',' ')+",");
					contenidoArchivo.append(((String)hashrecu.get("TASA_TOTAL")).replace(',',' ')+",");
					contenidoArchivo.append(((String)hashrecu.get("DESCRIPCION_TASA")).replace(',',' ')+",");
					contenidoArchivo.append(((String)hashrecu.get("TASA_MORATORIA")).replace(',',' ')+",");
					contenidoArchivo.append(((String)hashrecu.get("DESCRIPCION_MORATORIA")).replace(',',' ')+",");
					contenidoArchivo.append(((String)hashrecu.get("CUOTAS_VENCIDAS")).replace(',',' ')+",");
					contenidoArchivo.append(((String)hashrecu.get("CUOTAS_POR_EMITIR")).replace(',',' ')+"\n");
				}
			}//if(recuperado.size()>0)
			
			if(pie.size()>0)
			{	
				contenidoArchivo.append(",\nNOTAS:");
				Iterator p = pie.iterator();
				while(p.hasNext()){
					HashMap hashpie = new HashMap();
					hashpie = (HashMap)p.next();
					contenidoArchivo.append("\n"+"*"+((String)hashpie.get("TEXTO")).replace(',',' '));
				}
			}
			

	if(estadoCuentaFinDeMes.size()>=0){
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>

<%System.out.println("15avisosext.data.jsp (S)"); %>

