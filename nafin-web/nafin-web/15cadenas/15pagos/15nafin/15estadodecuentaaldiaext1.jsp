<%@ page 
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.exception.NafinException,
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<html:html>
<head> 
	<title>Nafi@net</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<style type="text/css" media="screen" >
		.titulo 				{  font-family: Arial, Helvetica, sans-serif; font-size: 15px; font-weight: bold; text-align: left; color:  #000000;  }
			.aviso 				{  font-family: Arial, Helvetica, sans-serif; font-size: 8px; font-weight: normal; font-style: italic; color:  #000000; text-align: justify; text-decoration: none; padding-right: 2px; padding-left: 2px; }
			table.collapse 	{  border-collapse:collapse;  border: 2px solid #000000; }
			td.resultado 		{  border: 2px solid #000000; padding:3px; font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; color:  #000000; text-decoration: none;  }
			.texto 				{  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; color:  #000000; text-decoration: none;  }
			.textoEnNegritas 	{  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:  #000000; text-decoration: none;  }
			.rojo 				{  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:  #FF0000; text-decoration: none;  }
			.invertido 			{  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:  #FFFFFF; background-color: #000000; text-decoration: none;   }
			.celdaEnNegritas  {  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:  #000000; text-decoration: none; padding: 0px; margin: 0px; border:  0px;  }
			.celdaNormal		{  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; color:  #000000; text-decoration: none; padding: 0px; margin: 0px; border: 0px;}
			.navigation       {}
			
		</style>
		<style type="text/css" media="print" >
			.titulo 				{  font-family: Arial, Helvetica, sans-serif; font-size: 15px; font-weight: bold; text-align: left; color:  #000000;  }
			.aviso 				{  font-family: Arial, Helvetica, sans-serif; font-size: 8px; font-weight: normal; font-style: italic; color:  #000000; text-align: justify; text-decoration: none; padding-right: 2px; padding-left: 2px; }
			table.collapse 	{  border-collapse:collapse;  border: 2px solid #000000; }
			td.resultado 		{  border: 2px solid #000000; padding:3px; font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; color:  #000000; text-decoration: none;  }
			.texto 				{  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; color:  #000000; text-decoration: none;  }
			.textoEnNegritas 	{  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:  #000000; text-decoration: none;  }
			.rojo 				{  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:  #FF0000; text-decoration: none;  }
			.invertido 			{  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:  #FFFFFF; background-color: #000000; text-decoration: none;   }
			.celdaEnNegritas  {  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:  #000000; text-decoration: none; padding: 0px; margin: 0px; border:  0px;  }
			.celdaNormal		{  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; color:  #000000; text-decoration: none; padding: 0px; margin: 0px; border: 0px;}
			.navigation       { 	display: none; }
			
		</style>
		<link rel="stylesheet" href="<%=strDirecVirtualCSS%>css/<%=strClase%>">
	</head>
	<body>
<%
String txt_cliente = (request.getParameter("txt_cliente")!=null)?request.getParameter("txt_cliente"):"";
String txt_num_prestamo = (request.getParameter("txt_num_prestamo")!=null)?request.getParameter("txt_num_prestamo"):"";

HashMap estadoDeCuenta = null;
EstadoCuentaPyme estadoCuentaPyme = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB",EstadoCuentaPyme.class);

// Realizar consulta
estadoDeCuenta = estadoCuentaPyme.getEstadoDeCuentaAlDia(txt_num_prestamo,txt_cliente);
	
	
%>

	<%if(estadoDeCuenta.size()>0){%>
		<table align="center" width="670">
			<tr>
				<td class="titulos" style="padding-right: 18px; padding-left: 17px; " width="100%"><img src="/nafin/00utils/gif/nafin-tu-brazo-derecho-105x62.gif"  width="105" height="62" >
				<span  style="width: 69px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Estado de Cuenta al D&iacute;a 
				<%=estadoDeCuenta.get("FECHA_EMISION").toString()%>
				</td>
			</tr>
			<tr>
				<td class="formas" style="padding-JUSTIFIED: 18px; padding-JUSTIFIED: 18px; ">
					Importante: Los montos y cifras que en esta pantalla aparecen son una mera indicaci�n del estado de cuenta que le presenta Nacional Financiera, sin que deban considerarse como definitivas, debido a que puede haber montos correspondientes a capital, intereses, comisiones, gastos, abonos, etc., que pueden no estar reflejados en las cifras que se presentan en esta pantalla, por lo que al realizar el pago por las cantidades que ah� se indican no los libera de otros adeudos.
				</td>
			</tr>
			<tr>
				<td>
					<table class="formas"  bordercolor="#000000" border="1"  >
						<tr>
							<td width="120"  class="formas" style="vertical-align: text-top;">
								<b>Cliente:</b><br>
								<%=estadoDeCuenta.get("COD_CLIENTE").toString()%>							
							</td>
							<td width="350"  class="formas" style="word-wrap: break-word; padding-right: 2px;"  >
								<b>Nombre:</b><br>
								<span style="width: 20px;">&nbsp;</span><%=estadoDeCuenta.get("NOMBRE").toString()%>									
							</td>
							<td   class="formas" style="vertical-align: text-top;">
								<b>RFC:</b><br>
								<span style="width: 20px;">&nbsp;</span>
								<%=estadoDeCuenta.get("RFC").toString()%>									
							</td>
						</tr>
						<tr>	
							<td colspan="2" class="formas" style="padding-top: 6px; vertical-align: text-top; word-wrap: break-word;" >
								<b>Domicilio:</b><br>
								 <strong>Calle/N&uacute;mero:</strong>
								 <%=estadoDeCuenta.get("CALLE").toString()%>
								 <br>
								 <strong>Colonia:</strong>								
								<%=estadoDeCuenta.get("COLONIA").toString()%>	<br>
								<strong>Municipio/Delegaci&oacute;n/Estado:</strong>
								<%=estadoDeCuenta.get("MUNICIPIO").toString()%>,&nbsp;
								<%=estadoDeCuenta.get("CIUDAD").toString()%>						
								<br/>
							  <strong>C&oacute;digo&nbsp;Postal:</strong>								
								<%=estadoDeCuenta.get("CODIGO_POSTAL").toString()%>
								<br>
							</td>
							<td class="formas" style="padding-top: 6px; vertical-align: text-top;">
								<b>Fecha Emisi&oacute;n:</b><span style="width: 20px;">&nbsp;</span>
								<%=estadoDeCuenta.get("FECHA_EMISION").toString()%>
								<br>
							</td>
						</tr>
						<tr>
							<td class="formas" bgcolor="black" style="text-align: center;font-size: 12px;" colspan="3">><font color="white">PR�STAMO:<%=estadoDeCuenta.get("NUM_PRESTAMO").toString()%></font></td>
						</tr>
						<tr>
							<td colspan="2" style="padding-top: 6px; vertical-align: text-top;">
								<table  style="border-collapse:collapse;">
								<tr>
									<td class="formas" style="width: 140px; text-align: left;  padding-left: 2px;" >Monto Inicial</td>
									<td class="formas" style="width: 9px;   text-align: right;  border-left: 2px solid white; " >$</td>
									<td class="formas" style="width: <%=estadoDeCuenta.get("LONG_CELDA").toString()%> text-align: right; ">									
									<%=estadoDeCuenta.get("MONTO_INICIAL").toString()%>									
									</td>
									<td class="formas">&nbsp;</td>
								</tr>
								<tr>
									<td class="formas" style="width: 140px; text-align: left;  padding-left: 2px;" >Fecha de Operaci�n:</td>
									<td class="formas" style="width: 9px;   text-align: right;" >&nbsp;</td>
									<td class="formas" 	style="width: "<%=estadoDeCuenta.get("LONG_CELDA").toString()%>"text-align: right;" >									
									<%=estadoDeCuenta.get("FECHA_OPERACION").toString()%>	
									</td>
								</tr>
								<tr>
									<td  class="formas">&nbsp;</td>
								</tr>
								<tr>
									<td class="formas" style="width: 140px; text-align: left; padding-left: 7px; " >
										Saldo Insoluto de<br>
										Capital a la Fecha
									</td>
									<td class="formas" style="width: 9px;   text-align: right;  border-left: 2px solid white; " >$</td>
									<td class="formas" 	style="width: <%=estadoDeCuenta.get("LONG_CELDA").toString()%>	text-align: right;" >									
										<%=estadoDeCuenta.get("SALDO_INSOLUTO").toString()%>	
									</td>
								</tr>
								<tr>
									<td class="formas" style="width: 140px; text-align: left; padding-left: 7px; " >
										Inter�s Normal
									</td>
									<td class="formas" style="width: 9px;   text-align: right;  border-left: 2px solid white; " >$</td>
									<td class="formas" style="width: <%=estadoDeCuenta.get("LONG_CELDA").toString()%>  text-align: right;" >
									<%=estadoDeCuenta.get("INTERES_NORMAL").toString()%>
									</td>
								</tr>
								<tr>
									<td class="formas" style="width: 140px; text-align: left; padding-left: 7px; " >
										Inter�s Moratorio
									</td>
									<td class="formas" style="width: 9px;   text-align: right;  border-left: 2px solid white; " >$</td>
									<td class="formas" style="width: <%=estadoDeCuenta.get("LONG_CELDA").toString()%>  text-align: right;  padding-bottom: 5px;" >
									<%=estadoDeCuenta.get("INTERES_MORATORIO").toString()%>	
									
									</td>
								</tr>
								<tr>
									<td class="formas" style="width: 140px; text-align: left; padding-left: 27px; letter-spacing: 3px;  " >
										Total
									</td>
									<td class="formas" style="width: 9px;   text-align: right;  border-bottom: 2px solid black; border-left: 2px solid black; " >$</td>
									<td class="formas" style="width: <%=estadoDeCuenta.get("LONG_CELDA").toString()%> text-align: right;  border-bottom: 2px solid black; " >
									<%=estadoDeCuenta.get("TOTAL").toString()%>
									</td>
								</tr>
								</table>
							</td>
							<td colspan="1" style="padding-top: 6px; vertical-align: text-top;">
								<table width="100%" style=" border-collapse:collapse;">
									<tr>
									<td style=" border-collapse:collapse; margin: 0px; padding: 0px;">
										<table  style=" border-collapse:collapse;" >
										<tr>
											<td colspan="2" class="formas" style="text-align: left; border-left: 2px solid white; padding-left: 3px;" >Tasa&nbsp;aplicada</td>
										</tr>
										<tr>
											<td class="formas" style="text-align: left; border-bottom: 2px solid black; border-left: 2px solid black; padding-left: 3px;" >
											<%=estadoDeCuenta.get("TASA_APLICADA").toString()%>	&nbsp;&nbsp;</td>
										</tr>
										<tr>
											<td colspan="2" class="formas" style="text-align: left; border-left: 2px solid white; padding-left: 3px;" >Tasa Total</td>
										</tr>
										<tr>
											<td class="formas" style="text-align: left; border-bottom: 2px solid black; border-left: 2px solid black; padding-left: 3px;" >
											<%=estadoDeCuenta.get("TASA_TOTAL").toString()%>%&nbsp;&nbsp;</td>
										</tr>
										<tr>
											<td class="formas" style="text-align: left; " >&nbsp;</td>
										</tr>
										</table>
								   </td>
									</tr>
									<tr>
									<td style=" border-collapse:collapse; margin: 0px; padding: 0px;">
										<table  style=" border-collapse:collapse;" >
										<tr>
											<td class="formas" style="text-align: left;  border-left: 2px solid white;  padding-left: 3px;" colspan="2">Periodicidad de Pago de Capital</td>
										</tr>
										<tr>
											<td class="formas" style="text-align: left; border-bottom: 2px solid black; border-left: 2px solid black;  padding-left: 3px;" >
											<%=estadoDeCuenta.get("PER_PAGO_CAPITAL").toString()%>&nbsp;&nbsp;</td><td  class="formas" style=" width: 90px;">&nbsp;</td>
										</tr>
										<tr>
											<td class="formas" style="text-align: left;  border-left: 2px solid white;  padding-left: 3px;" colspan="2">Periodicidad de Pago de Inter&eacute;s</td>
										</tr>
										<tr>
											<td class="formas" style="text-align: left; border-bottom: 2px solid black; border-left: 2px solid black;  padding-left: 3px;" >
											<%=estadoDeCuenta.get("PER_PAGO_INTERES").toString()%>&nbsp;&nbsp;</td>
										</tr>
										<tr>
											<td class="formas" style="text-align: left; padding-left: 3px; " >&nbsp;</td>
										</tr>
										<tr>
											<td class="formas" style="text-align: left;  border-left: 2px solid white;  padding-left: 3px;" colspan="2">Fecha &Uacute;ltimo Pago:</td>
										</tr>
										<tr>
											<td class="formas" style="text-align: left; border-bottom: 2px solid black; border-left: 2px solid black;  padding-left: 3px;" >
											<%=estadoDeCuenta.get("FECHA_ULTIMO_PAGO").toString()%>	&nbsp;&nbsp;</td>
										</tr>
										</table>
									</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="formas" colspan="3">Nota: Si tiene alguna duda con respecto a este Estado de Cuenta, favor de comunicarse a Nacional Financiera con el C.P. PEDRO MACEDO TEL. 53 25 68 72 O AL LIC. FERNANDO S�NCHEZ TEL. 53 25 64 81.</td>
						</tr>
					</table>
				</td>
			</tr>	
		</table>
		<%}%>
		<%if(estadoDeCuenta.size()==0){%>
		<table bordercolor="#000000"  algin="center" border="1" width="680" align="left">
				<tr>
					<td height="200">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center">
						<table  border="1" cellspacing="0" cellpadding="2" bordercolor="#A5B8BF">
							<tr>
								<td class="formas" align="center" bgcolor="silver"  height="50" width="400" style="text-align:justify;">Ocurri&oacute; un error al obtener el saldo de fin de mes, favor de comunicarse a Nacional Financiera con el C.P. PEDRO MACEDO TEL. 53 25 68 72 O AL LIC. FERNANDO S�NCHEZ TEL. 53 25 64 81</td>
							</tr>
						</table>
					</td>
				</tr>				
			</table>
		<%}%>
		
	</body>
</html:html>
