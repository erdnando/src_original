<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.exception.NafinException,
		com.netro.pdf.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
System.out.println("15avisosext.data.jsp (E)"); 
String fecha = (request.getParameter("fecha")!=null)?request.getParameter("fecha"):"";
String txt_cliente = (request.getParameter("txt_cliente")!=null)?request.getParameter("txt_cliente"):"";
String txt_num_prestamo = (request.getParameter("txt_num_prestamo")!=null)?request.getParameter("txt_num_prestamo"):"";
String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";

JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
List encabezado 	= new ArrayList();

try {

	HashMap estadoCuentaFinDeMes = null;
	HashMap direccionCliente = null;
	ArrayList disposicion= new ArrayList();
	ArrayList recuperado= new ArrayList();
	ArrayList pie = new ArrayList();
	
	EstadoCuentaPyme estadoCuentaPyme = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB",EstadoCuentaPyme.class);
	
	//Establecer la fecha del ultimo dia de los ultimos 3 meses
	List fechasUltimoDiaFinMes 	= estadoCuentaPyme.getFechasAviso();
	
	if (fechasUltimoDiaFinMes != null ){
		List fechaUltimoDia 				= (List)fechasUltimoDiaFinMes.get(1);				
		if(mes.equals("ultimo")){
			fecha = fechaUltimoDia.get(2).toString();
		}
			
		fechaUltimoDia 					= (List)fechasUltimoDiaFinMes.get(2);				
		if(mes.equals("penultimo")){
			fecha = fechaUltimoDia.get(2).toString();
		}
		fechaUltimoDia 					= (List)fechasUltimoDiaFinMes.get(3);
		if(mes.equals("antepenultimo")){
			fecha =fechaUltimoDia.get(2).toString();
		}
	}
	System.out.println("fecha "+fecha );		
	System.out.println("txt_cliente "+txt_cliente );		
	System.out.println("txt_num_prestamo "+txt_num_prestamo );
		
	// Realizar consulta
	estadoCuentaFinDeMes = estadoCuentaPyme.getEstadoCuentaFinDeMes(txt_cliente, fecha);
	
	//Obtener los pie de pagina
	pie = estadoCuentaPyme.getPieEstadoCuenta();
	
	//Obtener la direccion del cliente
	direccionCliente = estadoCuentaPyme.getDireccionCliente(txt_cliente);
	
 //Obtener la(s) disposicion(es)
	disposicion = estadoCuentaPyme.getDisposicionEstadoCuenta(txt_num_prestamo, txt_cliente, fecha);
	
	// Obtener la(s) disposicion(es)
	recuperado = estadoCuentaPyme.getRecuperadoEstadoCuenta(txt_num_prestamo, txt_cliente, fecha);
	
	
	CreaArchivo archivo 			= null;
	ComunesPDF	docPdf1  		= null;
	List encabezadoEdoCuenta 	= new ArrayList();
		

	ComunesPDF docPdf = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
		
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	java.text.SimpleDateFormat formatoHora2 = new java.text.SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a"); 
	String hoyFecha = (formatoHora2.format(new java.util.Date()));
		
		
	docPdf.encabezadoConImagenes(docPdf,(String)session.getAttribute("strPais"),
	((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	docPdf.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					
	archivo 			         = new CreaArchivo();
	nombreArchivo 	        	= archivo.nombreArchivo()+".pdf";
	docPdf 	         		= new ComunesPDF(2,strDirectorioTemp+nombreArchivo,"",false,false,false,true);
	docPdf1						= new ComunesPDF();
	String imagen 				= strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif" ;
	int i							= 0;
	String tipo ="";
	if(!"".equals(txt_num_prestamo) ){
		tipo = "ESTADO DE CUENTA POR PRESTAMO";
	}	else{
		tipo = "ESTADO DE CUENTA";
	}	
			
			String datos = "NACIONAL FINANCIERA, S.N.C." +
            "\nRegistro Federal de Causantes: NFI-340630-5TO" +
            "\nAvenida Insurgentes Sur 1971, Colonia Guadalupe Inn" +
            "\nCódigo Postal 01020, México, D.F., teléfono 53 25 60 00";
			
			String periodo = "Del " + (String)estadoCuentaFinDeMes.get("FECHA_INI") + " Al "+(String)estadoCuentaFinDeMes.get("FECHA_FIN") + 
								"\n" + "Días del Periodo:  "+(String)estadoCuentaFinDeMes.get("DIAS");
								
			String domicilio = (String)(direccionCliente.get("CALLE")==null?"":direccionCliente.get("CALLE")) +
										(String)(direccionCliente.get("COLONIA")==null?"":" COL "+direccionCliente.get("COLONIA")) + 
										(String)(direccionCliente.get("CODIGO_POSTAL")==null?"":" C.P. "+direccionCliente.get("CODIGO_POSTAL")) + 
										(String)(direccionCliente.get("MUNICIPIO")==null?"":", "+direccionCliente.get("MUNICIPIO")) + 
										(String)(direccionCliente.get("CIUDAD")==null?"":", "+direccionCliente.get("CIUDAD"));
			
			encabezadoEdoCuenta.add(tipo);
			encabezadoEdoCuenta.add(periodo);
			encabezadoEdoCuenta.add((String)(estadoCuentaFinDeMes.get("MONEDA")==null?" ":estadoCuentaFinDeMes.get("MONEDA")));
			encabezadoEdoCuenta.add((String)(estadoCuentaFinDeMes.get("CODIG_CLIENTE")==null?" ":estadoCuentaFinDeMes.get("CODIG_CLIENTE")));
			encabezadoEdoCuenta.add((String)(estadoCuentaFinDeMes.get("NOMBRE_RAZON_SOCIAL")==null?" ":estadoCuentaFinDeMes.get("NOMBRE_RAZON_SOCIAL")));
			encabezadoEdoCuenta.add((String)(direccionCliente.get("RFC")==null?" ":direccionCliente.get("RFC")));
			encabezadoEdoCuenta.add(domicilio);
			
			docPdf.encabezadoPersonalizado(datos,"nafinsa.gif",strDirectorioPublicacion, encabezadoEdoCuenta);
            
			float widths2[] = new float[7];
			for(int j=0;j<7;j++) {
				widths2[j] = 1;
			}//for
			
			//detalles
			docPdf.setTable(7);
			docPdf.setCell("Saldo Inicial Capital","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Monto Dispuesto","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Monto Recuperado Capital","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Saldo Insoluto Capital","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("", "formasrepB", ComunesPDF.CENTER, 2,1,0);
			docPdf.setCell("Total Comisiones Cobradas","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("$"+(String)(estadoCuentaFinDeMes.get("SALDO_ANTERIOR")==null?"":estadoCuentaFinDeMes.get("SALDO_ANTERIOR")),"formasmen",ComunesPDF.CENTER, 1,1,0);
			docPdf.setCell("$"+(String)(estadoCuentaFinDeMes.get("MONTO_OPERADO")==null?"":estadoCuentaFinDeMes.get("MONTO_OPERADO")),"formasmen",ComunesPDF.CENTER, 1,1,0);
			docPdf.setCell("$"+(String)(estadoCuentaFinDeMes.get("CAPITAL_RECUPERADO")==null?"":estadoCuentaFinDeMes.get("CAPITAL_RECUPERADO")),"formasmen",ComunesPDF.CENTER, 1,1,0);
			docPdf.setCell("$"+(String)(estadoCuentaFinDeMes.get("SALDO_INSOLUTO")==null?"":estadoCuentaFinDeMes.get("SALDO_INSOLUTO")),"formasmen",ComunesPDF.CENTER, 1,1,0);
			docPdf.setCell("", "formasrepB", ComunesPDF.CENTER, 2,1,0);
			docPdf.setCell("$"+(String)(estadoCuentaFinDeMes.get("COMISION")==null?"":estadoCuentaFinDeMes.get("COMISION")),"formasmen",ComunesPDF.CENTER, 1,1,0);
			//docPdf.setCell("____________________________________________________________________________________________________________________________________________________________________________\n", "formasrepB", ComunesPDF.LEFT,7,1,0);
			docPdf.addTable();
			
			//docPdf.addText("Movimientos del "+(String)general.get("PERIODO")+"\n","formasrepB",ComunesPDF.LEFT);
			//docPdf.addText("");
				
			docPdf.setTable(7,100,widths2,0);
			docPdf.setCell("Movimientos del "+(String)estadoCuentaFinDeMes.get("PERIODO")+"\n","formasrepB",ComunesPDF.LEFT,7,1,0);
			docPdf.setCell("","formasrepB",ComunesPDF.LEFT,7,1,0);
			docPdf.setCell("DISPOSICIONES","subrayado",ComunesPDF.LEFT,7,1,0);
			docPdf.setCell("Número de Préstamo","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Fecha de Disposición","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Monto Dispuesto","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("", "", ComunesPDF.LEFT, 3,1,0);
			docPdf.setCell("Monto Total Depositado","formasrepB",ComunesPDF.CENTER,1,1,0);
			
			//disposiciones
			if(disposicion.size()>0)
			{
				Iterator disp = disposicion.iterator();
				while(disp.hasNext()){	
					HashMap hashdisp = new HashMap();
					hashdisp = (HashMap)disp.next();
					//docPdf.setTable(7);					
					docPdf.setCell((String)hashdisp.get("NUM_PRESTAMO"),"formasmen",ComunesPDF.CENTER, 1,1,0);
					docPdf.setCell((String)hashdisp.get("FECHA_DISPOSICION"),"formasmen",ComunesPDF.CENTER, 1,1,0);
					docPdf.setCell("$"+(String)hashdisp.get("MONTO_DISPUESTO"),"formasmen",ComunesPDF.CENTER, 1,1,0);
					docPdf.setCell("", "", ComunesPDF.LEFT, 3,1,0);
					docPdf.setCell("$"+(String)hashdisp.get("MONTO_TOTAL"),"formasmen",ComunesPDF.CENTER, 1,1,0);
				}//while(disp.hasNext()){
			}//if(disposicion.size()>0)
			docPdf.addTable();
			//docPdf.setCell("____________________________________________________________________________________________________________________________________________________________________________\n", "formasrepB", ComunesPDF.LEFT,7,1,0);
			docPdf.setTable(7);
			docPdf.setCell("","formasrepB",ComunesPDF.LEFT,7,1,0);
			docPdf.setCell("COMISIONES","subrayado",ComunesPDF.LEFT,7,1,0);
			docPdf.setCell("Descripción de Comisión","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Fecha de Pago Comisión","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Monto de la Línea Autorizada","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Porcentaje Comisión","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Monto Comisión Pagada","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("IVA","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Total Pagado","formasrepB",ComunesPDF.CENTER,1,1,0);
					
			if(disposicion.size()>0){
				Iterator coms = disposicion.iterator();
				while(coms.hasNext()){
					i=i+1;
					HashMap hashdisp = new HashMap();
					hashdisp = (HashMap)coms.next();
					
					docPdf.setCell((String)hashdisp.get("DESCRIPCION_COM"),"formasmen",ComunesPDF.CENTER, 1,1,0);
					docPdf.setCell((String)hashdisp.get("FECHA_PAGO_COMISION"),"formasmen",ComunesPDF.CENTER, 1,1,0);
					docPdf.setCell("$"+(String)hashdisp.get("IMPORTE_LINEA"),"formasmen",ComunesPDF.CENTER, 1,1,0);
					docPdf.setCell((String)hashdisp.get("PORC_COMISION")+" %","formasmen",ComunesPDF.CENTER, 1,1,0);
					docPdf.setCell("$"+(String)hashdisp.get("IMPORTE_COMISION"),"formasmen",ComunesPDF.CENTER, 1,1,0);
					docPdf.setCell("$"+(String)hashdisp.get("IVA"),"formasmen",ComunesPDF.CENTER, 1,1,0);
					docPdf.setCell("$"+(String)hashdisp.get("TOTAL_PAGADO"),"formasmen",ComunesPDF.CENTER, 1,1,0);
				}//while(disp.hasNext()){		
			}//if(disposicion.size()>0)
			//docPdf.setCell("____________________________________________________________________________________________________________________________________________________________________________\n", "formasrepB", ComunesPDF.LEFT,7,1,0);
			docPdf.addTable();
			
			
			float widths3[] = new float[46];
			for(int j=0;j<46;j++) {
				widths3[j] = 1;
			}//for
			
			//recuperado
			docPdf.setTable(46,100,widths3,0);
			docPdf.setCell("","formasrepB",ComunesPDF.LEFT,46,1,0);
			docPdf.setCell("RECUPERADO","subrayado",ComunesPDF.LEFT,46,1,0);
			docPdf.setCell("A","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Número de Préstamo","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Importe p/Cálculo Interés Ordinario","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Importe p/Cálculo Interés Moratorio","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Tasa Interés Ordinaria Anual","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Esquema tasa","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Tasa Interés Moratoria Anual","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Esquema tasa Moratoria","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Pagos en cartera vencida","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Cuotas por emitir","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("B","formasrepB",ComunesPDF.CENTER,1,1,0);
			docPdf.setCell("Fecha de Pago","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Capital","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Prepagos Capital","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Interés Ordinario","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Interés Moratorio","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Comisión por Prepago","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("IVA","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Subsidio","formasrepB",ComunesPDF.CENTER,5,1,0);
			docPdf.setCell("Total Pagado","formasrepB",ComunesPDF.CENTER,5,1,0);
			
			if(recuperado.size()>0)
			{
				Iterator recu = recuperado.iterator();
				while(recu.hasNext()){
					HashMap hashrecu = new HashMap();
					hashrecu = (HashMap)recu.next();
					docPdf.setCell("A","formasmen",ComunesPDF.CENTER,1,1,0);
					docPdf.setCell((String)hashrecu.get("NUM_PRESTAMO"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell("$"+(String)hashrecu.get("CAPITAL_VIGENTE"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell("$"+(String)hashrecu.get("CAPITAL_VENCIDO"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell((String)hashrecu.get("TASA_TOTAL"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell((String)hashrecu.get("DESCRIPCION_TASA"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell((String)hashrecu.get("TASA_MORATORIA"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell((String)hashrecu.get("DESCRIPCION_MORATORIA"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell((String)hashrecu.get("CUOTAS_VENCIDAS"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell((String)hashrecu.get("CUOTAS_POR_EMITIR"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell("B","formasmen",ComunesPDF.CENTER,1,1,0);
					docPdf.setCell((String)hashrecu.get("FECHA_PAGO"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell((String)hashrecu.get("CAPITAL"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell("$"+(String)hashrecu.get("PREPAGO_CAPITAL"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell("$"+(String)hashrecu.get("INT_ORDINARIO"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell("$"+(String)hashrecu.get("INT_MORATORIO"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell("$"+(String)hashrecu.get("COMISIONES"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell("$"+(String)hashrecu.get("IVA"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell("$"+(String)hashrecu.get("SUBSIDIO"),"formasmen",ComunesPDF.CENTER, 5,1,0);
					docPdf.setCell("$"+(String)hashrecu.get("TOTAL_PAGADO"),"formasmen",ComunesPDF.CENTER, 5,1,0);
				}//while(recu.hasNext()){
			}//if(recuperado.size()>0)
			//docPdf.setCell("____________________________________________________________________________________________________________________________________________________________________________\n", "formasrepB", ComunesPDF.LEFT,46,1,0);
			docPdf.addTable();
			
			docPdf.setTable(1);
			docPdf.setCell("","formasmen",ComunesPDF.CENTER,1,1,0);
			docPdf.addTable();
			
			docPdf.addText("","formasmen",ComunesPDF.RIGHT);
			docPdf.addText("NO ES VALIDO COMO COMPROBANTE FISCAL","formasmen",ComunesPDF.RIGHT);
			
			docPdf.setTable(1,100,new float[]{2f},1);
		
			//docPdf.setCell("NOTAS","formasrepB",ComunesPDF.LEFT,1,1,0);
			if(pie.size()>0)
			{	
				Iterator p = pie.iterator();
				while(p.hasNext()){
					HashMap hashpie = new HashMap();
					hashpie = (HashMap)p.next();
					docPdf.setCell("*"+(String)hashpie.get("TEXTO"),"formasmen",ComunesPDF.LEFT,1,1,0);
				}
			}
			docPdf.addTable();	

			// Terminar documento
			docPdf.endDocument();
	
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>

<%System.out.println("15avisosext.data.jsp (S)"); %>

