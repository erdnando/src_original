<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.io.*,
		java.sql.*,
		com.netro.zip.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.descuento.*,
		com.netro.model.catalogos.*,
		com.netro.exception.NafinException,
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String boton = (request.getParameter("tipoBoton")!=null)?request.getParameter("tipoBoton"):"";
String intermediario = (request.getParameter("intermediario")!=null)?request.getParameter("intermediario"):"";
String cliente = (request.getParameter("cliente")!=null)?request.getParameter("cliente"):"";
String infoRegresar ="", consulta ="";
String intermOrig = "";

PagosIFNB  beanPagos = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);

JSONObject jsonObj	=	new JSONObject();
ConsSaldoDiarMensual pagina = new ConsSaldoDiarMensual(); 
if(strTipoUsuario.equals("IF") || strTipoUsuario.equals("CLIENTE")){
  intermOrig = iNoCliente;
  iNoCliente = (!intermediario.equals(""))?request.getParameter("intermediario"):iNoCliente;
	intermediario = pagina.getClaveFinanciera(iNoCliente);
}
System.out.println("iNoCliente  ========== "+iNoCliente);
pagina.setSelecBoton(boton);
pagina.setClaveIf("12".equals(iNoCliente)?"":intermediario);
pagina.setClaveCliente(cliente);
CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pagina);

if(informacion.equals("valoresIniciales")){
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	boolean  diaHabil = pagina.getDiaHabil();

	jsonObj.put("success", Boolean.TRUE);
	System.out.println("diaHabil ---->>   "+Boolean.toString(diaHabil));
	if(diaHabil==true){
	
	 if(strTipoUsuario.equals("IF") || strTipoUsuario.equals("CLIENTE")){
    String numSirac ="";
    if(!"LINEA CREDITO".equals(strPerfil)){
      HashMap hmDataIf = beanPagos.getSiracFinaciera(iNoCliente);
	  numSirac = (hmDataIf.get("NUMERO_SIRAC")==null || ((String)hmDataIf.get("NUMERO_SIRAC")).equals(""))?"0":(String)hmDataIf.get("NUMERO_SIRAC");
      jsonObj.put("IC_FINANCIERA", hmDataIf.get("IC_FINANCIERA"));
      jsonObj.put("NUMERO_SIRAC",numSirac);
    }else{
    System.out.println("iNoCliente ==== sirac line acredito ==" + iNoCliente);
      numSirac = beanPagos.getSiracLineaCredito(iNoCliente);
      jsonObj.put("IC_FINANCIERA","");
      jsonObj.put("NUMERO_SIRAC", ("".equals(numSirac)?"0":numSirac));
    }
   
		String fechaConsulta = "";
		
    if("LINEA CREDITO".equals(strPerfil) || "IF LI".equals(strPerfil) || "IF 4CP".equals(strPerfil) || "IF 5CP".equals(strPerfil) 
		|| "IF 4MIC".equals(strPerfil) || "IF 5MIC".equals(strPerfil)){
      fechaConsulta = pagina.getFechaXcliente(numSirac);
    }else{
      fechaConsulta = pagina.getFecha(intermediario);
    }
    
		if(!fechaConsulta.equals("")){
			String diaTMes    = fechaConsulta.substring(0,2);
			String mesTMes    = meses[Integer.parseInt(fechaConsulta.substring(3,5))-1];
			String anioTMes   = fechaConsulta.substring(6,10);
			String mensaje = "<left><FONT SIZE=2 >La información presentada corresponde al mes de  "+mesTMes+" de "+anioTMes+"<br><br>En caso de cualquier duda o comentario,"+
							"favor de enviarnos un correo electrónico a la siguiente dirección:<br><br><FONT SIZE=3>C.P. PEDRO MACEDO CASTILLO</FONT> <BR>Subdirector de Operaciónes "+
							"de Crédito<br><FONT SIZE=3>tel.(55)53256872</FONT><br>Correo:</FONT><u><b><FONT COLOR=BLUE >pmacedo@nafin.gob.mx</FONT></b></u></left>";
			jsonObj.put("mensaje", mensaje);
			jsonObj.put("fechaConsulta", "Saldos mensuales ("+ fechaConsulta+" ) ");
		}else{
			jsonObj.put("mensaje", "");
			jsonObj.put("fechaConsulta", "");
		}
    
    
    
	 }
	}
	jsonObj.put("diaHabil", Boolean.toString(diaHabil));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("catalogoIF")){
	JSONObject	resultado	= new JSONObject();
	JSONArray jsObjArrayCat = new JSONArray();
	List cat = pagina.catalogoIF();
	jsObjArrayCat = jsObjArrayCat.fromObject(cat);
	resultado.put("registros",jsObjArrayCat.toString() );	
   infoRegresar = resultado.toString();
	
}else if(informacion.equals("SaldoDiario") || informacion.equals("SaldoMensual") ){
	
	String file = "Archivosaldos";
	File fichero = new File(strDirectorioTemp+file);
	if (fichero.exists()){
		boolean borrado = fichero.delete();
		System.err.println("SE BORRO EL ARCHIVO "+file);   
	}
	try{
		String fechaConsulta ="";
		 if(informacion.equals("SaldoMensual")){
			if(strTipoUsuario.equals("IF") || strTipoUsuario.equals("CLIENTE")){
				String numSirac ="";
				if(!"LINEA CREDITO".equals(strPerfil)){
				  HashMap hmDataIf = beanPagos.getSiracFinaciera(intermOrig);
				  numSirac =  (String)hmDataIf.get("NUMERO_SIRAC");
				}else{
				  numSirac = beanPagos.getSiracLineaCredito(intermOrig);
				}
			   
					
				if("LINEA CREDITO".equals(strPerfil) || "IF LI".equals(strPerfil) || "IF 4CP".equals(strPerfil) || "IF 5CP".equals(strPerfil) 
					|| "IF 4MIC".equals(strPerfil) || "IF 5MIC".equals(strPerfil)){
					fechaConsulta = pagina.getFechaXcliente(numSirac);
				}else{
					fechaConsulta = pagina.getFecha(intermOrig);
				}

			}else{
			 fechaConsulta = pagina.getFecha(intermediario);
			}
			
			
			 
			 if(!fechaConsulta.equals("")){
				jsonObj.put("fechaConsulta", "Saldos mensuales ("+ fechaConsulta+" ) ");
			 }else{
				jsonObj.put("fechaConsulta", "");
			}
		 }
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "TXT");
		GenerarArchivoZip zip = new GenerarArchivoZip();
		String directory = strDirectorioPublicacion + "16archivos/cadenas/";
		zip.generandoZip(strDirectorioTemp,directory,nombreArchivo.replaceAll(".txt",""));			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo","16archivos/cadenas/"+nombreArchivo.replaceAll(".txt",".zip"));
		
		jsonObj.put("informacion", informacion);
		infoRegresar = jsonObj.toString();
	}catch(Throwable e) {
		throw new AppException("Error al generar el archivo txt", e);
	}
}
%>
<%=infoRegresar%>

<%System.out.println("15saldoDiarioMensual.data.jsp (S)"); %>

