<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.exception.NafinException,
		com.netro.pdf.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
System.out.println("15avisosext.data.jsp (E)"); 
String txt_cliente = (request.getParameter("txt_cliente")!=null)?request.getParameter("txt_cliente"):"";
String txt_num_prestamo = (request.getParameter("txt_num_prestamo")!=null)?request.getParameter("txt_num_prestamo"):"";

JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
List encabezado 	= new ArrayList();

System.out.println("txt_cliente   "+txt_cliente);
System.out.println("txt_num_prestamo   "+txt_num_prestamo);

try {

HashMap estadoDeCuenta = null;

EstadoCuentaPyme estadoCuentaPyme = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB",EstadoCuentaPyme.class);

// Realizar consulta
estadoDeCuenta = estadoCuentaPyme.getEstadoDeCuentaAlDia(txt_num_prestamo,txt_cliente);
	
	
	CreaArchivo archivo 			= null;	
	List encabezadoEdoCuenta 	= new ArrayList();
		

	ComunesPDF pdfDoc = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
		
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	java.text.SimpleDateFormat formatoHora2 = new java.text.SimpleDateFormat ("dd/MM/yyyy"); 
	String hoyFecha = (formatoHora2.format(new java.util.Date()));
	
		
		
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);		
	pdfDoc.addText("Usuario:	"+strNombreUsuario,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText("Fecha:		"+hoyFecha,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);	
	
	String imagen 				= strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif" ;
	String mensaje1= "Importante: Los montos y cifras que en esta pantalla aparecen son una mera indicación del estado de cuenta que le presenta Nacional Financiera, sin que deban considerarse como definitivas, debido a que puede haber montos correspondientes a capital, intereses, comisiones, gastos, abonos, etc., que pueden no estar reflejados en las cifras que se presentan en esta pantalla, por lo que al realizar el pago por las cantidades que ahí se indican no los libera de otros adeudos.";
	String mensaje2 ="Nota: Si tiene alguna duda con respecto a este Estado de Cuenta, favor de comunicarse a Nacional Financiera con el C.P. PEDRO MACEDO TEL. 53 25 68 72 O AL LIC. FERNANDO SÁNCHEZ TEL. 53 25 64 81.";
	          
		
	pdfDoc.addText("Estado de Cuenta al Día  "+hoyFecha+"","formas", ComunesPDF.CENTER);	
	pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
	pdfDoc.addText(mensaje1+"", "formas", ComunesPDF.JUSTIFIED);	
	pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
		
	pdfDoc.setTable(5, 100);
	pdfDoc.setCell("Cliente ","celda01",ComunesPDF.CENTER);	
	pdfDoc.setCell("Nombre ","celda01",ComunesPDF.CENTER,3);	
	pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER);		
	pdfDoc.setCell(estadoDeCuenta.get("COD_CLIENTE").toString(),"formas",ComunesPDF.CENTER);	
	pdfDoc.setCell(estadoDeCuenta.get("NOMBRE").toString(),"formas",ComunesPDF.CENTER,3);	
	pdfDoc.setCell(estadoDeCuenta.get("RFC").toString(),"formas",ComunesPDF.CENTER);	
	
	pdfDoc.setCell("Domicilio","celda01",ComunesPDF.CENTER,5);	
	pdfDoc.setCell("Calle/Número: ","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Colonia: ","celda01",ComunesPDF.CENTER);	
	pdfDoc.setCell("Municipio/Delegación/Estado: ","celda01",ComunesPDF.CENTER);	
	pdfDoc.setCell("Código Postal: ","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Emisión","celda01",ComunesPDF.CENTER);	
	pdfDoc.setCell(estadoDeCuenta.get("CALLE").toString(),"formas",ComunesPDF.CENTER);
	pdfDoc.setCell(estadoDeCuenta.get("COLONIA").toString(),"formas",ComunesPDF.CENTER);	
	pdfDoc.setCell(estadoDeCuenta.get("CIUDAD").toString(),"formas",ComunesPDF.CENTER);	
	pdfDoc.setCell(estadoDeCuenta.get("CODIGO_POSTAL").toString(),"formas",ComunesPDF.CENTER);
	pdfDoc.setCell(estadoDeCuenta.get("FECHA_EMISION").toString(),"formas",ComunesPDF.CENTER);	
	pdfDoc.addTable();
	
	pdfDoc.setTable(2, 100);
	pdfDoc.setCell("PRÉSTAMO:   "+estadoDeCuenta.get("NUM_PRESTAMO").toString(),"celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Monto Inicial","formas",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell("$"+estadoDeCuenta.get("MONTO_INICIAL").toString(),"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Fecha de Operación","formas",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell(estadoDeCuenta.get("FECHA_OPERACION").toString(),"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Saldo Insoluto de Capital a la Fecha","formas",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell("$"+estadoDeCuenta.get("SALDO_INSOLUTO").toString(),"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Interés Normal","formas",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell("$"+estadoDeCuenta.get("INTERES_NORMAL").toString(),"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Interés Moratorio","formas",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell("$"+estadoDeCuenta.get("INTERES_MORATORIO").toString(),"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Total","celda01",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell("$"+estadoDeCuenta.get("TOTAL").toString(),"formas",ComunesPDF.RIGHT);
		pdfDoc.addTable();	

	pdfDoc.setTable(2, 100);
	pdfDoc.setCell("Tasa aplicada","formas",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell(estadoDeCuenta.get("TASA_APLICADA").toString(),"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Tasa Total","formas",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell(estadoDeCuenta.get("TASA_TOTAL").toString(),"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Periodicidad de Pago de Capital","formas",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell(estadoDeCuenta.get("PER_PAGO_CAPITAL").toString(),"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Periodicidad de Pago de Interés Mensual","formas",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell(estadoDeCuenta.get("PER_PAGO_INTERES").toString(),"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Último Pago","formas",ComunesPDF.JUSTIFIED);
	pdfDoc.setCell(estadoDeCuenta.get("FECHA_ULTIMO_PAGO").toString(),"formas",ComunesPDF.CENTER);
	pdfDoc.setCell(mensaje2,"formas",ComunesPDF.JUSTIFIED,2);
	
	
	
	
	
	
	



	pdfDoc.addTable();	

			// Terminar documento
	pdfDoc.endDocument();
	
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>

<%System.out.println("15avisosext.data.jsp (S)"); %>

