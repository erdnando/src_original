	var texto = [
	'IMPORTANTE: Los montos y cifras que en esta pantalla aparecen son una mera indicaci�n del estado de cuenta que presenta la PYME, ',
	'sin que deban considerarse como definitivas, debido a que puede haber montos correspondientes a capital, intereses, comisiones, ',
	'gastos, abonos, etc., que pueden no estar reflejados en las cifras que se presentan en esta pantalla, por lo que el realizar ',
	'el pago por las cantidades que ah� se indican no los libera de otros adeudos.'	
	];
	
	var busquedaAvanza = ['Utilice el *para b�squeda gen�rica'];
 
	Ext.onReady(function() {
	
	var tipoUsuario;	
	var columnas =10;
	
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
				tipoUsuario= jsonValoresIniciales.TIPOUSUARIO;
				var  peffil= jsonValoresIniciales.PERFIL;						
			}			
			if(tipoUsuario =='NAFIN'){
				if(peffil != 'CLIENTE EXT'){					
					Ext.getCmp('compositefield1').show();
					Ext.getCmp('btnBusqAv').show();		
					Ext.getCmp('txt_cliente1').show();
					columnas = 11;
				}
			}			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//ventana de Estado de Cuentas al Dia
	var procesarEstadoCuentaAlDia = function(grid, rowIndex, colIndex, item, event) {
	var registro = grid.getStore().getAt(rowIndex);
	var txt_num_prestamo = registro.get('IG_NUMERO_PRESTAMO');	
	var txt_cliente = registro.get('IG_CODIGO_CLIENTE');
	var ventana = Ext.getCmp('VerEstadoDia');	
	var btnGenerarPDFA = Ext.getCmp('btnGenerarPDFA');
	var btnBajarPDFA = Ext.getCmp('btnBajarPDFA');				
		
	if (ventana) {	
		btnGenerarPDFA.enable();
		btnBajarPDFA.hide();
		ventana.show();	
	} else {	
		new Ext.Window({
			layout: 'fit',
			width: 800,
			height: 305,			
			id: 'VerEstadoDia',
			closeAction: 'hide',
			items: [					
				estadoCuentaAlDia
			],
			title: 'Estado de Cuenta Al Dia ',
			bbar: {
				xtype: 'toolbar',
				buttons: [	
					'->',
					'-',
					{
						xtype: 'button',
						text: 'Generar PDF',
						id: 'btnGenerarPDFA'											
					},
					'-',
					{
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDFA',
						hidden: true
					}
				]
			}
		}).show();
	}
	var btnGenerar = Ext.getCmp('btnGenerarPDFA');
	btnGenerar.setHandler(
		function(boton, evento) {
		boton.disable();
		boton.setIconClass('loading-indicator');								
			Ext.Ajax.request({
				url: '15estadodecuentaaldiaextPdf.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'ArchivoPDF',
					tipoArchivo:'pdf',
					txt_num_prestamo: txt_num_prestamo,
					txt_cliente: txt_cliente						
				}),
				callback: procesarSuccessFailureGenerarPDFA
			});
		}
	);				
	
	var bodyPanel = Ext.getCmp('estadoCuentaAlDia').body;
	var mgr = bodyPanel.getUpdater();
	mgr.on('failure', 
			function(el, response) {
				x.update('');
				NE.util.mostrarErrorResponse(response);
			});		
		mgr.update({
			url: '15estadodecuentaaldiaext1.jsp',	
			scripts: false,
			params: {
				txt_num_prestamo: txt_num_prestamo,
				txt_cliente:txt_cliente		
			},
			indicatorText: 'Cargando Estado de Cuenta al D�a '
		});	
	}
	
	//Venta de tasas de Amortizaciones
	var procesarVerTasa = function(grid, rowIndex, colIndex, item, event) {
	var registro = grid.getStore().getAt(rowIndex);
	var txt_num_prestamo = registro.get('IG_NUMERO_PRESTAMO');
	var ventana = Ext.getCmp('VerTasa');
	
	var btnGenerarPDFT = Ext.getCmp('btnGenerarPDFT');
	var btnBajarPDFT = Ext.getCmp('btnBajarPDFT');		
	
	if (ventana) {	
		btnGenerarPDFT.enable();
		btnBajarPDFT.hide();
		ventana.show();			
	} else {	
		new Ext.Window({
			layout: 'fit',
			width: 800,
			height: 305,			
			id: 'VerTasa',
			closeAction: 'hide',
			items: [					
				panelT
			],
			title: 'Tabla Amortizaci�n',
				fbar: {
					xtype: 'toolbar',
					buttons: [	
						'->',
						'-',
							{
								xtype: 'button',
								text: 'Generar PDF',
								id: 'btnGenerarPDFT'					
							},
							'-',
							{
								xtype: 'button',
								text: 'Bajar PDF',
								id: 'btnBajarPDFT',
								hidden: true
							}	
						]
					}
					}).show();
			}	
				
			var btnGenerar = Ext.getCmp('btnGenerarPDFT');
			btnGenerar.setHandler(
				function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');				
					Ext.Ajax.request({
						url: '15tablaamortizacionextpdf.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF',
							tipoArchivo:'pdf',
							txt_num_prestamo:txt_num_prestamo
						}),
						callback: procesarSuccessFailureGenerarPDFT
					});
				}
			);
		
				var x = Ext.getCmp('panelT').body;
				var mgr = x.getUpdater();
				mgr.on('failure', 
				function(el, response) {
					x.update('');
					NE.util.mostrarErrorResponse(response);
				});		
				mgr.update({
					url: '15tablaamortizacionext1.jsp',	
					scripts: false,
					params: {
						txt_num_prestamo: txt_num_prestamo							
					},
					indicatorText: 'Cargando Tabla Amortizaci�n '
				});				
	}
	
	//Ventana Aviso de Pago 
		var procesarVerAvisoPago = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var txt_num_prestamo = registro.get('IG_NUMERO_PRESTAMO');
		var ventana = Ext.getCmp('VerAvisoPago');
		var btnGenerarPDFAP = Ext.getCmp('btnGenerarPDFAP');
		var btnBajarPDFAP = Ext.getCmp('btnBajarPDFAP');	
		
		if (ventana) {	
			btnGenerarPDFAP.enable();
			btnBajarPDFAP.hide();			
			ventana.show();
		} else {	
			new Ext.Window({
					layout: 'fit',
					width: 800,
					height: 305,			
					id: 'VerAvisoPago',
					closeAction: 'hide',
					items: [					
						panelAviso
					],
					title: 'Aviso Pago',
					fbar: {
						xtype: 'toolbar',
						buttons: [			
							'->',
							'-',
							{
								xtype: 'button',
								text: 'Generar PDF',
								id: 'btnGenerarPDFAP'						
							},
							'-',
							{
								xtype: 'button',
								text: 'Bajar PDF',
								id: 'btnBajarPDFAP',
								hidden: true
							}	
						]
						}
					}).show();						
			}			
			
			var btnGenerar = Ext.getCmp('btnGenerarPDFAP');
			btnGenerar.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '15avisopagoextpdf.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF',
							tipoArchivo:'pdf',
							txt_num_prestamo: txt_num_prestamo
						}),
						callback: procesarSuccessFailureGenerarPDFAP
					});
				}
			);				
		
				var x = Ext.getCmp('panelAviso').body;
				var mgr = x.getUpdater();
				mgr.on('failure', 
				function(el, response) {
					x.update('');
					NE.util.mostrarErrorResponse(response);
				});		
				mgr.update({
					url: '15avisopagoext1.jsp',	
					scripts: false,
					params: {
						txt_num_prestamo: txt_num_prestamo							
					},
					indicatorText: 'Cargando Aviso Pago '
				});						
	}
	
	
		//ventana Aviso Pago Vencido
		var procesarVerAvisoPagoV = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var txt_num_prestamo = registro.get('IG_NUMERO_PRESTAMO');		
		var ventana = Ext.getCmp('VerAvisoPagoV');
		var btnGenerarPDFAPV = Ext.getCmp('btnGenerarPDFAPV');
		var btnBajarPDFAPV = Ext.getCmp('btnBajarPDFAPV');	
	
		if (ventana) {	
			btnGenerarPDFAPV.enable();
			btnBajarPDFAPV.hide();					
			ventana.show();
		} else {	
			new Ext.Window({
					layout: 'fit',
					width: 800,
					height: 305,			
					id: 'VerAvisoPagoV',
					closeAction: 'hide',
					items: [					
						panelAvisoV
					],
					title: 'Aviso Pago Vencido',
					fbar: {
						xtype: 'toolbar',
						buttons: [			
						'->',
						'-',
							{
								xtype: 'button',
								text: 'Generar PDF',
								id: 'btnGenerarPDFAPV'				
							},
						'-',
						{
							xtype: 'button',
							text: 'Bajar PDF',
							id: 'btnBajarPDFAPV',
							hidden: true
						}	
					]
				}
			}).show();
			}
			
			var btnGenerar = Ext.getCmp('btnGenerarPDFAPV');
			btnGenerar.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '15avisopagovencidoextpdf.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ArchivoPDF',
						tipoArchivo:'pdf',
						txt_num_prestamo: txt_num_prestamo
					}),
					callback: procesarSuccessFailureGenerarPDFAPV
				});
				}
			);		
			
		
				var x = Ext.getCmp('panelAvisoV').body;
				var mgr = x.getUpdater();
				mgr.on('failure', 
				function(el, response) {
					x.update('');
					NE.util.mostrarErrorResponse(response);
				});		
				mgr.update({
					url: '15avisopagovencidoext1.jsp',	
					scripts: false,
					params: {
						txt_num_prestamo: txt_num_prestamo							
					},
					indicatorText: 'Cargando Aviso Pago Vencido '
				});			
	}
	
	//Ventana Ultimo estado de Cuenta 
		var procesarEstadoUltimo = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var txt_num_prestamo = registro.get('IG_NUMERO_PRESTAMO');	
		var txt_cliente = registro.get('IG_CODIGO_CLIENTE');
		var ventana = Ext.getCmp('EstadoCuenta01');
		
		var btnGenerarPDFE1 = Ext.getCmp('btnGenerarPDFE1');
		var btnBajarPDFE1 = Ext.getCmp('btnBajarPDFE1');	
		var btnGenerarCSVE1 = Ext.getCmp('btnGenerarCSVE1');
		var btnBajarCSVE1 = Ext.getCmp('btnBajarCSVE1');	
		
		
		if (ventana) {	
			btnGenerarPDFE1.enable();
			btnBajarPDFE1.hide();	
			
			btnGenerarCSVE1.enable();
			btnBajarCSVE1.hide();	
			
		} else {				
		new Ext.Window({
				layout: 'fit',
				width: 800,
				height: 305,			
				id: 'EstadoCuenta01',
				closeAction: 'hide',
				items: [					
					EstadoCuenta1
				],
				title: 'Estado de Cuenta Ultimo D�a  del Mes',
				fbar: {
					xtype: 'toolbar',
					buttons: [
							'->',
							'-',
							{
								xtype: 'button',
								text: 'Generar PDF',
								id: 'btnGenerarPDFE1'			
							},
							'-',
							{
								xtype: 'button',
								text: 'Bajar PDF',
								id: 'btnBajarPDFE1',
								hidden: true
							},
							'-',
							{
								xtype: 'button',
								text		: 'Generar Archivo',
								tooltip	: 'Generar Archivo ',
								id			: 'btnGenerarCSVE1'
							},
						{
							xtype: 'button',
							text		: 'Bajar Archivo',
							tooltip	: 'Abrir archivo',
							id			: 'btnBajarCSVE1',
							hidden: true, disabled: false
						}
						]
				}
		}).show();	
		}
		
		var btnGenerar = Ext.getCmp('btnGenerarPDFE1');
			btnGenerar.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '15estadocuentafindemesextpdf.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ArchivoPDF',
						tipoArchivo:'pdf',
						txt_num_prestamo: txt_num_prestamo,
						txt_cliente:txt_cliente,
						mes:'ultimo'
					}),
					callback: procesarSuccessFailureGenerarPDFE1
				});
				}
			);		
			
			
			var btnGenerarCSV = Ext.getCmp('btnGenerarCSVE1');
			btnGenerarCSV.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				
				Ext.Ajax.request({
					url: '15estadocuentafindemesextCsv.jsp',
					params: Ext.apply(fp.getForm().getValues(),{									
						txt_num_prestamo: txt_num_prestamo,
						txt_cliente:txt_cliente,
						mes:'ultimo'
					}),
					callback: procesarSuccessFailureGenerarCSVE1
				});
				}
			);		
		
					
		var x = Ext.getCmp('EstadoCuenta1').body;
			var mgr = x.getUpdater();
			mgr.on('failure', 
			function(el, response) {
				x.update('');
				NE.util.mostrarErrorResponse(response);
			});		
			mgr.update({
				url: '15estadocuentafindemes1.jsp',	
				scripts: false,
				params: {
					txt_num_prestamo: txt_num_prestamo,
					txt_cliente:txt_cliente,
					mes:'ultimo'
				},
				indicatorText: 'Cargando Aviso Pago '
			});
	}
		
		//Ventana de penultimo estado de Cuenta 
		var procesarEstadoPenUltimo = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var txt_num_prestamo = registro.get('IG_NUMERO_PRESTAMO');	
		var txt_cliente = registro.get('IG_CODIGO_CLIENTE');
		var ventana = Ext.getCmp('EstadoCuenta02');
		
		var btnGenerarPDFE2 = Ext.getCmp('btnGenerarPDFE2');
		var btnBajarPDFE2 = Ext.getCmp('btnBajarPDFE2');	
		var btnGenerarCSVE2 = Ext.getCmp('btnGenerarCSVE2');
		var btnBajarCSVE2 = Ext.getCmp('btnBajarCSVE2');	
		
		
		if (ventana) {
			btnGenerarPDFE2.enable();
			btnBajarPDFE2.hide();	
			
			btnGenerarCSVE2.enable();
			btnBajarCSVE2.hide();	
			ventana.show();
		} else {
		
		new Ext.Window({
				layout: 'fit',
				width: 800,
				height: 305,			
				id: 'EstadoCuenta02',
				closeAction: 'hide',
				items: [					
					EstadoCuenta2
				],
				title: 'Estado de Cuenta Penultimo D�a del Mes ',
				fbar: {
				xtype: 'toolbar',
				buttons: [	
					'->',
					'-',
					{
						xtype: 'button',
						text: 'Generar PDF',
						id: 'btnGenerarPDFE2'	
					},
					'-',
					{
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDFE2',
						hidden: true
					},
					'-',
				{
					xtype: 'button',
					text		: 'Generar Archivo',
					tooltip	: 'Generar Archivo ',
					id			: 'btnGenerarCSVE2'			
				},
				'-',
				{
					xtype: 'button',
					text		: 'Bajar Archivo',
					tooltip	: 'Abrir archivo',
					id			: 'btnBajarCSVE2',
					hidden: true, disabled: false
				}
			]
				}
		}).show();	
		}
		
		
		var btnGenerar = Ext.getCmp('btnGenerarPDFE2');
			btnGenerar.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '15estadocuentafindemesextpdf.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ArchivoPDF',
						tipoArchivo:'pdf',
						txt_num_prestamo: txt_num_prestamo,
						txt_cliente:txt_cliente,
						mes:'penultimo'
					}),
					callback: procesarSuccessFailureGenerarPDFE2
				});
				}
			);		
			
			
			var btnGenerarCSV = Ext.getCmp('btnGenerarCSVE2');
			btnGenerarCSV.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '15estadocuentafindemesextCsv.jsp',
					params: Ext.apply(fp.getForm().getValues(),{									
						txt_num_prestamo: txt_num_prestamo,
						txt_cliente:txt_cliente,
						mes:'penultimo'
					}),
					callback: procesarSuccessFailureGenerarCSVE2
				});
				}
			);		
					
		
		var x = Ext.getCmp('EstadoCuenta2').body;
			var mgr = x.getUpdater();
			mgr.on('failure', 
			function(el, response) {
				x.update('');
				NE.util.mostrarErrorResponse(response);
			});		
			mgr.update({
				url: '15estadocuentafindemes1.jsp',	
				scripts: false,
				params: {
					txt_num_prestamo: txt_num_prestamo,
					txt_cliente:txt_cliente,
					mes:'penultimo'
				},
				indicatorText: 'Cargando Estado de Cuenta'
			});
			
	}
	
	
	//ventana antePenultimo estado de Cuenta 
	var procesarEstadoAntePenUltimo = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var txt_num_prestamo = registro.get('IG_NUMERO_PRESTAMO');	
		var txt_cliente = registro.get('IG_CODIGO_CLIENTE');
		var ventana = Ext.getCmp('EstadoCuenta03');
		
		var btnGenerarPDFE3 = Ext.getCmp('btnGenerarPDFE3');
		var btnBajarPDFE3 = Ext.getCmp('btnBajarPDFE3');	
		var btnGenerarCSVE3 = Ext.getCmp('btnGenerarCSVE3');
		var btnBajarCSVE3 = Ext.getCmp('btnBajarCSVE3');	
		
		if (ventana) {
		
			btnGenerarPDFE3.enable();
			btnBajarPDFE3.hide();	
			
			btnGenerarCSVE3.enable();
			btnBajarCSVE3.hide();	
		
			ventana.show();
		} else {
		new Ext.Window({
				layout: 'fit',
				width: 800,
				height: 305,			
				id: 'EstadoCuenta03',
				closeAction: 'hide',
				items: [					
					EstadoCuenta3
				],
				title: 'Estado de Cuenta AntePenultimo D�a del Mes ',
				fbar: {
					xtype: 'toolbar',
					buttons: [	
					'->',
					'-',
					{
						xtype: 'button',
						text: 'Generar PDF',
						id: 'btnGenerarPDFE3'			
					},
					'-',
					{
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDFE3',
						hidden: true
					},
					'-',
					{
					xtype: 'button',
					text		: 'Generar Archivo',
					tooltip	: 'Generar Archivo ',
					id			: 'btnGenerarCSVE3'				
					},
					{
					xtype: 'button',
					text		: 'Bajar Archivo',
					tooltip	: 'Abrir archivo',
					id			: 'btnBajarCSVE3',
					hidden: true, disabled: false
				}
				]
			}
		}).show();
		}
		
	
		
			var btnGenerar = Ext.getCmp('btnGenerarPDFE3');
			btnGenerar.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '15estadocuentafindemesextpdf.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ArchivoPDF',
						tipoArchivo:'pdf',
						txt_num_prestamo: txt_num_prestamo,
						txt_cliente:txt_cliente,
						mes:'antepenultimo'
					}),
					callback: procesarSuccessFailureGenerarPDFE3
				});
				}
			);		
			
			
			var btnGenerarCSV = Ext.getCmp('btnGenerarCSVE3');
			btnGenerarCSV.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '15estadocuentafindemesextCsv.jsp',
					params: Ext.apply(fp.getForm().getValues(),{									
						txt_num_prestamo: txt_num_prestamo,
						txt_cliente:txt_cliente,
						mes:'antepenultimo'
					}),
					callback: procesarSuccessFailureGenerarCSVE3
				});
				}
			);	
					
			var x = Ext.getCmp('EstadoCuenta3').body;
			var mgr = x.getUpdater();
			mgr.on('failure', 
			function(el, response) {
				x.update('');
				NE.util.mostrarErrorResponse(response);
			});		
			mgr.update({
				url: '15estadocuentafindemes1.jsp',	
				scripts: false,
				params: {
					txt_num_prestamo: txt_num_prestamo,
					txt_cliente:txt_cliente,
					mes:'antepenultimo'
				},
				indicatorText: 'Cargando Estado de Cuenta'
			});
			
	}
	
	//GRID GENERAL
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
	var jsonData = store.reader.jsonData;
	 tipoUsuario = jsonData.TIPOUSUARIO;	 
	 var fechaActual = 'Monto Total del Adeudo al  '+jsonData.FECHAACTUAL;
	  
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
				fpE.show();
			}			
			
			var cm = grid.getColumnModel();
			
			if(tipoUsuario =='NAFIN'){			
				grid.getColumnModel().setHidden(cm.findColumnIndex('CG_DESC_CLIENTE'), false);		
			}				
			grid.getColumnModel().setColumnHeader(cm.findColumnIndex('FN_SALDO_INSOLUTO'),fechaActual);
		
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');				
						
			var el = grid.getGridEl();		
				
				if(store.getTotalCount() > 0) {
				
					if(!btnBajarPDF.isVisible()) {
						btnImprimirPDF.enable();
					} else {
						btnImprimirPDF.disable();
					}		
					totalesData.load({
						params: Ext.apply(fp.getForm().getValues(),{	})
					});	
					Ext.getCmp('gridTotales').show();	
					
					el.unmask();
						
					
				} else {					
						Ext.getCmp('gridTotales').hide();	
						btnImprimirPDF.disable();
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	
			//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		//ESTADO DE CUENTA ULTIMO DIA 
		var procesarSuccessFailureGenerarPDFA =  function(opts, success, response) {
		var btnGenerarPDFA = Ext.getCmp('btnGenerarPDFA');
		btnGenerarPDFA.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDFA.disable();
			var btnBajarPDFA = Ext.getCmp('btnBajarPDFA');
			btnBajarPDFA.show();
			btnBajarPDFA.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDFA.focus();
			btnBajarPDFA.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFA.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//para imprimir la Tasa de Amoritizacion
	var procesarSuccessFailureGenerarPDFT =  function(opts, success, response) {
		var btnGenerarPDFT = Ext.getCmp('btnGenerarPDFT');
		btnGenerarPDFT.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDFT.disable();
			var btnBajarPDFT = Ext.getCmp('btnBajarPDFT');
			btnBajarPDFT.show();
			btnBajarPDFT.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDFT.focus();
			btnBajarPDFT.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFT.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//AVISO DE PAGO
		var procesarSuccessFailureGenerarPDFAP =  function(opts, success, response) {
		var btnGenerarPDFAP = Ext.getCmp('btnGenerarPDFAP');
		btnGenerarPDFAP.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {		
			btnGenerarPDFAP.disable();
			var btnBajarPDFAP = Ext.getCmp('btnBajarPDFAP');
			btnBajarPDFAP.show();
			btnBajarPDFAP.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDFAP.focus();
			btnBajarPDFAP.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFAP.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//AVISO DE PAGO VENCIDO	
		var procesarSuccessFailureGenerarPDFAPV =  function(opts, success, response) {
		var btnGenerarPDFAPV = Ext.getCmp('btnGenerarPDFAPV');
		btnGenerarPDFAPV.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDFAPV.disable();
			var btnBajarPDFAPV = Ext.getCmp('btnBajarPDFAPV');
			btnBajarPDFAPV.show();
			btnBajarPDFAPV.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDFAPV.focus();
			btnBajarPDFAPV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFAPV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
		
	//estado de cuenta ultimo mes 	
	var procesarSuccessFailureGenerarCSVE1 =  function(opts, success, response) {
		var btnGenerarCSVE1 = Ext.getCmp('btnGenerarCSVE1');
		btnGenerarCSVE1.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarCSVE1.disable();
			var btnBajarCSVE1 = Ext.getCmp('btnBajarCSVE1');
			btnBajarCSVE1.show();
			btnBajarCSVE1.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarCSVE1.focus();
			btnBajarCSVE1.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarCSVE1.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPDFE1 =  function(opts, success, response) {
		var btnGenerarPDFE1 = Ext.getCmp('btnGenerarPDFE1');
		btnGenerarPDFE1.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDFE1.disable();
			var btnBajarPDFE1 = Ext.getCmp('btnBajarPDFE1');
			btnBajarPDFE1.show();
			btnBajarPDFE1.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDFE1.focus();
			btnBajarPDFE1.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFE1.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
			
	//estado de cuenta penultimo 	
	var procesarSuccessFailureGenerarCSVE2 =  function(opts, success, response) {
		var btnGenerarCSVE2 = Ext.getCmp('btnGenerarCSVE2');
		btnGenerarCSVE2.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarCSVE2.disable();
			var btnBajarCSVE2 = Ext.getCmp('btnBajarCSVE2');
			btnBajarCSVE2.show();
			btnBajarCSVE2.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarCSVE2.focus();
			btnBajarCSVE2.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarCSVE2.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPDFE2 =  function(opts, success, response) {
		var btnGenerarPDFE2 = Ext.getCmp('btnGenerarPDFE2');
		btnGenerarPDFE2.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDFE2.disable();
			var btnBajarPDFE2 = Ext.getCmp('btnBajarPDFE2');
			btnBajarPDFE2.show();
			btnBajarPDFE2.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDFE2.focus();
			btnBajarPDFE2.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFE2.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//estado de cuenta penultimo 	
	var procesarSuccessFailureGenerarCSVE3 =  function(opts, success, response) {
		var btnGenerarCSVE3 = Ext.getCmp('btnGenerarCSVE3');
		btnGenerarCSVE3.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarCSVE3.disable();
			var btnBajarCSVE3 = Ext.getCmp('btnBajarCSVE3');
			btnBajarCSVE3.show();
			btnBajarCSVE3.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarCSVE3.focus();
			btnBajarCSVE3.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarCSVE3.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPDFE3 =  function(opts, success, response) {
		var btnGenerarPDFE3 = Ext.getCmp('btnGenerarPDFE3');
		btnGenerarPDFE3.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDFE3.disable();
			var btnBajarPDFE3 = Ext.getCmp('btnBajarPDFE3');
			btnBajarPDFE3.show();
			btnBajarPDFE3.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDFE3.focus();
			btnBajarPDFE3.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDFE3.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	
		//panel para mostrar  el texto 
	var fpE = new Ext.form.FormPanel({		
			id: 'forma1',
			width: 600,			
			frame: true,		
			titleCollapse: false,
			bodyStyle: 'padding: 6px',			
			defaultType: 'textfield',
			hidden: true,
			html: texto.join('')
			});
			
	//panel para mostrar  el texto 
	 var fpB = {
			xtype: 'panel',
			id: 'forma2',
			width: 600,			
			frame: true,		
			titleCollapse: false,
			bodyStyle: 'padding: 6px',			
			defaultType: 'textfield',
			align: 'center',
			html: busquedaAvanza.join('')
			};
		
	
	
	
	var estadoCuentaAlDia = new Ext.Panel({
	id: 'estadoCuentaAlDia',
	//width: 600,
	height: 'auto',
	hidden: false,
	align: 'center',
	autoScroll: true	
});

	var panelT = new Ext.Panel({
		id: 'panelT',
		//width: 600,
		height: 'auto',
		hidden: false,	
		align: 'center',	
		autoScroll: true	
	});
						

				
 var panelAviso = new Ext.Panel({
	id: 'panelAviso',
	//width: 600,
	height: 'auto',
	hidden: false,
	align: 'center',	
	autoScroll: true	
});

	
	
var panelAvisoV = new Ext.Panel({
	id: 'panelAvisoV',
	//width: 600,
	height: 'auto',
	hidden: false,
	align: 'center',	
	autoScroll: true	
});

	
	

var EstadoCuenta1 = new Ext.Panel({
	id: 'EstadoCuenta1',
	//width: 700,
	height: 'auto',
	hidden: false,
	align: 'center',	
	autoScroll: true	
});


var EstadoCuenta2 = new Ext.Panel({
	id: 'EstadoCuenta2',
	//width: 700,
	height: 'auto',
	hidden: false,
	align: 'center',	
	autoScroll: true	
});


var EstadoCuenta3 = new Ext.Panel({
	id: 'EstadoCuenta3',
	//width: 700,
	height: 'auto',
	hidden: false,
	align: 'center',	
	autoScroll: true
});




		
	var totalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '15avisosext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'			
		},
		fields: [
			{name: 'SALDO_INSOLUTO', mapping: 'SALDO_INSOLUTO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	 var storeBusqAvanzada = new Ext.data.JsonStore({
		id: 'storeBusqAvanzada',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '15avisosext.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	
	var elementsFormBusq = [
		
		{
			xtype: 'textfield',
			name: 'nombre',
			id: 'nombre1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfc',
			id: 'rfc1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},		
		{
			xtype: 'numberfield',
			name: 'noNafinElec',
			id: 'noNafinElec1',
			fieldLabel: 'N�mero Nafin Electr�nico',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'panel',
			items: [
		{
			xtype: 'button',
			width: 80,
			height:10,
			text: 'Buscar',
			iconCls: 'iconoLupa',
			id: 'btnBuscar',
			anchor: '',
			style: 'float:right',
			handler: function(boton, evento) {
				var nombre= Ext.getCmp("nombre1");
				var rfc= Ext.getCmp("rfc1");
				var noNafinElec= Ext.getCmp("noNafinElec1");
				if (Ext.isEmpty(nombre.getValue())  &&  Ext.isEmpty(rfc.getValue())  &&  Ext.isEmpty(noNafinElec.getValue()) ) {
						nombre.markInvalid('Es necesario que ingrese datos en al menos un campo');						
						return;
				}	
				var cbpyme = Ext.getCmp('cbpyme1');
				cbpyme.setValue('');
				cbpyme.setDisabled(false);				
				storeBusqAvanzada.load({
						params: Ext.apply(fpBusqAvanzada.getForm().getValues())
					});	
			}
		}
		
		]
		},
				
		{
			xtype: 'combo',
			name: 'cbpyme',
			id: 'cbpyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzada
		}
	];
	
	
		var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 'fBusqAvanzada',
		width: 400,
		title: 'B�squeda  Avanzada',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
				fpB, NE.util.getEspaciador(20), elementsFormBusq
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoBuscar',
				formBind: true,
				disabled: true,
				align: 'center',
				handler: function(boton, evento) {	
					var cbpyme= Ext.getCmp("cbpyme1");
					var txt_cliente = Ext.getCmp('txt_cliente1');
					
					if (Ext.isEmpty(cbpyme.getValue())) {
						cbpyme.markInvalid('Seleccione la Pyme');
						return;
					}
						
					var record = cbpyme.findRecord(cbpyme.valueField, cbpyme.getValue());
					record =  record ? record.get(cbpyme.displayField) : cbpyme.valueNotFoundText;					
					txt_cliente.setValue(cbpyme.getValue());
						
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {								
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
					//gridTotales.hide();
				}
				
			}
		]
	});
	
	var elementosForma = [	
	{
		xtype: 'radio',
		name: 'rad_prestamos',
		id: 'rad_prestamos',
		fieldLabel: 'Pr�stamos Vigentes',
		allowBlank: true,
		startDay: 0,
		width: 100,
		height:20,
		msgTarget: 'side',
		vtype: 'rad_prestamos', 
		checked: true,
		campoFinFecha: 'txt_num_prestamo',
		margins: '0 20 0 0'
	},	
	{
			xtype: 'compositefield',
			fieldLabel: 'No. Cliente',			
			msgTarget: 'side',
			id: 'compositefield1',
			combineErrors: false,
			hidden: true,
			items: [
				{
					xtype: 'numberfield',
					name: 'txt_cliente',
					id: 'txt_cliente1',
					allowBlank: true,
					hidden: false,
					maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
				xtype: 'button',
				hidden: false,
				text: 'B�squeda  Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					var cbpyme = Ext.getCmp('cbpyme1');
					cbpyme.setValue('');
					cbpyme.setDisabled(true);	
					
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								layout: 'fit',
								width: 400,
								height: 300,
								modal: true,								
								resizable:false,
								closeAction: 'destroy',
								closable:false,
								autoDestroy:true,	
								id: 'winBusqAvan',								
								items: [
									fpBusqAvanzada
								]
							}).show();
					}
				}
			}					
			]
		},	
		{
		xtype: 'numberfield',
		name: 'txt_num_prestamo',
		id: 'txt_num_prestamo',
		fieldLabel: 'No. Pr�stamo',
		allowBlank: true,
		startDay: 0,
		maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
		width: 100,
		msgTarget: 'side',
		vtype: 'txt_num_prestamo', 
		campoFinFecha: 'txt_num_prestamo',
		margins: '0 20 0 0'		
	}			
	];
	
	var fp = new Ext.form.FormPanel({
			id: 'forma',
			width: 500,
			title: 'Resumen de Pr�stamos',
			frame: true,
			collapsible: true,
			titleCollapse: false,
			bodyStyle: 'padding: 6px',
			style: 'margin:0 auto;',
			labelWidth: 126,
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			items: elementosForma,			
			monitorValid: true,
			buttons: [
				{
					text: 'Consultar',
					iconCls: 'icoBuscar',
					formBind: true,
					handler: function(boton, evento) {
					
					if(tipoUsuario =='NAFIN'){					
						var txt_cliente = Ext.getCmp("txt_cliente1");
							if (Ext.isEmpty(txt_cliente.getValue()) ) {
								txt_cliente.markInvalid('Debe especificar el n�mero de cliente');
								return;
						}			
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');
					
					var ventanaEstadoDia = Ext.getCmp('VerEstadoDia');
					if (ventanaEstadoDia) {					
						ventanaEstadoDia.destroy();
					}
					var ventanaTasa = Ext.getCmp('VerTasa');
					if (ventanaTasa) {					
						ventanaTasa.destroy();
					}
					var ventanaAvisoPago = Ext.getCmp('VerAvisoPago');
					if (ventanaAvisoPago) {					
						ventanaAvisoPago.destroy();
					}	
					var ventanaAvisoPagoV = Ext.getCmp('VerAvisoPagoV');
					if (ventanaAvisoPagoV) {					
						ventanaAvisoPagoV.destroy();
					}
					var ventanaEstado1= Ext.getCmp('EstadoCuenta01');
					if (ventanaEstado1) {					
						ventanaEstado1.destroy();
					}
					var ventanaEstado2= Ext.getCmp('EstadoCuenta02');
					if (ventanaEstado2) {					
						ventanaEstado2.destroy();
					}
					var ventanaEstado3= Ext.getCmp('EstadoCuenta03');
					if (ventanaEstado3) {					
						ventanaEstado3.destroy();
					}					
					var ventana = Ext.getCmp('winTotales');
					if (ventana) {					
						ventana.destroy();
					}	
				
					
					consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15
							})
						});	
						Ext.getCmp('btnImprimirPDF').disable();
						Ext.getCmp('btnBajarPDF').hide();
									
					
					}					
				},				
				{
					text: 'Limpiar',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '15avisosext.jsp';
					}
					
				}
			]	
		});
			

		
		
		var gridTotales = {
		xtype: 'grid',
		store: totalesData,
		id: 'gridTotales',	
		hidden:true,
		columns: [	
			{
				header: 'TOTAL A PAGAR',
				dataIndex: 'SALDO_INSOLUTO',
				width: 200,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			}
			],
			height: 50,			
			width: 943,	
			frame: false
		};	
		var consultaData = new Ext.data.JsonStore({
			root : 'registros',
			url : '15avisosext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
				{name: 'CG_DESC_CLIENTE'},
				{name: 'IG_NUMERO_PRESTAMO'}, 
				{name: 'CG_DESC_PRODUCTO_BANCO'}, 
				{name: 'DF_FECHA_VENCIMIENTO',type: 'date', dateFormat: 'd/m/Y'}, 
				{name: 'FN_MONTO_INICIAL',type: 'float'}, 
				{name: 'HAY_TABLA_AMORTIZACION'}, 
				{name: 'HAY_AVISOS'}, 				
				{name: 'HAY_AVISOS_VENCIDOS' }, 
				{name: 'FN_SALDO_INSOLUTO',type: 'float'}, 
				{name: 'ESTADO_ULTIMO_MES'},
				{name: 'ESTADO_PENULTIMO_MES'},
				{name: 'ESTADO_ANTEPENULTIMO_MES'},
				{name: 'IG_CODIGO_CLIENTE'},
				{name: 'MESULTIMO'},
				{name: 'MESPENULTIMO'},
				{name: 'MESANTEPENULTIMO'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});		
	
		var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: columnas, align: 'center'},
				{header: 'Estado de Cuenta por Pr�stamo', colspan: 3, align: 'center'}								
			]
		]
	});
	
		//Seleccionado Pyme a Rechazado IF
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		plugins: grupos,
		title: 'Resumen de Pr�stamos ',
		columns: [
		{
				header: 'Codigo Cliente',
				tooltip: 'Codigo Cliente',
				dataIndex: 'IG_CODIGO_CLIENTE ',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'Cliente',
				tooltip: 'Cliente',
				dataIndex: 'CG_DESC_CLIENTE',
				sortable: true,
				resizable: true	,
				width: 300,
				hidden: true,
				align: 'left'
			},
			{
	       xtype: 'actioncolumn',
				 header: 'Num. Pr�stamo',
				 tooltip: 'Num. Pr�stamo',
				 dataIndex: 'IG_NUMERO_PRESTAMO',
         width: 130,
				 align: 'center',
				 renderer: function(value, metadata, record, rowindex, colindex, store) {
					return (record.get('IG_NUMERO_PRESTAMO'));
					},
				 items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {				
							if (registro.get('IG_NUMERO_PRESTAMO') !='') {
								this.items[0].tooltip = 'Ver';								
								return 'iconoLupa';
							}					
						},
						handler: procesarEstadoCuentaAlDia
					}
				]	
			}	,			

			
			{
				header: 'Programa',
				tooltip: 'Programa',
				dataIndex: 'CG_DESC_PRODUCTO_BANCO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'DF_FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Monto del Cr�dito Solicitado',
				tooltip: 'Monto del Cr�dito Solicitado',
				dataIndex: 'FN_MONTO_INICIAL',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},			
			{
	       xtype: 'actioncolumn',
				 header: 'Tabla Amortizaci�n',
				 tooltip: 'Tabla Amortizaci�n',
         width: 130,
				 align: 'center',	
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('HAY_TABLA_AMORTIZACION') =='true') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler: procesarVerTasa
					}
				]	
			},
			{
	       xtype: 'actioncolumn',
				 header: 'Aviso de Pago',
				 tooltip: 'Aviso de Pago',
         width: 130,
				 align: 'center',	
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('HAY_AVISOS') =='true') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler: procesarVerAvisoPago
					}
				]				
			}	,	
			{
	       xtype: 'actioncolumn',
				 header: 'Aviso de Pago Vencido',
				 tooltip: 'Aviso de Pago Vencido',
         width: 130,
				 align: 'center',					 
				 items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('HAY_AVISOS_VENCIDOS') =='true') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler: procesarVerAvisoPagoV
					}
				]							
			}	,
			{
				//header: 'Monto Total del Adeudo al ',
				//tooltip: 'Monto Total del Adeudo al ',
				dataIndex: 'FN_SALDO_INSOLUTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},									
			{
	       xtype: 'actioncolumn',
				header: '',
				tooltip: '',				
        width: 130,
				align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {							 
					return record.get('MESULTIMO');
					},
				 items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('ESTADO_ULTIMO_MES') =='true') {
								this.items[0].tooltip = 'JULIO';
								return 'iconoLupa';
							}
						},
						handler: procesarEstadoUltimo
					}
				]										
			},			
			{
	       xtype: 'actioncolumn',
				header: '',
				tooltip: '',
        width: 130,
				align: 'center',		
				renderer: function(value, metadata, record, rowindex, colindex, store) {					
					return record.get('MESPENULTIMO');
				},
				 items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('ESTADO_PENULTIMO_MES') =='true') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}						
						},
						handler: procesarEstadoPenUltimo
					}
				]				
			},	
			{
	      xtype: 'actioncolumn',
				header: '',
				tooltip: '',
         width: 130,
				 align: 'center',	
					renderer: function(value, metadata, record, rowindex, colindex, store) {								 
					return record.get('MESANTEPENULTIMO');;
				},
				
				 items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('ESTADO_ANTEPENULTIMO_MES') =='true') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}											
						},
						handler: procesarEstadoAntePenUltimo
					}
				]			
			}							
			],			
			stripeRows: true,
			loadMask: true,
			height: 400,
			width: 943,		
			frame: true,
			bbar: {
				xtype: 'paging',
				pageSize: 15,
				buttonAlign: 'left',
				id: 'barraPaginacion',
				displayInfo: true,
				store: consultaData,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No hay registros.",
				items: [
					'-',					
					{
						xtype: 'button',
						text: 'Imprimir PDF',
						id: 'btnImprimirPDF',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url: '15avisosext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoPDF'								
								}),
								callback: procesarSuccessFailurePDF
							});
						}
					},
					{
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDF',
						hidden: true
					}					
				]
			}	
		});
		
		//panel para mostrar  el texto 
		var fpE = new Ext.form.FormPanel({		
			id: 'forma1',
			width: 943,			
			frame: true,		
			titleCollapse: false,
			bodyStyle: 'padding: 6px',	
			style: 'margin:0 auto;',
			defaultType: 'textfield',
			hidden: true,
			html: texto.join('')
			});
		
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		items: [
			fp,
			NE.util.getEspaciador(20),
			fpE,
			NE.util.getEspaciador(20),
			grid,
			gridTotales
		]
		});
		
	//-------------------------------- ----------------- -----------------------------------
	
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '15avisosext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});
	
	});