Ext.onReady(function() {
	var strTipoUsuario = Ext.getDom('auxStrTipoUsuario').value;
	var mensaje ="";
	var diaHabilHora = "";
  
  var objGral = {
    strPerfil : Ext.getDom('strPerfil').value,
    ic_if: '',
    sirac: '',
    financiera: ''
  }
	
	
	//--------------------------------------------------------------------------
	//----------------------------------- Handlers ------------------------------
  function validaPerfil(perfil){
    if(perfil =='LINEA CREDITO' || perfil =='IF LI' || perfil =='IF 4CP' || perfil =='IF 5CP' ||
       perfil =='IF 4MIC' || perfil =='IF 5MIC' ){
      return 'LINEA CREDITO';
    }else{
      return 'DESCUENTO';
    }
  }
  
	function procesaValoresIniciales(opts, success, response) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				
				diaHabilHora = jsonData.diaHabil;   
        
        objGral.sirac = jsonData.NUMERO_SIRAC;
        objGral.financiera = jsonData.IC_FINANCIERA;
        
        if(validaPerfil(objGral.strPerfil)!='LINEA CREDITO'){
          if(strTipoUsuario=="IF" || strTipoUsuario=="CLIENTE"){
            mensaje =jsonData.mensaje;
            Ext.getCmp('ley2').update(jsonData.fechaConsulta);
            
            
            if(jsonData.NUMERO_SIRAC=='' || jsonData.IC_FINANCIERA==''){
              if(jsonData.IC_FINANCIERA==''){
                Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
                Ext.getCmp("txtCliente").setValue(jsonData.NUMERO_SIRAC);
                
              }
            }else{
              Ext.getCmp("tipoLinea").show();
            }
          }else{
			Ext.getCmp("tipoLinea").show();
		  }
        }else{
          mensaje =jsonData.mensaje;
          Ext.getCmp('ley2').update(jsonData.fechaConsulta);
          Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
          Ext.getCmp("txtCliente").setValue(jsonData.sirac);

        }
      
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureInterfases =  function(opts, success, response) {
			Ext.getCmp('btnConsultar').enable();
			var btnCon = Ext.getCmp('btnConsultar');
			btnCon.setIconClass('icoBuscar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;	
			if(infoR.informacion=="SaldoMensual"){
				Ext.getCmp('ley2').show();
				Ext.getCmp('ley2').update("");
				Ext.getCmp('ley2').update("<left><FONT SIZE=2 >"+infoR.fechaConsulta+"</left>");
			}
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
				btnCon.enable();
				btnCon.setIconClass('icoBuscar');
			NE.util.mostrarConnError(response,opts);
		}
	}
	//--------------------------------------------------------------------------
	//----------------------------------- Stores ------------------------------
	var catalogoIF = new Ext.data.JsonStore({
	   id: 'catologoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15saldoDiarioMensual.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoIF'
		},
		autoLoad: true,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	//--------------------------------------------------------------------------
	//----------------------------------- elementos ------------------------------
	var elementosLay= [
		{
			xtype: 'panel', id:'elementosLay', layout:'table',	width:700,	border:true,	layoutConfig:{ columns: 4 },
			defaults: {frame:false, border: true,width:100, height: 30,bodystyle:'padding:4px'},
			items:[
				{ width:100 ,	frame:true,	html:'<b><div class="formas" align="center">No.  De Campo</div></b>'	},//1
				{ width:250,	frame:true,	html:'<b><div class="formas"  align="center">Descripci�n</div></b>'	},
				{ width:200,	frame:true,	html:'<b><div class="formas"  align="center">Tipo de Dato</div></b>'	},
				{ width:150,	frame:true,   html:'<b><div class="formas"  align="center">Longitud</div></b>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">1</div>'	},//2
				{ width:250, height: 20,	html:'<div class="formas"  align="left">MONEDA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">2</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">2</div>'	},//3
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_MONEDA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">30</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">3</div>'	},//4
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CODSUCURSAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">4</div>'	},//5
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NOMSUCURSAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">60</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">5</div>'	},//6
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERMEDIARIO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">6</div>'	},//7
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_INTER</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">150</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">7</div>'	},//8
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CODIGO_BASE_OPERACION</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">8</div>'	},//9
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_BASE_DE_OPERACION</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">100</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">9</div>'	},//10
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TIPO_CREDITO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">10</div>'	},//11
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_TIPO_CREDITO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">60</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">11</div>'	},//12
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CLIENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">12</div>'	},//13
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NOMBRECLIENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">150</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">13</div>'	},//14
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NUMERO_PRESTAMO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">14</div>'	},//15
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NUM_ELECTRONICO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">15</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">15</div>'	},//16
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CODIGO_EPO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">16</div>'	},//17
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_EPO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">150</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">17</div>'	},//18
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_DEBE_DESDE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">18</div>'	},//19
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CUOTAS_VENCIDAS</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">19</div>'	},//20
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_APERTURA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">20</div>'	},//21
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_VENCIMIENTO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">21</div>'	},//22
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FRECUENCIA_CAPITAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">2</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">22</div>'	},//23
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FRECUENCIA_INTERES</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">2</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">23</div>'	},//24
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_PRIMER_PAGO_CAPITAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">24</div>'	},//25
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_PRIMER_PAGO_INTERES</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">25</div>'	},//27
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_PROXIMO_PAGO_CAPITAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">26</div>'	},//28
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_PROXIMO_PAGO_INTERES</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">27</div>'	},//29
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_ULTIMO_PAGO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">28</div>'	},//30
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TASAREFERENCIAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">29</div>'	},//31
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCIONTASA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">60</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">30</div>'	},//32
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TASA_TOTAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">7,4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">31</div>'	},//33
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TIPO_INTERES</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">1</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">32</div>'	},//34
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DIA_REVISION_TASA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">33</div>'	},//35
				{ width:250, height: 20,	html:'<div class="formas"  align="left">MONTOOPERADO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">34</div>'	},//36
				{ width:250, height: 20,	html:'<div class="formas"  align="left">SALDO_INSOLUTO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">35</div>'	},//37
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CAPITAL_VIGENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">36</div>'	},//38
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CAPITAL_VENCIDO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">37</div>'	},//39
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERES_VIGENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">38</div>'	},
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERES_VENCIDO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">39</div>'	},
				{ width:250, height: 20,	html:'<div class="formas"  align="left">MORATORIOS</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	}
			]
		}
	];
  
  
  //LINEA CREDITO
  var elementosLayCredito= [
		{
			xtype: 'panel', id:'elementosLayCredito', layout:'table',	width:700,	border:true,	layoutConfig:{ columns: 4 }, 
			defaults: {frame:false, border: true,width:100, height: 30,bodystyle:'padding:4px'},
			items:[
				{ width:100 ,	frame:true,	html:'<b><div class="formas" align="center">No.  De Campo</div></b>'	},//1
				{ width:250,	frame:true,	html:'<b><div class="formas"  align="center">Descripci�n</div></b>'	},
				{ width:200,	frame:true,	html:'<b><div class="formas"  align="center">Tipo de Dato</div></b>'	},
				{ width:150,	frame:true,   html:'<b><div class="formas"  align="center">Longitud</div></b>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">1</div>'	},//2
				{ width:250, height: 20,	html:'<div class="formas"  align="left">MONEDA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">2</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">2</div>'	},//3
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_MONEDA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">30</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">3</div>'	},//4
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CODSUCURSAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">4</div>'	},//5
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NOMSUCURSAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">60</div>'	},
				/*{ width:100, height: 20,	html:'<div class="formas" align="center">5</div>'	},//6
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERMEDIARIO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">6</div>'	},//7
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_INTER</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">150</div>'	},*/
				{ width:100, height: 20,	html:'<div class="formas" align="center">5</div>'	},//8
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CODIGO_BASE_OPERACION</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">6</div>'	},//9
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_BASE_DE_OPERACION</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">100</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">7</div>'	},//10
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TIPO_CREDITO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">8</div>'	},//11
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_TIPO_CREDITO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">60</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">9</div>'	},//12
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CLIENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">10</div>'	},//13
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NOMBRECLIENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">150</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">11</div>'	},//14
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NUMERO_PRESTAMO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">12</div>'	},//15
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NUM_ELECTRONICO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">15</div>'	},
			/*	{ width:100, height: 20,	html:'<div class="formas" align="center">15</div>'	},//16
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CODIGO_EPO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">16</div>'	},//17
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_EPO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">150</div>'	},*/
				{ width:100, height: 20,	html:'<div class="formas" align="center">13</div>'	},//16
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_DEBE_DESDE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">14</div>'	},//17
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CUOTAS_VENCIDAS</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">15</div>'	},//18
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_APERTURA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">16</div>'	},//19
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_VENCIMIENTO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">17</div>'	},//20
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FRECUENCIA_CAPITAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">2</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">18</div>'	},//21
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FRECUENCIA_INTERES</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">2</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">19</div>'	},//22
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_PRIMER_PAGO_CAPITAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">20</div>'	},//23
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_PRIMER_PAGO_INTERES</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">21</div>'	},//24
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_PROXIMO_PAGO_CAPITAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">22</div>'	},//25
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_PROXIMO_PAGO_INTERES</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">23</div>'	},//26
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHA_ULTIMO_PAGO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">24</div>'	},//27
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TASAREFERENCIAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">25</div>'	},//28
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCIONTASA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">60</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">26</div>'	},//29
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TASA_TOTAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">7,4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">27</div>'	},//30
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TIPO_INTERES</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">1</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">28</div>'	},//31
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DIA_REVISION_TASA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">29</div>'	},//32
				{ width:250, height: 20,	html:'<div class="formas"  align="left">MONTOOPERADO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">30</div>'	},//33
				{ width:250, height: 20,	html:'<div class="formas"  align="left">SALDO_INSOLUTO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">31</div>'	},//34
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CAPITAL_VIGENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">32</div>'	},//35
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CAPITAL_VENCIDO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">33</div>'	},//36
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERES_VIGENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">34</div>'	},//37
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERES_VENCIDO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">35</div>'	},//38
				{ width:250, height: 20,	html:'<div class="formas"  align="left">MORATORIOS</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,6</div>'	}
			]
		}
	];
  
	var elementosLayMensual= [
		{
			xtype: 'panel',layout:'table',	width:700,	border:true,	layoutConfig:{ columns: 4 },
			defaults: {frame:false, border: true,width:100, height: 35,bodyStyle:'padding:4px'},
			items:[
				{ width:100 , frame:true,	html:'<b><div class="formas" align="center">No. De Campo</div></b>'	},//1
				{ width:250, frame:true,	html:'<b><div class="formas"  align="center">Descripci�n</div></b>'	},
				{ width:200, frame:true,	html:'<b><div class="formas"  align="center">Tipo de Dato</div></b>'	},
				{ width:150, frame:true,  html:'<b><div class="formas"  align="center">Longitud</div></b>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">1</div>'	},//2
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHAFINMES</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">2</div>'	},//3
				{ width:250, height: 20,	html:'<div class="formas"  align="left">PRESTAMO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">3</div>'	},//4
				{ width:250, height: 20,	html:'<div class="formas"  align="left">MONEDA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">2</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">4</div>'	},//5
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_MONEDA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">30</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">5</div>'	},//6
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERMEDIARIO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">5</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">6</div>'	},//7
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_INTER</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">60</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">7</div>'	},//8
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CODSUCURSAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">8</div>'	},//9
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NOMSUCURSAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">30</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">9</div>'	},//10
				{ width:250, height: 20,	html:'<div class="formas"  align="left">BASE_DE_OPERACI�N</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">10</div>'	},//11
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_BASE_DE_OPERACI�N</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">100</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">11</div>'	},//12
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TIPO_CREDITO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">12</div>'	},//13
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_TIPO_CREDITO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">40</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">13</div>'	},//14
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CLIENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">14</div>'	},//15
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NOMBRECLIENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">150</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">15</div>'	},//16
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NUMELECTRONICO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">16</div>'	},//17
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHOPERACION</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">17</div>'	},//18
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHVENCIMIENTO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">18</div>'	},//19
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCIONTASA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">30</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">19</div>'	},//20
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TASATOTAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">7,4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">20</div>'	},//21
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TASAMORATORIA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">7,4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">21</div>'	},//22
				{ width:250, height: 20,	html:'<div class="formas"  align="left">1aCUOTAVENCIDA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">22</div>'	},//23
				{ width:250, height: 20,	html:'<div class="formas"  align="left">MONTOOPERADO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,2</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">23</div>'	},//27
				{ width:250, height: 20,	html:'<div class="formas"  align="left">SALDOINSOLUTO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">24</div>'	},//28
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CAPITALVIGENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">25</div>'	},//29
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CAPITALVENCIDO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">26</div>'	},//30
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERES_DEVENG_AL_CIERRE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">27</div>'	},//31
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERESVENCIDO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">28</div>'	},//32
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TOTAL MORATORIOS</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">29</div>'	},//33
				{ width:250, height: 20,	html:'<div class="formas"  align="left">ADEUDOTOTAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	}
			]
		}
	];
  
  var elementosLayMensualCredito= [
		{
			xtype: 'panel',layout:'table',	width:700,	border:true,	layoutConfig:{ columns: 4 },
			defaults: {frame:false, border: true,width:100, height: 35,bodyStyle:'padding:4px'},
			items:[
				{ width:100 , frame:true,	html:'<b><div class="formas" align="center">No. De Campo</div></b>'	},//1
				{ width:250, frame:true,	html:'<b><div class="formas"  align="center">Descripci�n</div></b>'	},
				{ width:200, frame:true,	html:'<b><div class="formas"  align="center">Tipo de Dato</div></b>'	},
				{ width:150, frame:true,  html:'<b><div class="formas"  align="center">Longitud</div></b>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">1</div>'	},//2
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHAFINMES</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">2</div>'	},//3
				{ width:250, height: 20,	html:'<div class="formas"  align="left">PRESTAMO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">3</div>'	},//4
				{ width:250, height: 20,	html:'<div class="formas"  align="left">MONEDA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">2</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">4</div>'	},//5
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_MONEDA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">30</div>'	},
				/*{ width:100, height: 20,	html:'<div class="formas" align="center">5</div>'	},//6
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERMEDIARIO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">5</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">6</div>'	},//7
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_INTER</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">60</div>'	},*/
				{ width:100, height: 20,	html:'<div class="formas" align="center">5</div>'	},//8
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CODSUCURSAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">6</div>'	},//9
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NOMSUCURSAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">30</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">7</div>'	},//10
				{ width:250, height: 20,	html:'<div class="formas"  align="left">BASE_DE_OPERACI�N</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">6</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">8</div>'	},//11
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_BASE_DE_OPERACI�N</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">100</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">9</div>'	},//12
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TIPO_CREDITO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">10</div>'	},//13
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCION_TIPO_CREDITO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">40</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">11</div>'	},//14
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CLIENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">12</div>'	},//15
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NOMBRECLIENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">150</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">13</div>'	},//16
				{ width:250, height: 20,	html:'<div class="formas"  align="left">NUMELECTRONICO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">12</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">14</div>'	},//17
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHOPERACION</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">15</div>'	},//18
				{ width:250, height: 20,	html:'<div class="formas"  align="left">FECHVENCIMIENTO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">16</div>'	},//19
				{ width:250, height: 20,	html:'<div class="formas"  align="left">DESCRIPCIONTASA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Alfanum�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">30</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">17</div>'	},//20
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TASATOTAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">7,4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">18</div>'	},//21
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TASAMORATORIA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">7,4</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">19</div>'	},//22
				{ width:250, height: 20,	html:'<div class="formas"  align="left">1aCUOTAVENCIDA</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Fecha</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">dd/mm/aaaa</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">20</div>'	},//23
				{ width:250, height: 20,	html:'<div class="formas"  align="left">MONTOOPERADO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,2</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">21</div>'	},//27
				{ width:250, height: 20,	html:'<div class="formas"  align="left">SALDOINSOLUTO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">22</div>'	},//28
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CAPITALVIGENTE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">23</div>'	},//29
				{ width:250, height: 20,	html:'<div class="formas"  align="left">CAPITALVENCIDO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">24</div>'	},//30
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERES_DEVENG_AL_CIERRE</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">25</div>'	},//31
				{ width:250, height: 20,	html:'<div class="formas"  align="left">INTERESVENCIDO</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">26</div>'	},//32
				{ width:250, height: 20,	html:'<div class="formas"  align="left">TOTAL MORATORIOS</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	},
				{ width:100, height: 20,	html:'<div class="formas" align="center">27</div>'	},//33
				{ width:250, height: 20,	html:'<div class="formas"  align="left">ADEUDOTOTAL</div>'	},
				{ width:200, height: 20,	html:'<div class="formas"  align="left">Num�rico</div>'	},
				{ width:150, height: 20,  html:'<div class="formas"  align="center">19,3</div>'	}
			]
		}
	];
  
	var elementosForma = [	
		{
xtype: 'radiogroup',
        id:'tipoLinea',
        name: 'tipoLinea',
        fieldLabel: 'Tipo de Linea',
        //hidden: true,
        items: [
           {boxLabel: 'LINEA CREDITO', name: 'tipoLinea', id:'tipoLineaC', inputValue: 'C', value:'C' },
           {boxLabel: 'LINEA DESCUENTO', name: 'tipoLinea', id:'tipoLineaD', inputValue: 'D', checked:true, value:'D' }
        ],
        listeners:{
          change: function(rgroup, radio){
            muestraAyuda.hide();
            muestraAyudaMensual.hide();
            muestraAyudaCredito.hide();
            muestraAyudaMensualCredito.hide();
            if(radio.value=='C'){
				
              if(strTipoUsuario != "IF"  &&  strTipoUsuario != "CLIENTE"){
                Ext.getCmp('cmbIntermediario').hide();
                Ext.getCmp('txtCliente').show();
              }else{
                objGral.ic_if = 12;
                Ext.getCmp('txtCliente').setValue(objGral.sirac);
              }
              Ext.getCmp('cmbIntermediario').setValue("12");
              
            }else{
              
			  if(strTipoUsuario != "IF"  ){
                //Ext.getCmp('cmbIntermediario').setValue("");
                Ext.getCmp('cmbIntermediario').show();
                Ext.getCmp('txtCliente').hide();
              }else{
                objGral.ic_if = "";
              }
              Ext.getCmp('cmbIntermediario').setValue("");
              
              Ext.getCmp('txtCliente').setValue("");
              
            }
          }
        }
      },
    /*{
			xtype:'compositefield', 
			id:'comboInt',
			hidden:true,
			combineErrors: false, 
			items: [*/
				{
					xtype: 'combo',
					name:'intermediario',
					id: 'cmbIntermediario',
					hiddenName: 'intermediario',
					fieldLabel: 'Intermediario Financiero',
					mode: 'local',
					width: 300,
					autoLoad: false,
					displayField: 'descripcion',
					valueField: 'clave',
					emptyText: 'Seleccionar...',
					forceSelection: true,
					triggerAction: 'all',
					typeAhead: true,
					msgTarget: 'side',
					minChars: 1,
					allowBlank: true,	
					store: catalogoIF,
					tpl: NE.util.templateMensajeCargaCombo,
					hidden:true,
					tpl:  '<tpl for=".">' +
						'<tpl if="!Ext.isEmpty(loadMsg)">'+
						'<div class="loading-indicator">{loadMsg}</div>'+
						'</tpl>'+
						'<tpl if="Ext.isEmpty(loadMsg)">'+
						'<div class="x-combo-list-item">' +
						'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
						'</div></tpl></tpl>'
				},
        {
          xtype			:	'numberfield',
          fieldLabel 	: 'N�mero Cliente SIRAC',
          name 			: 'cliente',
          id 			: 'txtCliente',
          allowBlank	: true,
          hidden: true,
          maxLength 	: 12,
          width 		: 100,
          msgTarget 	: 'side',
          tabIndex		: 3,
          margins		: '0 20 0 0'
        },
			/*]
		},*/
		{	
			xtype:'panel',
			id: 'panel1',
			bodyStyle: 'padding: 6px',
			items:[
				{
					xtype:'compositefield', 
					triggerAction : 'all',
					combineErrors: false, 
					items: [
						{
							xtype:'button',
							id:'ayudaH',
							iconCls:'icoAyuda',
							handler: function() {
								if(Ext.getCmp('cs_tipo_D').getValue()==true){
                  if(Ext.getCmp('tipoLinea').getValue().value=='C'){
                    muestraAyudaCredito.show();
                  }else{
                    muestraAyuda.show();
                  }
								}else if(Ext.getCmp('cs_tipo_M').getValue()==true){
									if(Ext.getCmp('tipoLinea').getValue().value=='C'){
                    muestraAyudaMensualCredito.show();
                  }else{
                    muestraAyudaMensual.show();
                  }
								}
							}
						},
						{ 
							xtype: 'displayfield', 
							value: 'Lay-out de ayuda de Saldos Diarios', 
							id: 'nomAyuda',
							labelSeparator:' ',
							width: 110 }
						,
						{
							xtype:'radiogroup',
							id:	'radioGp',
							width: 300,
							columns: 2,
							hidden: false,
							items:[
								{
									boxLabel: 'Saldos Diarios',
									id:'cs_tipo_D',
									name: 'cs_saldo',
									inputValue:'D', 
									checked: true,
									listeners:{
										check:	function(radio){
											if(radio.checked){
												Ext.getCmp('nomAyuda').setValue('Lay-out de ayuda de Saldos Diarios');
												Ext.getCmp('fpMensaje_1').hide();
												Ext.getCmp('ley2').hide();
                        
                        muestraAyuda.hide();
                        muestraAyudaMensual.hide();
                        muestraAyudaCredito.hide();
                        muestraAyudaMensualCredito.hide();
                        
											}
										}
									}
								},
								{	
									boxLabel: 'Saldos Mensuales',
									id:'cs_tipo_M',
									name: 'cs_saldo',
									inputValue:'M',
									checked: false,
									listeners:{
										check:	function(radio){	
											if(radio.checked){
												Ext.getCmp('nomAyuda').setValue('Lay-out de ayuda de Saldos Mensuales');
												Ext.getCmp('fpMensaje_1').hide();
												Ext.getCmp('ley2').hide();
                        
                        muestraAyuda.hide();
                        muestraAyudaMensual.hide();
                        muestraAyudaCredito.hide();
                        muestraAyudaMensualCredito.hide();
											}
										}
									}							
								}
							] 
						}
					]
				}
			]
		}
	];
	var ventanaAyuda = new Ext.Window({
		title: '',
		hidden: true,
		id: 'venanaAyuda',
		width: 650,
		height: 'auto',
		maximizable: false,
		closable: true,
		modal: true,
		closeAction: 'hide',
		resizable: false,
		minWidth: 500,
		minHeight: 450,
		maximized: false,
		constrain: true
	});
	var muestraAyuda=new Ext.FormPanel({	
		style: 'margin:0 auto;',
		height:'auto',
		width:700,	
		border:true,	
		frame:false,	
		items:[elementosLay],
		hidden:true 	
	});
  var muestraAyudaCredito=new Ext.FormPanel({	
		style: 'margin:0 auto;',
		height:'auto',
		width:700,	
		border:true,	
		frame:false,	
		items:[elementosLayCredito],
		hidden:true 	
	});
	var muestraAyudaMensual=new Ext.FormPanel({	
		style: 'margin:0 auto;',
		height:'auto',
		width:700,	
		border:true,	
		frame:false,	
		items:elementosLayMensual,	
		hidden:true 	
	});
	
  var muestraAyudaMensualCredito=new Ext.FormPanel({	
		style: 'margin:0 auto;',
		height:'auto',
		width:700,	
		border:true,	
		frame:false,	
		items:elementosLayMensualCredito,	
		hidden:true 	
	});
  
	
  var fpMensaje_1 = new Ext.Container({
		layout: 'table',
		id: 'fpMensaje_1',
		hidden: true,	
		width:	600,		
		style: 'margin:0 auto;',
		align: 'center',
		items: [			
			{
				xtype: 'label',
				width		: 600,	
				id: 'mensajeTitulo_1',
				align: 'center',
				value: ''
			}
		]
	});
	
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame: true,
		width: 600,
		title: 'Saldos',
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		hidden : false,
		collapsible: true,
		titleCollapse: false,
		defaultType: 'textfield',
		items: elementosForma,	
		buttons: [
			{ 
					xtype: 'displayfield', 
					value: 'Saldos al d�a de hoy por la ma�ana', 
					id: 'ley2',
					hidden:true
			},
			{
					xtype: 'displayfield',
					value: '       ',
					width: 60
			},
			{
				text: 'Consultar',
				id:'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					Ext.getCmp('ley2').update("");
					Ext.getCmp('mensajeTitulo_1').update("");
					if(diaHabilHora=="false"  ){
						Ext.MessageBox.alert("Mensaje","<b>Informaci�n disponible s�lo en d�as y <br>horarios h�biles.</b>");
						return;
					}
					var d = new Date();
					var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
					if(strTipoUsuario == "IF" || strTipoUsuario == "CLIENTE"  ){
						if(Ext.getCmp('cs_tipo_D').getValue()==true){
							Ext.getCmp('mensajeTitulo_1').update("<left><FONT SIZE=2 >La informaci�n presentada corresponde al d�a  "+d.getDate()+" de "+meses[d.getMonth()]+" de "+d.getFullYear() +"<br><br>En caso de cualquier duda o comentario, favor de enviarnos un correo electr�nico a la siguiente direcci�n:<br><br><FONT SIZE=3>C.P. PEDRO MACEDO CASTILLO</FONT> <BR>Subdirector de Operaci�nes de Cr�dito<br><FONT SIZE=3>tel.(55)53256872</FONT><br>Correo:</FONT><u><b><FONT COLOR=BLUE >pmacedo@nafin.gob.mx</FONT></b></u></left>");

						}else if(Ext.getCmp('cs_tipo_M').getValue()==true){
							Ext.getCmp('mensajeTitulo_1').update(mensaje);

						}
						Ext.getCmp('fpMensaje_1').show();
					}
					if(strTipoUsuario == "NAFIN"  ){
						if(Ext.getCmp('tipoLinea').getValue().value!='C'){
							var interF = Ext.getCmp('cmbIntermediario');
							if(Ext.isEmpty(interF.getValue())){
								interF.markInvalid('Seleccione un Intermediario Financiero por favor');
								return;
							}
						}else{
							var clienteSiac = Ext.getCmp('txtCliente');
							if(Ext.isEmpty(clienteSiac.getValue())){
								clienteSiac.markInvalid('Introduzca un N�mero de Cliente SIRAC');
								return;
							}
						}
						
					}
					boton.disable();
					boton.setIconClass('loading-indicator');
					var tBoton ="";
					var informacion = "";
					if(Ext.getCmp('cs_tipo_D').getValue()==true){
							tBoton = "Diario";	
							informacion = 'SaldoDiario';
							Ext.getCmp('ley2').show();
							Ext.getCmp('ley2').update("<left><FONT SIZE=2 >Saldos al d�a de hoy por la ma�ana</left>");


					}else if(Ext.getCmp('cs_tipo_M').getValue()==true){
							tBoton = "Mensual";	
							informacion = 'SaldoMensual';
							//Ext.getCmp('ley2').update("");
							
					}
          
          
					Ext.Ajax.request({
						url: '15saldoDiarioMensual.data.jsp',
						params:	Ext.apply(fp.getForm().getValues(),{
							intermediario: objGral.ic_if,
							intermOrig: Ext.getDom('strPerfil').value,
							informacion: informacion, 
							tipoBoton : tBoton
							
						}	),
						callback: procesarSuccessFailureInterfases
					});
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '15saldoDiarioMensual.jsp';
				}
				
			}
		]	
	});
			
//----------------------------Contenedor Principal------------------------------	
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			fpMensaje_1,
			muestraAyuda,
      muestraAyudaCredito,
			muestraAyudaMensual,
      muestraAyudaMensualCredito
			
		]
		});
		
//--------------------------------INICIO ----------------- -----------------------------------
	fp.el.mask('Cargando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '15saldoDiarioMensual.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});
	if(strTipoUsuario == "NAFIN" ){
	//	Ext.getCmp('comboInt').show();//cmbIntermediario
		Ext.getCmp('cmbIntermediario').show();
    Ext.getCmp("tipoLinea").show();
	}
	if(strTipoUsuario == "ADMIN IF" ){
	}
	
	Ext.getCmp('tipoLinea').hide();
	
	
});