<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.exception.NafinException,
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<% 
System.out.println("15avisosext.data.jsp (E)"); 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String txt_cliente = (request.getParameter("txt_cliente")!=null)?request.getParameter("txt_cliente"):"";
String rad_prestamos = (request.getParameter("rad_prestamos")!=null)?request.getParameter("rad_prestamos"):"";
String txt_num_prestamo = (request.getParameter("txt_num_prestamo")!=null)?request.getParameter("txt_num_prestamo"):"";
String infoRegresar ="", consulta ="";
int start = 0;
int limit = 0;

EstadoCuentaPyme BeanEstadoCuenta = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB",EstadoCuentaPyme.class);
if(txt_cliente.equals("")){
	String tipoUsuario = (String) request.getSession().getAttribute("strTipoUsuario");
	if(tipoUsuario.equals("PYME")) {
		String numeroSIRAC = BeanEstadoCuenta.getNumeroSIRAC(iNoCliente);
		   txt_cliente = numeroSIRAC;
		}
}
if (informacion.equals("valoresIniciales")) {
	
		JSONObject 	jsonObj	= new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("TIPOUSUARIO",strTipoUsuario);		
		jsonObj.put("PERFIL",strPerfil);		
			
		infoRegresar = jsonObj.toString();		
			
}else 	if (informacion.equals("busquedaAvanzada")) {
	
		String nombre = (request.getParameter("nombre")!=null)?request.getParameter("nombre"):"";
		String rfc = (request.getParameter("rfc")!=null)?request.getParameter("rfc"):"";
		String noNafinElec = (request.getParameter("noNafinElec")!=null)?request.getParameter("noNafinElec"):"";
		
		infoRegresar = 	BeanEstadoCuenta.busquedaAvanzaResumenPrestamos(nombre, rfc,  noNafinElec);
		
	
	} else  if (informacion.equals("Consulta") || 	informacion.equals("ArchivoPDF") ) {
	
	
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		String 	fechaActual = "", mesUltimo= "" , mesPenultimo ="", mesAntePenultimo ="" ;
		
		List meses = BeanEstadoCuenta.getFechasAviso();
		fechaActual = ((List)meses.get(0)).get(0).toString();		
			
		ConsPrestamosNafinCA paginador = new com.netro.cadenas.ConsPrestamosNafinCA();
		
		// Establecer la fecha del ultimo dia de los ultimos 3 meses
		List fechasUltimoDiaFinMes 	= BeanEstadoCuenta.getFechasAviso();
			if (fechasUltimoDiaFinMes != null ){
				List fechaUltimoDia 				= (List)fechasUltimoDiaFinMes.get(1);				
				paginador.setTxt_ultimo_dia_mes_ultimo(fechaUltimoDia.get(2).toString());
				
				fechaUltimoDia 					= (List)fechasUltimoDiaFinMes.get(2);				
				paginador.setTxt_ultimo_dia_mes_penultimo(fechaUltimoDia.get(2).toString());
				
				fechaUltimoDia 					= (List)fechasUltimoDiaFinMes.get(3);				
				paginador.setTxt_ultimo_dia_mes_antepenultimo(fechaUltimoDia.get(2).toString());
			}
				
		paginador.setRad_prestamos(rad_prestamos);
		paginador.setTxt_cliente(txt_cliente);
		paginador.setTxt_num_prestamo(txt_num_prestamo);		
		

		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		JSONObject 	resultado	= new JSONObject();
	
		if (informacion.equals("Consulta")) { 
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
							
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				
				/*String  consultar = queryHelper.getJSONPageResultSet(request,start,limit);	
				
				resultado = JSONObject.fromObject(consultar);
				*/
				Registros reg = new Registros();
				// Edito los registros 
				reg = queryHelper.getPageResultSet(request,start,limit);		
				// Editos los campos que se requien				
				while(reg.next()){
						String  aux = (reg.getString("FN_SALDO_INSOLUTO") == null) ? "" : reg.getString("FN_SALDO_INSOLUTO");
						reg.setObject("FN_SALDO_INSOLUTO",aux);
					for(int i=1;i<=3;i++) {
						List saldosFinMes = (List)meses.get(i);
						boolean haySaldo = false;
						if(i == 1)      {  
							haySaldo=(reg.getString("ESTADO_ULTIMO_MES").equals("true")) ?true:false; 
							if(haySaldo){
								reg.setObject("MESULTIMO",saldosFinMes.get(1).toString());
							}
						} 	else if(i == 2) {  
							haySaldo=(reg.getString("ESTADO_PENULTIMO_MES").equals("true")) ?true:false; 
							if(haySaldo){
								reg.setObject("MESPENULTIMO",saldosFinMes.get(1).toString());
							}						
						} else if(i == 3) {  
							haySaldo=(reg.getString("ESTADO_ANTEPENULTIMO_MES").equals("true"))?true:false; 
							if(haySaldo){
								reg.setObject("MESANTEPENULTIMO",saldosFinMes.get(1).toString());
							}
						}
					}								
			}	
			consulta =  "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + reg.getJSONData()+"}";
			
			resultado = JSONObject.fromObject(consulta);	
				
				
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}	
				
			resultado.put("TIPOUSUARIO",strTipoUsuario);
			resultado.put("FECHAACTUAL",fechaActual);						
		
			
			infoRegresar = resultado.toString();	
		
		} else 	if (informacion.equals("ArchivoPDF")) {
				try {
																								
					String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
					
					JSONObject jsonObj = new JSONObject();
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					infoRegresar = jsonObj.toString();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
		}	 
		
	}else if (informacion.equals("ResumenTotales")) {
				
		//Datos para el Resumen de Totales
		//Debe ser llamado despues de Consulta
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion
		
		System.out.println("infoRegresar  "+infoRegresar);
	
	}

%>
<%=infoRegresar%>

<%System.out.println("15avisosext.data.jsp (S)"); %>

