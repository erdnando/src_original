Ext.onReady(function(){
//----------------------------------FUNCIONES-----------------------------------
	var jsonValoresIniciales = null;
	
	var fechaHoy = "";
	
	function procesaValoresIniciales(opts, success, response) {
		fp.el.mask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
				jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
				if(jsonValoresIniciales != null){
					fp.el.unmask();
					
					Ext.getCmp('strTipoUsuario').setValue(jsonValoresIniciales.strTipoUsuario);
					if(jsonValoresIniciales.strTipoUsuario =='NAFIN')  {
						Ext.getCmp('compositefield1').show();
					}
				}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}
//----------------------------------HANDLERS------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('btnGenerarXpaginaPDF').setIconClass('icoPdf');				
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarConsultaData = function(store,arrRegistros,opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros!=null){
				if(!grid.isVisible()){
					grid.show();
				}
				var jsonData = store.reader.jsonData;				
				var btnGenerarXpaginaPDF = Ext.getCmp('btnGenerarXpaginaPDF');				
				var el = grid.getGridEl();
				var panel =	Ext.getCmp('Panel');				
			
				Ext.getCmp('lblpanel').update(jsonData.html2);
				Ext.getCmp('lblpanelFijo').update(jsonData.html1);
					
				if(store.getTotalCount()>0){
					
					Ext.getCmp('nombreCliente').setValue(jsonData.nombreCliente);
				
					resumenTotalesData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ResumenTotales'
						})
					});
					Ext.getCmp('gridTotales').show();				
					panel.show();					
					btnGenerarXpaginaPDF.enable();					
					el.unmask();
				} else {
					panel.hide();	
					Ext.getCmp('gridTotales').hide();		
					btnGenerarXpaginaPDF.disable();					
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
		}
	}
//-----------------------------------STORES-------------------------------------
	var consultaData = new Ext.data.JsonStore({
			root: 'registros',
			url: '15edocuentaext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
						{name: 'IG_NUMERO_PRESTAMO'},
						{name: 'CG_DESC_PRODUCTO_BANCO'},
						{name: 'DF_FECHA_APERTURA'},
						{name: 'DF_FECHA_VENCIMIENTO'},
						{name: 'FN_MONTO_INICIAL'},
						{name: 'FN_CAPITAL_VIGENTE'},
						{name: 'FN_CAPITAL_VENCIDO'},
						{name: 'FN_INTERES_VIGENTE'},
						{name: 'FN_INTERES_VENCIDO'},
						{name: 'FN_MORA100'},
						{name: 'CG_DESC_VALOR_TASA_CARTERA'},
						{name: 'FN_TASA_TOTAL'},
						{name: 'FN_SALDO_INSOLUTO'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
					load: procesarConsultaData,
					exception: {
								fn: function(proxy,type,action,optionsRequest,response,args){
										NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
										procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
								}
					}
			}
	});
	var resumenTotalesData = new Ext.data.JsonStore({
			root: 'registros',
			url: '15edocuentaext.data.jsp',
			baseParams: {
						informacion: 'ResumenTotales'
			},
			fields: [
						{name: 'NOMBRE_MONEDA'},
						{name: 'REGISTROS',type:'float'},
						{name: 'MONTO_INICIAL', type: 'float'},
						{name: 'CAPITAL_VIGENTE', type: 'float'},
						{name: 'CAPITAL_VENCIDO', type: 'float'},
						{name: 'INTERES_VIGENTE', type: 'float'},
						{name: 'INTERES_VENCIDO', type: 'float'},
						{name: 'INTERES_MORATORIO', type: 'float'},
						{name: 'SALDO_INSOLUTO', type: 'float'}
			],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
						exception: NE.util.mostrarDataProxyError
			}
	});
//---------------------------------ELEMENTOS------------------------------------

	var storeBusqAvanzada = new Ext.data.JsonStore({
		id: 'storeBusqAvanzada',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '15avisosext.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var busquedaAvanza = ['Utilice el *para b�squeda gen�rica'];
	
		//panel para mostrar  el texto 
	 var fpB = {
			xtype: 'panel',
			id: 'forma2',
			width: 600,			
			frame: true,		
			titleCollapse: false,
			bodyStyle: 'padding: 6px',			
			defaultType: 'textfield',
			align: 'center',
			html: busquedaAvanza.join('')
			};
	var elementsFormBusq = [
		
		{
			xtype: 'textfield',
			name: 'nombre',
			id: 'nombre1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfc',
			id: 'rfc1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},		
		{
			xtype: 'numberfield',
			name: 'noNafinElec',
			id: 'noNafinElec1',
			fieldLabel: 'N�mero Nafin Electr�nico',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'panel',
			items: [
		{
			xtype: 'button',
			width: 80,
			height:10,
			text: 'Buscar',
			iconCls: 'iconoLupa',
			id: 'btnBuscar',
			anchor: '',
			style: 'float:right',
			handler: function(boton, evento) {
				var nombre= Ext.getCmp("nombre1");
				var rfc= Ext.getCmp("rfc1");
				var noNafinElec= Ext.getCmp("noNafinElec1");
				if (Ext.isEmpty(nombre.getValue())  &&  Ext.isEmpty(rfc.getValue())  &&  Ext.isEmpty(noNafinElec.getValue()) ) {
						nombre.markInvalid('Es necesario que ingrese datos en al menos un campo');						
						return;
				}	
				var cbpyme = Ext.getCmp('cbpyme1');
				cbpyme.setValue('');
				cbpyme.setDisabled(false);				
				storeBusqAvanzada.load({
						params: Ext.apply(fpBusqAvanzada.getForm().getValues())
					});	
			}
		}
		
		]
		},
				
		{
			xtype: 'combo',
			name: 'cbpyme',
			id: 'cbpyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzada
		}
	];
	
	
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 'fBusqAvanzada',
		width: 400,
		title: 'B�squeda  Avanzada',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
				fpB, NE.util.getEspaciador(20), elementsFormBusq
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoBuscar',
				formBind: true,
				disabled: true,
				align: 'center',
				handler: function(boton, evento) {	
					var cbpyme= Ext.getCmp("cbpyme1");
					var txt_cliente = Ext.getCmp('txt_cliente1');
					
					if (Ext.isEmpty(cbpyme.getValue())) {
						cbpyme.markInvalid('Seleccione la Pyme');
						return;
					}
						
					var record = cbpyme.findRecord(cbpyme.valueField, cbpyme.getValue());
					record =  record ? record.get(cbpyme.displayField) : cbpyme.valueNotFoundText;					
					txt_cliente.setValue(cbpyme.getValue());
						
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();					
				}
				
			}
		]
	});
	
	var elementosForma = [
		{
			xtype: 'compositefield',
			fieldLabel: 'No. Cliente',			
			msgTarget: 'side',
			id: 'compositefield1',
			combineErrors: false,
			hidden: true,
			items: [
				{
					xtype: 'numberfield',
					name: 'txt_cliente',
					id: 'txt_cliente1',
					allowBlank: true,
					hidden: false,
					maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
				xtype: 'button',
				hidden: false,
				text: 'B�squeda  Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var cbpyme = Ext.getCmp('cbpyme1');
					cbpyme.setValue('');
					cbpyme.setDisabled(true);	
				
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								layout: 'fit',
								width: 400,
								height: 300,
								modal: true,								
								resizable:false,
								closeAction: 'destroy',
								closable:false,
								autoDestroy:true,	
								id: 'winBusqAvan',								
								items: [
									fpBusqAvanzada
								]
							}).show();
					}
				}
			}					
			]
		},
			{
				xtype: 'numberfield',
				name: 'ig_numero_prestamo',
				id: 'numero_Prestamo',
				fieldLabel: 'No. Pr�stamo',
				allowBlank: true,
				allowDecimals: false,
            allowNegative: false,
				decimalPrecision: 0,
				maxLength: 10,
				width: 100,
				minValue: 0
			},
			{ 	xtype: 'textfield',  hidden: true, id: 'nombreCliente', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'strTipoUsuario', 	value: '' }
			
	]; 
	var grid = new Ext.grid.GridPanel({
	id: 'gridPrincipal',
	style: ' margin:0 auto;',
	store: consultaData,
	title: '<center>DETALLE</center> ',
	hidden: true,
	columns: [
					{
						header: 'No. pr�stamo', 
						tooltip: 'No. pr�stamo',
						dataIndex: 'IG_NUMERO_PRESTAMO',
						sortable: true, 
						resiazable: true, 
						align: 'center',
						width: 150
					},
					{
						header: 'Descripci�n',
						tooltip: 'Descripci�n',
						dataIndex: 'CG_DESC_PRODUCTO_BANCO',
						sortable: true,
						resiazable: true,
						align: 'left',
						width: 150
					},
					{
						header: 'Fecha Operaci�n',
						tooltip: 'Fecha Operaci�n',
						dataIndex: 'DF_FECHA_APERTURA',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 150
					},
					{
						header: 'Fecha de Vencimiento del pr�stamo',
						tooltip: 'Fecha de Vencimiento del pr�stamo',
						dataIndex: 'DF_FECHA_VENCIMIENTO',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 150
					},
					{
						header: 'Monto Operado',
						tooltip: 'Monto Operado',
						dataIndex: 'FN_MONTO_INICIAL',
						sortable: true,
						resiazable: true,
						align: 'right',
						width: 150,
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						header: 'Capital Vigente',
						tooltip: 'Capital Vigente',
						dataIndex: 'FN_CAPITAL_VIGENTE',
						sortable: true,
						resiazable: true,
						align: 'right',
						width: 150,
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						header: 'Capital Vencido',
						tooltip: 'Capital Vencido',
						dataIndex: 'FN_CAPITAL_VENCIDO',
						sortable: true,
						resiazable: true,
						align: 'right',
						width: 150,
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						header: 'Inter�s Vigente',
						tooltip: 'Inter�s Vigente',
						dataIndex: 'FN_INTERES_VIGENTE',
						sortable: true,
						resiazable: true,
						align: 'right',
						width: 150,
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						header: 'Inter�s Vencido',
						tooltip: 'Inter�s Vencido',
						dataIndex: 'FN_INTERES_VENCIDO',
						sortable: true,
						resiazable: true,
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						header: 'Inter�s Moratorio',
						tooltip: 'Inter�s Moratorio',
						dataIndex: 'FN_MORA100',
						sortable: true,
						resiazable: true,
						align: 'right',
						width: 150,
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					},
					{
						header: 'Tipo Tasa',
						tooltip: 'Tipo Tasa',
						dataIndex: 'CG_DESC_VALOR_TASA_CARTERA',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 150
					},
					{
						header: 'Tasa Total',
						tooltip: 'Tasa Total',
						dataIndex: 'FN_TASA_TOTAL',
						sortable: true,
						resiazable: true,
						align: 'center',
						width: 150,
						renderer: Ext.util.Format.numberRenderer('0.0000%')
					},
					{
						header: 'Adeudo total al',
						tooltip: 'Adeudo total al d�a de hoy',
						dataIndex: 'FN_SALDO_INSOLUTO',
						sortable: true,
						resiazable: true,
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					}		
				],
	stripeRows: true,
	loadMask: true,
	height: 400,
	width: 900,	
	frame: true,
	bbar: {
				xtype: 'paging',
				pageSize: 15,
				buttonAlign: 'left',
				id: 'barraPaginacion',
				displayInfo: true,
				store: consultaData,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No hay registros.",
				items: [
							'->',	'-',
							{
								xtype: 'button',
								text: 'Imprimir PDF',
								iconCls: 'icoPdf',
								id: 'btnGenerarXpaginaPDF',
								handler: function(boton, evento) {									 
									boton.setIconClass('loading-indicator');
									var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
									Ext.Ajax.request({
									url: '15edocuentaext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
											informacion: 'ArchivoPaginaPDF',
											start: cmpBarraPaginacion.cursor,
											limit: cmpBarraPaginacion.pageSize,
											nombreCliente:Ext.getCmp('nombreCliente').getValue()
									}),
									callback: procesarDescargaArchivos
									});
								}
							},
							'-'
					]
			}		
});
	var gridTotales = {
		xtype: 'grid',
		style: ' margin:0 auto;',
		store: resumenTotalesData,
		id: 'gridTotales',
		hidden: true,
		columns: [
			    {
						header: 'TOTALES',
						dataIndex: 'NOMBRE_MONEDA',
						align: 'left',
						width: 150
				 },
				 {
						header: 'Registros',
						dataIndex: 'REGISTROS',
						width: 100,
						align: 'center'
				 },
				 {
						header: 'Monto Operado',
						dataIndex: 'MONTO_INICIAL',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
				 },
				 {
						header: 'Capital Vigente',
						dataIndex: 'CAPITAL_VIGENTE',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
				 },
				 {
						header: 'Capital Vencido',
						dataIndex: 'CAPITAL_VENCIDO',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
				  },
				  {
						header: 'Inter�s Vigente',
						dataIndex: 'INTERES_VIGENTE',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
				   },
				   {
						header: 'Inter�s Vencido',
						dataIndex: 'INTERES_VENCIDO',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
				   },
				   {
						header: 'Inter�s Moratorio',
						dataIndex: 'INTERES_MORATORIO',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
				   },
					{
						header: 'Adeudo Total al '+fechaHoy,
						dataIndex: 'SALDO_INSOLUTO',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
				   }
				],
			width: 900,
			height: 120,
			title: 'Totales',			
			frame: false
	};
	
	var PanelFijo = new Ext.Container({
		layout: 'table',		
		id: 'PanelFijo',							
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		items: [			
			{
				xtype: 'label',
				id: 'lblpanelFijo',
				style: 'font-weight:bold;text-align:justify;display:block;',
				fieldLabel: '',
				text: ''
			}		
		]
	});
	
	var Panel = new Ext.Container({
		layout: 'table',		
		id: 'Panel',							
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		items: [			
			{
				xtype: 'label',
				id: 'lblpanel',
				style: 'font-weight:bold;text-align:center;display:block;',
				fieldLabel: '',
				text: ''
			}			
		]
	});
	
	var fp = new Ext.form.FormPanel({
			id: 'forma',
			width: 500,
			style: ' margin:0 auto;',
			title: 'DETALLE',
			hidden: false,
			frame: true,
			collapsible: true,
			titleCollapse: false,
			bodyStyle: 'padding: 6px',
			labelWidth: 130,
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-50'
			},
			items: elementosForma,
			monitorValid: false,
			buttons: [
							{ 
								text: 'Consultar',
								id: 'btnColsultar',
								iconCls: 'icoBuscar',
								formBind: true,
								handler: function(boton, evento) {
									var numFieldPrestamo = Ext.getCmp('numero_Prestamo');
									if(numFieldPrestamo.getValue()>999999999)
									{
										numFieldPrestamo.markInvalid('El n�mero de prestamo debe ser menor a 10 d�gitos o dejarlo en blanco');
										numFieldPrestamo.focus();
										return;
									}
									fechaHoy=jsonValoresIniciales.fechaHoy;
									var encabezado = 'Adeudo total al ' + fechaHoy;
									grid.getColumnModel().setColumnHeader(12, encabezado);
									grid.hide();
									Ext.getCmp('PanelFijo').show();
									var totales = Ext.getCmp('gridTotales');
									if(totales.isVisible()){
											totales.hide();
									}
									var ventanaDoctos = Ext.getCmp('cambioDoctos');
									if (ventanaDoctos){
											ventanaDoctos.destroy();
									}
									
									if(Ext.getCmp('strTipoUsuario').getValue()=='NAFIN')  {
										var txt_cliente = Ext.getCmp("txt_cliente1");
										if (Ext.isEmpty(txt_cliente.getValue()) ) {
											txt_cliente.markInvalid('Debe especificar el n�mero de cliente');
											return;
										}			
									}
									
									
									fp.el.mask('Enviando...','x-mask-loading');
									consultaData.load({
										params: Ext.apply(fp.getForm().getValues(),{
												operacion: 'Generar',//Generar datos para la consulta
												start: 0,
												limit: 15
										})
									});
								}
							},
							{
								text: 'Limpiar',
								iconCls: 'icoLimpiar',
								handler: function(){
									window.location = '15edocuentaext.jsp'
								}
							}
			]
	});
//---------------------------------PRINCIPAL------------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			PanelFijo,
			Panel,
			grid,
			gridTotales
		]
	});
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '15edocuentaext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
});
});


