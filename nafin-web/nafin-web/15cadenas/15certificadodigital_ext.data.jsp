<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,
		netropology.utilerias.*,
		com.netro.seguridadbean.*,
		netropology.utilerias.usuarios.*,
		com.netro.pdf.*,
		com.netro.afiliacion.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
JSONObject jsonObj = new JSONObject();
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String tipoAfiliado = (request.getParameter("tipoAfiliado")!=null)?request.getParameter("tipoAfiliado"):"";
String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
String infoRegresar	=	"";


UtilUsr utilUsr = new UtilUsr();
//SeguridadHome seguridadHome = (SeguridadHome)ServiceLocator.getInstance().getEJBHome("SeguridadEJB",SeguridadHome.class);	
//com.netro.seguridadbean.Seguridad seguridad = seguridadHome.create();
com.netro.seguridadbean.Seguridad seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);

//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB",AfiliacionHome.class);	
//Afiliacion afiliacion = afiliacionHome.create();
Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

String cveAfiliado = strTipoUsuario.equals("NAFIN")?"N":(strTipoUsuario.equals("IF")?"I":(strTipoUsuario.equals("PYME")?"P":(strTipoUsuario.equals("EPO")?"E":"")));
String nafinElectronico = iNoNafinElectronico==null?"0":String.valueOf(iNoNafinElectronico);

if("E".equals(cveAfiliado) && "S".equals(sesEpoInternacional)){
	cveAfiliado = "EI";
}
if("P".equals(cveAfiliado)){
	if("D".equals(afiliacion.getTipoCliente(iNoCliente)) || "PD".equals(afiliacion.getTipoCliente(iNoCliente))){
		cveAfiliado = "D";
	}
}


System.out.println("iNoNafinElectronico == "+iNoNafinElectronico);
System.out.println("strTipoAfiliacion == "+strTipoAfiliacion);

if (informacion.equals("obtieneSerialCert")){
	String strSerialAct = seguridad.getSerialCertDigital(iNoUsuario);
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("strSerialAct", strSerialAct);
	jsonObj.put("opcion", opcion);
	infoRegresar = jsonObj.toString();
} else if (informacion.equals("obtieneInfoCertificado")){
	if("1".equals(opcion)){
		String fecVigenciaCert = seguridad.getFecVigenciaCertificado(iNoUsuario);
		jsonObj.put("success", Boolean.TRUE);
		jsonObj.put("fecVigenciaCert", fecVigenciaCert);
		jsonObj.put("opcion", opcion);
		infoRegresar = jsonObj.toString();
	}else if("2".equals(opcion)){
		Usuario usuario = utilUsr.getUsuario(iNoUsuario);
		String mail = usuario.getEmail();
		
		boolean bfolioVigente =  seguridad.validaFolioRevocacionVigente(iNoUsuario);
		jsonObj.put("success", Boolean.TRUE);
		jsonObj.put("mailUsuario", mail);
		jsonObj.put("bfolioVigente", new Boolean(bfolioVigente));
		jsonObj.put("opcion", opcion);
		infoRegresar = jsonObj.toString();
	}else if("3".equals(opcion)){
		Usuario usuario = utilUsr.getUsuario(iNoUsuario);
		String mail = usuario.getEmail();
		
		boolean bfolioVigente =  seguridad.validaFolioRevocacionVigente(iNoUsuario);
		jsonObj.put("success", Boolean.TRUE);
		jsonObj.put("mailUsuario", mail);
		jsonObj.put("bfolioVigente", new Boolean(bfolioVigente));
		jsonObj.put("opcion", opcion);
		infoRegresar = jsonObj.toString();
	}
}else if (informacion.equals("validaVigRevocacion")){
	
	boolean bfolioVigente =  seguridad.validaFolioRevocacionVigente(iNoUsuario);
	jsonObj.put("bfolioVigente", new Boolean(bfolioVigente));
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("solicitaRevocacion")){
	
	seguridad.generaFolioRevocacionCert(iNoUsuario, nafinElectronico, cveAfiliado);
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("confirmaRevocacion")){
	String password_nuevo = (request.getParameter("password_nuevo")!=null)?request.getParameter("password_nuevo"):"";
	boolean bFolio = seguridad.validaFolioCert(password_nuevo,iNoUsuario);
	if(bFolio){
		seguridad.removerCertificadoDigital(iNoUsuario, nafinElectronico, cveAfiliado);
		//strSerial = "";
		List pantallasNavegacionComplementaria = new ArrayList();
		if(strTipoUsuario.equals("NAFIN")) {
			
			if (esEsquemaExtJS) {
				session.setAttribute("sesMenu", MenuUsuario.getMenuNafin(strPerfil, "", appWebContextRoot));
				session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));
			}

		} else if(strTipoUsuario.equals("EPO")) {
			
			session.setAttribute("sesMenu", MenuUsuario.getMenuEpo(strPerfil, iNoCliente, "", appWebContextRoot));
			session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));
			
		} else if(strTipoUsuario.equals("IF")) {
			session.setAttribute("sesMenu", MenuUsuario.getMenuIF(strPerfil, iNoCliente, "", appWebContextRoot));
			session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));
	
		} else if(strTipoUsuario.equals("PYME")){
			session.setAttribute("sesMenu", MenuUsuario.getMenuPyme(strPerfil, iNoCliente, iNoEPO, "", appWebContextRoot));
			session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuPymeDirecto((String)session.getAttribute("sesMenu"), strPerfil));
			
		}
		
	}
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("bFolio", new Boolean(bFolio));
	infoRegresar = jsonObj.toString();
}

%>

<%=infoRegresar%>