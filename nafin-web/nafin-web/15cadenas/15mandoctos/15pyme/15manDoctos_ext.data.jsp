<%@ page language="java" %>  
<%@ page	import="
		java.util.*,java.text.*,netropology.utilerias.*,com.netro.cadenas.*,
		com.netro.mandatodoc.*,com.netro.pdf.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String tipoCadena = (request.getParameter("tipoCadena")!=null)?request.getParameter("tipoCadena"):"";
String claveIf = (request.getParameter("clave_if")!=null)?request.getParameter("clave_if"):"";
String clave_epo			=	(request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String infoRegresar	=	"";

iNoCliente = (String)session.getAttribute("iNoCliente");
  
System.out.println("iNoCliente " + iNoCliente);

//MandatoDocumentosHome mandatoDocumentosHome = (MandatoDocumentosHome)ServiceLocator.getInstance().getEJBHome("MandatoDocumentosEJB", MandatoDocumentosHome.class);
//MandatoDocumentos mandatoDocumentosBean = mandatoDocumentosHome.create();
MandatoDocumentos mandatoDocumentosBean = ServiceLocator.getInstance().lookup("MandatoDocumentosEJB",MandatoDocumentos.class);

if (informacion.equals("CatalogoIf")){

	if (tipoCadena != null && !tipoCadena.equals("")) {
      //FODEA 022-2012 : garellano
		List catalogoIF = mandatoDocumentosBean.getComboIfTipoCadena(tipoCadena, false);

		JSONArray jsonArr = new JSONArray();  
		Iterator it = catalogoIF.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar = "{\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString()+"}";
		
	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

} else if (informacion.equals("CatalogoDescontante")){
	if (claveIf != null && !claveIf.equals("")) {
		List catalogoDescontante = mandatoDocumentosBean.getComboDescontantes(claveIf);
		JSONArray jsonArr = new JSONArray();
		Iterator it = catalogoDescontante.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar = "{\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString()+"}";
	} else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

} else if (informacion.equals("Parametrizacion")){

	if (iNoCliente != null && !iNoCliente.equals("") ) {
		List eposA = new ArrayList();
		HashMap eposAfilia = mandatoDocumentosBean.obtenerEposAfiliadas(iNoCliente, "1");
		int numeroRegistro = Integer.parseInt((String)eposAfilia.get("numeroRegistros"));
		for (int i = 0; i < numeroRegistro; i++) {
			HashMap epoAfiliada = (HashMap)eposAfilia.get("epoAfiliada"+i);
			eposA.add(epoAfiliada);
		}
		List eposB = new ArrayList();
		HashMap eposAfiliadas = mandatoDocumentosBean.obtenerEposAfiliadas(iNoCliente, "2");
		int numeroRegistros = Integer.parseInt((String)eposAfiliadas.get("numeroRegistros"));
		for (int i = 0; i < numeroRegistros; i++) {
			HashMap epoAfiliada = (HashMap)eposAfiliadas.get("epoAfiliada"+i);
			eposB.add(epoAfiliada);
		}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("publica", eposA);
		jsonObj.put("privada", eposB);
		infoRegresar = jsonObj.toString();
	}else{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
} else if (informacion.equals("CatalogoEpoConsulta")){

			CatalogoEPO catalogo = new CatalogoEPO();
			catalogo.setCampoClave("ic_epo");
			catalogo.setCampoDescripcion("cg_razon_social");
			catalogo.setMandatoDocumentos("S");
			infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("CatalogoIfConsulta")){

	if(!clave_epo.equals("") && clave_epo != null){
		CatalogoIF catalogo = new CatalogoIF();
		catalogo.setCampoClave("ic_if");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setMandatoDocumentos("S");
		catalogo.setClaveEpo(clave_epo);
		infoRegresar = catalogo.getJSONElementos();

	} else{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
	
} else if (informacion.equals("CatalogoEstatusConsulta")){

		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus_man_doc");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("comcat_estatus_man_doc");
		catalogo.setOrden("ic_estatus_man_doc");
		infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("Aceptar")){
	String claveDescontante	=	(request.getParameter("claveDescontante")!=null)?request.getParameter("claveDescontante"):"";
	String stringClavesEpos	=	(request.getParameter("stringClavesEpos")!=null)?request.getParameter("stringClavesEpos"):"";
	String relacionPymeDescontante	=	"N";
	String rel_epo_pyme					=	"";
	String rel_epo_pyme_if				=	"";
	String rel_pyme_solic				=	"";
	
	if (tipoCadena != null && !tipoCadena.equals("")) {
			if (tipoCadena.equals("1"))	{
				List evaluaSolicPymeEpo = mandatoDocumentosBean.evaluaSolicitudDeMandatoPymeEpo(clave_epo, iNoCliente, claveIf);
				List evaluaSolicPymeIf = mandatoDocumentosBean.evaluaSolicitudDeMandatoPymeIf(clave_epo, iNoCliente, claveIf);
				List evaluaSolicPymeSolic = mandatoDocumentosBean.evaluaSolicitudDeMandatoPymeSolic(clave_epo, iNoCliente, claveIf);
		
				if (claveDescontante != null && !claveDescontante.equals("")) {
					relacionPymeDescontante = mandatoDocumentosBean.existeRelacionPymeDescontante(iNoCliente, clave_epo, claveDescontante);
				}
				if(!evaluaSolicPymeEpo.isEmpty()){
					rel_epo_pyme = "SI";
				}else{
					rel_epo_pyme = "NO";
				}
				if(!evaluaSolicPymeIf.isEmpty() || (claveDescontante != null && !claveDescontante.equals(""))){
					rel_epo_pyme_if = "SI";
				}else{
					rel_epo_pyme_if = "NO";
				}
				if(!evaluaSolicPymeSolic.isEmpty()){
					rel_pyme_solic = "SI";
				}
			} else if (tipoCadena.equals("2"))	{

				List evaluaSolicPymeEpo = new ArrayList();
				List evaluaSolicPymeIf = new ArrayList();
				List evaluaSolicPymeSolic = new ArrayList();
				StringTokenizer clavesEposTokenizer = new StringTokenizer(stringClavesEpos, ","); 
				String claveEpoToken = "";
				
				while (clavesEposTokenizer.hasMoreTokens()) {
					claveEpoToken = clavesEposTokenizer.nextToken();
					
					evaluaSolicPymeEpo = mandatoDocumentosBean.evaluaSolicitudDeMandatoPymeEpo(claveEpoToken, iNoCliente, claveIf);
					evaluaSolicPymeIf = mandatoDocumentosBean.evaluaSolicitudDeMandatoPymeIf(claveEpoToken, iNoCliente, claveIf);
					evaluaSolicPymeSolic = mandatoDocumentosBean.evaluaSolicitudDeMandatoPymeSolic(claveEpoToken, iNoCliente, claveIf);
					
					if (claveDescontante != null && !claveDescontante.equals("")) {
						relacionPymeDescontante = mandatoDocumentosBean.existeRelacionPymeDescontante(iNoCliente, claveEpoToken, claveDescontante);
					}
					if(!evaluaSolicPymeEpo.isEmpty()){
						rel_epo_pyme = "SI";
					}else{
						rel_epo_pyme = "NO";
					}
					if(!evaluaSolicPymeIf.isEmpty() || (claveDescontante != null && !claveDescontante.equals(""))){
						rel_epo_pyme_if = "SI";
					}else{
						rel_epo_pyme_if = "NO";
					}
					if(!evaluaSolicPymeSolic.isEmpty()){
						rel_pyme_solic = "SI";
					}
				}
		}

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("rel_epo_pyme", rel_epo_pyme);
		jsonObj.put("rel_epo_pyme_if", rel_epo_pyme_if);
		jsonObj.put("rel_pyme_solic", rel_pyme_solic);
		if (claveDescontante != null && !claveDescontante.equals("")) {
			jsonObj.put("relacionPymeDescontante", relacionPymeDescontante);
		}

		infoRegresar = jsonObj.toString();
	} else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
} else if (informacion.equals("confirmaClaveCesion")){
		String msgError			= "";
		String valCve				=	(request.getParameter("valCve") == null)?"N":request.getParameter("valCve");
		String claveDescontante	=	(request.getParameter("claveDescontante")!=null)?request.getParameter("claveDescontante"):"";
		String nombreEpo			=	(request.getParameter("nombreEpo")!=null)?request.getParameter("nombreEpo"):"";
		String nombreIf			=	(request.getParameter("nombreIf")!=null)?request.getParameter("nombreIf"):"";
		String nombreDescontante=	(request.getParameter("nombreDescontante")!=null)?request.getParameter("nombreDescontante"):"";
		String stringClavesEpos	=	(request.getParameter("stringClavesEpos")!=null)?request.getParameter("stringClavesEpos"):"";
		String usuarioCarga = iNoUsuario.trim()+" - "+strNombreUsuario.trim();
		String fechaAct = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		String horaAct = (new SimpleDateFormat ("hh:mm a")).format(new java.util.Date());
		
		JSONObject jsonObj = new JSONObject();
		String folioInstruccionIrrevocable = "";
		if("S".equals(valCve)){
			if (claveDescontante == null || claveDescontante.equals("")) {
				claveDescontante = claveIf;
				nombreDescontante = nombreIf;
			}
			if (tipoCadena.equals("1")) {
				folioInstruccionIrrevocable = mandatoDocumentosBean.insertSolicMandato(clave_epo, iNoCliente, claveIf, claveDescontante, tipoCadena, usuarioCarga);
			} else if (tipoCadena.equals("2")) {
				folioInstruccionIrrevocable = mandatoDocumentosBean.insertSolicMandato(stringClavesEpos, iNoCliente, claveIf, claveDescontante, tipoCadena, usuarioCarga);
			}
			ConsultaMandatoDocumentos pag = new ConsultaMandatoDocumentos();
			pag.setClaveTipoCadena(tipoCadena);
			pag.setNombreEpo(nombreEpo);
			pag.setNombreIf(nombreIf);
			pag.setNombreDescontante(nombreDescontante);
			pag.setFolioInstruccion(folioInstruccionIrrevocable);
			pag.setFechaAct(fechaAct);
			pag.setHoraAct(horaAct);
			pag.setUsuarioCarga(usuarioCarga);
			String nombreArchivo = pag.crearPageCustomFile(request, null,strDirectorioTemp, "AcusePDF");
			
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			jsonObj.put("folioInstruccionIrrevocable", folioInstruccionIrrevocable);
			jsonObj.put("fechaAct", fechaAct);
			jsonObj.put("horaAct", horaAct);
			jsonObj.put("iNoUsuario", iNoUsuario);
			jsonObj.put("strNombreUsuario", strNombreUsuario);
			jsonObj.put("esCveCorrecta",new Boolean(true));

		} else	{
			msgError = "La confirmaci�n de la clave y contrase�a es incorrecta, el proceso ha sido cancelado";
			jsonObj.put("esCveCorrecta",new Boolean(false));		
		}

		jsonObj.put("msgError",msgError);
		jsonObj.put("success",new Boolean(true));
		infoRegresar = jsonObj.toString();

} else if (informacion.equals("Consulta")){
	String clave_estatus = (request.getParameter("Estatus")!=null)?request.getParameter("Estatus"):"";
	String fecha_aceptacion_ini = (request.getParameter("fecha_aceptacion_ini")!=null)?request.getParameter("fecha_aceptacion_ini"):"";
	String fecha_aceptacion_fin = (request.getParameter("fecha_aceptacion_fin")!=null)?request.getParameter("fecha_aceptacion_fin"):"";
	String fecha_solicitud_ini = (request.getParameter("fecha_solicitud_ini")!=null)?request.getParameter("fecha_solicitud_ini"):"";
	String fecha_solicitud_fin = (request.getParameter("fecha_solicitud_fin")!=null)?request.getParameter("fecha_solicitud_fin"):"";

	int start = 0;
	int limit = 0;
	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	ConsultaMandatoDocumentos paginador = new ConsultaMandatoDocumentos();
	paginador.setClave_epo(clave_epo);
	paginador.setClave_estatus(claveIf);
	paginador.setClave_estatus(clave_estatus);
	paginador.setFecha_aceptacion_ini(fecha_aceptacion_ini);
	paginador.setFecha_aceptacion_fin(fecha_aceptacion_fin);
	paginador.setFecha_solicitud_ini(fecha_solicitud_ini);
	paginador.setFecha_solicitud_fin(fecha_solicitud_fin);
	paginador.setNumero_nafele(session.getAttribute("iNoNafinElectronico").toString());
	paginador.setRegistrosAux(null);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		Registros registros  = new Registros();
		Registros registrosEpos  = new Registros();
		registros = queryHelper.getPageResultSet(request,start,limit);
		registrosEpos = queryHelper.getPageResultSet(request,start,limit);
		
		List reg = new ArrayList();
		String epos = "";
		String folioAnterior = "";
		String folioActual = "";
		int rowspan = 0;
		while (registros.next()){
			folioActual = registros.getString("folio_instruccion");
			if (!folioActual.equals(folioAnterior)) {
				epos = "";
				//rowspan = 0;
				while (registrosEpos.next()) {
					if (folioActual.equals(registrosEpos.getString("folio_instruccion"))) {
						epos = epos + registrosEpos.getString("nombre_epo") + '\n';
					}
				}
				HashMap hash = new HashMap();
				hash.put("FOLIO_INSTRUCCION",	registros.getString("folio_instruccion"));
				hash.put("NOMBRE_EPO",	epos);
				hash.put("NOMBRE_IF",	registros.getString("nombre_if"));
				hash.put("NOMBRE_DESCONTANTE",	registros.getString("nombre_descontante"));
				hash.put("FECHA_SOLICITUD",	registros.getString("fecha_solicitud"));
				hash.put("FECHA_AUTORIZACION_IF",	registros.getString("fecha_autorizacion_if"));
				hash.put("MONTO_CREDITO",	registros.getString("monto_credito"));
				hash.put("FECHA_VENCIMIENTO",	registros.getString("fecha_vencimiento"));
				hash.put("BANCO_SERVICIO",	registros.getString("banco_servicio"));
				hash.put("CUENTA_BANCARIA",	registros.getString("cuenta_bancaria"));
				hash.put("TIPO_CUENTA",	registros.getString("tipo_cuenta"));
				hash.put("NUMERO_CREDITO",	registros.getString("numero_credito"));
				hash.put("MONEDA",	registros.getString("moneda"));
				hash.put("MONEDA_DESCUENTO",	registros.getString("moneda_descuento"));
				hash.put("OBSERVACIONES",	registros.getString("observaciones").replaceAll("\r", " "));
				hash.put("NUEVA_FECHA_VENCIMIENTO",	!registros.getString("nueva_fecha_vencimiento").trim().equals("")	?registros.getString("nueva_fecha_vencimiento")	:"NA");
				hash.put("FECHA_MODIFICACION",	!registros.getString("fecha_modificacion").trim().equals("")		?registros.getString("fecha_modificacion")		:"NA");
				hash.put("ESTATUS_SOLICITUD",	registros.getString("estatus_solicitud"));
				hash.put("USUARIO_SOLICITUD",	registros.getString("usuario_solicitud"));
				reg.add(hash);
				folioAnterior = folioActual;
			}
		}

		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(reg);
		infoRegresar = "{\"success\": true, \"total\": "+ queryHelper.getIdsSize() +" , \"registros\": " + jsObjArray.toString() + " }";

	} catch(Exception e) {	
		throw new AppException("Error en la paginacion", e);
	}
}  else if (informacion.equals("ResumenTotales")) {		//Datos para el Resumen de Totales
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

}	else if (informacion.equals("ParametroConsulta")){
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("numero_nafele", iNoNafinElectronico);
		infoRegresar = jsonObj.toString();

} else if (informacion.equals("ArchivoCSV")){
	String claveTipoCadena = (request.getParameter("claveTipoCadena")!=null)?request.getParameter("claveTipoCadena"):"";
	String claveEstatus = (request.getParameter("Estatus")!=null)?request.getParameter("Estatus"):"";
	String fechaAceptacionIni = (request.getParameter("fecha_aceptacion_ini")!=null)?request.getParameter("fecha_aceptacion_ini"):"";
	String fechaAceptacionFin = (request.getParameter("fecha_aceptacion_fin")!=null)?request.getParameter("fecha_aceptacion_fin"):"";
	String fechaSolicitudIni = (request.getParameter("fecha_solicitud_ini")!=null)?request.getParameter("fecha_solicitud_ini"):"";
	String fechaSolicitudFin = (request.getParameter("fecha_solicitud_fin")!=null)?request.getParameter("fecha_solicitud_fin"):"";
	String perfilUsuario = (request.getParameter("perfil_usuario")!=null)?request.getParameter("perfil_usuario"):"";
	try{
		JSONObject jsonObj = new JSONObject();
		CreaArchivo archivo = new CreaArchivo();
		HashMap parametrosConsulta = new HashMap();
		StringBuffer contenidoArchivo = new StringBuffer();
		List listaEpos = new ArrayList();
		int rowspan = 0;
		String folioActual = "";
		String folioAnterior = "";

		parametrosConsulta.put("claveTipoCadena", claveTipoCadena);
		parametrosConsulta.put("claveEpo", clave_epo);
		parametrosConsulta.put("claveIf", claveIf.trim());
		parametrosConsulta.put("numeroNafele", session.getAttribute("iNoNafinElectronico").toString());
		parametrosConsulta.put("claveEstatus", claveEstatus);
		parametrosConsulta.put("fechaSolicitudIni", fechaSolicitudIni);
		parametrosConsulta.put("fechaSolicitudFin", fechaSolicitudFin);
		parametrosConsulta.put("fechaAceptacionIni", fechaAceptacionIni);
		parametrosConsulta.put("fechaAceptacionFin", fechaAceptacionFin);
		parametrosConsulta.put("perfilUsuario", strPerfil);
		Registros registros = mandatoDocumentosBean.consultarIntruccionesIrrevocables(parametrosConsulta);

		parametrosConsulta = new HashMap();
		parametrosConsulta.put("claveTipoCadena", claveTipoCadena);
		parametrosConsulta.put("claveIf", claveIf.trim());
		parametrosConsulta.put("numeroNafele", session.getAttribute("iNoNafinElectronico").toString());
		parametrosConsulta.put("claveEstatus", claveEstatus);
		parametrosConsulta.put("fechaSolicitudIni", fechaSolicitudIni);
		parametrosConsulta.put("fechaSolicitudFin", fechaSolicitudFin);
		parametrosConsulta.put("fechaAceptacionIni", fechaAceptacionIni);
		parametrosConsulta.put("fechaAceptacionFin", fechaAceptacionFin);
		parametrosConsulta.put("perfilUsuario", perfilUsuario);
		
		Registros registrosEpos = mandatoDocumentosBean.consultarIntruccionesIrrevocables(parametrosConsulta);

		ConsultaMandatoDocumentos pag = new ConsultaMandatoDocumentos();
		pag.setRegistrosAux(registrosEpos);
		String nombreArchivo = pag.crearPageCustomFile(request, registros,strDirectorioTemp, "ConsultaCSV");

		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	
	} catch (Exception e) {
		throw new AppException("Error al generar el archivo", e);
	}	finally {
		}
} else if (informacion.equals("ArchivoPDF")){

	String claveTipoCadena = (request.getParameter("claveTipoCadena")!=null)?request.getParameter("claveTipoCadena"):"";
	String claveEstatus = (request.getParameter("Estatus")!=null)?request.getParameter("Estatus"):"";
	String fechaAceptacionIni = (request.getParameter("fecha_aceptacion_ini")!=null)?request.getParameter("fecha_aceptacion_ini"):"";
	String fechaAceptacionFin = (request.getParameter("fecha_aceptacion_fin")!=null)?request.getParameter("fecha_aceptacion_fin"):"";
	String fechaSolicitudIni = (request.getParameter("fecha_solicitud_ini")!=null)?request.getParameter("fecha_solicitud_ini"):"";
	String fechaSolicitudFin = (request.getParameter("fecha_solicitud_fin")!=null)?request.getParameter("fecha_solicitud_fin"):"";
	String perfilUsuario = (request.getParameter("perfil_usuario")!=null)?request.getParameter("perfil_usuario"):"";
	try{
		JSONObject jsonObj = new JSONObject();
		HashMap parametrosConsulta = new HashMap();
		StringBuffer contenidoArchivo = new StringBuffer();
		String epos = "";
		String folioActual = "";
		String folioAnterior = "";

		parametrosConsulta.put("claveTipoCadena", claveTipoCadena);
		parametrosConsulta.put("claveEpo", clave_epo);
		parametrosConsulta.put("claveIf", claveIf.trim());
		parametrosConsulta.put("numeroNafele", session.getAttribute("iNoNafinElectronico").toString());
		parametrosConsulta.put("claveEstatus", claveEstatus);
		parametrosConsulta.put("fechaSolicitudIni", fechaSolicitudIni);
		parametrosConsulta.put("fechaSolicitudFin", fechaSolicitudFin);
		parametrosConsulta.put("fechaAceptacionIni", fechaAceptacionIni);
		parametrosConsulta.put("fechaAceptacionFin", fechaAceptacionFin);
		parametrosConsulta.put("perfilUsuario", strPerfil);
		Registros registros = mandatoDocumentosBean.consultarIntruccionesIrrevocables(parametrosConsulta);

		parametrosConsulta = new HashMap();
		parametrosConsulta.put("claveTipoCadena", claveTipoCadena);
		parametrosConsulta.put("claveIf", claveIf.trim());
		parametrosConsulta.put("numeroNafele", session.getAttribute("iNoNafinElectronico").toString());
		parametrosConsulta.put("claveEstatus", claveEstatus);
		parametrosConsulta.put("fechaSolicitudIni", fechaSolicitudIni);
		parametrosConsulta.put("fechaSolicitudFin", fechaSolicitudFin);
		parametrosConsulta.put("fechaAceptacionIni", fechaAceptacionIni);
		parametrosConsulta.put("fechaAceptacionFin", fechaAceptacionFin);
		parametrosConsulta.put("perfilUsuario", perfilUsuario);
		
		Registros registrosEpos = mandatoDocumentosBean.consultarIntruccionesIrrevocables(parametrosConsulta);

		ConsultaMandatoDocumentos pag = new ConsultaMandatoDocumentos();
		pag.setRegistrosAux(registrosEpos);
		String nombreArchivo = pag.crearPageCustomFile(request, registros,strDirectorioTemp, "ConsultaPDF");
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

		infoRegresar = jsonObj.toString();
	
	} catch (Exception e) {
		throw new AppException("Error al generar el archivo", e);
	}	finally {
		}	

} else if (informacion.equals("AcusePyme")) {
	try{
		JSONObject jsonObj = new JSONObject();
		String folioSolicitud = (request.getParameter("numero_folio")!=null)?request.getParameter("numero_folio"):"";
		String nafinElectronicoPyme = "";
		String nombrePyme = "";
		String nombreEpo = "";
		String nombreIf = "";
		String nombreDescontante = "";
		String fechaSolicitud = "";
		String horaSolicitud = "";
		String usuarioSolicitud = "";
		
		HashMap informacionSolicitud = mandatoDocumentosBean.obtenerInformacionInstruccion(folioSolicitud);
		String tipoCadenaInstruccion = mandatoDocumentosBean.obtenerTipoCadenaInstruccion(folioSolicitud);
	  
		int numeroRegistros = Integer.parseInt((String)informacionSolicitud.get("numeroRegistros"));
		
		if (numeroRegistros > 0) {
			for (int i = 0; i < numeroRegistros; i++) {
				HashMap solicitud = (HashMap)informacionSolicitud.get("solicitud"+i);
				if (i == 0) {
					nafinElectronicoPyme = (String)solicitud.get("nafinElectronicoPyme");
					nombrePyme = (String)solicitud.get("nombrePyme");							
					nombreIf = (String)solicitud.get("nombreIf");
					nombreDescontante = (String)solicitud.get("nombreDescontante");
					fechaSolicitud = (String)solicitud.get("fechaSolicitud");
					horaSolicitud = (String)solicitud.get("horaSolicitud");
					usuarioSolicitud = (String)solicitud.get("usuarioSolicitud");
				}
				nombreEpo += (String)solicitud.get("nombreEpo");
				if (numeroRegistros > 1) {
					nombreEpo += "\n";
				}
			}
		}

		ConsultaMandatoDocumentos pag = new ConsultaMandatoDocumentos();
		pag.setClaveTipoCadena(tipoCadenaInstruccion);
		pag.setNombreEpo(nombreEpo);
		pag.setNombreIf(nombreIf);
		pag.setNombreDescontante(nombreDescontante);
		pag.setFolioInstruccion(folioSolicitud);
		pag.setFechaAct(fechaSolicitud);
		pag.setHoraAct(horaSolicitud);
		pag.setUsuarioCarga(usuarioSolicitud);
		String nombreArchivo = pag.crearPageCustomFile(request, null,strDirectorioTemp, "AcusePDF");

		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	
	} catch (Exception e) {
		throw new AppException("Error al generar el archivo", e);
	}	finally {
		}
}

%>
<%=infoRegresar%>