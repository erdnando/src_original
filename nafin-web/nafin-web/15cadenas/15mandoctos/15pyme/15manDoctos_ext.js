var varGlobal = {
	claveEpo:null,
	nombre_epo:null,
	claveIf:null,
	nombre_if:null,
	claveDescontante:null,
	nombreDesconta:null,
	tipoCadena:null,
	stringClavesEpos:null
}

var textoPublico = '<DIV align="left"><center><b>INSTRUCCI�N IRREVOCABLE</b></center><br/>'+
										'Para efectos de las presentes instrucciones irrevocables se entiende por:<br/><br/>'+
										'<b>CADENAS PRODUCTIVAS</b>: Productos y servicios a trav�s de un sistema en Internet, desarrollado por <b>NAFIN</b>, para, entre otras cosas consultar informaci�n, intercambiar datos, enviar mensajes de datos y realizar transacciones financieras.'+ 
																					'<br/><br/>'+
										'<b>CONTRATO</b>: Instrumento jur�dico formalizado entre una <b>DEPENDENCIA O ENTIDAD</b> y el <b>SUJETO DE APOYO</b>, con objeto de que �ste le provea determinados bienes o servicios o realice obra p�blica.<br/><br/>'+
										'<b>CR�DITO</b>: Financiamiento otorgado por el <b>INTERMEDIARIO FINANCIERO</b> al <b>SUJETO DE APOYO</b>, en relaci�n con un <b>CONTRATO</b> hasta por el 50% (cincuenta por ciento) del monto del mismo.<br/><br/>'+
										'<b>CUENTA BANCARIA</b>: La aperturada por el <b>SUJETO DE APOYO</b> con el <b>INTERMEDIARIO FINANCIERO</b>, con el fin de que en �sta '+
																				'se realice el dep�sito de las disposiciones del <b>CR�DITO</b> otorgado por el <b>INTERMEDIARIO FINANCIERO</b> al '+
																				'<b>SUJETO DE APOYO</b>, as� como la totalidad de los recursos que tenga derecho a recibir el <b>SUJETO DE APOYO</b>'+
																				'como contraprestaci�n por las operaciones de <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b>, as� como '+
																				'para que el <b>INTERMEDIARIO FINANCIERO</b> realice los cargos para el pago del <b>CR�DITO</b> y sus '+
																				'accesorios, de conformidad a lo se�alado en este instrumento y en el contrato de cr�dito correspondiente.<br/><br/>'+
										'<b>DEPENDENCIA O ENTIDAD</b>:	Dependencias y Entidades de la Administraci�n P�blica Federal centralizada y paraestatal que en <b>CADENAS PRODUCTIVAS</b> '+
										'										tienen el car�cter de <b>EMPRESA DE PRIMER ORDEN</b>, y que tengan celebrado un <b>CONTRATO</b> con el <b>SUJETO DE APOYO.</b><br/><br/>'+
										'<b>DOCUMENTOS</b>:	Contrarrecibos o cualquier otro instrumento en papel o en cualquier medio electr�nico, que emite la '+
										'										<b>DEPENDENCIA O ENTIDAD</b> con motivo de uno o m�s <b>CONTRATOS</b>, en los cuales '+
										'										se hacen constar derechos de cr�dito a su cargo y en favor del <b>SUJETO DE APOYO</b>.<br/><br/>'+
										'<b>DESCONTANTE</b>: Entidad financiera, del mismo grupo financiero del que forma parte el <b>INTERMEDIARIO FINANCIERO</b> que,'+
										'										de conformidad a la <b>INSTRUCCI�N IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b>,'+
										'										realizar� las operaciones de <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b> con el <b>SUJETO '+
										'										DE APOYO</b> a trav�s de <b>CADENAS PRODUCTIVAS</b>.<br/><br/>'+
										'<b>EMPRESA DE PRIMER ORDEN</b>: Sociedades mercantiles, Dependencias o Entidades de la Administraci�n P�blica Federal, Gobiernos de los '+
										'										Estados y Municipios, que reciben bienes o servicios u obra p�blica y que tinen el car�cter de <b>EMPRESAS DE PRIMER '+
										'										ORDEN</b> en <b>CADENAS PRODUCTIVAS</b>.<br/><br/>                  '+
										'<b>FACTORAJE O DESCUENTO ELECTR�NICO</b>: Operaciones que se realizan en forma autom�tica, a trav�s de <b>CADENAS PRODUCTIVAS</b> una vez que el '+
										'										<b>SUJETO DE APOYO</b> haya otorgado la <b>INSTRUCCI�N IRREVOCABLE NAFIN</b> y la <b>INSTRUCCION '+
										'										IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b>; mediante las cuales el '+
										'										<b>INTERMEDIARIO FINANCIERO</b> o el <b>DESCONTANTE</b> adquiere la propiedad de los derechos de cr�dito que '+
										'										se hacen constar en los <b>DOCUMENTOS</b> sin necesidad de que se otorgue para cada operaci�n el '+
										'										consentimiento del <b>SUJETO DE APOYO</b>.<br/><br/>                  '+
										'<b>INSTRUCCI�N IRREVOCABLE</b>: Es la otorgada por el <b>SUJETO DE APOYO</b> a <b>NAFIN</b> para el efecto de que <b>NAFIN</b> por cuenta y orden del <b>SUJETO DE '+
										'										APOYO</b>, active en <b>CADENAS PRODUCTIVAS</b> �nicamente al <b>INTERMEDIARIO FINANCIERO</b> o '+
										'										<b>DESCONTANTE</b>, a efecto de que sea �ste exclusivamente quien realice el <b>FACTORAJE O DESCUENTO '+
										'										ELECTR�NICO AUTOM�TICO</b> de la totalidad de los <b>DOCUMENTOS</b> emitidos por la <b>DEPENDENCIA O ENTIDAD</b>'+
										'										con la que el <b>SUJETO DE APOYO</b> celebr� el <b>CONTRATO</b> relacionado con el <b>CR�DITO</b>, '+
										'										a�n cuando dichos <b>DOCUMENTOS</b> no hayan derivado de un <b>CONTRATO</b> relacionado con un <b>CR�DITO</b>,'+
										'										as� como para que <b>NAFIN</b> d� a conocer exclusivamente al <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b>,'+
										'										a trav�s de <b>CADENAS PRODUCTIVAS</b> todos los <b>DOCUMENTOS</b> que han sido publicados por dicha <b>DEPENDENCIA'+
										'										O ENTIDAD</b>.<br/><br/>																			'+
										'										En el entendido de que la instrucci�n irrevocable implica que con los recursos del <b>FACTORAJE O '+
										'										DESCUENTO ELECTR�NICO AUTOM�TICO</b> se pueda pagar el <b>CR�DITO</b> que otorg� el <b>INTERMEDIARIO '+
										'										FINANCIERO</b> al <b>SUJETO DE APOYO</b>.<br/><br/>																			'+
										'<b>INSTRUCCI�N IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b>: Es la otorgada por el <b>SUJETO DE APOYO</b> al <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> para que '+
										'										opere en <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b>, en <b>CADENAS PRODUCTIVAS</b>, la '+
										'										totalidad de los <b>DOCUMENTOS</b> publicados por la <b>DEPENDENCIA O ENTIDAD</b> que se�ale el <b>SUJETO DE '+
										'										APOYO</b> al otorgar la instrucci�n, as� como para que el <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> '+
										'										deposite en la <b>CUENTA BANCARIA</b> los recursos que tenga derecho a recibir el <b>SUJETO DE APOYO</b> '+
										'										como contraprestaci�n por las operaciones de <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b>, '+
										'										adem�s para que dichos recursos puedan ser aplicados por el <b>INTERMEDIARIO FINANCIERO</b> al pago del '+
										'										<b>CR�DITO</b> y sus accesorios.<br/><br/>                 																	'+
										'<b>INTERMEDIARIO FINANCIERO</b>: Instituci�n que otorgue cr�dito al <b>SUJETO DE APOYO</b> relacionado con un <b>CONTRATO</b> y en su caso, autorizada '+
										'										para realizar operaciones de <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b> con el <b>SUJETO '+
										'										DE APOYO</b> a trav�s de <b>CADENAS PRODUCTIVAS</b>.<br/><br/>'+
										'<b>L�NEA DE FINANCIAMIENTO</b>: Recursos que el <b>INTERMEDIARIO FINANCIERO</b> pone a disposici�n del <b>SUJETO DE APOYO</b> para ser ejercidos'+
										'										de conformidad con lo se�alado en el convenio y al <b>CR�DITO</b>.<br/><br/>'+
										'<b>REDESCUENTO ELECTR�NICO</b>: Acto por el cual el <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b>, transmite bajo su responsabilidad a '+
										'										<b>NAFIN</b>, el mensaje de datos para solicitar a <b>NAFIN</b> recursos para su operaci�n.<br/><br/>          '+
										'<b>SUJETO DE APOYO</b>: Micro, peque�as y medianas empresas y personas f�sicas con actividad empresarial que se encuentren dentro de'+
										'										los rangos fijados por la Secretar�a de Econom�a o que �sta llegue a establecer, as� como en los par�metros '+
										'										establecidos por <b>NAFIN</b> en sus diversas circulares y todas aquellas personas morales que sean '+
										'										proveedores de una <b>DEPENDENCIA O ENTIDAD</b>, que cuenten con un <b>CR�DITO</b> otorgado '+
										'										por el <b>INTERMEDIARIO FINANCIERO</b> y que hayan otorgado la <b>INSTRUCCI�N IRREVOCABLE NAFIN</b> y la '+
										'										<b>INSTRUCCI�N IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b>.<br/><br/>'+
										'<b><center>INSTRUCCI�N IRREVOCABLE NAFIN</center></b><br/>'+
										'Que en este acto otorga el <b>SUJETO DE APOYO</b> a Nacional Financiera, S.N.C. Instituci�n de Banca de Desarrollo (<b>NAFIN</b>), derivada del <b>CR�DITO</b> autorizado '+
										'por parte del <b>INTERMEDIARIO FINANCIERO</b>, relacionado con el <b>CONTRATO</b> y que dicha instrucci�n aceptar� <b>NAFIN</b>, a efecto de que:<br/><br/>'+
										''+
										'<dd/>a)	<b>NAFIN</b> por cuenta y orden del <b>SUJETO DE APOYO</b>, active �nicamente en <b>CADENAS PRODUCTIVAS</b> al <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> '+
										'para el efecto de que sea �ste exclusivamente quien realice el <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b> de todos y cada uno de los <b>DOCUMENTOS</b> '+
										'en que consten derechos de cr�dito a favor del <b>SUJETO DE APOYO</b> emitidos por la <b>DEPENDENCIA O ENTIDAD</b> con la que tenga celebrado el <b>CONTRATO</b> relacionado '+
										'con el <b>CR�DITO</b>, a�n cuando dichos <b>DOCUMENTOS</b> no hayan derivado de un <b>CONTRATO</b> relacionado con un <b>CR�DITO</b>.<br/><br/>'+
										''+
										'<dd/>b)	<b>NAFIN</b> d� a conocer exclusivamente al <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b>, que haya elegido el <b>SUJETO DE APOYO</b>, los <b>DOCUMENTOS</b> '+
										'que han sido publicados por la <b>DEPENDENCIA O ENTIDAD</b> correspondiente en las <b>CADENAS PRODUCTIVAS</b>, de los que sea beneficiario el <b>SUJETO DE APOYO</b>.<br/><br/> '+
										''+
										'<dd/>c)	<b>NAFIN</b> d� a conocer al <b>INTERMEDIARIO FINANCIERO</b> y/o al <b>DESCONTATARIO</b> a trav�s de <b>CADENAS PRODUCTIVAS</b> los <b>DOCUMENTOS</b> y montos '+
										'que se encuentren publicados en dicho sistema de <b>CADENAS PRODUCTIVAS</b>.<br/><br/> '+
										''+
										'Lo anterior para el efecto de que el <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> se encuentre en facultad de operar en <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b>, '+
										'en <b>CADENAS PRODUCTIVAS</b>, los derechos de cobro o cr�dito a favor del <b>SUJETO DE APOYO</b> que otorga la presente instrucci�n y que se hacen constar en los <b>DOCUMENTOS</b> y, '+
										'que los recursos que deba recibir el <b>SUJETO DE APOYO</b> como contraprestaci�n por las operaciones de <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b> los deposite en la '+
										'<b>CUENTA BANCARIA</b>, manifestando en este acto, que dichos recursos podr�n ser aplicados por el <b>INTERMEDIARIO FINANCIERO</b> al pago del <b>CR�DITO</b> y sus accesorios.<br/><br/>'+
										''+
										'Esta instrucci�n irrevocable no podr� ser extinguida o modificada por el <b>SUJETO DE APOYO</b> mientras exista cualquier saldo pendiente de pago al <b>INTERMEDIARIO FINANCIERO</b>, '+
										'y solo podr� ser revocada con la autorizaci�n expresa del mismo.<br/><br/>'+
										''+
										'Asimismo, como <b>SUJETO DE APOYO</b> manifiesto mi conformidad para que <b>NAFIN</b> se reserve el derecho de no proveer al amparo de esta instrucci�n el servicio de Cadenas Productivas en caso de incumplimiento '+
										'del <b>SUJETO DE APOYO</b> con el <b>INTERMEDIARIO FINANCIERO</b>.<br/><br/>'+
										''+
										'En este acto como <b>SUJETO DE APOYO</b> me obligo a no modificar la forma de pago y a no solicitar mi baja de <b>CADENAS PRODUCTIVAS</b>, mientras exista cualquier saldo pendiente de pago al '+
										'<b>INTERMEDIARIO FINANCIERO</b>, sin la autorizaci�n expresa de �ste.<br/><br/>'+
										''+
										'Me obligo a no ceder los derechos de cobro de ning�n <b>CONTRATO</b> adjudicado por la <b>DEPENDENCIA O ENTIDAD</b> seleccionada al otorgar esta instrucci�n, a persona alguna distinta del '+
										'<b>INTERMEDIARIO FINANCIERO o DESCONTANTE</b> durante la vigencia del <b>CR�DITO</b><br/><br/>'+
										''+
										'Al transmitir el mensaje de datos que contiene esta instrucci�n irrevocable, manifiesto mi conformidad con la misma, para todos los efectos legales correspondientes, '+
										'liberando a <b>NAFIN</b> de cualquier responsabilidad por su aceptaci�n y cumplimiento.<br/><br/>'+
										''+
										'<center><b>INSTRUCCI�N IRREVOCABLE - INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b></center><br/><br/>'+
										''+
										'Que en este acto otorga el <b>SUJETO DE APOYO</b> al <b>INTERMEDIARIO FINANCIERO o DESCONTANTE</b>, derivado del <b>CR�DITO</b> que le otorg� el <b>INTERMEDIARIO FINANCIERO</b>, a efecto de que:<br/><br/>'+
										''+
										'<dd/>a)	El <b>INTERMEDIARIO FINANCIERO</b> o el <b>DESCONTANTE</b> opere en <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b>, en <b>CADENAS PRODUCTIVAS</b> todos y cada uno de los <b>DOCUMENTOS</b> publicados por la <b>DEPENDENCIA O ENTIDAD</b>'+
										'con la que el <b>SUJETO DE APOYO</b> tenga celebrado el <b>CONTRATO</b> relacionado con el <b>CR�DITO</b>, a�n cuando dichos <b>DOCUMENTOS</b> no hayan derivado de un <b>CONTRATO</b> relacionado con un <b>CR�DITO</b>.<br/><br/>                  '+
										'<dd/>b)	El <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> deposite los recursos que deba recibir el <b>SUJETO DE APOYO</b> como contraprestaci�n por el  <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b> en la <b>CUENTA BANCARIA</b>, '+
										'en el entendido de que dichos recursos podr�n ser aplicados por el <b>INTERMEDIARIO FINANCIERO</b> al pago del <b>CR�DITO</b> y sus accesorios.<br/><br/>                  '+
										'Lo anterior no exime al <b>SUJETO DE APOYO</b> de continuar pagando el <b>CR�DITO</b>, m�s sus accesorios, en las fechas convenidas con el <b>INTERMEDIARIO FINANCIERO</b>.<br/><br/>                  '+
										'<dd/>c)	El <b>INTERMEDIARIO FINANCIERO</b> proporcione a <b>NAFIN</b> y al <b>DESCONTANTE</b>, en su caso,  las caracter�sticas del <b>CR�DITO</b> que me fue otorgado como <b>SUJETO DE APOYO</b> (cuenta, monto, fecha de vencimiento, banco para dep�sito etc.).<br/><br/> '+
										'Esta instrucci�n irrevocable no podr� ser extinguida o modificada por el <b>SUJETO DE APOYO</b> mientras exista cualquier saldo pendiente de pago al <b>INTERMEDIARIO FINANCIERO</b>, y solo podr� ser revocada con la autorizaci�n expresa del mismo.<br/><br/>					'+			
										'En este acto como <b>SUJETO DE APOYO</b> me obligo a no modificar la forma de pago y a no solicitar mi baja de <b>CADENAS PRODUCTIVAS</b>, mientras exista cualquier saldo pendiente de pago al <b>INTERMEDIARIO FINANCIERO</b>, sin la autorizaci�n expresa de �ste.<br/><br/>'+
										'Me obligo como <b>SUJETO DE APOYO</b> a no ceder los derechos de cobro de ning�n <b>CONTRATO</b> adjudicado por la <b>DEPENDENCIA O ENTIDAD</b> seleccionada al otorgar esta instrucci�n, a persona alguna distinta del <b>INTERMEDIARIO FINANCIERO o DESCONTANTE</b> durante la vigencia del <b>CR�DITO</b>.<br/><br/>'+
										'Al transmitir el mensaje de datos que contiene esta instrucci�n irrevocable, manifiesto mi conformidad con la misma, para todos los efectos legales correspondientes, liberando al <b>INTERMEDIARIO FINANCIERO</b> y/o al <b>DESCONTANTE</b> de cualquier responsabilidad por su aceptaci�n y cumplimiento.<br/><br/></DIV>';
var textoPrivado = '	<DIV align="left">'+
										'<center><b>INSTRUCCI�N IRREVOCABLE</b></center><br/>'+
										'Para efectos de las presentes instrucciones irrevocables se entiende por:<br/><br/>'+
										'<b>CADENAS PRODUCTIVAS</b>: Productos y servicios a trav�s de un sistema en Internet, desarrollado por <b>NAFIN</b>, para, entre otras cosas consultar informaci�n, intercambiar datos, enviar mensajes de datos y realizar transacciones financieras. '+
										'											<br/><br/>'+
										'<b>CR�DITO</b>: Financiamiento otorgado por el <b>INTERMEDIARIO FINANCIERO</b> al <b>SUJETO DE APOYO</b>.<br/><br/>'+
										'<b>CUENTA BANCARIA</b>: La aperturada por el <b>SUJETO DE APOYO</b> con el <b>INTERMEDIARIO FINANCIERO</b>, con el fin de que en �sta '+
										'										se realice el dep�sito de las disposiciones del <b>CR�DITO</b> otorgado por el <b>INTERMEDIARIO FINANCIERO</b> al '+
										'										<b>SUJETO DE APOYO</b>, as� como la totalidad de los recursos que tenga derecho a recibir el <b>SUJETO DE APOYO</b>'+
										'										como contraprestaci�n por las operaciones de <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b>, as� como '+
										'										para que el <b>INTERMEDIARIO FINANCIERO</b> realice los cargos para el pago del <b>CR�DITO</b> y sus '+
										'										accesorios, de conformidad a lo se�alado en este instrumento y en el contrato de cr�dito correspondiente.<br/><br/>'+
										'<b>DOCUMENTOS</b>:	Contrarrecibos o cualquier otro instrumento en papel o en cualquier medio electr�nico, que emite la '+
										'										<b>EMPRESA DE PRIMER ORDEN</b>, en los cuales se hacen constar derechos de cr�dito a su cargo y en favor del <b>SUJETO DE APOYO</b>.<br/><br/>'+
										'<b>DESCONTANTE</b>: Entidad financiera, del mismo grupo financiero del que forma parte el <b>INTERMEDIARIO FINANCIERO</b> que, '+
										'										de conformidad a la <b>INSTRUCCI�N IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b>,'+
										'										realizar� las operaciones de <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b> con el <b>SUJETO '+
										'										DE APOYO</b> a trav�s de <b>CADENAS PRODUCTIVAS</b>.<br/><br/>'+
										'<b>EMPRESA DE PRIMER ORDEN</b>: Sociedad mercantil, que recibe bienes o servicios del <b>SUJETO DE APOYO</b>.<br/><br/>                  '+
										'<b>FACTORAJE O DESCUENTO ELECTR�NICO</b>: Operaciones que se realizan en forma autom�tica, a trav�s de <b>CADENAS PRODUCTIVAS</b> una vez que el '+
										'										<b>SUJETO DE APOYO</b> haya otorgado la <b>INSTRUCCI�N IRREVOCABLE NAFIN</b> y la <b>INSTRUCCION '+
										'										IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b>; mediante las cuales el '+
										'										<b>INTERMEDIARIO FINANCIERO</b> o el <b>DESCONTANTE</b> adquiere la propiedad de los derechos de cr�dito que '+
										'										se hacen constar en los <b>DOCUMENTOS</b> sin necesidad de que se otorgue para cada operaci�n el '+
										'										consentimiento del <b>SUJETO DE APOYO</b>.<br/><br/>                  '+
										'<b>INSTRUCCI�N IRREVOCABLE NAFIN</b>: Es la otorgada por el <b>SUJETO DE APOYO</b> a <b>NAFIN</b> para el efecto de que <b>NAFIN</b> por cuenta y orden del <b>SUJETO DE '+
										'										APOYO</b>, active en <b>CADENAS PRODUCTIVAS</b> �nicamente al <b>INTERMEDIARIO FINANCIERO</b> o '+
										'										<b>DESCONTANTE</b>, a efecto de que sea �ste exclusivamente quien realice el <b>FACTORAJE O DESCUENTO '+
										'										ELECTR�NICO AUTOM�TICO</b> de la totalidad de los <b>DOCUMENTOS</b> emitidos por la <b>EMPRESA DE PRIMER ORDEN</b>, '+
										'										as� como para que <b>NAFIN</b> d� a conocer exclusivamente al <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b>,'+
										'										a trav�s de <b>CADENAS PRODUCTIVAS</b> todos los <b>DOCUMENTOS</b> que han sido publicados por dicha <b>EMPRESA DE PRIMER ORDEN</b>.<br/><br/>'+
										'										En el entendido de que la instrucci�n irrevocable implica que con los recursos del <b>FACTORAJE O '+
										'										DESCUENTO ELECTR�NICO AUTOM�TICO</b> se pueda pagar el <b>CR�DITO</b> que otorg� el <b>INTERMEDIARIO '+
										'										FINANCIERO</b> al <b>SUJETO DE APOYO</b>.<br/><br/>'+
										'<b>INSTRUCCI�N IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b>: Es la otorgada por el <b>SUJETO DE APOYO</b> al <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> para que '+
										'										opere en <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b>, en <b>CADENAS PRODUCTIVAS</b>, la '+
										'										totalidad de los <b>DOCUMENTOS</b> publicados por la <b>EMPRESA DE PRIMER ORDEN</b> que se�ale el <b>SUJETO DE '+
										'										APOYO</b> al otorgar la instrucci�n, as� como para que el <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> '+
										'										deposite en la <b>CUENTA BANCARIA</b> los recursos que tenga derecho a recibir el <b>SUJETO DE APOYO</b> '+
										'										como contraprestaci�n por las operaciones de <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b>, '+
										'										adem�s para que dichos recursos puedan ser aplicados por el <b>INTERMEDIARIO FINANCIERO</b> al pago del '+
										'										<b>CR�DITO</b> y sus accesorios.<br/><br/>'+
										'<b>INTERMEDIARIO FINANCIERO</b>: Instituci�n que otorgue cr�dito al <b>SUJETO DE APOYO</b> relacionado con un <b>CONTRATO</b> y en su caso, autorizada '+
										'										para realizar operaciones de <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b> con el <b>SUJETO '+
										'										DE APOYO</b> a trav�s de <b>CADENAS PRODUCTIVAS</b>.<br/><br/>'+
										'<b>REDESCUENTO ELECTR�NICO</b>: Acto por el cual el <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b>, transmite bajo su responsabilidad a '+
										'										<b>NAFIN</b>, el mensaje de datos para solicitar a <b>NAFIN</b> recursos para su operaci�n.<br/><br/>'+
										'<b>SUJETO DE APOYO</b>: Micro, peque�as y medianas empresas y personas f�sicas con actividad empresarial que se encuentren dentro de '+
										'										los rangos fijados por la Secretar�a de Econom�a o que �sta llegue a establecer, as� como en los par�metros '+
										'										establecidos por <b>NAFIN</b> en sus diversas circulares y todas aquellas personas morales que sean '+
										'										proveedores de una <b>EMPRESA DE PRIMER ORDEN</b>, que cuenten con un <b>CR�DITO</b> otorgado '+
										'										por el <b>INTERMEDIARIO FINANCIERO</b> y que hayan otorgado la <b>INSTRUCCI�N IRREVOCABLE NAFIN</b> y la '+
										'										<b>INSTRUCCI�N IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b>.<br/><br/>'+
										''+
										'<b><center>INSTRUCCI�N IRREVOCABLE NAFIN</center></b><br/>'+
										''+
										'Que en este acto otorga el <b>SUJETO DE APOYO</b> a Nacional Financiera, S.N.C. Instituci�n de Banca de Desarrollo (<b>NAFIN</b>), derivada del <b>CR�DITO</b> '+
										'y que aceptada por <b>NAFIN</b>, a efecto de que:<br/><br/>'+
										''+
										'<dd/>a)	<b>NAFIN</b> por cuenta y orden del <b>SUJETO DE APOYO</b>, active �nicamente en <b>CADENAS PRODUCTIVAS</b> al <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> '+
										'para el efecto de que sea �ste exclusivamente quien realice el <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b> de todos y cada uno de los <b>DOCUMENTOS</b> '+
										'en que consten derechos de cr�dito a favor del <b>SUJETO DE APOYO</b> emitidos por la <b>EMPRESA DE PRIMER ORDEN</b>.<br/><br/>'+
										''+
										'<dd/>b)	<b>NAFIN</b> d� a conocer exclusivamente al <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b>, que haya elegido el <b>SUJETO DE APOYO</b>, los <b>DOCUMENTOS</b> '+
										'que han sido publicados por la <b>EMPRESA DE PRIMER ORDEN</b> correspondiente en las <b>CADENAS PRODUCTIVAS</b>, de los que sea beneficiario el <b>SUJETO DE APOYO</b>.<br/><br/> '+
										''+
										'<dd/>c)	<b>NAFIN</b> d� a conocer al <b>INTERMEDIARIO FINANCIERO</b> y/o al <b>DESCONTATARIO</b> a trav�s de <b>CADENAS PRODUCTIVAS</b> los <b>DOCUMENTOS</b> y montos '+
										'que se encuentren publicados en dicho sistema de <b>CADENAS PRODUCTIVAS</b>.<br/><br/>'+
										''+
										'Lo anterior para el efecto de que el <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> se encuentre en facultad de operar en <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b>, '+
										'en <b>CADENAS PRODUCTIVAS</b>, los <b>DOCUMENTOS</b> a favor del <b>SUJETO DE APOYO</b> que otorga la presente instrucci�n y que los recursos que deba recibir el '+
										'<b>SUJETO DE APOYO</b> como contraprestaci�n por las operaciones de <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b> los deposite el <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> en la '+
										'<b>CUENTA BANCARIA</b>, manifestando en este acto, que dichos recursos podr�n ser aplicados por el <b>INTERMEDIARIO FINANCIERO</b> al pago del <b>CR�DITO</b> y sus accesorios.<br/><br/>'+
										'				'+
										'Esta instrucci�n irrevocable no podr� ser extinguida o modificada por el <b>SUJETO DE APOYO</b> mientras exista cualquier saldo pendiente de pago al <b>INTERMEDIARIO FINANCIERO</b>, '+
										'y solo podr� ser revocada con la autorizaci�n expresa del mismo.<br/><br/>'+
										''+
										'Asimismo, como <b>SUJETO DE APOYO</b> manifiesto mi conformidad para que <b>NAFIN</b> se reserve el derecho de no proveer al amparo de esta instrucci�n el servicio de Cadenas Productivas en caso de incumplimiento '+
										'del <b>SUJETO DE APOYO</b> con el <b>INTERMEDIARIO FINANCIERO</b>.<br/><br/>'+
										 ''+
										'En este acto como <b>SUJETO DE APOYO</b> me obligo a no modificar la forma de pago y a no solicitar mi baja de <b>CADENAS PRODUCTIVAS</b>, mientras exista cualquier saldo pendiente de pago al '+
										'<b>INTERMEDIARIO FINANCIERO</b>, sin la autorizaci�n expresa de �ste.<br/><br/>'+
										' '+
										'Me obligo a no ceder los derechos de cobro de los <b>DOCUMENTOS</b> publicados por la <b>EMPRESA DE PRIMER ORDEN</b> seleccionada al otorgar esta instrucci�n, a persona alguna distinta del '+
										'<b>INTERMEDIARIO FINANCIERO o DESCONTANTE</b> durante la vigencia del <b>CR�DITO</b><br/><br/>'+
										'	'+
										'Al transmitir el mensaje de datos que contiene esta instrucci�n irrevocable, manifiesto mi conformidad con la misma, para todos los efectos legales correspondientes, '+
										'liberando a <b>NAFIN</b> de cualquier responsabilidad por su aceptaci�n y cumplimiento.<br/><br/>'+
										''+
										'<center><b>INSTRUCCI�N IRREVOCABLE - INTERMEDIARIO FINANCIERO Y/O DESCONTANTE</b></center><br/><br/>'+
										''+
										'Que en este acto otorga el <b>SUJETO DE APOYO</b> al <b>INTERMEDIARIO FINANCIERO o DESCONTANTE</b>, derivado del <b>CR�DITO</b> que le otorg� el <b>INTERMEDIARIO FINANCIERO</b>, a efecto de que:<br/><br/>'+
										''+
										'<dd/>a)	El <b>INTERMEDIARIO FINANCIERO</b> o el <b>DESCONTANTE</b> opere en <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b>, en <b>CADENAS PRODUCTIVAS</b> todos y cada uno de los <b>DOCUMENTOS</b> publicados por la <b>EMPRESA DE PRIMER ORDEN</b>.'+
										'<dd/>b)	El <b>INTERMEDIARIO FINANCIERO</b> o <b>DESCONTANTE</b> deposite los recursos que deba recibir el <b>SUJETO DE APOYO</b> como contraprestaci�n por el  <b>FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO</b> en la <b>CUENTA BANCARIA</b>, '+
										'en el entendido de que dichos recursos podr�n ser aplicados por el <b>INTERMEDIARIO FINANCIERO</b> al pago del <b>CR�DITO</b> y sus accesorios.<br/><br/>'+
										'Lo anterior no exime al <b>SUJETO DE APOYO</b> de continuar pagando el <b>CR�DITO</b>, m�s sus accesorios, en las fechas convenidas con el <b>INTERMEDIARIO FINANCIERO</b>.<br/><br/>'+
										'<dd/>c)	El <b>INTERMEDIARIO FINANCIERO</b> proporcione a <b>NAFIN</b> y al <b>DESCONTANTE</b>, en su caso,  las caracter�sticas del <b>CR�DITO</b> que me fue otorgado como <b>SUJETO DE APOYO</b> (cuenta, monto, fecha de vencimiento, banco para dep�sito etc.).<br/><br/>'+
										'Esta instrucci�n irrevocable no podr� ser extinguida o modificada por el <b>SUJETO DE APOYO</b> mientras exista cualquier saldo pendiente de pago al <b>INTERMEDIARIO FINANCIERO</b>, y solo podr� ser revocada con la autorizaci�n expresa del mismo.<br/><br/>					'+				
										'En este acto como <b>SUJETO DE APOYO</b> me obligo a no modificar la forma de pago y a no solicitar mi baja de <b>CADENAS PRODUCTIVAS</b>, mientras exista cualquier saldo pendiente de pago al <b>INTERMEDIARIO FINANCIERO</b>, sin la autorizaci�n expresa del mismo.<br/><br/>'+
										'Me obligo como <b>SUJETO DE APOYO</b> a no ceder los derechos de cobro de los <b>DOCUMENTOS</b> publicados por la <b>EMPRESA DE PRIMER ORDEN</b> seleccionada al otorgar esta instrucci�n, a persona alguna distinta del <b>INTERMEDIARIO FINANCIERO o DESCONTANTE</b> durante la vigencia del <b>CR�DITO</b>.<br/><br/>'+
										'Al transmitir el mensaje de datos que contiene esta instrucci�n irrevocable, manifiesto mi conformidad con la misma, para todos los efectos legales correspondientes, liberando al <b>INTERMEDIARIO FINANCIERO</b> y/o al <b>DESCONTANTE</b> de cualquier responsabilidad por su aceptaci�n y cumplimiento.<br/><br/>'+
									'</DIV>';

Ext.onReady(function() {

	var jsonParametrizacion = null;

	function procesarSuccessFailureParametrizacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if (!fpBotones.isVisible())	{
				fpBotones.show();
			}
			if (!fp.isVisible())	{
				fp.show();
			}
			if (!fpCaptura.isVisible())	{
				fpCaptura.show();
			}
			jsonParametrizacion = Ext.util.JSON.decode(response.responseText);
			var camposPublicos = jsonParametrizacion.publica;
			var camposPrivados = jsonParametrizacion.privada;
			var cmbCadena = Ext.getCmp('cmbCadena');
			var panelCaptura = Ext.getCmp('formaCaptura');
			if (camposPublicos != undefined && !Ext.isEmpty(camposPublicos)){
				var storeCombo = catalogoEpoData;
				var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
				Ext.each(camposPublicos, function(item, index, arrItems){
					storeCombo.add(new reg({
												clave: item.claveEpo,
												descripcion: item.nombreEpo,
												loadMsg: null
										})
					);
				});
				var config = {
					xtype: 'combo',
					name: 'clave_epo',
					id: 'claveEpo',
					anchor:'97%', 
					fieldLabel: 'Nombre de la EPO',
					emptyText: 'Seleccionar . . . ',
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					hiddenName : 'clave_epo',
					forceSelection : false,
					allowBlank: false,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					hidden: true,
					store: catalogoEpoData,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners:	{
						select:	{
							fn: function(combo)	{
								varGlobal.nombre_epo = combo.lastSelectionText;
								varGlobal.claveEpo = combo.getValue();
							}
						}
					}
				}
				panelCaptura.add(config);
				panelCaptura.insert(4,config);
				panelCaptura.doLayout();
			}
			if (camposPrivados != undefined && !Ext.isEmpty(camposPrivados)){
				var arrayChecks = [];
				Ext.each(camposPrivados, function(item, index, arrItems){
					arrayChecks.push({xtype: 'checkbox',value:	item.claveEpo,	id: 'clavesEpos'+index ,name: 'clavesEpos'+index,boxLabel:	item.nombreEpo, anchor: '97%'});
				});
				var config =	{xtype: 'checkboxgroup', id: 'cbg', name: 'cbg', hidden: true,fieldLabel: 'Nombre de la EPO',columns: 1,items: arrayChecks, blankText: 'Es necesario seleccionar una EPO.', allowBlank: false}
				panelCaptura.add(config);
				panelCaptura.insert(4,config);
				panelCaptura.doLayout();
			}
			if (!fp.isVisible())	{
				fp.show();
			}
			if (!fpCaptura.isVisible())	{
				fpCaptura.show();
			}
		} else {
			fp.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarSuccessFailureAcepto(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fp.el.unmask();
			fpCaptura.el.unmask();
			fpTexto.el.unmask();
			fpBotones.el.unmask();
			var panelCaptura = Ext.getCmp('formaCaptura');
			var datosCaptura = Ext.util.JSON.decode(response.responseText);

			if (datosCaptura.rel_epo_pyme != undefined && datosCaptura.rel_epo_pyme == "NO")	{
				panelCaptura.el.dom.scrollIntoView();
				Ext.Msg.alert('', 'No se encuentra Afiliado y/o Habilitado con la EPO seleccionada.');
				return;
			} else if (datosCaptura.rel_epo_pyme_if != undefined && datosCaptura.rel_epo_pyme_if == "NO")	{
				panelCaptura.el.dom.scrollIntoView();
				Ext.Msg.alert('','No se encuentra Afiliado y/o Habilitado con\n el Intermediario Financiero seleccionado.');
				return;
			} if (datosCaptura.rel_pyme_solic != undefined && datosCaptura.rel_pyme_solic == "SI")	{
				panelCaptura.el.dom.scrollIntoView();
				Ext.Msg.alert('','No se puede tener mas de una Instrucci�n Irrevocable con la misma EPO.');
				return;
			} if (datosCaptura.relacionPymeDescontante != undefined && datosCaptura.relacionPymeDescontante == "N")	{
				panelCaptura.el.dom.scrollIntoView();
				Ext.Msg.alert('','No se encuentra Afiliado y/o Habilitado con\n el Descontante seleccionado.');
				return;
			}

			var disEpo = Ext.getCmp('disEpo');
			var disIF = Ext.getCmp('disIF');
			var disDesconta = Ext.getCmp('disDesconta');
			disEpo.setValue(Ext.util.Format.nl2br(varGlobal.nombre_epo));
			disIF.setValue(varGlobal.nombre_if);
			if (!Ext.isEmpty(varGlobal.nombreDesconta)){
				disDesconta.setValue(varGlobal.nombreDesconta);
			}else{
				disDesconta.setValue(varGlobal.nombre_if);
			}

			fp.hide();
			fpCaptura.hide();
			fpBotones.hide();
			fpCifras.show();
			var win = new NE.cesion.WinCesionDerechos({
				getResultValid:	resulValidCesion,
				metodo: 'utilerias'
			}).show();
			win.el.dom.scrollIntoView();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var resulValidCesion= function(okResp, siCorreo, email, errorMessage){
		var registrosEnviar = [];
		if(okResp=='S'){
			fpBotones.el.mask();
			fpCifras.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15manDoctos_ext.data.jsp',
				params: {
					informacion: 'confirmaClaveCesion',
					clave_epo:			varGlobal.claveEpo,
					clave_if:			varGlobal.claveIf,
					claveDescontante:	varGlobal.claveDescontante,
					tipoCadena:			varGlobal.tipoCadena,
					nombreEpo:			varGlobal.nombre_epo,
					nombreIf:			varGlobal.nombre_if,
					nombreDescontante:varGlobal.nombreDesconta,
					stringClavesEpos:	varGlobal.stringClavesEpos,
					valCve:okResp
					//correo:siCorreo,
					//email: email
				},
				callback: procesarSuccessConfirmaCesion
			});  
		
		}
	}

	var procesarSuccessConfirmaCesion = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			if(resp.msgError=='' && resp.esCveCorrecta){
				fpBotones.el.unmask();
				fpCifras.el.unmask();
				var disFolio	= Ext.getCmp('disFolio');
				var disFecCarga= Ext.getCmp('disFecCarga');
				var disHrsCarga= Ext.getCmp('disHrsCarga');
				var disCaptura	= Ext.getCmp('disCaptura');
				var dt = new Date();
				disFolio.setValue(resp.folioInstruccionIrrevocable);
				disFolio.show();
				disFecCarga.setValue(resp.fechaAct);
				disFecCarga.show();
				disHrsCarga.setValue(resp.horaAct);
				disHrsCarga.show();
				disCaptura.setValue(resp.iNoUsuario+" - "+resp.strNombreUsuario);
				disCaptura.show();
				var btnCancelar = Ext.getCmp('btnCancelarCifras');
				Ext.getCmp('btnAceptoCifras').hide();
				Ext.getCmp('btnAbrirPDF').show();
				Ext.getCmp('btnAbrirPDF').focus();
				btnCancelar.setText('Salir');
				fpCifras.el.dom.scrollIntoView();
				Ext.getCmp('btnAbrirPDF').setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = resp.urlArchivo;
					forma.submit();
				});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarCatalogoDescontanteData = function(store, arrRegistros, opts) {
		var total = store.reader.jsonData.total;
		var paramInstruccion = Ext.getCmp("hiddenParamInstruccion");
		var comboDesconta = Ext.getCmp('cmbDesconta');
		if (total > 0){
			paramInstruccion.setValue('S');
			comboDesconta.setDisabled(false);
		}else{
			comboDesconta.setDisabled(true);
			paramInstruccion.setValue('N');
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		var fpConsulta = Ext.getCmp('formaConsulta');
		fp.el.unmask();
		fpConsulta.el.unmask();

		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				btnGenerarPDF.enable();
				btnGenerarArchivo.enable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGeneraPDF = Ext.getCmp('btnGenerarPDF');
		btnGeneraPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajaPDF = Ext.getCmp('btnBajarPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarMuestraAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
			var el = grid.getGridEl();
			el.unmask();
		} else	{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var muestraAcuse = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var no_folio = registro.get('FOLIO_INSTRUCCION');
		var el = grid.getGridEl();
		el.mask('Descargando Archivo', 'x-mask');
		Ext.Ajax.request({url: '15manDoctos_ext.data.jsp',params: Ext.apply({informacion: "AcusePyme",	numero_folio: no_folio}),callback: procesarMuestraAcuse});
	}

	var catalogoIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15manDoctos_ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIf'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoDescontanteData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15manDoctos_ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoDescontante'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoDescontanteData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEpoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion']
	});

	var cadenaData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','PUBLICAS'],
			['2','PRIVADAS']
		 ]
	});

	var catalogoEstatusConsultaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15manDoctos_ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusConsulta'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoIfConsultaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15manDoctos_ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIfConsulta'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEpoConsultaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15manDoctos_ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEpoConsulta'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15manDoctos_ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'NUMERO_REGISTROS', type: 'float'},
			{name: 'TOTAL_MONTO_DOCUMENTOS', type: 'float'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '15manDoctos_ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'FOLIO_INSTRUCCION'},
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_DESCONTANTE'},
			{name: 'FECHA_SOLICITUD'},
			{name: 'FECHA_AUTORIZACION_IF'},
			{name: 'MONTO_CREDITO'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'BANCO_SERVICIO'},
			{name: 'CUENTA_BANCARIA'},
			{name: 'TIPO_CUENTA'},
			{name: 'NUMERO_CREDITO'},
			{name: 'MONEDA'},
			{name: 'MONEDA_DESCUENTO'},
			{name: 'OBSERVACIONES'},
			{name: 'NUEVA_FECHA_VENCIMIENTO'},
			{name: 'FECHA_MODIFICACION'},
			{name: 'ESTATUS_SOLICITUD'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
		
	});

	var panelPublico = new Ext.Panel({
	id:	'panelPublico',
	width:	940,
	frame:	true,
	border:	true,
	hidden:	true,
	html:	textoPublico
	});

	var panelPrivado = new Ext.Panel({
	id:	'panelPrivado',
	width:	940,
	frame:	true,
	border:	true,
	hidden:	true,
	html:	textoPrivado
	});

	var fpBotones = new Ext.form.FormPanel({
		id: 'formaBotones',
		width: 940,
		hidden: false,
		frame: false,
		border: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 5px',
		labelWidth: 170,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: false,
		hidden: true,
		buttonAlign: 'center',
		buttons: [
			{
				text: 'Acepto',id: 'btnAcepto',iconCls: 'aceptar', align: 'stretch',
				handler: function(){
					var panelCaptura = Ext.getCmp('formaCaptura');
					var comboDesconta = Ext.getCmp('cmbDesconta');
					var paramInstruccion = Ext.getCmp('hiddenParamInstruccion');
					if(!verificaPanel('formaCaptura')) {
						panelCaptura.el.dom.scrollIntoView();
						return;
					}
					if (paramInstruccion.value == "S" && comboDesconta.value == ""){
						comboDesconta.markInvalid('Es necesario seleccionar un descontante.');
						comboDesconta.focus();
						panelCaptura.el.dom.scrollIntoView();
						return;
					}
					var comboEpo =  Ext.getCmp('claveEpo');
					var comboCadena = Ext.getCmp('cmbCadena');
					if (comboEpo == undefined && comboCadena.getValue() == '1') {
						Ext.Msg.alert('','No se puede continuar, No existen EPO�s a seleccionar');
						return;
					}
					fp.el.mask();
					fpCaptura.el.mask('Procesando...', 'x-mask-loading');
					fpTexto.el.mask();
					fpBotones.el.mask();
					if (varGlobal.tipoCadena == "2")	{
						varGlobal.nombre_epo = "";
						stringClavesEpos = "";
						var cbGroup = Ext.getCmp('cbg');
						cbGroup.items.each(function(check){
							if (check.checked == true)	{
								varGlobal.stringClavesEpos = varGlobal.stringClavesEpos + check.value + ',';
								varGlobal.nombre_epo = varGlobal.nombre_epo + check.boxLabel + '\n';
							}
						});
					}
					Ext.Ajax.request({
						url: '15manDoctos_ext.data.jsp',
						params: Ext.apply(fpCaptura.getForm().getValues(),{informacion: 'Aceptar', stringClavesEpos: varGlobal.stringClavesEpos}),
						callback: procesarSuccessFailureAcepto
					});
				}
			},{
				text:'Cancelar',	id: 'btnCancelar',iconCls: 'borrar', align: 'stretchmax',
				handler: function() {window.location = '15manDoctos_ext.jsp';}
			}
		]
	});

	var fpTexto = new Ext.form.FormPanel({
		id: 'formaTexto',
		width: 940,
		hidden: true,
		frame: false,
		border: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 2px',
		labelWidth: 130,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: false,
		items: [panelPublico,panelPrivado]
	});

	var elementosCaptura = [
		{
			xtype: 'combo',
			name: 'tipoCadena',	
			id: 'cmbCadena',
			anchor: '97%',
			fieldLabel: 'Tipo de Cadena', 
			mode: 'local',	
			hiddenName : 'tipoCadena',
			emptyText: 'Seleccionar . . . ',
			forceSelection : true,	triggerAction : 'all',	typeAhead: true,
			minChars : 1,
			store : cadenaData,
			displayField : 'descripcion',	valueField : 'clave', allowBlank: false,
			listeners: {
				select: {
					fn: function(combo) {
						varGlobal.claveEpo			=	"";
						varGlobal.claveIf				=	"";
						varGlobal.claveDescontante	=	"";
						varGlobal.tipoCadena			=	"";
						varGlobal.stringClavesEpos	=	"";
						varGlobal.nombre_epo			=	"";
						varGlobal.nombre_if			=	"";
						varGlobal.nombreDesconta	=	"";
						fpTexto.hide();
						var pPublic = Ext.getCmp('panelPublico');
						var pPrivate = Ext.getCmp('panelPrivado');
						var panelCaptura = Ext.getCmp('formaCaptura');
						var comboEpo =  Ext.getCmp('claveEpo');
						var cbg =  Ext.getCmp('cbg');
						pPublic.hide();
						pPrivate.hide();
						if (combo.getValue() == '1'){
							if (comboEpo){comboEpo.show();}
							if (cbg){cbg.hide();}
							fpTexto.show();
							pPublic.show();
						}else	if (combo.getValue() == '2')	{
							if (comboEpo){comboEpo.hide();}
							if (cbg){cbg.show();}
							fpTexto.show();
							pPrivate.show();
						}
						varGlobal.tipoCadena	=	combo.getValue();
						var comboDesconta = Ext.getCmp('cmbDesconta');
						comboDesconta.setValue('');
						comboDesconta.store.removeAll();
						comboDesconta.setDisabled(true);

						var comboIF = Ext.getCmp('cmbIF');
						comboIF.setValue('');
						comboIF.store.removeAll();
						comboIF.setDisabled(false);
						comboIF.store.reload({params: {	tipoCadena: combo.getValue()	}	});
					}
				}
			}
		},{
			xtype: 'combo',
			name: 'clave_if',
			id:	'cmbIF',
			fieldLabel: 'Nombre del IF que otorga el cr�dito',
			emptyText: 'Seleccionar . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'clave_if',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIfData,
			tpl : NE.util.templateMensajeCargaCombo,
			anchor:	'97%',allowBlank: false,
			listeners:	{
				select:	{
					fn: function(combo) {
						varGlobal.nombre_if = combo.lastSelectionText;
						varGlobal.claveIf = combo.getValue();
						var comboDesconta = Ext.getCmp('cmbDesconta');
						varGlobal.nombreDesconta = "";
						comboDesconta.setValue('');
						comboDesconta.store.removeAll();
						comboDesconta.setDisabled(false);
						comboDesconta.store.reload({
								params: {
									clave_if: varGlobal.claveIf
								}
						});
					}
				}
			}
		},{
			xtype: 'combo',
			name: 'claveDescontante',
			id:	'cmbDesconta',
			fieldLabel: 'Descontante',
			emptyText: 'Seleccionar . . . ',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'claveDescontante',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoDescontanteData,
			tpl : NE.util.templateMensajeCargaCombo,
			anchor:	'97%',allowBlank: true,
			listeners:	{
				select:	{
					fn: function(combo){
						varGlobal.nombreDesconta = combo.lastSelectionText;
						varGlobal.claveDescontante = combo.getValue();
					}
				}
			}
		},{
			xtype:'hidden',
			id:'hiddenParamInstruccion',
			name:'parametroInstruccionIrrevocable', 
			value:''
		}
	];	

	function verificaPanel(idPanel){
		var myPanel = Ext.getCmp(idPanel);
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype != 'label')	{	//El metodo isValid() no aplica para los tipos de objetos label.
				if (!panelItem.isValid() && panelItem.isVisible())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}

	var fpCaptura = new Ext.form.FormPanel({
		id: 'formaCaptura',
		width: 740,
		frame: true,
		border: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		items: elementosCaptura,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: false,
		hidden: true, title: '<div><center>Captura</div>'
	});

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header : 'Folio', tooltip: 'Folio',
				dataIndex : 'FOLIO_INSTRUCCION',
				width : 100, sortable : true, hidden: false, align: 'center'
			},{
				header: 'Nombre EPO', tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true, width: 200, resizable: true, hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return Ext.util.Format.nl2br(value);
				}
			},{
				header: 'IF que tr�mita el cr�dito', tooltip: 'IF que tr�mita el cr�dito',
				dataIndex: 'NOMBRE_IF',
				sortable: true,	width: 200,	resizable: true, hidden: false
			},{
				header: 'Descontante', tooltip: 'Descontante',
				dataIndex: 'NOMBRE_DESCONTANTE',
				sortable: true,	width: 200,	resizable: true, hidden: false			
			},{
				header : 'Fecha de Solicitud', tooltip: 'Fecha de Solicitud',
				dataIndex : 'FECHA_SOLICITUD',
				sortable : true, width : 120, align: 'center', hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if (value != 'NA'){
									return Ext.util.Format.date(value,'d/m/Y');
								}else{
									return value;
								}
				}
			},{
				header : 'Fecha de Aceptaci�n IF', tooltip: 'Fecha de Aceptaci�n IF',
				dataIndex : 'FECHA_AUTORIZACION_IF',
				sortable : true, width : 120, align: 'center', hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if (value != 'NA'){
									return Ext.util.Format.date(value,'d/m/Y');
								}else{
									return value;
								}
				}
			},{
				header : 'Monto del Cr�dito', tooltip: 'Monto del Cr�dito',
				dataIndex : 'MONTO_CREDITO',
				sortable : true, width : 120, align: 'right', hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if (value != 'NA'){
									return Ext.util.Format.number(value,'$0,0.00');
								}else{
									return value;
								}
				}
			},{
				header : 'Fecha de Vencimiento del Cr�dito', tooltip: 'Fecha de Vencimiento del Cr�dito',
				dataIndex : 'FECHA_VENCIMIENTO',
				sortable : true, width : 120, align: 'center', hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if (value != 'NA'){
									return Ext.util.Format.date(value,'d/m/Y');
								}else{
									return value;
								}
				}
			},{
				header : 'Tipo de Cuenta', tooltip: 'Tipo de Cuenta',
				dataIndex : 'TIPO_CUENTA',
				width : 150, sortable : true, hidden: false
			},{
				header : 'Banco de Servicio', tooltip: 'Banco de Servicio',
				dataIndex : 'BANCO_SERVICIO',
				width : 150, sortable : true, hidden: false
			},{
				header : 'Cuenta Bancaria', tooltip: 'Cuenta Bancaria',
				dataIndex : 'CUENTA_BANCARIA',
				width : 150, sortable : true, hidden: false
			},{
				header : 'N�mero de Cr�dito', tooltip: 'N�mero de Cr�dito',
				dataIndex : 'NUMERO_CREDITO',
				width : 150, sortable : true, hidden: false
			},{
				header : 'Moneda de Cr�dito', tooltip: 'Moneda de Cr�dito',
				dataIndex : 'MONEDA',
				width : 150, sortable : true, hidden: false
			},{
				header : 'Moneda para Descuento', tooltip: 'Moneda para Descuento',
				dataIndex : 'MONEDA_DESCUENTO',
				width : 150, sortable : true, hidden: false
			},{
				header : 'Observaciones', tooltip: 'Observaciones',
				dataIndex : 'OBSERVACIONES',
				width : 250, sortable : true, hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header : 'Nueva Fecha de Vencimiento del Cr�dito', tooltip: 'Nueva Fecha de Vencimiento del Cr�dito',
				dataIndex : 'NUEVA_FECHA_VENCIMIENTO',
				sortable : true, width : 120, align: 'center', hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if (value != 'NA'){
									return Ext.util.Format.date(value,'d/m/Y')
								}else{
									return value;
								}
				}
			},{
				header : 'Fecha de Modificaci�n de Monto', tooltip: 'Fecha de Modificaci�n de Monto',
				dataIndex : 'FECHA_MODIFICACION',
				sortable : true, width : 120, align: 'center', hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if (value != 'NA'){
									return Ext.util.Format.date(value,'d/m/Y')
								}else{
									return value;
								}
				}
			},{
				header : 'Estatus', tooltip: 'Estatus',
				dataIndex : 'ESTATUS_SOLICITUD',
				width : 150, sortable : true, hidden: false
			},{
				xtype:	'actioncolumn',
				header : 'Acuse', tooltip: 'Num. Acuse',
				dataIndex : 'FOLIO_INSTRUCCION',
				width:	150,	align: 'center', hidden: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.json.USUARIO_SOLICITUD == 'S')	{
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else	{
								return "";
							}
						},
						handler:	muestraAcuse
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
            '-',{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
               iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15manDoctos_ext.data.jsp',
							params: Ext.apply(fpConsulta.getForm().getValues(),{
										informacion: 'ArchivoCSV'}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
               iconCls: 'icoXls',
					hidden: true
				},'-',{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
               iconCls: 'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '15manDoctos_ext.data.jsp',
							params: Ext.apply(fpConsulta.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
               iconCls: 'icoPdf',
					hidden: true
				},'-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
               iconCls: 'icon-register-edit',
					hidden: true,
					handler: function(boton, evento) {
						resumenTotalesData.load();
						var totalesCmp = Ext.getCmp('gridTotalesh');
						if (!totalesCmp.isVisible()) {
							totalesCmp.show();
							totalesCmp.el.dom.scrollIntoView();
						}
					}
				}
			]
		}
	});

	var gridTotales = {
		xtype: 'grid',
		store: resumenTotalesData,
		id: 'gridTotalesh',
		hidden:	true,
		columns: [
			{
				header: 'TOTALES',
				dataIndex: 'NOMMONEDA',
				align: 'left',	width: 250
			},
			{
				header: 'Registros',
				dataIndex: 'NUMERO_REGISTROS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},
			{
				header: 'Monto Documentos',
				dataIndex: 'TOTAL_MONTO_DOCUMENTOS',
				width: 150,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	var elementosConsulta = [
			{
				xtype: 'combo',
				name: 'clave_epo',
				id: 'cmbEpoConsulta',
				fieldLabel: 'EPO',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'clave_epo',
				emptyText: 'Todas . . .',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : catalogoEpoConsultaData,
				tpl : NE.util.templateMensajeCargaCombo,
				anchor: '100%', allowBlank: true,
				listeners: {
					select: {
						fn: function(combo) {
							grid.hide();
							var comboIF = Ext.getCmp('cmbIFConsulta');
							comboIF.setValue('');
							comboIF.store.removeAll();
							//comboIF.setDisabled(false);
							comboIF.store.reload({
									params: {
										clave_epo: combo.getValue()
									}
							});
						}
					}
				}
			},{
				xtype: 'combo',
				name: 'clave_if',
				id:	'cmbIFConsulta',
				fieldLabel: 'IF',
				emptyText: 'Todos . . . ',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				hiddenName : 'clave_if',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store: catalogoIfConsultaData,
				tpl : NE.util.templateMensajeCargaCombo,
				anchor:	'100%',allowBlank: true
			},{
				xtype: 'combo',
				name: 'clave_estatus',
				id:	'cmbEstatusConsulta',
				fieldLabel: 'Estatus',
				emptyText: 'Todos . . . ',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				hiddenName : 'Estatus',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store: catalogoEstatusConsultaData,
				tpl : NE.util.templateMensajeCargaCombo,
				anchor:	'100%',allowBlank: true
			},{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de Solicitud',
				combineErrors: false,
				msgTarget: 'side',
				anchor:'100%',
				items: [
					{
						xtype: 'datefield',
						name: 'fecha_solicitud_ini',
						id: 'fecha_solicitud_ini',
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoFinFecha: 'fecha_solicitud_fin',
						margins: '0 25 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'a',
						width: 20
					},
					{
						xtype: 'datefield',
						name: 'fecha_solicitud_fin',
						id: 'fecha_solicitud_fin',
						allowBlank: true,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoInicioFecha: 'fecha_solicitud_ini',
						margins: '0 25 0 0'  //necesario para mostrar el icono de error
					}
				]
			},{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de Aceptaci�n IF',
				combineErrors: false,
				msgTarget: 'side',
				anchor:'100%',
				items: [
					{
						xtype: 'datefield',
						name: 'fecha_aceptacion_ini',
						id: 'fecha_aceptacion_ini',
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoFinFecha: 'fecha_aceptacion_fin',
						margins: '0 25 0 0'  //necesario para mostrar el icono de error
					},{
						xtype: 'displayfield',
						value: 'a',
						width: 20
					},{
						xtype: 'datefield',
						name: 'fecha_aceptacion_fin',
						id: 'fecha_aceptacion_fin',
						allowBlank: true,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoInicioFecha: 'fecha_aceptacion_ini',
						margins: '0 25 0 0'  //necesario para mostrar el icono de error
					}
				]
			}/*,{
						xtype:'hidden',
						id:'hidden_numero_nafele',
						name:'numero_nafele',
						value:''
					}*/
	];

	var fpConsulta = new Ext.form.FormPanel({
		id: 'formaConsulta',
		width: 740,
		hidden: true,
		frame: true,
		border: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		monitorValid: true,
		title: '<div><center>Consulta</div>',
		items: elementosConsulta,
		buttonAlign: 'center',
		buttons: [
			{
				text: 'Consultar',id: 'btnConsultar_b',iconCls: 'icoBuscar', formBind: true,
				handler:	function()	{
					grid.hide();
					fp.el.mask();
					fpConsulta.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fpConsulta.getForm().getValues(),{
							operacion: 'Generar',
							start: 0,
							limit: 15
						})
					});
				}
			},{
				text:'Limpiar',	id: 'btnLimpiar',iconCls: 'icoLimpiar',
					handler: function() {
								fpConsulta.getForm().reset();
								grid.store.removeAll();
								Ext.getCmp('btnBajarArchivo').hide();
								Ext.getCmp('btnBajarPDF').hide();
								grid.hide();
				}
			}
		]
	});

	var elementosCifras = [
		{xtype: 'displayfield',	id: 'disEpo',		name:	'nombreEpo',	fieldLabel: 'Nombres de las(s) EPO(s)',	value:'',	anchor: '100%'},
		{xtype: 'displayfield',	id: 'disIF',		name:	'nombreIf',		fieldLabel: 'If que tramita el cr�dito', 	value:'',	anchor: '100%'},
		{xtype: 'displayfield',	id: 'disDesconta',name:	'Descontante',	fieldLabel: 'Descontante', 					value:'',	anchor: '100%'},
		{xtype: 'displayfield',	id: 'disFolio',	name:	'numFolio',		fieldLabel: 'N�mero de Folio', 				value:'',	anchor: '100%',hidden: true},
		{xtype: 'displayfield',	id: 'disFecCarga',name:	'fechaAct',		fieldLabel: 'Fecha de Carga', 				value:'',	anchor: '100%',hidden: true},
		{xtype: 'displayfield',	id: 'disHrsCarga',name:	'horaAct',		fieldLabel: 'Hora de Carga', 					value:'',	anchor: '100%',hidden: true},
		{xtype: 'displayfield',	id: 'disCaptura',	name:	'nombreUsuario',fieldLabel:'Usuario de Captura', 			value:'',	anchor: '100%',hidden: true}
	];

	var fpCifras = new Ext.form.FormPanel({
		id: 'formaCifras',
		width: 640,
		hidden: true,
		frame: false,
		border: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: false,
		items: elementosCifras,
		title: '<div align="left"><center>Cifras de Control</center></div>',
		buttonAlign: 'center',
		buttons: [
			{
				text: 'Acepto',id: 'btnAceptoCifras',iconCls: 'aceptar',
				handler:	function()	{
						var win = new NE.cesion.WinCesionDerechos({
							getResultValid:resulValidCesion,
							metodo: 'utilerias'
						}).show();
						win.el.dom.scrollIntoView();
				}
			},{
				text:'Abrir PDF',	id: 'btnAbrirPDF',iconCls: 'icoBuscar', hidden: true
			},{
				text:'Cancelar',	id: 'btnCancelarCifras',iconCls: 'borrar',
				handler: function() {
					window.location = '15manDoctos_ext.jsp';
				}
			}
		]
	});

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 940,
		frame: false,
		border: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 2px',
		labelWidth: 130,
		buttonAlign: 'center',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		hidden: true,
		buttons: [
			{
				text: 'Captura',id: 'btnCapturar',iconCls: 'modificar',
				handler: function() {
					window.location = '15manDoctos_ext.jsp';
				}
			},{
				text:'Consulta',	id: 'btnConsultar',iconCls: 'iconoLupa',
				handler: function() {
					fpCaptura.hide();
					fpTexto.hide();
					fpBotones.hide();
					if (!fpConsulta.isVisible())	{
						fpConsulta.show();
						fpConsulta.getForm().reset();
					}
					pnl.doLayout();
					catalogoEpoConsultaData.load();
					catalogoEstatusConsultaData.load();
					grid.store.removeAll();
					Ext.getCmp('btnBajarArchivo').hide();
					Ext.getCmp('btnBajarPDF').hide();
					grid.hide();
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,NE.util.getEspaciador(5),fpCifras,fpConsulta,fpCaptura,NE.util.getEspaciador(5),fpTexto,fpBotones,grid,NE.util.getEspaciador(5)
		]
	});

	Ext.Ajax.request({
		url: '15manDoctos_ext.data.jsp',
		params: {
			informacion: "Parametrizacion"
		},
		callback: procesarSuccessFailureParametrizacion
	});

});