Ext.onReady(function() {

var claveTipoCadena='';
//----------------------Handlers----------------------
function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambas Fechas son necesarias');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambas Fechas son necesarias');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	

var procesaConsultaNE = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
				
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}
var accionConsulta = function(estadoSiguiente, respuesta){
if(  estadoSiguiente == "OBTENER_NOMBRE_PYME" 										){
		
			fp.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '15MandatoDocumentosExt.data.jsp',	
				params: {
							numeroDeProveedor:	respuesta.noNafinElec,
							ic_banco_fondeo:		respuesta.ic_banco_fondeo,
							ic_epo:					respuesta.ic_epo,
							informacion: 			"obtenNombrePyme" 
				},
				callback: procesaConsultaNE
			});

		}else if(  estadoSiguiente == "RESPUESTA_OBTENER_NOMBRE_PYME"){
		
			Ext.getCmp('hid_ic_pyme').setValue(respuesta.ic_pyme);
			Ext.getCmp('_txt_nombre').setValue(respuesta.txtCadenasPymes);
			
			fp.el.unmask();
			if (respuesta.muestraMensaje != undefined && respuesta.muestraMensaje){
				Ext.Msg.alert("Mensaje de p�gina web.",	respuesta.textoMensaje);
				Ext.getCmp('_txt_nafelec').setValue('');
				
			}else{
				//grid.setVisible(false);
				
			}

		}

}

function procesarArchivoSuccess(opts, success, response) {
			
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
			var boton=Ext.getCmp('btnGenerarArchivo');
			boton.setIconClass('');
			boton.enable();
			 
			var botonPdf=Ext.getCmp('btnGenerarPDF');
			botonPdf.setIconClass('');
			botonPdf.enable();
	}
	
var descargaAcuse = function(grid, rowIndex, colIndex, item, event)	{
	registro = grid.getStore().getAt(rowIndex);
	Ext.Ajax.request({
		url: '15MandatoDocumentosExt.data.jsp',
		params: {
		folio_instruccion: registro.get('FOLIO_INSTRUCCION'),
		usuario_solicitud: registro.get('USUARIO_SOLICITUD'),
		informacion: 'archivoAcuse'},
		callback: procesarArchivoSuccess
	});
	
}
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	
	var procesarConsultaData = function(store,arrRegistros,opts){
		//accionBotones();
		
		var botonArchivo= Ext.getCmp('btnGenerarArchivo');
		var botonPDF = Ext.getCmp('btnGenerarPDF');
			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
				botonPDF.setVisible(true);
				botonArchivo.setVisible(true);
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					botonPDF.setVisible(false);
					botonArchivo.setVisible(false);
					//accionBotonesDisabled();
				}
			}
		}
		
		var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
//--------------------------STORES------------------------


var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15MandatoDocumentosExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '15MandatoDocumentosExt.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'FOLIO_INSTRUCCION'},
				{name: 'NOMBRE_EPO'},
				{name: 'NOMBRE_PYME'},
				{name: 'IC_EPO'},
				{name: 'NAFIN_ELECTRONICO'},
				{name: 'IC_IF'},
				{name: 'NOMBRE_IF'},
				{name: 'NOMBRE_DESCONTANTE'},
				{name: 'TIPO_CADENA'},
				{name: 'FECHA_SOLICITUD'},
				{name: 'FECHA_AUTORIZACION_IF'},
				{name: 'MONTO_CREDITO'},
				{name: 'FECHA_VENCIMIENTO'},
				{name: 'CLAVE_BANCO_SERVICIO'},
				{name: 'BANCO_SERVICIO'},
				{name: 'CUENTA_BANCARIA'},
				{name: 'CLAVE_TIPO_CUENTA'},
				{name: 'TIPO_CUENTA'},
				{name: 'NUMERO_CREDITO'},
				{name: 'MONEDA'},
				{name: 'MONEDA_DESCUENTO'},
				{name: 'OBSERVACIONES'},
				{name: 'ESTATUS_SOLICITUD'},
				{name: 'CLAVE_ESTATUS'},
				{name: 'USUARIO_SOLICITUD'},
				{name: 'NUEVA_FECHA_VENCIMIENTO'},
				{name: 'FECHA_MODIFICACION'},
				{name: 'DESCAUT'},
				{name: 'FECHADESCAUT'},
				{name: 'HABILITACAMPOS'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
						}	},
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var catalogoCadena = new Ext.data.JsonStore
  ({
	   id: 'catologoCadena',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15MandatoDocumentosExt.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoCadena'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError
		}
  });
  
var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15MandatoDocumentosExt.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEpo',
		 claveTipoCadena: claveTipoCadena
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError
		}
  });
  
var catalogoIF = new Ext.data.JsonStore
  ({
	   id: 'catologoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15MandatoDocumentosExt.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoIF'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError
		}
  });
  
var catalogoEstatus = new Ext.data.JsonStore
  ({
	   id: 'catologoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15MandatoDocumentosExt.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEstatus'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError
		}
  });
  
	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '15MandatoDocumentosExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
  



//--------------------COMPONENTES--------------------
var elementosForma = [
	{
				xtype: 'combo',
				fieldLabel: 'Tipo de Cadena',
				emptyText: 'Todos',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoCadena,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'Hcadena',
				id: 'cadena',
				mode: 'local',
				hiddenName: 'Hcadena',
				forceSelection: true,
				listeners: {
					select:function(combo){
						catalogoEpo.load({
														params: {
															Hcadena: combo.getValue(),
															HclaveIf:	Ext.getCmp('claveIf').getValue()
														}
												});
					
					}
				}
    },{
				xtype: 'combo',
				fieldLabel: 'EPO',
				emptyText: 'Todas',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'Hepo',
				id: 'epo',
				mode: 'local',
				hiddenName: 'Hepo',
				forceSelection: true,
				listeners: {
					select:function(combo){
					Ext.getCmp('claveIf').setValue('');
					if(combo.getValue()!='')
						catalogoIF.load({
														params: {
															Hepo: combo.getValue()
														}
												});
					else{
						
						catalogoIF.removeAll();
					}
					}
				}
    },{
				xtype: 'combo',
				fieldLabel: 'IF',
				emptyText: 'Todos',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoIF,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'HclaveIf',
				id: 'claveIf',
				mode: 'local',
				hiddenName: 'HclaveIf',
				forceSelection: true,
				listeners: {
					select:function(combo){
						catalogoEpo.load({
														params: {
															claveTipoCadena: Ext.getCmp('cadena').getValue(),
															claveIf:	combo.getValue()
														}
												});
					
					}
				}        
				
    },
	 {
					xtype:'hidden',
					id:	'hid_nombre',
					value:''
				},{
					xtype:'hidden',
					id:	'hid_ic_pyme',
					value:''
				},
				{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
						xtype:		'textfield',
						name:			'txt_ne',
						id:			'_txt_nafelec',
						maskRe:		/[0-9]/,
						allowBlank:	true,
						fieldLabel: 'N@E<br/>Proveedor',
						maxLength:	25,
						listeners:	{
							'change':function(field){
											if ( !Ext.isEmpty(field.getValue()) ) {
	
												var respuesta = new Object();
												respuesta["noNafinElec"]		= field.getValue();
												//respuesta["ic_banco_fondeo"]	= Ext.getCmp('_ic_banco_fondeo').getValue();
												//respuesta["ic_epo"]				= Ext.getCmp('ic_epo').getValue();
												
												accionConsulta("OBTENER_NOMBRE_PYME",respuesta);
	
											}else{
												Ext.getCmp('hid_ic_pyme').setValue('');
												Ext.getCmp('_txt_nombre').setValue('');
											}
										}
						}
					},{
						xtype:			'textfield',
						name:				'txt_nombre',
						id:				'_txt_nombre',
						width:			270,
						disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
						disabled:		true,
						allowBlank:		true,
						maxLength:		100
					}
				]
				},{
					xtype: 'compositefield',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:'displayfield',
							id:'disEspacio',
							text:''
						},
						{
							xtype:	'button',
							id:		'btnBuscaA',
							iconCls:	'icoBuscar',
							text:		'B�squeda Avanzada',
							handler: function(boton, evento) {
										var winVen = Ext.getCmp('winBuscaA');
											if (winVen){
												Ext.getCmp('fpWinBusca').getForm().reset();
												Ext.getCmp('fpWinBuscaB').getForm().reset();
												
												Ext.getCmp('cmb_num_ne').setValue();
												Ext.getCmp('cmb_num_ne').store.removeAll();
												Ext.getCmp('cmb_num_ne').reset();
												winVen.show();
											}else{
												var winBuscaA = new Ext.Window ({
													id:'winBuscaA',
													height: 320,
													x: 300,
													y: 100,
													width: 550,
													heigth: 100,
													modal: true,
													closeAction: 'hide',
													title: 'B�squeda Avanzada',
													items:[
														{
															xtype:'form',
															id:'fpWinBusca',
															frame: true,
															border: false,
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 130,
															items:[
																{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:'Utilice el * para b�squeda gen�rica'
																},{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:''
																},{
																	xtype: 'textfield',
																	name: 'nombre_pyme',
																	id:	'txtNombre',
																	fieldLabel:'Nombre',
																	maxLength:	100
																},{
																	xtype: 'textfield',
																	name: 'rfc_pyme',
																	id:	'txtRfc',
																	fieldLabel:'RFC',
																	maxLength:	20
																},{
																	xtype: 'textfield',
																	name: 'num_pyme',
																	id:	'txtNe',
																	hidden:true,
																	fieldLabel:'N�mero Proveedor',
																	maxLength:	25
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Buscar',
																	iconCls:'icoBuscar',
																	handler: function(boton) {
																					catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {
																					Ext.getCmp('winBuscaA').hide();
																				}
																}
															]
														},{
															xtype:'form',
															frame: true,
															id:'fpWinBuscaB',
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															monitorValid: true,
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 80,
															items:[
																{
																	xtype: 'combo',
																	id:	'cmb_num_ne',
																	name: 'ic_pyme',
																	hiddenName : 'ic_pyme',
																	fieldLabel: 'Nombre',
																	emptyText: 'Seleccione Proveedor. . .',
																	displayField: 'descripcion',
																	valueField: 'clave',
																	triggerAction : 'all',
																	forceSelection:true,
																	allowBlank: false,
																	typeAhead: true,
																	mode: 'local',
																	minChars : 1,
																	store: catalogoNombreProvData,
																	tpl : NE.util.templateMensajeCargaCombo
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Aceptar',
																	iconCls:'aceptar',
																	formBind:true,
																	//hidden:true,
																	handler: function() {
																					if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																						var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																						var cveP = disp.substr(0,disp.indexOf(" "));
																						var desc = disp.slice(disp.indexOf(" ")+1);
																						Ext.getCmp('hid_ic_pyme').setValue(Ext.getCmp('cmb_num_ne').getValue())
																						Ext.getCmp('_txt_nafelec').setValue(cveP);
																						Ext.getCmp('_txt_nombre').setValue(desc);
																						Ext.getCmp('winBuscaA').hide();
																					}
																				}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {	Ext.getCmp('winBuscaA').hide();	}
																}
															]
														}
													]
												}).show();
											}
										}
						}
					]
				},
		{
				//Banco
				xtype: 'combo',
				fieldLabel: 'Estatus',
				emptyText: 'Todos',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEstatus,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'Hestatus',
				id: 'estatus',
				mode: 'local',
				hiddenName: 'Hestatus',
				forceSelection: true
    },
	 {
							xtype: 'compositefield',
							fieldLabel: 'Fecha de Solicitud',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaSolic1',
									id: 'fechaSolic1',
									startDay: 0,
									width: 100,
									minValue: '01/01/1901',
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaSolic2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaSolic2',
									id: 'fechaSolic2',
									startDay: 1,
									width: 100,
									minValue: '01/01/1901',
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: 'fechaSolic1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de Aceptaci�n IF',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaIF1',
									id: 'fechaIF1',
									startDay: 0, 
									width: 100,
									minValue: '01/01/1901',
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaIF2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaIF2',
									id: 'fechaIF2',
									startDay: 1,
									width: 100,
									minValue: '01/01/1901',
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: 'fechaIF1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						}

];

var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						
						header:'Folio',
						tooltip: 'Folio',
						sortable: true,
						dataIndex: 'FOLIO_INSTRUCCION',
						width: 80,
						align: 'center'
						},
						{
						header: 'N@E PyME',
						tooltip: 'N@E PyME',
						sortable: true,
						dataIndex: 'NAFIN_ELECTRONICO',
						width: 90,
						align: 'center'						},
						{
						header: 'PyME',
						tooltip: 'PyME',
						sortable: true,
						dataIndex: 'NOMBRE_PYME',
						width: 150,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'EPO',
						tooltip: 'EPO',
						sortable: true,
						dataIndex: 'NOMBRE_EPO',
						width: 150,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'IF que tramita el cr�dito',
						tooltip: 'IF que tramita el cr�dito',
						sortable: true,
						dataIndex: 'NOMBRE_IF',
						width: 150,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						
						},
						{
						align:'center',
						header: 'Descontante',
						tooltip: 'Descontante',
						sortable: true,
						dataIndex: 'NOMBRE_DESCONTANTE',
						width: 150,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},{
						
						header:'Tipo de Cadena',
						tooltip: 'Tipo de Cadena',
						sortable: true,
						dataIndex: 'TIPO_CADENA',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'Fecha de Solicitud',
						tooltip: 'Fecha de Solicitud',
						sortable: true,
						dataIndex: 'FECHA_SOLICITUD',
						width: 150,
						align: 'center'
						},
						{
						header: 'Fecha de Aceptaci�n',
						tooltip: 'Fecha de Aceptaci�n',
						sortable: true,
						dataIndex: 'FECHA_AUTORIZACION_IF',
						width: 150,
						align: 'center'
						},
						{
						header: 'Monto del Cr�dito',
						tooltip: 'Monto del Cr�dito',
						sortable: true,
						dataIndex: 'MONTO_CREDITO',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						
						},
						{
						align:'center',
						header: 'Fecha de Vencimiento del Cr�dito',
						tooltip: 'Fecha de Vencimiento del Cr�dito',
						sortable: true,
						dataIndex: 'FECHA_VENCIMIENTO',
						width: 130
						},
						{
						header: 'Tipo de Cuenta',
						tooltip: 'Tipo de Cuenta',
						sortable: true,
						dataIndex: 'TIPO_CUENTA',
						width: 130,
						align: 'center'
						},
						{
						header: 'Banco de Servicio',
						tooltip: 'Banco de Servicio',
						sortable: true,
						dataIndex: 'BANCO_SERVICIO',
						width: 130,
						align: 'left'
						},
						{
						header: 'Cuenta Bancaria',
						tooltip: 'Cuenta Bancaria',
						sortable: true,
						dataIndex: 'CUENTA_BANCARIA',
						width: 150,
						align: 'center'
						},
						{
						header: 'N�mero de Cr�dito',
						tooltip: 'N�mero de Cr�dito',
						sortable: true,
						dataIndex: 'NUMERO_CREDITO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Moneda de Cr�dito',
						tooltip: 'Moneda de Cr�dito',
						sortable: true,
						dataIndex: 'MONEDA',
						width: 130,
						align: 'center'
						},
						{
						header: 'Moneda para Descuento',
						tooltip: 'Moneda para Descuento',
						sortable: true,
						dataIndex: 'MONEDA_DESCUENTO',
						width: 130,
						align: 'center'
						},
						{
						header: 'Observaciones',
						tooltip: 'Observaciones',
						sortable: true,
						dataIndex: 'OBSERVACIONES',
						width: 150,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Nueva Fecha de Vencimiento del Cr�dito',
						tooltip: 'Nueva Fecha de Vencimiento del Cr�dito',
						sortable: true,
						dataIndex: 'NUEVA_FECHA_VENCIMIENTO',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								if (value=='')
									return 'NA';
								else
									return value;
							}
						},
						{
						header: 'Fecha de Modificaci�n de Monto',
						tooltip: 'Fecha de Modificaci�n de Monto',
						sortable: true,
						dataIndex: 'FECHA_MODIFICACION',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								if (value=='')
									return 'NA';
								else
									return value;
							}
						
						},
						{
						header: 'Descuento Autom�tico',
						tooltip: 'Descuento Autom�tico',
						sortable: true,
						dataIndex: 'FECHADESCAUT',
						width: 130,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Estatus',
						tooltip: 'Estatus',
						sortable: true,
						dataIndex: 'ESTATUS_SOLICITUD',
						width: 130,
						align: 'center'
						},
						{
						xtype: 'actioncolumn',
						header: 'Acuse',
						tooltip: 'Acuse',
						sortable: true,
						dataIndex: 'USUARIO_SOLICITUD',
						width: 150,
						align: 'center',
						items: [
							{
								getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
									if(registro.get('USUARIO_SOLICITUD')!='N'){
										this.items[0].tooltip = 'Ver';
										return 'iconoLupa';
									}else{
										
										return 'Acuse no Disponible';
									}
									
								}
								,handler:	descargaAcuse
							}
						]
						}

				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					autoScroll:true,
					height: 30,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					
					items: ['->',
								{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '15MandatoDocumentosExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'GenerarCSV'}),
							callback: procesarArchivoSuccess
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '15MandatoDocumentosExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'GenerarPDF',
									start: cmpBarraPaginacion.cursor,
									limit: cmpBarraPaginacion.pageSize
								}),
							callback: procesarArchivoSuccess
						});
					}
				},
								'-']
				}
		});


var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		labelWidth: 130,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						if(!fp.getForm().isValid()){
							return;
						}
						if(verificaFechas('fechaSolic1','fechaSolic2')&&verificaFechas('fechaIF1','fechaIF2')){
						
							grid.show();
							consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15})
								});
							
							
						}
						//Mi acci�n
						//if(verificaFechas('dFechaRegIni','dFechaRegFin')){
							//grid.show();
							//consulta.load({ params: Ext.apply(fp.getForm().getValues(),{txt_ne: IC_Pyme})
						//	});				
//						}
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						location.reload();
				}
			 }
			 
		]
	});

var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [fp,NE.util.getEspaciador(30),grid]
	});
	
	catalogoCadena.load();
	catalogoEpo.load();
	catalogoEstatus.load();
});