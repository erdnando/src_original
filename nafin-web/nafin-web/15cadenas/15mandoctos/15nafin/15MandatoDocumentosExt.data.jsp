	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,  
	net.sf.json.JSONObject,
	com.netro.cadenas.*,
	com.netro.afiliacion.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.mandatodoc.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray"
	errorPage="/00utils/error_extjs.jsp"%>
	
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion            =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";	
	String claveTipoCadena			=(request.getParameter("Hcadena")    != null) ?   request.getParameter("Hcadena") :"";	
	String claveIf			=(request.getParameter("HclaveIf")    != null) ?   request.getParameter("HclaveIf") :"";	
	String clave_epo			=(request.getParameter("Hepo")    != null) ?   request.getParameter("Hepo") :"";	
	String numero_nafele			=(request.getParameter("txt_ne")    != null) ?   request.getParameter("txt_ne") :"";	
	String clave_estatus			=(request.getParameter("Hestatus")    != null) ?   request.getParameter("Hestatus") :"";	
	String fecha_solicitud_ini			=(request.getParameter("fechaSolic1")    != null) ?   request.getParameter("fechaSolic1") :"";	
	String fecha_solicitud_fin			=(request.getParameter("fechaSolic2")    != null) ?   request.getParameter("fechaSolic2") :"";	
	String fecha_aceptacion_ini			=(request.getParameter("fechaIF1")    != null) ?   request.getParameter("fechaIF1") :"";	
	String fecha_aceptacion_fin			=(request.getParameter("fechaIF2")    != null) ?   request.getParameter("fechaIF2") :"";	
	String operacion 				=(request.getParameter("operacion")    != null) ?   request.getParameter("operacion") :"";	


	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar           = "";
	if(informacion.equals("catologoEpo")){
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setTipoCadena(claveTipoCadena);
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveIf(claveIf);
		catalogo.setMandatoDocumentos("S");
		infoRegresar = catalogo.getJSONElementos();
	}else if(informacion.equals("catologoCadena")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_tipo_epo");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("comcat_tipo_epo");
		catalogo.setOrden("ic_tipo_epo");
		infoRegresar = catalogo.getJSONElementos();
  }else if(informacion.equals("catologoIF")){
		CatalogoIF catalogo = new CatalogoIF();
		catalogo.setClave("ic_if");
		catalogo.setDescripcion("cg_razon_social");
		catalogo.setG_cs_mandato_documento("S");
		catalogo.setIc_epo(clave_epo);
		infoRegresar = catalogo.getJSONElementos();
  }
  else if(informacion.equals("catologoEstatus")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus_man_doc");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("comcat_estatus_man_doc");
		catalogo.setOrden("ic_estatus_man_doc");
		infoRegresar = catalogo.getJSONElementos();
  } else if (	informacion.equals("CatalogoBuscaAvanzada") ) {

	String num_pyme	= (request.getParameter("num_pyme")==null)?"":request.getParameter("num_pyme");
	String rfc_pyme	= (request.getParameter("rfc_pyme")==null)?"":request.getParameter("rfc_pyme");
	String nombre_pyme= (request.getParameter("nombre_pyme")==null)?"":request.getParameter("nombre_pyme");
	String ic_epo		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	//AfiliacionHome afiliacionHome = (AfiliacionHome)ServiceLocator.getInstance().getEJBHome("AfiliacionEJB", AfiliacionHome.class);
	//Afiliacion afiliacion = afiliacionHome.create();
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

	Registros registros = afiliacion.getProveedores("",ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pyme,"","");

	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (registros.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(registros.getString("IC_PYME"));
		elementoCatalogo.setDescripcion(registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("CG_RAZON_SOCIAL"));
		coleccionElementos.add(elementoCatalogo);
	}	
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	 jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenNombrePyme")) {

	//ParametrosDescuentoHome parametrosDescuentoHome = (ParametrosDescuentoHome)ServiceLocator.getInstance().getEJBHome("ParametrosDescuentoEJB", ParametrosDescuentoHome.class);
	//ParametrosDescuento BeanParamDscto = parametrosDescuentoHome.create();
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

	 jsonObj			= new JSONObject();
	String numeroDeProveedor	= (request.getParameter("numeroDeProveedor")!=null)?request.getParameter("numeroDeProveedor"):"";

	Registros registros			= new Registros();

	String ic_pyme					= "";
	String txtCadenasPymes		= "";

	

		registros					= BeanParamDscto.getParametrosPymeNafinSeleccionIF(numeroDeProveedor);

		if(registros != null && registros.next()){
			ic_pyme					= (registros.getString("PYME")	== null)?"":registros.getString("PYME");
			txtCadenasPymes		= (registros.getString("NOMBRE")	== null)?"":registros.getString("NOMBRE");
		}else{
			jsonObj.put("muestraMensaje", new Boolean(true));
			jsonObj.put("textoMensaje", 	"El nafin electrónico no corresponde a una<br>PyME afiliada a la EPO o no existe.");
		
	}

	jsonObj.put("ic_pyme",					ic_pyme									);
	jsonObj.put("txt_nafelec",				numeroDeProveedor						);
	jsonObj.put("txtCadenasPymes",		txtCadenasPymes						);
	jsonObj.put("estadoSiguiente",		"RESPUESTA_OBTENER_NOMBRE_PYME"	);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Consulta")||informacion.equals("GenerarPDF")||informacion.equals("GenerarCSV")){
		
		int start=0,limit=0;
				
		ConsultaMandatoDocumentos paginador=new ConsultaMandatoDocumentos();
		
		paginador.setClave_epo(clave_epo);
		paginador.setClaveTipoCadena(claveTipoCadena);
		paginador.setClave_if(claveIf);
		paginador.setNumero_nafele(numero_nafele);
		paginador.setClave_estatus(clave_estatus);
		paginador.setFecha_solicitud_ini(fecha_solicitud_ini);
		paginador.setFecha_solicitud_fin(fecha_solicitud_fin);
		paginador.setFecha_aceptacion_ini(fecha_aceptacion_ini);
		paginador.setFecha_aceptacion_fin(fecha_aceptacion_fin);
		paginador.setPerfil_usuario((String)session.getAttribute("sesPerfil"));
		
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
			try{
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));	
			}catch(Exception e){
					System.out.println("Error en parametros");
			}
		if (operacion.equals("Generar")) {	//Nueva consulta
				cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		if (informacion.equals("Consulta")){
		String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
		jsonObj = JSONObject.fromObject(consultar);
		infoRegresar=jsonObj.toString();
		}else	 if(informacion.equals("GenerarPDF")){
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				String nombreArchivo = cqhelper.getCreatePageCustomFile(request, start, limit,strDirectorioTemp, "PDF");
				jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF", e);
			}
		
		}else if(informacion.equals("GenerarCSV")){
			String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
}else if (informacion.equals("archivoAcuse")){
	//MandatoDocumentosHome mandatoDocumentosHome = (MandatoDocumentosHome)ServiceLocator.getInstance().getEJBHome("MandatoDocumentosEJB", MandatoDocumentosHome.class);
	//MandatoDocumentos mandatoDocumentosBean = mandatoDocumentosHome.create();
	MandatoDocumentos mandatoDocumentosBean = ServiceLocator.getInstance().lookup("MandatoDocumentosEJB",MandatoDocumentos.class);
	try{
		//JSONObject jsonObj = new JSONObject();
		String folioSolicitud = (request.getParameter("folio_instruccion")!=null)?request.getParameter("folio_instruccion"):"";
		String nafinElectronicoPyme = "";
		String nombrePyme = "";
		String nombreEpo = "";
		String nombreIf = "";
		String nombreDescontante = "";
		String fechaSolicitud = "";
		String horaSolicitud = "";
		String usuarioSolicitud = "";
		
		HashMap informacionSolicitud = mandatoDocumentosBean.obtenerInformacionInstruccion(folioSolicitud);
		String tipoCadenaInstruccion = mandatoDocumentosBean.obtenerTipoCadenaInstruccion(folioSolicitud);
	  
		int numeroRegistros = Integer.parseInt((String)informacionSolicitud.get("numeroRegistros"));
		
		if (numeroRegistros > 0) {
			for (int i = 0; i < numeroRegistros; i++) {
				HashMap solicitud = (HashMap)informacionSolicitud.get("solicitud"+i);
				if (i == 0) {
					nafinElectronicoPyme = (String)solicitud.get("nafinElectronicoPyme");
					nombrePyme = (String)solicitud.get("nombrePyme");							
					nombreIf = (String)solicitud.get("nombreIf");
					nombreDescontante = (String)solicitud.get("nombreDescontante");
					fechaSolicitud = (String)solicitud.get("fechaSolicitud");
					horaSolicitud = (String)solicitud.get("horaSolicitud");
					usuarioSolicitud = (String)solicitud.get("usuarioSolicitud");
				}
				nombreEpo += (String)solicitud.get("nombreEpo");
				if (numeroRegistros > 1) {
					nombreEpo += "\n";
				}
			}
		}

		ConsultaMandatoDocumentos pag = new ConsultaMandatoDocumentos();
		pag.setClaveTipoCadena(tipoCadenaInstruccion);
		pag.setClave_pyme(nombrePyme);
		pag.setNumero_nafele(nafinElectronicoPyme);
		pag.setNombreEpo(nombreEpo);
		pag.setNombreIf(nombreIf);
		pag.setNombreDescontante(nombreDescontante);
		pag.setFolioInstruccion(folioSolicitud);
		pag.setFechaAct(fechaSolicitud);
		pag.setHoraAct(horaSolicitud);
		pag.setUsuarioCarga(usuarioSolicitud);
		String nombreArchivo = pag.crearPageCustomFile(request, null,strDirectorioTemp, "AcusePDF");

		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	
	} catch (Exception e) {
		throw new AppException("Error al generar el archivo", e);
	}	
	
}
	
%>
<%=infoRegresar%>

