<%@ page contentType="application/json;charset=UTF-8"%>
<%@ include file="../22secsession_extjs.jspf" %>
<%@ include file="/appComun.jspf"%>
<%@ page language="java" import="java.io.*,
	java.lang.*,
	java.util.*, 
	java.sql.*, 
	java.io.*,
	net.sf.json.JSONArray,  
	net.sf.json.JSONObject,
	netropology.utilerias.AccesoDB,
	com.jspsmart.upload.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<jsp:useBean id="mySmartUpload" scope="page" class="com.jspsmart.upload.SmartUpload" /> 
<% 
	String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
	String infoRegresar = "",consulta ="", mensaje="";
	String moneda = (request.getParameter("claveMoneda") !=null)?request.getParameter("claveMoneda"):"";
	String tituloMenu = (request.getParameter("tituloMenu")!=null)?request.getParameter("tituloMenu"):"";
	String tituloReporte = (request.getParameter("tituloReporte")!=null)?request.getParameter("tituloReporte"):"";
	String subReporte = (request.getParameter("subReporte")!=null)?request.getParameter("subReporte"):"";
	String cabecera = (request.getParameter("cabecera")!=null)?request.getParameter("cabecera"):"";
	String pieCuadro = (request.getParameter("pieCuadro")!=null)?request.getParameter("pieCuadro"):"";

	AccesoDB ad = new AccesoDB();
	ResultSet rs=null;
	boolean salida=true;
	String auxSalida = "true";
	if (informacion.equals("enviar")) {
		String pedazo="";
		String maximoText="";
		ad.conectarDB();
		try{
			int idcuadro=1;
			String maximum = "select max(idcuadro) as cuadro from cuadros";
			rs = ad.queryDB(maximum);
			while(rs.next())
			{
				maximoText = " Salida máximo = " + rs.getString("cuadro");
				idcuadro += rs.getInt("cuadro");
			}
			ad.cierraStatement(); 
			pedazo = "INSERT INTO CUADROS (idcuadro, idmoneda, titmenu, titreporte, subtitrep, txtcab, txtpie) VALUES (" + idcuadro + "," + moneda +  ",'" + tituloMenu + "','" + tituloReporte +  "','" + subReporte +  "','" + cabecera +  "','" + pieCuadro +  "')";
			ad.ejecutaSQL(pedazo);
			ad.cerrarDB(true);
		}catch(Exception e){
			salida = false;
			auxSalida ="false"+"<BR>Error: subeCuadroExt01.jsp" + e.getMessage();
		}
		if(salida){
			mensaje="Datos insertados";
		}else{
			mensaje="No se insertaron los datos";
		}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("leyendaEncabezado", "CARGA DE CUADROS");
		jsonObj.put("salida","Salida: "+auxSalida);
		jsonObj.put("mensaje", mensaje);
		jsonObj.put("maximoText", maximoText);
		jsonObj.put("pedazo", pedazo);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("subirArchivo")){
		int numFiles=0;
		String resultado = "";
		String strInsert="";
		boolean commit = true;
		try{
			mySmartUpload.initialize(pageContext);
			mySmartUpload.setAllowedFilesList("csv");  
			mySmartUpload.setTotalMaxFileSize(100000);
			mySmartUpload.upload();  
			numFiles = mySmartUpload.save(strpath);
			com.jspsmart.upload.File myFile = mySmartUpload.getFiles().getFile(0);
			if (myFile.getFileName().trim().equals("")){
				resultado = "No se especificó ningún archivo .";
			}else{
				//ad.conectarDB();
				ad.conexionDB();
				//String pedazo = "UPDATE CUADROS set ARCHIVO = '" + myFile.getFileName() + "' where idcuadro = (select max(idcuadro) from cuadros)";
				String pedazo = "UPDATE CUADROS set ARCHIVO = ?, bi_archivo = ? where idcuadro = (select max(idcuadro) from cuadros)";
				System.out.println("VALOR:"+"select max(idcuadro) from cuadros");
				System.out.println(pedazo);
				
				//ad.ejecutaSQL(pedazo);
				java.io.File farchivo = new java.io.File(strpath+myFile.getFileName());
				FileInputStream fis = new FileInputStream(farchivo);
				
				
				PreparedStatement ps = null;
				ps = ad.queryPrecompilado(pedazo);
				ps.setString(1, myFile.getFileName());
				ps.setBinaryStream(2, fis, (int)farchivo.length());
				ps.executeUpdate();
				ps.close();
				
				strInsert =	" INSERT INTO BIT_ALTA_CURVA( " +
							   " IDCURVA,DC_ALTA,CG_NOMBRE_USUARIO,CG_TIPO)" + 
							   " VALUES((select max(idcuadro) from cuadros), Sysdate , ? ,'T')";
									
				System.out.println("Query INSERTA HISTORICO:::"+strInsert);	
				//ad.ejecutaSQL(strInsert);
				ps = ad.queryPrecompilado(strInsert);
				ps.setString(1, strNombreUsuario);
				ps.executeUpdate();
				ps.close();
				//ad.cerrarDB(true);
				resultado = "El archivo "+myFile.getFileName()+" se subió exitosamente.";
			}
	 
		}catch(Exception e){
			commit = false;
			System.out.println("----------------------");
			System.out.println("Error: subeCuadroExt01.data.jsp");
			e.printStackTrace();
			System.out.println("----------------------"); 
		} finally {
			if (ad.hayConexionAbierta()){
				ad.terminaTransaccion(commit);
				ad.cierraConexionDB();
			}
		}
		JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("success", new Boolean(true));
		jsonObj1.put("resultado", resultado);
		infoRegresar = jsonObj1.toString();

	}
%>
<%=infoRegresar%>