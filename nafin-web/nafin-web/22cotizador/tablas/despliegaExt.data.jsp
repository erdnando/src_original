	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,
	java.io.*,
	java.sql.*,
	netropology.utilerias.negocio.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONObject"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
	<%
	
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String informacion         =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String tipoMoneda 			=(request.getParameter("moneda")    != null) ?   request.getParameter("moneda") :"";
	String id = (request.getParameter("id")    != null) ?   request.getParameter("id") :"";
	if(id.equals("")){
	if (informacion.equals("catologoMonedaDist")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();	
	}else if(informacion.equals("Consulta")){
			AccesoDB ad = new AccesoDB();
			ad.conectarDB();
			ResultSet rs=null;
		try{
			
			String pedazo = "";
			String rutas="";

			if(tipoMoneda.equals("1"))
				pedazo = "select * FROM CUADROS where idmoneda = 1 and idcuadro not in (15,16) order by idcuadro asc";
			else if(tipoMoneda.equals("54"))
				pedazo = "select * FROM CUADROS where idmoneda = 54 and idcuadro not in (14) order by idcuadro asc";
			else if(tipoMoneda.equals("75"))
				pedazo = "select * FROM CUADROS where idmoneda = 75 order by idcuadro asc";
			if(!tipoMoneda.equals("") && !pedazo.equals("")){
				rs = ad.queryDB(pedazo);
				while(rs.next())
				{
					rutas+="<TR><TD><a href=\"despliegaExt.jsp?id=" + rs.getString("idcuadro") + "\"><font size=\"1\">" + rs.getString("titmenu") + "</font></a></TD></TR><TR><TD>&nbsp;</TD></TR>";
					//out.println("<TR><TD><a href=\"reporte.jsp?id=" + rs.getString("idcuadro") + "\"><font size=\"1\">" + rs.getString("titmenu") + "</font></a></TD></TR>");
				}
				rs.close();
				ad.cierraStatement();
			}
			jsonObj.put("rutas",rutas);
			jsonObj.put("success",new Boolean(true));
			infoRegresar = jsonObj.toString();
		}catch(Exception e){
			out.println("Error: despliega.jsp" + e.getMessage());
		}finally{
			ad.cierraConexionDB();
		}
	}
	}else{
			
			InformacionGeneralBean datosGenerales = new InformacionGeneralBean();
			String encabezado="<tr><td><img src='../../../nafin/00archivos/15cadenas/15archcadenas/logos/nafinsa.gif' border=0></td>"+
		"<td><font size='1'>NACIONAL FINANCIERA S.N.C.<BR/>"+
		  "Subdirección de Planeación de Tesorería</font></td></tr><tr> <td colspan='2'>&nbsp;</td></tr><BR/><BR/>";
	  String pie="";
			String strGrupoUsuario=strPerfil;
			String usuario=strLogin;
			String strIntermediario="";
			String strTipoIntermediario="";
			String strIdTipoIntermediario="";
			String strIdUsuario="";
			String strQuery = "";
			String queryfecha="";
			boolean regreso = true;
			
			AccesoDB ad = new AccesoDB();
			ResultSet rs = null;    
			ResultSet rs2 = null;    
			ResultSet rsTipo = null;
			ResultSet rsFecha = null;
			ResultSet rsCredit = null;
			ad.conexionDB();
			
			String renglon = "";
			String renglon2 = "";
			String decide = ",0,";
			int i=0;
			int j=0;
			int k=0;
			
			String alfa="";
			boolean flag=true;
			
			
			String notas="";
			
			try
			{
			
				if (datosGenerales.load()) {
				if ("IF".equals(strTipoUsuario)||strGrupoUsuario.equalsIgnoreCase("admin if cotiz")||strGrupoUsuario.equalsIgnoreCase("admin if")||strGrupoUsuario.equalsIgnoreCase("admin if corto")||strGrupoUsuario.equalsIgnoreCase("consul if")||strGrupoUsuario.equalsIgnoreCase("consul if cotiz"))	{
					if (datosGenerales.loadDatosUsuario(iNoCliente)){
					  strTipoIntermediario=(datosGenerales.getTipoIntermediario()).trim();
					  strIdTipoIntermediario=(datosGenerales.getIdTipoIntermediario()).trim();
					}
				}
			
			/*ARS Cambios al 7 de octubre de 2002 FASE 2-2*/
				else {
					if (strGrupoUsuario.equalsIgnoreCase("credito")||strGrupoUsuario.equalsIgnoreCase("admin operc"))
					{
					/*Join que extrae los riesgos y el id de usuario a partir de su login*/
						String risk = "select ic_usuario,riesgo from riesgousr " + 
									  " where ic_usuario ='" + usuario + "'";
						rsCredit = ad.queryDB(risk);
						while(rsCredit.next())
						{
							strTipoIntermediario = rsCredit.getString("ic_usuario");
							strIdTipoIntermediario = rsCredit.getString("riesgo");
						}
						rsCredit.close();
						strTipoIntermediario = "Crédito";
					}
				}
			/*ARS*/
			
					strQuery="select * from Cuadros where idcuadro= " + request.getParameter("id");
					rs = ad.queryDB(strQuery);
			
					while(rs.next())
					{
						
						encabezado+=("<tr><td colspan=\"2\"><font size=1><b>&nbsp;&nbsp;&nbsp;" + rs.getString("titreporte").toUpperCase() + "</b></font></td></tr>");
						encabezado+=("<tr><td colspan=\"2\"><font size=1><b>&nbsp;&nbsp;&nbsp;" + rs.getString("subtitrep") + "</b></font></td></tr>");
						encabezado+=("<tr><td align=right colspan=\"2\"><font size=1>Fecha de Consulta: &nbsp;&nbsp;");
			
				/*fecha*/
			
						queryfecha = "select to_char(sysdate,'dd/mm/rrrr') from dual";
						rsFecha = ad.queryDB(queryfecha);
			
						while(rsFecha.next())
						{
						encabezado+=(rsFecha.getString(1));
						}
			
						rsFecha.close();
			
				/*fecha*/
			
			
						encabezado+=("<tr><td colspan=\"2\">&nbsp;</td></tr>");
						encabezado+=("<tr><td colspan=\"2\"><font size=1>&nbsp;&nbsp;&nbsp;" + rs.getString("txtcab") + "</font></td></tr>");
						encabezado+=("<tr><td colspan=\"2\">&nbsp;</td></tr>");
			
			/*			out.println("<TR><TD>idcuadro = " + rs.getString("idcuadro") + ",idmoneda = " + rs.getString("idmoneda") +",titmenu = " + rs.getString("titmenu") +",titreporte = " + rs.getString("titreporte") +",subtitrep = " + rs.getString("subtitrep") +",txtcab = " + rs.getString("txtcab") +",txtpie = " + rs.getString("txtpie") + ",archivo = " + rs.getString("archivo") +"</TD></TR>");*/
			
						//SE INTEGRA CODIGO PARA OBTENR EL ARCHIVO DE LA BASE
						Blob blob = rs.getBlob("bi_archivo");
						InputStream inputStream = blob.getBinaryStream();
						
						//FileReader fDatos = new FileReader(strpath+rs.getString("archivo"));
						//--FileReader fDatos = new FileReader(strDirTrabajo+"tablas/"+rs.getString("archivo"));
						//FileReader fDatos = new FileReader(".\\applications\\nafin\\nafin-web\\nafin-web\\22cotizador\\tablas\\" + rs.getString("archivo"));
						//--BufferedReader bDatos = new BufferedReader(fDatos);
						BufferedReader bDatos   = new BufferedReader(new InputStreamReader(inputStream,"ISO-8859-1"));
						
						//--InputStream inputStream = new FileInputStream(strDirTrabajo+"tablas/"+rs.getString("archivo"));
						/*
						Reader      reader      = new InputStreamReader(inputStream);
						
						int data = reader.read();
						while(data != -1){
							 char theChar = (char) data;
							 data = reader.read();
							 System.out.print(theChar);
						}						
						reader.close(); 
						*/
						
						
						int contador=0;
						String tokenFinal="";
						String var="";
						String var2="";
						String auxi="";
			
						if (!(strGrupoUsuario.equalsIgnoreCase("TESORERIA")||strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA")||strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("ADMIN OPERC")||strGrupoUsuario.equalsIgnoreCase("TABLAS")))
						{
							if((renglon = bDatos.readLine()) != null) 
							{
								renglon= new String(renglon.getBytes("UTF-8"));
								StringTokenizer datos = new StringTokenizer(renglon, ",");
								while(datos.hasMoreElements()) 
								{
									var = datos.nextToken();
									var2 = strIdTipoIntermediario;
									/*ARS modificaciones para eliminar las comillas*/
										StringTokenizer temporal = new StringTokenizer(var, "\"");
										while(temporal.hasMoreElements())
										{
											if(temporal.nextToken().equals(var2))
												decide += contador + ",";
											contador++;
										}
									/*FIN ARS*/
								}
							}	
						}
			/*ARS modificaciones al 7 de oct de 2002 para usuarios crédito*/
			/*					decide = ",0" + strIdTipoIntermediario;       */
			
						else
						{
							//FileReader fDatos2 = new FileReader(strpath+rs.getString("archivo"));
							//--FileReader fDatos2 = new FileReader(strDirTrabajo+"tablas/"+rs.getString("archivo"));
							//FileReader fDatos2 = new FileReader(".\\applications\\nafin\\nafin-web\\nafin-web\\22cotizador\\tablas\\" + rs.getString("archivo"));
							
							//--BufferedReader bDatos2 = new BufferedReader(fDatos2);
							//BufferedReader bDatos2   = new BufferedReader(new InputStreamReader(inputStream,"ISO-8859-1"));
							
							//--InputStream inputStream2 = new FileInputStream(strDirTrabajo+"tablas/"+rs.getString("archivo"));
							/*
							Reader      reader2      = new InputStreamReader(inputStream);
						
							int data2 = reader2.read();
							while(data2 != -1){
								 char theChar2 = (char) data2;
								 data2 = reader2.read();
								 System.out.print(theChar2);
							}						
							reader2.close();  
							*/
							
							
							if (strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("ADMIN OPERC"))
							{
								if((renglon2 = bDatos.readLine()) != null)
								{
									renglon2= new String(renglon2.getBytes("UTF-8"));
									StringTokenizer datos = new StringTokenizer(renglon2 , ",");
									while(datos.hasMoreElements())
									{
										var = datos.nextToken();
										var2 = strIdTipoIntermediario;
			
										StringTokenizer temporal = new StringTokenizer(var, "\"");
										while(temporal.hasMoreElements())
										{
											tokenFinal = temporal.nextToken();
			
											StringTokenizer creditos = new StringTokenizer(var2, ",");
											while(creditos.hasMoreElements())
											{
												auxi = creditos.nextToken();
												if( tokenFinal.equals(auxi) )
													decide += contador + ",";
											}
											contador++;
										}				
									}
								}	
							}
						}
			/*ARS*/
			
			/*			out.println(" decide: " + decide);*/
						
						contador=0;
						int paranotas=0; 
			/*paranotas es una variable que va cambiando por renglón en la lectura del archivo .xls, cuando se lee la 4a ó 5a 
			línea (dependiendo del tipo de reporte, se procede a tomar en cuenta un arreglo para las notas y su despliegue*/
						boolean bander=true;
						String cstrg;		
						boolean banderauser=true;
							encabezado+=("<tr><td colspan=\"2\">&nbsp;</td></tr>");
							if (!(strGrupoUsuario.equalsIgnoreCase("admin if cotiz")||strGrupoUsuario.equalsIgnoreCase("admin if")||strGrupoUsuario.equalsIgnoreCase("admin if corto")||strGrupoUsuario.equalsIgnoreCase("consul if")||strGrupoUsuario.equalsIgnoreCase("consul if cotiz")))	{
								encabezado+=("<tr><td colspan=\"2\"><font size=1>&nbsp;" + strTipoIntermediario + "&nbsp;</font></td></tr>");
							}
							encabezado+=("<tr><td colspan=\"2\"><table width=\"100%\" border=1 cellpadding=0 cellspacing=0><tr><td><table width=\"100%\">");
						int numCols = 0;
						int numRenglon = 0;
						while((renglon = bDatos.readLine()) != null) 
						{
							numRenglon++;
							encabezado+=("<tr>");
			
							StringTokenizer datos2 = new StringTokenizer(renglon, ",");
							while(datos2.hasMoreElements()) 
							{
								var = datos2.nextToken();
								cstrg = ","+contador+",";
							
								if (!(strGrupoUsuario.equalsIgnoreCase("TESORERIA")||strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA")||strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("ADMIN OPERC")||strGrupoUsuario.equalsIgnoreCase("TABLAS")))
								{					
									if(decide.indexOf(cstrg)>-1)
									{
										if (paranotas==4)
											notas = notas + var + ",";
										else
										{
										/*ARS modificaciones para eliminar las comillas*/
											StringTokenizer caso = new StringTokenizer(var, "\"");
											while(caso.hasMoreElements())
											{
												var2 = caso.nextToken();
											}
										/*FIN ARS*/
											encabezado+=("<td><font size=1>&nbsp;" + var2 + "&nbsp;</font></td>");
											if(numRenglon==1)
												numCols++;
										}
									}
								}
			/*ARS cambios 8 de oct 2002 para reporte de usurios tipo crédito*/
								else 
								{
									if (strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("ADMIN OPERC"))
									{							
										if(decide.indexOf(cstrg)>-1)
										{
											if (paranotas==5)
											{
												notas = notas + var + ",";
											}
											else
											{
												StringTokenizer caso = new StringTokenizer(var, "\"");
												while(caso.hasMoreElements())
												{
													var2 = caso.nextToken();
												}
			
												if(paranotas == 0)
												{
													if (banderauser)
													{
														encabezado+=("<td>&nbsp;</td>");
														banderauser = false;
														if(numRenglon==1)
															numCols++;
													}
			
													strQuery="select * from TIPOINTERMEDIARIO where idTipoIntermediario= " + var2;
													rsTipo = ad.queryDB(strQuery);
			
													while(rsTipo.next())
													{
														encabezado+=("<td><font size=1>&nbsp;" + rsTipo.getString("descripcion") + "&nbsp;</font></td>");
														if(numRenglon==1)
															numCols++;
													}
												}
												else{
													encabezado+=("<td nowrap><font size=1>&nbsp;" + var2 + "&nbsp;</font></td>");
													if(numRenglon==1)
														numCols++;
												}
			
											}
										}
									}
			/*ARS*/
									else
									{
										if (paranotas==5)
										{
											notas = notas + var + ",";
										}
										else
										{
										/*ARS modificaciones para eliminar las comillas*/
											StringTokenizer caso = new StringTokenizer(var, "\"");
											while(caso.hasMoreElements()) 
											{
												var2 = caso.nextToken();
											}
										/*FIN ARS*/
											if(paranotas == 0)
											{
												if (banderauser)
												{
													encabezado+=("<td>&nbsp;</td>");
													banderauser = false;
													if(numRenglon==1)
														numCols++;
												}
			
												strQuery="select * from TIPOINTERMEDIARIO where idTipoIntermediario= " + var2;
												rsTipo = ad.queryDB(strQuery);
											
												while(rsTipo.next())
												{
													encabezado+=("<td><font size=1>&nbsp;" + rsTipo.getString("descripcion") + "&nbsp;</font></td>");
													if(numRenglon==1)
														numCols++;
												}								
											}
											else
												encabezado+=("<td nowrap><font size=1>&nbsp;" + var2 + "&nbsp;</font></td>");
										}
									}
								}
								contador++;
							}
							contador=0;
							encabezado+=("</tr>");
							paranotas++;
						}	
						encabezado+=("</table></td></tr></table></td></tr>");
						encabezado+=("<tr><td colspan=\"2\">&nbsp;</td></tr>");
			//			out.println("<tr><td colspan=\"2\"><font size=1>&nbsp;&nbsp;&nbsp;<a href=\"Formato_Cotizacion_Especifica.doc\" target=\"_blank\">Formato de cotizaci&oacute;n espec&iacute;fica (Archivo Word)</a></font></td></tr>");
			
						
						pie+=("<tr><td colspan=\"2\"><font size=1>&nbsp;&nbsp;&nbsp;" + rs.getString("txtpie") + "</font></td></tr>");
						jsonObj.put("numCols",numCols+"");
					}
				
					rs.close();
			/*			out.println("<tr><td colspan=\"2\">" + notas + "</td></tr>");*/
			
					notas = notas + "0";
					String notemp="";
					int cuentas=0;
			
					StringTokenizer prueba = new StringTokenizer(notas, "\"");
					while(prueba.hasMoreElements()) 
					{
						notemp=notemp+prueba.nextToken();
					}		
					
					jsonObj.put("pie",pie);
					jsonObj.put("encabezado",encabezado);
					jsonObj.put("success",new Boolean(true));
					ad.cierraStatement(); 
				} /*cierre if de carga*/
			}catch(Exception e){
			  System.out.println("----------------------");
			  System.out.println("Error: reporte.jsp");
			  e.printStackTrace();
			  
			  jsonObj.put("mensaje",e.getMessage());
			  jsonObj.put("success",new Boolean(false));
			  System.out.println("----------------------");
			}finally{
			
			infoRegresar = jsonObj.toString();
			ad.terminaTransaccion(regreso);
			datosGenerales.release();
			ad.cierraConexionDB();
			}
	
	}
	
	%>

<%=infoRegresar%>