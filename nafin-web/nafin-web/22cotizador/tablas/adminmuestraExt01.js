Ext.onReady(function(){
	
//--------------------------------HANDLERS-----------------------------------
//---------------------------------------------------------------------------	
	var procesaCargaArchivo = function(opts, success, response) {
		Ext.getCmp('btnGuardar').enable();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var rep = Ext.decode(response.responseText);
		
			if(rep.msgError=='') {
				Ext.getCmp('formArchivo').hide();
				Ext.getCmp('formArchivo').getForm().reset();
				Ext.getCmp('grid').hide();
				Ext.getCmp('txtResultado').setValue(rep.resultado);
				Ext.getCmp('fpResArchivo').show();
				setTimeout(function() {window.location = 'adminmuestraExt01.jsp';}, 3000);
			}else  {
				Ext.MessageBox.alert('Error', rep.msgError);				
			}
		}
		

	}
	var procesarDatos = function (store,arrRegistros,opts){
		var grid = Ext.getCmp('grid');//grid
		var el = grid.getGridEl();	
		
		var jsonData = store.reader.jsonData;
		if(arrRegistros!=null){
			if (!grid.isVisible()) {
				grid.show();
			}
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}else{
			el.unmask();
		}
		
	}
	var modificaInformacionCuadro = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var el = grid.getGridEl();
		var regIdCuadro = registro.get('IDCUADRO');
		var regArchivo = registro.get('ARCHIVO');
		var regIdCuadro = registro.get('IDMONEDA');
		var regArchivo = registro.get('TITMENU');
		Ext.getCmp('grid').hide();
		Ext.getCmp('txtCveCuadroArchivo').setValue(registro.get('IDCUADRO'));
		Ext.getCmp('txtTituloMenu').setValue(registro.get('TITMENU'));
		Ext.getCmp('txtNombreArchivo').setValue("Archivo :  "+registro.get('ARCHIVO'));
		Ext.getCmp('formArchivo').show();
	}
//---------------------------------STORES------------------------------------
//---------------------------------------------------------------------------	
	var catalogoMoneda = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['1','Moneda Nacional'],
			['54','Moneda Extranjera']
		 ]
	});
	var consultaDataGrid = new Ext.data.GroupingStore({
		root : 'registros',   
		url : 'adminmuestraExt01.data.jsp',
		fields : ['clave', 'descripcion','loadMsg'],
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [		
				{name: 'IDCUADRO'},
				{name: 'ARCHIVO'},
				{name:'IDMONEDA'},
				{name: 'TITMENU'},
				{name: 'MONEDA'}
			]
		}),
		groupField: 'IDMONEDA',
		sortInfo:{field: 'IDMONEDA', direction: 'ASC'},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarDatos(null, null, null);
				}
			}
		}
	});
//------------------------------COMPONENTES----------------------------------
//---------------------------------------------------------------------------
	var grid = new Ext.grid.EditorGridPanel({
		store: consultaDataGrid,
		columnLines:false,
		id: 'grid',
		title:'ADMINISTRACI�N - MODIFICA ARCHIVO',
		hidden: false,
		frame: true,
		stripeRows : true,
		loadMask: true,
		height: 400,
		width:850,
		header	  : true,
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		columns: [
			{
				header: '',
				tooltip: '',
				dataIndex: 'IDMONEDA',
				sortable: true,
				width: 10,	
				hidden:true,
				resizable: true,				
				align: 'center'				
			},
			{
				header: '<center>T�tulo Men�</center>',
				tooltip: 'Titulo Menu',
				dataIndex: 'TITMENU',
				sortable: true,
				width: 500,			
				resizable: true,				
				align: 'LEFT'				
			},
			{
				header: '<center>Archivo</center>',
				tooltip: 'Archivo',
				dataIndex: 'ARCHIVO',
				sortable: true,
				width: 200,			
				resizable: true,				
				align: 'LEFT'				
			},
			{
				xtype: 'actioncolumn',
				header: 'Modificar',
				tooltip: 'Modificar',
				dataIndex: 'IDCUADRO',
				width: 100,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Modificar';
							return 'icoModificar';		
															
						},handler: modificaInformacionCuadro
						
					}
				]				
			}
		],
		view: new Ext.grid.GroupingView({  
			forceFit:true,
			groupTextTpl:' {[ values.rs[0].data["IDMONEDA"] == "1" ? "PESOS" : "DOLARES" ]}'
		})
	});
	var elementosFormaArchivo = [
		{ 	
			xtype: 'textfield',
			hidden: false,
			name:'idCuadroArchivo',
			id: 'txtCveCuadroArchivo', 
			value: '',
			hidden:true,
			displayField: 'descripcion',			
			valueField: 'clave'
		},
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtTituloMenu', 
			value: ''
		},
		{ 	
			xtype: 'displayfield',
			hidden: false,
			fieldLabel:'Archivo',
			id: 'txtNombreArchivo', 
			value: ''
		},
		{ 	
			xtype: 'displayfield',
			height: 10, 
			value: '  '
		},
		{
			xtype: 'container',
			id:	'panelIzq',
			labelWidth:70,
			columnWidth:.95,
			defaults: {	msgTarget: 'side',	anchor: '-40'	},
			layout: 'form',
			items: [
				{
					xtype: 'fileuploadfield',
					id: 'fpfArchivo',
					emptyText: 'Ruta del Archivo',
					buttonText: 'Examinar.',
					name: 'archivoCuadros',
					fieldLabel: 'Archivo',
					buttonCfg: {
						width:115
					},
					anchor: '-10'
				}
			]
		}	
		 
	];
	var fpArchivo = new Ext.form.FormPanel({
		id: 'formArchivo',
		layout :'anchor',
		width: 550,
		title: '<center>ADMINISTRACION - CARGA DE ARCHIVOS DE CUADROS<center>',
		frame: true,
		collapsible: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		fileUpload: true,
		labelWidth: 150,
		hidden:false,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaArchivo,
		buttons:[
			{
				xtype:'button',
				text:'SALIR-(NO REEMPLEZAR EL ARCHIVO)',
				id:'btnSalir',
				iconCls: 'icoSalir',
				formBind: true,
				handler: function(boton, evento){
					window.location = 'adminmuestraExt01.jsp';
				}
			},
			{
				xtype:'button',
				text:'Guardar',
				id:'btnGuardar',
				iconCls: 'icoGuardar',
				formBind: true,
				handler: function(boton, evento){
					var nombreArchivo 	= Ext.getCmp("fpfArchivo");
					if( Ext.isEmpty( nombreArchivo.getValue() ) ){
							nombreArchivo.markInvalid("Debe capturar el archivo a subir");
							return;
					}
					var myRegex = /^.+\.([Cc][Ss][Vv])$/;
					var nombreArchivo1 	= nombreArchivo.getValue();
					if( !myRegex.test(nombreArchivo1) ) {
							nombreArchivo.markInvalid("El formato de el Archivo no es v�lido. Formato(s) soportado(s): CSV.");
							return;
					}else{
						boton.disable();
						var form = fpArchivo.getForm();	
						form.submit({
							url: 'adminmuestraExt01.data.jsp?informacion=subeArchivo',
							waitMsg:   				'Subiendo archivo...',
							waitTitle: 				'Por favor espere',
							success:function(form, action, resp) {
								procesaCargaArchivo(null,  true,  action.response );
							},
							failure: function(form, action) {
								 procesaCargaArchivo(null,  true, action.response );
							}
						
						})
				  }
				}
			}
		]
	});
	var elementosFormaResuArchi = [
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtResultado', 
			value: ''
		}
	];
	

	
	var fpResArchivo = new Ext.form.FormPanel({
		id: 'fpResArchivo',
		frame			: true,
		width: 600,		
		autoHeight	: true,
		hidden:true,
		title: '<center>ADMINISTRACI�N - CARGA DE ARCHIVOS DE CUADROS</center>',				
		layout		: 'form',
		style: 'margin:0 auto;',
		//bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaResuArchi,		
		monitorValid: true
	});
	
//-------------------------COMPONENTE PRINCIPAL------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		//height: 'auto',
		//style: 'margin:0 auto;',
		items:[
			NE.util.getEspaciador(20),
			grid,
			fpArchivo,
			fpResArchivo
		]
	});
	consultaDataGrid.load();
	Ext.getCmp('formArchivo').hide();

});