Ext.onReady(function(){

	/********** Variables globales **********/
	var recordType = Ext.data.Record.create([{name:'clave'}, {name:'descripcion'}]);

	/********** Regresa la pantalla a su condición inicial **********/
	function procesoSalir(){
		Ext.getCmp('formaPrincipal').getForm().reset();
		Ext.getCmp('formaReporte1').getForm().reset();
		Ext.getCmp('formaReporte2').getForm().reset();
		Ext.getCmp('formaPrincipal').show();
		Ext.getCmp('grid').hide();
		Ext.getCmp('formaReporte1').hide();
		Ext.getCmp('fpDetalle').hide();
		Ext.getCmp('formaReporte2').hide();
	}

	/********** Obtiene los datos del servidor para llenar el grid**********/
	function procesoConsultar(){
		if(Ext.getCmp('moneda_id').getValue() == ''){
			Ext.getCmp('moneda_id').markInvalid('Seleccione un tipo de moneda');
		} else{
			formaPrincipal.el.mask('Procesando...', 'x-mask-loading');
			consultaData.load({
				params: Ext.apply({
					id_moneda: Ext.getCmp('moneda_id').getValue()
				})
			});
		}
	}

	/********** Obtiene los datos del reporte de acuerdo a la opcion seleccionada en el grid **********/ 
	function generarReporte(rec){
		formaPrincipal.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '22despliega01ext.data.jsp',
			params: Ext.apply({
				informacion: 'Consulta_Datos_Grid',
				id_cuadro:   rec.get('clave')
			}),
			callback: procesarReporte
		});
	}

	/********** Llena los datos del reporte **********/ 
	function procesarReporte(opts, success, response){
		formaPrincipal.el.unmask();
		if(Ext.util.JSON.decode(response.responseText).success == true){

			/***** Oculto o muestro los elementos correspondientes *****/
			Ext.getCmp('formaPrincipal').hide();
			Ext.getCmp('grid').hide();
			Ext.getCmp('formaReporte1').show();
			Ext.getCmp('fpDetalle').show();
			Ext.getCmp('formaReporte2').show();
			/***** Lleno los campos del reporte *****/
			Ext.getCmp('formaReporte1').setTitle(Ext.util.JSON.decode(response.responseText).TITMENU);
			Ext.getCmp('fecha_consulta_id').setValue(Ext.util.JSON.decode(response.responseText).FECHA_CONS);
			Ext.getCmp('tit_reporte_id').setValue(Ext.util.JSON.decode(response.responseText).TITREPORTE);
			Ext.getCmp('subtit_reporte_id').setValue(Ext.util.JSON.decode(response.responseText).SUBTITREP);
			Ext.getCmp('txt_cab_id').setValue(Ext.util.JSON.decode(response.responseText).TXTCAB);
			Ext.getCmp('txt_pie_id').setValue(Ext.util.JSON.decode(response.responseText).TXTPIE);
			Ext.getCmp('archivo_id').setValue(Ext.util.JSON.decode(response.responseText).ARCHIVO);
			Ext.getCmp('id_cuadro_id').setValue(Ext.util.JSON.decode(response.responseText).IDCUADRO);
			/***** Determina si se lee el archivo o no *****/
			if(Ext.getCmp('archivo_id').getValue() == ''){
				Ext.Msg.alert('Error...', 'Error al obtener los datos del archivo.<br> La Tabla no pudo ser Generada.');
				Ext.getCmp('fpDetalle').hide();
			} else{
				Ext.getCmp('fpDetalle').hide();
								
				Ext.Ajax.request({
					url: '22despliega01ext.data.jsp',
					params: {
						informacion: 'Datos_Archivo',
						nombreArchivo: Ext.getCmp('archivo_id').getValue(),
						id_cuadro: Ext.getCmp('id_cuadro_id').getValue()		
					},
					callback: procesarConsultaDataArchivo
			});				
				
				
			}

		} else{
			Ext.Msg.alert('Error...', Ext.util.JSON.decode(response.responseText).mensaje );
		}
	}

	
	/********** Genera el archivo pdf o csv **********/
	function generaArchivo(tipoArchivo){
		
		if(tipoArchivo == 'PDF'){
			Ext.getCmp('btnGeneraPdf').setIconClass('loading-indicator');
		} else{
			Ext.getCmp('btnGeneraCsv').setIconClass('loading-indicator');
		}

		Ext.Ajax.request({
			url: '22despliega01ext.data.jsp',
			params: Ext.apply({
				informacion:    'Genera_Archivo',
				tipo_archivo:   tipoArchivo,				
				nombreArchivo: Ext.getCmp('archivo_id').getValue(),
				id_cuadro: Ext.getCmp('id_cuadro_id').getValue(),						
				tit_reporte:    Ext.getCmp('tit_reporte_id').getValue(),
				subtit_reporte: Ext.getCmp('subtit_reporte_id').getValue(),
				fecha_consulta: Ext.getCmp('fecha_consulta_id').getValue(),
				txt_cab:        Ext.getCmp('txt_cab_id').getValue(),
				txt_pie:        Ext.getCmp('txt_pie_id').getValue()				
			}),
			callback: descargarArchivo
		});
	}

	/********** Descarga el archivo pdf o csv **********/
	function descargarArchivo(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			formaReporte1.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			formaReporte1.getForm().getEl().dom.submit();

		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGeneraPdf').setIconClass('icoPdf');
		Ext.getCmp('btnGeneraCsv').setIconClass('icoXls');
	}

	/********** Proceso para generar el Grid **********/
	var procesarConsultaData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('formaPrincipal');
		var gridConsulta = Ext.getCmp('grid');
		var el = gridConsulta.getGridEl();
		fp.el.unmask();
		el.unmask();
		if (arrRegistros != null){
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0){
				Ext.getCmp('grid').show();
				if(Ext.getCmp('moneda_id').getValue() == '1'){
					Ext.getCmp('grid').setTitle('Moneda Nacional');
				} else if(Ext.getCmp('moneda_id').getValue() == '54'){
					Ext.getCmp('grid').setTitle('Moneda Extranjera');
				}
			} else{
				Ext.getCmp('grid').hide();
			}
		}
	};


	
	

	var procesarConsultaDataArchivo =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonObj = Ext.util.JSON.decode(response.responseText)
			var fpDetalle = Ext.getCmp('fpDetalle');		
				
				consultaDataArchivo.fields =  jsonObj.columnasStore;
				consultaDataArchivo.data =  jsonObj.columnasRecords;
				gridArchivo.columns = jsonObj.columnasGrid;	
				
				fpDetalle.remove("gridArchivo");			
				fpDetalle.add(gridArchivo);
				fpDetalle.el.unmask();
				fpDetalle.doLayout();	
				fpDetalle.show();	
				
			 
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	

	var consultaDataArchivo = {
		xtype: 'jsonstore',
		id:'consultaDataArchivo',
		root: 'registros',
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		fields: null,
		data: null
	};
	
	var gridArchivo = {		
		xtype: 'grid',	
		id:'gridArchivo',	
		store:consultaDataArchivo,		
		columns: [],		
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		frame: true,
		height: 200,
		width: 790,	
		style: 'margin:0 auto;',
		title: ''
	};

	var fpDetalle = {
		xtype: 'panel',
		id: 'fpDetalle',
		layout: 'form',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		width: 800,	
		title: '',
		frame: true,
		hidden: true,
		collapsible: false,
		titleCollapse: false,			
		items:[	],
		monitorValid: true		
	};

	/********** Se crea el store del Grid **********/
	var consultaData = new Ext.data.JsonStore({
		root:            'registros',
		url:             '22despliega01ext.data.jsp',
		baseParams: {
			informacion:  'Catalogo_Cuadros'
		},
		fields: [
			{ name: 'descripcion'},
			{ name: 'clave'}
		],
		totalProperty:   'total',
		messageProperty: 'mensaje',
		autoLoad:        false,
		listeners: {
			load:         procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	/********** Se crea el grid **********/
	var grid = new Ext.grid.EditorGridPanel({
		width:             600,
		height:            300,
		id:                'grid',
		style:             'margin:0 auto;',
		title:             '&nbsp;',
		align:             'center',
		frame:             false,
		border:            true,
		hidden:            true,
		displayInfo:       true,
		loadMask:          true,
		stripeRows:        true,
		store:             consultaData,
		listeners: {
			rowclick: function(grid, rowIndex, colIndex){
				var rec = consultaData.getAt(rowIndex); 
				generarReporte(rec);
			}
		},
		columns: [{
			width:          595,
			header:         'Elija el tipo de reporte que desea obtener',
			dataIndex:      'descripcion',
			align:          'left',
			sortable:       false,
			resizable:      false
		}]
	});

	

	/********** Form Panel Reporte **********/
	var formaReporte1 = new Ext.form.FormPanel({
		width:             800,
		labelWidth:        50,
		id:                'formaReporte1',
		title:             '&nbsp;',
		layout:            'form',
		style:             'margin: 0 auto;',
		frame:             false,
		border:           false,
		autoHeight:        true,
		hidden:            true,
		items: [
			{ width:700,  xtype:'displayfield',  name:'tit_reporte',     id:'tit_reporte_id'   },
			{ width:700,  xtype:'displayfield',  name:'subtit_reporte',  id:'subtit_reporte_id'},
			{ width:700,  xtype:'displayfield',  name:'fecha_consulta',  id:'fecha_consulta_id'},
			{ width:700,  xtype:'displayfield',  name:'txt_cab',         id:'txt_cab_id'       }
		]
	});

	var formaReporte2 = new Ext.form.FormPanel({
		width:             800,
		labelWidth:        50,
		id:                'formaReporte2',
		layout:            'form',
		style:             'margin: 0 auto;',
		frame:             false,
		border:            false,
		autoHeight:        true,
		hidden:            true,
		items: [
			{ width:700,  xtype:'displayfield',  name:'txt_pie',         id:'txt_pie_id'                },
			{ width:700,  xtype:'displayfield',  name:'archivo',         id:'archivo_id',   hidden:true },
			{ width:700,  xtype:'displayfield',  name:'id_cuadro',       id:'id_cuadro_id', hidden:true },
			{ width:700,  xtype:'displayfield',  name:'num_col',         id:'num_col_id',   hidden:true }
		],
		buttons: [
			{ xtype:'button',  text:'Generar PDF',      id:'btnGeneraPdf',  iconCls:'icoPdf',       handler: function(boton, evento){ generaArchivo('PDF') } },
			{ xtype:'button',  text:'Generar Archivo',  id:'btnGeneraCsv',  iconCls:'icoXls',       handler: function(boton, evento){ generaArchivo('CSV') } },
			{ xtype:'button',  text:'Salir',            id:'btnSalir',      iconCls:'icoRegresar',  handler: procesoSalir        }
		]
	});

	/********** Form Panel Principal **********/
	var formaPrincipal = new Ext.form.FormPanel({
		width:             350,
		labelWidth:        70,
		id:                'formaPrincipal',
		title:             'Tablas de Precios',
		layout:            'form',
		style:             'margin: 0 auto;',
		frame:             true,
		border:            true,
		autoHeight:        true,
		items: [{
			width:          160,
			xtype:          'combo',
			id:             'moneda_id',
			name:           'moneda',
			fieldLabel:     '&nbsp;&nbsp; Moneda',
			msgTarget:      'side',
			mode:           'local',
			emptyText:      'Seleccione Moneda',
			triggerAction:  'all',
			forceSelection: true,
			typeAhead:      true,
			allowBlank:     false,
			store:          [['1','Moneda Nacional'], ['54','Moneda Extranjera']]
		}],
		buttons: [{
				xtype:       'button',
				text:        'Consultar',
				id:          'btnBuscar',
				iconCls:     'icoBuscar',
				hidden:      false,
				handler:     procesoConsultar
		}]
	});

	/********** Contenedor Principal **********/
	var pnl = new Ext.Container({
		width:             949,
		id:                'contenedorPrincipal',
		applyTo:           'areaContenido',
		style:             'margin: 0 auto;',
		items: [
			NE.util.getEspaciador(20),
			formaPrincipal,
			NE.util.getEspaciador(10),
			grid,
			formaReporte1,
			fpDetalle,
			formaReporte2
		]
	});

});