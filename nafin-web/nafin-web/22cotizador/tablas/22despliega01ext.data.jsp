<%@ page contentType="application/json;charset=UTF-8" import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		java.math.*,
		java.io.BufferedReader,
		java.io.InputStream,
		java.io.InputStreamReader,
		com.netro.cotizador.ConsTablaPrecios,
		com.netro.cotizador.TablasPrecios,
		netropology.utilerias.negocio.InformacionGeneralBean,
		netropology.utilerias.usuarios.Usuario" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="../22secsession_extjs.jspf"%>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion    = request.getParameter("informacion")    == null?"": (String)request.getParameter("informacion");
String id_moneda      = request.getParameter("id_moneda")      == null?"": (String)request.getParameter("id_moneda");
String id_cuadro      = request.getParameter("id_cuadro")      == null?"": (String)request.getParameter("id_cuadro");
String nombreArchivo  = request.getParameter("nombreArchivo")  == null?"": (String)request.getParameter("nombreArchivo");
String tipo_archivo   = request.getParameter("tipo_archivo")   == null?"": (String)request.getParameter("tipo_archivo");
String datos_grid     = request.getParameter("datos_grid")     == null?"": (String)request.getParameter("datos_grid");
String tit_reporte    = request.getParameter("tit_reporte")    == null?"": (String)request.getParameter("tit_reporte");
String subtit_reporte = request.getParameter("subtit_reporte") == null?"": (String)request.getParameter("subtit_reporte");
String fecha_consulta = request.getParameter("fecha_consulta") == null?"": (String)request.getParameter("fecha_consulta");
String txt_cab        = request.getParameter("txt_cab")        == null?"": (String)request.getParameter("txt_cab");
String txt_pie        = request.getParameter("txt_pie")        == null?"": (String)request.getParameter("txt_pie");
String num_columnas   = request.getParameter("num_columnas")   == null?"": (String)request.getParameter("num_columnas");
String mensaje        = "";
String infoRegresar   = "";
JSONObject resultado  = new JSONObject();

boolean success               = true;
String strGrupoUsuario        = strPerfil;
String usuario                = strLogin;
String strIntermediario       = "";
String strTipoIntermediario   = "";
String strIdTipoIntermediario = "";

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("Catalogo_Cuadros")){

	CatalogoCuadros cat = new CatalogoCuadros();
	cat.setClave("IDCUADRO");
	cat.setDescripcion("TITMENU");
	cat.setIdMoneda(id_moneda);
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Consulta_Datos_Grid")){

	String strIdUsuario  = "";
	String fechaConsulta = "";
	HashMap mapaReporte  = new HashMap();
	HashMap mapaRiesgos  = new HashMap();

	InformacionGeneralBean datosGenerales = new InformacionGeneralBean();
	ConsTablaPrecios consTablaPrecios = new ConsTablaPrecios();

	try{

		if(strTipoUsuario.equals("IF") || strGrupoUsuario.equalsIgnoreCase("admin if cotiz") || strGrupoUsuario.equalsIgnoreCase("admin if") || strGrupoUsuario.equalsIgnoreCase("admin if corto") || strGrupoUsuario.equalsIgnoreCase("consul if") || strGrupoUsuario.equalsIgnoreCase("consul if cotiz")){
			if(datosGenerales.loadDatosUsuario(iNoCliente)){
				strTipoIntermediario=(datosGenerales.getTipoIntermediario()).trim();
				strIdTipoIntermediario=(datosGenerales.getIdTipoIntermediario()).trim();
			}
		} else{
			if(strGrupoUsuario.equalsIgnoreCase("credito") || strGrupoUsuario.equalsIgnoreCase("admin operc")){
				/*extrae los riesgos y el id de usuario a partir de su login*/
				consTablaPrecios.setUsuario(usuario);
				mapaRiesgos            = consTablaPrecios.getIntermediario();
				strTipoIntermediario   = (String)mapaRiesgos.get("IC_USUARIO");
				strIdTipoIntermediario = (String)mapaRiesgos.get("RIESGO");
				strTipoIntermediario   = "Crédito";
			}
		}

		consTablaPrecios.setIdCuadro(Integer.parseInt(id_cuadro));
		mapaReporte = consTablaPrecios.getDatosReporte();
		fechaConsulta = consTablaPrecios.getFecha();

	} catch(Exception e){
		log.warn("Ocurrió un error al obtener los datos de reporte. " + e);
		mensaje = "Ocurrió un error al obtener los datos de reporte. " + e;
		success = false;
	}

	resultado.put("success",    new Boolean(success)                 );
	resultado.put("mensaje",    mensaje                              );
	resultado.put("IDCUADRO",   (String)mapaReporte.get("IDCUADRO")  );
	resultado.put("TITMENU",    (String)mapaReporte.get("TITMENU")   );
	resultado.put("TITREPORTE", (String)mapaReporte.get("TITREPORTE"));
	resultado.put("SUBTITREP",  (String)mapaReporte.get("SUBTITREP") );
	resultado.put("TXTCAB",     (String)mapaReporte.get("TXTCAB")    );
	resultado.put("TXTPIE",     (String)mapaReporte.get("TXTPIE")    );
	resultado.put("ARCHIVO",    (String)mapaReporte.get("ARCHIVO")   );
	resultado.put("FECHA_CONS", "Fecha de consulta: " + fechaConsulta);
	infoRegresar = resultado.toString();

} else if(informacion.equals("Datos_Archivo")  ||   informacion.equals("Genera_Archivo") ){

	JSONArray registros    = new JSONArray();
	HashMap   mapaRiesgos  = new HashMap();
	String    numCol       = "0";
	String    total        = "0";
	String    strReg       = "";
	String    width        = "800";

	ConsTablaPrecios consTablaPrecios = new ConsTablaPrecios();
	InformacionGeneralBean datosGenerales = new InformacionGeneralBean();

	try{

//-----------------		AQUI EMPIEZA MI CODIGO 12/04/2016 AGUSTIN BAUTISTA RUIZ		-------------------
		if(strTipoUsuario.equals("IF") || strGrupoUsuario.equalsIgnoreCase("admin if cotiz") || strGrupoUsuario.equalsIgnoreCase("admin if") || strGrupoUsuario.equalsIgnoreCase("admin if corto") || strGrupoUsuario.equalsIgnoreCase("consul if") || strGrupoUsuario.equalsIgnoreCase("consul if cotiz")){
			if(datosGenerales.loadDatosUsuario(iNoCliente)){
				strTipoIntermediario=(datosGenerales.getTipoIntermediario()).trim();
				strIdTipoIntermediario=(datosGenerales.getIdTipoIntermediario()).trim();
			}
			//System.err.println("strTipoIntermediario: " + strTipoIntermediario);
			//System.err.println("strIdTipoIntermestrPerfil: " + strIdTipoIntermediario);
		} else{
			if(strGrupoUsuario.equalsIgnoreCase("credito") || strGrupoUsuario.equalsIgnoreCase("admin operc")){
				//extrae los riesgos y el id de usuario a partir de su login
				consTablaPrecios.setUsuario(usuario);
				mapaRiesgos            = consTablaPrecios.getIntermediario();
				strTipoIntermediario   = (String)mapaRiesgos.get("IC_USUARIO");
				strIdTipoIntermediario = (String)mapaRiesgos.get("RIESGO");
				strTipoIntermediario   = "Crédito";
			}
		}
		int    contador   = 0;
		String tokenFinal = "";
		String var        = "";
		String var2       = "";
		String auxi       = "";
		String renglon    = "";
		String decide     = ",0,";

		// Hago la consulta para obtener el archivo
		InputStream  inStream     = null;
		consTablaPrecios.setIdCuadro(Integer.parseInt(id_cuadro));
		inStream = consTablaPrecios.leeArchivo();
		BufferedReader bDatos = new BufferedReader(new InputStreamReader(inStream,"ISO-8859-1"));

		if (	!(strGrupoUsuario.equalsIgnoreCase("TESORERIA")     ||
				strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA") ||
				strGrupoUsuario.equalsIgnoreCase("CREDITO")         ||
				strGrupoUsuario.equalsIgnoreCase("ADMIN OPERC")     ||
				strGrupoUsuario.equalsIgnoreCase("TABLAS"))){

			if((renglon = bDatos.readLine()) != null){
				StringTokenizer datos = new StringTokenizer(renglon, ",");
				while(datos.hasMoreElements()){
					var = datos.nextToken();
					var2 = strIdTipoIntermediario;
					//Se eliminan comillas
					StringTokenizer temporal = new StringTokenizer(var, "\"");
					while(temporal.hasMoreElements()){
						if(temporal.nextToken().equals(var2))
							decide += contador + ",";
						contador++;
					}
				}
			}

		} else{
			if (strGrupoUsuario.equalsIgnoreCase("CREDITO") || strGrupoUsuario.equalsIgnoreCase("ADMIN OPERC")){
				if((renglon = bDatos.readLine()) != null){
					StringTokenizer datos = new StringTokenizer(renglon, ",");
					while(datos.hasMoreElements()){
						var = datos.nextToken();
						var2 = strIdTipoIntermediario; // strIdTipoIntermediario = null
						StringTokenizer temporal = new StringTokenizer(var, "\"");
						while(temporal.hasMoreElements()){
							tokenFinal = temporal.nextToken();
							StringTokenizer creditos = new StringTokenizer(var2, ",");
							while(creditos.hasMoreElements()){
								auxi = creditos.nextToken();
								if( tokenFinal.equals(auxi) )
									decide += contador + ",";
							}
							contador++;
						}
					}
				}
			}

		}
                //EGOY Julio 2017
                bDatos.close();
                bDatos=null;
                inStream.close();
                inStream=null;
                inStream = consTablaPrecios.leeArchivo();
                bDatos = new BufferedReader(new InputStreamReader(inStream,"ISO-8859-1"));

/*
paranotas es una variable que va cambiando por renglón en la lectura del archivo .xls, cuando se lee la 4a ó 5a 
línea (dependiendo del tipo de reporte, se procede a tomar en cuenta un arreglo para las notas y su despliegue
*/
		contador            = 0;
		int paranotas       = 0;
		int numCols         = 0;
		int numRenglon      = 0;
		int tamanioMapa     = 0;
		int contKeys        = 0;
		boolean bander      = true;
		boolean banderauser = true;
		String cstrg        = "";
		String notas        = "";

		if (!(strGrupoUsuario.equalsIgnoreCase("admin if cotiz")||strGrupoUsuario.equalsIgnoreCase("admin if")||strGrupoUsuario.equalsIgnoreCase("admin if corto")||strGrupoUsuario.equalsIgnoreCase("consul if")||strGrupoUsuario.equalsIgnoreCase("consul if cotiz")))	{
			resultado.put("STR_TIPO_INTERMEDIARIO", strTipoIntermediario);
		} else{
			resultado.put("STR_TIPO_INTERMEDIARIO", "");
		}

		HashMap columnas = new HashMap();
		JSONArray filas  = new JSONArray();

		while((renglon = bDatos.readLine()) != null){
			//System.err.println("renglon: " + renglon);
			columnas = new HashMap();
			numRenglon++;
			StringTokenizer datos2 = new StringTokenizer(renglon, ",");
			while(datos2.hasMoreElements()){
				var = datos2.nextToken();
				cstrg = ","+contador+",";
				if (!(strGrupoUsuario.equalsIgnoreCase("TESORERIA")       ||
						strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA") ||
						strGrupoUsuario.equalsIgnoreCase("CREDITO")         ||
						strGrupoUsuario.equalsIgnoreCase("ADMIN OPERC")     ||
						strGrupoUsuario.equalsIgnoreCase("TABLAS"))){
					if(decide.indexOf(cstrg)>-1){
						if (paranotas==4){
							notas = notas + var + ",";
						} else{
							// eliminar las comillas
							StringTokenizer caso = new StringTokenizer(var, "\"");
							while(caso.hasMoreElements()){
								var2 = caso.nextToken();
							}
							columnas.put("CAMPO_" + (contKeys++), var2);
							if(numRenglon==1)
								numCols++;
						}
					}
				} else{
					if (strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("ADMIN OPERC")){
						if(decide.indexOf(cstrg)>-1){
							if (paranotas==5){
								notas = notas + var + ",";
							} else{
								StringTokenizer caso = new StringTokenizer(var, "\"");
								while(caso.hasMoreElements()){
									var2 = caso.nextToken();
								}
								if(paranotas == 0){
									if (banderauser){
										columnas.put("CAMPO_" + (contKeys++), "");
										banderauser = false;
										if(numRenglon==1)
											numCols++;
									}
									String descripcion = consTablaPrecios.getTipoIntermediario(var2);
									if(descripcion != null){
										columnas.put("CAMPO_" + (contKeys++), descripcion);
										if(numRenglon==1)
											numCols++;
									}
								} else{
									columnas.put("CAMPO_" + (contKeys++), var2);
									if(numRenglon==1)
										numCols++;
								}
							}
						}
					} else{
						if (paranotas==5){
							notas = notas + var + ",";
						} else{
							StringTokenizer caso = new StringTokenizer(var, "\"");
							while(caso.hasMoreElements()){
								var2 = caso.nextToken();
							}
							if(paranotas == 0){
								if (banderauser){
									columnas.put("CAMPO_" + (contKeys++), "");
									banderauser = false;
									if(numRenglon==1)
										numCols++;
								}
								String descripcion = consTablaPrecios.getTipoIntermediario(var2);
								if(descripcion != null){
									columnas.put("CAMPO_" + (contKeys++), descripcion);
									if(numRenglon==1)
										numCols++;
								}
							} else{
								columnas.put("CAMPO_" + (contKeys++), var2);
							}
						}
					}
				}
				contador++;
			}
			contador=0;
			paranotas++;
			contKeys=0;
			//System.err.println("columnas: " + columnas.values());
			if(numRenglon == 1){
				tamanioMapa = columnas.size();
				//System.err.println("tamanioMapa: " + tamanioMapa);
			}
			if(columnas.size() > 0){
				filas.add(columnas);
			}
		}
/*DEBUG
for(int i=0; i<filas.size(); i++){
	System.err.println("FILAS: "+  filas.get(i));
}
*/
//-----------------		TERMINA MI CODIGO 12/04/2016 AGUSTIN BAUTISTA RUIZ		-------------------
		if(informacion.equals("Datos_Archivo") ){

		// Genero el grid dinámico
		StringBuffer columnasGrid    = new StringBuffer();
		StringBuffer columnasStore   = new StringBuffer();
		StringBuffer columnasRecords = new StringBuffer();

		columnasGrid.append(" [ ");
		columnasStore.append(" [ ");
		columnasRecords.append(" [ ");

		for(int i=0; i<filas.size(); i++){ // Este for cuenta las filas
			JSONObject auxiliar = filas.getJSONObject(i);
			columnasRecords.append(" { ");
			for(int j = 0; j < auxiliar.size(); j++){ // Este for cuenta las columnas
				String  campoValor = (auxiliar.getString("CAMPO_" + j)).equals("null")?"" : auxiliar.getString("CAMPO_" + j);
				String  nombreCampo = "CAMPO_" + j;

				if(i==0){
					columnasStore.append("  {  name: '"+nombreCampo+"',  mapping : '"+nombreCampo+"' },");
					int porc = 800/tamanioMapa;
					columnasGrid.append("  {  ");
					columnasGrid.append(" header: '', ");
					columnasGrid.append(" tooltip: '"+nombreCampo+"', ");
					columnasGrid.append(" width: "+porc+", ");
					columnasGrid.append(" align: 'LEFT', ");
					columnasGrid.append(" dataIndex: '"+nombreCampo+"' ");
					columnasGrid.append(" },");
				}

				columnasRecords.append("'"+nombreCampo+"'"+":'"+campoValor+"' , ");

			}

			columnasRecords.append(" },");

		}

		columnasGrid.deleteCharAt(columnasGrid.length()-1);
		columnasGrid.append(" ] ");
		columnasStore.deleteCharAt(columnasStore.length()-1);
		columnasStore.append(" ] ");
		columnasRecords.deleteCharAt(columnasRecords.length()-1);
		columnasRecords.append(" ] ");

			infoRegresar = "{\"success\": true, \"columnasGrid\": " + columnasGrid.toString() + 
			", \"columnasStore\": " + columnasStore.toString() +
			", \"columnasRecords\": {\"registros\": " + columnasRecords.toString() +
			", \"total\": "+filas.size()+" }  }";

		} else if(informacion.equals("Genera_Archivo")){

			TablasPrecios tablasPrecios = new TablasPrecios();
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(tablasPrecios);

			System.out.println("tit_reporte  "+tit_reporte);
			System.out.println("subtit_reporte  "+subtit_reporte);
			System.out.println("fecha_consulta  "+fecha_consulta);
			System.out.println("txt_cab  "+txt_cab);
			System.out.println("txt_pie  "+txt_pie);
			System.out.println("numCol  "+tamanioMapa);

			System.out.println("total  "+filas.size());
			System.out.println("tipo_archivo  "+tipo_archivo);
			System.out.println("registros  "+filas);

			if(filas.size()>0){
				tablasPrecios.setArrayGrid(filas);
				tablasPrecios.setMuestraGrid(true);
			} else{
				tablasPrecios.setMuestraGrid(false);
			}

			tablasPrecios.setTituloReporte(tit_reporte);
			tablasPrecios.setSubtituloReporte(subtit_reporte);
			tablasPrecios.setFechaConsulta(fecha_consulta);
			tablasPrecios.setCabeceraReporte(txt_cab);
			tablasPrecios.setPieReporte(txt_pie);
			tablasPrecios.setNumColumnas(tamanioMapa);
			try{
				nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipo_archivo);
				success = true;
				resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e){
				throw new AppException("Error al generar el archivo. ", e);
			}

			resultado.put("success", new Boolean(success));
			infoRegresar = resultado.toString();

		}

	} catch(Exception e){
		mensaje = "Ocurrió un error al leer los datos del archivo. " + e;
		strReg  = "";
		total   = "0";
		success = false;
		log.warn(mensaje);
        e.printStackTrace();
	}


}
%>
<%=infoRegresar%>