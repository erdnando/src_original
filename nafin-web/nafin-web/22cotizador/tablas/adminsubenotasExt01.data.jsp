<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		com.netro.cadenas.*,
		com.netro.catalogos.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion   = request.getParameter("informacion")  == null?"": (String)request.getParameter("informacion");
String idNota			= request.getParameter("idNota")  == null?"": (String)request.getParameter("idNota");
String nota				= request.getParameter("nota")    		== null?"": (String)request.getParameter("nota");
String consulta		= "";
String mensaje       = "";
String infoRegresar  = "";

boolean success      = true;
boolean err      		= false;

if(informacion.equals("Consultar")){
	
	JSONObject jsonObj   = new JSONObject();
	JSONObject resultado = new JSONObject();
	PaginadorGenerico pG = new PaginadorGenerico();	
	try{ 
		pG.setCampos("*");
		pG.setTabla("CATALOGONOTAS");
		pG.setOrden("IDNOTA");
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pG);
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+ queryHelper.getIdsSize() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	infoRegresar = jsonObj.toString();				
	} catch(Exception e){
		log.debug("Error al consultar: "+ e);
		mensaje  = "Ocurrio un error al Consultar.";
		success 	= false;
		err		= true;
	}

} else if(informacion.equals("Enviar") || informacion.equals("Eliminar") ){
	
	JSONObject jsonObj   = new JSONObject();
	JSONObject resultado = new JSONObject();
	PaginadorGenerico pG = new PaginadorGenerico();	
	try{ 
		ActualizacionCatalogosBean ac = new ActualizacionCatalogosBean();
		MantenimientoCatalogo mC = new MantenimientoCatalogo ();
		if(informacion.equals("Enviar")){
			mC.setTabla(" CATALOGONOTAS ");
			mC.setClaveVal("nota='"+nota+"' ");
			mC.setCondicion(" idnota= ?");
			mC.setBind(idNota);		
			ac.actualizarCatalogo(mC);
			mensaje = "El registro fue Actualizado.";	
			
		} else if(informacion.equals("Eliminar")){
			mC.setTabla(" CATALOGONOTAS ");
			mC.setCondicion(" idnota= ?");
			mC.setBind(idNota);
			int regAfectados=ac.eliminarDelCatalogo(mC);
			if(regAfectados==1){
				mensaje = "El registro fue eliminado.";
			} else {
				mensaje = "No se puede eliminar";
				err	  = true;
			}
		}	
	} catch(Exception e){
		log.debug("Error al actualizar: "+ e);
		mensaje  = "No se actualizaron los datos.";
		success 	= false;
		err		= true;
	}
	resultado.put("success",   new Boolean(success));
	resultado.put("mensaje",   mensaje             );
	resultado.put("error",     new Boolean(err));	
	infoRegresar = resultado.toString();
} 
%>
<%=infoRegresar%>