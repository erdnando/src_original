		<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,
	java.io.*,
	netropology.utilerias.negocio.*,
	com.netro.model.catalogos.*,
	com.netro.pdf.*,
	net.sf.json.JSONObject"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
	<%
	
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String informacion         =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String tipoMoneda 			=(request.getParameter("moneda")    != null) ?   request.getParameter("moneda") :"";
	String id = (request.getParameter("id")    != null) ?   request.getParameter("id") :"";
	InformacionGeneralBean datosGenerales= new InformacionGeneralBean();
	
	if(informacion.equals("archivoPDF")){
	
		String strGrupoUsuario=strPerfil;
		String usuario=strLogin;
		String strIntermediario="";
		String strTipoIntermediario=""; 
		String strIdTipoIntermediario="";
		String strIdUsuario="";
		String strQuery = "";
		String queryfecha="";
		boolean regreso = true;
		
		AccesoDB ad = new AccesoDB();
		ResultSet rs = null;    
		ResultSet rs2 = null;    
		ResultSet rsTipo = null;
		ResultSet rsFecha = null;
		ResultSet rsCredit = null;
		ad.conexionDB();
		
		String renglon = "";
		String renglon2 = "";
		String decide = ",0,";
		int i=0;
		int j=0;
		int k=0;
		
		String alfa="";
		boolean flag=true;
		
		String notas="";
		
		//>>CREA REPORTE PDF
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = archivo.nombreArchivo()+".pdf";
		//local String nombreArchivo = "nelson2.pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		//local 	ComunesPDF pdfDoc = new ComunesPDF(2,"nelson2.pdf");
		boolean validaPDF=false;
		//<<CREA REPORTE PDF
		String numCols = (request.getParameter("numCols")==null)?"1":request.getParameter("numCols");
		try
		{
			if (datosGenerales.load()) {
				if ("IF".equals(strTipoUsuario)||strGrupoUsuario.equalsIgnoreCase("admin if")||strGrupoUsuario.equalsIgnoreCase("admin if corto")||strGrupoUsuario.equalsIgnoreCase("consul if"))	{
					if (datosGenerales.loadDatosUsuario(iNoCliente)){
					  strTipoIntermediario=(datosGenerales.getTipoIntermediario()).trim();
					  strIdTipoIntermediario=(datosGenerales.getIdTipoIntermediario()).trim();
					}
				}
		/*ARS Cambios al 7 de octubre de 2002 FASE 2-2*/
				else {
					if (strGrupoUsuario.equalsIgnoreCase("credito")||strGrupoUsuario.equalsIgnoreCase("admin operc"))
					{
						/*Join que extrae los riesgos y el id de usuario a partir de su login*/
						String risk = "select ic_usuario,riesgo from riesgousr " + 
								  " where ic_usuario ='" + usuario +"'";
						rsCredit = ad.queryDB(risk);
						while(rsCredit.next())
						{
							strTipoIntermediario = rsCredit.getString("ic_usuario");
							strIdTipoIntermediario = rsCredit.getString("riesgo");
						}
						rsCredit.close();
						strTipoIntermediario = "Crédito";
					}
				}
		/*ARS*/
		
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			/* 
				 pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
														((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
											 (String)session.getAttribute("sesExterno"),
														(String) session.getAttribute("strNombre"),
															(String) session.getAttribute("strNombreUsuario"),
											 (String)session.getAttribute("strLogo"),"uno");
			*/
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String)session.getAttribute("strNombre"),
					(String)session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),
					(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		
				pdfDoc.setTable(3);
				//pdfDoc.setCellImage((String)session.getAttribute("strLogo"),ComunesPDF.LEFT,imageWidthLimit);
				//local pdfDoc.setCellImage("nafinsa.gif",ComunesPDF.LEFT,imageWidthLimit);
				 pdfDoc.setCell("\n\n\nNACIONAL FINANCIERA S.N.C.\nSubdirección de Planeación de Tesorería","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
				pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
				 pdfDoc.addTable();
				pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("\n\n","formas",ComunesPDF.RIGHT);
				
				strQuery="select * from Cuadros where idcuadro= " + request.getParameter("id");
				rs = ad.queryDB(strQuery);
				while(rs.next())
				{
					String tituloReporte = Comunes.reemplaza(rs.getString("titreporte").toUpperCase(),"<FONT SIZE=+1>","");
					tituloReporte = Comunes.reemplaza(tituloReporte,"</FONT>","");
		
					pdfDoc.addText(tituloReporte,"formasB",ComunesPDF.CENTER);
					pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
					String subtituloReporte = Comunes.reemplaza(rs.getString("subtitrep").toUpperCase(),"<FONT SIZE=-1>","");
					subtituloReporte = Comunes.reemplaza(subtituloReporte,"</FONT><BR>","");
					pdfDoc.addText(subtituloReporte,"formas",ComunesPDF.CENTER);
					pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
		
			/*fecha*/
					queryfecha = "select to_char(sysdate,'dd/mm/rrrr') from dual";
					rsFecha = ad.queryDB(queryfecha);
					while(rsFecha.next())
					{
						//pdfDoc.addText("Fecha de Consulta     "+rsFecha.getString(1),"formas",ComunesPDF.RIGHT);
					}
					rsFecha.close();
			/*fecha*/
					pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);

					String Requisitos = Comunes.reemplaza(rs.getString("txtcab"),"<FONT size=-1 color=blue>","");
					
					Requisitos = Comunes.reemplaza(Requisitos,"<FONT size=2 color=purple>","");
					Requisitos = Comunes.reemplaza(Requisitos,"<U>","");
					Requisitos = Comunes.reemplaza(Requisitos,"</U>","");
					Requisitos = Comunes.reemplaza(Requisitos,"<BR>","");
					Requisitos = Comunes.reemplaza(Requisitos,"<OL>","");
					Requisitos = Comunes.reemplaza(Requisitos,"</OL>","");
					Requisitos = Comunes.reemplaza(Requisitos,"<LI>","");
					Requisitos = Comunes.reemplaza(Requisitos,"</LI>","");
					Requisitos = Comunes.reemplaza(Requisitos,"</FONT>","");
		
					StringTokenizer RequisitosST = new StringTokenizer(Requisitos,"\r\n");
					Requisitos = RequisitosST.nextToken() + "\r\n\r\n";
					int numeroRequisitos=1;
					while(RequisitosST.hasMoreTokens()){
						Requisitos += numeroRequisitos + ".  " + RequisitosST.nextToken()+"\r\n";
						numeroRequisitos++;
					}
					pdfDoc.addText(Requisitos,"formas",ComunesPDF.LEFT);
					//FileReader fDatos = new FileReader(strDirTrabajo+"tablas/"+rs.getString("archivo"));
					//BufferedReader bDatos = new BufferedReader(fDatos);
					
					Blob blob = rs.getBlob("bi_archivo");
					InputStream inputStream = blob.getBinaryStream();
					BufferedReader bDatos   = new BufferedReader(new InputStreamReader(inputStream,"ISO-8859-1"));
					
					
					int contador=0;
					String tokenFinal="";
					String var="";
					String var2="";
					String auxi="";
					if (!(strGrupoUsuario.equalsIgnoreCase("TESORERIA")||strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA")||strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("admin operc")||strGrupoUsuario.equalsIgnoreCase("TABLAS")))
					{
						if((renglon = bDatos.readLine()) != null) 
						{
							StringTokenizer datos = new StringTokenizer(renglon, ",");
							while(datos.hasMoreElements()) 
							{
								var = datos.nextToken();
								var2 = strIdTipoIntermediario;
								/*ARS modificaciones para eliminar las comillas*/
									StringTokenizer temporal = new StringTokenizer(var, "\"");
									while(temporal.hasMoreElements())
									{
										if(temporal.nextToken().equals(var2))
											decide += contador + ",";
										contador++;
									}
								/*FIN ARS*/
							}
						}	
					}
		/*ARS modificaciones al 7 de oct de 2002 para usuarios crédito*/
		/*					decide = ",0" + strIdTipoIntermediario;       */
					else
					{
						//FileReader fDatos2 = new FileReader(strDirTrabajo+"tablas/"+rs.getString("archivo"));
						// local FileReader fDatos2 = new FileReader(".\\applications\\nafin\\nafin-web\\nafin-web\\22cotizador\\tablas\\" + rs.getString("archivo"));
						
						//BufferedReader bDatos2 = new BufferedReader(fDatos2);
						Blob blob2 = rs.getBlob("bi_archivo");
						InputStream inputStream2 = blob2.getBinaryStream();
						BufferedReader bDatos2   = new BufferedReader(new InputStreamReader(inputStream2,"ISO-8859-1"));
						if (strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("admin operc"))
						{
							if((renglon2 = bDatos2.readLine()) != null)
							{
								StringTokenizer datos = new StringTokenizer(renglon2 , ",");
								while(datos.hasMoreElements())
								{
									var = datos.nextToken();
									var2 = strIdTipoIntermediario;
		
									StringTokenizer temporal = new StringTokenizer(var, "\"");
									while(temporal.hasMoreElements())
									{
										tokenFinal = temporal.nextToken();
		
										StringTokenizer creditos = new StringTokenizer(var2, ",");
										while(creditos.hasMoreElements())
										{
											auxi = creditos.nextToken();
											if( tokenFinal.equals(auxi) )
												decide += contador + ",";
										}
										contador++;
									}				
								}
							}	
						}
					}
		/*ARS*/
		/*			out.println(" decide: " + decide);*/
					contador=0;
					int paranotas=0; 
		/*paranotas es una variable que va cambiando por renglón en la lectura del archivo .xls, cuando se lee la 4a ó 5a 
		línea (dependiendo del tipo de reporte, se procede a tomar en cuenta un arreglo para las notas y su despliegue*/
					boolean bander=true;
					String cstrg;		
					boolean banderauser=true;
					
					if (!(strGrupoUsuario.equalsIgnoreCase("admin if cotiz")||strGrupoUsuario.equalsIgnoreCase("admin if")||strGrupoUsuario.equalsIgnoreCase("admin if corto")||strGrupoUsuario.equalsIgnoreCase("consul if")||strGrupoUsuario.equalsIgnoreCase("consul if cotiz")))	{
						pdfDoc.addText(strTipoIntermediario,"formas",ComunesPDF.LEFT);
						pdfDoc.addText("\n","formas",ComunesPDF.LEFT);
					}
		
					/*while(!pdfDoc.newPage()){
						pdfDoc.addText("\n","formas",ComunesPDF.LEFT);
					}*/
		
					
					int num_renglon=0;
					/*while((renglon = bDatosCount.readLine()) != null){
						numCols++;
					}*/
					
					while((renglon = bDatos.readLine()) != null) 
					{
						num_renglon++;
						StringTokenizer datos2 = new StringTokenizer(renglon, ",");
						if(num_renglon==1){
							pdfDoc.setTable(Integer.parseInt(numCols));
						}
						datos2 = new StringTokenizer(renglon, ",");				
						while(datos2.hasMoreElements()) 
						{
							var = datos2.nextToken();
							cstrg = ","+contador+",";
							if (!(strGrupoUsuario.equalsIgnoreCase("TESORERIA")||strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA")||strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("admin operc")||strGrupoUsuario.equalsIgnoreCase("TABLAS")))
							{					
								if(decide.indexOf(cstrg)>-1)
								{
									if (paranotas==4)
										notas = notas + var + ",";
									else
									{
									/*ARS modificaciones para eliminar las comillas*/
										StringTokenizer caso = new StringTokenizer(var, "\"");
										while(caso.hasMoreElements())
										{
											var2 = caso.nextToken();
										}
									/*FIN ARS*/
										pdfDoc.setCell(var2,"formasmen",ComunesPDF.LEFT);
									}
								}
							}
		/*ARS cambios 8 de oct 2002 para reporte de usurios tipo crédito*/
							else 
							{
								if (strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("admin operc"))
								{							
									if(decide.indexOf(cstrg)>-1)
									{
										if (paranotas==5)
										{
											notas = notas + var + ",";
										}
										else
										{
											StringTokenizer caso = new StringTokenizer(var, "\"");
											while(caso.hasMoreElements())
											{
												var2 = caso.nextToken();
											}
		
											if(paranotas == 0)
											{
												if (banderauser)
												{
													pdfDoc.setCell("","formasmen",ComunesPDF.LEFT);
													banderauser = false;
												}
												strQuery="select * from TIPOINTERMEDIARIO where idTipoIntermediario= " + var2;
												rsTipo = ad.queryDB(strQuery);
												while(rsTipo.next())
												{
													pdfDoc.setCell(rsTipo.getString("descripcion"),"formasmen",ComunesPDF.LEFT);
												}								
											}
											else{
												pdfDoc.setCell(var2,"formasmen",ComunesPDF.LEFT);
											}
										}
									}
								}
		/*ARS*/
								else
								{
									if (paranotas==5)
									{
										notas = notas + var + ",";
									}
									else
									{
									/*ARS modificaciones para eliminar las comillas*/
										StringTokenizer caso = new StringTokenizer(var, "\"");
										while(caso.hasMoreElements()) 
										{
											var2 = caso.nextToken();
										}
									/*FIN ARS*/
										if(paranotas == 0)
										{
											if (banderauser)
											{
												pdfDoc.setCell("","formasmen",ComunesPDF.LEFT);
												banderauser = false;
											}
											strQuery="select * from TIPOINTERMEDIARIO where idTipoIntermediario= " + var2;
											rsTipo = ad.queryDB(strQuery);
											while(rsTipo.next())
											{
												pdfDoc.setCell(rsTipo.getString("descripcion"),"formasmen",ComunesPDF.LEFT);
											}								
										}
										else{
											pdfDoc.setCell(var2,"formasmen",ComunesPDF.LEFT);
										}
									}
								}
							}
							contador++;
						}
						contador=0;
						paranotas++;
					}	
					pdfDoc.addTable();
		
					pdfDoc.addText("\n","formas",ComunesPDF.LEFT);
					String pieReporte = Comunes.reemplaza(rs.getString("txtpie"),"<FONT size=-1>","");
					pieReporte = Comunes.reemplaza(pieReporte,"<FONT size=-1 >","");
					pieReporte = Comunes.reemplaza(pieReporte,"<FONT size=2 color=purple>","");		
					pieReporte = Comunes.reemplaza(pieReporte,"</FONT>","");
					pieReporte = Comunes.reemplaza(pieReporte,"<LI>","");
					pieReporte = Comunes.reemplaza(pieReporte,"</LI>","");
					pieReporte = Comunes.reemplaza(pieReporte,"<FONT size=-2>","");
					pieReporte = Comunes.reemplaza(pieReporte,"<BR>","");
					
					StringTokenizer pieReporteST = new StringTokenizer(pieReporte,"\r\n");
					pieReporte = "";
					String pieReporteSub="";
					while(pieReporteST.hasMoreTokens()){
					
						pieReporteSub=pieReporteST.nextToken();
						if(!pieReporteSub.equals(" "))
							pieReporte += pieReporteSub + "\r\n\r\n";
					}
					pieReporte = pieReporte.substring(0,pieReporte.length()-4);
					
					pdfDoc.addText(pieReporte,"formas",ComunesPDF.LEFT);
				}
				rs.close();
				
		/*			out.println("<tr><td colspan=\"2\">" + notas + "</td></tr>");*/
				notas = notas + "0";
				String notemp="";
				int cuentas=0;
		
				StringTokenizer prueba = new StringTokenizer(notas, "\"");
				while(prueba.hasMoreElements()) 
				{
					notemp=notemp+prueba.nextToken();
				}
				
		/*	10/02/2005	strQuery="select distinct idnota,nota from CATALOGONOTAS where idnota in (" + notemp + ")";*/
		
		/*		strQuery="select distinct idnota,nota from CATALOGONOTAS where idnota in (" + notas + ")";*/
		
		/* 10/02/2005
				rs2 = ad.queryDB(strQuery);
				
				cuentas=1;
				pdfDoc.addText("NOTAS.","formas",ComunesPDF.LEFT);
				while(rs2.next())
				{
					pdfDoc.addText(cuentas+ " "+ rs2.getString("Nota"),"formas",ComunesPDF.LEFT);
					cuentas++;
				}
		*/
				pdfDoc.endDocument();
				validaPDF = true;
				rs.close();
				ad.cierraStatement();
				jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
				jsonObj.put("success",new Boolean(true));
			} /*cierre if de carga*/
		}catch(Exception e){
		  System.out.println("----------------------");
		  System.out.println("Error: reporte.jsp");
		  e.printStackTrace();
		  jsonObj.put("mensaje",e.getMessage());
		  jsonObj.put("success",new Boolean(false));
		  System.out.println("----------------------");  
		}finally{
			infoRegresar = jsonObj.toString();
			ad.terminaTransaccion(regreso);
			datosGenerales.release();
			ad.cierraConexionDB();
		}
	
	}else 
///////////////INICIA ARCHIVO CSV///////////////////////////	
	
	if(informacion.equals("archivoCSV")){
			String strGrupoUsuario=strPerfil;
			String usuario=strLogin;
			String strIntermediario="";
			String strTipoIntermediario="";
			String strIdTipoIntermediario="";
			String strIdUsuario="";
			String strQuery = "";
			String queryfecha="";
			boolean regreso = true;
			
			AccesoDB ad = new AccesoDB();
			ResultSet rs = null;    
			ResultSet rs2 = null;    
			ResultSet rsTipo = null;
			ResultSet rsFecha = null;
			ResultSet rsCredit = null;
			ad.conexionDB();
			
			String renglon = "";
			String renglon2 = "";
			String decide = ",0,";
			int i=0;
			int j=0;
			int k=0;
			
			String alfa="";
			boolean flag=true;
			
			String notas="";
			
			/*Archivo*/
			CreaArchivo archivo = new CreaArchivo();
			String nombreArchivo = "";
			StringBuffer contenidoArchivo = new StringBuffer();
			boolean archivoGenerado=true;
			/*Archivo*/
			try
			{
				if (datosGenerales.load()) {
					if (strGrupoUsuario.equalsIgnoreCase("admin if")||strGrupoUsuario.equalsIgnoreCase("admin if corto")||strGrupoUsuario.equalsIgnoreCase("consul if"))	{
						if (datosGenerales.loadDatosUsuario(iNoCliente)){
						  strTipoIntermediario=(datosGenerales.getTipoIntermediario()).trim();
						  strIdTipoIntermediario=(datosGenerales.getIdTipoIntermediario()).trim();
						}
					}
			/*ARS Cambios al 7 de octubre de 2002 FASE 2-2*/
					else {
						if (strGrupoUsuario.equalsIgnoreCase("credito"))
						{
							/*Join que extrae los riesgos y el id de usuario a partir de su login*/
							String risk = "select * from riesgousr " + 
									  " where ic_usuario ='" + usuario + "'";
							rsCredit = ad.queryDB(risk);
							while(rsCredit.next())
							{
								strTipoIntermediario = rsCredit.getString("ic_usuario");
								strIdTipoIntermediario = rsCredit.getString("riesgo");
							}
							rsCredit.close();
							strTipoIntermediario = "Crédito";
						}
					}
			/*ARS*/
					contenidoArchivo.append("\nNACIONAL FINANCIERA S.N.C. \nSubdirección de Planeación de Tesorería\n\n");
					strQuery="select * from Cuadros where idcuadro= " + request.getParameter("id");
					rs = ad.queryDB(strQuery);
					while(rs.next())
					{
						String tituloReporte = Comunes.reemplaza(rs.getString("titreporte").toUpperCase(),"<FONT SIZE=+1>","");
						tituloReporte = Comunes.reemplaza(tituloReporte,"</FONT>","");
			
						contenidoArchivo.append(",,"+tituloReporte+"\n");
			
						String subtituloReporte = Comunes.reemplaza(rs.getString("subtitrep").toUpperCase(),"<FONT SIZE=-1>","");
						subtituloReporte = Comunes.reemplaza(subtituloReporte,"</FONT><BR>","");
						
						contenidoArchivo.append(",,"+subtituloReporte+"\n\n");
				/*fecha*/
						queryfecha = "select to_char(sysdate,'dd/mm/rrrr') from dual";
						rsFecha = ad.queryDB(queryfecha);
						while(rsFecha.next())
						{
							contenidoArchivo.append("Fecha de Consulta     ," + rsFecha.getString(1) +"\n\n\n");
						}
						rsFecha.close();
				/*fecha*/
			
						
						String Requisitos = Comunes.reemplaza(rs.getString("txtcab"),"<FONT size=-1 color=blue>","");
						Requisitos = Comunes.reemplaza(Requisitos,"<U>","");
						Requisitos = Comunes.reemplaza(Requisitos,"</U>","");
						Requisitos = Comunes.reemplaza(Requisitos,"<BR>","");
						Requisitos = Comunes.reemplaza(Requisitos,"<OL>","");
						Requisitos = Comunes.reemplaza(Requisitos,"</OL>","");
						Requisitos = Comunes.reemplaza(Requisitos,"<LI>","");
						Requisitos = Comunes.reemplaza(Requisitos,"</LI>","");
						Requisitos = Comunes.reemplaza(Requisitos,"</FONT>","");
						
						StringTokenizer RequisitosST = new StringTokenizer(Requisitos,"\r\n");
						Requisitos = RequisitosST.nextToken() + "\r\n\r\n";
						int numeroRequisitos=1;
						while(RequisitosST.hasMoreTokens()){
							Requisitos += numeroRequisitos + ".  " + RequisitosST.nextToken()+"\r\n";
							numeroRequisitos++;
						}
						contenidoArchivo.append(Requisitos + "\n , , \n");
						
						Blob blob = rs.getBlob("bi_archivo");
						InputStream inputStream = blob.getBinaryStream();
						BufferedReader bDatos   = new BufferedReader(new InputStreamReader(inputStream,"ISO-8859-1"));
						
						int contador=0;
						String tokenFinal="";
						String var="";
						String var2="";
						String auxi="";
						if (!(strGrupoUsuario.equalsIgnoreCase("TESORERIA")||strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA")||strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("TABLAS")))
						{
							if((renglon = bDatos.readLine()) != null) 
							{
								StringTokenizer datos = new StringTokenizer(renglon, ",");
								while(datos.hasMoreElements()) 
								{
									var = datos.nextToken();
									var2 = strIdTipoIntermediario;
									/*ARS modificaciones para eliminar las comillas*/
										StringTokenizer temporal = new StringTokenizer(var, "\"");
										while(temporal.hasMoreElements())
										{
											if(temporal.nextToken().equals(var2))
												decide += contador + ",";
											contador++;
										}
									/*FIN ARS*/
								}
							}	
						}
			/*ARS modificaciones al 7 de oct de 2002 para usuarios crédito*/
			/*					decide = ",0" + strIdTipoIntermediario;       */
						else
						{
							//FileReader fDatos2 = new FileReader(strDirTrabajo+"tablas/"+rs.getString("archivo"));
							// local FileReader fDatos2 = new FileReader(".\\applications\\nafin\\nafin-web\\nafin-web\\22cotizador\\tablas\\" + rs.getString("archivo"));
							//BufferedReader bDatos2 = new BufferedReader(fDatos2);
							Blob blob2 = rs.getBlob("bi_archivo");
							InputStream inputStream2 = blob2.getBinaryStream();
							BufferedReader bDatos2   = new BufferedReader(new InputStreamReader(inputStream2,"ISO-8859-1"));
						if (strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("admin operc"))
							if (strGrupoUsuario.equalsIgnoreCase("CREDITO"))
							{
								if((renglon2 = bDatos2.readLine()) != null)
								{
									StringTokenizer datos = new StringTokenizer(renglon2 , ",");
									while(datos.hasMoreElements())
									{
										var = datos.nextToken();
										var2 = strIdTipoIntermediario;
			
										StringTokenizer temporal = new StringTokenizer(var, "\"");
										while(temporal.hasMoreElements())
										{
											tokenFinal = temporal.nextToken();
			
											StringTokenizer creditos = new StringTokenizer(var2, ",");
											while(creditos.hasMoreElements())
											{
												auxi = creditos.nextToken();
												if( tokenFinal.equals(auxi) )
													decide += contador + ",";
											}
											contador++;
										}				
									}
								}	
							}
						}
			/*ARS*/
			/*			out.println(" decide: " + decide);*/
						contador=0;
						int paranotas=0; 
			/*paranotas es una variable que va cambiando por renglón en la lectura del archivo .xls, cuando se lee la 4a ó 5a 
			línea (dependiendo del tipo de reporte, se procede a tomar en cuenta un arreglo para las notas y su despliegue*/
						boolean bander=true;
						String cstrg;		
						boolean banderauser=true;
						if (!(strGrupoUsuario.equalsIgnoreCase("admin if cotiz")||strGrupoUsuario.equalsIgnoreCase("admin if")||strGrupoUsuario.equalsIgnoreCase("admin if corto")||strGrupoUsuario.equalsIgnoreCase("consul if")||strGrupoUsuario.equalsIgnoreCase("consul if cotiz")))	{
							contenidoArchivo.append(strTipoIntermediario + "\n\n");
						}			
						int num_renglon=0;
						while((renglon = bDatos.readLine()) != null) 
						{
							num_renglon++;
							StringTokenizer datos2 = new StringTokenizer(renglon, ",");
							while(datos2.hasMoreElements()) 
							{
								var = datos2.nextToken();
								cstrg = ","+contador+",";
								if (!(strGrupoUsuario.equalsIgnoreCase("TESORERIA")||strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA")||strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("TABLAS")))
								{					
									if(decide.indexOf(cstrg)>-1)
									{
										if (paranotas==4)
											notas = notas + var + ",";
										else
										{
										/*ARS modificaciones para eliminar las comillas*/
											StringTokenizer caso = new StringTokenizer(var, "\"");
											while(caso.hasMoreElements())
											{
												var2 = caso.nextToken();
											}
										/*FIN ARS*/
			
											contenidoArchivo.append(var2+ ",");
											
										}
									}
								}
			/*ARS cambios 8 de oct 2002 para reporte de usurios tipo crédito*/
								else 
								{
									if (strGrupoUsuario.equalsIgnoreCase("CREDITO"))
									{							
										if(decide.indexOf(cstrg)>-1)
										{
											if (paranotas==5)
											{
												notas = notas + var + ",";
											}
											else
											{
												StringTokenizer caso = new StringTokenizer(var, "\"");
												while(caso.hasMoreElements())
												{
													var2 = caso.nextToken();
												}
			
												if(paranotas == 0)
												{
													if (banderauser)
													{
			
														contenidoArchivo.append(" "+ ",");
														
														banderauser = false;
													}
													strQuery="select * from TIPOINTERMEDIARIO where idTipoIntermediario= " + var2;
													rsTipo = ad.queryDB(strQuery);
													while(rsTipo.next())
													{
														contenidoArchivo.append(rsTipo.getString("descripcion")+ ",");
													}								
												}
												else{
													contenidoArchivo.append(var2+ ",");
												}
											}
										}
									}
			/*ARS*/
									else
									{
										if (paranotas==5)
										{
											notas = notas + var + ",";
										}
										else
										{
										/*ARS modificaciones para eliminar las comillas*/
											StringTokenizer caso = new StringTokenizer(var, "\"");
											while(caso.hasMoreElements()) 
											{
												var2 = caso.nextToken();
											}
										/*FIN ARS*/
											if(paranotas == 0)
											{
												if (banderauser)
												{
													contenidoArchivo.append(" "+ ",");
													
													banderauser = false;
												}
												strQuery="select * from TIPOINTERMEDIARIO where idTipoIntermediario= " + var2;
												rsTipo = ad.queryDB(strQuery);
												while(rsTipo.next())
												{
													contenidoArchivo.append(rsTipo.getString("descripcion")+ ",");
												}								
											}
											else{
												contenidoArchivo.append(var2+ ",");
											}
										}
									}
								}
								contador++;
							}
							contador=0;
							paranotas++;
							contenidoArchivo.append("\n");
						}	
						contenidoArchivo.append(", \n , ,\n");
						String pieReporte = Comunes.reemplaza(rs.getString("txtpie"),"<FONT size=-1>","");
						pieReporte = Comunes.reemplaza(pieReporte,"<FONT size=-1 >","");
						pieReporte = Comunes.reemplaza(pieReporte,"<FONT size=2 color=purple>","");		
						pieReporte = Comunes.reemplaza(pieReporte,"</FONT>","");
						pieReporte = Comunes.reemplaza(pieReporte,"<LI>","");
						pieReporte = Comunes.reemplaza(pieReporte,"</LI>","");
						pieReporte = Comunes.reemplaza(pieReporte,"<FONT size=-2>","");
						pieReporte = Comunes.reemplaza(pieReporte,"<BR>","");
						
						pieReporte = pieReporte.replace(',',' ');
						
						StringTokenizer pieReporteST = new StringTokenizer(pieReporte,"\r\n");
						pieReporte = "";
						String pieReporteSub="";
						while(pieReporteST.hasMoreTokens()){
						
							pieReporteSub=pieReporteST.nextToken();
							if(!pieReporteSub.equals(" "))
								pieReporte += pieReporteSub + "\r\n\r\n";
						}
						
						contenidoArchivo.append("\n"+pieReporte+"\n");
					}
					rs.close();
					
			/*			out.println("<tr><td colspan=\"2\">" + notas + "</td></tr>");*/
					notas = notas + "0";
					String notemp="";
					int cuentas=0;
			
					StringTokenizer prueba = new StringTokenizer(notas, "\"");
					while(prueba.hasMoreElements()) 
					{
						notemp=notemp+prueba.nextToken();
					}
					
			/*	10/02/2005	strQuery="select distinct idnota,nota from CATALOGONOTAS where idnota in (" + notemp + ")";*/
			
			/*		strQuery="select distinct idnota,nota from CATALOGONOTAS where idnota in (" + notas + ")";*/
			
			/* 10/02/2005
					rs2 = ad.queryDB(strQuery);
					
					cuentas=1;
					pdfDoc.addText("NOTAS.","formas",ComunesPDF.LEFT);
					while(rs2.next())
					{
						pdfDoc.addText(cuentas+ " "+ rs2.getString("Nota"),"formas",ComunesPDF.LEFT);
						cuentas++;
					}
			*/
					rs.close();
					ad.cierraStatement();
					if(!archivo.make(contenidoArchivo.toString(),strDirectorioTemp, ".csv")){
						archivoGenerado=false;
						jsonObj.put("mensaje","Error al generar el archivo");
						jsonObj.put("success",new Boolean(false));
					}
					else{
						nombreArchivo = archivo.nombre;
						jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
						jsonObj.put("success",new Boolean(true));
						}
				} /*cierre if de carga*/
			}catch(Exception e){
			  System.out.println("----------------------");
			  System.out.println("Error: reporte.jsp");
			  e.printStackTrace();
			  System.out.println(e.getMessage().toString());
			  jsonObj.put("mensaje",e.getMessage());
				jsonObj.put("success",new Boolean(false));
			  System.out.println("----------------------");  
			}finally{
			  ad.terminaTransaccion(regreso);
			  infoRegresar = jsonObj.toString();
			  datosGenerales.release();
			  
			  ad.cierraConexionDB();
			}
	
	
	}
	
	%>

<%=infoRegresar%>
	
	
	
	
	
	