<%@ page contentType="application/json;charset=UTF-8"%>
<%@ include file="../22secsession_extjs.jspf" %>
<%@ include file="/appComun.jspf"%>
<%@ page language="java" import="java.io.*,
	java.lang.*,
	java.util.*, 
	java.sql.*, 
	com.netro.cotizador.consAdmin,
	net.sf.json.JSONArray,  
	net.sf.json.JSONObject,
	netropology.utilerias.AccesoDB,
	com.jspsmart.upload.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<jsp:useBean id="mySmartUpload" scope="page" class="com.jspsmart.upload.SmartUpload" /> 

<% 

	String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
	String infoRegresar = "",consulta ="", mensaje="";
	String moneda = (request.getParameter("cveMoneda") !=null)?request.getParameter("cveMoneda"):"";
	String regIdCuadro = (request.getParameter("regIdCuadro")!=null)?request.getParameter("regIdCuadro"):"";
	String regTitMenu = (request.getParameter("regTitMenu")!=null)?request.getParameter("regTitMenu"):"";
	String monedaModifica = (request.getParameter("claveMoneda") !=null)?request.getParameter("claveMoneda"):"";
	String tituloMenu = (request.getParameter("tituloMenu")!=null)?request.getParameter("tituloMenu"):"";
	String tituloReporte = (request.getParameter("tituloReporte")!=null)?request.getParameter("tituloReporte"):"";
	String subReporte = (request.getParameter("subReporte")!=null)?request.getParameter("subReporte"):"";
	String cabecera = (request.getParameter("cabecera")!=null)?request.getParameter("cabecera"):"";
	String pieCuadro = (request.getParameter("pieCuadro")!=null)?request.getParameter("pieCuadro"):"";
	String idCuadro = (request.getParameter("idCuadro")!=null)?request.getParameter("idCuadro"):"";
	String archivo = (request.getParameter("archivo")!=null)?request.getParameter("archivo"):"";

	JSONObject 	resultado	= new JSONObject();
	consAdmin paginado = new consAdmin();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginado);
	JSONObject jsonObj = new JSONObject();
	
	if (informacion.equals("ConsultaGrid")) {
		paginado.setCveMoneda(moneda);
		infoRegresar ="";
		try {    
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			resultado = JSONObject.fromObject(consulta);
			if(moneda.equals("1")){
				resultado.put("TIPO_MONEDA","Moneda Nacional");
			}else{
				resultado.put("TIPO_MONEDA","Moneda Extranjera");
			}
			infoRegresar = resultado.toString();
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos", e);
		}
	}else if(informacion.equals("modificaCuadro")){
		String IDCUADRO= "", IDMONEDA ="", TITMENU ="",TITREPORTE= "", SUBTITREP ="", TXTCAB ="", TXTPIE ="", ARCHIVO ="";
		List datos = paginado.datosCuadro(regIdCuadro);
	
		IDCUADRO = (String)datos.get(0);
		IDMONEDA = (String)datos.get(1);
		TITMENU = (String)datos.get(2);
		TITREPORTE = (String)datos.get(3);
		SUBTITREP = (String)datos.get(4);
		TXTCAB = (String)datos.get(5);
		TXTPIE = (String)datos.get(6);
		ARCHIVO = (String)datos.get(7);
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("IDCUADRO", IDCUADRO);
		jsonObj.put("IDMONEDA",IDMONEDA);
		jsonObj.put("TITMENU", TITMENU);
		jsonObj.put("TITREPORTE", TITREPORTE);
		jsonObj.put("SUBTITREP", SUBTITREP);
		jsonObj.put("TXTCAB", TXTCAB);
		jsonObj.put("TXTPIE", TXTPIE);
		jsonObj.put("ARCHIVO", ARCHIVO);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("enviarDatosNuevos")){
		String query = paginado.modificaCuadro(idCuadro,monedaModifica,tituloMenu,tituloReporte,subReporte, cabecera,pieCuadro);
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("QUERY", query);
		jsonObj.put("idCuadro", idCuadro);
		jsonObj.put("tituloMenu", tituloMenu);
		jsonObj.put("ARCHIVO", archivo);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("subirArchivo")){
		AccesoDB ad = new AccesoDB();
		ResultSet rs=null;
		String resultadoArch = "";
		JSONObject jsonObj1 = new JSONObject();
		try{
			String strInsert="";
			int numFiles=0;
			String 	name= "";
			String value= ""; 
			String idCuadroArch = "";
			mySmartUpload.initialize(pageContext);
			mySmartUpload.setAllowedFilesList("csv");  
			mySmartUpload.setTotalMaxFileSize(100000);
			mySmartUpload.upload();  
			numFiles = mySmartUpload.save(strpath);
			com.jspsmart.upload.File myFile = mySmartUpload.getFiles().getFile(0);
	 
			Enumeration en = mySmartUpload.getRequest().getParameterNames();
			Hashtable hiddens=new Hashtable();
			while (en.hasMoreElements()) {
				name = en.nextElement().toString().trim();
				value = mySmartUpload.getRequest().getParameter(name);
				hiddens.put(name,value);
			}
			idCuadroArch= hiddens.get("idCuadroArch").toString();
			if (myFile.getFileName().trim().equals("")){
				resultadoArch = "No se especific� ning�n archivo .";
			}else{
				//ad.conectarDB();
				ad.conexionDB();
				//String pedazo = "UPDATE CUADROS set ARCHIVO = '" + myFile.getFileName() + "' where idcuadro = "+idCuadroArch;
				String pedazo = "UPDATE CUADROS set ARCHIVO = ?, bi_archivo = ? where idcuadro =  ? ";
				//ad.ejecutaSQL(pedazo);
				java.io.File farchivo = new java.io.File(strpath+myFile.getFileName());
				FileInputStream fis = new FileInputStream(farchivo);
				PreparedStatement ps = null;
				ps = ad.queryPrecompilado(pedazo);
				ps.setString(1, myFile.getFileName());
				ps.setBinaryStream(2, fis, (int)farchivo.length());
				ps.setLong(3, Long.parseLong(idCuadroArch));
				ps.executeUpdate();
				ps.close();
				
				strInsert =	" INSERT INTO BIT_ALTA_CURVA( " +
							   " IDCURVA,DC_ALTA,CG_NOMBRE_USUARIO,CG_TIPO)" + 
							   " VALUES("+idCuadroArch+", Sysdate , '"+ strNombreUsuario+"','T')";							
				System.out.println("Query INSERTA HISTORICO:::"+strInsert);	
				ad.ejecutaSQL(strInsert);
				ad.cerrarDB(true);
				resultadoArch = "El archivo "+myFile.getFileName()+" se subi� exitosamente.";
			}
			
			jsonObj1.put("success", new Boolean(true));
			jsonObj1.put("resultado", resultadoArch);
			
		}catch(Exception e){
			System.out.println("----------------------");
			System.out.println("Error: subeCuadroExt01.data.jsp");
			e.printStackTrace();
			System.out.println("----------------------"); 
			e.printStackTrace();
		}
		infoRegresar = jsonObj1.toString();
%>

<%
	}
%>
<%=infoRegresar%>