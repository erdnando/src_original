Ext.onReady(function(){
var arreglo;
var id;
var numCols;
//----------------------------Handlers--------------------------


function procesarArchivoPdfSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('btnPDF').enable();
			Ext.getCmp('btnPDF').setIconClass('');
			Ext.getCmp('btnCSV').enable();
			Ext.getCmp('btnCSV').setIconClass('');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

var procesarSuccessFailurePDF = function(opts, success, response){
		var btnGenerarArchivo = Ext.getCmp('btnPDF');
		
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarArchivo = Ext.getCmp('descarga');
			btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			verDescarga.setVisible(true);
			btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
var procesarSuccessFailureCSV = function(opts, success, response){
		var btnGenerarArchivo = Ext.getCmp('btnCSV');
		
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarArchivo = Ext.getCmp('descarga');
			btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			verDescarga.setVisible(true);
			btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
var procesarConsulta = function(opts, success, response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = Ext.util.JSON.decode(response.responseText);
		var rutas = resp.rutas;
		var texto;
		if(Ext.getCmp('cbMoneda').getValue()==1){
			texto='<TR><TD class=\"titulos\">Moneda Nacional</TD></TR>';
		}else{
			texto='<TR><TD class=\"titulos\">Moneda Extranjera</TD></TR>';
		}
		if(rutas!=''){
			Ext.getCmp('Mensaje').setValue('<TABLE ALIGN="CENTER" WIDTH="60%">'+texto+'<TR><TD><font size=\"1\">Elija el tipo de reporte que desea obtener:</font></TD></TR>'+
			'<TR><TD>&nbsp;</TD></TR>'+rutas+'</TABLE>');
		}else{
			Ext.getCmp('Mensaje').setValue('<TABLE ALIGN="CENTER" WIDTH="80%"><TR><TD align="center">No se encontraron registros</TD></TR></TABLE>');
		}
	}
		
}
	
var procesarId = function(opts, success, response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		fp.show();
		Ext.getCmp('Mensaje').setValue('<table width="600" border="0" cellspacing="5" cellpadding="5">'+
		'<tr><td><table width="564" border="0" cellspacing="0" cellpadding="0">'+Ext.util.JSON.decode(response.responseText).encabezado);				
		Ext.getCmp('Mensaje2').setValue(Ext.util.JSON.decode(response.responseText).pie+'</td></tr></table>');
		Ext.getCmp('btnCSV').setVisible(true);
		Ext.getCmp('btnPDF').setVisible(true);
		Ext.getCmp('salir').setVisible(true);
		numCols=Ext.util.JSON.decode(response.responseText).numCols;
	}else{
		Ext.MessageBox.alert('Error',Ext.util.JSON.decode(response.responseText).mensaje,function(){
			window.location = 'despliegaExt.jsp';
		});
	}
}
	
//----------------------------Stores----------------------
var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catologoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : 'despliegaExt.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
//-------------------Componentes----------------------------------------------
var elementosForma= [
	{
		xtype: 'combo',
		fieldLabel: 'Moneda',
		anchor:'80%',
		displayField: 'descripcion',
		valueField: 'clave',
		triggerAction: 'all',
		typeAhead: true,
		allowBlank: false,
		minChars: 1,
		name:'cbMoneda',
		id: 'cbMoneda',
		mode: 'local',
		forceSelection : true,
		allowBlank: false,
		hiddenName: 'HcbMoneda',
		hidden: false,
		emptyText: 'Seleccionar Moneda',
		store: catalogoMoneda,
		tpl: NE.util.templateMensajeCargaCombo
	}
]

var panelPie = new Ext.Panel({  
    hidden: false, 
    bodyStyle:'padding: 10px',
	 
	 items: [{
		xtype: 'displayfield',
		name: 'Mensaje2',
		id: 'Mensaje2',
		anchor:'85%',
		value:''
	}]});
var panelJSP = new Ext.Panel({  
    hidden: false, 
    bodyStyle:'padding: 10px',
	 
	 items: [{
		xtype: 'displayfield',
		name: 'Mensaje',
		id: 'Mensaje',
		anchor:'85%',
		value:''
	}],
	buttons: [
			{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnPDF',	
				hidden:true,
				handler: function(boton, evento) {	
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
								url: 'despliegaExtPDF.data.jsp',
								params: Ext.apply({
									informacion: 'archivoPDF',id:id,numCols:numCols}),
								callback: procesarArchivoPdfSuccess
							});
				}
			},{
				xtype: 'button',
				text: 'Generar Archivo',
				id: 'btnCSV',
				hidden:true,
				handler: function(boton, evento) {	
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
								url: 'despliegaExtPDF.data.jsp',
								params: Ext.apply({
									informacion: 'archivoCSV',id:id,numCols:numCols}),
								callback: procesarArchivoPdfSuccess
							});
				}
			},{
				xtype: 'button',
				text: 'Salir',
				id:'salir',
				hidden:true,
				handler: function(boton, evento) {		
					window.location = 'despliegaExt.jsp';
				}
			}
		]
	 });

var panelDetalles = new Ext.Panel
  ({
	
		hidden: false,
		height: 'auto',
		width: 800,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 160,
		defaultType: 'textfield',
		frame: true,
		id: 'forma5',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		
		monitorValid: true,
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  align: 'center',
			  text: 'Descargar Archivo',
			  id:'descarga',
			  hidden: false,
			  formBind: true
			 }
		  ]
  });
	 

var panelForma= new Ext.form.FormPanel({  
    hidden: false,
	 frame: true,
	 width: 400,
	 style: 'margin:0 auto;',
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px',
	 monitorValid: true,
	 //html: 'hoeleaijr',
	 items: [elementosForma],
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  hideLabel:true,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						Ext.Ajax.request({
								url: 'despliegaExt.data.jsp',
								params: Ext.apply({
									informacion: 'Consulta',moneda:Ext.getCmp('cbMoneda').getValue()}),
								callback: procesarConsulta
							});
							fp.show();
					}
			 }
		]
	 });
	 
	 
var verDescarga = new Ext.Window({
			title: 'Descarga Archivo',
			width: 200,
			height: 100,
			maximizable: false,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			maximized: false,
			constrain: true,
			items: [panelDetalles]
});
  var fp = new Ext.form.FormPanel
  ({
		hidden: true,
		height: 'auto',
		width: 700,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items:[panelJSP,panelPie]
	});
	

	
//-----------------------Principal---------------------------------------
	
	
	var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 800,
	  items: [panelForma,NE.util.getEspaciador(30),fp,verDescarga]
  });
	
		id = Ext.get('id').getValue();
		if(id=='null'){
		catalogoMoneda.load();
		}else{
			panelForma.setVisible(false);
			Ext.Ajax.request({
			url: 'despliegaExt.data.jsp',
			params: Ext.apply({id:id}),
			callback: procesarId
		});
		}
  
  });