<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import=
	" java.util.*, 
	  net.sf.json.JSONArray,
	  net.sf.json.JSONObject,
	  com.netro.catalogos.*,
	  com.netro.exception.*,
	  com.netro.model.catalogos.*,
	  com.netro.cadenas.Consultas,	
	  netropology.utilerias.usuarios.*,	
	  com.netro.cadenas.PaginadorGenerico,
	  com.netro.cadenas.MantenimientoCatalogo,	    
	  org.apache.commons.logging.Log,
          netropology.utilerias.AccesoDB"
			
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/22cotizador/22secsession_extjs.jspf"%>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String claveUsuario	= (request.getParameter("clvUser")	!=null)	?	request.getParameter("clvUser")	:	"";
String infoRegresar	= "";
String consulta 		= "";

if (informacion.equals("consultarUsuario") )	{
	boolean usuario=false;
	UtilUsr ut = new UtilUsr();
	Usuario usr = ut.getUsuario(claveUsuario);
	String nombreUsuario="";
	try {
		if(!usr.getPerfil().equals("CREDITO")){
			throw new Exception("El usuario no existe");
		}
		nombreUsuario = usr.getNombre()+ " "+ usr.getApellidoPaterno()+ " " +usr.getApellidoMaterno();
		usuario=true;
	} catch (Exception e){
                e.printStackTrace();
		log.debug(e);
	}
 
	JSONObject 		jsonObj 	= new JSONObject();
	jsonObj.put("success"	, new Boolean(true)); 
	jsonObj.put("claveUsr", claveUsuario);
	jsonObj.put("usr", new Boolean(usuario));
	jsonObj.put("nombreUsuario", nombreUsuario);
 
  infoRegresar = jsonObj.toString();

} else if (informacion.equals("consultar")){

	PaginadorGenerico pG= new PaginadorGenerico();
	Consultas consultas = new Consultas();
	List 		 registros = new ArrayList();
	HashMap	 datos	  = new HashMap();
	String	 str		  = new String("");
	boolean 	 bandera	  = false; 
	
	UtilUsr ut = new UtilUsr();
	Usuario usr = ut.getUsuario(claveUsuario);
	try { 
		if(!usr.getPerfil().equals("CREDITO")){
			throw new Exception("El usuario no existe");
		}
		String compara="",base=""; 
		pG.setCampos("riesgo");
		pG.setTabla("riesgousr");
		pG.setCondicion("ic_usuario = '"+claveUsuario+"'");
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pG);
		Registros reg = queryHelper.doSearch(); 
		if(reg.next())	{	
			base = reg.getString("riesgo");
			bandera=true;
		} else {
			bandera=false;
		} 
		pG.setCampos("*");
		pG.setTabla("TIPOINTERMEDIARIO");
		pG.setCondicion("cg_tipo = 'I'");
		queryHelper = new CQueryHelperRegExtJS(pG);
		reg = queryHelper.doSearch(); 
		while(reg.next())	{							
			compara = "," + reg.getString("idtipointermediario") + ",";
			if ( (base != "") && (base.indexOf(compara) > -1) ) {
				datos.put("CLAVE_INTER"		,reg.getString("idtipointermediario"));
				datos.put("DESCRIPCION"		,reg.getString("descripcion"));
				datos.put("CHECK"				,new Boolean(true));
			} else {
				datos.put("CLAVE_INTER"		,reg.getString("idtipointermediario"));
				datos.put("DESCRIPCION"		,reg.getString("descripcion"));
				datos.put("CHECK"				,new Boolean(false));
			}
			registros.add(datos);
			datos		 = new HashMap();
		} 
	} catch (Exception e){
		log.debug(e);
	}  
	JSONObject 		jsonObj 	= new JSONObject();
	jsonObj.put("success"	, new Boolean(true)); 
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("enviar")){

	boolean 	 bandera	  = false;
        PaginadorGenerico pG= new PaginadorGenerico();
        String claveInter	= (request.getParameter("claves")	!=null)	?	request.getParameter("claves")	:	"";
	MantenimientoCatalogo mC = new MantenimientoCatalogo ();
	boolean salida=true;

		pG.setCampos("riesgo");
		pG.setTabla("riesgousr");
		pG.setCondicion("ic_usuario = '"+claveUsuario+"'");
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pG);
		Registros reg = queryHelper.doSearch(); 
		if(reg.next())	{	
			bandera=true;
		} else {
			bandera=false;
		} 


	if (bandera){
            AccesoDB con = new AccesoDB();
            String insQry="update RIESGOUSR set riesgo=? where IC_USUARIO=?";
            List varBind = new ArrayList();
            try{
                con.conexionDB();
                varBind.add(claveInter);
                varBind.add(claveUsuario);
                con.ejecutaUpdateDB(insQry,varBind);
            }catch(Exception e) {
                e.printStackTrace();
            } finally {
                if(con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
            }
	} else	{
            AccesoDB con = new AccesoDB();
            String insQry="insert into RIESGOUSR values (?,?)";
            List varBind = new ArrayList();
            try{
                con.conexionDB();
                varBind.add(claveUsuario);
                varBind.add(claveInter);
                con.ejecutaUpdateDB(insQry,varBind);
            }catch(Exception e) {
                e.printStackTrace();
            } finally {
                if(con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
            }
	} 
	JSONObject 	jsonObj 	=  new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 
	jsonObj.put("exito"	,  new Boolean(salida)); 
	infoRegresar = jsonObj.toString();
}

%>
<%=   infoRegresar %>
