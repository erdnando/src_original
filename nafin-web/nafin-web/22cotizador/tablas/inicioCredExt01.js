
Ext.onReady(function(){

//-----------------------------procesarConsultaData-----------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
   var jsonData = store.reader.jsonData;
	
   var fp = Ext.getCmp('forma');
		 fp.el.unmask();							
	var grid1 = Ext.getCmp('griduno');	
	var el = grid1.getGridEl();
 
		if (arrRegistros != null) {
			
			if (!grid1.isVisible()) {
				grid1.show();
				}				
			if(store.getTotalCount() > 0) {
				fp.hide();
				Ext.getCmp('btnEnviar').setDisabled(false);
				Ext.getCmp('btnOtroUsuario').setDisabled(false);
				el.unmask();
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('claveUsuario').reset();
				Ext.getCmp('btnEnviar').setDisabled(true);
				Ext.getCmp('btnOtroUsuario').setDisabled(true);				
			}
		}
	}
//--------------------------Fin procesarConsultaData----------------------------

//------------------------------consultaData ------------------------------------
 var consultaData = new Ext.data.JsonStore({
   autoDestroy : true,
	root 			: 'registros',
	url 			: 'inicioCredExt01.data.jsp',
	baseParams	: {
		              informacion:'consultar'
                  },
    fields      : [
						{	name: 'CLAVE_USR'},
                  {	name: 'CLAVE_INTER'},
                  {	name: 'DESCRIPCION'},
						{	name: 'CHECK'}
                ],		
	totalProperty 	: 'total',
   messageProperty: 'msg',
	autoLoad			: false,
   listeners		: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}	
	});
////-------------------------------FIN consultaData--------------------------------

//-------------------------------Elementos Forma--------------------------------
var  elementosForma =  [
	{
	xtype			: 'textfield',
	id          : 'claveUsuario',
	name			: 'clvUser',
	hiddenName	: 'clvUserHid',
	fieldLabel  : 'CLAVE DEL USUARIO',
	maxLength	: 8,
	anchor		: '80%',
	margins		: '0 20 0 0'	
	} 	
];
//-----------------------------Fin Elementos Forma------------------------------

//----------------------------- griduno -----------------------------
var griduno = new Ext.grid.GridPanel({
	id: 'griduno',
	store: consultaData,
	margins: '20 0 0 0',		
	style: 'margin:0 auto;',
	title: 'ADMINISTRACI�N - ASIGNACI�N DE PERMISOS',
	hidden: true,
	height: 500,
	width: 400,
	displayInfo: true,		
	emptyMsg: "No hay registros.",		
	loadMask: true,
	stripeRows: true,
   align: 'center',
	frame: true,
		columns: [
		{		xtype: 'checkcolumn',
				header: 'RIESGOS',
				tooltip: 'RIESGOS',
				dataIndex: 'CHECK',
				sortable: false,
				width: 100,			
				resizable: false,									
				align: 'center'
			},	{
				header: '',
				tooltip: '',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				resizable: true,
				align: 'center',
				width: 270
			}
		],
   bbar: {
		//xtype: 'paging',			
		buttonAlign: 'left',
		id: 'barraPaginacion',
		displayInfo: false,
		store: consultaData,
		displayMsg: '{0} - {1} de {2}',
		emptyMsg: "No hay registros.",
		pageSize: 15,
		items: [
				'->','-',
			{
				xtype : 'button',
				id    : 'btnEnviar',
				text  : 'Enviar',					
				tooltip:	'Enviar',
				iconCls: 'icoContinuar',					
				handler: function(boton, evento) {
					var clavesInter = ",";
					var envia=0;
					consultaData.each(function(record){
						 if ( record.data['CHECK'] ){ 
							  clavesInter = clavesInter +record.data['CLAVE_INTER']+",";
							  envia++;
							  }
						 });
					if(envia > 0){
						Ext.Ajax.request({
						url: 'inicioCredExt01.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						  informacion: 'enviar',
						  claves:clavesInter
						}),
						success : function(response) {
							var info = Ext.util.JSON.decode(response.responseText);
							var exito = info.exito;
							
							if(exito){
								Ext.Msg.show({
								title: 'Enviar.',
								msg: 'Datos Insertados.',
								modal: true,
								icon: Ext.Msg.INFO,
								buttons: Ext.Msg.OK,
								fn: function (){
									Ext.getCmp('claveUsuario').reset();
									fp.show();
									griduno.hide();
								} // FIN FN
								});
							}else {
								Ext.Msg.show({
								title: 'Enviar.',
								msg: 'No se insertaron los datos.',
								modal: true,
								icon: Ext.Msg.ERROR,
								buttons: Ext.Msg.OK,
								fn: function (){
									Ext.getCmp('claveUsuario').reset();
									fp.show();
									griduno.hide();
								} // FIN FN
								});
							}
						}
					 });
					} else {
						Ext.Msg.show({
						title: 'Riesgo.',
						msg: 'Seleccione al menos un riesgo.',
						modal: true,
						icon: Ext.Msg.INFO,
						buttons: Ext.Msg.OK
						});
					}
				}//FIN HANDLER
			}, {
				xtype : 'button',
				id    : 'btnOtroUsuario',
				text  : 'Elegir otro usuario',					
				tooltip:	'Elegir otro usuario',
				iconCls: 'icoRegresar',					
				handler: function(boton, evento) {
					Ext.getCmp('claveUsuario').reset();
					fp.show();
					griduno.hide();
					} // FIN HANDLER
				}	// FIN XTYPE				
			]// FIN ITEMS
		}// FIN BBAR
	});
//----------------------------- FIN griduno ----------------------------- 

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id					:'forma',
		width				:400,
		heigth			:'auto',
		title				:'ADMINISTRACI�N - ASIGNACI�N DE PERMISOS USUARIOS CR�DITO',
		layout			:'form',
		frame				:true,
		collapsible		:true,
		titleCollapse	:false,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 6px',
		labelWidth		:150,
		monitorValid	:false,	
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosForma
							],
		buttons			:[{
							text				:'Enviar',
							id					:'btnConsultar',
							iconCls			:'icoContinuar',
							formBind			:false,
							handler			:function(boton, evento){
							if (!Ext.getCmp('claveUsuario').getValue()==''){
								if ( Ext.getCmp('claveUsuario').isValid()) {
									Ext.Ajax.request({
									url: 'inicioCredExt01.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									  informacion: 'consultarUsuario'
									}),
									success : function(response) {
										var info = Ext.util.JSON.decode(response.responseText);
										var usr = info.usr;
										var claveUsr = info.claveUsr;
										var nombreUsuario = info.nombreUsuario;
										if(usr){
											fp.el.mask('Cargando...', 'x-mask-loading');
											griduno.setTitle('ADMINISTRACI�N - ASIGNACI�N DE PERMISOS<br>USUARIO CR�DITO: '+nombreUsuario);
											consultaData.load({
											params		:Ext.apply(fp.getForm().getValues(),{
											informacion	:'consultar'
														})
												});
										}else {
											Ext.Msg.show({
											title: 'Clave Usuario.',
											msg: 'LA CLAVE CAPTURADA NO CORRESPONDE A UN USUARIO DE CR�DITO.',
											modal: true,
											icon: Ext.Msg.ERROR,
											buttons: Ext.Msg.OK,
											fn: function (){
											Ext.getCmp('claveUsuario').focus();
												}
											});
										}
									}
								 });
							 } else {
								Ext.getCmp('claveUsuario').focus();
							 }
							}else {
								Ext.Msg.show({
								title: 'Clave Usuario.',
								msg: 'Debe elegir un usuario de cr�dito <br>para modificar o actualizar sus permisos.',
								modal: true,
								icon: Ext.Msg.INFO,
								buttons: Ext.Msg.OK,
								fn: function (){
								Ext.getCmp('claveUsuario').focus();
									}
								});
							}	
							}//fin handler 
							}]
	});
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		:949,
		height	:'auto',
		style		:'margin:0 auto;',
		items		:[
					NE.util.getEspaciador(20),
					fp,
					NE.util.getEspaciador(20),
					griduno,
					NE.util.getEspaciador(20)
					]
	});
//-----------------------------Fin Contenedor Principal-------------------------

});//-----------------------------------------------Fin Ext.onReady(function(){}