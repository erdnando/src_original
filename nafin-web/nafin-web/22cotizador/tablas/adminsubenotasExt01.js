Ext.onReady(function() {

//------------------------------------------------------------------- procesarConsultaData --------------------------------------------------------------------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  var jsonData = store.reader.jsonData;
  var gridConsulta = Ext.getCmp('gridConsulta');	
  var el = gridConsulta.getGridEl();
  if (arrRegistros != null) {	
		gridConsulta.show();
		if(store.getTotalCount() > 0) {
			el.unmask();	
		} else {	
			el.mask('No se encontr� ning�n registro', 'x-mask');				
		}
  }
};
//----------------------------------------------------------------------procesarConsultaData-----------------------------------------------------------------------------------

//-------------------------------------------------------------------------Consulta Data---------------------------------------------------------------------------------------
var consultaData   = new Ext.data.JsonStore({ 
  root : 'registros',
  url : 'adminsubenotasExt01.data.jsp',
  baseParams: {
    informacion: 'Consultar'
  },
  autoDestroy: true,
  fields: [	
    {	name: 'IDNOTA'},
    {	name: 'NOTA'}
    ],		
  totalProperty : 'total',
  messageProperty: 'msg',
  autoLoad: true,
  listeners: {
    load: procesarConsultaData,
    exception: {
      fn: function(proxy, type, action, optionsRequest, response, args) {
        NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
        //LLama procesar consulta, para que desbloquee los componentes.
        procesarConsultaData(null, null, null);					
      }
    }
  }		
});
//------------------------------------------------------------------------Fin ConsultaData------------------------------------------------------------------------------------

//------------------------------------------------------------------------- gridConsulta -------------------------------------------------------------------------------------
var gridConsulta = new Ext.grid.EditorGridPanel({	
  store		: consultaData,
  id			: 'gridConsulta',
  margins	: '20 0 0 0',		
  style		: 'margin:0 auto;',
  title		: '<center>Modificar Notas.</center>',
  clicksToEdit: 1,
  autoScroll: true,
  frame		:  true,
  hidden		: false,
  enableColumnMove: true,
  enableColumnHide: false,
  height		: 400,
  width		: 800,  
  listeners: {
	show:function(grid){
		gridConsulta.getView().refresh(true);
	},
	cellclick: function(grid, rowIndex, columnIndex, e) {
	},
	beforeedit: function(e){
	}
  },
  tbar	: {
	xtype	: 'container',
	items	: [
		{
		xtype: 'toolbar',
		items:[
				{
				xtype	: 'label',
				id		: 'DetalleNuevaTasaBase.labelConfigurar',
				html	: 'ADMINISTRACI�N CATALOGO DE NOTAS.',
				style	: 'font-weight:bold;padding-bottom:10pt;text-align:center'
				}
				]
		}]
	},		
   columns: [	
    { 
		header	:'IDNOTA',
		tooltip	: 'N�mero de Nota',	
      dataIndex: 'IDNOTA',
      width		: 55,	
      sortable	: true,		
      resizable: true,				
      align		: 'center'			
    },{ 
		header	:'TEXTO DE LA NOTA',
		tooltip	: 'Texto de la Nota',	
      dataIndex: 'NOTA',
      width		: 620,	
      sortable	: true,		
      resizable: true,				
      align		: 'left',
		editor	:{
			xtype			: 'textarea',							
			id				: 'idNota',
			maxLength	: 3500,
			name			: 'nota'
		}
    },{
		xtype		: 'actioncolumn',
		header	: 'Enviar - Eliminar',
		tooltip	: 'Enviar - Eliminar',	
		width		: 90,							
		align		: 'center',
		items		: [
						{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Enviar';	
							return 'icoContinuar';
							},
							handler: function(grid, rowIndex, colIndex){
								var rec = grid.getStore().getAt(rowIndex);
								if(rec.get('NOTA')!=""){
									var grid = grid.getGridEl();
									grid.mask('Enviando...', 'x-mask-loading');
									Ext.Ajax.request({
									url: 'adminsubenotasExt01.data.jsp',
									params: {
									 informacion: 'Enviar',
									 idNota	:rec.get('IDNOTA'),
									 nota		:rec.get('NOTA')
									},
									success : function(response) {
										var info = Ext.util.JSON.decode(response.responseText);
										var grid = Ext.getCmp('gridConsulta').getGridEl();
										grid.unmask();
									},
									failure: NE.util.mostrarConnError,
									callback: function(){
										consultaData.load();
									}
									});
								} else{
									var columnModelGrid = grid.getColumnModel();
									grid.startEditing(rowIndex, columnModelGrid.findColumnIndex('NOTA'));
								}
								
							}
						},{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[1].tooltip = 'Eliminar';	
							return 'icoCancelar';
							},
							handler: function(grid, rowIndex, colIndex){
								var rec = grid.getStore().getAt(rowIndex);
								var grid = grid.getGridEl();
								Ext.Msg.show({
								title	: 'Eliminar.',
								msg	: 'Se va a eliminar la Nota '+rec.get('IDNOTA')+'.�Est� usted seguro de eliminarla?',
								modal	: true,
								icon	: Ext.Msg.QUESTION,
								buttons: Ext.Msg.OKCANCEL,
								fn: function (btn, text){
									if (btn == 'ok'){
										grid.mask('Eliminando...', 'x-mask-loading');
										Ext.Ajax.request({
											url: 'adminsubenotasExt01.data.jsp',
											params: {
											 informacion: 'Eliminar',
											 idNota		:rec.get('IDNOTA')
											},
											success : function(response) {
												var info = Ext.util.JSON.decode(response.responseText);
												var grid = Ext.getCmp('gridConsulta').getGridEl();
												grid.unmask();
											},
											failure: NE.util.mostrarConnError,
											callback: function(){
												consultaData.load();
											}
											});
									}
									else {}
									}
								});								
							}
						}

			]
		}
  ]
});//----------------------------------------------------------------Fin GridConsulta--------------------------------------------------------------------------------------------

//-----------------------------------------------------------------Contenedor Principal------------------------------------------------------------------------------------------
var pnl = new Ext.Container({
	id			: 'contenedorPrincipal',
	width		: 949,		
	style		: 'margin:0 auto;',
	applyTo	: 'areaContenido',
	items: [
		NE.util.getEspaciador(20),	
		gridConsulta
	]
});
//----------------------------------------------------------------Fin Contenedor Principal----------------------------------------------------------------------------------------

});//------------------------------------------------------------Fin Ext.onReady(function(){}