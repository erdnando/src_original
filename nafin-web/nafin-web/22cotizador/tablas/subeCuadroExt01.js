Ext.onReady(function(){
	
//--------------------------------HANDLERS-----------------------------------
//---------------------------------------------------------------------------	
	var procesaCargaArchivo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('btnGuardar').enable();
			var resultado = Ext.decode(response.responseText).resultado;
			Ext.getCmp('formArchivo').hide();
			Ext.getCmp('formArchivo').getForm().reset();
			Ext.getCmp('forma').hide();
			Ext.getCmp('forma').getForm().reset();
			Ext.getCmp('txtResultado').setValue(resultado);
			Ext.getCmp('fpResArchivo').show();
		}else{
			var element = Ext.getCmp("formArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
         // Ocultar mascara del panel de resultados de la validacion
         element = Ext.getCmp("formArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			Ext.Msg.alert("Error",	"Error al cargar los archivos. Favor de probar m�s tarde");
		}

	}
	var procesarSuccessFailureInsertar = function(opts, success, response) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		Ext.getCmp('btnEnviar').enable();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var mensaje = Ext.util.JSON.decode(response.responseText).mensaje;
			var maximoText = Ext.util.JSON.decode(response.responseText).maximoText;
			var pedazo = Ext.util.JSON.decode(response.responseText).pedazo;
			var salida = Ext.util.JSON.decode(response.responseText).salida;
			Ext.getCmp('forma').hide();
			Ext.getCmp('forma').getForm().reset();
			Ext.getCmp('formaResultado').show();
			Ext.getCmp('txtSalida').setValue(salida);
			Ext.getCmp('txtMensaje').setValue(mensaje);
			Ext.getCmp('txtQuery').setValue(pedazo);
			Ext.getCmp('txtMaxima').setValue(maximoText);
			setTimeout(function cargarArchivo(){Ext.getCmp('formaResultado').getForm().reset();Ext.getCmp('formaResultado').hide();Ext.getCmp('formArchivo').show(); }, 3000);
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarGuardarArchivo = function(opts, success, response) {
		Ext.getCmp('btnGuardar').enable();
		var fpArchivo = Ext.getCmp('formArchivo');
		fpArchivo.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resultado = Ext.util.JSON.decode(response.responseText).resultado;
			Ext.getCmp('formArchivo').hide();
			Ext.getCmp('forma').hide();
			Ext.getCmp('txtResultado').setValue(resultado);
			Ext.getCmp('formaResultado').show();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------------STORES------------------------------------
//---------------------------------------------------------------------------	
	var catalogoMoneda = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['1','Pesos'],
			['54','D�lares']
		 ]
	});
//------------------------------COMPONENTES----------------------------------
//---------------------------------------------------------------------------
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'claveMoneda',
			id: 'cmbClaveMoneda',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Moneda',
			emptyText: 'Seleccionar...',	
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveMoneda',
			forceSelection : true,
			allowBlank: true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoMoneda		
		},
		{
			xtype: 'textarea',
			name:	'tituloMenu',
			id: 'txtaTituloMenu',
			hiddenName:	'tituloMenu',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T�tulo en Men�',
			maxLength: 3000,
			width: 200,
			allowBlank:	true,
			margins: '0 20 0 0'	
		},
		{
			xtype: 'textarea',
			name:	'tituloReporte',
			width: 200,
			id: 'txtaTituloReporte',
			hiddenName:	'tituloReporte',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T�tulo en Reporte',
			maxLength: 3000,
			allowBlank:	true,
			margins: '0 20 0 0'	
		},
		{
			xtype: 'textarea',
			name:	'subReporte',
			id: 'txtaSubReporte',
			width: 200,
			hiddenName:	'subReporte',
			fieldLabel: '&nbsp;&nbsp;Subt�tulo en Reporte',
			maxLength: 3000,
			allowBlank:	true,
			margins: '0 20 0 0'	
		},
		{
			xtype: 'textarea',
			name:	'cabecera',
			id: 'txtaCabecera',
			hiddenName:	'cabecera',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Texto Cabecera',
			maxLength: 3000,
			width: 200,
			allowBlank:	true,
			margins: '0 20 0 0'	
		},{
			xtype: 'textarea',
			name:	'pieCuadro',
			id: 'txtaPieCuadro',
			hiddenName:	'pieCuadro',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Texto Pie Cuadro',
			maxLength: 3000,
			width: 200,
			allowBlank:	true,
			margins: '0 20 0 0'	
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame			: true,
		width: 500,		
		autoHeight	: true,
		title: '<center>CARGA DE INFORMACI�N DE CUADROS</center>',				
		layout		: 'form',
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid: true,
		buttons: [		
			{
				text: 'Enviar',
				id: 'btnEnviar',
				iconCls: 'icoEnviar',
				formBind: true,	
				handler: function(boton, evento){
					var moneda = Ext.getCmp('cmbClaveMoneda');
					var tituloMenu = Ext.getCmp('txtaTituloMenu');
					var tituloRepo = Ext.getCmp('txtaTituloReporte');
					var subRepo = Ext.getCmp('txtaSubReporte');
					var cabecera = Ext.getCmp('txtaCabecera');
					var pie = Ext.getCmp('txtaPieCuadro');
					if(Ext.isEmpty(moneda.getValue())){					
						moneda.markInvalid('Debe capturar el tipo de moneda');
						return;	
					}
					if(Ext.isEmpty(tituloMenu.getValue())){					
						tituloMenu.markInvalid('Debe capturar el t�tulo del men�');
						return;	
					}
					if(Ext.isEmpty(tituloRepo.getValue())){					
						tituloRepo.markInvalid('Debe capturar el t�tulo del reporte');
						return;	
					}
					
					if(Ext.isEmpty(subRepo.getValue())){					
						subRepo.markInvalid('Debe capturar el subt�tulo del reporte');
						return;	
					}
					
					if(Ext.isEmpty(cabecera.getValue())){					
						cabecera.markInvalid('Debe capturar el texto de cabecera');
						return;	
					}
					if(Ext.isEmpty(pie.getValue())){					
						pie.markInvalid('Debe capturar el texto del pie de p�gina');
						return;	
					}
					Ext.getCmp('btnEnviar').disable();
					fp.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: 'subeCuadroExt01.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
										informacion : 'enviar'
						}),
						callback: procesarSuccessFailureInsertar
					});
				}
			}
		]
	});
	var elementosFormaResu = [
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtQuery', 
			value: ''
		},
		{ 	
			xtype: 'displayfield',
			height: 5, 
			value: '  '
		},
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtSalida', 
			value: ''
		},
		{ 	
			xtype: 'displayfield',
			height: 5 
		},
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtMensaje', 
			value: '  '
		},
		{ 	
			xtype: 'displayfield',
			height: 5, 
			value: '  '
		},
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtTitulo',
			align:'center',
			value: ' CARGA DE CUADROS '
		},
		{ 	
			xtype: 'displayfield',
			height: 5, 
			value: '  '
		},
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtMaxima', 
			value: ''
		}	
		
	];
	var fpResultado = new Ext.form.FormPanel({
		id: 'formaResultado',
		frame			: true,
		width: 600,		
		autoHeight	: true,
		hidden:true,
		title: '',				
		layout		: 'form',
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaResu,		
		monitorValid: true
	});
	var elementosFormaResuArchi = [
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtResultado', 
			value: ''
		}
	];
	var fpResArchivo = new Ext.form.FormPanel({
		id: 'fpResArchivo',
		frame			: true,
		width: 600,		
		autoHeight	: true,
		hidden:true,
		title: '<center>CARGA DE ARCHIVOS DE CUADROS</center>',				
		layout		: 'form',
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaResuArchi,		
		monitorValid: true
	});
	var elementosFormaArchivo = [
		{
			xtype: 'container',
			id:	'panelIzq',
			labelWidth:50,
			columnWidth:.95,
			defaults: {	msgTarget: 'side',	anchor: '-40'	},
			layout: 'form',
			items: [
				{
					xtype: 'fileuploadfield',
					id: 'fpfArchivo',
					emptyText: 'Ruta del Archivo',
					buttonText: 'Examinar.',
					name: 'archivoCuadros',
					fieldLabel: 'Archivo',
					buttonCfg: {
						//iconCls: 'iconoLupa',
						width:50
					},
					anchor: '-10'
				}
			]
		},
		{ 	
			xtype: 'displayfield',
			height: 5, 
			value: '  '
		},
		{ 	
			xtype: 'displayfield',
			hidden: true,
			id: 'dfResultado', 
			value: ''
		}	
	];
	var fpArchivo = new Ext.form.FormPanel({
		id: 'formArchivo',
		layout :'anchor',
		width: 500,
		title: '<center>CARGA DE ARCHIVOS DE CUADROS<center>',
		frame: true,
		//collapsible: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		fileUpload: true,
		labelWidth: 30,
		//hidden:true,
		//defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaArchivo,
		buttons:[
			{
				xtype:'button',
				text:'Guardar',
				id:'btnGuardar',
				iconCls: 'icoGuardar',
				formBind: true,
				handler: function(boton, evento){
					var nombreArchivo 	= Ext.getCmp("fpfArchivo");
					if( Ext.isEmpty( nombreArchivo.getValue() ) ){
							nombreArchivo.markInvalid("Debe especificar un archivo");
							return;
					}
					var myRegex = /^.+\.([Cc][Ss][Vv])$/;
					var nombreArchivo1 	= nombreArchivo.getValue();
					if( !myRegex.test(nombreArchivo1) ) {
							nombreArchivo.markInvalid("El formato de el Archivo no es v�lido. Formato(s) soportado(s): CSV.");
							return;
					}else{
							boton.disable();
							var form = Ext.getCmp("formArchivo").getForm();	
							form.submit({
								url: 'subeCuadroExt01.data.jsp?informacion=subirArchivo',
								waitMsg:   				'Subiendo archivo...',
								waitTitle: 				'Por favor espere',
								success:function(form, action, resp) {
									procesaCargaArchivo(null,  true,  action.response );
								},
								failure: function(form, action) {
									 procesaCargaArchivo(null,  true, action.response );
								}
							})
					}
				}
			}
		]
	});
//-------------------------COMPONENTE PRINCIPAL------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 900,
		height: 'auto',
		style: 'margin:0 auto;',
		items:[
			NE.util.getEspaciador(10),
			fp,
			fpResultado,
			NE.util.getEspaciador(10),
			fpArchivo,
			fpResArchivo
		]
	});
	Ext.getCmp('formArchivo').hide();
});