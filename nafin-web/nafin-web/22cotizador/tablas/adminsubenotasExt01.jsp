<!DOCTYPE html> 
<%@page contentType="text/html;charset=windows-1252"%>
<%@ page import="java.util.*" errorPage="/00utils/error.jsp"%>
 <script language="JavaScript" src="../../../00utils/valida.js?<%=session.getId()%>"></script>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<% /* PANTALLA :: ADMIN TESORERIA -> TASAS DE CREDITO NAFIN - ADMINISTRAR TABLAS - MODIFICAR NOTAS */ %>

<html>
  <head>  
   <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Nafi@net</title>
	 <%@ include file="/extjs.jspf" %>
	  <%if(esEsquemaExtJS) {%>
		<%@ include file="/01principal/menu.jspf"%>
		<%}%>
		
	 <script type="text/javascript" src="adminsubenotasExt01.js?<%=session.getId()%>"></script>
	</head>
  
    
<%if(esEsquemaExtJS) {%>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">	
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
				<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
		
		<form id='formParametros' name="formParametros">
			<input type="hidden" id="sTipoUsuario" name=" " value="<%=strTipoUsuario%>"/>	
		</form>	
		
		<form id='formAux' name="formAux" target='_new'></form>
	</body>
	 
<%}else  if(!esEsquemaExtJS) {%>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<div id='areaContenido' style="margin-left: 3px; margin-top: 3px;"></div>
		<form id='formAux' name="formAux" target='_new'></form>
		
		<form id='formParametros' name="formParametros">
			<input type="hidden" id="sTipoUsuario" name=" " value="<%=strTipoUsuario%>"/>	
		</form>

		<form id='formAux' name="formAux" target='_new'></form>		
	</body>	
	
<%}%>

</html>
