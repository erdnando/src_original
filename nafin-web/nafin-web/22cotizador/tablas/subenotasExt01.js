Ext.onReady(function() {

//-------------------------------Elementos Forma--------------------------------	
var elementosForma =  [	
	{
		xtype			: 'displayfield',
		value			: 'CARGA DE INFORMACI�N DE NOTAS.'
	},{
		xtype			: 'textarea',							
		id				: 'idMensaje',
		fieldLabel	: 'Texto de la Nota',
		maxLength	: 3500,
		name			: 'nota',
		width			: 350,
		height 		: 100
	}
	]
//-----------------------------Fin Elementos Forma------------------------------


//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: '<center>Alta de Notas.</center>',
		frame: true,
		collapsible		:true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Enviar',
				id: 'btnEnviar',
				iconCls: 'icoContinuar',
				handler: function (boton, evento){
					var mensaje = Ext.getCmp('idMensaje').getValue();
					if(!Ext.getCmp('idMensaje').isValid()){
						Ext.Msg.show({
						title	: 'Texto de la Nota:',
						msg	: 'El texto excede el tama�o permitido.',
						modal	: true,
						icon	: Ext.Msg.INFO,
						buttons: Ext.Msg.OK,
						fn: function (){
							Ext.getCmp('idMensaje').focus();
							}
						});
					} else if(mensaje == ""){
						Ext.Msg.show({
						title	: 'Texto de la Nota:',
						msg	: 'Debe capturar el texto de la nota.',
						modal	: true,
						icon	: Ext.Msg.INFO,
						buttons: Ext.Msg.OK,
						fn: function (){
							Ext.getCmp('idMensaje').focus();
							}
						});
					} else {
						fp.el.mask('Enviando...', 'x-mask-loading');
						Ext.Ajax.request({
						url: 'subenotasExt01.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						 informacion: 'Enviar'
						}),
						success : function(response) {
							fp.el.unmask();
							var info = Ext.util.JSON.decode(response.responseText);
							if(info.error){ 
								Ext.Msg.show({
								title	: 'Nota:',
								msg	: info.mensaje,
								modal	: true,
								icon	: Ext.Msg.ERROR,
								buttons: Ext.Msg.OK,
								fn: function (){
									Ext.getCmp('idMensaje').reset();
									}
								});
							} else {
								Ext.Msg.show({
								title	: 'Nota:',
								msg	: info.mensaje,
								modal	: true,
								icon	: Ext.Msg.INFO,
								buttons: Ext.Msg.OK,
								fn: function (){
									Ext.getCmp('idMensaje').reset();
									}
								});
							}
						},
						failure: NE.util.mostrarConnError
						});
					}
				}
			}
		]
	});
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fp
		]
	})
//-----------------------------Fin Contenedor Principal-------------------------

});//-----------------------------------------------Fin Ext.onReady(function(){}