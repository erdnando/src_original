<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		com.netro.cadenas.*,
		com.netro.catalogos.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion   = request.getParameter("informacion")  == null?"": (String)request.getParameter("informacion");
String nota				= request.getParameter("nota")    		== null?"": (String)request.getParameter("nota");
String mensaje       = "";
String infoRegresar  = "";

boolean success      = true;
boolean err      		= false;

if(informacion.equals("Enviar")){
	
	JSONObject jsonObj   = new JSONObject();
	JSONObject resultado = new JSONObject();
	PaginadorGenerico pG = new PaginadorGenerico();	
	try{ 
		int idNota =1;
		pG.setCampos("max(idnota) as nota");
		pG.setTabla("CATALOGONOTAS");
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pG);
		Registros reg	=	queryHelper.doSearch();
		while(reg.next()){
			idNota += Integer.parseInt(reg.getString(1));
		}
		ActualizacionCatalogosBean ac = new ActualizacionCatalogosBean();
		MantenimientoCatalogo mC = new MantenimientoCatalogo ();
		mC.setTabla("CATALOGONOTAS");
		mC.setValues("?,"+"'"+nota+"'");
		mC.setBind(String.valueOf(idNota));		
		ac.insertarEnCatalogo(mC);
		mensaje = "Datos insertados.";				
	} catch(Exception e){
		log.debug("Error al insertar: "+ e);
		mensaje  = "No se insertaron los datos.";
		success 	= false;
		err		= true;
	}
	resultado.put("success",   new Boolean(success));
	resultado.put("mensaje",   mensaje             );
	resultado.put("error",     new Boolean(err));	
	infoRegresar = resultado.toString();
} 
%>
<%=infoRegresar%>