Ext.onReady(function(){
	
//--------------------------------HANDLERS-----------------------------------
//---------------------------------------------------------------------------	
	function procesarModificaInformacionCuadro(opts, success, response) {
		var grid = Ext.getCmp('grid');
		var el = grid.getGridEl();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('grid').hide();
			Ext.getCmp('formaIni').hide();
			Ext.getCmp('forma').show();
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('txtCveCuadro').setValue(infoR.IDCUADRO);
			Ext.getCmp('cmbClaveMoneda').setValue(infoR.IDMONEDA);
			Ext.getCmp('txtaTituloMenu').setValue(infoR.TITMENU);
			Ext.getCmp('txtaTituloReporte').setValue(infoR.TITREPORTE);
			Ext.getCmp('txtaSubReporte').setValue(infoR.SUBTITREP);
			Ext.getCmp('txtaCabecera').setValue(infoR.TXTCAB);
			Ext.getCmp('txtaPieCuadro').setValue(infoR.TXTPIE);
			Ext.getCmp('txtaArchivo').setValue(infoR.ARCHIVO)
		}else {
			el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
	function muestraObjeto(IDCUADRO,TITMENU,ARCHIVO){
		
			Ext.getCmp('forma').hide();
			Ext.getCmp('formaResultado').hide();
			Ext.getCmp('grid').hide();
			Ext.getCmp('formaIni').hide();
			Ext.getCmp('txtCveCuadroArch').setValue(IDCUADRO);
			Ext.getCmp('txtTituloMenu').setValue(TITMENU);
			Ext.getCmp('txtNombreArchivo').setValue(ARCHIVO);
			Ext.getCmp('formArchivo').show();
	}
	
	var procesarSuccessFailureEnviar = function(opts, success, response) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		Ext.getCmp('btnEnviar').enable();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('formaResultado').show();
			Ext.getCmp('forma').hide();
			Ext.getCmp('grid').hide();
			Ext.getCmp('formaIni').hide();
			var jsondatos = Ext.util.JSON.decode(response.responseText);
			var IDCUADRO = jsondatos.idCuadro;
			var TITMENU = jsondatos.tituloMenu;
			var ARCHIVO = jsondatos.ARCHIVO;
			Ext.getCmp('txtQuery').setValue(jsondatos.QUERY);
			setTimeout( function muestraObjeto(){ Ext.getCmp('forma').hide();
															  Ext.getCmp('formaResultado').hide();
															  Ext.getCmp('grid').hide();
															  Ext.getCmp('formaIni').hide();
															  Ext.getCmp('txtCveCuadroArch').setValue(IDCUADRO);
															  Ext.getCmp('txtTituloMenu').setValue(TITMENU);
															  Ext.getCmp('txtNombreArchivo').setValue(ARCHIVO);
															  Ext.getCmp('formArchivo').show();
															},3000);
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarDatos = function (store,arrRegistros,opts){
		var fpIni = Ext.getCmp('formaIni');
		fpIni.el.unmask();
		Ext.getCmp('btnConsultar').enable();
		var grid = Ext.getCmp('grid');
		var el = grid.getGridEl();	
		el.unmask();
		var jsonData = store.reader.jsonData;
		Ext.getCmp("grid").setTitle("<center>"+jsonData.TIPO_MONEDA+"</center>");	
		if(arrRegistros!=null){
			if (!grid.isVisible()) {
				grid.show();
			}
			if(store.getTotalCount()>0){
				Ext.getCmp('btnConsultar').enable();
				el.unmask();
			}else{
				Ext.getCmp('btnConsultar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}else{
			Ext.getCmp('btnConsultar').disable();
		}
		
	}
	var modificaInformacionCuadro = function (grid,rowIndex,colIndex,item,event){
		
		var registro = grid.getStore().getAt(rowIndex);
		var el = grid.getGridEl();
		var regIdCuadro = registro.get('IDCUADRO');
		var regTitMenu = registro.get('TITMENU');
		el.mask('Descargando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: 'adminExt01.data.jsp',
			params: Ext.apply({				
				informacion: 'modificaCuadro',
				 regIdCuadro:regIdCuadro,		
				 regTitMenu:regTitMenu
			}),
			callback: procesarModificaInformacionCuadro
		});		
	}
//---------------------------------STORES------------------------------------
//---------------------------------------------------------------------------	
	var catalogoMonedaConsulta = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['1','Pesos'],
			['54','D�lares']
		 ]
	});
	var catalogoMoneda = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['1','Moneda Nacional'],
			['54','Moneda Extranjera']
		 ]
	});
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : 'adminExt01.data.jsp',
		baseParams: {
			informacion: 'ConsultaGrid'
		},
		fields: [		
			{name: 'IDCUADRO'},
			{name: 'TITMENU'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDatos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarDatos(null, null, null);
				}
			}
		}
	});
	var procesaCargaArchivo = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resultado = Ext.decode(response.responseText).resultado;
			var fpArchivo = Ext.getCmp('formArchivo');
			fpArchivo.el.unmask();
			Ext.getCmp('formArchivo').hide();
			Ext.getCmp('formArchivo').getForm().reset();
			Ext.getCmp('forma').hide();
			Ext.getCmp('forma').getForm().reset();
			Ext.getCmp('grid').hide();
			Ext.getCmp('formaIni').hide();
			Ext.getCmp('formaIni').getForm().reset();
			Ext.getCmp('txtResultado').setValue(resultado);
			Ext.getCmp('fpResArchivo').show();

			

		}else{
			
			var element = Ext.getCmp("formArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			// Mostrar mensaje de error
			Ext.Msg.alert("Error",	"Error al cargar los archivos. Favor de probar m�s tarde");

		}

	}
	
//------------------------------COMPONENTES----------------------------------
//---------------------------------------------------------------------------
	var grid = new Ext.grid.GridPanel({	
		store: consultaDataGrid,
		id: 'grid',
		title:'',
		hidden: true,
		frame: true,
		stripeRows : true,
		loadMask: true,
		height: 390,
		width:750,
		header	  : true,
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		align: 'center',
		columns: [
			
			{
				header: '<center>T�tulo Men�</center>',
				tooltip: 'T�tulo Men�',
				dataIndex: 'TITMENU',
				sortable: true,
				width: 600,			
				resizable: true,				
				align: 'left'				
			},
			{
				xtype: 'actioncolumn',
				header: 'Modificar',
				tooltip: 'Modificar',
				dataIndex: 'IDCUADRO',
				width: 100,
				align: 'center',
				
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Modificar';
							return 'icoModificar';		
															
						},handler: modificaInformacionCuadro
						
					}
				]				
			}
		]
	});
	var elementosFormaArchivo = [
		{ 	
			xtype: 'textfield',
			hidden: false,
			name:'idCuadroArch',
			id: 'txtCveCuadroArch', 
			value: '',
			hidden:true,
			displayField: 'descripcion',			
			valueField: 'clave'
		},
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtTituloMenu', 
			value: ''
		},
		{ 	
			xtype: 'displayfield',
			hidden: false,
			fieldLabel:'Archivo',
			id: 'txtNombreArchivo', 
			value: ''
		},
		{ 	
			xtype: 'displayfield',
			height: 10
		},
		{
			xtype: 'container',
			labelWidth:70,
			columnWidth:.95,
			defaults: {	msgTarget: 'side',	anchor: '-40'	},
			layout: 'form',
			items: [
				{
					xtype: 'fileuploadfield',
					id: 'fpfArchivo',
					emptyText: 'Ruta del Archivo',
					buttonText: 'Examinar.',
					name: 'archivoCuadros',
					fieldLabel: 'Archivo',
					buttonCfg: {
						//iconCls: 'iconoLupa',
						width:115
					},
					anchor: '-10'
				}
			]
		}
		 
	];    
	var fpArchivo = new Ext.form.FormPanel({
		id: 'formArchivo',
		layout :'anchor',
		width: 500,
		title: '<center>ADMINISTRACION - CARGA DE ARCHIVOS DE CUADROS<center>',
		layout		: 'form',
		frame: true,
		collapsible: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		fileUpload: true,
		labelWidth: 150,
		hidden:false,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaArchivo,
		buttons:[
			{
				xtype:'button',
				text:'SALIR-(NO REEMPLEZAR EL ARCHIVO)',
				id:'btnSalir',
				iconCls: 'icoSalir',
				formBind: true,
				handler: function(boton, evento){
					window.location = 'adminExt01.jsp';
					
				}
			},
			{
				xtype:'button',
				text:'Guardar',
				id:'btnGuardar',
				iconCls: 'icoGuardar',
				formBind: true,
				handler: function(boton, evento){
					var nombreArchivo 	= Ext.getCmp("fpfArchivo");
					if( Ext.isEmpty( nombreArchivo.getValue() ) ){
							nombreArchivo.markInvalid("Debe capturar el archivo a subir");
							return;
					}
					var myRegex = /^.+\.([Cc][Ss][Vv])$/;
					var nombreArchivo1 	= nombreArchivo.getValue();
					if( !myRegex.test(nombreArchivo1) ) {
							nombreArchivo.markInvalid("El formato de el Archivo no es v�lido. Formato(s) soportado(s): CSV.");
							return;
					}else{
					 boton.disable();
					 var form = fpArchivo.getForm();	
					 form.submit({
						url: 'adminExt01.data.jsp?informacion=subirArchivo',
						success:function(form, action, resp) {
							procesaCargaArchivo(null,  true,  action.response );
						},
						failure: function(form, action) {
							
							 procesaCargaArchivo(null,  true, action.response );
						}
						
					})
				 }	
				}
			}
		]
	});
	var elementosFormaResuArchi = [
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtResultado', 
			value: ''
		}
		
	];
	var elementosFormaResu = [
		{ 	
			xtype: 'displayfield',
			hidden: false,
			id: 'txtQuery', 
			value: ''
		}
	];
	var elementosForma = [
		{ 	
			xtype: 'textfield',
			hidden: false,
			name:'idCuadro',
			id: 'txtCveCuadro', 
			value: '',
			hidden:true,
			displayField: 'descripcion',			
			valueField: 'clave'
		},
		{
			xtype: 'combo',
			name: 'claveMoneda',
			id: 'cmbClaveMoneda',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Moneda',
			emptyText: 'Seleccionar...',	
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveMoneda',
			forceSelection : true,
			allowBlank: true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoMonedaConsulta		
		},
		{
			xtype: 'textarea',
			name:	'tituloMenu',
			id: 'txtaTituloMenu',
			hiddenName:	'tituloMenu',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T�tulo en Men�',
			width: 200,
			maxLength: 3000,
			allowBlank:	true,
			margins: '0 20 0 0'	
		},
		{
			xtype: 'textarea',
			name:	'tituloReporte',
			width: 200,
			maxLength: 3000,
			id: 'txtaTituloReporte',
			hiddenName:	'tituloReporte',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T�tulo en Reporte',
			allowBlank:	true,
			margins: '0 20 0 0'	
		},
		{
			xtype: 'textarea',
			name:	'subReporte',
			id: 'txtaSubReporte',
			width: 200,
			maxLength: 3000,
			hiddenName:	'subReporte',
			fieldLabel: '&nbsp;&nbsp;Subt�tulo en Reporte',
			allowBlank:	true,
			margins: '0 20 0 0'	
		},
		{
			xtype: 'textarea',
			name:	'cabecera',
			maxLength: 3000,
			id: 'txtaCabecera',
			hiddenName:	'cabecera',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Texto Cabecera',
			width: 200,
			allowBlank:	true,
			margins: '0 20 0 0'	
		},{
			xtype: 'textarea',
			name:	'pieCuadro',
			id: 'txtaPieCuadro',
			hiddenName:	'pieCuadro',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Texto Pie Cuadro',
			width: 200,
			maxLength: 3000,
			allowBlank:	true,
			margins: '0 20 0 0'	
		},{
			xtype: 'textarea',
			name:	'archivo',
			id: 'txtaArchivo',
			hiddenName:	'archivo',
			width: 200,
			hidden:true,
			allowBlank:	true,
			margins: '0 20 0 0'	
		}
	];
	
	var elementosFormaIni = [
		{
			xtype: 'combo',
			name: 'cveMoneda',
			id: 'cmbCveMoneda',
			fieldLabel: 'ELEGIR MONEDA',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cveMoneda',
			forceSelection : true,
			allowBlank: true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoMoneda		
		}
	];
	var fpResArchivo = new Ext.form.FormPanel({
		id: 'fpResArchivo',
		frame			: true,
		width: 600,		
		autoHeight	: true,
		title: '<center>ADMINISTRACI�N - CARGA DE ARCHIVOS DE CUADROS</center>',				
		layout		: 'form',
		style: 'margin:0 auto;',
		//bodyStyle: 'padding: 6px',
		//collapsible: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		hidden:true,
		items: elementosFormaResuArchi,		
		monitorValid: true
	});
	var fpResultado = new Ext.form.FormPanel({
		id: 'formaResultado',
		frame			: true,
		width: 650,
		autoHeight	: true,
		title: '<center>ADMINISTRACI�N - CARGA DE CUADROS</center>',	
		layout		: 'form',
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		collapsible: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		hidden:true,
		items: elementosFormaResu,		
		monitorValid: true
	});
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame			: true,
		width: 500,		
		title: 'ADMINISTRACI�N - MODIFICA INFORMACI�N DE CUADROS',				
		collapsible: false,
		titleCollapse: true,
		style: 'margin:0 auto;',
		labelWidth: 150,
		hidden:true,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid: true,
		buttons: [		
			{
				text: 'Enviar',
				id: 'btnEnviar',
				iconCls: 'icoEnviar',
				formBind: true,	
				handler: function(boton, evento){
					var moneda = Ext.getCmp('cmbClaveMoneda');
					var tituloMenu = Ext.getCmp('txtaTituloMenu');
					var tituloRepo = Ext.getCmp('txtaTituloReporte');
					var subRepo = Ext.getCmp('txtaSubReporte');
					var cabecera = Ext.getCmp('txtaCabecera');
					var pie = Ext.getCmp('txtaPieCuadro');
					if(Ext.isEmpty(moneda.getValue())){					
						moneda.markInvalid('Debe capturar el tipo de moneda');
						return;	
					}
					if(Ext.isEmpty(tituloMenu.getValue())){					
						tituloMenu.markInvalid('Debe capturar el t�tulo del men�');
						return;	
					}
					if(Ext.isEmpty(tituloRepo.getValue())){					
						tituloRepo.markInvalid('Debe capturar el t�tulo del reporte');
						return;	
					}
					
					if(Ext.isEmpty(subRepo.getValue())){					
						subRepo.markInvalid('Debe capturar el subt�tulo del reporte');
						return;	
					}
					
					if(Ext.isEmpty(cabecera.getValue())){					
						cabecera.markInvalid('Debe capturar el texto de cabecera');
						return;	
					}
					if(Ext.isEmpty(pie.getValue())){					
						pie.markInvalid('Debe capturar el texto del pie de p�gina');
						return;	
					}
					Ext.getCmp('btnEnviar').disable();
					fp.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: 'adminExt01.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
										informacion : 'enviarDatosNuevos'
						}),
						callback: procesarSuccessFailureEnviar
					});
				}
			}
		]
	});
	var fpIni = new Ext.form.FormPanel({
		id: 'formaIni',
		frame			: true,
		width: 500,		
		autoHeight	: true,
		title: '<center>ADMINISTRACI�N - TIPOS DE REPORTES</center>',				
		layout		: 'form',
		style: 'margin:0 auto;',
		//bodyStyle: 'padding: 6px',
		collapsible: true,
		labelWidth: 150,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaIni,
		buttons:[
			{
				text:'Enviar',
				id:'btnConsultar',
				handler:function(boton,evento){
					var moneda = Ext.getCmp('cmbCveMoneda');
					if(Ext.isEmpty(moneda.getValue())){
						moneda.markInvalid('Debe elegir un tipo de moneda');
						return;
					}
					Ext.getCmp('btnConsultar').disable();
					fpIni.el.mask('Consultando...', 'x-mask-loading');
					consultaDataGrid.load({
						params: Ext.apply(fpIni.getForm().getValues(),{
							informacion : 'ConsultaGrid'
						})
						
					});
				}
			}
		]
	});
//-------------------------COMPONENTE PRINCIPAL------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		//height: 'auto',
		//style: 'margin:0 auto;',
		align: 'center',
		items:[
			NE.util.getEspaciador(20),
			fpIni,
			NE.util.getEspaciador(10),
			grid,
			fp,
			fpResultado,
			fpArchivo,
			fpResArchivo
		]
	});
	Ext.getCmp('formArchivo').hide();
});