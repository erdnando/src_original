<%@ page contentType="application/json;charset=UTF-8"%>
<%@ include file="../22secsession_extjs.jspf" %>
<%@ include file="/appComun.jspf"%>
<%@ page language="java" import="java.io.*,
	java.lang.*,
	java.util.*, 
	java.sql.*, 
	com.netro.cotizador.consAdminMuestra,
	net.sf.json.JSONArray,  
	net.sf.json.JSONObject,
	netropology.utilerias.AccesoDB,
	com.jspsmart.upload.*"
	errorPage="/00utils/error_extjs.jsp"
%>

<jsp:useBean id="mySmartUpload" scope="page" class="com.jspsmart.upload.SmartUpload" /> 

<% 
	String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
	String infoRegresar = "",consulta ="", mensaje="";
	String moneda = (request.getParameter("cveMoneda") !=null)?request.getParameter("cveMoneda"):"";
	String regIdCuadro = (request.getParameter("regIdCuadro")!=null)?request.getParameter("regIdCuadro"):"";
	String regTitMenu = (request.getParameter("regTitMenu")!=null)?request.getParameter("regTitMenu"):"";
	String monedaModifica = (request.getParameter("claveMoneda") !=null)?request.getParameter("claveMoneda"):"";
	String tituloMenu = (request.getParameter("tituloMenu")!=null)?request.getParameter("tituloMenu"):"";
	String tituloReporte = (request.getParameter("tituloReporte")!=null)?request.getParameter("tituloReporte"):"";
	String subReporte = (request.getParameter("subReporte")!=null)?request.getParameter("subReporte"):"";
	String cabecera = (request.getParameter("cabecera")!=null)?request.getParameter("cabecera"):"";
	String pieCuadro = (request.getParameter("pieCuadro")!=null)?request.getParameter("pieCuadro"):"";
	String idCuadro = (request.getParameter("idCuadro")!=null)?request.getParameter("idCuadro"):"";
	JSONObject 	resultado	= new JSONObject();

	consAdminMuestra paginado = new consAdminMuestra();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginado);
	JSONObject jsonObj = new JSONObject();

	if (informacion.equals("ConsultaGrid")) {
		infoRegresar ="";   
		try {    
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			resultado = JSONObject.fromObject(consulta);
			infoRegresar = resultado.toString();
		} catch(Exception e) {
			throw new AppException("Error: adminmuestraExt01.jsp", e);
		}
	}else if(informacion.equals("subeArchivo")){
		
		JSONObject jsonObj1 = new JSONObject();
		String msgError = ""; 
		// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
		String myContentType = "text/html;charset=UTF-8";
		response.setContentType(myContentType);
		request.setAttribute("myContentType", myContentType);

		AccesoDB ad = new AccesoDB();
		ResultSet rs=null;
		String resultadoArch = "";
		String horaHoy		= "";
		String fechaActual = "";
		try{
			horaHoy = new java.text.SimpleDateFormat("hh:mm aaa").format(new java.util.Date());
			fechaActual 	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		}catch(Exception e){
			horaHoy = "";
			fechaActual ="";
		}
		try{
			String strInsert="";
			int numFiles=0;
			String name= "";
			String value= ""; 
			String idCuadroArch = "";
			mySmartUpload.initialize(pageContext);
			mySmartUpload.setAllowedFilesList("csv");
			mySmartUpload.setTotalMaxFileSize(100000);
			mySmartUpload.upload();
			numFiles = mySmartUpload.save(strpath);
			com.jspsmart.upload.File myFile = mySmartUpload.getFiles().getFile(0);
			Enumeration en = mySmartUpload.getRequest().getParameterNames();
			Hashtable hiddens=new Hashtable();
			while (en.hasMoreElements()) {
				name = en.nextElement().toString().trim();
				value = mySmartUpload.getRequest().getParameter(name);
				hiddens.put(name,value);
			}
			idCuadroArch= hiddens.get("idCuadroArchivo").toString();
			if (myFile.getFileName().trim().equals("")){
				resultadoArch = "No se especificó ningún archivo .";
			}else{
				ad.conectarDB();
				
				String pedazo = "UPDATE CUADROS set bi_archivo = ?  ,  ARCHIVO = ?  where idcuadro = ? " ;
				
				
				java.io.File farchivo = new java.io.File(strpath+myFile.getFileName());
				FileInputStream fis = new FileInputStream(farchivo);
				PreparedStatement ps = null;
				ps = ad.queryPrecompilado(pedazo);
				
				ps.setBinaryStream(1, fis, (int)farchivo.length());
				ps.setString(2, myFile.getFileName());
				ps.setString(3, idCuadroArch);
				ps.executeUpdate();
				ps.close();
				
				System.out.println("pedazo  "+pedazo);
				
				strInsert =	" INSERT INTO BIT_ALTA_CURVA( " +
							   " IDCURVA,DC_ALTA,CG_NOMBRE_USUARIO,CG_TIPO)" + 
							   " VALUES("+idCuadroArch+", Sysdate , '"+ strNombreUsuario+"','T')";		
								
				System.out.println("Query INSERTA HISTORICO:::"+strInsert);	
				ad.ejecutaSQL(strInsert);
				ad.cerrarDB(true);
				resultadoArch = "El archivo "+myFile.getFileName()+" se subió exitosamente.";
			}
			jsonObj1.put("resultado", resultadoArch+"<br>Fecha "+fechaActual+" Hora "+horaHoy);
			
	} catch (Exception exception) {
		
		java.io.StringWriter outSW = new java.io.StringWriter();
		exception.printStackTrace(new java.io.PrintWriter(outSW));
		 msgError = outSW.toString();		
	}
		jsonObj1.put("success", new Boolean(true));
		jsonObj1.put("msgError", msgError);		
		infoRegresar = jsonObj1.toString();
		
		infoRegresar = jsonObj1.toString();
		
		System.out.println("infoRegresar  "+infoRegresar);  
	}
%>
<%=infoRegresar%>