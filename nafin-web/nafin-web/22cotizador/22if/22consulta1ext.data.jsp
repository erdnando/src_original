<%@ page contentType="application/json;charset=UTF-8" 
    import=" 
	java.util.*,
	netropology.utilerias.*,
	com.netro.pdf.*,
	com.netro.afiliacion.*,
	com.netro.exception.*,
	com.netro.cotizador.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="../22secsession_extjs.jspf"%>
<jsp:useBean id="consulta" scope="page" class="com.netro.cotizador.Solicitud" />
<jsp:setProperty name="consulta" property="*" />
<% 
  String informacion  = request.getParameter("informacion")==null?"":request.getParameter("informacion");	
  String operacion_2  = request.getParameter("operacion_2")==null?"":request.getParameter("operacion_2");		
  String operacion    = request.getParameter("operacion")==null?"":request.getParameter("operacion");	
  String accion       = (request.getParameter("accion")==null)?"":request.getParameter("accion");
  String fechaInicio  = (request.getParameter("fechaInicio")==null)?"":request.getParameter("fechaInicio");
  String fechaFinal   = (request.getParameter("fechaFinal")==null)?"":request.getParameter("fechaFinal");
  String numeroSolicitud = (request.getParameter("numeroSolicitud")==null)?"":request.getParameter("numeroSolicitud");
  String moneda       = (request.getParameter("Hmoneda")==null)?"":request.getParameter("Hmoneda");
  String montoInicio  = (request.getParameter("montoInicio")==null)?"":request.getParameter("montoInicio");
  String montoFinal   = (request.getParameter("montoFinal")==null)?"":request.getParameter("montoFinal");
  String estatus      = (request.getParameter("Hestatus")==null)?"":request.getParameter("Hestatus");
  String resultado    = "";
  
  
  if(informacion.equals("CatalogoMoneda")){
    CatalogoMoneda catalogo = new CatalogoMoneda();
    catalogo.setCampoClave("ic_moneda");
    catalogo.setCampoDescripcion("cd_nombre");
    catalogo.setOrden("2");
    resultado = catalogo.getJSONElementos();
  }
  
  else if(informacion.equals("CatalogoEstatus")){
    CatalogoSimple catalogo = new CatalogoSimple();
    catalogo.setTabla("cotcat_estatus");
    catalogo.setCampoClave("ic_estatus");
    catalogo.setCampoDescripcion("cg_descripcion");
    catalogo.setOrden("2");
    resultado = catalogo.getJSONElementos();
  }
  else if(informacion.equals("Consulta")){ 
  	 HashMap varConsulta = new HashMap();
    varConsulta.put("fechaInicio",fechaInicio);
    varConsulta.put("fechaFinal",fechaFinal);
    varConsulta.put("numeroSolicitud",numeroSolicitud);
    varConsulta.put("moneda",moneda);
    varConsulta.put("montoInicio",montoInicio);
    varConsulta.put("montoFinal",montoFinal);
    varConsulta.put("estatus",estatus);
  	 session.setAttribute("varConsulta",varConsulta);
  	 
    CotizacionIf paginador = new CotizacionIf();
    paginador.setFechaInicio(fechaInicio);
    paginador.setFechaFinal(fechaFinal);
    paginador.setNumeroSolicitud(numeroSolicitud);
    paginador.setMoneda(moneda);
    paginador.setMontoInicio(montoInicio);
    paginador.setMontoFinal(montoFinal);
    paginador.setEstatus(estatus);
    paginador.setIc_if(iNoCliente);
    paginador.setIc_usuario("");
    paginador.setOrigen("");
    paginador.setUsuario_nafin("");
    CQueryHelperRegExtJS queryHelperRegExtJS	= new CQueryHelperRegExtJS(paginador);
    int start = 0, limit = 0;
    	try {
        start = Integer.parseInt((request.getParameter("start")));
        limit = Integer.parseInt((request.getParameter("limit")));				
      } catch(Exception e) {
        throw new AppException("Error en los parametros recibidos", e);
      }
    if(informacion.equals("Consulta") && !operacion_2.equals("PDF-All")){
      try {
        try {
          if(operacion.equals("Generar")){ //Nueva consulta
            queryHelperRegExtJS.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
          }
          String cadena = queryHelperRegExtJS.getJSONPageResultSet(request,start,limit);			
          resultado	= cadena;
        } catch(Exception e) {
          throw new AppException("Error en la paginacion", e);
        }
      }	catch(Exception e) {
        throw new AppException("Error en la paginación", e);
      }
    }
    if(operacion_2.equals("PDF-All")){
      	AccesoDB con = null;
        try {
      
          //>>CREA REPORTE PDF
          CreaArchivo archivo = new CreaArchivo();
          String nombreArchivo = archivo.nombreArchivo()+".pdf";
          //<<CREA REPORTE PDF
          boolean validaPDF=false;
      
          String sConsultar = (request.getParameter("sConsultar")==null)?"":request.getParameter("sConsultar");
          con = new AccesoDB();
          con.conexionDB();
          CQueryHelper queryHelper = null;
          String paginaNo = (request.getParameter("paginaNo") == null) ? "1" : request.getParameter("paginaNo");
          String offset	= 	(request.getParameter("pageOffset") == null) ? "0" : request.getParameter("pageOffset");
          
          queryHelper = new CQueryHelper(new CotizacionIf());
          queryHelper.cleanSession(request);
          int numReg = 0;
          
           if(operacion_2.equals("PDF-All")) {
            String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
            String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaActual    = fechaActual.substring(0,2);
            String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
            String anioActual   = fechaActual.substring(6,10);
            String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
          
            ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
          
              pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                                           ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
                             (String)session.getAttribute("sesExterno"),
                                           (String) session.getAttribute("strNombre"),
                                              (String) session.getAttribute("strNombreUsuario"),
                             (String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
            pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
            pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
            pdfDoc.setTable(12,100);
            numReg = 0;
            // Consultar 
            if(!queryHelper.readyForPaging(request)) {
              queryHelper.executePKQueryIfNecessary(request,con);
            }
            if(queryHelper.readyForPaging(request)) {
              ResultSet rs = queryHelper.getPageResultSet(request,con,Integer.parseInt(offset),15);
              while(rs.next()) {
                if(numReg==0) { //Encabezado
          //Encabezado
          pdfDoc.setCell("Fecha de Solicitud","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Hora y Fecha de Cotización","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Número de Solicitud","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Moneda","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Ejecutivo","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Monto Total Requerido","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Plazo de la Operación","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Estatus","celda02",ComunesPDF.CENTER);
          //pdfDoc.setCell("Tasa mismo dia","celda02",ComunesPDF.CENTER);
          //pdfDoc.setCell("Tasa 48 horas","celda02",ComunesPDF.CENTER);
          //pdfDoc.setCell("Número de Referencia","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Tasa confirmada","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Tipo de cotizacion","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Causas de rechazo","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("Observaciones","celda02",ComunesPDF.CENTER);
        }//encabezado
          String fecha_solicitud = (rs.getString("fecha_solicitud")==null?"":rs.getString("fecha_solicitud").trim());
          String fecha_cotizacion = (rs.getString("fecha_cotizacion")==null?"":rs.getString("fecha_cotizacion").trim());
          String numero_solicitud = (rs.getString("numero_solicitud")==null?"":rs.getString("numero_solicitud").trim());
          String moneda_solicitud = (rs.getString("moneda")==null?"":rs.getString("moneda").trim());
          String ejecutivo = (rs.getString("ejecutivo")==null?"":rs.getString("ejecutivo").trim());
          String monto = (rs.getString("monto")==null?"":(rs.getString("monto").trim()));
          String plazo  = (rs.getString("plazo")==null?"":rs.getString("plazo").trim());
          String estatus_solicitud = (rs.getString("estatus")==null?"":rs.getString("estatus").trim());
          //String tasa_mismo_dia = (rs.getString("tasa_mismo_dia")==null?"":Comunes.formatoDecimal(rs.getString("tasa_mismo_dia"),4)+" %");
          //String tasa_spot = (rs.getString("tasa_spot")==null?"":Comunes.formatoDecimal(rs.getString("tasa_spot"),4)+" %");
          //String num_referencia = (rs.getString("")==null?"":rs.getString("").trim());
          String tasa_confirmada = (rs.getString("tasa_confirmada")==null?"":rs.getString("tasa_confirmada").trim());
          String causa_rechazo = (rs.getString("causa_rechazo")==null?"":rs.getString("causa_rechazo").trim());	
          String obser_oper = (rs.getString("obser_oper")==null?"":rs.getString("obser_oper").trim());	
          String ic_estatus = (rs.getString("ic_estatus")==null?"":rs.getString("ic_estatus").trim());
          String rs_tipo_cotizacion =(rs.getString("tipoCotiza")==null?"":rs.getString("tipoCotiza").trim());
    
          Solicitud sol = new Solicitud();
          sol.setIc_moneda(rs.getString("ic_moneda"));
          sol.setIg_plazo(plazo);
          sol.setIc_tipo_tasa(rs.getString("ic_tipo_tasa"));
          sol.setIc_esquema_recup(rs.getString("ic_esquema_recup"));
          sol.setCg_ut_ppc(rs.getString("cg_ut_ppc"));
          sol.setCg_ut_ppi(rs.getString("cg_ut_ppi"));
          
          String descTasaInd = sol.getDescTasaIndicativa()+" ";			
          
          pdfDoc.setCell(fecha_solicitud,"formas",ComunesPDF.CENTER);
          pdfDoc.setCell(fecha_cotizacion,"formas",ComunesPDF.CENTER);
          pdfDoc.setCell(numero_solicitud,"formas",ComunesPDF.CENTER);
          pdfDoc.setCell(moneda_solicitud,"formas",ComunesPDF.CENTER);
          pdfDoc.setCell(ejecutivo,"formas",ComunesPDF.CENTER);
          pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
          pdfDoc.setCell(plazo+" días","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(estatus_solicitud,"formas",ComunesPDF.CENTER);
          //pdfDoc.setCell(tasa_mismo_dia,"formas",ComunesPDF.CENTER);
          //pdfDoc.setCell(tasa_spot,"formas",ComunesPDF.CENTER);
          //pdfDoc.setCell(num_referencia,"formas",ComunesPDF.CENTER);
          
          if(ic_estatus.equals("7")||ic_estatus.equals("9")){//estatus 7 == Aceptada
            pdfDoc.setCell(descTasaInd+Comunes.formatoDecimal(tasa_confirmada,2)+" %","formas",ComunesPDF.CENTER);
          }else{
            pdfDoc.setCell("","formas",ComunesPDF.CENTER);
          }
          pdfDoc.setCell(rs_tipo_cotizacion,"formas",ComunesPDF.CENTER);
          pdfDoc.setCell(causa_rechazo,"formas",ComunesPDF.CENTER);
          pdfDoc.setCell(obser_oper,"formas",ComunesPDF.CENTER);
          numReg++;
        } // while
        rs.close();
        con.cierraStatement();
        pdfDoc.addTable();
        pdfDoc.endDocument();
        validaPDF = true;
      } //if (queryHelper.readyForPaging(request))
    } //if(operacion.equals("Generar"))
    
          resultado = new String();
          JSONObject jsonObj = new JSONObject();
          jsonObj.put("success", new Boolean(true));
          jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
          resultado = jsonObj.toString();	
  }catch(Exception e){ }
    }
  }
  
  else if(informacion.equals("Cotizacion")){
    String tec = "";
    String ic_solicitud = request.getParameter("ic_solicitud");
    String estatus_solicitud = (request.getParameter("estatus_solicitud")==null)?"":request.getParameter("estatus_solicitud");
    String intermediario = (request.getParameter("intermediario")==null)?"":request.getParameter("intermediario");
    String origen = (request.getParameter("origen")==null)?"":request.getParameter("origen");
    String periodo = "";
    
    SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
    AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
    
    try{
      if(!"".equals(ic_solicitud)) {
        if("2".equals(estatus_solicitud)&&"IF".equals(strTipoUsuario)) {
          autSol.setEstatusSolic(ic_solicitud, "5", estatus_solicitud);
        }
        consulta = solCotEsp.consultaSolicitud(ic_solicitud);
        
        String clase="";
        int tipoClase=0;
        if("3".equals(consulta.getIc_tipo_tasa())){
          tec = solCotEsp.techoTasaProtegida();
        }
        String nombreArchivo= consulta.getNumero_solicitud()+".pdf";
        if( estatus_solicitud.equals("10") && consulta.getPagos()!=null && consulta.getPagos().size()>0 )//10-Confirmada Vencida
            session.setAttribute("pagos2",consulta.getPagos());
    
    if(  operacion.equals("InicializaVariablesGlobales")   ){
         JSONObject jsonObj = new JSONObject();
         jsonObj.put("success",                 new Boolean(true));
         jsonObj.put("frmContenido",            request.getRequestURI());
         jsonObj.put("fn_monto",                Double.toString(consulta.getFn_monto()));
         jsonObj.put("ic_ejecutivo",            consulta.getIc_ejecutivo());
         jsonObj.put("nombre_ejecutivo",        consulta.getNombre_ejecutivo());
         jsonObj.put("df_solicitud",            consulta.getDf_solicitud());
         jsonObj.put("cg_programa",             consulta.getCg_programa());
         jsonObj.put("ic_moneda",               consulta.getIc_moneda());
         jsonObj.put("ic_tipo_tasa",            consulta.getIc_tipo_tasa());
         jsonObj.put("in_num_disp",             consulta.getIn_num_disp());
         jsonObj.put("ig_tipo_piso",            consulta.getIg_tipo_piso());
         jsonObj.put("ig_plazo_max_oper",       Integer.toString(consulta.getIg_plazo_max_oper()));
         jsonObj.put("ig_num_max_disp",         Integer.toString(consulta.getIg_num_max_disp()));
         jsonObj.put("ig_plazo",                consulta.getIg_plazo());
         jsonObj.put("cs_tasa_unica",           consulta.getCs_tasa_unica());
         jsonObj.put("ic_esquema_recup",        consulta.getIc_esquema_recup());
         //System.err.println(".:::::::::::::::::::::: getIc_esquema_recup :::  " + consulta.getIc_esquema_recup());
         jsonObj.put("ig_ppi",                  consulta.getIg_ppi());
         jsonObj.put("cg_ut_ppi",               consulta.getCg_ut_ppi());
         jsonObj.put("in_dia_pago",             consulta.getIn_dia_pago());
         jsonObj.put("ig_ppc",                  consulta.getIg_ppc());
         jsonObj.put("cg_ut_ppc",               consulta.getCg_ut_ppc());
         jsonObj.put("ig_periodos_gracia",      consulta.getIg_periodos_gracia());
         jsonObj.put("cg_ut_periodos_gracia",   consulta.getCg_ut_periodos_gracia());
         jsonObj.put("cg_razon_social_if",      consulta.getCg_razon_social_if());
         jsonObj.put("ig_riesgo_if",            consulta.getIg_riesgo_if());
         jsonObj.put("cg_acreditado",           consulta.getCg_acreditado());
         jsonObj.put("ig_riesgo_ac",            consulta.getIg_riesgo_ac());
         jsonObj.put("diasIrreg",               Integer.toString(consulta.getDiasIrreg()));
         jsonObj.put("observaciones",				consulta.getCg_observaciones());
         
         if( estatus_solicitud.equals("10") && consulta.getPagos()!=null && consulta.getPagos().size()>0 )//10-Confirmada Vencida
            session.setAttribute("pagos2",consulta.getPagos());
         
         List lista = new ArrayList();
         
         for(int i=0;i<Integer.parseInt(consulta.getIn_num_disp());i++){  
            lista.add(consulta.getFn_monto_disp(i));
         }
         jsonObj.put("fn_monto_disp", lista);
            
         resultado = jsonObj.toString();	
    }
    
    else if(operacion.equals("PDF")){
         solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	 
         
         ic_solicitud = request.getParameter("ic_solicitud");
         estatus_solicitud = (request.getParameter("estatus_solicitud")==null)?"":request.getParameter("estatus_solicitud");
         intermediario = (request.getParameter("intermediario")==null)?"":request.getParameter("intermediario");
         origen = (request.getParameter("origen")==null)?"":request.getParameter("origen");
         nombreArchivo= "";
         periodo = "";
         boolean validaPDF = false;
         clase="";
         tipoClase=0;
         
         try{
            consulta = solCotEsp.consultaSolicitud(ic_solicitud);

            nombreArchivo= consulta.getNumero_solicitud()+".pdf";
            boolean muestraCancelado = false;
            if("8".equals(estatus_solicitud))
               muestraCancelado = true;
            ComunesPDF	pdfDoc			= new ComunesPDF(1,strDirectorioTemp+nombreArchivo,"Solicitud Número: "+ consulta.getNumero_solicitud() +"           Pagina ", muestraCancelado);
            float widthsDocs[] = {0.35f,0.75f};
            String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
            String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaActual   = fechaActual.substring(0,2);
            String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
            String anioActual  = fechaActual.substring(6,10);
            String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
         
         
             pdfDoc.encabezadoConImagenes(
               pdfDoc,
               (String)session.getAttribute("strPais"),
               ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
               (String)session.getAttribute("sesExterno"),
               (String)session.getAttribute("strNombre"),
               (String)session.getAttribute("strNombreUsuario"),
               (String)session.getAttribute("strLogo"),
               strDirectorioPublicacion);	
            pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
            pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
         
            pdfDoc.setTable(2,65);
            pdfDoc.setCell("Datos de la Cotización","celda04",ComunesPDF.CENTER,2,1,1);
            pdfDoc.setCell("Número de Solicitud:","formas",ComunesPDF.LEFT,1,1,1);
            pdfDoc.setCell(consulta.getNumero_solicitud(),"formas",ComunesPDF.LEFT,1,1,1);
         
            pdfDoc.setCell("Fecha de Solicitud:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
            pdfDoc.setCell(consulta.getDf_solicitud(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
            pdfDoc.setCell("Hora de Solicitud:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
            pdfDoc.setCell(consulta.getHora_solicitud(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);		
            
            if( !estatus_solicitud.equals("1") & !estatus_solicitud.equals("3") & !estatus_solicitud.equals("4") ){//1=Solicictada,3=Rechazada,4=En proceso
               String auxFecha = "Cotización";
               if("8".equals(estatus_solicitud))
                  auxFecha = "Cancelación";		
               pdfDoc.setCell("Fecha de "+auxFecha+":",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
               pdfDoc.setCell(consulta.getDf_cotizacion(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);		
               pdfDoc.setCell("Hora de "+auxFecha+":",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
               pdfDoc.setCell(consulta.getHora_cotizacion(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
            }
               pdfDoc.setCell("Usuario:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
               pdfDoc.setCell(consulta.getIc_usuario() +" - "+consulta.getNombre_usuario(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
            if(estatus_solicitud.equals("7")||estatus_solicitud.equals("8")||estatus_solicitud.equals("9")||origen.equals("TF")){
               if(!origen.equals("TF")){
                  pdfDoc.setCell("Número de Recibo:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
                  pdfDoc.setCell(consulta.getIg_num_referencia(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
               }
               pdfDoc.setCell(
                  "En caso de que la disposición de los recursos no se lleve a cabo total o parcialmente\n"+
                  "en la fecha estipulada, Nacional Financiera aplicará el cobro de una comisión\n"+
                  "sobre los montos no dispuestos, conforme a las políticas de crédito aplicables\n"+
                  "a este tipo de operaciones."
               ,"formas",ComunesPDF.CENTER,2,1,1);
            }
            pdfDoc.addTable();
            pdfDoc.addText("\n","formas",ComunesPDF.LEFT);
            pdfDoc.setTable(2,90,widthsDocs);
         /*	pdfDoc.setCell("Intermediario / Acreditado","formas",ComunesPDF.LEFT,1,1,0);
            if("IF".equals(strTipoUsuario)){
               intermediario=strNombre;
            }
            pdfDoc.setCell(intermediario + ((!"".equals(intermediario)&&!"".equals(consulta.getCg_acreditado()))?" / ":"")+consulta.getCg_acreditado() ,"formas",ComunesPDF.LEFT,1,1,0);
         */	
            tipoClase=0;
            
            if(!origen.equals("TFE")){
               pdfDoc.setCell("Datos del Ejecutivo Nafin","celda04",ComunesPDF.LEFT,2,1,0);
               pdfDoc.setCell("Nombre del Ejecutivo","formas",ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell(consulta.getNombre_ejecutivo(),"formas",ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell("Correo electrónico",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell(consulta.getCg_mail(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell("Teléfono",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell(consulta.getCg_telefono(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell("Fax",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell(consulta.getCg_fax(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            }
            
            tipoClase=0;
            String tipooper = consulta.getTipoOper();
            
            if(!origen.equals("TFE")){
               pdfDoc.setCell("Datos del Intermediario Financiero/Acreditado","celda04",ComunesPDF.LEFT,2,1,0);
               pdfDoc.setCell("Tipo de operación","formas",ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell(tipooper,"formas",ComunesPDF.LEFT,1,1,0);
               
               if(!"Primer Piso".equals(tipooper)){
                  pdfDoc.setCell("Nombre del Intermediario",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell((!"".equals(consulta.getNombre_if_ci()))?consulta.getNombre_if_ci():consulta.getNombre_if_cs(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                  if(!"IF".equals(strTipoUsuario)&&!"".equals(consulta.getRiesgoIf())){
                     pdfDoc.setCell("Riesgo del Intermediario",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                     pdfDoc.setCell(consulta.getRiesgoIf(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                  }
               }	
            }
            
            if(!"IF".equals(strTipoUsuario)&&!"".equals(consulta.getCg_acreditado())){
               pdfDoc.setCell("Nombre del Acreditado",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell(consulta.getCg_acreditado(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               if("Primer Piso".equals(tipooper)){
                  if(!origen.equals("TFE")){
                     pdfDoc.setCell("Riesgo del Acreditado",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                     pdfDoc.setCell(consulta.getRiesgoAc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                  }
               }
            }
            
            tipoClase=1;
            
            pdfDoc.setCell("Características de la Operación","celda04",ComunesPDF.LEFT,2,1,0);
         
            if(!"".equals(consulta.getCg_programa())){
               pdfDoc.setCell("Nombre del programa o\nacreditado final:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell(consulta.getCg_programa(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);	
            }
            pdfDoc.setCell("Monto total requerido:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell("$ "+Comunes.formatoDecimal(consulta.getFn_monto(),2)+(consulta.getIc_moneda().equals("1")?" Pesos":" Dólares"),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell("Tipo de tasa requerida:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell(consulta.getTipo_tasa(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell("Número de Disposiciones",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell(consulta.getIn_num_disp(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            if(Integer.parseInt(consulta.getIn_num_disp())>1){
               pdfDoc.setCell("Disposiciones múltiples. Tasa de Interés:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell(("S".equals(consulta.getCs_tasa_unica())?"Única":"Varias"),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            }
            pdfDoc.setCell("Plazo de la operación:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell(consulta.getIg_plazo()+" Días",((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell("Esquema de recuperación del capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell(consulta.getEsquema_recup(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
            if(!"1".equals(consulta.getIc_esquema_recup())){
               String ic_esquema_recup = consulta.getIc_esquema_recup();//Valores que puede tomar ic_esquema_recup :
                                                          //1=Cupon cero,2=Tradicional,3=Plan de pagos,4=Rentas
               if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("3")){
                  pdfDoc.setCell("Periodicidad del pago de Interés:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                  //pdfDoc.setCell(consulta.getIg_ppi()+" "+consulta.descripcionUT(consulta.getCg_ut_ppi()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell(consulta.periodoTasa(consulta.getCg_ut_ppi()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               }
               if(!"3".equals(consulta.getIc_esquema_recup())){
                  if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("4")){
                     pdfDoc.setCell("Periodicidad del Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                     //pdfDoc.setCell(consulta.getIg_ppc()+" "+consulta.descripcionUT(consulta.getCg_ut_ppc()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                     pdfDoc.setCell(consulta.periodoTasa(consulta.getCg_ut_ppc()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                  }
               }
               if( ic_esquema_recup.equals("2") && !"".equals(consulta.getCg_pfpc())){
                  pdfDoc.setCell("Primer fecha de pago de Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell(consulta.getCg_pfpc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell("Penúltima fecha de pago de Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell(consulta.getCg_pufpc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
               }
            }
            pdfDoc.addTable();
            
            pdfDoc.setTable(4,90);
            pdfDoc.setCell("Detalle de Disposiciones","celda04",ComunesPDF.LEFT,4,1,0);
            pdfDoc.setCell("Número de Disposición","formas",ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell("Monto","formas",ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell("Fecha de Disposición","formas",ComunesPDF.LEFT,1,1,0);
            pdfDoc.setCell("Fecha de Vencimiento","formas",ComunesPDF.LEFT,1,1,0);
            for(int i=0;i<Integer.parseInt(consulta.getIn_num_disp());i++){
               clase = ((i%2)!=0)?"formas":"celda01";
               pdfDoc.setCell((i+1)+"",clase,ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell("$ "+Comunes.formatoDecimal(consulta.getFn_monto_disp(i),2),clase,ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell(consulta.getDf_disposicion(i),clase,ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell(consulta.getDf_vencimiento(i),clase,ComunesPDF.LEFT,1,1,0);
            }
            pdfDoc.addTable();
            if((consulta.getPagos()).size() > 0){
               ArrayList alFilas = consulta.getPagos();
               pdfDoc.setTable(3,90);
               pdfDoc.setCell("Plan de Pagos Capital","celda04",ComunesPDF.LEFT,3,1,0);
               pdfDoc.setCell("Número de Pago","formas",ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell("Fecha de Pago","formas",ComunesPDF.LEFT,1,1,0);
               pdfDoc.setCell("Monto a pagar","formas",ComunesPDF.LEFT,1,1,0);
               for(int i=0;i<alFilas.size();i++){
                  ArrayList alColumnas = (ArrayList)alFilas.get(i);
                  clase = ((i%2)!=0)?"formas":"celda01";
                  pdfDoc.setCell(alColumnas.get(0).toString(),clase,ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell(alColumnas.get(1).toString(),clase,ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell(Comunes.formatoDecimal(alColumnas.get(2).toString(),2),clase,ComunesPDF.LEFT,1,1,0);
               }
               pdfDoc.addTable();
            }
             //Obteniendo el periodo de las tasas
            if("1".equals(consulta.getIc_esquema_recup())){
               periodo = "Al Vencimiento";
            }else if("4".equals(consulta.getIc_esquema_recup())&&"0".equals(consulta.getCg_ut_ppc())){
               periodo = "Al Vencimiento";
            }else if( !consulta.periodoTasa(consulta.getCg_ut_ppi()).equals("") ){
               periodo = consulta.periodoTasa(consulta.getCg_ut_ppi())+"mente";
            }
            else if( !consulta.periodoTasa(consulta.getCg_ut_ppc()).equals("") ){
               periodo = consulta.periodoTasa(consulta.getCg_ut_ppc())+"mente";
            }else{
               periodo = "Al Vencimiento";
            }
         
            if(!"TF".equals(origen) && !"TFE".equals(origen) ){
               if( estatus_solicitud.equals("2") || estatus_solicitud.equals("5") ){
                  pdfDoc.setTable(1,90);
                  pdfDoc.setCell("TASA INDICATIVA","celda04",ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell("Tasa Indicativa con oferta mismo día:  "+consulta.getDescTasaIndicativa()+" "+consulta.getIg_tasa_md() + " % anual pagadera "+periodo+"\n"+
                              "Tasa Indicativa con oferta 48 horas:     "+consulta.getDescTasaIndicativa()+" "+consulta.getIg_tasa_spot()+ " % anual pagadera "+periodo
                              ,"celda01",ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell("Nota:\n\n","formas",ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell("Tasa con oferta Mismo Día: El Intermediario Financiero tiene la opción de tomarla en firme el mismo día que haya recibido la cotización indicativa a más tardar a las 12:30 horas.\n\n"
                     ,"formas",ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell("Tasa con oferta 48 Horas: El Intermediario Financiero tiene la opción de tomarla en firme a más tardar dos días hábiles después de que la Tesorería haya proporcionado la cotización indicativa antes de las 12:30 horas del segundo día hábil."
                     ,"formas",ComunesPDF.LEFT,1,1,0);				
                  pdfDoc.setCell("\nLas cotizaciones indicativas proporcionadas por la Tesorería de Nacional Financiera son de uso reservado para el intermediario solicitante y será responsabilidad de dicho intermediario el mal uso de la información proporcionada."
                     ,"formas",ComunesPDF.LEFT,1,1,0);				
            //		pdfDoc.setCell("Tasa Mismo Día: El intermediario Financiero tiene la opción de tomarla en firme el mismo día que haya recibido la respuesta antes de las 12:30 pm.","formas",ComunesPDF.LEFT,1,1,1);
                  //pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
            //		pdfDoc.setCell("Tasa 48 Horas: El intermediario Financiero tiene la opción de tomarla en firme a mas tardar dos días hábiles después contados a partir de la fecha que haya recibido respuesta.","formas",ComunesPDF.LEFT,1,1,1);
                  pdfDoc.addTable();
               }
               
               if( estatus_solicitud.equals("7") || estatus_solicitud.equals("8") || estatus_solicitud.equals("9")){
                  pdfDoc.setTable(2,90);
                  pdfDoc.setCell("TASA CONFIRMADA","celda04",ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell("FECHA Y HORA DE "+("8".equals(estatus_solicitud)?"CANCELACION":"CONFIRMACION"),"celda04",ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
                  pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
                  if(consulta.getCs_aceptado_md().equals("S"))
                     pdfDoc.setCell("Tasa Confirmada " +consulta.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(consulta.getIg_tasa_md(),2) + " % anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
                  else
                     pdfDoc.setCell("Tasa Confirmada " +consulta.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2) + "% anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
                  pdfDoc.setCell(("8".equals(estatus_solicitud)?consulta.getDf_cotizacion()+" "+consulta.getHora_cotizacion():consulta.getDf_aceptacion()) ,"celda01",ComunesPDF.LEFT,1,1,1);
                  pdfDoc.addTable();
               }
            }else{
                  autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
                  boolean mismoDiaOk = autSol.validaMismoDia(ic_solicitud);
                  pdfDoc.setTable(1,90);
                  pdfDoc.setCell("TASA COTIZADA","celda04",ComunesPDF.LEFT,1,1,0);
                  
                  if(mismoDiaOk)
                     pdfDoc.setCell("Tasa con oferta mismo dia" +consulta.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(consulta.getIg_tasa_md(),2) + " % anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
                  else
                     pdfDoc.setCell("Tasa con oferta 48 horas" +consulta.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2) + "% anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
                  pdfDoc.addTable();
            }
            
            //pdfDoc.newPage();
            if(!consulta.getCg_observaciones().trim().equals("")){
               pdfDoc.setTable(3,90);
               pdfDoc.setCell("Observaciones:","celda04",ComunesPDF.LEFT,3,1,0);
               pdfDoc.setCell(consulta.getCg_observaciones(),"formas",ComunesPDF.LEFT,3,1,0);
               pdfDoc.addTable();
            }
         
            pdfDoc.endDocument();
            validaPDF = true;
           }catch(Exception e){
               System.err.println(e);
            }
    
      
          JSONObject jsonObj = new JSONObject();
          jsonObj.put("success", new Boolean(true));
          jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
          resultado = jsonObj.toString();	
        }
        
        else if(operacion.equals("Datos")) {
          List reg = new ArrayList();
          HashMap datos = new HashMap();
          
          datos.put("TITULO_NUMERO_SOLICITUD","Número de Solicitud: ");
          datos.put("NUMERO_SOLICITUD",consulta.getNumero_solicitud());
          reg.add(datos);
          
          datos = new HashMap();
          datos.put("TITULO_NUMERO_SOLICITUD","Fecha de Solicitud: ");
          datos.put("NUMERO_SOLICITUD",consulta.getDf_solicitud());
          reg.add(datos);
          
          datos = new HashMap();
          datos.put("TITULO_NUMERO_SOLICITUD","Hora de Solicitud: ");
          datos.put("NUMERO_SOLICITUD",consulta.getHora_solicitud());
          reg.add(datos);

         if( !estatus_solicitud.equals("1") & !estatus_solicitud.equals("3") & !estatus_solicitud.equals("4") ){//1=Solicictada,3=Rechazada,4=En proceso   
             datos = new HashMap();
             String auxFecha = "Cotización: &nbsp;";
             if("8".equals(estatus_solicitud))
                auxFecha = "Cancelación: &nbsp;";
             datos.put("TITULO_NUMERO_SOLICITUD","Fecha de " + auxFecha);
             datos.put("NUMERO_SOLICITUD",consulta.getDf_cotizacion());
             reg.add(datos);
                   
             datos = new HashMap();
             datos.put("TITULO_NUMERO_SOLICITUD","Hora de " + auxFecha);
             datos.put("NUMERO_SOLICITUD",consulta.getHora_cotizacion());
             reg.add(datos);
         }
         
          datos = new HashMap();
          datos.put("TITULO_NUMERO_SOLICITUD","Número y Nombre de Usuario: ");
          datos.put("NUMERO_SOLICITUD",consulta.getIc_usuario() + " - " + consulta.getNombre_usuario());
          reg.add(datos);
          
          if(estatus_solicitud.equals("7")||estatus_solicitud.equals("8")||estatus_solicitud.equals("9")){         
             datos = new HashMap();
             datos.put("TITULO_NUMERO_SOLICITUD","Número de Recibo: ");
             datos.put("NUMERO_SOLICITUD",consulta.getIg_num_referencia());
             reg.add(datos);
          }
          
          JSONObject jsonObj = new JSONObject();
          jsonObj.put("success", new Boolean(true));
          jsonObj.put("estatus_solicitud", estatus_solicitud);
          jsonObj.put("registros",reg);
          resultado = jsonObj.toString();	
        }
        else if(operacion.equals("EjecutivoNafin")){    
          List reg = new ArrayList();
          HashMap datos = new HashMap();
          
          datos.put("TITULO_FILA","Nombre del Ejecutivo: ");
          datos.put("FILA",consulta.getNombre_ejecutivo());
          reg.add(datos);
          
          datos = new HashMap();
          datos.put("TITULO_FILA","Correo Electrónico: ");
          datos.put("FILA",consulta.getCg_mail());
          reg.add(datos);
          
          datos = new HashMap();  
          datos.put("TITULO_FILA","Teléfono: ");
          datos.put("FILA",consulta.getCg_telefono());
          reg.add(datos);
                    
          datos = new HashMap();
          datos.put("TITULO_FILA","Fax: ");
          datos.put("FILA",consulta.getCg_fax());
          reg.add(datos);
          
          String tasaIndicativa = "false";  
          String tasaConfirmada = "false";
          
          if("I".equals(consulta.getTipoCotizacion())&&"IF".equals(strTipoUsuario)){
            tasaIndicativa = "true";  
          }
          if( estatus_solicitud.equals("7") || estatus_solicitud.equals("8") || estatus_solicitud.equals("9")){
            tasaConfirmada = "true";
          } 
          
          resultado = new String();       
          JSONObject jsonObj = new JSONObject();
          jsonObj.put("success", new Boolean(true));
          jsonObj.put("OMTI",tasaIndicativa);
          jsonObj.put("OMTC",tasaIndicativa);
          jsonObj.put("registros",reg);
          resultado = jsonObj.toString();
        }
        else if(operacion.equals("Intermediario")){    
          List reg = new ArrayList();
          HashMap datos = new HashMap();
          
          datos.put("TITULO_FILA_I","Tipo de Operación: ");
          datos.put("FILA_I",consulta.getTipoOper());
          reg.add(datos);
          
          String tipooper = consulta.getTipoOper();
          if(!"Primer Piso".equals(tipooper)){
            datos = new HashMap();
            datos.put("TITULO_FILA_I","Nombre del Intermediario: ");
            datos.put("FILA_I",(!"".equals(consulta.getNombre_if_ci()))?consulta.getNombre_if_ci():consulta.getNombre_if_cs());
            reg.add(datos);
          
            if(!"IF".equals(strTipoUsuario)&&!"".equals(consulta.getRiesgoIf())){
              datos = new HashMap();          
              datos.put("TITULO_FILA_I","Riesgo del Intermediario: ");
              datos.put("FILA_I",consulta.getRiesgoIf());
              reg.add(datos);
            }
          }
          if(!"IF".equals(strTipoUsuario)&&!"".equals(consulta.getCg_acreditado())){         
            datos = new HashMap();
            datos.put("TITULO_FILA_I","Nombre del Acreditado: ");
            datos.put("FILA_I",consulta.getCg_acreditado());
            reg.add(datos);
            if("Primer Piso".equals(tipooper)){
              datos = new HashMap();          
              datos.put("TITULO_FILA_I","Riesgo del Acreditado: ");
              datos.put("FILA_I",consulta.getRiesgoAc());
              reg.add(datos);
            }
          }
          resultado = new String();       
          JSONObject jsonObj = new JSONObject();
          jsonObj.put("success", new Boolean(true));
          jsonObj.put("registros",reg);
          resultado = jsonObj.toString();
        }
        
        else if(operacion.equals("CaracteristicasOperacion")){
          List reg = new ArrayList();
          HashMap datos;
          
          if(!"".equals(consulta.getCg_programa())){
            datos = new HashMap();
            datos.put("TITULO_FILA_I","Nombre del programa o acreditado Final: ");
            datos.put("FILA_I",consulta.getCg_programa());
            reg.add(datos);
          }
          
          datos = new HashMap();
          datos.put("TITULO_FILA_I","Monto total requerido: ");
          datos.put("FILA_I","$ " + Comunes.formatoDecimal(consulta.getFn_monto(),2) + " " + (consulta.getIc_moneda().equals("1")?"Pesos":"") + (consulta.getIc_moneda().equals("54")?"D&oacute;lares":""));
          reg.add(datos);
          
          datos = new HashMap();
          datos.put("TITULO_FILA_I","Tipo de tasa requerida: ");
          datos.put("FILA_I",consulta.getTipo_tasa());
          reg.add(datos);
          
          datos = new HashMap();
          datos.put("TITULO_FILA_I","Número de Disposiciones: ");
          datos.put("FILA_I",consulta.getIn_num_disp());
          reg.add(datos);
          
          if(Integer.parseInt(consulta.getIn_num_disp())>1){
            datos = new HashMap();
            datos.put("TITULO_FILA_I","Disposiciones Múltiples. Tasa de Interés: ");
            datos.put("FILA_I",("S".equals(consulta.getCs_tasa_unica())?"Unica":"Varias"));
            reg.add(datos);
          }
          
          datos = new HashMap();
          datos.put("TITULO_FILA_I","Plazo de Operación: ");
          datos.put("FILA_I",consulta.getIg_plazo() + " Días");
          reg.add(datos);
          
          datos = new HashMap();
          datos.put("TITULO_FILA_I","Esquema de recuperaci&oacute;n del capital: ");
          datos.put("FILA_I",consulta.getEsquema_recup());
          reg.add(datos);
          
          if(!"1".equals(consulta.getIc_esquema_recup())){
            String ic_esquema_recup = consulta.getIc_esquema_recup();//Valores que puede tomar ic_esquema_recup :
						//1=Cupon cero,2=Tradicional,3=Plan de pagos,4=Rentas
            if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("3")){
              datos = new HashMap();
              datos.put("TITULO_FILA_I","Periodicidad del pago de Interés: ");
              datos.put("FILA_I",consulta.periodoTasa(consulta.getCg_ut_ppi()));
              reg.add(datos);
            }
            if(!"3".equals(consulta.getIc_esquema_recup())){
              if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("4")){
                datos = new HashMap();
                datos.put("TITULO_FILA_I","Periodicidad de Capital: ");
                datos.put("FILA_I",consulta.periodoTasa(consulta.getCg_ut_ppc()));
                reg.add(datos);
              }
            }
            if(ic_esquema_recup.equals("2") && !"".equals(consulta.getCg_pfpc())){
                datos = new HashMap();
                datos.put("TITULO_FILA_I","Primer fecha de pago de Capital: ");
                datos.put("FILA_I",consulta.getCg_pfpc());
                reg.add(datos);
                
                datos = new HashMap();
                datos.put("TITULO_FILA_I","Penúltima  fecha de pago de Capital: ");
                datos.put("FILA_I",consulta.getCg_pufpc());
                reg.add(datos);
            }
          }
          resultado = new String();       
          JSONObject jsonObj = new JSONObject();
          jsonObj.put("success", new Boolean(true));
          jsonObj.put("registros",reg);
          resultado = jsonObj.toString();
        }
        
        else if(operacion.equals("DetalleDisp")){
          List reg = new ArrayList();
          HashMap datos;
          
          for(int i=0;i<Integer.parseInt(consulta.getIn_num_disp());i++){
            datos = new HashMap();
            datos.put("TITULO_1",(i+1) + "");
            datos.put("TITULO_2","$ " + Comunes.formatoDecimal(consulta.getFn_monto_disp(i),2));
            datos.put("TITULO_3",consulta.getDf_disposicion(i));
            datos.put("TITULO_4",consulta.getDf_vencimiento(i));
            reg.add(datos);
          }
          resultado = new String();       
          JSONObject jsonObj = new JSONObject();
          jsonObj.put("success", new Boolean(true));
          jsonObj.put("registros",reg);
          resultado = jsonObj.toString();
        }
        else if(operacion.equals("PagosCapital")){
          List reg = new ArrayList();
          HashMap datos;
          ArrayList alFilas = consulta.getPagos();
          int contPlanPagosCapital = consulta.getPagos().size();
          int count = 0;
          
          System.err.println("consulta.getPagos().size() " + consulta.getPagos().size());
          
          for(int i=0;i<alFilas.size();i++){
				ArrayList alColumnas = (ArrayList)alFilas.get(i);
            
            datos = new HashMap();
            datos.put("TITULO_1",(i+1) + "");
            datos.put("TITULO_2",(String)alColumnas.get(0));
            datos.put("TITULO_3",(String)alColumnas.get(1));
            datos.put("TITULO_4","$ " + Comunes.formatoDecimal(alColumnas.get(2).toString(),2));
            reg.add(datos);
            count++;
          }
          
          resultado = new String();       
          JSONObject jsonObj = new JSONObject();
          jsonObj.put("success", new Boolean(true));
          jsonObj.put("registros",reg);
          jsonObj.put("PlanPagosCapital",Integer.toString(count));
          resultado = jsonObj.toString();
        }
        else if(operacion.equals("TasaIndicativa")){
          HashMap datos = new HashMap();
          List reg = new ArrayList();
          
          if("1".equals(consulta.getIc_esquema_recup())){
               periodo = "Al Vencimiento";
            }else if("4".equals(consulta.getIc_esquema_recup())&&"0".equals(consulta.getCg_ut_ppc())){
               periodo = "Al Vencimiento";
            }else if( !consulta.periodoTasa(consulta.getCg_ut_ppi()).equals("") ){
               periodo = consulta.periodoTasa(consulta.getCg_ut_ppi())+"mente";
            }
            else if( !consulta.periodoTasa(consulta.getCg_ut_ppc()).equals("") ){
               periodo = consulta.periodoTasa(consulta.getCg_ut_ppc())+"mente";
            }else{
               periodo = "Al Vencimiento";
            }
          
          
          StringBuffer str = new StringBuffer();
          if( estatus_solicitud.equals("2") || estatus_solicitud.equals("5") ){
             str.append("<b>Nota:</b><br>");
                  str.append("<b><br>Tasa con oferta Mismo D&iacute;a:</b>&nbsp;El Intermediario Financiero tiene la opci&oacute;n de tomarla en firme el mismo d&iacute;a que haya recibido la cotizaci&oacute;n indicativa a m&aacute;s tardar a las 12:30 horas.<br>");
             if(!(Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2)).equals("0.00")){
               str.append("<b><br>Tasa con oferta 48 Horas:</b>&nbsp;El Intermediario Financiero tiene la opción de tomarla en firme a m&aacute;s tardar dos días hábiles despu&eacute;s de que la Tesorería haya proporcionado la cotización indicativa antes de las 12:30 horas del segundo día hábil.");
             }
             str.append("<br><br><br>Las cotizaciones indicativas proporcionadas por la Tesorería de Nacional Financiera son de uso reservado para el intermediario solicitante y será responsabilidad de dicho intermediario el mal uso de la información proporcionada.");
             
             datos.put("NOTA","Nota");
             datos.put("LABELNOTA",str.toString());
             
             if( "I".equals(consulta.getTipoCotizacion()) && "IF".equals(strTipoUsuario) ){
               datos.put("LEYENDA","COTIZACION INDICATIVA,<br> NO ES POSIBLE TOMAR EN FIRME LA OPERACION,<br> FAVOR DE CONSULTAR A SU EJECUTIVO.");
             } else if( "N".equals(consulta.getCs_vobo_ejec()) && "IF".equals(strTipoUsuario) ){
               datos.put("LEYENDA","Para realizar la toma en firme favor de ponerse en contacto <br>con su ejecutivo");
             }
             
             datos.put("TITLE","Tasa Indicativa con oferta mismo día");
             datos.put("LABEL",consulta.getDescTasaIndicativa() + " " + Comunes.formatoDecimal(consulta.getIg_tasa_md(),2) + "% anual pagadera " + periodo);
             
            if(!(Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2)).equals("0.00")){
               reg.add(datos);
               datos=new HashMap();
               datos.put("TITLE","Tasa Indicativa con oferta 48 horas:");
               datos.put("LABEL",consulta.getDescTasaIndicativa() + " " + Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2) + "% anual pagadera " + periodo);
             }
          }
          reg.add(datos);
          resultado = new String();       
          JSONObject jsonObj = new JSONObject();
          jsonObj.put("success", new Boolean(true));
          jsonObj.put("registros",reg);
          resultado = jsonObj.toString();
        }
        else if(operacion.equals("TasaConfirmada")){
          if("1".equals(consulta.getIc_esquema_recup())){
               periodo = "Al Vencimiento";
            }else if("4".equals(consulta.getIc_esquema_recup())&&"0".equals(consulta.getCg_ut_ppc())){
               periodo = "Al Vencimiento";
            }else if( !consulta.periodoTasa(consulta.getCg_ut_ppi()).equals("") ){
               periodo = consulta.periodoTasa(consulta.getCg_ut_ppi())+"mente";
            }
            else if( !consulta.periodoTasa(consulta.getCg_ut_ppc()).equals("") ){
               periodo = consulta.periodoTasa(consulta.getCg_ut_ppc())+"mente";
            }else{
               periodo = "Al Vencimiento";
            }
          HashMap datos = new HashMap();
          List reg = new ArrayList();
          if( estatus_solicitud.equals("7") || estatus_solicitud.equals("8") || estatus_solicitud.equals("9")){
             datos.put("TITLE","FECHA Y HORA DE " + ("8".equals(estatus_solicitud)?"CANCELACION":"CONFIRMACION"));
             if(consulta.getCs_aceptado_md().equals("S")){
               datos.put("LABEL","Tasa Confirmada "+consulta.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(consulta.getIg_tasa_md(),2)+" % anual pagadera " + periodo+" "+tec);
             }else{
               datos.put("LABEL","Tasa Confirmada "+consulta.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2)+" % anual pagadera " + periodo+" "+tec);
             }
             datos.put("LABEL_FECHA",("8".equals(estatus_solicitud)?consulta.getDf_cotizacion()+" "+consulta.getHora_cotizacion():consulta.getDf_aceptacion()));
          }   
          if(!consulta.getCg_observaciones().trim().equals("") ){
            if(!"NB".equals(origen)) {
              datos.put("OBSERVACIONES","<b>Observaciones</b><br><br>" + consulta.getCg_observaciones());
            }
          }          
          
          reg.add(datos);
          resultado = new String();       
          JSONObject jsonObj = new JSONObject();
          jsonObj.put("success", new Boolean(true));
          jsonObj.put("registros",reg);
          resultado = jsonObj.toString();
        }
      } 
    }catch(Exception e){   
      System.out.println("Error: " + e);
    }
  }
  
  else if(informacion.equals("obtieneVariablesSesion")){
  	  HashMap varConsulta = (HashMap)session.getAttribute("varConsulta");
     
      JSONObject jsonObj = new JSONObject();
      jsonObj.put("success", new Boolean(true));
      jsonObj.put("fechaInicio",varConsulta.get("fechaInicio"));
      jsonObj.put("fechaFinal",varConsulta.get("fechaFinal"));
      jsonObj.put("numeroSolicitud",varConsulta.get("numeroSolicitud"));
      jsonObj.put("moneda",varConsulta.get("moneda"));
      jsonObj.put("montoInicio",varConsulta.get("montoInicio"));
      jsonObj.put("montoFinal",varConsulta.get("montoFinal"));
      jsonObj.put("estatus",varConsulta.get("estatus"));
      resultado = jsonObj.toString();
  }
%>
<%=resultado%>
