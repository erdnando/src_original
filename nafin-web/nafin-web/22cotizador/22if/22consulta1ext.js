Ext.onReady(function() { 

  var bandera=false;
  var peticion = 0;
  var regEstatusSolic='';;
  var regIcSolic='';

//------------------------------------------------------------------------------
  /* Funcion para obtener variable URL  */
	function getUrlVar() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			vars[key] = value==''?null:value;
		});
		return vars;
	}
//------------------------------------------------------------------------------
  var ParametroGeneral = {
      fn_monto                :  '',
      ic_ejecutivo            :  '',
      nombre_ejecutivo        :  '',
      df_solicitud            :  '',
      cg_programa             :  '',
      ic_moneda               :  '',
      ic_tipo_tasa            :  '',
      in_num_disp             :  '',
      ig_tipo_piso            :  '',
      ig_plazo_max_oper       :  '',
      ig_num_max_disp         :  '',
      ig_plazo                :  '',
      cs_tasa_unica           :  '',
      ic_esquema_recup        :  '',
      ig_ppi                  :  '',
      cg_ut_ppi               :  '',
      in_dia_pago             :  '',
      ig_ppc                  :  '',
      cg_ut_ppc               :  '',
      ig_periodos_gracia      :  '',
      cg_ut_periodos_gracia   :  '',
      cg_razon_social_if      :  '',
      ig_riesgo_if            :  '',
      cg_acreditado           :  '',
      ig_riesgo_ac            :  '',
      diasIrreg               :  '',
      fn_monto_disp           :  [],
      df_disposicion          :  new Array(),
      df_vencimiento          :  new Array(),
      estatus_solicitud			:	'',
      ic_solicitud				:	'',
      ic_if							:	'',
      observaciones				:	null,
      banderaRecotiza         :  '',
      
      inicializaVariables		:	function(){
      	if(this.ic_solicitud==''){//getUrlVar()['ic_solicitud'];	
				this.ic_solicitud		=	(Ext.getDom('ic_solicitud').value=='')?null:Ext.getDom('ic_solicitud').value;
			}
			if(this.banderaRecotiza==''){//getUrlVar()['banderaRecotiza'];	
				this.banderaRecotiza		=	(Ext.getDom('banderaRecotiza').value=='')?null:Ext.getDom('banderaRecotiza').value;
			}
         //this.banderaRecotiza =  (this.banderaRecotiza=='')?Ext.getDom('banderaRecotiza').value:null;//getUrlVar()['banderaRecotiza'];	
      	this.ic_if				=	Ext.getDom('ic_if').value;
      }
  }
  
  ParametroGeneral.inicializaVariables();
  
  
      var setFormRecotiza	=	function(){
      }
      
  //----------------------- OBTIENE VARIABLES ----------------------------------
  var InicializaVariablesGlobales = {    
      successFnInicializaVariables : function(  result,  request   ){
      	//console.log("InicializaVariablesGlobales.successFnInicializaVariables");
         var jsonData = Ext.util.JSON.decode(result.responseText);         
         try{
            ParametroGeneral.frmContenido          = jsonData.frmContenido;
            ParametroGeneral.fn_monto              = jsonData.fn_monto; //<---MONTO TOTAL REQUERIDO / ENVIAR
            ParametroGeneral.ic_ejecutivo          = jsonData.ic_ejecutivo;
            ParametroGeneral.nombre_ejecutivo      = jsonData.nombre_ejecutivo; //<----NOMBRE EJECUTIVO / ENVIAR
            ParametroGeneral.df_solicitud          = jsonData.df_solicitud;
            ParametroGeneral.cg_programa           = jsonData.cg_programa; //<----NOMBRE DEL PROGRAMA O ACREDITADO FINAL / ENVIAR
            ParametroGeneral.ic_moneda             = jsonData.ic_moneda; //<----IC MONEDA / ENVIAR
            ParametroGeneral.ic_tipo_tasa          = jsonData.ic_tipo_tasa; //<---TIPO TASA / ENVIAR
            ParametroGeneral.in_num_disp           = jsonData.in_num_disp; //<---NUMERO DISPOSICIONES / ENVIAR
            ParametroGeneral.ig_tipo_piso          = jsonData.ig_tipo_piso;
            ParametroGeneral.ig_plazo_max_oper     = jsonData.ig_plazo_max_oper;
            ParametroGeneral.ig_num_max_disp       = jsonData.ig_num_max_disp;
            ParametroGeneral.ig_plazo              = jsonData.ig_plazo;
            ParametroGeneral.cs_tasa_unica         = jsonData.cs_tasa_unica;
            ParametroGeneral.ic_esquema_recup      = jsonData.ic_esquema_recup;
            ParametroGeneral.ig_ppi                = jsonData.ig_ppi;
            ParametroGeneral.cg_ut_ppi             = jsonData.cg_ut_ppi;
            ParametroGeneral.in_dia_pago           = jsonData.in_dia_pago;
            ParametroGeneral.ig_ppc                = jsonData.ig_ppc;
            ParametroGeneral.cg_ut_ppc             = jsonData.cg_ut_ppc;
            ParametroGeneral.ig_periodos_gracia    = jsonData.ig_periodos_gracia;
            ParametroGeneral.cg_ut_periodos_gracia = jsonData.cg_ut_periodos_gracia;
            ParametroGeneral.cg_razon_social_if    = jsonData.cg_razon_social_if; // <----INTERMEDIARIO / ENVIAR
            ParametroGeneral.ig_riesgo_if          = jsonData.ig_riesgo_if;
            ParametroGeneral.cg_acreditado         = jsonData.cg_acreditado;
            ParametroGeneral.ig_riesgo_ac          = jsonData.ig_riesgo_ac;
            ParametroGeneral.diasIrreg             = jsonData.diasIrreg;
            ParametroGeneral.fn_monto_disp         = jsonData.fn_monto_disp;
            ParametroGeneral.df_disposicion        = jsonData.df_disposicion;
            ParametroGeneral.df_vencimiento        = jsonData.df_vencimiento;
            ParametroGeneral.observaciones         = jsonData.observaciones;
            
            /** Llena Forma Re-Cotiza **/
				//console.log("InicializaVariablesGlobales.setFormRecotiza");
				Ext.getCmp('fn_monto').setValue(ParametroGeneral.fn_monto);
				Ext.getCmp('ic_ejecutivo').setValue(ParametroGeneral.ic_ejecutivo);
				Ext.getCmp('nombre_ejecutivo').setValue(ParametroGeneral.nombre_ejecutivo);
				Ext.getCmp('df_solicitud').setValue(ParametroGeneral.df_solicitud);
				Ext.getCmp('cg_programa').setValue(ParametroGeneral.cg_programa);
				Ext.getCmp('ic_moneda').setValue(ParametroGeneral.ic_moneda);
				Ext.getCmp('ic_tipo_tasa').setValue(ParametroGeneral.ic_tipo_tasa);
				Ext.getCmp('in_num_disp').setValue(ParametroGeneral.in_num_disp);
				Ext.getCmp('ig_tipo_piso').setValue(ParametroGeneral.ig_tipo_piso);
				Ext.getCmp('ig_plazo_max_oper').setValue(ParametroGeneral.ig_plazo_max_oper);
				Ext.getCmp('ig_num_max_disp').setValue(ParametroGeneral.ig_num_max_disp);
				Ext.getCmp('ig_plazo').setValue(ParametroGeneral.ig_plazo);
				Ext.getCmp('cs_tasa_unica').setValue(ParametroGeneral.cs_tasa_unica);
				Ext.getCmp('ic_esquema_recup').setValue(ParametroGeneral.ic_esquema_recup);
				Ext.getCmp('ig_ppi').setValue(ParametroGeneral.ig_ppi);
				Ext.getCmp('cg_ut_ppi').setValue(ParametroGeneral.cg_ut_ppi);
				Ext.getCmp('in_dia_pago').setValue(ParametroGeneral.in_dia_pago);
				Ext.getCmp('ig_ppc').setValue(ParametroGeneral.ig_ppc);
				Ext.getCmp('cg_ut_ppc').setValue(ParametroGeneral.cg_ut_ppc);
				Ext.getCmp('ig_periodos_gracia').setValue(ParametroGeneral.ig_periodos_gracia);
				Ext.getCmp('cg_ut_periodos_gracia').setValue(ParametroGeneral.cg_ut_periodos_gracia);
				Ext.getCmp('cg_razon_social_if').setValue(ParametroGeneral.cg_razon_social_if);
				Ext.getCmp('ig_riesgo_if').setValue(ParametroGeneral.ig_riesgo_if);
				Ext.getCmp('cg_acreditado').setValue(ParametroGeneral.cg_acreditado);
				Ext.getCmp('ig_riesgo_ac').setValue(ParametroGeneral.ig_riesgo_ac);
				Ext.getCmp('diasIrreg').setValue(ParametroGeneral.diasIrreg);
				Ext.getCmp('ic_solicitud').setValue(ParametroGeneral.ic_solicitud);
				Ext.getCmp('df_disposicion').setValue(ParametroGeneral.df_disposicion);
				Ext.getCmp('df_vencimiento').setValue(ParametroGeneral.df_vencimiento);
            
            var array=[];
            Ext.each(ParametroGeneral.fn_monto_disp, function(item, record){
              array.push(item);
            });            
				Ext.getCmp('fn_monto_disp').setValue(array);
            
				if(peticion==1){
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						consultaData.load({
							params: Ext.apply(paramSubmit,{
							operacion: 'Generar',
							start:0,limit:15
						})
					});
				}else{
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaDatosCotizacion.load({
						params: Ext.apply(paramSubmit,{
							informacion:'Cotizacion',
							operacion: 'Datos',
							estatus_solicitud: regEstatusSolic,
							ic_solicitud: regIcSolic
						})
					});
				}
				
         }catch(e){
            Ext.Msg.show({
               title    :  'Problemas con los par�metros generales',
               msg      :  'InicializaVariablesGlobales::successFnInicializaVariables(R, R)' + '<br>' + e.description,
               icon     :  Ext.Msg.ERROR,
               buttons  :  Ext.Msg.OK
            });
         }
      },
      
      failureFnInicializaValiables : function(  response,   request   ){
         Ext.Msg.show({
            title    :  response.status + ' - <b>Error al obtener los par�metros generales</b>',// + request.url,
            msg      :  response.responseText,
            icon     :  Ext.Msg.ERROR,
            buttons  :  Ext.Msg.OK
         })
      },
      
      obtieneVariables : function(estatus_solicitud, ic_solicitud, peticiones){
			peticion = peticiones;
         Ext.Ajax.request({
            url      :  '22consulta1ext.data.jsp',
            method   :  'POST',
            params   :  {  informacion : 'Cotizacion', operacion : 'InicializaVariablesGlobales', estatus_solicitud: estatus_solicitud, ic_solicitud : ic_solicitud },
            success  :  this.successFnInicializaVariables,
            failure  :  this.failureFnInicializaValiables
         })
      }      
  }
  
  // >>>>>>> FORMA DINAMICA >>>>>>>
  var creaFormaPrincipal = function(){
	  var elementosForma = [{
		 xtype:'compositefield',
		 fieldLabel:'Fecha de solicitud',
		 border:true,
		 //defaultType: 'datefield',
		 combineErrors: false,
		 msgTarget: 'side',
		 items:[{
			xtype:'datefield',
			name: 'fechaInicio',
			id: 'fechaInicio',
			msgTarget: 'side',
			width:100,
			margins: '0 20 0 0',
			value: new Date()
		 },{
			xtype:'displayfield',
			value:'a',
			width: 5
		 },{
			xtype:'datefield',
			name: 'fechaFinal',
			id: 'fechaFinal',
			msgTarget: 'side',
			width:100,
			margins: '0 20 0 0',
			value: new Date()
		 }]
	  },{
		 xtype: 'textfield',
		 fieldLabel:'N�mero de solicitud',
		 name: 'numeroSolicitud',
		 id: 'numeroSolicitud',
		 maskRe: /^[0-9*(\-)]$/ // Valida n�meros, puntos y guiones
	  },{
		 xtype:'combo',
		 fieldLabel: 'Moneda',
		 displayField:'descripcion',
		 valueField:'clave',
		 triggerAction:'all',
		 typeAhead: true,
		 minChars: 1,
		 name: 'moneda',
		 hiddenName:'Hmoneda',
		 id: 'moneda',
		 emptyText: '- Seleccione -',
		 store: catalogoMoneda
	  },{
		 xtype:'compositefield',
		 fieldLabel: 'Monto del Cr�dito',
		 combineErrors:false,
		 items:[{
			xtype: 'textfield',
			name: 'montoInicio',
			id: 'montoInicio',
			msgTarget: 'side',
			width: 100,
			margins: '0 20 0 0',
			maskRe:/[0-9.]/,
			maxLength:24/*,
			listeners:{
				blur:function(numero){
					if ((/^([0-9])*[.]?[0-9]*$/.test(numero))){
						 Ext.Msg.show({
								 title	:	'Formato Invalido',
								 msg		:	'El valor introducido parece ser un n�mero inv�lido',
								 buttons	:	Ext.Msg.OK,
								 icon		:	Ext.Msg.ERROR,
								 fn		:	function(){Ext.getCmp('montoInicio').setValue('');}
						 });
					}
			 } 
			}*/
		 },{
			xtype: 'displayfield',
			value: 'a',
			width: 5
		 },{
			xtype:'textfield',
			name: 'montoFinal',
			id: 'montoFinal',
			msgTarget: 'side',
			width: 100,
			margins: '0 20 0 0',
			maskRe:/[0-9.]/,
			maxLength:24/*,
			listeners:{
				'blur':function(numero){
					if ((/^([0-9])*[.]?[0-9]*$/.test(numero))){
						 Ext.Msg.show({
								 title	:	'Formato Invalido',
								 msg		:	'El valor introducido parece ser un n�mero inv�lido',
								 buttons	:	Ext.Msg.OK,
								 icon		:	Ext.Msg.ERROR,
								 fn		:	function(){Ext.getCmp('montoFinal').setValue('');}
						 });
					}
			 } 
			}*/
		 }]
	  },{
		 xtype:'combo',
		 fieldLabel: 'Estatus',
		 displayField:'descripcion',
		 valueField:'clave',
		 triggerAction:'all',
		 typeAhead: true,
		 minChars: 1,
		 name: 'estatus',
		 hiddenName:'Hestatus',
		 id: 'estatus',
		 emptyText: '- Seleccione -',
		 store: catalogoEstatus,
		 anchor: '93%',	 
			listeners:	{
				select:	{
					fn:	function(comboField, record, index){
						ParametroGeneral.estatus_solicitud = record.get('clave');
					}
				}
			}
	  }]
     
  	var fp = {
		xtype: 'form',
		id: 'forma',
		style: 'margin:0 auto;',
		collapsible: true,
		title: 'Criterios de B�squeda',
		frame:true,
		width: 410,
		labelWidth: 120,
		titleCollapse: false,
		bodyStyle: 'text-align:left;padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[elementosForma],
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
				
               var fechaInicio = Ext.getCmp('fechaInicio');
               var fechaFinal  = Ext.getCmp('fechaFinal');
               
               var montoInicial  = Ext.getCmp('montoInicio');
               var montoFinal  = Ext.getCmp('montoFinal');
               
               if((Ext.isEmpty(fechaInicio.getValue()) && !Ext.isEmpty(fechaFinal.getValue())) || (!Ext.isEmpty(fechaInicio.getValue()) && Ext.isEmpty(fechaFinal.getValue())) ){
                  //df_consultaNotMax.markInvalid('Debe capturar la fecha de Notificaci�n final');
                  if(Ext.isEmpty(fechaInicio.getValue()) && !Ext.isEmpty(fechaFinal.getValue())){
                     fechaInicio.markInvalid('Debe capturar la fecha de Solicitud Inicial');
                     return;
                  }else if(!Ext.isEmpty(fechaInicio.getValue()) && Ext.isEmpty(fechaFinal.getValue())){
                     fechaFinal.markInvalid('Debe capturar la fecha de Solicitud Final');
                     return;
                  }
               }
               
               if((Ext.isEmpty(montoInicial.getValue())&&!Ext.isEmpty(montoFinal.getValue())) || (!Ext.isEmpty(montoInicial.getValue())&&Ext.isEmpty(montoFinal.getValue())) ){
                  if(Ext.isEmpty(montoInicial.getValue()) && !Ext.isEmpty(montoFinal.getValue())){
                     montoInicial.markInvalid('Debe capturar el monto Inicial');
                     return;
                  }else if(!Ext.isEmpty(montoInicial.getValue()) && Ext.isEmpty(montoFinal.getValue())){
                     montoFinal.markInvalid('Debe capturar el monto Final');
                     return;
                  }
               }
            
            /********   *********/
               Ext.getCmp('grid').show();
               Ext.getCmp('btnGenerarPDFAll').enable();
               Ext.getCmp('btnBajarPDFAll').hide();
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaData.load({
						params: Ext.apply(paramSubmit,{
						operacion: 'Generar',
                  start:0,limit:15
					})
				});
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '22consulta1ext.jsp';
				}
			}
		]
	};//FIN DE LA FORMA
	pnl.insert(0,fp);
	pnl.doLayout();
  }
  
   var frmRecotiza = new Ext.form.FormPanel({  
   	id             :	'frmRecotiza',
   	standardSubmit :  true,
   	hidden	      :	true,
   	url            :  "../SolCotEsp/CapturaSolicitudext.jsp",
		items	:	[{
			xtype	:	'hidden',
			id		:	'fn_monto',
			name	:	'fn_monto',
			value	:	ParametroGeneral.fn_monto
		},{
			xtype	:	'hidden',
			name	:	'ic_ejecutivo',
			id		:	'ic_ejecutivo',
			value	:	ParametroGeneral.ic_ejecutivo
		},{
			xtype	:	'hidden',
			name	:	'nombre_ejecutivo',
			id		:	'nombre_ejecutivo',
			value	:	ParametroGeneral.nombre_ejecutivo
		},{
			xtype	:	'hidden',
			name	:	'ic_solicitud',
			id		:	'ic_solicitud',
         value :  ParametroGeneral.ic_solicitud
      },{
			xtype	:	'hidden',
			name	:	'df_solicitud',
			id		:	'df_solicitud',
			value	:	ParametroGeneral.df_solicitud
		},{
			xtype	:	'hidden',
			name	:	'cg_programa',
			id		:	'cg_programa',
			value	:	ParametroGeneral.cg_programa
		},{
			xtype	:	'hidden',
			name	:	'ic_moneda',
			id		:	'ic_moneda',
			value	:	ParametroGeneral.ic_moneda
		},{
			xtype	:	'hidden',
			name	:	'ic_tipo_tasa',
			id		:	'ic_tipo_tasa',
			value	:	ParametroGeneral.ic_tipo_tasa
		},{
			xtype	:	'hidden',
			name	:	'in_num_disp',
			id		:	'in_num_disp',
			value	:	ParametroGeneral.in_num_disp
		},{
			xtype	:	'hidden',
			name	:	'ig_tipo_piso',
			id		:	'ig_tipo_piso',
			value	:	ParametroGeneral.ig_tipo_piso
		},{
			xtype	:	'hidden',
			name	:	'ig_plazo_max_oper',
			id		:	'ig_plazo_max_oper',
			value	:	ParametroGeneral.ig_plazo_max_oper
		},{
			xtype	:	'hidden',
			name	:	'ig_num_max_disp',
			id		:	'ig_num_max_disp',
			value	:	ParametroGeneral.ig_num_max_disp
		},{
			xtype	:	'hidden',
			name	:	'ig_plazo',
			id		:	'ig_plazo',
			value	:	ParametroGeneral.ig_plazo
		},{
			xtype	:	'hidden',
			name	:	'cs_tasa_unica',
			id		:	'cs_tasa_unica',
			value	:	ParametroGeneral.cs_tasa_unica
		},{
			xtype	:	'hidden',
			name	:	'ic_esquema_recup',
			id		:	'ic_esquema_recup',
			value	:	ParametroGeneral.ic_esquema_recup
		},{
			xtype	:	'hidden',
			name	:	'ig_ppi',
			id		:	'ig_ppi',
			value	:	ParametroGeneral.ig_ppi
		},{
			xtype	:	'hidden',
			name	:	'cg_ut_ppi',
			id		:	'cg_ut_ppi',
			value	:	ParametroGeneral.cg_ut_ppi
		},{
			xtype	:	'hidden',
			name	:	'in_dia_pago',
			id		:	'in_dia_pago',
			value	:	ParametroGeneral.in_dia_pago
		},{
			xtype	:	'hidden',
			name	:	'ig_ppc',
			id		:	'ig_ppc',
			value	:	ParametroGeneral.ig_ppc
		},{
			xtype	:	'hidden',
			name	:	'cg_ut_ppc',
			id		:	'cg_ut_ppc',
			value	:	ParametroGeneral.cg_ut_ppc
		},{
			xtype	:	'hidden',
			name	:	'ig_periodos_gracia',
			id		:	'ig_periodos_gracia',
			value	:	ParametroGeneral.ig_periodos_gracia
		},{
			xtype	:	'hidden',
			name	:	'cg_ut_periodos_gracia',
			id		:	'cg_ut_periodos_gracia',
			value	:	ParametroGeneral.cg_ut_periodos_gracia
		},{
			xtype	:	'hidden',
			name	:	'cg_razon_social_if',
			id		:	'cg_razon_social_if',
			value	:	ParametroGeneral.cg_razon_social_if
		},{
			xtype	:	'hidden',
			name	:	'ig_riesgo_if',
			id	   :	'ig_riesgo_if',
			value	:	ParametroGeneral.ig_riesgo_if
		},{
			xtype	:	'hidden',
			name	:	'cg_acreditado',
			id		:	'cg_acreditado',
			value	:	ParametroGeneral.cg_acreditado
		},{
			xtype	:	'hidden',
			name	:	'ig_riesgo_ac',
			id		:	'ig_riesgo_ac',
			value	:	ParametroGeneral.ig_riesgo_ac
		},{
			xtype	:	'hidden',
			name	:	'diasIrreg',
			id		:	'diasIrreg',
			value	:	ParametroGeneral.diasIrreg
		},{
			xtype	:	'hidden',
			name	:	'fn_monto_disp',
			id		:	'fn_monto_disp',
			value	:	ParametroGeneral.fn_monto_disp
		},{
			xtype	:	'hidden',
			name	:	'df_disposicion',
			id		:	'df_disposicion',
			value	:	ParametroGeneral.df_disposicion
		},{
			xtype	:	'hidden',
			name	:	'df_vencimiento',
			id		:	'df_vencimiento',
			value	:	ParametroGeneral.df_vencimiento
		}]			  
   })
  
//----------------------------------------------------------------------------------------------
	var procesarConsultaDatosCotizacion = function(store, arrRegistros, opts) { 
		if(parseInt(ParametroGeneral.estatus_solicitud)==8){
			Ext.getCmp('divCancel').show();
		}else{
         Ext.getCmp('divCancel').hide();
      }
      
      if(parseInt(ParametroGeneral.estatus_solicitud)==10 || ParametroGeneral.banderaRecotiza=='true'){
         Ext.getCmp('btnRecotizar').show();
      }else{
         Ext.getCmp('btnRecotizar').hide();
      }
      if(parseInt(ParametroGeneral.estatus_solicitud)==7 || parseInt(ParametroGeneral.estatus_solicitud)==8 || parseInt(ParametroGeneral.estatus_solicitud)==9){
         Ext.getCmp('leyenda').setText('<div style="margin:-4px auto;padding: 10px 30px 10px 30px;text-align:center;width:588px;border:#99bce8 1px solid;background:#fff">En caso de que la disposici�n de los recursos no se lleve a cabo total o parcialmente en la ' +
							'fecha estipulada, Nacional Financiera aplicar� el cobro de una comisi�n sobre los ' +
							'montos no dispuestos, conforme a las pol�ticas de cr�dito aplicables a este tipo de operaciones.</div>',false);
      }else{
      	Ext.getCmp('leyenda').hide();	
      }
      
      if(parseInt(ParametroGeneral.estatus_solicitud)==2 || parseInt(ParametroGeneral.estatus_solicitud)==5){
				Ext.getCmp('gridTasaIndicativa').show(); 
				Ext.getCmp('tasaOperNota').show();
      }else{
         Ext.getCmp('gridTasaIndicativa').hide(); 
         Ext.getCmp('tasaOperNota').hide();
      }
		
		var cmpForma = Ext.getCmp('forma');
		paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaDatosEjecutivoNafin.load({
							params: Ext.apply(paramSubmit,{
						  informacion:'Cotizacion',
						  operacion: 'EjecutivoNafin',
						  estatus_solicitud: regEstatusSolic,
						  ic_solicitud: regIcSolic
					})
          });
	}
	
  var procesarSuccessFailureGenerarPDFAll =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFAll');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFAll');
         btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
        var forma = Ext.getDom('formAux');
        forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
        forma.submit();        
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
  var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		btnGenerarPDF.enable();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');			
      var forma = Ext.getDom('formAux');
      forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
      forma.submit();        
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
  
  var procesarConsultaEjecutivoNafin = function(store, arrRegistros, opts) {
    var gridTasaIndicativa = Ext.getCmp('gridTasaIndicativa');
    var gridTasaConfirmada = Ext.getCmp('gridTasaConfirmada');
    var tasaIndicativa = store.getAt(0).get('OMTI');
    var tasaConfirmada = store.getAt(0).get('OMTC');  
    var tasaOperNota_2 = Ext.getCmp('tasaOperNota');
	 
	 var cmpForma = Ext.getCmp('forma');
	 paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
			consultaDatosIntermediario.load({
				params: Ext.apply(paramSubmit,{
				  informacion:'Cotizacion',
				  operacion: 'Intermediario',
				  estatus_solicitud: regEstatusSolic,
				  ic_solicitud: regIcSolic
			})
	 });
	 
	}
	
	var procesarConsultaIntermediario = function(store, arrRegistros, opts) {  
		
		var cmpForma = Ext.getCmp('forma');
		paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
		consultaCaracteristicasOper.load({
			params: Ext.apply(paramSubmit,{
				informacion:'Cotizacion',
				operacion: 'CaracteristicasOperacion',
				estatus_solicitud: regEstatusSolic,
				ic_solicitud: regIcSolic
			})
		});
		
	}
	
	var procesarConsultaCaractOper  = function(store, arrRegistros, opts) { 
		
		var cmpForma = Ext.getCmp('forma');
		paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
		consultaDetalleDisp.load({
			params: Ext.apply(paramSubmit,{
			  informacion:'Cotizacion',
			  operacion: 'DetalleDisp',
			  estatus_solicitud: regEstatusSolic,
			  ic_solicitud: regIcSolic
			})
		});
		
	}
  
  var procesarConsultaDetalleDisp = function(store, arrRegistros, opts) {
		
		var cmpForma = Ext.getCmp('forma');
		paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
		consultaPlanPagosCapital.load({
				params: Ext.apply(paramSubmit,{
				  informacion:'Cotizacion',
				  operacion: 'PagosCapital',
				  estatus_solicitud: regEstatusSolic,
					ic_solicitud: regIcSolic
			})
		 });
  
  }
  
  
  var procesarConsultaTasaIndicativa = function(store, arrRegistros, opts) {
    //var jsonData = store.reader.jsonData;	
    var tasaOperNota_2 = Ext.getCmp('tasaOperNota');
    var tasaOperNota_3 = Ext.getCmp('nota');
    tasaOperNota_2.setText('<div style="color:#006699;font-weight:normal;font-size:14px;margin:-4px auto;padding: 10px 30px 10px 30px;text-align:center;width:588px;border:#99bce8 1px solid;background:#fff">'+store.getAt(0).get('LEYENDA')+'</div>',false);
    tasaOperNota_3.setText('<div style="margin:-4px auto;padding: 10px 30px 10px 30px;text-align:left;width:588px;border:#99bce8 1px solid;background:#fff">'+store.getAt(0).get('LABELNOTA')+'</div>',false);
    
		tasaOperNota_2.show();
		tasaOperNota_3.show();
         
      if(parseInt(ParametroGeneral.estatus_solicitud)==2 || parseInt(ParametroGeneral.estatus_solicitud)==5){
         Ext.getCmp('gridTasaIndicativa').show(); 
      }else{
      	Ext.getCmp('gridTasaIndicativa').hide(); 
         tasaOperNota_2.hide();
         tasaOperNota_3.hide()
      }
		
		var cmpForma = Ext.getCmp('forma');
		paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
		consultaTasaConfirmada.load({
			params: Ext.apply(paramSubmit,{
			  informacion:'Cotizacion',
			  operacion: 'TasaConfirmada',
			  estatus_solicitud: regEstatusSolic,
				ic_solicitud: regIcSolic
			})
		});
   }
  
  var procesaConsultaTasaConfirmada = function(store, arrRegistros, opts) {
  	  try{
		 var observaciones = Ext.getCmp('observaciones');
		 observaciones.setText('<div style="margin:-4px auto;padding: 10px 30px 10px 30px;text-align:left;width:588px;border:#99bce8 1px solid;background:#fff">'+store.getAt(0).get('OBSERVACIONES')+'</div>',false);
		 		 
		 if(parseInt(ParametroGeneral.estatus_solicitud)==7 || parseInt(ParametroGeneral.estatus_solicitud)==8 || parseInt(ParametroGeneral.estatus_solicitud)==9){
		 	gridTasaConfirmada.getColumnModel().setColumnHeader(1,store.getAt(0).get('TITLE'));
		 	Ext.getCmp('gridTasaConfirmada').show();
			Ext.getCmp('tasaOperNota').show();
         
         // 
		 }else{
			Ext.getCmp('gridTasaConfirmada').hide();
			Ext.getCmp('tasaOperNota').hide();
		 }
		 
		 if(ParametroGeneral.observaciones!=''){
		 	 observaciones.show();
		 }else{
		 	 observaciones.hide();
		 }
		 
		 Ext.getCmp('grid').hide();
		 Ext.getCmp('forma').destroy();
		 pnl.doLayout();
		 winCotizacion.show();
		 
     }catch(e){
     	  Ext.Msg.show({
     	  		  title	:	'Error de ejecuci�n',
     	  		  msg		:	e,
     	  		  buttons:	Ext.Msg.OK,
     	  		  icon	:	Ext.Msg.ERROR
     	  });
     }
   }
  
  var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		var btnGenerarPDFAll = Ext.getCmp('btnGenerarPDFAll');
		var btnBajarPDFAll = Ext.getCmp('btnBajarPDFAll');
		
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				btnBajarPDFAll.hide();
				btnGenerarPDFAll.setDisabled(true);
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
   
   var procesaConsultaPlanPagosCapital = function(store, arrRegistros, opts) 	{
      var jsonData = store.reader.jsonData;
      if(parseInt(jsonData.PlanPagosCapital)>0){
         Ext.getCmp('gridDatosPlanPagosCapital').show();
      }else{
      	Ext.getCmp('gridDatosPlanPagosCapital').hide();
      }
		
		var cmpForma = Ext.getCmp('forma');
		paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
		consultaTasaIndicativa.load({
			params: Ext.apply(paramSubmit,{
			  informacion:'Cotizacion',
			  operacion: 'TasaIndicativa',
			  estatus_solicitud: regEstatusSolic,
			  ic_solicitud: regIcSolic
			})
		});
      
   }
  /****************************************************************************
  *                           CATALOGOS                                       *
  ****************************************************************************/
  var catalogoMoneda = new Ext.data.JsonStore({
    id: 'catalogoMoneda',
    root: 'registros',
    fields:['clave','descripcion','loadMsg'],
    url: '22consulta1ext.data.jsp',
    baseParams:{
      informacion: 'CatalogoMoneda'
    },
    totaProperty: 'total',
    autoLoad: true,
    listeners:{
      exception: NE.util.mostrarDataProxyError,
      beforeLoad: NE.util.initMensajeCargaCombo
    }
  });
  var catalogoEstatus = new Ext.data.JsonStore({
    id: 'catalogoMoneda',
    root: 'registros',
    fields:['clave','descripcion','loadMsg'],
    url: '22consulta1ext.data.jsp',
    baseParams:{
      informacion: 'CatalogoEstatus'
    },
    totaProperty: 'total',
    autoLoad: true,
    listeners:{
      exception: NE.util.mostrarDataProxyError,
      beforeLoad: NE.util.initMensajeCargaCombo
    }
  });

  /****************************************************************************
  *                           STORE FOR GRD                                   *
  ****************************************************************************/  
  var consultaDatosCotizacion = new Ext.data.JsonStore({
	root: 'registros',
	url: '22consulta1ext.data.jsp',
	baseParams: {
		informacion:'Cotizacion',
    operacion:'Datos'
	},
	fields: [
				{name: 'NUMERO_SOLICITUD'},
				{name: 'TITULO_NUMERO_SOLICITUD'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaDatosCotizacion,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							//procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
}); 

 var consultaCaracteristicasOper = new Ext.data.JsonStore({
	root: 'registros',
	url: '22consulta1ext.data.jsp',
	baseParams: {
		informacion:'Cotizacion',
    operacion: 'CaracteristicasOperacion'
	},
	fields: [
				{name: 'TITULO_FILA_I'},
				{name: 'FILA_I'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaCaractOper,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaCaractOper(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

  var consultaDatosEjecutivoNafin = new Ext.data.JsonStore({
	root: 'registros',
	url: '22consulta1ext.data.jsp',
	baseParams: {
		informacion:'Cotizacion',
    operacion: 'EjecutivoNafin'
	},
	fields: [
				{name: 'TITULO_FILA'},
				{name: 'FILA'},
        {name: 'OMTI'},
        {name: 'OMTC'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaEjecutivoNafin,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							//procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

  var consultaDatosIntermediario = new Ext.data.JsonStore({
	root: 'registros',
	url: '22consulta1ext.data.jsp',
	baseParams: {
		informacion:'Cotizacion',
    operacion: 'Intermediario'
	},
	fields: [
				{name: 'TITULO_FILA_I'},
				{name: 'FILA_I'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaIntermediario,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							//procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
var consultaDetalleDisp = new Ext.data.JsonStore({
	root: 'registros',
	url: '22consulta1ext.data.jsp',
	baseParams: {
		informacion:'Cotizacion',
    operacion: 'DetalleDisp'
	},
	fields: [
				{name: 'TITULO_1'},
				{name: 'TITULO_2'},
				{name: 'TITULO_3'},
				{name: 'TITULO_4'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaDetalleDisp,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaDetalleDisp(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
//consultaPlanPagosCapital
  var consultaPlanPagosCapital = new Ext.data.JsonStore({
	root: 'registros',
	url: '22consulta1ext.data.jsp',
	baseParams: {
		informacion:'Cotizacion',
    operacion: 'PagosCapital'
	},
	fields: [
				{name: 'PLAN_PAGOS_CAPITAL'},
				{name: 'TITULO_1'},
				{name: 'TITULO_2'},
				{name: 'TITULO_3'},
				{name: 'TITULO_4'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
   listeners   :  {
      load: procesaConsultaPlanPagosCapital
   }
});

  var consultaTasaConfirmada = new Ext.data.JsonStore({
	root: 'registros',
	url: '22consulta1ext.data.jsp',
	baseParams: {
		informacion:'Cotizacion',
    operacion: 'TasaConfirmada'
	},
	fields: [
				{name: 'TITLE'},
				{name: 'LABEL'},
				{name: 'LABEL_FECHA'},
				{name: 'OBSERVACIONES'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
   listeners   :  {
    load: procesaConsultaTasaConfirmada,
    exception: {
		fn: function(proxy, type, action, optionsRequest, response, args) {
			NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
		}
	}
  }
});

  var consultaTasaIndicativa = new Ext.data.JsonStore({
	root: 'registros',
	url: '22consulta1ext.data.jsp',
	baseParams: {
		informacion:'Cotizacion',
    operacion: 'TasaIndicativa'
	},
	fields: [
				{name: 'TITLE'},
				{name: 'LABEL'},
				{name: 'LEYENDA'},
        {name: 'NOTA'},
        {name: 'LABELNOTA'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
  listeners:{
    load: procesarConsultaTasaIndicativa
  }
});

  var consultaData = new Ext.data.JsonStore({
	root: 'registros',
	url: '22consulta1ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'FECHA_SOLICITUD'},
				{name: 'FECHA_COTIZACION'},
				{name: 'NUMERO_SOLICITUD'},
				{name: 'MONEDA'},
				{name: 'EJECUTIVO'},
				{name: 'MONEDA'},
				{name: 'MONTO'},
				{name: 'PLAZO'},
				{name: 'ESTATUS'},
				{name: 'TASA_CONFIRMADA'},
				{name: 'TIPOCOTIZA'},
				{name: 'CAUSA_RECHAZO'},
				{name: 'OBSER_OPER'},
           {name: 'IC_SOLIC_ESP'},
           {name: 'IC_ESTATUS'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});


  var grid = new Ext.grid.GridPanel({
    id         :  'grid',
    hidden     :  true,
    header     :  true,
    loadMask   :  true,
    store      :  consultaData,
    style      :  'margin:0 auto;',
    height     :  453,
	 width      :  900,
    colunmWidth:  true,
	 frame      :  true,
    stripeRows :  true,
	 loadMask   :  true,
    title      :  'Consulta de Solicitudes',
	 deferRowRender: false,
    columns:[{
      header:'Fecha de Solicitud',
      dataIndex: 'FECHA_SOLICITUD',
      width: 150, align: 'center'
    },{
      header:'Hora y Fecha de Cotizaci�n',
      dataIndex: 'FECHA_COTIZACION',
      width: 150, align: 'center'
    },{
      header:'N�mero de Solicitud',
      dataIndex: 'NUMERO_SOLICITUD',
      width: 150, align: 'center'
    },{
      header:'Moneda',
      dataIndex: 'MONEDA',
      width: 150, align: 'center'
    },{
      header:'Ejecutivo',
      dataIndex: 'EJECUTIVO',
      width: 150, align: 'center'
    },{
      header:'Monto Total Requerido',
      dataIndex: 'MONTO',
      width: 150, align: 'right', renderer: Ext.util.Format.usMoney
    },{
      header:'Plazo de la Operaci�n',
      dataIndex: 'PLAZO',
      width: 150, align: 'center',
      renderer :  function(val) { return (val + ' d�as'); }
    },{
      header:'Estatus',
      dataIndex: 'ESTATUS',
      width: 150, align: 'center'
    },{
      header:'Tasa confirmada',
      dataIndex: 'TASA_CONFIRMADA',
      width: 150, align: 'center',
      renderer :  function(value, metadata, registro, rowIndex, colIndex, store){
         if(registro.get('IC_ESTATUS')=='7'){
            return value;
         }else{
            return '';
         }
      }
    },{
      header:'Tipo de Cotizaci�n',
      dataIndex: 'TIPOCOTIZA',
      width: 150, align: 'center'
    },{
      header:'Causas de rechazo',
      dataIndex: 'CAUSA_RECHAZO',
      width: 150, align: 'center'
    },{
      header:'Observaciones',
      dataIndex: 'OBSER_OPER',
      width: 150, align: 'center'
    },{
      xtype: 'actioncolumn',
      header:'Consultar',
      dataIndex: 'IC_ESTATUS',
      width: 150, align: 'center',
      renderer: function(value){      	
        if(value=='1' || value=='3' || value=='4'){
          return 'Solicitud';
        }else{
          return 'Cotizaci�n';
        }
      },
      items: [{ //para cambiar icono segun extension PDF/DOC 
       getClass: function(value, metadata, registro, rowIndex, colIndex, store){
        return 'icoBuscar';
       },
        handler: function(grid, rowIdx, colIds){			
          var reg = grid.getStore().getAt(rowIdx);          
          var cmpForma = Ext.getCmp('forma');
          
			 Ext.getCmp('grid').hide();
			 Ext.getCmp('btnRecotizar').hide();
			          
          ParametroGeneral.estatus_solicitud = reg.get('IC_ESTATUS');
          ParametroGeneral.ic_solicitud = reg.get('IC_SOLIC_ESP');
          
          try{
				 regEstatusSolic = reg.get('IC_ESTATUS');
				 regIcSolic = reg.get('IC_SOLIC_ESP');
				 InicializaVariablesGlobales.obtieneVariables(ParametroGeneral.estatus_solicitud, ParametroGeneral.ic_solicitud, 2);
          }catch(e){
            Ext.Msg.show({
               title    :  'Problemas con los par�metros generales',
               msg      :  e.description,
               icon     :  Ext.Msg.ERROR,
               buttons  :  Ext.Msg.OK
            });          	 
          }
					
			}				
		}]
    }],
		bbar: {
      xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacionVer',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFAll',
					iconCls: 'icoPdf',
					//disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '22consulta1ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion_2:'PDF-All',
                        operacion:'Generar',
								informacion: 'Consulta',
                         ic_if: ParametroGeneral.ic_if,
                         start:0,limit:15
							}),
							callback: procesarSuccessFailureGenerarPDFAll
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFAll',
					hidden: true
				}
			]
		}
  });
  
  /*                      GRID DATOS DE LA COTIZACION                         */
  var gridDatosCotizacion = new Ext.grid.GridPanel({
    xtype: 'grid',
    id: 'gridDatosCotizacion',
    title		:	'Datos de la Cotizaci�n',
    hidden: false,
    header:true,
    store: consultaDatosCotizacion,
    style: 'margin:0 auto;',
		height: 220,
		width: 650,
		frame: false,
    hideHeaders: true,
    enableColumnMove: false,
    enableDragDrop: false,
		enableHdMenu: false,
    stripeRows: true,
		loadMask: true,
    hideHeaders: true,
    columnLines: true,
		deferRowRender: false,
    disableSelection: true,
    columns:[{
      dataIndex:'TITULO_NUMERO_SOLICITUD',
      width:230,align:'right', menuDisabled:true,renderer:function(val){ return '<b>' + val + '</b>'; }
    },{
      dataIndex:'NUMERO_SOLICITUD',
      width:415,align:'left', menuDisabled:true
    }]
  });
  /*                      GRID DATOS EJECUTIVO NAFIN                         */
  var gridDatosEjecutivoNafin = new Ext.grid.GridPanel({
    xtype: 'grid',
    id: 'gridDatosEjecutivoNafin',
    hidden: false,
    header:true,
    store: consultaDatosEjecutivoNafin,
    style: 'margin:0 auto;',
		height: 125,
		width: 650,
		frame: false,
    border:0,
    title:'Datos del Ejecutivo Nafin',
    titleAlign:'center',
    enableColumnMove: false,
    enableDragDrop: false,
		enableHdMenu: false,
    stripeRows: true,
		loadMask: true,
    hideHeaders: false,
    columnLines: true,
		deferRowRender: false,
    disableSelection: true,
    columns:[{
      header:'',
      dataIndex:'TITULO_FILA',
      width:130,align:'left', menuDisabled:true,renderer:function(val){ return '<b>' + val + '</b>'; }
    },{
      header:'',
      dataIndex:'FILA',
      width:515,align:'left', menuDisabled:true
    }]
  });/*                      GRID DATOS INTERMEDIARIO                         */
  var gridDatosIntermediario = new Ext.grid.GridPanel({
    xtype: 'grid',
    id: 'gridDatosIntermediario',
    hidden: false,
    header:true,
    store: consultaDatosIntermediario,
    style: 'margin:0 auto;',
		height: 125,
		width: 650,
		frame: false,
    border:0,
    title:'Datos del Intermediario Financiero/Acreditado',
    titleAlign:'center',
    enableColumnMove: false,
    enableDragDrop: false,
		enableHdMenu: false,
    stripeRows: true,
		loadMask: true,
    hideHeaders: false,
    columnLines: true,
		deferRowRender: false,
    disableSelection: true,
    columns:[{
      header:'',
      dataIndex:'TITULO_FILA_I',
      width:150,align:'left', menuDisabled:true,renderer:function(val){ return '<b>' + val + '</b>'; }
    },{
      header:'',
      dataIndex:'FILA_I',
      width:495,align:'left', menuDisabled:true
    }]
  });
  var gridDatosCaracteristicasOper = new Ext.grid.GridPanel({
    xtype: 'grid',
    id: 'gridDatosCaracteristicasOper',
    hidden: false,
    header:true,
    store: consultaCaracteristicasOper,
    style: 'margin:0 auto;',
		height: 225,
		width: 650,
		frame: false,
    border:0,
    title:'Caracter�sticas de la Operaci�n',
    titleAlign:'center',
    enableColumnMove: false,
    enableDragDrop: false,
		enableHdMenu: false,
    stripeRows: true,
		loadMask: true,
    hideHeaders: false,
    columnLines: true,
		deferRowRender: false,
    disableSelection: true,
    columns:[{
      header:'',
      dataIndex:'TITULO_FILA_I',
      width:230,align:'left', menuDisabled:true,renderer:function(val){ return '<b>' + val + '</b>'; }
    },{
      header:'',
      dataIndex:'FILA_I',
      width:410,align:'left', menuDisabled:true
    }]
  });
  
  var gridDetalleDisp = new Ext.grid.GridPanel({
    xtype: 'grid',
    id: 'gridDetalleDisp',
    hidden: false,
    header:true,
    store: consultaDetalleDisp,
    style: 'margin:0 auto;',
		height: 130,
		width: 650,
		frame: false,
    border:0,
    title:'Detalle de Disposiciones',
    titleAlign:'center',
    enableColumnMove: false,
    enableDragDrop: false,
		enableHdMenu: false,
    stripeRows: true,
		loadMask: true,
    hideHeaders: false,
    columnLines: true,
		deferRowRender: false,
    disableSelection: true,
    columns:[{
      header:'N�mero de Disposici�n',
      dataIndex:'TITULO_1',
      width:120,align:'center', menuDisabled:true,renderer:function(val){ return '<b>' + val + '</b>'; }
    },{
      header:'Monto',
      dataIndex:'TITULO_2',
      width:175,align:'center', menuDisabled:true
    },{
      header:'Fecha de Disposici�n',
      dataIndex:'TITULO_3',
      width:175,align:'center', menuDisabled:true
    },{
      header:'Fecha de Vencimiento',
      dataIndex:'TITULO_4',
      width:175,align:'center', menuDisabled:true
    }]
  });
  var gridDatosPlanPagosCapital = new Ext.grid.GridPanel({
    xtype: 'grid',
    id: 'gridDatosPlanPagosCapital',
    hidden: true,
    header:true,
    store: consultaPlanPagosCapital,
    style: 'margin:0 auto;',
		height: 140,
		width: 650,
		frame: false,
    border:0,
    title:'Plan de Pagos Capital',
    titleAlign:'center',
    enableColumnMove: false,
    enableDragDrop: false,
		enableHdMenu: false,
    stripeRows: true,
		loadMask: true,
    hideHeaders: false,
    columnLines: true,
		deferRowRender: false,
    disableSelection: true,
    columns:[{
      header:'N�mero de Pago',
      dataIndex:'TITULO_2',
      width:200,align:'left', menuDisabled:true
    },{
      header:'Fecha de Pago',
      dataIndex:'TITULO_3',
      width:200,align:'center', menuDisabled:true
    },{
      header:'Monto a Pagar',
      dataIndex:'TITULO_4',
      width:240,align:'right', menuDisabled:true
    }]
  });
  var gridTasaIndicativa = new Ext.grid.GridPanel({
    xtype: 'grid',
    id: 'gridTasaIndicativa',
    hidden: true,
    header:true,
    store: consultaTasaIndicativa,
    style: 'margin:0 auto;',
		height: 100,
		width: 650,
		frame: false,
    border:0,
    title:'Tasa Indicativa',
    titleAlign:'center',
    enableColumnMove: false,
    enableDragDrop: false,
		enableHdMenu: false,
    stripeRows: true,
		loadMask: true,
    hideHeaders: true,
    columnLines: true,
		deferRowRender: false,
    disableSelection: true,
    columns:[{
      dataIndex:'TITLE',
      width:230,align:'left', menuDisabled:true
    },{
      dataIndex:'LABEL',
      width:415,align:'left', menuDisabled:true  
    }]
  });
  
  var gridTasaConfirmada = new Ext.grid.GridPanel({
    xtype: 'grid',
    id: 'gridTasaConfirmada',
    hidden: true,
    store: consultaTasaConfirmada,
    style: 'margin:0 auto;',
	 height: 110,
	 width: 650,
	 frame: false,
    border:0,
    title:'Tasa Confirmada',
    enableColumnMove: false,
    enableDragDrop: false,
    enableHdMenu: false,
    stripeRows: true,
    loadMask: true,
    columnLines: true,
    disableSelection: true,
    columns:[{
      header:'TASA CONFIRMADA', dataIndex:'LABEL',
      width:350,align:'left', menuDisabled:true,
      renderer	:	function(value){	return '<b>' + value + '</b>'	}
    },{
      Header: 'FECHA Y HORA DE...', dataIndex:'LABEL_FECHA',
      width:290,align:'left', menuDisabled:true,
      renderer	:	function(value){	return '<b>' + value + '</b>'	}
    }]
  });

  
  var leyenda = {
    id:   'leyenda',
    xtype: 'label',
    html: ''
  }
  var leyenda2 = {
    xtype: 'label',
    id:'tasaOperNota', 
    width:280, height:150
  }
  var leyenda3 = {
    xtype: 'label',
    id:'nota',hidden:true,
    width:280, height:150
  }
  var observaciones = {
    xtype: 'label',
    id:'observaciones',hidden:true,
    width:280, height:150
  }
  var divCancel = {
   
      xtype    :  'label',
      id       :  'divCancel',
      html     :  '<div style="z-index:100000000;opacity:0.2;filter:alpha(opacity=20);background:url(../../00utils/gif/Cancelado.gif) no-repeat center; position:absolute;left:0px;top:0px;width:800px;height:600px">&nbsp;</div>'
    
  }
  
  var winCotizacion = new Ext.Panel({  
      id			:  'winCotizacion',
      hidden	:	true,
      width  	: 890,  
      style		:	'margin: 0 auto',
      //autoScroll:true,
      items:[
         divCancel,
         gridDatosCotizacion,
         leyenda,
         NE.util.getEspaciador(10),
         gridDatosEjecutivoNafin,
         gridDatosIntermediario,
         gridDatosCaracteristicasOper,
         gridDetalleDisp,
         gridDatosPlanPagosCapital,
         leyenda2,
         gridTasaIndicativa,
         leyenda3,
         gridTasaConfirmada,
         observaciones     
     ],
     bbar:['-','->',{
      xtype :  'button',
      id    :  'btnRecotizar',
      text  :  'Re-Cotizar',
      hidden:  true,
      iconCls: 'icon-register-edit',
      handler: function(){
        Ext.getCmp('frmRecotiza').getForm().submit();          
      }
    },{
      xtype: 'button',
      text: 'Generar Archivo PDF',
      id: 'btnGenerarPDF',
      iconCls: 'icoPdf',
      handler: function(boton, evento){
      	boton.disable();
      	boton.setIconClass('loading-indicator');
      	
        var cmpForma = Ext.getCmp('forma');
        paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
        Ext.Ajax.request({
          url: '22consulta1ext.data.jsp',
          params: Ext.apply(paramSubmit,{
            informacion: 'Cotizacion',
            operacion: 'PDF',
            estatus_solicitud:ParametroGeneral.estatus_solicitud,
            ic_solicitud:ParametroGeneral.ic_solicitud,
            intermediario:ParametroGeneral.cg_razon_social_if
          }),
          callback: procesarSuccessFailureGenerarPDF
        });
      }
    },{
      xtype: 'button',
      text: 'Salir',
      iconCls: 'icoRechazar',
      handler: function(){
      if(bandera&&ParametroGeneral.banderaRecotiza==null){
			window.location.href='/nafin/22cotizador/SolCotEsp/CapturaSolicitudext.jsp';
      }else{
			 winCotizacion.hide();
			 Ext.getCmp('grid').show();
			 creaFormaPrincipal();
			 var frm = Ext.getCmp('forma');
			 ParametroGeneral.inicializaVariables();
			 
			 Ext.Ajax.request({
				url      :  '22consulta1ext.data.jsp',
				params   :  { informacion : 'obtieneVariablesSesion'  },
				failure  :  InicializaVariablesGlobales.failureFnInicializaValiables,
				success  :  function(  result,  request   ){
					var jsonData = Ext.util.JSON.decode(result.responseText);
					Ext.getCmp('fechaInicio').setValue(Ext.util.Format.date(jsonData.fechaInicio, 'd/m/Y'));
					Ext.getCmp('fechaFinal').setValue(Ext.util.Format.date(jsonData.fechaFinal, 'd/m/Y'));
					Ext.getCmp('numeroSolicitud').setValue(jsonData.numeroSolicitud);
					Ext.getCmp('moneda').setValue(jsonData.moneda);
					Ext.getCmp('montoInicio').setValue(jsonData.montoInicio);
					Ext.getCmp('montoFinal').setValue(jsonData.montoFinal);
					Ext.getCmp('estatus').setValue(jsonData.estatus);
					
					try{
						InicializaVariablesGlobales.obtieneVariables(2, ParametroGeneral.ic_solicitud, 1);
					}catch(e){
						Ext.Msg.show({
							title    :  'Problemas con los par�metros generales',
							msg      :  e.description,
							icon     :  Ext.Msg.ERROR,
							buttons  :  Ext.Msg.OK
						});          	 
					}
					
				},
				callback :  function(){ frm.getEl().unmask(); }
			 });
       }
      }
    }] 
   });

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			NE.util.getEspaciador(10),
			grid,
         winCotizacion,
         frmRecotiza
		]
	}); 
	creaFormaPrincipal();
	/*  */
	if(!isNaN(ParametroGeneral.ic_solicitud) && ParametroGeneral.ic_solicitud!=null){
		bandera=true;
      ParametroGeneral.inicializaVariables();
      ParametroGeneral.estatus_solicitud = 1;
      
      try{
         regEstatusSolic = '10';
			regIcSolic = ParametroGeneral.ic_solicitud;
			InicializaVariablesGlobales.obtieneVariables(ParametroGeneral.estatus_solicitud, ParametroGeneral.ic_solicitud, 2);
      }catch(e){
         Ext.Msg.show({
            title    :  'Problemas con los par�metros generales',
            msg      :  e.description,
            icon     :  Ext.Msg.ERROR,
            buttons  :  Ext.Msg.OK
         });          	 
      }
      
		//ParametroGeneral.banderaRecotiza
		//var cmpForma = Ext.getCmp('forma');
		//paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
		/*
		* El estatus_solicitud debe ser igual a 1 || 3 || 4 
		*/
			
		
	}
	/* */ 
});
