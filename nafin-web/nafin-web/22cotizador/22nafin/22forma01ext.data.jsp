<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		java.math.*,
		com.netro.cotizador.MantenimientoCotBean,
		netropology.utilerias.usuarios.Usuario"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion   = request.getParameter("informacion")   == null?"": (String)request.getParameter("informacion");
String ejecutivo     = request.getParameter("ejecutivo")     == null?"": (String)request.getParameter("ejecutivo");
String ic_ejecutivo  = request.getParameter("ic_ejecutivo")  == null?"": (String)request.getParameter("ic_ejecutivo");
String ic_usuario    = request.getParameter("ic_usuario")    == null?"": (String)request.getParameter("ic_usuario");
String areaAnterior  = request.getParameter("areaAnterior")  == null?"": (String)request.getParameter("areaAnterior");
String antesDirector = request.getParameter("antesDirector") == null?"": (String)request.getParameter("antesDirector");
String nombre        = request.getParameter("nombre")        == null?"": (String)request.getParameter("nombre");
String paterno       = request.getParameter("paterno")       == null?"": (String)request.getParameter("paterno");
String materno       = request.getParameter("materno")       == null?"": (String)request.getParameter("materno");
String email         = request.getParameter("email")         == null?"": (String)request.getParameter("email");
String telefono      = request.getParameter("telefono")      == null?"": (String)request.getParameter("telefono");
String fax           = request.getParameter("fax")           == null?"": (String)request.getParameter("fax");
String area          = request.getParameter("area")          == null?"": (String)request.getParameter("area");
String areaId        = request.getParameter("areaId")        == null?"": (String)request.getParameter("areaId");
String director_area = request.getParameter("director_area") == null?"": (String)request.getParameter("director_area");
String accion        = "";
String mensaje       = "";
String infoRegresar  = "";
boolean directorArea = false;
JSONObject resultado = new JSONObject();
MantenimientoCotBean cotBean = new MantenimientoCotBean();

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("CatalogoEjecutivo")){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COTCAT_EJECUTIVO");
	cat.setCampoClave("IC_EJECUTIVO");
	cat.setCampoDescripcion("CG_NOMBRE ||' '|| CG_APPATERNO ||' '|| CG_APMATERNO");
	cat.setOrden("CG_NOMBRE");
	infoRegresar = cat.getJSONElementos();
	
} else if(informacion.equals("CatalogoArea")){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COTCAT_AREA");
	cat.setCampoClave("IC_AREA");
	cat.setCampoDescripcion("CG_DESCRIPCION");
	cat.setOrden("CG_DESCRIPCION");
	infoRegresar = cat.getJSONElementos();
	
} else if(informacion.equals("Buscar_Datos")){
	
	// Limpio las variables
	ic_ejecutivo  = "";
	ic_usuario    = "";
	areaAnterior  = "";
	antesDirector = "";
	nombre        = "";
	paterno       = "";
	materno       = "";
	email         = "";
	telefono      = "";
	fax           = "";
	area          = "";
	director_area = "";
	
	try{
		
		if(!ejecutivo.equals("")){
			
			Vector vdatosEjecutivo = null;
			vdatosEjecutivo = cotBean.obtenDatosEjecutivo(Integer.parseInt(ejecutivo));
			ic_ejecutivo    = (String)vdatosEjecutivo.get(0);
			ic_usuario      = (String)vdatosEjecutivo.get(1);
			areaAnterior    = (String)vdatosEjecutivo.get(8);
			antesDirector   = (String)vdatosEjecutivo.get(9);
			nombre          = (String)vdatosEjecutivo.get(2);
			paterno         = (String)vdatosEjecutivo.get(3);
			materno         = (String)vdatosEjecutivo.get(4);
			email           = (String)vdatosEjecutivo.get(5);
			telefono        = (String)vdatosEjecutivo.get(6);
			fax             = (String)vdatosEjecutivo.get(7);
			area            = (String)vdatosEjecutivo.get(8);
			director_area   = (String)vdatosEjecutivo.get(9);
			// Proceso la variable del checkbox
			if(director_area.equals("S")){      directorArea = true;  } 
			else if(director_area.equals("N")){ directorArea = false; }
			else{                               directorArea = false; }
			
		} else{
			mensaje = "Ocurrió un error al obtener los datos. Vuelva a intentarlo.";
		}
		
	} catch(Exception e){
		
		mensaje = "Ocurrió un error al obtener los datos. " + e;
		log.warn("Ocurrió un error al obtener los datos. " + e);
		
	}
	
	resultado.put("success",       new Boolean(true)        );
	resultado.put("director_area", new Boolean(directorArea));
	resultado.put("ic_ejecutivo",  ic_ejecutivo             );
	resultado.put("ic_usuario",    ic_usuario               );
	resultado.put("areaAnterior",  areaAnterior             );
	resultado.put("antesDirector", antesDirector            );
	resultado.put("nombre",        nombre                   );
	resultado.put("paterno",       paterno                  );
	resultado.put("materno",       materno                  );
	resultado.put("email",         email                    );
	resultado.put("telefono",      telefono                 );
	resultado.put("fax",           fax                      );
	resultado.put("area",          area                     );
	resultado.put("mensaje",       mensaje                  );
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("Guardar_Cambios")){

	boolean existe_director = false;
	// Verifico si ya existe un director 
	if(!director_area.equals("")){
		try{
			existe_director = cotBean.existeDirector(areaId, areaAnterior, antesDirector);
		} catch(Exception e){
		accion = "error";
		mensaje = "Ocurrió un error al modificar los datos. No se pudo procesar el Director. " + e;
		log.warn("Error al validar director. " + e);
		}
	}
	
	if(!accion.equals("error")){
		if(existe_director){
			accion = "error";
			mensaje = "No se puede realizar la modificación debido a que ya existe un director para el area especificada";
		} else{
			try{
				
				if(!director_area.equals("")){
					director_area = "S";
				} else{
					director_area = "N";
				}
				
				cotBean.modificaEjecutivo(Integer.parseInt(ic_ejecutivo), nombre, paterno, materno, email, telefono, fax, areaId, director_area);
				
				Usuario usuario = new Usuario();
				SolicitudArgus solicitud = new SolicitudArgus();
				UtilUsr utilUsr = new UtilUsr();
				
				usuario.setLogin(ic_usuario);
				usuario.setEmail(email);
				usuario.setApellidoPaterno(paterno.toUpperCase());
				usuario.setApellidoMaterno(materno.toUpperCase());
				usuario.setNombre(nombre.toUpperCase());
				solicitud.setUsuario(usuario);
				
				utilUsr.actualizarUsuario(solicitud);
				
				accion = "exito";
				mensaje = "";
				
			} catch(Exception e){
				accion = "error";
				mensaje = "Ocurrió un error al modificar los datos. " + e;
				log.warn("Error al modificar los datos. " + e);
			}
		}
	}
	
	resultado.put("success", new Boolean(true));
	resultado.put("accion",  accion           );
	resultado.put("mensaje", mensaje          );
	infoRegresar = resultado.toString();
	
}
%>
<%=infoRegresar%>