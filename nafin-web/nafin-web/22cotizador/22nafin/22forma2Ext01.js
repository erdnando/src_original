     
    
Ext.onReady(function() {
    
     
    function procesarArchivos(opts, success, response){
        if (success === true && Ext.util.JSON.decode(response.responseText).success ===  true){
            var infoR = Ext.util.JSON.decode(response.responseText);
	    var archivo = infoR.urlArchivo;
	    archivo = archivo.replace('/nafin', '');
	    var params = {nombreArchivo: archivo};
	    fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
	    fp.getForm().getEl().dom.submit();    
            var botonPdf=Ext.getCmp('btnGeneraPDF'); 	
            botonPdf.setIconClass('icoXls');  
            botonPdf.enable();	
	} else{
	    NE.util.mostrarConnError(response, opts);
	}
    }; 
    
    var tipoEjecutivo = new Ext.data.ArrayStore({
        fields	: ['clave', 'ejecutivo'],
	data : [
            ['N','Ninguno'],	
            ['T','Todos'],
            ['M','Monto Mayor a']
	]
    });
    
    var tipoCotizacion = new Ext.data.ArrayStore({
        fields	: ['clave', 'cotizacion'],
	data : [	
            ['S','Habilitado'],
            ['N','Desabilitado']
	]
    });


    var catalogoEjecutivosData = new Ext.data.JsonStore({
	id: 'catalogoEjecutivoDataStore',
        root: 'registros',
	fields: ['clave', 'descripcion', 'loadMsg'],
	url: '22forma2Ext01.data.jsp',
	baseParams: { informacion: 'catalogoEjecutivosData' },
	totalProperty: 'total',
        autoLoad: false,
	listeners: {	    
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
       

    function validaCorreos(correo){
     
        var errores = 0;
	var expr = /^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z]{2,3})$/;
	if(correo !==''){ 
            // Verifico el formato de los correos
            var vecCorreos = correo.split(',');
	    for(var i = 0; i < vecCorreos.length; i++){
                if (!expr.test(vecCorreos[i])){
                    errores++;
                }
            }		    
        }          
        return errores;
     };
         
    var clavesIF = [];
    var autorizacion = [];
    var monto = [];
    var cotizacion = [];
    var correos = [];
    var ejecutivo = [];
    var errorCorreo =0;
            
    function limpiar(){     
        clavesIF = [];
        autorizacion = [];
        monto = [];
        cotizacion = [];
        correos = [];
        ejecutivo = [];
        errorCorreo =0;
    };
    
    
    
    function procesarConsultaDinamica(opts, success, response){
        if (success === true && Ext.util.JSON.decode(response.responseText).success ===  true){
            var jsonData = Ext.util.JSON.decode(response.responseText);
            var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
                                
            var consultaDataDim = new Ext.data.JsonStore( {
                xtype : 'jsonstore', 
                id : 'consultaDataDim', 
                root : 'registros', 
                totalProperty : 'total', 
                messageProperty : 'msg', 
                autoLoad : false, 
                fields : [
                    {   name: 'CLAVE_INTER'},
                    {   name: 'NOMBRE_INTER'},	
                    {   name: 'AUTORIZACION'},
                    {   name: 'MONTO'},
                    {   name: 'COTIZACION'},
                    {   name: 'CORREO_ELECTRONICO'},
                    {   name: 'EJECUTIVO'}                
                ]
            });
            
            consultaDataDim.loadData(jsonData.columnasRegistros);
         
            var grupoDim = new Ext.ux.grid.ColumnHeaderGroup( {
                rows : [
                    [
                        {   header : 'Intermediario <br>Financiero', colspan : 1, align : 'center'},
                        {   header : 'Solicitar Autorizaci�n<br>del Ejecutivo', colspan : 2, align : 'center'},
                        {   header : 'Permitir Cotizaci�n <br>en L�nea', colspan : 1, align : 'center'},
                        {   header : 'Correo Electr�nico', colspan : 1, align : 'center'},
                        {   header : 'Ejecutivos', colspan : 1, align : 'center'}
                    ]
                ]
            }); 
            
            var gridConsulDimamic = new Ext.grid.EditorGridPanel({	
                id: 'gridConsulDimamic',
                margins: '20 0 0 0',		
                style: 'margin:0 auto;',
                title: '.',
                clicksToEdit: 1,                
                plugins: grupoDim,
                autoScroll: true, 
                frame:  true,           
                height: 400,
                width: 900,           
                store: consultaDataDim,  
               columns: [
                { 
                    dataIndex: 'NOMBRE_INTER',  
                    header: 'Intermediario Financiero', 
                    width: 200, 
                    sortable: true, 
                    resizable: true, 
                    align: 'left',  
                    renderer:function(value, metadata, record, rowindex, colindex, store) {
                        metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
                        return value;
                    }
                },
                { 
                    dataIndex: 'AUTORIZACION',
                    header: '',
                    width: 100,	
                    sortable: true,		
                    resizable: true,				
                    align: 'center',
                    editor:{
                        xtype: 'combo',
                        name: 'cmbEje',
                        hiddenName: 'cmbEjeHid',
                        id: 'cmbEjecutivo',	 
                        mode	: 'local',	
                        emptyText: 'Seleccionar',			
                        valueField : 'clave',
                        displayField : 'ejecutivo',	
                        editable: true,
                        typeAhead: true,
                        minChars : 1,
                        store : tipoEjecutivo,
                        forceSelection : true,	
                        triggerAction 	: 'all',	
                        fixed	: true,
                        lazyInit: false,
                        lazyRender: true,
                        listeners:{
                            focus:{ 
                                fn: function (comboField,record,index) { 
                                    comboField.doQuery(comboField.allQuery, true); 
                                    comboField.expand(); 
                                }
                            },
                            select:{
                                fn:function (comboField, record, index) {
                                    comboField.fireEvent('blur'); 
                                }
                            }		
                        }//fin listeners
                    },//fin editor
                    renderer:function(value,params,record){
                        if(value=='N'){
                            record.data.AUTORIZACION=="Ninguno";
                            return "<div align='center'>"+"Ninguno"+"</div>"; 
                        } else if(value=='T'){
                            record.data.AUTORIZACION=="Ninguno";
                            return "<div align='center'>"+"Todos"+"</div>"; 
                        } else if(value=='M'){
                            record.data.AUTORIZACION=="Ninguno";
                            return "<div align='center'>"+"Monto Mayor a"+"</div>"; 
                        } 
                    }
                },
                { 
                    dataIndex: 'MONTO',
                    header: '',
                    width: 100,	
                    sortable: true,		
                    resizable: true,				
                    align: 'center',
                    editor:{
                        xtype: 'numberfield',
                        height: 10,
                        id: 'numMonto',
                        allowBlank: false,
                        minValue: 1,
                        maxValue: 9999999999999998,
                        decimalPrecision: 4
                    },
                    renderer:function(value,params,record){
                        if(record.data.AUTORIZACION !='M'){
                            return ""
			}
			if (value == 'Ninguno'  ){
                            return "";
			} else if (value != 0) {
                            return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>"
			}
                    }
                },
                { 
                    dataIndex: 'COTIZACION',
                    header: '', 
                    width: 100,	
                    sortable: true,		
                    resizable: true,				
                    align: 'center',
                    editor:{		
                        xtype: 'combo',	
                        name: 'cmbCotiza',	
                        hiddenName	: 'cmbCotizaHid',	
                        id	: 'cmbCotizacion',	
                        mode: 'local',
                        emptyText: 'Seleccionar',
                        valueField : 'clave',
                        displayField: 'cotizacion',	
                        editable	: true,	
                        typeAhead	: true,
                        minChars : 1,	
                        store : tipoCotizacion,
                        forceSelection : true,	
                        triggerAction : 'all',
                        fixed: true,
                        lazyInit	: false,
                        lazyRender	: true,
                        listeners	:{
                            focus	:{ 
                                fn	: function (comboField,record,index) { 
                                    comboField.doQuery(comboField.allQuery, true); 
                                    comboField.expand(); 
                                }
                            },
                            select	:{
                                fn :function (comboField, record, index) {
                                    comboField.fireEvent('blur'); 
                                }
                            }		
                        }//fin listeners
                    },//fin editor
                    renderer:function(value){
                        if(value=='S'){
                            return "<div align='center'>"+"Habilitado"+"</div>"; 
			} else if(value=='N'){
                            return "<div align='center'>"+"Desabilitado"+"</div>"; 
			} 
                    }//fin renderer
                },
                {
                    header: 'Correo Electr�nico',  
                    tooltip: 'Correo Electr�nico',
                    dataIndex: 	'CORREO_ELECTRONICO', 
                    fixed:		true,
                    sortable: 	false,	
                    width: 		174,
                    hiddeable: 	false,
                    editor:{
                        xtype:	'textarea',					
                        maxLength: 400,
                        enableKeyEvents: true,
                        listeners:	{
                            blur: function(field, e){
                            
                            if ( (field.getValue()).length > 0){
                                if( validaCorreos(field.getValue()) >0 ) {
                                    Ext.Msg.alert('Observaciones','Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
                                    field.focus();  
                                }
                                
                                if ( (field.getValue()).length > 400 ){
                                    field.setValue((field.getValue()).substring(0,400));
                                    Ext.Msg.alert('Observaciones','Los Correo Electr�nico no debe sobrepasar los 400 caracteres.');								
                                    field.focus();
                                }
                            }
                                
                            }
                        }
                    }
                },
                { 
                    dataIndex: 'EJECUTIVO',
                    header: '',
                    width: 220,	
                    sortable: true,		
                    resizable: true,				
                    align: 'center',
                    editor:{
                        xtype: 'combo',                        
                        mode	: 'local',	
                        emptyText: 'Seleccionar',			
                        valueField : 'clave',
                        displayField : 'descripcion',	                      
                        typeAhead: true,
                        minChars : 1,
                        store : catalogoEjecutivosData,
                        forceSelection : true,	
                        triggerAction 	: 'all',	
                        fixed	: true,
                        lazyInit: false,
                        lazyRender: true,
                        tpl: '<tpl for=".">' +
                            '<tpl if="!Ext.isEmpty(loadMsg)">'+
                            '<div class="loading-indicator">{loadMsg}</div>'+
                            '</tpl>'+
                            '<tpl if="Ext.isEmpty(loadMsg)">'+
                            '<div class="x-combo-list-item">' +
                            '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                            '</div></tpl></tpl>',
                        listeners:{                          
                            select:{
                                fn:function (comboField, record, index) {
                                    comboField.fireEvent('blur'); 
                                }
                            }		
                        }  //fin listeners                                        
                    },//fin editor  
                    renderer:function(value,metadata,registro, rowIndex, colIndex){                            
                        if(!Ext.isEmpty(value)){
                            var dato = catalogoEjecutivosData.findExact("clave", value);
                            if (dato !== -1 ) {
                                var reg = catalogoEjecutivosData.getAt(dato);
                                value = reg.get('descripcion');
                                return NE.util.colorCampoEdit(value,metadata,registro);
                            }
                        }else{
                            return 'Seleccionar...';
                        }                                
                    }   
                }
                ],
                listeners: {                
                    beforeedit: function(e){
                        var record = e.record;                       
                        var campo= e.field;                                               
                        if (campo === 'MONTO' && record.data['AUTORIZACION']!=='M'  ){
                            e.cancel=true;
                        }                        
                    }
                    ,afteredit : function(e){
                        var record = e.record;                       
                        var campo= e.field;                         
                        if (campo === 'AUTORIZACION'  && record.data['AUTORIZACION']!=='M'  ){
                            record.data['MONTO']='';	
                        } 
                    }
                },               
                bbar: {
                    xtype: 'toolbar',
                    items: [
                        '->',  '-',
                        {
                            text: 'Guardar',   iconCls: 'icoGuardar',   id: 'idGuardar',
                            handler: function(boton, evento){                                
                                procesarGuardar();
                            }					
                        },	
                        {
                            text: 'Generar Archivo',   iconCls: 'icoXls',   id: 'btnGeneraPDF', 
                            handler: function(boton, evento){
                                boton.disable();
                                boton.setIconClass('loading-indicator');
                                   
                                Ext.Ajax.request({
                                url : '22forma2Ext01.data.jsp',
                                params : {
                                    informacion: 'GeneraCSV',
                                    cmbArea:Ext.getCmp('cmbArea').getValue(),
                                    icIf:Ext.getCmp('icIf1').getValue()
                                },
                                callback: procesarArchivos
                                });                                
                            }					
                        }			
                    ]
                }
            });
    
        
            var nombreDirector =jsonData.nombreDirector; 
            if(nombreDirector=="vacio"){
                Ext.getCmp('gridConsulDimamic').setTitle('Asignaci�n Ejecutivo al IF.');
            }else {
                Ext.getCmp('gridConsulDimamic').setTitle('Director: '+nombreDirector);
            }
      
            var mensajes1 = new Ext.Container({
                layout: 'table',
		id: 'mensajes1',				
		width:	900,
		heigth:	300,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		items: [		
                    { 
                        xtype:   'panel',  
                        width: 900,
			html: "* Los correos tendr�n que estar separados por ',' y no deber�n tener espacios en blanco. <br>"+
                            " * El Formato de captura de correo electr�nico deber� ser el siguiente: usuario@dominio.com,usuario2@dominio.com ",
                        style: 'margin:0 auto;',  
                        cls:'x-form-item',
                        align: 'center',
                        frame: true
                    }				
                ]
            });
           
           contenedorPrincipalCmp.remove(gridConsulDimamic);           
           contenedorPrincipalCmp.remove(mensajes1);
            
            contenedorPrincipalCmp.add(gridConsulDimamic);           
           contenedorPrincipalCmp.add(NE.util.getEspaciador(20));  
           contenedorPrincipalCmp.add(mensajes1);
           contenedorPrincipalCmp.doLayout(); 
            
            var fp = Ext.getCmp('forma');
            fp.el.unmask();
                                       
                             
            var procesarGuardar = function() {
                
                limpiar(); //limpia variables 
            
                var grid = Ext.getCmp('gridConsulDimamic');
                var store = grid.getStore();                  
                var modifiedRecords = store.getModifiedRecords();
                                
                 if( store.getModifiedRecords().length>0)  {
                 
                     Ext.each(modifiedRecords, function(record) {                     
                        clavesIF.push(record.data['CLAVE_INTER']); 
                        autorizacion.push(record.data['AUTORIZACION']);
                        if(record.data['AUTORIZACION']!=='M'){
                          monto.push('');   
                        }else {
                            monto.push(record.data['MONTO']); 
                        }
                        cotizacion.push(record.data['COTIZACION']); 
                        
                        
                        if(record.data['CORREO_ELECTRONICO']!==''){
                            //verifica que el correo este en el formato correcto
                            if(validaCorreos(record.data['CORREO_ELECTRONICO'])>0) {  
                               errorCorreo++;  
                            }else  {
                              correos.push(record.data['CORREO_ELECTRONICO']);   
                            }
                        }
                        
                        ejecutivo.push(record.data['EJECUTIVO']);                         
                    });
                       
                      if(errorCorreo>0) {
                            Ext.Msg.alert('Observaciones','Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
                      }else  {
                       
                       Ext.getCmp('idGuardar').setIconClass('loading-indicator'); 
                       Ext.getCmp('idGuardar').disable();
                       
                        Ext.Ajax.request({
                            url: '22forma2Ext01.data.jsp',
                            params: Ext.apply(fp.getForm().getValues(),{
                                informacion: 'guardar',
                                clavesIF:clavesIF,
                                autorizacion:autorizacion,
                                monto:monto,
                                cotizacion:cotizacion,
                                correos:correos,
                                ejecutivo:ejecutivo                        
                            }),
                            success : function(response) {
                                var infoR = Ext.util.JSON.decode(response.responseText);
                            
                                if (infoR.respuesta ==='OK' ){
                                    Ext.Msg.show({  title: 'Guardar', msg: 'Fue guardado con �xito', modal: true,
                                        icon: Ext.Msg.INFO, buttons: Ext.Msg.OK, fn: function (){
                                       
                                        Ext.getCmp('idGuardar').setIconClass('icoGuardar');  
                                        Ext.getCmp('idGuardar').enable();           
                                        }
                                    });				
                                } else {
                                    Ext.Msg.show({ 	title: 'Guardar',msg: 'Ocurrio un error.', modal: true,	icon: Ext.Msg.ERROR,	buttons: Ext.Msg.OK,
                                        fn: function (){                                        
                                            Ext.getCmp('idGuardar').setIconClass('icoGuardar');  
                                            Ext.getCmp('idGuardar').enable();                                     
                                        }
                                       
                                    });
                                }
                            }
                        });                               
                    
                    }
                    
                 }else  {
                    Ext.Msg.alert('Mensaje','No ha sido modificado ning�n registro.');	
                    
                    Ext.getCmp('idGuardar').setIconClass('icoGuardar');  
                    Ext.getCmp('idGuardar').enable(); 
                 }
            };
    
        }    
    }
    
    var catalogoArea = new Ext.data.JsonStore({
	id: 'catalogoAreaDataStore',
        root: 'registros',
	fields: ['clave', 'descripcion', 'loadMsg'],
	url: '22forma2Ext01.data.jsp',
	baseParams: { informacion: 'catalogoArea' },
	totalProperty: 'total',
        autoLoad: false,
	listeners: {	    
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
    
     var catalogoIFData = new Ext.data.JsonStore({
	id: 'catalogoIFDataStore',
        root: 'registros',
	fields: ['clave', 'descripcion', 'loadMsg'],
	url: '22forma2Ext01.data.jsp',
	baseParams: { informacion: 'catalogoIFData' },
	totalProperty: 'total',
        autoLoad: false,
	listeners: {	    
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
    
      var elementosForma = [   
    {
        xtype: 'combo',  
        name: 'comboArea',
        id: 'cmbArea',
        fieldLabel: 'Seleccionar �rea',
        mode: 'local',
        displayField : 'descripcion',
        valueField: 'clave',
        hiddenName: 'cmbArea',
        emptyText: 'Seleccione...',
        forceSelection: true,
        triggerAction: 'all',  
        typeAhead: true,  
        width: 345,
        minChars : 1,
        allowBlank: true,
        store : catalogoArea,
        tpl: '<tpl for=".">' +
            '<tpl if="!Ext.isEmpty(loadMsg)">'+
            '<div class="loading-indicator">{loadMsg}</div>'+
            '</tpl>'+
            '<tpl if="Ext.isEmpty(loadMsg)">'+
            '<div class="x-combo-list-item">' +
            '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
            '</div></tpl></tpl>',
            listeners: {
            select: {
                fn: function(combo) {
                    catalogoIFData.load({ params: { cmbArea:Ext.getCmp('cmbArea').getValue() } });
                    catalogoEjecutivosData.load({ params: { cmbArea:Ext.getCmp('cmbArea').getValue() } });                   
                    
                }// FIN fn
            }//FIN select
        }//FIN listeners
    },
    {
        xtype: 'combo',  
        name: 'icIf',
        id: 'icIf1',
        fieldLabel: 'Intermediario Financiero',
        mode: 'local',
        displayField : 'descripcion',
        valueField: 'clave',
        hiddenName: 'icIf',
        emptyText: 'Seleccione...',
        forceSelection: true,
        triggerAction: 'all',  
        typeAhead: true,  
        width: 345,
        minChars : 1,
        allowBlank: true,
        store : catalogoIFData,
        tpl: '<tpl for=".">' +
            '<tpl if="!Ext.isEmpty(loadMsg)">'+
            '<div class="loading-indicator">{loadMsg}</div>'+
            '</tpl>'+
            '<tpl if="Ext.isEmpty(loadMsg)">'+
            '<div class="x-combo-list-item">' +
            '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
            '</div></tpl></tpl>'
    }
    ];
    
    var fp = new Ext.form.FormPanel({	
        id: 'forma',
        width: 600, 
        title: '<center>Asignaci�n Ejecutivos al IF</center',
        frame: true, 
        titleCollapse: false,
        style: 'margin:0 auto;',
        bodyStyle: 'padding: 6px',
        labelWidth: 150,
        defaultType: 'textfield',
        defaults: { msgTarget: 'side',  anchor: '-20'},
        items: elementosForma,			
        monitorValid: true,
        buttons	:[{
            text: 'Consultar', 
            id: 'btnAConsultar',	
            iconCls: 'icoBuscar',
            formBind: true,
            handler: function() {	                
                var cmbArea = Ext.getCmp('cmbArea');
                if(Ext.isEmpty(cmbArea.getValue())) {
                    cmbArea.markInvalid('Este campo es obligatorio');
                    cmbArea.focus();
                    return;
                }
                   
                fp.el.mask('Cargando...', 'x-mask-loading');	
                                   
                    Ext.Ajax.request({
                        url : '22forma2Ext01.data.jsp',
			params : {
                            informacion: 'Consultar',
                            cmbArea:Ext.getCmp('cmbArea').getValue(),
                                icIf:Ext.getCmp('icIf1').getValue()
                            },
			    callback: procesarConsultaDinamica
			});                        
            }
        }, {
            text:'Limpiar', id:'btnLimpiar',
            iconCls: 'icoLimpiar',
            handler: function(boton, evento) {               
                 window.location = '22forma2Ext01.jsp?idMenu=22NAFIN22FORMA2';	
            }
        }]
    });

//----------------------------Contenedor Principal------------------------------
    var pnl = new Ext.Container({
        id:'contenedorPrincipal',
        applyTo	:'areaContenido',
        width: 949,
        style:'margin:0 auto;',
        items: [
            NE.util.getEspaciador(20),
            fp,
            NE.util.getEspaciador(20)            
        ]
    });

    catalogoArea.load();

});