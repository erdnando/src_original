//Ext.define('Ext.ux.grid.Column.RadioGroupColumn', {//Ext.ux.grid.ColumnHeaderGroup
Ext.ns('Ext.ux.grid');


	var recordsToSaveT=[] ;
	var recordsToSave=[] ;
	
	var limpiaRecordsToSave =  function() {  
		recordsToSaveT=[] ;
		recordsToSaveT=[] ;
	}
	
	var fields				=	[];
	var ic_solic_esp		=	[];
	var ic_estatus			=	[];
	var ig_tasa_md			=	[];
	var ig_tasa_spot		=	[];
	var ig_num_referencia=	[];
	var cg_causa_rechazo	=	[];
	var cg_tipo_solic		=	[];
	var ic_tasa				=	[];
			
	
	var limpiaSAVE_INFO =  function() {  
		fields				=	[];
		ic_solic_esp		=	[];
		ic_estatus			=	[];
		ig_tasa_md			=	[];
		ig_tasa_spot		=	[];
		ig_num_referencia=	[];
		cg_causa_rechazo	=	[];
		cg_tipo_solic		=	[];
		 ic_tasa				=	[];
		}
	
Ext.ux.grid.RadioGroupColumn =Ext.extend(Ext.grid.Column, {

	processEvent : function(name, e, grid, rowIndex, colIndex){
        if (name == 'mousedown') {
            var record = grid.store.getAt(rowIndex);
            record.set(this.dataIndex, !record.data[this.dataIndex]);
            return false; // Cancel row selection.
        } else {
            return Ext.grid.ActionColumn.superclass.processEvent.apply(this, arguments);
        }
    },
	  renderer : function(v, p, record){
        p.css += ' x-grid3-check-col-td'; 
        return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
    },
	 // Deprecate use as a plugin. Remove in 4.0
    init: Ext.emptyFn

});
// register ptype. Deprecate. Remove in 4.0
Ext.preg('radiocolumn', Ext.ux.grid.RadioGroupColumn);

// backwards compat. Remove in 4.0
Ext.grid.RadioGroupColumn = Ext.ux.grid.RadioGroupColumn;

// register Column xtype
Ext.grid.Column.types.radiocolumn = Ext.ux.grid.RadioGroupColumn;

	function setTipoSolic(valor, indice) {
		var grid = Ext.getCmp('grid');	
		var reg = grid.getStore().getAt(indice);
		if(valor == 1){
			reg.set('TIPO_COTIZACION', 'I');
		} else if(valor == 2){
			reg.set('TIPO_COTIZACION', 'F');
		}
	}	

Ext.onReady(function(){
//---VARIABLES GLOBALES
var _URL_='22forma03Ext.data.jsp';
	
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var gridConsulta = Ext.getCmp('grid');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			if(store.getTotalCount() > 0) {//////////Verifica que existan registros
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnGuardar').enable();//
				el.unmask();					
			} else {	
				Ext.getCmp('btnArchivoPDF').disable();//btnGuardar
				Ext.getCmp('btnGuardar').disable();//
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	function verTabla (grid, rowIndex, colIndex, item, event){
	//PLAN_PAGO
		var registro = grid.getStore().getAt(rowIndex);
		var ic_plan_de_pagos = registro.get('CONSULTAR');
		var intermediario =registro.get('IF');
		var estatus_solicitud =1;
		var origen = 2;
		var params=[	
			{		
				ic_plan_de_pagos:ic_plan_de_pagos,
				intermediario:intermediario,
				estatus_solicitud:estatus_solicitud,
				origen:origen
			}
		];
		cotizador('GENERAR_ARCH_CSV_PLAN_PAGOS',params[0]);
		
	}
	function verSolicitud(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		var ic_solic_esp = registro.get('CONSULTAR');
		var intermediario =registro.get('IF');
		var estatus_solicitud =1;
		var origen = 2;
		var opts=[
			{
				ic_solic_esp:ic_solic_esp,
				intermediario:intermediario,
				estatus_solicitud:estatus_solicitud,
				origen:origen
			}
		];

		var fieldsComun = [{ name: 'DESCRIPCION',mappin:0 },{ name: 'CONTENIDO',mapping:1 },{ name: 'TIPO_CONTENIDO',mapping: 2 }]
		var columnsComun =[
			{header:'Descripcion',	dataIndex: 	'DESCRIPCION',	sortable:true,	resizable: 	true,	width: 200,	hidden:false,hideable:	false,fixed:true,	align: 'left'	},
			{header:'Contenido',dataIndex:'CONTENIDO',sortable:true,	resizable:true,hidden:false,hideable:false,align:'right',renderer:renderContenido}
		]	;
		var datosCotizacion 			= new Ext.data.ArrayStore	({	storeId: 'datosCotizacionDataStore',		autoDestroy: true,fields: fieldsComun,idIndex:0,autoLoad:false});	
		var datosEjecuticoNafin 	= new Ext.data.ArrayStore	({storeId: 'datosEjecuticoNafinDataStore',	autoDestroy: true,fields: fieldsComun,idIndex:0,autoLoad:false});
		var datosIntermediario		= new Ext.data.ArrayStore	({storeId: 'datosIntermediarioF_ADataStore',	autoDestroy: true,fields: fieldsComun,idIndex:0,autoLoad:false});
		var datosCaracteristicas 	= new Ext.data.ArrayStore	({storeId: 'datosCaracteristicasDataStore',	autoDestroy: true,fields: fieldsComun,idIndex:0,autoLoad:false});
		var detallesDisposicion 	= new Ext.data.ArrayStore	({storeId: 'datosDisposicionDataStore',		autoDestroy: true,fields: [{ name:'NUM',mapping: 0 },{ name:'MONTO',mapping: 1 },{ name:'FECHA_DISPOSICION',mapping: 2 },{ name: 'FECHA_VENCIMIENTO',mapping: 3 }],idIndex:0,autoLoad:false});
		var datosPlanPagos			= new Ext.data.ArrayStore	({storeId:'datosPlanPagosDataStore',			autoDestroy: true,fields: [{ name:'NUM_PAGO',mapping: 0 },{ name:'FECHA_PAGO',mapping: 1 },{ name:'MONTO',mapping: 2 }],idIndex:0,autoLoad:false});
		var datosDetalles 			= new Ext.data.ArrayStore	({storeId: 'datosDetallesDataStore',			autoDestroy: true,fields: fieldsComun,idIndex:0,autoLoad:false});	
		var datosTasaIndicativa		= new Ext.data.ArrayStore	({storeId: 'datosTasaIndicativaDataStore',			autoDestroy: true,fields: fieldsComun,idIndex:0,autoLoad:false});	
		var datosNotas					= new Ext.data.ArrayStore	({storeId: 'datosNotasDataStore',			autoDestroy: true,fields: fieldsComun,idIndex:0,autoLoad:false});	
		var datosObservaciones		= new Ext.data.ArrayStore	({storeId: 'datosNotasDataStore',			autoDestroy: true,fields: fieldsComun,idIndex:0,autoLoad:false});	
			var gridDatosCotizacion = new Ext.grid.EditorGridPanel({
				store:datosCotizacion,xtype:'grid',id:'datosCotizacion',
				title:'<div align=center>Datos de la Cotizaci�n</div>',
				stripeRows:true,loadMask:true,
				width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight: 	true,				hideHeaders: 	true,
				region:'border',style: {borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns:columnsComun
			});
			var girdDatosEjecutiovoNafin = new Ext.grid.EditorGridPanel({
				store:datosEjecuticoNafin,	xtype:'grid',id:'datosEjecuticoNafin',
				title:			'<div align=left>Datos del Ejecutivo Nafin</div>',
				stripeRows:true,loadMask: true,width: '100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight:true,hideHeaders: 	true,	region:'center',
				style: {	borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns:columnsComun
			});
			var gridDatosIntermediarioF_A =	new Ext.grid.EditorGridPanel({
				store: 			datosIntermediario,	xtype: 			'grid',
				id:				'datosIntermediarioF_A',
				title:'<div align=left>Datos del Intermediario Financiero/Acreditado</div>',
				stripeRows: 	true,loadMask: true,	width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight: 	true,				hideHeaders: 	true,
				region: 			'left',	style: {	borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns:columnsComun
			});
			var gridCaracteristicasOperacion = new Ext.grid.EditorGridPanel({
				store: 			datosCaracteristicas,	xtype: 			'grid',
				id:				'datosCaracteristicas_',
				title:'<div align=left>Caracter�sticas de la Operaci�n</div>',
				stripeRows: 	true,loadMask: true,	width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight: 	true,				hideHeaders: 	true,
				region: 			'center',	style: {	borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns:columnsComun
			});
			var gridDetallesDisposicion	=	new Ext.grid.EditorGridPanel({
				store: 			detallesDisposicion,	xtype: 			'grid',
				id:				'detallesDisposicion',
				title:'<div align=left>Detalle de Disposiciones</div>',
				stripeRows: 	true,loadMask: true,	width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight: 	true,				hideHeaders: 	false,
				region: 			'center',	style: {	borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns: [
					{	header:'N�mero de Disposici�n',tooltip:'N�mero de Disposici�n',dataIndex:'NUM',sortable:true,resizable:true,width:200,hidden:false,hideable:false,fixed:true,align:'centre'},
					{	header: 		'Monto',tooltip: 	'Monto',dataIndex: 	'MONTO',sortable: 	true,
						resizable: 	true,	hidden: 		false,hideable:	false,align:		'center',	renderer: 	renderContenido
					},
					{	header: 		'Fecha de Disposici�n',tooltip: 	'Fecha de Disposici�n',dataIndex: 	'FECHA_DISPOSICION',sortable: 	true,
						resizable: 	true,	hidden: 		false,hideable:	false,align:		'center',	renderer: 	renderContenido
					},
					{	header: 		'Fecha de Vencimiento',tooltip: 	'Fecha de Vencimiento',dataIndex: 	'FECHA_VENCIMIENTO',sortable: 	true,
						resizable: 	true,	hidden: 		false,hideable:	false,align:		'cener',	renderer: 	renderContenido
					}
				]
			});
			var gridPlanPagos = new Ext.grid.EditorGridPanel({
				store:datosPlanPagos, xtype: 'grid', id:'datosPlanPagos',
				title:	'<div align=left>Plan de Pagos Capital</div>',
				stripeRows: 	true,loadMask: true,	width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight: 	true,				hideHeaders: 	false,
				region: 			'center',	style: {	borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns: [
					{	header: 		'N�mero de Pago',	tooltip: 	'N�mero de Pago',	dataIndex: 	'NUM_PAGO',
						sortable: 	true,	resizable: 	true,	width: 		200,	hidden: 		false,hideable:	false,
						fixed:		true,	align: 		'centre'
					},
					{	header: 		'Fecha de Pago',tooltip: 	'Fecha de pago',dataIndex: 	'FECHA_PAGO',sortable: 	true,
						resizable: 	true,	hidden: 		false,hideable:	false,align:		'center',	renderer: 	renderContenido
					},
					{	header: 		'Monto a Pagar',tooltip: 	'Monto a Pagar',dataIndex: 	'MONTO',sortable: 	true,
						resizable: 	true,	hidden: 		false,hideable:	false,align:		'center',	renderer: 	renderContenido
					}
				]
			});
			var gridDatosDetalles = new Ext.grid.EditorGridPanel({
				store: 			datosDetalles,	xtype: 			'grid',
				id:				'datosDetalles_',
				//title:'<div align=left>Datos Detalles</div>',
				stripeRows: 	true,loadMask: true,	width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight: 	true,				hideHeaders: 	true,
				region: 			'center',	style: {	borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns:[{header:'Contenido',	dataIndex: 	'CONTENIDO',	sortable:true,	resizable: 	true,	width: 600,	hidden:false,hideable:	false,fixed:true,	align: 'center'	}]
			});
			var gridDatosTasaIndicativa = new Ext.grid.EditorGridPanel({
				store: 			datosTasaIndicativa,	xtype: 			'grid',
				id:				'datosTasaIndicativa',
				//title:'<div align=left>Datos Detalles</div>',
				stripeRows: 	true,loadMask: true,	width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight: 	true,				hideHeaders: 	true,
				region: 			'center',	style: {	borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns:[{header:'Contenido',	dataIndex: 	'CONTENIDO',	sortable:true,	resizable: 	true,	width: 600,	hidden:false,hideable:	false,fixed:true,	align: 'center'	}]
			});
			var gridNotas  = new Ext.grid.EditorGridPanel({
				store: 			datosNotas,	xtype: 			'grid',
				id:				'datosNotas',
				//title:'<div align=left>Datos Detalles</div>',
				stripeRows: 	true,loadMask: true,	width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight: 	true,				hideHeaders: 	true,
				region: 			'center',	style: {	borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns:[{header:'Contenido',	dataIndex: 	'CONTENIDO',	sortable:true,	resizable: 	true,	width: 600,	hidden:false,hideable:	false,fixed:true,	align: 'center'	}]//columnsComun
			});
			var gridTasaConfirmada  = new Ext.grid.EditorGridPanel({
				store: 			datosNotas,	xtype: 			'grid',
				id:				'datosTasaConfirmada',
				//title:'<div align=left>Datos Detalles</div>',
				stripeRows: 	true,loadMask: true,	width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight: 	true,				hideHeaders: 	false,
				region: 			'center',	style: {	borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns:[
					{header:'Descripcion',	dataIndex: 	'DESCRIPCION',	sortable:true,	resizable: 	true,width: 300,		hidden:false,hideable:	false,fixed:true,	align: 'left'	},
					{header:'Contenido',	dataIndex: 	'CONTENIDO',	sortable:true,	resizable: 	true,		hidden:false,hideable:	false,fixed:true,	align: 'center'	}
				]//columnsComun
			});
			var gridObservaciones   = new Ext.grid.EditorGridPanel({
				store: 			datosObservaciones,	xtype: 			'grid',
				id:				'datosObservaciones',
				title:'<div align=left>Observaciones</div>',
				stripeRows: 	true,loadMask: true,	width: 		'100%', // Como no se puede utilizar anchor para expresar un porcentaje...
				autoHeight: 	true,				hideHeaders: 	true,
				region: 			'center',	style: {	borderWidth: 1,width: '90%'},
				viewConfig: {	autoFill: 		true,	forceFit:true,scrollOffset:	0	},
				columns:[
					//{header:'Descripcion',	dataIndex: 	'DESCRIPCION',	sortable:true,	resizable: 	true,width: 300,		hidden:false,hideable:	false,fixed:true,	align: 'left'	},
					{header:'Contenido',	dataIndex: 	'CONTENIDO',	sortable:true,	resizable: 	true,	width: 600,	hidden:false,hideable:	false,fixed:true,	align: 'center'	}
				]//columnsComun
			});
		Ext.Ajax.request({
			url	:	_URL_,
			params	:Ext.apply(opts[0],	{
				informacion	:	'Cotizador.solicitud_detalles'
			}),
			success:function(form, action, resp) {
				var data=Ext.decode(form.responseText);
				var cifrasControlData = Ext.StoreMgr.key('datosCotizacionDataStore');
				cifrasControlData.loadData(data.cifrasDatosCotizacionDataArray);

				var datosEjecutivo = Ext.StoreMgr.key('datosEjecuticoNafinDataStore');
				datosEjecutivo.loadData(data.cifrasDatosEjecutivoNafinDataArray);
				
				var cifrasDatosIntermediarioF_ADataArray = Ext.StoreMgr.key('datosIntermediarioF_ADataStore');
				cifrasDatosIntermediarioF_ADataArray.loadData(data.cifrasDatosIntermediarioF_ADataArray);
				
				var cifrasCaracteristicasDataArray =Ext.StoreMgr.key('datosCaracteristicasDataStore');
				cifrasCaracteristicasDataArray.loadData(data.cifrasCaracteristicasDataArray);
				
				var cifrasDetallesDisposicionDataArray = Ext.StoreMgr.key('datosDisposicionDataStore');
				cifrasDetallesDisposicionDataArray.loadData(data.cifrasDetallesDisposicionDataArray);
				
				var cifrasPlanPagosDataArray = Ext.StoreMgr.key('datosPlanPagosDataStore');
				cifrasPlanPagosDataArray.loadData(data.cifrasPlanPagosDataArray);
				if(data.cifrasPlanPagosDataArray.length==0){gridPlanPagos.hide();}
				
				var cifrasDetallesDataArray = Ext.StoreMgr.key('datosDetallesDataStore');
				cifrasDetallesDataArray.loadData(data.cifrasDetallesDataArray);
				if(data.cifrasDetallesDataArray.length==0){gridDatosDetalles.hide();}
				
				var cifrasTasaIndicativaDataArray = Ext.StoreMgr.key('datosTasaIndicativaDataStore');
				cifrasTasaIndicativaDataArray.loadData(data.cifrasTasaIndicativaDataArray);
				if(data.cifrasTasaIndicativaDataArray.length==0){gridDatosTasaIndicativa.hide();}
				
				var cifrasNotaDataArray = Ext.StoreMgr.key('datosNotasDataStore');
				cifrasNotaDataArray.loadData(data.cifrasNotaDataArray);
				if(data.cifrasTasaIndicativaDataArray.length==0){gridNotas.hide();}
				
				var cifrasTasaConfirmadaDataArray = Ext.StoreMgr.key('datosNotasDataStore');
				cifrasTasaConfirmadaDataArray.loadData(data.cifrasTasaConfirmadaDataArray);
				var colModel = Ext.getCmp('datosTasaConfirmada').getColumnModel( );
				colModel.setColumnHeader( 1, data.headerFecha );
				colModel.setColumnHeader( 0, "TASA CONFIRMADA" );
				if(data.cifrasTasaConfirmadaDataArray.length==0){gridTasaConfirmada.hide();}
				
				var cifrasObservacionesDataArray = Ext.StoreMgr.key('datosNotasDataStore');
				cifrasObservacionesDataArray.loadData(data.cifrasObservacionesDataArray);
				if(data.cifrasObservacionesDataArray.length==0){gridObservaciones.hide();}
				
				new Ext.Window({
					title: '.::N@fin Electr�nico::Cotizador::.',
					id : 'win',	closable:false,	width:600,autoHeight : true,	border:false,
					modal: true,plain:true,resizable : false,
					items	:[	
						gridDatosCotizacion,NE.util.getEspaciador(5),
						girdDatosEjecutiovoNafin,NE.util.getEspaciador(5),
						gridDatosIntermediarioF_A,NE.util.getEspaciador(5),
						gridCaracteristicasOperacion,NE.util.getEspaciador(5),
						gridDetallesDisposicion,NE.util.getEspaciador(5),
						gridPlanPagos,gridDatosDetalles,gridDatosTasaIndicativa,
						gridNotas,gridTasaConfirmada,gridObservaciones
					],
					buttons: [
						{
							text:'Generar Archivo PDF',iconCls: 'icoPdf',
							handler	:function(){
							var opts=[	
								{ic_solic_esp:ic_solic_esp,intermediario:intermediario,estatus_solicitud:estatus_solicitud,origen:origen}
							];
								cotizador('SOLICITUD_ARCH_PDF',opts[0]);
								 Ext.getCmp('win').destroy();
							}
						},
						{
							text: 'Cerrar',iconCls: 'icoCancelar',
							handler: function(){

		// Se actualiza el grid de consulta
		var ic_solic_esp      = [];
		var ic_estatus        = [];
		var ig_tasa_md        = [];
		var ig_tasa_spot      = [];
		var ig_num_referencia = [];
		var cg_causa_rechazo  = [];
		var cg_tipo_solic     = [];
		var ic_tasa           = [];

		ic_solic_esp.push(      registro.get('CONSULTAR')      );
		ic_estatus.push(        '4'                            );
		ig_tasa_md.push(        registro.get('TASA_MD')        );
		ig_tasa_spot.push(      registro.get('TASA_48HRS')     );
		ig_num_referencia.push( registro.get('NUM_REFERENCIA') );
		cg_causa_rechazo.push(  registro.get('CUASA_RECHAZO')  );
		cg_tipo_solic.push(     registro.get('TIPO_COTIZACION'));
		ic_tasa.push(           registro.get('IC_TASA')        );

		Ext.Ajax.request({
			url:_URL_,
			params: {
				informacion:       'Cotizador.Procesar_guardar_cambios',
				ic_solic_esp:      ic_solic_esp,
				ic_estatus:        ic_estatus,
				ig_tasa_md:        ig_tasa_md,
				ig_tasa_spot:      ig_tasa_spot,
				ig_num_referencia: ig_num_referencia,
				cg_causa_rechazo:  cg_causa_rechazo,
				cg_tipo_solic:     cg_tipo_solic,
				ic_tasa:           ic_tasa
			},
			success: function(response){
				/*Ext.Msg.alert('','OK',function(btn,text){
					cotizador('INICIALIZACION',null)
				});*/
			}
		});


								cotizador('INICIALIZACION',null);
								Ext.getCmp('win').destroy();
							}
						}
					]
				}).show();
			}
		});
	}
	
	function verObservaciones(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		var num_solicitud = registro.get('NUM_SOLIC');
		var ic_solic_esp = registro.get('CONSULTAR');
		var observaciones = registro.get('OBSERVACIONES'); 
		var intermediario =registro.get('IF');
		
			
		
		var win =new Ext.Window({
			title	: '.::Nafinet::.',width	: 500,height: 230,modal	: true,closable:true,
			style: 'margin:0 auto;',bodyStyle: 'padding: 6px',defaults: {msgTarget: 'side',anchor: '-20'},
			items	:[
				{	xtype: 'displayfield',fieldLabel: 'N�mero de Solicitud',align: 'center',
					labelStyle: 'font-weight:bold;',	value: '<br>N�mero de Solicitud: '+num_solicitud+'<br><br>'
				},{xtype: 'displayfield',fieldLabel: 'N�mero de Solicitud',align: 'center',
					labelStyle: 'font-weight:bold;',	value: 'Observaciones:'
				},{xtype: 'textarea',id: 'txtObservaciones',	align: 'center',width	: '98%',	height: 100,value: observaciones }
			],
			buttons: [
				{
					text:'Guardar',iconCls: 'icoGuardar',
					handler	:function(){
						var data=[Ext.getCmp('txtObservaciones').getValue(),ic_solic_esp]
						cotizador('GUARDAR_OBSERVACIONES',data);
						cotizador('INICIALIZACION',null);
						win.destroy();
					}
				},
				{
					text: 'Cerrar',iconCls: 'icoCancelar',	handler: function(){	win.destroy();	}
				}
			]
		}).show();
	}

//______________________MAQUINA DE ESTADO_______________________________________	
	var procesarCotizador = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cotizador(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cotizador(resp.estadoSiguiente,resp);
			}
		}else{
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
		}
	}
	var cotizador = function (	estado,	respuesta	){
		if(	estado =='INICIALIZACION'	){
		
			limpiaRecordsToSave();
			limpiaSAVE_INFO();
			gridConsulta.getStore().commitChanges();
											
			Ext.Ajax.request({
				url	:	_URL_,
				params	:	{
					informacion	:	'Cotizador.inicializacion'
				},
				callback	:	procesarCotizador
			});
		}else if(	estado == 'MOSTRAR_VENTANA'	){//Ventana de aviso de solicitudes nuevas
			//Mostrar la ventana COTIZA--->(1)
				Ext.MessageBox.alert('Cotizador',respuesta.mensaje,
					function(){ 					
					cotizador('MOSTRAR_GRID_CONSULTA',null);						
				});
			
		}else if(	estado == 'MOSTRAR_GRID_CONSULTA'	){//Grid de las solicitudes
			//---Inicializar el grid 
			consultaData.load();		
		}else if(	estado == 'GENERAR_ARCHIVO'	){//Archivo del Grid
			Ext.Ajax.request({
				url	:	_URL_,
				params	:	{
					informacion	:	'Cotizador.GenerarPDF'
				},success: function(response){
					var btnGenerar = Ext.getCmp('btnArchivoPDF');
					btnGenerar.setIconClass('icoPdf');
					btnGenerar.enable();
				},
				callback	:	procesarDescargaArchivos
			});
		}else if (	estado 	==	'GUARDAR_OBSERVACIONES'){
			Ext.Ajax.request({
				url	:	_URL_,
				params	:	{
					informacion		:	'Cotizador.Guardar_Observaciones',
					ic_solic_esp	:	respuesta[1],
					observaciones	:	respuesta[0]
				},
				callback	:	procesarCotizador
			});
		}else if(	estado	==	'SOLICITUD_ARCH_PDF'){//Archivo de la ventana detalles Solicitud
				Ext.Ajax.request({
				url	:	_URL_,
				params	:Ext.apply(respuesta,	{
					informacion		:	'Cotizador.Solicitud_arch_pdf'
				}),
				callback	:	procesarDescargaArchivos
			});
		}else if(	estado	==	'GENERAR_ARCH_CSV_PLAN_PAGOS'	){
			Ext.Ajax.request({
				url	:	_URL_,
				params	:Ext.apply(respuesta,	{
					informacion		:	'Cotizador.Generar_archcsv_plan_pagos'
				}),
				callback	:	procesarDescargaArchivos
			});
		}else if (	estado	==	'SHOW_INF_TO_SAVE'){
			var cifrasPreGuardarDataArray = Ext.StoreMgr.key('datosPreGuardarDataStore');
				cifrasPreGuardarDataArray.loadData(respuesta);
			var win = Ext.getCmp('winPreGuardar');
			win.show();
			Ext.getCmp("butonConfirmar").show();
			
		}else if (	estado	==	'SAVE_INFO'){
			
			var data =(respuesta.reader.arrayData);
			
			limpiaSAVE_INFO();			
			
			for(var i = 0; i<data.length; i++){
				 ic_solic_esp.push( respuesta.data.items[i].data.CONSULTAR);
				 ic_estatus.push( respuesta.data.items[i].data.ESTATUS);
				 ig_tasa_md.push( respuesta.data.items[i].data.TASA_MD);
				 ig_tasa_spot.push( respuesta.data.items[i].data.TASA_48HRS);
				 ig_num_referencia.push( respuesta.data.items[i].data.NUM_REFERENCIA);
				 cg_causa_rechazo.push( respuesta.data.items[i].data.CUASA_RECHAZO);
				 cg_tipo_solic.push( respuesta.data.items[i].data.TIPO_COTIZACION);
				 ic_tasa.push( respuesta.data.items[i].data.IC_TASA);
			}
				Ext.Ajax.request({ 		// step 5
						url:_URL_,
						params :{
							informacion:'Cotizador.Procesar_guardar_cambios',
							ic_solic_esp:ic_solic_esp,
							ic_estatus:ic_estatus,
							ig_tasa_md:ig_tasa_md,
							ig_tasa_spot:ig_tasa_spot,
							ig_num_referencia:ig_num_referencia,
							cg_causa_rechazo:cg_causa_rechazo,
							cg_tipo_solic:cg_tipo_solic,
							ic_tasa:ic_tasa				
						},
						scope:this,
						callback: procesarGuardaCambios
						/*success : function(response) { //step 6							
							Ext.Msg.alert('','La operaci�n se realiz� con �xito',function(btn,text){								
								var win = Ext.getCmp('winPreGuardar');
								win.show();
								 Ext.getCmp("butonConfirmar").hide();
							});
						}*/
					});	
			
		}
	
	}

	var procesarGuardaCambios = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var exito = Ext.util.JSON.decode(response.responseText).ok;
			if(exito == true){
				Ext.Msg.alert('Mensaje...','La operaci�n se realiz� con �xito',function(btn,text){
					var win = Ext.getCmp('winPreGuardar');
					win.show();
					 Ext.getCmp("butonConfirmar").hide();
				});
			} else{
				Ext.Msg.alert('Error...','Ocurri� un error al guardar los datos');
			}
		}
	}
//----------------------------Stores--------------------------------------------
//------------------------------------------------------------------------------

//PARA LOS GRIDS GENERADOS EN LA VENTANA SOLICITUD
	var renderContenido = function(value, metaData, record, rowIndex, colIndex, store) {
		if(        record.get('TIPO_CONTENIDO') == 'NUMERO_ENTERO' ){
			metaData.style += 'text-align: left !important';
		} else if( record.get('TIPO_CONTENIDO') == 'FECHA' 		  ){
			metaData.style += 'text-align: left  !important';
		} else if( record.get('TIPO_CONTENIDO') == 'TEXTO' 		  ){
			metaData.style += 'text-align: left  !important';
		}
		return value;
	}
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',url : _URL_,
		baseParams: {informacion: 'Cotizador.Consultar'},		
		fields: [
			{	name:	'IC_SOLIC_ESP'},
			{	name: 'FECHA'},
			{	name: 'NUM_SOLIC'},	
			{	name: 'REQ_AUT'},
			{	name: 'IF'},	
			{	name: 'MONEDA'},
			{	name: 'MONTO_REQUERIDO'},
			{	name: 'PLAZO_OPER'},	
			{	name: 'ESTATUS'},
			//OTROS
			{	name:	'IC_TASA'},
			{	name:	'TASA_MD'},
			{	name: 'TASA_48HRS'},
			{	name: 'TASA_INDICATIVA'},
			{	name: 'NUM_REFERENCIA'},
			{	name: 'CUASA_RECHAZO'},
			{	name: 'TIPO_COTIZACION'},
			{	name: 'OBSERVACIONES'},
			{	name: 'PLAN_PAGO'},
			{	name: 'CONSULTAR'},
			{	name: 'BANDERA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	})
	

//------------------------------------------------------------------------------
//---------------------Componentes visuales-----------------------------------(2)
//------------------------------------------------------------------------------

//------>>>>Ventana Cotiza<<<<<<--------------------
	var ventanaCotiza = new Ext.Window({//VENTANA INICIAL (AVISO))
		id	:	'ventanaCotiza',name	:	'avisoCotizador',title	:	'Cotizador',
		width:480,autoScroll:true,height:130,border:false,modal: true,	plain:true,
		 buttons: [
			 {
				  text: 'Acepter', iconCls: 'aceptar',
				  handler: function(){
						ventanaCotiza.hide();
						fp.show();
						cotizador('MOSTRAR_GRID_CONSULTA',null);
				  }
			 }
		 ]
	});
	var consultaDataPreGuardar = new Ext.data.ArrayStore({
		storeId:     'datosPreGuardarDataStore',
		idIndex:     0,
		autoDestroy: true,
		autoLoad:    false,
		fields: [
			{name: 'IC_SOLIC_ESP',    mapping: 0},
			{name: 'FECHA',           mapping: 1},
			{name: 'NUM_SOLIC',       mapping: 2},
			{name: 'REQ_AUT',         mapping: 3},
			{name: 'IF',              mapping: 4},
			{name: 'MONEDA',          mapping: 5},
			{name: 'MONTO_REQUERIDO', mapping: 6},
			{name: 'PLAZO_OPER',      mapping: 7},
			{name: 'ESTATUS',         mapping: 8},
			//OTROS
			{name: 'TASA_MD',         mapping: 9},
			{name: 'TASA_48HRS',      mapping: 10},
			{name: 'TASA_INDICATIVA', mapping: 11},
			{name: 'IC_TASA',         mapping: 12},
			{name: 'NUM_REFERENCIA',  mapping: 13},
			{name: 'CUASA_RECHAZO',   mapping: 14},
			{name: 'TIPO_COTIZACION', mapping: 15},
			{name: 'OBSERVACIONES',   mapping: 16},
			{name: 'PLAN_PAGO',       mapping: 17},
			{name: 'CONSULTAR',       mapping: 18}
		]
	});

	var girdPreGuardar = new Ext.grid.EditorGridPanel({
	store:   consultaDataPreGuardar,
	id:      'gridPre',
	margins: '20 0 0 0',
	style:   'margin:0 auto;',
	columns: [{
		header:       'Fecha de solicitud',
		tooltip:      'Fecha de solicitud',
		dataIndex:    'FECHA',
		sortable:     true,
		width:        100,
		resizable:    true,
		align:        'center',
		menuDisabled: false,
		hideable:     false
	},{
		header:       'N�mero de Solicitud',
		tooltip:      'N�mero de<br>Solicitud',
		dataIndex:    'NUM_SOLIC',
		sortable:     true,
		width:        100,
		resizable:    true,
		align:        'center',
		hideable:     false
	},{
		header:       'Requiere<br>autorizaci�n<br>del<br>ejecutivo',
		tooltip:      'Requiere autorizaci�n del ejecutivo',
		dataIndex:    'REQ_AUT',
		sortable:     true,
		width:        80,
		resizable:    true,
		align:        'center',
		hideable:     false
	},{
		header:       'Intermediario<br>financiero',
		tooltip:      'Intermediario financiero',
		dataIndex:    'IF',
		sortable:     true,
		width:        350,
		resizable:    true,
		align:        'left',
		hideable:     false
	},{
		header:       'Moneda',
		tooltip:      'Moneda',
		dataIndex:    'MONEDA',
		sortable:     true,
		width:        150,
		resizable:    true,
		align:        'center',
		hideable:     false
	},{
		header:       'Monto Total<br>Requerido',
		tooltip:      'Monto Total Requerido',
		dataIndex:    'MONTO_REQUERIDO',
		sortable:     true,
		width:        150,
		resizable:    true,
		align:        'right',
		renderer:     function(v){
			return    '<div align=rigth>'+Ext.util.Format.number(v,'$0,0.00');
		},hideable:   false
	},{
		header:       'Plazo de<br>la<br>Operaci�n',
		tooltip:      'Plazo de la Operaci�n',
		dataIndex:    'PLAZO_OPER',
		sortable:     true,
		width:        100,
		resizable:    true,
		align:        'center',
		hideable:     false,
		renderer:     function(v){
			return v + ' d�as'
		}
	},{
		header:       'Estatus',
		tooltip:      'Estatus',
		dataIndex:    'ESTATUS',
		sortable:     true,
		width:        150,
		resizable:    true,
		align:        'center',
		hideable:     false,
		renderer:     function(v){
			if(v==2){
				return 'Cotizada';
			}
			if(v==3){
				return 'Rechazada';
			}
		}
	},{
		header:       'Tasa mismo d�a',
		tootip:       'Tasa mismo d�a',
		dataIndex:    'TASA_MD',
		align:        'center',
		renderer:     function(value,metadata,registro){
			var tasaIndicativa = registro.get('TASA_INDICATIVA');
			return '<b>'+tasaIndicativa+' '+value+'</b>';
		},
		hideable:     false
	},{
		header:       'Tasa 48 horas',
		tootip:       'Tasa 48 horas',
		dataIndex:    'TASA_48HRS',
		align:        'center',
		hideable:     false,
		renderer:     function(value,metadata,registro){
			var tasaIndicativa = registro.get('TASA_INDICATIVA');
			return '<b>'+tasaIndicativa+' '+value+'</b>';
		}
	},{
		header:       'Causas de<br>rechazo',
		tootip:       'Causas de rechazo',
		dataIndex:    'CUASA_RECHAZO',
		align:        'center',
		hideable:     false
	},{
		header:       'Tipo de<br>Cotizacion',
		tootip:       'Tipo de Cotizaci�n',
		dataIndex:    'TIPO_COTIZACION',
		align:        'center',
		hideable:     false,
		renderer:     function(v,params,record,row,col){
			if (v == 'F'){
				return 'En Firme';
			}else{
				return 'Indicativa';
			}
		}
	}],
	displayInfo:      true,
	emptyMsg:         'No hay registros.',
	loadMask:         true,
	stripeRows:       true,
	height:           300,
	width:            780,
	align:            'center',
	shadow:           true,
	frame:            true
});

	var ventanaPreGuardar = new Ext.Window({//PRE-GUARDAR //girdPreGuardar
		id:'winPreGuardar',
		width:800,
		autoScroll:true,
		closable: false,
		height:400,
		border:true,
		modal: true,	
		plain:true,
		items:[girdPreGuardar],
		buttons: [
			{
				text:'Regresar',iconCls:'icoRegresar',
				 handler: function(){
						gridConsulta.getStore().commitChanges();
						ventanaPreGuardar.hide();
						cotizador('INICIALIZACION',null);
				  }
			},
			{
				text: 'Confirmar',
				iconCls: 'aceptar',
				id:'butonConfirmar',
				handler: function(){
					ventanaPreGuardar.hide();
					var datos = girdPreGuardar.getStore();
					cotizador('SAVE_INFO',datos);
				}
			}
		 ]
	});
//--------------------------------gridConsulta----------------------------------	
var grupo = new Ext.ux.grid.ColumnHeaderGroup({
  rows: [
    [
		 {header		:'Fecha de solicitud', colspan: 1, align: 'center',locked: true},
		 {header		:'N�mero de<br>Solicitud', colspan: 1, align: 'center'},
		 {header		:'Requiere<br>autorizaci�n<br>del<br>ejecutivo', colspan: 1, align: 'center'},
		 {header		:'Intermediario<br>financiero', colspan: 1, align: 'center'},
		 {header		:'Moneda', colspan: 1, align: 'center'},
		 {header		:'Monto Total<br>Requerido', colspan: 1, align: 'center'},
		 {header		:'Plazo de<br>la<br>Operaci�n', colspan: 1, align: 'center'},
		 {header		:'Estatus', colspan: 1, align: 'center'},
		 {header		:'Tasa mismo d�a', colspan: 2, align: 'center'},
		 {header		:'Tasa 48 horas', colspan: 2, align: 'center'},
		 {header		:'Causas de rechazo', colspan: 1, align: 'center'},
		 {header		:'Tipo de<br>Cotizacion', colspan: 1, align: 'center'},
		 {header		: 'Observaciones ', colspan: 1, align: 'center'},
		 {header		: 'Plan de<br>Pago ', colspan: 1, align: 'center'},
		 {header		: 'Consultar', colspan: 1, align: 'center'}
    ]
  ]
});

var fieldsComun = [{ name: 'CLAVE',mappin:0 },{ name: 'CONTENIDO',mapping:1 }]


	var catalogoEstatusAsignar = new Ext.data.JsonStore({
		id: 'catalogoEstatusAsignar',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '22forma03Ext.data.jsp', 
		baseParams: {
			informacion: 'catalogoEstatusAsignar'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	

	//para la creacion del combo en el grid
	var comboEstatusAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catalogoEstatusAsignar
	});

	
	Ext.util.Format.comboRenderer = function(comboEstatusAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var valor = registro.data['ESTATUS'];	
			
			if(valor ==4 && value !=''){
				var record = comboEstatusAsignar.findRecord(comboEstatusAsignar.valueField, value);
				return record ? record.get(comboEstatusAsignar.displayField) : comboEstatusAsignar.valueNotFoundText;
			}			
			if(valor ==4 && value =='')  {
				return 'No modificar estatus';
			}else if(valor !=4 && value =='')  {			
            return 'Favor de consultar Solicitud';
         }
			
		}
	}	
	
var gridConsulta = new Ext.grid.EditorGridPanel({	
	store				:consultaData,
	id					:'grid',
	margins			:'20 0 0 0',		
	style				:'margin:0 auto;',
	clicksToEdit: 1,	
	height: 600,
	width: 900,
	plugins: grupo,
	columns			:[
		{
			tooltip		:'Fecha de solicitud',
			dataIndex	:'FECHA',
			sortable	:true,
			width		:100,	
			resizable	:true,
			align		:'center',
			menuDisabled: false,
			hideable:	false
		},
		{
			tooltip		:'N�mero de Solicitud',
			dataIndex	:'NUM_SOLIC',
			sortable	:true,
			width		:100,			
			resizable	:true,
			align			:'center',
			hideable:	false
		},
		{
			tooltip		:'Requiere autorizaci�n del ejecutivo',
			dataIndex	:'REQ_AUT',
			sortable	:true,
			width		:100,			
			resizable	:true,
			align		:'center',
			hideable:	false
		},
		{
			tooltip		:'Intermediario financiero',
			dataIndex	:'IF',
			sortable	:true,
			width		:300,			
			resizable	:true,
			align		:'left',
			hideable:	false
		},
		{
			tooltip		:'Moneda',
			dataIndex	:'MONEDA',
			sortable	:true,
			width		:150,			
			resizable	:true,
			align		:'center',
			hideable:	false
		},
		{
			tooltip		:'Monto Total Requerido',
			dataIndex	:'MONTO_REQUERIDO',
			sortable	:true,
			width		:150,			
			resizable	:true,
			align		:'right',
			renderer	:function(v){
				return '<div align=rigth>'+Ext.util.Format.number(v,'$0,0.00');
			},hideable:	false
		},
		{
			tooltip		:'Plazo de la Operaci�n',
			dataIndex	:'PLAZO_OPER',
			sortable	:true,
			width		:100,			
			resizable	:true,
			align		:'center',
			renderer	:function(v){
				return v+' d�as'
			},hideable:	false
		},		
		{
				header : '',
				tooltip: 'Estatus',
				dataIndex: 'BANDERA',
				sortable: true,
				resizable: true,
				width: 150,					
				align: 'center',				
				editor:comboEstatusAsignar,
				renderer: Ext.util.Format.comboRenderer(comboEstatusAsignar)					
		},	
		{	//TASA_INDICATIVA
			//tootip		:'Tasa 48 horas',
			dataIndex	:'TASA_INDICATIVA',
			align		:'center',hideable:	false
		},
		{
			tootip		:'Tasa mismo d�a',
			dataIndex	:'TASA_MD',
			align		:'center',
			renderer		:function(value,metadata,registro){
				return NE.util.colorCampoEdit(value,metadata,registro);
			},
			editor:{
				xtype:'numberfield',
				allowBlank: false
			},
			hideable:	false
		},
		{	//TASA_INDICATIVA
			//tootip		:'Tasa 48 horas',
			dataIndex	:'TASA_INDICATIVA',
			align		:'center',hideable:	false
		},
		{
			tootip		:'Tasa 48 horas',
			dataIndex	:'TASA_48HRS',
			align		:'center',
			renderer		:function(value,metadata,registro){
				return NE.util.colorCampoEdit(value,metadata,registro);
			},
			editor:{xtype:'numberfield'},hideable:	false
		},
		{
			tootip		:'Causas de rechazo',
			dataIndex	:'CUASA_RECHAZO',
			align		:'center',
			width		:150,
			renderer	:function(value, metadata,registro){
				return NE.util.colorCampoEdit(value,metadata,registro);
			},
			editor:{xtype:'textarea',name:'causa'},hideable:	false
		},
		{
			align		:'center',
			width		:150,
			renderer		:function(v,params,record,row,col){
				var radio_I="<input onclick='setTipoSolic(1,"+row+")' type='radio' name = 'rad_tipo_solic"+row+"' value='I'  >Indicativa<br>";
				var radio_F="<input onclick='setTipoSolic(2,"+row+")' type='radio' name = 'rad_tipo_solic"+row+"'  checked value='F'  >En Firme";
				return radio_I+''+radio_F;
			},hideable:	false
		},
		{
			xtype: 'actioncolumn',
			tooltip: 'Observaciones',
			width: 100,
			align: 'center',					
			items: [
				{
					getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						this.items[0].tooltip = 'Ver';
						return 'iconoLupa';										
					} ,
					handler: verObservaciones
				}
			],hideable:	false			
		},
		{
			xtype: 'actioncolumn',
			tooltip: 'Pland de Pago',
			dataindex		:'PLAN_PAGO',
			width: 100,
			align: 'center',					
			items: [
				{
					getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						this.items[0].tooltip = 'Ver Tabla';
						
						var planPago=registro.data['PLAN_PAGO'];
						if(planPago!=''){
							return 'icoLupa';										
						}else{
							
						}
					} ,
					handler:verTabla
				}
			]		,hideable:	false		
		},
	   {
			 xtype		: 'actioncolumn',
			 tooltip		: 'Consultar',				
			 dataIndex	: 'CONSULTAR',
			 align		: 'center',
			 width		:100,
			 items		: [
				{
					getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
					
					  this.items[0].tooltip = 'Solicitud';
					  return 'icoModificar';										
					},
					handler: verSolicitud
				}
			],hideable:	false
		}
	],			
	displayInfo		: true,		
	emptyMsg			: "No hay registros.",		
	loadMask			: true,
	stripeRows		: true,	
	align				: 'center',
	shadow			:true,
	frame				: true,
	listeners: {	
		beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
			var record = e.record;
			var estatus =  record.data['ESTATUS'];
			
			catalogoEstatusAsignar.load({
				params: {
					informacion: 'catalogoEstatusAsignar',							
					estatus:estatus
				}						
			});
			
		}
	}				
});
//-----------------------------Fin gridConsulta---------------------------------

var fp =new Ext.form.FormPanel({
		id: 'forma',
		width: 920,
		height: 700,
		frame: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [gridConsulta],
		buttons: [
			{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls: 'icoGuardar',
				formBlind	:true,
				handler: function (boton, evento){

					var datos = gridConsulta.getStore();
					if(!Ext.isEmpty(datos.getModifiedRecords())){

						limpiaRecordsToSave();

						var indice=0;
						var mostrarWin = false;
						Ext.each(datos.getModifiedRecords(), function(record) { //step 2

							if(datos.modified.ESTATUS || record.data.BANDERA =='' ){

								Ext.Msg.alert('Mensaje...','No se ha modificado el estatus de ninguna solicitud');
								return;
							}else{

								if(record.data.BANDERA=="2"){
									if(record.data.TASA_MD==""){
										Ext.Msg.alert("","Existen campos vacios, favor de capturarlos");
										return;
									}
									
									if(parseFloat(record.data.TASA_MD.value)<=0){
										Ext.Msg.alert('Error...','El valor de la tasa debe ser mayor a cero');
										return;
									}
									
									if(record.data.TASA_48HRS==""){
										Ext.Msg.alert('Error...','Existen campos vacios, favor de capturarlos');
										return;
									}
									if(parseFloat(record.data.TASA_48HRS)<=0){
										Ext.Msg.alert('Error...','El valor de la tasa debe ser mayor a cero');
										return;
									}

								}
								if(record.data.BANDERA=="3"){
									if(record.data.CUASA_RECHAZO==""){
										Ext.Msg.alert('Error...','Existen campos vacios, favor de capturarlos');
										return;
									}
								}

								mostrarWin = true;
								
							}

							recordsToSave.push(record.data.CONSULTAR);//0
							recordsToSave.push(record.data.FECHA);//1
							recordsToSave.push(record.data.NUM_SOLIC);//2
							recordsToSave.push(record.data.REQ_AUT);//3
							recordsToSave.push(record.data.IF);//4
							recordsToSave.push(record.data.MONEDA);//5
							recordsToSave.push(record.data.MONTO_REQUERIDO);//6
							recordsToSave.push(record.data.PLAZO_OPER);//7
							recordsToSave.push(record.data.BANDERA);//8
							recordsToSave.push(record.data.TASA_MD);//9
							recordsToSave.push(record.data.TASA_48HRS);//10
							recordsToSave.push(record.data.TASA_INDICATIVA == '' ? ' ' : record.data.TASA_INDICATIVA);//11
							recordsToSave.push(record.data.IC_TASA == '' ? '0' : record.data.IC_TASA);//12
							recordsToSave.push(record.data.NUM_REFERENCIA == '' ? ' ' : record.data.NUM_REFERENCIA);//13
							recordsToSave.push(record.data.CUASA_RECHAZO == '' ? ' ' : record.data.CUASA_RECHAZO);//14
							recordsToSave.push(record.data.TIPO_COTIZACION == '' ? 'F' : record.data.TIPO_COTIZACION);//15
							recordsToSave.push(record.data.OBSERVACIONES == '' ? ' ' : record.data.OBSERVACIONES);//16
							recordsToSave.push(record.data.PLAN_PAGO == '' ? ' ' : record.data.PLAN_PAGO);//17
							recordsToSave.push(record.data.CONSULTAR);//18
							recordsToSaveT.push(recordsToSave);
							recordsToSave=[];
							indice++;
							////////////////////////////////////////////
							//girdPreGuardar.el.mask('Procesando...', 'x-mask-loading'); //step 3
						});
						if(mostrarWin ){
							cotizador('SHOW_INF_TO_SAVE',recordsToSaveT);
						}
						
					}else{
						Ext.Msg.alert(boton.getText(),'No se ha modificado el estatus de ninguna solicitud');
					}
				}
			},
			{
				text: 'Generar Archivo',
				id: 'btnArchivoPDF',
				iconCls: 'icoPdf',
				handler: function (boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
					var forma = fp.getForm().getValues();
					cotizador('GENERAR_ARCHIVO',forma);
				}//fin Handler
			}
		]
	});

//------------------------------------------------------------------------------
//----------------------------GRIDS DETALLES SOLICITUD--------------------------
//------------------------------------------------------------------------------
	
//------------------------------------------------------------------------------
//----------------------------------- CONTENEDOR -------------------------------
//------------------------------------------------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		renderHidden:	true,
		height: 	'auto',
		style: 'margin:0 auto;',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(50),
			ventanaCotiza			
			],
			ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Derefenrencia ultimo elemento...
			element = null;

		}

	});
	cotizador('INICIALIZACION',null);

});

