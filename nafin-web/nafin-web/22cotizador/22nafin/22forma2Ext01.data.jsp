<%@ page contentType="application/json;charset=UTF-8" import="
		 com.netro.cotizador.*,
		 netropology.utilerias.*,
		 net.sf.json.JSONArray,
		 net.sf.json.JSONObject,
                 java.io.BufferedWriter.*,
                 java.io.*,
		 java.util.*,
                 com.netro.model.catalogos.*,
		 org.apache.commons.logging.Log" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../22secsession_extjs.jspf"%>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>
<%
String informacion = (request.getParameter("informacion") !=null) ? request.getParameter("informacion") : "";
String operacion   = (request.getParameter("operacion")   !=null) ? request.getParameter("operacion")   : "";
String area        = (request.getParameter("cmbArea") !=null) ? request.getParameter("cmbArea") : "";
String icIf        = (request.getParameter("icIf") !=null) ? request.getParameter("icIf") : "";
String infoRegresar = "";
int totalIF = 0;
JSONObject jsonObj = new JSONObject();

log.debug("informacion===>"+informacion);
log.debug("area===>"+area);

if ( "catalogoArea".equals(informacion)   ) { 

    CatalogoSimple catalogo = new CatalogoSimple();
    catalogo.setCampoClave("ic_area");
    catalogo.setCampoDescripcion("cg_descripcion");
    catalogo.setTabla("cotcat_area");		
    catalogo.setOrden("cg_descripcion");	
    infoRegresar = catalogo.getJSONElementos();
                
} else if ( "catalogoIFData".equals(informacion)   ) { 
        
    CatalogoIFCotizador catalogo = new CatalogoIFCotizador();
    catalogo.setCampoClave("cif.ic_if");
    catalogo.setCampoDescripcion("cif.cg_razon_social");
    catalogo.setArea(area);	
    catalogo.setOrden("2");	
    infoRegresar = catalogo.getJSONElementos(); 
    
} else if ( "catalogoEjecutivosData".equals(informacion)   ) { 
        
    CatalogoEjecutivoIF catalogo = new CatalogoEjecutivoIF();
    catalogo.setCampoClave("ic_ejecutivo");
    catalogo.setCampoDescripcion("cg_nombre || ' ' || cg_appaterno || ' ' || cg_apmaterno");
    catalogo.setArea(area);	
    catalogo.setOrden("1");	
    infoRegresar = catalogo.getJSONElementos(); 
    
} else if ( "Consultar".equals(informacion)   ) { 

    MantenimientoCot mntoCot = ServiceLocator.getInstance().lookup("MantenimientoCotEJB", MantenimientoCot.class);
    
    List ejecutivo = new ArrayList();
    HashMap datEje = new HashMap();
    
    String nombreDirector = mntoCot.getDirectorXArea(area);
    List ejecutivos = mntoCot.getEjecutivosXArea(area);
    int numEjecutivos = ejecutivos.size();   
    
    //Numero de ejecutivos en la  cotcat_ejecutivo
    for(int i = 0; i < numEjecutivos; i++) {
        datEje = new HashMap();        
        List datosEjecutivo = (List)ejecutivos.get(i);	
        String claveEjecutivo = datosEjecutivo.get(0).toString();
	String nombreEjecutivo = datosEjecutivo.get(1).toString();
	String[] nuevoNombreEj = nombreEjecutivo.split(" ");
	StringBuffer nomEjeCom = new StringBuffer("");        
	for (int z = 0; z < nuevoNombreEj.length; z++){
            nomEjeCom.append(nuevoNombreEj[z]+" ");
	}	 
        datEje.put("clave",claveEjecutivo);
	datEje.put("nombre",nombreEjecutivo);        
        ejecutivo.add(datEje);       
    }
	        
    StringBuffer  columnasRegistros = new StringBuffer();
    
    if(numEjecutivos > 0) {
    
        List intermeriarios = (List)mntoCot.getIntermediariosFin(area, icIf);
	totalIF = intermeriarios.size();
        
        columnasRegistros.append("[ "); 
        
	for(int i = 0;i < totalIF; i++) {
        
            List datosIntermediario = (List)intermeriarios.get(i);            
            String claveIF = datosIntermediario.get(0).toString();
            String nombreIF = datosIntermediario.get(1).toString();            
            String tipoAutorizacion = "";
            String monto_mayor = "";
            String cotizaLinea = "";
            String correos = "";
            String claveEjecutivo ="";
                
            columnasRegistros.append("{ ");
            columnasRegistros.append(" CLAVE_INTER : '"+ claveIF +"', ");
            columnasRegistros.append(" NOMBRE_INTER : '"+ nombreIF +"', ");   
            
            Vector VDatosSolicAut = new Vector();            
            VDatosSolicAut = mntoCot.getDatosAutorizacion("", claveIF);
            if(VDatosSolicAut.size()>0) {
                tipoAutorizacion = (( VDatosSolicAut.get(0) == null)?"N":VDatosSolicAut.get(0).toString());
                monto_mayor = (( VDatosSolicAut.get(1) == null)?"":VDatosSolicAut.get(1).toString());
                cotizaLinea= (( VDatosSolicAut.get(2) == null)?"":VDatosSolicAut.get(2).toString());
                correos= (( VDatosSolicAut.get(3) == null)?"":VDatosSolicAut.get(3).toString());
                claveEjecutivo= (( VDatosSolicAut.get(4) == null)?"":VDatosSolicAut.get(4).toString());
                                               
                columnasRegistros.append(" AUTORIZACION : '"+ tipoAutorizacion +"', ");  
                   
                   
                if (!"".equals(monto_mayor)  ) {
                    columnasRegistros.append(" MONTO : '"+ monto_mayor +"', ");
                } else {
                    columnasRegistros.append(" MONTO : '"+"Ninguno"+"', ");                     
                }                
                    
                if (!"".equals(cotizaLinea)  ) {
                    columnasRegistros.append(" COTIZACION : '"+ cotizaLinea +"', ");
                } else {
                    columnasRegistros.append(" COTIZACION : '"+"N"+"', ");                   
                }

                columnasRegistros.append(" CORREO_ELECTRONICO : '"+ correos +"', ");
                  
                columnasRegistros.append(" EJECUTIVO : '"+ claveEjecutivo +"', ");
            }                 
                      
            columnasRegistros.append(" },");           
        }//for(int i = 0;i < totalIF; i++) {
        
        columnasRegistros.deleteCharAt(columnasRegistros.length()-1);
        columnasRegistros.append(" ] ");        
    }
    
    if("".equals(nombreDirector) || nombreDirector == null) {
        nombreDirector="vacio";
    }
    
    jsonObj = new JSONObject();
    jsonObj.put("success", new Boolean(true));   
    jsonObj.put("columnasRegistros", "{registros : "+columnasRegistros.toString()+",total : "+totalIF+"}"); 
    jsonObj.put("nombreDirector", nombreDirector);
    jsonObj.put("numEje", numEjecutivos);
  
    infoRegresar = jsonObj.toString();

} else if ("guardar".equals(informacion)  ) {

    MantenimientoCot mntoCot = ServiceLocator.getInstance().lookup("MantenimientoCotEJB", MantenimientoCot.class);

    String clavesIF[] = request.getParameterValues("clavesIF");
    String autorizacion[] = request.getParameterValues("autorizacion");
    String monto[] = request.getParameterValues("monto");
    String cotizacion[] = request.getParameterValues("cotizacion");
    String correos[] = request.getParameterValues("correos");
    String ejecutivo[] = request.getParameterValues("ejecutivo");                      
    String respuesta = "OK";
    String revisarSolicEjecutivo="";
    String revisarSolicEjecutivoAnt="anterior";
    try {
        
        Vector listEjecutivos = mntoCot.getEjecutivosXArea(area);
	int numEjecutivos = listEjecutivos.size();
       
        for(int i=0;i<clavesIF.length;i++){            
           
            for(int j=0; j<numEjecutivos; j++){
            
                List datosEjecutivo = (List)listEjecutivos.get(j);
                String claveEjecutivo = datosEjecutivo.get(0).toString(); 
                                 
                String accion ="N";
                String existeRelacion ="N";
                if(claveEjecutivo.equals(ejecutivo[i])){
                    accion ="S";
                    existeRelacion = mntoCot.getRelacionEjecutivoIF(claveEjecutivo, clavesIF[i]);
                }    
                            
                if ( "S".equals(accion) ) { 
                    if ("S".equals(existeRelacion)  ) {
                        mntoCot.actualizaDatosAutorizacion(claveEjecutivo,clavesIF[i],autorizacion[i],monto[i], cotizacion[i], correos[i]);
                    }else   if ("N".equals(existeRelacion)  ) { 
                        mntoCot.setRelacionEjecutivoIF(accion,clavesIF[i],claveEjecutivo);
                        mntoCot.actualizaDatosAutorizacion(claveEjecutivo,clavesIF[i],autorizacion[i],monto[i], cotizacion[i], correos[i]);     
                    }                
                }else    if ( "N".equals(accion)   ) {
                    mntoCot.setRelacionEjecutivoIF(accion,clavesIF[i],claveEjecutivo);
                }
                
               
                
            }
	}	
	
    } catch (Exception e){
	respuesta = "ERROR";
        log.error("error "+e);
    }
    
   
    jsonObj = new JSONObject();
    jsonObj.put("success", new Boolean(true));
    jsonObj.put("respuesta", respuesta);    
    infoRegresar = jsonObj.toString();
	
} else if (informacion.equals("GeneraCSV") ) {

    MantenimientoCot mntoCot = ServiceLocator.getInstance().lookup("MantenimientoCotEJB", MantenimientoCot.class);

    Map<String,Object> datos = new HashMap<>();
    datos.put("strDirectorioTemp", strDirectorioTemp);
    datos.put("area", area);
    datos.put("icIf", icIf);
    
    try{
        String nombreArchivo = mntoCot.genArchAsignaEjecutivoIF(datos);
        jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();
    } catch(Throwable e){
        throw new AppException("Error al generar el archivo CSV. ", e);
    }
}

//log.debug("infoRegresar===>"+infoRegresar);

%>
<%=  infoRegresar %>