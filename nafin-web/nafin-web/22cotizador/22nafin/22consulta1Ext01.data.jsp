<%@ page 
	contentType=
		"application/json;charset=UTF-8" 
	import="
		 com.netro.cotizador.*,
		 netropology.utilerias.*,
		 com.netro.pdf.*,
		 net.sf.json.JSONArray, 
		 net.sf.json.JSONObject,
		 java.util.*,
		 org.apache.commons.logging.Log  " 
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="../22secsession_extjs.jspf" %>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%    
String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String operacion		= (request.getParameter("operacion")	!=null)	?	request.getParameter("operacion")	:	"";

String fechaInicial	= (request.getParameter("_fechaReg")	!=null)	?	request.getParameter("_fechaReg")	:	"";
String fechaFinal		= (request.getParameter("_fechaFinReg")!=null)	?	request.getParameter("_fechaFinReg"):	"";
String moneda   		= (request.getParameter("cmb_mon_h")	!=null)	?	request.getParameter("cmb_mon_h")	:	"";
String numSolicitud  = (request.getParameter("_numSol")	!=null)	?	request.getParameter("_numSol")	:	"";
String montoInicial	= (request.getParameter("montoIni")	!=null)	?	request.getParameter("montoIni")	:	"";
String montoFinal		= (request.getParameter("montoFin")!=null)	?	request.getParameter("montoFin"):	"";
String intermediario = (request.getParameter("cmb_ti")	!=null)	?	request.getParameter("cmb_ti")	:	"";
String estatus			= (request.getParameter("cmb_tipo")	!=null)	?	request.getParameter("cmb_tipo")	:	"";
String persona	      = (request.getParameter("cmb_per")	!=null)	?	request.getParameter("cmb_per")	:	"";
String idSolicitud	= (request.getParameter("idPago")	!=null)	?	request.getParameter("idPago")	:	"";
String nombreInter	= (request.getParameter("nomInt")	!=null)	?	request.getParameter("nomInt")	:	"";
String estatusSolici = (request.getParameter("estSol")	!=null)	?	request.getParameter("estSol")	:	"";

String infoRegresar	= "";
String consulta   	= "";
int  start= 0, limit =0;


if (informacion.equals("cargaFecha") )	{
  
  JSONObject		 jsonObj  = new JSONObject();
  Calendar calendario = Calendar.getInstance();  
  SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
  String fechaHoy = formatoFecha.format(calendario.getTime());
  
  jsonObj.put("success"	,  new Boolean(true)); 
  jsonObj.put("fechaHoy", fechaHoy);
  infoRegresar = jsonObj.toString();

} else if (informacion.equals("catalogomon") )	{
	
	ConsultaSolicitudes cs = new ConsultaSolicitudes();	
	Registros reg= cs.catalogoMoneda();
	JSONObject		 jsonObj  = new JSONObject();
	jsonObj.put("total",new Integer( reg.getNumeroRegistros()) );	
	jsonObj.put("registros", reg.getJSONData() );	
   infoRegresar = jsonObj.toString();

} else if (informacion.equals("catalogoint") )	{
	
   ConsultaSolicitudes cs = new ConsultaSolicitudes();	
	Registros reg= cs.catalogoIntermediario();
	JSONObject		 jsonObj  = new JSONObject();
	jsonObj.put("total",new Integer( reg.getNumeroRegistros()) );	
	jsonObj.put("registros", reg.getJSONData() );	
   infoRegresar = jsonObj.toString();	

} else if (informacion.equals("catalogoEstatus") )	{

   ConsultaSolicitudes cs = new ConsultaSolicitudes();	
	Registros reg= cs.catalogoEstatus();
	JSONObject		 jsonObj  = new JSONObject();
	jsonObj.put("total",new Integer( reg.getNumeroRegistros()) );	
	jsonObj.put("registros", reg.getJSONData() );	
   infoRegresar = jsonObj.toString();	

} else if (informacion.equals("catalogoPersona") )	{
	
   ConsultaSolicitudes cs = new ConsultaSolicitudes();	
	Registros reg= cs.catalogoPersona();
	JSONObject		 jsonObj  = new JSONObject();
	jsonObj.put("total",new Integer( reg.getNumeroRegistros()) );	
	jsonObj.put("registros", reg.getJSONData() );	
   infoRegresar = jsonObj.toString();	

} else if (informacion.equals("Consultar") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")  ){
  JSONObject jsonObj = new JSONObject();
  SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
  ConsultaSolicitudes paginador = new ConsultaSolicitudes();
  paginador.setFechaSolicitudInicial(fechaInicial);
  paginador.setFechaSolicitudFinal(fechaFinal);
  paginador.setMoneda(moneda);
  paginador.setNumeroSolicitud(numSolicitud);
  paginador.setMontoCreditoInicial(montoInicial);
  paginador.setMontoCreditoFinal(montoFinal);
  paginador.setIntermediarioFinanciero(intermediario);
  paginador.setEstatus(estatus);
  paginador.setPersonaResponsable(persona);
   
  CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
  if(informacion.equals("Consultar") ||  informacion.equals("ArchivoPDF")){
    try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));										
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	if(informacion.equals("Consultar") ){
	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}

	Registros reg = queryHelper.getPageResultSet(request,start,limit);
	while(reg.next()){	
		Solicitud sol = new Solicitud();
		sol.setIc_moneda(reg.getString("ic_moneda"));
		sol.setIg_plazo(reg.getString("PLAZO"));
		sol.setIc_tipo_tasa(reg.getString("ic_tipo_tasa"));
		sol.setIc_esquema_recup(reg.getString("ic_esquema_recup"));
		sol.setCg_ut_ppc(reg.getString("cg_ut_ppc"));
		sol.setCg_ut_ppi(reg.getString("cg_ut_ppi"));
		
		String ic_plan_de_pagos = "";
		if("3".equals(reg.getString("ic_esquema_recup")))
			ic_plan_de_pagos = reg.getString("IC_SOLIC_ESP"); 
		
		String descTasaInd = sol.getDescTasaIndicativa();
		String tec = "";
		if("3".equals(reg.getString("ic_tipo_tasa"))){
			tec = solCotEsp.techoTasaProtegida();
		}
		try{
			String valor= reg.getString("TASA_MISMO_DIA");
			String valorS= reg.getString("TASA_SPOT");
			String remplazado=valor.replace('.',',');
			String remplazadoS=valorS.replace('.',',');
			String[] valorArr = remplazado.split(",");
			String[] valorArrS = remplazadoS.split(",");
			String entero = "", enteroS = "";
			String decimal= "", decimalS= "";
			for (int z=0; z<valorArr.length; z++){
				if (z==0)			entero = valorArr[z];
				if (z==1)			decimal  = valorArr[z];
			}
			for (int z=0; z<valorArrS.length; z++){
				if (z==0)			enteroS = valorArrS[z];
				if (z==1)			decimalS  = valorArrS[z];
			}
			if(decimal.length()==1){
				StringBuffer newDec = new StringBuffer(decimal);
				newDec.append("0");
				entero=entero+"."+newDec.toString();
				reg.setObject("TASA_MISMO_DIA",entero);
			} else if(decimal.length()==2){
				entero=entero+"."+decimal;
				reg.setObject("TASA_MISMO_DIA",entero);
			}			
			if(decimalS.length()==1){
				StringBuffer newDecS = new StringBuffer(decimalS);
				newDecS.append("0");
				enteroS=enteroS+"."+newDecS.toString();
				reg.setObject("TASA_SPOT",enteroS);
			} else if(decimalS.length()==2){
				enteroS=enteroS+"."+decimalS;
				reg.setObject("TASA_SPOT",enteroS);
			}
			
		} catch (Exception e){
			System.err.println("error: "+e);
		}
		if(!reg.getString("TASA_MISMO_DIA").equals("") && !reg.getString("IC_ESTATUS").equals("3")  ){
			String res=descTasaInd+reg.getString("TASA_MISMO_DIA")+tec;
			reg.setObject("TASA_MISMO_DIA",res);
		}
		if(!reg.getString("TASA_SPOT").equals("") && !reg.getString("IC_ESTATUS").equals("3")  ){
			String res=descTasaInd+reg.getString("TASA_SPOT")+tec;
			reg.setObject("TASA_SPOT",res);
		}
		if(reg.getString("IC_ESTATUS").equals("7") || reg.getString("IC_ESTATUS").equals("9") ){
			String res=descTasaInd+reg.getString("TASA_CONFIRMADA")+tec;
			reg.setObject("TASA_CONFIRMADA",res);
		}
		if( !ic_plan_de_pagos.equals("") ){
			String res="Ver Tabla";
			reg.setObject("PLAN_PAGO",res);
		}
		if(reg.getString("IC_ESTATUS").equals("1") || reg.getString("IC_ESTATUS").equals("3")|| reg.getString("IC_ESTATUS").equals("4")){ 
			String res="Solicitud";
			reg.setObject("CONSULTAR",res);
		} else {
			String res="Cotizacion";
			reg.setObject("CONSULTAR",res);
		}
	}
	consulta = 	"{\"success\": true, \"total\": \""+ queryHelper.getIdsSize() +"\", \"registros\": " + reg.getJSONData()+"}";
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
   jsonObj = JSONObject.fromObject(consulta);        
    
   } else if(informacion.equals("ArchivoPDF") ){
				try {
					String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
			}//fin pdf
  // fin consulta | pdf  
  } else  if(informacion.equals("ArchivoCSV") )  {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, ".csv");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
	}
	infoRegresar = jsonObj.toString();
	//log.debug("infoR: "+infoRegresar);
} else if (informacion.equals("Ver Tabla")){ 
  JSONObject				jsonObj 		= new JSONObject();
  ConsultaSolicitudes 	paginador 	= new ConsultaSolicitudes();
  SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
  CreaArchivo 				archivo 		= new CreaArchivo();
  String 			contenidoArchivo	= "";
  String				nombreArchivo		= "";
  
  Solicitud sol = new Solicitud();
  sol = solCotEsp.consultaSolicitud(idSolicitud);
  if((sol.getPagos()).size() > 0){
		ArrayList alFilas = sol.getPagos();
		contenidoArchivo = "\r\n Plan de Pagos Capital - Solicitud Número: "+sol.getNumero_solicitud() + "\r\n\r\n"+
						   "Número de Pago,Fecha de Pago,Monto a pagar \r\n";
		for(int i=0;i<alFilas.size();i++){
			ArrayList alColumnas = (ArrayList)alFilas.get(i);
			contenidoArchivo += alColumnas.get(0).toString() + "," +alColumnas.get(1).toString() + "," +
							   alColumnas.get(2).toString() + "\r\n";
		}
	}
	try {
		if(!archivo.make(contenidoArchivo, strDirectorioTemp, ".csv"))
			jsonObj.put("success", new Boolean(false));
		else{
			nombreArchivo = archivo.nombre;
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}		
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}	
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("Detalle")){

	JSONObject				jsonObj 			= new JSONObject();
	List						datosUno			= new ArrayList();
	List						datosDos			= new ArrayList();
	HashMap					hmUno				= new HashMap();
	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
	Solicitud 				sol 				= new Solicitud();
	String origen = (request.getParameter("origen")==null)?"":request.getParameter("origen");
	String periodo = "";
	String tec 		= "";
	String clase	= "";
	int tipoClase	= 0;
	
	try {
	if(!"".equals(idSolicitud)) {
		if("2".equals(estatusSolici)&&"IF".equals(strTipoUsuario)) {
			autSol.setEstatusSolic(idSolicitud, "5", estatusSolici);
		}
		sol = solCotEsp.consultaSolicitud(idSolicitud);
		if("3".equals(sol.getIc_tipo_tasa())){
			tec = solCotEsp.techoTasaProtegida();
		}
		String nombreArchivo= sol.getNumero_solicitud()+".pdf";
		double monto		=sol.getFn_monto();
		String idEjec		=sol.getIc_ejecutivo();
		String nomEjec		=sol.getNombre_ejecutivo();
		String fechaSolic	=sol.getDf_solicitud();
		String programa	=sol.getCg_programa();
		String idMoneda	=sol.getIc_moneda();
		String idTasa		=sol.getIc_tipo_tasa();
		String numDisp		=sol.getIn_num_disp();
		String tipoPiso	=sol.getIg_tipo_piso();
		int plazoMax		=sol.getIg_plazo_max_oper();
		int numeroMax		=sol.getIg_num_max_disp();
		String idPlazo		=sol.getIg_plazo();
		String tasaUnica	=sol.getCs_tasa_unica();
		String esquemaRe	=sol.getIc_esquema_recup();
		String idPpi		=sol.getIg_ppi();
		String utPpi		=sol.getCg_ut_ppi();
		String diaPago		=sol.getIn_dia_pago();
		String idPpc		=sol.getIg_ppc();
		String utPpc		=sol.getCg_ut_ppc();
		String periodos	=sol.getIg_periodos_gracia();
		String utPeriod	=sol.getCg_ut_periodos_gracia();
		String razonSocIf =sol.getCg_razon_social_if();
		String riesgoIf	=sol.getIg_riesgo_if();
		String acreditado =sol.getCg_acreditado();
		String riesgoAc	=sol.getIg_riesgo_ac();
		int 	 diasIrre	=sol.getDiasIrreg();
		int contador=4;
		
		String montoDisp="";
		if( estatusSolici.equals("10") && sol.getPagos()!=null && sol.getPagos().size()>0 )//10-Confirmada Vencida
			session.setAttribute("pagos2",sol.getPagos());
		for(int i=0;i<Integer.parseInt(sol.getIn_num_disp());i++){
			montoDisp=sol.getFn_monto_disp(i);
		}
		String numeroSolicitud 	 = sol.getNumero_solicitud();
		String fechaSolicitud	 = sol.getDf_solicitud();
		String horaSolicitud		 = sol.getHora_solicitud();
		StringBuffer nombreFecha = new StringBuffer("");
		StringBuffer nombreHora  = new StringBuffer("");
		String 		 valorFecha	 = "";
		String		 valorHora	 = "";
		
		hmUno.put("uno","\""+numeroSolicitud+"\"");	
		hmUno.put("dos","\"N&uacute;mero de Solicitud\"");					
		datosUno.add(hmUno);
		hmUno=new HashMap();
		hmUno.put("uno","\""+fechaSolicitud+"\"");	
		hmUno.put("dos","\"Fecha de Solicitud\"");					
		datosUno.add(hmUno);
		hmUno=new HashMap();
		hmUno.put("uno","\""+horaSolicitud+"\"");	
		hmUno.put("dos","\"Hora de Solicitud\"");					
		datosUno.add(hmUno);
		
		if( !estatusSolici.equals("1") & !estatusSolici.equals("3") & !estatusSolici.equals("4") ){
			String auxFecha = "Cotizaci&oacute;n";
			if("8".equals(idSolicitud))
				auxFecha = "Cancelaci&oacute;n";
			nombreFecha.append("Fecha de "+auxFecha);
			valorFecha =  sol.getDf_cotizacion();
			nombreHora.append("Hora de "+auxFecha);
			valorHora  = sol.getHora_cotizacion();		
			
			hmUno=new HashMap();
			hmUno.put("uno","\""+valorFecha+"\"");	
			hmUno.put("dos","\""+nombreFecha.toString()+"\"");					
			datosUno.add(hmUno);
			hmUno=new HashMap();
			hmUno.put("uno","\""+valorHora+"\"");	
			hmUno.put("dos","\""+nombreHora.toString()+"\"");					
			datosUno.add(hmUno);
			contador=6;
		}
		String numeroUsuario = sol.getIc_usuario();
		String nombreUsuario = sol.getNombre_usuario();
		StringBuffer numNomUs= new StringBuffer("");
		numNomUs.append(numeroUsuario+" - "+nombreUsuario);

		hmUno=new HashMap();
		hmUno.put("uno","\""+numNomUs+"\"");	
		hmUno.put("dos","\"N&uacute;mero y Nombre del Usuario\"");				
		datosUno.add(hmUno);
		
		String numeroRecibo ="";
		String msg="";
		if(estatusSolici.equals("7")||estatusSolici.equals("8")||estatusSolici.equals("9")){
			numeroRecibo = sol.getIg_num_referencia();
			msg = "En caso de que la disposici&oacute;n de los recursos no se lleve a cabo total o parcialmente en la "+
					"fecha estipulada, Nacional Financiera aplicar&aacute; el cobro de una comisi&oacute;n sobre los "+
					"montos no dispuestos, conforme a las pol&iacute;ticas de cr&eacute;dito aplicables a este tipo de "+
					"operaciones.";
			hmUno=new HashMap();
			hmUno.put("uno","\""+numeroRecibo+"\"");	
			hmUno.put("dos","\"N&uacute;mero de Recibo\"");				
			datosUno.add(hmUno);	
			hmUno=new HashMap();
			hmUno.put("uno","\""+msg+"\"");	
			hmUno.put("dos","\"msg\"");				
			datosUno.add(hmUno);	
			contador=7;
		}
		
		
		HashMap hmDos = new HashMap();
		
		String nombreEjecutivo	=sol.getNombre_ejecutivo();
		String correoElectroni	=sol.getCg_mail();
		String telefono			=sol.getCg_telefono();
		String fax					=sol.getCg_fax();
		
		hmDos=new HashMap();
		hmDos.put("tres","\""+nombreEjecutivo+"\"");	
		hmDos.put("cuatro","\"Nombre del Ejecutivo\"");				
		datosDos.add(hmDos);		
		hmDos=new HashMap();
		hmDos.put("tres","\""+correoElectroni+"\"");	
		hmDos.put("cuatro","\"Correo Electr&oacute;nico\"");				
		datosDos.add(hmDos);
		hmDos=new HashMap();
		hmDos.put("tres","\""+telefono+"\"");	
		hmDos.put("cuatro","\"Tel&eacute;fono\"");				
		datosDos.add(hmDos);
		hmDos=new HashMap();
		hmDos.put("tres","\""+fax+"\"");	
		hmDos.put("cuatro","\"Fax\"");				
		datosDos.add(hmDos);
		
		List datosTres = new ArrayList();
		HashMap hmTres = new HashMap();
		int contador3	= 1;
		String tipooper = sol.getTipoOper();
		String nombreIntermediario="";
		
		hmTres=new HashMap();
		hmTres.put("cinco","\""+tipooper+"\"");	
		hmTres.put("seis","\"Tipo de Operaci&oacute;n\"");				
		datosTres.add(hmTres);
		if(!"Primer Piso".equals(tipooper)){
			if(!"".equals(sol.getNombre_if_ci())) {
				nombreIntermediario = sol.getNombre_if_ci();
			} else {
				nombreIntermediario = sol.getNombre_if_cs();
			}
			hmTres=new HashMap();
			hmTres.put("cinco","\""+nombreIntermediario+"\"");	
			hmTres.put("seis","\"Nombre del Intermediario\"");				
			datosTres.add(hmTres);
			contador3++;
			if(!"IF".equals(strTipoUsuario)&&!"".equals(sol.getRiesgoIf())){
				String riesgoIntermediario =sol.getRiesgoIf(); 
				hmTres=new HashMap();
				hmTres.put("cinco","\""+riesgoIntermediario+"\"");	
				hmTres.put("seis","\"Riesgo del Intermediario\"");				
				datosTres.add(hmTres);
				contador3++;
			}
		}
		if(!"IF".equals(strTipoUsuario)&&!"".equals(sol.getCg_acreditado())){
			String nombreAcreditado =sol.getCg_acreditado(); 
			hmTres=new HashMap();
			hmTres.put("cinco","\""+nombreAcreditado+"\"");	
			hmTres.put("seis","\"Nombre del Acreditado\"");				
			datosTres.add(hmTres);
			contador3++;
			if("Primer Piso".equals(tipooper)){
				String riesgoAcreditado =sol.getRiesgoAc(); 
				hmTres=new HashMap();
				hmTres.put("cinco","\""+riesgoAcreditado+"\"");	
				hmTres.put("seis","\"Riesgo del Acreditado\"");				
				datosTres.add(hmTres);
				contador3++;
			}
		}
		
		List datosCinco = new ArrayList();
		HashMap hmCinco = new HashMap();
		int contador5	= 0;
		
		if(!"".equals(sol.getCg_programa())){
			hmCinco=new HashMap();
			hmCinco.put("siete","\""+sol.getCg_programa()+"\"");	
			hmCinco.put("ocho","\"Nombre del programa o acreditado Final\"");				
			datosCinco.add(hmCinco);
			contador5++;
		}
		hmCinco=new HashMap();
		StringBuffer montoTotalReq = new StringBuffer ("");
		montoTotalReq.append(Comunes.formatoDecimal(sol.getFn_monto(),2));
		if(sol.getIc_moneda().equals("1")){
			montoTotalReq.append(" Pesos");
		} else {
			montoTotalReq.append(" D&oacute;lares");
		}
		hmCinco.put("siete","\""+"$ "+montoTotalReq.toString()+"\"");	
		hmCinco.put("ocho","\"Monto total requerido\"");				
		datosCinco.add(hmCinco);
		contador5++;
		
		String tipoTasaReq = sol.getTipo_tasa();
		hmCinco=new HashMap();
		hmCinco.put("siete","\""+tipoTasaReq+"\"");	
		hmCinco.put("ocho","\"Tipo de tasa requerida\"");				
		datosCinco.add(hmCinco);
		contador5++;
		
		String numeroDispos = sol.getIn_num_disp();
		hmCinco=new HashMap();
		hmCinco.put("siete","\""+numeroDispos+"\"");	
		hmCinco.put("ocho","\"Número de Disposiciones\"");				
		datosCinco.add(hmCinco);
		contador5++;
		
		if(Integer.parseInt(sol.getIn_num_disp())>1){
			String dispMult = "";
			if("S".equals(sol.getCs_tasa_unica())){
				dispMult="Unica";
			} else {
				dispMult="Varias";
			}
			hmCinco=new HashMap();
			hmCinco.put("siete","\""+dispMult+"\"");	
			hmCinco.put("ocho","\"Disposiciones múltiples Tasa de Interés\"");				
			datosCinco.add(hmCinco);
			contador5++;
		}
		
		String plazoOper = sol.getIg_plazo();
		hmCinco=new HashMap();
		hmCinco.put("siete","\""+plazoOper+" Dias"+"\"");	
		hmCinco.put("ocho","\"Plazo de la Operación\"");				
		datosCinco.add(hmCinco);
		contador5++;
		
		String esquemaRec = sol.getEsquema_recup();
		hmCinco=new HashMap();
		hmCinco.put("siete","\""+esquemaRec+"\"");	
		hmCinco.put("ocho","\"Esquema de recuperación del capital\"");				
		datosCinco.add(hmCinco);
		contador5++;
		
		if(!"1".equals(sol.getIc_esquema_recup())){
			String ic_esquema_recup = sol.getIc_esquema_recup();
			if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("3")){
				String perioPago = sol.periodoTasa(sol.getCg_ut_ppi());
				hmCinco=new HashMap();
				hmCinco.put("siete","\""+perioPago+"\"");	
				hmCinco.put("ocho","\"Periodicidad del pago de Interés\"");				
				datosCinco.add(hmCinco);
				contador5++;
			}
			if(!"3".equals(sol.getIc_esquema_recup())){
				if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("4")){
					String perioCapital = sol.periodoTasa(sol.getCg_ut_ppc());
					hmCinco=new HashMap();
					hmCinco.put("siete","\""+perioCapital+"\"");	
					hmCinco.put("ocho","\"Periodicidad de Capital\"");				
					datosCinco.add(hmCinco);
					contador5++;
				}
			}
			if(ic_esquema_recup.equals("2") && !"".equals(sol.getCg_pfpc())){
				String fechaPago = sol.getCg_pfpc();
				hmCinco=new HashMap();
				hmCinco.put("siete","\""+fechaPago+"\"");	
				hmCinco.put("ocho","\"Primer fecha de pago de Capital\"");				
				datosCinco.add(hmCinco);
				contador5++;
				String fechaPenul = sol.getCg_pufpc();
				hmCinco=new HashMap();
				hmCinco.put("siete","\""+fechaPenul+"\"");	
				hmCinco.put("ocho","\"Penúltima fecha de pago de Capital:\"");				
				datosCinco.add(hmCinco);
				contador5++;
			}
		}
		
		List datosSeis = new ArrayList();
		HashMap hmSeis = new HashMap();
		int contador6	= 0;

		for(int i=0;i<Integer.parseInt(sol.getIn_num_disp());i++){
			hmSeis=new HashMap();
			
			hmSeis.put("nueve","\""+ (i+1)  +"\"");	
			hmSeis.put("diez","\""+"$ "+ Comunes.formatoDecimal(sol.getFn_monto_disp(i),2)  +"\"");	
			hmSeis.put("once","\""+ sol.getDf_disposicion(i)  +"\"");				
			hmSeis.put("doce","\""+ sol.getDf_vencimiento(i) +"\"");				
			datosSeis.add(hmSeis);
			contador6++;
		}
		

		List datosSiete = new ArrayList();
		HashMap hmSiete = new HashMap();
		int contador7	= 0;
	
	if((sol.getPagos()).size() > 0){
		ArrayList alFilas = sol.getPagos();
		
		for(int i=0;i<alFilas.size();i++){
			ArrayList alColumnas = (ArrayList)alFilas.get(i);
			hmSiete=new HashMap();
			hmSiete.put("nueve","\""+ (String)alColumnas.get(0) +"\"");	
			hmSiete.put("diez","\""+ (String)alColumnas.get(1)+"\"");	
			hmSiete.put("once","\""+"$ "+ Comunes.formatoDecimal(alColumnas.get(2).toString(),2)  +"\"");				
			hmSiete.put("doce","\" \"");				
			datosSiete.add(hmSiete);
			contador7++;
		}		
		}		
		
		if("1".equals(sol.getIc_esquema_recup())){
			periodo = "Al Vencimiento";
		}else if("4".equals(sol.getIc_esquema_recup())&&"0".equals(sol.getCg_ut_ppc())){
			periodo = "Al Vencimiento";
		}else if( !sol.periodoTasa(sol.getCg_ut_ppi()).equals("") ){
			periodo = sol.periodoTasa(sol.getCg_ut_ppi())+"mente";
		}
		else if( !sol.periodoTasa(sol.getCg_ut_ppc()).equals("") ){
			periodo = sol.periodoTasa(sol.getCg_ut_ppc())+"mente";
		}else{
			periodo = "Al Vencimiento";
		}
		
		List datosOcho = new ArrayList();
		HashMap hmOcho = new HashMap();
		int contador8	= 0;
		List datosNueve = new ArrayList();
		HashMap hmNueve = new HashMap();
		int contador9	= 0;
		
		if( estatusSolici.equals("2") || estatusSolici.equals("5") ){
			if("I".equals(sol.getTipoCotizacion())&&"IF".equals(strTipoUsuario)){
				hmOcho=new HashMap();
				hmOcho.put("msg","\"<center>COTIZACION INDICATIVA,<br> NO ES POSIBLE TOMAR EN FIRME LA OPERACION, FAVOR DE CONSULTAR A SU EJECUTIVO.</center> \"");				
				datosOcho.add(hmOcho);
				contador8++;
			} else if ("N".equals(sol.getCs_vobo_ejec())&&"IF".equals(strTipoUsuario)) {
				hmOcho=new HashMap();				
				hmOcho.put("msg","\"<center>Para realizar la toma en firme <br>favor de ponerse en contacto con su ejecutivo.</center> \"");				
				datosOcho.add(hmOcho);
				contador8++;
			}
			
			hmNueve=new HashMap();
			hmNueve.put("nueveDos","\""+"<b>"+ sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_md(),2)+"</b> %"+" anual pagadera "+periodo+"\"");	
			hmNueve.put("nueveUno","\"Tasa Indicativa con oferta mismo día\"");				
			datosNueve.add(hmNueve);
			contador9++;
			if(!(Comunes.formatoDecimal(sol.getIg_tasa_spot(),2)).equals("0.00")){
				hmNueve=new HashMap();
				hmNueve.put("nueveDos","\""+"<b>"+ sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_spot(),2)+"</b> %"+" anual pagadera "+periodo+"\"");	
				hmNueve.put("nueveUno","\"Tasa Indicativa con oferta 48 horas\"");				
				datosNueve.add(hmNueve);
				contador9++;
			}
			hmNueve=new HashMap();	
			hmNueve.put("nueveDos","\" \"");	
			hmNueve.put("nueveUno","\"<b>Nota:</b>\"");				
			datosNueve.add(hmNueve);
			contador9++;
			hmNueve=new HashMap();	
			hmNueve.put("nueveDos","\"El Intermediario Financiero tiene la opción de tomarla en firme el mismo día que haya recibido la cotización indicativa a más tardar a las 12:30 horas. \"");	
			hmNueve.put("nueveUno","\"<b>Tasa con oferta Mismo Día</b>\"");				
			datosNueve.add(hmNueve);
			contador9++;
			if(!(Comunes.formatoDecimal(sol.getIg_tasa_spot(),2)).equals("0.00")){
				hmNueve=new HashMap();	
				hmNueve.put("nueveDos","\"El Intermediario Financiero tiene la opción de tomarla en firme a más tardar dos días hábiles después de que la Tesorería haya proporcionado la cotización indicativa antes de las 12:30 horas del segundo día hábil. \"");	
				hmNueve.put("nueveUno","\"<b>Tasa con oferta 48 Horas</b>\"");				
				datosNueve.add(hmNueve);
				contador9++;
			}
			hmNueve=new HashMap();	
			hmNueve.put("nueveDos","\" Las cotizaciones indicativas proporcionadas por la Tesorería de Nacional Financiera son de uso reservado para el intermediario solicitante y será responsabilidad de dicho intermediario el mal uso de la información proporcionada.\"");	
			hmNueve.put("nueveUno","\" \"");				
			datosNueve.add(hmNueve);
			contador9++;
		}
		
		List datosDiez = new ArrayList();
		HashMap hmDiez = new HashMap();
		int contador10	= 0;
		
		if( estatusSolici.equals("7") || estatusSolici.equals("8") || estatusSolici.equals("9")){
			String nombreHeader="";
			if ("8".equals(estatusSolici)){
				nombreHeader="CANCELACION";
			} else {
				nombreHeader="CONFIRMACION";
			}
			hmDiez=new HashMap();	
			hmDiez.put("diezDos","\" FECHA Y HORA DE "+nombreHeader+"\"");	
			hmDiez.put("diezUno","\" TASA CONFIRMADA \"");				
			datosDiez.add(hmDiez);
			contador10++;
			hmDiez=new HashMap();
			if(sol.getCs_aceptado_md().equals("S")){
				hmDiez.put("diezUno","\" Tasa Confirmada <b>"+sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_md(),2)+" </b> anual pagadera "+periodo+" "+tec+" \"");
			} else {
				hmDiez.put("diezUno","\" Tasa Confirmada <b>"+sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_spot(),2)+" </b>% anual pagadera "+periodo+" "+tec+" \"");
			}
			if(("8".equals(estatusSolici))){
				hmDiez.put("diezDos","\"<b>"+sol.getDf_cotizacion()+" "+sol.getHora_cotizacion()+"</b>\"");	
			} else {
				hmDiez.put("diezDos","\"<b>"+sol.getDf_aceptacion()+"</b>\"");	
			}
			datosDiez.add(hmDiez);
			contador10++;
		}
		
		
		List datosOnce = new ArrayList();
		HashMap hmOnce = new HashMap();
		int contador11	= 0;
		boolean muestraCancelado = false;
		
		if(!sol.getCg_observaciones().trim().equals("") ){
			if(!"NB".equals(origen)) {
				hmOnce.put("onceUno","\""+sol.getCg_observaciones() +" \"");				
				datosOnce.add(hmOnce);
				contador11++;
			}
		}
		int botonRecotizar =0;
		int botonSalir1	 =0;
		if(!"NB".equals(origen) && !"C".equals(origen)) {
			if(estatusSolici.equals("10")){
				botonRecotizar=1;
			}
			if(origen.equals("2")){
			} else if(origen.equals("3")){
				botonSalir1=1;
			}
		}
		int cancelado=0;
		if(estatusSolici.equals("8")){
			cancelado=1;
			muestraCancelado = true; 
		}
		String a = strDirecVirtualTemp+nombreArchivo;
		String b = estatusSolici;
		String c = idSolicitud;
		String d = nombreInter;
		
		
		String pais = (String)session.getAttribute("strPais");
		String noNaf= (String)session.getAttribute("iNoNafinElectronico");
	   if(noNaf==null) noNaf ="";
		String sExt = (String)session.getAttribute("sesExterno");
		String strNo= (String) session.getAttribute("strNombre");
		String strUs= (String) session.getAttribute("strNombreUsuario");
		String logo = (String)session.getAttribute("strLogo"); 
		String dirPu= (String)session.getServletContext().getAttribute("strDirectorioPublicacion");
		
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		float widthsDocs[] = {0.35f,0.75f};
		
		try {
			
		  pdfDoc	= new ComunesPDF(1,strDirectorioTemp+nombreArchivo,"Solicitud Número: "+ sol.getNumero_solicitud() +"Pagina ", muestraCancelado);
  
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,pais,noNaf,sExt,strNo,strUs,logo,dirPu);	
			
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			pdfDoc.setTable(2,65);
			pdfDoc.setCell("Datos de la Cotización","celda04",ComunesPDF.CENTER,2,1,1);
			pdfDoc.setCell("Número de Solicitud:","formas",ComunesPDF.LEFT,1,1,1);
			pdfDoc.setCell(sol.getNumero_solicitud(),"formas",ComunesPDF.LEFT,1,1,1);
		
			pdfDoc.setCell("Fecha de Solicitud:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
			pdfDoc.setCell(sol.getDf_solicitud(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
			pdfDoc.setCell("Hora de Solicitud:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
			pdfDoc.setCell(sol.getHora_solicitud(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);		
			
			if( !estatusSolici.equals("1") & !estatusSolici.equals("3") & !estatusSolici.equals("4") ){//1=Solicictada,3=Rechazada,4=En proceso
				String auxFecha = "Cotización";
				if("8".equals(estatusSolici))
					auxFecha = "Cancelación";		
				pdfDoc.setCell("Fecha de "+auxFecha+":",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
				pdfDoc.setCell(sol.getDf_cotizacion(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);		
				pdfDoc.setCell("Hora de "+auxFecha+":",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
				pdfDoc.setCell(sol.getHora_cotizacion(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
			}
				pdfDoc.setCell("Usuario:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
				pdfDoc.setCell(sol.getIc_usuario() +" - "+sol.getNombre_usuario(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
			if(estatusSolici.equals("7")||estatusSolici.equals("8")||estatusSolici.equals("9")||origen.equals("TF")){
				if(!origen.equals("TF")){
					pdfDoc.setCell("Número de Recibo:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
					pdfDoc.setCell(sol.getIg_num_referencia(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
				}
				pdfDoc.setCell(
					"En caso de que la disposición de los recursos no se lleve a cabo total o parcialmente\n"+
					"en la fecha estipulada, Nacional Financiera aplicará el cobro de una comisión\n"+
					"sobre los montos no dispuestos, conforme a las políticas de crédito aplicables\n"+
					"a este tipo de operaciones."
				,"formas",ComunesPDF.CENTER,2,1,1);
			}
			pdfDoc.addTable();
			tipoClase=0;
			pdfDoc.addText("\n","formas",ComunesPDF.LEFT);
			pdfDoc.setTable(2,90,widthsDocs);
			
			if(!origen.equals("TFE")){
			pdfDoc.setCell("Datos del Ejecutivo Nafin","celda04",ComunesPDF.LEFT,2,1,0);
			pdfDoc.setCell("Nombre del Ejecutivo","formas",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(sol.getNombre_ejecutivo(),"formas",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Correo electrónico",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(sol.getCg_mail(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Teléfono",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(sol.getCg_telefono(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Fax",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(sol.getCg_fax(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			}
			
			tipoClase=0;
			 tipooper = sol.getTipoOper();
			if(!origen.equals("TFE")){
			pdfDoc.setCell("Datos del Intermediario Financiero/Acreditado","celda04",ComunesPDF.LEFT,2,1,0);
			pdfDoc.setCell("Tipo de operación","formas",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(tipooper,"formas",ComunesPDF.LEFT,1,1,0);
			
			if(!"Primer Piso".equals(tipooper)){
				pdfDoc.setCell("Nombre del Intermediario",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell((!"".equals(sol.getNombre_if_ci()))?sol.getNombre_if_ci():sol.getNombre_if_cs(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				if(!"IF".equals(strTipoUsuario)&&!"".equals(sol.getRiesgoIf())){
					pdfDoc.setCell("Riesgo del Intermediario",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getRiesgoIf(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				}
				}	
			}
			if(!"IF".equals(strTipoUsuario)&&!"".equals(sol.getCg_acreditado())){
				pdfDoc.setCell("Nombre del Acreditado",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(sol.getCg_acreditado(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				if("Primer Piso".equals(tipooper)){
					if(!origen.equals("TFE")){
						pdfDoc.setCell("Riesgo del Acreditado",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell(sol.getRiesgoAc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					}
				}
			}
			tipoClase=1;
			pdfDoc.setCell("Características de la Operación","celda04",ComunesPDF.LEFT,2,1,0);
			if(!"".equals(sol.getCg_programa())){
				pdfDoc.setCell("Nombre del programa o\nacreditado final:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(sol.getCg_programa(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);	
			}
			pdfDoc.setCell("Monto total requerido:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(sol.getFn_monto(),2)+(sol.getIc_moneda().equals("1")?" Pesos":" Dólares"),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Tipo de tasa requerida:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(sol.getTipo_tasa(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Número de Disposiciones",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(sol.getIn_num_disp(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			if(Integer.parseInt(sol.getIn_num_disp())>1){
				pdfDoc.setCell("Disposiciones múltiples. Tasa de Interés:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(("S".equals(sol.getCs_tasa_unica())?"Única":"Varias"),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			}
			pdfDoc.setCell("Plazo de la operación:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(sol.getIg_plazo()+" Días",((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Esquema de recuperación del capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(sol.getEsquema_recup(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			if(!"1".equals(sol.getIc_esquema_recup())){
				String ic_esquema_recup = sol.getIc_esquema_recup();//Valores que puede tomar ic_esquema_recup :
																		 //1=Cupon cero,2=Tradicional,3=Plan de pagos,4=Rentas
				if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("3")){
					pdfDoc.setCell("Periodicidad del pago de Interés:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					//pdfDoc.setCell(consulta.getIg_ppi()+" "+consulta.descripcionUT(consulta.getCg_ut_ppi()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.periodoTasa(sol.getCg_ut_ppi()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				}
				if(!"3".equals(sol.getIc_esquema_recup())){
					if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("4")){
						pdfDoc.setCell("Periodicidad del Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						//pdfDoc.setCell(consulta.getIg_ppc()+" "+consulta.descripcionUT(consulta.getCg_ut_ppc()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell(sol.periodoTasa(sol.getCg_ut_ppc()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					}
				}
				if( ic_esquema_recup.equals("2") && !"".equals(sol.getCg_pfpc())){
					pdfDoc.setCell("Primer fecha de pago de Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getCg_pfpc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Penúltima fecha de pago de Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getCg_pufpc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				}
			}
			pdfDoc.addTable();	
			
			pdfDoc.setTable(4,90);
			pdfDoc.setCell("Detalle de Disposiciones","celda04",ComunesPDF.LEFT,4,1,0);
			pdfDoc.setCell("Número de Disposición","formas",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Monto","formas",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Fecha de Disposición","formas",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Fecha de Vencimiento","formas",ComunesPDF.LEFT,1,1,0);
			for(int i=0;i<Integer.parseInt(sol.getIn_num_disp());i++){
				clase = ((i%2)!=0)?"formas":"celda01";
				pdfDoc.setCell((i+1)+"",clase,ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(sol.getFn_monto_disp(i),2),clase,ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(sol.getDf_disposicion(i),clase,ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(sol.getDf_vencimiento(i),clase,ComunesPDF.LEFT,1,1,0);
			}
			pdfDoc.addTable();
			
			if((sol.getPagos()).size() > 0){
				ArrayList alFilas = sol.getPagos();
				pdfDoc.setTable(3,90);
				pdfDoc.setCell("Plan de Pagos Capital","celda04",ComunesPDF.LEFT,3,1,0);
				pdfDoc.setCell("Número de Pago","formas",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("Fecha de Pago","formas",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("Monto a pagar","formas",ComunesPDF.LEFT,1,1,0);
				for(int i=0;i<alFilas.size();i++){
					ArrayList alColumnas = (ArrayList)alFilas.get(i);
					clase = ((i%2)!=0)?"formas":"celda01";
					pdfDoc.setCell(alColumnas.get(0).toString(),clase,ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(alColumnas.get(1).toString(),clase,ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(alColumnas.get(2).toString(),2),clase,ComunesPDF.LEFT,1,1,0);
				}
				pdfDoc.addTable();
			}
			
			if(!"TF".equals(origen) && !"TFE".equals(origen) ){
				if( estatusSolici.equals("2") || estatusSolici.equals("5") ){
					pdfDoc.setTable(1,90);
					pdfDoc.setCell("TASA INDICATIVA","celda04",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Tasa Indicativa con oferta mismo día:  "+sol.getDescTasaIndicativa()+" "+sol.getIg_tasa_md() + " % anual pagadera "+periodo+"\n"+
									"Tasa Indicativa con oferta 48 horas:     "+sol.getDescTasaIndicativa()+" "+sol.getIg_tasa_spot()+ " % anual pagadera "+periodo
									,"celda01",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Nota:\n\n","formas",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Tasa con oferta Mismo Día: El Intermediario Financiero tiene la opción de tomarla en firme el mismo día que haya recibido la cotización indicativa a más tardar a las 12:30 horas.\n\n"
						,"formas",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Tasa con oferta 48 Horas: El Intermediario Financiero tiene la opción de tomarla en firme a más tardar dos días hábiles después de que la Tesorería haya proporcionado la cotización indicativa antes de las 12:30 horas del segundo día hábil."
						,"formas",ComunesPDF.LEFT,1,1,0);				
					pdfDoc.setCell("\nLas cotizaciones indicativas proporcionadas por la Tesorería de Nacional Financiera son de uso reservado para el intermediario solicitante y será responsabilidad de dicho intermediario el mal uso de la información proporcionada."
						,"formas",ComunesPDF.LEFT,1,1,0);				
			//		pdfDoc.setCell("Tasa Mismo Día: El intermediario Financiero tiene la opción de tomarla en firme el mismo día que haya recibido la respuesta antes de las 12:30 pm.","formas",ComunesPDF.LEFT,1,1,1);
					//pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
			//		pdfDoc.setCell("Tasa 48 Horas: El intermediario Financiero tiene la opción de tomarla en firme a mas tardar dos días hábiles después contados a partir de la fecha que haya recibido respuesta.","formas",ComunesPDF.LEFT,1,1,1);
					pdfDoc.addTable();
				}
		
				if( estatusSolici.equals("7") || estatusSolici.equals("8") || estatusSolici.equals("9")){
					pdfDoc.setTable(2,90);
					pdfDoc.setCell("TASA CONFIRMADA","celda04",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("FECHA Y HORA DE "+("8".equals(estatusSolici)?"CANCELACION":"CONFIRMACION"),"celda04",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
					if(sol.getCs_aceptado_md().equals("S"))
						pdfDoc.setCell("Tasa Confirmada " +sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_md(),2) + " % anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
					else
						pdfDoc.setCell("Tasa Confirmada " +sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_spot(),2) + "% anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
					pdfDoc.setCell(("8".equals(estatusSolici)?sol.getDf_cotizacion()+" "+sol.getHora_cotizacion():sol.getDf_aceptacion()) ,"celda01",ComunesPDF.LEFT,1,1,1);
					pdfDoc.addTable();
				}
			}else{

					boolean mismoDiaOk = autSol.validaMismoDia(estatusSolici);
					pdfDoc.setTable(1,90);
					pdfDoc.setCell("TASA COTIZADA","celda04",ComunesPDF.LEFT,1,1,0);
					
					if(mismoDiaOk)
						pdfDoc.setCell("Tasa con oferta mismo dia" +sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_md(),2) + " % anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
					else
						pdfDoc.setCell("Tasa con oferta 48 horas" +sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_spot(),2) + "% anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
					pdfDoc.addTable();
			}
			
			if(!sol.getCg_observaciones().trim().equals("")){
				pdfDoc.setTable(3,90);
				pdfDoc.setCell("Observaciones:","celda04",ComunesPDF.LEFT,3,1,0);
				pdfDoc.setCell(sol.getCg_observaciones(),"formas",ComunesPDF.LEFT,3,1,0);
				pdfDoc.addTable();
			}			 
	
			
			pdfDoc.endDocument();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			} catch(Exception e) {}
		
		}
		
		String archivo = (String)nombreArchivo;//cs.file(strDirectorioTemp,nombreArchivo,"PDF",pais,noNaf,sExt,strNo,strUs,logo,dirPu);
		String newArchivo = strDirectorioTemp+archivo;
		String consultaUno = 	"{\"success\": true,\"contador1\":"+contador+" ,\"contador3\": "+contador3+" ,\"contador5\": "+contador5+
										" ,\"contador6\": "+contador6+" ,\"contador7\": "+contador7+" ,\"contador8\": "+contador8+" ,\"contador9\": "+contador9+" ,\"contador10\": "+contador10+" ,\"contador11\": "+contador11+
										" ,\"registros\": "+datosUno+" ,\"registros2\": "+datosDos+" ,\"registros3\": "+datosTres+" ,\"registros5\": "+datosCinco+
										" ,\"registros6\": "+datosSeis+" ,\"registros7\": "+datosSiete+" ,\"registros8\": "+datosOcho+" ,\"registros9\": "+datosNueve+" ,\"registros10\": "+datosDiez+
										" ,\"registros11\": "+datosOnce+" ,\"botonRecotizar\": "+botonRecotizar+" ,\"botonSalir1\": "+botonSalir1+" ,\"cancelado\": "+cancelado+" ,\"miPDF\": "+archivo+"}";
		jsonObj = JSONObject.fromObject(consultaUno);
		}//fin if
	}catch(Exception e){
		log.debug("Error: "+ e);
		String consultaErr = 	"{\"success\": false"+"}";
		jsonObj = JSONObject.fromObject(consultaErr);
	}
	
	
	infoRegresar = jsonObj.toString();

	
} 

%>
<%=  infoRegresar %>