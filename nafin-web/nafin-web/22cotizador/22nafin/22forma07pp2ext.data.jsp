<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		java.math.*,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		com.netro.cotizador.*,
		com.netro.cotizador.Solicitud,
		netropology.utilerias.usuarios.Usuario,
		java.util.Date"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion        = request.getParameter("informacion")        == null ? "": (String)request.getParameter("informacion");
String nombre_ejecutivo   = request.getParameter("nombre_ejecutivo")   == null ? "": (String)request.getParameter("nombre_ejecutivo");
String cg_mail            = request.getParameter("cg_mail")            == null ? "": (String)request.getParameter("cg_mail");
String cg_telefono        = request.getParameter("cg_telefono")        == null ? "": (String)request.getParameter("cg_telefono");
String cg_fax             = request.getParameter("cg_fax")             == null ? "": (String)request.getParameter("cg_fax");
String ejecutivo_nafin    = request.getParameter("ejecutivo_nafin")    == null ? "": (String)request.getParameter("ejecutivo_nafin");
String ig_tipo_piso       = request.getParameter("ig_tipo_piso")       == null ? "": (String)request.getParameter("ig_tipo_piso");
String cg_razon_social_if = request.getParameter("cg_razon_social_if") == null ? "": (String)request.getParameter("cg_razon_social_if");
String ig_riesgo_if       = request.getParameter("ig_riesgo_if")       == null ? "": (String)request.getParameter("ig_riesgo_if");
String cg_acreditado      = request.getParameter("cg_acreditado")      == null ? "": (String)request.getParameter("cg_acreditado");
String ig_valor_riesgo_ac = request.getParameter("ig_valor_riesgo_ac") == null ? "": (String)request.getParameter("ig_valor_riesgo_ac");
String df_solicitud       = request.getParameter("df_solicitud")       == null ? "": (String)request.getParameter("df_solicitud");
String df_cotizacion      = request.getParameter("df_cotizacion")      == null ? "": (String)request.getParameter("df_cotizacion");
String cg_programa        = request.getParameter("cg_programa")        == null ? "": (String)request.getParameter("cg_programa");
String ic_moneda          = request.getParameter("ic_moneda")          == null ? "": (String)request.getParameter("ic_moneda");
String fn_monto           = request.getParameter("fn_monto")           == null ? "": (String)request.getParameter("fn_monto");
String ic_tipo_tasa       = request.getParameter("ic_tipo_tasa")       == null ? "": (String)request.getParameter("ic_tipo_tasa");
String in_num_disp        = request.getParameter("in_num_disp")        == null ? "": (String)request.getParameter("in_num_disp");
String ig_plazo_conv      = request.getParameter("ig_plazo_conv")      == null ? "": (String)request.getParameter("ig_plazo_conv");
String cg_ut_plazo        = request.getParameter("cg_ut_plazo")        == null ? "": (String)request.getParameter("cg_ut_plazo");
String ig_plazo           = request.getParameter("ig_plazo")           == null ? "": (String)request.getParameter("ig_plazo");
String ic_esquema_recup   = request.getParameter("ic_esquema_recup")   == null ? "": (String)request.getParameter("ic_esquema_recup");
String cg_ut_ppi          = request.getParameter("cg_ut_ppi")          == null ? "": (String)request.getParameter("cg_ut_ppi");
String ig_ppi             = request.getParameter("ig_ppi")             == null ? "": (String)request.getParameter("ig_ppi");
String cg_ut_ppc          = request.getParameter("cg_ut_ppc")          == null ? "": (String)request.getParameter("cg_ut_ppc");
String ig_ppc             = request.getParameter("ig_ppc")             == null ? "": (String)request.getParameter("ig_ppc");
String pfpc               = request.getParameter("pfpc")               == null ? "": (String)request.getParameter("pfpc");
String pufpc              = request.getParameter("pufpc")              == null ? "": (String)request.getParameter("pufpc");
String idcurva            = request.getParameter("idcurva")            == null ? "": (String)request.getParameter("idcurva");
String id_interpolacion   = request.getParameter("id_interpolacion")   == null ? "": (String)request.getParameter("id_interpolacion");
String metodoCalculo      = request.getParameter("metodoCalculo")      == null ? "": (String)request.getParameter("metodoCalculo");
String ic_solic_esp       = request.getParameter("ic_solic_esp")       == null ? "": (String)request.getParameter("ic_solic_esp");
String ic_ejecutivo       = request.getParameter("ic_ejecutivo")       == null ? "": (String)request.getParameter("ic_ejecutivo");
String ig_plazo_max_oper  = request.getParameter("ig_plazo_max_oper")  == null ? "": (String)request.getParameter("ig_plazo_max_oper");
String ig_num_max_disp    = request.getParameter("ig_num_max_disp")    == null ? "": (String)request.getParameter("ig_num_max_disp");
String ig_plazo_conv_ant  = request.getParameter("ig_plazo_conv_ant")  == null ? "": (String)request.getParameter("ig_plazo_conv_ant");
String num_disp_capt      = request.getParameter("num_disp_capt")      == null ? "": (String)request.getParameter("num_disp_capt");
String fn_monto_grid      = request.getParameter("fn_monto_grid")      == null ? "": (String)request.getParameter("fn_monto_grid");
String disposicion_grid   = request.getParameter("disposicion_grid")   == null ? "": (String)request.getParameter("disposicion_grid");
String vencimiento_grid   = request.getParameter("vencimiento_grid")   == null ? "": (String)request.getParameter("vencimiento_grid");
String ic_proc_pago       = request.getParameter("ic_proc_pago")       == null ? "": (String)request.getParameter("ic_proc_pago");

String nombreArchivo  = "";
String urlArchivo     = "";
String mensaje        = "";
String consulta       = "";
String infoRegresar   = "";
JSONObject resultado  = new JSONObject();
boolean success       = true;

log.debug("***** informacion:"  + informacion + "*****");

if(informacion.equals("Panel_Ayuda")){

	StringBuffer ayuda = new StringBuffer();
	ayuda.append("<TABLE BORDER='0' CELLSPACING='1' CELLPADDING='5' WIDTH='90%' align='center'>"                                                   );
	ayuda.append("<TR>"                                                                                                                            );
	ayuda.append("<TD ALIGN='center' class='titulos'><strong><font size='+1'>CARGA MASIVA DE TABLA DE AMORTIZACIÓN</font></strong></TD>"           );
	ayuda.append("</TR>"                                                                                                                           );
	ayuda.append("</TABLE>"                                                                                                                        );
	ayuda.append("<TABLE BORDER='0' CELLSPACING='1' CELLPADDING='5' WIDTH='90%' align='center'>"                                                   );
	ayuda.append("<TR>"                                                                                                                            );
	ayuda.append("<TD ALIGN='center'><img src='../../01principal/imagenes/carga_masiva1.jpg' alt='Tamaño original' border='0'></TD>"                                          );
	ayuda.append("</TR>"                                                                                                                           );
	ayuda.append("</TABLE>"                                                                                                                        );
	ayuda.append("<div ALIGN='left' class='titulos'>"                                                                                              );
	ayuda.append("<P>1. El documento debe ser un archivo TXT.</P><BR>"                                                                             );
	ayuda.append("<P>2. Los campos debe ser divididos por pipes <strong><font color='#0000FF'>\"|\"</font></strong> y sin espacios</P><BR>"        );
	ayuda.append("<P>3. Número de pagos debe ser:</P>"                                                                                             );
	ayuda.append("<P>&nbsp; a) Enteros</P>"                                                                                                        );
	ayuda.append("<P>&nbsp; b) No debe de ser mayor de tres dígitos. por ejemplo: <strong><font color='#0000FF'>999</font></strong></P>"           );
	ayuda.append("<P>&nbsp; c) Deben ser consecutivos.</P><BR>"                                                                                    );
	ayuda.append("<P>4. La fecha debe tener el formato.</P>"                                                                                       );
	ayuda.append("<P>&nbsp; a) <strong><font color='#0000FF'>dd/mm/aaaa</font></strong>, donde <strong><font color='#0000FF'>dd</font></strong>: " );
	ayuda.append("indica el día, <strong><font   color='#0000FF'>mm</font></strong>: indica el mes y <strong><font color='#0000FF'>aaaa: </font>"  );
	ayuda.append("</strong>indica el año, por ejemplo: <strong><font color='#0000FF'>01/02/2005</font></strong></P><BR>"                           );
	ayuda.append("<P>5. El monto estará formado por enteros y decimales para indicar los decimales es necesario un punto, "                        ); 
	ayuda.append("por ejemplo: <strong><font color='#0000FF'>1000000.25</font></strong></P>"                                                       );
	ayuda.append("</div>"                                                                                                                          );

	resultado.put("success",   new Boolean(success));
	resultado.put("mensaje",   mensaje             );
	resultado.put("registros", ayuda.toString()    );
	infoRegresar = resultado.toString();

} else if(informacion.equals("Carga_Archivo")){

	String rutaArchivo   = "";
	StringBuffer resumen = null;
	StringBuffer errores = null;
	String linea         = "";
	ArrayList allPagos   = new ArrayList();

	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType);
	request.setAttribute("myContentType", myContentType);
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try{
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(2097152);
		myUpload.upload();
	} catch(Exception e){
		success = false;
		log.error("Error al cargar en memoria el contenido del archivo");
		mensaje = "Error al cargar en memoria el contenido del archivo. " + e;
		e.printStackTrace();
		if(myUpload.getSize() > 2097152) {
			mensaje = "Error, el Archivo es muy Grande, excede el Límite que es de 2 MB. " + e;
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 2 MB.");
		} else{
			mensaje = "Problemas al Subir el Archivo. " + e;
			throw new AppException("Problemas al Subir el Archivo.");
		}
	}
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest = myUpload.getRequest();
	// Guardar Archivo en Disco
	int numFiles = 0;
	try {
		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," "));
		resultado.put("fileName", 	myFile.getFileName().replaceAll("\\s+"," "));
		nombreArchivo = myFile.getFileName().replaceAll("\\s+"," ");
	}catch(Exception e){
		success = false;
		log.error("Error al Guardar Archivo en Disco");
		mensaje = "Error al Guardar Archivo en Disco. " + e;
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
	
	if(success == true){

		SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
		Solicitud solicitud         = new Solicitud();

		solicitud.setFn_monto(Double.parseDouble(fn_monto.trim()));
		solicitud.setFn_monto1(fn_monto.trim());
		solicitud.setIc_ejecutivo(ic_ejecutivo.trim());
		solicitud.setCg_telefono(cg_telefono.trim());
		solicitud.setCg_fax(cg_fax.trim());
		solicitud.setCg_mail(cg_mail.trim());
		solicitud.setNombre_ejecutivo(nombre_ejecutivo.trim());
		solicitud.setDf_solicitud(df_solicitud.trim());
		solicitud.setDf_cotizacion(df_cotizacion.trim());
		solicitud.setCg_programa(cg_programa.trim());
		solicitud.setIc_moneda(ic_moneda.trim());
		solicitud.setIc_tipo_tasa(ic_tipo_tasa.trim());
		solicitud.setIn_num_disp(in_num_disp.trim());
		solicitud.setIg_tipo_piso(ig_tipo_piso.trim());
		solicitud.setIg_plazo_max_oper(ig_plazo_max_oper.trim());
		solicitud.setIg_num_max_disp(ig_num_max_disp.trim());
		solicitud.setIg_plazo(ig_plazo.trim());
		solicitud.setIc_esquema_recup(ic_esquema_recup.trim());
		solicitud.setIg_ppi(ig_ppi.trim());
		solicitud.setCg_ut_ppi(cg_ut_ppi.trim());
		solicitud.setIg_ppc(ig_ppc.trim());
		solicitud.setCg_ut_ppc(cg_ut_ppc.trim());
		solicitud.setCg_razon_social_if(cg_razon_social_if.trim());
		solicitud.setIg_riesgo_if(ig_riesgo_if.trim());
		solicitud.setCg_acreditado(cg_acreditado.trim());
		solicitud.setIg_valor_riesgo_ac(ig_valor_riesgo_ac.trim());
		solicitud.setCg_ut_plazo(cg_ut_plazo.trim());
		solicitud.setCg_pfpc(pfpc.trim());
		solicitud.setCg_pufpc(pufpc.trim());
		solicitud.setIdCurva(idcurva.trim());
		solicitud.setMetodoCalculo(metodoCalculo);
		solicitud.setId_interpolacion(id_interpolacion);
		solicitud.setFn_monto1(Comunes.formatoDecimal(fn_monto,2,false));

		String[] df_disposicion = null;
		String[] df_vencimiento = null;
		String[] fn_monto_disp = null;

		df_disposicion = disposicion_grid.split(",");
		df_vencimiento = vencimiento_grid.split(",");
		fn_monto_disp  = fn_monto_grid.split(",");

		solicitud.setFn_monto_disp(fn_monto_disp);
		solicitud.setDf_disposicion(df_disposicion);
		solicitud.setDf_vencimiento(df_vencimiento);

		// Leo el archivo para ser validado
		rutaArchivo = strDirectorioTemp + iNoUsuario + "." + nombreArchivo;
		java.io.File lfArchivo = new java.io.File(rutaArchivo);
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(lfArchivo)));
		StringBuffer lsbDocumentos = new StringBuffer();
		while( (linea = br.readLine()) != null ) {
			if(!"".equals(linea))
				allPagos.add(linea);
		}

		solicitud = solCotEsp.guardaPagosMasiva(solicitud, allPagos);
		ic_proc_pago = "" + solicitud.getIc_proc_pago();
		resumen = solicitud.getResumen();
		errores = solicitud.getErrores();
		
	}

	// Enviar resultado de la operacion
	resultado.put("success",      new Boolean(success));
	resultado.put("mensaje",      mensaje);
	resultado.put("correctos",    resumen.toString());
	resultado.put("errores",      errores.toString());
	resultado.put("ic_proc_pago", ic_proc_pago);
	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>