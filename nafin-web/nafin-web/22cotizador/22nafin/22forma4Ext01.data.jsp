<%@ page contentType="text/html;charset=UTF-8"
	import="
		java.util.*,
		com.netro.cotizador.*,
		com.netro.exception.*,
		javax.naming.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		org.apache.commons.logging.Log  " 
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<jsp:useBean id="consulta" scope="page" class="com.netro.cotizador.Solicitud" />
<%@ include file="../22secsession_extjs.jspf" %>

<%    
	String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
	String txt_num_solic		= request.getParameter("numSolicitud")==null?"":request.getParameter("numSolicitud");
	String infoRegresar	= "", mensaje = "";
	String txt_origen = "NB";
	String observaciones		= request.getParameter("observaciones")==null?"":request.getParameter("observaciones");
	String clave		= request.getParameter("clave")==null?"":request.getParameter("clave");
	
	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
	
	String tec = "";
	String periodo = "";
	
	JSONObject jsonObj = new JSONObject();
	List detalle = new ArrayList();
	List planPago = new ArrayList();
	Vector detalles = null;
	if(informacion.equals("enviar")){
		String txt_clave = "", txt_estatus = "", txt_intermediario = "", txt_observaciones = "";
		String auxFecha = "";
		try {
			detalles = solCotEsp.getClaveSolicitud(txt_num_solic);
			txt_clave = detalles.get(0).toString();
			txt_estatus = detalles.get(1).toString(); 
			txt_intermediario = detalles.get(2).toString(); 
			txt_observaciones = detalles.get(3).toString(); 
			jsonObj.put("success", new Boolean(true));
			if(!"7".equals(txt_estatus) && !"10".equals(txt_estatus) && !"2".equals(txt_estatus) && !"5".equals(txt_estatus)) {
				mensaje = "La solicitud no esta en el estatus correspondiente";
			}else{
				if("2".equals(txt_estatus)&&"IF".equals(strTipoUsuario)) {
					autSol.setEstatusSolic(txt_clave, "5", txt_estatus);
				}
				consulta = solCotEsp.consultaSolicitud(txt_clave);
				String clase="";
				int tipoClase=0;
				if("3".equals(consulta.getIc_tipo_tasa())){
					tec = solCotEsp.techoTasaProtegida();
				}
				String nombreArchivo= consulta.getNumero_solicitud()+".pdf";
				if( txt_estatus.equals("10") && consulta.getPagos()!=null && consulta.getPagos().size()>0 ){//10-Confirmada Vencida
					session.setAttribute("pagos2",consulta.getPagos());
				}
				if( !txt_estatus.equals("1") & !txt_estatus.equals("3") & !txt_estatus.equals("4") ){
					auxFecha = "Cotizaci&oacute;n";
					if("8".equals(txt_estatus)){
						auxFecha = "Cancelaci&oacute;n";
					}
				}
				jsonObj.put("nombreArchivo", nombreArchivo);
				jsonObj.put("in_num_disp", consulta.getIn_num_disp());
				jsonObj.put("num_solicitud", consulta.getNumero_solicitud());
				jsonObj.put("fecha_solicitud", consulta.getDf_solicitud());
				jsonObj.put("hora_solocitud", consulta.getHora_solicitud());
				jsonObj.put("fecha_citiza_aux", "Fecha de " );
				jsonObj.put("fecha_citiza", consulta.getDf_cotizacion());
				jsonObj.put("hora_cotiza_aux", "Hora de ");
				jsonObj.put("hora_cotiza", consulta.getHora_cotizacion());
				jsonObj.put("nombre_usuario", consulta.getIc_usuario()+"-"+consulta.getNombre_usuario());
				jsonObj.put("txt_estatus", txt_estatus);
				jsonObj.put("numero_recibo", consulta.getIg_num_referencia());
				jsonObj.put("mensaje_cotiza", "En caso de que la disposici&oacute;n de los recursos no se lleve a cabo total o parcialmente en la fecha estipulada, Nacional Financiera aplicar&aacute; el cobro de una comisi&oacute;n sobre los montos no dispuestos, conforme a las pol&iacute;ticas de cr&eacute;dito aplicables a este tipo de operaciones.");
				
				//Datos del Ejecutivo Nafin
				jsonObj.put("nombre_ejecutivo", consulta.getNombre_ejecutivo());
				jsonObj.put("correo", consulta.getCg_mail());
				jsonObj.put("telefono", consulta.getCg_telefono());
				jsonObj.put("fax", consulta.getCg_fax());
				
				//Datos del Intermediario Financiero/Acreditado
				jsonObj.put("tipo_operacion", consulta.getTipoOper());
				jsonObj.put("nombre_intermediario", (!"".equals(consulta.getNombre_if_ci()))?consulta.getNombre_if_ci():consulta.getNombre_if_cs());
				jsonObj.put("strTipoUsuario", strTipoUsuario);
				jsonObj.put("riesgo_intermediero", consulta.getRiesgoIf());
				jsonObj.put("nombre_acreditado",consulta.getCg_acreditado());
				jsonObj.put("riesgo_acreditado",consulta.getRiesgoAc());
				
				//Caracteristicas de la operación 
				jsonObj.put("nombre_programa", consulta.getCg_programa());
				jsonObj.put("monto_total","$ "+String.valueOf(Comunes.formatoDecimal(consulta.getFn_monto(),2)));
				jsonObj.put("moneda", consulta.getIc_moneda());
				jsonObj.put("tipo_tasa_requerida", consulta.getTipo_tasa());
				jsonObj.put("numero_disposicion",consulta.getIn_num_disp());
				jsonObj.put("disposicion_multiple","S".equals(consulta.getCs_tasa_unica())?"Unica":"Varias");
				jsonObj.put("plazo_operacion",consulta.getIg_plazo() +" Días");
				jsonObj.put("recuperacion_capital", consulta.getEsquema_recup());
				jsonObj.put("ic_recuperacion_capital",consulta.getIc_esquema_recup());
				jsonObj.put("perioricidad_pago",consulta.periodoTasa(consulta.getCg_ut_ppi()));
				jsonObj.put("perioricidad_capital",consulta.periodoTasa(consulta.getCg_ut_ppc()));
				jsonObj.put("cg_pfpc",consulta.getCg_pfpc());
				jsonObj.put("primer_pago_capital",consulta.getCg_pfpc());
				jsonObj.put("penultimo_pago_capital",consulta.getCg_pufpc());
				for(int i=0;i<Integer.parseInt(consulta.getIn_num_disp());i++){
					HashMap hash = new HashMap();
					hash.put("NUMERO_DISPOSICION",String.valueOf(i+1));
					hash.put("MONTO", "$ "+Comunes.formatoDecimal(consulta.getFn_monto_disp(i),2));
					hash.put("FECHA_DISPOSICION",consulta.getDf_disposicion(i));
					hash.put("FECHA_VENCIMIENTO", consulta.getDf_vencimiento(i));
					detalle.add(hash);
				}
				if (detalle != null){
					JSONArray auxDetalle = new JSONArray();
					auxDetalle = JSONArray.fromObject(detalle);
					jsonObj.put("gridDisposicion", auxDetalle);
				}
				// plan de pagos capital
				jsonObj.put("pagos_tam",String.valueOf(consulta.getPagos().size()));
				if((consulta.getPagos()).size() > 0){
					ArrayList alFilas = consulta.getPagos();
					for(int i=0;i<alFilas.size();i++){
						HashMap hash = new HashMap();
						ArrayList alColumnas = (ArrayList)alFilas.get(i);
						hash.put("NUMERO_PAGO",(String)alColumnas.get(0));
						hash.put("FECHA_PAGO", (String)alColumnas.get(1));
						hash.put("MONTO_PAGO","$ "+Comunes.formatoDecimal(alColumnas.get(2).toString(),2));
						planPago.add(hash);
					}
				}
				if (planPago != null){
					JSONArray auxPlan = new JSONArray();
					auxPlan = JSONArray.fromObject(planPago);
					jsonObj.put("gridPlanPagoCapital", auxPlan);
				}
				if("1".equals(consulta.getIc_esquema_recup())){
						periodo = "Al Vencimiento";
				}else if("4".equals(consulta.getIc_esquema_recup())&&"0".equals(consulta.getCg_ut_ppc())){
					periodo = "Al Vencimiento";
				}else if( !consulta.periodoTasa(consulta.getCg_ut_ppi()).equals("") ){
					periodo = consulta.periodoTasa(consulta.getCg_ut_ppi())+"mente";
				}else if( !consulta.periodoTasa(consulta.getCg_ut_ppc()).equals("") ){
					periodo = consulta.periodoTasa(consulta.getCg_ut_ppc())+"mente";
				}else{
					periodo = "Al Vencimiento";
				}
				String mansajeAlert = "";
				if("I".equals(consulta.getTipoCotizacion())&&"IF".equals(strTipoUsuario)){
					mansajeAlert = "COTIZACION INDICATIVA,<br> NO ES POSIBLE TOMAR EN FIRME LA OPERACION,<br> FAVOR DE CONSULTAR A SU EJECUTIVO.";
				}else if("N".equals(consulta.getCs_vobo_ejec())&&"IF".equals(strTipoUsuario)){
					mansajeAlert = "Para realizar la toma en firme favor de ponerse en contacto <br>con su ejecutivo";
				}
				jsonObj.put("tipo_cotizacion",consulta.getTipoCotizacion());
				jsonObj.put("cs_vobo",consulta.getCs_vobo_ejec());
				jsonObj.put("mansajeAlert",mansajeAlert);
				jsonObj.put("tasa_ind_oferta_mismo_dia",consulta.getDescTasaIndicativa()+" "+String.valueOf(Comunes.formatoDecimal(consulta.getIg_tasa_md(),2))+"  anual pagadera  "+periodo);
				jsonObj.put("ig_tasa_sport",(Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2)));
				jsonObj.put("tasaIndCuarOcho",consulta.getDescTasaIndicativa()+" "+String.valueOf(Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2))+" "+"anual pagadera"+" "+periodo);
				// tasa confirmada 
				jsonObj.put("fechaHoras","FECHA Y HORA DE "+("8".equals(txt_estatus)?"CANCELACION":"CONFIRMACION"));
				jsonObj.put("cs_aceptado_md",consulta.getCs_aceptado_md());
				if(consulta.getCs_aceptado_md().equals("S")){
					jsonObj.put("r_tasa_confirmada","Tasa Confirmada"+" "+consulta.getDescTasaIndicativa()+" "+String.valueOf(Comunes.formatoDecimal(consulta.getIg_tasa_md(),2))+" anual pagadera "+periodo+" "+tec);
				}else {
					jsonObj.put("r_tasa_confirmada","Tasa Confirmada"+" "+consulta.getDescTasaIndicativa()+" "+String.valueOf(Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2))+" anual pagadera "+periodo+" "+tec);
				}
				jsonObj.put("df_cotiza",("8".equals(txt_estatus)?consulta.getDf_cotizacion()+" "+consulta.getHora_cotizacion():consulta.getDf_aceptacion()));
				//observaciones 
				jsonObj.put("obser", consulta.getCg_observaciones().trim());
				jsonObj.put("txt_obsr", txt_observaciones);
				jsonObj.put("txt_clave", txt_clave);
			}
		} catch(NafinException ex) {
			jsonObj.put("success", new Boolean(true));
			mensaje = "No se encontro la solicitud, verifique el folio y que este en el estatus correspondiente.";
		}
		jsonObj.put("mensaje", mensaje);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("Confirmar")){
		jsonObj = new JSONObject();
		try {
				solCotEsp.setRechazaTomaFirme(clave, observaciones);	
				txt_origen = "C";
			} catch(NafinException ex) {
				txt_num_solic = "";
				mensaje = "Hubo un problema al actualizar la solicitud.";
			}
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("mensaje", mensaje);
			jsonObj.put("observaciones", observaciones);
			jsonObj.put("txt_origen", txt_origen);
			infoRegresar = jsonObj.toString();
	}
%>


<%=  infoRegresar %>
