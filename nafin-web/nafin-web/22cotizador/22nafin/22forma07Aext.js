Ext.onReady(function(){

/*****************************************************************************
 Variables globales
******************************************************************************/
	var fm = Ext.form;
	var jsonDataEncode;
	var monto = '';
	var dispo = '';
	var venc  = '';
	var pagos = '';
	var recordType     = Ext.data.Record.create([{name:'clave'}, {name:'descripcion'}]);
	var opcionPantalla = Ext.getDom('opcionPantalla').value; // Determina si es una cotizaci�n nueva (AGREGAR) o se edita una existente (EDITAR).
	var dfCotizacion   = Ext.getDom('df_cotizacion').value;
	var icSolicEsp     = Ext.getDom('icSolicEsp').value;
	var Tmes = new Array();
	var sCadena;
	Tmes[1]  = '31';
	Tmes[2]  = '28';
	Tmes[3]  = '31';
	Tmes[4]  = '30';
	Tmes[5]  = '31';
	Tmes[6]  = '30';
	Tmes[7]  = '31';
	Tmes[8]  = '31';
	Tmes[9]  = '30';
	Tmes[10] = '31';
	Tmes[11] = '30';
	Tmes[12] = '31';

/**************************************************************************************************************
 *                                             OVERRIDES                                                      *
 * Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del componente.            *
 * Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth  *
 * BY: Jesus Salim Hernandez David.                                                                           *
 **************************************************************************************************************/
	Ext.override(Ext.Element,{
		getWidth: function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

/*****************************************************************************
PANTALLA: COTIZADOR DE CREDITOS
******************************************************************************/

	/***** Regresa a la pantalla anterior *****/
	function procesoSalir(){
		window.location.href='22forma07ext.jsp';
	}

	/***** Inicializa la pantalla. Si hay que editar se cargan los datos *****/
	function procesoInicializar(){

		/***** Cargo los combos *****/
		catalogoRiesgoIntermediario.load();
		catalogoMoneda.load();
		catalogoTasaInteres.load();
		catalogoRecCapital.load();

		if(opcionPantalla.trim() == 'AGREGAR'){

			catalogoCurva.load({
				params: Ext.apply({
					ic_moneda: '1'
				})
			});

			Ext.getCmp('titulos_0').hide();
			Ext.getCmp('nombre_ejecutivo_id').hide();
			Ext.getCmp('cg_mail_id').hide();
			Ext.getCmp('cg_telefono_id').hide();
			Ext.getCmp('cg_fax_id').hide();

			Ext.getCmp('ejecutivo_nafin_id').hide();

			Ext.getCmp('cg_razon_social_if_id').hide();
			Ext.getCmp('ig_riesgo_if_id').hide();
			Ext.getCmp('fecha_pago_cap_id').hide();

			Ext.getCmp('ig_tipo_piso_id').setValue('1');
			Ext.getCmp('fn_monto_id').setValue('0.00');
			Ext.getCmp('tipo_monto_id').setValue('');
			Ext.getCmp('in_num_disp_id').setValue('1');
			Ext.getCmp('ig_plazo_conv_id').setValue('0');
			Ext.getCmp('ig_plazo_conv_ant_id').setValue('0');
			Ext.getCmp('ig_plazo_id').setValue('0');
			Ext.getCmp('cg_ut_plazo_id').setValue('30');
			Ext.getCmp('idcurva_id').setValue('1');
			Ext.getCmp('id_interpolacion_id').setValue('L');
			Ext.getCmp('metodoCalculo_id').setValue('P');

			Ext.Ajax.request({
				url:      '22forma07Aext.data.jsp',
				params:   Ext.apply({ informacion: 'Primera_Cotizacion'}),
				callback: procesarPrimeraCotizacion
			});

		} else if(opcionPantalla.trim() == 'EDITAR'){

			formaPrincipal.el.mask('Cargando datos...', 'x-mask-loading');

			if(icSolicEsp.trim() != ''){

				Ext.Ajax.request({
					url: '22forma07Aext.data.jsp',
					params: Ext.apply({
						informacion:   'Consulta_Cotizacion',
						icSolicEsp:    icSolicEsp.trim(),
						df_cotizacion: dfCotizacion.trim()
					}),
					callback: procesarDatosCotizacion
				});
			} else{
				Ext.Msg.alert('Error...', 'Ocurri� un error al obtener los datos.');
			}
		} else if(opcionPantalla.trim() == 'RECARGAR'){

			formaPrincipal.el.mask('Cargando datos...', 'x-mask-loading');

			Ext.Ajax.request({
				url: '22forma07Aext.data.jsp',
				params: Ext.apply({
					informacion:        'Recarga_Pagina',
					nombre_ejecutivo:   (Ext.getDom('nombre_ejecutivo').value).trim(),
					cg_mail:            (Ext.getDom('cg_mail').value).trim(),
					cg_telefono:        (Ext.getDom('cg_telefono').value).trim(),
					cg_fax:             (Ext.getDom('cg_fax').value).trim(),
					ejecutivo_nafin:    (Ext.getDom('ejecutivo_nafin').value).trim(),
					ig_tipo_piso:       (Ext.getDom('ig_tipo_piso').value).trim(),
					cg_razon_social_if: (Ext.getDom('cg_razon_social_if').value).trim(),
					ig_riesgo_if:       (Ext.getDom('ig_riesgo_if').value).trim(),
					cg_acreditado:      (Ext.getDom('cg_acreditado').value).trim(),
					ig_valor_riesgo_ac: (Ext.getDom('ig_valor_riesgo_ac').value).trim(),
					df_solicitud:       (Ext.getDom('df_solicitud').value).trim(),
					df_cotizacion:      (Ext.getDom('df_cotizacion').value).trim(),
					cg_programa:        (Ext.getDom('cg_programa').value).trim(),
					ic_moneda:          (Ext.getDom('ic_moneda').value).trim(),
					fn_monto:           (Ext.getDom('fn_monto').value).trim(),
					ic_tipo_tasa:       (Ext.getDom('ic_tipo_tasa').value).trim(),
					in_num_disp:        (Ext.getDom('in_num_disp').value).trim(),
					ig_plazo_conv:      (Ext.getDom('ig_plazo_conv').value).trim(),
					cg_ut_plazo:        (Ext.getDom('cg_ut_plazo').value).trim(),
					ig_plazo:           (Ext.getDom('ig_plazo').value).trim(),
					ic_esquema_recup:   (Ext.getDom('ic_esquema_recup').value).trim(),
					cg_ut_ppi:          (Ext.getDom('cg_ut_ppi').value).trim(),
					ig_ppi:             (Ext.getDom('ig_ppi').value).trim(),
					cg_ut_ppc:          (Ext.getDom('cg_ut_ppc').value).trim(),
					ig_ppc:             (Ext.getDom('ig_ppc').value).trim(),
					pfpc:               (Ext.getDom('pfpc').value).trim(),
					pufpc:              (Ext.getDom('pufpc').value).trim(),
					idcurva:            (Ext.getDom('idcurva').value).trim(),
					id_interpolacion:   (Ext.getDom('id_interpolacion').value).trim(),
					metodoCalculo:      (Ext.getDom('metodoCalculo').value).trim(),
					icSolicEsp:         (Ext.getDom('icSolicEsp').value).trim(),
					ic_ejecutivo:       (Ext.getDom('ic_ejecutivo').value).trim(),
					ig_plazo_max_oper:  (Ext.getDom('ig_plazo_max_oper').value).trim(),
					ig_num_max_disp:    (Ext.getDom('ig_num_max_disp').value).trim(),
					ig_plazo_conv_ant:  (Ext.getDom('ig_plazo_conv_ant').value).trim(),
					num_disp_capt:      (Ext.getDom('num_disp_capt').value).trim(),
					fn_monto_disp:      (Ext.getDom('fn_monto_grid').value).trim(),
					df_disposicion:     (Ext.getDom('disposicion_grid').value).trim(),
					df_vencimiento:     (Ext.getDom('vencimiento_grid').value).trim()
				}),
				callback: procesarDatosCotizacion
			});
		}
	}

	/***** Se carga el grid por primera vez cuando es una Nueva Cotizaci�n *****/
	var procesarPrimeraCotizacion = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			capturaDisposicion.loadData(Ext.util.JSON.decode(response.responseText).regGrid);
		}
	}

	/***** Se cargan los datos en el form, se muestran y ocultan los campos correspondientes *****/
	var procesarDatosCotizacion = function(opts, success, response){

		var fp = Ext.getCmp('formaPrincipal');

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){

			var ic_moneda        = Ext.util.JSON.decode(response.responseText).registro.ic_moneda;
			var ic_esquema_recup = Ext.util.JSON.decode(response.responseText).registro.ic_esquema_recup;
			
			// Este combo depende del tipo de moneda, por eso se carga en esta secci�n
			catalogoCurva.load({
				params: Ext.apply({
					ic_moneda: ic_moneda
				})
			});

			// Este combo depende del tipo de moneda, por eso se carga en esta secci�n
			catalogoTasaInteres.load({
				params: Ext.apply({
					ic_moneda: ic_moneda
				})
			});

			// Este combo depende del esquema de recuperaci�n, por eso se carga en esta secci�n
			catalogoUnidadTiempo1.load({
				params: Ext.apply({
					ic_esquema_recup: ic_esquema_recup
				})
			});

			// Este combo depende del esquema de recuperaci�n, por eso se carga en esta secci�n
			catalogoUnidadTiempo2.load({
				params: Ext.apply({
					ic_esquema_recup: ic_esquema_recup
				})
			});

			/***** Asigno los datos al form principal *****/
			valorFormulario = Ext.util.JSON.decode(response.responseText).registro;
			fp.getForm().setValues(valorFormulario);

			if(opcionPantalla.trim() == 'RECARGAR'){
				pagos = Ext.getDom('pagos').value;
			} else{
				pagos = Ext.util.JSON.decode(response.responseText).pagos; // Este en java es un ArrayList
			}

			/***** Muestro y oculto los campos del form seg�n los datos obtenidos *****/
			if(opcionPantalla.trim() == 'RECARGAR'){
				Ext.getCmp('titulos_0').hide();
				Ext.getCmp('nombre_ejecutivo_id').setValue('');
				Ext.getCmp('nombre_ejecutivo_id').hide();
				Ext.getCmp('cg_mail_id').setValue('');
				Ext.getCmp('cg_mail_id').hide();
				Ext.getCmp('cg_telefono_id').setValue('');
				Ext.getCmp('cg_telefono_id').hide();
				Ext.getCmp('cg_fax_id').setValue('');
				Ext.getCmp('cg_fax_id').hide();
			} else if(Ext.getCmp('nombre_ejecutivo_id').getValue() != '' && ( Ext.getCmp('cg_mail_id').getValue() != '' || Ext.getCmp('cg_telefono_id').getValue() != '' || Ext.getCmp('cg_fax_id').getValue() != '' )){
				Ext.getCmp('titulos_0').show();
				Ext.getCmp('nombre_ejecutivo_id').show();
				Ext.getCmp('cg_mail_id').show();
				Ext.getCmp('cg_telefono_id').show();
				Ext.getCmp('cg_fax_id').show();
			} else{
				Ext.getCmp('titulos_0').hide();
				Ext.getCmp('nombre_ejecutivo_id').hide();
				Ext.getCmp('cg_mail_id').hide();
				Ext.getCmp('cg_telefono_id').hide();
				Ext.getCmp('cg_fax_id').hide();
			}

			if(Ext.getCmp('mensaje_error_id').getValue() != ''){
				Ext.getCmp('mensaje_error_id').show();
			} else{
				Ext.getCmp('mensaje_error_id').hide();
			}

			if(Ext.getCmp('str_tipo_usuario_id').getValue() == 'IF'){
				Ext.getCmp('ejecutivo_nafin_id').show();
				Ext.getCmp('ig_tipo_piso_id').hide();
				Ext.getCmp('cg_acreditado_id').hide();
			} else{
				Ext.getCmp('ejecutivo_nafin_id').hide();
				Ext.getCmp('ig_tipo_piso_id').show();
				Ext.getCmp('cg_acreditado_id').show();
				if(Ext.util.JSON.decode(response.responseText).registro.ig_tipo_piso == '2'){
					Ext.getCmp('riesgo_acreditado_id').hide();
					Ext.getCmp('cg_razon_social_if_id').show();
					Ext.getCmp('ig_riesgo_if_id').show();
				} else if(Ext.util.JSON.decode(response.responseText).registro.ig_tipo_piso == '1'){
					Ext.getCmp('riesgo_acreditado_id').show();
					Ext.getCmp('cg_razon_social_if_id').hide();
					Ext.getCmp('ig_riesgo_if_id').hide();
				}
			}

			if(Ext.getCmp('ic_moneda_id').getValue() == '1'){
				Ext.getCmp('tipo_monto_id').setValue('Pesos');
			} else if(Ext.getCmp('ic_moneda_id').getValue() == '54'){
				Ext.getCmp('tipo_monto_id').setValue('D&oacute;lares');
			}

			/***** Actualiza esquema de recuperaci�n *****/
			actualizaEsquemaRecuperacion();

			if(Ext.getCmp('ic_esquema_recup_id').getValue() != '1'){
				if(Ext.getCmp('ic_esquema_recup_id').getValue() != '4'){
					Ext.getCmp('periodicidad_pago_interes_id').show();
				} else{
					Ext.getCmp('periodicidad_pago_interes_id').hide();
				}

				if(Ext.getCmp('ic_esquema_recup_id').getValue() != '3'){
					Ext.getCmp('periodicidad_cap_unidad_tiempo_id').show();
					Ext.getCmp('cg_ut_ppc_id').show();
				} else{
					Ext.getCmp('periodicidad_cap_unidad_tiempo_id').hide();
					Ext.getCmp('cg_ut_ppc_id').hide();
				}

				if(Ext.getCmp('ic_esquema_recup_id').getValue() == '3' || (Ext.getCmp('ic_esquema_recup_id').getValue() == '2' && Ext.getCmp('cg_ut_ppc_id').getValue() != '')){
					Ext.getCmp('periodicidad_cap_num_pagos_id').show();
					Ext.getCmp('ig_ppc_id').show();
				} else{
					Ext.getCmp('periodicidad_cap_num_pagos_id').hide();
					Ext.getCmp('ig_ppc_id').hide();
				}

				if(Ext.getCmp('ic_esquema_recup_id').getValue() == '2' && Ext.getCmp('cg_ut_ppc_id').getValue() != '0'){
					if(Ext.getCmp('pfpc_id').getValue() != ''){//if(pfpc != ''){
						Ext.getCmp('fecha_pago_cap_id').show();
					} else{
						Ext.getCmp('fecha_pago_cap_id').hide();
					}
				} else{
					Ext.getCmp('fecha_pago_cap_id').hide();
				}
			} else{
				Ext.getCmp('periodicidad_pago_interes_id').hide();
				Ext.getCmp('periodicidad_cap_unidad_tiempo_id').hide();
				Ext.getCmp('cg_ut_ppc_id').hide();
				Ext.getCmp('periodicidad_cap_num_pagos_id').hide();
				Ext.getCmp('ig_ppc_id').hide();
				Ext.getCmp('fecha_pago_cap_id').hide();
			}
			
			/***** Calcula la PPI *****/
			calculaPPI();
			
			/***** Asigno los datos al form Captura Disposiciones*****/
			capturaDisposicion.loadData(Ext.util.JSON.decode(response.responseText).regGrid);
			Ext.getCmp('monto_total_id').setValue(Ext.util.JSON.decode(response.responseText).monto_total);
			Ext.getCmp('num_disp_capt_id').setValue(Ext.util.JSON.decode(response.responseText).num_disp_capt);
			if(Ext.util.JSON.decode(response.responseText).total > 1){
				Ext.getCmp('nota_id').show();
			} else{
				Ext.getCmp('nota_id').hide();
			}
			
		} else{
			Ext.Msg.alert('Error...', Ext.util.JSON.decode(response.responseText).mensaje);
		}

		fp.el.unmask();

	}

	/***** Actualiza esquema de recuperaci�n *****/
	function actualizaEsquemaRecuperacion(){
        Ext.getCmp('ig_ppc_id').setReadOnly(true);
		if(Ext.getCmp('ic_esquema_recup_id').getValue() == '1'){

			Ext.getCmp('esquema_recup_id').setValue('Un solo pago de capital e inter�s al vencimiento');
			Ext.getCmp('periodicidad_cap_unidad_tiempo_id').hide();
			Ext.getCmp('cg_ut_ppc_id').hide();
			Ext.getCmp('periodicidad_cap_num_pagos_id').hide();
			Ext.getCmp('ig_ppc_id').hide();
			Ext.getCmp('per_capital_id').hide(); //Periodicidad de capital
			Ext.getCmp('periodicidad_pago_interes_id').hide(); //Periodicidad del pago de inter�s
			Ext.getCmp('fecha_pago_cap_id').hide(); //Fecha de pago de Capital

		} else if(Ext.getCmp('ic_esquema_recup_id').getValue() == '2'){

			Ext.getCmp('esquema_recup_id').setValue('Pagos iguales de capital e intereses sobre saldo insolutos (incluye el caso particular de un solo pago de capital al vencimiento e intereses peri�dicos)');
			Ext.getCmp('periodicidad_cap_unidad_tiempo_id').show();
			Ext.getCmp('cg_ut_ppc_id').show();
			Ext.getCmp('periodicidad_cap_num_pagos_id').show();
			Ext.getCmp('ig_ppc_id').show();
			Ext.getCmp('per_capital_id').show(); //Periodicidad de capital
			Ext.getCmp('periodicidad_pago_interes_id').show(); //Periodicidad del pago de inter�s
			Ext.getCmp('fecha_pago_cap_id').hide(); //Fecha de pago de Capital

		} else if(Ext.getCmp('ic_esquema_recup_id').getValue() == '3'){ //Plan de Pagos

			Ext.getCmp('esquema_recup_id').setValue('Favor de anexar tabla de amortizaci�n incluyendo fecha de pago');
			Ext.getCmp('periodicidad_cap_unidad_tiempo_id').hide();
			Ext.getCmp('cg_ut_ppc_id').hide();
			Ext.getCmp('periodicidad_cap_num_pagos_id').show();
			Ext.getCmp('ig_ppc_id').show();
            Ext.getCmp('ig_ppc_id').setReadOnly(false);
			Ext.getCmp('per_capital_id').show(); //Periodicidad de capital
			Ext.getCmp('periodicidad_pago_interes_id').show(); //Periodicidad del pago de inter�s
			Ext.getCmp('fecha_pago_cap_id').hide(); //Fecha de pago de Capital

		} else if(Ext.getCmp('ic_esquema_recup_id').getValue() == '4'){

			Ext.getCmp('esquema_recup_id').setValue('Pago de capital m�s inter�s constantes con capital creciente');
			Ext.getCmp('periodicidad_cap_unidad_tiempo_id').show();
			Ext.getCmp('cg_ut_ppc_id').show();
			Ext.getCmp('periodicidad_cap_num_pagos_id').hide();
			Ext.getCmp('ig_ppc_id').hide();
			Ext.getCmp('per_capital_id').show(); //Periodicidad de capital
			Ext.getCmp('periodicidad_pago_interes_id').hide(); //Periodicidad del pago de inter�s
			Ext.getCmp('fecha_pago_cap_id').hide(); //Fecha de pago de Capital
	
		}
	}

	/***** Calcula la PPI *****/
	function calculaPPI(){
		if(Ext.getCmp('ic_esquema_recup_id').getValue() == '2' || Ext.getCmp('ic_esquema_recup_id').getValue() == '3'){

			var plazoConv = parseFloat( Ext.getCmp('ig_plazo_conv_id').getValue());
			var utPlazo   = parseFloat( Ext.getCmp('cg_ut_plazo_id').getValue());
			var utppi     = parseFloat( Ext.getCmp('cg_ut_ppi_id').getValue());

			if(Ext.getCmp('cg_ut_ppi_id').getValue() != ''){
				Ext.getCmp('ig_ppi_id').setValue(parseInt(plazoConv*utPlazo/utppi));
			} else{
				Ext.getCmp('ig_ppi_id').setValue('');
			}

			if(Ext.getCmp('ic_esquema_recup_id').getValue() == '2' && Ext.getCmp('cg_ut_ppc_id').getValue() != ''){
				var utppc= parseFloat(Ext.getCmp('cg_ut_ppc_id').getValue());
				if(Ext.getCmp('cg_ut_ppc_id').getValue() != '' && Ext.getCmp('ig_ppi_id').getValue() != ''){
					var plazomeses = (plazoConv*utPlazo)/30;
					var igppc = parseInt((plazomeses*30)/utppc);
					Ext.getCmp('ig_ppc_id').setValue(parseInt(igppc));
				}
			}

		}
	}

	/***** Recarga los datos del form principal y del grid *****/
	function recargaDatos(){

		// Primero obtengo los datos del grid
		var datar = new Array();
		var jsonDataEncode = '';
		var records = capturaDisposicion.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		jsonDataEncode = Ext.util.JSON.encode(datar);
		
		// Env�o los datos para actualizarlos
		Ext.Ajax.request({
			url: '22forma07Aext.data.jsp',
			params: Ext.apply({
				informacion:       'Recarga_Cotizacion',
				cg_ut_plazo:       Ext.getCmp('cg_ut_plazo_id').getValue(),
				ig_plazo_conv:     Ext.getCmp('ig_plazo_conv_id').getValue(),
				ig_plazo_conv_ant: Ext.getCmp('ig_plazo_conv_ant_id').getValue(),
				ig_plazo:          Ext.getCmp('ig_plazo_id').getValue(),
				fn_monto:          Ext.getCmp('fn_monto_id').getValue(),
				in_num_disp:       Ext.getCmp('in_num_disp_id').getValue(),
				df_cotizacion:     new Date(Ext.getCmp('df_cotizacion_id').getValue()).format('d/m/Y'),
				ic_esquema_recup:  Ext.getCmp('ic_esquema_recup_id').getValue(),
				datos_grid:        jsonDataEncode				
			}),
			callback: procesarRecargaDatos
		});
	}

	/***** Se recargan solo los datos actualizados  *****/
	var procesarRecargaDatos = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.getCmp('ig_plazo_conv_id').setValue(Ext.util.JSON.decode(response.responseText).ig_plazo_conv);
			Ext.getCmp('ig_plazo_conv_ant_id').setValue(Ext.util.JSON.decode(response.responseText).ig_plazo_conv_ant);
			Ext.getCmp('ig_plazo_id').setValue(Ext.util.JSON.decode(response.responseText).ig_plazo);
			Ext.getCmp('cg_ut_plazo_id').setValue(Ext.util.JSON.decode(response.responseText).cg_ut_plazo);
			
			/***** Asigno los datos al form Captura Disposiciones*****/
			capturaDisposicion.loadData(Ext.util.JSON.decode(response.responseText).regGrid);
			Ext.getCmp('monto_total_id').setValue(Ext.util.JSON.decode(response.responseText).monto_total);
			Ext.getCmp('num_disp_capt_id').setValue(Ext.util.JSON.decode(response.responseText).num_disp_capt);
			if(Ext.util.JSON.decode(response.responseText).total > 1){
				Ext.getCmp('nota_id').show();
			} else{
				Ext.getCmp('nota_id').hide();
			}

			/***** Calcula la PPI *****/
			calculaPPI();

		} else{
			Ext.Msg.alert('Error...', Ext.util.JSON.decode(response.responseText).mensaje);
		}
	}

	/***** Valida el form y envia los adtos a 22forma07Bext.jsp *****/
	function procesoContinuar(opcion){
	
		if(!Ext.getCmp('formaPrincipal').getForm().isValid()){
			return;
		}

		// Variable para obtener el valor de los checbox tipo de operaci�n
		var pantalla = '';
		var valor = (Ext.getCmp('ig_tipo_piso_id')).getValue();

		if(Ext.getCmp('str_tipo_usuario_id').getValue() != 'IF'){
			// Valida tipo de operaci�n
			if(Ext.getCmp('ig_tipo_piso_id').getValue() == null){
				Ext.getCmp('ig_tipo_piso_id').markInvalid('Este campo es obligatorio');
				return;
			}
			// Valida el intermediario y el riesgo del intermediario
			if(valor.getGroupValue() == '2'){
				if(Ext.getCmp('cg_razon_social_if_id').getValue() == ''){
					Ext.getCmp('cg_razon_social_if_id').markInvalid('Este campo es obligatorio');
					return;
				}
				if(Ext.getCmp('ig_riesgo_if_id').getValue() == ''){
					Ext.getCmp('ig_riesgo_if_id').markInvalid('Este campo es obligatorio');
					return;
				}
			}

			// Valida que la fecha de solicitud no est� vacia
			if(Ext.getCmp('df_solicitud_id').getValue() == ''){
				Ext.getCmp('df_solicitud_id').markInvalid('Este campo es obligatorio');
				return;
			}

			// Valida que la fecha de cotizaci�n no est� vacia
			if(Ext.getCmp('df_cotizacion_id').getValue() == ''){
				Ext.getCmp('df_cotizacion_id').markInvalid('Este campo es obligatorio');
				return;
			}

		}

		// Valida el riesgo del acreditado
		if(Ext.getCmp('ig_tipo_piso_id').getValue() != null){
			if(valor.getGroupValue() == '1' && Ext.getCmp('ig_valor_riesgo_ac_id').getValue() == ''){
				Ext.getCmp('ig_valor_riesgo_ac_id').markInvalid('Este campo es obligatorio');
				return;
			}
		}

		// Valida el n�mero de disposiciones.

		if(Ext.getCmp('in_num_disp_id').getValue() == '' || Ext.getCmp('in_num_disp_id').getValue() == '0.00'){
			Ext.getCmp('in_num_disp_id').markInvalid('El n�mero de disposiciones debe ser mayor a cero.');
			return;
		}

		if(Ext.getCmp('in_num_disp_id').getValue() != '' && Ext.getCmp('ig_num_max_disp_id').getValue() != ''){
			if(parseInt(Ext.getCmp('in_num_disp_id').getValue()) > parseInt(Ext.getCmp('ig_num_max_disp_id').getValue())){
				Ext.getCmp('in_num_disp_id').markInvalid('El n�mero de disposiciones es mayor al par�metro establecido. <br>Favor de corregirlo.');
				return;
			}
		}

		if(parseInt(Ext.getCmp('in_num_disp_id').getValue()) > 1 && parseInt(Ext.getCmp('ic_moneda_id').getValue()) != 1){
			Ext.getCmp('in_num_disp_id').markInvalid('El n�mero de disposiciones es mayor al par�metro establecido. <br>Favor de corregirlo.');
			return;
		}
		
		
	//	if(valor.getGroupValue() == '2'){
			// Se valida el moto total requerido
			if(Ext.getCmp('fn_monto_id').getValue() == '' || parseInt(Ext.getCmp('fn_monto_id').getValue()) == 0){
				Ext.getCmp('fn_monto_id').markInvalid('El monto total requerido debe ser mayor a cero.');
				return;
			}
	//	}

		// Se ejecuta una vez que todos los campos son correctos para mostrar el grid
		if(opcion == 'CONSULTA_DISPOSICIONES'){
			recargaDatos();
			Ext.getCmp('formaPrincipal').hide();
			Ext.getCmp('formaCapturaDisp').show();

		} else if(opcion == 'CONTINUAR'){ // Ya estan capturadas las disposiciones

			/***** Si no se han capturado las disposiciones, se llenan los arrays sin validar el grid *****/
			if(monto == '' && dispo == '' && venc == ''){
			/* LAs siguentes cuatro lineas es como yo creo que deber�a funcionar,
			 *	ya que necesariamente se tienen que capturar las disposiciones y validarlas en el grid
			 */
				recargaDatos();
				Ext.getCmp('formaPrincipal').hide();
				Ext.getCmp('formaCapturaDisp').show();
				return;
/***** El siguiente bloque de c�digo es para que funcione como en la versi�n original
				var montoVec         = new Array();
				var dispoVec         = new Array();
				var vencVec          = new Array();
				var records          = capturaDisposicion.getRange();
				monto                = '';
				dispo                = '';
				venc                 = '';

				for (var i = 0; i < records.length; i++){
					montoVec.push(records[i].data.MONTO);
					dispoVec.push((new Date(records[i].data.FECHA_DISP)).format('d/m/Y'));
					vencVec.push((new Date(records[i].data.FECHA_VENC)).format('d/m/Y'));
					exito = true;
				}

				monto = montoVec.toString();
				ispo  = dispoVec.toString();
				venc  = vencVec.toString();

				// Estos campos se llenan para comparar las fechas con el form de CAPTURA SOLICITUD DE FECHAS
				Ext.getCmp('fecha_disp_hid_id').setValue((new Date(records[0].data.FECHA_DISP)).format('d/m/Y'));
				Ext.getCmp('fecha_venc_hid_id').setValue((new Date(records[0].data.FECHA_VENC)).format('d/m/Y'));
 *****/
			}

			if(Ext.getCmp('idcurva_id').getValue() == ''){
				Ext.getCmp('idcurva_id').markInvalid('Este campo es obligatorio');
				return;
			}

			if(parseInt(Ext.getCmp('num_disp_capt_id').getValue()) == parseInt(Ext.getCmp('in_num_disp_id').getValue()) && parseFloat((Ext.getCmp('monto_total_id').getValue()).replace('$','')) == parseFloat(Ext.getCmp('fn_monto_id').getValue() )){

				pantalla = '22forma07Bext.jsp';

				if(Ext.getCmp('ic_esquema_recup_id').getValue() == ''){
					Ext.getCmp('ic_esquema_recup_id').markInvalid('Este campo es obligatorio');
					return;
				}

				if(Ext.getCmp('ic_esquema_recup_id').getValue() != '1'){ // No es cupon cero

					if(Ext.getCmp('ic_esquema_recup_id').getValue() != '4'){

						// Valida la periodicidad de pago de interes
						if(Ext.getCmp('cg_ut_ppi_id').getValue() == ''){
							Ext.getCmp('cg_ut_ppi_id').markInvalid('Este campo es obligatorio');
							return;
						}
						if(Ext.getCmp('ig_ppi_id').getValue() == ''){
							Ext.getCmp('ig_ppi_id').markInvalid('Este campo es obligatorio');
							return;
						}
					}

					if(Ext.getCmp('ic_esquema_recup_id').getValue() == '3' || (Ext.getCmp('ic_esquema_recup_id').getValue() == '2' && Ext.getCmp('cg_ut_ppc_id').getValue() != '0')){
						// Valida la periodicidad de pago de capital
						if(Ext.getCmp('ig_ppc_id').getValue() == ''){
							Ext.getCmp('ig_ppc_id').markInvalid('Este campo es obligatorio');
							return;
						}
					} else{
						// Valida la unidad de tiempo de la periodicidad de pago de capital
						if(Ext.getCmp('cg_ut_ppc_id').getValue() == ''){
							Ext.getCmp('cg_ut_ppc_id').markInvalid('Este campo es obligatorio');
							return;
						}
					}

					if(Ext.getCmp('ic_esquema_recup_id').getValue() == '2'){
						var ppc           = parseFloat(Ext.getCmp('ig_ppc_id').getValue());
						var ut_ppc        = parseFloat(Ext.getCmp('cg_ut_ppc_id').getValue());
						var ig_plazo_conv = parseFloat(Ext.getCmp('ig_plazo_conv_id').getValue());
						var cg_ut_plazo   = parseFloat(Ext.getCmp('cg_ut_plazo_id').getValue());
						if(ut_ppc != 0){
							if(ppc == 0){
								Ext.getCmp('cg_ut_ppc_id').markInvalid('Los pagos de capital deben ser mayores a 0.');
								return;
							}
							if((ppc*ut_ppc) > ((ig_plazo_conv+1)*cg_ut_plazo)){
								Ext.Msg.alert('Mensaje...', 'El n�mero de pagos de capital debe ser menor al plazo de la operaci�n.');
								return;
							}
						}
					}

				} else{ // Si es cupon cero

					if(Ext.getCmp('ic_tipo_tasa_id').getValue() == '2'){
						if(parseInt(Ext.getCmp('ig_plazo_conv_id').getValue()) > 366){
							Ext.Msg.alert('Mensaje...', 'S�lo se puede cotizar tasa variable para cupon cero menor a un a�o.');
							return;
						}
					}
				}

				if(Ext.getCmp('ic_esquema_recup_id').getValue() == '3'){
					pantalla = '22forma07pp1ext.jsp';
				}
			}

			if(Ext.getCmp('ic_esquema_recup_id').getValue() == '2' && Ext.getCmp('cg_ut_ppc_id').getValue() != '0' && icSolicEsp.trim() == ''){

				pantalla = '';

				Ext.getCmp('formaCapturaDisp').hide();
				Ext.getCmp('formaSolicitudFechas').show();
				Ext.getCmp('formaPrincipal').hide();
				return;
			}
			if(pantalla != ''){
				redireccionaPagina(pantalla);
			}
		} // FIN: opcion = CONTINUAR

	}

	/***** Direcciona a la pagina siguiente *****/
	function redireccionaPagina(pagina){

			var valor = (Ext.getCmp('ig_tipo_piso_id')).getValue();

			window.location.href=pagina                                                                    +
			'?nombre_ejecutivo='   + Ext.getCmp('nombre_ejecutivo_id').getValue()                          +
			'&cg_mail='            + Ext.getCmp('cg_mail_id').getValue()                                   +
			'&cg_telefono='        + Ext.getCmp('cg_telefono_id').getValue()                               +
			'&cg_fax='             + Ext.getCmp('cg_fax_id').getValue()                                    +
			'&ejecutivo_nafin='    + Ext.getCmp('ejecutivo_nafin_id').getValue()                           +
			'&ig_tipo_piso='       + valor.getGroupValue()                                                 +
			'&cg_razon_social_if=' + Ext.getCmp('cg_razon_social_if_id').getValue()                        +
			'&ig_riesgo_if='       + Ext.getCmp('ig_riesgo_if_id').getValue()                              +
			'&cg_acreditado='      + Ext.getCmp('cg_acreditado_id').getValue()                             +
			'&ig_valor_riesgo_ac=' + Ext.getCmp('ig_valor_riesgo_ac_id').getValue()                        +
			'&df_solicitud='       + (new Date(Ext.getCmp('df_solicitud_id').getValue()).format('d/m/Y'))  +
			'&df_cotizacion='      + (new Date(Ext.getCmp('df_cotizacion_id').getValue()).format('d/m/Y')) +
			'&cg_programa='        + Ext.getCmp('cg_programa_id').getValue()                               +
			'&ic_moneda='          + Ext.getCmp('ic_moneda_id').getValue()                                 +
			'&fn_monto='           + Ext.getCmp('fn_monto_id').getValue()                                  +
			'&ic_tipo_tasa='       + Ext.getCmp('ic_tipo_tasa_id').getValue()                              +
			'&in_num_disp='        + Ext.getCmp('in_num_disp_id').getValue()                               +
			'&ig_plazo_conv='      + Ext.getCmp('ig_plazo_conv_id').getValue()                             +
			'&cg_ut_plazo='        + Ext.getCmp('cg_ut_plazo_id').getValue()                               +
			'&ig_plazo='           + Ext.getCmp('ig_plazo_id').getValue()                                  +
			'&ic_esquema_recup='   + Ext.getCmp('ic_esquema_recup_id').getValue()                          +
			'&cg_ut_ppi='          + Ext.getCmp('cg_ut_ppi_id').getValue()                                 +
			'&ig_ppi='             + Ext.getCmp('ig_ppi_id').getValue()                                    +
			'&cg_ut_ppc='          + Ext.getCmp('cg_ut_ppc_id').getValue()                                 +
			'&ig_ppc='             + Ext.getCmp('ig_ppc_id').getValue()                                    +
			'&pfpc='               + (new Date(Ext.getCmp('pfpc_id').getValue()).format('d/m/Y'))        +
			'&pufpc='              + (new Date(Ext.getCmp('pufpc_id').getValue()).format('d/m/Y'))         +
			'&idcurva='            + Ext.getCmp('idcurva_id').getValue()                                   +
			'&id_interpolacion='   + Ext.getCmp('id_interpolacion_id').getValue()                          +
			'&metodoCalculo='      + Ext.getCmp('metodoCalculo_id').getValue()                             +
			'&ic_solic_esp='       + Ext.getCmp('ic_solic_esp_id').getValue()                              +
			'&str_tipo_usuario='   + Ext.getCmp('str_tipo_usuario_id').getValue()                          +
			'&ic_ejecutivo='       + Ext.getCmp('ic_ejecutivo_id').getValue()                              +
			'&ig_plazo_max_oper='  + Ext.getCmp('ig_plazo_max_oper_id').getValue()                         +
			'&ig_num_max_disp='    + Ext.getCmp('ig_num_max_disp_id').getValue()                           +
			'&ig_plazo_conv_ant='  + Ext.getCmp('ig_plazo_conv_ant_id').getValue()                         +
			'&num_disp_capt='      + Ext.getCmp('num_disp_capt_id').getValue()                             +
			'&fecha_venc_hid='     + (new Date(Ext.getCmp('fecha_venc_hid_id').getValue()).format('d/m/Y'))+
			'&fecha_disp_hid='     + (new Date(Ext.getCmp('fecha_disp_hid_id').getValue()).format('d/m/Y'))+
			'&fn_monto_grid='      + monto.toString()                                                      +
			'&disposicion_grid='   + dispo.toString()                                                      +
			'&vencimiento_grid='   + venc.toString()                                                       +
			'&pagos='              + pagos;

	}

	/***** Se crea el store para el combo Riesgo del Intermediario *****/
	var catalogoRiesgoIntermediario = new Ext.data.JsonStore({
		id:     'catalogoRiesgoIntermediario',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07Aext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Riesgo_Intermediario'
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners: {
			load: function(){
				if(Ext.getCmp('ig_riesgo_if_id').getValue()!=''){
					Ext.getCmp('ig_riesgo_if_id').setValue(Ext.getCmp('ig_riesgo_if_id').getValue());
				}
			},
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/***** Se crea el store para el combo Moneda *****/
	var catalogoMoneda = new Ext.data.JsonStore({
		id:     'catalogoMoneda',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07Aext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Moneda'
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners: {
			load: function(){
				if(Ext.getCmp('ic_moneda_id').getValue()!=''){
					Ext.getCmp('ic_moneda_id').setValue(Ext.getCmp('ic_moneda_id').getValue());
				}
			},
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/***** Se crea el store para el combo Tasa de inter�s *****/
	var catalogoTasaInteres = new Ext.data.JsonStore({
		id:     'catalogoTasaInteres',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07Aext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Tasa_Interes'
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners: {
			load: function(){
				if(Ext.getCmp('ic_tipo_tasa_id').getValue()!=''){
					Ext.getCmp('ic_tipo_tasa_id').setValue(Ext.getCmp('ic_tipo_tasa_id').getValue());
				} else{
					var records = catalogoTasaInteres.getRange();
					var tmp = records[0].data.clave;
					Ext.getCmp('ic_tipo_tasa_id').setValue(tmp);
				}
			},
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/***** Se crea el store para el combo Esquema de recuperaci�n de calpital *****/
	var catalogoRecCapital = new Ext.data.JsonStore({
		id:     'catalogoRecCapital',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07Aext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Rec_Capital'
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners: {
			load: function(){
				if(Ext.getCmp('ic_esquema_recup_id').getValue()!=''){
					Ext.getCmp('ic_esquema_recup_id').setValue(Ext.getCmp('ic_esquema_recup_id').getValue());
				}
			},
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/***** Se crea el store para el combo Periodicidad de pago *****/
	var catalogoPeriodicidadPago = new Ext.data.JsonStore({
		id:     'catalogoPeriodicidadPago',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07Aext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Periodicidad_Pago' 
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners: {
			load: function(){
				if(Ext.getCmp('cg_ut_ppi_id').getValue()!=''){
					Ext.getCmp('cg_ut_ppi_id').setValue(Ext.getCmp('cg_ut_ppi_id').getValue());
				}
				if(Ext.getCmp('cg_ut_ppc_id').getValue()!=''){
					Ext.getCmp('cg_ut_ppc_id').setValue(Ext.getCmp('cg_ut_ppc_id').getValue());
				}
			},
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	// Se crea el store para el combo Unidad de tiempo
	var catalogoUnidadTiempo1 = new Ext.data.JsonStore({
		id:     'catalogoUnidadTiempo1',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07Aext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Unidad_Tiempo_1'
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners: {
			load: function(){
				if(Ext.getCmp('cg_ut_ppi_id').getValue()!=''){
					Ext.getCmp('cg_ut_ppi_id').setValue(Ext.getCmp('cg_ut_ppi_id').getValue());
				}
				if(Ext.getCmp('cg_ut_ppc_id').getValue()!=''){
					Ext.getCmp('cg_ut_ppc_id').setValue(Ext.getCmp('cg_ut_ppc_id').getValue());
				}
			},
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	// Se crea el store para el combo Unidad de tiempo
	var catalogoUnidadTiempo2 = new Ext.data.JsonStore({
		id:     'catalogoUnidadTiempo2',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07Aext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Unidad_Tiempo_1'
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners: {
			load: function(){
				if(Ext.getCmp('cg_ut_ppc_id').getValue()!=''){
					Ext.getCmp('cg_ut_ppc_id').setValue(Ext.getCmp('cg_ut_ppc_id').getValue());
				}
			},
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/***** Se crea el store para el combo Curva *****/
	var catalogoCurva = new Ext.data.JsonStore({
		id:     'catalogoCurva',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07Aext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Curva'
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners: {
			load: function(){
				if(Ext.getCmp('idcurva_id').getValue()!=''){
					Ext.getCmp('idcurva_id').setValue(Ext.getCmp('idcurva_id').getValue());
				} else{
					var records = catalogoCurva.getRange();
					var tmp = records[0].data.clave;
					Ext.getCmp('idcurva_id').setValue(tmp);
				}
			},
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/***** Se crea el form principal *****/
	var formaPrincipal = new Ext.form.FormPanel({
		width:               800,
		labelWidth:          180,
		id:                  'formaPrincipal',
		title:               'Cotizador de Cr�ditos',
		layout:              'form',
		style:               'margin: 0 auto;',
		labelAlign:          'right',
		frame:               true,
		hidden:              false,
		border:              true,
		autoHeight:          true,
		items: [{
			width:            788,
			xtype:            'panel',
			id:               'titulos_0',
			title:            'Datos generales del ejecutivo',
			layout:           'table',
			border:           true,
			hidden:           false,
			layoutConfig:     {columns: 1}
		},{
			width:            445,
			xtype:            'displayfield',
			id:               'nombre_ejecutivo_id',
			name:             'nombre_ejecutivo',
			fieldLabel:       'Nombre del ejecutivo',
			msgTarget:        'side',
			value:            '&nbsp;',
			hidden:           false
		},{
			width:            445,
			xtype:            'displayfield',
			id:               'cg_mail_id',
			name:             'cg_mail',
			fieldLabel:       'Correo',
			msgTarget:        'side',
			value:            '&nbsp;',
			hidden:           false
		},{
			width:            445,
			xtype:            'displayfield',
			id:               'cg_telefono_id',
			name:             'cg_telefono',
			fieldLabel:       'Tel�fono',
			msgTarget:        'side',
			value:            '&nbsp;',
			hidden:           false
		},{
			width:            445,
			xtype:            'displayfield',
			id:               'cg_fax_id',
			name:             'cg_fax',
			fieldLabel:       'Fax',
			msgTarget:        'side',
			value:            '&nbsp;',
			hidden:           false
		},{
			width:            788,
			xtype:            'panel',
			id:               'titulos_1',
			title:            'Caracter�sticas de la Operaci�n',
			layout:           'table',
			border:           true,
			hidden:           false,
			layoutConfig:     { columns: 1 }
		},{
			width:            545,
			xtype:            'displayfield',
			id:               'mensaje_error_id',
			name:             'mensaje_error',
			value:            '&nbsp;',
			hidden:           false
		},{
			width:            545,
			xtype:            'textfield',
			id:               'ejecutivo_nafin_id',
			name:             'ejecutivo_nafin',
			fieldLabel:       'Ejecutivo NAFIN',
			msgTarget:        'side',
			allowBlank:       true
		},{
			xtype:            'radiogroup',
			id:               'ig_tipo_piso_id',
			name:             'ig_tipo_piso',
			fieldLabel:       'Seleccione el tipo de operaci�n',
			msgTarget:        'side',
			anchor:           '55%',
			align:            'center',
			cls:              'x-check-group-alt',
			allowBlank:       true,
			items:[{
				boxLabel:      'Primer Piso',
				name:          'ig_tipo_piso',
				inputValue:    '1'
			},{
				boxLabel:      'Segundo Piso',
				name:          'ig_tipo_piso',
				inputValue:    '2'
			}],
			listeners: {
				change: {
					fn: function(){
						var valor = (Ext.getCmp('ig_tipo_piso_id')).getValue();
						if(valor.getGroupValue() == '1'){
							Ext.getCmp('riesgo_acreditado_id').show();
							Ext.getCmp('cg_razon_social_if_id').hide();
							Ext.getCmp('ig_riesgo_if_id').hide();
						} else if(valor.getGroupValue() == '2'){
							Ext.getCmp('riesgo_acreditado_id').hide();
							Ext.getCmp('cg_razon_social_if_id').show();
							Ext.getCmp('ig_riesgo_if_id').show();
						}
					}
				}
			}
		},{
			width:            545,
			xtype:            'textfield',
			id:               'cg_razon_social_if_id',
			name:             'cg_razon_social_if',
			fieldLabel:       'Intermediario',
			msgTarget:        'side',
			allowBlank:       true,
			maxLength:        60
		},{
			width:            545,
			xtype:            'combo',
			id:               'ig_riesgo_if_id',
			name:             'ig_riesgo_if',
			fieldLabel:       'Riesgo del Intermediario',
			emptyText:        'Seleccione ...',
			displayField:     'descripcion',
			valueField:       'clave',
			msgTarget:        'side',
			mode:             'local',
			triggerAction:    'all',
			forceSelection:   true,
			typeAhead:        true,
			allowBlank:       true,
			store:            catalogoRiesgoIntermediario
		},{
			width:            545,
			xtype:            'textfield',
			id:               'cg_acreditado_id',
			name:             'cg_acreditado',
			fieldLabel:       'Acreditado',
			msgTarget:        'side',
			allowBlank:       true,
			maxLength:        100
		},{
			xtype:            'compositefield',
			fieldLabel:       'Riesgo del Acreditado',
			id:               'riesgo_acreditado_id',
			msgTarget:        'side',
			combineErrors:    false,
			hidden:           false,
			items: [{
				width:         510,
				xtype:         'numberfield',
				id:            'ig_valor_riesgo_ac_id',
				name:          'ig_valor_riesgo_ac',
				msgTarget:     'side',
				minValue:      0,
				maxValue:      100,
				allowDecimals: true,
				allowBlank:    true
			},{
				width:         7,
				xtype:         'displayfield',
				value:         '&nbsp;'
			},{
				width:         25,
				xtype:         'displayfield',
				value:         '&nbsp; %'
			}]
		},{
			xtype:            'compositefield',
			fieldLabel:       'Fecha de solicitud',
			msgTarget:        'side',
			combineErrors:    false,
			items: [{
				width:         180,
				xtype:         'datefield',
				id:            'df_solicitud_id',
				name:          'df_solicitud',
				msgTarget:     'side',
				minValue:      '01/01/1901',
				value:         new Date(),
				startDay:      0
			},{
				width:         175,
				xtype:         'displayfield',
				value:         '<div align="right"> Fecha de cotizaci�n: </div>'
			},{
				width:         180,
				xtype:         'datefield',
				id:            'df_cotizacion_id',
				name:          'df_cotizacion',
				msgTarget:     'side',
				minValue:      '01/01/1901',
				value:         new Date(),
				startDay:      0
			}]
		},{
			width:            545,
			xtype:            'textfield',
			id:               'cg_programa_id',
			name:             'cg_programa',
			fieldLabel:       'Nombre del Programa y/o Acreditado Final',
			msgTarget:        'side',
			allowBlank:       true
		},{
			xtype:            'compositefield',
			fieldLabel:       'Moneda',
			msgTarget:        'side',
			combineErrors:    false,
			items: [{
				width:         180,
				xtype:         'combo',
				id:            'ic_moneda_id',
				name:          'ic_moneda',
				emptyText:     'Seleccione ...',
				displayField:  'descripcion',
				valueField:    'clave',
				msgTarget:     'side',
				mode:          'local',
				triggerAction: 'all',
				forceSelection:true,
				typeAhead:     true,
				allowBlank:    false,
				store:         catalogoMoneda,
				listeners:{
					select:{
						fn: function(combo){
							var campo = combo.getValue();
							if(campo != ''){
								Ext.getCmp('ic_tipo_tasa_id').setValue('');
								catalogoTasaInteres.load({
									params: Ext.apply({
										ic_moneda: campo
									})
								});
								Ext.getCmp('idcurva_id').setValue('');
								catalogoCurva.load({
									params: Ext.apply({
										ic_moneda: campo
									})
								});
								// Se actualiza el campo monto toal requerido y el combo Interpolaci�n
								if(campo == 1){
									Ext.getCmp('tipo_monto_id').setValue('PESOS');
									Ext.getCmp('id_interpolacion_id').setValue('L');
								} else {
									Ext.getCmp('tipo_monto_id').setValue('D�LARES');
									Ext.getCmp('id_interpolacion_id').setValue('A');
								}
							}
						}
					}
				}
			},{
				width:         175,
				xtype:         'displayfield',
				value:         '<div align="right"> Monto total requerido: </div>'
			},{
				width:         125,
				xtype:         'textfield',
				id:            'fn_monto_id',
				name:          'fn_monto',
				msgTarget:     'side',
				allowDecimals: true,
				allowBlank:    false,
				maxLength:     20,
				renderer:      Ext.util.Format.numberRenderer('0.00')
			},{
				width:         90,
				xtype:         'displayfield',
				id:            'tipo_monto_id',
				value:         'DOLARES'
			}]
		},{
			width:            180,
			xtype:            'combo',
			id:               'ic_tipo_tasa_id',
			name:             'ic_tipo_tasa',
			fieldLabel:       'Tasa de inter�s requerida',
			emptyText:        'Seleccione ...',
			displayField:     'descripcion',
			valueField:       'clave',
			msgTarget:        'side',
			mode:             'local',
			triggerAction:    'all',
			forceSelection:   true,
			typeAhead:        true,
			allowBlank:       false,
			store:            catalogoTasaInteres
		},{
			xtype:            'compositefield',
			fieldLabel:       'No. de disposiciones',
			msgTarget:        'side',
			combineErrors:    false,
			items: [{
				width:         180,
				xtype:         'numberfield',
				id:            'in_num_disp_id',
				name:          'in_num_disp',
				msgTarget:     'side',
				allowBlank:    false,
				allowDecimals: false,
				minValue:      0,
				maxValue:      99,
				listeners: {
					blur: function(d){
						recargaDatos();
					}
				}
			},{
				width:         175,
				xtype:         'displayfield',
				value:         '&nbsp;'
			},{
				xtype:         'button',
				text:          'Ver tabla de disposiciones',
				id:            'btnDisposiciones',
				iconCls:       'icoBuscar',
				handler:       function(boton, evento){
					procesoContinuar('CONSULTA_DISPOSICIONES');
				}
			}]
		},{
			xtype:            'compositefield',
			fieldLabel:       'Plazo de la operaci�n',
			msgTarget:        'side',
			combineErrors:    false,
			items: [{
				width:         180,
				xtype:         'numberfield',
				id:            'ig_plazo_conv_id',
				name:          'ig_plazo_conv',
				msgTarget:     'side',
				allowBlank:    false,
				allowDecimals: false,
				minValue:      0,
				listeners: {
					blur: function(d){ // var newVal = d.getValue().trim();
						recargaDatos();
					}
				}
			},{
				width:         7,
				xtype:         'displayfield',
				value:         '&nbsp;'
			},{
				width:         140,
				xtype:         'combo',
				id:            'cg_ut_plazo_id',
				name:          'cg_ut_plazo',
				emptyText:     'Seleccione...',
				displayField:  'descripcion',
				valueField:    'clave',
				msgTarget:     'side',
				mode:          'local',
				triggerAction: 'all',
				forceSelection:true,
				typeAhead:     true,
				allowBlank:    false,
				store:         [['30','Meses'], ['90','Trimestres'], ['180','Semestres'], ['360','A�os']],
				listeners: {
					select: {
						fn: function(combo){
							recargaDatos();
						}
					}
				}
			},{
				width:         85,
				xtype:         'displayfield',
				id:            'equivalencia_dias_plazo_operacion0_id',
				value:         '&nbsp; Equivalente a'
			},{
				width:         65,
				xtype:         'textfield',
				id:            'ig_plazo_id',
				name:          'ig_plazo'
			},{
				width:         30,
				xtype:         'displayfield',
				id:            'equivalencia_dias_plazo_operacion1_id',
				value:         '&nbsp; d�as'
			}]
		},{
			xtype:            'compositefield',
			fieldLabel:       'Esquema de recuperaci�n del capital',
			msgTarget:        'side',
			combineErrors:    false,
			autoScroll:       true,
			items: [{
				width:         180,
				xtype:         'combo',
				id:            'ic_esquema_recup_id',
				name:          'ic_esquema_recup',
				emptyText:     'Seleccione...',
				displayField:  'descripcion',
				valueField:    'clave',
				msgTarget:     'side',
				mode:          'local',
				triggerAction: 'all',
				forceSelection:true,
				typeAhead:     true,
				allowBlank:    false,
				store:         catalogoRecCapital,
				listeners: {
					select: {
						fn: function(combo){
							actualizaEsquemaRecuperacion();
							catalogoUnidadTiempo1.load({
								params: {
									ic_esquema_recup: combo.getValue()
								}
							});
							catalogoUnidadTiempo2.load({
								params: {
									ic_esquema_recup: combo.getValue()
								}
							});
						}
					}
				}
			},{
				width:         7,
				xtype:         'displayfield',
				value:         '&nbsp;'
			},{
				width:         350,
				height:        30,
				xtype:         'textarea',
				id:            'esquema_recup_id',
				readOnly:      true,
				autoScroll:    true
			}]
		},{
			xtype:            'compositefield',
			fieldLabel:       'Periodicidad del pago de inter�s',
			id:               'periodicidad_pago_interes_id',
			msgTarget:        'side',
			combineErrors:    false,
			items: [{
				xtype:         'displayfield',
				value:         '<div align="right"> Unidad de tiempo: </div>',
				width:         120
			},{
				width:         150,
				xtype:         'combo',
				id:            'cg_ut_ppi_id',
				name:          'cg_ut_ppi',
				emptyText:     'Seleccione...',
				displayField:  'descripcion',
				valueField:    'clave',
				msgTarget:     'side',
				mode:          'local',
				triggerAction: 'all',
				forceSelection:true,
				typeAhead:     true,
				allowBlank:    true,
				store:         catalogoUnidadTiempo1,
				listeners: {
					select: {
						fn: function(combo){
							recargaDatos();
						}
					}
				}
			},{
				xtype:         'displayfield',
				value:         '<div align="right"> No. pagos: </div>',
				width:         120
			},{
				width:         140,
				xtype:         'numberfield',
				id:            'ig_ppi_id',
				name:          'ig_ppi',
				msgTarget:     'side',
				allowBlank:    true,
				readOnly:      true,
				maxLength:     10
			}]
		},{
			xtype:            'compositefield',
			fieldLabel:       'Periodicidad de capital',
			id:               'per_capital_id',
			msgTarget:        'side',
			combineErrors:    false,
			items: [{
				xtype:         'displayfield',
				id:            'periodicidad_cap_unidad_tiempo_id',
				value:         '<div align="right"> Unidad de tiempo: </div>',
				width:         120
			},{
				width:         150,
				xtype:         'combo',
				id:            'cg_ut_ppc_id',
				name:          'cg_ut_ppc',
				emptyText:     'Seleccione...',
				displayField:  'descripcion',
				valueField:    'clave',
				msgTarget:     'side',
				mode:          'local',
				triggerAction: 'all',
				forceSelection:true,
				typeAhead:     true,
				allowBlank:    true,
				store:         catalogoUnidadTiempo2,
				listeners: {
					select: {
						fn: function(combo){
							recargaDatos();
							if(combo.getValue() =='0'){
								Ext.getCmp('ig_ppc_id').hide();
								Ext.getCmp('periodicidad_cap_num_pagos_id').hide();
							}else  {
								Ext.getCmp('ig_ppc_id').show();
								Ext.getCmp('periodicidad_cap_num_pagos_id').show();
							}
						}
					}
				}
			},{
				xtype:         'displayfield',
				id:            'periodicidad_cap_num_pagos_id',
				value:         '<div align="right"> No. pagos: </div>',
				width:         120
			},{
				width:         140,
				xtype:         'numberfield',
				id:            'ig_ppc_id',
				name:          'ig_ppc',
				msgTarget:     'side',
				allowBlank:    true,
				readOnly:      true,
				maxLength:     10,
				listeners: {
					select: {
						fn: function(combo){
							recargaDatos();
						}
					}
				}
			}]
		},{
			xtype:            'compositefield',
			fieldLabel:       'Primer fecha de pago de Capital',
			id:               'fecha_pago_cap_id',
			msgTarget:        'side',
			combineErrors:    false,
			items: [{
				width:         180,
				xtype:         'datefield',
				id:            'pfpc_id',
				name:          'pfpc',
				msgTarget:     'side',
				minValue:      '01/01/1901',
				allowBlank:    true,
				startDay:      0
			},{
				width:         175,
				xtype:         'displayfield',
				value:         '<div align="right"> Pen�ltima fecha de pago de Capital: </div>'
			},{
				width:         180,
				xtype:         'datefield',
				id:            'pufpc_id',
				name:          'pufpc',
				msgTarget:     'side',
				minValue:      '01/01/1901',
				allowBlank:    true,
				startDay:      0
			}]
		},{
			width:            180,
			xtype:            'combo',
			id:               'idcurva_id',
			name:             'idcurva',
			fieldLabel:       'Curva',
			emptyText:        'Seleccione...',
			displayField:     'descripcion',
			valueField:       'clave',
			msgTarget:        'side',
			mode:             'local',
			triggerAction:    'all',
			forceSelection:   true,
			typeAhead:        true,
			allowBlank:       true,
			store:            catalogoCurva
		},{
			xtype:            'compositefield',
			fieldLabel:       'Interpolaci�n',
			msgTarget:        'side',
			combineErrors:    false,
			items: [{
				width:         180,
				xtype:         'combo',
				id:            'id_interpolacion_id',
				name:          'id_interpolacion',
				emptyText:     'Seleccione...',
				displayField:  'descripcion',
				valueField:    'clave',
				msgTarget:     'side',
				mode:          'local',
				triggerAction: 'all',
				forceSelection:true,
				typeAhead:     true,
				allowBlank:    true,
				store:         [['L','Lineal'], ['A','Alambrada']]
			},{
				width:         175,
				xtype:         'displayfield',
				value:         '<div align="right"> C�lculo: </div>'
			},{
				width:         180,
				xtype:         'combo',
				id:            'metodoCalculo_id',
				name:          'metodoCalculo',
				emptyText:     'Seleccione...',
				displayField:  'descripcion',
				valueField:    'clave',
				msgTarget:     'side',
				mode:          'local',
				triggerAction: 'all',
				forceSelection:true,
				typeAhead:     true,
				allowBlank:    true,
				store:         [['P','Plazo Promedio de Vida'], ['D','Flujos'], ['U','Duraci�n']]
			}]
		},{ width:445, xtype:'displayfield', id:'ic_solic_esp_id',      name:'ic_solic_esp',      hidden:true, fieldLabel: 'ic_solic_esp'
		},{ width:445, xtype:'displayfield', id:'str_tipo_usuario_id',  name:'str_tipo_usuario',  hidden:true, fieldLabel: 'str_tipo_usuario'
		},{ width:445, xtype:'displayfield', id:'ic_ejecutivo_id',      name:'ic_ejecutivo',      hidden:true, fieldLabel: 'ic_ejecutivo'
		},{ width:445, xtype:'displayfield', id:'ig_plazo_max_oper_id', name:'ig_plazo_max_oper', hidden:true, fieldLabel: 'ig_plazo_max_oper'
		},{ width:445, xtype:'displayfield', id:'ig_num_max_disp_id',   name:'ig_num_max_disp',   hidden:true, fieldLabel: 'ig_num_max_disp'
		},{ width:445, xtype:'displayfield', id:'ig_plazo_conv_ant_id', name:'ig_plazo_conv_ant', hidden:true, fieldLabel: 'ig_plazo_conv_ant'
		},{ width:445, xtype:'displayfield', id:'num_disp_capt_id',     name:'num_disp_capt',     hidden:true, fieldLabel: 'num_disp_capt'
		}],
		buttons: [{
				xtype:         'button',
				text:          'Continuar',
				id:            'btnContinuar',
				iconCls:       'icoActualizar',
				handler:       function(boton, evento){
					procesoContinuar('CONTINUAR');
				}
		},{
				xtype:         'button',
				text:          'Cancelar',
				id:            'btnCancelar',
				iconCls:       'icoCancelar',
				handler:       procesoSalir
		}]
	});

/*****************************************************************************
PANTALLA: CAPTURA DISPOSICIONES
******************************************************************************/
	/***** Regresa la pantalla al form principal *****/
	function procesoCerrarDisposiciones(){
		/***** Se actualizan los datos si es que se modific� el grid *****/
		recargaDatos();
		Ext.getCmp('formaCapturaDisp').hide();
		Ext.getCmp('formaSolicitudFechas').hide();
		Ext.getCmp('formaPrincipal').show();
	}

	/****** Las siguientes dos funciones calculan en ig_plazo a partir de las fechas del grid *****/
	function fnDesentrama(Cadena){
		var Dato;
		sCadena     = Cadena
		tamano      = sCadena.length;
		posicion    = sCadena.indexOf("/");
		Dato        = sCadena.substr(0,posicion);
		cadenanueva = sCadena.substring(posicion + 1,tamano);
		sCadena     = cadenanueva;
		return Dato;
	}

	function calculaDias(sFecha, sFecha2){
		var iResAno, DiaMes, DiasDif=0, iCont=0, iMes, DiaMesA, iBan=0;
		var DiasPar, Dato, VencDia, VencMes, AltaMes, AltaDia;
		sCadena = sFecha
		AltaDia = fnDesentrama(sCadena);
		AltaMes = fnDesentrama(sCadena);
		AltaAno = sCadena;
		sCadena = sFecha2
		VencDia = fnDesentrama(sCadena);
		VencMes = fnDesentrama(sCadena);
		VencAno = sCadena;
		Dato    = AltaMes.substr(0,1);
		if (Dato == 0){
			AltaMes = AltaMes.substr(1,2)
		}
		Dato  = AltaDia.substr(0,1);
		if (Dato == 0){
			AltaDia = AltaDia.substr(1,2)
		}
		Dato  = VencDia.substr(0,1);
		if (Dato == 0){
			VencDia = VencDia.substr(1,2)
		}
		Dato = VencMes.substr(0,1);
		if (Dato == 0){
			VencMes = VencMes.substr(1,2)
		}
		while (VencAno != AltaAno || VencMes != AltaMes || VencDia != AltaDia){
			iCont++;
			while(AltaMes != VencMes || VencAno != AltaAno){
				iBan=1;
				iResAno = AltaAno % 4;
				if (iResAno == 0){
					Tmes[2] =29;
				} else{
					Tmes[2] =28;
				}
				if (iCont == 1){
					DiaMesA = Tmes[AltaMes];
					DiasDif = DiaMesA - AltaDia;
				} else{
					DiaMesA  = Tmes[AltaMes];
					DiasDif += parseInt(DiaMesA);
				}
				if (AltaMes == 12){
					AltaAno++;
					AltaMes = 1;
				} else{
					AltaMes++;
				}
				iCont++;
			} // fin de while
			if (AltaMes == VencMes && VencAno == AltaAno){
				if (iBan == 0){
					DiasDif = parseInt(VencDia) - parseInt(AltaDia);
					AltaDia = VencDia;
				} else if (iBan=1){
					DiasDif += parseInt(VencDia);
					AltaDia = VencDia;
				}
			}
		}
		return DiasDif;
	}	

	/***** Obtiene los datos del grid y los convierte en array *****/
	function getJsonOfStore(store){
		var datar            = new Array();
		var montoVec         = new Array();
		var dispoVec         = new Array();
		var vencVec          = new Array();
		var exito            = true;
		var fechaDisposicion = null;
		var fechaVencimiento = null;
		var fechaSolicitud   = (new Date(Ext.getCmp('df_solicitud_id').getValue())).format('m/d/Y');
		var records          = store.getRange();
		jsonDataEncode       = '';
		monto                = '';
		dispo                = '';
		venc                 = '';

		for (var i = 0; i < records.length; i++){

			fechaDisposicion = (new Date(records[i].data.FECHA_DISP)).format('m/d/Y');
			fechaVencimiento = (new Date(records[i].data.FECHA_VENC)).format('m/d/Y');

			if(records[i].data.MONTO == ''){
				Ext.MessageBox.alert('Mensaje...','El campo Monto es obligatorio.',
					function(){
						gridCapturaDisposicion.startEditing(i, 1);
						return;
					}
				);
				exito = false;
				break;
			} else if(records[i].data.FECHA_DISP == ''){
				Ext.MessageBox.alert('Mensaje...','El campo Fecha de Disposici�n es obligatorio.',
					function(){
						gridCapturaDisposicion.startEditing(i, 2);
						return;
					}
				);
				exito = false;
				break;
			} else if(records[i].data.FECHA_VENC == ''){
				Ext.MessageBox.alert('Mensaje...','El campo Fecha de Vencimiento es obligatorio.',
					function(){
						gridCapturaDisposicion.startEditing(i, 3);
						return;
					}
				);
				exito = false;
				break;
			} else if((Date.parse(fechaDisposicion)) < (Date.parse(fechaSolicitud))){
				Ext.MessageBox.alert('Mensaje...','La fecha de disposici�n debe ser mayor o igual a la fecha de solicitud.',
					function(){
						gridCapturaDisposicion.startEditing(i, 2);
						return;
					}
				);
				exito = false;
				break;
			} else if((Date.parse(fechaVencimiento)) <= (Date.parse(fechaDisposicion))){
				Ext.MessageBox.alert('Mensaje...','La fecha de vencimiento debe ser posterior a la fecha de disposici�n.',
					function(){
						gridCapturaDisposicion.startEditing(i, 3);
						return;
					}
				);
				exito = false;
				break;
			} else{
				
				/***** Estos campos se llenan para comparar las fechas con el form de CAPTURA SOLICITUD DE FECHAS *****/
				Ext.getCmp('fecha_disp_hid_id').setValue((new Date(records[0].data.FECHA_DISP)).format('d/m/Y'));
				Ext.getCmp('fecha_venc_hid_id').setValue((new Date(records[0].data.FECHA_VENC)).format('d/m/Y'));

				montoVec.push(records[i].data.MONTO);
				dispoVec.push((new Date(records[i].data.FECHA_DISP)).format('d/m/Y'));
				vencVec.push((new Date(records[i].data.FECHA_VENC)).format('d/m/Y'));
				exito = true;
			}
		}

		if(exito == true){
			// Actualizo el ig_plazo
			var igPlazo = calculaDias((new Date(Ext.getCmp('fecha_disp_hid_id').getValue())).format('d/m/Y'), (new Date(Ext.getCmp('fecha_venc_hid_id').getValue())).format('d/m/Y'));
			if(igPlazo == 0){
				Ext.Msg.alert('Mensaje...', 'El plazo de la operacion debe ser mayor a cero.');
				return;
			}
			if( igPlazo > parseInt(Ext.getCmp('ig_plazo_max_oper_id').getValue()) ){
				Ext.Msg.alert('Mensaje...', 'El plazo de la operaci�n excede al parametro.<br>El plazo maximo es de '+ Ext.getCmp('ig_plazo_max_oper_id').getValue() +' d�as.');
				return;
			}
			Ext.getCmp('ig_plazo_id').setValue(igPlazo);
		
			if(parseFloat((Ext.getCmp('monto_total_id').getValue()).replace('$','')) != parseFloat(Ext.getCmp('fn_monto_id').getValue() ) ){
				Ext.Msg.alert('Mensaje...','El total de los montos es diferente al Monto Total Requerido, favor de corregirlo');
				return;	
			}		
		
			monto = montoVec.toString();
			dispo = dispoVec.toString();
			venc  = vencVec.toString();

			/***** Se actualizan los datos si es que se modific� el grid *****/
			recargaDatos();

			Ext.getCmp('formaPrincipal').show();
			Ext.getCmp('formaCapturaDisp').hide();
		}
	}

	/***** Se crea el store del grid Captura Disposici�n *****/
	var capturaDisposicion = new Ext.data.JsonStore({
		fields: [
			{name: 'NUM_DISP'  },
			{name: 'MONTO'     },
			{name: 'FECHA_DISP', type: 'date', convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y'));}},
			{name: 'FECHA_VENC', type: 'date', convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y'));}}
		]
	});

	/***** Se crea el grid Captura Disposici�n *****/
	var gridCapturaDisposicion = new Ext.grid.EditorGridPanel({
		width:              800,
		height:             220,
		id:                 'gridCapturaDisposicion',
		margins:            '20 0 0 0',
		style:              'margin:0 auto;',
		align:              'center',
		hidden:             false,
		displayInfo:        true,
		loadMask:           true,
		stripeRows:         true,
		autoHeight:         false,
		frame:              true,
		border:             true,
		clicksToEdit:       1,
		store:              capturaDisposicion,
		columns: [{ 
			header:          'No. Disp.',
			dataIndex:       'NUM_DISP',
			align:           'center', 
			width:           200
		},{
			header:          'Monto',
			dataIndex:       'MONTO',
			align:           'center',
			width:           200,
			renderer:        Ext.util.Format.numberRenderer('0.00'),
			editor:          new fm.NumberField({
				allowBlank:   false
			})
		},{
			header:          'Fecha de Disposici�n',
			dataIndex:       'FECHA_DISP',
			align:           'center',
			xtype:           'datecolumn',
			format:          'd/m/Y',
			width:           200,
			editor:          new fm.DateField({
				minValue:     '01/01/1901',
				//disabledDays: [0, 6],
				allowBlank:   true
			})
		},{
			header:          'Fecha de Vencimiento',
			dataIndex:       'FECHA_VENC',
			align:           'center',
			xtype:           'datecolumn',
			format:          'd/m/Y',
			width:           200,
			editor:          new fm.DateField({
				minValue:     '01/01/1901',
				//disabledDays: [0, 6],
				allowBlank:   false
			})
		}],
		listeners: {
			afteredit: function(e) {
				var records = e.record.store.getRange();
				var montoTotal = 0;
				for (var i = 0; i < records.length; i++){
					montoTotal += parseFloat(records[i].data.MONTO);
				}
				Ext.getCmp('monto_total_id').setValue('$' + montoTotal);
			}
		}
	});

	/***** Se crea el form Captura Disposicion *****/
	var formaCapturaDisp = new Ext.form.FormPanel({
		width:               850,
		height:              350,
		id:                  'formaCapturaDisp',
		title:               'Captura Disposiciones',
		layout:              'form',
		style:               'margin: 0 auto;',
		labelAlign:          'right',
		frame:               true,
		hidden:              true,
		border:              true,
		autoHeight:          false,
		items: [
			gridCapturaDisposicion,{
			width:            150,
			xtype:            'textfield',
			id:               'monto_total_id',
			name:             'monto_total',
			fieldLabel:       'Monto Total',
			readOnly:         true
		},{
			width:            600,
			xtype:            'displayfield',
			id:               'nota_id',
			name:             'nota',
			value:            '<b>Nota:</b> Para poder capturar <b>Plan de pagos</b> o <b>Tipo Renta</b> debe ser s&oacute;lo una disposici&oacute;n.'
		}],
		buttons: [{
				xtype:         'button',
				text:          'Continuar',
				id:            'btnContinuarCapturaDisp',
				iconCls:       'icoActualizar',
				handler:       function(boton, evento){
					getJsonOfStore(capturaDisposicion);
				}
		},{
				xtype:         'button',
				text:          'Limpiar',
				id:            'btnLimpiarCapturaDisp',
				iconCls:       'icoLimpiar',
				handler:       function(boton, evento){
					//alert('No encuentro la l�gica de este bot�n');
				}
		},{
				xtype:         'button',
				text:          'Regresar',
				id:            'btnRegresarCapturaDisp',
				iconCls:       'icoRegresar',
				handler:       procesoCerrarDisposiciones
		}]
	});

/*****************************************************************************
PANTALLA: CAPTURA SOLICITUD DE FECHAS
******************************************************************************/

	/***** Regresa la pantalla al form principal *****/
	function procesoCerrarSolicitudFechas(){
		Ext.getCmp('formaCapturaDisp').hide();
		Ext.getCmp('formaSolicitudFechas').hide();
		Ext.getCmp('formaPrincipal').show();
	}

	/***** Valida las fechas introducidas en el form Solicitud de Fechas*****/
	function procesoContinuarSolicFechas(){

		if( !Ext.getCmp('formaSolicitudFechas').getForm().isValid() ){
			return;
		}
		var fechaDisp      = (new Date(Ext.getCmp('fecha_disp_hid_id').getValue()).format('m/d/Y'));
		var fechaVenc      = (new Date(Ext.getCmp('fecha_venc_hid_id').getValue()).format('m/d/Y'));
		var primerFecha    = (new Date(Ext.getCmp('primer_fecha_id').getValue()).format('m/d/Y'));
		var penultimaFecha = (new Date(Ext.getCmp('penultima_fecha_id').getValue()).format('m/d/Y'));

		if((Date.parse(primerFecha)) < (Date.parse(fechaDisp))){
			Ext.getCmp('primer_fecha_id').markInvalid('La fecha debe ser mayor a la fecha de disposici�n.');
			return;
		}

		if((Date.parse(penultimaFecha)) < (Date.parse(fechaDisp))){
			Ext.getCmp('penultima_fecha_id').markInvalid('La fecha debe ser mayor a la fecha de disposici�n.');
			return;
		}

		if((Date.parse(primerFecha)) >= (Date.parse(fechaVenc))){
			Ext.getCmp('primer_fecha_id').markInvalid('La fecha debe ser menor a la fecha de vencimiento.');
			return;
		}

		if((Date.parse(penultimaFecha)) >= (Date.parse(fechaVenc))){
			Ext.getCmp('penultima_fecha_id').markInvalid('La fecha debe ser menor a la fecha de vencimiento.');
			return;
		}

		if((Date.parse(primerFecha)) > (Date.parse(penultimaFecha))){
			Ext.getCmp('penultima_fecha_id').markInvalid('La primer fecha de pago de capital debe ser menor a la pen�ltima fecha de pago de capital.');
			return;
		}

		diap  = (primerFecha).substring(3,5);
		diapu = (penultimaFecha).substring(3,5);
		if(diap != diapu){
			Ext.Msg.alert('Mensaje...', 'Los d�as de pago deben coincidir.');
			return;
		}

		Ext.Ajax.request({
			url: '22forma07Aext.data.jsp',
			params: Ext.apply({
				informacion:       'Captura_Solicitud_Fechas',
				nombre_ejecutivo:  Ext.getCmp('ejecutivo_nafin_id').getValue(),
				ig_plazo_max_oper: Ext.getCmp('ig_plazo_max_oper_id').getValue(),
				ig_num_max_disp:   Ext.getCmp('ig_num_max_disp_id').getValue(),
				ic_moneda:         Ext.getCmp('ic_moneda_id').getValue(),
				pfpc:              (new Date(Ext.getCmp('primer_fecha_id').getValue()).format('d/m/Y')),
				pufpc:             (new Date(Ext.getCmp('penultima_fecha_id').getValue()).format('d/m/Y')),
				fn_monto_disp:     monto,
				df_disposicion:    dispo,
				df_vencimiento:    venc,
				dia_pago:          diapu
			}),
			callback: procesarRespuestaSolicFechas
		});
	}

	/***** Despues de validar la primer y penultima fecha se decida la siguiente acci�n *****/
	var procesarRespuestaSolicFechas = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){

			var valor = (Ext.getCmp('ig_tipo_piso_id')).getValue();
			window.location.href='22forma07Bext.jsp'                                                         +
			'?nombre_ejecutivo='   + Ext.getCmp('nombre_ejecutivo_id').getValue()                            +
			'&cg_mail='            + Ext.getCmp('cg_mail_id').getValue()                                     +
			'&cg_telefono='        + Ext.getCmp('cg_telefono_id').getValue()                                 +
			'&cg_fax='             + Ext.getCmp('cg_fax_id').getValue()                                      +
			'&ejecutivo_nafin='    + Ext.getCmp('ejecutivo_nafin_id').getValue()                             +
			'&ig_tipo_piso='       + valor.getGroupValue()                                                   +
			'&cg_razon_social_if=' + Ext.getCmp('cg_razon_social_if_id').getValue()                          +
			'&ig_riesgo_if='       + Ext.getCmp('ig_riesgo_if_id').getValue()                                +
			'&cg_acreditado='      + Ext.getCmp('cg_acreditado_id').getValue()                               +
			'&ig_valor_riesgo_ac=' + Ext.getCmp('ig_valor_riesgo_ac_id').getValue()                          +
			'&df_solicitud='       + (new Date(Ext.getCmp('df_solicitud_id').getValue()).format('d/m/Y'))    +
			'&df_cotizacion='      + (new Date(Ext.getCmp('df_cotizacion_id').getValue()).format('d/m/Y'))   +
			'&cg_programa='        + Ext.getCmp('cg_programa_id').getValue()                                 +
			'&ic_moneda='          + Ext.getCmp('ic_moneda_id').getValue()                                   +
			'&fn_monto='           + Ext.getCmp('fn_monto_id').getValue()                                    +
			'&ic_tipo_tasa='       + Ext.getCmp('ic_tipo_tasa_id').getValue()                                +
			'&in_num_disp='        + Ext.getCmp('in_num_disp_id').getValue()                                 +
			'&ig_plazo_conv='      + Ext.getCmp('ig_plazo_conv_id').getValue()                               +
			'&cg_ut_plazo='        + Ext.getCmp('cg_ut_plazo_id').getValue()                                 +
			'&ig_plazo='           + Ext.getCmp('ig_plazo_id').getValue()                                    +
			'&ic_esquema_recup='   + Ext.getCmp('ic_esquema_recup_id').getValue()                            +
			'&cg_ut_ppi='          + Ext.getCmp('cg_ut_ppi_id').getValue()                                   +
			'&ig_ppi='             + Ext.getCmp('ig_ppi_id').getValue()                                      +
			'&cg_ut_ppc='          + Ext.getCmp('cg_ut_ppc_id').getValue()                                   +
			'&ig_ppc='             + Ext.getCmp('ig_ppc_id').getValue()                                      +
			'&pfpc='               + (new Date(Ext.getCmp('primer_fecha_id').getValue()).format('d/m/Y'))    +
			'&pufpc='              + (new Date(Ext.getCmp('penultima_fecha_id').getValue()).format('d/m/Y')) +
			'&idcurva='            + Ext.getCmp('idcurva_id').getValue()                                     +
			'&id_interpolacion='   + Ext.getCmp('id_interpolacion_id').getValue()                            +
			'&metodoCalculo='      + Ext.getCmp('metodoCalculo_id').getValue()                               +
			'&ic_solic_esp='       + Ext.getCmp('ic_solic_esp_id').getValue()                                +
			'&str_tipo_usuario='   + Ext.getCmp('str_tipo_usuario_id').getValue()                            +
			'&ic_ejecutivo='       + Ext.getCmp('ic_ejecutivo_id').getValue()                                +
			'&ig_plazo_max_oper='  + Ext.getCmp('ig_plazo_max_oper_id').getValue()                           +
			'&ig_num_max_disp='    + Ext.getCmp('ig_num_max_disp_id').getValue()                             +
			'&ig_plazo_conv_ant='  + Ext.getCmp('ig_plazo_conv_ant_id').getValue()                           +
			'&num_disp_capt='      + Ext.getCmp('num_disp_capt_id').getValue()                               +
			'&fn_monto_grid='      + monto.toString()                                                        +
			'&disposicion_grid='   + dispo.toString()                                                        +
			'&vencimiento_grid='   + venc.toString();
		} else{
			Ext.Msg.alert('Error...', Ext.util.JSON.decode(response.responseText).mensaje);
		}
	}

	/***** Se crea el form Captura Solicitud de Fechas *****/
	var formaSolicitudFechas = new Ext.form.FormPanel({
		width:           700,
		labelWidth:      200,
		id:              'formaSolicitudFechas',
		title:           'Fechas de Pago de Capital',
		layout:          'form',
		style:           'margin: 0 auto;',
		frame:           true,
		hidden:          true,
		border:          true,
		autoHeight:      true,
		items: [{
			width:        700,
			xtype:        'panel',
			layout:       'table',
			border:       false,
			layoutConfig: {columns: 1},
			defaults:     {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{
					width:  680,
					frame:  true,
					border: false,
					html:   '<div align="left" class="titulos"> ' +
							  '<b>Favor de capturar las fechas que se le solicitan a continuaci�n.</b><br>&nbsp;<br>'+
						     '<li>Para fines exclusivos de c&aacute;lculo, le pedimos que el d&iacute;a que capture en ambas fechas sea el mismo (si alguna de las fechas es d&iacute;a inh&aacute;bil, le recordamos que su pago lo realizar&aacute; el siguiente d&iacute;a h&aacute;bil).<br></li><br>'+
						     '<li>Si en el esquema solicitado solo existen 2 pagos de capital (uno adem&aacute;s del vencimiento), favor de capturar la misma fecha en ambos campos.</li>'+
						     '</div>'
				}
			]
		},{
			width:        445,
			xtype:        'displayfield',
			name:         'texto',
			value:        '&nbsp;'
		},{
			width:        150,
			xtype:        'datefield',
			id:           'primer_fecha_id',
			name:         'primer_fecha',
			fieldLabel:   '&nbsp; Primer fecha de pago de capital',
			msgTarget:    'side',
			minValue:     '01/01/1901',
			startDay:     0,
			allowBlank:   false
		},{
			width:        150,
			xtype:        'datefield',
			id:           'penultima_fecha_id',
			name:         'penultima_fecha',
			fieldLabel:   '&nbsp; Pen�ltima fecha de pago de capital',
			msgTarget:    'side',
			minValue:     '01/01/1901',
			startDay:     0,
			allowBlank:   false
		},{
			width:        150,
			xtype:        'datefield',
			id:           'fecha_disp_hid_id',
			name:         'fecha_disp_hid',
			fieldLabel:   'fecha_disp_hid',
			hidden:       true
		},{
			width:        150,
			xtype:        'datefield',
			id:           'fecha_venc_hid_id',
			name:         'fecha_venc_hid',
			fieldLabel:   'fecha_venc_hid',
			hidden:       true
		}],
		buttons: [{
				xtype:     'button',
				text:      'Continuar',
				id:        'btnContinuarSolicFech',
				iconCls:   'icoActualizar',
				handler:   procesoContinuarSolicFechas
		},{
				xtype:     'button',
				text:      'Regresar',
				id:        'btnCancelarSolicFech',
				iconCls:   'icoRegresar',
				handler:   procesoCerrarSolicitudFechas
		}]
	});

/*****************************************************************************
 Contenedor Principal
******************************************************************************/
	var pnl = new Ext.Container({
		width:   949,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin: 0 auto;',
		items: [
			NE.util.getEspaciador(20),
			formaPrincipal,
			formaCapturaDisp,
			formaSolicitudFechas
		]
	});

	procesoInicializar();

});