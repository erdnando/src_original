Ext.onReady(function(){
var miPDF="";
var imagen  = "<div id='cancelado' style='visibility:show; position:absolute; left:20px; top:90px; width: 570px; filter:alpha(opacity=50);-moz-opacity:.50;opacity:.50; '>"+
				  "<img src='/nafin/00utils/gif/Cancelado.gif'  width='900'>"+
				  "</div>";
						 
var imagenCancelado = new Ext.Container({
		layout: 'table',		
		id: 'imagenCancelado',							
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		hidden: true,
		items: [			
			{ 
				xtype:   'label',  
				html:		imagen				
			}
		]
	});
	 
	
//**************************textoCompleto
var textoCompleto = function (value, metadata, record, rowIndex, colIndex, store){
  metadata.attr = 'style="white-space: normal; word-wrap: break-word; "';
  return value;
};
//**************************textoCompleto

//------------------------------------------------------------ procesarFecha ----------------------------------------------------------------
function procesarFecha(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
		var info = Ext.util.JSON.decode(response.responseText);
		Ext.getCmp('fechaRegistro').setValue(info.fechaHoy);
		Ext.getCmp('fechaFinRegistro').setValue(info.fechaHoy);
	}
};
//------------------------------------------------------------ Fin procesarFecha ------------------------------------------------------------

//---------------------------------------------------------------------Moneda----------------------------------------------------------------
var Moneda = Ext.data.Record.create([ 
 {name: "CLAVE", type: "string"}, 
 {name: "DESCRIPCION", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//------------------------------------------------------------------Fin Moneda---------------------------------------------------------------

//--------------------------------------------------------------- procesarMoneda -------------------------------------------------------------
var procesarMoneda = function(store, arrRegistros, opts) {
	store.insert(0,new Moneda({ 
	CLAVE: "", 
	DESCRIPCION: "Seleccione Moneda", 
	loadMsg: ""})); 		
	Ext.getCmp('cmbmon').setValue("");
	store.commitChanges(); 
};	
//---------------------------------------------------------------Fin procesarMoneda-----------------------------------------------------------

//-----------------------------------------------------------------Catalogo Moneda-----------------------------------------------------------
var catalogoMoneda = new Ext.data.JsonStore({
  id					: 'catalogoMoneda',
  root 				: 'registros',
  fields 			: ['CLAVE', 'DESCRIPCION', 'loadMsg'],
  url 				: '22consulta1Ext01.data.jsp',
  baseParams		: {
	 informacion	: 'catalogomon'  
						  },
  totalProperty 	: 'total',
  autoLoad			: true,
  listeners			: {
	 load				: procesarMoneda,
	 beforeload		: NE.util.initMensajeCargaCombo,
	 exception		: NE.util.mostrarDataProxyError								
						 }
});//-----------------------------------------------------------Fin Catalogo Moneda----------------------------------------------------------

//-----------------------------------------------------------------Intermediario----------------------------------------------------------------
var Intermediario = Ext.data.Record.create([ 
 {name: "CLAVE", type: "string"}, 
 {name: "DESCRIPCION", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//------------------------------------------------------------------Fin Intermediario---------------------------------------------------------------

//--------------------------------------------------------------- procesarInter -------------------------------------------------------------
var procesarInter = function(store, arrRegistros, opts) {
	store.insert(0,new Intermediario({ 
	CLAVE: "", 
	DESCRIPCION: "Seleccione IF", 
	loadMsg: ""})); 		
	Ext.getCmp('cmbti').setValue(""); 
	store.commitChanges(); 
};	
//---------------------------------------------------------------Fin procesarInter-----------------------------------------------------------

//--------------------------------------------------------------Catalogo Intermediario------------------------------------------------------
var catalogoIntermediario = new Ext.data.JsonStore({
  id					: 'catalogoIntermediario',
  root 				: 'registros',
  fields 			: ['CLAVE', 'DESCRIPCION', 'loadMsg'],
  url 				: '22consulta1Ext01.data.jsp',
  baseParams		: {
   informacion		: 'catalogoint'  
							},
  totalProperty 	: 'total',
  autoLoad			: true,
  listeners			: {			
   load				: procesarInter,
   beforeload		: NE.util.initMensajeCargaCombo,
   exception		: NE.util.mostrarDataProxyError								
              }
});//---------------------------------------------------------Fin Catalogo Intermediario-------------------------------------------------------


//-------------------------------------------------------------------- Estatus -------------------------------------------------------------
var Estatus = Ext.data.Record.create([ 
 {name: "CLAVE", type: "string"}, 
 {name: "DESCRIPCION", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//------------------------------------------------------------------Fin Estatus---------------------------------------------------------------

//--------------------------------------------------------------- procesarEstatus -------------------------------------------------------------
var procesarEstatus = function(store, arrRegistros, opts) {
	store.insert(0,new Estatus({ 
	CLAVE: "", 
	DESCRIPCION: "Seleccione un Estatus", 
	loadMsg: ""})); 		
	Ext.getCmp('cmbTipo').setValue(""); 
	store.commitChanges(); 
};	
//---------------------------------------------------------------Fin procesarEstatus --------------------------------------------------------

//---------------------------------------------------------------- Catalogo estatus ---------------------------------------------------------
var estatus = new Ext.data.JsonStore({
  id					: 'catalogoEstatus',
  root 				: 'registros',
  fields 			: ['CLAVE', 'DESCRIPCION', 'loadMsg'],
  url 				: '22consulta1Ext01.data.jsp',
  baseParams		: {
   informacion		: 'catalogoEstatus'  
							},
  totalProperty 	: 'total',
  autoLoad			: true,
  listeners			: {			
   load				: procesarEstatus,
   beforeload		: NE.util.initMensajeCargaCombo,
   exception		: NE.util.mostrarDataProxyError								
              }
});//--------------------------------------------------------- Fin Catalogo estatus ---------------------------------------------------------

//-------------------------------------------------------------------- Persona -------------------------------------------------------------
var Persona = Ext.data.Record.create([ 
 {name: "CLAVE", type: "string"}, 
 {name: "DESCRIPCION", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//------------------------------------------------------------------Fin Persona---------------------------------------------------------------

//--------------------------------------------------------------- procesarPersona -------------------------------------------------------------
var procesarPersona = function(store, arrRegistros, opts) {
	store.insert(0,new Persona({ 
	CLAVE: "", 
	DESCRIPCION: "Seleccione Responsable", 
	loadMsg: ""})); 		
	Ext.getCmp('cmbPer').setValue(""); 
	store.commitChanges(); 
};	
//---------------------------------------------------------------Fin procesarPersona --------------------------------------------------------

//---------------------------------------------------------------- catalogoPersona ---------------------------------------------------------
var catalogoPersona = new Ext.data.JsonStore({
  id					: 'catalogoPersona',
  root 				: 'registros',
  fields 			: ['CLAVE', 'DESCRIPCION', 'loadMsg'],
  url 				: '22consulta1Ext01.data.jsp',
  baseParams		: {
   informacion		: 'catalogoPersona'  
							},
  totalProperty 	: 'total',
  autoLoad			: true,
  listeners			: {			
   load				: procesarPersona,
   beforeload		: NE.util.initMensajeCargaCombo,
   exception		: NE.util.mostrarDataProxyError								
              }
});//--------------------------------------------------------- Fin catalogoPersona ---------------------------------------------------------

//----------------------------------------------------------- Procesara Descarga Archivos---------------------------------------------------
function procesarDescargaArchivos(opts, success, response) {
  if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
    var infoR = Ext.util.JSON.decode(response.responseText);
    var archivo = infoR.urlArchivo;				
    archivo = archivo.replace('/nafin','');
    var params = {nombreArchivo: archivo};				
    fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
    fp.getForm().getEl().dom.submit();
    fp.el.unmask();
    var el = gridConsulta.getGridEl();
    el.unmask();
	 var btncsv = Ext.getCmp('btnArchivoCSV');
	 btncsv.setIconClass('icoXls');     
    btncsv.setDisabled(false);
    var btnpdf = Ext.getCmp('btnArchivoPDF');
	 btnpdf.setIconClass('icoPdf');     
    btnpdf.setDisabled(false);	

  }else {
    NE.util.mostrarConnError(response,opts);
    fp.el.unmask();
  }
};
//------------------------------------------------------------------ Fin Descarga Archivos -----------------------------------------------

//------------------------------------------------------------------- procesarConsultaData --------------------------------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  var fp = Ext.getCmp('formcon');
  fp.el.unmask();
  var jsonData = store.reader.jsonData;
  var gridConsulta = Ext.getCmp('gridConsulta');	
  var el = gridConsulta.getGridEl();	

  if (arrRegistros != null) {
    if (!gridConsulta.isVisible()) {
      gridConsulta.show();
    }	
    if(store.getTotalCount() > 0) {//////////Verifica que existan registros
      Ext.getCmp('btnArchivoPDF').enable();
      Ext.getCmp('btnArchivoCSV').enable();
      el.unmask();					

      
    } else {	
      Ext.getCmp('btnArchivoPDF').disable();
      Ext.getCmp('btnArchivoCSV').disable();
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
};
//-----------------------------------------------------procesarConsultaData-----------------------------------------------------------------

//------------------------------------------------------Consulta Data------------------------------------------------------------------
var consultaData   = new Ext.data.JsonStore({ 
  root : 'registros',
  url : '22consulta1Ext01.data.jsp',
  baseParams: {
    informacion: 'Consultar'
  },		
  fields: [	
    {	name: 'IC_SOLIC_ESP'},
    {	name: 'FECHA_SOLICITUD'},	
    {	name: 'FECHA_COTIZACION'},
    {	name: 'NUMERO_SOLICITUD'},	
    {	name: 'MONEDA'},
    {	name: 'EJECUTIVO'},
    {	name: 'MONTO'},	
    {	name: 'PLAZO'},
    {	name:	'ESTATUS'},
    {	name:	'TASA_MISMO_DIA'},
    {	name: 'TASA_SPOT'},
	 {	name: 'NUM_REFERENCIA'},	
    {	name: 'CAUSA_RECHAZO'},
    {	name:	'OBSER_OPER'},
    {	name:	'IC_ESTATUS'},
    {	name: 'TASA_CONFIRMADA'},	
    {	name: 'CG_RAZON_SOCIAL'},
    {	name:	'IC_MONEDA'},
    {	name:	'IC_TIPO_TASA'},
    {	name: 'SOLICITAR_AUT'},
    {	name:	'TIPOCOTIZA'},
    {	name:	'CG_UT_PPC'},
    {	name: 'CG_UT_PPI'},
    {	name:	'IC_ESQUEMA_RECUP'},
    {	name: 'CS_LINEA'},
	 {	name: 'PLAN_PAGO'},
	 {	name: 'CONSULTAR'}	
  ],		
  totalProperty : 'total',
  messageProperty: 'msg',
  autoLoad: false,
  listeners: {
    load: procesarConsultaData,
    exception: {
      fn: function(proxy, type, action, optionsRequest, response, args) {
        NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
        //LLama procesar consulta, para que desbloquee los componentes.
        procesarConsultaData(null, null, null);					
      }
    }
  }		
});
//---------------------------------------------------------Fin ConsultaData-------------------------------------------------------------


//----------------------------------------------------------- Store infoUno -------------------------------------------------------------
var infoUno = new Ext.data.ArrayStore({ //new Ext.data.JsonStore({
  autoDestroy: true,
  storeId: 'infoUno',
  idIndex: 0, 
	 fields		: [
			  {name : 'uno'},
			  {name : 'dos' }
			  ]
	});
//----------------------------------------------------------- Fin Store infoUno -------------------------------------------------------------

//--------------------------------------------------------------  gridUno -------------------------------------------------------------
var gridUno = {// new Ext.grid.EditorGridPanel({
	xtype: 'grid',
	id: 'gridUno',
	store: infoUno,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: '<center>Datos de la Cotizaci�n</center>',
	height: 185,
	width: 480,
	hidden:false,
	stripeRows: true,
	columns: [
		{
		width : 180 ,
		dataIndex: 'uno',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 287 ,
		dataIndex: 'dos',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridUno -------------------------------------------------------------
	
//-------------------------------------------------------------- Store infoDos -------------------------------------------------------------
	var infoDos = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoDos',
     idIndex: 0, 
		 fields		: [
				  ]
		});
//----------------------------------------------------------- Fin Store infoDos -------------------------------------------------------------

//--------------------------------------------------------------  gridDos -------------------------------------------------------------
var gridDos = {
	xtype: 'grid',
	id: 'gridDos',
	store: infoDos,
	title:'...',
	height: 55,
	width: 600,
	hidden:false,
	columns: [
		]
};
//-------------------------------------------------------------- Fin  gridDos -------------------------------------------------------------
	
//-------------------------------------------------------------- Store infoTres -------------------------------------------------------------
	var infoTres = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoTres',
     idIndex: 0, 
		 fields		: [
              {name : 'tres'},
              {name : 'cuatro' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoTres -------------------------------------------------------------

//--------------------------------------------------------------  gridTres -------------------------------------------------------------
var gridTres = {
	xtype: 'grid',
	id: 'gridTres',
	store: infoTres,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Datos del Ejecutivo Nafin',
	height: 125,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 180 ,
		dataIndex: 'tres',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 410 ,
		dataIndex: 'cuatro',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridTres -------------------------------------------------------------

//-------------------------------------------------------------- Store infoCuatro -------------------------------------------------------------
	var infoCuatro = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoCuatro',
     idIndex: 0, 
		 fields		: [
              {name : 'cinco'},
              {name : 'seis' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoCuatro -------------------------------------------------------------

//--------------------------------------------------------------  gridCuatro -------------------------------------------------------------
var gridCuatro = {
	xtype: 'grid',
	id: 'gridCuatro',
	store: infoCuatro,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Datos del Intermediario Financiero / Acreditado',
	height: 175,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 180 ,
		dataIndex: 'cinco',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 410 ,
		dataIndex: 'seis',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridCuatro -------------------------------------------------------------

//-------------------------------------------------------------- Store infoCinco -------------------------------------------------------------
	var infoCinco = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoCinco',
     idIndex: 0, 
		 fields		: [
              {name : 'siete'},
              {name : 'ocho' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoCinco -------------------------------------------------------------

//--------------------------------------------------------------  gridCinco -------------------------------------------------------------
var gridCinco = {
	xtype: 'grid',
	id: 'gridCinco',
	store: infoCinco,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Caracter�sticas de la Operaci�n',
	height:250,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 220 ,
		dataIndex: 'siete',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 370 ,
		dataIndex: 'ocho',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridCinco -------------------------------------------------------------

//-------------------------------------------------------------- Store infoSeis -------------------------------------------------------------
	var infoSeis = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoSeis',
     idIndex: 0, 
		 fields		: [
              {name : 'nueve'},
              {name : 'diez' },
				  {name : 'once'},
              {name : 'doce' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoSeis -------------------------------------------------------------

//--------------------------------------------------------------  gridSeis -------------------------------------------------------------
var gridSeis = {
	xtype: 'grid',
	id: 'gridSeis',
	store: infoSeis,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Detalle de Disposiciones',
	height:80,
	width: 600,
	hidden:false,
	columns: [
		{
		header:'N�mero de Disposiciones',
		width : 150 ,
		dataIndex: 'nueve',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		header:'Monto',
		width : 145 ,
		dataIndex: 'diez',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		header:'Fecha de Disposiciones',
		width : 145 ,
		dataIndex: 'once',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		header:'Fecha de Vencimiento',
		width : 140 ,
		dataIndex: 'doce',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridSeis -------------------------------------------------------------


//-------------------------------------------------------------- Store infoSiete -------------------------------------------------------------
	var infoSiete = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoSiete',
     idIndex: 0, 
		 fields		: [
              {name : 'trece'},
              {name : 'catorce' },
				  {name : 'quince'},
              {name : 'dieciseis' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoSiete -------------------------------------------------------------

//--------------------------------------------------------------  gridSiete -------------------------------------------------------------
var gridSiete = {
	xtype: 'grid',
	id: 'gridSiete',
	store: infoSiete,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Plan de Pagos Capital',
	height:150,
	width: 600,
	hidden:false,
	columns: [
		{
		header:'N�mero de Pago',
		width : 150 ,
		dataIndex: 'trece',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		header:'Fecha de Pago',
		width : 145 ,
		dataIndex: 'catorce',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		header:'Monto a Pagar',
		width : 145 ,
		dataIndex: 'quince',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 140 ,
		dataIndex: 'dieciseis',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridSiete -------------------------------------------------------------

//-------------------------------------------------------------- Store infoOcho -------------------------------------------------------------
	var infoOcho = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoOcho',
     idIndex: 0, 
		 fields		: [
				  ]
		});
//----------------------------------------------------------- Fin Store infoOcho -------------------------------------------------------------

//--------------------------------------------------------------  gridOcho -------------------------------------------------------------
var gridOcho = {
	xtype: 'grid',
	id: 'gridOcho',
	store: infoOcho,
	title:'...',
	height: 42,
	width: 600,
	hidden:false,
	columns: [
		]
};
//-------------------------------------------------------------- Fin  gridOcho -------------------------------------------------------------

//-------------------------------------------------------------- Store infoNueve -------------------------------------------------------------
	var infoNueve = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoNueve',
     idIndex: 0, 
		 fields		: [
              {name : 'nueveUno'},
              {name : 'nueveDos' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoNueve -------------------------------------------------------------

//--------------------------------------------------------------  gridNueve -------------------------------------------------------------
var gridNueve = {
	xtype: 'grid',
	id: 'gridNueve',
	store: infoNueve,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Tasa Indicativa',
	height:250,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 200 ,
		dataIndex: 'nueveUno',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 395 ,
		dataIndex: 'nueveDos',
		sortable: false,
		resizable: false,
		align: 'left',
		renderer: textoCompleto
		}
		]
};
//-------------------------------------------------------------- Fin  gridNueve -------------------------------------------------------------

//-------------------------------------------------------------- Store infoDiez -------------------------------------------------------------
	var infoDiez = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoDiez',
     idIndex: 0, 
		 fields		: [
              {name : 'diezUno'},
              {name : 'diezDos' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoDiez -------------------------------------------------------------

//--------------------------------------------------------------  gridDiez -------------------------------------------------------------
var gridDiez = {
	xtype: 'grid',
	id: 'gridDiez',
	store: infoDiez,
	height:80,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 375 ,
		dataIndex: 'diezUno',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 210 ,
		dataIndex: 'diezDos',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridDiez -------------------------------------------------------------

//-------------------------------------------------------------- Store infoOnce -------------------------------------------------------------
	var infoOnce = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoOnce',
     idIndex: 0, 
		 fields		: [
              {name : 'onceUno'}
				  ]
		});
//----------------------------------------------------------- Fin Store infoOnce -------------------------------------------------------------

//--------------------------------------------------------------  gridOnce -------------------------------------------------------------
var gridOnce = {
	xtype: 'grid',
	id: 'gridOnce',
	title:'Observaciones',
	store: infoOnce,
	height:80,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 595 ,
		dataIndex: 'onceUno',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridOnce -------------------------------------------------------------


//------------------------------------------------------------ gridConsulta -----------------------------------------------------------------
var gridConsulta = new Ext.grid.EditorGridPanel({	
  store: consultaData,
  id: 'gridConsulta',
  margins: '20 0 0 0',		
  style: 'margin:0 auto;',
  title: '<center>Consulta de Solicitudes</center>',
  clicksToEdit: 1,
  hidden: true,
  columns: [	
    {
      header: 'Fecha de<br>Solicitud',
      tooltip: 'Fecha de Solicitud',
      dataIndex: 'FECHA_SOLICITUD',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center'			
    },
    {
      header: 'N�mero de<br>Solicitud',
      tooltip: 'N�mero de Solicitud',
      dataIndex: 'NUMERO_SOLICITUD',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center'
    },
    {
      header: 'Requiere autorizaci�n <br> del ejecutivo',
      tooltip: 'Requiere autorizaci�n del ejecutivo',
      dataIndex: 'SOLICITAR_AUT',
      sortable: true,
      width: 130,			
      resizable: true,				
      align: 'center'
    },
    {
      header: 'Intermediario <br>financiero',
      tooltip: 'Intermediario financiero',
      dataIndex: 'CG_RAZON_SOCIAL',
      sortable: true,
      width: 130,			
      resizable: true,				
      align: 'center',
		renderer:function(value){
        return "<div align='left'>"+value+"</div>";
      }
    },
    {
      header: 'Moneda',
      tooltip: 'Moneda',
      dataIndex: 'MONEDA',
      sortable: true,
      width: 130,			
      resizable: true,				
      align: 'center'
    },
    {
      header: 'Ejecutivo',
      tooltip: 'Ejecutivo',
      dataIndex: 'EJECUTIVO',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'left'
    },
    {
      header: 'Monto Total <br>Requerido',
      tooltip: 'Monto Total Requerido',
      dataIndex: 'MONTO',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
		renderer:function(value){
        return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
      }
    },
    {
      header: 'Plazo de la <br>Operaci�n',
      tooltip: 'Plazo de la Operaci�n',
      dataIndex: 'PLAZO',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center',
		renderer:function(value){
        return value+" dias";
      }
    },{
      header: 'Estatus',
      tooltip: 'Estatus',
      dataIndex: 'ESTATUS',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center'
    }, {
      header: 'Cotizada en<br>L�nea',
      tooltip: 'Cotizada en L�nea',
      dataIndex: 'CS_LINEA',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center',
		renderer:function(value){
			if(value ==="N"){
				return " ";
			} else {
			return value;
			}
      }
    },{
      header: 'Tasa<br>mismo d�a',
      tooltip: 'Tasa mismo d�a',
      dataIndex: 'TASA_MISMO_DIA',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center',
		renderer:function(value){
        return value + ' %' ;
      }
    },{
      header: 'Tasa<br>48 horas',
      tooltip: 'Tasa 48 horas',
      dataIndex: 'TASA_SPOT',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center',
		renderer:function(value){
        return value + ' %' ;
      }
    },{
      header: 'Tasa<br>Confirmada',
      tooltip: 'Tasa Confirmada',
      dataIndex: 'TASA_CONFIRMADA',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center',
		renderer: function(v,params,record){
		if(record.data.IC_ESTATUS=='7' || record.data.IC_ESTATUS=='9' ){ 
			 return v+'%';
		}else{
			return '';
		}
  }
    },{
      header: 'Tipo de<br>Cotizaci�n',
      tooltip: 'Tipo de Cotizaci�n',
      dataIndex: 'TIPOCOTIZA',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center'
    },{
      header: 'Causas de<br>rechazo',
      tooltip: 'Causas de rechazo',
      dataIndex: 'CAUSA_RECHAZO',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center'
    },{
		xtype		: 'actioncolumn',
      header: 'Plan de<br>Pago',
      tooltip: 'Plan de Pago',
      dataIndex: 'PLAN_PAGO',
      sortable: true,
      width: 70,			
      resizable: true,				
      align: 'center',
		items		: [{
			getClass: function(value,metadata,record,rowIndex,colIndex,store){		
				this.items[0].tooltip = 'Ver Tabla';
				if(record.data.PLAN_PAGO=='Ver Tabla' ){ 
					 return 'icoXls';
				}else{
					return ""
				}				
			},handler:function(record,rowIndex,colIndex,metadata,value) {
				var grid = Ext.getCmp('gridConsulta');				
				var registro = grid.getStore().getAt(rowIndex);
				var planPago = registro.get('IC_SOLIC_ESP'); 
				fp.el.mask('Generando Archivo...', 'x-mask-loading');
				Ext.Ajax.request({
            url: '22consulta1Ext01.data.jsp',
            params: Ext.apply(fp.getForm().getValues(),{							
              informacion: 'Ver Tabla',
				  idPago: planPago
            }),
            success : function(response) { },	
            callback: procesarDescargaArchivos
          }); 
			}
		}]
		},{
      header: 'Observaciones de<br>Operaciones',
      tooltip: 'Observaciones de Operaciones',
      dataIndex: 'OBSER_OPER',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center'
    },{
		xtype	: 'actioncolumn',
      header: 'Consultar',
      tooltip: 'Consultar',
      dataIndex: 'CONSULTAR',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
		items		: [{
			getClass: function(value,metadata,record,rowIndex,colIndex,store){		
			
				if(record.data.CONSULTAR=='Cotizacion' ){ 
					this.items[0].tooltip = 'Cotizaci�n';
					return 'iconoLupa';
				}else{
					this.items[0].tooltip = 'Solicitud';
					return 'iconoLupa';
				}				
			},
			handler:function(record,rowIndex,colIndex,metadata,value) {
				var grid = Ext.getCmp('gridConsulta');				
				var registro = grid.getStore().getAt(rowIndex);
				var planPago = registro.get('IC_SOLIC_ESP'); 
				var cadenaInt= registro.get('CG_RAZON_SOCIAL'); 
				var estatusSo= registro.get('IC_ESTATUS'); 
				Ext.Ajax.request({
            url: '22consulta1Ext01.data.jsp',
            params: Ext.apply(fp.getForm().getValues(),{							
              informacion: 'Detalle',
				  idPago: planPago,
				  nomInt: cadenaInt,
				  estSol: estatusSo
            }),
            success : function(response) {
					var info = Ext.util.JSON.decode(response.responseText);
					var contador1  =info.contador1; 
					var contador3  =info.contador3;
					var contador5  =info.contador5;
					var contador6  =info.contador6;
					var contador7  =info.contador7;
					var contador8  =info.contador8;
					var contador9  =info.contador9;
					var contador10  =info.contador10;
					var contador11  =info.contador11;
					miPDF="/nafin/00tmp/22cotizador/"+info.miPDF;
					
					var fGrid1 = parseInt(contador1); 
					var miData = new Array();					
					for (var f=0; f<fGrid1; f++){
						var registro = [];
						registro.push(info.registros[f].dos);
						registro.push(info.registros[f].uno);
						miData.push(registro);
					}
					var grid = Ext.getCmp('gridUno');
					var store = grid.getStore();
					store.loadData(miData);
					if (fGrid1===7){
					Ext.getCmp('primerMsgGrid').show();
					Ext.getCmp('gridDos').setTitle(''+info.registros[7].uno+'');
					} else {
						Ext.getCmp('primerMsgGrid').hide();
					}
					
					var miData = new Array();					
					for (var f=0; f<4; f++){
						var registro = [];
						registro.push(info.registros2[f].cuatro);
						registro.push(info.registros2[f].tres);
						miData.push(registro);
					}
					var grid = Ext.getCmp('gridTres');
					var store = grid.getStore();
					store.loadData(miData);
					
					var miData = new Array();					
					var fGrid3 = parseInt(contador3);
					for (var f=0; f<fGrid3; f++){
						var registro = [];
						registro.push(info.registros3[f].seis);
						registro.push(info.registros3[f].cinco);
						miData.push(registro);
					}
					var grid = Ext.getCmp('gridCuatro');
					var store = grid.getStore();
					store.loadData(miData);
					
					var miData = new Array();					
					var fGrid5 = parseInt(contador5);
					for (var f=0; f<fGrid5; f++){
						var registro = [];
						registro.push(info.registros5[f].ocho);
						registro.push(info.registros5[f].siete);
						miData.push(registro);
					}
					var grid = Ext.getCmp('gridCinco');
					var store = grid.getStore();
					store.loadData(miData);
					
					var miData = new Array();					
					var fGrid6 = parseInt(contador6);
					for (var f=0; f<fGrid6; f++){
						var registro = [];
						registro.push(info.registros6[f].nueve);
						registro.push(info.registros6[f].diez);
						registro.push(info.registros6[f].once);
						registro.push(info.registros6[f].doce);
						miData.push(registro);
					}
					var grid = Ext.getCmp('gridSeis');
					var store = grid.getStore();
					store.loadData(miData);
					
					var miData = new Array();					
					var fGrid7 = parseInt(contador7);
					if (fGrid7 >0){
						Ext.getCmp('sieteFilaGrid').show();
					} else {
						Ext.getCmp('sieteFilaGrid').hide();
					}
					for (var f=0; f<fGrid7; f++){
						var registro = [];
						registro.push(info.registros7[f].nueve);
						registro.push(info.registros7[f].diez);
						registro.push(info.registros7[f].once);
						registro.push(info.registros7[f].doce);
						miData.push(registro);
					}
					var grid = Ext.getCmp('gridSiete');
					var store = grid.getStore();
					store.loadData(miData);
					
					var fGrid8 = parseInt(contador8);
					if (fGrid8>0){
					Ext.getCmp('segundoMsgGrid').show();
					Ext.getCmp('gridOcho').setTitle(''+info.registros8[0].msg+'');
					} else {
						Ext.getCmp('segundoMsgGrid').hide();
					}
					
					var miData = new Array();					
					var fGrid9 = parseInt(contador9);
					if (fGrid9 >0){
						Ext.getCmp('nueveFilaGrid').show();
					} else {
						Ext.getCmp('nueveFilaGrid').hide();
					}
					for (var f=0; f<fGrid9; f++){
						var registro = [];
						registro.push(info.registros9[f].nueveUno);
						registro.push(info.registros9[f].nueveDos);
						miData.push(registro);
					}
					var grid = Ext.getCmp('gridNueve');
					var store = grid.getStore();
					store.loadData(miData);
					
					var miData = new Array();					
					var fGrid10 = parseInt(contador10);
					if (fGrid10 >0){
						Ext.getCmp('diezFilaGrid').show();
						var grid = Ext.getCmp('gridDiez');
						var colMod = grid.getColumnModel();
						var setHeader = colMod.setColumnHeader(1,'<b>'+info.registros10[0].diezDos+'</b>');
						var setHeader = colMod.setColumnHeader(0,'<b>'+info.registros10[0].diezUno+'</b>');
						for (var f=0; f<1; f++){
						var registro = [];
						registro.push(info.registros10[1].diezUno);
						registro.push(info.registros10[1].diezDos);
						miData.push(registro);
					}
					} else {
						Ext.getCmp('diezFilaGrid').hide();
					}					
					var grid = Ext.getCmp('gridDiez');
					var store = grid.getStore();
					store.loadData(miData);
					
					var miData = new Array();					
					var fGrid11 = parseInt(contador11);
					if (fGrid11 >0){
						Ext.getCmp('onceFilaGrid').show();
					} else {
						Ext.getCmp('onceFilaGrid').hide();
					}
					for (var f=0; f<fGrid11; f++){
						var registro = [];
						registro.push(info.registros11[f].onceUno);
						miData.push(registro);
					}
					var grid = Ext.getCmp('gridOnce');
					var store = grid.getStore();
					store.loadData(miData);
					
					var botonRecotizar  =info.botonRecotizar;
					var valorBtnRec = parseInt(botonRecotizar);
					if (valorBtnRec ===1){
						Ext.getCmp('recotizar').show();
					} else {
						Ext.getCmp('recotizar').hide();
					}
					var botonSalir1  =info.botonSalir1;
					var valorBtnSal = parseInt(botonSalir1);
					if (valorBtnSal ===1){
						Ext.getCmp('detalleSalir1').show();
						Ext.getCmp('detalleSalir').hide();
					} else {
						Ext.getCmp('detalleSalir1').hide();
						Ext.getCmp('detalleSalir').show();
					}
					
					var cancelado  = info.cancelado;
					var valorCanc  = parseInt(cancelado);
					if (valorCanc ===1){
						Ext.getCmp('imagenCancelado').show();						
					}
					fp.hide();
					gridConsulta.hide();
					fp2.show();
		  
				}//,	
           // callback: procesarVentana
          }); 
			}//handler
		}]
    }	
				
				
  ],			
  displayInfo: true,		
  emptyMsg: "No hay registros.",		
  loadMask: true,
  stripeRows: true,
  height: 450,
  width: 900,
  align: 'center',
  frame: true,
  bbar: {
    xtype: 'paging',
    pageSize: 15,
    buttonAlign: 'left',
    id: 'barraPaginacion',
    displayInfo: true,
    store: consultaData,
    displayMsg: '{0} - {1} de {2}',
    emptyMsg: "No hay registros.",
    items: [
      '->','-',
        {
        xtype: 'button',
        text: 'Generar Informaci�n',					
        tooltip:	'Generar Archivo',
        iconCls: 'icoXls',
        id: 'btnArchivoCSV',
        handler: function(boton, evento) {
        fp.el.mask('Generando Archivo...', 'x-mask-loading');
        var el = gridConsulta.getGridEl();	
        el.mask('Generando Archivo...', 'x-mask-loading');
       
        boton.setIconClass('loading-indicator');
        boton.setDisabled(true);
          var barraPaginacionA = Ext.getCmp("barraPaginacion");						
            Ext.Ajax.request({
            url: '22consulta1Ext01.data.jsp',
            params: Ext.apply(fp.getForm().getValues(),{							
              informacion: 'ArchivoCSV'
            }),
            success : function(response) {
              boton.setIconClass('icoXls');     
              boton.setDisabled(false);
            },	
            callback: procesarDescargaArchivos
          });						
        }
      },
      {
        xtype: 'button',
        text: 'Generar Archivo PDF',					
        tooltip:	'Imprimir',
        iconCls: 'icoPdf',
        id: 'btnArchivoPDF',
        handler: function(boton, evento) {
        fp.el.mask('Generando Archivo...', 'x-mask-loading');
        var el = gridConsulta.getGridEl();	
        el.mask('Generando Archivo...', 'x-mask-loading');
       
        boton.setIconClass('loading-indicator');
        boton.setDisabled(true);
          var barraPaginacionA = Ext.getCmp("barraPaginacion");						
            Ext.Ajax.request({
            url: '22consulta1Ext01.data.jsp',
            params: Ext.apply(fp.getForm().getValues(),{							
              informacion: 'ArchivoPDF',
              start: barraPaginacionA.cursor,
              limit: barraPaginacionA.pageSize
            }),
            success : function(response) {
              boton.setIconClass('icoPdf');     
              boton.setDisabled(false);
            },
            callback: procesarDescargaArchivos
          });						
        }
      }
    ]
  }
});//----------------------------------Fin GridConsulta

//-------------------------------Elementos Forma--------------------------------
var  elementosForma =  [
{
  xtype				: 'compositefield',
  id					:'filaUno',
  combineErrors	: false,
  msgTarget		: 'side',
  items			:[{
    xtype		: 'datefield',
    fieldLabel	: 'Fecha de Solicitud',
    name			: '_fechaReg',
    id			: 'fechaRegistro',
    hiddenName: 'fecha_registro',
    allowBlank: true,
    startDay	: 0,
    width		: 140,
	 minValue	: '01/01/1901',
    msgTarget	: 'side',
    margins		: '0 20 0 0'
    },	{
    xtype			: 'displayfield',
    value			: ' a: ',
    width			: 30
    },	{
    xtype			: 'datefield',
    name				: '_fechaFinReg',
    id				: 'fechaFinRegistro', 
    hiddenName	: 'fecha_fin_reg',
    allowBlank	: true,
    startDay	: 0,
    width		: 140,
	 minValue	: '01/01/1901',
    msgTarget	: 'side',
    margins		: '0 20 0 0'
    },{
    xtype			: 'displayfield',
    value			: 'Moneda: ',
    width			: 102
    },{
	  xtype			: 'combo',	
	  name			: 'cmb_mon',	
	  hiddenName	: 'cmb_mon_h',	
	  id				: 'cmbmon',	
	  displayField : 'DESCRIPCION',
	  valueField 	: 'CLAVE',
	  //emptyText		: 'Seleccionar',
	  mode				: 'local',
	  forceSelection : true,	
	  triggerAction  : 'all',	
	  editable    : true,
	  typeAhead		: true,
	  minChars 		: 1,	
	  store 			: catalogoMoneda
	}]
  },{
  xtype				: 'compositefield',
  id					:'filaDos',
  combineErrors	: false,
  msgTarget		: 'side',
  items			:[
		{
		  xtype			: 'textfield',
		  fieldLabel	: 'N�mero de solicitud',
		  id			   : 'numSol',
		  name		   : '_numSol',  
		  hiddenName	: 'h_numSol',
		  allowBlank	: true,
		  maxLength 	: 12,
		  msgTarget		: 'side',
		  width			: 140
	  },{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 205
		},{
		 xtype			: 'displayfield',
		 value			: 'Monto del Cr�dito:',
		 width			: 102
		},{
		  xtype			: 'numberfield',
		  id			   : 'monto1',
		  name		   : 'montoIni',  
		  hiddenName	: 'h_montoIni',
		  allowBlank	: true,
		  msgTarget		: 'side',
		  maxLength 	: 24,
		  width			: 100
	  },{
		 xtype			: 'displayfield',
		 value			: ' a:',
		 width			: 30
		},{
		  xtype			: 'numberfield',
		  id			   : 'monto2',  
		  name		   : 'montoFin',  
		  hiddenName	: 'h_montoFin',
		  allowBlank	: true,
		  maxLength 	: 24,
		  width			: 100,
		  msgTarget		: 'side',
		  margins		: '0 20 0 0'
	  }
  
  ]},{
  xtype				: 'compositefield',
  id					:'filaTres',
  combineErrors	: false,
  msgTarget		: 'side',
  items			:[{
	  xtype			: 'combo',	
	  name			: 'cmb_ti',	
	  hiddenName	: 'cmb_ti',	
	  id				: 'cmbti',	
	  fieldLabel	: 'Intermediario Financiero', 
	  displayField	: 'DESCRIPCION',
	  valueField 	: 'CLAVE',
	  emptyText		: 'Seleccionar',
	  forceSelection : true,	
	  triggerAction  : 'all',	
	  mode				: 'local',
	  editable    : true,
	  minChars 		: 1,
	  typeAhead		: true,
	  store 			: catalogoIntermediario,
	  width			: 335
  },{
	 xtype			: 'displayfield',
	 value			: '',
	 width			: 10
	},{
	 xtype			: 'displayfield',
	 value			: 'Estatus:',
	 width			: 102
	},{
  xtype				: 'combo',
  name			 	: 'cmb_tipo',
  hiddenName		: 'cmb_tipo',
  id          		: 'cmbTipo', 
  displayField		: 'DESCRIPCION',
  valueField 		: 'CLAVE',
  emptyText		: 'Seleccionar',
  forceSelection : true,	
  triggerAction 	: 'all',	
  mode				: 'local',
  editable    : true,
  typeAhead		: true,
  minChars 		: 1,	
  store 			: estatus,	
  listeners		: {
    select		: {
      fn			: function(combo) {
                }// FIN fn
              }//FIN select
            }//FIN listeners
  }
  ]},{
	  xtype			: 'combo',	
	  name			: 'cmb_per',	
	  hiddenName	: 'cmb_per',	
	  id				: 'cmbPer',	
	  fieldLabel	: 'Persona Responsable(Ejecutivo)', 
	  displayField : 'DESCRIPCION',
	  valueField 	: 'CLAVE',
	  forceSelection : true,	
	  triggerAction  : 'all',	
	  mode				: 'local',
	  editable     : true,
	  minChars 		: 1,
	  typeAhead		: true,
	  store 			: catalogoPersona,
	  anchor			:'55.5%'
  }
];//-----------------------------Fin Elementos Forma------------------------------


//-------------------------------Panel Consulta fp2---------------------------------
var fp2 = new Ext.form.FormPanel({
  id					:'fp2',
  width				:910,
  heigth				:'auto',
  frame				:true,
  collapsible		:true,
  hidden				: true,
  titleCollapse	:false,
  style				:'margin:0 auto;',
  bodyStyle			:'padding: 6px',
  labelWidth		:150,
  //monitorValid	:true,	
  defaults			:{
            msgTarget: 'side',
            anchor: '-20'
            },
  items		:[
	  {
	  xtype				: 'compositefield',
	  id					:'primerFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 50
		},gridUno
		]},{
	  xtype				: 'compositefield',
	  id					:'primerMsgGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridDos
		]},{
	  xtype				: 'compositefield',
	  id					:'terceraFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridTres
		]},{
	  xtype				: 'compositefield',
	  id					:'cuartaFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridCuatro
		]},{
	  xtype				: 'compositefield',
	  id					:'quintaFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridCinco
		]},{
	  xtype				: 'compositefield',
	  id					:'sextaFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridSeis
		]},{
	  xtype				: 'compositefield',
	  id					:'sieteFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridSiete
		]},{
	  xtype				: 'compositefield',
	  id					:'segundoMsgGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridOcho
		]},{
	  xtype				: 'compositefield',
	  id					:'nueveFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridNueve
		]},{
	  xtype				: 'compositefield',
	  id					:'diezFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridDiez
		]},{
	  xtype				: 'compositefield',
	  id					:'onceFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridOnce
		]}
            ],
  buttons		:[{
            text				:'Re-Cotizar',
            id					:'recotizar',
            iconCls			:'icoModificar',
            handler			:function(boton, evento){
					window.location  = "/nafin/22cotizador/SolCotEsp/CapturaDisp.jsp"; 
					}//fin handler
            },{
            text				:'Generar Archivo PDF',
            id					:'btnDetallePdf',
            iconCls			:'icoPdf',
            handler			:function(boton, evento){

				  //if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
					 var archivo = miPDF;				
					 archivo = archivo.replace('/nafin','');
					 var params = {nombreArchivo: archivo};				
					 fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					 fp.getForm().getEl().dom.submit();
					/* fp.el.unmask();
					 var el = gridConsulta.getGridEl();
					 el.unmask();
					 var btncsv = Ext.getCmp('btnArchivoCSV');
					 btncsv.setIconClass('icoXls');     
					 btncsv.setDisabled(false);
					 var btnpdf = Ext.getCmp('btnArchivoPDF');
					 btnpdf.setIconClass('icoPdf');     
					 btnpdf.setDisabled(false);	
				
				  }else {
					 NE.util.mostrarConnError(response,opts);
					 fp.el.unmask();
				  }*/

                      }
            },{
            text				:'Salir.',
            id					:'detalleSalir1',
            iconCls			:'icoRegresar',
            handler			:function(boton, evento){    
					window.location  = "/nafin/22cotizador/SolCotEsp/CapturaSolicitud.jsp"; 
            }//fin handler
            },{
            text				:'Salir',
            id					:'detalleSalir',
            iconCls			:'icoRegresar',
            handler			:function(boton, evento){     
					fp2.hide();
					fp.show();
					gridConsulta.show();
					Ext.getCmp('imagenCancelado').hide();	
            }//fin handler
            }]
});//------------------------------Fin Panel Consulta fp2-----------------------------

//-------------------------------Panel Consulta---------------------------------
var fp = new Ext.form.FormPanel({
  id					:'formcon',
  width				:910,
  heigth				:'auto',
  title				:'Consulta de Solicitudes',
  frame				:true,
  collapsible		:true,
  titleCollapse	:false,
  style				:'margin:0 auto;',
  bodyStyle			:'padding: 6px',
  labelWidth		:150,
  //monitorValid	:true,	
  defaults			:{
            msgTarget: 'side',
            anchor: '-20'
            },
  items				:[ 
            elementosForma
            ],
  buttons		:[{
            text				:'Consultar',
            id					:'consultar',
            iconCls			:'icoBuscar',
            handler			:function(boton, evento){     
					Ext.getCmp('gridConsulta').hide;
					 
					if (Ext.getCmp('fechaRegistro').isValid() & Ext.getCmp('fechaFinRegistro').isValid()){
					}else{return;}
					if(Ext.getCmp('fechaRegistro').getValue()==''||Ext.getCmp('fechaFinRegistro').getValue()==''){
						if(Ext.getCmp('fechaRegistro').getValue()=='' && Ext.getCmp('fechaFinRegistro').getValue()!='' ){
							Ext.getCmp('fechaRegistro').markInvalid('Ambas fechas son obligatorias');
							return;
						}else if(Ext.getCmp('fechaRegistro').getValue()!='' && Ext.getCmp('fechaFinRegistro').getValue()=='' ){
							Ext.getCmp('fechaFinRegistro').markInvalid('Ambas fechas son obligatorias');
							return;
						}
					}
					if ((Ext.getCmp('fechaRegistro').getValue() > Ext.getCmp('fechaFinRegistro').getValue())&&(Ext.getCmp('fechaRegistro').getValue()!=''&&Ext.getCmp('fechaFinRegistro').getValue()!='')){
						Ext.getCmp('fechaFinRegistro').markInvalid('La fecha final no puede ser menor a la fecha de inicio.');
						return;
					}else{
						Ext.getCmp('fechaFinRegistro').clearInvalid();
					}
					  
					if(Ext.getCmp('monto1').getValue() > Ext.getCmp('monto2').getValue()){
						Ext.getCmp('monto2').markInvalid('El monto final debe ser mayor que el inicial.');
						return;
					}else{
						Ext.getCmp('monto2').clearInvalid();
					}
					
					if (true) {//validaciones
						fp.el.mask('Cargando...', 'x-mask-loading');	
						consultaData.load({
							params		:Ext.apply(fp.getForm().getValues(),{
							informacion	:'Consultar',
							operacion	:'Generar',
							start	:0,
							limit	:15
							})
						});
					}
            }//fin handler
            },{
            text				:'Limpiar',
            id					:'btnLimpiar',
            iconCls			:'icoLimpiar',
            handler			:function(boton, evento){
                      Ext.getCmp('formcon').getForm().reset();    
							 Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'cargaFecha'	},	callback:procesarFecha });
                      Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogomon'	}});
							 Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoint'}});
							 Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoEstatus'}});
							 Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoPersona'}});
							 
							 Ext.getCmp('gridConsulta').hide();
                      }
            }]
});//------------------------------Fin Panel Consulta------------------------------


//----------------------------Contenedor Principal------------------------------
var pnl = new Ext.Container({
  id			:'contenedorPrincipal',
  applyTo	:'areaContenido',
  width		: 949,
  style		:'margin:0 auto;',
  items		:[
        NE.util.getEspaciador(20),
        fp,
        NE.util.getEspaciador(20),
        gridConsulta, 
		  NE.util.getEspaciador(20),
		  fp2,
		  imagenCancelado
        ]
});//-----------------------------Fin Contenedor Principal-------------------------


Ext.Ajax.request({
	url: '22consulta1Ext01.data.jsp',
	params: {
		informacion: 'cargaFecha'
	},
	callback:procesarFecha 
});

});//-----------------------------------------------Fin Ext.onReady(function(){}