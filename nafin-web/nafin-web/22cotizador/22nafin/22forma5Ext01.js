Ext.onReady(function() {
	 
var myValidFn = function(v) {
	var myRegex = /^.+\.([dDxXpP][oOlLpPdD][cCsStTfF])$/;
	return myRegex.test(v);
};

Ext.apply(Ext.form.VTypes, {
	archivozip 		: myValidFn,
	archivozipText 	: 'Formato de archivo no v�lido.<br> Formato soportado: <br>doc - xls - ppt - pdf</div>'
});

//-----------------------------procesarConsultaData-----------------------------
function TransmiteAgregaarch(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				Ext.Msg.show({
							title: 'Guardar',
							msg: 'Su informaci�n ha sido guardada con �xito.',
							modal: true,
							icon: Ext.Msg.INFO,
							buttons: Ext.Msg.OK,
							fn: function (){
								Ext.getCmp('idDescripcion').reset();
								Ext.getCmp('archivoCarga').reset();
								Ext.getCmp('claAr').reset();
								fp.el.mask('Cargando...', 'x-mask-loading');			
									consultaData.load();
								}
							});
			}
		} else {
			formaInicial.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  
var fp = Ext.getCmp('forma');
  fp.el.unmask();							
  var gridConsulta = Ext.getCmp('grid');	
  var el = gridConsulta.getGridEl();		
  if (arrRegistros != null) {
    if (!gridConsulta.isVisible()) {
      gridConsulta.show();
      }				
    if(store.getTotalCount() > 0) {		
      el.unmask();
    } else {		
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
};
//--------------------------Fin procesarConsultaData----------------------------
	
//-------------------------------ConsultaData-----------------------------------
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '22forma5Ext01.data.jsp',
		baseParams: {
			informacion: 'carga'
		},		
		fields: [	
			{	name: 'CLAVE'},
			{	name: 'NOMBRE'},
			{	name: 'DESCRIPCION'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
//----------------------------Fin ConsultaData----------------------------------

//-------------------------------elementosForma---------------------------------	
var elementosForma =  [	
	{
	 xtype	: 'textfield',
	 id      : 'idDescripcion',
	 name		: 'Desc_Archivo',
	 hiddenName: 'Desc_Archivo_hidden',
	 fieldLabel: 'Descripci�n',
	 anchor : '80%',
	 msg: 'side',
	 allowBlank : false,
	 margins		: '0 20 0 0'
	}, {
	xtype: 'fileuploadfield',
	id: 'archivoCarga',
	allowBlank : false,
	name: 'archivoPC',   
	buttonCfg: {
		text:'Examinar'
	},
	anchor: '90%',	vtype: 'archivozip'
	},	{
	 xtype	: 'textfield',
	 id      : 'claAr',
	 name		: 'claveArc',
	 hiddenName: 'claveArc_hid',
	 fieldLabel: 'clave',
	 anchor : '76%',
	 msg: 'side',
	 hidden: true,
	 margins		: '0 20 0 0'
	}
];
//-----------------------------Fin elementosForma-------------------------------

//--------------------------------gridConsulta----------------------------------	
var gridConsulta = new Ext.grid.EditorGridPanel({	
		store				:consultaData,
		id					:'grid',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'<center>Alta de Archivos</center>',	
		//hidden			:true,
		columns			:[
    {
    header:'Descripci�n',
    tooltip		:'Descripci�n',
    dataIndex	:'DESCRIPCION',
    sortable	:true,
    width			:350,			
    resizable	:true,
    align			:'center',
    renderer:function(value){
      return "<div align='left'>"+value+"</div>";
      }
    },{
    header:'Nombre del Archivo',
    tooltip		:'Nombre del Archivo',
    dataIndex	:'NOMBRE',
    sortable	:true,
    width			:150,			
    resizable	:true,
    align			:'center'
    },{
    xtype		: 'actioncolumn',
    header	: 'Seleccionar',
    tooltip	: 'Seleccionar',				
    align		: 'center',
    items		: [
     {
      getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
        this.items[0].tooltip = 'Modificar';
        return 'icoModificar';										
      },
      handler: function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		Ext.getCmp('idDescripcion').setValue(registro.get('DESCRIPCION'));
		Ext.getCmp('claAr').setValue(registro.get('CLAVE'));
		Ext.getCmp('archivoCarga').reset();
		Ext.getCmp('btnGuardar').show();
		Ext.getCmp('btnConsulta').hide();
		
	   }//fin handler
    },
    {
      getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
        this.items[1].tooltip = 'Eliminar';
        return 'cancelar';										
      },
      handler: function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		Ext.Ajax.request({
			url: '22forma5Ext01.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
			  informacion: 'Eliminar',
			  id: registro.get('CLAVE')
			}),
			success : function(a,b) {
			 
				  Ext.Msg.show({
				  title: 'Eliminar',
				  msg: 'Su archivo fue eliminado con exito',
				  modal: true,
				  icon: Ext.Msg.INFO,
				  buttons: Ext.Msg.OK,
				  fn: function (){
						Ext.getCmp('idDescripcion').reset();
						Ext.getCmp('archivoCarga').reset();
						Ext.getCmp('claAr').reset();
						Ext.getCmp('btnGuardar').hide();
						Ext.getCmp('btnConsulta').show();
					   fp.el.mask('Cargando...', 'x-mask-loading');			
					   consultaData.load();
					 }
				  });//fin Msg
				}//fin success
		 });//fin Ajax
		 
      
      }//fin handler
    } 
  ]
	}   
],			
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 350,
		width				: 630,
		align				: 'center',
		frame				: true
	});	
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: '<center>Alta de Archivos</center>',
		frame: true,		
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
		{
		text: 'Guardar',
		id: 'btnGuardar',
		iconCls: 'icoGuardar',
		hidden: true,
		handler: function (boton, evento){
		if ( Ext.getCmp('idDescripcion').isValid() ){
			if ( Ext.getCmp('archivoCarga').getValue( ) =="" ){
					Ext.Msg.show({
					title: 'Archivo',
					msg: 'El archivo no existe favor de verificarlo.',
					modal: true,
					icon: Ext.Msg.INFO,
					buttons: Ext.Msg.OK,
					fn: function (){
						Ext.getCmp('idDescripcion').reset();
						Ext.getCmp('archivoCarga').reset();
						Ext.getCmp('btnGuardar').hide();
						Ext.getCmp('btnConsulta').show();
						Ext.getCmp('claAr').reset();
						fp.el.mask('Cargando...', 'x-mask-loading');			
						consultaData.load();
						}
					});
			 } else if ( ! Ext.getCmp('archivoCarga').isValid()){
					Ext.Msg.show({
					title: 'Alta de Archivos',
					msg: 'La extension del archivo no esta permitida.',
					modal: true,
					icon: Ext.Msg.INFO,
					buttons: Ext.Msg.OK,
					fn: function (){
						var arc = Ext.getCmp('archivoCarga');
						 arc.focus();
						}
					});
				} else {
					// Todo OK GUARDAR   
					var desc = Ext.getCmp('idDescripcion').getValue();
					var form = fp.getForm();
					form.submit({
			       url:'22forma5Ext01.data.jsp?informacion=subirArchivo',	
					success: function(form, action, resp) {
						
						var jsonData=Ext.decode(action.response.responseText);
						if(jsonData.valida=='cero'){
							Ext.Msg.show({
							title: 'Guardar',
							msg: 'El archivo no existe, favor de verificarlo',
							modal: true,
							icon: Ext.Msg.INFO,
							buttons: Ext.Msg.OK,
							fn: function (){
								var arc = Ext.getCmp('archivoCarga');
								 arc.focus();
								}
							});
						}else if(jsonData.valida=='espacios'){
							Ext.Msg.show({
							title: 'Guardar',
							msg: 'El nombre del archivo no debe contener espacios',
							modal: true,
							icon: Ext.Msg.INFO,
							buttons: Ext.Msg.OK,
							fn: function (){
							var arc = Ext.getCmp('archivoCarga');
								 arc.focus();
								}
							});
						} else if(jsonData.valida=='ok'){
						} else if(jsonData.valida=='ruta'){
							Ext.Msg.show({
								title: 'Alta de Archivos',
								msg: 'La ruta no existe.',
								modal: true,
								icon: Ext.Msg.INFO,
								buttons: Ext.Msg.OK,
								fn: function (){
										
									Ext.getCmp('idDescripcion').reset();
									Ext.getCmp('archivoCarga').reset();
									Ext.getCmp('btnGuardar').hide();
									Ext.getCmp('btnConsulta').show();
									Ext.getCmp('claAr').reset();
						
									fp.el.mask('Cargando...', 'x-mask-loading');			
										consultaData.load();
									}
								});
						}
						if(jsonData.carga=='ok'){
							var arch = Ext.getCmp('archivoCarga').getValue();
						
							Ext.Ajax.request({
								url : '22forma5Ext01.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion:'NuevoArchivo',
									nombreArchivo : arch
								}),
								callback: TransmiteAgregaarch
							});
								
						}
					},
					failure: NE.util.mostrarSubmitError
					});
					//FIN OK
					}// else
				 } else {
					Ext.Msg.show({
					title: 'Descripci�n',
					msg: 'Introduzca una Descripci�n',
					modal: true,
					icon: Ext.Msg.INFO,
					buttons: Ext.Msg.OK,
					fn: function (){
						var dec = Ext.getCmp('idDescripcion');
						 dec.focus();
						}
					});
		 }
		}//fin handler
		},{
		text: 'Agregar',
		id: 'btnConsulta',
		iconCls: 'icoAceptar',
		handler: function (boton, evento){
		 if ( Ext.getCmp('idDescripcion').isValid() ){
			if ( Ext.getCmp('archivoCarga').getValue( ) =="" ){
					Ext.Msg.show({
					title: 'Archivo',
					msg: 'Introduzca una ruta de archivo.',
					modal: true,
					icon: Ext.Msg.INFO,
					buttons: Ext.Msg.OK,
					fn: function (){
						var arc = Ext.getCmp('archivoCarga');
						 arc.focus();
						}
					});
			 } else if ( ! Ext.getCmp('archivoCarga').isValid()){
					Ext.Msg.show({
					title: 'Alta de Archivos',
					msg: 'La extension del archivo no esta permitida.',
					modal: true,
					icon: Ext.Msg.INFO,
					buttons: Ext.Msg.OK,
					fn: function (){
						var arc = Ext.getCmp('archivoCarga');
						 arc.focus();
						}
					});
				} else {
					// Todo OK
					
					var desc = Ext.getCmp('idDescripcion').getValue();
				
					var form = fp.getForm();
					form.submit({
			      url:'22forma5Ext01.data.jsp?informacion=subirArchivo',
					success: function(form, action, resp) {
						var jsonData=Ext.decode(action.response.responseText);
						if(jsonData.valida=='cero'){
							Ext.Msg.show({
							title: 'Archivo',
							msg: 'El archivo no existe, favor de verificarlo',
							modal: true,
							icon: Ext.Msg.INFO,
							buttons: Ext.Msg.OK,
							fn: function (){
								var arc = Ext.getCmp('archivoCarga');
								 arc.focus();
								}
							});
						}else if(jsonData.valida=='espacios'){
						Ext.Msg.show({
							title: 'Agregar',
							msg: 'El nombre del archivo no debe contener espacios',
							modal: true,
							icon: Ext.Msg.INFO,
							buttons: Ext.Msg.OK,
							fn: function (){
								}
							});
						} else if(jsonData.valida=='ok'){
						} else if(jsonData.valida=='ruta'){
							Ext.Msg.show({
								title: 'Alta de Archivos',
								msg: 'La ruta no existe.',
								modal: true,
								icon: Ext.Msg.INFO,
								buttons: Ext.Msg.OK,
								fn: function (){
									Ext.getCmp('idDescripcion').reset();
									Ext.getCmp('archivoCarga').reset();
									Ext.getCmp('btnGuardar').hide();
									Ext.getCmp('btnConsulta').show();
									Ext.getCmp('claAr').reset();
									fp.el.mask('Cargando...', 'x-mask-loading');			
										consultaData.load();
									}
								});
						}
						if(jsonData.carga=='ok'){
							var arch = Ext.getCmp('archivoCarga').getValue();
							Ext.Ajax.request({
								url : '22forma5Ext01.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion:'NuevoArchivo',
									nombreArchivo : arch
								}),
								callback: TransmiteAgregaarch
							});
								
						}
					},
					failure: NE.util.mostrarSubmitError
					});
					//FIN OK
					}// else
		 } else {
					Ext.Msg.show({
					title: 'Descripci�n',
					msg: 'Introduzca una Descripci�n',
					modal: true,
					icon: Ext.Msg.INFO,
					buttons: Ext.Msg.OK,
					fn: function (){
						var dec = Ext.getCmp('idDescripcion');
						 dec.focus();
						}
					});
				}

				}//fin Handler
			}
		]
	});
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});	
//-----------------------------Fin Contenedor Principal-------------------------

consultaData.load();
});//-----------------------------------------------Fin Ext.onReady(function(){}