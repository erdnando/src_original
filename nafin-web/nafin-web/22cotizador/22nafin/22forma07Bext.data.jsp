<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		java.math.*,
		com.netro.cotizador.*,
		com.netro.cotizador.Solicitud,
		netropology.utilerias.usuarios.Usuario"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion        = request.getParameter("informacion")        == null ? "": (String)request.getParameter("informacion");
String accion             = request.getParameter("accion")             == null ? "": (String)request.getParameter("accion");
String nombre_ejecutivo   = request.getParameter("nombre_ejecutivo")   == null ? "": (String)request.getParameter("nombre_ejecutivo");
String cg_mail            = request.getParameter("cg_mail")            == null ? "": (String)request.getParameter("cg_mail");
String cg_telefono        = request.getParameter("cg_telefono")        == null ? "": (String)request.getParameter("cg_telefono");
String cg_fax             = request.getParameter("cg_fax")             == null ? "": (String)request.getParameter("cg_fax");
String ejecutivo_nafin    = request.getParameter("ejecutivo_nafin")    == null ? "": (String)request.getParameter("ejecutivo_nafin");
String ig_tipo_piso       = request.getParameter("ig_tipo_piso")       == null ? "": (String)request.getParameter("ig_tipo_piso");
String cg_razon_social_if = request.getParameter("cg_razon_social_if") == null ? "": (String)request.getParameter("cg_razon_social_if");
String ig_riesgo_if       = request.getParameter("ig_riesgo_if")       == null ? "": (String)request.getParameter("ig_riesgo_if");
String cg_acreditado      = request.getParameter("cg_acreditado")      == null ? "": (String)request.getParameter("cg_acreditado");
String ig_valor_riesgo_ac = request.getParameter("ig_valor_riesgo_ac") == null ? "": (String)request.getParameter("ig_valor_riesgo_ac");
String df_solicitud       = request.getParameter("df_solicitud")       == null ? "": (String)request.getParameter("df_solicitud");
String df_cotizacion      = request.getParameter("df_cotizacion")      == null ? "": (String)request.getParameter("df_cotizacion");
String cg_programa        = request.getParameter("cg_programa")        == null ? "": (String)request.getParameter("cg_programa");
String ic_moneda          = request.getParameter("ic_moneda")          == null ? "": (String)request.getParameter("ic_moneda");
String fn_monto           = request.getParameter("fn_monto")           == null ? "": (String)request.getParameter("fn_monto");
String ic_tipo_tasa       = request.getParameter("ic_tipo_tasa")       == null ? "": (String)request.getParameter("ic_tipo_tasa");
String in_num_disp        = request.getParameter("in_num_disp")        == null ? "": (String)request.getParameter("in_num_disp");
String ig_plazo_conv      = request.getParameter("ig_plazo_conv")      == null ? "": (String)request.getParameter("ig_plazo_conv");
String cg_ut_plazo        = request.getParameter("cg_ut_plazo")        == null ? "": (String)request.getParameter("cg_ut_plazo");
String ig_plazo           = request.getParameter("ig_plazo")           == null ? "": (String)request.getParameter("ig_plazo");
String ic_esquema_recup   = request.getParameter("ic_esquema_recup")   == null ? "": (String)request.getParameter("ic_esquema_recup");
String cg_ut_ppi          = request.getParameter("cg_ut_ppi")          == null ? "": (String)request.getParameter("cg_ut_ppi");
String ig_ppi             = request.getParameter("ig_ppi")             == null ? "": (String)request.getParameter("ig_ppi");
String cg_ut_ppc          = request.getParameter("cg_ut_ppc")          == null ? "": (String)request.getParameter("cg_ut_ppc");
String ig_ppc             = request.getParameter("ig_ppc")             == null ? "": (String)request.getParameter("ig_ppc");
String pfpc               = request.getParameter("pfpc")               == null ? "": (String)request.getParameter("pfpc");
String pufpc              = request.getParameter("pufpc")              == null ? "": (String)request.getParameter("pufpc");
String idcurva            = request.getParameter("idcurva")            == null ? "": (String)request.getParameter("idcurva");
String id_interpolacion   = request.getParameter("id_interpolacion")   == null ? "": (String)request.getParameter("id_interpolacion");
String metodoCalculo      = request.getParameter("metodoCalculo")      == null ? "": (String)request.getParameter("metodoCalculo");
String ic_solic_esp       = request.getParameter("ic_solic_esp")       == null ? "": (String)request.getParameter("ic_solic_esp");
String ic_ejecutivo       = request.getParameter("ic_ejecutivo")       == null ? "": (String)request.getParameter("ic_ejecutivo");
String ig_plazo_max_oper  = request.getParameter("ig_plazo_max_oper")  == null ? "": (String)request.getParameter("ig_plazo_max_oper");
String ig_num_max_disp    = request.getParameter("ig_num_max_disp")    == null ? "": (String)request.getParameter("ig_num_max_disp");
String ig_plazo_conv_ant  = request.getParameter("ig_plazo_conv_ant")  == null ? "": (String)request.getParameter("ig_plazo_conv_ant");
String num_disp_capt      = request.getParameter("num_disp_capt")      == null ? "": (String)request.getParameter("num_disp_capt");
String fn_monto_grid      = request.getParameter("fn_monto_grid")      == null ? "": (String)request.getParameter("fn_monto_grid");
String disposicion_grid   = request.getParameter("disposicion_grid")   == null ? "": (String)request.getParameter("disposicion_grid");
String vencimiento_grid   = request.getParameter("vencimiento_grid")   == null ? "": (String)request.getParameter("vencimiento_grid");

String nombre_moneda        = request.getParameter("nombre_moneda")        == null ? "": (String)request.getParameter("nombre_moneda");
String tasa_unica           = request.getParameter("tasa_unica")           == null ? "": (String)request.getParameter("tasa_unica");
String esquema_recup        = request.getParameter("esquema_recup")        == null ? "": (String)request.getParameter("esquema_recup");
String periodo_tasa_interes = request.getParameter("periodo_tasa_interes") == null ? "": (String)request.getParameter("periodo_tasa_interes");
String periodo_tasa_capital = request.getParameter("periodo_tasa_capital") == null ? "": (String)request.getParameter("periodo_tasa_capital");
String ppv                  = request.getParameter("ppv")                  == null ? "": (String)request.getParameter("ppv");
String ppv_360              = request.getParameter("ppv_360")              == null ? "": (String)request.getParameter("ppv_360");
String duracion             = request.getParameter("duracion")             == null ? "": (String)request.getParameter("duracion");
String duracion_360         = request.getParameter("duracion_360")         == null ? "": (String)request.getParameter("duracion_360");
String tre                  = request.getParameter("tre")                  == null ? "": (String)request.getParameter("tre");
String tre_con_acarreo      = request.getParameter("tre_con_acarreo")      == null ? "": (String)request.getParameter("tre_con_acarreo");
String impuesto_usd         = request.getParameter("impuesto_usd")         == null ? "": (String)request.getParameter("impuesto_usd");
String costo_all_in         = request.getParameter("costo_all_in")         == null ? "": (String)request.getParameter("costo_all_in");
String factores_riesgo_usd  = request.getParameter("factores_riesgo_usd")  == null ? "": (String)request.getParameter("factores_riesgo_usd");
String tasa_credito         = request.getParameter("tasa_credito")         == null ? "": (String)request.getParameter("tasa_credito");
String numero_solicitud     = request.getParameter("numero_solicitud")     == null ? "": (String)request.getParameter("numero_solicitud");
String datos_gridDD         = request.getParameter("datos_gridDD")         == null ? "": (String)request.getParameter("datos_gridDD");
String datos_gridPP         = request.getParameter("datos_gridPP")         == null ? "": (String)request.getParameter("datos_gridPP");
String ic_proc_pago         = request.getParameter("ic_proc_pago")         == null ? "0":(String)request.getParameter("ic_proc_pago");
String pagos                = request.getParameter("pagos")                == null ? "": (String)request.getParameter("pagos");

String mensaje       = "";
String consulta      = "";
String nombreArchivo = "";
String infoRegresar  = "";
JSONObject resultado = new JSONObject();
boolean success      = true;
int start = 0;
int limit = 0;

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("Inicializacion")){
	
	JSONArray regDetalleDisp = new JSONArray();
	JSONArray detalleDisp    = new JSONArray();
	JSONArray planPagosCap   = new JSONArray();
	JSONArray regPlanPagos   = new JSONArray();

	success = true;

	if(pfpc.equals("NaN/NaN/0NaN")){pfpc = ""; }
	if(pufpc.equals("NaN/NaN/0NaN")){ pufpc = ""; }

	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	Solicitud solicitud = new Solicitud();

	solicitud.setFn_monto(Double.parseDouble(fn_monto.trim()));
	solicitud.setIc_ejecutivo(ic_ejecutivo.trim());
	solicitud.setCg_telefono(cg_telefono.trim());
	solicitud.setCg_fax(cg_fax.trim());
	solicitud.setCg_mail(cg_mail.trim());
	solicitud.setNombre_ejecutivo(nombre_ejecutivo.trim());
	solicitud.setDf_solicitud(df_solicitud.trim());
	solicitud.setDf_cotizacion(df_cotizacion.trim());
	solicitud.setCg_programa(cg_programa.trim());
	solicitud.setIc_moneda(ic_moneda.trim());
	solicitud.setIc_tipo_tasa(ic_tipo_tasa.trim());
	solicitud.setIn_num_disp(in_num_disp.trim());
	solicitud.setIg_tipo_piso(ig_tipo_piso.trim());
	solicitud.setIg_plazo_max_oper(ig_plazo_max_oper.trim());
	solicitud.setIg_num_max_disp(ig_num_max_disp.trim());
	solicitud.setIg_plazo(ig_plazo.trim());
	solicitud.setIc_esquema_recup(ic_esquema_recup.trim());
	solicitud.setIg_ppi(ig_ppi.trim());
	solicitud.setCg_ut_ppi(cg_ut_ppi.trim());
	solicitud.setIg_ppc(ig_ppc.trim());
	solicitud.setCg_ut_ppc(cg_ut_ppc.trim());
	solicitud.setCg_razon_social_if(cg_razon_social_if.trim());
	solicitud.setIg_riesgo_if(ig_riesgo_if.trim());
	solicitud.setCg_acreditado(cg_acreditado.trim());
	solicitud.setIg_valor_riesgo_ac(ig_valor_riesgo_ac.trim());
	solicitud.setCg_ut_plazo(cg_ut_plazo.trim());
	//solicitud.setIg_plazo(ig_plazo_conv.trim());
	solicitud.setCg_pfpc(pfpc.trim());
	solicitud.setCg_pufpc(pufpc.trim());
	solicitud.setIdCurva(idcurva.trim());
	solicitud.setMetodoCalculo(metodoCalculo);
	solicitud.setId_interpolacion(id_interpolacion);
	if(ic_proc_pago.equals("")){
		ic_proc_pago = "0";
	}
        try{
            Integer.parseInt(ic_proc_pago);
        }catch(NumberFormatException nfe){
            ic_proc_pago = "0";
        }
        System.out.println("ic_proc_pago "+ic_proc_pago);
	solicitud.setIc_proc_pago(Integer.parseInt(ic_proc_pago));

	String[] df_disposicion = null;
	String[] df_vencimiento = null;
	String[] fn_monto_disp = null;
		
	df_disposicion = disposicion_grid.split(",");
	df_vencimiento = vencimiento_grid.split(",");
	fn_monto_disp  = fn_monto_grid.split(",");

	solicitud.setFn_monto_disp(fn_monto_disp);
	solicitud.setDf_disposicion(df_disposicion);
	//if(accion.equals("GUARDAR")){
		solicitud.setDf_vencimiento(df_vencimiento);
	//}

	solicitud = solCotEsp.complementaDatos(solicitud);

	String esquemaRecup = solicitud.getIc_esquema_recup();
	String cgutppc = solicitud.getCg_ut_ppc();

	try{
		if(esquemaRecup.equals("2") && !cgutppc.equals("0")){
			Hashtable respuesta = solCotEsp.getValidaFechasPagos(pfpc, pufpc, solicitud.getCg_ut_ppc());
			int pagosCap = ((Integer)respuesta.get("Npagos")).intValue();
			pagosCap += 2;
			solicitud.setIg_ppc(String.valueOf(pagosCap));
			//System.out.println("ppc::: " + solicitud.getIg_ppc());
			String dia_pago = pufpc.substring(0,2);
			solicitud.setIn_dia_pago(dia_pago);
			//System.out.println("dia_pago::: "+dia_pago);
		}
		solicitud = solCotEsp.CalculoTasasUSD(solicitud, false);
		if(accion.equals("GUARDAR")){
			solCotEsp.guardaCotizacionUSD(solicitud, strNombreUsuario, iNoUsuario);
		}
	} catch(Exception e){
		success = false;
		mensaje = "Error al validar las fechas de pagos. " + e;
	}

	if(success == true){

		String acreditado  = "";
		String tasaCredito = solicitud.getDescTasaIndicativa() + solicitud.formatoDecimal(solicitud.getTasaCreditoUSD())+" "+solicitud.getPeriodoTasaIndicativa();
		String tasaUnica   = solicitud.getCs_tasa_unica()=="S"?"&Uacute;nica":"Varias";
		String icMoneda    = "";

		if(solicitud.getNombre_moneda().equals("MONEDA NACIONAL")){
			icMoneda = "Pesos";
		} else{
			icMoneda = "D&oacute;lares";
		}
		
		acreditado += solicitud.getCg_razon_social_if();
		if(!(solicitud.getCg_razon_social_if()).equals("") && !(solicitud.getCg_acreditado()).equals("")){	acreditado += "/ "; }
		acreditado += solicitud.getCg_acreditado();

		HashMap datos = new HashMap();
		datos.put("fn_monto",             "$"+Comunes.formatoDecimal(solicitud.getFn_monto(),2) + " " + icMoneda);
		datos.put("nombre_ejecutivo",     solicitud.getNombre_ejecutivo()                                       );
		datos.put("cg_programa",          solicitud.getCg_programa()                                            );
		datos.put("ic_tipo_tasa",         solicitud.getTipo_tasa()                                              );
		datos.put("in_num_disp",          solicitud.getIn_num_disp()                                            );
		datos.put("tasa_unica",           tasaUnica                                                             );
		datos.put("ic_esquema_recup",     solicitud.getIc_esquema_recup()                                       );
		datos.put("esquema_recup",        solicitud.getEsquema_recup()                                          );
		datos.put("cg_acreditado",        acreditado                                                            );
		datos.put("ig_plazo",             solicitud.getIg_plazo() + " d&iacute;as"                              );
		datos.put("periodo_tasa_interes", solicitud.periodoTasa(solicitud.getCg_ut_ppi())                       );
		datos.put("periodo_tasa_capital", solicitud.periodoTasa(solicitud.getCg_ut_ppc())                       );
		datos.put("tre",                  solicitud.formatoDecimal(solicitud.getTRE()) + " %"                   );
		datos.put("tre_con_acarreo",      solicitud.formatoDecimal(solicitud.getTREConAcarreo()) + " %"         );
		datos.put("impuesto_usd",         solicitud.formatoDecimal(solicitud.getImpuestoUSD()) + " %"           );
		datos.put("costo_all_in",         solicitud.formatoDecimal(solicitud.getCostoAllIn()) + " %"            );
		datos.put("factores_riesgo_usd",  solicitud.formatoDecimal(solicitud.getFactoresRiesgoUSD()) + " %"     );
		datos.put("nombre_moneda",        solicitud.getNombre_moneda()                                          );
		datos.put("ppv",                  ""+solicitud.getPPV()                                                 );
		datos.put("ppv_360",              Comunes.formatoDecimal((solicitud.getPPV()/360),2,false)              );
		datos.put("duracion",             ""+solicitud.getDuracion()                                            );
		datos.put("duracion_360",         Comunes.formatoDecimal((solicitud.getDuracion()/360),2,false)         );
		datos.put("tasa_credito",         tasaCredito  + " %"                                                   );
		datos.put("cg_pfpc",              pfpc                                                                  );
		datos.put("cg_pufpc",             pufpc                                                                 );
		if(solicitud.getNumero_solicitud().equals("")){
			datos.put("numero_solicitud",  "CotizadorCreditos"                                                   );
		} else{
			datos.put("numero_solicitud",  solicitud.getNumero_solicitud()                                       );
		}
		resultado.put("registro", datos);

		/***** Se arma el Grid Detalle Disposiciones *****/
		for(int i = 0; i < fn_monto_disp.length; i++){
			detalleDisp    = new JSONArray();
			detalleDisp.add(""  + (i+1));                                      //NUM_DISPOSICION
			detalleDisp.add("$" + Comunes.formatoDecimal(fn_monto_disp[i],2)); //MONTO
			detalleDisp.add(""  + df_disposicion[i]);                          //FECHA_DISPOSICION
			detalleDisp.add(""  + df_vencimiento[i]);                          //FECHA_VENCIMIENTO
			regDetalleDisp.add(detalleDisp);
		}

		resultado.put("totalDetalleDisp",     ""+regDetalleDisp.size() );
		resultado.put("registrosDetalleDisp", regDetalleDisp.toString());

		/***** Se arma el grid Plan de Pagos Capital *****//*
                Solicitud consultaSol = new Solicitud();
                consultaSol  = solCotEsp.consultaSolicitud(ic_solic_esp);
                StringBuffer pagosSB= new StringBuffer();
                if((consultaSol.getPagos()).size() > 0){
                    HashMap mapaPlanPagos = new HashMap();
                    ArrayList alFilas = consultaSol.getPagos();
                    for(int col = 0; col < alFilas.size(); col++){
                            mapaPlanPagos = new HashMap();
                            ArrayList alColumnas = (ArrayList)alFilas.get(col);
                            pagosSB.append((String)alColumnas.get(0)+","+(String)alColumnas.get(1)+","+alColumnas.get(2).toString());
                            if(alFilas.size()!=(col+1)){
                                pagosSB.append(",");
                            }
                    }
                }
                pagos=pagosSB.toString();

                System.out.println(pagos);*/
		if(pagos != null && !pagos.trim().equals("")){
			//System.out.println("<"+pagos+">");//DEBUG
			String[] alPagos = pagos.split(",");
			int i = 0;
			if(alPagos.length > 0){
				while( i < alPagos.length){
					planPagosCap   = new JSONArray();
					planPagosCap.add("" + alPagos[i++]);
					planPagosCap.add("" + alPagos[i++]);
					planPagosCap.add("$" + Comunes.formatoDecimal(alPagos[i++],2,true));
					planPagosCap.add("");
					regPlanPagos.add(planPagosCap);
				}
			}
		}

		resultado.put("totalPlanPagos",     ""+regPlanPagos.size()  );
		resultado.put("registrosPlanPagos", regPlanPagos );

	}
	if(accion.equals("GUARDAR")){
		resultado.put("accion",  "IMPRIMIR");
	} else{
		resultado.put("accion",  "GUARDAR");
	}
	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();

} else if(informacion.equals("Impirmir_Archivo")){

	try{

		success = true;

		HashMap datosArchivo    = new HashMap();
		HashMap mapaDetalleDisp = new HashMap();
		HashMap mapaPlanPagos   = new HashMap();
		List listaDetalleDisp   = new ArrayList();
		List listaPlanPagos     = new ArrayList();

		datosArchivo.put("cg_acreditado",        cg_acreditado       );// Intermediario / Acreditado
		datosArchivo.put("nombre_ejecutivo",     nombre_ejecutivo    );// Nombre del Ejecutivo
		datosArchivo.put("cg_programa",          cg_programa         );// Nombre del programa o acreditado Final
		datosArchivo.put("nombre_moneda",        nombre_moneda       );// Moneda
		datosArchivo.put("fn_monto",             fn_monto            );// Monto total requerido
		datosArchivo.put("ic_tipo_tasa",         ic_tipo_tasa        );// Tipo de tasa requerida
		datosArchivo.put("in_num_disp",          in_num_disp         );// N&uacute;mero de Disposiciones
		datosArchivo.put("tasa_unica",           tasa_unica          );// Disposiciones m&uacute;ltiples. Tasa de Inter&eacute;s
		datosArchivo.put("ig_plazo",             ig_plazo            );// Plazo de la operaci&oacute;n
		datosArchivo.put("esquema_recup",        esquema_recup       );// Esquema de recuperaci&oacute;n del capital
		datosArchivo.put("ic_esquema_recup",     ic_esquema_recup    );// (OCULTO)
		datosArchivo.put("periodo_tasa_interes", periodo_tasa_interes);// Periodicidad del pago de Inter&eacute;s
		datosArchivo.put("periodo_tasa_capital", periodo_tasa_capital);// Periodicidad de Capital
		datosArchivo.put("pfpc",                 pfpc                );// Primer fecha de pago de Capital
		datosArchivo.put("pufpc",                pufpc               );// Pen&uacute;ltima fecha de pago de Capital
		datosArchivo.put("ppv",                  ppv                 );// Plazo promedio de vida
		datosArchivo.put("ppv_360",              ppv_360             );// Plazo promedio de vida
		datosArchivo.put("duracion",             duracion            );// Duraci&oacute;n
		datosArchivo.put("duracion_360",         duracion_360        );// Duraci&oacute;n
		datosArchivo.put("tre",                  tre                 );// Costo de Fondeo sin Acarreo
		datosArchivo.put("tre_con_acarreo",      tre_con_acarreo     );// Costo de Fondeo con Acarreo
		datosArchivo.put("impuesto_usd",         impuesto_usd        );// Impuesto
		datosArchivo.put("costo_all_in",         costo_all_in        );// Costo All-in
		datosArchivo.put("factores_riesgo_usd",  factores_riesgo_usd );// Factores Totales
		datosArchivo.put("tasa_credito",         tasa_credito        );// TASA DE CREDITO
		datosArchivo.put("numero_solicitud",     numero_solicitud    );// Nombre del archivo

		/***** Se arma la Tabla Detalle Disposiciones *****/
		String datosJson = datos_gridDD;
		JSONArray arrayRegNuevo = JSONArray.fromObject(datosJson);
		if(arrayRegNuevo.size() > 0){
			for(int i=0; i<arrayRegNuevo.size(); i++){
				mapaDetalleDisp = new HashMap();
				JSONObject auxiliar = arrayRegNuevo.getJSONObject(i);
				mapaDetalleDisp.put("NUM_DISPOSICION",   auxiliar.getString("NUM_DISPOSICION")  );
				mapaDetalleDisp.put("MONTO",             auxiliar.getString("MONTO")            );
				mapaDetalleDisp.put("FECHA_DISPOSICION", auxiliar.getString("FECHA_DISPOSICION"));
				mapaDetalleDisp.put("FECHA_VENCIMIENTO", auxiliar.getString("FECHA_VENCIMIENTO"));
				listaDetalleDisp.add(mapaDetalleDisp);
			}
		}

		/***** Se arma la Tabla Plan de Pagos Capital *****/
		datosJson = datos_gridPP;
		arrayRegNuevo = JSONArray.fromObject(datosJson);
		if(arrayRegNuevo.size() > 0){
			for(int i=0; i<arrayRegNuevo.size(); i++){
				mapaPlanPagos = new HashMap();
				JSONObject auxiliar = arrayRegNuevo.getJSONObject(i);
				mapaPlanPagos.put("NUM_PAGO",    auxiliar.getString("NUM_PAGO")   );
				mapaPlanPagos.put("FECHA_PAGO",  auxiliar.getString("FECHA_PAGO") );
				mapaPlanPagos.put("MONTO_PAGAR", auxiliar.getString("MONTO_PAGAR"));
				mapaPlanPagos.put("SIN_NOMBRE",  auxiliar.getString("SIN_NOMBRE") );
				listaPlanPagos.add(mapaPlanPagos);
			}
		}

		ConsCotizacionIfArchivos paginador = new ConsCotizacionIfArchivos();
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		paginador.setGeneraReporteFinal("S");
		paginador.setDatosConsulta(datosArchivo);
		paginador.setRegDetalleDisp(listaDetalleDisp);
		paginador.setRegPlanPagos(listaPlanPagos);

		try{
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			success = true;
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			success = false;
			throw new AppException("Error al generar el archivo PDF. ", e);
		}

	} catch(Exception e){
		success = false;
		mensaje = "Ocurrió un error al obtener los datos. " + e;
		log.warn("Ocurrió un error al obtener los datos. " + e);
	}

	resultado.put("mensaje", mensaje             );
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>