<%@ page contentType="application/json;charset=UTF-8" 
	import=" java.util.*,
				java.text.*,
				com.netro.exception.*,
				com.netro.procesos.*, 
				netropology.utilerias.*, 
				com.netro.cotizador.*,
				com.netro.model.catalogos.*,   
				net.sf.json.JSONArray,  
				net.sf.json.JSONObject" 
				errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="../22secsession_extjs.jspf"%>
<% 


String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String infoRegresar = "",consulta ="", mensaje="";
String idCurva = (request.getParameter("idCurva")!=null)?request.getParameter("idCurva"):"";
String nomArchivo = (request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";
String moneda = (request.getParameter("claveMoneda") !=null)?request.getParameter("claveMoneda"):"";
String tipo = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
String calculo = (request.getParameter("calculo")!=null)?request.getParameter("calculo"):"";
String base = (request.getParameter("base")!=null)?request.getParameter("base"):"360";
String default_c = (request.getParameter("default_c")!=null)?request.getParameter("default_c"):"0";
String monedaMod = (request.getParameter("moneda") !=null)?request.getParameter("moneda"):"";
String def_SWAP		= (request.getParameter("def_SWAP")==null)?"0":request.getParameter("def_SWAP");
String hidCsFondeo 	= (request.getParameter("hidCsFondeo")==null)?"":request.getParameter("hidCsFondeo");
String hidCveMoneda = (request.getParameter("hidCveMoneda")==null)?"":request.getParameter("hidCveMoneda");
String auxGuarda = (request.getParameter("modificar")==null)?"":request.getParameter("modificar");
boolean valida 		= false;

MantenimientoCot cotBean = ServiceLocator.getInstance().lookup("MantenimientoCotEJB", MantenimientoCot.class);

Vector vecFilas 	= null;
Vector vecColumnas 	= null;
HashMap datos = new HashMap();
JSONObject jsonObj= new JSONObject();
JSONArray registros = new JSONArray();

if (informacion.equals("catMoneda")) {
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();
}else if (informacion.equals("consultaInicial")) {
	String tipoMoneda 		= (request.getParameter("tipoMoneda")==null)?"":request.getParameter("tipoMoneda");
	vecFilas = cotBean.consultaCatalogoCurvas(idCurva);
	for(int i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector)vecFilas.get(i);
		String id_curva			= (String)vecColumnas.get(0);
		String nom_curva		= (String)vecColumnas.get(1);
		String tipo_curva 		= (String)vecColumnas.get(2);
		String calc_curva 		= (String)vecColumnas.get(3);
		String base_curva 		= (String)vecColumnas.get(4);
		String def_curva 		= (String)vecColumnas.get(5);//6
		String mon_curva 		= (String)vecColumnas.get(6);//7
		String SWAP_curva 		= (String)vecColumnas.get(8);
		String ic_mon_curva 	= (String)vecColumnas.get(7);
		String fondeo_curva		= (String)vecColumnas.get(9);
		String deposito_curva	= (String)vecColumnas.get(10);
		String cg_curva_if		= (String)vecColumnas.get(11);
		if(ic_mon_curva.equals("1")&&tipoMoneda.equals("1")){
			datos = new HashMap();
			datos.put("IDCURVA", id_curva);
			datos.put("NOMBRE", nom_curva);
			datos.put("TIPO", tipo_curva);
			datos.put("MODOCALCULO", calc_curva);
			datos.put("BASE", base_curva);   
			datos.put("CURVADEFAULT", def_curva);
			datos.put("MONEDA", mon_curva);
			datos.put("IC_MONEDA", ic_mon_curva);
			datos.put("CS_SWAP", SWAP_curva);
			datos.put("CS_FONDEO", fondeo_curva);
			datos.put("CS_DEPOSITO", deposito_curva);
			datos.put("CG_CURVA_IF", cg_curva_if);
			registros.add(datos);
		}else if(ic_mon_curva.equals("54")&&tipoMoneda.equals("54")){
			datos = new HashMap();
			datos.put("IDCURVA", id_curva);
			datos.put("NOMBRE", nom_curva);
			datos.put("TIPO", tipo_curva);
			datos.put("MODOCALCULO", calc_curva);
			datos.put("BASE", base_curva);   
			datos.put("CURVADEFAULT", def_curva);
			datos.put("MONEDA", mon_curva);
			datos.put("IC_MONEDA", ic_mon_curva);
			datos.put("CS_SWAP", SWAP_curva);
			datos.put("CS_FONDEO", fondeo_curva);
			datos.put("CS_DEPOSITO", deposito_curva);
			datos.put("CG_CURVA_IF", cg_curva_if);
			registros.add(datos);
		}
	}
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();
}else if (informacion.equals("eliminar")) {
	mensaje = cotBean.eliminaCatalogoCurvas(idCurva);
	if(mensaje.equals(""))
		mensaje = "La información se eliminó con éxito";
	infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + mensaje + "\"}";

}else if (informacion.equals("Agregar")) {
	tipo=(!tipo.equals(""))?tipo:(monedaMod.equals("1"))?"MUESTRA":"DIARIA";
	calculo=(!calculo.equals(""))?calculo:(monedaMod.equals("1"))?"ALAMBRADA":"INTERPOLACION";
   valida = cotBean.validaCurva(nomArchivo);
	if(valida){
		mensaje = "El nombre de archivo capturado ya existe. Favor de verificarlo.";
	}else {
		valida = cotBean.insertaCatalogoCurvas(idCurva,nomArchivo,tipo,calculo,base,default_c,moneda,def_SWAP);
		if(valida==false){
			mensaje = "Error al agregar los datos";
		}else{
			mensaje = "Su información se guardó con éxito";
		}
	}
	infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + mensaje + "\"}";
}else if (informacion.equals("Modificar")) {
	tipo=(!tipo.equals(""))?tipo:(monedaMod.equals("1"))?"MUESTRA":"DIARIA";
	calculo=(!calculo.equals(""))?calculo:(monedaMod.equals("1"))?"ALAMBRADA":"INTERPOLACION";
	valida = cotBean.insertaCatalogoCurvas(idCurva,nomArchivo,tipo,calculo,base,default_c,moneda,def_SWAP);
	if(valida==false){
			mensaje = "Error al guardar los datos";
		}else{
			mensaje = "Los cambios fueron guardados con éxito";
		}
	infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + mensaje + "\"}";
}else if (informacion.equals("validaFondeo")) {
	valida = cotBean.actualizaFondeo(idCurva,hidCsFondeo);
	if(valida == false){
		mensaje = "Error en la actualización de datos";
	}else{
		mensaje = "La actualización se realizó con éxito";
	}
	infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + mensaje + "\"}";
}else if (informacion.equals("validaDefault")) {
	valida = cotBean.actualizaCurvaD(idCurva,hidCveMoneda);
	if(valida == false){
		mensaje = "Error en la actualización de datos";
	}else{
		mensaje = "La actualización se realizó con éxito";
	}
	infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + mensaje + "\"}";
}else if (informacion.equals("validaDepositos")) {
	valida = cotBean.actualizaDepositos(idCurva,hidCveMoneda);
	if(valida == false){
		mensaje = "Error en la actualización de datos";
	}else{
		mensaje = "La actualización se realizó con éxito";
	}
	infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + mensaje + "\"}";
}else if (informacion.equals("validaSWAP")) {
	valida = cotBean.actualizaSWAP(idCurva);
	if(valida == false){
		mensaje = "Error en la actualización de datos";
	}else{
		mensaje = "La actualización se realizó con éxito";
	}
	infoRegresar = "{\"success\":true,\"MENSAJE\":\"" + mensaje + "\"}";
}

%>
<%=infoRegresar%>