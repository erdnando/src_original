Ext.onReady(function(){

	/***** Variables globales *****/
	var fm = Ext.form;
	var jsonDataEncode;
	var opcionPantalla = Ext.getDom('opcionPantalla').value; // Determina si es una cotizaci�n nueva (AGREGAR) o se edita una existente (EDITAR).
	var icSolicEsp     = Ext.getDom('ic_solic_esp').value;

	/***** Genera el archivo PDF *****/
	function procesoImprimir(){

		Ext.getCmp('btnImprimir').setIconClass('loading-indicator');

		// Primero obtengo los datos del grid
		var datarDD = new Array();
		var datarPP = new Array();
		var records = '';
		var jsonDataEncodeDD = '';
		var jsonDataEncodePP = '';
		// Grid Detalle Disposiciones
		records = storeDetalleDisposiciones.getRange();
		for (var i = 0; i < records.length; i++){
			datarDD.push(records[i].data);
		}
		jsonDataEncodeDD = Ext.util.JSON.encode(datarDD);
		records = '';
		// Grid Plan de Pagos Capital
		records = storePlanPagosCap.getRange();
		for (var i = 0; i < records.length; i++){
			datarPP.push(records[i].data);
		}
		jsonDataEncodePP = Ext.util.JSON.encode(datarPP);

		Ext.Ajax.request({
			url: '22forma07Bext.data.jsp',
			params: Ext.apply({
				informacion:          'Impirmir_Archivo',
				cg_acreditado:        Ext.getCmp('cg_acreditado_id').getValue(),        // Intermediario / Acreditado
				nombre_ejecutivo:     Ext.getCmp('nombre_ejecutivo_id').getValue(),     // Nombre del Ejecutivo
				cg_programa:          Ext.getCmp('cg_programa_id').getValue(),          // Nombre del programa o acreditado Final
				nombre_moneda:        Ext.getCmp('nombre_moneda_id').getValue(),        // Moneda
				fn_monto:             Ext.getCmp('fn_monto_id').getValue(),             // Monto total requerido
				ic_tipo_tasa:         Ext.getCmp('ic_tipo_tasa_id').getValue(),         // Tipo de tasa requerida
				in_num_disp:          Ext.getCmp('in_num_disp_id').getValue(),          // N&uacute;mero de Disposiciones
				tasa_unica:           Ext.getCmp('tasa_unica_id').getValue(),           // Disposiciones m&uacute;ltiples. Tasa de Inter&eacute;s
				ig_plazo:             Ext.getCmp('ig_plazo_id').getValue(),             // Plazo de la operaci&oacute;n
				esquema_recup:        Ext.getCmp('esquema_recup_id').getValue(),        // Esquema de recuperaci&oacute;n del capital
				ic_esquema_recup:     Ext.getCmp('ic_esquema_recup_id').getValue(),     // (OCULTO)
				periodo_tasa_interes: Ext.getCmp('periodo_tasa_interes_id').getValue(), // Periodicidad del pago de Inter&eacute;s
				periodo_tasa_capital: Ext.getCmp('periodo_tasa_capital_id').getValue(), // Periodicidad de Capital
				pfpc:                 Ext.getCmp('cg_pfpc_id').getValue(),              // Primer fecha de pago de Capital
				pufpc:                Ext.getCmp('cg_pufpc_id').getValue(),             // Pen&uacute;ltima fecha de pago de Capital
				ppv:                  Ext.getCmp('ppv_id').getValue(),                  // Plazo promedio de vida
				ppv_360:              Ext.getCmp('ppv_360_id').getValue(),              // Plazo promedio de vida
				duracion:             Ext.getCmp('duracion_id').getValue(),             // Duraci&oacute;n
				duracion_360:         Ext.getCmp('duracion_360_id').getValue(),         // Duraci&oacute;n
				tre:                  Ext.getCmp('tre_id').getValue(),                  // Costo de Fondeo sin Acarreo
				tre_con_acarreo:      Ext.getCmp('tre_con_acarreo_id').getValue(),      // Costo de Fondeo con Acarreo
				impuesto_usd:         Ext.getCmp('impuesto_usd_id').getValue(),         // Impuesto
				costo_all_in:         Ext.getCmp('costo_all_in_id').getValue(),         // Costo All-in
				factores_riesgo_usd:  Ext.getCmp('factores_riesgo_usd_id').getValue(),  // Factores Totales
				tasa_credito:         Ext.getCmp('tasa_credito_id').getValue(),         // TASA DE CREDITO
				numero_solicitud:     Ext.getCmp('numero_solicitud_id').getValue(),
				datos_gridDD:         jsonDataEncodeDD,
				datos_gridPP:         jsonDataEncodePP
			}),
			callback: descargaArchivo
		});
	}

	/***** Regresa a la pantalla inicial *****/
	function procesoSalir(){
	
		if(Ext.getCmp('btnImprimir').isVisible()){
			window.location.href='22forma07ext.jsp';
		} else{
			window.location.href='22forma07Aext.jsp'+
			'?opcionPantalla=RECARGAR'                                               +
			'&nombre_ejecutivo='   + (Ext.getDom('nombre_ejecutivo').value).trim()   +
			'&cg_mail='            + (Ext.getDom('cg_mail').value).trim()            +
			'&cg_telefono='        + (Ext.getDom('cg_telefono').value).trim()        +
			'&cg_fax='             + (Ext.getDom('cg_fax').value).trim()             +
			'&ejecutivo_nafin='    + (Ext.getDom('ejecutivo_nafin').value).trim()    +
			'&ig_tipo_piso='       + (Ext.getDom('ig_tipo_piso').value).trim()       +
			'&cg_razon_social_if=' + (Ext.getDom('cg_razon_social_if').value).trim() +
			'&ig_riesgo_if='       + (Ext.getDom('ig_riesgo_if').value).trim()       +
			'&cg_acreditado='      + (Ext.getDom('cg_acreditado').value).trim()      +
			'&ig_valor_riesgo_ac=' + (Ext.getDom('ig_valor_riesgo_ac').value).trim() +
			'&df_solicitud='       + (Ext.getDom('df_solicitud').value).trim()       +
			'&df_cotizacion='      + (Ext.getDom('df_cotizacion').value).trim()      +
			'&cg_programa='        + (Ext.getDom('cg_programa').value).trim()        +
			'&ic_moneda='          + (Ext.getDom('ic_moneda').value).trim()          +
			'&fn_monto='           + (Ext.getDom('fn_monto').value).trim()           +
			'&ic_tipo_tasa='       + (Ext.getDom('ic_tipo_tasa').value).trim()       +
			'&in_num_disp='        + (Ext.getDom('in_num_disp').value).trim()        +
			'&ig_plazo_conv='      + (Ext.getDom('ig_plazo_conv').value).trim()      +
			'&cg_ut_plazo='        + (Ext.getDom('cg_ut_plazo').value).trim()        +
			'&ig_plazo='           + (Ext.getDom('ig_plazo').value).trim()           +
			'&ic_esquema_recup='   + (Ext.getDom('ic_esquema_recup').value).trim()   +
			'&cg_ut_ppi='          + (Ext.getDom('cg_ut_ppi').value).trim()          +
			'&ig_ppi='             + (Ext.getDom('ig_ppi').value).trim()             +
			'&cg_ut_ppc='          + (Ext.getDom('cg_ut_ppc').value).trim()          +
			'&ig_ppc='             + (Ext.getDom('ig_ppc').value).trim()             +
			'&pfpc='               + (Ext.getDom('pfpc').value).trim()               +
			'&pufpc='              + (Ext.getDom('pufpc').value).trim()              +
			'&idcurva='            + (Ext.getDom('idcurva').value).trim()            +
			'&id_interpolacion='   + (Ext.getDom('id_interpolacion').value).trim()   +
			'&metodoCalculo='      + (Ext.getDom('metodoCalculo').value).trim()      +
			'&ic_solic_esp='       + (Ext.getDom('ic_solic_esp').value).trim()       +
			'&str_tipo_usuario='   + (Ext.getDom('str_tipo_usuario').value).trim()   +
			'&ic_ejecutivo='       + (Ext.getDom('ic_ejecutivo').value).trim()       +
			'&ig_plazo_max_oper='  + (Ext.getDom('ig_plazo_max_oper').value).trim()  +
			'&ig_num_max_disp='    + (Ext.getDom('ig_num_max_disp').value).trim()    +
			'&ig_plazo_conv_ant='  + (Ext.getDom('ig_plazo_conv_ant').value).trim()  +
			'&num_disp_capt='      + (Ext.getDom('num_disp_capt').value).trim()      +
			'&fn_monto_grid='      + (Ext.getDom('fn_monto_grid').value).trim()      +
			'&disposicion_grid='   + (Ext.getDom('disposicion_grid').value).trim()   +
			'&pagos='              + (Ext.getDom('pagos').value).trim()              +
			'&vencimiento_grid='   + (Ext.getDom('vencimiento_grid').value).trim();
		}
	}

	/***** Inicializa la pantalla *****/
	function procesoInicializar(accion){

		formaPrincipal.el.mask('Cargando datos...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '22forma07Bext.data.jsp',
			params: Ext.apply({
				informacion:        'Inicializacion',
				accion:             accion,
				nombre_ejecutivo:   (Ext.getDom('nombre_ejecutivo').value).trim(),
				cg_mail:            (Ext.getDom('cg_mail').value).trim(),
				cg_telefono:        (Ext.getDom('cg_telefono').value).trim(),
				cg_fax:             (Ext.getDom('cg_fax').value).trim(),
				ejecutivo_nafin:    (Ext.getDom('ejecutivo_nafin').value).trim(),
				ig_tipo_piso:       (Ext.getDom('ig_tipo_piso').value).trim(),
				cg_razon_social_if: (Ext.getDom('cg_razon_social_if').value).trim(),
				ig_riesgo_if:       (Ext.getDom('ig_riesgo_if').value).trim(),
				cg_acreditado:      (Ext.getDom('cg_acreditado').value).trim(),
				ig_valor_riesgo_ac: (Ext.getDom('ig_valor_riesgo_ac').value).trim(),
				df_solicitud:       (Ext.getDom('df_solicitud').value).trim(),
				df_cotizacion:      (Ext.getDom('df_cotizacion').value).trim(),
				cg_programa:        (Ext.getDom('cg_programa').value).trim(),
				ic_moneda:          (Ext.getDom('ic_moneda').value).trim(),
				fn_monto:           (Ext.getDom('fn_monto').value).trim(),
				ic_tipo_tasa:       (Ext.getDom('ic_tipo_tasa').value).trim(),
				in_num_disp:        (Ext.getDom('in_num_disp').value).trim(),
				ig_plazo_conv:      (Ext.getDom('ig_plazo_conv').value).trim(),
				cg_ut_plazo:        (Ext.getDom('cg_ut_plazo').value).trim(),
				ig_plazo:           (Ext.getDom('ig_plazo').value).trim(),
				ic_esquema_recup:   (Ext.getDom('ic_esquema_recup').value).trim(),
				cg_ut_ppi:          (Ext.getDom('cg_ut_ppi').value).trim(),
				ig_ppi:             (Ext.getDom('ig_ppi').value).trim(),
				cg_ut_ppc:          (Ext.getDom('cg_ut_ppc').value).trim(),
				ig_ppc:             (Ext.getDom('ig_ppc').value).trim(),
				pfpc:               (Ext.getDom('pfpc').value).trim(),
				pufpc:              (Ext.getDom('pufpc').value).trim(),
				idcurva:            (Ext.getDom('idcurva').value).trim(),
				id_interpolacion:   (Ext.getDom('id_interpolacion').value).trim(),
				metodoCalculo:      (Ext.getDom('metodoCalculo').value).trim(),
				ic_solic_esp:       (Ext.getDom('ic_solic_esp').value).trim(),
				str_tipo_usuario:   (Ext.getDom('str_tipo_usuario').value).trim(),
				ic_ejecutivo:       (Ext.getDom('ic_ejecutivo').value).trim(),
				ig_plazo_max_oper:  (Ext.getDom('ig_plazo_max_oper').value).trim(),
				ig_num_max_disp:    (Ext.getDom('ig_num_max_disp').value).trim(),
				ig_plazo_conv_ant:  (Ext.getDom('ig_plazo_conv_ant').value).trim(),
				num_disp_capt:      (Ext.getDom('num_disp_capt').value).trim(),
				fn_monto_grid:      (Ext.getDom('fn_monto_grid').value).trim(),
				disposicion_grid:   (Ext.getDom('disposicion_grid').value).trim(),
				vencimiento_grid:   (Ext.getDom('vencimiento_grid').value).trim(),
				ic_proc_pago:       (Ext.getDom('ic_proc_pago').value).trim(),
				pagos:              (Ext.getDom('pagos').value).trim()
			}),
			callback: procesarDatosCotizacion
		});

	}

	/***** Se cargan los datos en el form, se muestran y ocultan los campos correspondientes *****/
	var procesarDatosCotizacion = function(opts, success, response){

		var fp = Ext.getCmp('formaPrincipal');

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){

			/***** Asigno los datos al form *****/
			Ext.getCmp('formaPrincipal').show();
			valorFormulario = Ext.util.JSON.decode(response.responseText).registro;
			fp.getForm().setValues(valorFormulario);

			if(Ext.getCmp('ic_esquema_recup_id').getValue()){
			
			}

			/***** Asigno los datos a los grids *****/
			var datosCot = Ext.util.JSON.decode(response.responseText);
			if(datosCot.registrosDetalleDisp != ''){
				storeDetalleDisposiciones.loadData(datosCot.registrosDetalleDisp);
				Ext.getCmp('titulos_2').show();
				Ext.getCmp('gridDetalleDisposiciones').show();
			} else{
				Ext.getCmp('titulos_2').hide();
			}
			if(datosCot.registrosPlanPagos != ''){
				storePlanPagosCap.loadData(datosCot.registrosPlanPagos);
				Ext.getCmp('titulos_3').show();
				Ext.getCmp('gridPlanPagosCap').show();
			} else{
				Ext.getCmp('titulos_3').hide();
			}

			/***** Se ocultan o muestran los campos correspondientes *****/
			
			if(Ext.util.JSON.decode(response.responseText).accion == 'IMPRIMIR'){
				Ext.getCmp('titulos_G').show();
				Ext.getCmp('btnGuardar').hide();
				Ext.getCmp('btnImprimir').show();
			} else{
				Ext.getCmp('titulos_G').hide();
				Ext.getCmp('btnGuardar').show();
				Ext.getCmp('btnImprimir').hide();
			}
			if(Ext.getCmp('cg_programa_id').getValue() != ''){
				Ext.getCmp('pnl_cg_programa').show();
			} else{
				Ext.getCmp('pnl_cg_programa').hide();
			}
			if(parseInt(Ext.getCmp('in_num_disp_id').getValue()) > 1){
				Ext.getCmp('pnl_tasa_unica').show();
			} else{
				Ext.getCmp('pnl_tasa_unica').hide();
			}
			var esquemaRec = parseInt(Ext.getCmp('ic_esquema_recup_id').getValue()); // Valores que puede tomar ic_esquema_recup :1=Cupon cero,2=Tradicional,3=Plan de pagos,4=Rentas
			if(esquemaRec != 1){
				if(esquemaRec == 2 || esquemaRec == 3){
					Ext.getCmp('pnl_periodo_tasa_interes').show();
				} else{
					Ext.getCmp('pnl_periodo_tasa_interes').hide();
				}
				if(esquemaRec != 3){
					if(esquemaRec == 2 || esquemaRec == 4){
						Ext.getCmp('pnl_periodo_tasa_capital').show();
					} else{
						Ext.getCmp('pnl_periodo_tasa_capital').hide();
					}
					if(esquemaRec == 2 && Ext.getCmp('cg_pfpc_id').getValue() != ''){
						Ext.getCmp('pnl_cg_pfpc').show();
						Ext.getCmp('pnl_cg_pufpc').show();
					} else{
						Ext.getCmp('pnl_cg_pfpc').hide();
						Ext.getCmp('pnl_cg_pufpc').hide();
					}
				}
			}

		} else{
			Ext.getCmp('formaPrincipal').hide();
			Ext.MessageBox.alert('Error...', 
			'Ocurri� un error al obtener los datos. <br>' + Ext.util.JSON.decode(response.responseText).mensaje,
			procesoSalir);
			
		}
		fp.el.unmask();
	}

	/***** Descargar archivos PDF *****/
	function descargaArchivo(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			formaPrincipal.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			formaPrincipal.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnImprimir').setIconClass('icoPdf');
	}

	/***** Se crea el store del Grid Detalle Disposiciones *****/
	var storeDetalleDisposiciones = new Ext.data.ArrayStore({
		fields: [
			{ name: 'NUM_DISPOSICION',   mapping: 0},
			{ name: 'MONTO',             mapping: 1},
			{ name: 'FECHA_DISPOSICION', mapping: 2},
			{ name: 'FECHA_VENCIMIENTO', mapping: 3}
		],
		autoLoad: false
	});

	/***** Se crea el store del Grid Plan de Pagos Capital *****/
	var storePlanPagosCap = new Ext.data.ArrayStore({
		fields: [
			{ name: 'NUM_PAGO',    mapping: 0},
			{ name: 'FECHA_PAGO',  mapping: 1},
			{ name: 'MONTO_PAGAR', mapping: 2},
			{ name: 'SIN_NOMBRE',  mapping: 3}
		],
		autoLoad: false
	});

	/***** Se crea el grid Detalle Disposiciones *****/
	var gridDetalleDisposiciones = new Ext.grid.EditorGridPanel({
		width:        600,
		store:        storeDetalleDisposiciones,
		id:           'gridDetalleDisposiciones',
		margins:      '20 0 0 0',
		style:        'margin:0 auto;',
		align:        'center',
		autoHeight:   true,
		hidden:       true,
		displayInfo:  true,
		loadMask:     true,
		stripeRows:   true,
		frame:        false,
		clicksToEdit: 1,
		columns: [
		{width: 145, header: 'N�mero de Disposici�n', dataIndex: 'NUM_DISPOSICION',   align: 'center', sortable: false, resizable: false},
		{width: 150, header: 'Monto',                 dataIndex: 'MONTO',             align: 'center', sortable: false, resizable: false},
		{width: 150, header: 'Fecha de Disposici�n',  dataIndex: 'FECHA_DISPOSICION', align: 'center', sortable: false, resizable: false},
		{width: 150, header: 'Fecha de Vencimiento',  dataIndex: 'FECHA_VENCIMIENTO', align: 'center', sortable: false, resizable: false}
		]
	});

	/***** Se crea el grid Plan de Pagos Capital *****/
	var gridPlanPagosCap = new Ext.grid.EditorGridPanel({
		width:        600,
		store:        storePlanPagosCap,
		id:           'gridPlanPagosCap',
		margins:      '20 0 0 0',
		style:        'margin:0 auto;',
		align:        'center',
		autoHeight:   true,
		hidden:       true,
		displayInfo:  true,
		loadMask:     true,
		stripeRows:   true,
		frame:        false,
		clicksToEdit: 1,
		columns: [
		{width: 145, header: 'N�mero de Pago', dataIndex: 'NUM_PAGO',    align: 'center', sortable: false, resizable: false},
		{width: 150, header: 'Fecha de Pago',  dataIndex: 'FECHA_PAGO',  align: 'center', sortable: false, resizable: false},
		{width: 150, header: 'Monto a Pagar',  dataIndex: 'MONTO_PAGAR', align: 'center', sortable: false, resizable: false},
		{width: 150, header: '&nbsp;',         dataIndex: 'SIN_NOMBRE',  align: 'center', sortable: false, resizable: false}
		]
	});

	/***** Se crea el form principal *****/
	var formaPrincipal = new Ext.form.FormPanel({
		width:           600,
		labelWidth:      180,
		id:              'formaPrincipal',
		title:           'Cotizador de Cr�ditos',
		layout:          'form',
		style:           'margin: 0 auto;',
		labelAlign:      'right',
		frame:           false,
		hidden:          true,
		border:          true,
		autoHeight:      true,
		items: [{
			width:        600,
			xtype:        'panel',
			id:           'titulos_G',
			layout:       'table',
			border:       false,
			hidden:       true,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="center"> <br><h3> Su cotizaci�n ha sido guardada </h3><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_intermediario_acreditado',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Intermediario / Acreditado: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'cg_acreditado_id',
				name:      'cg_acreditado',
				value:     '&nbsp;'
			}]
		},{
			width:        600,
			xtype:        'panel',
			id:           'titulos_0',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"> <b> Datos del Ejecutivo Nafin </b><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_ejecutivo_nafin',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 3},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Nombre del Ejecutivo: </b><br> </div>'
			},{
				width:     338,
				xtype:     'displayfield',
				id:        'nombre_ejecutivo_id',
				name:      'nombre_ejecutivo',
				value:     '&nbsp;'
			},{
				width:     10,
				xtype:     'displayfield',
				id:        'numero_solicitud_id',
				name:      'numero_solicitud',
				value:     '&nbsp;',
				hidden:    true
			}]
		},{
			width:        600,
			xtype:        'panel',
			id:           'titulos_1',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"> <b> Caracter&iacute;sticas de la Operaci&oacute;n </b><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_cg_programa',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Nombre del programa o acreditado Final: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'cg_programa_id',
				name:      'cg_programa',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_nombre_moneda',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Moneda: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'nombre_moneda_id',
				name:      'nombre_moneda',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_fn_monto',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Monto total requerido: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'fn_monto_id',
				name:      'fn_monto',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_tipo_tasa',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Tipo de tasa requerida: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'ic_tipo_tasa_id',
				name:      'ic_tipo_tasa',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_num_disp',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>N&uacute;mero de Disposiciones: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'in_num_disp_id',
				name:      'in_num_disp',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_tasa_unica',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Disposiciones m&uacute;ltiples. Tasa de Inter&eacute;s: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'tasa_unica_id',
				name:      'tasa_unica',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_ig_plazo',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Plazo de la operaci&oacute;n: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'ig_plazo_id',
				name:      'ig_plazo',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_esquema_recup',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 3},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Esquema de recuperaci&oacute;n del capital: </b><br> </div>'
			},{
				width:     340,
				xtype:     'displayfield',
				id:        'esquema_recup_id',
				name:      'esquema_recup',
				value:     '&nbsp;'
			},{
				width:     8,
				xtype:     'displayfield',
				id:        'ic_esquema_recup_id',
				name:      'ic_esquema_recup',
				value:     '&nbsp;',
				hidden:    true
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_periodo_tasa_interes',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Periodicidad del pago de Inter&eacute;s: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'periodo_tasa_interes_id',
				name:      'periodo_tasa_interes',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_periodo_tasa_capital',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Periodicidad de Capital: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'periodo_tasa_capital_id',
				name:      'periodo_tasa_capital',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_cg_pfpc',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Primer fecha de pago de Capital: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'cg_pfpc_id',
				name:      'cg_pfpc',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_cg_pufpc',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Pen&uacute;ltima fecha de pago de Capital: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'cg_pufpc_id',
				name:      'cg_pufpc',
				value:     '&nbsp;'
			}]
		},{
			width:        600,
			xtype:        'panel',
			id:           'titulos_2',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"> <b> Detalle de Disposiciones </b><br> </div>'
			}]
		},gridDetalleDisposiciones,{
			width:        600,
			xtype:        'panel',
			id:           'titulos_3',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"> <b> Plan de Pagos Capital </b><br> </div>'
			}]
		}, gridPlanPagosCap,{
			width:        600,
			xtype:        'panel',
			id:           'titulos_4',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"> <b> Datos exclusivos de Tesorer&iacute;a </b><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_datos_tesoreria',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 3},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="center"> &nbsp; <br></div>'
			},{
				width:     174,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="left"><b> D&iacute;as </b><br> </div>'
			},{
				width:     174,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="left"><b> A&ntilde;os </b><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_ppv',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 3},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Plazo promedio de vida: </b><br> </div>'
			},{
				width:     174,
				xtype:     'displayfield',
				id:        'ppv_id',
				name:      'ppv',
				value:     '&nbsp;'
			},{
				width:     174,
				xtype:     'displayfield',
				id:        'ppv_360_id',
				name:      'ppv_360',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_duracion',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 3},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Duraci&oacute;n: </b><br> </div>'
			},{
				width:     174,
				xtype:     'displayfield',
				id:        'duracion_id',
				name:      'duracion',
				value:     '&nbsp;'
			},{
				width:     174,
				xtype:     'displayfield',
				id:        'duracion_360_id',
				name:      'duracion_360',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_blanco',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"> &nbsp; <br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_tre',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Costo de Fondeo sin Acarreo: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'tre_id',
				name:      'tre',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_tre_con_acarreo',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Costo de Fondeo con Acarreo: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'tre_con_acarreo_id',
				name:      'tre_con_acarreo',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_impuesto_usd',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Impuesto: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'impuesto_usd_id',
				name:      'impuesto_usd',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_costo_all_in',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Costo All-in: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'costo_all_in_id',
				name:      'costo_all_in',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_factores_riesgo_usd',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Factores Totales: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'factores_riesgo_usd_id',
				name:      'factores_riesgo_usd',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'pnl_tasa_credito',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     250,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> TASA DE CREDITO: </b><br> </div>'
			},{
				width:     348,
				xtype:     'displayfield',
				id:        'tasa_credito_id',
				name:      'tasa_credito',
				value:     '&nbsp;'
			}]
		}
		],
		buttons: [{
			xtype:        'button',
			text:         'Guardar',
			id:           'btnGuardar',
			iconCls:      'icoGuardar',
			hidden:       true,
			handler:      function(boton, evento){procesoInicializar('GUARDAR');}
		},{
			xtype:        'button',
			text:         'Imprimir',
			id:           'btnImprimir',
			iconCls:      'icoPdf',
			hidden:       true,
			handler:      procesoImprimir
		},{
			xtype:        'button',
			text:         'Regresar',
			id:           'btnRegresar',
			iconCls:      'icoRegresar',
			handler:      procesoSalir
		}]
	});


	/***** Contenedor Principal *****/
	var pnl = new Ext.Container({
		width:   949,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin: 0 auto;',
		items: [
			NE.util.getEspaciador(20),
			formaPrincipal
		]
	});

	/***** Inicializaci�n *****/
	procesoInicializar('INICIO');

});