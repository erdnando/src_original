<%@ page language="java" 
contentType="application/json;charset=UTF-8"
import="java.util.*,
	netropology.utilerias.*,
	com.netro.cotizador.*,
	com.netro.exception.*,
	com.netro.pdf.*,
	org.apache.commons.logging.Log,
	net.sf.json.JSONArray,  
	net.sf.json.JSONObject" 
	errorPage="/00utils/error_extjs.jsp"
%> 

<%@ include file="/appComun.jspf" %>

<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
//Parametros 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 


//Instancia del EJB AutorizaSolCotEJB
AutorizaSolCot solCot = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
ArrayList alConsulta = null;

String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
String estadoSiguiente = null;

HashMap registro = new HashMap();
JSONArray registros = new JSONArray();
	
	int contSolic = 0;
	
	if(informacion.equals("Cotizador.inicializacion")){
		alConsulta = solCot.consultaSolic(iNoUsuario,null,null,"1,4",null,false,"TESOR");
		for(int i=0;i<alConsulta.size();i++){
				AutSolic autSol = (AutSolic)alConsulta.get(i);
				if(!"4".equals(autSol.getIc_estatus())){
					contSolic++;
				}
		}
		if(contSolic > 0){
			estadoSiguiente = "MOSTRAR_VENTANA";//Ventana inicial de información
			jsonObj.put("mensaje","Ha recibido una nueva solicitud, favor de verificarla.");
		}else{
			estadoSiguiente = "MOSTRAR_GRID_CONSULTA";
		}
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("estadoSiguiente",estadoSiguiente);
		infoRegresar = jsonObj.toString();
		
	}else if (	informacion.equals(	"catalogoEstatusAsignar"	)	){
	
		String estatus = request.getParameter("estatus")==null?"":request.getParameter("estatus");
		
		registros = new JSONArray();		
		HashMap info = new HashMap();		
		if(estatus.equals("4"))  {
			info = new HashMap();
			info.put("clave", "");
			info.put("descripcion", "No modificar estatus");
			registros.add(info);
			
			info = new HashMap();
			info.put("clave", "2");
			info.put("descripcion", "Cotizada");	
			registros.add(info);
			
			info = new HashMap();
			info.put("clave", "3");
			info.put("descripcion", "Rechazada");	
			registros.add(info);
	
		}else  {
			info = new HashMap();
			info.put("clave", "");
			info.put("descripcion", "Favor de consultar Solicitud");	
			registros.add(info);
		}
		infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		
	}else if (	informacion.equals(	"Cotizador.Consultar"	)	){
		 
		alConsulta = solCot.consultaSolic(iNoUsuario,null,null,"1,4",null,false,"TESOR");
			for(int i=0;i<alConsulta.size();i++){
				AutSolic autSol = (AutSolic)alConsulta.get(i);

					String fechaSolicitud = autSol.getFecha_solicitud();
					String numSolicitud = autSol.getNum_solicitud();
					String autorizacion = autSol.getSolicitar_aut();
					String intermediarioFinanciero = autSol.getNombre_if();
					String moneda = autSol.getMoneda();
					String montoRequerido = autSol.getFn_monto()+"";
					String plazoOperacion = autSol.getPlazo_oper();
					String estatus = autSol.getIc_estatus();//SI ESTATUS = 4 (No modificar,Cotizada,Rechazada),(Favor de consultar Solicitud)

					String tasaMismoDia = autSol.getIg_tasa_md()+"";
					String tasaSPOT = autSol.getIg_tasa_spot()+"";
					String tasaIndicativa = autSol.getDescTasaIndicativa();
					String icTasa = autSol.getIc_tasa();
					
					String numReferencia = autSol.getIg_num_referencia();
					String causasRechazo = autSol.getCg_causa_rechazo();
					
					String tipoCotizacion = "";//opciones radio buttons (Indicativa, Enfirme)
					String ver = autSol.getCg_observaciones();//Action colum
					String ic_plan_de_pagos = "";
					if("3".equals(autSol.getIc_esquema_recup())){
						ic_plan_de_pagos = autSol.getIc_solic_esp();
					}
					String solicitud = autSol.getIc_solic_esp();

					registro = new HashMap();
					registro.put("FECHA",fechaSolicitud);
					registro.put("NUM_SOLIC",numSolicitud);
					registro.put("REQ_AUT",autorizacion);
					registro.put("IF",intermediarioFinanciero);
					registro.put("MONEDA",moneda);
					registro.put("MONTO_REQUERIDO",montoRequerido);
					registro.put("PLAZO_OPER",plazoOperacion);
					registro.put("ESTATUS",estatus);
					registro.put("IC_TASA",icTasa);
					registro.put("TASA_MD",tasaMismoDia);
					registro.put("TASA_48HRS",tasaSPOT);
					registro.put("TASA_INDICATIVA",tasaIndicativa);
					registro.put("NUM_REFERENCIA",numReferencia);
					registro.put("CUASA_RECHAZO",causasRechazo);
					registro.put("TIPO_COTIZACION","F");//
					registro.put("OBSERVACIONES",ver);
					registro.put("PLAN_PAGO",ic_plan_de_pagos);
					registro.put("CONSULTAR",solicitud);
					registros.add(registro);
					
					
			}	
			jsonObj.put("registros",registros);
		infoRegresar = jsonObj.toString();	
	}else if(	informacion.equals("Cotizador.GenerarPDF")){
		String nombreArchivo = "";
		//HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		String path = strDirectorioTemp;
		
		try{
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			//int cols = titulos.getData().size();
			pdfDoc.setTable((7), 100);
			
			alConsulta = solCot.consultaSolic(iNoUsuario,null,null,"1,4",null,false);
			for(int i = 0;i<alConsulta.size();i++){
				AutSolic autSol = (AutSolic)alConsulta.get(i);
				if(i==0){
					pdfDoc.setCell("Fecha de Solicitud","celda01",ComunesPDF.CENTER,1,1,1);
					pdfDoc.setCell("Número de Solicitud","celda01",ComunesPDF.CENTER,1,1,1);
					pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER,1,1,1);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER,1,1,1);
					pdfDoc.setCell("Monto total requerido","celda01",ComunesPDF.CENTER,1,1,1);
					pdfDoc.setCell("Plazo de la operación","celda01",ComunesPDF.CENTER,1,1,1);
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER,1,1,1);
				}
				pdfDoc.setCell(autSol.getFecha_solicitud(),"formas",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(autSol.getNum_solicitud(),"formas",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(autSol.getNombre_if(),"formas",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(autSol.getMoneda(),"formas",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(autSol.getFn_monto(),2),"formas",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(autSol.getPlazo_oper(),"formas",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(autSol.getEstatus(),"formas",ComunesPDF.CENTER,1,1,1);
			}
			pdfDoc.addTable();
			pdfDoc.endDocument();
			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				
		}catch(Exception e){
		
		}
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("Cotizador.Guardar_Observaciones")){
		String ic_solic_esp = (request.getParameter("ic_solic_esp")!=null)?request.getParameter("ic_solic_esp"):""; 
		String observaciones = (request.getParameter("observaciones")!=null)?request.getParameter("observaciones"):""; 
		
		try{
			solCot.guardaObservaciones(ic_solic_esp,observaciones);
			jsonObj.put("exito", "SI");
			estadoSiguiente="OCULTAR_VENTANA";
		}catch(NafinException ne){
			ne.printStackTrace();
			jsonObj.put("exito", "NO");
			estadoSiguiente="ESPERAR_DESICION";
		}
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("estadoSiguiente",estadoSiguiente);
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("Cotizador.solicitud_detalles")){
			SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
			
			AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
			
			String ic_solicitud	 		= (request.getParameter("ic_solic_esp")!=null)?request.getParameter("ic_solic_esp"):""; 
			String intermediario	  		= (request.getParameter("intermediario")!=null)?request.getParameter("intermediario"):""; 
			String estatus_solicitud	= (request.getParameter("estatus_solicitud")!=null)?request.getParameter("estatus_solicitud"):""; 
			String origen	  				= (request.getParameter("origen")!=null)?request.getParameter("origen"):""; 
			String tec = "";
			String periodo = "";
			try{
				if(!"".equals(ic_solicitud)){
					if("2".equals(estatus_solicitud)&&"IF".equals(strTipoUsuario)) {
						autSol.setEstatusSolic(ic_solicitud, "5", estatus_solicitud);
					}
					Solicitud	sol = solCotEsp.consultaSolicitud(ic_solicitud);
					
					//Datos de la Cotización
					JSONArray 	cifrasDatosCotizacionDataArray 	= new JSONArray();
					JSONArray	reg						= null;
					
					reg = new JSONArray();
					reg.add("Número de Solicitud:");
					reg.add(sol.getNumero_solicitud());
					reg.add("TEXTO");
					cifrasDatosCotizacionDataArray.add(reg);
					
					reg = new JSONArray();
					reg.add("Fecha de Solicitud:");
					reg.add(sol.getDf_solicitud());
					reg.add("FECHA");
					cifrasDatosCotizacionDataArray.add(reg);
						
					reg = new JSONArray();
					reg.add("Hora de Solicitud:");
					reg.add(sol.getHora_solicitud());
					reg.add("FECHA");
					cifrasDatosCotizacionDataArray.add(reg);
			
					
					if( !estatus_solicitud.equals("1") & !estatus_solicitud.equals("3") & !estatus_solicitud.equals("4") ){//1=Solicictada,3=Rechazada,4=En proceso
						String auxFecha = "Cotizaci&oacute;n";
						if("8".equals(estatus_solicitud))
							auxFecha = "Cancelaci&oacute;n";	
							
							reg = new JSONArray();
							reg.add("Fecha de "+auxFecha+":");
							reg.add(sol.getDf_cotizacion());
							reg.add("FECHA");
							cifrasDatosCotizacionDataArray.add(reg);
								
							reg = new JSONArray();
							reg.add("Hora de "+auxFecha+":");
							reg.add(sol.getHora_cotizacion());
							reg.add("FECHA");
							cifrasDatosCotizacionDataArray.add(reg);
					}
					reg = new JSONArray();
					reg.add("Número y Nombre de usuario:");
					reg.add(sol.getIc_usuario()+" - "+sol.getNombre_usuario());
					reg.add("TEXTO");
					cifrasDatosCotizacionDataArray.add(reg);
					
					if(estatus_solicitud.equals("7")||estatus_solicitud.equals("8")||estatus_solicitud.equals("9")){
						reg = new JSONArray();
						reg.add("N&uacute;mero de Recibo:");
						reg.add(sol.getIg_num_referencia());
						reg.add("TEXTO");
						cifrasDatosCotizacionDataArray.add(reg);
						//Aqui va un mensaje
					}
					jsonObj.put("cifrasDatosCotizacionDataArray",cifrasDatosCotizacionDataArray);
					
					///Datos del Ejecutivo Nafin
					JSONArray 	cifrasDatosEjecutivoNafinDataArray 	= new JSONArray();
	
					reg = new JSONArray();
					reg.add("Nombre del Ejecutivo:");
					reg.add(sol.getNombre_ejecutivo());
					reg.add("TEXTO");
					cifrasDatosEjecutivoNafinDataArray.add(reg);
					
					reg = new JSONArray();
					reg.add("Correo electr&oacute;nico:");
					reg.add(sol.getCg_mail());
					reg.add("TEXTO");
					cifrasDatosEjecutivoNafinDataArray.add(reg);
					
					reg = new JSONArray();
					reg.add("Tel&eacute;fono:");
					reg.add(sol.getCg_telefono());
					reg.add("TEXTO");
					cifrasDatosEjecutivoNafinDataArray.add(reg);
					
					reg = new JSONArray();
					reg.add("Fax:");
					reg.add(sol.getCg_fax());
					reg.add("TEXTO");
					cifrasDatosEjecutivoNafinDataArray.add(reg);						
					
					jsonObj.put("cifrasDatosEjecutivoNafinDataArray",cifrasDatosEjecutivoNafinDataArray);
					
					//	Datos del Intermediario Financiero/Acreditado
					JSONArray 	cifrasDatosIntermediarioF_ADataArray 	= new JSONArray();
					String tipoOper = sol.getTipoOper();
					reg = new JSONArray();
					reg.add("Tipo de operación:");
					reg.add(tipoOper);
					reg.add("TEXTO");
					cifrasDatosIntermediarioF_ADataArray.add(reg);
					if(!"Primer Piso".equals(tipoOper)){
						reg = new JSONArray();
						reg.add("Nombre del Intermediario:");
						reg.add((!"".equals(sol.getNombre_if_ci()))?sol.getNombre_if_ci():sol.getNombre_if_cs());
						reg.add("TEXTO");
						cifrasDatosIntermediarioF_ADataArray.add(reg);
						if(!"IF".equals(strTipoUsuario)&&!"".equals(sol.getRiesgoIf())){
							reg = new JSONArray();
							reg.add("Riesgo del Intermediario:");
							reg.add(sol.getRiesgoIf());
							reg.add("TEXTO");
							cifrasDatosIntermediarioF_ADataArray.add(reg);
						}
					}
					if(!"IF".equals(strTipoUsuario)&&!"".equals(sol.getCg_acreditado())){
						reg = new JSONArray();
						reg.add("Nombre del Acreditado:");
						reg.add(sol.getCg_acreditado());
						reg.add("TEXTO");
						cifrasDatosIntermediarioF_ADataArray.add(reg);
						if("Primer Piso".equals(tipoOper)){
							reg = new JSONArray();
							reg.add("Riesgo del Acreditado:");
							reg.add(sol.getRiesgoIf());
							reg.add("TEXTO");
							cifrasDatosIntermediarioF_ADataArray.add(reg);
						}
					}
								
					jsonObj.put("cifrasDatosIntermediarioF_ADataArray",cifrasDatosIntermediarioF_ADataArray);
					
					//Características de la Operación
					JSONArray 	cifrasCaracteristicasDataArray 	= new JSONArray();
					
					if(!"".equals(sol.getCg_programa())){
						reg = new JSONArray();
						reg.add("Nombre del programa o acreditado Final:");
						reg.add(sol.getCg_programa());
						reg.add("TEXTO");
						cifrasCaracteristicasDataArray.add(reg);
					}
					String moneda = (sol.getIc_moneda().equals("1"))?"Pesos":(sol.getIc_moneda().equals("54")?"Dólares":"");
					reg = new JSONArray();
					reg.add("Monto total requerido:");
					reg.add("$"+Comunes.formatoDecimal(sol.getFn_monto(),2)+" "+moneda);
					reg.add("NUMERO_ENTERO");
					cifrasCaracteristicasDataArray.add(reg);
					
					reg = new JSONArray();
						reg.add("Tipo de tasa requerida:");
						reg.add(sol.getTipo_tasa());
						reg.add("TEXTO");
						cifrasCaracteristicasDataArray.add(reg);
					reg = new JSONArray();
						reg.add("N&uacute;mero de Disposiciones:");
						reg.add(sol.getIn_num_disp());
						reg.add("TEXTO");
						cifrasCaracteristicasDataArray.add(reg);
					if(Integer.parseInt(sol.getIn_num_disp())>1){
						reg = new JSONArray();
							reg.add("Disposiciones m&uacute;ltiples.&nbsp;Tasa de Inter&eacute;s:");
							reg.add(("S".equals(sol.getCs_tasa_unica())?"Unica":"Varias"));
							reg.add("TEXTO");
							cifrasCaracteristicasDataArray.add(reg);
					}
					reg = new JSONArray();
						reg.add("Plazo de la operaci&oacute;n:");
						reg.add(sol.getIg_plazo()+" Días");
						reg.add("TEXTO");
						cifrasCaracteristicasDataArray.add(reg);
					reg = new JSONArray();
						reg.add("Esquema de recuperaci&oacute;n del capital:");
						reg.add(sol.getEsquema_recup());
						reg.add("TEXTO");
						cifrasCaracteristicasDataArray.add(reg);
					if(!"1".equals(sol.getIc_esquema_recup())){
						String ic_esquema_recup = sol.getIc_esquema_recup();//Valores que puede tomar ic_esquema_recup :
																			 //1=Cupon cero,2=Tradicional,3=Plan de pagos,4=Rentas
						if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("3")){
							reg = new JSONArray();
								reg.add("Periodicidad del pago de Inter&eacute;s:");
								reg.add(sol.periodoTasa(sol.getCg_ut_ppi()));
								reg.add("TEXTO");
								cifrasCaracteristicasDataArray.add(reg);
						}
						if(!"3".equals(sol.getIc_esquema_recup())){
							if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("4")){
								reg = new JSONArray();
									reg.add("Periodicidad de Capital:");
									reg.add(sol.periodoTasa(sol.getCg_ut_ppc()));
									reg.add("TEXTO");
									cifrasCaracteristicasDataArray.add(reg);
							}
						}		
						if(ic_esquema_recup.equals("2") && !"".equals(sol.getCg_pfpc())){
							reg = new JSONArray();
								reg.add("Primer fecha de pago de Capital:");
								reg.add(sol.getCg_pfpc());
								reg.add("TEXTO");
								cifrasCaracteristicasDataArray.add(reg);
							reg = new JSONArray();
								reg.add("Pen&uacute;ltima fecha de pago de Capital:");
								reg.add(sol.getCg_pufpc());
								reg.add("TEXTO");
								cifrasCaracteristicasDataArray.add(reg);
						}
					}
					jsonObj.put("cifrasCaracteristicasDataArray",cifrasCaracteristicasDataArray);
					
					//Detalle de Disposiciones
					JSONArray 	cifrasDetallesDisposicionDataArray 	= new JSONArray();
					JSONArray reg1 = new JSONArray();
					for(int i=0;i<Integer.parseInt(sol.getIn_num_disp());i++){
						reg1 = new JSONArray();
								reg1.add(new Integer(i+1));
								reg1.add("$"+Comunes.formatoDecimal(sol.getFn_monto_disp(i),2));
								reg1.add(sol.getDf_disposicion(i));
								reg1.add(sol.getDf_vencimiento(i));
								cifrasDetallesDisposicionDataArray.add(reg1);
					}
					jsonObj.put("cifrasDetallesDisposicionDataArray",cifrasDetallesDisposicionDataArray);
					
					//Plan de pagos
					JSONArray 	cifrasPlanPagosDataArray 	= new JSONArray();
					if((sol.getPagos()).size() > 0){
						JSONArray regPlan = new JSONArray();
						ArrayList alFilas = sol.getPagos();
						for(int i=0;i<alFilas.size();i++){
							ArrayList alColumnas = (ArrayList)alFilas.get(i);
							regPlan = new JSONArray();
							regPlan.add((String)alColumnas.get(0));
							regPlan.add((String)alColumnas.get(1));
							regPlan.add("$"+Comunes.formatoDecimal(alColumnas.get(2).toString(),2));
							cifrasPlanPagosDataArray.add(regPlan);
						}
					}
					jsonObj.put("cifrasPlanPagosDataArray",cifrasPlanPagosDataArray);	
					
					 //Obteniendo el periodo de las tasas
					if("1".equals(sol.getIc_esquema_recup())){
						periodo = "Al Vencimiento";
					}else if("4".equals(sol.getIc_esquema_recup())&&"0".equals(sol.getCg_ut_ppc())){
						periodo = "Al Vencimiento";
					}else if( !sol.periodoTasa(sol.getCg_ut_ppi()).equals("") ){
						periodo = sol.periodoTasa(sol.getCg_ut_ppi())+"mente";
					}
					else if( !sol.periodoTasa(sol.getCg_ut_ppc()).equals("") ){
						periodo = sol.periodoTasa(sol.getCg_ut_ppc())+"mente";
					}else{
						periodo = "Al Vencimiento";
					}
					JSONArray 	cifrasDetallesDataArray 	= new JSONArray();//Detalles
					JSONArray 	cifrasTasaIndicativaDataArray 	= new JSONArray();//Tasa Indicativa
					JSONArray 	cifrasNotaDataArray 	= new JSONArray();//Nota
					if( estatus_solicitud.equals("2") || estatus_solicitud.equals("5") ){
						if("I".equals(sol.getTipoCotizacion())&&"IF".equals(strTipoUsuario)){
							reg = new JSONArray();
								reg.add("MEnsaje:");
								reg.add("COTIZACION INDICATIVA,<br> NO ES POSIBLE TOMAR EN FIRME LA OPERACION,<br> FAVOR DE CONSULTAR A SU EJECUTIVO.");
								reg.add("TEXTO");
								cifrasDetallesDataArray.add(reg);
						}else if("N".equals(sol.getCs_vobo_ejec())&&"IF".equals(strTipoUsuario)){
							reg = new JSONArray();
								reg.add("Mensaje:");
								reg.add("Para realizar la toma en firme favor de ponerse en contacto <br>con su ejecutivo");
								reg.add("TEXTO");
								cifrasDetallesDataArray.add(reg);
						}
						//Tasa Indicativa
						reg = new JSONArray();
							reg.add("Tasa Indicativa con oferta mismo d&iacute;a:");
							reg.add("<strong>"+sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_md(),2)+"</strong>&nbsp;anual pagadera "+periodo);
							reg.add("TEXTO");
							cifrasTasaIndicativaDataArray.add(reg);
						if(!(Comunes.formatoDecimal(sol.getIg_tasa_spot(),2)).equals("0.00")){
							reg = new JSONArray();
								reg.add("Tasa Indicativa con oferta 48 horas:");
								reg.add("<strong>"+sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_spot(),2)+"</strong>&nbsp;anual pagadera "+periodo);
								reg.add("TEXTO");
								cifrasTasaIndicativaDataArray.add(reg);	
						}
						//Nota
						reg = new JSONArray();
							reg.add("Nota:");
							StringBuffer nota = new StringBuffer("");
							nota.append("<b>Tasa con oferta Mismo D&iacute;a:</b>&nbsp;El Intermediario Financiero tiene la opci&oacute;n de tomarla en firme el mismo d&iacute;a que haya recibido la cotizaci&oacute;n indicativa a m&aacute;s tardar a las 12:30 horas.<br>");
							if(!(Comunes.formatoDecimal(sol.getIg_tasa_spot(),2)).equals("0.00")){
								nota.append("<b>Tasa con oferta 48 Horas:</b>&nbsp;El Intermediario Financiero tiene la opci&oacute;n de tomarla en firme a m&aacute;s tardar dos d&iacute;as h&aacute;biles despu&eacute;s de que la Tesorer&iacute;a haya proporcionado la cotizaci&oacute;n indicativa antes de las 12:30 horas del segundo día h&aacute;bil.<br>");
							}
							nota.append("Las cotizaciones indicativas proporcionadas por la Tesorer&iacute;a de Nacional Financiera son de uso reservado para el intermediario solicitante y ser&aacute; responsabilidad de dicho intermediario el mal uso de la informaci&oacute;n proporcionada.");
							reg.add(nota.toString());
							reg.add("TEXTO");
							cifrasTasaIndicativaDataArray.add(reg);
						
					}
					jsonObj.put("cifrasDetallesDataArray",cifrasDetallesDataArray);	
					jsonObj.put("cifrasTasaIndicativaDataArray",cifrasTasaIndicativaDataArray);	
					jsonObj.put("cifrasNotaDataArray",cifrasNotaDataArray);
					//TASA CONFIRMADA
					JSONArray 	cifrasTasaConfirmadaDataArray 	= new JSONArray();//Nota
					String headerFecha = "";
					if( estatus_solicitud.equals("7") || estatus_solicitud.equals("8") || estatus_solicitud.equals("9")){
						headerFecha = "FECHA Y HORA DE "+("8".equals(estatus_solicitud)?"CANCELACION":"CONFIRMACION");
						//Headers (TASA CONFIRMADA),(headerFecha)
						if(sol.getCs_aceptado_md().equals("S")){
							reg = new JSONArray();
								//reg.add("");
								reg.add("Tasa Confirmada&nbsp;<strong>"+sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_md(),2)+"</strong> anual pagadera "+periodo+" "+tec);
								//reg.add("TEXTO");
								cifrasTasaConfirmadaDataArray.add(reg);	
						}else{
							reg = new JSONArray();
								//reg.add("");
								reg.add("Tasa Confirmada&nbsp;<strong>"+sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_spot(),2)+"</strong> anual pagadera "+periodo+" "+tec);
								//reg.add("TEXTO");
								cifrasTasaConfirmadaDataArray.add(reg);	
						}
						reg = new JSONArray();
							//reg.add("");
							reg.add(("8".equals(estatus_solicitud)?sol.getDf_cotizacion()+" "+sol.getHora_cotizacion():sol.getDf_aceptacion()));
							//reg.add("TEXTO");
							cifrasTasaConfirmadaDataArray.add(reg);	
					}
					jsonObj.put("headerFecha",headerFecha);
					jsonObj.put("cifrasTasaConfirmadaDataArray",cifrasTasaConfirmadaDataArray);
					//OBSEVACIONES
					JSONArray 	cifrasObservacionesDataArray 	= new JSONArray();//OBSERVACIONES
					if(!sol.getCg_observaciones().trim().equals("") ){
						if(!"NB".equals(origen)) {
							reg = new JSONArray();
								reg.add("");
								reg.add("<div rowspan=4 colspan=4 align=justify>"+sol.getCg_observaciones()+"</div>");
								reg.add("TEXTO");
								cifrasObservacionesDataArray.add(reg);	
						}
					}
					jsonObj.put("cifrasObservacionesDataArray",cifrasObservacionesDataArray);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			 infoRegresar =jsonObj.toString();	
	}else if(informacion.equals("Cotizador.Solicitud_arch_pdf")){
			SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
			AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
			
			String ic_solicitud	 		= (request.getParameter("ic_solic_esp")!=null)?request.getParameter("ic_solic_esp"):""; 
			String intermediario	  		= (request.getParameter("intermediario")!=null)?request.getParameter("intermediario"):""; 
			String estatus_solicitud	= (request.getParameter("estatus_solicitud")!=null)?request.getParameter("estatus_solicitud"):""; 
			String origen	  				= (request.getParameter("origen")!=null)?request.getParameter("origen"):""; 
			String tec = "";
			String periodo = "";
			String nombreArchivo ="";
			boolean validaPDF = false;
			String clase="";
			int tipoClase=0;
			try{
				
				Solicitud	sol = solCotEsp.consultaSolicitud(ic_solicitud);
				nombreArchivo= sol.getNumero_solicitud()+".pdf";
				
				boolean muestraCancelado = false;
				if("8".equals(estatus_solicitud)){
					muestraCancelado = true;
				}
						
				ComunesPDF	pdfDoc			= new ComunesPDF(1,strDirectorioTemp+nombreArchivo,"Solicitud Número: "+ sol.getNumero_solicitud() +"           Pagina ", muestraCancelado);
				float widthsDocs[] = {0.35f,0.75f};
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(
					pdfDoc,
					(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String)session.getAttribute("strNombre"),
					(String)session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),
					strDirectorioPublicacion);	
				pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
				
				pdfDoc.setTable(2,65);
				pdfDoc.setCell("Datos de la Cotización","celda04",ComunesPDF.CENTER,2,1,1);
				pdfDoc.setCell("Número de Solicitud:","formas",ComunesPDF.LEFT,1,1,1);
				pdfDoc.setCell(sol.getNumero_solicitud(),"formas",ComunesPDF.LEFT,1,1,1);
				
				pdfDoc.setCell("Fecha de Solicitud:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
				pdfDoc.setCell(sol.getDf_solicitud(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
				pdfDoc.setCell("Hora de Solicitud:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
				pdfDoc.setCell(sol.getHora_solicitud(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);		
				
				if( !estatus_solicitud.equals("1") & !estatus_solicitud.equals("3") & !estatus_solicitud.equals("4") ){//1=Solicictada,3=Rechazada,4=En proceso
					String auxFecha = "Cotización";
					if("8".equals(estatus_solicitud))
						auxFecha = "Cancelación";		
					pdfDoc.setCell("Fecha de "+auxFecha+":",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
					pdfDoc.setCell(sol.getDf_cotizacion(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);		
					pdfDoc.setCell("Hora de "+auxFecha+":",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
					pdfDoc.setCell(sol.getHora_cotizacion(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
				}
				pdfDoc.setCell("Usuario:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
				pdfDoc.setCell(sol.getIc_usuario() +" - "+sol.getNombre_usuario(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
				
				if(estatus_solicitud.equals("7")||estatus_solicitud.equals("8")||estatus_solicitud.equals("9")||origen.equals("TF")){
					if(!origen.equals("TF")){
						pdfDoc.setCell("Número de Recibo:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
						pdfDoc.setCell(sol.getIg_num_referencia(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
					}
					pdfDoc.setCell(
						"En caso de que la disposición de los recursos no se lleve a cabo total o parcialmente\n"+
						"en la fecha estipulada, Nacional Financiera aplicará el cobro de una comisión\n"+
						"sobre los montos no dispuestos, conforme a las políticas de crédito aplicables\n"+
						"a este tipo de operaciones."
					,"formas",ComunesPDF.CENTER,2,1,1);
				}
				pdfDoc.addTable();
				pdfDoc.addText("\n","formas",ComunesPDF.LEFT);
				pdfDoc.setTable(2,90,widthsDocs);
				
				tipoClase=0;
				
				if(!origen.equals("TFE")){
					pdfDoc.setCell("Datos del Ejecutivo Nafin","celda04",ComunesPDF.LEFT,2,1,0);
					pdfDoc.setCell("Nombre del Ejecutivo","formas",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getNombre_ejecutivo(),"formas",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Correo electrónico",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getCg_mail(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Teléfono",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getCg_telefono(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Fax",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getCg_fax(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				}
				
				tipoClase=0;
				String tipooper = sol.getTipoOper();
				
				if(!origen.equals("TFE")){
					pdfDoc.setCell("Datos del Intermediario Financiero/Acreditado","celda04",ComunesPDF.LEFT,2,1,0);
					pdfDoc.setCell("Tipo de operación","formas",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(tipooper,"formas",ComunesPDF.LEFT,1,1,0);
					
					if(!"Primer Piso".equals(tipooper)){
						pdfDoc.setCell("Nombre del Intermediario",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell((!"".equals(sol.getNombre_if_ci()))?sol.getNombre_if_ci():sol.getNombre_if_cs(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						if(!"IF".equals(strTipoUsuario)&&!"".equals(sol.getRiesgoIf())){
							pdfDoc.setCell("Riesgo del Intermediario",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
							pdfDoc.setCell(sol.getRiesgoIf(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						}
					}	
				}
				
				if(!"IF".equals(strTipoUsuario)&&!"".equals(sol.getCg_acreditado())){
					pdfDoc.setCell("Nombre del Acreditado",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getCg_acreditado(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					if("Primer Piso".equals(tipooper)){
						if(!origen.equals("TFE")){
							pdfDoc.setCell("Riesgo del Acreditado",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
							pdfDoc.setCell(sol.getRiesgoAc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						}
					}
				}
				
				tipoClase=1;
				
				pdfDoc.setCell("Características de la Operación","celda04",ComunesPDF.LEFT,2,1,0);
			
				if(!"".equals(sol.getCg_programa())){
					pdfDoc.setCell("Nombre del programa o\nacreditado final:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getCg_programa(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);	
				}
				pdfDoc.setCell("Monto total requerido:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(sol.getFn_monto(),2)+(sol.getIc_moneda().equals("1")?" Pesos":" Dólares"),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("Tipo de tasa requerida:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(sol.getTipo_tasa(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("Número de Disposiciones",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(sol.getIn_num_disp(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				if(Integer.parseInt(sol.getIn_num_disp())>1){
					pdfDoc.setCell("Disposiciones múltiples. Tasa de Interés:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(("S".equals(sol.getCs_tasa_unica())?"Única":"Varias"),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				}
				pdfDoc.setCell("Plazo de la operación:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(sol.getIg_plazo()+" Días",((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("Esquema de recuperación del capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(sol.getEsquema_recup(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				if(!"1".equals(sol.getIc_esquema_recup())){
					String ic_esquema_recup = sol.getIc_esquema_recup();//Valores que puede tomar ic_esquema_recup :
																			 //1=Cupon cero,2=Tradicional,3=Plan de pagos,4=Rentas
					if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("3")){
						pdfDoc.setCell("Periodicidad del pago de Interés:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						//pdfDoc.setCell(sol.getIg_ppi()+" "+sol.descripcionUT(sol.getCg_ut_ppi()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell(sol.periodoTasa(sol.getCg_ut_ppi()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					}
					if(!"3".equals(sol.getIc_esquema_recup())){
						if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("4")){
							pdfDoc.setCell("Periodicidad del Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
							//pdfDoc.setCell(sol.getIg_ppc()+" "+sol.descripcionUT(sol.getCg_ut_ppc()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
							pdfDoc.setCell(sol.periodoTasa(sol.getCg_ut_ppc()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						}
					}
					if( ic_esquema_recup.equals("2") && !"".equals(sol.getCg_pfpc())){
						pdfDoc.setCell("Primer fecha de pago de Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell(sol.getCg_pfpc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell("Penúltima fecha de pago de Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell(sol.getCg_pufpc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
					}
				}
				pdfDoc.addTable();
				
				pdfDoc.setTable(4,90);
				pdfDoc.setCell("Detalle de Disposiciones","celda04",ComunesPDF.LEFT,4,1,0);
				pdfDoc.setCell("Número de Disposición","formas",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("Monto","formas",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("Fecha de Disposición","formas",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("Fecha de Vencimiento","formas",ComunesPDF.LEFT,1,1,0);
				for(int i=0;i<Integer.parseInt(sol.getIn_num_disp());i++){
					clase = ((i%2)!=0)?"formas":"celda01";
					pdfDoc.setCell((i+1)+"",clase,ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(sol.getFn_monto_disp(i),2),clase,ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getDf_disposicion(i),clase,ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sol.getDf_vencimiento(i),clase,ComunesPDF.LEFT,1,1,0);
				}
				pdfDoc.addTable();
				if((sol.getPagos()).size() > 0){
					ArrayList alFilas = sol.getPagos();
					pdfDoc.setTable(3,90);
					pdfDoc.setCell("Plan de Pagos Capital","celda04",ComunesPDF.LEFT,3,1,0);
					pdfDoc.setCell("Número de Pago","formas",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Fecha de Pago","formas",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Monto a pagar","formas",ComunesPDF.LEFT,1,1,0);
					for(int i=0;i<alFilas.size();i++){
						ArrayList alColumnas = (ArrayList)alFilas.get(i);
						clase = ((i%2)!=0)?"formas":"celda01";
						pdfDoc.setCell(alColumnas.get(0).toString(),clase,ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell(alColumnas.get(1).toString(),clase,ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell(Comunes.formatoDecimal(alColumnas.get(2).toString(),2),clase,ComunesPDF.LEFT,1,1,0);
					}
					pdfDoc.addTable();
				}
				 //Obteniendo el periodo de las tasas
				if("1".equals(sol.getIc_esquema_recup())){
					periodo = "Al Vencimiento";
				}else if("4".equals(sol.getIc_esquema_recup())&&"0".equals(sol.getCg_ut_ppc())){
					periodo = "Al Vencimiento";
				}else if( !sol.periodoTasa(sol.getCg_ut_ppi()).equals("") ){
					periodo = sol.periodoTasa(sol.getCg_ut_ppi())+"mente";
				}
				else if( !sol.periodoTasa(sol.getCg_ut_ppc()).equals("") ){
					periodo = sol.periodoTasa(sol.getCg_ut_ppc())+"mente";
				}else{
					periodo = "Al Vencimiento";
				}
			
				if(!"TF".equals(origen) && !"TFE".equals(origen) ){
					if( estatus_solicitud.equals("2") || estatus_solicitud.equals("5") ){
						pdfDoc.setTable(1,90);
						pdfDoc.setCell("TASA INDICATIVA","celda04",ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell("Tasa Indicativa con oferta mismo día:  "+sol.getDescTasaIndicativa()+" "+sol.getIg_tasa_md() + " % anual pagadera "+periodo+"\n"+
										"Tasa Indicativa con oferta 48 horas:     "+sol.getDescTasaIndicativa()+" "+sol.getIg_tasa_spot()+ " % anual pagadera "+periodo
										,"celda01",ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell("Nota:\n\n","formas",ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell("Tasa con oferta Mismo Día: El Intermediario Financiero tiene la opción de tomarla en firme el mismo día que haya recibido la cotización indicativa a más tardar a las 12:30 horas.\n\n"
							,"formas",ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell("Tasa con oferta 48 Horas: El Intermediario Financiero tiene la opción de tomarla en firme a más tardar dos días hábiles después de que la Tesorería haya proporcionado la cotización indicativa antes de las 12:30 horas del segundo día hábil."
							,"formas",ComunesPDF.LEFT,1,1,0);				
						pdfDoc.setCell("\nLas cotizaciones indicativas proporcionadas por la Tesorería de Nacional Financiera son de uso reservado para el intermediario solicitante y será responsabilidad de dicho intermediario el mal uso de la información proporcionada."
							,"formas",ComunesPDF.LEFT,1,1,0);				
						pdfDoc.addTable();
					}
					
					if( estatus_solicitud.equals("7") || estatus_solicitud.equals("8") || estatus_solicitud.equals("9")){
						pdfDoc.setTable(2,90);
						pdfDoc.setCell("TASA CONFIRMADA","celda04",ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell("FECHA Y HORA DE "+("8".equals(estatus_solicitud)?"CANCELACION":"CONFIRMACION"),"celda04",ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
						pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
						if(sol.getCs_aceptado_md().equals("S"))
							pdfDoc.setCell("Tasa Confirmada " +sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_md(),2) + " % anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
						else
							pdfDoc.setCell("Tasa Confirmada " +sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_spot(),2) + "% anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
						pdfDoc.setCell(("8".equals(estatus_solicitud)?sol.getDf_cotizacion()+" "+sol.getHora_cotizacion():sol.getDf_aceptacion()) ,"celda01",ComunesPDF.LEFT,1,1,1);
						pdfDoc.addTable();
					}
				}else{
						boolean mismoDiaOk = autSol.validaMismoDia(ic_solicitud);
						pdfDoc.setTable(1,90);
						pdfDoc.setCell("TASA COTIZADA","celda04",ComunesPDF.LEFT,1,1,0);
						
						if(mismoDiaOk)
							pdfDoc.setCell("Tasa con oferta mismo dia" +sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_md(),2) + " % anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
						else
							pdfDoc.setCell("Tasa con oferta 48 horas" +sol.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(sol.getIg_tasa_spot(),2) + "% anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
						pdfDoc.addTable();
				}
				
				//pdfDoc.newPage();
				if(!sol.getCg_observaciones().trim().equals("")){
					pdfDoc.setTable(3,90);
					pdfDoc.setCell("Observaciones:","celda04",ComunesPDF.LEFT,3,1,0);
					pdfDoc.setCell(sol.getCg_observaciones(),"formas",ComunesPDF.LEFT,3,1,0);
					pdfDoc.addTable();
				}
			
				pdfDoc.endDocument();
				validaPDF = true;
	
			}catch(Exception e){
				e.printStackTrace();
			}			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				
			infoRegresar =jsonObj.toString();		
	}else if(informacion.equals("Cotizador.Generar_archcsv_plan_pagos")){
		SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
		AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
		
		String ic_solicitud = request.getParameter("ic_plan_de_pagos");
		CreaArchivo archivo = new CreaArchivo();
		String contenidoArchivo="";
		String	nombreArchivo= "";
		boolean validaCSV = false;
		
		try{
			boolean cambiar_clase_celda=false;
			
			Solicitud	sol = solCotEsp.consultaSolicitud(ic_solicitud);
			if((sol.getPagos()).size() > 0){
				ArrayList alFilas = sol.getPagos();
				contenidoArchivo = "\r\n Plan de Pagos Capital - Solicitud Número: "+sol.getNumero_solicitud() + "\r\n\r\n"+
									"Número de Pago,Fecha de Pago,Monto a pagar \r\n";
				for(int i=0;i<alFilas.size();i++){
					ArrayList alColumnas = (ArrayList)alFilas.get(i);
					contenidoArchivo += alColumnas.get(0).toString() + "," +alColumnas.get(1).toString() + "," +
										alColumnas.get(2).toString() + "\r\n";
				}
			}else{
				contenidoArchivo = "No se encontraron registros";
			}
			if(!archivo.make(contenidoArchivo, strDirectorioTemp, ".csv")){
				//out.print("<--!Error al generar el archivo-->");
			}else{
				nombreArchivo = archivo.nombre;
				validaCSV = true;
			}
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}catch(Exception e){
			e.printStackTrace();
		}		
	
			infoRegresar =jsonObj.toString();		
	}else if(	informacion.equals("Cotizador.Procesar_guardar_cambios")){

		String ic_solic_esp[]		=	request.getParameterValues("ic_solic_esp");
		String ic_estatus[] 			= 	request.getParameterValues("ic_estatus");
		String ig_tasa_md[]			=	request.getParameterValues("ig_tasa_md");
		String ig_tasa_spot[]		=	request.getParameterValues("ig_tasa_spot");
		String ig_num_referencia[]	=	request.getParameterValues("ig_num_referencia");
		String cg_causa_rechazo[]	=	request.getParameterValues("cg_causa_rechazo");
		String cg_tipo_solic[]		=	request.getParameterValues("cg_tipo_solic");
		String ic_tasa[]				=	request.getParameterValues("ic_tasa");
		
		boolean exito = true;
		boolean ok = false;
		try{
				ok = solCot.guardaCambios(ic_solic_esp,ic_estatus,ig_tasa_md,ig_tasa_spot,ig_num_referencia,cg_causa_rechazo,cg_tipo_solic,ic_tasa);
				
			for(int i=0;i<ic_estatus.length;i++){
				if(!ic_estatus[i].equals("")){
					double tasaMD = (ig_tasa_md[i].equals("")?0:Double.parseDouble(ig_tasa_md[i]));
					double tasaSpot = (ig_tasa_spot[i].equals("")?0:Double.parseDouble(ig_tasa_spot[i]));
					System.err.println(ic_estatus[i]);
					System.err.println(tasaMD);
					System.err.println(tasaSpot);
					System.err.println(ig_num_referencia[i]);
					System.err.println(cg_causa_rechazo[i]);
					System.err.println(cg_tipo_solic[i]);
					System.err.println((ic_tasa[i].equals("")?0:Integer.parseInt(ic_tasa[i])));
					System.err.println(ic_solic_esp[i]);
					System.err.println(ic_solic_esp[i]);
				}
			}
		}catch(Exception e){
			exito = false;
			e.printStackTrace();
		}

		jsonObj.put("success", new Boolean(exito));
		jsonObj.put("ok", new Boolean(ok));
		infoRegresar =jsonObj.toString();

	}
	
	log.debug("infoRegresar = <" + infoRegresar + ">"); 
%>

<%=infoRegresar%>