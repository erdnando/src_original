	var procesarConsultaData = function(store, arrRegistros, opts){
		var grid = Ext.getCmp('grid');
		grid.el.unmask();
		if(arrRegistros != null){
			if(!grid.isVisible()){
				grid.show();
			}
			if(store.getTotalCount() <= 0){
				grid.el.mask('No se encontro ning�n registro', 'x-mask');
			}
		}
	}
	var procesarConsultaDataDolar = function(store, arrRegistros, opts){
		var grid = Ext.getCmp('gridDolar');
		grid.el.unmask();
		if(arrRegistros != null){
			if(!grid.isVisible()){
				grid.show();
			}
			if(store.getTotalCount() <= 0){
				grid.el.mask('No se encontro ning�n registro', 'x-mask');
			}
		}
	}
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '22forma8Ext01.data.jsp',
		baseParams:{
			informacion: 'consultaInicial'
		},
		fields: [
			{name: 'IDCURVA'},
			{name: 'NOMBRE'},
			{name: 'TIPO'},
			{name: 'MODOCALCULO'},
			{name: 'BASE'},
			{name: 'CURVADEFAULT'},
			{name: 'IC_MONEDA'},
			{name: 'CS_SWAP'},
			{name: 'CS_FONDEO'},
			{name: 'CS_DEPOSITO'},
			{name: 'CG_CURVA_IF'},
			
			{name: 'AUX_CS_SWAP'},
			{name: 'AUX_CS_FONDEO'},
			{name: 'AUX_CS_DEPOSITO'},
			{name: 'AUX_CG_CURVA_IF'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}
	});
	var consultaDataDolar = new Ext.data.JsonStore({
		root: 'registros',
		url: '22forma8Ext01.data.jsp',
		baseParams:{
			informacion: 'consultaInicial'
		},
		fields: [
			{name: 'IDCURVA'},
			{name: 'NOMBRE'},
			{name: 'TIPO'},
			{name: 'MODOCALCULO'},
			{name: 'BASE'},
			{name: 'CURVADEFAULT'},
			{name: 'IC_MONEDA'},
			{name: 'CS_SWAP'},
			{name: 'CS_FONDEO'},
			{name: 'CS_DEPOSITO'},
			{name: 'CG_CURVA_IF'},
			
			{name: 'AUX_CS_SWAP'},
			{name: 'AUX_CS_FONDEO'},
			{name: 'AUX_CS_DEPOSITO'},
			{name: 'AUX_CG_CURVA_IF'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			load: procesarConsultaDataDolar,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataDolar(null, null, null);					
				}
			}
		}
	});
	var procesarModificarFondeo = function(opts, success, response){
		if(success == true){//MENSAJE
			var mensaje = 	Ext.util.JSON.decode(response.responseText).MENSAJE;;
			Ext.MessageBox.alert("Mensaje de confirmaci�n", mensaje);
			if(Ext.getCmp('cmbClaveMoneda').getValue()=="1"){
				consultaData.load({
					params: Ext.apply({							
						tipoMoneda: '1'
					})
				});
			}else if(Ext.getCmp('cmbClaveMoneda').getValue()=="54"){
				consultaDataDolar.load({
					params: Ext.apply({							
						tipoMoneda: '54'
					})
				});
			}else{
				consultaData.load({
					params: Ext.apply({							
						tipoMoneda: '1'
					})
				});
				consultaDataDolar.load({
					params: Ext.apply({							
						tipoMoneda: '54'
					})
				});
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}   
	function validaFondeo(check,rowIndex,colIds){
		var gridConsulta = Ext.getCmp('grid');	
		var store = gridConsulta.getStore();
		var reg = gridConsulta.getStore().getAt(rowIndex);
		var fondeo_curva = reg.get('CS_FONDEO')=="S"?"checked":"";
		if(fondeo_curva == 'checked'){
			check.checked = true;
			reg.set('CS_FONDEO','S');
		}else{
			reg.set('CS_FONDEO','N');
		}
		Ext.Ajax.request({
			url: '22forma8Ext01.data.jsp',
			params: Ext.apply({
				informacion : 'validaFondeo',
				idCurva :reg.get('IDCURVA'),
				hidCsFondeo:reg.get('CS_FONDEO')
			}),
			callback: procesarModificarFondeo
		});
	}
	function validaDefault(check,rowIndex,colIds){
		var gridConsulta = Ext.getCmp('grid');	
		var store = gridConsulta.getStore();
		var reg = gridConsulta.getStore().getAt(rowIndex);
		if(reg.get('CG_CURVA_IF') == 'checked'){
			check.checked = true
		}else{
			if(check.checked == true)  {
				reg.set('CG_CURVA_IF','S');
			}else{
				reg.set('CG_CURVA_IF','N');
			}	
		}
		if(reg.get('CS_FONDEO')=='N'){
			reg.set('CG_CURVA_IF','N');
			alert("Debe seleccionar una curva de fondeo");
			return;
		}else{
			Ext.Ajax.request({
				url: '22forma8Ext01.data.jsp',
				params: Ext.apply({
					informacion : 'validaDefault',
					idCurva :reg.get('IDCURVA'),
					hidCveMoneda:reg.get('IC_MONEDA')
				}),
				callback: procesarModificarFondeo
			});
		}
	}
	function validaDepositos(check,rowIndex,colIds){
		var gridConsulta = Ext.getCmp('grid');	
		var store = gridConsulta.getStore();
		var reg = gridConsulta.getStore().getAt(rowIndex);
		if(reg.get('CS_DEPOSITO') == 'checked'){
			check.checked = true
		}else{
			if(check.checked == true)  {
				reg.set('CS_DEPOSITO','S');
			}else{
				reg.set('CS_DEPOSITO','N');
			}	
		}
		Ext.Ajax.request({
			url: '22forma8Ext01.data.jsp',
			params: Ext.apply({
				informacion : 'validaDepositos',
				idCurva :reg.get('IDCURVA'),
				hidCveMoneda:reg.get('IC_MONEDA')
			}),
			callback: procesarModificarFondeo
		});
	}
	function validaFondeoDolar(check,rowIndex,colIds){
		var gridConsulta = Ext.getCmp('gridDolar');	
		var store = gridConsulta.getStore();
		var reg = gridConsulta.getStore().getAt(rowIndex);
		var fondeo_curva = reg.get('CS_FONDEO')=="S"?"checked":"";
		if(fondeo_curva ==  'checked'){
			check.checked = true
		}else{
			reg.set('CS_FONDEO','N');
		}
		Ext.Ajax.request({
			url: '22forma8Ext01.data.jsp',
			params: Ext.apply({
				informacion : 'validaFondeo',
				idCurva :reg.get('IDCURVA'),
				hidCsFondeo:reg.get('CS_FONDEO')
			}),
			callback: procesarModificarFondeo
		});
	}
	function validaDefaultDolar(check,rowIndex,colIds){
		var gridConsulta = Ext.getCmp('gridDolar');	
		var store = gridConsulta.getStore();
		var reg = gridConsulta.getStore().getAt(rowIndex);
		if(reg.get('CG_CURVA_IF') == 'checked'){
			check.checked = true
		}else{
			if(check.checked == true)  {
				reg.set('CG_CURVA_IF','S');
			}else{
				reg.set('CG_CURVA_IF','N');
			}	
		}
		if(reg.get('CS_FONDEO')=='N'){
			reg.set('CG_CURVA_IF','N');
			alert("Debe seleccionar una curva de fondeo");
			return;
		}else{
		Ext.Ajax.request({
			url: '22forma8Ext01.data.jsp',
			params: Ext.apply({
				informacion : 'validaDefault',
				idCurva :reg.get('IDCURVA'),
				hidCveMoneda:reg.get('IC_MONEDA')
			}),
			callback: procesarModificarFondeo
		});
		}
	}
	function validaDepositosDolar(check,rowIndex,colIds){
		var gridConsulta = Ext.getCmp('gridDolar');	
		var store = gridConsulta.getStore();
		var reg = gridConsulta.getStore().getAt(rowIndex);
		if(reg.get('CS_DEPOSITO') == 'checked'){
			check.checked = true
		}else{
			if(check.checked == true)  {
				reg.set('CS_DEPOSITO','S');
			}else{
				reg.set('CS_DEPOSITO','N');
			}	
		}
		Ext.Ajax.request({
			url: '22forma8Ext01.data.jsp',
			params: Ext.apply({
				informacion : 'validaDepositos',
				idCurva :reg.get('IDCURVA'),
				hidCveMoneda:reg.get('IC_MONEDA')
			}),
			callback: procesarModificarFondeo
		});
	}
	function validaSWAPDolar(check,rowIndex,colIds){
		var gridConsulta = Ext.getCmp('gridDolar');	
		var store = gridConsulta.getStore();
		var reg = gridConsulta.getStore().getAt(rowIndex);
		if(reg.get('CS_SWAP') == 'checked'){
			check.checked = true
		}else{
			if(check.checked == true)  {
				reg.set('CS_SWAP','S');
			}else{
				reg.set('CS_SWAP','N');
			}	
		}
		gridConsulta.getStore().commitChanges();
		Ext.Ajax.request({
			url: '22forma8Ext01.data.jsp',
			params: Ext.apply({
				informacion : 'validaSWAP',
				idCurva :reg.get('IDCURVA')
			}),
			callback: procesarModificarFondeo
		});
	}
Ext.onReady(function(){
	var modificar = "N" ;
	var idCurvaM = "";
	var tipo = "";
	var calculo = "";
	var base = "";
	var moneda = "";
	var default_c = "";
	var def_SWAP = "";
	var iniModifica =  function() {  
		modificar = "N";
		idCurvaM = "";
		tipo = "";
		calculo = "";
		base = "";
		default_c = "";
		def_SWAP = "";
		moneda = "";
	}
//--------------------------------HANDLERS-----------------------------------
//---------------------------------------------------------------------------	
	var procesarSuccessFailureInsertar = function(opts, success, response){
		var boton = Ext.getCmp('btnAgregar');
		if(success == true){
			var mensaje = 	Ext.util.JSON.decode(response.responseText).MENSAJE;;
			Ext.MessageBox.alert("Mensaje de confirmaci�n", mensaje);
			boton.enable();
			if(Ext.getCmp('cmbClaveMoneda').getValue()=="1"){
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{							
						tipoMoneda: '1'
					})
				});
			}else{
				consultaDataDolar.load({
					params: Ext.apply(fp.getForm().getValues(),{							
						tipoMoneda: '54'
					})
				});
			}
			Ext.getCmp('forma').getForm().reset();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureModificar = function(opts, success, response){
		var boton = Ext.getCmp('btnGuardar');
		if(success == true){
			var mensaje = 	Ext.util.JSON.decode(response.responseText).MENSAJE;;
			Ext.MessageBox.alert("Mensaje de confirmaci�n", mensaje);
			boton.enable();
			Ext.getCmp('btnGuardar').hide();
			Ext.getCmp('btnAgregar').show();
			if(Ext.getCmp('cmbClaveMoneda').getValue()=="1"){
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{							
						tipoMoneda: '1'
					})
				});
			}else{
				consultaDataDolar.load({
					params: Ext.apply(fp.getForm().getValues(),{							
						tipoMoneda: '54'
					})
				});
			}
			Ext.getCmp('forma').getForm().reset();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	var Modificar = function(grid, rowIndex, colIndex, item, event){
		iniModifica();
		modificar = "S";  		
		var registro = grid.getStore().getAt(rowIndex);
		idCurvaM = registro.get('IDCURVA');
		tipo = registro.get('TIPO');
		calculo = registro.get('MODOCALCULO');
		base = registro.get('BASE');
		moneda  = registro.get('IC_MONEDA');
		def_SWAP = registro.get('CS_SWAP');
		default_c = registro.get('CURVADEFAULT');
		var NOMBRE = registro.get('NOMBRE');
		var IC_MONEDA = registro.get('IC_MONEDA');
		Ext.getCmp('txtNombreArchivo').setValue(NOMBRE);
		Ext.getCmp('cmbClaveMoneda').setValue(IC_MONEDA);
		Ext.getCmp('btnGuardar').show();
		Ext.getCmp('btnAgregar').hide();
	}
	function resultadoElimina(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var mensaje = Ext.util.JSON.decode(response.responseText).MENSAJE;
			Ext.MessageBox.alert("Mensaje de confirmaci�n", mensaje);
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{							
					tipoMoneda: '1'
				})
			});
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	var eliminar = function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		var idCurva = registro.get('IDCURVA');  
		Ext.Msg.show({
			title: 'Confirmar',
			msg: '�Est� seguro de eliminar el registro?',
			buttons: Ext.Msg.OKCANCEL,
			fn: function peticionAjax(btn){
				if (btn == 'ok'){
					Ext.Ajax.request({
						url: '22forma8Ext01.data.jsp',
						params: {
							informacion: 'eliminar',
							idCurva: idCurva
						},
						callback: resultadoElimina
					});
				}
			}
		});
	}
	function resultadoEliminaDolar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var mensaje = Ext.util.JSON.decode(response.responseText).MENSAJE;
			Ext.MessageBox.alert("Mensaje de confirmaci�n", mensaje);
			consultaDataDolar.load({
				params: Ext.apply(fp.getForm().getValues(),{							
					tipoMoneda: '54'
				})
			});
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	var eliminarDolar = function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		var idCurva = registro.get('IDCURVA');  
		Ext.Msg.show({
			title: 'Confirmar',
			msg: '�Est� seguro de eliminar el registro?',
			buttons: Ext.Msg.OKCANCEL,
			fn: function peticionAjax(btn){
				if (btn == 'ok'){
					Ext.Ajax.request({
						url: '22forma8Ext01.data.jsp',
						params: {
							informacion: 'eliminar',
							idCurva: idCurva
						},
						callback: resultadoEliminaDolar
					});
				}
			}
		});
	}
//---------------------------------STORES------------------------------------
//---------------------------------------------------------------------------	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '22forma8Ext01.data.jsp',
		baseParams: {
			informacion: 'catMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		   
	});
//------------------------------COMPONENTES----------------------------------
//---------------------------------------------------------------------------
	var elementosForma = [
		{
			xtype: 'textfield',
			name:	'nombreArchivo',
			id: 'txtNombreArchivo',
			hiddenName:	'nombreArchivo',
			fieldLabel: 'Nombre del Archivo',
			maxLength: 50,
			allowBlank:	true,
			margins: '0 20 0 0'	
		},
		{
			xtype: 'combo',
			name: 'claveMoneda',
			id: 'cmbClaveMoneda',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Moneda',
			emptyText: 'Seleccionar...',	
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveMoneda',
			forceSelection : true,
			allowBlank: true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoMoneda,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select:{
					fn:function(combo){
						var monedaTipo = combo.getValue();
						if(monedaTipo=="1"){
							Ext.getCmp('grid').show();
							Ext.getCmp('gridDolar').hide();
							consultaData.load({
								params: Ext.apply(fp.getForm().getValues(),{							
									tipoMoneda: '1'
								})
							});
						}else if(monedaTipo=="54"){
							Ext.getCmp('gridDolar').show();
							Ext.getCmp('grid').hide();
							consultaDataDolar.load({
								params: Ext.apply(fp.getForm().getValues(),{							
									tipoMoneda: '54'
								})
							});
						}
					}
				}
			}			
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame			: true,
		width: 450,		
		autoHeight	: true,
		title: 'Parametrizaci�n de Curvas',				
		layout		: 'form',
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		monitorValid: true,
		buttons: [		
			{
				text: 'Agregar',
				id: 'btnAgregar',
				iconCls: 'icoAgregar',
				formBind: true,	
				handler: function(boton, evento){
					var nombre = Ext.getCmp('txtNombreArchivo');
					var moneda = Ext.getCmp('cmbClaveMoneda');
					if(Ext.isEmpty(nombre.getValue())){					
						nombre.markInvalid('Introduzca un nombre de archivo.');
						return;	
					}
					if(Ext.isEmpty(moneda.getValue())){					
						moneda.markInvalid('Seleccione un tipo de moneda.');
						return;	
					}
					Ext.getCmp('btnAgregar').disable();
					iniModifica();
					Ext.Ajax.request({
						url: '22forma8Ext01.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
										informacion : 'Agregar'
						}),
						callback: procesarSuccessFailureInsertar
					});					
				}
			},
			{
				text: 'Guardar',
				id: 'btnGuardar',
				hidden:true,
				iconCls: 'icoAgregar',
				formBind: false,
				handler: function(boton, evento){
					var nombre = Ext.getCmp('txtNombreArchivo');
					var moneda = Ext.getCmp('cmbClaveMoneda');
					if(Ext.isEmpty(nombre.getValue())){					
						nombre.markInvalid('Introduzca un nombre de archivo.');
						return;	
					}
					if(Ext.isEmpty(moneda.getValue())){					
						moneda.markInvalid('Seleccione un tipo de moneda.');
						return;	
					}
					Ext.Msg.show({
						title: 'Confirmar',
						msg: '�Desea modificar el registro?',
						buttons: Ext.Msg.OKCANCEL,
						fn: function peticionAjax(btn){
							if (btn == 'ok'){
								Ext.getCmp('btnGuardar').disable();
								Ext.Ajax.request({
									url: '22forma8Ext01.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
										informacion : 'Modificar',
										idCurva :idCurvaM,
										tipo:tipo,
										calculo:calculo,
										base:base,
										moneda:moneda,
										def_SWAP:def_SWAP,
										default_c:default_c
									}),
									callback: procesarSuccessFailureModificar
								});
							}
						}
					});
				}
			}
		]
	});
	
	var grid = new Ext.grid.GridPanel({
		id: 'grid',
		store: consultaData,
		title:'Moneda Nacional',
		height: 300,
		width: 510,
		hidden: false,
		frame: true,
		stripeRows : true,
		margins: '20 0 0 0',
		align: 'center',
		style: 'margin:0 auto',
		columns: [
			{
				header: 'Nombre del Archivo',
				tooltip: 'Nombre del Archivo',
				width: 150,
				dataIndex: 'NOMBRE',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header:'Fondeo',
				dataIndex : 'CS_FONDEO',
				width : 50,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){	
					if(record.data['CS_FONDEO'] == 'S'){
						return '<input  id="chkFondeo"  type="checkbox" checked  onclick="validaFondeo(this, '+rowIndex +','+colIndex+');" />';
					}else{
						return '<input  id="chkFondeo" type="checkbox"  onclick="validaFondeo(this, '+rowIndex +','+colIndex+');" />';
					}			
				}
			},
			{
				header:'Default',
				dataIndex : 'CG_CURVA_IF',
				width : 50,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){		
					if(record.data['CG_CURVA_IF'] == 'S'){
						return '<input  id="chkCurva"  type="checkbox" checked  onclick="validaDefault(this, '+rowIndex +','+colIndex+');" />';
					}else{
						return '<input  id="chkCurva" type="checkbox"  onclick="validaDefault(this, '+rowIndex +','+colIndex+');" />';
					}							
				}
			},
			{
				header:'Swap',
				dataIndex : 'CS_SWAP',
				width : 50,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['IC_MONEDA']!=54){
						return '<input  id="chkSwap" type="checkbox" disabled="true" />';
					}
				}
			},
			{
				header:'Dep�sito',
				dataIndex : 'CS_DEPOSITO',
				width : 100,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['CS_DEPOSITO'] == 'S'){
						return '<input  id="chkDeposito"  type="checkbox" checked  onclick="validaDepositos(this, '+rowIndex +','+colIndex+');" />';
					}else{
						return '<input  id="chkDeposito" type="checkbox"  onclick="validaDepositos(this, '+rowIndex +','+colIndex+');" />';
					}					
				}
			},
			{
				xtype: 'actioncolumn',
				header: 'Seleccionar',
				width : 100,
				tooltip: 'Seleccionar',
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Modificar';
							return 'icoModificar';										
						},
						handler: Modificar
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[1].tooltip = 'Eliminar';
							return 'cancelar';										
						},
						handler: eliminar
					}
				]
			}
		]
	});
	
	var gridDolar = new Ext.grid.GridPanel({
		id: 'gridDolar',
		store: consultaDataDolar,
		title:'D�lar Americano',
		height: 300,
		width: 510,
		hidden: false,
		frame: true,
		stripeRows : true,
		margins: '20 0 0 0',   
		align: 'center',
		style: 'margin:0 auto',
		columns: [
			{
				header: 'Nombre del Archivo',
				tooltip: 'Nombre del Archivo',
				width: 150,
				dataIndex: 'NOMBRE',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header:'Fondeo',
				dataIndex : 'CS_FONDEO',
				width : 50,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){			
					if(record.data['CS_FONDEO'] == 'S'){
						return '<input  id="chkFondeo"  type="checkbox" checked  onclick="validaFondeoDolar(this, '+rowIndex +','+colIndex+');" />';
					}else{
						return '<input  id="chkFondeo" type="checkbox"  onclick="validaFondeoDolar(this, '+rowIndex +','+colIndex+');" />';
					}		
				}						
			},
			{
				header:'Default',
				dataIndex : 'CG_CURVA_IF',
				width : 50,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['CG_CURVA_IF'] == 'S'){
						return '<input  id="chkCurva"  type="checkbox" checked  onclick="validaDefaultDolar(this, '+rowIndex +','+colIndex+');" />';
					}else{
						return '<input  id="chkCurva" type="checkbox"  onclick="validaDefaultDolar(this, '+rowIndex +','+colIndex+');" />';
					}						
				}						
			},
			{
				header:'Swap',
				dataIndex : 'CS_SWAP',
				width : 50,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['IC_MONEDA']!=54){
						return '<input  id="chkSwap" type="checkbox" disabled="true" />';
					}else{
						if(record.data['CS_SWAP'] == 'S'){
							return '<input  id="chkSwap"  type="checkbox" checked  onclick="validaSWAPDolar(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkSwap" type="checkbox"  onclick="validaSWAPDolar(this, '+rowIndex +','+colIndex+');" />';
						}
					}						
				}						
			},
			{
				header:'Dep�sito',
				dataIndex : 'CS_DEPOSITO',
				width : 100,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				
					if(record.data['CS_DEPOSITO'] == 'S'){
						return '<input  id="chkDeposito"  type="checkbox" checked  onclick="validaDepositosDolar(this, '+rowIndex +','+colIndex+');" />';
					}else{
						return '<input  id="chkDeposito" type="checkbox"  onclick="validaDepositosDolar(this, '+rowIndex +','+colIndex+');" />';
					}						
				}						
			},
			{
				xtype: 'actioncolumn',
				header: 'Seleccionar',
				width : 100,
				tooltip: 'Seleccionar',
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Modificar';
							return 'icoModificar';										
						},
						handler: Modificar
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[1].tooltip = 'Eliminar';
							return 'cancelar';										
						},
						handler: eliminarDolar
					}
				]
			}
		]
	});
//-------------------------COMPONENTE PRINCIPAL------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 900,
		height: 'auto',
		items:[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10),
			gridDolar
		]
	});
	catalogoMoneda.load();
	Ext.getCmp('grid').el.mask('Cargando...','x-mask-loading');
	consultaData.load({
		params: Ext.apply(fp.getForm().getValues(),{							
			tipoMoneda: '1'
		})
	});
	Ext.getCmp('gridDolar').el.mask('Cargando...','x-mask-loading');
	consultaDataDolar.load({
		params: Ext.apply(fp.getForm().getValues(),{							
			tipoMoneda: '54'
		})
	});
});