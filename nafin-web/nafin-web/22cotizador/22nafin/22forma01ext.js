Ext.onReady(function(){

	/********** Variables globales **********/
	var recordType = Ext.data.Record.create([ {name: 'clave'}, {name: 'descripcion'} ]);

	/********** Proceso para llenar el cat�logo Ejecutivo **********/
	var procesarCatalogoEjecutivo = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var ejecutivo = Ext.getCmp('ejecutivo_id');
				Ext.getCmp('ejecutivo_id').setValue('');
				if(ejecutivo.getValue() == ''){
					var newRecord = new recordType({
						clave:'',
						descripcion:'Seleccione...'
					});
					store.insert(0,newRecord);
					store.commitChanges();
					ejecutivo.setValue('');
				}
			}
		}
	};

	/********** Proceso para llenar el cat�logo Area **********/
	var procesarCatalogoArea = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var area = Ext.getCmp('area_id');
				Ext.getCmp('area_id').setValue('');
				if(area.getValue() == ''){
					var newRecord = new recordType({
						clave:'',
						descripcion:'Seleccione un �rea...'
					});
					store.insert(0,newRecord);
					store.commitChanges();
					area.setValue('');
				}
			}
		}
	};

	/********** Obtiene los datos del ejecutivo seleccionado **********/
	function buscarDatos(){
		var fp = Ext.getCmp('formaPrincipal');
		fp.el.mask('Cargando datos...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '22forma01ext.data.jsp',
			params: Ext.apply({
				informacion: 'Buscar_Datos',
				ejecutivo:   Ext.getCmp('ejecutivo_id').getValue()
			}),
			callback: procesarBuscarDatos
		});
	}

	/********** Carga los datos en el form **********/
	function procesarBuscarDatos(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var fp = Ext.getCmp('formaPrincipal');
			fp.el.unmask();
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				if(jsonData.mensaje == ''){
				
					/***** Lleno los campos *****/
					Ext.getCmp('ic_ejecutivo_id').setValue(jsonData.ic_ejecutivo);
					Ext.getCmp('ic_usuario_id').setValue(jsonData.ic_usuario);
					Ext.getCmp('areaAnterior_id').setValue(jsonData.areaAnterior);
					Ext.getCmp('antesDirector_id').setValue(jsonData.antesDirector);
					Ext.getCmp('nombre_id').setValue(jsonData.nombre);
					Ext.getCmp('paterno_id').setValue(jsonData.paterno);
					Ext.getCmp('materno_id').setValue(jsonData.materno);
					Ext.getCmp('email_id').setValue(jsonData.email);
					Ext.getCmp('telefono_id').setValue(jsonData.telefono);
					Ext.getCmp('fax_id').setValue(jsonData.fax);
					Ext.getCmp('area_id').setValue(jsonData.area);
					Ext.getCmp('director_area_id').setValue(jsonData.director_area);
					/***** Muestro los componentes *****/
					Ext.getCmp('nombre_id').show();
					Ext.getCmp('paterno_id').show();
					Ext.getCmp('materno_id').show();
					Ext.getCmp('email_id').show();
					Ext.getCmp('telefono_id').show();
					Ext.getCmp('fax_id').show();
					Ext.getCmp('area_id').show();
					Ext.getCmp('director_area_id').show();
					Ext.getCmp('btnGuardar').show();
					Ext.getCmp('btnCancelar').show();
					
				} else{
					Ext.Msg.alert('Error...', jsonData.mensaje);
				}
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	/********** Valida los campos y guarda los datos **********/
	function procesarGuardar(){
		if(Ext.getCmp('area_id').getValue() == ''){
			Ext.getCmp('area_id').markInvalid('Debe seleccionar un �rea.');
			return;
		} else{

			if (Ext.getCmp('formaPrincipal').getForm().isValid()){
				Ext.Msg.confirm('', '�Confirma la modificaci�n de los datos del usuario?',function(botonConf){
					if (botonConf == 'ok' || botonConf == 'yes'){
						var fp = Ext.getCmp('formaPrincipal');
						fp.el.mask('Procesando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '22forma01ext.data.jsp',
							params: Ext.apply(formaPrincipal.getForm().getValues(),{
								areaId: Ext.getCmp('area_id').getValue(),
								informacion: 'Guardar_Cambios'
							}),
							callback: procesarGuardarCambios
						});
					}
				});
			} else{
				Ext.Msg.alert('Error...', 'Uno o m�s campos de captura tienen errores.');
			}

		}

	}

	/********** Muestra el resultado despues de guardar los datos **********/
	function procesarGuardarCambios(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('formaPrincipal');
			fp.el.unmask();
			if (jsonData != null){
				if(jsonData.accion == 'exito'){
					Ext.getCmp('formaNotificacion').show();
					Ext.getCmp('formaPrincipal').hide();
					if(jsonData.mensaje != ''){
						Ext.Msg.alert('Mensaje...', jsonData.mensaje);
					}
				} else{
					if(jsonData.mensaje != ''){
						Ext.Msg.alert('Error...', jsonData.mensaje);
					}
				}
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	/********** Limpia el form **********/
	function procesarCancelar(){
		Ext.getCmp('formaNotificacion').hide();
		Ext.getCmp('formaPrincipal').getForm().reset();
		Ext.getCmp('formaPrincipal').show();
		Ext.getCmp('nombre_id').hide();
		Ext.getCmp('paterno_id').hide();
		Ext.getCmp('materno_id').hide();
		Ext.getCmp('email_id').hide();
		Ext.getCmp('telefono_id').hide();
		Ext.getCmp('fax_id').hide();
		Ext.getCmp('area_id').hide();
		Ext.getCmp('director_area_id').hide();
		Ext.getCmp('btnGuardar').hide();
		Ext.getCmp('btnCancelar').hide();
		catalogoEjecutivo.load();
	}

/********** Se crea el store para el combo Ejecutivo **********/
	var catalogoEjecutivo = new Ext.data.JsonStore({
		id:             'catalogoEjecutivo',
		root:           'registros',
		fields:         ['clave', 'descripcion', 'loadMsg'],
		url:            '22forma01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEjecutivo'
		},
		totalProperty:  'total',
		autoLoad:       true,
		listeners: {
			load:        procesarCatalogoEjecutivo,
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/********** Se crea el store para el combo Area **********/
	var catalogoArea = new Ext.data.JsonStore({
		id:             'catalogoArea',
		root:           'registros',
		fields:         ['clave', 'descripcion', 'loadMsg'],
		url:            '22forma01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoArea'
		},
		totalProperty:  'total',
		autoLoad:       true,
		listeners: {
			load:        procesarCatalogoArea,
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/********** Elementos de la forma principal **********/
	var elementosForma = [
	{ width: 300, xtype: 'textfield', id: 'ic_ejecutivo_id',  name: 'ic_ejecutivo',  fieldLabel: 'ic_ejecutivo',  hidden: true },
	{ width: 300, xtype: 'textfield', id: 'ic_usuario_id',    name: 'ic_usuario',    fieldLabel: 'ic_usuario',    hidden: true },
	{ width: 300, xtype: 'textfield', id: 'areaAnterior_id',  name: 'areaAnterior',  fieldLabel: 'areaAnterior',  hidden: true },
	{ width: 300, xtype: 'textfield', id: 'antesDirector_id', name: 'antesDirector', fieldLabel: 'antesDirector', hidden: true },
	{
		width:          400,
		xtype:          'combo',
		id:             'ejecutivo_id',
		name:           'ejecutivo',
		fieldLabel:     '&nbsp; * Ejecutivo',
		msgTarget:      'side',
		mode:           'local',
		emptyText:      'Seleccione ... ',
		displayField:   'descripcion',
		valueField:     'clave',
		triggerAction:  'all',
		forceSelection: true,
		typeAhead:      true,
		allowBlank:     false,
		store:          catalogoEjecutivo,
		listeners: {
			select: {
				fn: function(combo){
					if(combo.getValue() != ''){ buscarDatos(); }
				}
			}
		}
	},{
		width:          400,
		xtype:          'textfield',
		id:             'nombre_id',
		name:           'nombre',
		fieldLabel:     '&nbsp; * Nombre',
		msgTarget:      'side',
		maxLength:      40,
		allowBlank:     false,
		hidden:         true
	},{
		width:          400,
		xtype:          'textfield',
		id:             'paterno_id',
		name:           'paterno',
		fieldLabel:     '&nbsp; * Apellido Paterno',
		msgTarget:      'side',
		maxLength:      40,
		allowBlank:     false,
		hidden:         true
	},{
		width:          400,
		xtype:          'textfield',
		id:             'materno_id',
		name:           'materno',
		fieldLabel:     '&nbsp; * Apellido Materno',
		msgTarget:      'side',
		maxLength:      40,
		allowBlank:     false,
		hidden:         true
	},{
		width:          400,
		xtype:          'textfield',
		id:             'email_id',
		name:           'email',
		fieldLabel:     '&nbsp; * Email',
		msgTarget:      'side',
		allowBlank:     false,
		hidden:         true,
		regex:          /^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z]{2,3})$/,
		regexText:      'Este campo debe ser una direcci�n de correo electr�nico'
	},{
		width:          400,
		xtype:          'numberfield',
		id:             'telefono_id',
		name:           'telefono',
		fieldLabel:     '&nbsp; * Tel�fono',
		msgTarget:      'side',
		maxLength:      8,
		allowBlank:     false,
		hidden:         true
	},{
		width:          400,
		xtype:          'numberfield',
		id:             'fax_id',
		name:           'fax',
		fieldLabel:     '&nbsp; * Fax',
		msgTarget:      'side',
		maxLength:      8,
		allowBlank:     false,
		hidden:         true
	},{
		width:          400,
		xtype:          'combo',
		id:             'area_id',
		name:           'area',
		fieldLabel:     '&nbsp; * �rea',
		msgTarget:      'side',
		mode:           'local',
		emptyText:      'Seleccione ...',
		displayField:   'descripcion',
		valueField:     'clave',
		triggerAction:  'all',
		forceSelection: true,
		typeAhead:      true,
		allowBlank:     false,
		hidden:         true,
		store:          catalogoArea
	},{
		width:          400,
		xtype:          'checkbox',
		id:             'director_area_id',
		name:           'director_area',
		boxLabel:       'Director de �rea',
		hidden:         true
	}];

	/********** Form Panel Notificaciones **********/
	var formaNotificacion = new Ext.form.FormPanel({
		width:          515,
		id:             'formaNotificacion',
		title:          'Mantenimiento al Ejecutivo',
		layout:         'form',
		style:          'margin:0 auto;',
		frame:          true,
		autoHeight:     true,
		hidden:         true,
		items: [{
			width:       500,
			xtype:       'panel',
			layout:      'table',
			border:      true,
			layoutConfig:{
				columns:  1
			},
			defaults: {
				align:    'center',
				bodyStyle:'padding: 2px,'
			},
			items: [{
				width:    500,
				id:       'operacion_exitosa_id',
				html:     '<div class="formas" align="center"><br><b> El usuario ha sido modificado <br>&nbsp;</div>',
				frame:    true,
				border:   false
			}]
		}],
		buttons: [{
			text:        'Regresar',
			id:          'btnRegresar',
			iconCls:     'icoRegresar',
			handler:     procesarCancelar
		}]
	});

	/********** Form Panel Principal **********/
	var formaPrincipal = new Ext.form.FormPanel({
		width:          600,
		labelWidth:     120,
		id:             'formaPrincipal',
		title:          'Mantenimiento al Ejecutivo',
		layout:         'form',
		style:          'margin:0 auto;',
		monitorValid:   true,
		frame:          true,
		autoHeight:     true,
		items:          elementosForma,
		buttons: [{
			text:        'Guardar',
			id:          'btnGuardar',
			iconCls:     'icoAceptar',
			hidden:      true,
			handler:     procesarGuardar
		},{
			text:        'Cancelar',
			id:          'btnCancelar',
			iconCls:     'icoCancelar',
			hidden:      true,
			handler:     procesarCancelar
		}]
	});

	/********** Contenedor Principal **********/
	var pnl = new Ext.Container({
		width:          949,
		id:             'contenedorPrincipal',
		applyTo:        'areaContenido',
		style:          'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			formaNotificacion
		]
	});

});