Ext.onReady(function() {
//-----------------------------procesarConsultaData-----------------------------
	function procesarEnviarDatos(opts, success, response) {
		Ext.getCmp('btnEnviar').enable();
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			var mensaje = jsonData.mensaje;
			if(mensaje!=""){
				Ext.MessageBox.alert("Mensaje",mensaje);
				Ext.getCmp('forma').getForm().reset();
			}else{
				var estatus = jsonData.txt_estatus;
				var auxFecha = "";
				var tipooper = jsonData.tipo_operacion;
				var strTipoUsuario = jsonData.strTipoUsuario;
				var riesgo_intermediero = jsonData.riesgo_intermediero;
				var nombre_acreditado = jsonData.nombre_acreditado;
				var nombre_programa= jsonData.nombre_programa;
				var numero_disposicion =jsonData.numero_disposicion;
				var ic_recuperacion_capital = jsonData.ic_recuperacion_capital;
				var primer_pago_capital =jsonData.primer_pago_capital;
				var moneda =jsonData.moneda;
				var ig_tasa_sport =jsonData.ig_tasa_sport;
				var tipo_cotizacion = jsonData.tipo_cotizacion;
				var cs_vobo = jsonData.cs_vobo;
				var cs_aceptado = jsonData.cs_aceptado_md;
				if(moneda == "1"){
					moneda = "Pesos";
				}else{
					moneda = "D�lares";
				}
				Ext.getCmp('numSol').body.update('<div align="left">'+jsonData.num_solicitud+'</div>');
				Ext.getCmp('fecsol').body.update('<div align="left">'+jsonData.fecha_solicitud+'</div>');
				Ext.getCmp('horaSol').body.update('<div align="left">'+jsonData.hora_solocitud+'</div>');
				if(estatus!="1"&&estatus!="3"&&estatus!="4"){
					auxFecha = "Cotizaci&oacute;n";
					if(estatus=="8"){
						auxFecha = "Cancelaci&oacute;n";
					}
				}
				Ext.getCmp('fechAux1').body.update('<div align="left">'+jsonData.fecha_citiza_aux+auxFecha+'</div>');
				Ext.getCmp('fechAux2').body.update('<div align="left">'+jsonData.fecha_citiza+'</div>');
				Ext.getCmp('horaAux1').body.update('<div align="left">'+jsonData.hora_cotiza_aux+auxFecha+'</div>');
				Ext.getCmp('horaAux2').body.update('<div align="left">'+jsonData.hora_cotiza+'</div>');
				Ext.getCmp('nomUsuario').body.update('<div align="left">'+jsonData.nombre_usuario+'</div>');
				if(estatus=="7"||estatus=="8"||estatus=="9"){
					Ext.getCmp('numRecibo1').show();
					Ext.getCmp('numRecibo').show();
					Ext.getCmp('numRecibo').body.update('<div align="center">'+jsonData.numero_recibo+'</div>');
					Ext.getCmp('mensaje_cotiza').show();
				}

				//Datos del Ejecutivo Nafin
				Ext.getCmp('tfNombreE').setValue(jsonData.nombre_ejecutivo);
				Ext.getCmp('tfCorreo').setValue(jsonData.correo);
				Ext.getCmp('tfTelefono').setValue(jsonData.telefono);
				Ext.getCmp('tfFax').setValue(jsonData.fax);
				//Datos del intermediario Financiero
				Ext.getCmp('tfTipoOperacion').setValue(jsonData.tipo_operacion);
				if(tipooper!="Primer Piso"){
					Ext.getCmp('tfNombreInter').show();
					Ext.getCmp('tfNombreInter').setValue(jsonData.nombre_intermediario);
					if(strTipoUsuario!="IF"&&riesgo_intermediero!=""){
						Ext.getCmp('tfRiesgoInter').show();
						Ext.getCmp('tfRiesgoInter').setValue(jsonData.riesgo_intermediero);
					}
				}
				if(strTipoUsuario!="IF"&&nombre_acreditado!=""){
					Ext.getCmp('tfNombreAcred').show();
					Ext.getCmp('tfNombreAcred').setValue(jsonData.nombre_acreditado);
					if(tipooper=="Primer Piso"){
						Ext.getCmp('tfRiesgoCredito').show();
						Ext.getCmp('tfRiesgoCredito').setValue(jsonData.riesgo_acreditado);
					}
				}
				//Caracteristicas de la operaci�n 
				if(nombre_programa!=""){
					Ext.getCmp('tfNombrePrograma').show();
					Ext.getCmp('tfNombrePrograma').setValue(jsonData.nombre_programa);
				}
				Ext.getCmp('tfMontoTotal').setValue(jsonData.monto_total+" "+moneda);
				Ext.getCmp('tfTasaRequerida').setValue(jsonData.tipo_tasa_requerida);
				Ext.getCmp('tfNumDisposicion').setValue(jsonData.numero_disposicion);//
				if(numero_disposicion >1){
					Ext.getCmp('tfDispMultiple').show();
					Ext.getCmp('tfDispMultiple').setValue(jsonData.disposicion_multiple);
				}
				Ext.getCmp('tfplazoOper').setValue(jsonData.plazo_operacion);
				Ext.getCmp('tfRecupCapital').setValue(jsonData.recuperacion_capital);
				if(ic_recuperacion_capital!="1"){
					if(ic_recuperacion_capital == "2" ||ic_recuperacion_capital == "3"  ){
						Ext.getCmp('tfPagInteres').show();
						Ext.getCmp('tfPagInteres').setValue(jsonData.perioricidad_pago);
					}
					if(ic_recuperacion_capital!="3"){
						if(ic_recuperacion_capital == "2" ||ic_recuperacion_capital == "4"  ){
							Ext.getCmp('tfPerCapital').show();
							Ext.getCmp('tfPerCapital').setValue(jsonData.perioricidad_capital);
						}
					}
					if(ic_recuperacion_capital == "2" && primer_pago_capital != ""  ){
						Ext.getCmp('tfPrimerFecPago').show();
						Ext.getCmp('tfPenultimafecPago').show();
						Ext.getCmp('tfPrimerFecPago').setValue(jsonData.primer_pago_capital);
						Ext.getCmp('tfPenultimafecPago').setValue(jsonData.penultimo_pago_capital);
					}
				}
				var gridDetalle = Ext.getCmp('gridDetalle');
				storeDetalle.loadData('');
				if (!gridDetalle.isVisible()){
						gridDetalle.show();
				}
				if (jsonData.gridDisposicion != undefined && jsonData.gridDisposicion.length > 0){
					storeDetalle.loadData(jsonData.gridDisposicion);
					gridDetalle.getGridEl().unmask();
				}else{
					gridDetalle.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				}
				// plan de pago Capital
				Ext.getCmp('gridPlanPago').hide();
				Ext.getCmp('panel4').hide();
				Ext.getCmp('panel5').hide();
				Ext.getCmp('panel6').hide();
				if(jsonData.pagos_tam > 0){
					var gridPlanPago = Ext.getCmp('gridPlanPago');
					storePlanPago.loadData('');
					if (!gridPlanPago.isVisible()){
						gridPlanPago.show();
					}
					if (jsonData.gridPlanPagoCapital != undefined && jsonData.gridPlanPagoCapital.length > 0){
						storePlanPago.loadData(jsonData.gridPlanPagoCapital);
						gridPlanPago.getGridEl().unmask();
					}else{
						gridPlanPago.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					}
				}
				// tasa indicativa 
				if(estatus =="2" ||  estatus =="5"){
					Ext.getCmp('panel4').show();
					if(tipo_cotizacion == "I"&&strTipoUsuario =="IF"){
						Ext.getCmp('menAlerta').show();
						Ext.getCmp('menAlerta').body.update('<div align="center">'+jsonData.mansajeAlert+'</div>');
					}else if(cs_vobo == "N"&&strTipoUsuario =="IF"){
						Ext.getCmp('menAlerta').show();
						Ext.getCmp('menAlerta').body.update('<div align="center">'+jsonData.mansajeAlert+'</div>');
					}
					Ext.getCmp('dfTasaIndicativa').setValue(jsonData.tasa_ind_oferta_mismo_dia);
					if(ig_tasa_sport != "0.00"){
						Ext.getCmp('dfTasaindiCuar').show();
						Ext.getCmp('dfTasaindiCuar').setValue(jsonData.tasaIndCuarOcho);
						Ext.getCmp('mensaTasaCua').show();
					}
				
				}
				// tasa confirmacion 
				if(estatus =="7" ||  estatus =="8"||  estatus =="9"){
					Ext.getCmp('panel5').show();
					Ext.getCmp('fechaHora').body.update('<div align="center">'+jsonData.fechaHoras+'</div>');
					Ext.getCmp('cs_aceptado').body.update('<div align="center">'+jsonData.r_tasa_confirmada+'</div>');
					Ext.getCmp('fechaHoraCotiza').body.update('<div align="center">'+jsonData.df_cotiza+'</div>');

				}
				Ext.getCmp('panel6').show();
				Ext.getCmp('tfOnservaciones').setValue(jsonData.txt_obsr);
				Ext.getCmp('txtClave').setValue(jsonData.txt_clave);
				
				Ext.getCmp('formaCotiza').show();
				Ext.getCmp('formaDatos').show();
				Ext.getCmp('forma').hide();
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarCancelacion(opts, success, response) {
		Ext.getCmp('btnCancelacion').enable();
		var fp = Ext.getCmp('formaDatos');
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			var mensaje = jsonData.mensaje;	
			if(mensaje!=""){
				Ext.MessageBox.alert(mensaje);
			}else{
				Ext.getCmp('textoTitulo').show();
				if(jsonData.txt_origen!="NB"){
					Ext.getCmp('tfOnservaciones').hide();
					Ext.getCmp('obsConfirmar').body.update('<div align="left">'+jsonData.observaciones+'</div>');
					Ext.getCmp('btnCancelacion').hide();
				}
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var elementosForma = [
		{
			xtype:      'textfield',
			id:         'txfNumSolicitud',
			name:       'numSolicitud',
			hiddenName: 'numSolicitud',
			fieldLabel: 'N�mero de Solicitud',
			maxLength:  10,
			anchor:     '76%',
			margins:    '0 20 0 0'
		}
	];
	var storeDetalle = new Ext.data.JsonStore({
		fields: [
			{name: 'NUMERO_DISPOSICION'},
			{name: 'MONTO'},
			{name: 'FECHA_DISPOSICION'},
			{name: 'FECHA_VENCIMIENTO'}
		],
		data:[
			{'NUMERO_DISPOSICION':'', 'MONTO':'', 'FECHA_DISPOSICION':'', 'FECHA_VENCIMIENTO':''}
		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var storePlanPago = new Ext.data.JsonStore({
		fields: [
			{name: 'NUMERO_PAGO'},
			{name: 'MONTO_PAGO'},
			{name: 'FECHA_PAGO'}
		],
		data:[
			{'NUMERO_PAGO':'', 'MONTO_PAGO':'', 'FECHA_PAGO':''}
		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
//--------------------------------gridConsulta----------------------------------	
	var gridDetalle = new Ext.grid.GridPanel ({
		title:           'Detalle de Disposiciones',
		store:           storeDetalle,
		style:           'margin:0 auto;',
		hidden:          false,
		stripeRows:      true,
		id:              'gridDetalle',
		loadMask:        true,
		height:          150,
		width:           550,
		frame:           true,
		columns: [
			{
				header:    'N&uacute;mero de Disposici&oacute;n',
				tooltip:   'N&uacute;mero de Disposici&oacute;n',
				dataIndex: 'NUMERO_DISPOSICION',
				sortable:  true, 
				width:     95,
				resizable: true,
				align:     'center'
			},{
				header:    'Monto',
				tooltip:   'Monto',
				dataIndex: 'MONTO',
				sortable:  true,
				width:     150,
				resizable: true,
				hidden:    false,
				align:     'right'
			},{
				header :   'Fecha de Disposici&oacute;n', 
				tooltip:   'Fecha de Disposici&oacute;n',	
				dataIndex: 'FECHA_DISPOSICION',
				sortable:  true,
				width:     150, 
				renderer:  Ext.util.Format.dateRenderer('d/m/Y'),
				align:     'center'
			},{
				header:    'Fecha de Vencimiento',
				tooltip:   'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable:  true,
				width:     150, 
				align:     'center', 
				renderer:  Ext.util.Format.dateRenderer('d/m/Y'),
				hidden:    false
			}
		]
	});

	var gridPlanPago = new Ext.grid.GridPanel({
		title:           'Plan de Pagos Capital',
		store:           storePlanPago,
		hidden:          true,
		id:              'gridPlanPago',
		stripeRows:      true,
		loadMask:        true,
		height:          150,
		width:           550,
		frame:           true,
		style:           'margin:0 auto;',
		columns: [
			{
				header:    '<center>N&uacute;mero de Pago</center>',
				tooltip:   'N&uacute;mero de Pago',
				dataIndex: 'NUMERO_PAGO',
				sortable:  true, 
				width:     95,
				resizable: true,
				align:     'center'
			},{
				header:    '<center>Fecha de Pago</center>', 
				tooltip:   'Fecha de Pago',	
				dataIndex: 'FECHA_PAGO',
				sortable:  true,
				width:     150, 
				renderer:  Ext.util.Format.dateRenderer('d/m/Y'),
				align:     'center'
			},{
				header:    '<center>Monto a Pagar</center>',
				tooltip:   'Monto a Pagar',
				dataIndex: 'MONTO_PAGO',
				sortable:  true,
				width:     200,
				resizable: true,
				hidden:    false,
				align:     'right'
			}
		]
	});
//-----------------------------Fin gridConsulta---------------------------------
	var ejecutivoNacional = {
		xtype		: 'panel',
		height: 'auto',
		labelWidth	: 300,
		id 		: 'ejecutivoNacional',
		items		: [{
			layout		: 'form',
			labelWidth	: 250,
			bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:2px;',
			items		:	
			[
				{
					xtype:         'displayfield',
					name:          'nombreE',
					fieldLabel: 	'Nombre del Ejecutivo ',
               id:        'tfNombreE',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'correo',
					fieldLabel: 	'Correo electr�nico ',
               id:        'tfCorreo',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'Tel�fono',
					fieldLabel: 	'Tel�fono ',
               id:        'tfTelefono',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'Fax',
					fieldLabel: 	'Fax ',
               id:        'tfFax',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				}
				
			]
		}]
	};
	var intermediarioFinanciero = {
		xtype		: 'panel',
		height: 'auto',
		labelWidth	: 300,
		id 		: 'intermediarioFinanciero',
		items		: [{
			layout		: 'form',
			labelWidth	: 250,
			bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:2px;',
			items		:	
			[
				{
					xtype:         'displayfield',
					name:          'tipoOperacion',
					fieldLabel: 	'Tipo de operaci�n ',
               id:        'tfTipoOperacion',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'nombreInter',
					fieldLabel: 	'Nombre del Intermediario ',
               id:        'tfNombreInter',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					hidden:true,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'riesgoInter',
					fieldLabel: 	'Riesgo del Intermediario ',
               id:        'tfRiesgoInter',
					hidden:true,
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'nombreAcred',
					fieldLabel: 	'Nombre del Acreditado ',
               id:        'tfNombreAcred',
					hidden:true,
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'riesgoAcred',
					fieldLabel: 	'Riesgo del Acreditado',
               id:        'tfRiesgoCredito',
					hidden:true,
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				}
				
			]
		}]
	};
	
	var caracteristicaOpera = {
		xtype		: 'panel',
		height: 'auto',
		labelWidth	: 300,
		id 		: 'caracteristicaOpera',
		items		: [{
			layout		: 'form',
			labelWidth	: 250,
			bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:2px;',
			items		:	
			[
				{
					xtype:         'displayfield',
					name:          'nombrePrograma',
					fieldLabel: 	'Nombre del programa o acreditado Final ',
               id:        'tfNombrePrograma',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
					hidden:true,
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'montoTotal',
					fieldLabel: 	'Monto total requerido ',
               id:        'tfMontoTotal',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'tasaRequerida',
					fieldLabel: 	'Tipo de tasa requerida ',
               id:        'tfTasaRequerida',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'numDisposicion',
					fieldLabel: 	'N�mero de Disposiciones ',
               id:        'tfNumDisposicion',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'dispMultiple',
					fieldLabel: 	'Disposiciones m&uacute;ltiples.&nbsp;Tasa de Inter&eacute;s',
               id:        'tfDispMultiple',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
					hidden:true,
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'plazoOper',
					fieldLabel: 	'Plazo de la operaci&oacute;n',
               id:        'tfplazoOper',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'recupCapital',
					fieldLabel: 	'Esquema de recuperaci&oacute;n del capital',
               id:        'tfRecupCapital',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'pagInteres',
					fieldLabel: 	'Periodicidad del pago de Inter&eacute;s',
               id:        'tfPagInteres',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
					hidden:true,
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'pagInteres',
					fieldLabel: 	'Periodicidad de Capital',
               id:        'tfPerCapital',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
					hidden:true,
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'primerFecPago',
					fieldLabel: 	'Primer fecha de pago de Capital',
               id:        'tfPrimerFecPago',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					hidden:true,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'penultimafecPago',
					fieldLabel: 	'Pen&uacute;ltima fecha de pago de Capital',
               id:        'tfPenultimafecPago',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					hidden:true,
					mode: 'local',
               submitValue:   true
				}
				
			]
		}]
	};
	var tasaIndicativa = {
		xtype		: 'panel',
		height: 'auto',
		labelWidth	: 300,
		id 		: 'tasaIndicativa',
		items		: [{
			layout		: 'form',
			labelWidth	: 250,
			bodyStyle: 	'padding: 2px; padding-right:0px;padding-left:2px;',
			items		:	
			[
				{	id:'menAlerta',
					html: '&nbsp;',
					height: 50,
					hidden:true
				},
				{	border:false,	
					html:'<div align="left"><h2>TASA INDICATIVA</h2></div>' 
				},
				{
					xtype:         'displayfield',
					name:          'tasaIndicativa',
					fieldLabel: 	'Tasa Indicativa con oferta mismo d&iacute;a: ',
               id:        'dfTasaIndicativa',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
               submitValue:   true
				},
				{
					xtype:         'displayfield',
					name:          'tasaindiCuar',
					fieldLabel: 	'Tasa Indicativa con oferta 48 horas ',
               id:        'dfTasaindiCuar',
               fieldStyle:    'font-weight: bold;',
					autoLoad: false,
					mode: 'local',
					hidden : true,
               submitValue:   true
				},
				{	border:false,
					html:'<div align="left"><h2>Nota: </h2><br><h2>Tasa con oferta Mismo D&iacute;a:</h2>&nbsp;El Intermediario Financiero tiene la opci&oacute;n de tomarla en firme el mismo d&iacute;a que haya recibido la cotizaci&oacute;n indicativa a m&aacute;s tardar a las 12:30 horas.</div>' 
				},
				{	border:false,
					html:'<div align="left"><h2>Tasa con oferta 48 Horas:</h2>&nbsp; El Intermediario Financiero tiene la opci&oacute;n de tomarla en firme a m&aacute;s tardar dos d&iacute;as h&aacute;biles despu&eacute;s de que la Tesorer&iacute;a haya proporcionado la cotizaci&oacute;n indicativa antes de las 12:30 horas del segundo d�a h&aacute;bil.<br><br></div>' ,
				   id:'mensaTasaCua',
					hidden :true
				},
				{	border:false,
					html:'<div align="left">Las cotizaciones indicativas proporcionadas por la Tesorer&iacute;a de Nacional Financiera son de uso reservado para el intermediario solicitante y ser&aacute; responsabilidad de dicho intermediario el mal uso de la informaci&oacute;n proporcionada.</div>' 
				}
				
			]
		}]
	};
	var elementosCotiza = [
		{
			xtype: 'panel',
			layout:'table',
			width:550,
			border:true,
			layoutConfig:{ columns: 2 },
			defaults: {frame:false, border: true,width:200, height: 20,bodyStyle:'padding:2px'},
			items:[
				{	border:false,	frame:true, width:400,	colspan:2,	html:'<div align="center"><h2>Datos de la Cotizaci�n:</h2></div>' },
				{	border:true,		html:'<div align="left">N�mero de Solicitud:</div>'	},
				{	id:'numSol',	html: '&nbsp;'	},
				{	border:true,		html:'<div align="left">Fecha de Solicitud:</div>'	},
				{	id:'fecsol',	html: '&nbsp;'	},
				{	border:true,		html:'<div align="left">Hora de Solicitud:</div>'	},
				{	id:'horaSol',	html: '&nbsp;'	},
				{	id:'fechAux1',	html: '&nbsp;'	},
				{	id:'fechAux2',	html: '&nbsp;'	},
				{	id:'horaAux1',	html: '&nbsp;'	},
				{	id:'horaAux2',	html: '&nbsp;'	},
				
				{	border:true,		html:'<div align="left">N&uacute;mero y Nombre de usuario:</div>'	},
				{	id:'nomUsuario',	html: '&nbsp;'	},
				{	hidden:true , id:'numRecibo1', border:true,		html:'<div align="left">N&uacute;mero de Recibo:</div>'	},
				{	hidden:true , id:'numRecibo',	html: '&nbsp;',width:300	},
				{	hidden:true , id:'mensaje_cotiza',width:400,	colspan:2,	height: 100 ,html:'<div align="left">En caso de que la disposici&oacute;n de los recursos no se lleve a cabo total o parcialmente en la fecha estipulada, Nacional Financiera aplicar&aacute; el cobro de una comisi&oacute;n sobre los montos no dispuestos, conforme a las pol&iacute;ticas de cr&eacute;dito aplicables a este tipo de operaciones.</div>' }
			]
		}
	];
	var elementosConfirma = [
		{
			xtype: 'panel',
			layout:'table',
			width:550,
			border:true,
			layoutConfig:{ columns: 2 },
			defaults: {frame:true, border: true,width:270, height: 20,bodyStyle:'padding:2px'},
			items:[
				{	border:true,
					frame:true, 
					width:270,	
					html:'<div align="center"><h2>TASA CONFIRMADA </h2></div>'
				},
				{	id:'fechaHora',
					html: '&nbsp;'
				},
				{	id:'cs_aceptado',
					html: '&nbsp;'
				},
				{	id:'fechaHoraCotiza',
					html: '&nbsp;'
				}
			]
		}
	];
	var observaciones = {
		xtype		: 'panel',
		height: 'auto',
		id 		: 'observaciones',
		items		: [{
			layout		: 'form',
			bodyStyle: 	'padding: 2px; padding-center:0px;padding-center:2px;',
			items		:	
			[
				{
					xtype:         'textarea',
					name:          'observaciones',
					width:300,
					align: 'left',
					height :80,
               id:        'tfOnservaciones'
				},
				{	id:'obsConfirmar',
					html: '&nbsp;'
				},
				{
					xtype	    : 'textfield',
					id        : 'txtClave',
					name		 : 'clave',
					hidden    : true
				}
				
			]
		}]
	};
//-------------------------------Panel Consulta---------------------------------
	var fpDatosCotiza	=	new Ext.FormPanel({
		id:'formaCotiza',
		style: 'margin:0 auto;',	
		autoHeight:true,
		bodyStyle:'padding:2px',	
		width:405,
		border:true,	
		frame:false,	
		items:elementosCotiza,	
		hidden:true
	});
	var fpDatos = new Ext.form.FormPanel({
		id					: 'formaDatos',
		layout			: 'form',
		width				: 600,
		style				: ' margin:0 auto;',
		frame				: true,
		hidden         :true,
		height: 'auto',
		labelWidth:		200,
		bodyStyle		: 'padding: 2px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			{
				layout	: 'fit',
				title		: 'Datos del Ejecutivo Nafin',
				id			: 'panel1',
				align: 'left',
				items		: [ejecutivoNacional ]
			},
			{
				layout	: 'fit',
				id			: 'panel2',
				title 	: 'Datos del Intermediario Financiero/Acreditado',
				align: 'left',
				items		: [intermediarioFinanciero		]
			},
			{
				layout	: 'fit',
				title		: 'Caracter�sticas de la Operaci�n',
				id			: 'panel3',
				align: 'left',
				items		: [caracteristicaOpera ]
			},gridDetalle, gridPlanPago,
			{
				layout	: 'fit',
				id			: 'panel4',
				align: 'left',
				bodyStyle:'padding:2px',
				items		: [tasaIndicativa ]
			},
			{
				layout	: 'fit',
				title		: '',
				id			: 'panel5',
				align: 'left',
				bodyStyle:'padding:10px',
				items		: [elementosConfirma ]
			},
			{
				layout	: 'fit',
				title		: 'Observaciones:',
				id			: 'panel6',
				align: 'left',
				labelWidth:		120,
				bodyStyle:'padding:10px',
				items		: [observaciones ]
			}
			
		],
		monitorValid	: true,
		buttons			: [
			{
				text: 'Confirma Cancelaci�n',
				id: 'btnCancelacion',
				iconCls:'icoCancelar',
				handler: function (boton, evento){
					var obs = Ext.getCmp('tfOnservaciones');
					var clave = Ext.getCmp('txtClave');
					if(Ext.isEmpty(obs.getValue())){
						obs.markInvalid('Debe capturar las causas por el rechazo de la Solicutd Tomada en Firme');
						return;
					}
					boton.disable();
					fpDatos.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '22forma4Ext01.data.jsp',
						params: Ext.apply({	
							informacion:"Confirmar",
							observaciones: obs.getValue(),
							clave: clave.getValue()
						}),
						callback: procesarCancelacion
					});
				}
			} 
		]
	});
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 400,
		title: '<center>Cancelaci�n de Cr�ditos</center>',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		labelWidth: 150,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text : 'Enviar',   
				id   : 'btnEnviar',
				iconCls :'icoContinuar',
				handler: function (boton, evento){
					var ic_solicitud = Ext.getCmp('txfNumSolicitud');
					if(Ext.isEmpty(ic_solicitud.getValue())){
						ic_solicitud.markInvalid('Debe capturar un numero de solicitud');
						return;
					}
					if ( Ext.getCmp('txfNumSolicitud').isValid()) {
						boton.disable();
						fp.el.mask('Enviando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '22forma4Ext01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{	
								informacion:"enviar",
								numSolicitud: ic_solicitud.getValue()
							}),
							callback: procesarEnviarDatos
						});
					}else {
								Ext.getCmp('txfNumSolicitud').focus();
					}
				}
			}
		]
	});
//------------------------------Fin Panel Consulta------------------------------
	var elementoTitulo = new Ext.Container({
		layout: 'table',		
		id: 'textoTitulo',							
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',
		aling:'center',
		hidden: true,
		style: 'margin:0 auto;',
		items: [
			{ 	xtype: 'displayfield',  id: 'mensaje', 	value: 'COTIZACION CANCELADA'}				
		]
	});
//----------------------------Contenedor Principal------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			elementoTitulo,
			NE.util.getEspaciador(5),	
			fp,
			NE.util.getEspaciador(20),
			fpDatosCotiza,
			NE.util.getEspaciador(5),
			fpDatos
		]
	});	

});