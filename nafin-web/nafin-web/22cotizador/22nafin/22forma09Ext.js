//[COTIZADOR]
 //Tasas de Credito Nafin / Reportes Tesorer�a / Bit�coras
 Ext.onReady(function(){
 //TEST
 //Ext.Msg.alert('','Funciona');
 ///VARIABLES GLOBALES 
	var _URL_ ='22forma09Ext.data.jsp'
	
	//-------------------------MAQUINA DE ESTADOS--------------------------------
	var procesarCotizador_Bitacoras = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cotizador(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cotizador(resp.estadoSiguiente,resp);
			}
		}else{
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var  ESTADO = function (	estado , respuesta	){
		
		if(	estado	==	'INICIALIZACION'	){
		/*
			->Cargar Catalogo Bitacora(1)
			->Cargar Catalogo Moneda(2)
		*/	
		catalogoBitacora.load();
		catalogoMoneda.load();
		}else if(	estado	==	'LIMPRIAR.FORMA'	){
		/*
			->ResetFroma(1)
			->Ocultar gridConsulta(2)
		*/
			respuesta.reset();
			//Ext.getCmp('gridConsulta').hode();
		}else if(	estado	==	'CARGAR_CATALOGO_CURVA'){
			catalogoCurva.load({
				params:{
					cboMoneda : respuesta.cboMoneda,
					cboTipoBitacora:respuesta.cboTipoBitacora
				}
			});
		}else if(	estado	==	'CARGAR_CATALOGO_TABLA'){
			catalogoTabla.load({
				params:{
					cboMoneda : respuesta.cboMoneda,
					cboTipoBitacora:respuesta.cboTipoBitacora
				}
			});
		}else if(	estado	==	'CONSULTAR'	){
				Ext.getCmp('gridConsulta').hide();	
				fp.el.mask('Procesando...', 'x-mask-loading');
				
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion : 'Bitacoras.Generar',
							informacion : 'Bitacoras.Consultar',
							cboTipoBitacora :fp.getForm().getValues().cboTipoBitacora,
							start:0,
							limit:15					
						})
					}); 
		}else if (	estado	==	'GENERAR_ARCHIVO'){//csv
			Ext.Ajax.request({
				url: _URL_,
				params: Ext.apply(respuesta,{							
					informacion: 'Bitacoras.ArchivoCSV'
				}),	
				callback: procesarDescargaArchivos
			});
		}else if(	estado	==	'GENERAR_PDF'){
				Ext.Ajax.request({
				url: _URL_,
				params: Ext.apply(respuesta,{							
					informacion: 'Bitacoras.ArchivoPDF',
					start: 0,//barraPaginacionA.cursor,
					limit: 15//barraPaginacionA.pageSize
				}),	
				callback: procesarDescargaArchivos
			});
		}
	}
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
				
			if(store.getTotalCount() > 0) {//////////Verifica que existan registros
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
				
				el.unmask();					
			} else {	
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}

//-------------------------------STORE's----------------------------------------
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : _URL_,
		baseParams: {
			informacion: 'Bitacoras.Consultar'
		},		
		fields: [	
			{	name: 'DC_ALTA'},
			{	name: 'HORA'},	
			{	name: 'NOMBRE'},	//CURVA O TABLA DESC
			{	name: 'CG_NOMBRE_USUARIO'},	
			{	name: 'ARCHIVO'},	
			{	name: 'CD_NOMBRE'},			
			
			{	name: 'TIPO_COBRANZA'},
			{  name: 'FECHA_OPERACION' },
			{	name: 'BANDERA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	var catalogoBitacora = new Ext.data.JsonStore ({
	   id			: 'catologoBitacoraDataStore',
		root 		: 'registros',
		fields 	: ['clave', 'descripcion', 'loadMsg'],
		url 		: _URL_,
		baseParams: 
			{
				informacion	:	'Bitacoras.cargaCatalogoBitacora'
			},
		totalProperty	: 'total',
		autoLoad	: false,
		listeners:
			{
				exception	:	NE.util.mostrarDataProxyError,
				beforeload	:	NE.util.initMensajeCargaCombo
			}
  });
  var catalogoMoneda = new Ext.data.JsonStore ({
	   id			: 'catologoMonedaDataStore',
		root 		: 'registros',
		fields 	: ['clave', 'descripcion', 'loadMsg'],
		url 		: _URL_,
		baseParams: 
			{
				informacion	:	'Bitacoras.cargaCatalogoMoneda'
			},
		totalProperty	: 'total',
		autoLoad	: false,
		listeners:
			{
				exception	:	NE.util.mostrarDataProxyError,
				beforeload	:	NE.util.initMensajeCargaCombo
			}
  });
  var catalogoCurva = new Ext.data.JsonStore ({
	   id			: 'catologoCurvaDataStore',
		root 		: 'registros',
		fields 	: ['clave', 'descripcion', 'loadMsg'],
		url 		: _URL_,
		baseParams: 
			{
				informacion	:	'Bitacoras.cargaCatalogoCurva'
			},
		totalProperty	: 'total',
		autoLoad	: false,
		listeners:
			{
				exception	:	NE.util.mostrarDataProxyError,
				beforeload	:	NE.util.initMensajeCargaCombo
			}
  });
  var catalogoTabla = new Ext.data.JsonStore ({
	   id			: 'catologoTablaDataStore',
		root 		: 'registros',
		fields 	: ['clave', 'descripcion', 'loadMsg'],
		url 		: _URL_,
		baseParams: 
			{
				informacion	:	'Bitacoras.cargaCatalogoTabla'
			},
		totalProperty	: 'total',
		autoLoad	: false,
		listeners:
			{
				exception	:	NE.util.mostrarDataProxyError,
				beforeload	:	NE.util.initMensajeCargaCombo
			}
  });
//--------------------------COMPONENTE VISUALES---------------------------------

	var elementosForma =	[
		{
			xtype			: 'combo',
			fieldLabel	: 'Tipo de Bitacora',
			displayField: 'descripcion',
			valueField	: 'clave',
			triggerAction: 'all',
			typeAhead	: true,
			allowBlank		: false,
			minChars		: 1,
			name			: 'cboTipoBitacora',
			id				: 'cbTipoBitacora',
			mode			: 'local',
			forceSelection : true,
			hiddenName	: 'cboTipoBitacora',
			hidden		: false,
			emptyText	: 'Seleccionar...',
			store			: catalogoBitacora,
			tpl			: NE.util.templateMensajeCargaCombo,
			listeners:{
				'select': function(combo){
					var valor= (combo.getValue());
					//DEPENDIENDO DE SELESCCION
					if(valor	==	'C'){
						
							//ESTADO('MOSTAR_CATALOGO_CURVA',null);
							Ext.getCmp('cbTabla').hide();
							Ext.getCmp('cbCurva').show();
					}else if (	valor	==	'T'){
							//ESTADO('MOSTAR_CATALOGO_Tabla',null);					
							Ext.getCmp('cbCurva').hide();
							Ext.getCmp('cbTabla').show();
							
					}
				
				}
			}
		},
		{
			xtype			: 'combo',
			fieldLabel	: 'Moneda',
			displayField: 'descripcion',
			valueField	: 'clave',   
			triggerAction: 'all',
			typeAhead	: true,
			minChars		: 1,
			name			:'cboMoneda',
			id				: 'cbMoneda',
			mode			: 'local',
			forceSelection : true,
			hiddenName	: 'cboMoneda',
			hidden		: false,
			emptyText	: 'Seleccionar Moneda',
			store			: catalogoMoneda,
			tpl			: NE.util.templateMensajeCargaCombo,
			listeners:{
				'select': function(v){
					var forma = fp.getForm().getValues();
					var tipoBitacora = Ext.getCmp('cbTipoBitacora').getValue();
					if (	tipoBitacora	==	'C'){
						ESTADO('CARGAR_CATALOGO_CURVA',forma);
					}else if(	tipoBitacora	==	'T'){
						ESTADO('CARGAR_CATALOGO_TABLA',forma);
					}else{
							ESTADO('ESPERAR_DESICION',Ext.getCmp('cbMoneda').getValue());
					}
				}
			}
		},
		{
			xtype	: 'combo',
			hidden		: true,
			fieldLabel : 'Curva',
			displayField: 'descripcion',
			valueField	: 'clave',
			triggerAction: 'all',
			typeAhead	: true,
			minChars		: 1,
			name			:'cboCurva',
			id				: 'cbCurva',
			mode			: 'local',
			forceSelection : true,
			hiddenName	: 'cboCurva',
			emptyText	: 'Seleccione',
			store			: catalogoCurva,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		{
			xtype	: 'combo',
			hidden		: true,
			fieldLabel : 'Tabla',
			displayField: 'DESCRIPCION',
			valueField	: 'CLAVE',
			triggerAction: 'all',
			typeAhead	: true,
			minChars		: 1,
			name			:'cboTabla',
			id				: 'cbTabla',
			mode			: 'local',
			forceSelection : true,
			hiddenName	: 'cboTabla',
			emptyText	: 'Seleccione',
			store			: catalogoTabla,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Carga',
			combineErrors: false,
			
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaCargaMin',
					id: 'txtFechaCargaMin',
					value : new Date(),
					allowBlank: true,
					startDay: 0,
					vtype: 'rangofecha',
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaCargaMax',
					margins: '0 20 0 0' ,
					regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
					regex     : /.*[1-9][0-9]{3}$/
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 5,margins:{top:0, right:10, bottom:0, left:-15}
					
				},
				{
					xtype: 'datefield',
					name: 'fechaCargaMax',
					id: 'txtFechaCargaMax',
					value : new Date(),
					allowBlank: true,
					startDay: 1,
					vtype: 'rangofecha',
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaCargaMin',
					margins: '0 20 0 0',
					regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
					regex     : /.*[1-9][0-9]{3}$/
				}
			]
		}
	]
	
	var fp =new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: '<center>Criterios de B�squeda</center>',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind	:true,
				handler: function (boton, evento){
					var forma = fp.getForm().getValues();
					var fechaCargoDE = Ext.getCmp('txtFechaCargaMin');
					var fechaCargoA = Ext.getCmp('txtFechaCargaMax');
					if(!Ext.isEmpty(fechaCargoDE.getValue()) || !Ext.isEmpty(fechaCargoA.getValue())){
						if(Ext.isEmpty(fechaCargoDE.getValue())){
							fechaCargoDE.markInvalid('Debe capturar un rango de fechas');
							fechaCargoDE.focus();
							return;
						}   
						if(Ext.isEmpty(fechaCargoA.getValue())){
							fechaCargoA.markInvalid('Debe capturar un rango de fechas');
							fechaCargoA.focus();
							return;
						}
					}
					//fp.el.mask('sssss');
						ESTADO('CONSULTAR',forma);
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function (boton, evento){
					var forma = fp.getForm();
					ESTADO('LIMPRIAR.FORMA',forma);
				}//fin Handler
			}
		]
	});
	var gridConsulta = new Ext.grid.GridPanel({
		id			: 'gridConsulta',
		hidden	: true,
		header	:true,
		loadMask	: true,	
		height	: 400,
		width		: 940,
		store		: consultaData,
		style		: 'margin:0 auto;',
		  viewConfig: {forceFit: true},
		columns	:[
			{
				header:'Fecha',
				tooltip: 'Fecha',
				sortable: true,
				dataIndex: 'DC_ALTA',
				width: 150,
				align: 'center'
				//,renderer:fecha
				},{
				
				header:'Hora',
				tooltip: 'Hora',
				sortable: true,
				dataIndex: 'HORA',
				width: 100,
				align: 'center'
				
				},{
				
				header:'Curva / Tabla',
				tooltip: 'Curva / Tabla',
				sortable: true,
				dataIndex: 'NOMBRE',///
				width: 150,
				align: 'center'
				},{
				header:'Nombre del Usuario',
				tooltip: 'Nombre del Usuario',
				sortable: true,
				dataIndex: 'CG_NOMBRE_USUARIO',
				width: 110,
				align: 'center'
				},{
				header:'Nombre del Archivo',
				tooltip: 'Nombre del Archivo',
				sortable: true,
				dataIndex: 'ARCHIVO',
				width: 130,
				align: 'center'
				},{
				header:'Moneda',
				tooltip: 'Moneda',
				sortable: true,
				dataIndex: 'CD_NOMBRE',
				width: 130,
				align: 'center'
				
				}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
					{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {
						var barraPaginacionA = Ext.getCmp("barraPaginacion");						
						var forma = fp.getForm().getValues();
							ESTADO('GENERAR_ARCHIVO',forma)
													
					}
				},
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						var barraPaginacionA = Ext.getCmp("barraPaginacion");						
						var forma = fp.getForm().getValues();
						ESTADO('GENERAR_PDF',forma);
					}
				}
			]
		}	
	});
//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		renderHidden:	true,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),fp,		
			NE.util.getEspaciador(20),
			gridConsulta
		]
	});
	
	ESTADO('INICIALIZACION',null);
	
 });