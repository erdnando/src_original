<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		java.math.*,
		com.netro.cotizador.*,
		netropology.utilerias.usuarios.Usuario"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion   = request.getParameter("informacion")   == null ? "": (String)request.getParameter("informacion");
String operacion     = request.getParameter("operacion")     == null ? "": (String)request.getParameter("operacion");
String fechaInicio   = request.getParameter("fechaInicio")   == null ? "": (String)request.getParameter("fechaInicio");
String fechaFinal    = request.getParameter("fechaFinal")    == null ? "": (String)request.getParameter("fechaFinal");
String numSolicitud  = request.getParameter("numSolicitud")  == null ? "": (String)request.getParameter("numSolicitud");
String intermediario = request.getParameter("intermediario") == null ? "": (String)request.getParameter("intermediario");
String ejecutivo     = request.getParameter("ejecutivo")     == null ? "": (String)request.getParameter("ejecutivo");
String moneda        = request.getParameter("moneda")        == null ? "": (String)request.getParameter("moneda");
String montoInicio   = request.getParameter("montoInicio")   == null ? "": (String)request.getParameter("montoInicio");
String montoFinal    = request.getParameter("montoFinal")    == null ? "": (String)request.getParameter("montoFinal");
String estatus       = request.getParameter("estatus")       == null ? "": (String)request.getParameter("estatus");
String icSolicitud   = request.getParameter("icSolicitud")   == null ? "": (String)request.getParameter("icSolicitud");
String icPlanPagos   = request.getParameter("icPlanPagos")   == null ? "": (String)request.getParameter("icPlanPagos");

String cot_numero_solicitud     = request.getParameter("cot_numero_solicitud")     == null ? "": (String)request.getParameter("cot_numero_solicitud");
String cot_fecha_solicitud      = request.getParameter("cot_fecha_solicitud")      == null ? "": (String)request.getParameter("cot_fecha_solicitud");
String cot_hora_solicitud       = request.getParameter("cot_hora_solicitud")       == null ? "": (String)request.getParameter("cot_hora_solicitud");
String cot_fecha_cotizacion     = request.getParameter("cot_fecha_cotizacion")     == null ? "": (String)request.getParameter("cot_fecha_cotizacion");
String cot_hora_cotizacion      = request.getParameter("cot_hora_cotizacion")      == null ? "": (String)request.getParameter("cot_hora_cotizacion");
String cot_usuario              = request.getParameter("cot_usuario")              == null ? "": (String)request.getParameter("cot_usuario");
String cot_num_referencia       = request.getParameter("cot_num_referencia")       == null ? "": (String)request.getParameter("cot_num_referencia");
String cot_nombre_ejecutivo     = request.getParameter("cot_nombre_ejecutivo")     == null ? "": (String)request.getParameter("cot_nombre_ejecutivo");
String cot_email                = request.getParameter("cot_email")                == null ? "": (String)request.getParameter("cot_email");
String cot_telefono             = request.getParameter("cot_telefono")             == null ? "": (String)request.getParameter("cot_telefono");
String cot_fax                  = request.getParameter("cot_fax")                  == null ? "": (String)request.getParameter("cot_fax");
String cot_tipo_oper            = request.getParameter("cot_tipo_oper")            == null ? "": (String)request.getParameter("cot_tipo_oper");
String cot_intermediario        = request.getParameter("cot_intermediario")        == null ? "": (String)request.getParameter("cot_intermediario");
String cot_intermediario_ci     = request.getParameter("cot_intermediario_ci")     == null ? "": (String)request.getParameter("cot_intermediario_ci");
String cot_intermediario_cs     = request.getParameter("cot_intermediario_cs")     == null ? "": (String)request.getParameter("cot_intermediario_cs");
String cot_riesgo_inter         = request.getParameter("cot_riesgo_inter")         == null ? "": (String)request.getParameter("cot_riesgo_inter");
String cot_acreditado           = request.getParameter("cot_acreditado")           == null ? "": (String)request.getParameter("cot_acreditado");
String cot_riesgo_acred         = request.getParameter("cot_riesgo_acred")         == null ? "": (String)request.getParameter("cot_riesgo_acred");
String cot_programa             = request.getParameter("cot_programa")             == null ? "": (String)request.getParameter("cot_programa");
String cot_monto_total          = request.getParameter("cot_monto_total")          == null ? "": (String)request.getParameter("cot_monto_total");
String ic_moneda                = request.getParameter("ic_moneda")                == null ? "": (String)request.getParameter("ic_moneda");
String cot_tipo_tasa            = request.getParameter("cot_tipo_tasa")            == null ? "": (String)request.getParameter("cot_tipo_tasa");
String cot_num_disposiciones    = request.getParameter("cot_num_disposiciones")    == null ? "": (String)request.getParameter("cot_num_disposiciones");
String cot_mult_disposiciones   = request.getParameter("cot_mult_disposiciones")   == null ? "": (String)request.getParameter("cot_mult_disposiciones");
String cot_plazo_oper           = request.getParameter("cot_plazo_oper")           == null ? "": (String)request.getParameter("cot_plazo_oper");
String cot_esquema_recuperacion = request.getParameter("cot_esquema_recuperacion") == null ? "": (String)request.getParameter("cot_esquema_recuperacion");
String cot_ic_esquema_recup     = request.getParameter("cot_ic_esquema_recup")     == null ? "": (String)request.getParameter("cot_ic_esquema_recup");
String cot_periodo_ppi          = request.getParameter("cot_periodo_ppi")          == null ? "": (String)request.getParameter("cot_periodo_ppi");
String cot_periodo_ppc          = request.getParameter("cot_periodo_ppc")          == null ? "": (String)request.getParameter("cot_periodo_ppc");
String cot_primer_fecha         = request.getParameter("cot_primer_fecha")         == null ? "": (String)request.getParameter("cot_primer_fecha");
String cot_penultima_fecha      = request.getParameter("cot_penultima_fecha")      == null ? "": (String)request.getParameter("cot_penultima_fecha");
String cot_disposicion          = request.getParameter("cot_disposicion")          == null ? "": (String)request.getParameter("cot_disposicion");
String cot_monto                = request.getParameter("cot_monto")                == null ? "": (String)request.getParameter("cot_monto");
String cot_fecha_disposicion    = request.getParameter("cot_fecha_disposicion")    == null ? "": (String)request.getParameter("cot_fecha_disposicion");
String cot_fecha_vencimiento    = request.getParameter("cot_fecha_vencimiento")    == null ? "": (String)request.getParameter("cot_fecha_vencimiento");
String cot_tipo_cotizacion      = request.getParameter("cot_tipo_cotizacion")      == null ? "": (String)request.getParameter("cot_tipo_cotizacion");
String cot_ig_tasa_md           = request.getParameter("cot_ig_tasa_md")           == null ? "": (String)request.getParameter("cot_ig_tasa_md");
String cot_ig_tasa_spot         = request.getParameter("cot_ig_tasa_spot")         == null ? "": (String)request.getParameter("cot_ig_tasa_spot");
String cot_tasa_indicativa      = request.getParameter("cot_tasa_indicativa")      == null ? "": (String)request.getParameter("cot_tasa_indicativa");
String cot_aceptado_md          = request.getParameter("cot_aceptado_md")          == null ? "": (String)request.getParameter("cot_aceptado_md");
String cot_df_Aceptacion        = request.getParameter("cot_df_Aceptacion")        == null ? "": (String)request.getParameter("cot_df_Aceptacion");
String cot_observaciones        = request.getParameter("cot_observaciones")        == null ? "": (String)request.getParameter("cot_observaciones");

String mensaje           = "";
String consulta          = "";
String nombreArchivo     = "";
String infoRegresar      = "";
JSONObject resultado     = new JSONObject();
JSONArray listaRegistros = new JSONArray();
HashMap mapaRegistros    = new HashMap();
boolean success          = true;

int start = 0;
int limit = 0;

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("Catalogo_IF")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COMCAT_IF");
	cat.setCampoClave("IC_IF");
	cat.setCampoDescripcion("CG_RAZON_SOCIAL");
	cat.setOrden("CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Catalogo_Ejecutivo")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COTCAT_EJECUTIVO");
	cat.setCampoClave("IC_USUARIO");
	cat.setCampoDescripcion("CG_NOMBRE||' '||CG_APPATERNO||' '|| CG_APMATERNO");
	cat.setOrden("CG_NOMBRE");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Catalogo_Moneda")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COMCAT_MONEDA");
	cat.setCampoClave("IC_MONEDA");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setValoresCondicionIn("1,54", Integer.class);
	cat.setOrden("CD_NOMBRE");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Catalogo_Estatus")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COTCAT_ESTATUS");
	cat.setCampoClave("IC_ESTATUS");
	cat.setCampoDescripcion("CG_DESCRIPCION");
	cat.setValoresCondicionIn("1,2,4,5", Integer.class);
	cat.setOrden("CG_DESCRIPCION");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Consultar_Datos") || informacion.equals("Imprimir_PDF") || informacion.equals("Imprimir_CSV")){

	/***** Le doy formato a las fechas [dd/mm/aaaa] *****/
	if(!fechaInicio.equals("")){
		fechaInicio = fechaInicio.substring(8,10) + "/" + fechaInicio.substring(5,7) + "/" + fechaInicio.substring(0,4);
	}
	if(!fechaFinal.equals("")){
		fechaFinal = fechaFinal.substring(8,10) + "/" + fechaFinal.substring(5,7) + "/" + fechaFinal.substring(0,4);
	}
	/***** Si no seleccionó un estatus, se muestran todos *****/
	if(estatus.equals("")){
		estatus = "1, 2, 4, 5";
	}

	log.debug("Fecha inicio: " + fechaInicio);
	log.debug("Fecha fin: " + fechaFinal);
	log.debug("Estatus: " + estatus);

	ConsCotizacionIf paginador = new ConsCotizacionIf();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	paginador.setFechaInicio(fechaInicio);
	paginador.setFechaFinal(fechaFinal);
	paginador.setNumeroSolicitud(numSolicitud);
	paginador.setMoneda(moneda);
	paginador.setMontoInicio(montoInicio);
	paginador.setMontoFinal(montoFinal);
	paginador.setEstatus(estatus);
	paginador.setIcIf(intermediario);
	paginador.setIcUsuario(ejecutivo);
	paginador.setOrigen("CALCULADORAPESOSDOLARES");
	paginador.setUsuarioNafin("");

	try{
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e){
		throw new AppException("Error en los parametros start y limit recibidos", e);
	}

	if (informacion.equals("Consultar_Datos")){
		try{
			if (operacion.equals("generar")){ //Nueva consulta
				queryHelper.executePKQuery(request);
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);
		} catch(Exception e){
			throw new AppException(" Error en la paginacion. ", e);
		}

		/**** Proceso los campos correspondientes a las Tasas *****/
		String tmp              = "";
		String total            = "";
		String ic_plan_de_pagos = "";
		String descTasaInd      = "";
		String datos_grid       = "";
		JSONObject resultado1   = new JSONObject();

		resultado1 = JSONObject.fromObject(consulta);
		datos_grid = resultado1.get("registros").toString();
		JSONArray arrayGrid = JSONArray.fromObject(datos_grid);

		if(!(resultado1.get("total")).equals("")){
			total = resultado1.get("total").toString();
		} else{
			total = "0";
		}
		
		for(int i=0; i<arrayGrid.size(); i++){
			tmp = "";
			JSONObject auxiliar = arrayGrid.getJSONObject(i);
			mapaRegistros = new HashMap();

			Solicitud sol = new Solicitud();
			sol.setIc_moneda(auxiliar.getString("IC_MONEDA"));
			sol.setIg_plazo(auxiliar.getString("PLAZO"));
			sol.setIc_tipo_tasa(auxiliar.getString("IC_TIPO_TASA"));
			sol.setIc_esquema_recup(auxiliar.getString("IC_ESQUEMA_RECUP"));
			sol.setCg_ut_ppc(auxiliar.getString("CG_UT_PPC"));
			sol.setCg_ut_ppi(auxiliar.getString("CG_UT_PPI"));

			if((auxiliar.getString("IC_ESQUEMA_RECUP")).equals("3")){
				ic_plan_de_pagos = auxiliar.getString("IC_SOLIC_ESP");
			} else{
				ic_plan_de_pagos = "";
			}
			descTasaInd = sol.getDescTasaIndicativa();

			if(!(auxiliar.getString("TASA_MISMO_DIA")).equals("") && !(auxiliar.getString("IC_ESTATUS")).equals("3")){
				tmp = descTasaInd + " " + auxiliar.getString("TASA_MISMO_DIA") + " %";
				mapaRegistros.put("TASA_MISMO_DIA", tmp);
			} else{
				mapaRegistros.put("TASA_MISMO_DIA", "");
			}
			if(!(auxiliar.getString("TASA_SPOT")).equals("") && !(auxiliar.getString("IC_ESTATUS")).equals("3")){
				tmp = descTasaInd + " " + auxiliar.getString("TASA_SPOT") + " %";
				mapaRegistros.put("TASA_SPOT", tmp);
			} else{
				mapaRegistros.put("TASA_SPOT", "");
			}
			if((auxiliar.getString("IC_ESTATUS")).equals("7") || (auxiliar.getString("IC_ESTATUS")).equals("9")){
				tmp = descTasaInd + " " + auxiliar.getString("TASA_CONFIRMADA") + " %";
				mapaRegistros.put("TASA_CONFIRMADA", tmp);
			} else{
				mapaRegistros.put("TASA_CONFIRMADA", "");
			}
			if(!(auxiliar.getString("PLAZO")).equals("")){
				tmp = auxiliar.getString("PLAZO") + " días";
				mapaRegistros.put("PLAZO", tmp);
			} else{
				mapaRegistros.put("PLAZO", "");
			}

			/***** Paso los demas parámetros al HashMap *****/
			mapaRegistros.put("IC_SOLIC_ESP",     auxiliar.getString("IC_SOLIC_ESP")      );
			mapaRegistros.put("FECHA_SOLICITUD",  auxiliar.getString("FECHA_SOLICITUD")   );
			mapaRegistros.put("FECHA_COTIZACION", auxiliar.getString("FECHA_COTIZACION")  );
			mapaRegistros.put("NUMERO_SOLICITUD", auxiliar.getString("NUMERO_SOLICITUD")  );
			mapaRegistros.put("SOLICITAR_AUT",    auxiliar.getString("SOLICITAR_AUT")     );
			mapaRegistros.put("CG_RAZON_SOCIAL",  auxiliar.getString("CG_RAZON_SOCIAL")   );
			mapaRegistros.put("MONEDA",           auxiliar.getString("MONEDA")            );
			mapaRegistros.put("EJECUTIVO",        auxiliar.getString("EJECUTIVO")         );
			mapaRegistros.put("MONTO",            auxiliar.getString("MONTO")             );
			mapaRegistros.put("ESTATUS",          auxiliar.getString("ESTATUS")           );
			mapaRegistros.put("TIPOCOTIZA",       auxiliar.getString("TIPOCOTIZA")        );
			mapaRegistros.put("CAUSA_RECHAZO",    auxiliar.getString("CAUSA_RECHAZO")     );
			mapaRegistros.put("OBSER_OPER",       auxiliar.getString("OBSER_OPER")        );
			mapaRegistros.put("IC_ESTATUS",       auxiliar.getString("IC_ESTATUS")        );
			mapaRegistros.put("IC_PLAN_PAGOS",    ic_plan_de_pagos                        );
			listaRegistros.add(mapaRegistros);
		}
		consulta  = "{\"success\": "+new Boolean(success)+", \"total\": \"" + total + "\", \"registros\": " + listaRegistros.toString() +"}";
		resultado = JSONObject.fromObject(consulta);
		
	} else if(informacion.equals("Imprimir_PDF")){
		try{
			nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			throw new AppException("Error al generar el archivo PDF. ", e);
		}
	} else if(informacion.equals("Imprimir_CSV")){
		try{
			if (operacion.equals("generar")){ //Nueva consulta
				queryHelper.executePKQuery(request);
			}
			nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "CSV");
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			throw new AppException("Error al generar el archivo CSV. ", e);
		}
	}
	infoRegresar = resultado.toString();

} else if(informacion.equals("Consulta_Cotizacion") || informacion.equals("Consulta_Cotizacion_PDF")){

	JSONArray regDetalleDisp = new JSONArray();
	JSONArray detalleDisp    = new JSONArray();
	JSONArray planPagosCap   = new JSONArray();
	JSONArray regPlanPagos   = new JSONArray();
	String periodo           = "";

	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	Solicitud consultaSol = new Solicitud();

	try{
		consultaSol              = solCotEsp.consultaSolicitud(icSolicitud);
		cot_numero_solicitud     = consultaSol.getNumero_solicitud();
		cot_fecha_solicitud      = consultaSol.getDf_solicitud();
		cot_hora_solicitud       = consultaSol.getHora_solicitud();
		cot_fecha_cotizacion     = consultaSol.getDf_cotizacion();
		cot_hora_cotizacion      = consultaSol.getHora_cotizacion();
		cot_usuario              = consultaSol.getIc_usuario() +" - " + consultaSol.getNombre_usuario();
		cot_num_referencia       = consultaSol.getIg_num_referencia();
		cot_nombre_ejecutivo     = consultaSol.getNombre_ejecutivo();
		cot_email                = consultaSol.getCg_mail();
		cot_telefono             = consultaSol.getCg_telefono();
		cot_fax                  = consultaSol.getCg_fax();
		cot_tipo_oper            = consultaSol.getTipoOper();
		cot_intermediario_ci     = consultaSol.getNombre_if_ci();
		cot_intermediario_cs     = consultaSol.getNombre_if_cs();
		cot_riesgo_inter         = consultaSol.getRiesgoIf();
		cot_acreditado           = consultaSol.getCg_acreditado();
		cot_riesgo_acred         = consultaSol.getRiesgoAc();
		cot_programa             = consultaSol.getCg_programa();
		ic_moneda                = consultaSol.getIc_moneda();
		cot_monto_total          = "$" + Comunes.formatoDecimal(consultaSol.getFn_monto(),2);
		cot_tipo_tasa            = consultaSol.getTipo_tasa();
		cot_num_disposiciones    = consultaSol.getIn_num_disp();
		cot_mult_disposiciones   = consultaSol.getCs_tasa_unica();
		cot_plazo_oper           = consultaSol.getIg_plazo();
		cot_ic_esquema_recup     = consultaSol.getIc_esquema_recup();
		cot_esquema_recuperacion = consultaSol.getEsquema_recup();
		cot_periodo_ppi          = consultaSol.periodoTasa(consultaSol.getCg_ut_ppi());
		cot_periodo_ppc          = consultaSol.periodoTasa(consultaSol.getCg_ut_ppc());
		cot_primer_fecha         = consultaSol.getCg_pfpc();
		cot_penultima_fecha      = consultaSol.getCg_pufpc();
		cot_tipo_cotizacion      = consultaSol.getTipoCotizacion();
		cot_ig_tasa_md           = Comunes.formatoDecimal(consultaSol.getIg_tasa_md(),2);
		cot_ig_tasa_spot         = Comunes.formatoDecimal(consultaSol.getIg_tasa_spot(),2);
		cot_tasa_indicativa      = consultaSol.getDescTasaIndicativa();
		cot_aceptado_md          = consultaSol.getCs_aceptado_md();
		cot_df_Aceptacion        = consultaSol.getDf_aceptacion();
		cot_observaciones        = consultaSol.getCg_observaciones();

		/***** Obteniendo el tipo de moneda del monto toal requerido *****/
		if(ic_moneda.equals("1")){
			ic_moneda = "Pesos";
		} else if(ic_moneda.equals("54")){
			ic_moneda = "Dolares";
		}
		cot_monto_total = cot_monto_total + " " + ic_moneda;

		/***** Obteniendo el periodo de las tasas *****/
		if("1".equals(consultaSol.getIc_esquema_recup())){
			periodo = "Al Vencimiento";
		}else if("4".equals(consultaSol.getIc_esquema_recup())&&"0".equals(consultaSol.getCg_ut_ppc())){
			periodo = "Al Vencimiento";
		}else if( !consultaSol.periodoTasa(consultaSol.getCg_ut_ppi()).equals("") ){
			periodo = consultaSol.periodoTasa(consultaSol.getCg_ut_ppi())+"mente";
		} else if( !consultaSol.periodoTasa(consultaSol.getCg_ut_ppc()).equals("") ){
			periodo = consultaSol.periodoTasa(consultaSol.getCg_ut_ppc())+"mente";
		} else{
			periodo = "Al Vencimiento";
		}
		cot_ig_tasa_md   = cot_ig_tasa_md   + "% anual pagadera " + periodo;
		cot_ig_tasa_spot = cot_ig_tasa_spot + "% anual pagadera " + periodo;		

		/***** Se arma el Grid Detalle Disposiciones *****/
		for(int i = 0; i < Integer.parseInt(consultaSol.getIn_num_disp()); i++){
			detalleDisp   = new JSONArray();
			detalleDisp.add(""  + (i+1));                                                     //NUM_DISPOSICION
			detalleDisp.add("$" + Comunes.formatoDecimal(consultaSol.getFn_monto_disp(i),2)); //MONTO
			detalleDisp.add(""  + consultaSol.getDf_disposicion(i));                          //FECHA_DISPOSICION
			detalleDisp.add(""  + consultaSol.getDf_vencimiento(i));                          //FECHA_VENCIMIENTO
			regDetalleDisp.add(detalleDisp);
		}

		/***** Se arma el grid Plan de Pagos Capital *****/
		if((consultaSol.getPagos()).size() > 0){
			ArrayList alFilas = consultaSol.getPagos();
			for(int i = 0; i < alFilas.size(); i++){
				ArrayList alColumnas = (ArrayList)alFilas.get(i);
				planPagosCap   = new JSONArray();
				planPagosCap.add((String)alColumnas.get(0));
				planPagosCap.add((String)alColumnas.get(1));
				planPagosCap.add("$" + Comunes.formatoDecimal(alColumnas.get(2).toString(),2));
				planPagosCap.add("");
				regPlanPagos.add(planPagosCap);
			}
		}

		if(informacion.equals("Consulta_Cotizacion")){

			resultado.put("cot_numero_solicitud",     cot_numero_solicitud    );
			resultado.put("cot_fecha_solicitud",      cot_fecha_solicitud     );
			resultado.put("cot_hora_solicitud",       cot_hora_solicitud      );
			resultado.put("cot_fecha_cotizacion",     cot_fecha_cotizacion    );
			resultado.put("cot_hora_cotizacion",      cot_hora_cotizacion     );
			resultado.put("cot_usuario",              cot_usuario             );
			resultado.put("cot_num_referencia",       cot_num_referencia      );
			resultado.put("cot_nombre_ejecutivo",     cot_nombre_ejecutivo    );
			resultado.put("cot_email",                cot_email               );
			resultado.put("cot_telefono",             cot_telefono            );
			resultado.put("cot_fax",                  cot_fax                 );
			resultado.put("cot_tipo_oper",            cot_tipo_oper           );
			resultado.put("cot_intermediario_ci",     cot_intermediario_ci    );
			resultado.put("cot_intermediario_cs",     cot_intermediario_cs    );
			resultado.put("cot_riesgo_inter",         cot_riesgo_inter        );
			resultado.put("cot_acreditado",           cot_acreditado          );
			resultado.put("cot_riesgo_acred",         cot_riesgo_acred        );
			resultado.put("cot_programa",             cot_programa            );
			resultado.put("cot_monto_total",          cot_monto_total         );
			resultado.put("cot_tipo_tasa",            cot_tipo_tasa           );
			resultado.put("cot_num_disposiciones",    cot_num_disposiciones   );
			resultado.put("cot_mult_disposiciones",   cot_mult_disposiciones  );
			resultado.put("cot_plazo_oper",           cot_plazo_oper + " días");
			resultado.put("cot_ic_esquema_recup",     cot_ic_esquema_recup    );
			resultado.put("cot_esquema_recuperacion", cot_esquema_recuperacion);
			resultado.put("cot_periodo_ppi",          cot_periodo_ppi         );
			resultado.put("cot_periodo_ppc",          cot_periodo_ppc         );
			resultado.put("cot_primer_fecha",         cot_primer_fecha        );
			resultado.put("cot_penultima_fecha",      cot_penultima_fecha     );
			resultado.put("cot_tipo_cotizacion",      cot_tipo_cotizacion     );
			resultado.put("cot_ig_tasa_md",           cot_ig_tasa_md          );
			resultado.put("cot_ig_tasa_spot",         cot_ig_tasa_spot        );
			resultado.put("cot_tasa_indicativa",      cot_tasa_indicativa     );
			resultado.put("cot_aceptado_md",          cot_aceptado_md         );
			resultado.put("cot_df_Aceptacion",        cot_df_Aceptacion       );
			resultado.put("cot_observaciones",        cot_observaciones       );
			resultado.put("totalDetalleDisp",         ""+regDetalleDisp.size());
			resultado.put("registrosDetalleDisp",    regDetalleDisp.toString());
			resultado.put("totalPlanPagos",           ""+planPagosCap.size()  );
			resultado.put("registrosPlanPagos",       regPlanPagos.toString() );
			resultado.put("strTipoUsuario",           strTipoUsuario          );

		} else if(informacion.equals("Consulta_Cotizacion_PDF")){

			ConsCotizacionIfArchivos paginador1 = new ConsCotizacionIfArchivos();
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador1);

			HashMap datosReporte = new HashMap();
			datosReporte.put("cot_numero_solicitud",     cot_numero_solicitud    );
			datosReporte.put("cot_fecha_solicitud",      cot_fecha_solicitud     );
			datosReporte.put("cot_hora_solicitud",       cot_hora_solicitud      );
			datosReporte.put("cot_fecha_cotizacion",     cot_fecha_cotizacion    );
			datosReporte.put("cot_hora_cotizacion",      cot_hora_cotizacion     );
			datosReporte.put("cot_usuario",              cot_usuario             );
			datosReporte.put("cot_num_referencia",       cot_num_referencia      );
			datosReporte.put("cot_nombre_ejecutivo",     cot_nombre_ejecutivo    );
			datosReporte.put("cot_email",                cot_email               );
			datosReporte.put("cot_telefono",             cot_telefono            );
			datosReporte.put("cot_fax",                  cot_fax                 );
			datosReporte.put("cot_tipo_oper",            cot_tipo_oper           );
			datosReporte.put("cot_intermediario_ci",     cot_intermediario_ci    );
			datosReporte.put("cot_intermediario_cs",     cot_intermediario_cs    );
			datosReporte.put("cot_riesgo_inter",         cot_riesgo_inter        );
			datosReporte.put("cot_acreditado",           cot_acreditado          );
			datosReporte.put("cot_riesgo_acred",         cot_riesgo_acred        );
			datosReporte.put("cot_programa",             cot_programa            );
			datosReporte.put("cot_monto_total",          cot_monto_total         );
			datosReporte.put("cot_tipo_tasa",            cot_tipo_tasa           );
			datosReporte.put("cot_num_disposiciones",    cot_num_disposiciones   );
			datosReporte.put("cot_mult_disposiciones",   cot_mult_disposiciones  );
			datosReporte.put("cot_plazo_oper",           cot_plazo_oper + " días");
			datosReporte.put("cot_ic_esquema_recup",     cot_ic_esquema_recup    );
			datosReporte.put("cot_esquema_recuperacion", cot_esquema_recuperacion);
			datosReporte.put("cot_periodo_ppi",          cot_periodo_ppi         );
			datosReporte.put("cot_periodo_ppc",          cot_periodo_ppc         );
			datosReporte.put("cot_primer_fecha",         cot_primer_fecha        );
			datosReporte.put("cot_penultima_fecha",      cot_penultima_fecha     );
			datosReporte.put("cot_tipo_cotizacion",      cot_tipo_cotizacion     );
			datosReporte.put("cot_ig_tasa_md",           cot_ig_tasa_md          );
			datosReporte.put("cot_ig_tasa_spot",         cot_ig_tasa_spot        );
			datosReporte.put("cot_tasa_indicativa",      cot_tasa_indicativa     );
			datosReporte.put("cot_aceptado_md",          cot_aceptado_md         );
			datosReporte.put("cot_df_Aceptacion",        cot_df_Aceptacion       );
			datosReporte.put("cot_observaciones",        cot_observaciones       );
			datosReporte.put("estatus",                  estatus                 );
			datosReporte.put("strTipoUsuario",           strTipoUsuario          );

			/***** Se arma la Tabla Detalle Disposiciones *****/
			HashMap mapaDetalleDisp = new HashMap();
			List listaDetalleDisp = new ArrayList();
			for(int i = 0; i < Integer.parseInt(consultaSol.getIn_num_disp()); i++){
				mapaDetalleDisp = new HashMap();
				mapaDetalleDisp.put("NUM_DISPOSICION", "" + (i+1));
				mapaDetalleDisp.put("MONTO", "$" + Comunes.formatoDecimal(consultaSol.getFn_monto_disp(i),2));
				mapaDetalleDisp.put("FECHA_DISPOSICION", consultaSol.getDf_disposicion(i));
				mapaDetalleDisp.put("FECHA_VENCIMIENTO",  consultaSol.getDf_vencimiento(i));
				listaDetalleDisp.add(mapaDetalleDisp);
			}

			/********** Se arma la Tabla Plan de Pagos Capital **********/
			ArrayList listaPlanPagos = new ArrayList();
			if((consultaSol.getPagos()).size() > 0){
				HashMap mapaPlanPagos = new HashMap();
				ArrayList alFilas = consultaSol.getPagos();
				for(int i = 0; i < alFilas.size(); i++){
					mapaPlanPagos = new HashMap();
					ArrayList alColumnas = (ArrayList)alFilas.get(i);
					mapaPlanPagos.put("NUM_PAGO",   (String)alColumnas.get(0));
					mapaPlanPagos.put("FECHA_PAGO", (String)alColumnas.get(1));
					mapaPlanPagos.put("MONTO_PAGAR", "$" + Comunes.formatoDecimal(alColumnas.get(2).toString(),2));
					listaPlanPagos.add(mapaPlanPagos);
				}
			}
			paginador1.setDatosConsulta(datosReporte);
			paginador1.setRegDetalleDisp(listaDetalleDisp);
			paginador1.setRegPlanPagos(listaPlanPagos);
			paginador1.setGeneraReporteFinal("N");
			try{
				nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				resultado.put("success", new Boolean(true));
				resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF. ", e);
			}
		}

	} catch(Exception e){
		success = false;
		mensaje = "Ocurrió un error al obtener los datos. " + e;
		log.warn("Ocurrió un error al obtener los datos. " + e);
	}

	resultado.put("mensaje", mensaje             );
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Generar_Pagos")){

	try{

		success = true;

		SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
		Solicitud consultaSol = new Solicitud();

		consultaSol = solCotEsp.consultaSolicitud(icPlanPagos);
		if((consultaSol.getPagos()).size() > 0){
			List datosPagos = consultaSol.getPagos();
			ConsCotizacionIfArchivos paginador1 = new ConsCotizacionIfArchivos();
			paginador1.setDatosPagos(datosPagos);
			paginador1.setNumSolicitud(numSolicitud);
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador1);
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
		} else{
			success = false;
			mensaje = "La consulta no arroj&oacute; datos. No se gener&oacute; el archivo.";
		}

		if(nombreArchivo.equals("ERROR")){
			success = false;
			mensaje = "Ocurrió un error al generar el archivo CSV.";
		}

	} catch(Exception e){
		success = false;
		mensaje = "Ocurri&oacute; un error al generar el archivo. " + e;
		log.warn("Ocurrió un error al generar el archivo CSV. " + e);
	}

	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje             );
	resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>