Ext.onReady(function(){

	/***** Variables globales *****/
	var fm = Ext.form;
	var jsonDataEncode;
	var ic_proc_pago = ''; 

	function procesoInicializar(){

		Ext.getCmp('df_disposicion_id').setValue((Ext.getDom('fecha_disp_hid').value).trim());
		storePlanPagosCap.load({
			params: Ext.apply({
				nombre_ejecutivo:   (Ext.getDom('nombre_ejecutivo').value).trim(),
				cg_mail:            (Ext.getDom('cg_mail').value).trim(),
				cg_telefono:        (Ext.getDom('cg_telefono').value).trim(),
				cg_fax:             (Ext.getDom('cg_fax').value).trim(),
				ejecutivo_nafin:    (Ext.getDom('ejecutivo_nafin').value).trim(),
				ig_tipo_piso:       (Ext.getDom('ig_tipo_piso').value).trim(),
				cg_razon_social_if: (Ext.getDom('cg_razon_social_if').value).trim(),
				ig_riesgo_if:       (Ext.getDom('ig_riesgo_if').value).trim(),
				cg_acreditado:      (Ext.getDom('cg_acreditado').value).trim(),
				ig_valor_riesgo_ac: (Ext.getDom('ig_valor_riesgo_ac').value).trim(),
				df_solicitud:       (Ext.getDom('df_solicitud').value).trim(),
				df_cotizacion:      (Ext.getDom('df_cotizacion').value).trim(),
				cg_programa:        (Ext.getDom('cg_programa').value).trim(),
				ic_moneda:          (Ext.getDom('ic_moneda').value).trim(),
				fn_monto:           (Ext.getDom('fn_monto').value).trim(),
				ic_tipo_tasa:       (Ext.getDom('ic_tipo_tasa').value).trim(),
				in_num_disp:        (Ext.getDom('in_num_disp').value).trim(),
				ig_plazo_conv:      (Ext.getDom('ig_plazo_conv').value).trim(),
				cg_ut_plazo:        (Ext.getDom('cg_ut_plazo').value).trim(),
				ig_plazo:           (Ext.getDom('ig_plazo').value).trim(),
				ic_esquema_recup:   (Ext.getDom('ic_esquema_recup').value).trim(),
				cg_ut_ppi:          (Ext.getDom('cg_ut_ppi').value).trim(),
				ig_ppi:             (Ext.getDom('ig_ppi').value).trim(),
				cg_ut_ppc:          (Ext.getDom('cg_ut_ppc').value).trim(),
				ig_ppc:             (Ext.getDom('ig_ppc').value).trim(),
				pfpc:               (Ext.getDom('pfpc').value).trim(),
				pufpc:              (Ext.getDom('pufpc').value).trim(),
				idcurva:            (Ext.getDom('idcurva').value).trim(),
				id_interpolacion:   (Ext.getDom('id_interpolacion').value).trim(),
				metodoCalculo:      (Ext.getDom('metodoCalculo').value).trim(),
				ic_solic_esp:       (Ext.getDom('ic_solic_esp').value).trim(),
				str_tipo_usuario:   (Ext.getDom('str_tipo_usuario').value).trim(),
				ic_ejecutivo:       (Ext.getDom('ic_ejecutivo').value).trim(),
				ig_plazo_max_oper:  (Ext.getDom('ig_plazo_max_oper').value).trim(),
				ig_num_max_disp:    (Ext.getDom('ig_num_max_disp').value).trim(),
				ig_plazo_conv_ant:  (Ext.getDom('ig_plazo_conv_ant').value).trim(),
				num_disp_capt:      (Ext.getDom('num_disp_capt').value).trim(),
				fn_monto_grid:      (Ext.getDom('fn_monto_grid').value).trim(),
				disposicion_grid:   (Ext.getDom('disposicion_grid').value).trim(),
				vencimiento_grid:   (Ext.getDom('vencimiento_grid').value).trim(),
				pagos:              (Ext.getDom('pagos').value).trim()
			})
		});

	}

	/***** Limpia el form principal *****/
	function limpiar(){
		Ext.getCmp('formaPrincipal').getForm().reset();
	}

	/********** Direcciona a la p�gina indicada: 22forma07Aext.jsp, 22forma07pp2ext.jsp, 22forma07Bext.jsp **********/
	function procesoDireccionaPagina(pantalla){
		window.location.href=pantalla                                            +
		'?opcionPantalla=RECARGAR'                                               +
		'&nombre_ejecutivo='   + (Ext.getDom('nombre_ejecutivo').value).trim()   +
		'&cg_mail='            + (Ext.getDom('cg_mail').value).trim()            +
		'&cg_telefono='        + (Ext.getDom('cg_telefono').value).trim()        +
		'&cg_fax='             + (Ext.getDom('cg_fax').value).trim()             +
		'&ejecutivo_nafin='    + (Ext.getDom('ejecutivo_nafin').value).trim()    +
		'&ig_tipo_piso='       + (Ext.getDom('ig_tipo_piso').value).trim()       +
		'&cg_razon_social_if=' + (Ext.getDom('cg_razon_social_if').value).trim() +
		'&ig_riesgo_if='       + (Ext.getDom('ig_riesgo_if').value).trim()       +
		'&cg_acreditado='      + (Ext.getDom('cg_acreditado').value).trim()      +
		'&ig_valor_riesgo_ac=' + (Ext.getDom('ig_valor_riesgo_ac').value).trim() +
		'&df_solicitud='       + (Ext.getDom('df_solicitud').value).trim()       +
		'&df_cotizacion='      + (Ext.getDom('df_cotizacion').value).trim()      +
		'&cg_programa='        + (Ext.getDom('cg_programa').value).trim()        +
		'&ic_moneda='          + (Ext.getDom('ic_moneda').value).trim()          +
		'&fn_monto='           + (Ext.getDom('fn_monto').value).trim()           +
		'&ic_tipo_tasa='       + (Ext.getDom('ic_tipo_tasa').value).trim()       +
		'&in_num_disp='        + (Ext.getDom('in_num_disp').value).trim()        +
		'&ig_plazo_conv='      + (Ext.getDom('ig_plazo_conv').value).trim()      +
		'&cg_ut_plazo='        + (Ext.getDom('cg_ut_plazo').value).trim()        +
		'&ig_plazo='           + (Ext.getDom('ig_plazo').value).trim()           +
		'&ic_esquema_recup='   + (Ext.getDom('ic_esquema_recup').value).trim()   +
		'&cg_ut_ppi='          + (Ext.getDom('cg_ut_ppi').value).trim()          +
		'&ig_ppi='             + (Ext.getDom('ig_ppi').value).trim()             +
		'&cg_ut_ppc='          + (Ext.getDom('cg_ut_ppc').value).trim()          +
		'&ig_ppc='             + (Ext.getDom('ig_ppc').value).trim()             +
		'&pfpc='               + (Ext.getDom('pfpc').value).trim()               +
		'&pufpc='              + (Ext.getDom('pufpc').value).trim()              +
		'&idcurva='            + (Ext.getDom('idcurva').value).trim()            +
		'&id_interpolacion='   + (Ext.getDom('id_interpolacion').value).trim()   +
		'&metodoCalculo='      + (Ext.getDom('metodoCalculo').value).trim()      +
		'&ic_solic_esp='       + (Ext.getDom('ic_solic_esp').value).trim()       +
		'&str_tipo_usuario='   + (Ext.getDom('str_tipo_usuario').value).trim()   +
		'&ic_ejecutivo='       + (Ext.getDom('ic_ejecutivo').value).trim()       +
		'&ig_plazo_max_oper='  + (Ext.getDom('ig_plazo_max_oper').value).trim()  +
		'&ig_num_max_disp='    + (Ext.getDom('ig_num_max_disp').value).trim()    +
		'&ig_plazo_conv_ant='  + (Ext.getDom('ig_plazo_conv_ant').value).trim()  +
		'&num_disp_capt='      + (Ext.getDom('num_disp_capt').value).trim()      +
		'&fn_monto_grid='      + (Ext.getDom('fn_monto_grid').value).trim()      +
		'&disposicion_grid='   + (Ext.getDom('disposicion_grid').value).trim()   +
		'&vencimiento_grid='   + (Ext.getDom('vencimiento_grid').value).trim()   +
		'&pagos='              + (Ext.getDom('pagos').value).trim()              +
		'&ic_proc_pago='       + ic_proc_pago;
	}

	/********** Valida los campos del grid y el monto total requerido, y env�a los datos a la siguiente p�gina **********/
	function continuar(store){

		var exito          = true;
		var jsonDataEncode = null;
		var fechaPago      = null;
		var fechaPagoAnt   = null;
		var datar          = new Array();
		var fechaSol       = (new Date(Ext.getCmp('df_disposicion_id').getValue())).format('m/d/Y');
		var records        = store.getRange();
		var df_pago        = new Array();
		var fn_monto_pago  = new Array();

		for (var i = 0; i < records.length; i++){

			if(records[i].data.FECHA_PAGO != '' || records[i].data.FECHA_PAGO != null){
				fechaPago = (new Date(records[i].data.FECHA_PAGO)).format('m/d/Y');
			} else{
				fechaPago = '';
			}

			if(records[i].data.FECHA_PAGO == '' || records[i].data.FECHA_PAGO == null){
				Ext.MessageBox.alert('Mensaje...','El campo Fecha de Pago es obligatorio.',
					function(){
						gridPlanPagosCap.startEditing(i, 1);
						return;
					}
				);
				exito = false;
				break;
			} else if(records[i].data.MONTO == ''){
				Ext.MessageBox.alert('Mensaje...','El campo Monto es obligatorio.',
					function(){
						gridPlanPagosCap.startEditing(i, 2);
						return;
					}
				);
				exito = false;
				break;
			} else if((Date.parse(fechaPago)) <= (Date.parse(fechaSol))){
				Ext.MessageBox.alert('Mensaje...','La fecha de pago debe ser posterior a la fecha de solicitud.',
					function(){
						gridPlanPagosCap.startEditing(i, 1);
						return;
					}
				);
				exito = false;
				break;
			} else if(i == 0 && ((Date.parse(fechaPago)) <= (Date.parse(fechaSol)))){
				Ext.MessageBox.alert('Mensaje...','Deben existir cinco dias naturales entre la fecha de disposicion y la primer fecha de pago.',
					function(){
						gridPlanPagosCap.startEditing(i, 1);
						return;
					}
				);
				exito = false;
				break;
			}

			if(i==0){
				var resultado =  parseInt(Date.parse(fechaPago)) - parseInt(Date.parse(fechaSol));
				// 432000000 corresponden a 5 d�as
				if(resultado < 432000000){
					Ext.MessageBox.alert('Mensaje...','Deben existir cinco dias naturales entre la fecha de disposicion y la primer fecha de pago.',
						function(){
							gridPlanPagosCap.startEditing(i, 1);
							return;
						}
					);
					exito = false;
					break;
				}
			}

			if(i > 0){
				fechaPagoAnt = (new Date(records[i-1].data.FECHA_PAGO)).format('m/d/Y');
				if((Date.parse(fechaPago)) <= (Date.parse(fechaPagoAnt))){
					Ext.MessageBox.alert('Mensaje...','La fecha es menor a la anterior, favor de corregirla.',
						function(){
							gridPlanPagosCap.startEditing(i, 1);
							return;
						}
					);
					exito = false;
					break;
				}
			}

			if(i == records.length-1){
				var fechaVenc = (Ext.getDom('fecha_venc_hid').value).trim();
				var venc = fechaVenc.substring(3,5) + '/' + fechaVenc.substring(0,2) + '/' + fechaVenc.substring(6,10);
				if((Date.parse(fechaPago)) != (Date.parse(venc))){
					Ext.MessageBox.alert('Mensaje...','La �ltima fecha de pago debe coincidir con la fecha de vencimiento.',
						function(){
							gridPlanPagosCap.startEditing(i, 1);
							return;
						}
					);
					exito = false;
					break;
				}
			}

			df_pago.push((new Date(records[i].data.FECHA_PAGO)).format('d/m/Y'));
			fn_monto_pago.push(records[i].data.MONTO);

		}

		// Comparo el monto total
		var fn_monto    = (Ext.getDom('fn_monto').value).trim();
		var monto_total = (Ext.getCmp('monto_id').getValue()).replace(',','');
		//console.log('fn_monto: ' + fn_monto);
		//console.log('monto_total: ' + monto_total);
		if(parseFloat(fn_monto) != parseFloat(monto_total)){
			Ext.Msg.alert('Mensaje... ', 'El total de los montos es diferente al Monto Total Requerido, favor de corregirlo.');
			exito = false;
		}

		if(exito == true){
			Ext.Ajax.request({
				url: '22forma07pp1ext.data.jsp',
				params: Ext.apply({
					informacion:        'Aceptar',
					nombre_ejecutivo:   (Ext.getDom('nombre_ejecutivo').value).trim(),
					cg_mail:            (Ext.getDom('cg_mail').value).trim(),
					cg_telefono:        (Ext.getDom('cg_telefono').value).trim(),
					cg_fax:             (Ext.getDom('cg_fax').value).trim(),
					ejecutivo_nafin:    (Ext.getDom('ejecutivo_nafin').value).trim(),
					ig_tipo_piso:       (Ext.getDom('ig_tipo_piso').value).trim(),
					cg_razon_social_if: (Ext.getDom('cg_razon_social_if').value).trim(),
					ig_riesgo_if:       (Ext.getDom('ig_riesgo_if').value).trim(),
					cg_acreditado:      (Ext.getDom('cg_acreditado').value).trim(),
					ig_valor_riesgo_ac: (Ext.getDom('ig_valor_riesgo_ac').value).trim(),
					df_solicitud:       (Ext.getDom('df_solicitud').value).trim(),
					df_cotizacion:      (Ext.getDom('df_cotizacion').value).trim(),
					cg_programa:        (Ext.getDom('cg_programa').value).trim(),
					ic_moneda:          (Ext.getDom('ic_moneda').value).trim(),
					fn_monto:           (Ext.getDom('fn_monto').value).trim(),
					ic_tipo_tasa:       (Ext.getDom('ic_tipo_tasa').value).trim(),
					in_num_disp:        (Ext.getDom('in_num_disp').value).trim(),
					ig_plazo_conv:      (Ext.getDom('ig_plazo_conv').value).trim(),
					cg_ut_plazo:        (Ext.getDom('cg_ut_plazo').value).trim(),
					ig_plazo:           (Ext.getDom('ig_plazo').value).trim(),
					ic_esquema_recup:   (Ext.getDom('ic_esquema_recup').value).trim(),
					cg_ut_ppi:          (Ext.getDom('cg_ut_ppi').value).trim(),
					ig_ppi:             (Ext.getDom('ig_ppi').value).trim(),
					cg_ut_ppc:          (Ext.getDom('cg_ut_ppc').value).trim(),
					ig_ppc:             (Ext.getDom('ig_ppc').value).trim(),
					pfpc:               (Ext.getDom('pfpc').value).trim(),
					pufpc:              (Ext.getDom('pufpc').value).trim(),
					idcurva:            (Ext.getDom('idcurva').value).trim(),
					id_interpolacion:   (Ext.getDom('id_interpolacion').value).trim(),
					metodoCalculo:      (Ext.getDom('metodoCalculo').value).trim(),
					ic_solic_esp:       (Ext.getDom('ic_solic_esp').value).trim(),
					str_tipo_usuario:   (Ext.getDom('str_tipo_usuario').value).trim(),
					ic_ejecutivo:       (Ext.getDom('ic_ejecutivo').value).trim(),
					ig_plazo_max_oper:  '0',//(Ext.getDom('ig_plazo_max_oper').value).trim(),// No se por qu� en la versi�n original, esto se convierte en cero
					ig_num_max_disp:    '0',//(Ext.getDom('ig_num_max_disp').value).trim(),// No se por qu� en la versi�n original, esto se convierte en cero
					ig_plazo_conv_ant:  (Ext.getDom('ig_plazo_conv_ant').value).trim(),
					num_disp_capt:      (Ext.getDom('num_disp_capt').value).trim(),
					fn_monto_grid:      (Ext.getDom('fn_monto_grid').value).trim(),
					disposicion_grid:   (Ext.getDom('disposicion_grid').value).trim(),
					vencimiento_grid:   (Ext.getDom('vencimiento_grid').value).trim(),
					df_pago:            df_pago.toString(),
					fn_monto_pago:      fn_monto_pago.toString(),
					pagos:              (Ext.getDom('pagos').value).trim()
				}),
				callback: procesarDatosAceptar
			});
		}
	}

	var procesarDatosAceptar = function(opts, success, response){

		var fp = Ext.getCmp('formaPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
		
			if(Ext.util.JSON.decode(response.responseText).mensaje == ''){
				ic_proc_pago = Ext.util.JSON.decode(response.responseText).ic_proc_pago;
				procesoDireccionaPagina('22forma07Bext.jsp');
			} else{
				Ext.MessageBox.alert('Error...', 
				'' + Ext.util.JSON.decode(response.responseText).mensaje,
				procesoDireccionaPagina('22forma07Aext.jsp'));
				}

		} else{
			Ext.MessageBox.alert('Error...', '' + Ext.util.JSON.decode(response.responseText).mensaje/*,procesoDireccionaPagina('22forma07Aext.jsp')*/);
		}
		fp.el.unmask();
	}

	/********** Se crea el store del Grid Plan de Pagos Capital **********/
	var storePlanPagosCap = new Ext.data.JsonStore({
		root: 'registros',
		url:  '22forma07pp1ext.data.jsp',
		baseParams: {
			informacion: 'Consulta_Tabla_Amortizacion'
		},
		fields: [
			{ name: 'NUM_PAGO'   },
			{ name: 'FECHA_PAGO', type: 'date', convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y'));} },
			{ name: 'MONTO'      }
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			load: function(store, records, response){
				/*
				 * Originalmente el monto total se obten�a del jsp y se tra�a en la siguiente variable
				 * Ext.getCmp('monto_id').setValue(store.reader.jsonData.sumaTotal);
				 * Pero como tengo diferencias en los decimales al comparar los montos, ahora la suma se hace en javascript
				 */
				var record = storePlanPagosCap.getRange();
				var suma = 0;
				for (var i = 0; i < record.length; i++){
					suma += parseFloat(record[i].data.MONTO);
				}
				//console.log('suma ' + suma.toFixed(2)); //DEBUG
				Ext.getCmp('monto_id').setValue(suma.toFixed(2));
			},
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/********** Se crea el grid Plan de Pagos Capital **********/
	var gridPlanPagosCap = new Ext.grid.EditorGridPanel({
		width:              600,
		store:              storePlanPagosCap,
		id:                 'gridPlanPagosCap',
		margins:            '20 0 0 0',
		style:              'margin:0 auto;',
		align:              'center',
		autoHeight:         true,
		hidden:             false,
		displayInfo:        true,
		loadMask:           true,
		stripeRows:         true,
		frame:              true,
		boder:              true,
		columns: [{
			width:           195,
			header:          'N�mero de Pago',
			dataIndex:       'NUM_PAGO',
			align:           'center',
			sortable:        false,
			resizable:       false
		},{
			width:           200,
			xtype:           'datecolumn',
			header:          'Fecha de Pago',
			dataIndex:       'FECHA_PAGO',
			align:           'center',
			format:          'd/m/Y',
			sortable:        false,
			resizable:       false,
			editor:          new fm.DateField({
				minValue:     '01/01/1901',
				//disabledDays: [0, 6],
				allowBlank:   false
			})
		},{
			width:           200,
			header:          'Monto',
			dataIndex:       'MONTO',
			align:           'center',
			sortable:        false,
			resizable:       false,
			editor: {
				xtype:        'textfield',
				regex:        /^([0-9])*[.]?[0-9]{2}$/ ,
				allowBlank:   false
			}
		}],
		listeners: {
			afteredit: function(e){
				var records = e.record.store.getRange();
				var montoTotal = 0;
				for (var i = 0; i < records.length; i++){
					montoTotal += parseFloat(records[i].data.MONTO);
				}
				if(montoTotal == 0){
					Ext.getCmp('monto_id').setValue('0.00');
				} else{
					var mt = montoTotal.toFixed(2);
					Ext.getCmp('monto_id').setValue(mt);
				}
			}
		}
	});

	/********** Se crea el form principal **********/
	var formaPrincipal = new Ext.form.FormPanel({
		width:            615,
		id:               'formaPrincipal',
		title:            'Tabla de Amortizaci�n / Esquema: Plan de Pagos',
		layout:           'form',
		style:            'margin: 0 auto;',
		labelAlign:       'right',
		frame:            true,
		hidden:           false,
		border:           true,
		autoHeight:       true,
		items: [gridPlanPagosCap, {
			xtype:         'compositefield',
			msgTarget:     'side',
			combineErrors: false,
			items: [{
				width:      215,
				xtype:      'displayfield',
				value:      '&nbsp;'
			},{
				width:      70,
				xtype:      'displayfield',
				value:      'Monto total:'
			},{
				width:      185,
				xtype:      'textfield',
				id:         'monto_id',
				name:       'monto',
				msgTarget:  'side',
				readOnly:   true,
				allowBlank: false
			}]
		},{
				width:      150,
				xtype:      'datefield',
				id:         'df_disposicion_id',
				name:       'df_disposicion',
				hidden:     true
		}],
		buttons: [{ 
				xtype:      'button',
				text:       'Continuar',
				id:         'btnContinuar',
				iconCls:    'icoActualizar',
				handler:    function(boton, evento){continuar(storePlanPagosCap);}
			},{ 
				xtype:      'button',
				text:       'Limpiar',
				id:         'btnLimpiar',
				iconCls:    'icoLimpiar',
				handler:    limpiar
			},{
				xtype:      'button',
				text:       'Regresar', 
				id:         'btnRegresar',
				iconCls:    'icoRegresar',
				handler:    function(boton, evento){procesoDireccionaPagina('22forma07Aext.jsp');}
			},{
				xtype:      'button',
				text:       'Cargar Archivo',
				id:         'btnCargarArchivo',
				iconCls:    'icoModificar',
				handler:    function(boton, evento){procesoDireccionaPagina('22forma07pp2ext.jsp');}
			}]
	});

	/***** Contenedor Principal *****/
	var pnl = new Ext.Container({
		width:   949,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin: 0 auto;',
		items: [
			NE.util.getEspaciador(20),
			formaPrincipal
		]
	});

	/***** Inicializaci�n *****/
	procesoInicializar();

});