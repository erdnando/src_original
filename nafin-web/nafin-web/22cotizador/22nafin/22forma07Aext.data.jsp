<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		java.math.*,
		com.netro.cotizador.*,
		com.netro.cotizador.Solicitud,
		netropology.utilerias.usuarios.Usuario,
		java.util.Date"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%! 
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
String informacion        = request.getParameter("informacion")        == null ? "": (String)request.getParameter("informacion");
String nombre_ejecutivo   = request.getParameter("nombre_ejecutivo")   == null ? "": (String)request.getParameter("nombre_ejecutivo");
String cg_mail            = request.getParameter("cg_mail")            == null ? "": (String)request.getParameter("cg_mail");
String cg_telefono        = request.getParameter("cg_telefono")        == null ? "": (String)request.getParameter("cg_telefono");
String cg_fax             = request.getParameter("cg_fax")             == null ? "": (String)request.getParameter("cg_fax");
String ejecutivo_nafin    = request.getParameter("ejecutivo_nafin")    == null ? "": (String)request.getParameter("ejecutivo_nafin");
String ig_tipo_piso       = request.getParameter("ig_tipo_piso")       == null ? "": (String)request.getParameter("ig_tipo_piso");
String cg_razon_social_if = request.getParameter("cg_razon_social_if") == null ? "": (String)request.getParameter("cg_razon_social_if");
String ig_riesgo_if       = request.getParameter("ig_riesgo_if")       == null ? "": (String)request.getParameter("ig_riesgo_if");
String cg_acreditado      = request.getParameter("cg_acreditado")      == null ? "": (String)request.getParameter("cg_acreditado");
String ig_valor_riesgo_ac = request.getParameter("ig_valor_riesgo_ac") == null ? "": (String)request.getParameter("ig_valor_riesgo_ac");
String df_solicitud       = request.getParameter("df_solicitud")       == null ? "": (String)request.getParameter("df_solicitud");
String df_cotizacion      = request.getParameter("df_cotizacion")      == null ? "": (String)request.getParameter("df_cotizacion");
String cg_programa        = request.getParameter("cg_programa")        == null ? "": (String)request.getParameter("cg_programa");
String ic_moneda          = request.getParameter("ic_moneda")          == null ? "1":(String)request.getParameter("ic_moneda");
String fn_monto           = request.getParameter("fn_monto")           == null ? "": (String)request.getParameter("fn_monto");
String ic_tipo_tasa       = request.getParameter("ic_tipo_tasa")       == null ? "": (String)request.getParameter("ic_tipo_tasa");
String in_num_disp        = request.getParameter("in_num_disp")        == null ? "0":(String)request.getParameter("in_num_disp");
String ig_plazo           = request.getParameter("ig_plazo")           == null ? "": (String)request.getParameter("ig_plazo");
String ig_plazo_conv      = request.getParameter("ig_plazo_conv")      == null ? "": (String)request.getParameter("ig_plazo_conv");
String ig_plazo_conv_ant  = request.getParameter("ig_plazo_conv_ant")  == null ? "": (String)request.getParameter("ig_plazo_conv_ant");
String cg_ut_plazo        = request.getParameter("cg_ut_plazo")        == null ? "": (String)request.getParameter("cg_ut_plazo");
String ic_esquema_recup   = request.getParameter("ic_esquema_recup")   == null ? "": (String)request.getParameter("ic_esquema_recup");
String cg_ut_ppi          = request.getParameter("cg_ut_ppi")          == null ? "": (String)request.getParameter("cg_ut_ppi");
String ig_ppi             = request.getParameter("ig_ppi")             == null ? "": (String)request.getParameter("ig_ppi");
String cg_ut_ppc          = request.getParameter("cg_ut_ppc")          == null ? "": (String)request.getParameter("cg_ut_ppc");
String ig_ppc             = request.getParameter("ig_ppc")             == null ? "": (String)request.getParameter("ig_ppc");
String pfpc               = request.getParameter("pfpc")               == null ? "": (String)request.getParameter("pfpc");
String pufpc              = request.getParameter("pufpc")              == null ? "": (String)request.getParameter("pufpc");
String idcurva            = request.getParameter("idcurva")            == null ? "": (String)request.getParameter("idcurva");
String id_interpolacion   = request.getParameter("id_interpolacion")   == null ? "": (String)request.getParameter("id_interpolacion");
String metodoCalculo      = request.getParameter("metodoCalculo")      == null ? "": (String)request.getParameter("metodoCalculo");
String ig_plazo_max_oper  = request.getParameter("ig_plazo_max_oper")  == null ? "": (String)request.getParameter("ig_plazo_max_oper");
String ig_num_max_disp    = request.getParameter("ig_num_max_disp")    == null ? "": (String)request.getParameter("ig_num_max_disp");
String ic_ejecutivo       = request.getParameter("ic_ejecutivo")       == null ? "": (String)request.getParameter("ic_ejecutivo");
String icSolicEsp         = request.getParameter("icSolicEsp")         == null ? "": (String)request.getParameter("icSolicEsp");

String fn_monto_disp      = request.getParameter("fn_monto_disp")      == null ? "": (String)request.getParameter("fn_monto_disp");
String df_disposicion     = request.getParameter("df_disposicion")     == null ? "": (String)request.getParameter("df_disposicion");
String df_vencimiento     = request.getParameter("df_vencimiento")     == null ? "": (String)request.getParameter("df_vencimiento");
String datos_grid         = request.getParameter("datos_grid")         == null ? "": (String)request.getParameter("datos_grid");
String dia_pago           = request.getParameter("dia_pago")           == null ? "": (String)request.getParameter("dia_pago");

double sumaMontos = 0;
HashMap datosGrid = new HashMap();
JSONArray regGrid = new JSONArray();

String mensaje        = "";
String consulta       = "";
String nombreArchivo  = "";
String infoRegresar   = "";
JSONObject resultado  = new JSONObject();
boolean success       = true;
int start = 0;
int limit = 0;

if(informacion.equals("Catalogo_Riesgo_Intermediario")){

	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);

	List lFilas = solCotEsp.getRiesgos("I");
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	for(int i=0; i<lFilas.size(); i++){
		List lColumnas = (List)lFilas.get(i);
		datos = new HashMap();
		datos.put("clave",lColumnas.get(0));
		datos.put("descripcion",lColumnas.get(1));
		registros.add(datos);
	}
	resultado.put("success", new Boolean(true));
	resultado.put("total", ""+ registros.size());
	resultado.put("registros", registros.toString());
	infoRegresar = resultado.toString();

} else if(informacion.equals("Catalogo_Riesgo_Acreditado")){

	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);

	List lFilas = solCotEsp.getRiesgos("A");
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	for(int i=0; i<lFilas.size(); i++){
		List lColumnas = (List)lFilas.get(i);
		datos = new HashMap();
		datos.put("clave",lColumnas.get(0));
		datos.put("descripcion",lColumnas.get(1));
		registros.add(datos);
	}
	resultado.put("success", new Boolean(true));
	resultado.put("total", ""+ registros.size());
	resultado.put("registros", registros.toString());
	infoRegresar = resultado.toString();

} else if(informacion.equals("Catalogo_Moneda")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COMCAT_MONEDA");
	cat.setCampoClave("IC_MONEDA");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setValoresCondicionIn("1,54", Integer.class);
	cat.setOrden("CD_NOMBRE");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Catalogo_Tasa_Interes")){

	String condicion = "";
	if(ic_moneda.equals("1")){
		condicion = "1";
	} else{
		condicion = "1,2";
	}
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COTCAT_TIPO_TASA");
	cat.setCampoClave("IC_TIPO_TASA");
	cat.setCampoDescripcion("CG_DESCRIPCION");
	cat.setValoresCondicionIn(condicion, Integer.class);
	cat.setOrden("CG_DESCRIPCION");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Catalogo_Rec_Capital")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COTCAT_ESQUEMA_RECUP");
	cat.setCampoClave("IC_ESQUEMA_RECUP");
	cat.setCampoDescripcion("CG_DESCRIPCION");
	cat.setOrden("CG_DESCRIPCION");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Catalogo_Periodicidad_Pago")){

	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	JSONObject jsonObj = new JSONObject();

	datos.put("clave","30");
	datos.put("descripcion","Mensual");
	registros.add(datos);
	datos = new HashMap();
	datos.put("clave","60");
	datos.put("descripcion","Bimestral");
	registros.add(datos);
	datos = new HashMap();
	datos.put("clave","90");
	datos.put("descripcion","Trimestral");
	registros.add(datos);
	datos = new HashMap();
	datos.put("clave","120");
	datos.put("descripcion","Cuatrimestral");
	registros.add(datos);
	datos = new HashMap();
	datos.put("clave","180");
	datos.put("descripcion","Semestral");
	registros.add(datos);
	datos = new HashMap();
	datos.put("clave","360");
	datos.put("descripcion","Anual");
	registros.add(datos);
	if(ic_esquema_recup.equals("2")) {
		datos = new HashMap();
		datos.put("clave","0");
		datos.put("descripcion","Al vencimiento");
		registros.add(datos);
	}
	 

	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();  

} else if(informacion.equals("Catalogo_Unidad_Tiempo_1")){

	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	JSONObject jsonObj = new JSONObject();

	if(ic_esquema_recup.equals("3")){
		datos.put("clave","30");
		datos.put("descripcion","Mensual");
		registros.add(datos);
		datos = new HashMap();
	} else{// if(!ic_esquema_recup.equals("3")){
		if(ic_esquema_recup.equals("2")){
			datos.put("clave","0");
			datos.put("descripcion","Al vencimiento");
			registros.add(datos);
			datos = new HashMap();
		}
		datos.put("clave","30");
		datos.put("descripcion","Mensual");
		registros.add(datos);
		datos = new HashMap();
		datos.put("clave","60");
		datos.put("descripcion","Bimestral");
		registros.add(datos);
		datos = new HashMap();
		datos.put("clave","90");
		datos.put("descripcion","Trimestral");
		registros.add(datos);
		datos = new HashMap();
		datos.put("clave","120");
		datos.put("descripcion","Cuatrimestral");
		registros.add(datos);
		datos = new HashMap();
		datos.put("clave","180");
		datos.put("descripcion","Semestral");
		registros.add(datos);
		datos = new HashMap();
		datos.put("clave","360");
		datos.put("descripcion","Anual");
		registros.add(datos);
	}

	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();  

} else if(informacion.equals("Catalogo_Curva")){

	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	List lFilas = solCotEsp.getCurvasUSD(ic_moneda);
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	for(int i=0; i<lFilas.size(); i++){
		List lColumnas = (List)lFilas.get(i);
		datos = new HashMap();
		datos.put("clave",       lColumnas.get(0));
		datos.put("descripcion", lColumnas.get(1));
		registros.add(datos);
	}
	resultado.put("success",   new Boolean(true)    );
	resultado.put("total",     ""+ registros.size() );
	resultado.put("registros", registros.toString() );
	infoRegresar = resultado.toString();

} else if(informacion.equals("Primera_Cotizacion")){

	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	String fechaTemporal = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

	datosGrid.put("NUM_DISP",   "1"          );
	datosGrid.put("MONTO",      "0.00"       );
	datosGrid.put("FECHA_DISP", fechaTemporal);
	datosGrid.put("FECHA_VENC", fechaTemporal);
	regGrid.add(datosGrid);

	resultado.put("total", "1"                   );
	resultado.put("regGrid", regGrid             );
	resultado.put("mensaje", mensaje             );
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Consulta_Cotizacion")){

	int i = 0;
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();

	try{

		success = true;
		SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
		Solicitud captura   = new Solicitud();

		captura = solCotEsp.consultaSolicitud(icSolicEsp);
		captura = solCotEsp.cargaDatos(iNoCliente, iNoUsuario, "CALUSD", captura);

		if(captura.getDf_cotizacion().equals("")){
			captura.setDf_cotizacion(new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()));
		}

		datos.put("nombre_ejecutivo",   captura.getNombre_ejecutivo()                        );
		datos.put("cg_mail",            captura.getCg_mail()                                 );
		datos.put("cg_telefono",        captura.getCg_telefono()                             );
		datos.put("cg_fax",             captura.getCg_fax()                                  );
		datos.put("mensaje_error",      captura.getMensajeError()                            );
		datos.put("ejecutivo_nafin",    captura.getNombre_ejecutivo()                        ); // Ejecutivo NAFIN
		datos.put("ig_tipo_piso",       captura.getIg_tipo_piso()                            );
		datos.put("cg_razon_social_if", captura.getCg_razon_social_if()                      );
		datos.put("ig_riesgo_if",       captura.getIg_riesgo_if()                            );
		datos.put("cg_acreditado",      captura.getCg_acreditado()                           );
		datos.put("ig_valor_riesgo_ac", captura.getIg_valor_riesgo_ac()                      );
		datos.put("df_solicitud",       captura.getDf_solicitud()                            );
		datos.put("df_cotizacion",      captura.getDf_cotizacion()                           ); // captura.getDf_cotizacion()
		datos.put("cg_programa",        captura.getCg_programa()                             );
		datos.put("ic_moneda",          captura.getIc_moneda()                               );
		datos.put("fn_monto",           Comunes.formatoDecimal(captura.getFn_monto(),2,false));
		if(captura.getIc_moneda().equals("1")){
			datos.put("tipo_monto",      "PESOS"                                              );
		} else{
			datos.put("tipo_monto",      "DOLARES"                                            );
		}
		if(captura.getIc_moneda().equals("1")){
			datos.put("ic_tipo_tasa",    "1"                                                  );
		} else{
			datos.put("ic_tipo_tasa",    captura.getIc_tipo_tasa()                            );
		}
		datos.put("in_num_disp",        "" + captura.getIn_num_disp()                        );
		datos.put("ig_plazo",           "" + captura.getIg_plazo()                           );
		datos.put("ig_plazo_conv",      "" + solCotEsp.getPlazoConv(captura)                 );
		datos.put("ig_plazo_conv_ant",  "" + solCotEsp.getPlazoConv(captura)                 );
		datos.put("cg_ut_plazo",        captura.getCg_ut_plazo()                             );
		datos.put("ic_esquema_recup",   captura.getIc_esquema_recup()                        );
		if(captura.getIc_esquema_recup().equals("1")){
			datos.put("esquema_recup",   "Un solo pago de capital e inter&eacute;s al vencimiento"                                                                                                        );
		} else if(captura.getIc_esquema_recup().equals("2")){
			datos.put("esquema_recup",   "Pagos iguales de capital e intereses sobre saldo insolutos (incluye el caso particular de un solo pago de capital al vencimiento e intereses peri&oacute;dicos)");
		} else if(captura.getIc_esquema_recup().equals("3")){
			datos.put("esquema_recup",   "Favor de anexar tabla de amortizaci&oacute;n incluyendo fecha de pago"                                                                                          );
		} else if(captura.getIc_esquema_recup().equals("4")){
			datos.put("esquema_recup",   "Pago de capital m&aacute;s inter&eacute;s constantes con capital creciente"                                                                                     );
		}
		datos.put("cg_ut_ppi",          captura.getCg_ut_ppi()                               );
		datos.put("ig_ppi",             captura.getIg_ppi()                                  );
		datos.put("cg_ut_ppc",          captura.getCg_ut_ppc()                               ); // REVISAR COMO SE LLENA ESTE COMBO
		datos.put("ig_ppc",             captura.getIg_ppc()                                  ); // REVISAR COMO SE LLENA ESTE COMBO
		datos.put("pfpc",               captura.getCg_pfpc()                                 );
		datos.put("pufpc",              captura.getCg_pufpc()                                );
		datos.put("id_interpolacion",   captura.getId_interpolacion()                        );
		datos.put("metodoCalculo",      captura.getMetodoCalculo()                           );
		datos.put("ig_plazo_max_oper",  "" + captura.getIg_plazo_max_oper()                  );
		datos.put("ig_num_max_disp",    "" + captura.getIg_num_max_disp()                    );
		datos.put("ic_ejecutivo",       captura.getIc_ejecutivo()                            );
		datos.put("ic_solic_esp",       icSolicEsp                                           ); 
		datos.put("str_tipo_usuario",   strTipoUsuario                                       );

		// Obtengo el valor del combo de la Curva
		List lFilas    = solCotEsp.getCurvasUSD(captura.getIc_moneda());
		List lColumnas = (List)lFilas.get(0);
		lColumnas.get(0);
		if(!captura.getIdCurva().equals("")){
			datos.put("idcurva",         captura.getIdCurva()                                 );
		} else{
			datos.put("idcurva",         lColumnas.get(0).toString()                          );
		}

		resultado.put("registro", datos);

		if(captura.getFn_monto_disp(i) != null && !captura.getFn_monto_disp(i).equals("") && !captura.getFn_monto_disp(i).equals("0.00")){
			for(i = 0; i < Integer.parseInt(captura.getIn_num_disp()); i++){
				datosGrid = new HashMap();
				datosGrid.put("FECHA_DISP", captura.getDf_disposicion(i)      );
				datosGrid.put("FECHA_VENC", captura.getDfVencimientoCalcUSD(i));
				datosGrid.put("NUM_DISP",   "" + (i+1)                        );
				datosGrid.put("MONTO",      captura.getFn_monto_disp(i)       );
				regGrid.add(datosGrid);
				sumaMontos += Double.parseDouble(captura.getFn_monto_disp(i));
			}
		}

		if(i == 0){
			i++;
			datosGrid = new HashMap();
			datosGrid.put("FECHA_DISP", captura.getDf_cotizacion());
			datosGrid.put("FECHA_VENC", "");
			datosGrid.put("NUM_DISP",   "1"                       );
			datosGrid.put("MONTO",      "" + captura.getFn_monto());
			regGrid.add(datosGrid);
			sumaMontos += captura.getFn_monto();
		}

		if(captura.getPagos()!=null&&captura.getPagos().size()>0){
			resultado.put("pagos", captura.getPagos());
		} else{
			resultado.put("pagos", "");
		}
		resultado.put("num_disp_capt", "" + i                                          );
		resultado.put("total",         "" + regGrid.size()                             );
		resultado.put("monto_total",   "$" + Comunes.formatoDecimal(sumaMontos,2,false));
		resultado.put("regGrid",       regGrid                                         );

	} catch(Exception e){
		success = false;
		mensaje = "Ocurrió un error al obtener los datos. <br>Regrese a la pagina anterior y vuelva a intentarlo";
		log.warn("Error en informacion = " + informacion + ". " + e);
	}

	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

}  else if(informacion.equals("Recarga_Pagina")){

	int i = 0;
	int numDisp = Integer.parseInt(in_num_disp);
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();

	datos.put("nombre_ejecutivo",   nombre_ejecutivo  );
	datos.put("cg_mail",            cg_mail           );
	datos.put("cg_telefono",        cg_telefono       );
	datos.put("cg_fax",             cg_fax            );
	datos.put("ejecutivo_nafin",    ejecutivo_nafin   ); // Ejecutivo NAFIN
	datos.put("ig_tipo_piso",       ig_tipo_piso      );
	datos.put("cg_razon_social_if", cg_razon_social_if);
	datos.put("ig_riesgo_if",       ig_riesgo_if      );
	datos.put("cg_acreditado",      cg_acreditado     );
	datos.put("ig_valor_riesgo_ac", ig_valor_riesgo_ac);
	datos.put("df_solicitud",       df_solicitud      );
	datos.put("df_cotizacion",      df_cotizacion     );
	datos.put("cg_programa",        cg_programa       );
	datos.put("ic_moneda",          ic_moneda         );
	datos.put("fn_monto",           fn_monto          );
	datos.put("ic_tipo_tasa",       ic_tipo_tasa      );
	datos.put("in_num_disp",        in_num_disp       );
	datos.put("ig_plazo",           ig_plazo          );
	datos.put("ig_plazo_conv",      ig_plazo_conv     );
	datos.put("ig_plazo_conv_ant",  ig_plazo_conv_ant );
	datos.put("cg_ut_plazo",        cg_ut_plazo       );
	datos.put("ic_esquema_recup",   ic_esquema_recup  );
	datos.put("cg_ut_ppi",          cg_ut_ppi         );
	datos.put("ig_ppi",             ig_ppi            );
	datos.put("cg_ut_ppc",          cg_ut_ppc         );
	datos.put("ig_ppc",             ig_ppc            );
	datos.put("pfpc",               pfpc              );
	datos.put("pufpc",              pufpc             );
	datos.put("id_interpolacion",   id_interpolacion  );
	datos.put("metodoCalculo",      metodoCalculo     );
	datos.put("ig_plazo_max_oper",  ig_plazo_max_oper );
	datos.put("ig_num_max_disp",    ig_num_max_disp   );
	datos.put("ic_ejecutivo",       ic_ejecutivo      );
	datos.put("ic_solic_esp",       icSolicEsp        ); 
	datos.put("str_tipo_usuario",   strTipoUsuario    );
	datos.put("idcurva",            idcurva           );

	if(numDisp > 0){
		String[] df_disposicionTmp = new String[numDisp];
		String[] df_vencimientoTmp = new String[numDisp];
		String[] fn_monto_dispTmp  = new String[numDisp];

		df_disposicionTmp = df_disposicion.split(",");
		df_vencimientoTmp = df_vencimiento.split(",");
		fn_monto_dispTmp  = fn_monto_disp.split(",");

		for(i = 0; i < numDisp; i++){
			datosGrid = new HashMap();
			datosGrid.put("NUM_DISP",   "" + (i+1)          );
			datosGrid.put("MONTO",      fn_monto_dispTmp[i] );
			datosGrid.put("FECHA_DISP", df_disposicionTmp[i]);
			datosGrid.put("FECHA_VENC", df_vencimientoTmp[i]);
			regGrid.add(datosGrid);
			if(!fn_monto_dispTmp[i].equals("")) {
				sumaMontos += Double.parseDouble(fn_monto_dispTmp[i]);
			}
		}
	}

	if(i == 0){
		i++;
		datosGrid = new HashMap();
		datosGrid.put("NUM_DISP",   "1"          );
		datosGrid.put("MONTO",      fn_monto     );
		datosGrid.put("FECHA_DISP", df_cotizacion);
		datosGrid.put("FECHA_VENC", ""           );
		regGrid.add(datosGrid);
		sumaMontos = Double.parseDouble(fn_monto);
	}

	resultado.put("regGrid", regGrid);
	resultado.put("num_disp_capt", "" + i);
	resultado.put("total", "" + regGrid.size());
	resultado.put("monto_total", "$" + Comunes.formatoDecimal(sumaMontos,2,false));
	resultado.put("registro", datos);
	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Recarga_Cotizacion")){

	int i = 0;
	String datosJson = datos_grid;
	JSONArray arrayRegNuevo = JSONArray.fromObject(datosJson);
	String fechaTmp = "";

	try{
		success = true;
		SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
		Solicitud solicitud = new Solicitud();

		solicitud.setCg_ut_plazo(cg_ut_plazo);
		solicitud.setDf_cotizacion(df_cotizacion);
		solicitud.setIg_plazo_conv(Integer.parseInt(ig_plazo_conv));
		solicitud.setIg_plazo_conv_ant(Integer.parseInt(ig_plazo_conv_ant));
		solicitud.setIg_plazo(ig_plazo);
		solicitud.setIn_num_disp(in_num_disp);
		solicitud.setFn_monto(Double.parseDouble(fn_monto));

		if(arrayRegNuevo.size() > 0){
	
			String[] fecha_disposicion = new String[arrayRegNuevo.size()];
			String[] fecha_vencimiento = new String[arrayRegNuevo.size()];
			String[] monto_disposicion = new String[arrayRegNuevo.size()];

			for(i=0; i<arrayRegNuevo.size(); i++){

				JSONObject auxiliar = arrayRegNuevo.getJSONObject(i);
				if(in_num_disp.equals("1") && (auxiliar.getString("MONTO")).equals("0.00")){
					monto_disposicion[i] = fn_monto;
				} else{
					monto_disposicion[i] = auxiliar.getString("MONTO");
				}
				fechaTmp = auxiliar.getString("FECHA_DISP");
				if(!fechaTmp.equals("") && fechaTmp.length() > 9){
				fechaTmp = fechaTmp.substring(8,10) + "/" + fechaTmp.substring(5,7) + "/" + fechaTmp.substring(0,4);
				}
				fecha_disposicion[i] = fechaTmp;
				fechaTmp = "";
				fechaTmp = auxiliar.getString("FECHA_VENC");
				if(!fechaTmp.equals("") && fechaTmp.length() > 9){
					fechaTmp = fechaTmp.substring(8,10) + "/" + fechaTmp.substring(5,7) + "/" + fechaTmp.substring(0,4);
				}
				fecha_vencimiento[i] = fechaTmp;
				
			}
			solicitud.setFn_monto_disp(monto_disposicion);
			solicitud.setDf_disposicion(fecha_disposicion);
			solicitud.setDf_vencimiento(fecha_vencimiento);

		} else{
			String[] monto_disposicion = new String[1];
			String[] fecha_disposicion = new String[1];
			String[] fecha_vencimiento = new String[1];
			fecha_disposicion[0] = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			fecha_vencimiento[0] = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			monto_disposicion[0] = fn_monto;
			solicitud.setFn_monto_disp(monto_disposicion);
			solicitud.setDf_disposicion(fecha_disposicion);
			solicitud.setDf_vencimiento(fecha_vencimiento);
		}
		i = 0;
		if(solicitud.getFn_monto_disp(0) != null){// && !solicitud.getFn_monto_disp(0).equals("") && !solicitud.getFn_monto_disp(i).equals("0.00")){
			for(i = 0; i < Integer.parseInt(solicitud.getIn_num_disp()); i++){
				datosGrid = new HashMap();
				datosGrid.put("NUM_DISP",   "" + (i+1)                          );
				datosGrid.put("MONTO",      solicitud.getFn_monto_disp(i)       );
				datosGrid.put("FECHA_DISP", solicitud.getDf_disposicion(i)      );
				datosGrid.put("FECHA_VENC", solicitud.getDfVencimientoCalcUSD(i));
				regGrid.add(datosGrid);			
				if(!solicitud.getFn_monto_disp(i).equals("") ) {
					sumaMontos += Double.parseDouble(solicitud.getFn_monto_disp(i));
				}			
			}
		}

		if(i == 0){
			i++;
			datosGrid = new HashMap();
			datosGrid.put("NUM_DISP",   "1");
			datosGrid.put("MONTO",      "" + solicitud.getFn_monto());
			datosGrid.put("FECHA_DISP", solicitud.getDf_cotizacion());
			datosGrid.put("FECHA_VENC", "");
			regGrid.add(datosGrid);
			sumaMontos += solicitud.getFn_monto();
		}

		resultado.put("regGrid", regGrid);
		resultado.put("total", "" + regGrid.size());
		if(solicitud.getDf_disposicion(0) != null && solicitud.getDf_vencimiento(0) != null){
			resultado.put("ig_plazo_conv", "" + solCotEsp.getPlazoConv(solicitud));
		} else{
			resultado.put("ig_plazo_conv", ig_plazo_conv);
		}
		resultado.put("ig_plazo", solicitud.getIg_plazo());
		resultado.put("cg_ut_plazo", solicitud.getCg_ut_plazo());
		resultado.put("ig_plazo_conv_ant", "" + solCotEsp.getPlazoConv(solicitud));
		resultado.put("num_disp_capt", "" + i);
		resultado.put("monto_total", "$" + Comunes.formatoDecimal(sumaMontos,2,false));
	} catch(Exception e){
		success = false;
		mensaje = "Error al actualizar los datos: " + e;
		log.warn("Error en Recarga_Cotizacion: " + e);
	}

	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Captura_Solicitud_Fechas")){

	try{
		SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
		Solicitud captura = new Solicitud();

		captura.setNombre_ejecutivo(nombre_ejecutivo);
		captura.setIg_plazo_max_oper(ig_plazo_max_oper);
		captura.setIg_num_max_disp(ig_num_max_disp);
		captura.setIc_moneda(ic_moneda);
		
		captura = solCotEsp.cargaDatos(iNoCliente,iNoUsuario,strTipoUsuario,captura);
		
		boolean flag = false;

		/***** Asigno los datos que vienen del grid de disposiciones *****/
		String[] df_disposicionArray = null;
		String[] df_vencimientoArray = null;
		String[] fn_monto_dispArray  = null;

		df_disposicionArray = fn_monto_disp.split(",");
		df_vencimientoArray = df_disposicion.split(",");
		fn_monto_dispArray  = df_vencimiento.split(",");

		captura.setFn_monto_disp(fn_monto_dispArray);
		captura.setDf_disposicion(df_disposicionArray);
		captura.setDf_vencimiento(df_vencimientoArray);

		if(!dia_pago.equals("")){
			captura.setIn_dia_pago(dia_pago);
		}

		Hashtable respuesta = solCotEsp.getValidaFechasPagos(pfpc, pufpc, captura.getCg_ut_ppc());
		boolean lbOk = ((Boolean) respuesta.get("Respuesta")).booleanValue();
		int pagosCap = ((Integer)respuesta.get("Npagos")).intValue();
		pagosCap += 2;
		captura.setIg_ppc(String.valueOf(pagosCap));

		if(lbOk){
			success = true;
		} else{
			success = false;
			mensaje = "Entre las fechas capturadas no existen periodos completos de pago de capital.\n"+
					  "Favor de verificar la periodicidad definida y/o las fechas capturadas.";
		}

	} catch(Exception e){
		success = false;
		mensaje = "" + e;
		log.warn(e);
	}

	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

}
%>
<%=infoRegresar%>