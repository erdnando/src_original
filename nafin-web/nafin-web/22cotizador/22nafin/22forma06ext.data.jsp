<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		java.math.*,
		com.netro.cotizador.ConsCatalogoRiesgos,
		com.netro.cotizador.MantenimientoCotBean,
		netropology.utilerias.usuarios.Usuario"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion   = request.getParameter("informacion") == null?"": (String)request.getParameter("informacion");
String operacion     = request.getParameter("operacion")   == null?"": (String)request.getParameter("operacion");
String tipo_riesgo   = request.getParameter("tipo")        == null?"": (String)request.getParameter("tipo");
String id_riesgo     = request.getParameter("id_riesgo")   == null?"": (String)request.getParameter("id_riesgo");
String desc_riesgo   = request.getParameter("desc_riesgo") == null?"": (String)request.getParameter("desc_riesgo");
String tipoRiesgo    = "";
String accion        = "";
String mensaje       = "";
String consulta      = "";
String infoRegresar  = "";
JSONObject resultado = new JSONObject();

MantenimientoCotBean cotBean  = new MantenimientoCotBean();
ConsCatalogoRiesgos paginador = new ConsCatalogoRiesgos();

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("Consulta_Datos_Grid")){

	int start = Integer.parseInt(request.getParameter("start"));
	int limit = Integer.parseInt(request.getParameter("limit"));
	
	paginador.setIdTipo(id_riesgo);
	paginador.setTipoRiesgo(tipo_riesgo);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	try{
		if (operacion.equals("Generar")) { //Nueva consulta
			queryHelper.executePKQuery(request);
		}	
		consulta = queryHelper.getJSONPageResultSet(request,start,limit);
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
		resultado = JSONObject.fromObject(consulta);
		infoRegresar = resultado.toString();

} else if(informacion.equals("Guardar_Cambios")){

	if(id_riesgo.equals("")){ // Si la variable está vacia, entonces los datos se insertan
		try{
			if(cotBean.insertaCatalogo("", desc_riesgo, tipo_riesgo)){
				mensaje = "Su información se guardó con éxito.";
				accion = "exito";
			} else{
				mensaje = "Ocurrió un error al intentar guardar la información.";
				accion = "error";
			}
		} catch(Exception e){
			log.warn("Ocurrió un error al insertar la información. " + e);
			mensaje = "Ocurrió un error al intentar guardar la información. " + e;
			accion = "error";
		}
	} else{ // En caso contrario, los datos se actualizan
		try{
			if(cotBean.insertaCatalogo(id_riesgo, desc_riesgo, tipo_riesgo)){
				mensaje = "Los cambios fueron guardados con éxito";
				accion = "exito";
			} else{
				mensaje = "Ocurrió un error al intentar guardar los cambios.";
				accion = "error";
			}
		} catch(Exception e){
			log.warn("Ocurrió un error aal actualizar los cambios. " + e);
			mensaje = "Ocurrió un error al intentar guardar los cambios. " + e;
			accion = "error";
		}
	}

	resultado.put("success", new Boolean(true));
	resultado.put("accion",  accion           );
	resultado.put("mensaje", mensaje          );
	infoRegresar = resultado.toString();

}
%>
<%=infoRegresar%>