<%@ page language="java" 
contentType="application/json;charset=UTF-8"
import="java.util.*,
	com.netro.cotizador.*,
	com.netro.exception.*,
	javax.naming.*,
	org.apache.commons.logging.Log,
	net.sf.json.JSONArray,  
	net.sf.json.JSONObject,
	com.netro.model.catalogos.*,
	netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%> 

<%@ include file="/appComun.jspf" %>

<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%

//Parametros 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
//Parametor de consulta
String tipo = (request.getParameter("cboTipoBitacora")!=null)?request.getParameter("cboTipoBitacora"):""; 
String moneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):""; 

String curva = (request.getParameter("cboCurva")!=null)?request.getParameter("cboCurva"):""; 
String tabla = (request.getParameter("cboTabla")!=null)?request.getParameter("cboTabla"):""; 

String fechaCargaMin = (request.getParameter("fechaCargaMin")!=null)?request.getParameter("fechaCargaMin"):""; 
String fechaCargaMax = (request.getParameter("fechaCargaMax")!=null)?request.getParameter("fechaCargaMax"):""; 

String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
JSONObject jsonObj = new JSONObject();
JSONArray registros = new JSONArray();
String estadoSiguiente = null;

int  start= 0, limit =0;

HashMap catBit = new HashMap();


if(informacion.equals("Bitacoras.cargaCatalogoBitacora")){
	catBit = new HashMap();	
	catBit.put("clave","C");
	catBit.put("descripcion","Curva");
	registros.add(catBit);
	catBit = new HashMap();	
	catBit.put("clave","T");
	catBit.put("descripcion","Tabla");
	registros.add(catBit);
	
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("total",new Integer(registros.size()));
	jsonObj.put("registros",registros);
	jsonObj.put("estadoSiguienta",estadoSiguiente);
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("Bitacoras.cargaCatalogoMoneda")){
	ConsBitacoraCurvas clase = new ConsBitacoraCurvas();
	JSONArray catMoneda= clase.getCatalogoMoneda();
	estadoSiguiente =	"ESPERAR_DESICION";
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("total", new Integer(catMoneda.size()));
	jsonObj.put("registros", catMoneda);
	jsonObj.put("estadoSiguiente",estadoSiguiente);
	infoRegresar= jsonObj.toString();

}else if (informacion.equals("Bitacoras.Consultar")|| informacion.equals("Bitacoras.ArchivoCSV") || informacion.equals("Bitacoras.ArchivoPDF")){
		ConsBitacoraCurvas paginador = new ConsBitacoraCurvas();
		if(tipo.equals("C")){
			paginador.setClaveCurva(curva);
		}else if(tipo.equals("T")){
			paginador.setClaveCurva(tabla);
		}
		paginador.setClaveMoneda(moneda);
		paginador.setFechaCargaMin(fechaCargaMin);
		paginador.setFechaCargaMax(fechaCargaMax);
		paginador.setTipoBit(tipo);
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
			
	
		if(informacion.equals("Bitacoras.Consultar")  ||  informacion.equals("Bitacoras.ArchivoPDF") )  {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
										
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		}	
		if(informacion.equals("Bitacoras.Consultar") ){
			try {
				if (operacion.equals("Bitacoras.Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
					consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
				jsonObj = JSONObject.fromObject(consulta);
			
		}else if(informacion.equals("Bitacoras.ArchivoPDF") ){
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}else  if(informacion.equals("Bitacoras.ArchivoCSV") )  {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}
	infoRegresar = jsonObj.toString();
}else if (informacion.equals("Bitacoras.cargaCatalogoTabla")||informacion.equals("Bitacoras.cargaCatalogoCurva")){
	ConsBitacoraCurvas clase = new ConsBitacoraCurvas();
	clase.setClaveMoneda(moneda);
	clase.setTipoBit(tipo);
	JSONArray catTabla =clase.getDataCurvaTabla();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("total", new Integer(catTabla.size()));
	jsonObj.put("registros", catTabla);
	infoRegresar= jsonObj.toString();
	

}else if (informacion.equals("")){

}else if (informacion.equals("")){

}else if (informacion.equals("")){

}else if (informacion.equals("")){

}else if (informacion.equals("")){

}

	log.debug("infoRegresar = <" + infoRegresar + ">"); 
%>

<%=infoRegresar%>