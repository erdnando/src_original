<%@ page 
contentType=
		"application/json;charset=UTF-8"
		
	import="
		java.util.*,
		java.io.*,
		java.util.zip.*,
		com.jspsmart.upload.*,
		com.netro.cadenas.AltaArchivos,
		com.netro.cotizador.*,
		com.netro.exception.*, 
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,
		org.apache.commons.logging.Log  " 
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%    
	String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
	String infoRegresar	= "";
	String consulta   	= "";
	String idArchivo 		= "";
	String archivo 		= "";
	String desc 			= "";
	Vector vecFilas 		= null;
	Vector vecColumnas 	= null;
	int 	 tarchivo		= 0;
	boolean valida 		= true;
	boolean existe 		= true;
	if(informacion.equals("subirArchivo")){
		try {
			String path=strDirTrabajo+"archivos/";
			MantenimientoCot mantCot = ServiceLocator.getInstance().lookup("MantenimientoCotEJB",MantenimientoCot.class);
			
			String myContentType = "text/html;charset=UTF-8";
			response.setContentType(myContentType						);
			request.setAttribute("myContentType", myContentType	);
			com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
			myUpload.initialize(pageContext);
			myUpload.upload();
			myUpload.save(path);
			com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
			String nombreArch = myFile.getFileName();
			String tipoArchivo = myFile.getFileExt();	
			tarchivo = myFile.getSize();
			desc = myUpload.getRequest().getParameter("Desc_Archivo");
			idArchivo = myUpload.getRequest().getParameter("claveArc");
			archivo= myFile.getFileName();
			String[] newarchivo = archivo.split(" ");
			int espacios=0;
			for (int z=0; z<newarchivo.length; z++){
				espacios++;
			}	
			if(espacios<2){
			if (tarchivo >0){	
				if(!"".equals(idArchivo)){
					if(!myFile.isMissing()){
						vecFilas = mantCot.consultaArchivo(idArchivo);	
						vecColumnas = (Vector)vecFilas.get(0);
						archivo = (String)vecColumnas.get(1);
						java.io.File f = new java.io.File(path+archivo);
						valida = f.delete();
						myFile.saveAs(path + myFile.getFileName());
					}
					valida = true;
				} else {
					archivo= myFile.getFileName();		
					myFile.saveAs(path + myFile.getFileName());
					valida = true;
				}
			} else
				existe = false;
		  }//fin espacios
			JSONObject	 jsonObj 	= new JSONObject();
			if	(!existe)
				consulta = 	"{\"success\": true,\"valida\":\"cero\" }";
			else if(espacios > 1)
				consulta = 	"{\"success\": true,\"valida\":\"espacios\" }";
			else if(valida)
				consulta = 	"{\"success\": true,\"valida\":\"ok\",\"carga\":\"ok\" }";
			else 
				consulta = 	"{\"success\": true,\"valida\":\"er\" }";
				
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString(); System.err.println(infoRegresar);
		} catch (Exception e){
			log.error("Error : "+e);
			JSONObject	 jsonObj 	= new JSONObject();
			consulta = 	"{\"success\": true,\"valida\":\"ruta\" }";
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString(); System.err.println(infoRegresar);
		}


	} else if (informacion.equals("carga") )	{
		JSONObject	 jsonObj 	= new JSONObject();
		AltaArchivos carga 		= new AltaArchivos();	
		Registros 	 reg			= carga.carga();
		jsonObj.put("total",new Integer( reg.getNumeroRegistros()) );	
		jsonObj.put("registros", reg.getJSONData() );	
		infoRegresar = jsonObj.toString();
		
	} else if (informacion.equals("Eliminar") )	{
		String id	= (request.getParameter("id")	!=null)	?	request.getParameter("id")	:	"";
		MantenimientoCot mantCot = ServiceLocator.getInstance().lookup("MantenimientoCotEJB",MantenimientoCot.class);
		String path=strDirTrabajo+"archivos/";
		String nom_archivo = mantCot.eliminaArchivo(id);
		java.io.File f = new java.io.File(path+nom_archivo);
		valida = f.delete();
		JSONObject	 jsonObj 	= new JSONObject();
		consulta = 	"{\"success\": true,\"valida\":\"ok\" }";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("NuevoArchivo")){
		String path=strDirTrabajo+"archivos/";
		String Desc_Archivo = (request.getParameter("Desc_Archivo")!=null)?request.getParameter("Desc_Archivo"):"";
		String claveArc = (request.getParameter("claveArc")!=null)?request.getParameter("claveArc"):"";
		String nombreArchivo = (request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";
		try {
		MantenimientoCot mantCot = ServiceLocator.getInstance().lookup("MantenimientoCotEJB",MantenimientoCot.class);
			if(!"".equals(claveArc)){
				valida = mantCot.modificaArchivo(claveArc,nombreArchivo,Desc_Archivo,path);
			} else {
				valida = mantCot.insertaArchivo(nombreArchivo,Desc_Archivo, path);
			}
		JSONObject	 jsonObj 	= new JSONObject();
			
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString(); System.err.println(infoRegresar);
		} catch (Exception e){
			log.error("Error : "+e);
			JSONObject	 jsonObj 	= new JSONObject();
			consulta = 	"{\"success\": true,\"valida\":\"ruta\" }";
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString(); System.err.println(infoRegresar);
		}
}

%>
<%=  infoRegresar %>
