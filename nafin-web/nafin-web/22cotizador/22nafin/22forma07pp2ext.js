Ext.onReady(function(){

	/***** Variables globales *****/
	var fm = Ext.form;
	var jsonDataEncode;
	var ic_proc_pago = ''; //Esta variable es en caso de que se haya guardado correctamente el Array en el metodo guardaPagosMasiva

	/***** Carga el html de ayuda *****/
	function inicializacion(){
		Ext.Ajax.request({
			url: '22forma07pp2ext.data.jsp',
			params: Ext.apply({
				informacion: 'Panel_Ayuda'
			}),
			callback: procesarAyuda
		});
	}

	/***** Llena el panel de ayuda *****/
	function procesarAyuda(opts, success, response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var labelAviso 	= Ext.getCmp('labelAviso');
			labelAviso.setText(Ext.util.JSON.decode(response.responseText).registros,false);
		} else{
			Ext.Msg.alert('Mensaje...', 'Ocurri� un error al cargar el panel de Ayuda');
		}
	}

	/***** Muestra/Oculta el panel de ayuda *****/
	function ayuda(opcion){

		Ext.getCmp('formaResultadoValidacion').hide();
		if(opcion == 'MOSTRAR'){
			Ext.getCmp('panelAvisos').show();
			Ext.getCmp('formaPrincipal').hide();
		} else if(opcion == 'CERRAR'){
			Ext.getCmp('panelAvisos').hide();
			Ext.getCmp('formaPrincipal').show();
		}

	}

	/***** Direcciona a la p�gina indicada: 22forma07Aext.jsp, 22forma07Bext.jsp *****/
	function procesoDireccionaPagina(pantalla){
		window.location.href=pantalla                                            +
		'?opcionPantalla=RECARGAR'                                               +
		'&nombre_ejecutivo='   + (Ext.getDom('nombre_ejecutivo').value).trim()   +
		'&cg_mail='            + (Ext.getDom('cg_mail').value).trim()            +
		'&cg_telefono='        + (Ext.getDom('cg_telefono').value).trim()        +
		'&cg_fax='             + (Ext.getDom('cg_fax').value).trim()             +
		'&ejecutivo_nafin='    + (Ext.getDom('ejecutivo_nafin').value).trim()    +
		'&ig_tipo_piso='       + (Ext.getDom('ig_tipo_piso').value).trim()       +
		'&cg_razon_social_if=' + (Ext.getDom('cg_razon_social_if').value).trim() +
		'&ig_riesgo_if='       + (Ext.getDom('ig_riesgo_if').value).trim()       +
		'&cg_acreditado='      + (Ext.getDom('cg_acreditado').value).trim()      +
		'&ig_valor_riesgo_ac=' + (Ext.getDom('ig_valor_riesgo_ac').value).trim() +
		'&df_solicitud='       + (Ext.getDom('df_solicitud').value).trim()       +
		'&df_cotizacion='      + (Ext.getDom('df_cotizacion').value).trim()      +
		'&cg_programa='        + (Ext.getDom('cg_programa').value).trim()        +
		'&ic_moneda='          + (Ext.getDom('ic_moneda').value).trim()          +
		'&fn_monto='           + (Ext.getDom('fn_monto').value).trim()           +
		'&ic_tipo_tasa='       + (Ext.getDom('ic_tipo_tasa').value).trim()       +
		'&in_num_disp='        + (Ext.getDom('in_num_disp').value).trim()        +
		'&ig_plazo_conv='      + (Ext.getDom('ig_plazo_conv').value).trim()      +
		'&cg_ut_plazo='        + (Ext.getDom('cg_ut_plazo').value).trim()        +
		'&ig_plazo='           + (Ext.getDom('ig_plazo').value).trim()           +
		'&ic_esquema_recup='   + (Ext.getDom('ic_esquema_recup').value).trim()   +
		'&cg_ut_ppi='          + (Ext.getDom('cg_ut_ppi').value).trim()          +
		'&ig_ppi='             + (Ext.getDom('ig_ppi').value).trim()             +
		'&cg_ut_ppc='          + (Ext.getDom('cg_ut_ppc').value).trim()          +
		'&ig_ppc='             + (Ext.getDom('ig_ppc').value).trim()             +
		'&pfpc='               + (Ext.getDom('pfpc').value).trim()               +
		'&pufpc='              + (Ext.getDom('pufpc').value).trim()              +
		'&idcurva='            + (Ext.getDom('idcurva').value).trim()            +
		'&id_interpolacion='   + (Ext.getDom('id_interpolacion').value).trim()   +
		'&metodoCalculo='      + (Ext.getDom('metodoCalculo').value).trim()      +
		'&ic_solic_esp='       + (Ext.getDom('ic_solic_esp').value).trim()       +
		'&str_tipo_usuario='   + (Ext.getDom('str_tipo_usuario').value).trim()   +
		'&ic_ejecutivo='       + (Ext.getDom('ic_ejecutivo').value).trim()       +
		'&ig_plazo_max_oper='  + (Ext.getDom('ig_plazo_max_oper').value).trim()  +
		'&ig_num_max_disp='    + (Ext.getDom('ig_num_max_disp').value).trim()    +
		'&ig_plazo_conv_ant='  + (Ext.getDom('ig_plazo_conv_ant').value).trim()  +
		'&num_disp_capt='      + (Ext.getDom('num_disp_capt').value).trim()      +
		'&fn_monto_grid='      + (Ext.getDom('fn_monto_grid').value).trim()      +
		'&disposicion_grid='   + (Ext.getDom('disposicion_grid').value).trim()   +
		'&vencimiento_grid='   + (Ext.getDom('vencimiento_grid').value).trim()   +
		'&ic_proc_pago='       + ic_proc_pago;
	}

	/***** Proceso de carga del archivo *****/
	function cargaArchivo(){

		// Revisar si la forma es invalida
		var forma = Ext.getCmp('formaPrincipal').getForm();
		if(!forma.isValid()){
			return;
		}

		// Validar que se haya especificado un archivo
		var archivo = Ext.getCmp('archivo');
		if( Ext.isEmpty( archivo.getValue() ) ){
			archivo.markInvalid('Debe especificar un archivo');
			return;
		}

		// Revisar el tipo de extension del archivo
		var myRegexTXT = /^.+\.([tT][xX][tT])$/;
		var nombreArchivo = archivo.getValue();
		if( !myRegexTXT.test(nombreArchivo) ) {
			archivo.markInvalid('El formato del archivo de origen no es el correcto. Debe tener extensi�n TXT.');
			return;
		}

		formaPrincipal.el.mask('Cargando datos...', 'x-mask-loading');

		Ext.getCmp("formaPrincipal").getForm().submit({
			clientValidation: true,
			url: '22forma07pp2ext.data.jsp?informacion=Carga_Archivo'                +
			'&nombre_ejecutivo='   + (Ext.getDom('nombre_ejecutivo').value).trim()   +
			'&cg_mail='            + (Ext.getDom('cg_mail').value).trim()            +
			'&cg_telefono='        + (Ext.getDom('cg_telefono').value).trim()        +
			'&cg_fax='             + (Ext.getDom('cg_fax').value).trim()             +
			'&ejecutivo_nafin='    + (Ext.getDom('ejecutivo_nafin').value).trim()    +
			'&ig_tipo_piso='       + (Ext.getDom('ig_tipo_piso').value).trim()       +
			'&cg_razon_social_if=' + (Ext.getDom('cg_razon_social_if').value).trim() +
			'&ig_riesgo_if='       + (Ext.getDom('ig_riesgo_if').value).trim()       +
			'&cg_acreditado='      + (Ext.getDom('cg_acreditado').value).trim()      +
			'&ig_valor_riesgo_ac=' + (Ext.getDom('ig_valor_riesgo_ac').value).trim() +
			'&df_solicitud='       + (Ext.getDom('df_solicitud').value).trim()       +
			'&df_cotizacion='      + (Ext.getDom('df_cotizacion').value).trim()      +
			'&cg_programa='        + (Ext.getDom('cg_programa').value).trim()        +
			'&ic_moneda='          + (Ext.getDom('ic_moneda').value).trim()          +
			'&fn_monto='           + (Ext.getDom('fn_monto').value).trim()           +
			'&ic_tipo_tasa='       + (Ext.getDom('ic_tipo_tasa').value).trim()       +
			'&in_num_disp='        + (Ext.getDom('in_num_disp').value).trim()        +
			'&ig_plazo_conv='      + (Ext.getDom('ig_plazo_conv').value).trim()      +
			'&cg_ut_plazo='        + (Ext.getDom('cg_ut_plazo').value).trim()        +
			'&ig_plazo='           + (Ext.getDom('ig_plazo').value).trim()           +
			'&ic_esquema_recup='   + (Ext.getDom('ic_esquema_recup').value).trim()   +
			'&cg_ut_ppi='          + (Ext.getDom('cg_ut_ppi').value).trim()          +
			'&ig_ppi='             + (Ext.getDom('ig_ppi').value).trim()             +
			'&cg_ut_ppc='          + (Ext.getDom('cg_ut_ppc').value).trim()          +
			'&ig_ppc='             + (Ext.getDom('ig_ppc').value).trim()             +
			'&pfpc='               + (Ext.getDom('pfpc').value).trim()               +
			'&pufpc='              + (Ext.getDom('pufpc').value).trim()              +
			'&idcurva='            + (Ext.getDom('idcurva').value).trim()            +
			'&id_interpolacion='   + (Ext.getDom('id_interpolacion').value).trim()   +
			'&metodoCalculo='      + (Ext.getDom('metodoCalculo').value).trim()      +
			'&ic_solic_esp='       + (Ext.getDom('ic_solic_esp').value).trim()       +
			'&str_tipo_usuario='   + (Ext.getDom('str_tipo_usuario').value).trim()   +
			'&ic_ejecutivo='       + (Ext.getDom('ic_ejecutivo').value).trim()       +
			'&ig_plazo_max_oper='  + (Ext.getDom('ig_plazo_max_oper').value).trim()  +
			'&ig_num_max_disp='    + (Ext.getDom('ig_num_max_disp').value).trim()    +
			'&ig_plazo_conv_ant='  + (Ext.getDom('ig_plazo_conv_ant').value).trim()  +
			'&num_disp_capt='      + (Ext.getDom('num_disp_capt').value).trim()      +
			'&fn_monto_grid='      + (Ext.getDom('fn_monto_grid').value).trim()      +
			'&disposicion_grid='   + (Ext.getDom('disposicion_grid').value).trim()   +
			'&vencimiento_grid='   + (Ext.getDom('vencimiento_grid').value).trim()   +
			'&ic_proc_pago='       + ic_proc_pago,
			success: function(form, action) {
				procesaCargaArchivo(null,  true, action.response);
			},
			failure: function(form, action) {
				formaPrincipal.el.unmask();
				action.response.status = 200;
				Ext.Msg.alert('Error...', 'Ocurri� un error al cargar el archivo en el disco. ' + action.response.mensaje);
			}
		});		

	}

	/***** Si el archivo se valid� corretamente, se muestra el panel de resultados *****/
	var procesaCargaArchivo = function(opts, success, response){
		var fp = Ext.getCmp('formaPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('panelAvisos').hide();
			Ext.getCmp('formaResultadoValidacion').show();
			Ext.getCmp('resumen_id').setValue(resp.correctos);
			Ext.getCmp('errores_id').setValue(resp.errores);
			ic_proc_pago = resp.ic_proc_pago;
			if(resp.errores == ''){
				Ext.getCmp('btnAceptar').show();
			} else{
				Ext.getCmp('btnAceptar').hide();
			}
		} else{
			Ext.Msg.alert('Error...', Ext.util.JSON.decode(response.responseText).mensaje);
		}
		fp.el.unmask();
	}

	/***** Se crea el panel de ayuda *****/
	var panelAvisos = {
		width:      600,
		xtype:      'panel',
		id:         'panelAvisos',
		title:      'Ayuda',
		style:      'margin: 0 auto',
		bodyStyle:  'padding:10px',
		frame:      true,
		hidden:     true,
		autoHeight: true,
		items: [{
			xtype:   'label',
			id:      'labelAviso',
			cls:     'x-form-item',
			html:    ''
		}],
		buttons: [{
			xtype:   'button',
			text:    'Salir',
			id:      'btnSalirAyuda',
			iconCls: 'icoRegresar',
			handler: function(boton, evento){
				ayuda('CERRAR');
			}
		}]
	}

	/***** Se crea el form con los resultados de la validaci�n *****/
	var formaResultadoValidacion = new Ext.form.FormPanel({
		width:               610,
		id:                  'formaResultadoValidacion',
		title:               '&nbsp;',
		layout:              'form',
		style:               'margin: 0 auto;',
		labelAlign:          'top',
		frame:               true,
		hidden:              true,
		border:              true,
		autoHeight:          true,
		items: [{
			layout:           'column',
			items:[{
				columnWidth:   .5,
				layout:        'form',
				items: [{
					width:      290,
					height:     300,
					xtype:      'textarea',
					id:         'resumen_id',
					name:       'resumen',
					fieldLabel: 'Resumen del proceso'
				}]
			},{
				columnWidth:   .5,
				layout:        'form',
				items: [{
				width:         290,
				height:        300,
				xtype:         'textarea',
				id:            'errores_id',
				name:          'errores',
				fieldLabel:    'Detalle de pagos con errores'
				}]
			}]
		}],
		buttons: [{
			xtype:            'button',
			text:             'Aceptar',
			id:               'btnAceptar',
			iconCls:          'icoAceptar',
			handler:          function(boton, evento){procesoDireccionaPagina('22forma07Bext.jsp');}
		},{
			xtype:            'button', 
			text:             'Regresar',
			id:               'btnRegresar',
			iconCls:          'icoRegresar',
			handler:          function(boton, evento){procesoDireccionaPagina('22forma07Aext.jsp');}
		}]
	});

	/***** Se crea el form principal *****/
	var formaPrincipal = new Ext.form.FormPanel({
		width:         615,
		id:            'formaPrincipal',
		title:         'Carga masiva de tabla de amortizaci&oacute;n',
		layout:        'form',
		style:         'margin: 0 auto;',
		labelAlign:    'right',
		fileUpload:    true,
		frame:         true,
		hidden:        false,
		border:        true,
		autoHeight:    true,
		items: [{
			width:      475,
			xtype:      'fileuploadfield',
			id:         'archivo',
			name:       'archivo',
			emptyText:  'Seleccione archivo...',
			fieldLabel: 'Ruta del Archivo',
			msgTarget:  'side',
			buttonText: null,
			allowBlank: false,
			buttonCfg:  {iconCls: 'upload-icon'}
		}],
		buttons: [{
			xtype:      'button',
			text:       'Ayuda',
			id:         'btnAyuda',
			iconCls:    'icoAyuda',
			handler:    function(boton, evento){ ayuda('MOSTRAR'); }
		},{
			xtype:      'button', 
			text:       'Continuar',
			id:         'btnContinuar',
			iconCls:    'icoActualizar',
			handler:    cargaArchivo
		}]
	});

	/***** Contenedor Principal *****/
	var pnl = new Ext.Container({
		width:   949,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin: 0 auto;',
		items: [
			NE.util.getEspaciador(20),
			formaPrincipal,
			NE.util.getEspaciador(10),
			formaResultadoValidacion,
			panelAvisos
		]
	});

	inicializacion();

});