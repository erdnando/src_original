<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		java.math.*,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		com.netro.cotizador.*,
		com.netro.cotizador.Solicitud,
		netropology.utilerias.usuarios.Usuario,
		java.util.Date"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion        = request.getParameter("informacion")        == null ? "": (String)request.getParameter("informacion");
String nombre_ejecutivo   = request.getParameter("nombre_ejecutivo")   == null ? "": (String)request.getParameter("nombre_ejecutivo");
String cg_mail            = request.getParameter("cg_mail")            == null ? "": (String)request.getParameter("cg_mail");
String cg_telefono        = request.getParameter("cg_telefono")        == null ? "": (String)request.getParameter("cg_telefono");
String cg_fax             = request.getParameter("cg_fax")             == null ? "": (String)request.getParameter("cg_fax");
String ejecutivo_nafin    = request.getParameter("ejecutivo_nafin")    == null ? "": (String)request.getParameter("ejecutivo_nafin");
String ig_tipo_piso       = request.getParameter("ig_tipo_piso")       == null ? "": (String)request.getParameter("ig_tipo_piso");
String cg_razon_social_if = request.getParameter("cg_razon_social_if") == null ? "": (String)request.getParameter("cg_razon_social_if");
String ig_riesgo_if       = request.getParameter("ig_riesgo_if")       == null ? "": (String)request.getParameter("ig_riesgo_if");
String cg_acreditado      = request.getParameter("cg_acreditado")      == null ? "": (String)request.getParameter("cg_acreditado");
String ig_valor_riesgo_ac = request.getParameter("ig_valor_riesgo_ac") == null ? "": (String)request.getParameter("ig_valor_riesgo_ac");
String df_solicitud       = request.getParameter("df_solicitud")       == null ? "": (String)request.getParameter("df_solicitud");
String df_cotizacion      = request.getParameter("df_cotizacion")      == null ? "": (String)request.getParameter("df_cotizacion");
String cg_programa        = request.getParameter("cg_programa")        == null ? "": (String)request.getParameter("cg_programa");
String ic_moneda          = request.getParameter("ic_moneda")          == null ? "": (String)request.getParameter("ic_moneda");
String fn_monto           = request.getParameter("fn_monto")           == null ? "": (String)request.getParameter("fn_monto");
String ic_tipo_tasa       = request.getParameter("ic_tipo_tasa")       == null ? "": (String)request.getParameter("ic_tipo_tasa");
String in_num_disp        = request.getParameter("in_num_disp")        == null ? "": (String)request.getParameter("in_num_disp");
String ig_plazo_conv      = request.getParameter("ig_plazo_conv")      == null ? "": (String)request.getParameter("ig_plazo_conv");
String cg_ut_plazo        = request.getParameter("cg_ut_plazo")        == null ? "": (String)request.getParameter("cg_ut_plazo");
String ig_plazo           = request.getParameter("ig_plazo")           == null ? "": (String)request.getParameter("ig_plazo");
String ic_esquema_recup   = request.getParameter("ic_esquema_recup")   == null ? "": (String)request.getParameter("ic_esquema_recup");
String cg_ut_ppi          = request.getParameter("cg_ut_ppi")          == null ? "": (String)request.getParameter("cg_ut_ppi");
String ig_ppi             = request.getParameter("ig_ppi")             == null ? "": (String)request.getParameter("ig_ppi");
String cg_ut_ppc          = request.getParameter("cg_ut_ppc")          == null ? "": (String)request.getParameter("cg_ut_ppc");
String ig_ppc             = request.getParameter("ig_ppc")             == null ? "": (String)request.getParameter("ig_ppc");
String pfpc               = request.getParameter("pfpc")               == null ? "": (String)request.getParameter("pfpc");
String pufpc              = request.getParameter("pufpc")              == null ? "": (String)request.getParameter("pufpc");
String idcurva            = request.getParameter("idcurva")            == null ? "": (String)request.getParameter("idcurva");
String id_interpolacion   = request.getParameter("id_interpolacion")   == null ? "": (String)request.getParameter("id_interpolacion");
String metodoCalculo      = request.getParameter("metodoCalculo")      == null ? "": (String)request.getParameter("metodoCalculo");
String ic_solic_esp       = request.getParameter("ic_solic_esp")       == null ? "": (String)request.getParameter("ic_solic_esp");
String ic_ejecutivo       = request.getParameter("ic_ejecutivo")       == null ? "": (String)request.getParameter("ic_ejecutivo");
String ig_plazo_max_oper  = request.getParameter("ig_plazo_max_oper")  == null ? "": (String)request.getParameter("ig_plazo_max_oper");
String ig_num_max_disp    = request.getParameter("ig_num_max_disp")    == null ? "": (String)request.getParameter("ig_num_max_disp");
String ig_plazo_conv_ant  = request.getParameter("ig_plazo_conv_ant")  == null ? "": (String)request.getParameter("ig_plazo_conv_ant");
String num_disp_capt      = request.getParameter("num_disp_capt")      == null ? "": (String)request.getParameter("num_disp_capt");
String fn_monto_grid      = request.getParameter("fn_monto_grid")      == null ? "": (String)request.getParameter("fn_monto_grid");
String disposicion_grid   = request.getParameter("disposicion_grid")   == null ? "": (String)request.getParameter("disposicion_grid");
String vencimiento_grid   = request.getParameter("vencimiento_grid")   == null ? "": (String)request.getParameter("vencimiento_grid");
String pagos              = request.getParameter("pagos")              == null ? "": (String)request.getParameter("pagos");

String df_pago            = request.getParameter("df_pago")            == null ? "": (String)request.getParameter("df_pago");
String fn_monto_pago      = request.getParameter("fn_monto_pago")      == null ? "": (String)request.getParameter("fn_monto_pago");

String mensaje        = "";
String consulta       = "";
String infoRegresar   = "";
JSONObject resultado  = new JSONObject();
boolean success       = true;

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("Consulta_Tabla_Amortizacion")){

	HashMap datos       = new HashMap();
	JSONArray registros = new JSONArray();
	String dfPago       = "";
	String montoPago    = "";
	int numPagos        = 0;
	int i               = 0;

	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	Solicitud solicitud = new Solicitud();
        Solicitud consultaSol = new Solicitud();

	solicitud.setFn_monto(Double.parseDouble(fn_monto.trim()));
	solicitud.setIc_ejecutivo(ic_ejecutivo.trim());
	solicitud.setCg_telefono(cg_telefono.trim());
	solicitud.setCg_fax(cg_fax.trim());
	solicitud.setCg_mail(cg_mail.trim());
	solicitud.setNombre_ejecutivo(nombre_ejecutivo.trim());
	solicitud.setDf_solicitud(df_solicitud.trim());
	solicitud.setDf_cotizacion(df_cotizacion.trim());
	solicitud.setCg_programa(cg_programa.trim());
	solicitud.setIc_moneda(ic_moneda.trim());
	solicitud.setIc_tipo_tasa(ic_tipo_tasa.trim());
	solicitud.setIn_num_disp(in_num_disp.trim());
	solicitud.setIg_tipo_piso(ig_tipo_piso.trim());
	solicitud.setIg_plazo_max_oper(ig_plazo_max_oper.trim());
	solicitud.setIg_num_max_disp(ig_num_max_disp.trim());
	solicitud.setIg_plazo(ig_plazo.trim());
	solicitud.setIc_esquema_recup(ic_esquema_recup.trim());
	solicitud.setIg_ppi(ig_ppi.trim());
	solicitud.setCg_ut_ppi(cg_ut_ppi.trim());
	solicitud.setIg_ppc(ig_ppc.trim());
	solicitud.setCg_ut_ppc(cg_ut_ppc.trim());
	solicitud.setCg_razon_social_if(cg_razon_social_if.trim());
	solicitud.setIg_riesgo_if(ig_riesgo_if.trim());
	solicitud.setCg_acreditado(cg_acreditado.trim());
	solicitud.setIg_valor_riesgo_ac(ig_valor_riesgo_ac.trim());
	solicitud.setCg_ut_plazo(cg_ut_plazo.trim());
	solicitud.setCg_pfpc(pfpc.trim());
	solicitud.setCg_pufpc(pufpc.trim());
	solicitud.setIdCurva(idcurva.trim());
	solicitud.setMetodoCalculo(metodoCalculo);
	solicitud.setId_interpolacion(id_interpolacion);

	String[] df_disposicion = null;
	String[] df_vencimiento = null;
	String[] fn_monto_disp = null;

	df_disposicion = disposicion_grid.split(",");
	df_vencimiento = vencimiento_grid.split(",");
	fn_monto_disp  = fn_monto_grid.split(",");

	solicitud.setFn_monto_disp(fn_monto_disp);
	solicitud.setDf_disposicion(df_disposicion);
	solicitud.setDf_vencimiento(df_vencimiento);

	solicitud = solCotEsp.complementaDatos(solicitud);
        if (ic_solic_esp != null && ic_solic_esp.length() >0){ // verificar que la solicitud no sea nula o vacia		
            consultaSol  = solCotEsp.consultaSolicitud(ic_solic_esp);
            StringBuffer pagosSB= new StringBuffer();
            if((consultaSol.getPagos()).size() > 0){
                HashMap mapaPlanPagos = new HashMap();
                ArrayList alFilas = consultaSol.getPagos();
                for(int col = 0; col < alFilas.size(); col++){
                        mapaPlanPagos = new HashMap();
                        ArrayList alColumnas = (ArrayList)alFilas.get(col);
                        pagosSB.append((String)alColumnas.get(0)+","+(String)alColumnas.get(1)+","+alColumnas.get(2).toString());
                        if(alFilas.size()!=(col+1)){
                            pagosSB.append(",");
                        }
                }
            }
            pagos=pagosSB.toString();
        }
        String[] alPagos = pagos.split(",");
	numPagos = Integer.parseInt(ig_ppc);
	float sumaTotal = 0;
	String monto = "";
	if (alPagos.length>2){  
        while( i < alPagos.length){
            datos = new HashMap();
            datos.put("NUM_PAGO",   alPagos[i++]);
            datos.put("FECHA_PAGO", alPagos[i++]);
            datos.put("MONTO",      Comunes.formatoDecimal(alPagos[i++],2,false));
            if(datos.get("MONTO") != null || (""+datos.get("MONTO")).equals("")){
                monto = ("" + datos.get("MONTO")).replaceAll(",","");
                sumaTotal += Float.parseFloat(monto);
            }
            registros.add(datos);
        }        
    }
	resultado.put("success",   new Boolean(success) );
	resultado.put("mensaje",   mensaje              );
	resultado.put("registros", registros            );
	resultado.put("total",     "" + registros.size());
	resultado.put("sumaTotal", Comunes.formatoDecimal(sumaTotal,2,false));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Aceptar")){

	try{
		success = true;
		SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
		Solicitud solicitud = new Solicitud();

		// Lleno el objeto de la clase solicitud
		solicitud.setFn_monto(Double.parseDouble(fn_monto.trim()));
		solicitud.setIc_ejecutivo(ic_ejecutivo.trim());
		solicitud.setCg_telefono(cg_telefono.trim());
		solicitud.setCg_fax(cg_fax.trim());
		solicitud.setCg_mail(cg_mail.trim());
		solicitud.setNombre_ejecutivo(nombre_ejecutivo.trim());
		solicitud.setDf_solicitud(df_solicitud.trim());
		solicitud.setDf_cotizacion(df_cotizacion.trim());
		solicitud.setCg_programa(cg_programa.trim());
		solicitud.setIc_moneda(ic_moneda.trim());
		solicitud.setIc_tipo_tasa(ic_tipo_tasa.trim());
		solicitud.setIn_num_disp(in_num_disp.trim());
		solicitud.setIg_tipo_piso(ig_tipo_piso.trim());
		solicitud.setIg_plazo_max_oper(ig_plazo_max_oper.trim());
		solicitud.setIg_num_max_disp(ig_num_max_disp.trim());
		solicitud.setIg_plazo(ig_plazo.trim());
		solicitud.setIc_esquema_recup(ic_esquema_recup.trim());
		solicitud.setIg_ppi(ig_ppi.trim());
		solicitud.setCg_ut_ppi(cg_ut_ppi.trim());
		solicitud.setIg_ppc(ig_ppc.trim());
		solicitud.setCg_ut_ppc(cg_ut_ppc.trim());
		solicitud.setCg_razon_social_if(cg_razon_social_if.trim());
		solicitud.setIg_riesgo_if(ig_riesgo_if.trim());
		solicitud.setCg_acreditado(cg_acreditado.trim());
		solicitud.setIg_valor_riesgo_ac(ig_valor_riesgo_ac.trim());
		solicitud.setCg_ut_plazo(cg_ut_plazo.trim());
		solicitud.setIg_plazo(ig_plazo_conv.trim());
		solicitud.setCg_pfpc(pfpc.trim());
		solicitud.setCg_pufpc(pufpc.trim());
		solicitud.setIdCurva(idcurva.trim());

		if(!disposicion_grid.equals("") && !vencimiento_grid.equals("") && !fn_monto_grid.equals("")){
			String[] df_disposicion = null;
			String[] df_vencimiento = null;
			String[] fn_monto_disp  = null;
	
			df_disposicion = disposicion_grid.split(",");
			df_vencimiento = vencimiento_grid.split(",");
			fn_monto_disp  = fn_monto_grid.split(",");

			solicitud.setFn_monto_disp(fn_monto_disp);
			solicitud.setDf_disposicion(df_disposicion);
			solicitud.setDf_vencimiento(df_vencimiento);
		}

		solicitud = solCotEsp.cargaDatos(iNoCliente, iNoUsuario, strTipoUsuario, solicitud);

		solicitud.setId_interpolacion(id_interpolacion);
		solicitud.setMetodoCalculo(metodoCalculo);

		String[] arrFechas    = null;
		String[] arrMontoPago = null;
		arrFechas = df_pago.split(",");
		arrMontoPago = fn_monto_pago.split(",");

		List diasPagarInt = new ArrayList();
		List diasPagarCap = new ArrayList();
		diasPagarInt = solCotEsp.getDiasAPagar(solicitud);
		diasPagarCap = solCotEsp.getDiasPagoPP(solicitud, arrFechas);
		int cuenta = 0;
		for(int a = 0; a < diasPagarInt.size(); a++){
			for(int b = 0; b < diasPagarCap.size(); b++){
				if(diasPagarInt.get(a).toString().compareTo(diasPagarCap.get(b).toString()) == 0){
					cuenta++;
				}
			}
		}

		if(cuenta != diasPagarCap.size()){
			mensaje = "El pago de Capital debe hacerse en fechas de pago de inter&eacute;s";
		} else{
			try{

				String[] alPagos = pagos.split(",");
				int numPagos = Integer.parseInt(ig_ppc);
				float sumaTotal = 0;
				String monto = "";
				int i = 0;
				int contador = 0;
				System.out.println(alPagos.length);
				int dimension = alPagos.length/3;
				String pagosV[] = new String[dimension];
				String fechas[] = new String[dimension];
				String montos[] = new String[dimension];
				while(i < alPagos.length){
					pagosV[contador] = "" + alPagos[i++];
					fechas[contador] = "" + alPagos[i++];
					montos[contador] = "" + alPagos[i++];
					contador++;
				}

				solicitud = solCotEsp.guardaPagos(solicitud, fechas, montos);
				resultado.put("ic_proc_pago", "" + solicitud.getIc_proc_pago());
				mensaje = "";	
			} catch(Exception e){
				log.warn("Error en 22forma07pp1ext al mandar a llamar al método guardaPagos(). " + e);
			}
		}
	} catch(Exception e){
		success = false;
		mensaje = "Error en 22forma07pp1ext. " + e;
		log.warn("Error en 22forma07pp1ext. " + e);
	}

	// Enviar resultado de la operacion
	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>