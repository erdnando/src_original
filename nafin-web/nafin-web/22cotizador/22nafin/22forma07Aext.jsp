<!DOCTYPE html>
<%@ page import="java.util.*" 
contentType="text/html;charset=windows-1252"
errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
<script type="text/javascript" src="22forma07Aext.js"></script>
</head>
<%@ include file="/01principal/menu.jspf"%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS){%>
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:230px"></div></div>
		</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
<%} else{%>
	<div id="Contcentral">
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<form id='formAux' name="formAux" target='_new'></form>
<%}%>
	<input type="hidden" id="opcionPantalla"     value="<%=(request.getParameter("opcionPantalla")     ==null)?"":request.getParameter("opcionPantalla")%>      ">
	<input type="hidden" id="nombre_ejecutivo"   value="<%=(request.getParameter("nombre_ejecutivo")   == null)?"": request.getParameter("nombre_ejecutivo")%>  ">
	<input type="hidden" id="cg_mail"            value="<%=(request.getParameter("cg_mail")            == null)?"": request.getParameter("cg_mail")%>           ">
	<input type="hidden" id="cg_telefono"        value="<%=(request.getParameter("cg_telefono")        == null)?"": request.getParameter("cg_telefono")%>       ">
	<input type="hidden" id="cg_fax"             value="<%=(request.getParameter("cg_fax")             == null)?"": request.getParameter("cg_fax")%>            ">
	<input type="hidden" id="ejecutivo_nafin"    value="<%=(request.getParameter("ejecutivo_nafin")    == null)?"": request.getParameter("ejecutivo_nafin")%>   ">
	<input type="hidden" id="ig_tipo_piso"       value="<%=(request.getParameter("ig_tipo_piso")       == null)?"": request.getParameter("ig_tipo_piso")%>      ">
	<input type="hidden" id="cg_razon_social_if" value="<%=(request.getParameter("cg_razon_social_if") == null)?"": request.getParameter("cg_razon_social_if")%>">
	<input type="hidden" id="ig_riesgo_if"       value="<%=(request.getParameter("ig_riesgo_if")       == null)?"": request.getParameter("ig_riesgo_if")%>      ">
	<input type="hidden" id="cg_acreditado"      value="<%=(request.getParameter("cg_acreditado")      == null)?"": request.getParameter("cg_acreditado")%>     ">
	<input type="hidden" id="ig_valor_riesgo_ac" value="<%=(request.getParameter("ig_valor_riesgo_ac") == null)?"": request.getParameter("ig_valor_riesgo_ac")%>">
	<input type="hidden" id="df_solicitud"       value="<%=(request.getParameter("df_solicitud")       == null)?"": request.getParameter("df_solicitud")%>      ">
	<input type="hidden" id="df_cotizacion"      value="<%=(request.getParameter("df_cotizacion")      == null)?"": request.getParameter("df_cotizacion")%>     ">
	<input type="hidden" id="cg_programa"        value="<%=(request.getParameter("cg_programa")        == null)?"": request.getParameter("cg_programa")%>       ">
	<input type="hidden" id="ic_moneda"          value="<%=(request.getParameter("ic_moneda")          == null)?"": request.getParameter("ic_moneda")%>         ">
	<input type="hidden" id="fn_monto"           value="<%=(request.getParameter("fn_monto")           == null)?"": request.getParameter("fn_monto")%>          ">
	<input type="hidden" id="ic_tipo_tasa"       value="<%=(request.getParameter("ic_tipo_tasa")       == null)?"": request.getParameter("ic_tipo_tasa")%>      ">
	<input type="hidden" id="in_num_disp"        value="<%=(request.getParameter("in_num_disp")        == null)?"": request.getParameter("in_num_disp")%>       ">
	<input type="hidden" id="ig_plazo_conv"      value="<%=(request.getParameter("ig_plazo_conv")      == null)?"": request.getParameter("ig_plazo_conv")%>     ">
	<input type="hidden" id="cg_ut_plazo"        value="<%=(request.getParameter("cg_ut_plazo")        == null)?"": request.getParameter("cg_ut_plazo")%>       ">
	<input type="hidden" id="ig_plazo"           value="<%=(request.getParameter("ig_plazo")           == null)?"": request.getParameter("ig_plazo")%>          ">
	<input type="hidden" id="ic_esquema_recup"   value="<%=(request.getParameter("ic_esquema_recup")   == null)?"": request.getParameter("ic_esquema_recup")%>  ">
	<input type="hidden" id="cg_ut_ppi"          value="<%=(request.getParameter("cg_ut_ppi")          == null)?"": request.getParameter("cg_ut_ppi")%>         ">
	<input type="hidden" id="ig_ppi"             value="<%=(request.getParameter("ig_ppi")             == null)?"": request.getParameter("ig_ppi")%>            ">
	<input type="hidden" id="cg_ut_ppc"          value="<%=(request.getParameter("cg_ut_ppc")          == null)?"": request.getParameter("cg_ut_ppc")%>         ">
	<input type="hidden" id="ig_ppc"             value="<%=(request.getParameter("ig_ppc")             == null)?"": request.getParameter("ig_ppc")%>            ">
	<input type="hidden" id="pfpc"               value="<%=(request.getParameter("pfpc")               == null)?"": request.getParameter("pfpc")%>              ">
	<input type="hidden" id="pufpc"              value="<%=(request.getParameter("pufpc")              == null)?"": request.getParameter("pufpc")%>             ">
	<input type="hidden" id="idcurva"            value="<%=(request.getParameter("idcurva")            == null)?"": request.getParameter("idcurva")%>           ">
	<input type="hidden" id="id_interpolacion"   value="<%=(request.getParameter("id_interpolacion")   == null)?"": request.getParameter("id_interpolacion")%>  ">
	<input type="hidden" id="metodoCalculo"      value="<%=(request.getParameter("metodoCalculo")      == null)?"": request.getParameter("metodoCalculo")%>     ">
	<input type="hidden" id="icSolicEsp"         value="<%=(request.getParameter("ic_solic_esp")       == null)?"": request.getParameter("ic_solic_esp")%>      ">
	<input type="hidden" id="str_tipo_usuario"   value="<%=(request.getParameter("str_tipo_usuario")   == null)?"": request.getParameter("str_tipo_usuario")%>  ">
	<input type="hidden" id="ic_ejecutivo"       value="<%=(request.getParameter("ic_ejecutivo")       == null)?"": request.getParameter("ic_ejecutivo")%>      ">
	<input type="hidden" id="ig_plazo_max_oper"  value="<%=(request.getParameter("ig_plazo_max_oper")  == null)?"": request.getParameter("ig_plazo_max_oper")%> ">
	<input type="hidden" id="ig_num_max_disp"    value="<%=(request.getParameter("ig_num_max_disp")    == null)?"": request.getParameter("ig_num_max_disp")%>   ">
	<input type="hidden" id="ig_plazo_conv_ant"  value="<%=(request.getParameter("ig_plazo_conv_ant")  == null)?"": request.getParameter("ig_plazo_conv_ant")%> ">
	<input type="hidden" id="num_disp_capt"      value="<%=(request.getParameter("num_disp_capt")      == null)?"": request.getParameter("num_disp_capt")%>     ">
	<input type="hidden" id="fn_monto_grid"      value="<%=(request.getParameter("fn_monto_grid")      == null)?"": request.getParameter("fn_monto_grid")%>     ">
	<input type="hidden" id="disposicion_grid"   value="<%=(request.getParameter("disposicion_grid")   == null)?"": request.getParameter("disposicion_grid")%>  ">
	<input type="hidden" id="vencimiento_grid"   value="<%=(request.getParameter("vencimiento_grid")   == null)?"": request.getParameter("vencimiento_grid")%>  ">
	<input type="hidden" id="fecha_disp_hid"     value="<%=(request.getParameter("fecha_disp_hid")     ==null)?"" : request.getParameter("fecha_disp_hid")%>    ">
	<input type="hidden" id="fecha_venc_hid"     value="<%=(request.getParameter("fecha_venc_hid")     ==null)?"" : request.getParameter("fecha_venc_hid")%>    ">
	<input type="hidden" id="pagos"              value="<%=(request.getParameter("pagos")              ==null)?"" : request.getParameter("pagos")%>             ">
</body>
</html>