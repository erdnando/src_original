Ext.onReady(function(){

	/********** Garga los datos en el grid **********/
	function inicializacion(idRiesgo, tipoRiesgo){
		consultaData.load({
			params: Ext.apply({
				operacion: 'Generar',
				tipo:      tipoRiesgo,
				id_riesgo: idRiesgo,
				start:     0,
				limit:     15
			})
		});
	}

	/********** Obtiene los datos del registro seleccionado en el grid **********/
	function obtieneDescripcionGrid(rec){
		Ext.getCmp('ic_riesgo_id').setValue(rec.get('ID_RIESGO'));
		Ext.getCmp('descripcion_id').setValue(rec.get('DESCRIPCION'));
		Ext.getCmp('tipo_riesgo_id').setValue(rec.get('TIPO_STR'));
		Ext.getCmp('ic_tipo_riesgo_id').setValue(rec.get('TIPO_STR'));
	}

	/********** Valida los campos y guarda los datos **********/
	function procesarGuardar(){
		var opcion = (Ext.getCmp('tipo_riesgo_id')).getValue();
		var tipoRiesgo = '';
		if(!Ext.getCmp('formaPrincipal').getForm().isValid()){
			return;
		} else{
			if(opcion == '' || opcion == null){
					Ext.getCmp('tipo_riesgo_id').markInvalid('Seleccione un tipo de riesgo');
					return;
			} else{
				tipoRiesgo = opcion.getGroupValue();
				if(tipoRiesgo == ''){
					Ext.getCmp('tipo_riesgo_id').markInvalid('Seleccione un tipo de riesgo.');
					return;
				}
			}
		}
		Ext.Msg.confirm('', '¿Confirma la modificación de los datos?',function(botonConf){
			if (botonConf == 'ok' || botonConf == 'yes'){
				var fp = Ext.getCmp('formaPrincipal');
				fp.el.mask('Procesando...', 'x-mask-loading');
				Ext.Ajax.request({
					url: '22forma06ext.data.jsp',
					params: Ext.apply({
						informacion: 'Guardar_Cambios',
						tipo:        tipoRiesgo,
						id_riesgo:   Ext.getCmp('ic_riesgo_id').getValue(),
						desc_riesgo: Ext.getCmp('descripcion_id').getValue()
					}),
					callback: procesarGuardarCambios
				});
			}
		});
	}

	/********** Muestra el resultado despues de guardar los datos **********/
	function procesarGuardarCambios(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('formaPrincipal');
			fp.el.unmask();
			if (jsonData != null){
				if(jsonData.accion == 'exito'){
					Ext.Msg.alert('Mensaje...', jsonData.mensaje);
					procesarLimpiar();
				} else{
					Ext.Msg.alert('Error...', jsonData.mensaje);
				}
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	/********** Regresa la pantalla a su condicion inicial **********/
	function procesarLimpiar(){
		Ext.getCmp('formaPrincipal').getForm().reset();
		inicializacion('', '');
	}

	/********** Proceso para generar el Grid **********/  
	var procesarConsultaData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('formaPrincipal');
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();
		fp.el.unmask();
		el.unmask();
		if (arrRegistros != null){
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0){
			} else{
			}
		}
	};

	/********** Se crea el store del Grid **********/
	var consultaData = new Ext.data.JsonStore({
		root:            'registros',
		url:             '22forma06ext.data.jsp',
		baseParams: {
			informacion:  'Consulta_Datos_Grid'
		},
		fields: [
			{ name: 'ID_RIESGO'  },
			{ name: 'DESCRIPCION'},
			{ name: 'TIPO'       },
			{ name: 'TIPO_STR'   }
		],
		totalProperty:   'total',
		messageProperty: 'mensaje',
		autoLoad:        false,
		listeners: {
			load:         procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	/********** Se crea el grid **********/
	var gridConsulta = new Ext.grid.EditorGridPanel({
		width:           600,
		id:              'gridConsulta',
		style:           'margin:0 auto;',
		align:           'center',
		frame:           false,
		border:          true,
		hidden:          false,
		displayInfo:     true,
		loadMask:        true,
		stripeRows:      true,
		autoHeight:      true,
		store:           consultaData,
		columns: [{
			width:        295,
			header:       'Descripción',
			dataIndex:    'DESCRIPCION',
			align:        'center',
			sortable:     true,
			resizable:    false
		},{
			width:        200,
			header:       'Acreditado / Intermediario',
			dataIndex:    'TIPO',
			align:        'center',
			sortable:     true,
			resizable:    false
		},{
			width:        100,
			xtype:        'actioncolumn',
			header:       'Acciones',
			align:        'center',
			sortable:     false,
			resizable:    false,
			items: [{
				iconCls: 'modificar',
				tooltip: 'Modificar',
				handler: function(gridConsulta, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					obtieneDescripcionGrid(rec);
				}
			}]
		}],
		bbar: {
			xtype:       'paging',
			id:          'barraPaginacion',
			buttonAlign: 'left',
			displayMsg:  '{0} - {1} de {2}',
			emptyMsg:    'No hay registros.',
			pageSize:    15,
			displayInfo: true,
			store:       consultaData
		}
	});

	/********** Form Panel Principal **********/
	var formaPrincipal = new Ext.form.FormPanel({
		width:           600,
		labelWidth:      100,
		id:              'formaPrincipal',
		title:           'Catálogo de Riesgo',
		layout:          'form',
		style:           'margin:0 auto;',
		frame:           true,
		border:          true,
		autoHeight:      true,
		items: [{
			width:        400,
			xtype:        'textfield',
			id:           'descripcion_id',
			name:         'descripcion',
			fieldLabel:   '&nbsp; Descripción',
			msgTarget:    'side',
			allowBlank:   false,
			maxLength:    100
		},{
			width:        400,
			xtype:        'textfield',
			id:           'ic_riesgo_id',
			name:         'ic_riesgo',
			fieldLabel:   '&nbsp; Id Riesgo',
			hidden:        true
		},{
			width:        400,
			xtype:        'textfield',
			id:           'ic_tipo_riesgo_id',
			name:         'ic_tipo_riesgo',
			fieldLabel:   '&nbsp; Tipo de riesgo',
			hidden:        true
		},{
			width:        300,
			xtype:        'radiogroup',
			id:           'tipo_riesgo_id',
			name:         'tipo_riesgo',
			align:        'center',
			cls:          'x-check-group-alt',
			msgTarget:    'side',
			allowBlank:   true,
			style: {
				width:     '70%',
				marginLeft:'30px'
			},
			items: [
				{boxLabel: 'Acreditado',    name: 'tipo_riesgo', inputValue: 'A'},
				{boxLabel: 'Intermediario', name: 'tipo_riesgo', inputValue: 'I'}
			],
			listeners: {
				change: {
					fn: function(){
						// Recarga el grid dependiendo de la opción seleccionada
						var opcion = (Ext.getCmp('tipo_riesgo_id')).getValue();
						if(opcion == '' || opcion == null){
						
						} else{
							var tipoRiesgo = opcion.getGroupValue();
							var idRiesgo = Ext.getCmp('ic_riesgo_id').getValue();
							if(Ext.getCmp('ic_tipo_riesgo_id').getValue() != idRiesgo){
								Ext.getCmp('tipo_riesgo_id').setValue(Ext.getCmp('ic_tipo_riesgo_id').getValue());
							}
						inicializacion('', tipoRiesgo);
						}
					}
				}
			}
		}],
		buttons: [{
			text:         'Guardar',
			id:           'btnGuardar',
			iconCls:      'icoAceptar',
			hidden:       false,
			handler:      procesarGuardar
		},{
			text:         'Limpiar',
			id:           'btnLimpiar',
			iconCls:      'icoRegresar',
			hidden:       false,
			handler:      procesarLimpiar
		}]
	});

	/********** Contenedor Principal **********/
	var pnl = new Ext.Container({
		width:           949,
		id:              'contenedorPrincipal',
		applyTo:         'areaContenido',
		style:           'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			gridConsulta
		]
	});

	/********** Inicialización **********/
	inicializacion('', '');
});