Ext.onReady(function(){

	/********** Variables globales **********/
	var recordType = Ext.data.Record.create([{name:'clave'}, {name:'descripcion'}]);

	/***** Descargar archivos PDF o CSV *****/
	function descargaArchivo(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			formaPrincipal.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			formaPrincipal.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnImprimir').setIconClass('icoPdf');
		Ext.getCmp('btnCotizacionPDF').setIconClass('icoPdf');
		Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
	}

	/********** Valida que el monto inicial sea menor qu el monto final **********/
	function validaMonto(){
		var montoInicio;
		var montoFin;
		if(Ext.getCmp('monto_inicio_id').getValue() !=''){
			montoInicio = parseFloat(Ext.getCmp('monto_inicio_id').getValue());
		}
		if(Ext.getCmp('monto_final_id').getValue() !=''){
			montoFin = parseFloat(Ext.getCmp('monto_final_id').getValue());
		}
		if(Ext.getCmp('monto_inicio_id').getValue() !='' && Ext.getCmp('monto_final_id').getValue() !=''){
			if(montoInicio < montoFin){
				return true;
			}else{
				return false;
			}
		} else{
			return true;
		}
	}

	/********** Muestra el form Cotizador de Cr�ditos **********/
	function procesoFormCotizador(){
		window.location.href=/*'22forma07Bext.jsp';*/'22forma07Aext.jsp?opcionPantalla=AGREGAR';
	}

	/********** Muestra el form Cotizador de Cr�ditos con los datos seleccionados del grid **********/
	function procesarModificar(rec){
		var icSolicEsp = rec.get('IC_SOLIC_ESP');
		var dfCotizacion = rec.get('FECHA_COTIZACION');
		if(dfCotizacion == ''){
			dfCotizacion = new Date().format('d/m/Y');
		}
		window.location.href='22forma07Aext.jsp?opcionPantalla=EDITAR&ic_solic_esp=' + icSolicEsp + '&df_cotizacion=' + dfCotizacion;
	}

	/********** Realiza la consulta para llenar el Grid **********/
	function procesoConsultarCotizacion(){
		if( Ext.getCmp('formaPrincipal').getForm().isValid()){
			if(!validaMonto()){
				Ext.getCmp('monto_inicio_id').markInvalid('El monto de cr�dito inicial debe se menor al monto de cr�dito final');
				Ext.getCmp('monto_final_id').markInvalid('El monto de cr�dito final debe se mayor al monto de cr�dito inicial');
			} else{
				formaPrincipal.el.mask('Procesando...', 'x-mask-loading');
				consultaData.load({
					params: Ext.apply({
						operacion    : 'generar',
						fechaInicio  : Ext.getCmp('fecha_inicio').getValue(),
						fechaFinal   : Ext.getCmp('fecha_final').getValue(),
						numSolicitud : Ext.getCmp('num_solicitud_id').getValue(),
						intermediario: Ext.getCmp('intermediario_id').getValue(),
						ejecutivo    : Ext.getCmp('ejecutivo_id').getValue(),
						moneda       : Ext.getCmp('moneda_id').getValue(),
						montoInicio  : Ext.getCmp('monto_inicio_id').getValue(),
						montoFinal   : Ext.getCmp('monto_final_id').getValue(),
						estatus      : Ext.getCmp('estatus_id').getValue(),
						start        : 0,
						limit        : 15
					})
				});
			}
		}
	}

	/********** Obtiene los datos del registro seleccionado en el grid **********/
	function procesarConsultar(rec){
		Ext.getCmp('estatus_sol_id').setValue(rec.get('IC_ESTATUS'));
		Ext.getCmp('solic_esp_id').setValue(rec.get('IC_SOLIC_ESP'));
		Ext.Ajax.request({
			url: '22forma07ext.data.jsp',
			params: Ext.apply({
				informacion: 'Consulta_Cotizacion',
				icSolicitud: rec.get('IC_SOLIC_ESP')
			}),
			callback: procesarDatosCotizacion
		});
	}

	/********** Obtiene los datos del registro seleccionado en el grid para mostrar los pagos **********/
	function procesarGeneraPagos(rec){
		Ext.Ajax.request({
			url: '22forma07ext.data.jsp',
			params: Ext.apply({
				informacion: 'Generar_Pagos',
				icPlanPagos: rec.get('IC_PLAN_PAGOS'),
				numSolicitud: rec.get('NUMERO_SOLICITUD')
			}),
			callback: descargaArchivo
		});
	}

	/********** Llena el form DatosCotizacion **********/
	function procesarDatosCotizacion(opts, success, response){
		//fm.el.unmask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){

			var datosCot = Ext.util.JSON.decode(response.responseText);
			var tipoOper = datosCot.cot_tipo_oper;
			var estatus  = Ext.getCmp('estatus_sol_id').getValue();

			if(estatus != '1' || estatus != '3' || estatus != '4'){ //1=Solicictada, 3=Rechazada, 4=En proceso
				if(estatus == '8'){
					Ext.getCmp('panel_fecha_cancelacion').show();
					Ext.getCmp('panel_hora_cancelacion').show();
					Ext.getCmp('panel_fecha_cotizacion').hide();
					Ext.getCmp('panel_hora_cotizacion').hide();
					Ext.getCmp('cot_fecha_cancelacion_id').setValue(datosCot.cot_fecha_cotizacion);
					Ext.getCmp('cot_hora_cancelacion_id').setValue(datosCot.cot_hora_cotizacion);
				} else{
					Ext.getCmp('panel_fecha_cotizacion').show();
					Ext.getCmp('panel_hora_cotizacion').show();
					Ext.getCmp('panel_fecha_cancelacion').hide();
					Ext.getCmp('panel_hora_cancelacion').hide();
					Ext.getCmp('cot_fecha_cotizacion_id').setValue(datosCot.cot_fecha_cotizacion);
					Ext.getCmp('cot_hora_cotizacion_id').setValue(datosCot.cot_hora_cotizacion);
				}
			}

			if(estatus == '7' || estatus == '8' || estatus == '9'){
				Ext.getCmp('panel_num_referencia').show();
				Ext.getCmp('panel_aviso1').show();
				Ext.getCmp('cot_num_referencia_id').setValue(datosCot.cot_num_referencia);
			} else{
					Ext.getCmp('panel_num_referencia').hide();
					Ext.getCmp('panel_aviso1').hide();
			}

			if(tipoOper != 'Primer Piso'){
				Ext.getCmp('panel_intermediario').show();
				if(datosCot.cot_intermediario_ci != ''){
					Ext.getCmp('cot_intermediario_id').setValue(datosCot.cot_intermediario_ci);
				} else{
					Ext.getCmp('cot_intermediario_id').setValue(datosCot.cot_intermediario_cs);
				}
				if(datosCot.strTipoUsuario != 'IF' && datosCot.cot_riesgo_inter !=''){
					Ext.getCmp('panel_riesgo_inter').show();
					Ext.getCmp('cot_riesgo_inter_id').setValue(datosCot.cot_riesgo_inter);
				} else{
					Ext.getCmp('panel_riesgo_inter').hide();
				}
			} else{
				Ext.getCmp('panel_intermediario').hide();
				Ext.getCmp('panel_riesgo_inter').hide();
			}

			if(datosCot.strTipoUsuario != 'IF' && datosCot.cot_acreditado != ''){
				Ext.getCmp('panel_acreditado').show();
				Ext.getCmp('cot_acreditado_id').setValue(datosCot.cot_acreditado);
				if(tipoOper == 'Primer Piso'){
					Ext.getCmp('panel_riesgo_acred').show();
					Ext.getCmp('cot_riesgo_acred_id').setValue(datosCot.cot_riesgo_acred);
				
				} else{
					Ext.getCmp('panel_riesgo_acred').hide();
				}
			} else{
				Ext.getCmp('panel_acreditado').hide();
				Ext.getCmp('panel_riesgo_acred').hide();
			}

			if(datosCot.cot_programa != ''){
				Ext.getCmp('panel_programa').show();
				Ext.getCmp('cot_programa_id').setValue(datosCot.cot_programa);
			} else{
				Ext.getCmp('panel_programa').hide();
			}

			if(parseInt(datosCot.cot_num_disposiciones) > 1){
				Ext.getCmp('panel_mult_disposiciones').show();
				Ext.getCmp('cot_mult_disposiciones_id').setValue(datosCot.cot_mult_disposiciones);
			} else{
				Ext.getCmp('panel_mult_disposiciones').hide();
			}

			if(datosCot.cot_ic_esquema_recup != '1'){
				//Valores que puede tomar cot_ic_esquema_recup: 1=Cupon cero, 2=Tradicional, 3=Plan de pagos, 4=Rentas
				if(datosCot.cot_ic_esquema_recup == '2' || datosCot.cot_ic_esquema_recup == '3'){
					Ext.getCmp('panel_periodo_ppi').show();
					Ext.getCmp('cot_periodo_ppi_id').setValue(datosCot.cot_periodo_ppi);
				} else{
					Ext.getCmp('panel_periodo_ppi').hide();
				}
				if(datosCot.cot_ic_esquema_recup != '3'){
					if(datosCot.cot_ic_esquema_recup == '2' || datosCot.cot_ic_esquema_recup == '4'){
						Ext.getCmp('panel_periodo_ppc').show();
						Ext.getCmp('cot_periodo_ppc_id').setValue(datosCot.cot_periodo_ppc);
					} else{
						Ext.getCmp('panel_periodo_ppc').hide();
					}
				}
				if(datosCot.cot_ic_esquema_recup == '2' && datosCot.cot_primer_fecha != ''){
					Ext.getCmp('panel_primer_fecha').show();
					Ext.getCmp('panel_penultima_fecha').show();
					Ext.getCmp('cot_primer_fecha_id').setValue(datosCot.cot_primer_fecha);
					Ext.getCmp('cot_penultima_fecha_id').setValue(datosCot.cot_penultima_fecha);
				} else{
					Ext.getCmp('panel_primer_fecha').hide();
					Ext.getCmp('panel_penultima_fecha').hide();
				}
			}

			if(estatus == '2' || estatus == '5'){
				if(datosCot.cot_tipo_cotizacion == 'I' && datosCot.strTipoUsuario == 'IF'){
					Ext.getCmp('panel_aviso2').show();
				} else if(datosCot.cot_tipo_cotizacion == 'N' && datosCot.strTipoUsuario == 'IF'){
					Ext.getCmp('panel_aviso3').show();
				}
			}

			if(estatus == '2' || estatus == '5'){
				Ext.getCmp('titulos_6').show();
				Ext.getCmp('panel_ig_tasa_md').show();
				Ext.getCmp('panel_aviso4').show();
				Ext.getCmp('panel_aviso6').show();
				Ext.getCmp('cot_ig_tasa_md_id').setValue(datosCot.cot_tasa_indicativa + ' ' + datosCot.cot_ig_tasa_md);
				if(datosCot.cot_ig_tasa_spot != '0.00'){
					Ext.getCmp('panel_ig_tasa_spot').show();
					Ext.getCmp('panel_aviso5').show();
					Ext.getCmp('cot_ig_tasa_spot_id').setValue(datosCot.cot_tasa_indicativa + ' ' + datosCot.cot_ig_tasa_spot);
				} else{
					Ext.getCmp('panel_ig_tasa_spot').hide();
					Ext.getCmp('panel_aviso5').hide();
				}
			} else{
				Ext.getCmp('titulos_6').hide();
				Ext.getCmp('panel_ig_tasa_md').hide();
				Ext.getCmp('panel_aviso4').hide();
				Ext.getCmp('panel_aviso6').hide();
			}

			if(estatus == '7' || estatus == '8' || estatus == '9'){
				Ext.getCmp('titulos_7').show();
				Ext.getCmp('panel_tasa_confirmada').show();
				if(datosCot.cot_aceptado_md == 'S'){
					Ext.getCmp('cot_confirmacion_id').setValue(datosCot.cot_tasa_indicativa + ' ' + datosCot.cot_ig_tasa_md);
				} else{
					Ext.getCmp('cot_confirmacion_id').setValue(datosCot.cot_tasa_indicativa + ' ' + datosCot.cot_ig_tasa_spot);
				}
				if(estatus == '8'){
					Ext.getCmp('cot_fecha_hora_id').setValue('<b> Fecha y Hora de Cancelaci�n </b>');
					Ext.getCmp('cot_df_confirmacion_id').setValue(datosCot.cot_fecha_cotizacion + ' ' + datosCot.cot_hora_cotizacion);
				} else{
					Ext.getCmp('cot_fecha_hora_id').setValue('<b> Fecha y Hora de Confirmaci�n </b>');
					Ext.getCmp('cot_df_confirmacion_id').setValue(datosCot.cot_df_Aceptacion);
				}
			} else{
				Ext.getCmp('titulos_7').hide();
				Ext.getCmp('panel_tasa_confirmada').hide();
			}

			if(datosCot.cot_observaciones != ''){
				Ext.getCmp('titulos_8').show();
				Ext.getCmp('panel_observaciones').show();
				Ext.getCmp('cot_observaciones_id').setValue(datosCot.cot_observaciones);
			} else{
				Ext.getCmp('titulos_8').hide();
				Ext.getCmp('panel_observaciones').hide();
			}

			Ext.getCmp('cot_numero_solicitud_id').setValue(datosCot.cot_numero_solicitud);
			Ext.getCmp('cot_fecha_solicitud_id').setValue(datosCot.cot_fecha_solicitud);
			Ext.getCmp('cot_hora_solicitud_id').setValue(datosCot.cot_hora_solicitud);
			Ext.getCmp('cot_usuario_id').setValue(datosCot.cot_usuario);
			Ext.getCmp('cot_nombre_ejecutivo_id').setValue(datosCot.cot_nombre_ejecutivo);
			Ext.getCmp('cot_email_id').setValue(datosCot.cot_email);
			Ext.getCmp('cot_telefono_id').setValue(datosCot.cot_telefono);
			Ext.getCmp('cot_fax_id').setValue(datosCot.cot_fax);
			Ext.getCmp('cot_tipo_oper_id').setValue(datosCot.cot_tipo_oper);
			Ext.getCmp('cot_monto_total_id').setValue(datosCot.cot_monto_total);
			Ext.getCmp('cot_tipo_tasa_id').setValue(datosCot.cot_tipo_tasa);
			Ext.getCmp('cot_num_disposiciones_id').setValue(datosCot.cot_num_disposiciones);
			Ext.getCmp('cot_plazo_oper_id').setValue(datosCot.cot_plazo_oper );
			Ext.getCmp('cot_esquema_recuperacion_id').setValue(datosCot.cot_esquema_recuperacion);

			/***** Se arma el Grid Detalle Disposiciones *****/
			if(parseInt(datosCot.totalDetalleDisp) > 0){
				Ext.getCmp('titulos_4').show();
				Ext.getCmp('gridDetalleDisposiciones').show();
				storeDetalleDisposiciones.loadData(datosCot.registrosDetalleDisp);
			} else{
				Ext.getCmp('titulos_4').hide();
				Ext.getCmp('gridDetalleDisposiciones').hide();
			}

			/***** Se arma el Grid Plan de Pagos Capital *****/
			if(parseInt(datosCot.totalPlanPagos) > 0){
				Ext.getCmp('titulos_5').show();
				Ext.getCmp('gridPlanPagosCap').show();
				storePlanPagosCap.loadData(datosCot.registrosPlanPagos);
			} else{
				Ext.getCmp('titulos_5').hide();
				Ext.getCmp('gridPlanPagosCap').hide();
			}

			Ext.getCmp('formaPrincipal').hide();
			Ext.getCmp('gridConsulta').hide();
			Ext.getCmp('formaDatosCotizacion').show();

		} else{
			Ext.Msg.alert('Mensaje', Ext.util.JSON.decode(response.responseText).mensaje );
		}
	}

	/********** Regresa la pantalla a su condici�n inicial **********/
	function procesoSalir(){
		Ext.getCmp('formaPrincipal').getForm().reset();
		Ext.getCmp('formaDatosCotizacion').getForm().reset();
		Ext.getCmp('formaPrincipal').show();
		Ext.getCmp('formaDatosCotizacion').hide();
		Ext.getCmp('gridConsulta').hide();
	}

	/********** Regresa la pantalla a la vista del grid **********/
	function procesoRegresar(){
		Ext.getCmp('formaDatosCotizacion').getForm().reset();
		Ext.getCmp('formaDatosCotizacion').hide();
		Ext.getCmp('formaPrincipal').show();
		Ext.getCmp('gridConsulta').show();
	}

	/********** Se crea el store para el combo IF **********/
	var catalogoIF = new Ext.data.JsonStore({
		id:     'catalogoIF',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07ext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_IF'
		},
		totalProperty:  'total',
		autoLoad:       true,
		listeners: {
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/********** Se crea el store para el combo Persona Responsable **********/
	var catalogoEjecutivo = new Ext.data.JsonStore({
		id:     'catalogoEjecutivo',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07ext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Ejecutivo'
		},
		totalProperty:  'total',
		autoLoad:       true,
		listeners: {
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/********** Se crea el store para el combo Moneda **********/
	var catalogoMoneda = new Ext.data.JsonStore({
		id:     'catalogoMoneda',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07ext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Moneda'
		},
		totalProperty:  'total',
		autoLoad:       true,
		listeners: {
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/********** Se crea el store para el combo Estatus **********/
	var catalogoEstatus = new Ext.data.JsonStore({
		id:     'catalogoEstatus',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '22forma07ext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Estatus'
		},
		totalProperty:  'total',
		autoLoad:       true,
		listeners: {
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/********** Proceso y consulta para la consulta del grid **********/
	var procesarConsultaData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('formaPrincipal');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();
		if (arrRegistros != null){
			var jsonData = store.reader.jsonData;
			gridConsulta.show();
			if(store.getTotalCount() > 0){
				el.unmask(); 
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarCSV').enable();
			} else{
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	/********** Se crea el store para el Grid **********/
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url:  '22forma07ext.data.jsp',
		baseParams: {
			informacion: 'Consultar_Datos'
		},
		fields: [
			{ name: 'IC_SOLIC_ESP'     },
			{ name: 'FECHA_SOLICITUD'  },
			{ name: 'FECHA_COTIZACION' },
			{ name: 'NUMERO_SOLICITUD' },
			{ name: 'SOLICITAR_AUT'    },
			{ name: 'CG_RAZON_SOCIAL'  },
			{ name: 'MONEDA'           },
			{ name: 'EJECUTIVO'        },
			{ name: 'MONTO'            },
			{ name: 'PLAZO'            },
			{ name: 'ESTATUS'          },
			{ name: 'TASA_MISMO_DIA'   },
			{ name: 'TASA_SPOT'        },
			{ name: 'TASA_CONFIRMADA'  },
			{ name: 'TIPOCOTIZA'       },
			{ name: 'CAUSA_RECHAZO'    },
			{ name: 'IC_PLAN_PAGOS'    },
			{ name: 'OBSER_OPER'       },
			{ name: 'NUM_REFERENCIA'   },
			{ name: 'IC_ESTATUS'       },
			{ name: 'IC_MONEDA'        },
			{ name: 'IC_TIPO_TASA'     }
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	/********** Se crea el store del Grid Detalle Disposiciones **********/
	var storeDetalleDisposiciones = new Ext.data.ArrayStore({
		fields: [
			{ name: 'NUM_DISPOSICION',   mapping: 0},
			{ name: 'MONTO',             mapping: 1},
			{ name: 'FECHA_DISPOSICION', mapping: 2},
			{ name: 'FECHA_VENCIMIENTO', mapping: 3}
		],
		autoLoad: false
	});

	/********** Se crea el store del Grid Plan de Pagos Capital **********/
	var storePlanPagosCap = new Ext.data.ArrayStore({
		fields: [
			{ name: 'NUM_PAGO',    mapping: 0},
			{ name: 'FECHA_PAGO',  mapping: 1},
			{ name: 'MONTO_PAGAR', mapping: 2},
			{ name: 'SIN_NOMBRE',  mapping: 3}
		],
		autoLoad: false
	});

	/********** Se crea el grid de consulta**********/
	var gridConsulta = new Ext.grid.EditorGridPanel({
		width:        900,
		height:       420,
		store:        consultaData,
		id:           'gridConsulta',
		margins:      '20 0 0 0',
		style:        'margin:0 auto;',
		align:        'center',
		hidden:       true,
		displayInfo:  true,
		loadMask:     true,
		stripeRows:   true,
		frame:        false,
		clicksToEdit: 1,
		columns: [
		{width: 115, header: 'Fecha de Solicitud',                  dataIndex: 'FECHA_SOLICITUD',  align: 'center', sortable: true, resizable: true},
		//{width: 115, header: 'FECHA_COTIZACION',                    dataIndex: 'FECHA_COTIZACION', align: 'center', sortable: true, resizable: true},
		{width: 115, header: 'N�mero de Solicitud',                 dataIndex: 'NUMERO_SOLICITUD', align: 'center', sortable: true, resizable: true},
		{width: 200, header: 'Requiere autorizaci�n del ejecutivo', dataIndex: 'SOLICITAR_AUT',    align: 'center', sortable: true, resizable: true},
		{width: 300, header: 'Intermediario financiero',            dataIndex: 'CG_RAZON_SOCIAL',  align: 'center', sortable: true, resizable: true},
		{width: 120, header: 'Moneda',                              dataIndex: 'MONEDA',           align: 'center', sortable: true, resizable: true},
		{width: 200, header: 'Ejecutivo',                           dataIndex: 'EJECUTIVO',        align: 'center', sortable: true, resizable: true},
		{width: 135, header: 'Monto Total Requerido',               dataIndex: 'MONTO',            align: 'center', sortable: true, resizable: true, renderer: Ext.util.Format.numberRenderer('$0,000.00')},
		{width: 125, header: 'Plazo de la Operaci�n',               dataIndex: 'PLAZO',            align: 'center', sortable: true, resizable: true},
		{width: 100, header: 'Estatus',                             dataIndex: 'ESTATUS',          align: 'center', sortable: true, resizable: true},
		{width: 120, header: 'Tasa mismo d�a',                      dataIndex: 'TASA_MISMO_DIA',   align: 'center', sortable: true, resizable: true},
		{width: 120, header: 'Tasa 48 horas',                       dataIndex: 'TASA_SPOT',        align: 'center', sortable: true, resizable: true},
		{width: 120, header: 'Tasa Confirmada',                     dataIndex: 'TASA_CONFIRMADA',  align: 'center', sortable: true, resizable: true},
		{width: 110, header: 'Tipo de Cotizaci�n',                  dataIndex: 'TIPOCOTIZA',       align: 'center', sortable: true, resizable: true},
		{width: 130, header: 'Causas de Rechazo',                   dataIndex: 'CAUSA_RECHAZO',    align: 'center', sortable: true, resizable: true},
		{width: 180, header: 'Observaciones de Operaciones',        dataIndex: 'OBSER_OPER',       align: 'center', sortable: true, resizable: true},
		{
			xtype:      'actioncolumn',
			header:     'Plan de Pago',
			align:      'center',
			sortable:   false,
			resizable:  false,
			items: [{
				getClass: function(v, meta, rec){
					if(rec.get('IC_PLAN_PAGOS') != ''){
						this.items[0].tooltip = 'Ver Tabla';
						return 'icoXls';
					} else{
						return '';
					}
				},
				handler: function(gridConsulta, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					if(rec.get('IC_PLAN_PAGOS') != ''){
						procesarGeneraPagos(rec);
					}
				}
			}]
		},{
			xtype:      'actioncolumn',
			header:     'Consultar',
			align:      'center',
			sortable:   false,
			resizable:  false,
			items: [{
				getClass: function(v, meta, rec){
					if(rec.get('IC_ESTATUS') == '1' || rec.get('IC_ESTATUS') == '3' || rec.get('IC_ESTATUS') == '4'){
						this.items[0].tooltip = 'Solicitud';
						return 'icoBuscar';
					} else{
						this.items[0].tooltip = 'Cotizaci�n';
						return 'icoBuscar';
					}
				},
				handler: function(gridConsulta, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					procesarConsultar(rec);
				}
			}]
		},{
			xtype:      'actioncolumn',
			header:     'Cotizar',
			align:      'center',
			sortable:   false,
			resizable:  false,
			items: [{
				iconCls: 'modificar',
				handler: function(gridConsulta, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					procesarModificar(rec);
				}
			}]
		}],
		bbar: {
			xtype:       'paging',
			id:          'barraPaginacion',
			displayMsg:  '{0} - {1} de {2}',
			emptyMsg:    'No hay registros.',
			buttonAlign: 'left',
			displayInfo: true,
			pageSize:    15,
			store:       consultaData,
			items: ['->','-',{
				xtype:   'button',
				text:    'Generar Informaci�n',
				iconCls: 'icoXls',
				id:      'btnGenerarCSV',
				handler: function(boton, evento){
					boton.setIconClass('loading-indicator');
					var barraPaginacionA = Ext.getCmp('barraPaginacion');
					Ext.Ajax.request({
						url: '22forma07ext.data.jsp',
						params: Ext.apply({
							informacion  : 'Imprimir_CSV',
							operacion    : 'generar',
							fechaInicio  : Ext.getCmp('fecha_inicio').getValue(),
							fechaFinal   : Ext.getCmp('fecha_final').getValue(),
							numSolicitud : Ext.getCmp('num_solicitud_id').getValue(),
							intermediario: Ext.getCmp('intermediario_id').getValue(),
							ejecutivo    : Ext.getCmp('ejecutivo_id').getValue(),
							moneda       : Ext.getCmp('moneda_id').getValue(),
							montoInicio  : Ext.getCmp('monto_inicio_id').getValue(),
							montoFinal   : Ext.getCmp('monto_final_id').getValue(),
							estatus      : Ext.getCmp('estatus_id').getValue(),
							start        : 0,
							limit        : barraPaginacionA.store.totalLength
						}),
						callback: descargaArchivo
					});
				}
			},'-',{
				xtype:   'button',
				text:    'Generar Archivo PDF',
				iconCls: 'icoPdf',
				id:      'btnImprimir',
				handler: function(boton, evento){
					var barraPaginacionA = Ext.getCmp('barraPaginacion');
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '22forma07ext.data.jsp',
						params: Ext.apply({
							informacion  : 'Imprimir_PDF',
							fechaInicio  : Ext.getCmp('fecha_inicio').getValue(),
							fechaFinal   : Ext.getCmp('fecha_final').getValue(),
							numSolicitud : Ext.getCmp('num_solicitud_id').getValue(),
							intermediario: Ext.getCmp('intermediario_id').getValue(),
							ejecutivo    : Ext.getCmp('ejecutivo_id').getValue(),
							moneda       : Ext.getCmp('moneda_id').getValue(),
							montoInicio  : Ext.getCmp('monto_inicio_id').getValue(),
							montoFinal   : Ext.getCmp('monto_final_id').getValue(),
							estatus      : Ext.getCmp('estatus_id').getValue(),
							start        : barraPaginacionA.cursor,
							limit        : barraPaginacionA.pageSize
						}),
						callback: descargaArchivo
					});
				}
			}]
		}
	});

	/********** Se crea el grid Detalle Disposiciones **********/
	var gridDetalleDisposiciones = new Ext.grid.EditorGridPanel({
		width:        600,
		store:        storeDetalleDisposiciones,
		id:           'gridDetalleDisposiciones',
		margins:      '20 0 0 0',
		style:        'margin:0 auto;',
		align:        'center',
		autoHeight:   true,
		hidden:       true,
		displayInfo:  true,
		loadMask:     true,
		stripeRows:   true,
		frame:        false,
		clicksToEdit: 1,
		columns: [
		{width: 145, header: 'N�mero de Disposici�n', dataIndex: 'NUM_DISPOSICION',   align: 'center', sortable: false, resizable: false},
		{width: 150, header: 'Monto',                 dataIndex: 'MONTO',             align: 'center', sortable: false, resizable: false},
		{width: 150, header: 'Fecha de Disposici�n',  dataIndex: 'FECHA_DISPOSICION', align: 'center', sortable: false, resizable: false},
		{width: 150, header: 'Fecha de Vencimiento',  dataIndex: 'FECHA_VENCIMIENTO', align: 'center', sortable: false, resizable: false}
		]
	});

	/********** Se crea el grid Plan de Pagos Capital **********/
	var gridPlanPagosCap = new Ext.grid.EditorGridPanel({
		width:        600,
		store:        storePlanPagosCap,
		id:           'gridPlanPagosCap',
		margins:      '20 0 0 0',
		style:        'margin:0 auto;',
		align:        'center',
		autoHeight:   true,
		hidden:       true,
		displayInfo:  true,
		loadMask:     true,
		stripeRows:   true,
		frame:        false,
		clicksToEdit: 1,
		columns: [
		{width: 145, header: 'N�mero de Pago', dataIndex: 'NUM_PAGO',    align: 'center', sortable: false, resizable: false},
		{width: 150, header: 'Fecha de Pago',  dataIndex: 'FECHA_PAGO',  align: 'center', sortable: false, resizable: false},
		{width: 150, header: 'Monto a Pagar',  dataIndex: 'MONTO_PAGAR', align: 'center', sortable: false, resizable: false},
		{width: 150, header: '&nbsp;',         dataIndex: 'SIN_NOMBRE',  align: 'center', sortable: false, resizable: false}
		]
	});

	/********** Form Panel Datos Cotizacion **********/
	var formaDatosCotizacion = new Ext.form.FormPanel({
		width:           600,
		id:              'formaDatosCotizacion',
		title:           'Cotizador de Cr�ditos',
		layout:          'form',
		style:           'margin: 0 auto;',
		labelAlign:      'right',
		frame:           false,
		hidden:          true,
		border:          true,
		autoHeight:      true,
		items: [{
			width:        600,
			xtype:        'panel',
			id:           'titulos_0',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"> <b> Datos de la Cotizaci�n </b><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_numero_solicitud',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     300,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>N�mero de Solicitud: </b><br> </div>'
			},{
				width:     297,
				xtype:     'displayfield',
				id:        'cot_numero_solicitud_id',
				name:      'cot_numero_solicitud',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_fecha_solicitud',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     300,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Fecha de Solicitud: </b><br> </div>'
			},{
				width:     297,
				xtype:     'displayfield',
				id:        'cot_fecha_solicitud_id',
				name:      'cot_fecha_solicitud',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_hora_solicitud',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     300,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Hora de Solicitud: </b><br> </div>'
			},{
				width:     297,
				xtype:     'displayfield',
				id:        'cot_hora_solicitud_id',
				name:      'cot_hora_solicitud',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_fecha_cancelacion',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     300,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Fecha de Cancelaci�n: </b><br> </div>'
			},{
				width:     297,
				xtype:     'displayfield',
				id:        'cot_fecha_cancelacion_id',
				name:      'cot_fecha_cancelacion',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_hora_cancelacion',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     300,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Hora de Cancelaci�n: </b><br> </div>'
			},{
				width:     297,
				xtype:     'displayfield',
				id:        'cot_hora_cancelacion_id',
				name:      'cot_hora_cancelacion',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_fecha_cotizacion',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     300,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Fecha de Cotizaci�n: </b><br> </div>'
			},{
				width:     297,
				xtype:     'displayfield',
				id:        'cot_fecha_cotizacion_id',
				name:      'cot_fecha_cotizacion',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_hora_cotizacion',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     300,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>Hora de Cotizaci�n: </b><br> </div>'
			},{
				width:     297,
				xtype:     'displayfield',
				id:        'cot_hora_cotizacion_id',
				name:      'cot_hora_cotizacion',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_cot_usuario',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     300,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>N�mero y Nombre de usuario: </b><br> </div>'
			},{
				width:     297,
				xtype:     'displayfield',
				id:        'cot_usuario_id',
				name:      'cot_usuario',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_num_referencia',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     300,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b>N�mero de Recibo: </b><br> </div>'
			},{
				width:     297,
				xtype:     'displayfield',
				id:        'cot_num_referencia_id',
				name:      'cot_num_referencia',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_aviso1',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="left"><br>En caso de que la disposici&oacute;n de los recursos no se ' +
				           'lleve a cabo total o parcialmente en la fecha estipulada, Nacional Financiera aplicar&aacute; '   +
							  'el cobro de una comisi&oacute;n sobre los montos no dispuestos, conforme a las pol&iacute;ticas ' +
							  'de cr&eacute;dito aplicables a este tipo de operaciones.<br>&nbsp;</div>'
			}]
		},{
			width:        600,
			xtype:        'panel',
			id:           'titulos_1',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"> <b> Datos del Ejecutivo Nafin </b><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_nombre_ejecutivo',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Nombre del ejecutivo: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_nombre_ejecutivo_id',
				name:      'cot_nombre_ejecutivo',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_email',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Correo electr�nico: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_email_id',
				name:      'cot_email',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_telefono',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Tel�fono: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_telefono_id',
				name:      'cot_telefono',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_fax',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Fax: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_fax_id',
				name:      'cot_fax',
				value:     '&nbsp;'
			}]
		},{
			width:        600,
			xtype:        'panel',
			id:           'titulos_2',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"><b> Datos del Intermediario Financiero / Acreditado </b><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_tipo_oper',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Tipo de operaci�n: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_tipo_oper_id',
				name:      'cot_tipo_oper',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_intermediario',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Nombre del intermediario: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_intermediario_id',
				name:      'cot_intermediario',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_riesgo_inter',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Riesgo del intermediario: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_riesgo_inter_id',
				name:      'cot_riesgo_inter',
				value:     '&nbsp;'
			}]
		},{
			width:        600,
			xtype:        'panel',
			id:           'titulos_3',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"><b> Caracter�sticas de la Operaci�n </b><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_acreditado',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Nombre del Acreditado: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_acreditado_id',
				name:      'cot_acreditado',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_riesgo_acred',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Riesgo del Acreditado: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_riesgo_acred_id',
				name:      'cot_riesgo_acred',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_programa',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Nombre del programa o Acreditado Final: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_programa_id',
				name:      'cot_programa',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_monto_total',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Monto total requerido: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_monto_total_id',
				name:      'cot_monto_total',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_tipo_tasa',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Tipo de tasa requerida: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_tipo_tasa_id',
				name:      'cot_tipo_tasa',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_num_disposiciones',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> N�mero de Disposiciones: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_num_disposiciones_id',
				name:      'cot_num_disposiciones',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_mult_disposiciones',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Disposiciones m�ltiples tasas de inter�s: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_mult_disposiciones_id',
				name:      'cot_mult_disposiciones',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_plazo_oper',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Plazo de la operaci�n: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_plazo_oper_id',
				name:      'cot_plazo_oper',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_esquema_recuperacion',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Esquema de recuperaci�n del capital: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_esquema_recuperacion_id',
				name:      'cot_esquema_recuperacion',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_periodo_ppi',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Periodicidad del pago de inter�s: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_periodo_ppi_id',
				name:      'cot_periodo_ppi',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_periodo_ppc',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Periodicidad de Capital: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_periodo_ppc_id',
				name:      'cot_periodo_ppc',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_primer_fecha',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Primer fecha de Pago de Capital: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_primer_fecha_id',
				name:      'cot_primer_fecha',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_penultima_fecha',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Pen�ltima fecha de pago de Capital: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_penultima_fecha_id',
				name:      'cot_penultima_fecha',
				value:     '&nbsp;'
			}]
		},{
			width:        600,
			xtype:        'panel',
			id:           'titulos_4',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"><b> Detalle de Disposiciones </b><br> </div>'
			}]
		}, gridDetalleDisposiciones,
		{
			width:        600,
			xtype:        'panel',
			id:           'titulos_5',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"><b> Plan de Pagos Capital </b><br> </div>'
			}]
		}, gridPlanPagosCap,
		{
			width:        598,
			xtype:        'panel',
			id:           'panel_aviso2',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="left"><br> COTIZACION INDICATIVA, NO ES POSIBLE TOMAR EN FIRME LA OPERACION, FAVOR DE CONSULTAR A SU EJECUTIVO <br>&nbsp;</div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_aviso3',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="left"><br> Para realizar la toma en firme favor de ponerse en contacto con su ejecutivo <br>&nbsp;</div>'
			}]
		},{
			width:        600,
			xtype:        'panel',
			id:           'titulos_6',
			layout:       'table',
			border:       false,
			hidden:       false,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"><b> Tasa Indicativa </b><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_ig_tasa_md',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Tasa Indicativa con oferta mismo d�a: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_ig_tasa_md_id',
				name:      'cot_ig_tasa_md',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_ig_tasa_spot',
			layout:       'table',
			border:       true,
			hidden:       true,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     240,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="right"><b> Tasa Indicativa con oferta 48 horas: </b><br> </div>'
			},{
				width:     358,
				xtype:     'displayfield',
				id:        'cot_ig_tasa_spot_id',
				name:      'cot_ig_tasa_spot',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_aviso4',
			layout:       'table',
			border:       false,
			hidden:       true,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="left"><br><b> Nota: </b><br> ' +
				           '<b>Tasa con oferta Mismo D&iacute;a:</b>&nbsp;El Intermediario Financiero tiene la opci&oacute;n de tomarla en firme el mismo     ' +
				           'd&iacute;a que haya recibido la cotizaci&oacute;n indicativa a m&aacute;s tardar a las 12:30 horas. <br></div> '
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_aviso5',
			layout:       'table',
			border:       false,
			hidden:       true,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="left"><b>Tasa con oferta 48 Horas:</b>&nbsp;El Intermediario Financiero tiene la opci&oacute;n de ' +
				           'tomarla en firme a m&aacute;s tardar dos d&iacute;as h&aacute;biles despu&eacute;s de que la Tesorer&iacute;a haya proporcionado ' +
				           'la cotizaci&oacute;n indicativa antes de las 12:30 horas del segundo d�a h&aacute;bil. <br></div> '
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_aviso6',
			layout:       'table',
			border:       false,
			hidden:       true,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="left"> Las cotizaciones indicativas proporcionadas por la Tesorer&iacute;a de Nacional Financiera ' +
			               'son de uso reservado para el intermediario solicitante y ser&aacute; responsabilidad de dicho intermediario el mal uso de la '     +
			               'informaci&oacute;n proporcionada. <br>&nbsp;</div> '
			}]
		},{
			width:        600,
			xtype:        'panel',
			id:           'titulos_7',
			layout:       'table',
			frame:        true,
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     298,
				xtype:     'displayfield',
				id:        'cot_tasa_confirmada_id',
				name:      'cot_tasa_confirmada',
				value:     '<b> Tasa confirmada </b>'
			},{
				width:     298,
				xtype:     'displayfield',
				id:        'cot_fecha_hora_id',
				name:      'cot_fecha_hora',
				value:     '&nbsp;'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_tasa_confirmada',
			layout:       'table',
			border:       true,
			hidden:       false,
			layoutConfig: {columns: 2},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     298,
				xtype:     'displayfield',
				id:        'cot_confirmacion_id',
				name:      'cot_confirmacion',
				value:     '&nbsp;'
			},{
				width:     298,
				xtype:     'displayfield',
				id:        'cot_df_confirmacion_id',
				name:      'cot_df_confirmacion',
				value:     '&nbsp;'
			}]
		},{
			width:        600,
			xtype:        'panel',
			id:           'titulos_8',
			layout:       'table',
			border:       false,
			hidden:       true,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				frame:     true,
				border:    true,
				html:      '<div align="left"><b> Observaciones </b><br> </div>'
			}]
		},{
			width:        598,
			xtype:        'panel',
			id:           'panel_observaciones',
			layout:       'table',
			border:       false,
			hidden:       true,
			layoutConfig: {columns: 1},
			defaults:     {align: 'center', bodyStyle: 'padding:2px,'},
			items: [{
				width:     598,
				xtype:     'displayfield',
				id:        'cot_observaciones_id',
				name:      'cot_observaciones',
				value:     '&nbsp;'
			}]
		}],
		buttons: [{
				xtype:     'button',
				text:      'Generar Archivo PDF',
				id:        'btnCotizacionPDF',
				iconCls:   'icoPdf',
				handler:   function(boton, evento){
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '22forma07ext.data.jsp',
						params: Ext.apply({
							informacion: 'Consulta_Cotizacion_PDF',
							icSolicitud: Ext.getCmp('solic_esp_id').getValue()
						}),
						callback: descargaArchivo
					});
				}
			},{
				xtype:     'button',
				text:      'Salir',
				id:        'btnSalir',
				iconCls:   'icoRegresar',
				handler:   procesoRegresar
		}]
	});

	/********** Se crean los componentes del Form Principal **********/
	var elementosForma = [{
		layout:                    'column',
		items:[{
			columnWidth:            .5,
			id:                     'panelIzq',
			width:                  '50%',
			layout:                 'form',
			items: [{
				xtype:               'compositefield',
				fieldLabel:          'Fecha de solicitud',
				msgTarget:           'side',
				combineErrors:       false,
				items: [{
					width:            126,
					xtype:            'datefield',
					id:               'fecha_inicio',
					name:             'fecha_inicio',
					msgTarget:        'side',
					vtype:            'rangofecha',
					minValue:         '01/01/1901',
					campoFinFecha:    'fecha_final',
					margins:          '0 20 0 0',
					value:            new Date(),
					allowBlank:       true,
					startDay:         0
				},{
					xtype:            'displayfield',
					value:            'a',
					width:            18
				},{
					width:            126,
					xtype:            'datefield',
					id:               'fecha_final',
					name:             'fecha_final',
					msgTarget:        'side',
					vtype:            'rangofecha',
					minValue:         '01/01/1901',
					campoInicioFecha: 'fecha_inicio',
					margins:          '0 20 0 0',
					value:            new Date(),
					allowBlank:       true,
					startDay:         1
				}]
			},{
				xtype:               'textfield',
				id:                  'num_solicitud_id',
				name:                'num_solicitud',
				fieldLabel:          'N�mero de solicitud',
				msgTarget:           'side',
				anchor:              '95%',
				maxLength:           12
			},{
				xtype:               'combo',
				id:                  'intermediario_id',
				name:                'intermediario',
				fieldLabel:          'Intermediario',
				emptyText:           'Seleccione un Intermediario...',
				displayField:        'descripcion',
				valueField:          'clave',
				msgTarget:           'side',
				mode:                'local',
				triggerAction:       'all',
				anchor:              '95%',
				forceSelection:      true,
				typeAhead:           true,
				allowBlank:          true,
				store:               catalogoIF
			},{
				xtype:               'combo',
				id:                  'ejecutivo_id',
				name:                'ejecutivo',
				fieldLabel:          'Persona responsable',
				emptyText:           'Seleccione un Ejecutivo...',
				displayField:        'descripcion',
				valueField:          'clave',
				msgTarget:           'side',
				mode:                'local',
				triggerAction:       'all',
				anchor:              '95%',
				forceSelection:      true,
				typeAhead:           true,
				allowBlank:          true,
				store:               catalogoEjecutivo
			}]
		},{
			columnWidth:            .5,
			width:                  '50%',
			id:                     'panelDer',
			layout:                 'form',
			items: [{
				xtype:               'combo',
				id:                  'moneda_id',
				name:                'moneda',
				fieldLabel:          'Moneda',
				emptyText:           'Seleccione una Moneda...',
				displayField:        'descripcion',
				valueField:          'clave',
				msgTarget:           'side',
				mode:                'local',
				triggerAction:       'all',
				anchor:              '95%',
				forceSelection:      true,
				typeAhead:           true,
				allowBlank:          true,
				store:               catalogoMoneda
			},{
				xtype:               'compositefield',
				fieldLabel:          'Monto del cr�dito',
				msgTarget:           'side',
				combineErrors:       false,
				items: [{
					width:            125,
					xtype:            'numberfield',
					id:               'monto_inicio_id',
					name:             'monto_inicio',
					fieldLabel:       'Monto del cr�dito',
					msgTarget:        'side',
					margins:          '0 20 0 0',
					maxLength:        24,
					allowDecimals:    true
				},{
					xtype:            'displayfield',
					value:            'a',
					width:            22
				},{
					width:            125,
					xtype:            'numberfield',
					id:               'monto_final_id',
					name:             'monto_final',
					msgTarget:        'side',
					margins:          '0 20 0 0',
					maxLength:        24,
					allowDecimals:    true
				}]
			},{
				xtype:               'combo',
				id:                  'estatus_id',
				name:                'estatus',
				fieldLabel:          'Estatus',
				emptyText:           'Seleccione un Estatus...',
				displayField:        'descripcion',
				valueField:          'clave',
				msgTarget:           'side',
				mode:                'local',
				triggerAction:       'all',
				anchor:              '95%',
				forceSelection:      true,
				typeAhead:           true,
				allowBlank:          true,
				store:               catalogoEstatus
			},{
				xtype:               'textfield',
				id:                  'estatus_sol_id',
				name:                'estatus_sol',
				hidden:              true
			},{
				xtype:               'textfield',
				id:                  'solic_esp_id',
				name:                'solic_esp',
				hidden:              true
			}]
		}]
	}];

	/********** Form Panel Principal **********/
	var formaPrincipal = new Ext.form.FormPanel({
		width:      900,
		labelWidth: 115,
		id:         'formaPrincipal',
		title:      'Criterios de B�squeda',
		layout:     'form',
		style:      'margin: 0 auto;',
		bodyStyle:  'padding: 6px',
		frame:      true,
		hidden:     false,
		border:     true,
		autoHeight: true,
		items:      elementosForma,
		buttons: [{
			xtype:   'button',
			text:    'Nueva Cotizaci�n',
			id:      'btnNuevaCotizacion',
			iconCls: 'modificar',
			handler: procesoFormCotizador
		},{
			xtype:   'button',
			text:    'Consultar',
			id:      'btnConsultarCotizacion',
			iconCls: 'icoBuscar',
			handler: procesoConsultarCotizacion
		},{
			xtype:   'button',
			text:    'Limpiar',
			id:      'btnLimpiar',
			iconCls: 'icoLimpiar',
			handler: procesoSalir
		}]
	});

	/********** Contenedor Principal **********/
	var pnl = new Ext.Container({
		width:   949,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin: 0 auto;',
		items: [
			NE.util.getEspaciador(20),
			formaPrincipal,
			formaDatosCotizacion,
			NE.util.getEspaciador(10),
			gridConsulta
		]
	});

});