Ext.onReady(function(){

	/********** Valida el campo y sube el archivo al servidor **********/
	function subeArchivo(){

		// Validar que se haya especificado un archivo
		var archivo = Ext.getCmp("archivo");
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid("Debe especificar un archivo");
			return;
		}

		// Revisar el tipo de extension del archivo
		var myRegexTXT    = /^.+\.([tT][xX][tT])$/;
		var myRegexCSV    = /^.+\.([cC][sS][vV])$/;
		var nombreArchivo = archivo.getValue();
		if(myRegexTXT.test(nombreArchivo) || myRegexCSV.test(nombreArchivo)){
			Ext.getCmp('formaPrincipal').getForm().submit({
				clientValidation: true,
				url:              '22rompimientoFondeo01ext.data.jsp?informacion=SubirArchivo',
				waitMsg:          'Cargando archivo...',
				waitTitle:        'Por favor espere',
				success:          function(form, action){
					procesaArchivo(null,  true,  action.response );
				},
				failure: function(form, action){
					action.response.status = 200;
					procesaArchivo(null,  true,  action.response );
				}
			});
		} else{
			archivo.markInvalid("El formato del archivo no es correcto. Debe tener extensi�n txt o csv.");
			return;
		}

	}

	/********** Muestra el resultado despues de subir el erchivo **********/
	function procesaArchivo(opts, success, response){
		var jsonData = Ext.util.JSON.decode(response.responseText);
		if (success == true){
			if(jsonData.success == true){
				Ext.Msg.alert('Mensaje...', 'El archivo ' + Ext.getCmp('archivo').getValue() + ' se subi� exitosamente.');
			} else{
				if(jsonData.msg != '' || jsonData.msg != null){
					Ext.Msg.alert('Error...', jsonData.msg);
				} else{
					Ext.Msg.alert('Error...', 'Error al subir el archivo. Por favor int�ntelo de nuevo.');
				}
			}
			Ext.getCmp('formaPrincipal').getForm().reset();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	/********** Form Panel Principal **********/
	var formaPrincipal = new Ext.form.FormPanel({
		width:         600,
		labelWidth:    110,
		id:            'formaPrincipal',
		title:         'Subir archivo rompimiento de fondeo',
		layout:        'form',
		style:         'margin:0 auto;',
		frame:         true,
		border:        true,
		autoHeight:    true,
		fileUpload:    true,
		items: [{
			width:      350,
			xtype:      'fileuploadfield',
			id:         'archivo',
			name:       'archivo',
			fieldLabel: '&nbsp; Ruta del Archivo',
			emptyText:  'Seleccione un archivo...',
			anchor:     '-20',
			msgTarget:  'side',
			buttonCfg:  { iconCls: 'upload-icon' },
			buttonText: null,
			allowBlank: true
		}],
		buttons: [{
			text:       'Subir Archivo',
			id:         'btnAceptar',
			iconCls:    'icoAceptar',
			hidden:     false,
			handler:    subeArchivo
		}]
	});

	/********** Contenedor Principal **********/
	var pnl = new Ext.Container({
		width:         949,
		id:            'contenedorPrincipal',
		applyTo:       'areaContenido',
		style:         'margin: 0 auto;',
		items: [
			NE.util.getEspaciador(20),
			formaPrincipal
		]
	});

});