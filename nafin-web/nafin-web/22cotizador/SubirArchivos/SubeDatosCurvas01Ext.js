Ext.onReady(function(){
var _URL = 'SubeDatosCurvas01Ext.data.jsp'
var _URL_JSP = 'SubeDatosCurvas01Ext.jsp'

var myValidFn = function(v) {
		var myRegex = /^.+\.([Cc][Ss][Vv]|[Tt][Xx][Tt]??)$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivo 		: myValidFn,
		archivoText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): csv, txt .'
	});

//----------------------------- "MAQUINA DE ESTADO" -------------------------------	
	var procesaInformacion = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
	
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						InformacionProyecto(resp.estadoSiguiente,resp);
					}
				);
			}else {
				InformacionProyecto(resp.estadoSiguiente,resp);
			}
		} else {
			// Mostrar mensaje de error
			//NE.util.mostrarConnError(response,opts);
		}
	}
	
//----------------------ESTADOS----------------------------------------------
	var InformacionProyecto = function( estado, respuesta ){
		if(	estado	=='INICIAR_CARGA'){
			 respuesta.submit({
				  url:_URL,
					  success:function(form,action){
					var resp = 	Ext.util.JSON.decode(action.response.responseText);
					InformacionProyecto(resp.estadoSiguiente,resp);
				  },//procesaInformacion,
					failure: procesaInformacion
			 });  
		
		}else if(	estado	==	'MOSTRAR_RESULTADOS'){//msgArchivosNOGuardados
			Ext.getCmp('msgArchivosGuardados').update('<b >'+respuesta.msgArchivosGuardados+'</b><br>');
			Ext.getCmp('archivosCargados').update('<b>N�mero de archivos que se subieron: '+respuesta.archivosCargados+'</b><br><br>');
			Ext.getCmp('archivosGuardados').update('<b>N�mero de archivos que se guardaron en la BD: '+respuesta.archivosGuardados+'</b><br><br>');
			Ext.getCmp('horaActual').update('<b>Hora de Carga: '+respuesta.horaActual+'</b><br><br>');
			panelResultados.show();
			fp.hide();
		
		}else if(	estado	==	'MOSTRAR_ERROR'){
			console.dir(respuesta);
			
			var panelerr =Ext.getCmp('panelError');
			panelerr.load({
				url:'../22MuestraErrores.jsp',
				scripts:true,
				params:{
					titulo: respuesta.titulo, 
					mensajeError:respuesta.mensajeError
				}
			});
			fp.hide();
			panelerr.show();			
			
		}else if(	estado	==	'ESPERAR_DESICION'){
			Ext.Msg.alert(estado,'procesar info');
		}
	}

//--------------------------COMPONENTES VISUALES--------------------------------
	var panelError = new Ext.Panel({
		id:'panelError',
		bodyBorder : true,
		style: 'margin:0 auto;border:0;',
		padding:5,
		hidden : true,
		frame: true,
		width: 600,
		autoHeight: true,
		fbar: [
			{
				  text: 'Regresar',
				  iconCls:	'icoRegresar',
				  handler:function(){
						window.location =''+_URL_JSP;//
				  }
			 }
		 ]
	});
 var panelResultados = new Ext.Panel({
	id:'panel',
	bodyBorder : true,
	style: 'margin:0 auto;border:0;',
	padding:5,
	hidden : true,
	frame: true,
	width: 600,
	autoHeight: true,
	title: '<div align=center>Resultados</div>',
	buttonAlign: 'center', 
	items: [{
				xtype:'label',
				id:'msgArchivosGuardados',
				html:''
				
			},{
				xtype:'label',
				id:'msgArchivosNOGuardados',
				html:''
				
			},{
				xtype:'label',
				id:'archivosCargados',
				html:''
			},{
				xtype:'label',
				id:'archivosGuardados',//
				html:''
			},{
				xtype:'label',
				id:'horaActual',//
				html:''
			}
			],
		fbar: [
			{
				  text: 'Regresar',
				  iconCls:	'icoRegresar',
				  handler:function(){
							panelResultados.hide();
							fp.getForm().reset();
							fp.show();
						}
			 }
		 ]
    });

	var elementosForma = [
		{
			xtype: 'fileuploadfield',
			id: 'archivoCarga1',
			emptyText: 'Ruta del Archivo',
			name: 'Archivo1',   
			buttonCfg: {				
				text:'Examinar...'
			},			
			vtype: 'archivo'
		 },
		 {
			xtype: 'fileuploadfield',
			id: 'archivoCarga2',
			emptyText: 'Ruta del Archivo',
			name: 'Archivo2',   
			buttonCfg: {			
				text:'Examinar...'
			},			
			vtype: 'archivo'
		 },
		 {
			xtype: 'fileuploadfield',
			id: 'archivoCarga3',
			emptyText: 'Ruta del Archivo',
			name: 'Archivo3',   
			buttonCfg: {				
				text:'Examinar...'
			},
			vtype: 'archivo'
		 },
		 {
			xtype: 'fileuploadfield',
			id: 'archivoCarga4',
			emptyText: 'Ruta del Archivo',
			name: 'Archivo4',   
			buttonCfg: {				
				text:'Examinar...'
			},
			vtype: 'archivo'
		 },
		 {
			xtype: 'fileuploadfield',
			id: 'archivoCarga5',
			emptyText: 'Ruta del Archivo',
			name: 'Archivo5',   
			buttonCfg: {				
				text:'Examinar...'
			},
			vtype: 'archivo'
		 },
		 {
			xtype: 'fileuploadfield',
			id: 'archivoCarga6',
			emptyText: 'Ruta del Archivo',
			name: 'Archivo6',   
			buttonCfg: {				
				text:'Examinar...'
			},
			vtype: 'archivo'
		 },
		 {
			xtype: 'fileuploadfield',
			id: 'archivoCarga7',
			emptyText: 'Ruta del Archivo',
			name: 'Archivo7',   
			buttonCfg: {				
				text:'Examinar...'
			},
			vtype: 'archivo'
		 },
		 {
			xtype: 'fileuploadfield',
			id: 'archivoCarga8',
			emptyText: 'Ruta del Archivo',
			name: 'Archivo8',   
			buttonCfg: {				
				text:'Examinar...'
			},
			vtype: 'archivo'
		 }
	];
	
	var fp = new Ext.form.FormPanel({
      id: 'forma',
		layout :'anchor',
		width: 300,
		title: 'Subir archivos con datos de las curvas',
		frame: true,
		//collapsible: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		fileUpload: true,
		labelWidth: 30,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'			
		},
		items: elementosForma,
		buttons:[
			{
				xtype:'button',
				text:'Subir',
				iconCls: 'upload-icon',
				formBind: true,
				handler	: function(){
					var form = fp.getForm();
					InformacionProyecto('INICIAR_CARGA',form)					
							
				}
			}
		]
	});
	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 900,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			fp,panelResultados,panelError,
			NE.util.getEspaciador(20)
		]
	});
});