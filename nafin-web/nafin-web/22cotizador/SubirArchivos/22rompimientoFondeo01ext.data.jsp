<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		com.netro.exception.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*,
		com.netro.credito.*"
	errorPage= "/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion   = request.getParameter("informacion") == null?"": (String)request.getParameter("informacion");
String archivo       = request.getParameter("archivo")     == null?"": (String)request.getParameter("archivo");
String mensaje       = "";
String infoRegresar  = "";
boolean success      = true;
JSONObject resultado = new JSONObject();

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("SubirArchivo")){

	success = true;

	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType);
	request.setAttribute("myContentType", myContentType);

	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try{
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(2097152);
		myUpload.upload();
		log.info("El archivo se cargó correctamente en memoria");
	} catch(Exception e){
		success = false;
		log.warn("SubirArchivo(Exception): Cargar en memoria el contenido del archivo: " + e);
		if(myUpload.getSize() > 2097152) {
			throw new AppException("El Archivo es muy Grande, excede el Límite que es de 2 MB.");
		} else{
			throw new AppException("Problemas al Subir el Archivo."); 
		}
	}

	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest = myUpload.getRequest();

	// Guardar Archivo en Disco
	int numFiles = 0;
	try{
		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," "));
		mensaje = "OK";
		log.info("El archivo se guardó correctamente en Disco");
	} catch(Exception e){
		success = false;
		log.warn("SubirArchivo(Exception): Guardar Archivo en Disco: " + e);
		throw new AppException("Ocurrió un error al guardar el archivo");
	}

	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje             );
	infoRegresar = resultado.toString();

}
%>
<%=infoRegresar%>