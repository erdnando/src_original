Ext.onReady(function() {

form = { info: "vacio"};
	 
var myValidFn = function(v) {
	var myRegex = /^.+\.([cCtT][sSxX][vVtT])$/;
	return myRegex.test(v);
};

Ext.apply(Ext.form.VTypes, {
	archivozip 		: myValidFn,
	archivozipText 	: 'Formato de archivo no v�lido.<br> Formato soportado:  csv - txt'
});



//---------------------------------------------------------------------Moneda----------------------------------------------------------------
var Moneda = Ext.data.Record.create([ 
 {name: "clave", type: "string"}, 
 {name: "descripcion", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//------------------------------------------------------------------Fin Moneda---------------------------------------------------------------

//--------------------------------------------------------------- procesarMoneda -------------------------------------------------------------
var procesarMoneda = function(store, arrRegistros, opts) {
	store.insert(0,new Moneda({ 
	clave: "", 
	descripcion: "Seleccione Moneda", 
	loadMsg: ""})); 		
	Ext.getCmp('cmbmon').setValue("");
	store.commitChanges(); 
};	
//---------------------------------------------------------------Fin procesarMoneda-----------------------------------------------------------

//-----------------------------------------------------------------Catalogo Moneda-----------------------------------------------------------
var catalogoMoneda = new Ext.data.JsonStore({
  id					: 'catalogoMoneda',
  root 				: 'registros',
  fields 			: ['clave', 'descripcion', 'loadMsg'],
  url 				: 'frmFactoresRiesgoExt01.data.jsp',
  baseParams		: {
	 informacion	: 'catalogomon'  
						  },
  totalProperty 	: 'total',
  autoLoad			: true,
  listeners			: {
	 load				: procesarMoneda,
	 beforeload		: NE.util.initMensajeCargaCombo,
	 exception		: NE.util.mostrarDataProxyError								
						 }
});//-----------------------------------------------------------Fin Catalogo Moneda----------------------------------------------------------



//-------------------------------elementosForma---------------------------------	
var elementosForma =  [	
	{
	  xtype			: 'combo',	
	  name			: 'cmb_mon',	
	  hiddenName	: 'cmb_mon_h',	
	  id				: 'cmbmon',	
	  fieldLabel	: 'Moneda',
	  displayField : 'descripcion',
	  valueField 	: 'clave',
	  //emptyText		: 'Seleccionar',
	  mode				: 'local',
	  forceSelection : true,	
	  triggerAction  : 'all',	
	  editable    : true,
	  typeAhead		: true,
	  minChars 		: 1,	
	  anchor			:'80%',
	  store 			: catalogoMoneda
	},{
	 xtype	: 'displayfield',
	 id      : 'idLabel',
	 name		: 'nameTitulo',
	 hiddenName: 'hidTitulo',
	 value		: '<center><b><font color="blue">Subir Archivo con los Factores de Riesgo</font></b></center>',
	 anchor  : '80%'
	}, {
	xtype: 'fileuploadfield',
	id: 'archivoCarga',
	allowBlank : false,
	name: 'archivoPC', 
	anchor: '90%',
	vtype: 'archivozip',
	buttonCfg: {
		//iconCls: 'upload-icon',
		text:'Examinar'
		}	
	}
];
//-----------------------------Fin elementosForma-------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: '<center>Factores Riesgo</center>',
		frame: true,		
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
		{
		text: 'Subir Archivo',
		id: 'btnConsulta',
		iconCls: 'upload-icon',
		handler: function (boton, evento){
		 if ( Ext.getCmp('cmbmon').getValue() !="" ){
			if ( Ext.getCmp('archivoCarga').getValue( ) =="" ){
					Ext.Msg.show({
					title: 'Archivo',
					msg: 'No especific� ningun archivo.',
					modal: true,
					icon: Ext.Msg.INFO,
					buttons: Ext.Msg.OK,
					fn: function (){
						var arc = Ext.getCmp('archivoCarga');
						 arc.focus();
						}
					});
			 } else if ( ! Ext.getCmp('archivoCarga').isValid()){
					Ext.Msg.show({
					title: 'Alta de Archivos',
					msg: 'La extensi�n del archivo no est� permitida.',
					modal: true,
					icon: Ext.Msg.INFO,
					buttons: Ext.Msg.OK,
					fn: function (){
						var arc = Ext.getCmp('archivoCarga');
						 arc.focus();
						}
					});
				} else {	// Todo OK	
					fp.el.mask('Cargando...', 'x-mask-loading');			 
					var form = fp.getForm();
					form.submit({
			      url:'frmFactoresRiesgoExt01.data.jsp',
					success: function(form, action, resp) {
						fp.el.unmask();	
						var jsonData=Ext.decode(action.response.responseText);
						if(jsonData.exito){
							var mensaje = '';
							if(jsonData.msg == 'OK'){
								mensaje = 'Los datos del archivo '+ Ext.getCmp('archivoCarga').getValue() + '<br>han sido guardados en la base de datos.';
							} else if(jsonData.msg == 'ERROR'){
								mensaje = 'No se pudo guardar en la base de datos el archivo ' + Ext.getCmp('archivoCarga').getValue() + '<br>Verifique que tenga el formato correcto.';
							} else{
								mensaje = jsonData.msg;
							}
							Ext.Msg.show({
							title: 'Resultados',
							msg: mensaje,
							modal: true,
							icon: Ext.Msg.INFO,
							buttons: Ext.Msg.OK,
							fn: function (){
								Ext.getCmp('archivoCarga').reset();
								}
							});								
						} else {
							Ext.Msg.show({
							title: 'Resultados',
							msg: jsonData.msg,
							modal: true,
							icon: Ext.Msg.ERROR,
							buttons: Ext.Msg.OK,
							fn: function (){
								Ext.getCmp('archivoCarga').reset();
								}
							});
						}
					},
					failure: NE.util.mostrarSubmitError
					});
					
					} //FIN OK 
		 } else {
					Ext.Msg.show({
					title: 'Moneda',
					msg: 'Seleccione una moneda.',
					modal: true,
					icon: Ext.Msg.INFO,
					buttons: Ext.Msg.OK,
					fn: function (){}
					});
				}

				}//fin Handler
			}
		]
	});
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20)
		]
	});	
//-----------------------------Fin Contenedor Principal-------------------------

});//-----------------------------------------------Fin Ext.onReady(function(){}