<%@ page contentType="text/html;charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		com.jspsmart.upload.*,
		com.netro.cotizador.*,
		com.netro.exception.*, 
		com.netro.model.catalogos.CatalogoMoneda,
		netropology.utilerias.negocio.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,
		org.apache.commons.logging.Log  " 
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload" />
<jsp:useBean id="leeArchivo" scope="page" class="netropology.utilerias.negocio.SubeArchivoBean" /> 
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%    
String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String infoRegresar	= "";
String consulta   	= "";
String archivo 		= "";
String mensaje 		= "";
boolean existe 		= true;
boolean excede			= false;
try { 
	if (leeArchivo.load()){
		String path=strDirectorioTemp;
		netropology.utilerias.negocio.Global.setRutaFisicaArchivoFactoresRiesgo(strDirectorioTemp);

		myUpload.initialize(pageContext);
		try {
			myUpload.setTotalMaxFileSize(100000);
			myUpload.upload();
		} catch (Exception e){
			excede=true;
		}
		if (!excede){
		String moneda = myUpload.getRequest().getParameter("cmb_mon_h");		
		for (int i=0;i<myUpload.getFiles().getCount();i++){
			com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(i);
			if(!myFile.isMissing()){
				if (myFile.getSize()>0){
					myFile.saveAs(path + myFile.getFileName());
					archivo= path + myFile.getFileName();
					if (leeArchivo.leeFactoresRiesgo(archivo,moneda)){
						mensaje = "OK"; // Los datos del archivo han sido guardados en la base de datos
					} else {
						mensaje = "ERROR"; // Error al guardar los datos del archivo en la base de datos
					}
				} else {
					mensaje = "El archivo especificado no existe o tiene un formato incorrecto.";
				}
			} else {
				mensaje = "No especificó ningún archivo.";
			}
		}// FIN FoR
	 } else {
		existe=false;
		mensaje = "El archivo excede el tamaño permitido.";
	 }
	} else {
		existe=false;
		mensaje = "No hay conexión con la base de datos.";
	}
	JSONObject	 jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("exito", new Boolean(existe));
	jsonObj.put("msg", mensaje );		
	infoRegresar = jsonObj.toString();
} catch (Exception e){
	existe = false;
	log.error("Error : "+e);
	JSONObject	 jsonObj 	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("exito", new Boolean(existe));
	jsonObj.put("msg", mensaje );
	infoRegresar = jsonObj.toString(); 
} finally{
  leeArchivo.release();
}

if (informacion.equals("catalogomon") )	{
	
  CatalogoMoneda catalogoMoneda =  new CatalogoMoneda();
  catalogoMoneda.setCampoClave("ic_moneda");
  catalogoMoneda.setCampoDescripcion("cd_nombre"); 
  catalogoMoneda.setValoresCondicionIn("1,54", Integer.class);
  catalogoMoneda.setOrden("CLAVE");
  List l = catalogoMoneda.getListaElementos();  
  
	JSONObject	jsonObj  = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("total",	  new Integer( l.size()) );	
	jsonObj.put("registros",  l );	
   infoRegresar = jsonObj.toString();
} 

%>
<%=  infoRegresar %>


