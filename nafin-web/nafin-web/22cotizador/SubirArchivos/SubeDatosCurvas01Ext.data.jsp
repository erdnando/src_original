<%@ include file="../22secsession.jspf" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="com.jspsmart.upload.*,
	netropology.utilerias.negocio.*,net.sf.json.JSONArray,  
		net.sf.json.JSONObject "
	errorPage="/00utils/error_extjs_fileupload.jsp"
%> 
<jsp:useBean id="mySmartUpload" scope="page" class="com.jspsmart.upload.SmartUpload" />  
<jsp:useBean id="leeArchivo" scope="page" class="netropology.utilerias.negocio.SubeArchivoBean" />  
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	String 	PATH_FILE = strDirectorioTemp;	
	String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
	String estadoSiguiente = null;	
	String horaHoy		= new java.text.SimpleDateFormat("hh:mm aaa").format(new java.util.Date());
	 String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	boolean resultado = false;
	
try{
  if (leeArchivo.load()){
    int numUploads=0;
    int numInserts=0;
	 StringBuffer msgArchivosGuardados = new StringBuffer();
	 StringBuffer msgArchivosNOGuardados = new StringBuffer();
    String path=strDirectorioTemp;
    netropology.utilerias.negocio.Global.setRutaFisicaArchivosCurvas(strDirectorioTemp);
    mySmartUpload.initialize(pageContext);
    mySmartUpload.setAllowedFilesList("csv,txt,,");
    mySmartUpload.setTotalMaxFileSize(10000000);
    mySmartUpload.upload();
    
		JSONArray 	resultadosDataArray 	= new JSONArray();
		JSONArray	registro						= null;
	 // Selecciona cada archivo
	 for (int i=0;i<mySmartUpload.getFiles().getCount();i++){
      com.jspsmart.upload.File myFile = mySmartUpload.getFiles().getFile(i);
      if (!myFile.isMissing()) {
          myFile.saveAs(strDirectorioTemp + myFile.getFileName());
			 jsonObj.put("nombreArch",myFile.getFileName());
          //  Despliega las propiedades del archivo 
  
          numUploads ++;
          String archivo= path + myFile.getFileName();
			 try{
					resultado = leeArchivo.leeDatosCurva(archivo,strNombreUsuario);
			  }catch (Exception e){
					System.out.println("   resultado  0 ");
					resultado= false;
					e.printStackTrace();
					
				}
          if (resultado == true){ 
            numInserts ++;
				msgArchivosGuardados.append("Los datos del archivo "+myFile.getFileName()+"	han sido guardados en la base de datos<br>");
          }
          else{
			 msgArchivosGuardados = new StringBuffer();
				msgArchivosGuardados.append("No se pudo guardar en la base de datos el archivo "+myFile.getFileName()+". Verifique que el nombre del archivo sea del formato: \"aaaammddNOMBRECURVA.ext\"<br>");
			 //No se pudo guardar en la base de datos el
		  }
      }//fin del if
    } //fin del for 
	 estadoSiguiente	=	"MOSTRAR_RESULTADOS";
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("estadoSiguiente",estadoSiguiente);
		jsonObj.put("archivosCargados",new Integer(numUploads));
		jsonObj.put("archivosGuardados",new Integer(numInserts));
		jsonObj.put("horaActual",horaHoy);
		String user=strLogin+" "+strNombreUsuario;
		jsonObj.put("usuario",user);
		jsonObj.put("msgArchivosGuardados",msgArchivosGuardados.toString());
		infoRegresar =jsonObj.toString();
  }else{
		estadoSiguiente ="ESPERAR_DESICION";
		jsonObj.put("estadoSiguiente",estadoSiguiente);
		jsonObj.put("leeArchivo",new Boolean(false));
		jsonObj.put("leeArchivoMSG","No hay conexión con la BD");	
	  infoRegresar =jsonObj.toString();
  }
  
}catch (Exception e){
  e.printStackTrace();
	estadoSiguiente = "MOSTRAR_ERROR";
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("estadoSiguiente",estadoSiguiente);
	jsonObj.put("origen","SubeDatosCurvas01Ext.jsp");
	jsonObj.put("url","../22MuestraErrores.jsp");
	String err =org.apache.commons.lang.StringEscapeUtils.escapeJava(e.getMessage());
	jsonObj.put("titulo","Subir Archivos con Datos de las Curvas");
	jsonObj.put("mensajeError",err );  
	infoRegresar =jsonObj.toString();

}finally{
  leeArchivo.release();
}
	

%>

<%=infoRegresar%>

