<!DOCTYPE  HTML> 
<%@  page 
	import="java.util.*, java.text.SimpleDateFormat, com.netro.exception.*" 
	contentType="text/html;charset=windows-1252"
	errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf"%>
<html>
  <head>
    <title>.:: Factores Riesgo ::.</title>	
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    
    <%@ include file="/extjs.jspf" %>
		<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
		<script type="text/javascript" src="frmFactoresRiesgoExt01.js?<%=session.getId()%>"></script>  
		<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
    <%if(esEsquemaExtJS) {%>
			<%@ include file="/01principal/menu.jspf"%>		
		<%}%>
  </head>

<%if(esEsquemaExtJS) {%>

  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <%@ include file="/01principal/01nafin/cabeza.jspf"%>
  <div id="_menuApp"></div>
  <div id="Contcentral">  
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:230px"></div></div>
  </div>
	</div>    
  <%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>  
  </body>  
  
<%}else  {%>
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
      <div id="areaContenido"></div>
    <form id='formAux' name="formAux" target='_new'></form>
  </body>
<%}%> 
  
</html>
