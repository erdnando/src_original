<!DOCTYPE html>
<%@ page import="java.util.*" 
	contentType="text/html;charset=windows-1252"
	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/22cotizador/22secsession.jspf" %>
<html> 
	<head>
		<title>Nafinet</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta http-equiv="X-UA-Compatible" content="IE=9" /><!-- Evaluar el funcionamiento de este tag-->
		
		<%@ include file="/extjs.jspf" %>
		<%@ include file="/01principal/menu.jspf"%>
		<script type="text/javascript" src="SubeDatosCurvas01Ext.js?<%=session.getId()%>"></script>
		<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
		<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
		<!--script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script-->
	</head>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%if(esEsquemaExtJS) {%>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
				<div id="areaContenido"><div style="height:230px"></div></div>
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
		<%}else  {%>
			<div id="Contcentral">
				<div id="areaContenido"><div style="height:230px"></div></div>
			</div>
			</div>
			<form id='formAux' name="formAux" target='_new'></form>	
		<%}%>
	</body>
</html>