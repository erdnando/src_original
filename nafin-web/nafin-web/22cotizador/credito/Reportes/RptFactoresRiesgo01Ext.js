
//Factores de Riesgo
Ext.onReady(function(){

//variables globales 
var _URL_ = 'RptFactoresRiesgo01Ext.data.jsp';
var gridConsulta= null;
//------------------------MAQUINA DE ESTADOS------------------------------------
	
	var procesarFactoresRiesgo = function(opts,success,response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						factoresRiesgo(resp.estadoSiguiente,resp);
					}
				);
			} else {
			
				factoresRiesgo(resp.estadoSiguiente,resp);
			}
		}else{
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
		}
	}
	var factoresRiesgo = function(	estado,	respuesta	){
		if(	estado ==	'INICIALIZACION'){
			//CARGAR CATALOGO MONEDA
			catalogoMoneda.load({
				
			});
		}else if(	estado ==	'PROCESAR_SELECCION'){
		//PERICION AJAX CON EL VALOR DEL CATALOGO MONEDA---->(1)
		if(gridConsulta!=null){
			Ext.getCmp('gridConsulta').hide();	
		}
		gridConsulta= null;
		var fp = Ext.getCmp('forma');
		fp.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: _URL_,
			params: {
				informacion: 'FactoresRiesgo.Consultar',
				moneda		: respuesta
			},
			callback: procesarFactoresRiesgo
		});
		
		
	}else if(	estado	==	'MOSTRAR_CONSULTA'	){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var consulta = new Ext.data.JsonStore({
			root		: 'registros',
			url		: _URL_,
			baseParams: {
				informacion	:	'FactoresRiesgo.Consulta'
			},
			fields: (respuesta.fields),
			data: {'registros':(respuesta.data)},
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false
		});
		 gridConsulta = new Ext.grid.GridPanel({
			id			: 'gridConsulta',
			title	:	respuesta.tituloGrid,
			//hidden	: true,
			header	:true,
			loadMask	: true,	
			height	: 400,
			width		: 940,
			store		: consulta,
			style		: 'margin:0 auto;',
			columns	:(respuesta.cols),
			displayInfo: true,		
			emptyMsg: "No hay registros.",		
			loadMask: true,
			stripeRows: true,
			height: 400,
			width: 900,
			align: 'center',
			frame: false,
			buttons:[
				{
					xtype: 'button',
					text: 'Generar Archivo', 
					id: 'btnImprimirCSV',
					iconCls: 'icoXls',
					handler: function(boton, event) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var forma = fp.getForm().getValues().HcbMoneda;
						factoresRiesgo('GENERAR_CSV',forma);
					}
				},
				{
					xtype: 'button',
					text: 'Generar PDF', 
					id: 'btnImprimirPDF',
					iconCls: 'icoPdf',
					handler: function(boton, event) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var forma = fp.getForm().getValues().HcbMoneda;
						factoresRiesgo('GENERAR_PDF',forma);
					}
				}
			]
		});
		
		contenedorPrincipal.add(gridConsulta);
		contenedorPrincipal.add(NE.util.getEspaciador(5));
		contenedorPrincipal.doLayout();
		Ext.getCmp('labelPrograma').hide();
		
		
	}else if (	estado	==	'GENERAR_PDF'	){
		Ext.Ajax.request({
			url	:	_URL_,
			params:{
				informacion	:	'FactoresRiesgo.gerenarPDF',
				moneda		:	respuesta,
				tipo	:	'PDF'
			},success:function(response){
				var btnGenerar = Ext.getCmp('btnImprimirPDF');
				btnGenerar.setIconClass('icoPdf');
				btnGenerar.enable();
			},
			callback			:	procesarDescargaArchivoSuccess 
		});
	}else if	(	estado	==	'GENERAR_CSV'	){
	//�PETICION AJAX PARA DESCARGAR CSV
		Ext.Ajax.request({
			url	:	_URL_,
			params:{
				informacion	:	'FactoresRiesgo.gerenarCSV',
				moneda		:	respuesta,
				tipo :'CSV'
			},success:function(response){
				var btnGenerar = Ext.getCmp('btnImprimirCSV');
				btnGenerar.setIconClass('icoXls');
				btnGenerar.enable();
			},
			callback			:	procesarDescargaArchivoSuccess 
		});
		
	}else if	(	estado	==	'ESPERAR_DESICION'){
		//NO HACER NADA
		Ext.getCmp('labelPrograma').show();
		var fp = Ext.getCmp('forma');
		fp.el.unmask();	
	}
}
	

//-------------------------HANDDLER's-------------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
/*	var procesarConsultaData = function(store, arrRegistros, opts) 	{
	var fp = Ext.getCmp('forma');
		var el = grid.getGridEl();	
	//	fp.el.unmask();
	
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}			
								
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {					
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
*/
//------------------------------STRORE's----------------------------------------

	var catalogoMoneda = new Ext.data.JsonStore ({
	   id			: 'catologoMonedaDataStore',
		root 		: 'registros',
		fields 	: ['clave', 'descripcion', 'loadMsg'],
		url 		: _URL_,
		baseParams: 
			{
				informacion	:	'FactoresRiesgo.cargaCatalogoMoneda'
			},
		totalProperty	: 'total',
		autoLoad	: false,
		listeners:
			{
				exception	:	NE.util.mostrarDataProxyError,
				beforeload	:	NE.util.initMensajeCargaCombo
			}
  });
  
	
//-----------------------COMPONENTES VISUALES-----------------------------------	
var programa = new Ext.Container ({
		layout: 'table',
		id: 'labelPrograma',
		width: '481',
		heigth: 'auto',
		style: 'margin: 0 auto',
		align: 'center',
		hidden:true,
		defaults:{
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
			{
				xtype: 'displayfield',
				value: '<div align=center style="width:481px;"><div align=center> <font size="3"><b>No se encontraron registros</b></font></div>'
			}
		]
	});
	var elementosForma = [
		{
			xtype			: 'combo',
			fieldLabel	: 'Moneda',
			displayField: 'descripcion',
			valueField	: 'clave',
			triggerAction: 'all',
			typeAhead	: true,
			allowBlank	: false,
			minChars		: 1,
			name			:'HcbMoneda',
			id				: 'cbMoneda',
			mode			: 'local',
			forceSelection : true,
			allowBlank	: false,
			hiddenName	: 'HcbMoneda',
			hidden		: false,
			emptyText	: 'Seleccionar Moneda',
			store			: catalogoMoneda,
			tpl			: NE.util.templateMensajeCargaCombo,
			listeners:{
				'select': function(v){
					factoresRiesgo('PROCESAR_SELECCION',Ext.getCmp('cbMoneda').getValue());
				}
			}
		}
	];
	
	var fp = new Ext.form.FormPanel({
		hidden		: false,
		height		: 'auto',
		width			: 600,
		collapsible	: false,
		titleCollapse: false,
		style			: 'margin:0 auto;',
		bodyStyle	: 'padding: 6px',
		labelWidth	: 170,
		frame			: true,
		id				: 'forma',
		defaults		: {
			msgTarget: 'side',
			anchor	: '-20'
		},
		monitorValid: true,
		items: elementosForma
	});

//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		renderHidden:	true,
		height: 	'auto',
		items: 	[NE.util.getEspaciador(10),fp,NE.util.getEspaciador(20),programa],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Derefenrencia ultimo elemento...
			element = null;

		}

	});
factoresRiesgo('INICIALIZACION',null);

});
