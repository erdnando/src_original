Ext.onReady(function() {
	
	var procesarConsultaData = function(store, arrRegistros, response) {
		var gridPrincipal = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!gridPrincipal.isVisible()) {
				grid.show();
			}
			var el = gridPrincipal.getGridEl();
			var jsonData = store.reader.jsonData;	
         var resp = 	Ext.util.JSON.decode(response.responseText);
			
			if(resp.CREDITO_RIESGO=='S'){
				grid.getColumnModel().setHidden(3,false);
				grid.getColumnModel().setHidden(5,false);
			}
			if(resp.RIESGO=='S'){
				grid.getColumnModel().setHidden(10,false);
				grid.getColumnModel().setHidden(12,false);
				grid.getColumnModel().setHidden(15,false);
			}
			if(resp.ADMIN_IF=='N'){
				grid.getColumnModel().setHidden(18,false);
			}
		
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var consultaDataGrid = new Ext.data.JsonStore({
		root: 'registros',
		url: 'RptCotizacionesext.data.jsp',
		baseParams:{
			informacion: 'Consulta'
		},
		fields:[
			{name: 'REFERENCIA'},
			{name: 'FECH_COTIZACION'},
			{name: 'FECH_DISPOSICION'},
			{name: 'AREA_RESPONSABLE'},
			{name: 'INTERMEDIARIO'},
			{name: 'TIPO_INTERMEDIARIO'},
			{name: 'ACREDITADO'},
			{name: 'MONTO_OPERACION'},
			{name: 'PLAZO_CREDITO'},
			{name: 'UNIDAD_TIEMPO'},
			{name: 'DURACION_CREDITO'},
			{name: 'TASA_CREDITO'},
			{name: 'TASA_CREDITO_CM'},
			{name: 'PLAZO_VIGENCIA'},
			{name: 'NPAGOS_INTE'},
			{name: 'PER_GRACIA_INTE'},
			{name: 'NUM_PAGOS_CAP'},
			{name: 'PER_GRACIA_CAP'},
			{name: 'ESTATUS'},
			{name: 'CREDITO_RIESGO'},
			{name: 'RIESGO'},
			{name: 'ADMIN_IF'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					if (type == "remote") {
						Ext.MessageBox.alert("Error", response.errors.reason);
					} else {
						Ext.MessageBox.alert("Warning", "Cannot load data");
					}
				}
			}
		}
	});
	
	var grid = new Ext.grid.GridPanel({ 
		xtype: 'grid',
		store:consultaDataGrid,
      style :  'margin: 0 auto;',
		height: 433,
		width: 900,
		stateful: true,
		id: 'grid',
		loadMask: true,
		title:'Cr�ditos Confirmados',
		columns:[{
			header:'Referencia',tooltip:'Referencia',dataIndex:'REFERENCIA',
			menuDisabled:true, sorteable: true,hidden:false,
			width:100, resizable: true, align:'left'
		},{
			header:'Fecha Cotizaci�n',tooltip:'Fecha Cotizaci�n',dataIndex:'FECH_COTIZACION',
			menuDisabled:true, sorteable: true,hidden:false,
			width:130, resizable: true, align:'left'
		},{
			header:'Fecha Disposici�n',tooltip:'Fecha Disposici�n',dataIndex:'FECH_DISPOSICION',
			menuDisabled:true, sorteable: true,hidden:false,
			width:130, resizable: true, align:'left'
		},{
			header:'�rea Responsable',tooltip:'�rea Responsable',dataIndex:'AREA_RESPONSABLE',
			menuDisabled:true, sorteable: true, hidden:true,
			width:100, resizable: true, align:'left'
		},{
			header:'Intermediario',tooltip:'Intermediario',dataIndex:'INTERMEDIARIO',
			menuDisabled:true, sorteable: true,hidden:false,
			width:200, resizable: true, align:'left'
		},{
			header:'Tipo de Intermediario',tooltip:'Tipo de Intermediario',dataIndex:'TIPO_INTERMEDIARIO',
			menuDisabled:true, sorteable: true,hidden:true,
			width:100, resizable: true, align:'left'
		},{
			header:'Acreditado',tooltip:'Acreditado',dataIndex:'ACREDITADO',
			menuDisabled:true, sorteable: true,hidden:false,
			width:150, resizable: true, align:'left'
		},{
			header:'Monto de la Operaci�n',tooltip:'Monto de la Operaci�n',dataIndex:'MONTO_OPERACION',
			menuDisabled:true, sorteable: true,hidden:false,
			width:150, resizable: true, align:'center',
			renderer:function(val){ return '<div style="text-align:right">' + val + '</div>'; }
		},{
			header:'Plazo del Cr�dito',tooltip:'Plazo del Cr�dito',dataIndex:'PLAZO_CREDITO',
			menuDisabled:true, sorteable: true,hidden:false,
			width:50, resizable: true, align:'center'
		},{
			header:'Unidad de Tiempo',tooltip:'Unidad de Tiempo',dataIndex:'UNIDAD_TIEMPO',
			menuDisabled:true, sorteable: true,hidden:false,
			width:80, resizable: true, align:'center'
		},{
			header:'Duraci�n Cr�dito en D�as',tooltip:'Duraci�n Cr�dito en D�as',dataIndex:'DURACION_CREDITO',
			menuDisabled:true, sorteable: true,hidden:true,
			width:100, resizable: true, align:'left'
		},{
			header:'Tasa Cr�dito',tooltip:'Tasa Cr�dito',dataIndex:'TASA_CREDITO',
			menuDisabled:true, sorteable: true,hidden:false,
			width:80, resizable: true, align:'right',
			renderer: function(val){ return val + ' %'; }
		},{
			header:'Tasa Cr�dito Curva Mensual',tooltip:'Tasa Cr�dito Curva Mensual',dataIndex:'TASA_CREDITO_CM',
			menuDisabled:true, sorteable: true,hidden:true,
			width:100, resizable: true, align:'left'
		},{
			header:'Plazo Vigencia',tooltip:'Plazo Vigencia',dataIndex:'PLAZO_VIGENCIA',
			menuDisabled:true, sorteable: true,hidden:false,
			width:80, resizable: true, align:'center'
		},{
			header:'N�mero Pagos Intereses',tooltip:'N�mero Pagos Intereses',dataIndex:'NPAGOS_INTE',
			menuDisabled:true, sorteable: true,hidden:false,
			width:80, resizable: true, align:'center'
		},{
			header:'Periodos Gracia Intereses',tooltip:'Periodos Gracia Intereses',dataIndex:'PER_GRACIA_INTE',
			menuDisabled:true, sorteable: true,hidden:true,
			width:80, resizable: true, align:'center'
		},{
			header:'N�mero Pagos Capital',tooltip:'N�mero Pagos Capital',dataIndex:'NUM_PAGOS_CAP',
			menuDisabled:true, sorteable: true,hidden:false,
			width:100, resizable: true, align:'center'
		},{
			header:'Periodos Gracia Capital',tooltip:'Periodos Gracia Capital',dataIndex:'PER_GRACIA_CAP',
			menuDisabled:true, sorteable: true,hidden:false,
			width:80, resizable: true, align:'center'
		},{
			header:'Estatus',tooltip:'Estatus',dataIndex:'ESTATUS',
			menuDisabled:true, sorteable: true,hidden:true,
			width:100, resizable: true, align:'center'
		}]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items:[grid,NE.util.getEspaciador(20)]
	});
});
