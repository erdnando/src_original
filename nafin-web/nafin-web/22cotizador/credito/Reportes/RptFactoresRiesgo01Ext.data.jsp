<%@ page language="java" 
contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
import="java.util.*,
	com.netro.cotizador.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	javax.naming.*,
	org.apache.commons.logging.Log,
	net.sf.json.JSONArray,  
	net.sf.json.JSONObject,
	netropology.utilerias.negocio.ReportesBean,
	netropology.utilerias.*"
%> 

<%@ include file="/appComun.jspf" %>

<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
//Parametros 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 

String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
JSONObject jsonObj = new JSONObject();
String estadoSiguiente = null;

if (informacion.equals("FactoresRiesgo.cargaCatalogoMoneda")){
	
	estadoSiguiente =	"ESPERAR_DESICION";
	
	CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setValoresCondicionIn("1,54", Integer.class);
		cat.setOrden("2");
		List lis = cat.getListaElementos();  
		jsonObj.put("registros", lis);	
		jsonObj.put("estadoSiguiente",estadoSiguiente);
	
	infoRegresar =jsonObj.toString(); 
}else if (informacion.equals("FactoresRiesgo.Consultar")){
	String moneda =	(request.getParameter("moneda")!=null)?request.getParameter("moneda"):"";
	String strGrupoUsuario = strPerfil;
	String strUsuario = strLogin;
	String strFormato = "";
	if (strGrupoUsuario.equalsIgnoreCase("TESORERIA")||strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA")){
		strFormato="##0.0000";
	}else{
		strFormato="##0.00";
	}
	int numTiposInter=0;
	Vector encabezados=null;
	String nombre = "";
	if(moneda.equals("1")){
		nombre = "factoresriesgo1";
	}else{
		nombre = "factoresriesgo54";	
	}

	ReportesBean reporte = new ReportesBean();
		reporte.setMoneda(moneda);
		reporte.setFormato(strFormato);
	if (reporte.load(nombre, strGrupoUsuario, strUsuario)){
		encabezados = reporte.getHeaderFactoresRiesgo(moneda);
		numTiposInter = encabezados.size();
		//Enumeration en = encabezados.elements();
		
		List fields= new ArrayList();//fields del store
		HashMap cols = new HashMap();
		JSONArray regData = new JSONArray(); 
		
		if(numTiposInter > 0){
			fields = new ArrayList();
			fields.add("PLAZO");
			cols = new HashMap(); 	
			cols.put("header", "Plazo"); 
			cols.put("align", "center");
			cols.put("width",new Integer(100));
			cols.put("dataIndex","PLAZO");
			regData.add(cols);
				Registros header = reporte.getEncabezados();
			while(header.next()){
				//titulos = new ArrayList();
				String field =header.getString("descripcion").replaceAll("\\s","_");
				String text = header.getString("descripcion");
				fields.add(field.toUpperCase());
				cols = new HashMap(); 	
				cols.put("header", text); 
				cols.put("align", "center");
				cols.put("width",new Integer(120));
				cols.put("dataIndex",field.toUpperCase());
				regData.add(cols);
			}	
			Registros reg = reporte.getFactoresRiesgo();
			estadoSiguiente="MOSTRAR_CONSULTA";
			jsonObj.put("total",new Integer(reg.getNumeroRegistros()));
			jsonObj.put("data",reg.getJSONData() );
			jsonObj.put("fields",fields);
			jsonObj.put("cols",regData.toString() );	 
		}else{
		//no hay registros
		estadoSiguiente="ESPERAR_DESICION";
		}
	}	
	
	jsonObj.put("estadoSiguiente",estadoSiguiente);
	jsonObj.put("tituloGrid","Lista de Factores de Riesgo por Tipo de Intermediario");
	jsonObj.put("success",new Boolean(true));
	infoRegresar =jsonObj.toString(); //cat.getJSONElementos();	
	
}else if(	informacion.equals("FactoresRiesgo.gerenarPDF")||informacion.equals("FactoresRiesgo.gerenarCSV")){
	String tipo = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):""; 
	String moneda =	(request.getParameter("moneda")!=null)?request.getParameter("moneda"):"";
	String strGrupoUsuario = strPerfil;
	String strUsuario = strLogin;
	String strFormato = "";
	if (strGrupoUsuario.equalsIgnoreCase("TESORERIA")||strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA")){
		strFormato="##0.0000";
	}else{
		strFormato="##0.00";
	}
	
	ReportesBean reporte = new ReportesBean();
	reporte.setMoneda(moneda);
	reporte.setFormato(strFormato);
	Registros header = reporte.getEncabezados();
	Registros reg = reporte.getFactoresRiesgo();
	
	
	String nombreArchivo = reporte.generarPDFCSV(request,tipo,header,reg,strDirectorioTemp);
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	infoRegresar =jsonObj.toString(); 
}
	
	log.debug("infoRegresar = <" + infoRegresar + ">"); 
%>

<%=infoRegresar%>