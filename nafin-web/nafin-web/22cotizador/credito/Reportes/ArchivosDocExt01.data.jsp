	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	netropology.utilerias.negocio.*,
	net.sf.json.JSONObject,
	com.netro.cotizador.*,
	java.io.*"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
	
	<%
	
	String informacion = request.getParameter("informacion")==null?"":request.getParameter("informacion");
	
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String archivos="";

	//String path = "../../../00archivos/22cotizador/archivos/";
	Vector vecFilas 	= null;
	Vector vecColumnas 	= null;
	String icArchivo	= "";
	String nomArchivo	= "";
	String descArchivo	= "";

if(informacion.equals("ConsultaArchivos"))	{
	
	try{
		MantenimientoCot mantCot = ServiceLocator.getInstance().lookup("MantenimientoCotEJB", MantenimientoCot.class);
		
		vecFilas = mantCot.consultaArchivo("");
		if (vecFilas.size()==0){
			archivos+="<tr><td align='center' class='formas' colspan='3'>No hay archivos disponibles</td></tr>	";
		} else {
					for(int i=0;i<vecFilas.size();i++){
						
						vecColumnas = (Vector)vecFilas.get(i);
						icArchivo	= (String)vecColumnas.get(0);
						nomArchivo 	= (String)vecColumnas.get(1);
						descArchivo = (String)vecColumnas.get(2);
						//archivos+="<tr><td align='center' class='formas'><a href='"+path+nomArchivo+"' target='_new'>"+descArchivo+"</td></tr>";
						archivos+="<tr><td align='center' class='formas'><a href='javascript:muestraArchivo(\""+icArchivo+"\")' >"+descArchivo+"</td></tr>";
						archivos+="<tr><td align='center' class='formas'> <br>  </td></tr>";
				
					}		
		}	
		jsonObj.put("Archivos",archivos);
		jsonObj.put("success",new Boolean(true));
		infoRegresar = jsonObj.toString();

		
	}catch (Exception e){
		System.out.println("----------------------");
		System.out.println("Error: ArchivosDocExt01.JSP");
		e.printStackTrace();
		System.out.println("----------------------"); 
	}
}else if(informacion.equals("DescargaArchivo")){
	MantenimientoCot mantCot = ServiceLocator.getInstance().lookup("MantenimientoCotEJB", MantenimientoCot.class);
	icArchivo = request.getParameter("icArchivo")==null?"":request.getParameter("icArchivo");
	
	
	String nombreArchivo = mantCot.getArchivoTesoreria(icArchivo,strDirectorioTemp );
	jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success",new Boolean(true));
	infoRegresar = jsonObj.toString();
}
%>

<%=infoRegresar%>