	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,
	netropology.utilerias.negocio.*,
	net.sf.json.JSONObject,
	net.sf.json.JSONArray,
	com.netro.pdf.*"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
	
		<%
	
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String informacion         =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	
	if(informacion.equals("Consulta")){
	try{
		String strGrupoUsuario=strPerfil;
		
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo = null;  
		
		CreaArchivo archivoPDF = new CreaArchivo();
		String nombreArchivoPDF = archivo.nombreArchivo()+".pdf";
		
		String strUsuario=strLogin;
		String strFormato="";
		if (strGrupoUsuario.equalsIgnoreCase("TESORERIA")||strGrupoUsuario.equalsIgnoreCase("ADMIN TESORERIA"))
		  strFormato="##0.0000";
		else
		  strFormato="##0.00";
		  
		
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivoPDF);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	    pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	                                 (session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString(),
			    					 (String)session.getAttribute("sesExterno"),
	                                 (String) session.getAttribute("strNombre"),
                                     (String) session.getAttribute("strNombreUsuario"),
						    		 (String)session.getAttribute("strLogo"),strDirectorioPublicacion);			

		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
		
		ReportesBean reporte=new ReportesBean();
		if (reporte.load("rompimientofondeo", strGrupoUsuario, strUsuario)){
			netropology.utilerias.negocio.Global.setRutaFisicaArchivoRompFondeo(strDirTrabajo);
			String renglon[]=reporte.getRompimientoFondeo();
			HashMap dato;
			List reg=new ArrayList();
			contenidoArchivo.append("Plazo del Crédito Solicitado,-0.5 pp,-1.0 pp, -1.5 pp,-2.0 pp,-2.5 pp,-3.0 pp,-3.5 pp,-4.0 pp,-4.5 pp,-5.0 pp \n");
			
			pdfDoc.setTable(11); 
		// titulos del Pdf
		pdfDoc.setCell("Plazo del Crédito Solicitado","celda01",ComunesPDF.CENTER); 
		pdfDoc.setCell("-0.5 pp","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("-1.0 pp","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("-1.5 pp","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("-2.0 pp","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("-2.5 pp","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("-3.0 pp","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("-3.5 pp","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("-4.0 pp","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("-4.5 pp","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("-5.0 pp","celda01",ComunesPDF.CENTER);
			
			for (int i=0; i<renglon.length; i++){
				StringTokenizer datos=new StringTokenizer(renglon[i], ",");
				
				while(datos.hasMoreElements()){
					dato=new HashMap();
					dato.put("PLAZO",datos.nextToken());
					dato.put("COL1",InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+"%");
					dato.put("COL2",InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+"%");
					dato.put("COL3",InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+"%");
					dato.put("COL4",InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+"%");
					dato.put("COL5",InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+"%");
					dato.put("COL6",InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+"%");
					dato.put("COL7",InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+"%");
					dato.put("COL8",InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+"%");
					dato.put("COL9",InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+"%");
					dato.put("COL10",InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+"%");
					reg.add(dato);
					
					
					//Archivos
				
				 }
				 datos=new StringTokenizer(renglon[i], ",");
				 while(datos.hasMoreElements()){
				 	contenidoArchivo.append(datos.nextToken()+
					","+InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+
					","+InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+
					","+InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+
					","+InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+
					","+InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+
					","+InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+
					","+InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+
					","+InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+
					","+InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+
					","+InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato)+
					"\n");
				}
				 datos=new StringTokenizer(renglon[i], ",");
				 while(datos.hasMoreElements()){
					pdfDoc.setCell(datos.nextToken(),"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato),"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato),"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato),"formas",ComunesPDF.CENTER); 				
					pdfDoc.setCell(InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato),"formas",ComunesPDF.CENTER); 			
					pdfDoc.setCell(InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato),"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato),"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato),"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato),"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato),"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(InformacionGeneralBean.formatoNumeros(Double.parseDouble(datos.nextToken())*100, strFormato),"formas",ComunesPDF.CENTER); 
				 }
			 }
			pdfDoc.addTable();
	
			pdfDoc.addText("Instrucciones:\n\n","formas",ComunesPDF.LEFT);
			pdfDoc.addText("1. Obtener una cotización consistente con las características del crédito solicitado, en términos de plazo, frecuencias de pagos, etc.\n\n","formas",ComunesPDF.LEFT);
			pdfDoc.addText("2. Comparar la cotización obtenida con la tasa tomada \"en firme\":\n","formas",ComunesPDF.LEFT);
			pdfDoc.addText("    a) Si (cotización - tasa en firme) < 0, aplicar comisión conforme a la tabla, considerando el plazo del crédito y la diferencia en puntos porcentuales entre ambas tasas.\n","formas",ComunesPDF.LEFT);
			pdfDoc.addText("    b) Si (cotización - tasa en firme) > = 0, no aplicará el cobro de la comisión.\n\n","formas",ComunesPDF.LEFT);
			pdfDoc.addText("3. El monto de comisión a cobrar será el resultado de multiplicar los porcentajes de comisión de la tabla por el monto de la operación solicitada.\n\n","formas",ComunesPDF.LEFT);
			pdfDoc.addText("Nota:  Los porcentajes de comisión no incluyen IVA","formas",ComunesPDF.LEFT);
			pdfDoc.endDocument(); 
			
			if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
				out.print("<--!Error al generar el archivo-->");
			else
				nombreArchivo = archivo.nombre;
			
			
			jsonObj.put("ArchivoCSV",strDirecVirtualTemp+nombreArchivo);
			jsonObj.put("ArchivoPDF",strDirecVirtualTemp+nombreArchivoPDF);
			jsonObj.put("registros", reg);
			jsonObj.put("success",new Boolean(true));
			infoRegresar=jsonObj.toString();
			 
		}
		}catch(Exception e){
		  System.out.println("----------------------");
		  e.printStackTrace();
		  System.out.println("----------------------"); 
		  jsonObj.put("Error",e.getMessage());
		  jsonObj.put("success",new Boolean(false));
		  infoRegresar=jsonObj.toString();
		  }
		
	}
	
%>

<%=infoRegresar%>