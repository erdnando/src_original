Ext.onReady(function(){
var codigoPanel='';
var nombreCurva;
//------------------------Handlers----------------------------
var procesarValoeresIniciales = function(opts, success, response){
	 pnl.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success1 == true) {
		if(Ext.util.JSON.decode(response.responseText).comisionTF){
			codigoPanel+='<tr><td><a href="/nafin/00archivos/22cotizador/comisionTFMXN.pdf" target="_new">'+
			'<font size="1"><LI>Cr�ditos a Tasa Fija en Moneda Nacional</LI></font></a></td></tr>';
		}
		if(Ext.util.JSON.decode(response.responseText).comisionTP){
			codigoPanel+='<tr><td><a href="/nafin/00archivos/22cotizador/comisionTFMXN.pdf" target="_new">'+
			'<font size="1"><LI>Cr�ditos con Tasa Protegida en Moneda Nacional</LI></font></a></td></tr>';
		}
		if(Ext.util.JSON.decode(response.responseText).comisionTUS){
			codigoPanel+='<tr><td><a href="/nafin/00archivos/22cotizador/comisionTFMXN.pdf" target="_new">'+
			'<font size="1"><LI>Cr�ditos a Tasa Fija en D�lares</LI></font></a></td></tr>';
		}
		Ext.getCmp('Mensaje').setValue( '<table align="center">'+
		'<tr>'+
			'<td align="center" class="textos">'+
				'<br>'+
				'<hr>'+
				'<br>'+
				'FORMULAS PARA EL CALCULO DE COMISIONES<br>POR PAGO ANTICIPADO TOTAL O PARCIAL DE RECURSOS'+
				'<br>'+
				'<br>'+
			'</td>'+
		'</tr>'+codigoPanel+'<tr>'+
			'<td align="center" class="textos">'+
				'<br>'+
				'<hr>'+
				'<br>'+
				'CURVAS APLICABLES A LAS FORMULAS DE PREPAGO'+
			'</td>'+
		'</tr>'+
		'<TR>'+
			'<TD align="center">'+
				'<br>'+
				'<br>'+
				'<a href="?nombreCurva=ValmerCETES"><font size="1">CURVA NOMINAL LIBRE DE RIESGO "Valmer CETES"<br>'+
				'(Curva para el calculo de la comisi�n por pago anticipado de recursos aplicable a cr�ditos con tasa protegida)</font></a>'+
			'</TD>'+
		'</TR>'+
		'<TR>'+
			'<TD align="center">'+
				'<br>'+
				'<a href="?nombreCurva=NafinSWAP"><font size="1">CURVA NOMINAL LIBRE DE RIESGO "NafinSWAP"<br>'+
				'(Curva para el calculo de la comisi�n por pago anticipado de recursos aplicable a cr�ditos con Moneda Extranjera)</font></a>'+
				'<HR>'+
			'</TD>'+
		'</TR>'+
	'</table>');
		panelJSP.doLayout();
	}
	if (success == true && Ext.util.JSON.decode(response.responseText).success2 == true) {
		grid.setVisible(true);
		if(Ext.util.JSON.decode(response.responseText).Totales>0){
		consulta.loadData(Ext.util.JSON.decode(response.responseText).registros);
		
		Ext.getCmp('Mensaje').setValue(Ext.util.JSON.decode(response.responseText).mensaje);
		Ext.getCmp('Mensaje2').setValue(Ext.util.JSON.decode(response.responseText).mensaje2);
		
		Ext.getCmp('btnCSV').setVisible(true);
		Ext.getCmp('btnPDF').setVisible(true);
		Ext.getCmp('btnPDF').setHandler( function(boton, evento){
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).ArchivoPDF;
				forma.submit();
		}
		);
		Ext.getCmp('btnCSV').setHandler( function(boton, evento){
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).ArchivoCSV;
				forma.submit();
		});
		}else{
			
			grid.el.mask('No se cargaron los valores', 'x-mask');
		}
	}
	if( Ext.util.JSON.decode(response.responseText).success2 == false&& Ext.util.JSON.decode(response.responseText).success2 == false){
		pnl.el.mask('No se cargaron los valores');
	}
}

//--------------------------Stores------------------------
var consulta = new Ext.data.JsonStore({
	
	fields: [
				{name: 'PLAZO'},
				{name: 'TASA'}
				
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false

	
});

//---------------------------Componentes------------------------------

var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						header:'Plazo',
						tooltip: 'Plazo',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 140,
						align: 'center'
						},
						{
						header: 'Tasa',
						tooltip: 'Tasa',
						sortable: true,
						dataIndex: 'TASA',
						width: 130,
						align: 'center',
						renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return value+'%';
							}
						}
					],
				//stripeRows: true,
				loadMask: true,
				height: 200,
				width: 320,
				frame: true
				
	});
var panelJSP= new Ext.Panel({  
    hidden: false,
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px',
	 items: [{
		xtype: 'displayfield',
		name: 'Mensaje',
		id: 'Mensaje',
		anchor:'85%',
	//	fieldLabel: 'Periodicidad del pago de Inter�s',
		value:''
	},grid,{
		xtype: 'displayfield',
		name: 'Mensaje2',
		id: 'Mensaje2',
		anchor:'85%',
	//	fieldLabel: 'Periodicidad del pago de Inter�s',
		value:''
	}],
	buttons:
	[
		 {
		  //Bot�n BUSCAR
		  xtype: 'button',
		  text: 'Generar archivo',
		  name: 'btnCSV',
		  id: 'btnCSV',
		  hidden: true,
		  formBind: true,
		  handler: function(boton, evento) 
				{	
				}
		 },{
			text:'Generar PDF',
			name: 'btnPDF',
			hidden: true,
		   id: 'btnPDF',
			handler: function() {
				
					 }
		 }
	]
    
		
	 });
	 
	 
	 
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 650,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 200,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items:[panelJSP]
			 
		
	});
//-------------Principal---------------------------------------


var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [fp]
  });
  pnl.el.mask('Cargando...');
 nombreCurva = Ext.get('nombreCurva').getValue();
	Ext.Ajax.request({
			url: 'CurvaImerext.data.jsp',
			params: Ext.apply({
				informacion: 'ValoeresIniciales',nombreCurva:nombreCurva}),
			callback: procesarValoeresIniciales
		});

});