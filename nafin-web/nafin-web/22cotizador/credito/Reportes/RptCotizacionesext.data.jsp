<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8" import="
	java.util.*,
	com.netro.exception.*,
	netropology.utilerias.negocio.*,
	netropology.utilerias.negocio.InformacionGeneralBean,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%//@ include file="../../22secsession.jspf"%>
<%@ include file="../../../15cadenas/015secsession.jspf" %>
<% 

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String strGrupoUsuario=strPerfil;
String strUsuario=strLogin;
String informacion	= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String resultado 		= null;

/*** INICIO CONSULTA ***/
if(informacion.equals("Consulta")) { 
	//Indica la condición de búsqueda: 1-créditos cotizados, 2-créditos confirmados
	String strWhere="2";
	
	ReportesBean reporte = new ReportesBean();
	reporte.setWhere(strWhere);
	List reg = new ArrayList();
	HashMap datos;
	int cont=0;	
	
	if (reporte.load("creditosconfirmados", strGrupoUsuario, strUsuario)){
		String admin 		= (strGrupoUsuario.equalsIgnoreCase("admin if")||strGrupoUsuario.equalsIgnoreCase("admin if corto")||strGrupoUsuario.equalsIgnoreCase("consul if"))?"S":"N";
		String credRiesgo = strGrupoUsuario.equalsIgnoreCase("CREDITO")||strGrupoUsuario.equalsIgnoreCase("RIESGO")?"S":"N";
		String riesgo 		= strGrupoUsuario.equalsIgnoreCase("riesgo")?"S":"N";
				
		while (reporte.eof()){
			datos = new HashMap();
			datos.put("REFERENCIA",reporte.getNoReferencia());
			datos.put("FECH_COTIZACION",reporte.getFechaCotizacion().substring(6,8)+"/"+InformacionGeneralBean.equivalenteMes(reporte.getFechaCotizacion().substring(4,6))+"/"+reporte.getFechaCotizacion().substring(0,4));
			datos.put("FECH_DISPOSICION",reporte.getFechaDisposicion().substring(6,8)+"/"+InformacionGeneralBean.equivalenteMes(reporte.getFechaDisposicion().substring(4,6))+"/"+reporte.getFechaDisposicion().substring(0,4));
			datos.put("AREA_RESPONSABLE",reporte.getAreaResponsable());
			datos.put("INTERMEDIARIO",reporte.getIntermediario());
			datos.put("TIPO_INTERMEDIARIO",reporte.getTipoIntermediario());
			datos.put("ACREDITADO",reporte.getAcreditado());
			datos.put("MONTO_OPERACION",InformacionGeneralBean.formatoMontos(Double.parseDouble(reporte.getMontoOperacion())));
			datos.put("PLAZO_CREDITO",reporte.getPlazoCredito());
			datos.put("UNIDAD_TIEMPO",reporte.getUnidadTiempo());
			datos.put("DURACION_CREDITO",reporte.getDuracionCreditoDias());
			datos.put("TASA_CREDITO",InformacionGeneralBean.formatoNumeros(Double.parseDouble(reporte.getTasaCredito())*100, "##.00"));
			datos.put("TASA_CREDITO_CM",InformacionGeneralBean.formatoNumeros(Double.parseDouble(reporte.getTasaCreditoCurvaMensual())*100, "##.00"));
			datos.put("PLAZO_VIGENCIA",reporte.getPlazoVigencia());
			datos.put("NPAGOS_INTE",reporte.getNumeroPagosIntereses());
			datos.put("PER_GRACIA_INTE",reporte.getPeriodosGraciaIntereses());
			datos.put("NUM_PAGOS_CAP",reporte.getNumeroPagosCapital());
			datos.put("PER_GRACIA_CAP",reporte.getPeriodosGraciaCapital());
			datos.put("ESTATUS",reporte.getStatusDesc());
			datos.put("CREDITO_RIESGO",credRiesgo);
			datos.put("RIESGO",riesgo);
			datos.put("ADMIN_IF",admin);
			reg.add(datos);
			cont++;
		}
	}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("total",Integer.toString(cont));
		jsonObj.put("registros", reg);
		resultado = jsonObj.toString();
} /*** FIN DE CONSULTA ***/
%>
<%= resultado%>
