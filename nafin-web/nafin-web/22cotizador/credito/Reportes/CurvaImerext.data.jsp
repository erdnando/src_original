	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,
	netropology.utilerias.negocio.*,
	net.sf.json.JSONObject,
	net.sf.json.JSONArray,
	com.netro.pdf.*,
	java.io.*"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
	
		<%
	
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String informacion         =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";

	if(informacion.equals("ValoeresIniciales")){
		ServletContext scon=pageContext.getServletContext();
		String strLiga = scon.getRealPath(strDirTrabajo);
		File dirDoc = new File(strDirTrabajo+"/");
		GregorianCalendar dDoc = new GregorianCalendar();
		String strDoc;
		String meses[]={"Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"};
		
		  String idCurva=request.getParameter("nombreCurva");
		  System.out.println("idCurva[[[[[ "+idCurva);
		  if (idCurva.equals("null")){
			idCurva="";
			boolean band1=false,band2=false,band3=false;
			int i=0;
			while(i< dirDoc.list().length){
				if((dirDoc.list()[i].endsWith("doc")||dirDoc.list()[i].endsWith("pdf"))&&((dirDoc.listFiles()[i]).getName().equals("comisionTFMXN.pdf")||(dirDoc.listFiles()[i]).getName().equals("comisionTPMXN.pdf")||(dirDoc.listFiles()[i]).getName().equals("comisionTFUSD.pdf"))){
					dDoc.setTime(new java.util.Date(dirDoc.listFiles()[i].lastModified()));
					strDoc=""+dDoc.get(Calendar.DATE)+"/"+meses[dDoc.get(Calendar.MONTH)]+"/"+dDoc.get(Calendar.YEAR)+" "+dDoc.get(Calendar.HOUR_OF_DAY)+":"+dDoc.get(Calendar.MINUTE)+":"+dDoc.get(Calendar.SECOND);
					if((dirDoc.listFiles()[i]).getName().equals("comisionTFMXN.pdf")){
						band1=true;
					}
					if((dirDoc.listFiles()[i]).getName().equals("comisionTPMXN.pdf")){
						band2=true;
					}
					if((dirDoc.listFiles()[i]).getName().equals("comisionTFUSD.pdf")){
						band3=true;
					}
				}
				i++;
			}
		jsonObj.put("comisionTF",new Boolean(band1));
		jsonObj.put("comisionTP",new Boolean(band2));
		jsonObj.put("comisionTUS",new Boolean(band3));
		jsonObj.put("success1",new Boolean(true));
	}else{
	String fecha="";
	CostosTransferenciaBean costosTransferencia=new CostosTransferenciaBean();
  try{
		if (costosTransferencia.load(idCurva, costosTransferencia.ultimaFecha(idCurva),costosTransferencia.ultimaFecha(idCurva),"yyyymmdd")){
				fecha =costosTransferencia.getFechaCurva();
				String mensaje="<center>Curva ("+costosTransferencia.getNombreCurva()+") para el Cálculo de Comisiones por Pago <br> Anticipado de Recursos del<br></center>"
               +"<center>"+(fecha.substring(6,8)+" de "+InformacionGeneralBean.equivalenteMes(Integer.parseInt(fecha.substring(4,6)))+" de "+fecha.substring(0,4))+"<br></center>";
				HashMap	datos;
				List reg=new ArrayList();
				while (costosTransferencia.eof()){
					datos=new HashMap();
						datos.put("PLAZO",costosTransferencia.getPlazo());
						datos.put("TASA",InformacionGeneralBean.formatoNumeros((Double.parseDouble(costosTransferencia.getTasa()))*100, "#0.000000"));
						reg.add(datos);
				}
				String  mensaje2="";
				if ("ValmerCETES".equals(idCurva)){
					mensaje2 = "<center><br>Fuente: Valuación Operativa y de Referencia de Mercado, S.A. de <br>C.V. (VALMER). Proveedor oficial de precios de Nafin.</center>";
					} else {
					mensaje2="<center><br>Fuente: Subdirecci&oacute;n de Estrategias de Tesorer&iacute;a <br>(Datos obtenidos del sistema de informaci&oacute;n Bloomberg)</center>";
					}
					
			CreaArchivo archivo = new CreaArchivo();
			String nombreArchivo = archivo.nombreArchivo()+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
    //	Archivos
			StringBuffer contenidoArchivo = new StringBuffer("");
			String nombreArchivoCSV = null;
			contenidoArchivo.append("Plazo,Tasa\n");
			
			if (costosTransferencia.load(idCurva, costosTransferencia.ultimaFecha(idCurva),costosTransferencia.ultimaFecha(idCurva),"yyyymmdd")){
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
				 pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
														(session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString(),
											 (String)session.getAttribute("sesExterno"),
														(String) session.getAttribute("strNombre"),
														 (String) session.getAttribute("strNombreUsuario"),
											 (String)session.getAttribute("strLogo"),strDirectorioPublicacion);			
		
				pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
				pdfDoc.addText("Curva ("+costosTransferencia.getNombreCurva()+") para el Cálculo de Comisiones por Pago Anticipado de Recursos del","formas",ComunesPDF.LEFT);
				fecha =costosTransferencia.getFechaCurva();
				pdfDoc.addText(fecha.substring(6,8)+" de "+InformacionGeneralBean.equivalenteMes(Integer.parseInt(fecha.substring(4,6)))+" de "+fecha.substring(0,4),"formas",ComunesPDF.LEFT);
		
				pdfDoc.setTable(2,40); 
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
				String plazo;
				String tasa;
				while (costosTransferencia.eof()){
				plazo=costosTransferencia.getPlazo();
				tasa=InformacionGeneralBean.formatoNumeros((Double.parseDouble(costosTransferencia.getTasa()))*100, "#0.000000");
				
				contenidoArchivo.append(plazo+","+tasa+"\n");
				pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
				}
			pdfDoc.addTable();
			if ("ValmerCETES".equals(idCurva)){
				pdfDoc.addText("Fuente: Valuación Operativa y de Referencia de Mercado, S.A. de C.V. (VALMER). Proveedor oficial de precios de Nafin.","formas",ComunesPDF.CENTER);
			}else{
				pdfDoc.addText("Fuente: Subdirección de Estrategias de Tesorería (Datos obtenidos del sistema de información Bloomberg)","formas",ComunesPDF.CENTER);
			}
			pdfDoc.endDocument(); 	
			
			if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
				out.print("<--!Error al generar el archivo-->");
			else
				nombreArchivoCSV = archivo.nombre;
			
			}
			
			
			jsonObj.put("Totales",reg.size()+"");
			jsonObj.put("ArchivoCSV", strDirecVirtualTemp+nombreArchivoCSV);	
			jsonObj.put("ArchivoPDF", strDirecVirtualTemp+nombreArchivo);
			jsonObj.put("mensaje", mensaje);
			jsonObj.put("mensaje2", mensaje2);
			jsonObj.put("registros", reg);
			jsonObj.put("success2",new Boolean(true));
			}
			else{
				jsonObj.put("success2",new Boolean(false));
			}
			//if
	  }catch(Exception e){
		 e.printStackTrace();
		 System.out.println("----------------------");
		 System.out.println("Error: CURVALMER.JSP "+e);
		 System.out.println("----------------------"); 
		 jsonObj.put("success2",new Boolean(false));
			//jsonObj.put("success",new Boolean(false));
		 }finally{ 
		 costosTransferencia.release();
		 }
	
	}
	infoRegresar = jsonObj.toString();
	}



%>

<%=infoRegresar%>