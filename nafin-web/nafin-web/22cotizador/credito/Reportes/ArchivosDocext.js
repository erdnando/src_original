var muestraArchivo;

Ext.onReady(function(){

muestraArchivo = function(icArchivo){
	Ext.Ajax.request({
		params:{
			informacion: 'DescargaArchivo',
			icArchivo: icArchivo
		},
		url: 'ArchivosDocExt01.data.jsp',
		callback: procesarDescargaArchivo
	});
}

var procesarValoeresIniciales = function(opts, success, response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		Ext.getCmp('Mensaje').setValue('<table align="center" border="0">'+Ext.util.JSON.decode(response.responseText).Archivos+'</table>');
	
	}
}

var procesarDescargaArchivo = function(opts, success, response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var infoR = Ext.util.JSON.decode(response.responseText);
		var archivo = infoR.urlArchivo;
		archivo = archivo.replace('/nafin','');
		var params = {nombreArchivo: archivo};
		fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
		fp.getForm().getEl().dom.submit();
	
	}
}


var panelJSP= new Ext.Panel({
    hidden: false,
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px',
	 items: [{
		xtype: 'displayfield',
		name: 'Mensaje',
		id: 'Mensaje',
		anchor:'85%',
	//	fieldLabel: 'Periodicidad del pago de Inter�s',
		value:'-'
	}]
	 });



var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 500,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 200,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: [panelJSP]
		
			 
		
	});



//-----------Principal------------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [ 
		 fp
		 ]
  });
  
  	Ext.Ajax.request({
		params:{
			informacion: 'ConsultaArchivos'
		},
		url: 'ArchivosDocext.data.jsp',
		callback: procesarValoeresIniciales
	});
});