Ext.onReady(function(){

var procesarConsulta =  function(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		Ext.getCmp('btnPDF').setHandler( function(boton, evento){
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).ArchivoPDF;
				forma.submit();
		}
		);
		Ext.getCmp('btnCSV').setHandler( function(boton, evento){
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).ArchivoCSV;
				forma.submit();
		});
		consulta.loadData(Ext.util.JSON.decode(response.responseText).registros);
		fp.setVisible(true);
	}else{
		Ext.getCmp('Error').setValue('<div align="Center"><h1>'+Ext.util.JSON.decode(response.responseText).Error+'<h1></div>');
		fpError.setVisible(true);
	}
}

//-----------------------STORES-----------------------------
var consulta = new Ext.data.JsonStore({
	
	fields: [
				{name: 'PLAZO'},
				{name: 'COL1'},
				{name: 'COL2'},
				{name: 'COL3'},
				{name: 'COL4'},
				{name: 'COL5'},
				{name: 'COL6'},
				{name: 'COL7'},
				{name: 'COL8'},
				{name: 'COL9'},
				{name: 'COL10'}
				
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false

	
});

//-----------------------COMPONENTES------------------------
var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: false,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				tbar: {
				id:'toolBar',
				renderTo:fp,
				items: [
					'->',
					{
						xtype: 'button',
						text: 'Ejemplo de Uso',
						id: 'btnEjemplo',
						hidden: false,
						handler: function(boton, evento) {									
							panelEjemplo.setVisible(true);
							grid.setVisible(false);
							panelInstrucciones.setVisible(false);
							}
						}
					]
				},
				columns:[
				//CAMPOS DEL GRID
						{
						header:'Plazo del <br/>Cr�dito Solicitado',
						tooltip: 'Plazo del <br/>Cr�dito Solicitado',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 140,
						align: 'center'
						},
						{
						header: '-0.5 pp',
						tooltip: '-0.5 pp',
						sortable: true,
						dataIndex: 'COL1',
						width: 130,
						align: 'center'
						},
						{
						header: '-1.0 pp',
						tooltip: '-1.0 pp',
						sortable: true,
						dataIndex: 'COL2',
						width: 130,
						align: 'center'
						},
						{
						header: '-1.5 pp',
						tooltip: '-1.5 pp',
						sortable: true,
						dataIndex: 'COL3',
						width: 130,
						align: 'center'
						},
						{
						header: '-2.0 pp',
						tooltip: '-2.0 pp',
						sortable: true,
						dataIndex: 'COL4',
						width: 130,
						align: 'center'
						},
						{
						align:'center',
						header: '-2.5 pp',
						tooltip: '-2.5 pp',
						sortable: true,
						dataIndex: 'COL5',
						width: 130
						},{
						header:'-3.0 pp',
						tooltip: '-3.0 pp',
						sortable: true,
						dataIndex: 'COL6',
						width: 130,
						align: 'center'
						},
						{
						header: '-3.5 pp',
						tooltip: '-3.5 pp',
						sortable: true,
						dataIndex: 'COL7',
						width: 130,
						align: 'center'
						},
						{
						header: '-4.0 pp',
						tooltip: '-4.0 pp',
						sortable: true,
						dataIndex: 'COL8',
						width: 130,
						align: 'center'
						},
						{
						header: '-4.0 pp',
						tooltip: '-4.0 pp',
						sortable: true,
						dataIndex: 'COL9',
						width: 130,
						align: 'center'
						
						},
						{
						header: '-4.5 pp',
						tooltip: '-4.5 pp',
						sortable: true,
						dataIndex: 'COL10',
						width: 130,
						align: 'center'
						}
				],
				stripeRows: true,
				loadMask: true,
				height: 300,
				width: 940,
				frame: true
				
	});




var panelInstrucciones= new Ext.Panel({
    hidden: false,
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px', 
    html: '<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="3" ALIGN="LEFT">'+
				'<TR>'+
				 '<TD class="textos" align="left" COLSPAN="11">'+
				'<b>Instrucciones: </b><br>'+
				'1. Obtener una cotizaci�n consistente con las caracter�sticas del cr�dito solicitado, en t�rminos de plazo, frecuencias de pagos, etc. <br><br>'+
				'2. Comparar la cotizaci�n obtenida con la tasa tomada "en firme":<br>'+
				'	a) Si (cotizaci�n - tasa en firme) < 0, aplicar comisi�n conforme a la tabla, considerando el plazo del cr�dito y la diferencia en puntos porcentuales entre ambas tasas. <br>'+
				'	b) Si (cotizaci�n - tasa en firme) > = 0, no aplicar� el cobro de la comisi�n. <br>'+
				'<br>'+
				'3. El monto de comisi�n a cobrar ser� el resultado de multiplicar los porcentajes de comisi�n de la tabla por el monto de la operaci�n solicitada. <br>'+
				'<br>'+
				'<b>Nota:</b> Los porcentajes de comisi�n no incluyen IVA</TD>'+
				'</TR>'+
				'</TABLE>',
	buttons:
	[
		 {
		  //Bot�n BUSCAR
		  xtype: 'button',
		  text: 'Generar archivo',
		  name: 'btnCSV',
		  id: 'btnCSV',
		  hidden: false,
		  formBind: true,
		  handler: function(boton, evento) 
				{	
				}
		 },{
			text:'Generar PDF',
			name: 'btnPDF',
		   id: 'btnPDF',
			handler: function() {
				
					 }
		 }
	]
		
	 });
	 
	 var panelEjemplo= new Ext.Panel({  
    hidden: true,
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px', 
    html: '<TABLE WIDTH="70%" align="center">'+
				'<tr><td>&nbsp;</td></tr>'+
				'<tr><td><h3>Ejemplo de Uso</h3></td></tr>'+
				 ' <TR>'+
				'<TD class="textos"><blockquote>Si el cr�dito solicitado es de $100,000.00, y el '+
				'plazo de 60 meses, cuya tasa "en firme" obtenida fue de 10%, y la tasa'+
				'actualizada por el mismo plazo de la operaci�n fue de 9.5%, es decir -0.5 pp, el '+
				'porcentaje de comisi�n aplicable ser�a de 1.2% sobre los montos no dispuestos.<BR><BR>'+
				'Monto comisi�n= $100,000 * 0.012 = $1,200.00</blockquote></TD>'+
				'</TR>'+
				'</TABLE>',
					buttons:
							[
								 {
								  //Bot�n BUSCAR
								  xtype: 'button',
								  text: 'Aceptar',
								  name: 'btnAceptar',
								  id: 'btnAceptar',
								  hidden: false,
								  formBind: true,
								  handler: function(boton, evento) 
										{	
											panelEjemplo.setVisible(false);
											grid.setVisible(true);
											panelInstrucciones.setVisible(true);
										}
								 }
							]
	 });

var fpError = new Ext.form.FormPanel
  ({
		hidden: true,
		height: 'auto',
		width: 940,
		collapsible: false,
		title: 'Lista de Comisiones por No Disposici�n',
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'formaError',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items:[NE.util.getEspaciador(30),{
		xtype: 'displayfield',
		name: 'Error',
		id: 'Error',
		allowBlank: false,
		fieldLabel: '',
		anchor:'85%',
		value:''
	},NE.util.getEspaciador(30)]
		
	});



var fp = new Ext.form.FormPanel
  ({
		hidden: true,
		height: 'auto',
		width: 940,
		collapsible: false,
		title: 'Lista de Comisiones por No Disposici�n',
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items:[NE.util.getEspaciador(30),grid,panelInstrucciones,panelEjemplo]
	});


//------------------Principal-------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [ 
			fp
			,fpError
			
			
		 ]
  });
  Ext.Ajax.request({
					url: 'RptRompimientoFodeoext.data.jsp',
					params: {
						informacion: 'Consulta'},
					callback: procesarConsulta
				});

});