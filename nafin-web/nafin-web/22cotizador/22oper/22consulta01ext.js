Ext.onReady(function(){

//------------------------------ Handlers ------------------------------
	// Vuelve a cargar la p�gina
	function limpiar(){
		this.up('form').getForm().reset();
		var gridConsulta = Ext.ComponentQuery.query('#gridCotizacion')[0];
		gridConsulta.hide();
		gridConsulta = Ext.ComponentQuery.query('#gridCalculadora')[0];
		gridConsulta.hide();

	}

	function salirEstatus(){
		Ext.ComponentQuery.query('#formaPrincipal')[0].show();
		Ext.ComponentQuery.query('#gridCotizacion')[0].hide();
		Ext.ComponentQuery.query('#gridCalculadora')[0].hide();
		Ext.ComponentQuery.query('#formaEstatus')[0].hide();
	}

	// Realiza la consulta para llenar el grid
	function buscar(){

		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var campo_ini = '';
		var campo_fin = '';

		// Valido la forma principal
		if(!this.up('form').getForm().isValid()){
			return;
		}

		// Validar la fecha de solicitud
		campo_ini = forma.query('#fecha_solicitud_ini_id')[0].getValue();
		campo_fin = forma.query('#fecha_solicitud_fin_id')[0].getValue();
		if(campo_ini == null)
			campo_ini = '';
		if(campo_fin == null)
			campo_fin = '';
		if(campo_ini == '' && campo_fin != ''){
			forma.query('#fecha_solicitud_ini_id')[0].markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			return;
		}
		if(campo_fin == '' && campo_ini != ''){
			forma.query('#fecha_solicitud_fin_id')[0].markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			return;
		}

		// Validar la fecha de cambio de estatus
		campo_ini = forma.query('#fecha_estatus_ini_id')[0].getValue();
		campo_fin = forma.query('#fecha_estatus_fin_id')[0].getValue();
		if(campo_ini == null)
			campo_ini = '';
		if(campo_fin == null)
			campo_fin = '';
		if(campo_ini == '' && campo_fin != ''){
			forma.query('#fecha_estatus_ini_id')[0].markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			return;
		}
		if(campo_fin == '' && campo_ini != ''){
			forma.query('#fecha_estatus_fin_id')[0].markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			return;
		}

		// Realiza la consulta del grid
		main.el.mask('Procesando...', 'x-mask-loading');
		Ext.ComponentQuery.query('#gridCotizacion')[0].show();
		Ext.ComponentQuery.query('#gridCalculadora')[0].show();
		consultaData.loadPage(1, {
			params: Ext.apply({
				fecha_solicitud_ini: forma.query('#fecha_solicitud_ini_id')[0].getValue(),
				fecha_solicitud_fin: forma.query('#fecha_solicitud_fin_id')[0].getValue(),
				no_solicitud:        forma.query('#no_solicitud_id')[0].getValue(),
				ic_if:               forma.query('#ic_if_id')[0].getValue(),
				fecha_estatus_ini:   forma.query('#fecha_estatus_ini_id')[0].getValue(),
				fecha_estatus_fin:   forma.query('#fecha_estatus_fin_id')[0].getValue(),
				usuario:             forma.query('#usuario_id')[0].getValue(),
				ic_moneda:           forma.query('#ic_moneda_id')[0].getValue(),
				monto_credito_ini:   forma.query('#monto_credito_ini_id')[0].getValue(),
				monto_credito_fin:   forma.query('#monto_credito_fin_id')[0].getValue(),
				ic_ejecutivo:        forma.query('#ic_ejecutivo_id')[0].getValue(),
				ic_estatus:          forma.query('#ic_estatus_id')[0].getValue(),
				operacion:           'generar',
				start:               0,
				limit:               15
			})
		});
		consultaCalculadora.loadPage(1, {
			params: Ext.apply({
				fecha_solicitud_ini: forma.query('#fecha_solicitud_ini_id')[0].getValue(),
				fecha_solicitud_fin: forma.query('#fecha_solicitud_fin_id')[0].getValue(),
				no_solicitud:        forma.query('#no_solicitud_id')[0].getValue(),
				ic_if:               forma.query('#ic_if_id')[0].getValue(),
				fecha_estatus_ini:   forma.query('#fecha_estatus_ini_id')[0].getValue(),
				fecha_estatus_fin:   forma.query('#fecha_estatus_fin_id')[0].getValue(),
				usuario:             forma.query('#usuario_id')[0].getValue(),
				ic_moneda:           forma.query('#ic_moneda_id')[0].getValue(),
				monto_credito_ini:   forma.query('#monto_credito_ini_id')[0].getValue(),
				monto_credito_fin:   forma.query('#monto_credito_fin_id')[0].getValue(),
				ic_ejecutivo:        forma.query('#ic_ejecutivo_id')[0].getValue(),
				ic_estatus:          forma.query('#ic_estatus_id')[0].getValue(),
				operacion:           'generar',
				start:               0,
				limit:               15
			})
		});

	}
	
	// Genera el reporte en PDF.
	// Nota: Imprime los dos grids en el mismo reporte, para hacerlo m�s sencillo
	// se pasar�n los dats de los store en lugar de volver a hacer las consultas. 
	function generarPdf(){

		main.el.mask('Procesando...', 'x-mask-loading');

		var grid = Ext.ComponentQuery.query('#gridCotizacion')[0];
		var storeCotizacion = grid.getStore();
		var datosCotizacion = '';
		var datosCalculadora = '';
		var datar = new Array();

		var records = storeCotizacion.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		datosCotizacion = Ext.encode(datar);

		grid = Ext.ComponentQuery.query('#gridCalculadora')[0];
		var storeCalculadora = grid.getStore();
		datar = new Array();
		records = storeCalculadora.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		datosCalculadora = Ext.encode(datar);

		Ext.Ajax.request({
			url: '22consulta01ext.data.jsp',
			params: Ext.apply({
				informacion:      'GENERAR_REPORTE_PDF',
				datosCotizacion:  datosCotizacion,
				datosCalculadora: datosCalculadora,
				datosGrid:        ''
			}),
			callback: procesaGeneraArchivo
		});

	}

	// Realiza las validaciones necesarias de ambos grids para actualizar
	// el estatus de lod registros seleccionados
	function cambiaEstatus(){

		// Valido el estatus
		var grid = Ext.ComponentQuery.query('#gridCotizacion')[0];
		var store = grid.getStore();
		var contador = 0;
		var numRegistro = 0;
		var edit = grid.editingPlugin;
		store.each(function(record){
			if(record.data['IC_ESTATUS'] == '7'||record.data['IC_ESTATUS'] == '10'){
				if(record.data['CAMBIA_OPERADO'] == true){
					contador++;
				}
			}
		});
		grid = Ext.ComponentQuery.query('#gridCalculadora')[0];
		store = grid.getStore();
		edit = grid.editingPlugin;
		store.each(function(record){
			numRegistro = store.indexOf(record);
			if(record.data['ID_STATUS'] == '2'){
				if(record.data['CAMBIA_OPERADO'] == true){
					contador++;
				}
			}
		});
		
		if(contador == 0){
			Ext.MessageBox.alert('Mensaje', 'No se ha marcado ning�n registro.');
			return;
		}

		//Env�o los datos al jsp
		main.el.mask('Procesando...', 'x-mask-loading');
		grid = Ext.ComponentQuery.query('#gridCotizacion')[0];
		var storeCotizacion = grid.getStore();
		var datosCotizacion = '';
		var datosCalculadora = '';
		var datar = new Array();

		var records = storeCotizacion.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		datosCotizacion = Ext.encode(datar);

		grid = Ext.ComponentQuery.query('#gridCalculadora')[0];
		var storeCalculadora = grid.getStore();
		datar = new Array();
		records = storeCalculadora.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		datosCalculadora = Ext.encode(datar);

		Ext.Ajax.request({
			url: '22consulta01ext.data.jsp',
			params: Ext.apply({
				informacion:      'CAMBIA_ESTATUS',
				datosCotizacion:  datosCotizacion,
				datosCalculadora: datosCalculadora
			}),
			callback: procesacambiaEstatus
		});

	}

	function generarPdfEstatus(){

		main.el.mask('Procesando...', 'x-mask-loading');

		var grid = Ext.ComponentQuery.query('#gridCotizacionEstatus')[0];
		var storeCotizacion = grid.getStore();
		var datosCotizacion = '';
		var datosCalculadora = '';
		var datosEstatus = '';
		var datar = new Array();

		var records = storeCotizacion.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		datosCotizacion = Ext.encode(datar);

		grid = Ext.ComponentQuery.query('#gridCalculadoraEstatus')[0];
		var storeCalculadora = grid.getStore();
		datar = new Array();
		records = storeCalculadora.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		datosCalculadora = Ext.encode(datar);

		grid = Ext.ComponentQuery.query('#gridDatosEstatus')[0];
		var storeEstatus = grid.getStore();
		datar = new Array();
		records = storeEstatus.getRange();
		for (var i = 0; i < records.length; i++){
			datar.push(records[i].data);
		}
		datosEstatus = Ext.encode(datar);

		Ext.Ajax.request({
			url: '22consulta01ext.data.jsp',
			params: Ext.apply({
				informacion:      'GENERAR_REPORTE_PDF',
				datosCotizacion:  datosCotizacion,
				datosCalculadora: datosCalculadora,
				datosEstatus:     datosEstatus
			}),
			callback: procesaGeneraArchivo
		});

	}

	// Realiza la consulta de solicitud del registro seleccionado en el grid
	function procesarConsulta(estatus, solicitud){
		main.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '22consulta01ext.data.jsp',
			params: Ext.apply({
				informacion:  'CONSULTA_SOLICITUD',
				ic_estatus:   estatus,
				ic_solicitud: solicitud
			}),
			callback: procesarFormaSolicitud
		});
	}

	// Realiza la consulta del formato calculadora del registro seleccionado en el grid
	function procesarConsultaBis(no_referencia){
		main.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '22consulta01ext.data.jsp',
			params: Ext.apply({
				informacion:  'FORMATO_CALCULADORA',
				ic_solicitud: no_referencia
			}),
			callback: procesarFormaSolicitud
		});
	}

	// Genera el PDF del form solicitud
	function generaPDFSolicitud(){

		var forma             = Ext.ComponentQuery.query('#formaSolicitud')[0];
		var tipo_consulta     = forma.query('#tipo_consulta_id')[0].getValue();
		var criterio_busqueda = forma.query('#criterio_busqueda_id')[0].getValue();
		var estatus_solic     = forma.query('#estatus_solic_id')[0].getValue();

		if(tipo_consulta == 'CONSULTA_SOLICITUD'){
			tipo_consulta = 'PDF_SOLICITUD';
		} else if(tipo_consulta == 'FORMATO_CALCULADORA'){
			tipo_consulta = 'PDF_CALCULADORA';
		}

		//main.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '22consulta01ext.data.jsp',
			params: Ext.apply({
				informacion:  tipo_consulta,
				ic_solicitud: criterio_busqueda,
				ic_estatus:   estatus_solic
			}),
			callback: procesaGeneraArchivo
		});
	}

	// Se oculta el form solicitud y se muestra el form principal
	function salir(){
		var forma = Ext.ComponentQuery.query('#formaSolicitud')[0];
		forma.hide();
		forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		forma.show();
		forma = Ext.ComponentQuery.query('#gridCotizacion')[0];
		forma.show();
		forma = Ext.ComponentQuery.query('#gridCalculadora')[0];
		forma.show();
	}

//------------------------------ Callback --------------------------------
	// Proceso posterior a la consulta del grid Consulta Cotizaci�n
	var procesarConsultaData = function(store, arrRegistros, success, opts){
		main.el.unmask();
		var gridConsulta = Ext.ComponentQuery.query('#gridCotizacion')[0];
                gridConsulta.getView().getEl().mask('Procesando...', 'x-mask-loading');

		if (arrRegistros != null){
			gridConsulta.show();
                        gridConsulta.getView().getEl().unmask();
			if(store.getTotalCount() > 0){
                                gridConsulta.query('#btnReporteGeneral')[0].enable();
				gridConsulta.query('#btnCambiarEstatus')[0].enable();

			} else{
				gridConsulta.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
                                gridConsulta.query('#btnReporteGeneral')[0].disable();
				gridConsulta.query('#btnCambiarEstatus')[0].disable();

			}
		}
	}

	// Proceso posterior a la consulta del grid Calculadora
	var procesarConsultaCalculadora = function(store, arrRegistros, success, opts){
		main.el.unmask();
		var gridConsulta = Ext.ComponentQuery.query('#gridCalculadora')[0];
                gridConsulta.getView().getEl().mask('Procesando...', 'x-mask-loading');

		if (arrRegistros != null){
			gridConsulta.show();
                        gridConsulta.getView().getEl().unmask();
			if(store.getTotalCount() > 0){
				gridConsulta.query('#btnReporteGeneral')[0].enable();
				gridConsulta.query('#btnCambiarEstatus')[0].enable();
			} else{
				gridConsulta.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridConsulta.query('#btnReporteGeneral')[0].disable();
				gridConsulta.query('#btnCambiarEstatus')[0].disable();
			}
		}
	}

	// Handler que procesa respuesta creacion archivo
	function procesaGeneraArchivo(opts, success, response){

		// PARSEAR RESPUESTA DEL SERVIDOR
		var resp = null;
		try {
			resp = Ext.JSON.decode(response.responseText);
		} catch(error) {
			NE.util.mostrarErrorResponse4(
				{ status: -1 }, // Para que no asuma que es un est�tus devuelto por el servidor
				error.name + " - " + error.message
			);
			return;
		}

		// REALIZAR LA DESCARGA AUTOM�TICA DEL ARCHIVO
		if ( success == true && resp.success == true ) {

			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo';
			forma.method = 'post';
			forma.target = '_self';

			// Suprimir el Context root del url del archivo
			var urlArchivo = resp.urlArchivo;
			nombreArchivo  = urlArchivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');

			// Insertar Nombre Archivo
			var inputNombreArchivo = Ext.DomHelper.insertFirst(
				forma,
				{
					tag:   'input',
					type:  'hidden',
					id:    'nombreArchivo',
					name:  'nombreArchivo',
					value: nombreArchivo
				},
				true
			);
			// Solicitar Archivo Al Servidor
			forma.submit();
			// Remover nodo agregado
			inputNombreArchivo.remove();

		} else {
				NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
		main.el.unmask();

	}

	// Procesa la respuesta de cambiar el estatus
	var procesacambiaEstatus = function(opts, success, response){
		main.el.unmask();
		if (success == true && Ext.JSON.decode(response.responseText).success == true){
			var datosCot = Ext.JSON.decode(response.responseText);
			if(datosCot.mensaje != '' && datosCot.mensaje == 'OK'){
				datosEstatus.loadData(datosCot.datosEstatus);
				consultaDataEstatus.loadData(datosCot.cotizacionEstatus);
				consultaCalculadoraEstatus.loadData(datosCot.calculadoraEstatus);
				Ext.ComponentQuery.query('#formaPrincipal')[0].hide();
				Ext.ComponentQuery.query('#gridCotizacion')[0].hide();
				Ext.ComponentQuery.query('#gridCalculadora')[0].hide();
				if(datosCot.datosEstatus!= ''){
					Ext.ComponentQuery.query('#gridDatosEstatus')[0].show();
				} else{
					Ext.ComponentQuery.query('#gridDatosEstatus')[0].hide();
				}
				if(datosCot.cotizacionEstatus != ''){
					Ext.ComponentQuery.query('#gridCotizacionEstatus')[0].show();
				} else{
					Ext.ComponentQuery.query('#gridCotizacionEstatus')[0].hide();
				}
				if(datosCot.calculadoraEstatus != ''){
					Ext.ComponentQuery.query('#gridCalculadoraEstatus')[0].show();
				} else{
					Ext.ComponentQuery.query('#gridCalculadoraEstatus')[0].hide();
				}
				Ext.ComponentQuery.query('#formaEstatus')[0].show();
			} else{
				Ext.MessageBox.alert('Mensaje', datosCot.mensaje);
				return;
			}
		}
	}

	// Muestra el form Solicitud
	var procesarFormaSolicitud = function(opts, success, response){
		if (success == true && Ext.JSON.decode(response.responseText).success == true){

			var datosCot = Ext.JSON.decode(response.responseText);
			var forma = Ext.ComponentQuery.query('#formaSolicitud')[0];

			forma.query('#tipo_consulta_id')[0].setValue(datosCot.informacion);
			forma.query('#criterio_busqueda_id')[0].setValue(datosCot.criterio_busqueda);
			forma.query('#estatus_solic_id')[0].setValue(datosCot.estatus);

			datosCotizacion.loadData(datosCot.listaDatosCotizacion);
			if(datosCot.listaEjecutivoNafin != ''){
				Ext.ComponentQuery.query('#gridEjecutivoNafin')[0].show();
				datosEjecutivoNafin.loadData(datosCot.listaEjecutivoNafin);
			} else{
				Ext.ComponentQuery.query('#gridEjecutivoNafin')[0].hide();
			}
			datosIntermediario.loadData(datosCot.listaIntermediario);
			datosOperacion.loadData(datosCot.listaOperacion);
			if(datosCot.listaDetalleDisp != ''){
				Ext.ComponentQuery.query('#gridDetalleDisposiciones')[0].show();
				datosDetalleDisp.loadData(datosCot.listaDetalleDisp);
			} else{
				Ext.ComponentQuery.query('#gridDetalleDisposiciones')[0].hide();
			}
			if(datosCot.listaPlanPagos != ''){
				Ext.ComponentQuery.query('#gridPlanPagos')[0].show();
				datosPlanPagos.loadData(datosCot.listaPlanPagos);
			} else{
				Ext.ComponentQuery.query('#gridPlanPagos')[0].hide();
			}
			if(datosCot.listaTasaIndicativa != ''){
				Ext.ComponentQuery.query('#gridTasaIndicativa')[0].show();
				datosTasaIndicativa.loadData(datosCot.listaTasaIndicativa);
			} else{
				Ext.ComponentQuery.query('#gridTasaIndicativa')[0].hide();
			}
			if(datosCot.listaTasaConfirmada != ''){
				Ext.ComponentQuery.query('#gridTasaConfirmada')[0].show();
				datosTasaConfirmada.loadData(datosCot.listaTasaConfirmada);
			} else{
				Ext.ComponentQuery.query('#gridTasaConfirmada')[0].hide();
			}

			forma.query('#mensaje1_id')[0].setValue(datosCot.mensaje1);
			if(datosCot.mensaje2 != ''){
				forma.query('#mensaje2_id')[0].show();
				forma.query('#mensaje2_id')[0].setValue(datosCot.mensaje2);
			} else{
				forma.query('#mensaje2_id')[0].hide();
			}
			if(datosCot.mensaje3 != ''){
				forma.query('#mensaje3_id')[0].show();
				forma.query('#mensaje3_id')[0].setValue(datosCot.mensaje3);
			} else{
				forma.query('#mensaje3_id')[0].hide();
			}
			if(datosCot.mensaje4 != ''){
				forma.query('#mensaje4_id')[0].show();
				forma.query('#mensaje4_id')[0].setValue(datosCot.mensaje4);
			} else{
				forma.query('#mensaje4_id')[0].hide();
			}
			forma.show();

			// Se oculta el form principal
			forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
			forma.hide();
			// Se oculta el grid Cotizaci�n
			forma = Ext.ComponentQuery.query('#gridCotizacion')[0];
			forma.hide();
			// Se oculta el grid Calculadora
			forma = Ext.ComponentQuery.query('#gridCalculadora')[0];
			forma.hide();
		}
		main.el.unmask();
	}

//------------------------------ Stores ------------------------------
	// Se crea el MODEL para los grids 'Consulta Cotizaci�n'
	Ext.define('ListaConsultas',{
		extend: 'Ext.data.Model',
		fields: [{name: 'ETIQUETA'}, {name: 'DESCRIPCION'}]
	});

	// Se crea el MODEL para el grid 'Consulta Cotizaci�n'
	Ext.define('ListaData',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'IC_SOLIC_ESP'          },
			{name: 'FECHA_SOLICITUD'       },
			{name: 'FECHA_HORA_SOLICITUD'  },
			{name: 'NUMERO_SOLICITUD'      },
			{name: 'INTERMEDIARIO'         },
			{name: 'MONEDA_SOLICITUD'      },
			{name: 'EJECUTIVO'             },
			{name: 'MONTO'                 },
			{name: 'PLAZO'                 },
			{name: 'TASA'                  },
			{name: 'ESQUEMA_RECUP'         },
			{name: 'CG_OBSERVACIONES'      },
			{name: 'IC_PLAN_DE_PAGOS'      },
			{name: 'IC_ESTATUS'            },
			{name: 'RS_ESTATUS'            },
			{name: 'CG_OBSERVACIONES_SIRAC'},
			{name: 'SOLICITUD_PORTAL'      },
			{name: 'FECHACAMBIOESTATUS'    },
			{name: 'USUARIOCAMBIOESTATUS'  },
			{name: 'DESCTASAIND'           },
			{name: 'CAMBIA_OPERADO', type: 'boolean'}
		]
	});

	// Se crea el MODEL para el grid 'Consulta Calculadora'
	Ext.define('ListaCalculadora',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'FECHA_COTIZACION' },
			{name: 'FECHA_TF'         },
			{name: 'NO_REFERENCIA'    },
			{name: 'NOMBRE_IF'        },
			{name: 'NOMBRE_MONEDA'    },
			{name: 'EJECUTIVO'        },
			{name: 'MONTO'            },
			{name: 'PLAZO'            },
			{name: 'ESQUEMA'          },
			{name: 'TASA'             },
			{name: 'ESTATUS'          },
			{name: 'OBSERVACIONES'    },
			{name: 'ID_STATUS'        },
			{name: 'CAMBIA_OPERADO', type: 'boolean'}
		]
	});

	// Se crea el MODEL para los combos
	Ext.define('ModelCatologo',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);

	// Se crea el STORE del grid 'Consulta Cotizaci�n'
	var consultaData = Ext.create('Ext.data.Store',{
		storeId: 'consultaData',
		model: 'ListaData',
		pageSize: 15,
		proxy: {
			type: 'ajax',
			url:  '22consulta01ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CONSULTA_DATA'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultaData
		}
	});

	// Se crea el STORE del grid 'Consulta Calculadora'
	var consultaCalculadora = Ext.create('Ext.data.Store',{
		storeId: 'consultaCalculadora',
		model: 'ListaCalculadora',
		pageSize: 15,
		proxy: {
			type: 'ajax',
			url:  '22consulta01ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CONSULTA_CALCULADORA'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultaCalculadora
		}
	});

	var consultaDataEstatus = Ext.create('Ext.data.ArrayStore',{
		storeId:  'consultaDataEstatus',
		fields: [
			{name: 'IC_SOLIC_ESP',           mapping:0  },
			{name: 'FECHA_SOLICITUD',        mapping:1  },
			{name: 'FECHA_HORA_SOLICITUD',   mapping:2  },
			{name: 'NUMERO_SOLICITUD',       mapping:3  },
			{name: 'INTERMEDIARIO',          mapping:4  },
			{name: 'MONEDA_SOLICITUD',       mapping:5  },
			{name: 'EJECUTIVO',              mapping:6  },
			{name: 'MONTO',                  mapping:7  },
			{name: 'PLAZO',                  mapping:8  },
			{name: 'TASA',                   mapping:9  },
			{name: 'ESQUEMA_RECUP',          mapping:10 },
			{name: 'CG_OBSERVACIONES',       mapping:11 },
			{name: 'IC_PLAN_DE_PAGOS',       mapping:12 },
			{name: 'IC_ESTATUS',             mapping:13 },
			{name: 'RS_ESTATUS',             mapping:14 },
			{name: 'CG_OBSERVACIONES_SIRAC', mapping:15 },
			{name: 'SOLICITUD_PORTAL',       mapping:16 },
			{name: 'FECHACAMBIOESTATUS',     mapping:17 },
			{name: 'USUARIOCAMBIOESTATUS',   mapping:18 },
			{name: 'DESCTASAIND',            mapping:19 }
		],
		autoLoad: false
	});

	var consultaCalculadoraEstatus = Ext.create('Ext.data.ArrayStore',{
		storeId:  'consultaCalculadoraEstatus',
		fields: [
			{name: 'FECHA_COTIZACION', mapping:0 },
			{name: 'FECHA_TF',         mapping:1 },
			{name: 'NO_REFERENCIA',    mapping:2 },
			{name: 'NOMBRE_IF',        mapping:3 },
			{name: 'NOMBRE_MONEDA',    mapping:4 },
			{name: 'EJECUTIVO',        mapping:5 },
			{name: 'MONTO',            mapping:6 },
			{name: 'PLAZO',            mapping:7 },
			{name: 'ESQUEMA',          mapping:8 },
			{name: 'TASA',             mapping:9 },
			{name: 'ESTATUS',          mapping:10},
			{name: 'OBSERVACIONES',    mapping:11},
			{name: 'ID_STATUS',        mapping:12}
		],
		autoLoad: false
	});

	// Se crea el STORE del grid 'Datos Estatus'
	var datosEstatus = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosEstatus',
		fields:   [{name:'ETIQUETA', mapping:0}, {name:'DESCRIPCION', mapping:1}],
		autoLoad: false
	});

	// Se crea el STORE del grid 'Datos de Cotizacion'
	var datosCotizacion = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosCotizacion',
		fields:   [{name:'ETIQUETA', mapping:0}, {name:'DESCRIPCION', mapping:1}],
		autoLoad: false
	});

	// Se crea el STORE del grid 'Datos del Ejecutivo'
	var datosEjecutivoNafin = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosEjecutivoNafin',
		fields:   [{name:'ETIQUETA', mapping:0}, {name:'DESCRIPCION', mapping:1}],
		autoLoad: false
	});

	// Se crea el STORE del grid 'Datos del Intermediario'
	var datosIntermediario = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosIntermediario',
		fields:   [{name:'ETIQUETA', mapping:0}, {name:'DESCRIPCION', mapping:1}],
		autoLoad: false
	});

	// Se crea el STORE del grid 'Caracter�sticas de la Operaci�n'
	var datosOperacion = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosOperacion',
		fields:   [{name:'ETIQUETA', mapping:0}, {name:'DESCRIPCION', mapping:1}],
		autoLoad: false
	});

	// Se crea el STORE del grid 'Detalle de Disposiciones'
	var datosDetalleDisp = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosDetalleDisp',
		fields:   [{name:'NUMERO_DISPOSICION', mapping:0}, {name:'MONTO', mapping:1},{name:'FECHA_DISPOSICION', mapping:2}, {name:'FECHA_VENCIMIENTO', mapping:3}],
		autoLoad: false
	});

	// Se crea el STORE del grid 'Plan de Pagos Capital'
	var datosPlanPagos = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosPlanPagos',
		fields:   [{name:'NUMERO_PAGO', mapping:0}, {name:'FECHA_PAGO', mapping:1},{name:'MONTO_PAGAR', mapping:2}, {name:'VACIO', mapping:3}],
		autoLoad: false
	});

	// Se crea el STORE del grid 'Tasa Indicativa'
	var datosTasaIndicativa = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosTasaIndicativa',
		fields:   [{name:'ETIQUETA', mapping:0}, {name:'DESCRIPCION', mapping:1}],
		autoLoad: false
	});

	// Se crea el STORE del grid 'Tasa Indicativa'
	var datosTasaConfirmada = Ext.create('Ext.data.ArrayStore',{
		storeId:  'datosTasaConfirmada',
		fields:   [{name:'ETIQUETA', mapping:0}, {name:'DESCRIPCION', mapping:1}],
		autoLoad: false
	});

	// Se crea el STORE para el cat�logo 'Intermediario'
	var catalogoIntermediario = Ext.create('Ext.data.Store',{
		model: 'ModelCatologo',
		proxy: {
			type: 'ajax',
			url: '22consulta01ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_INTERMEDIARIO'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: true
	});

	// Se crea el STORE para el cat�logo 'Moneda'
	var catalogoMoneda = Ext.create('Ext.data.Store',{
		model: 'ModelCatologo',
		proxy: {
			type: 'ajax',
			url: '22consulta01ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_MONEDA'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: true
	});

	// Se crea el STORE para el cat�logo 'Persona Responsable (Ejecutivo)'
	var catalogoEjecutivo = Ext.create('Ext.data.Store',{
		model: 'ModelCatologo',
		proxy: {
			type: 'ajax',
			url: '22consulta01ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_EJECUTIVO'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: true
	});

	// Se crea el STORE para el cat�logo 'Estatus'
	var catalogoEstatus = Ext.create('Ext.data.Store',{
		model: 'ModelCatologo',
		proxy: {
			type: 'ajax',
			url: '22consulta01ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_ESTATUS'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: true
	});

//------------------------------ Componentes ------------------------------
	// Se crea el grid 'Cotizaci�n espec�fica'
	var gridCotizacion = Ext.create('Ext.grid.Panel',{
		height:           445,
		width:            '95%',
		xtype:            'cell-editing',
		itemId:           'gridCotizacion',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            'Cotizaci�n espec�fica',
		selType:          'cellmodel',
		store:            Ext.data.StoreManager.lookup('consultaData'),
		plugins:          [Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		border:           true,
		hidden:           true,
		frame:            true,
		listeners: {
			cellclick: function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
				var fieldName = view.getGridColumns()[cellIndex].dataIndex;
				if( record.get('IC_ESTATUS') != '7'){ 
					if( fieldName=='CG_OBSERVACIONES_SIRAC'){
						return false;
					} else{
						return true;
					}
				}
			}
		},
		columns: [{
			width:         120,
			dataIndex:     'FECHA_SOLICITUD',
			header:        'Fecha de Solicitud',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         170,
			dataIndex:     'FECHA_HORA_SOLICITUD',
			header:        'Fecha y hora de Toma en Firme',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         110,
			dataIndex:     'NUMERO_SOLICITUD',
			header:        'N�mero de Solicitud',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         260,
			dataIndex:     'INTERMEDIARIO',
			header:        'Intermediario financiero',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         140,
			dataIndex:     'MONEDA_SOLICITUD',
			header:        'Moneda',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         210,
			dataIndex:     'EJECUTIVO',
			header:        'Ejecutivo',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         160,
			dataIndex:     'MONTO',
			header:        'Monto Total Requerido',
			align:         'right',
			sortable:      true,
			resizable:     true
		},{
			width:         150,
			dataIndex:     'PLAZO',
			header:        'Plazo de la Operaci�n',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         140,
			dataIndex:     'ESQUEMA_RECUP',
			header:        'Esquema de Recuperaci�n',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         120,
			dataIndex:     'DESCTASAIND',
			header:        'Tasa Confirmada',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         150,
			dataIndex:     'RS_ESTATUS',
			header:        'Estatus',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         90,
			xtype:         'actioncolumn',
			header:        'Consultar',
			align:         'center',
			sortable:      false,
			resizable:     false,
			items: [{
				iconCls:    'icoBuscar',
				tooltip:    'Ver formato',
				handler:    function(grid, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					var solicitud = rec.get('IC_SOLIC_ESP');
					var estatus = rec.get('IC_ESTATUS')
					procesarConsulta(estatus, solicitud);
				}
			}]
		},{
			width:         90,
			xtype:         'actioncolumn',
			header:        'Plan de Pagos',
			align:         'center',
			sortable:      false,
			resizable:     false,
			items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
					if (record.get('IC_PLAN_DE_PAGOS') != ''){
						return 'icoBuscar';
					}
				},
				getTip: function(value,metadata,record,rowIndex,colIndex,store){
					if (record.get('IC_PLAN_DE_PAGOS') != ''){
						return 'Ver Tabla';
					}
				},
				handler: function(grid, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					if (rec.get('IC_PLAN_DE_PAGOS') != ''){
						main.el.mask('Procesando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '22consulta01ext.data.jsp',
							params: Ext.apply({
								informacion:   'GENERAR_PAGOS',
								ic_plan_pagos: rec.get('IC_PLAN_DE_PAGOS')
							}),
							callback: procesaGeneraArchivo
						});
					}
				}
			}]
		},{
			width:         240,
			dataIndex:     'CG_OBSERVACIONES',
			text:          'Observaciones',
			align:         'left',
			sortable:      false,
			resizable:     false
		},{
			width:         240,
			dataIndex:     'CG_OBSERVACIONES_SIRAC',
			header:        'Observaciones de Operaciones',
			align:         'left',
			tdCls:         'wrap',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			editor: {
				xtype:      'textfield'
			}
		},{
			width:         170,
			dataIndex:     'FECHACAMBIOESTATUS',
			header:        'Fecha de cambio de estatus',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         160,
			dataIndex:     'USUARIOCAMBIOESTATUS',
			header:        'Usuario',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         130,
			xtype:         'checkcolumn',
			dataIndex:     'CAMBIA_OPERADO',
			header:        'Cambiar a Operado',
			align:         'center',
			sortable:      false,
			resizable:     false,
			renderer:      function(value, metaData, record, rowIndex, colIndex, store, view) {
				var cssPrefix = Ext.baseCSSPrefix,
				cls = cssPrefix + 'grid-checkcolumn';
				if(record.data['CAMBIA_OPERADO'] ==false){
					if(record.data['IC_ESTATUS'] == '7'||record.data['IC_ESTATUS'] == '10'){
						cls = cssPrefix + 'grid-checkcolumn';
					} else{
						metaData.tdCls += ' ' + this.disabledCls;
					}
				} else if(record.data['CAMBIA_OPERADO'] == true){
					if(record.data['IC_ESTATUS'] == '7'||record.data['IC_ESTATUS'] == '10'){
						cls += ' ' + cssPrefix + 'grid-checkcolumn-checked';
					} else{
						metaData.tdCls += ' ' + this.disabledCls;
					}
				}
				return '<img class="' + cls + '" src="' + Ext.BLANK_IMAGE_URL + '"/>';
			}
		}],
		bbar: [
			Ext.create('Ext.PagingToolbar',{
				store:      Ext.data.StoreManager.lookup('consultaData'),
				itemId:     'barraPaginacion',
				displayMsg: '{0} - {1} de {2}',
				emptyMsg:   'No se encontraron registros',
				displayInfo:true,
				border:     false
			}),
			'->','-',{
				xtype:      'button',
				itemId:     'btnReporteGeneral',
				iconCls:    'icoPdf', 
				text:       'Generar Archivo PDF',
				tooltip:    'Generar Archivo en formato PDF',
				handler:    generarPdf
			},'-',{
				xtype:      'button',
				itemId:     'btnCambiarEstatus',
				iconCls:    'icoAceptar', 
				text:       'Cambiar Estatus',
				tooltip:    'Cambiar Estatus',
				handler:    cambiaEstatus
			}
		]
	});

	// Se crea el grid 'Calculadora'
	var gridCalculadora = Ext.create('Ext.grid.Panel',{
		height:           445,
		width:            '95%',
		xtype:            'cell-editing',
		selType:          'cellmodel',
		itemId:           'gridCalculadora',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            'Calculadora',
		border:           true,
		stripeRows:       true,
		autoScroll:       false,
		hidden:           true,
		frame:            true,
		store:            Ext.data.StoreManager.lookup('consultaCalculadora'),
		plugins: [
			Ext.create('Ext.grid.plugin.CellEditing', {
				clicksToEdit: 1
			})
		],
		listeners: {
			cellclick: function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
				var fieldName = view.getGridColumns()[cellIndex].dataIndex;
				if( record.get('ID_STATUS') != '2'){ 
					if( fieldName=='OBSERVACIONES'){
						return false;
					} else{
						return true;
					}
				}
			}
		},
		columns: [{
			width:         120,
			dataIndex:     'FECHA_COTIZACION',
			header:        'Fecha de Cotizaci�n',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         170,
			dataIndex:     'FECHA_TF',
			header:        'Fecha de Toma en Firme',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         130,
			dataIndex:     'NO_REFERENCIA',
			header:        'N�mero de Referencia',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         250,
			dataIndex:     'NOMBRE_IF',
			header:        'Intermediario Financiero',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         140,
			dataIndex:     'NOMBRE_MONEDA',
			header:        'Moneda',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         210,
			dataIndex:     'EJECUTIVO',
			header:        'Ejecutivo',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         160,
			dataIndex:     'MONTO',
			header:        'Monto Total Requerido',
			align:         'right',
			sortable:      true,
			resizable:     true
		},{
			width:         150,
			dataIndex:     'PLAZO',
			header:        'Plazo de la Operaci�n',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         150,
			dataIndex:     'ESQUEMA',
			header:        'Esquema de Recuperaci�n',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         140,
			dataIndex:     'TASA',
			header:        'Tasa Confirmada',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         120,
			dataIndex:     'ESTATUS',
			header:        'Estatus',
			align:         'left',
			sortable:      false,
			resizable:     false
		},{
			width:         100,
			xtype:         'actioncolumn',
			header:        'Consultar',
			align:         'center',
			sortable:      false,
			resizable:     false,
			items: [{
				iconCls:    'icoBuscar',
				tooltip:    'Ver formato',
				handler:    function(grid, rowIndex, colIndex){
					var rec = consultaCalculadora.getAt(rowIndex);
					var no_referencia = rec.get('NO_REFERENCIA');
					procesarConsultaBis(no_referencia);
				}
			}]
		},{
			width:         240,
			dataIndex:     'OBSERVACIONES',
			header:        'Observaciones de Operaciones',
			align:         'left',
			tdCls:         'wrap',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			editor: {
				xtype:      'textfield'
			}
		},{
			width:         130,
			xtype:         'checkcolumn',
			dataIndex:     'CAMBIA_OPERADO',
			header:        'Cambiar a Operado',
			align:         'center',
			sortable:      false,
			resizable:     false,
			renderer:      function(value, metaData, record, rowIndex, colIndex, store, view) {
				var cssPrefix = Ext.baseCSSPrefix,
				cls = cssPrefix + 'grid-checkcolumn';
				if(record.data['CAMBIA_OPERADO'] ==false){
					if(record.data['ID_STATUS'] == '2'){
						cls = cssPrefix + 'grid-checkcolumn';
					} else{
						metaData.tdCls += ' ' + this.disabledCls;
					}
				} else if(record.data['CAMBIA_OPERADO'] == true){
					if(record.data['ID_STATUS'] == '2'){
						cls += ' ' + cssPrefix + 'grid-checkcolumn-checked';
					} else{
						metaData.tdCls += ' ' + this.disabledCls;
					}
				}
				return '<img class="' + cls + '" src="' + Ext.BLANK_IMAGE_URL + '"/>';
			}
		}],
		bbar: [
			Ext.create('Ext.PagingToolbar',{
				store:      Ext.data.StoreManager.lookup('consultaCalculadora'),
				itemId:     'barraPaginacion',
				displayMsg: '{0} - {1} de {2}',
				emptyMsg:   'No se encontraron registros',
				displayInfo:true,
				border:     false
			}),
			'->','-',{
				xtype:      'button',
				itemId:     'btnReporteGeneral',
				iconCls:    'icoPdf', 
				text:       'Generar Archivo PDF',
				tooltip:    'Generar Archivo en formato PDF',
				handler:    generarPdf
			},'-',{
				xtype:      'button',
				itemId:     'btnCambiarEstatus',
				iconCls:    'icoAceptar', 
				text:       'Cambiar Estatus',
				tooltip:    'Cambiar Estatus',
				handler:    cambiaEstatus
			}
		]
	});

	// Se crea el grid 'Cotizaci�n Estatus'
	var gridCotizacionEstatus = Ext.create('Ext.grid.Panel',{
		height:           200,
		width:            '100%',
		itemId:           'gridCotizacionEstatus',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            'Cotizaci�n espec�fica',
		selType:          'cellmodel',
		store:            Ext.data.StoreManager.lookup('consultaDataEstatus'),
		scroll:           true,
		border:           true,
		hidden:           true,
		frame:            false,
		columns: [{
			width:         120,
			dataIndex:     'FECHA_SOLICITUD',
			header:        'Fecha de Solicitud',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         170,
			dataIndex:     'FECHA_HORA_SOLICITUD',
			header:        'Fecha y hora de Toma en Firme',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         110,
			dataIndex:     'NUMERO_SOLICITUD',
			header:        'N�mero de Solicitud',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         260,
			dataIndex:     'INTERMEDIARIO',
			header:        'Intermediario financiero',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         140,
			dataIndex:     'MONEDA_SOLICITUD',
			header:        'Moneda',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         210,
			dataIndex:     'EJECUTIVO',
			header:        'Ejecutivo',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         160,
			dataIndex:     'MONTO',
			header:        'Monto Total Requerido',
			align:         'right',
			sortable:      true,
			resizable:     true
		},{
			width:         150,
			dataIndex:     'PLAZO',
			header:        'Plazo de la Operaci�n',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         140,
			dataIndex:     'ESQUEMA_RECUP',
			header:        'Esquema de Recuperaci�n',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         120,
			dataIndex:     'DESCTASAIND',
			header:        'Tasa Confirmada',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         150,
			dataIndex:     'RS_ESTATUS',
			header:        'Estatus',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         180,
			dataIndex:     'CG_OBSERVACIONES',
			text:          'Observaciones',
			align:         'left',
			sortable:      false,
			resizable:     false
		},{
			width:         180,
			dataIndex:     'CG_OBSERVACIONES_SIRAC',
			header:        'Observaciones de Operaciones',
			align:         'left',
			sortable:      false,
			resizable:     false
		},{
			width:         170,
			dataIndex:     'FECHACAMBIOESTATUS',
			header:        'Fecha de cambio de estatus',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         160,
			dataIndex:     'USUARIOCAMBIOESTATUS',
			header:        'Usuario',
			align:         'center',
			sortable:      true,
			resizable:     true
		}]
	});

	// Se crea el grid 'Calculadora Estatus'
	var gridCalculadoraEstatus = Ext.create('Ext.grid.Panel',{
		height:           200,
		width:            '100%',
		itemId:           'gridCalculadoraEstatus',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            'Calculadora',
		border:           true,
		stripeRows:       true,
		autoScroll:       true,
		hidden:           true,
		frame:            false,
		store:            Ext.data.StoreManager.lookup('consultaCalculadoraEstatus'),
		columns: [{
			width:         120,
			dataIndex:     'FECHA_COTIZACION',
			header:        'Fecha de Cotizaci�n',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         170,
			dataIndex:     'FECHA_TF',
			header:        'Fecha de Toma en Firme',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         130,
			dataIndex:     'NO_REFERENCIA',
			header:        'N�mero de Referencia',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         250,
			dataIndex:     'NOMBRE_IF',
			header:        'Intermediario Financiero',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         140,
			dataIndex:     'NOMBRE_MONEDA',
			header:        'Moneda',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         210,
			dataIndex:     'EJECUTIVO',
			header:        'Ejecutivo',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         160,
			dataIndex:     'MONTO',
			header:        'Monto Total Requerido',
			align:         'right',
			sortable:      true,
			resizable:     true
		},{
			width:         150,
			dataIndex:     'PLAZO',
			header:        'Plazo de la Operaci�n',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         150,
			dataIndex:     'ESQUEMA',
			header:        'Esquema de Recuperaci�n',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         140,
			dataIndex:     'TASA',
			header:        'Tasa Confirmada',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         120,
			dataIndex:     'ESTATUS',
			header:        'Estatus',
			align:         'left',
			sortable:      false,
			resizable:     false
		},{
			width:         240,
			dataIndex:     'OBSERVACIONES',
			header:        'Observaciones de Operaciones',
			align:         'left',
			tdCls:         'wrap',
			sortable:      false,
			resizable:     false
		}]
	});

	// Se crea el grid 'Datos Estatus'
	var gridDatosEstatus = Ext.create('Ext.grid.Panel',{
		store:       Ext.data.StoreManager.lookup('datosEstatus'),
		width:       500,
		itemId:      'gridDatosEstatus',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		title:       'La operaci�n se llev� a cabo con �xito',
		autoHeight:  true,
		hideHeaders: true,
		columnLines: true,
		columns: [
			{width:165, dataIndex:'ETIQUETA',    align:'right', sortable:false, resizable:false},
			{width:318, dataIndex:'DESCRIPCION', align:'left',  sortable:false, resizable:false}
		]
	});

	// Se crea el grid 'Datos de la Cotizaci�n'
	var gridDatosCotizacion = Ext.create('Ext.grid.Panel',{
		store:       Ext.data.StoreManager.lookup('datosCotizacion'),
		width:       500,
		itemId:      'gridDatosCotizacion',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		title:       'Datos de la Cotizaci�n',
		autoHeight:  true,
		hideHeaders: true,
		columnLines: true,
		columns: [
			{width:165, dataIndex:'ETIQUETA',    align:'right', sortable:false, resizable:false},
			{width:318, dataIndex:'DESCRIPCION', align:'left',  sortable:false, resizable:false}
		]
	});

	// Se crea el grid 'Datos del Ejecutivo Nafin'
	var gridEjecutivoNafin = Ext.create('Ext.grid.Panel',{
		store:       Ext.data.StoreManager.lookup('datosEjecutivoNafin'),
		width:       620,
		itemId:      'gridEjecutivoNafin',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		title:       'Datos del Ejecutivo Nafin',
		autoHeight:  true,
		hideHeaders: true,
		columnLines: true,
		columns: [
			{width:210, dataIndex:'ETIQUETA',    align:'right', sortable:false, resizable:false},
			{width:395, dataIndex:'DESCRIPCION', align:'left',  sortable:false, resizable:false}
		]
	});

	// Se crea el grid 'Datos del Intermediario Financiero'
	var gridIntermediario = Ext.create('Ext.grid.Panel',{
		store:       Ext.data.StoreManager.lookup('datosIntermediario'),
		width:       620,
		itemId:      'gridIntermediario',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		title:       'Datos del Intermediario Financiero / Acreditado',
		autoHeight:  true,
		hideHeaders: true,
		columnLines: true,
		columns: [
			{width:210, dataIndex:'ETIQUETA',    align:'right', sortable:false, resizable:false},
			{width:395, dataIndex:'DESCRIPCION', align:'left',  sortable:false, resizable:false}
		]
	});

	// Se crea el grid 'Caracter�sticas de la Operaci�n'
	var gridOperacion = Ext.create('Ext.grid.Panel',{
		store:       Ext.data.StoreManager.lookup('datosOperacion'),
		width:       620,
		itemId:      'gridOperacion',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		title:       'Caracter�sticas de la Operaci�n',
		autoHeight:  true,
		hideHeaders: true,
		columnLines: true,
		columns: [
			{width:210, dataIndex:'ETIQUETA',    align:'right', sortable:false, resizable:false},
			{width:395, dataIndex:'DESCRIPCION', align:'left',  sortable:false, resizable:false}
		]
	});

	// Se crea el grid 'Detalle de Disposiciones'
	var gridDetalleDisposiciones = Ext.create('Ext.grid.Panel',{
		store:       Ext.data.StoreManager.lookup('datosDetalleDisp'),
		width:       620,
		itemId:      'gridDetalleDisposiciones',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		title:       'Detalle de Disposiciones',
		autoHeight:  true,
		columns: [
			{width:149, header:'N�mero de disposici�n', dataIndex:'NUMERO_DISPOSICION', align:'center', sortable:false, resizable:false},
			{width:152, header:'Monto',                 dataIndex:'MONTO',              align:'right',  sortable:false, resizable:false},
			{width:152, header:'Fecha de disposici�n',  dataIndex:'FECHA_DISPOSICION',  align:'center', sortable:false, resizable:false},
			{width:152, header:'Fecha de vencimiento',  dataIndex:'FECHA_VENCIMIENTO',  align:'center', sortable:false, resizable:false}
		]
	});

	// Se crea el grid 'Plan de Pagos Capital'
	var gridPlanPagos = Ext.create('Ext.grid.Panel',{
		store:       Ext.data.StoreManager.lookup('datosPlanPagos'),
		width:       620,
		itemId:      'gridPlanPagos',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		title:       'Plan de Pagos Capital',
		autoHeight:  true,
		columns: [
			{width:149, header:'N�mero de pago', dataIndex:'NUMERO_PAGO', align:'center', sortable:false, resizable:false},
			{width:152, header:'Fecha de pago',  dataIndex:'FECHA_PAGO',  align:'center', sortable:false, resizable:false},
			{width:152, header:'Monto a pagar',  dataIndex:'MONTO_PAGAR', align:'right',  sortable:false, resizable:false},
			{width:152, header:'&nbsp',          dataIndex:'VACIO',       align:'center', sortable:false, resizable:false}
		]
	});

	// Se crea el grid 'Tasa Indicativa'
	var gridTasaIndicativa = Ext.create('Ext.grid.Panel',{
		store:       Ext.data.StoreManager.lookup('datosTasaIndicativa'),
		width:       620,
		itemId:      'gridTasaIndicativa',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		title:       'Tasa Indicativa',
		autoHeight:  true,
		hideHeaders: true,
		columnLines: true,
		columns: [
			{width:210, dataIndex:'ETIQUETA',    align:'right', sortable:false, resizable:false},
			{width:395, dataIndex:'DESCRIPCION', align:'left',  sortable:false, resizable:false}
		]
	});

	// Se crea el grid 'Tasa Confirmada'
	var gridTasaConfirmada = Ext.create('Ext.grid.Panel',{
		store:       Ext.data.StoreManager.lookup('datosTasaConfirmada'),
		width:       620,
		itemId:      'gridTasaConfirmada',
		style:       'margin:0 auto;',
		bodyStyle:   'padding: 6px',
		autoHeight:  true,
		hideHeaders: true,
		hideTitle:   true,
		columnLines: true,
		columns: [
			{width:302, dataIndex:'ETIQUETA',    align:'center', sortable:false, resizable:false},
			{width:303, dataIndex:'DESCRIPCION', align:'center', sortable:false, resizable:false}
		]
	});

	// Se crea el form Solicitud
	var formaSolicitud = Ext.create( 'Ext.form.Panel',{
		width:                   700,
		itemId:                  'formaSolicitud',
		title:                   '&nbsp',
		bodyPadding:             '12 6 12 6',
		style:                   'margin: 0px auto 0px auto;',
		frame:                   false,
		border:                  true,
		hidden:                  true,
		autoHeight:              true,
		items: [
			gridDatosCotizacion,
			{
				xtype:             'container',
				layout:            'hbox',
				anchor:            '100%',
				items:[{
					width:          74,
					xtype:          'displayfield',
					hideLabel:      true
				},{
					width:          500,
					height:         60,
					margins:        '2 20 2 20',
					xtype:          'displayfield',
					name:           'mensaje1',
					itemId:         'mensaje1_id',
					hideLabel:      true,
					border:         1,
					style: {
						borderColor: 'black',
						borderStyle: 'solid'
					}
				}]
			},
			gridEjecutivoNafin,
			gridIntermediario,
			gridOperacion,
			gridDetalleDisposiciones,
			gridPlanPagos,
			{
				xtype:             'container',
				layout:            'hbox',
				anchor:            '100%',
				items:[{
					width:          14,
					xtype:          'displayfield',
					hideLabel:      true
				},{
					width:          620,
					height:         60,
					margins:        '2 20 2 20',
					xtype:          'displayfield',
					name:           'mensaje2',
					itemId:         'mensaje2_id',
					hideLabel:      true,
					border:         1,
					style: {
						borderColor: 'black',
						borderStyle: 'solid'
					}
				}]
			},
			gridTasaIndicativa,
			gridTasaConfirmada,
			{
				xtype:             'container',
				layout:            'hbox',
				anchor:            '100%',
				items:[{
					width:          14,
					xtype:          'displayfield',
					hideLabel:      true
				},{
					width:          620,
					height:         60,
					margins:        '2 20 2 20',
					xtype:          'displayfield',
					name:           'mensaje3',
					itemId:         'mensaje3_id',
					hideLabel:      true,
					border:         1,
					style: {
						borderColor: 'black',
						borderStyle: 'solid'
					}
				}]
			},{
				xtype:             'container',
				layout:            'hbox',
				anchor:            '100%',
				items:[{
					width:          14,
					xtype:          'displayfield',
					hideLabel:      true
				},{
					width:          620,
					height:         60,
					margins:        '2 20 2 20',
					xtype:          'displayfield',
					name:           'mensaje4',
					itemId:         'mensaje4_id',
					hideLabel:      true,
					hidden:         true,
					border:         1,
					style: {
						borderColor: 'black',
						borderStyle: 'solid'
					}
				}]
			},{
				xtype:          'textfield',
				fieldLabel:     'Tipo Consulta',
				name:           'tipo_consulta',
				itemId:         'tipo_consulta_id',
				hidden:         true
			},{
				xtype:          'textfield',
				fieldLabel:     'Criterio busqueda',
				name:           'criterio_busqueda',
				itemId:         'criterio_busqueda_id',
				hidden:         true
			},{
				xtype:          'textfield',
				fieldLabel:     'Estatus_solic',
				name:           'estatus_solic',
				itemId:         'estatus_solic_id',
				hidden:         true
			}
		],
		buttons: [{
			text:                 'Generar Archivo PDF',
			itemId:               'btnGeneraPDFSolicitud',
			iconCls:              'icoPdf',
			handler:              generaPDFSolicitud
		},{
			text:                 'Salir',
			itemId:               'btnSalir',
			iconCls:              'icoRegresar',
			handler:              salir
		}]
	});

	// Se crea el form Estatus
	var formaEstatus = Ext.create( 'Ext.form.Panel',{
		width:                   '95%',
		itemId:                  'formaEstatus',
		title:                   '&nbsp',
		bodyPadding:             '12 6 12 6',
		style:                   'margin: 0px auto 0px auto;',
		frame:                   false,
		border:                  true,
		hidden:                  true,
		autoHeight:              true,
		items: [
			gridDatosEstatus,
			gridCotizacionEstatus,
			gridCalculadoraEstatus
		],
		buttons: [{
			text:                 'Generar Archivo PDF',
			itemId:               'btnGeneraPDFEstatus',
			iconCls:              'icoPdf',
			handler:              generarPdfEstatus
		},{
			text:                 'Salir',
			itemId:               'btnSalir',
			iconCls:              'icoRegresar',
			handler:              salirEstatus
		}]
	});


	// Se crea el form principal
	var formaPrincipal = Ext.create( 'Ext.form.Panel',{
		width:                        '95%',
		itemId:                       'formaPrincipal',
		title:                        'Criterios de b�squeda',
		bodyPadding:                  '12 6 12 6',
		style:                        'margin: 0px auto 0px auto;',
		frame:                        true,
		border:                       true,
		fieldDefaults: {
			msgTarget:                 'side'
		},
		items: [{
			xtype:                     'container',
			anchor:                    '100%',
			layout:                    'hbox',
			items:[{
				xtype:                  'container',
				layout:                 'anchor',
				flex:                   1,
				items: [{
					xtype:               'container',
					layout:              'hbox',
					anchor:              '95%',
					margin:              '0 0 5 0',
					items:[{
						width:            240,
						labelWidth:       120,
						xtype:            'datefield',
						id:               'fecha_solicitud_ini_id',
						name:             'fecha_solicitud_ini',
						hiddenName:       'fecha_solicitud_ini',
						fieldLabel:       'Fecha Solicitud',
						vtype:            'rangofecha',
						minValue:         '01/01/1901',
						campoFinFecha:    'fecha_solicitud_fin_id',
						margins:          '0 20 0 0',
						startDay:         0
					},{
						width:            158,
						labelWidth:       35,
						xtype:            'datefield',
						id:               'fecha_solicitud_fin_id',
						name:             'fecha_solicitud_fin',
						hiddenName:       'fecha_solicitud_fin',
						fieldLabel:       'a',
						vtype:            'rangofecha',
						minValue:         '01/01/1901',
						campoInicioFecha: 'fecha_solicitud_ini_id',
						margins:          '0 20 0 0',
						startDay:         1
					}]
				},{
					labelWidth:          120,
					anchor:              '95%',
					xtype:               'textfield',
					fieldLabel:          'N�mero de Solicitud',
					name:                'no_solicitud',
					itemId:              'no_solicitud_id',
					maxLength:            12
				},{
					labelWidth:          120,
					anchor:              '95%',
					xtype:               'combobox',
					itemId:              'ic_if_id',
					name:                'ic_if',
					hiddenName:          'ic_if',
					fieldLabel:          'Intermediario',
					emptyText:           'Seleccione ...',
					displayField:        'descripcion',
					valueField:          'clave',
					queryMode:           'local',
					triggerAction:       'all',
					listClass:           'x-combo-list-small',
					typeAhead:           true,
					selectOnTab:         true,
					lazyRender:          true,
					forceSelection:      true,
					editable:            true,
					store:               catalogoIntermediario
				},{
					xtype:               'container',
					layout:              'hbox',
					anchor:              '95%',
					margin:              '0 0 5 0',
					items:[{
						width:            240,
						labelWidth:       120,
						xtype:            'datefield',
						id:               'fecha_estatus_ini_id',
						name:             'fecha_estatus_ini',
						hiddenName:       'fecha_estatus_ini',
						fieldLabel:       'Fecha cambio estatus',
						vtype:            'rangofecha',
						minValue:         '01/01/1901',
						campoFinFecha:    'fecha_estatus_fin_id',
						margins:          '0 20 0 0',
						startDay:         0
					},{
						width:            158,
						labelWidth:       35,
						xtype:            'datefield',
						id:               'fecha_estatus_fin_id',
						name:             'fecha_estatus_fin',
						hiddenName:       'fecha_estatus_fin',
						fieldLabel:       'a',
						vtype:            'rangofecha',
						minValue:         '01/01/1901',
						campoInicioFecha: 'fecha_estatus_ini_id',
						margins:          '0 20 0 0',
						startDay:         1
					}]
				},{
					labelWidth:          120,
					anchor:              '95%',
					xtype:               'textfield',
					fieldLabel:          'Usuario',
					name:                'usuario',
					itemId:              'usuario_id'
				}]
			},{
				xtype:                  'container',
				layout:                 'anchor',
				flex:                   1,
				items: [{
					labelWidth:          120,
					anchor:              '95%',
					xtype:               'combobox',
					itemId:              'ic_moneda_id',
					name:                'ic_moneda',
					hiddenName:          'ic_moneda',
					fieldLabel:          'Moneda',
					emptyText:           'Seleccione ...',
					displayField:        'descripcion',
					valueField:          'clave',
					queryMode:           'local',
					triggerAction:       'all',
					listClass:           'x-combo-list-small',
					typeAhead:           true,
					selectOnTab:         true,
					lazyRender:          true,
					forceSelection:      true,
					editable:            true,
					store:               catalogoMoneda
				},{
					xtype:               'container',
					layout:              'hbox',
					anchor:              '95%',
					margin:              '0 0 5 0',
					items:[{
						width:            240,
						labelWidth:       120,
						xtype:            'bigdecimal',
						id:               'monto_credito_ini_id',
						name:             'monto_credito_ini',
						hiddenName:       'monto_credito_ini',
						fieldLabel:       'Monto del cr�dito',
						margins:          '0 20 0 0',
						maxText:          'El tama�o m�ximo del campo es de 17 enteros 2 decimales',
						maxValue:         '9999999999999.99',
						format:           '0000000000000.00',
						vtype:            'rangoValor',
						campoFinValor:    'monto_credito_fin_id',
						allowDecimals:    true,
						allowNegative:    false,
						allowBlank:       true
					},{
						width:            158,
						labelWidth:       35,
						xtype:            'bigdecimal',
						id:               'monto_credito_fin_id',
						name:             'monto_credito_fin',
						hiddenName:       'monto_credito_fin',
						fieldLabel:       'a',
						margins:          '0 20 0 0',
						maxText:          'El tama�o m�ximo del campo es de 17 enteros 2 decimales',
						maxValue:         '9999999999999.99',
						format:           '0000000000000.00',
						vtype:            'rangoValor',
						campoInicioValor: 'monto_credito_ini_id',
						allowDecimals:    true,
						allowNegative:    false,
						allowBlank:       true
					}]
				},{
					labelWidth:          120,
					anchor:              '95%',
					xtype:               'combobox',
					itemId:              'ic_ejecutivo_id',
					name:                'ic_ejecutivo',
					hiddenName:          'ic_ejecutivo',
					fieldLabel:          'Persona Responsable (Ejecutivo)',
					emptyText:           'Seleccione ...',
					displayField:        'descripcion',
					valueField:          'clave',
					queryMode:           'local',
					triggerAction:       'all',
					listClass:           'x-combo-list-small',
					typeAhead:           true,
					selectOnTab:         true,
					lazyRender:          true,
					forceSelection:      true,
					editable:            true,
					store:               catalogoEjecutivo
				},{
					labelWidth:          120,
					anchor:              '95%',
					xtype:               'combobox',
					itemId:              'ic_estatus_id',
					name:                'ic_estatus',
					hiddenName:          'ic_estatus',
					fieldLabel:          'Estatus',
					emptyText:           'Seleccione ...',
					displayField:        'descripcion',
					valueField:          'clave',
					queryMode:           'local',
					triggerAction:       'all',
					listClass:           'x-combo-list-small',
					typeAhead:           true,
					selectOnTab:         true,
					lazyRender:          true,
					forceSelection:      true,
					editable:            true,
					store:               catalogoEstatus
				}]
			}]
		}],
		buttons: [{
			text:                      'Consultar',
			itemId:                    'btnBuscar',
			iconCls:                   'icoBuscar',
			handler:                   buscar
		},{
			text:                      'Limpiar',
			itemId:                    'btnLimpiar',
			iconCls:                   'icoLimpiar',
			handler:                   limpiar
		}]
	});

	// Contenedor principal
	var main = Ext.create('Ext.container.Container',{
		width:      949,
		minHeight:  650,
		autoHeight: true,
		id:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		style:      'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			formaSolicitud,
			NE.util.getEspaciador(15),
			gridCotizacion,
			NE.util.getEspaciador(15),
			gridCalculadora,
			formaEstatus
		]
	});

});