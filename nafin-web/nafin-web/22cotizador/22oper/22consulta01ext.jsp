<!DOCTYPE html>
<%@ page import="java.util.*" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>
<html>
<head>
	<title>Nafinet</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<%@ include file="/extjs4.jspf" %>
	<%@ include file="/01principal/menu.jspf"%>
	<script type="text/javascript" src="22consulta01ext.js?<%=session.getId()%>"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<div id="areaContenido" style="width:700px"></div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>