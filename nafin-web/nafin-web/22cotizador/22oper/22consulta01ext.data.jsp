<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		java.math.*,
		com.netro.cotizador.*,
		com.netro.cotizador.Solicitud,
		netropology.utilerias.usuarios.Usuario,
		java.util.Date"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../22secsession_extjs.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion       = request.getParameter("informacion")         == null ? "": (String)request.getParameter("informacion");
String operacion         = request.getParameter("operacion")           == null ? "": (String)request.getParameter("operacion");
String strStart          = request.getParameter("start")               == null ? "0":(String)request.getParameter("start");
String strLimit          = request.getParameter("operacion")           == null ? "15":(String)request.getParameter("limit");
String fechaSolicitudIni = request.getParameter("fecha_solicitud_ini") == null ? "": (String)request.getParameter("fecha_solicitud_ini");
String fechaSolicitudFin = request.getParameter("fecha_solicitud_fin") == null ? "": (String)request.getParameter("fecha_solicitud_fin");
String noSolicitud       = request.getParameter("no_solicitud")        == null ? "": (String)request.getParameter("no_solicitud");
String icIf              = request.getParameter("ic_if")               == null ? "": (String)request.getParameter("ic_if");
String fechaEstatusIni   = request.getParameter("fecha_estatus_ini")   == null ? "": (String)request.getParameter("fecha_estatus_ini");
String fechaEstatusFin   = request.getParameter("fecha_estatus_fin")   == null ? "": (String)request.getParameter("fecha_estatus_fin");
String usuario           = request.getParameter("usuario")             == null ? "": (String)request.getParameter("usuario");
String icMoneda          = request.getParameter("ic_moneda")           == null ? "": (String)request.getParameter("ic_moneda");
String montoCreditoIni   = request.getParameter("monto_credito_ini")   == null ? "": (String)request.getParameter("monto_credito_ini");
String montoCreditoFin   = request.getParameter("monto_credito_fin")   == null ? "": (String)request.getParameter("monto_credito_fin");
String icEjecutivo       = request.getParameter("ic_ejecutivo")        == null ? "": (String)request.getParameter("ic_ejecutivo");
String icEstatus         = request.getParameter("ic_estatus")          == null ? "": (String)request.getParameter("ic_estatus");
String icPlanPagos       = request.getParameter("ic_plan_pagos")       == null ? "": (String)request.getParameter("ic_plan_pagos");
String icSolicitud       = request.getParameter("ic_solicitud")        == null ? "": (String)request.getParameter("ic_solicitud");

String datosCotizacion   = request.getParameter("datosCotizacion")     == null ? "": (String)request.getParameter("datosCotizacion");
String datosCalculadora  = request.getParameter("datosCalculadora")    == null ? "": (String)request.getParameter("datosCalculadora");
String datosGridEstatus  = request.getParameter("datosEstatus")        == null ? "": (String)request.getParameter("datosEstatus");

HashMap datosGrid = new HashMap();
JSONArray regGrid = new JSONArray();

String mensaje        = "";
String consulta       = "";
String nombreArchivo  = "";
String infoRegresar   = "";
JSONObject resultado  = new JSONObject();
boolean success       = true;

if(!fechaSolicitudIni.equals("") && fechaSolicitudIni.length() > 10){
	fechaSolicitudIni = fechaSolicitudIni.substring(8, 10) + "/" + fechaSolicitudIni.substring(5, 7) + "/" + fechaSolicitudIni.substring(0, 4);
}
if(!fechaSolicitudFin.equals("") && fechaSolicitudFin.length() > 10){
	fechaSolicitudFin = fechaSolicitudFin.substring(8, 10) + "/" + fechaSolicitudFin.substring(5, 7) + "/" + fechaSolicitudFin.substring(0, 4);
}
if(!fechaEstatusIni.equals("") && fechaEstatusIni.length() > 10){
	fechaEstatusIni = fechaEstatusIni.substring(8, 10) + "/" + fechaEstatusIni.substring(5, 7) + "/" + fechaEstatusIni.substring(0, 4);
}
if(!fechaEstatusFin.equals("") && fechaEstatusFin.length() > 10){
	fechaEstatusFin = fechaEstatusFin.substring(8, 10) + "/" + fechaEstatusFin.substring(5, 7) + "/" + fechaEstatusFin.substring(0, 4);
}

log.info("informacion: <<<<<" + informacion + ">>>>>");
/*
log.info("fechaSolicitudIni: <<<<<" + fechaSolicitudIni + ">>>>>");
log.info("fechaSolicitudFin: <<<<<" + fechaSolicitudFin + ">>>>>");
log.info("noSolicitud: <<<<<"       + noSolicitud       + ">>>>>");
log.info("icIf: <<<<<"              + icIf              + ">>>>>");
log.info("fechaEstatusIni: <<<<<"   + fechaEstatusIni   + ">>>>>");
log.info("fechaEstatusFin: <<<<<"   + fechaEstatusFin   + ">>>>>");
log.info("usuario: <<<<<"           + usuario           + ">>>>>");
log.info("icMoneda: <<<<<"          + icMoneda          + ">>>>>");
log.info("montoCreditoIni: <<<<<"   + montoCreditoIni   + ">>>>>");
log.info("montoCreditoFin: <<<<<"   + montoCreditoFin   + ">>>>>");
log.info("icEjecutivo: <<<<<"       + icEjecutivo       + ">>>>>");
log.info("icEstatus: <<<<<"         + icEstatus         + ">>>>>");
log.info("icPlanPagos: <<<<<"       + icPlanPagos       + ">>>>>");
log.info("icSolicitud: <<<<<"       + icSolicitud       + ">>>>>");
log.info("strStart: <<<<<"          + strStart          + ">>>>>");
log.info("strLimit: <<<<<"          + strLimit          + ">>>>>");
*/

if(informacion.equals("CATALOGO_INTERMEDIARIO")){

	String    clave   = "";
	String    descr   = "";
	HashMap   mapaIf  = new HashMap();
	JSONArray jsonArr = new JSONArray();

	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	Solicitud consultaSol = new Solicitud();
	ArrayList listaIfs = solCotEsp.obtenerListaTodosIFCotizador();

	for(int i = 0; i < listaIfs.size(); i++){
		StringTokenizer stIFS = new StringTokenizer((String) listaIfs.get(i),"|");
		clave  = stIFS.nextToken().toString();
		descr  = stIFS.nextToken().toString();
		mapaIf = new HashMap();
		mapaIf.put("clave", clave);
		mapaIf.put("descripcion", descr);
		jsonArr.add(mapaIf);
	}

	infoRegresar =  "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

} else if(informacion.equals("CATALOGO_MONEDA")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COMCAT_MONEDA");
	cat.setCampoClave("IC_MONEDA");
	cat.setCampoDescripcion("CD_NOMBRE");
	cat.setValoresCondicionIn("1,54", Integer.class);
	cat.setOrden("CD_NOMBRE");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CATALOGO_EJECUTIVO")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COTCAT_EJECUTIVO");
	cat.setCampoClave("IC_USUARIO");
	cat.setCampoDescripcion("CG_NOMBRE||' '||CG_APPATERNO||' '|| CG_APMATERNO");
	cat.setOrden("CG_NOMBRE");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CATALOGO_ESTATUS")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("COTCAT_ESTATUS");
	cat.setCampoClave("IC_ESTATUS");
	cat.setCampoDescripcion("CG_DESCRIPCION");
	cat.setValoresCondicionIn("7,8,9,10", Integer.class);
	cat.setOrden("CG_DESCRIPCION");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CONSULTA_DATA")){

	String temporal    = "";
	String descTasaInd      = "";
	String consultaE   = "";
	JSONObject resultadoE  = new JSONObject();

	ConsSolCotiTasaCredNafin paginador = new ConsSolCotiTasaCredNafin();
	paginador.setFechaSolicitudIni(fechaSolicitudIni);
	paginador.setFechaSolicitudFin(fechaSolicitudFin);
	paginador.setNoSolicitud(noSolicitud);
	paginador.setIcIf(icIf);
	paginador.setFechaEstatusIni(fechaEstatusIni);
	paginador.setFechaEstatusFin(fechaEstatusFin);
	paginador.setUsuario(usuario);
	paginador.setIcMoneda(icMoneda);
	paginador.setMontoCreditoIni(montoCreditoIni);
	paginador.setMontoCreditoFin(montoCreditoFin);
	paginador.setIcEjecutivo(icEjecutivo);
	paginador.setIcEstatus(icEstatus);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	queryHelper.setMultiplesPaginadoresXPagina(true);


	int start = 0;
	int limit = 0;

	try{
		start = Integer.parseInt(strStart);
		limit = Integer.parseInt(strLimit);
	} catch(Exception e){
		throw new AppException("Error en los parametros start y limit recibidos", e);
	}

	try{
		if (operacion.equals("generar")){ //Nueva consulta
			queryHelper.executePKQuery(request);
		}
		Registros registros = queryHelper.getPageResultSet(request,start,limit);
		while (registros.next()){
			temporal =registros.getString("MONTO").toString();
			registros.setObject("MONTO", "$" + Comunes.formatoDecimal(temporal,2));

			temporal =registros.getString("NUMERO_DE_PAGO").toString();
			if(temporal.trim().equals("0")){
				registros.setObject("IC_PLAN_DE_PAGOS", "");
		} else{
				registros.setObject("IC_PLAN_DE_PAGOS", temporal);
		}

			temporal =registros.getString("USUARIOCAMBIOESTATUS").toString();
			if(temporal.trim().equals("-")){
				registros.setObject("USUARIOCAMBIOESTATUS", "");
			} else{
				registros.setObject("USUARIOCAMBIOESTATUS", temporal);
			}

			Solicitud sol = new Solicitud();
			sol.setIc_moneda(registros.getString("IC_MONEDA").toString());
			sol.setIg_plazo(registros.getString("PLAZO").toString());
			sol.setIc_tipo_tasa(registros.getString("IC_TIPO_TASA").toString());
			sol.setIc_esquema_recup(registros.getString("IC_ESQUEMA_RECUP").toString());
			sol.setCg_ut_ppc(registros.getString("CG_UT_PPC").toString());
			sol.setCg_ut_ppi(registros.getString("CG_UT_PPI").toString());
			descTasaInd = sol.getDescTasaIndicativa();
			temporal =registros.getString("TASA").toString();
			registros.setObject("DESCTASAIND", descTasaInd + Comunes.formatoDecimal(temporal, 2, false));
			
		}

		consultaE = "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
		resultadoE = JSONObject.fromObject(consultaE);

	} catch(Exception e){
		throw new AppException(" Error en la paginacion. ", e);
	}

	infoRegresar = resultadoE.toString();

} else if(informacion.equals("CONSULTA_CALCULADORA")){

	String     temporalC   = "";
	String     consultaC   = "";
	JSONObject resultadoC  = new JSONObject();

	ConsCreditosCalcOper paginadorC = new ConsCreditosCalcOper();
	paginadorC.setFechaSolicitudIni(fechaSolicitudIni);
	paginadorC.setFechaSolicitudFin(fechaSolicitudFin);
	paginadorC.setNoSolicitud(noSolicitud);
	paginadorC.setIcIf(icIf);
	paginadorC.setFechaEstatusIni(fechaEstatusIni);
	paginadorC.setFechaEstatusFin(fechaEstatusFin);
	paginadorC.setUsuario(usuario);
	paginadorC.setIcMoneda(icMoneda);
	paginadorC.setMontoCreditoIni(montoCreditoIni);
	paginadorC.setMontoCreditoFin(montoCreditoFin);
	paginadorC.setIcEjecutivo(icEjecutivo);
	paginadorC.setIcEstatus(icEstatus);
	CQueryHelperRegExtJS queryHelperC = new CQueryHelperRegExtJS(paginadorC);
	queryHelperC.setMultiplesPaginadoresXPagina(true);

	int start = 0;
	int limit = 0;
	try{
		start = Integer.parseInt(strStart);
		limit = Integer.parseInt(strLimit);
	} catch(Exception e){
		throw new AppException("Error en los parametros start y limit recibidos", e);
	}

	try{
		if (operacion.equals("generar")){ //Nueva consulta
			queryHelperC.executePKQuery(request);
		}
		Registros registrosC = queryHelperC.getPageResultSet(request,start,limit);
		while (registrosC.next()){
			temporalC =registrosC.getString("MONTO").toString();
			registrosC.setObject("MONTO", "$" + Comunes.formatoDecimal(temporalC,2));
		}

		consultaC = "{\"success\": true, \"total\": \"" + queryHelperC.getIdsSize() + "\", \"registros\": " + registrosC.getJSONData()+"}";
		resultadoC = JSONObject.fromObject(consultaC);

	} catch(Exception e){
		throw new AppException(" Error en la paginacion. ", e);
	}

	infoRegresar = resultadoC.toString();

}  else if(informacion.equals("GENERAR_REPORTE_PDF")){

	HashMap mapaEstatus     = new HashMap();
	HashMap mapaCotizacion  = new HashMap();
	HashMap mapaCalculadora = new HashMap();
	List listaEstatus       = new ArrayList();
	List listaCotizacion    = new ArrayList();
	List listaCalculadora   = new ArrayList();

	JSONArray arrayRegistros = JSONArray.fromObject(datosCotizacion);
	if(arrayRegistros.size() > 0){
		for(int i = 0; i < arrayRegistros.size(); i++){
			JSONObject auxiliar = arrayRegistros.getJSONObject(i);
			mapaCotizacion = new HashMap();
			mapaCotizacion.put("FECHA_SOLICITUD",      auxiliar.getString("FECHA_SOLICITUD")     );
			mapaCotizacion.put("FECHA_HORA_SOLICITUD", auxiliar.getString("FECHA_HORA_SOLICITUD"));
			mapaCotizacion.put("NUMERO_SOLICITUD",     auxiliar.getString("NUMERO_SOLICITUD")    );
			mapaCotizacion.put("INTERMEDIARIO",        auxiliar.getString("INTERMEDIARIO")       );
			mapaCotizacion.put("MONEDA_SOLICITUD",     auxiliar.getString("MONEDA_SOLICITUD")    );
			mapaCotizacion.put("EJECUTIVO",            auxiliar.getString("EJECUTIVO")           );
			mapaCotizacion.put("MONTO",                auxiliar.getString("MONTO")               );
			mapaCotizacion.put("PLAZO",                auxiliar.getString("PLAZO")               );
			mapaCotizacion.put("ESQUEMA_RECUP",        auxiliar.getString("ESQUEMA_RECUP")       );
			mapaCotizacion.put("DESCTASAIND",          auxiliar.getString("DESCTASAIND")         );
			mapaCotizacion.put("RS_ESTATUS",           auxiliar.getString("RS_ESTATUS")          );
			mapaCotizacion.put("CG_OBSERVACIONES",     auxiliar.getString("CG_OBSERVACIONES")    );
			listaCotizacion.add(mapaCotizacion);
		}
	}

	arrayRegistros = JSONArray.fromObject(datosCalculadora);
	if(arrayRegistros.size() > 0){
		for(int i = 0; i < arrayRegistros.size(); i++){
			JSONObject auxiliar = arrayRegistros.getJSONObject(i);
			mapaCalculadora = new HashMap();
			mapaCalculadora.put("FECHA_COTIZACION", auxiliar.getString("FECHA_COTIZACION") );
			mapaCalculadora.put("FECHA_TF",         auxiliar.getString("FECHA_TF")         );
			mapaCalculadora.put("NO_REFERENCIA",    auxiliar.getString("NO_REFERENCIA")    );
			mapaCalculadora.put("NOMBRE_IF",        auxiliar.getString("NOMBRE_IF")        );
			mapaCalculadora.put("NOMBRE_MONEDA",    auxiliar.getString("NOMBRE_MONEDA")    );
			mapaCalculadora.put("EJECUTIVO",        auxiliar.getString("EJECUTIVO")        );
			mapaCalculadora.put("MONTO",            auxiliar.getString("MONTO")            );
			mapaCalculadora.put("PLAZO",            auxiliar.getString("PLAZO")            );
			mapaCalculadora.put("ESQUEMA",          auxiliar.getString("ESQUEMA")          );
			mapaCalculadora.put("TASA",             auxiliar.getString("TASA")             );
			mapaCalculadora.put("ESTATUS",          auxiliar.getString("ESTATUS")          );
			mapaCalculadora.put("OBSERVACIONES",    auxiliar.getString("OBSERVACIONES")    );
			listaCalculadora.add(mapaCalculadora);
		}
	}

	if(!datosGridEstatus.equals("")){
		arrayRegistros = JSONArray.fromObject(datosGridEstatus);
		if(arrayRegistros.size() > 0){
			for(int i = 0; i < arrayRegistros.size(); i++){
				JSONObject auxiliar = arrayRegistros.getJSONObject(i);
				mapaEstatus = new HashMap();
				mapaEstatus.put("ETIQUETA",    auxiliar.getString("ETIQUETA")   );
				mapaEstatus.put("DESCRIPCION", auxiliar.getString("DESCRIPCION"));
				listaEstatus.add(mapaEstatus);
			}
		}
		
	}

	ConsCreditosCalcOper paginador = new ConsCreditosCalcOper();
	paginador.setGridCotizacion(listaCotizacion);
	paginador.setGridCalculadora(listaCalculadora);
	paginador.setGridEstatus(listaEstatus);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	try{
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Exception e) {
		success = false;
		throw new AppException("Error al generar el archivo PDF", e);
	}

	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();


} else if(informacion.equals("CAMBIA_ESTATUS")){

	HashMap mapaCotizacion  = new HashMap();
	HashMap mapaCalculadora = new HashMap();
	List listaCotizacion    = new ArrayList();
	List listaCalculadora   = new ArrayList();

	JSONArray cotizacionEstatus  = new JSONArray();
	JSONArray calculadoraEstatus = new JSONArray();

	JSONArray arrayRegistros = JSONArray.fromObject(datosCotizacion);
	if(arrayRegistros.size() > 0){
		for(int i = 0; i < arrayRegistros.size(); i++){
			JSONObject auxiliar = arrayRegistros.getJSONObject(i);
			if("7".equals(auxiliar.getString("IC_ESTATUS")) || "10".equals(auxiliar.getString("IC_ESTATUS"))){
				if("true".equals(auxiliar.getString("CAMBIA_OPERADO"))){
					mapaCotizacion = new HashMap();
					mapaCotizacion.put("IC_SOLIC_ESP",           auxiliar.getString("IC_SOLIC_ESP")          );
					mapaCotizacion.put("FECHA_SOLICITUD",        auxiliar.getString("FECHA_SOLICITUD")       );
					mapaCotizacion.put("FECHA_HORA_SOLICITUD",   auxiliar.getString("FECHA_HORA_SOLICITUD")  );
					mapaCotizacion.put("NUMERO_SOLICITUD",       auxiliar.getString("NUMERO_SOLICITUD")      );
					mapaCotizacion.put("INTERMEDIARIO",          auxiliar.getString("INTERMEDIARIO")         );
					mapaCotizacion.put("MONEDA_SOLICITUD",       auxiliar.getString("MONEDA_SOLICITUD")      );
					mapaCotizacion.put("EJECUTIVO",              auxiliar.getString("EJECUTIVO")             );
					mapaCotizacion.put("MONTO",                  auxiliar.getString("MONTO")                 );
					mapaCotizacion.put("PLAZO",                  auxiliar.getString("PLAZO")                 );
					mapaCotizacion.put("TASA",                   auxiliar.getString("TASA")                  );
					mapaCotizacion.put("ESQUEMA_RECUP",          auxiliar.getString("ESQUEMA_RECUP")         );
					mapaCotizacion.put("CG_OBSERVACIONES",       auxiliar.getString("CG_OBSERVACIONES")      );
					mapaCotizacion.put("IC_PLAN_DE_PAGOS",       auxiliar.getString("IC_PLAN_DE_PAGOS")      );
					mapaCotizacion.put("IC_ESTATUS",             "9"            );
					mapaCotizacion.put("RS_ESTATUS",             "Operada"            );
					mapaCotizacion.put("CG_OBSERVACIONES_SIRAC", auxiliar.getString("CG_OBSERVACIONES_SIRAC"));
					mapaCotizacion.put("SOLICITUD_PORTAL",       auxiliar.getString("SOLICITUD_PORTAL")      );
					mapaCotizacion.put("FECHACAMBIOESTATUS",     auxiliar.getString("FECHACAMBIOESTATUS")    );
					mapaCotizacion.put("USUARIOCAMBIOESTATUS",   auxiliar.getString("USUARIOCAMBIOESTATUS")  );
					mapaCotizacion.put("DESCTASAIND",            auxiliar.getString("DESCTASAIND")           );
					listaCotizacion.add(mapaCotizacion);
					cotizacionEstatus.add(mapaCotizacion);
				}
			}
		}
	}
	arrayRegistros = JSONArray.fromObject(datosCalculadora);
	if(arrayRegistros.size() > 0){
		for(int i = 0; i < arrayRegistros.size(); i++){
			JSONObject auxiliar = arrayRegistros.getJSONObject(i);
			if("2".equals(auxiliar.getString("ID_STATUS"))){
				if("true".equals(auxiliar.getString("CAMBIA_OPERADO"))){
					mapaCalculadora = new HashMap();
					mapaCalculadora.put("FECHA_COTIZACION", auxiliar.getString("FECHA_COTIZACION"));
					mapaCalculadora.put("FECHA_TF",         auxiliar.getString("FECHA_TF")        );
					mapaCalculadora.put("NO_REFERENCIA",    auxiliar.getString("NO_REFERENCIA"    ));
					mapaCalculadora.put("NOMBRE_IF",        auxiliar.getString("NOMBRE_IF"));
					mapaCalculadora.put("NOMBRE_MONEDA",    auxiliar.getString("NOMBRE_MONEDA"));
					mapaCalculadora.put("EJECUTIVO",        auxiliar.getString("EJECUTIVO"));
					mapaCalculadora.put("MONTO",            auxiliar.getString("MONTO"));
					mapaCalculadora.put("PLAZO",            auxiliar.getString("PLAZO"));
					mapaCalculadora.put("ESQUEMA",          auxiliar.getString("ESQUEMA"));
					mapaCalculadora.put("TASA",             auxiliar.getString("TASA"));
					mapaCalculadora.put("ESTATUS",          "En proceso");
					mapaCalculadora.put("OBSERVACIONES",    auxiliar.getString("OBSERVACIONES"));
					mapaCalculadora.put("ID_STATUS",        "4");
					mapaCalculadora.put("CAMBIA_OPERADO",  "false");
					listaCalculadora.add(mapaCalculadora);
					calculadoraEstatus.add(mapaCalculadora);
				}
			}
		}
	}

	if(listaCotizacion.size() > 0 || listaCalculadora.size() > 0){
		try{

			AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
			Solicitud consultaSol = new Solicitud();
			autSol.operaSolic(listaCotizacion, listaCalculadora, (String)session.getAttribute("Clave_usuario"), strNombreUsuario);
			mensaje = "OK";

			String Fecha=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
			String Hora=(new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
			HashMap mapaEstaus = new HashMap();
			JSONArray datosEstatus = new JSONArray();
			mapaEstaus.put("ETIQUETA", "Fecha de cambio de estatus");
			mapaEstaus.put("DESCRIPCION", Fecha);
			datosEstatus.add(mapaEstaus);
			mapaEstaus = new HashMap();
			mapaEstaus.put("ETIQUETA", "Hora de cambio de estatus");
			mapaEstaus.put("DESCRIPCION", Hora);
			datosEstatus.add(mapaEstaus);
			mapaEstaus = new HashMap();
			mapaEstaus.put("ETIQUETA", "Usuario");
			mapaEstaus.put("DESCRIPCION", iNoUsuario+" - "+strNombreUsuario);
			datosEstatus.add(mapaEstaus);

			resultado.put("datosEstatus",       datosEstatus);
			resultado.put("cotizacionEstatus",  cotizacionEstatus);
			resultado.put("calculadoraEstatus", calculadoraEstatus);

		} catch(Exception e){
			mensaje = e.getMessage();
		}
	} else{
		mensaje = "No se ha marcado ningun registro";
	}

	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();

} else if(informacion.equals("GENERAR_PAGOS")){

	try{

		SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
		Solicitud consultaSol = new Solicitud();
		CreaArchivo archivo = new CreaArchivo();
		StringBuilder contenidoArchivo=new StringBuilder();

		consultaSol = solCotEsp.consultaSolicitud(icPlanPagos);

		if((consultaSol.getPagos()).size() > 0){
			ArrayList alFilas = consultaSol.getPagos();
			contenidoArchivo.append("\r\n Plan de Pagos Capital - Solicitud Número: "+consultaSol.getNumero_solicitud() + "\r\n\r\n"+
								"Número de Pago,Fecha de Pago,Monto a pagar \r\n");
			for(int i=0;i<alFilas.size();i++){
				ArrayList alColumnas = (ArrayList)alFilas.get(i);
				contenidoArchivo.append(alColumnas.get(0).toString() + "," +alColumnas.get(1).toString() + "," +
						alColumnas.get(2).toString() + "\r\n");
			}
		}

		if(archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
			nombreArchivo = archivo.nombre;
		}
		resultado.put("success", new Boolean(true));
		resultado.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);

	} catch(Exception e){
		success = false;
		mensaje = "Ocurrió un error al obtener los datos. " + e;
		log.warn("Ocurrió un error al obtener los datos. " + e);
	}

	resultado.put("mensaje", mensaje             );
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("CONSULTA_SOLICITUD") || informacion.equals("PDF_SOLICITUD")){

	// Se crean las variables
	String tec     = "";
	String periodo = "";

	StringBuffer mensaje1 = new StringBuffer();
	StringBuffer mensaje2 = new StringBuffer();
	StringBuffer mensaje3 = new StringBuffer();
	StringBuffer mensaje4 = new StringBuffer(); // Observaciones
	StringBuffer mensaje5 = new StringBuffer();

	HashMap mapaDatosCotizacion = new HashMap();
	HashMap mapaEjecutivoNafin  = new HashMap();
	HashMap mapaIntermediario   = new HashMap();
	HashMap mapaOperacion       = new HashMap();
	HashMap mapaDetalleDisp     = new HashMap();
	HashMap mapaPlanPagos       = new HashMap();
	HashMap mapaTasaIndicativa  = new HashMap();
	HashMap mapaTasaConfirmada  = new HashMap();

	JSONArray listaDatosCotizacion = new JSONArray();
	JSONArray listaEjecutivoNafin  = new JSONArray();
	JSONArray listaIntermediario   = new JSONArray();
	JSONArray listaOperacion       = new JSONArray();
	JSONArray listaDetalleDisp     = new JSONArray();
	JSONArray listaPlanPagos       = new JSONArray();
	JSONArray listaTasaIndicativa  = new JSONArray();
	JSONArray listaTasaConfirmada  = new JSONArray();

	// Obtengo los datos
	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
	Solicitud consultaSol = new Solicitud();

	if(icSolicitud.equals("2") && strTipoUsuario.equals("IF")){
		autSol.setEstatusSolic(icSolicitud, "5", icEstatus);
	}
	consultaSol = solCotEsp.consultaSolicitud(icSolicitud);
	if((consultaSol.getIc_tipo_tasa()).equals("3")){
		tec = solCotEsp.techoTasaProtegida();
	}

	// Obtengo el periodo
	if((consultaSol.getIc_esquema_recup()).equals("1")){
		periodo = "Al Vencimiento";
	} else if((consultaSol.getIc_esquema_recup()).equals("4") && (consultaSol.getCg_ut_ppc()).equals("0")){
		periodo = "Al Vencimiento";
	} else if(!(consultaSol.periodoTasa(consultaSol.getCg_ut_ppi())).equals("")){
		periodo = consultaSol.periodoTasa(consultaSol.getCg_ut_ppi())+"mente";
	} else if(!(consultaSol.periodoTasa(consultaSol.getCg_ut_ppc())).equals("")){
		periodo = consultaSol.periodoTasa(consultaSol.getCg_ut_ppc())+"mente";
	} else{
		periodo = "Al Vencimiento";
	}

	// Se arman los mensajes
	mensaje1.append("En caso de que la disposición de los recursos no se lleve a cabo total o parcialmente en la fecha estipulada, ");
	mensaje1.append("Nacional Financiera aplicará el cobro de una comisión sobre los montos no dispuestos, ");
	mensaje1.append("conforme a las políticas de crédito aplicables a este tipo de operaciones.");

	if(icEstatus.equals("2") || icEstatus.equals("5")){
		if((consultaSol.getTipoCotizacion()).equals("I") && strTipoUsuario.equals("IF")){
			mensaje2.append("COTIZACION INDICATIVA, <BR>");
			mensaje2.append("NO ES POSIBLE TOMAR EN FIRME LA OPERACION, <BR>");
			mensaje2.append("FAVOR DE CONSULTAR A SU EJECUTIVO.");
		} else if((consultaSol.getCs_vobo_ejec()).equals("N") && strTipoUsuario.equals("IF")){
			mensaje2.append("Para realizar la toma en firme favor de ponerse en contacto<BR>");
			mensaje2.append("con su ejecutivo");
		}
		mensaje3.append("<B>Nota:</B><BR>");
		mensaje3.append("<B>Tasa con oferta Mismo Día:</B> El Intermediario Financiero tiene la opción de tomarla en firme ");
		mensaje3.append("el mismo día que haya recibido la cotización indicativa a más tardar a las 12:30 horas.<BR>");
		if(!(Comunes.formatoDecimal(consultaSol.getIg_tasa_spot(),2)).equals("0.00")){
			mensaje3.append("<B>Tasa con oferta 48 Horas:</B> El Intermediario Financiero tiene la opción de tomarla ");
			mensaje3.append("en firme a más tardar dos días hábiles después de que la Tesorería haya proporcionado la ");
			mensaje3.append("cotización indicativa antes de las 12:30 horas del segundo día hábil.<BR>");
		}
		mensaje3.append("Las cotizaciones indicativas proporcionadas por la Tesorería de Nacional Financiera son de ");
		mensaje3.append("uso reservado para el intermediario solicitante y será responsabilidad de dicho intermediario ");
		mensaje3.append("el mal uso de la información proporcionada.");
	}

	if(!consultaSol.getCg_observaciones().trim().equals("")){
		mensaje4.append("<b>Observaciones: </b><br>");
		mensaje4.append(consultaSol.getCg_observaciones());
	} else{
		mensaje4.append("");
	}

	if(icEstatus.equals("7") || icEstatus.equals("8") || icEstatus.equals("9")){
		if(consultaSol.getCs_aceptado_md().equals("S")){
			mensaje5.append("Tasa Confirmada " + consultaSol.getDescTasaIndicativa());
			mensaje5.append(" " + Comunes.formatoDecimal(consultaSol.getIg_tasa_md(),2));
			mensaje5.append(" anual pagadera " + periodo + " " + tec);
		} else{
			mensaje5.append("Tasa Confirmada " + consultaSol.getDescTasaIndicativa());
			mensaje5.append(" " + Comunes.formatoDecimal(consultaSol.getIg_tasa_spot(),2));
			mensaje5.append(" anual pagadera " + periodo + " " + tec);
		}
	}

	// Se arma el grid: Datos de la Cotización
	mapaDatosCotizacion.put("ETIQUETA", "Número de Solicitud:");
	mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getNumero_solicitud());
	listaDatosCotizacion.add(mapaDatosCotizacion);
	mapaDatosCotizacion = new HashMap();
	mapaDatosCotizacion.put("ETIQUETA", "Fecha de Solicitud:");
	mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getDf_solicitud());
	listaDatosCotizacion.add(mapaDatosCotizacion);
	mapaDatosCotizacion = new HashMap();
	mapaDatosCotizacion.put("ETIQUETA", "Hora de Solicitud:");
	mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getHora_solicitud());
	listaDatosCotizacion.add(mapaDatosCotizacion);
	if(!icEstatus.equals("1") && !icEstatus.equals("3") && !icEstatus.equals("4")){ //1=Solicictada,3=Rechazada,4=En proceso
		String aux = "Cotización";
		if(icEstatus.equals("8")){
			aux = "Cancelación";
		}
		mapaDatosCotizacion = new HashMap();
		mapaDatosCotizacion.put("ETIQUETA", "Fecha de " + aux + ":");
		mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getDf_cotizacion());
		listaDatosCotizacion.add(mapaDatosCotizacion);
		mapaDatosCotizacion = new HashMap();
		mapaDatosCotizacion.put("ETIQUETA", "Hora de " + aux + ":");
		mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getHora_cotizacion());
		listaDatosCotizacion.add(mapaDatosCotizacion);
	}
	mapaDatosCotizacion = new HashMap();
	mapaDatosCotizacion.put("ETIQUETA", "Número y Nombre de Usuario:");
	mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getIc_usuario() + " - " + consultaSol.getNombre_usuario());
	listaDatosCotizacion.add(mapaDatosCotizacion);
	if(icEstatus.equals("7") || icEstatus.equals("8") || icEstatus.equals("9")){
		mapaDatosCotizacion = new HashMap();
		mapaDatosCotizacion.put("ETIQUETA", "Número de Recibo:");
		mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getIg_num_referencia());
		listaDatosCotizacion.add(mapaDatosCotizacion);
	}

	// Se arma el grid: Datos del Ejecutivo
	mapaEjecutivoNafin.put("ETIQUETA", "Nombre del Ejecutivo:");
	mapaEjecutivoNafin.put("DESCRIPCION", (String)consultaSol.getNombre_ejecutivo());
	listaEjecutivoNafin.add(mapaEjecutivoNafin);
	mapaEjecutivoNafin = new HashMap();
	mapaEjecutivoNafin.put("ETIQUETA", "Correo electrónico:");
	mapaEjecutivoNafin.put("DESCRIPCION", (String)consultaSol.getCg_mail());
	listaEjecutivoNafin.add(mapaEjecutivoNafin);
	mapaEjecutivoNafin = new HashMap();
	mapaEjecutivoNafin.put("ETIQUETA", "Teléfono");
	mapaEjecutivoNafin.put("DESCRIPCION", (String)consultaSol.getCg_telefono());
	listaEjecutivoNafin.add(mapaEjecutivoNafin);
	mapaEjecutivoNafin = new HashMap();
	mapaEjecutivoNafin.put("ETIQUETA", "Fax:");
	mapaEjecutivoNafin.put("DESCRIPCION", (String)consultaSol.getCg_fax());
	listaEjecutivoNafin.add(mapaEjecutivoNafin);

	// Se arma el grid: Datos del Intermediario
	mapaIntermediario.put("ETIQUETA", "Tipo de operación:");
	mapaIntermediario.put("DESCRIPCION", (String)consultaSol.getTipoOper());
	listaIntermediario.add(mapaIntermediario);
	if(!(consultaSol.getTipoOper()).equals("Primer Piso")){
		mapaIntermediario = new HashMap();
		mapaIntermediario.put("ETIQUETA", "Nombre del Intermediario:");
		mapaIntermediario.put("DESCRIPCION", (!"".equals(consultaSol.getNombre_if_ci()))?consultaSol.getNombre_if_ci():consultaSol.getNombre_if_cs());
		listaIntermediario.add(mapaIntermediario);
		if(!strTipoUsuario.equals("IF") && !(consultaSol.getRiesgoIf()).equals("")){
			mapaIntermediario = new HashMap();
			mapaIntermediario.put("ETIQUETA", "Riesgo del Intermediario:");
			mapaIntermediario.put("DESCRIPCION", (String)consultaSol.getRiesgoIf());
			listaIntermediario.add(mapaIntermediario);
		}
	}
	if(!strTipoUsuario.equals("IF") && !(consultaSol.getCg_acreditado()).equals("")){
		mapaIntermediario = new HashMap();
		mapaIntermediario.put("ETIQUETA", "Nombre del Acreditado:");
		mapaIntermediario.put("DESCRIPCION", (String)consultaSol.getCg_acreditado());
		listaIntermediario.add(mapaIntermediario);
		if((consultaSol.getTipoOper()).equals("Primer Piso")){
			mapaIntermediario = new HashMap();
			mapaIntermediario.put("ETIQUETA", "Riesgo del Acreditado:");
			mapaIntermediario.put("DESCRIPCION", (String)consultaSol.getRiesgoAc());
			listaIntermediario.add(mapaIntermediario);
		}
	}
	
	// Se arma el grid: Características de la Operación
	if(!(consultaSol.getCg_programa()).equals("")){
		mapaOperacion.put("ETIQUETA", "Nombre del programa o acreditado final:");
		mapaOperacion.put("DESCRIPCION", (String)consultaSol.getCg_programa());
		listaOperacion.add(mapaOperacion);
	}
	mapaOperacion = new HashMap();
	mapaOperacion.put("ETIQUETA", "Monto toal requerido:");
	mapaOperacion.put("DESCRIPCION", "$" + Comunes.formatoDecimal(consultaSol.getFn_monto(),2) + " " + (consultaSol.getIc_moneda().equals("1")?"Pesos":"Dólares"));
	listaOperacion.add(mapaOperacion);
	mapaOperacion = new HashMap();
	mapaOperacion.put("ETIQUETA", "Tipo de tasa requerida:");
	mapaOperacion.put("DESCRIPCION", (String)consultaSol.getTipo_tasa());
	listaOperacion.add(mapaOperacion);
	mapaOperacion = new HashMap();
	mapaOperacion.put("ETIQUETA", "Número de disposiciones:");
	mapaOperacion.put("DESCRIPCION", (String)consultaSol.getIn_num_disp());
	listaOperacion.add(mapaOperacion);
	if(Integer.parseInt(consultaSol.getIn_num_disp())>1){
	mapaOperacion = new HashMap();
	mapaOperacion.put("ETIQUETA", "Disposiciones múltiples. Tasa de interés:");
	mapaOperacion.put("DESCRIPCION", (consultaSol.getCs_tasa_unica()).equals("S")?"Unica":"Varias");
	listaOperacion.add(mapaOperacion);
	}
	mapaOperacion = new HashMap();
	mapaOperacion.put("ETIQUETA", "Plazo de la operación:");
	mapaOperacion.put("DESCRIPCION", (String)consultaSol.getIg_plazo() + " días");
	listaOperacion.add(mapaOperacion);
	mapaOperacion = new HashMap();
	mapaOperacion.put("ETIQUETA", "Esquema de recuperación del capital:");
	mapaOperacion.put("DESCRIPCION", (String)consultaSol.getEsquema_recup());
	listaOperacion.add(mapaOperacion);
	if(!(consultaSol.getIc_esquema_recup()).equals("1")){
		String icEsquemaRecup = consultaSol.getIc_esquema_recup();//Valores que puede tomar ic_esquema_recup:1=Cupon cero,2=Tradicional,3=Plan de pagos,4=Rentas
		if(icEsquemaRecup.equals("2") || icEsquemaRecup.equals("3")){
			mapaOperacion = new HashMap();
			mapaOperacion.put("ETIQUETA", "Periodicidad del pago de Interés:");
			mapaOperacion.put("DESCRIPCION", (String)consultaSol.periodoTasa(consultaSol.getCg_ut_ppi()));
			listaOperacion.add(mapaOperacion);
		}
		if(!icEsquemaRecup.equals("3") && (icEsquemaRecup.equals("2") || icEsquemaRecup.equals("4"))){
			mapaOperacion = new HashMap();
			mapaOperacion.put("ETIQUETA", "Periodicidad de capital:");
			mapaOperacion.put("DESCRIPCION", (String)consultaSol.periodoTasa(consultaSol.getCg_ut_ppc()));
			listaOperacion.add(mapaOperacion);
		}
		if(icEsquemaRecup.equals("2") && !(consultaSol.getCg_pfpc()).equals("")){
			mapaOperacion = new HashMap();
			mapaOperacion.put("ETIQUETA", "Primer fecha de pago de capital:");
			mapaOperacion.put("DESCRIPCION", (String)consultaSol.getCg_pfpc());
			listaOperacion.add(mapaOperacion);
			mapaOperacion = new HashMap();
			mapaOperacion.put("ETIQUETA", "Penúltima fecha de pago de capital:");
			mapaOperacion.put("DESCRIPCION", (String)consultaSol.getCg_pufpc());
			listaOperacion.add(mapaOperacion);
		}
	}

	// Se arma el grid: Detalle de disposiciones
	for(int i = 0; i < Integer.parseInt(consultaSol.getIn_num_disp()); i++){
		mapaDetalleDisp = new HashMap();
		mapaDetalleDisp.put("NUMERO_DISPOSICION", "" + (i+1));
		mapaDetalleDisp.put("MONTO", "$" + Comunes.formatoDecimal(consultaSol.getFn_monto_disp(i),2));
		mapaDetalleDisp.put("FECHA_DISPOSICION", (String)consultaSol.getDf_disposicion(i));
		mapaDetalleDisp.put("FECHA_VENCIMIENTO", (String)consultaSol.getDf_vencimiento(i));
		listaDetalleDisp.add(mapaDetalleDisp);
	}

	// Se arma el grid: Plan de Pagos Capital
	if((consultaSol.getPagos()).size() > 0){
		ArrayList alFilas = consultaSol.getPagos();
		for(int i = 0; i < alFilas.size(); i++){
			ArrayList alColumnas = (ArrayList)alFilas.get(i);
			mapaPlanPagos = new HashMap();
			mapaPlanPagos.put("NUMERO_PAGO", (String)alColumnas.get(0));
			mapaPlanPagos.put("FECHA_PAGO", (String)alColumnas.get(1));
			mapaPlanPagos.put("MONTO_PAGAR", "$" + Comunes.formatoDecimal(alColumnas.get(2).toString(),2));
			mapaPlanPagos.put("VACIO", "");
			listaPlanPagos.add(mapaPlanPagos);
		}
	}

	// Se arma el grid: Tasa indicativa
	if(icEstatus.equals("2") || icEstatus.equals("5")){
		mapaTasaIndicativa.put("ETIQUETA", "Tasa Indicativa con oferta mismo día:");
		mapaTasaIndicativa.put("DESCRIPCION", (String)consultaSol.getDescTasaIndicativa() + " " + Comunes.formatoDecimal(consultaSol.getIg_tasa_md(),2) + " anual pagadera " + periodo);
		listaTasaIndicativa.add(mapaTasaIndicativa);
		if(!(Comunes.formatoDecimal(consultaSol.getIg_tasa_spot(),2)).equals("0.00")){
			mapaTasaIndicativa = new HashMap();
			mapaTasaIndicativa.put("ETIQUETA", "Tasa Indicativa con oferta 48 horas:");
			mapaTasaIndicativa.put("DESCRIPCION", (String)consultaSol.getDescTasaIndicativa() + " " + Comunes.formatoDecimal(consultaSol.getIg_tasa_spot(),2) + " anual pagadera " + periodo);
			listaTasaIndicativa.add(mapaTasaIndicativa);
		}
	}

	// Se arma el grid: Tasa confirmada
	if(icEstatus.equals("7") || icEstatus.equals("8") || icEstatus.equals("9")){
		mapaTasaConfirmada.put("ETIQUETA", "<b>TASA CONFIRMADA</b>");
		mapaTasaConfirmada.put("DESCRIPCION", "<b>FECHA Y HORA DE " + (icEstatus.equals("8")?"CANCELACION</b>":"CONFIRMACION</b>"));
		listaTasaConfirmada.add(mapaTasaConfirmada);
		mapaTasaConfirmada = new HashMap();
		mapaTasaConfirmada.put("ETIQUETA", mensaje5.toString());
		mapaTasaConfirmada.put("DESCRIPCION", icEstatus.equals("8")?consultaSol.getDf_cotizacion() + " " + consultaSol.getHora_cotizacion():consultaSol.getDf_aceptacion());
		listaTasaConfirmada.add(mapaTasaConfirmada);
	}

	if(informacion.equals("CONSULTA_SOLICITUD")){

		resultado.put("informacion",          informacion);
		resultado.put("mensaje",              mensaje);
		resultado.put("criterio_busqueda",    icSolicitud);
		resultado.put("estatus",              icEstatus);
		resultado.put("listaDatosCotizacion", listaDatosCotizacion);
		resultado.put("listaEjecutivoNafin",  listaEjecutivoNafin);
		resultado.put("listaIntermediario",   listaIntermediario);
		resultado.put("listaOperacion",       listaOperacion);
		resultado.put("listaDetalleDisp",     listaDetalleDisp);
		resultado.put("listaPlanPagos",       listaPlanPagos);
		resultado.put("listaTasaIndicativa",  listaTasaIndicativa);
		resultado.put("listaTasaConfirmada",  listaTasaConfirmada);
		resultado.put("mensaje1",             mensaje1.toString());
		resultado.put("mensaje2",             mensaje2.toString());
		resultado.put("mensaje3",             mensaje3.toString());
		resultado.put("mensaje4",             mensaje4.toString());
		resultado.put("success",              new Boolean(success));
		infoRegresar = resultado.toString();

	} else if(informacion.equals("PDF_SOLICITUD")){

		CreditosConfirmadosArchivos paginador = new CreditosConfirmadosArchivos();
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		if(icEstatus.equals("7") || icEstatus.equals("8") || icEstatus.equals("9")){
			paginador.setMensaje1(mensaje1.toString());
		} else{
			paginador.setMensaje1("");
		}
		paginador.setMensaje2(mensaje2.toString());
		paginador.setMensaje3(mensaje3.toString());
		paginador.setMensaje4(mensaje4.toString());
		paginador.setListaDatosCotizacion(listaDatosCotizacion);
		paginador.setListaEjecutivoNafin(listaEjecutivoNafin);
		paginador.setListaIntermediario(listaIntermediario);
		paginador.setListaOperacion(listaOperacion);
		paginador.setListaDetalleDisp(listaDetalleDisp);
		paginador.setListaPlanPagos(listaPlanPagos);
		paginador.setListaTasaIndicativa(listaTasaIndicativa);
		paginador.setListaTasaConfirmada(listaTasaConfirmada);

		try{
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Exception e) {
			success = false;
			throw new AppException("Error al generar el archivo PDF", e);
		}

		resultado.put("success", new Boolean(success));
		infoRegresar = resultado.toString();

	}

} else if(informacion.equals("FORMATO_CALCULADORA") || informacion.equals("PDF_CALCULADORA")){

	// Se crean las variables
	String tec     = "";
	String periodo = "";

	StringBuffer mensaje1 = new StringBuffer();

	HashMap mapaDatosCotizacion = new HashMap();
	HashMap mapaEjecutivoNafin  = new HashMap();
	HashMap mapaIntermediario   = new HashMap();
	HashMap mapaOperacion       = new HashMap();
	HashMap mapaTasaConfirmada  = new HashMap();

	JSONArray listaDatosCotizacion = new JSONArray();
	JSONArray listaEjecutivoNafin  = new JSONArray();
	JSONArray listaIntermediario   = new JSONArray();
	JSONArray listaOperacion       = new JSONArray();
	JSONArray listaDetalleDisp     = new JSONArray();
	JSONArray listaPlanPagos       = new JSONArray();
	JSONArray listaTasaIndicativa  = new JSONArray();
	JSONArray listaTasaConfirmada  = new JSONArray();

	// Obtengo los datos
	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
	Solicitud consultaSol = new Solicitud();
	consultaSol = solCotEsp.consultaCotizacionCalc(icSolicitud);

	// Se arma el grid: Datos de la Cotización
	mapaDatosCotizacion.put("ETIQUETA", "Número de Referencia:");
	mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getNumero_solicitud());
	listaDatosCotizacion.add(mapaDatosCotizacion);
	mapaDatosCotizacion = new HashMap();
	mapaDatosCotizacion.put("ETIQUETA", "Fecha de Cotización:");
	mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getDf_cotizacion());
	listaDatosCotizacion.add(mapaDatosCotizacion);
	mapaDatosCotizacion = new HashMap();
	mapaDatosCotizacion.put("ETIQUETA", "Número de Usuario:");
	mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getIc_usuario());
	listaDatosCotizacion.add(mapaDatosCotizacion);
	mapaDatosCotizacion = new HashMap();
	mapaDatosCotizacion.put("ETIQUETA", "Número de  Recibo:");
	mapaDatosCotizacion.put("DESCRIPCION", (String)consultaSol.getIg_num_referencia());
	listaDatosCotizacion.add(mapaDatosCotizacion);
	// Se arman los mensajes
	mensaje1.append("En caso de que la disposición de los recursos no se lleve a cabo total o parcialmente en la fecha estipulada, ");
	mensaje1.append("Nacional Financiera aplicará el cobro de una comisión sobre los montos no dispuestos, ");
	mensaje1.append("conforme a las políticas de crédito aplicables a este tipo de operaciones.");

	// Se arma el grid: Datos del Ejecutivo
	if(consultaSol.getNombre_ejecutivo() != null && !(consultaSol.getNombre_ejecutivo().trim()).equals("")){
		mapaEjecutivoNafin.put("ETIQUETA", "Nombre del Ejecutivo:");
		mapaEjecutivoNafin.put("DESCRIPCION", (String)consultaSol.getNombre_ejecutivo());
		listaEjecutivoNafin.add(mapaEjecutivoNafin);
		mapaEjecutivoNafin = new HashMap();
		mapaEjecutivoNafin.put("ETIQUETA", "Correo electrónico:");
		mapaEjecutivoNafin.put("DESCRIPCION", (String)consultaSol.getCg_mail());
		listaEjecutivoNafin.add(mapaEjecutivoNafin);
		mapaEjecutivoNafin = new HashMap();
		mapaEjecutivoNafin.put("ETIQUETA", "Teléfono");
		mapaEjecutivoNafin.put("DESCRIPCION", (String)consultaSol.getCg_telefono());
		listaEjecutivoNafin.add(mapaEjecutivoNafin);
		mapaEjecutivoNafin = new HashMap();
		mapaEjecutivoNafin.put("ETIQUETA", "Fax:");
		mapaEjecutivoNafin.put("DESCRIPCION", (String)consultaSol.getCg_fax());
		listaEjecutivoNafin.add(mapaEjecutivoNafin);
	}

	// Se arma el grid: Datos del Intermediario
	mapaIntermediario.put("ETIQUETA", "Nombre del Intermediario:");
	mapaIntermediario.put("DESCRIPCION", (String)consultaSol.getNombre_if_cs());
	listaIntermediario.add(mapaIntermediario);
	mapaIntermediario = new HashMap();
	mapaIntermediario.put("ETIQUETA", "Riesgo del Intermediario:");
	mapaIntermediario.put("DESCRIPCION", (String)consultaSol.getRiesgoIf());
	listaIntermediario.add(mapaIntermediario);
	mapaIntermediario = new HashMap();
	mapaIntermediario.put("ETIQUETA", "Nombre del Acreditado:");
	mapaIntermediario.put("DESCRIPCION", (String)consultaSol.getCg_acreditado());
	listaIntermediario.add(mapaIntermediario);

	// Se arma el grid: Características de la Operación
	mapaOperacion.put("ETIQUETA", "Fecha de la disposición:");
	mapaOperacion.put("DESCRIPCION", (String)consultaSol.getDf_solicitud());
	listaOperacion.add(mapaOperacion);
	mapaOperacion = new HashMap();
	mapaOperacion.put("ETIQUETA", "Plazo del Crédito:");
	mapaOperacion.put("DESCRIPCION", (String)consultaSol.getCg_ut_ppc());
	listaOperacion.add(mapaOperacion);
	mapaOperacion = new HashMap();
	mapaOperacion.put("ETIQUETA", "Monto Operación:");
	mapaOperacion.put("DESCRIPCION", "$" + Comunes.formatoDecimal(consultaSol.getFn_monto(),2));
	listaOperacion.add(mapaOperacion);
	mapaOperacion = new HashMap();
	mapaOperacion.put("ETIQUETA", "Esquema de recuperación del capital:");
	mapaOperacion.put("DESCRIPCION", (String)consultaSol.getEsquema_recup());
	listaOperacion.add(mapaOperacion);
	if((consultaSol.getEsquema_recup()).equalsIgnoreCase("Tradicional")){
		mapaOperacion = new HashMap();
		mapaOperacion.put("ETIQUETA", "Números de pagos de Intereses:");
		mapaOperacion.put("DESCRIPCION", (String)consultaSol.getIg_ppi() + " " + consultaSol.getCg_ut_ppi());
		listaOperacion.add(mapaOperacion);
		mapaOperacion = new HashMap();
		mapaOperacion.put("ETIQUETA", "Números de pagos de Capital:");
		mapaOperacion.put("DESCRIPCION", (String)consultaSol.getIg_ppc() + " " + consultaSol.getCg_ut_ppi());
		listaOperacion.add(mapaOperacion);
	}
	if((consultaSol.getEsquema_recup()).equalsIgnoreCase("Tradicional") || (consultaSol.getEsquema_recup()).equalsIgnoreCase("Tipo Renta")){
		mapaOperacion = new HashMap();
		mapaOperacion.put("ETIQUETA", "Periodos de Gracia:");
		mapaOperacion.put("DESCRIPCION", (String)consultaSol.getIg_periodos_gracia()+" "+consultaSol.getCg_ut_periodos_gracia());
		listaOperacion.add(mapaOperacion);
	}

	// Se arma el grid: Tasa confirmada
	mapaTasaConfirmada.put("ETIQUETA", "<b>TASA CONFIRMADA</b>");
	mapaTasaConfirmada.put("DESCRIPCION", "<b>FECHA DE CONFIRMACION</b>");
	listaTasaConfirmada.add(mapaTasaConfirmada);
	mapaTasaConfirmada = new HashMap();
	mapaTasaConfirmada.put("ETIQUETA", "Tasa para Documentar Crédito:" + Comunes.formatoDecimal(consultaSol.getIg_tasa_md(),2));
	mapaTasaConfirmada.put("DESCRIPCION", (String)consultaSol.getDf_solicitud());
	listaTasaConfirmada.add(mapaTasaConfirmada);

	if(informacion.equals("FORMATO_CALCULADORA")){

		resultado.put("informacion",          informacion);
		resultado.put("mensaje",              mensaje);
		resultado.put("criterio_busqueda",    icSolicitud);
		resultado.put("estatus",              icEstatus);
		resultado.put("listaDatosCotizacion", listaDatosCotizacion);
		resultado.put("listaEjecutivoNafin",  listaEjecutivoNafin);
		resultado.put("listaIntermediario",   listaIntermediario);
		resultado.put("listaOperacion",       listaOperacion);
		resultado.put("listaDetalleDisp",     "");
		resultado.put("listaPlanPagos",       "");
		resultado.put("listaTasaIndicativa",  "");
		resultado.put("listaTasaConfirmada",  listaTasaConfirmada);
		resultado.put("mensaje1",             mensaje1.toString());
		resultado.put("mensaje2",             "");
		resultado.put("mensaje3",             "");
		resultado.put("mensaje4",             "");
		resultado.put("success",              new Boolean(success));
		infoRegresar = resultado.toString();

	} else if(informacion.equals("PDF_CALCULADORA")){

		CreditosConfirmadosArchivos paginador = new CreditosConfirmadosArchivos();
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		paginador.setMensaje1(mensaje1.toString());
		paginador.setMensaje2("");
		paginador.setMensaje3("");
		paginador.setMensaje4("");
		paginador.setListaDatosCotizacion(listaDatosCotizacion);
		paginador.setListaEjecutivoNafin(listaEjecutivoNafin);
		paginador.setListaIntermediario(listaIntermediario);
		paginador.setListaOperacion(listaOperacion);
		paginador.setListaDetalleDisp(listaDetalleDisp);
		paginador.setListaPlanPagos(listaPlanPagos);
		paginador.setListaTasaIndicativa(listaTasaIndicativa);
		paginador.setListaTasaConfirmada(listaTasaConfirmada);

		try{
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Exception e) {
			success = false;
			throw new AppException("Error al generar el archivo PDF", e);
		}
		resultado.put("success", new Boolean(success));
		infoRegresar = resultado.toString();

	}

}

%>
<%=infoRegresar%>