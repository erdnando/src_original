<!DOCTYPE html>
<%@ page import="java.util.*,com.netro.cotizador.*," contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/22cotizador/22secsession.jspf" %>

<%
SolCotEsp solCotEsp = netropology.utilerias.ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
boolean bandera;
	
		if(!solCotEsp.mostrarInstrucciones(iNoUsuario,iNoCliente,strTipoUsuario))
			bandera=true;
		else 
			bandera=false;	
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<link rel="stylesheet" type="text/css" href="<%=appWebContextRoot%>/00utils/extjs/ux/FileUploadField.css" />
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/FileUploadField.js"></script>
<script type="text/javascript" src="CapturaSolicitudext.js?<%=session.getId()%>"></script>

</head>
<%@ include file="/01principal/menu.jspf"%>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01if/cabeza.jspf"%>
                <div id="_menuApp"></div>
                <div id="Contcentral">
                               <%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
                               <div id="areaContenido"><div style="height:230px"></div></div>
                </div>
                </div>
                <%@ include file="/01principal/01if/pie.jspf"%>
                <form id='formAux' name="formAux" target='_new'></form>
					 <form id='formParametros' name="formParametros">
						<input type="hidden" id="bandera" name="bandera" value="<%=bandera%>"/>	
						
						
						<input type="hidden" id="ic_solicitud" name="ic_solicitud" value="<%=(request.getParameter("ic_solicitud")    != null) ?   request.getParameter("ic_solicitud") :""%>"/>
						
						<input type="hidden" id="fn_monto" name="fn_monto" value="<%=(request.getParameter("fn_monto")    != null) ?   request.getParameter("fn_monto") :""%>"/>	
						<input type="hidden" id="ic_ejecutivo" name="ic_ejecutivo" value="<%=(request.getParameter("ic_ejecutivo")    != null) ?   request.getParameter("ic_ejecutivo") :""%>"/>	
						<input type="hidden" id="nombre_ejecutivo" name="nombre_ejecutivo" value="<%=(request.getParameter("nombre_ejecutivo")    != null) ?   request.getParameter("nombre_ejecutivo") :""%>"/>	
						<input type="hidden" id="df_solicitud" name="df_solicitud" value="<%=(request.getParameter("df_solicitud")    != null) ?   request.getParameter("df_solicitud") :""%>"/>	
						<input type="hidden" id="cg_programa" name="cg_programa" value="<%=(request.getParameter("cg_programa")    != null) ?   request.getParameter("cg_programa") :""%>"/>	
						<input type="hidden" id="ic_moneda" name="ic_moneda" value="<%=(request.getParameter("ic_moneda")    != null) ?   request.getParameter("ic_moneda") :""%>"/>	
						<input type="hidden" id="ic_tipo_tasa" name="ic_tipo_tasa" value="<%=(request.getParameter("ic_tipo_tasa")    != null) ?   request.getParameter("ic_tipo_tasa") :""%>"/>	
						<input type="hidden" id="in_num_disp" name="in_num_disp" value="<%=(request.getParameter("in_num_disp")    != null) ?   request.getParameter("in_num_disp") :""%>"/>	
						<input type="hidden" id="ig_tipo_piso" name="ig_tipo_piso" value="<%=(request.getParameter("ig_tipo_piso")    != null) ?   request.getParameter("ig_tipo_piso") :""%>"/>	
						<input type="hidden" id="ig_plazo_max_oper" name="ig_plazo_max_oper" value="<%=(request.getParameter("ig_plazo_max_oper")    != null) ?   request.getParameter("ig_plazo_max_oper") :""%>"/>	
						<input type="hidden" id="ig_num_max_disp" name="ig_num_max_disp" value="<%=(request.getParameter("ig_num_max_disp")    != null) ?   request.getParameter("ig_num_max_disp") :""%>"/>	
						<input type="hidden" id="ig_plazo" name="ig_plazo" value="<%=(request.getParameter("ig_plazo")    != null) ?   request.getParameter("ig_plazo") :""%>"/>	
						<input type="hidden" id="cs_tasa_unica" name="cs_tasa_unica" value="<%=(request.getParameter("cs_tasa_unica")    != null) ?   request.getParameter("cs_tasa_unica") :""%>"/>	
						<input type="hidden" id="ic_esquema_recup" name="ic_esquema_recup" value="<%=(request.getParameter("ic_esquema_recup")    != null) ?   request.getParameter("ic_esquema_recup") :""%>"/>	
						<input type="hidden" id="ig_ppi" name="ig_ppi" value="<%=(request.getParameter("ig_ppi")    != null) ?   request.getParameter("ig_ppi") :""%>"/>	
						<input type="hidden" id="cg_ut_ppi" name="cg_ut_ppi" value="<%=(request.getParameter("cg_ut_ppi")    != null) ?   request.getParameter("cg_ut_ppi") :""%>"/>	
						<input type="hidden" id="in_dia_pago" name="in_dia_pago" value="<%=(request.getParameter("in_dia_pago")    != null) ?   request.getParameter("in_dia_pago") :""%>"/>	
						<input type="hidden" id="ig_ppc" name="ig_ppc" value="<%=(request.getParameter("ig_ppc")    != null) ?   request.getParameter("ig_ppc") :""%>"/>	
						<input type="hidden" id="cg_ut_ppc" name="cg_ut_ppc" value="<%=(request.getParameter("cg_ut_ppc")    != null) ?   request.getParameter("cg_ut_ppc") :""%>"/>	
						<input type="hidden" id="ig_periodos_gracia" name="ig_periodos_gracia" value="<%=(request.getParameter("ig_periodos_gracia")    != null) ?   request.getParameter("ig_periodos_gracia") :""%>"/>	
						<input type="hidden" id="cg_ut_periodos_gracia" name="cg_ut_periodos_gracia" value="<%=(request.getParameter("cg_ut_periodos_gracia")    != null) ?   request.getParameter("cg_ut_periodos_gracia") :""%>"/>	
						<input type="hidden" id="cg_razon_social_if" name="cg_razon_social_if" value="<%=(request.getParameter("cg_razon_social_if")    != null) ?   request.getParameter("cg_razon_social_if") :""%>"/>	
						<input type="hidden" id="ig_riesgo_if" name="ig_riesgo_if" value="<%=(request.getParameter("ig_riesgo_if")    != null) ?   request.getParameter("ig_riesgo_if") :""%>"/>	
						<input type="hidden" id="cg_acreditado" name="cg_acreditado" value="<%=(request.getParameter("cg_acreditado")    != null) ?   request.getParameter("cg_acreditado") :""%>"/>	
						<input type="hidden" id="ig_riesgo_ac" name="ig_riesgo_ac" value="<%=(request.getParameter("ig_riesgo_ac")    != null) ?   request.getParameter("ig_riesgo_ac") :""%>"/>	
						<input type="hidden" id="diasIrreg" name="diasIrreg" value="<%=(request.getParameter("diasIrreg")    != null) ?   request.getParameter("diasIrreg") :""%>"/>	
						<input type="hidden" id="fn_monto_disp" name="fn_monto_disp" value="<%=(request.getParameter("fn_monto_disp")    != null) ?   request.getParameter("fn_monto_disp") :""%>"/>
						<input type="hidden" id="df_disposicion" name="df_disposicion" value="<%=(request.getParameter("df_disposicion")    != null) ?   request.getParameter("df_disposicion") :""%>"/>
						<input type="hidden" id="df_vencimiento" name="df_vencimiento" value="<%=(request.getParameter("df_vencimiento")    != null) ?   request.getParameter("df_vencimiento") :""%>"/>	

						
					</form>
</body>


</html>