	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONObject,
	net.sf.json.JSONArray,
	com.netro.cotizador.*"
	
	errorPage="/00utils/error_extjs.jsp"%>
	
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
	
	
 
<jsp:useBean id="ParametrosSistema" class="netropology.utilerias.negocio.ParaSistemaBean" scope="page" />
	
	<%
	
	
	
	
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String informacion         =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	String moneda 					=(request.getParameter("moneda")    != null) ?   request.getParameter("moneda") :"";
	String fch						=(request.getParameter("fecha")    != null) ?   request.getParameter("fecha") :"";
	String disp 					=(request.getParameter("numDisp")    != null) ?   request.getParameter("numDisp") :"";

	int disposicion=0;
	if(!disp.equals("")){
		disposicion=Integer.parseInt(disp);
	}
	SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
	Solicitud captura = new Solicitud();
	captura.setIc_moneda(moneda);
	captura.setStrTipoUsuario(strTipoUsuario);
	captura = solCotEsp.cargaDatos(iNoCliente,iNoUsuario,strTipoUsuario,captura);
	
	if(informacion.equals("Instrucciones")){
		solCotEsp.modifInstrucciones(iNoUsuario,iNoCliente,strTipoUsuario,request.getParameter("cs_instrucciones"));
	}else if (informacion.equals("catologoMonedaDist")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();	
	}else  if (informacion.equals("catologoUnidadTiempo")) {
		
		String desde 					=(captura.getCg_ut_ppi()!= null&&!captura.getCg_ut_ppi().equals("")) ?   captura.getCg_ut_ppi() :"0";
		String fechas					=(request.getParameter("fechas")    != null) ?   request.getParameter("fechas") :"";
		String miBand					=(request.getParameter("bandera")    != null) ?   request.getParameter("bandera") :"";
		double dblDesde=Double.parseDouble(desde);
		if(!fechas.equals(""))
			dblDesde=Double.parseDouble(fechas);
		List coleccionElementos = new ArrayList();
      JSONArray jsonArr = new JSONArray();
		if(30>=dblDesde){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
      		elementoCatalogo.setClave("30");
      		elementoCatalogo.setDescripcion("Mensual");
      		coleccionElementos.add(elementoCatalogo);
			}if(60>=dblDesde){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
      		elementoCatalogo.setClave("60");
      		elementoCatalogo.setDescripcion("Bimestral");
      		coleccionElementos.add(elementoCatalogo);			
			}if(90>=dblDesde){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
      		elementoCatalogo.setClave("90");
      		elementoCatalogo.setDescripcion("Trimestral");
      		coleccionElementos.add(elementoCatalogo);
				}
			if(120>=dblDesde){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
      		elementoCatalogo.setClave("120");
      		elementoCatalogo.setDescripcion("Cuartrimestral");
      		coleccionElementos.add(elementoCatalogo);
			}

			if(180>=dblDesde){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
      		elementoCatalogo.setClave("180");
      		elementoCatalogo.setDescripcion("Semestral");
      		coleccionElementos.add(elementoCatalogo);
			}if(360>=dblDesde){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
      		elementoCatalogo.setClave("360");
      		elementoCatalogo.setDescripcion("Anual");
      		coleccionElementos.add(elementoCatalogo);
			}
			if(!miBand.equals("")){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
      		elementoCatalogo.setClave("0");
      		elementoCatalogo.setDescripcion("Al vencimiento");
      		coleccionElementos.add(elementoCatalogo);
			}
			
				Iterator it = coleccionElementos.iterator();
				while(it.hasNext()) {
      		Object obj = it.next();
      		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
      			ElementoCatalogo ec = (ElementoCatalogo)obj;
      			jsonArr.add(JSONObject.fromObject(ec));
      		}
      	}
      	infoRegresar = "{\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString()+"}";	
	}else if (informacion.equals("CatalogoTasaInteres")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_tipo_tasa");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setTabla("cotcat_tipo_tasa");
		cat.setOrden("1");
		cat.setValoresCondicionIn(captura.getTasasIntReq(), Integer.class);
		infoRegresar = cat.getJSONElementos();
	}else if (informacion.equals("CatalogoEsquema")){
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_esquema_recup");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setTabla("cotcat_esquema_recup");
		cat.setOrden("1");
		cat.setValoresCondicionIn(captura.getEsquemasRecup("F"), Integer.class);
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("valoresIniciales")){
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("nombreEjecutivo", captura.getNombre_ejecutivo());
		jsonObj.put("fechaSolic", captura.getDf_solicitud());
		jsonObj.put("strNombre",strNombre);
		jsonObj.put("disposiciones",captura.getIn_num_disp());
		jsonObj.put("maxDisp",captura.getIg_num_max_disp()+"");
		jsonObj.put("plazoMax",captura.getIg_plazo_max_oper()+"");
		infoRegresar = jsonObj.toString();
	}else if(informacion.equals("validaFecha")){
		String DiasInhabiles = captura.getDias_inhabiles();
		String SigFechaDispHabilPermitida = captura.getSig_Fecha_Disp_Habil_Perm();
		
			String dia=fch.substring(0,2);
			String mes=fch.substring(3,5);
			String anio=fch.substring(6,10);
			Calendar calendar = Calendar.getInstance();
			calendar.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, Integer.parseInt(dia));    
			java.util.Date fecha = calendar.getTime();
			//new java.util.Date(Integer.parseInt(anio)-1900, Integer.parseInt(mes)-1, Integer.parseInt(dia));
			
			String fecha2 = SigFechaDispHabilPermitida;
			
			String dia2=fecha2.substring(0,2);
			String mes2=fecha2.substring(3,5);
			String anio2=fecha2.substring(6,10);
			calendar.set(Integer.parseInt(anio2), Integer.parseInt(mes2)-1, Integer.parseInt(dia2));    
			java.util.Date fecha3 = calendar.getTime(); //new java.util.Date(Integer.parseInt(anio2)-1900, Integer.parseInt(mes2)-1, Integer.parseInt(dia2));
			if(!fch.equals("")){
					jsonObj.put("success", new Boolean(true));
					if(fecha.before( fecha3)){
						jsonObj.put("mensaje","La fecha de disposición debe ser mayor  a los días hábiles permitidos para la solicitud de recursos");
						jsonObj.put("success", new Boolean(false));					
					}
					if(fecha.getDay()==0 || fecha.getDay()==6) // regresa 0(domingo) al 6(sábado).
					{
						jsonObj.put("mensaje","La fecha capturada es un dia inhabil");
						jsonObj.put("success", new Boolean(false));	
					}
					else if( DiasInhabiles.indexOf(dia+"/"+mes) != -1)
					{
						jsonObj.put("mensaje","La fecha capturada es un dia inhabil");
						jsonObj.put("success", new Boolean(false));	
					}
			}
			infoRegresar = jsonObj.toString();
	}else if(informacion.equals("esquemas")){
				String fn_monto_disp			=(request.getParameter("fn_monto_disp")    != null) ?   request.getParameter("fn_monto_disp") :"";
				String df_disposicion			=(request.getParameter("df_disposicion")    != null) ?   request.getParameter("df_disposicion") :"";
				String df_vencimiento			=(request.getParameter("df_vencimiento")    != null) ?   request.getParameter("df_vencimiento") :"";
				
				String[] fn_montoDisp= new String[disposicion];
				String[] dfDisposicion= new String[disposicion];
				String[] dfVencimiento= new String[disposicion];
				StringTokenizer tokens1 = new StringTokenizer(fn_monto_disp,"|");
				StringTokenizer tokens2 = new StringTokenizer(df_disposicion,"|");
				StringTokenizer tokens3 = new StringTokenizer(df_vencimiento,"|");
				for(int i =1;i<=disposicion;i++){
					fn_montoDisp[i-1]=tokens1.nextToken();
					dfDisposicion[i-1]=tokens2.nextToken();
					dfVencimiento[i-1]=tokens3.nextToken();
				}
				captura.setFn_monto_disp(fn_montoDisp);
				captura.setDf_disposicion(dfDisposicion);
				captura.setDf_vencimiento(dfVencimiento);
				
				
				jsonObj.put("igPlazo",captura.getIg_plazo());
				jsonObj.put("success", new Boolean(false));
				infoRegresar = jsonObj.toString();
	}else if(informacion.equals("cotizarAhora")){
		boolean band=true;
		String interpolacion = "";
		String calculo = "";
		String cg_razon_social_if = "";
		String fn_monto_disp			=(request.getParameter("fn_monto_disp")    != null) ?   request.getParameter("fn_monto_disp") :"";
		String[] fn_montoDisp= new String[disposicion];
		captura.setIg_plazo(request.getParameter("plazo"));
		StringTokenizer tokens1 = new StringTokenizer(fn_monto_disp,"|");
		captura.setIc_tipo_tasa(request.getParameter("HtipoTasa"));
		captura.setIg_plazo_max_oper(request.getParameter("plazoMax"));
		for(int i =1;i<=disposicion;i++)
					fn_montoDisp[i-1]=tokens1.nextToken();
		try{
		if(fn_montoDisp!=null){//if request.getParameter("fn_monto_disp")!=null
			if(ParametrosSistema.load()){//if ParametrosSistema.load()
				String hora_sistema  = new java.text.SimpleDateFormat("HH").format(new java.util.Date());
				String minutos_sistema  = new java.text.SimpleDateFormat("mm").format(new java.util.Date());
				double 	dMontoLimiteExternos 		= 0d;
				double 	dMontoLimiteExternosDl 		= 0d;
				double  dMontoMaxLinea				= 0d;
				double dMontoMaxLineaDl				= 0d;
				int 	iPlazoLimiteExternos 		= 0;
				int 	iPlazoLimiteExternosDl 		= 0;
				boolean bolEstatus 					= false;
				boolean bolEstatusDl 				= false;
				int 	iHoraInicioConsultaHrs 		= 0;
				int 	iHoraInicioConsultaMin 		= 0;
				int 	iHoraTerminoConsultaHrs 	= 0;
				int 	iHoraTerminoConsultaMin 	= 0;
				int 	ig_plazo_max_cot 			= 0;
				int 	ig_plazo_max_cot_dl 		= 0;
				int 	ig_num_max_disp_cot 		= 0;
				int 	ig_num_max_disp_cot_dl 		= 0;
				double 	ig_monto_max_cot 			= 0;
				double 	ig_monto_max_cot_dl 		= 0;
				int 	ig_plazo_max_cupon 			= 0;
				int 	ig_plazo_max_cupon_dl 		= 0;
				String	cg_tasa_f					= "N";
				String	cg_tasa_f_dl				= "N";
				String	cg_tasa_v					= "N";
				String	cg_tasa_v_dl				= "N";
				String nodisposiciones = "";
				if(ParametrosSistema.loadParametros()){//if ParametrosSistema.loadParametros()
					band = true;
					moneda = captura.getIc_moneda();
					nodisposiciones = captura.getIn_num_disp();
					iHoraInicioConsultaHrs=ParametrosSistema.getHoraInicioConfirmacion().get(ParametrosSistema.getHoraInicioConfirmacion().HOUR_OF_DAY);
					iHoraInicioConsultaMin=ParametrosSistema.getHoraInicioConfirmacion().get(ParametrosSistema.getHoraInicioConfirmacion().MINUTE);
					iHoraTerminoConsultaHrs=ParametrosSistema.getHoraTerminoConfirmacion().get(ParametrosSistema.getHoraTerminoConfirmacion().HOUR_OF_DAY);
					iHoraTerminoConsultaMin=ParametrosSistema.getHoraTerminoConfirmacion().get(ParametrosSistema.getHoraTerminoConfirmacion().MINUTE);
					if(Integer.parseInt(hora_sistema) < iHoraInicioConsultaHrs){
						System.out.println("Error:::horaI");
						band = false;
					}
					if(Integer.parseInt(hora_sistema) > iHoraTerminoConsultaHrs){
						System.out.println("Error:::horaT");
						band = false;
					}
					if(Integer.parseInt(hora_sistema) == iHoraInicioConsultaHrs){
						if(Integer.parseInt(minutos_sistema) < iHoraInicioConsultaMin){
							System.out.println("Error:::minutosI");
							band = false;
						}
					}
					if(Integer.parseInt(hora_sistema) == iHoraTerminoConsultaHrs){
						if(Integer.parseInt(minutos_sistema) > iHoraTerminoConsultaMin){
							System.out.println("Error:::minutosT");
							band = false;
						}
					}
					if(Integer.parseInt(captura.getIn_num_disp()) > 1){
						captura.setDf_vencimiento(request.getParameterValues("df_vencimiento"));
						int numDisp = Integer.parseInt(captura.getIn_num_disp());
						for(int x=0;x<=numDisp-2;x++){
							String fu = captura.getDf_vencimiento(x);
							String fd = captura.getDf_vencimiento(x+1);
							if(fu.compareTo(fd)!= 0){
								System.out.println("Error:::fechas");
								band = false;
							}
						}
						if("3".equals(captura.getIc_esquema_recup()) || "4".equals(captura.getIc_esquema_recup())){
							System.out.println("Error:::fechas:::esquemas");
							band = false;
						}
					}
					if(!"".equals(captura.getIc_esquema_recup())){
						if(!solCotEsp.validaEsquemaLinea(captura.getIc_esquema_recup(),moneda)){
							System.out.println("Error:::validaesquemalinea");
							band = false;
						}
					}
					String icejec = captura.getIc_ejecutivo();
					if(strTipoUsuario.equals("IF")){
						cg_razon_social_if = strNombre;
						if(!solCotEsp.CotizacionLinea(icejec, iNoCliente)){
							System.out.println("Error:::cotizacionlinea");
							band = false;
						}
					}
					if(!solCotEsp.ValidaCurvas(moneda)){
						System.out.println("Error:::curva");
						band = false;
					}
					if(moneda.equals("1")){
						calculo = ParametrosSistema.getCg_metodo_calc();
						interpolacion = ParametrosSistema.getCg_metodo_inter();
						dMontoLimiteExternos = ParametrosSistema.getMontoLimiteExternos();
						dMontoMaxLinea = ParametrosSistema.getIg_monto_max_cot();
						iPlazoLimiteExternos = ParametrosSistema.getPlazoLimiteExternos();
						bolEstatus = ParametrosSistema.getEstatus();
						ig_plazo_max_cot = ParametrosSistema.getIg_plazo_max_cot();
						ig_num_max_disp_cot = ParametrosSistema.getIg_num_max_disp_cot();
						ig_monto_max_cot = ParametrosSistema.getIg_monto_max_cot();
						ig_plazo_max_cupon = ParametrosSistema.getIg_plazo_max_cupon();
						cg_tasa_f = ParametrosSistema.getCg_tasa_f();
						cg_tasa_v = ParametrosSistema.getCg_tasa_v();
						if(dMontoLimiteExternos < new Double(captura.getFn_monto()).doubleValue()){
							System.out.println("Error:::montolimiteexterno");
							band = false;
						}
						if(dMontoMaxLinea < new Double(captura.getFn_monto()).doubleValue()){
							System.out.println("Error:::montomaxlinea");
							band = false;
						}
						if(iPlazoLimiteExternos < Integer.parseInt(captura.getIg_plazo())){
							System.out.println("Error:::plazolimiteexterno");
							band = false;
						}
						if(bolEstatus){
							System.out.println("Error:::estatus");
							band = false;
						}
						if(ig_plazo_max_cot < Integer.parseInt(captura.getIg_plazo())){
							System.out.println("Error:::plazomax");
							band = false;
						}
						if(ig_num_max_disp_cot < Integer.parseInt(captura.getIn_num_disp())){
							System.out.println("Error:::numeromaxcotlinea");
							band = false;
						}
						if(!"1".equals(captura.getIc_tipo_tasa())){
							System.out.println("Error:::tipotasa");
							band = false;
						}else{
							if(cg_tasa_f.equals("N")){
								System.out.println("Error:::tasa");
								band = false;
							}
						}
						if(!"".equals(captura.getIc_esquema_recup())){
							if("1".equals(captura.getIc_esquema_recup())){
								if(ig_plazo_max_cupon < Integer.parseInt(captura.getIg_plazo())){
									System.out.println("Error:::plazocupon");
									band = false;
								}
							}
						}
					}else{
						calculo = ParametrosSistema.getCg_metodo_calc_dl();
						interpolacion = ParametrosSistema.getCg_metodo_inter_dl();
						dMontoLimiteExternosDl = ParametrosSistema.getMontoLimiteExternosDl();
						dMontoMaxLineaDl = ParametrosSistema.getIg_monto_max_cot_dl();
						iPlazoLimiteExternosDl = ParametrosSistema.getPlazoLimiteExternosDl();
						bolEstatusDl = ParametrosSistema.getEstatusDl();
						ig_plazo_max_cot_dl = ParametrosSistema.getIg_plazo_max_cot_dl();
						ig_num_max_disp_cot_dl = ParametrosSistema.getIg_num_max_disp_cot_dl();
						ig_monto_max_cot_dl = ParametrosSistema.getIg_monto_max_cot_dl();
						ig_plazo_max_cupon_dl = ParametrosSistema.getIg_plazo_max_cupon_dl();
						cg_tasa_f_dl = ParametrosSistema.getCg_tasa_f_dl();
						cg_tasa_v_dl = ParametrosSistema.getCg_tasa_v_dl();
						if(dMontoLimiteExternosDl < new Double(captura.getFn_monto()).doubleValue()){
							System.out.println("Error:::montolimiteexterno");
							band = false;
						}
						if(dMontoMaxLineaDl < new Double(captura.getFn_monto()).doubleValue()){
							System.out.println("Error:::montomax");
							band = false;
						}
						if(iPlazoLimiteExternosDl < Integer.parseInt(captura.getIg_plazo())){
							System.out.println("Error:::plazolimiteexterno");
							band = false;
						}
						if(bolEstatusDl){
							System.out.println("Error:::estatus");
							band = false;
						}
						if(ig_plazo_max_cot_dl < Integer.parseInt(captura.getIg_plazo())){
							System.out.println("Error:::plazomaxcotlinea");
							band = false;
						}
						if(ig_num_max_disp_cot_dl < Integer.parseInt(captura.getIn_num_disp())){
							System.out.println("Error:::numeromax");
							band = false;
						}
						if("2".equals(captura.getIc_tipo_tasa())){
							if(!solCotEsp.ExisteSWAP()){
								System.out.println("Error:::existeswap");
								band = false;
							}
							if(cg_tasa_v_dl.equals("N")){
								System.out.println("Error:::tasa");
								band = false;
							}
						}else{
							if(cg_tasa_f_dl.equals("N")){
								System.out.println("Error:::tasaa");
								band = false;
							}
						}
						if(!"".equals(captura.getIc_esquema_recup())){
							if("1".equals(captura.getIc_esquema_recup())){
								if(ig_plazo_max_cupon_dl < Integer.parseInt(captura.getIg_plazo())){
									System.out.println("Error:::plazomaxcupon");
									band = false;
								}
							}
						}
					}
				}//fin if ParametrosSistema.loadParametros()
			}//fin if ParametrosSistema.load()
		}//fin if request.getParameter("fn_monto_disp")!=null
	}catch(Exception e){
		e.printStackTrace();
	}finally {
  		ParametrosSistema.release();
		jsonObj.put("success", new Boolean(band));
		infoRegresar = jsonObj.toString();
	}
	
	}else if(informacion.equals("validaCargaMasiva")||informacion.equals("insertaDatos")){
				String fn_monto_disp			=(request.getParameter("fn_monto_disp")    != null) ?   request.getParameter("fn_monto_disp") :"";
				String df_disposicion			=(request.getParameter("df_disposicion")    != null) ?   request.getParameter("df_disposicion") :"";
				String df_vencimiento			=(request.getParameter("df_vencimiento")    != null) ?   request.getParameter("df_vencimiento") :"";
				String df_pagos 						=(request.getParameterValues("df_pago") !=null)? request.getParameter("df_pago"):"";
				int numPagos							=((request.getParameterValues("numPagos") !=null)&&!(request.getParameterValues("numPagos").equals("")))? Integer.parseInt(request.getParameter("numPagos")):0;
				String montoPagos						=(request.getParameterValues("montoPagos") !=null)? request.getParameter("montoPagos"):"";
				String idPagos						=(request.getParameterValues("idPagos") !=null)? request.getParameter("idPagos"):"";
				String banderaCargaMasiva 	=(request.getParameterValues("banderaCargaMasiva") !=null)? request.getParameter("banderaCargaMasiva"):"";
				
				
				
				
				String[] dfPagos= new String[numPagos];
				String[] fn_monto_pago=new String[numPagos];
				String[] fn_montoDisp= new String[disposicion];
				String[] dfDisposicion= new String[disposicion];
				String[] dfVencimiento= new String[disposicion];
				
				
				StringTokenizer tokens1 = new StringTokenizer(fn_monto_disp,"|");
				StringTokenizer tokens2 = new StringTokenizer(df_disposicion,"|");
				StringTokenizer tokens3 = new StringTokenizer(df_vencimiento,"|");
				StringTokenizer tokens4 = new StringTokenizer(df_pagos,"|");
				StringTokenizer tokens5 = new StringTokenizer(montoPagos,"|");
				
				
				if(!banderaCargaMasiva.equals("true")){
					for(int i=1;i<=numPagos;i++){
						dfPagos[i-1]=tokens4.nextToken();
						fn_monto_pago[i-1]=tokens5.nextToken();
					}
				}
				for(int i =1;i<=disposicion;i++){
					fn_montoDisp[i-1]=tokens1.nextToken();
					dfDisposicion[i-1]=tokens2.nextToken();
					dfVencimiento[i-1]=tokens3.nextToken();
				}
				captura.setFn_monto_disp(fn_montoDisp);
				captura.setDf_disposicion(dfDisposicion);
				captura.setDf_vencimiento(dfVencimiento);
				
				
	
		
		//String cot_en_linea = (request.getParameter("cot_en_linea")==null)?"":request.getParameter("cot_en_linea");
		if(!banderaCargaMasiva.equals("true")){
				if(numPagos>0){
				List diasPagarInt = new ArrayList();
				List diasPagarCap = new ArrayList();
				diasPagarInt = solCotEsp.getDiasAPagar(captura);
				diasPagarCap = solCotEsp.getDiasPagoPP(captura,dfPagos);
				int cuenta = 0;
				
				for(int a=0;a<diasPagarInt.size();a++){
					for(int b=0;b<diasPagarCap.size();b++){
						if(diasPagarInt.get(a).toString().compareTo(diasPagarCap.get(b).toString()) == 0){
							cuenta++;
						}
					}
				}
			
			if(cuenta != diasPagarCap.size()){
				System.out.println("No Aceptado");
				jsonObj.put("success", new Boolean(false));
			}else{
				//request.setAttribute("captura",captura);
				jsonObj.put("success", new Boolean(true));
			
				}
				infoRegresar = jsonObj.toString();
				}
			}
			if(informacion.equals("insertaDatos")){
			boolean bandera=false;
			jsonObj.put("success", new Boolean(bandera));
				captura.setIc_tipo_tasa(request.getParameter("HtipoTasa"));
				captura.setIc_moneda(request.getParameter("HcbMoneda"));
				captura.setIc_esquema_recup(request.getParameter("HtipoEsquema"));
				captura.setCg_programa(request.getParameter("nomProg"));
				captura.setFn_monto(Double.parseDouble(request.getParameter("monto")));
				captura.setIn_num_disp(request.getParameter("numDisp"));
				captura.setIg_plazo(request.getParameter("plazo"));
				captura.setCs_tasa_unica(request.getParameter("Htasa"));				
				
				captura = solCotEsp.complementaDatos(captura);
				String esquemaRecup = captura.getIc_esquema_recup();
				String tipoTasa =  captura.getTipo_tasa();
				String esquemaPagos =  captura.getEsquema_recup();
				
				
				if(esquemaRecup.equals("1")){
					
				}else if(esquemaRecup.equals("2")){
				String pCapital = request.getParameter("HpCapital");
				captura.setCg_ut_ppi(request.getParameter("HpInteres"));
				//captura.setIg_ppc(request.getParameter("HpCapital"));
				captura.setCg_ut_ppc(request.getParameter("HpCapital"));
				captura.setCg_pfpc(request.getParameter("fechaPagoCap"));
				String fecha2=request.getParameter("fechaPagoCap2");
				captura.setCg_pufpc(fecha2);				
				if(!pCapital.equals("0")){
					captura.setIn_dia_pago(fecha2.substring(0,2));
				}
				}else if(esquemaRecup.equals("3")){
					captura.setCg_ut_ppi(request.getParameter("HpInteres"));
					captura.setIg_ppc(request.getParameter("noPagos"));
					if(!banderaCargaMasiva.equals("true")){
					captura = solCotEsp.guardaPagos(captura,dfPagos,fn_monto_pago);
					}else{
						captura.setIc_proc_pago(Integer.parseInt(idPagos));
					}
				}else if(esquemaRecup.equals("4")){
					captura.setIg_ppc(request.getParameter("HpCapital"));
					captura.setCg_ut_ppc(request.getParameter("HpCapital"));
				}
				//INSERTA
					String nombre_if  = "";
					String cot_en_linea = (request.getParameter("cotizador")==null)?"":request.getParameter("cotizador");
					String cg_pfpc = captura.getCg_pfpc();
					String cg_pufpc = captura.getCg_pufpc();
					String dia_pago = captura.getIn_dia_pago();
					String str = "";
					captura.setIc_usuario(iNoUsuario);
					captura.setNombre_usuario(strNombreUsuario);
				if(!dia_pago.equals(""))
					captura.setIn_dia_pago(dia_pago);
					
				if("S".equals(cot_en_linea)){
					
					String cgutppc = captura.getCg_ut_ppc();
					if(esquemaRecup.equals("2") && !cgutppc.equals("0")){
						Hashtable respuesta = solCotEsp.getValidaFechasPagos(cg_pfpc,cg_pufpc,captura.getCg_ut_ppc());
						int pagosCap = ((Integer)respuesta.get("Npagos")).intValue();
						pagosCap += 2;
						captura.setIg_ppc(String.valueOf(pagosCap));
					}
				
					if("IF".equals(strTipoUsuario)){
						nombre_if = strNombre;
						List lriesgos = solCotEsp.tipoOperRiesgo(iNoCliente);
						captura.setIg_tipo_piso(lriesgos.get(0).toString());
						captura.setIg_riesgo_if(lriesgos.get(1).toString());
						captura.setIg_valor_riesgo_ac(lriesgos.get(1).toString());
					}
					String df_fecha_hoy  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					captura.setDf_cotizacion(df_fecha_hoy);
					String curva = solCotEsp.buscaCurva(captura.getIc_moneda());
					captura.setIdCurva(curva);
					captura = solCotEsp.CalculoTasasUSD(captura,false);
					if(captura.getTasaCreditoUSD() == 0){
						
					}
				}
					captura = solCotEsp.guardaSolicitud(captura);
					bandera=true;
				if("S".equals(cot_en_linea)){
					String cliente = "";
					if("IF".equals(strTipoUsuario)){
						cliente = iNoCliente;
					}else{
						cliente = captura.getIc_ejecutivo();
					}
					String ic = captura.getIc_solic_esp();
					String igtasa = solCotEsp.getIc_tasa(captura.getIc_tipo_tasa(),captura.getIc_moneda(),captura.getIc_esquema_recup(),captura.getIg_plazo(),captura.getCg_ut_ppc(),captura.getCg_ut_ppi());
					solCotEsp.completarDatos(ic,cliente,strTipoUsuario,captura.formatoDecimal(captura.getTasaCreditoUSD()),igtasa);
				}
				
				session.setAttribute("CapturaSolicitud","N");
				
					if("IF".equals(strTipoUsuario)){
						nombre_if = strNombre;
					}else{
						nombre_if = captura.getCg_razon_social_if();
					}
					//Esta parte es para el envio del correo, para avisar que se acaba de guardar una solicitud
					if("IF".equals(strTipoUsuario) && !"S".equals(cot_en_linea)){
						
						Map<String,Object> datos = new HashMap<>();						
						datos.put("strDirectorioTemp", strDirectorioTemp);
						datos.put("strPais", (String)session.getAttribute("strPais"));
						datos.put("iNoNafinElectronico", session.getAttribute("iNoNafinElectronico").toString());
						datos.put("sesExterno", (String)session.getAttribute("sesExterno"));
						datos.put("strNombre", (String)session.getAttribute("strNombre"));
						datos.put("strNombreUsuario", (String)session.getAttribute("strNombreUsuario"));
						datos.put("strLogo", (String)session.getAttribute("strLogo"));
						datos.put("strDirectorioPublicacion", strDirectorioPublicacion);						
						datos.put("strTipoUsuario", strTipoUsuario);
						datos.put("pantalla", "CapSolicitud");
						datos.put("icSolicitud", captura.getIc_solic_esp());
						datos.put("remitente", "cotizador@nafin.com");
						datos.put("asunto", "Solicitud de Cotización");
						
						
						try{							
						    solCotEsp.enviaCorreoCotizador(datos);						
						}catch(Exception e){
						   jsonObj.put("Error", "Hubo un error  al enviar correo ");
						}
						    
						
					}
					jsonObj.put("ic_solicitud",captura.getIc_solic_esp());
					jsonObj.put("success", new Boolean(bandera));
					infoRegresar = jsonObj.toString();
				//FIN INSERTA
				
				
			}
	}else if(informacion.equals("gridPagos")){
				int numPagos							=((request.getParameterValues("numPagos") !=null)&&!(request.getParameterValues("numPagos").equals("")))? Integer.parseInt(request.getParameter("numPagos")):0;
				String ic_solicitud         =(request.getParameter("ic_solicitud")    != null) ?   request.getParameter("ic_solicitud") :"";
				captura = solCotEsp.consultaSolicitud(ic_solicitud);
				List lista=  captura.getPagos();
				
				HashMap	datosMap;
				List reg=new ArrayList();
				int i;
				for(i=1;i<=numPagos;i++){
				String dfPago		= "";
				String montoPago	= "";
				if(lista!=null){
					ArrayList alColumnas = (ArrayList)lista.get(i-1);
					dfPago		= (String)alColumnas.get(1);
					montoPago	= (String)alColumnas.get(2);
				}
				datosMap=new HashMap();
				datosMap.put("NO_PAGO",i+"");
				datosMap.put("FECHA_PAGO",dfPago);
				datosMap.put("MONTO",montoPago);
				reg.add(datosMap);
			}
			JSONObject datos = new JSONObject();
			datos.put("registros", reg);
			datos.put("success",new Boolean(true));
			infoRegresar = datos.toString();
		}
%>

<%=infoRegresar%>