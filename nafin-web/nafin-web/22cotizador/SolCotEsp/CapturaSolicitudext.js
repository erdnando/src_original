Ext.onReady(function(){ 
var banderaInicio;
var banderaAccion=false;
var fechaSolicitud;
var fechaValida;
var plazoMax;
var fn_monto_disp;
var df_disposicion;
var df_vencimiento;
var igPlazo;
var pagos;
var montoPagos='';
var numStore;
var mensajeEsquema=new Array();
var fila;
var idPagos;
var banderaCargaMasiva =false;
var cotizador='';
var banderaRecotiza;
var array;
var ic_solicitud='';
var banderaPagos=true;
var estado=0;
var banderaCargaArchivo=false;
mensajeEsquema[0]='Un solo pago de capital e inter�s al vencimiento';
mensajeEsquema[1]='Pagos iguales de capital e intereses sobre saldo insolutos (incluye el caso particular de un solo pago de capital al vencimiento e intereses peri�dicos)';
mensajeEsquema[2]='Favor de anexar tabla de amortizaci�n incluyendo fecha de pago';
mensajeEsquema[3]='Pago de capital m�s inter�s constantes con capital creciente';




//-------------------------Handlers----------------------------

function replaceAll( text, busca, reemplaza ){ 

   while (text.toString().indexOf(busca) != -1) 

       text = text.toString().replace(busca,reemplaza); 

   return text; 

 }

var procesarInstrucciones = function(){
			ventanaInstrucciones.hide();	
}

var procesaTipoEsquema = function(field){

										catalogoUnidadTiempo2.load({params: {
													bandera: true
												}
												});
										Ext.getCmp('tEsquema').setValue(mensajeEsquema[field.getValue()-1]);
										Ext.getCmp('perInt').setVisible(false);//allowBlank
										Ext.getCmp('perCap').setVisible(false);
										Ext.getCmp('perCapPagos').setVisible(false);
										Ext.getCmp('pInteres').allowBlank=true;//allowBlank
										Ext.getCmp('pCapital').allowBlank=true;
										Ext.getCmp('noPagos').allowBlank=true;
										
										Ext.getCmp('perInt').setValue('');//allowBlank
										Ext.getCmp('perCap').setValue('');
										//Ext.getCmp('perCapPagos').setValue('');
										Ext.getCmp('pInteres').setValue('');//allowBlank
										Ext.getCmp('pCapital').setValue('');
										Ext.getCmp('noPagos').setValue('');
										
										if(field.getValue()==2){
											
											
											
											Ext.getCmp('perInt').setVisible(true);
											Ext.getCmp('perCap').setVisible(true);
											Ext.getCmp('pInteres').allowBlank=false;
											Ext.getCmp('pCapital').allowBlank=false;
										}else if(field.getValue()==3){
											Ext.getCmp('perInt').setVisible(true);
											Ext.getCmp('perCapPagos').setVisible(true);
											Ext.getCmp('pInteres').allowBlank=false;
											Ext.getCmp('noPagos').allowBlank=false;
											Ext.getCmp('perCapPagos').setVisible(true);
										}else if(field.getValue()==4){
											Ext.getCmp('perCap').setVisible(true);
											Ext.getCmp('pCapital').allowBlank=false; 
										}
										
										
}

var procesarGridPagos= function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
					banderaCargaArchivo=true;
					storePago.loadData(Ext.util.JSON.decode(response.responseText).registros);
					banderaCargaArchivo=false;
					storePago.each(function(record){
						montoT+=parseFloat(record.get('MONTO'));
					});
					Ext.getCmp('montoPagos').setValue(Ext.util.Format.number(montoT,'0.00'));


		}else{
					Ext.MessageBox.alert('Mensaje de p�gina web','El sistema esta presentando dificultades tecnicas, pruebe mas tarde');
		}
}

var procesaInserta = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
					Ext.MessageBox.alert('','La solicitud ha sido dada de alta con �xito',function(){
						window.location.href='../22if/22consulta1ext.jsp?idMenu=22CAPSOLIC&ic_solicitud='+ Ext.util.JSON.decode(response.responseText).ic_solicitud;
					});

		}else{
					Ext.MessageBox.alert('Mensaje de p�gina web','El sistema esta presentando dificultades tecnicas, pruebe mas tarde');
		}
}

var procesarValoeresIniciales = function(opts, success, response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		Ext.getCmp('nombreEjecutivo').setValue(Ext.util.JSON.decode(response.responseText).nombreEjecutivo);
		Ext.getCmp('fechaSolic').setValue(Ext.util.JSON.decode(response.responseText).fechaSolic);
		if(!banderaRecotiza)
		Ext.getCmp('numDisp').setValue(Ext.util.JSON.decode(response.responseText).disposiciones);
		fechaSolicitud=Date.parseDate(Ext.util.JSON.decode(response.responseText).fechaSolic,'d/m/Y');
		Ext.getCmp('fechaDisp').setMinValue(fechaSolicitud);
		Ext.getCmp('fechaVenc').setMinValue(fechaSolicitud);		
		Ext.getCmp('dIntermediario').setValue(Ext.util.JSON.decode(response.responseText).strNombre);
		//alert(Ext.util.JSON.decode(response.responseText).maxDisp);
	}
	
	
	
}

var procesarFechaMasiva = function(opts,success,response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		muestraDatos();
		gridPagos2.setVisible(true);
		Ext.getCmp('dPeriodicidadC').setVisible(false);
		panelPagos.setVisible(false);
		panelMuestraDatos.setVisible(true);
		panelMuestraDatos2.setVisible(true);
	}else{
		Ext.MessageBox.alert('Mensaje de p�gina web','El pago de Capital, debe de hacerse en fechas de cobro de Inter�s');
	}
}

var muestraDatos	=	function(){
	
	Ext.getCmp('dEjecutivo').setValue(Ext.getCmp('nombreEjecutivo').getValue());
	Ext.getCmp('dPrograma').setValue(Ext.getCmp('nomProg').getValue());
	Ext.getCmp('dMonto').setValue('$'+Ext.getCmp('monto').getValue()+' '+Ext.getCmp('mon').getValue());
	Ext.getCmp('dTasa').setValue(Ext.getCmp('tipoTasa').lastSelectionText);
	Ext.getCmp('dDisposiciones').setValue(Ext.getCmp('numDisp').getValue());
	var txt;
	if(Ext.getCmp('unica').getValue()){
		txt='Unica';
	}else{
		txt='Varias';
	}
	Ext.getCmp('dTasaInteres').setValue(txt);
	Ext.getCmp('dPlazo').setValue(Ext.getCmp('igPlazo').getValue()+' D�as');
	Ext.getCmp('dCapital').setValue(Ext.getCmp('tipoEsquema').lastSelectionText);
	Ext.getCmp('dPeriodicidadI').setValue(Ext.getCmp('pInteres').lastSelectionText);
	Ext.getCmp('dPeriodicidadC').setValue(Ext.getCmp('pCapital').lastSelectionText);
	Ext.getCmp('dFechaPago').setValue(Ext.util.Format.date(Ext.getCmp('fechaPagoCap').getValue(),'d/m/Y'));
	Ext.getCmp('dFechaPago2').setValue(Ext.util.Format.date(Ext.getCmp('fechaPagoCap2').getValue(),'d/m/Y'));
}

var procesarCotizarAhora= function(opts, success, response){
		
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			Ext.getCmp('btnCotizar').setVisible(true);
		}
	}
//Inicia validaciones fechas
		var Tmes = new Array();
		var sCadena;
		Tmes[1] = "31"; Tmes[2] ="28"; Tmes[3]="31"; Tmes[4]="30"; Tmes[5]="31"; 
		Tmes[6]="30";
		Tmes[7] = "31"; Tmes[8] ="31"; Tmes[9]="30"; Tmes[10]="31";Tmes[11]="30"; 
		Tmes[12]="31";
	
		function fnDesentrama(Cadena){
			var Dato;
			sCadena      = Cadena
			tamano       = sCadena.length;
			posicion     = sCadena.indexOf('/');
			Dato         = sCadena.substr(0,posicion);
			cadenanueva  = sCadena.substring(posicion + 1,tamano);
			sCadena      = cadenanueva;
			return Dato;
		}
	
		function pantallaSolicitud(visible){
			panelSolicCot.setVisible(visible);
			panelOperacion.setVisible(visible);
		
		}
	
	   function calculaDias(sFecha,sFecha2){
	       var iResAno,DiaMes,DiasDif=0,iCont=0,iMes,DiaMesA,iBan=0;
	       var DiasPar,Dato,VencDia,VencMes,AltaMes,AltaDia;
	         	sCadena   = sFecha
	         	AltaDia   = fnDesentrama(sCadena);
	            AltaMes  = fnDesentrama(sCadena);
	            AltaAno   = sCadena;
	            sCadena     = sFecha2
	            VencDia     = fnDesentrama(sCadena);
	            VencMes     = fnDesentrama(sCadena);
	            VencAno     = sCadena;
	            Dato  = AltaMes.substr(0,1);
	            if (Dato ==0){
	               AltaMes = AltaMes.substr(1,2)
	            }
	            Dato  = AltaDia.substr(0,1);
	            if (Dato ==0){
	                      AltaDia = AltaDia.substr(1,2)
	            }
	            Dato  = VencDia.substr(0,1);
	            if (Dato ==0){
	               VencDia = VencDia.substr(1,2)
	            }
	            Dato  = VencMes.substr(0,1);
	            if (Dato ==0){
	              VencMes = VencMes.substr(1,2)
	            }
	            while (VencAno != AltaAno || VencMes != AltaMes || VencDia != AltaDia){
					iCont++;
	                while(AltaMes != VencMes || VencAno != AltaAno){
				      iBan=1;
				       iResAno = AltaAno % 4;
				       if (iResAno==0){
				       Tmes[2] =29;
				       }
				       else{
				       Tmes[2] =28;
				       }
				                         if (iCont == 1){
				         DiaMesA = Tmes[AltaMes];
				         DiasDif = DiaMesA - AltaDia;
				         //alert("DiasDi1"+DiasDif);
				       }
				       else{
				         DiaMesA  = Tmes[AltaMes];
				         DiasDif += parseInt(DiaMesA);
				         //alert("DiasDi2"+DiasDif);
				       }
				       if (AltaMes==12){
				         AltaAno++;
				         AltaMes=1;
				       }else{
				       AltaMes++;
				       }
				
				      iCont++;
					} // fin de while
	        if (AltaMes == VencMes && VencAno == AltaAno){
	        if (iBan==0){
	         DiasDif = parseInt(VencDia) - parseInt(AltaDia);
	         AltaDia = VencDia;
	        }
	        else if (iBan=1){
	            DiasDif += parseInt(VencDia);
	            AltaDia = VencDia;
	         }
	      }
		}
		return DiasDif;
		}



var validaFecha=function(fecha){
				Ext.Ajax.request({
								url: 'CapturaSolicitudext.data.jsp',
								params: Ext.apply({
									informacion: 'validaFecha',
									fecha: Ext.util.Format.date(fecha.value,'d/m/Y'),
									moneda: Ext.getCmp('cbMoneda').getValue(),
									fila: fecha.row,
									col: fecha.column
									
									}),
								callback: procesarFechas
							});
}

var procesarFechas = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			/*
			Ext.Ajax.request({
								url: 'CapturaSolicitudext.data.jsp',
								params: Ext.apply({
									informacion: 'validaFecha',
									fecha: Ext.util.Format.date(fecha.value,'d/m/Y'),
									moneda: Ext.getCmp('cbMoneda').getValue(),
									fila: fecha.row,
									col: fecha.column
									
									}),
								callback: procesarFechas
							});*/
			
		}else{
			Ext.MessageBox.alert('Mensaje de p�gina web',Ext.util.JSON.decode(response.responseText).mensaje,
			function() {
				var myReg=grid.getStore().getAt(opts.params.fila);
				var tmp;
				if(opts.params.col==2){
					tmp='FECHA_DISP';
				}else{
					tmp='FECHA_VENC';
				}
				
				myReg.set(tmp,'');
				grid.startEditing(opts.params.fila, opts.params.col);
			});
			
		}
}

var procesarDia = function(opts, success, response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		plazoMax= Ext.util.JSON.decode(response.responseText).plazoMax;
		
	}
}

var procesarDatos = function(opts, success, response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
		if(Ext.getCmp('numDisp').getValue()>Ext.util.JSON.decode(response.responseText).maxDisp){
			Ext.MessageBox.alert('Mensaje de p�gina web','El N�mero de disposiciones es mayor al par�metro establecido, favor de corregirlo');
		}
		else{
		//Mi otro panel data:	[{	'NO_DISP':'1','MONTO':'','FECHA_DISP':'',	'FECHA_VENC':''}]
			if(!banderaAccion){
					var regi = Ext.data.Record.create(['NO_DISP', 'MONTO','FECHA_DISP','FECHA_VENC']);
					pantallaSolicitud(false);
					panelSolicCot.doLayout();
					var temp=Ext.getCmp('numDisp').getValue();
					
					if(temp>1)
						Ext.getCmp('nota').setVisible(true);
					else
						Ext.getCmp('nota').setVisible(false);
					var numStore=0;
					panelDisposiciones.setVisible(true);
					plazoMax= Ext.util.JSON.decode(response.responseText).plazoMax;
					if(temp==1&&storeDisp.data.length==1){
						storeDisp.removeAll();
						storeDisp.add(	new regi({NO_DISP:'1',MONTO: Ext.getCmp('monto').getValue(),FECHA_DISP: '',FECHA_VENC: ''}) );
						Ext.getCmp('montoT').setValue(Ext.getCmp('monto').getValue());
					}
					while(temp<storeDisp.data.length){
						storeDisp.removeAt(storeDisp.data.length-1);
					}
					
					
					var temp2=storeDisp.data.length;
					
					
					while(temp>temp2){
						if(temp>1){
						
							if(banderaRecotiza)	
							{
							Ext.getCmp('montoT').setValue(Ext.getCmp('monto').getValue());
							}else{
							Ext.getCmp('montoT').setValue('0.00');
							}
						for(var i=0;temp>temp2;temp2++){
							var montoTmp;
							if(banderaRecotiza){
								montoTmp=array[i];
							}else{
								montoTmp='0.00';
							}
							storeDisp.add(	new regi({NO_DISP:(temp2+1),MONTO: montoTmp,FECHA_DISP: '',FECHA_VENC: ''}) );
							numStore++;
						}
					}else{
							numStore++;
							temp2++;
							storeDisp.add(	new regi({NO_DISP:'1',MONTO: Ext.getCmp('monto').getValue(),FECHA_DISP: '',FECHA_VENC: ''}) );
							Ext.getCmp('montoT').setValue(Ext.getCmp('monto').getValue());
						}
						
					}
					
					
					
					/*
					storeDisp.removeAll();
					var regi = Ext.data.Record.create(['NO_DISP', 'MONTO','FECHA_DISP','FECHA_VENC']);
					pantallaSolicitud(false);
					
					var temp=Ext.getCmp('numDisp').getValue();
					if(temp>1)
						Ext.getCmp('nota').setVisible(true);
					else
						Ext.getCmp('nota').setVisible(false);
						
					panelDisposiciones.setVisible(true);
					numStore=0;
					if(temp>1){
						Ext.getCmp('montoT').setValue('0.00');
						for(var i=0;i<temp;i++){
							storeDisp.add(	new regi({NO_DISP:(i+1),MONTO: '0.00',FECHA_DISP: '',FECHA_VENC: ''}) );
							numStore++;
						}
					}else{
							numStore++;
							storeDisp.add(	new regi({NO_DISP:'1',MONTO: Ext.getCmp('monto').getValue(),FECHA_DISP: '',FECHA_VENC: ''}) );
							Ext.getCmp('montoT').setValue(Ext.getCmp('monto').getValue());
					}
					plazoMax= Ext.util.JSON.decode(response.responseText).plazoMax;	
					*/
			}
			else{
				if(storeDisp.data.length!=Ext.getCmp('numDisp').getValue()||Ext.getCmp('monto').getValue()!=Ext.getCmp('montoT').getValue()){
					Ext.getCmp('btnCotizar').setVisible(false);
					pantallaSolicitud(false);
					var temp=Ext.getCmp('numDisp').getValue();
					if(temp>1)
						Ext.getCmp('nota').setVisible(true);
					else
						Ext.getCmp('nota').setVisible(false);
					var numStore=0;
					panelDisposiciones.setVisible(true);
					plazoMax= Ext.util.JSON.decode(response.responseText).plazoMax;
					while(temp<storeDisp.data.length){
						storeDisp.removeAt(storeDisp.data.length-1);
					}
					var regi = Ext.data.Record.create(['NO_DISP', 'MONTO','FECHA_DISP','FECHA_VENC']);
					var temp2=storeDisp.data.length;
					while(temp>temp2){
						if(temp>1){
						Ext.getCmp('montoT').setValue('0.00');
						for(var i=0;temp>temp2;temp2++){
							storeDisp.add(	new regi({NO_DISP:(temp2+1),MONTO: '0.00',FECHA_DISP: '',FECHA_VENC: ''}) );
							numStore++;
						}
					}else{
							numStore++;
							temp2++;
							storeDisp.add(	new regi({NO_DISP:'1',MONTO: Ext.getCmp('monto').getValue(),FECHA_DISP: '',FECHA_VENC: ''}) );
							Ext.getCmp('montoT').setValue(Ext.getCmp('monto').getValue());
						}
						
					}
					 var montoT=0;
					storeDisp.each(function(record){
						montoT+=parseFloat(record.get('MONTO'));
					});
					Ext.getCmp('montoT').setValue(Ext.util.Format.number(montoT,'0.00'));
				/*
					storeDisp.removeAll();
					Ext.getCmp('btnCotizar').setVisible(false);
					var regi = Ext.data.Record.create(['NO_DISP', 'MONTO','FECHA_DISP','FECHA_VENC']);
					var temp=Ext.getCmp('numDisp').getValue();
					pantallaSolicitud(false);
					if(temp>1)
						Ext.getCmp('nota').setVisible(true);
					else
						Ext.getCmp('nota').setVisible(false);
					panelDisposiciones.setVisible(true);
					numStore=0;
					if(temp>1){
						Ext.getCmp('montoT').setValue('0.00');
						for(var i=0;i<temp;i++){
							storeDisp.add(	new regi({NO_DISP:(i+1),MONTO: '0.00',FECHA_DISP: '',FECHA_VENC: ''}) );
							numStore++;
						}
					}else{
							numStore++;
							storeDisp.add(	new regi({NO_DISP:'1',MONTO: Ext.getCmp('monto').getValue(),FECHA_DISP: '',FECHA_VENC: ''}) );
							Ext.getCmp('montoT').setValue(Ext.getCmp('monto').getValue());
					}
					plazoMax= Ext.util.JSON.decode(response.responseText).plazoMax;	
					*/
				}else{
						pantallaSolicitud(false);
						muestraDatos();
					if(Ext.getCmp('tipoEsquema').getValue()==2){
						regi = storeDisp.getAt(0);
						Ext.getCmp('dPeriodicidadC').setVisible(true);
						Ext.getCmp('dPeriodicidadI').setVisible(true);
						
						regi2 = storeDisp.getAt(Ext.getCmp('numDisp').getValue()-1);
						
						if(Ext.getCmp('pCapital').getValue()!='0'){
							Ext.getCmp('fechaPagoCap').setMinValue(regi.get('FECHA_DISP'));
							Ext.getCmp('fechaPagoCap').setMaxValue(regi2.get('FECHA_VENC').add(Date.DAY, -1));	
							Ext.getCmp('fechaPagoCap2').setMinValue(regi.get('FECHA_DISP'));
							Ext.getCmp('fechaPagoCap2').setMaxValue(regi2.get('FECHA_VENC').add(Date.DAY, -1));	
							Ext.getCmp('dFechaPago').setVisible(true);
							Ext.getCmp('dFechaPago2').setVisible(true);
							pantallaFechas(true);
						}else{
							muestraDatos();
							panelMuestraDatos.setVisible(true);	
							panelMuestraDatos2.setVisible(true);
							pantallaFechas(false);
						}
					}else if(Ext.getCmp('tipoEsquema').getValue()==3){
						panelPagos.setVisible(true);
						storePago.removeAll();
						regi = storeDisp.getAt(0);
						Ext.getCmp('dPeriodicidadI').setVisible(true);
						regi2 = storeDisp.getAt(Ext.getCmp('numDisp').getValue()-1);
						Ext.getCmp('fechaPago').setMinValue(regi.get('FECHA_DISP').add(Date.DAY, 5));
						Ext.getCmp('fechaPago').setMaxValue(regi2.get('FECHA_VENC'));	
						var regi = Ext.data.Record.create(['NO_PAGO','FECHA_PAGO','MONTO']);
						var temp=Ext.getCmp('noPagos').getValue();
						if(banderaPagos==true&&banderaRecotiza==true){
							 banderaPagos=false;
							 if(temp==Ext.get('ig_ppc').getValue()){
							 Ext.Ajax.request({
									url: 'CapturaSolicitudext.data.jsp',
								params: Ext.apply({
									informacion: 'gridPagos',
									numPagos: temp,
									ic_solicitud:ic_solicitud
									}),
								callback: procesarGridPagos
							});
							 }
						}else{
							for(var i=0;i<temp;i++){
								storePago.add(	new regi({NO_PAGO:(i+1),FECHA_PAGO: '',MONTO: '0.00'}) );
							}
						}
					}else if(Ext.getCmp('tipoEsquema').getValue()==1){
						Ext.getCmp('dPeriodicidadC').setVisible(false);
						Ext.getCmp('dPeriodicidadI').setVisible(false);
						panelMuestraDatos.setVisible(true);
						panelMuestraDatos2.setVisible(true);
					}else if(Ext.getCmp('tipoEsquema').getValue()==4){
						panelMuestraDatos.setVisible(true);
						panelMuestraDatos2.setVisible(true);
						Ext.getCmp('dPeriodicidadI').setVisible(false);
						Ext.getCmp('dPeriodicidadC').setVisible(true);
					}
				}
			}
		}
	}
}
//-----------------Stores-----------------------------------------
var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catologoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : 'CapturaSolicitudext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo,
		 load:{ fn: function(){
		 if(banderaRecotiza){
			Ext.getCmp('cbMoneda').setValue(Ext.get('ic_moneda').getValue());
				var field=Ext.getCmp('monto');
				field.setValue(Ext.util.Format.number(replaceAll(field.getValue(),',',''), '00.00'));
				if(Ext.getCmp('monto').getValue()>0){
					Ext.Ajax.request({
					url: 'CapturaSolicitudext.data.jsp',
					params: Ext.apply({
						informacion: 'valoresIniciales',
						moneda: Ext.getCmp('cbMoneda').getValue()
						
						}),
					callback: procesarDatos
				});
			}else{
						Ext.MessageBox.alert('Mensaje de p�gina web','Error en el monto total requerido');
			}
			catalogoTasaInteres.load({
					params: {
						moneda: Ext.getCmp('cbMoneda').getValue()
					}
			});
		 }
		 }}
		}
  });

var catalogoTasaInteres = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : 'CapturaSolicitudext.data.jsp',
		baseParams: {	informacion: 'CatalogoTasaInteres'},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo,
		load:{ fn: function(){
		if(banderaRecotiza){
			Ext.getCmp('tipoTasa').setValue(Ext.get('ic_tipo_tasa').getValue());
			}
		}}
		}
	});

var catalogoEsquema = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : 'CapturaSolicitudext.data.jsp',
		baseParams: {	informacion: 'CatalogoEsquema'},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo,
		load: {fn:function(){
						if(banderaRecotiza){
							Ext.getCmp('tipoEsquema').setValue(Ext.get('ic_esquema_recup').getValue());
							procesaTipoEsquema(Ext.getCmp('tipoEsquema'));
							
							
							
						}
				}
			}
		}
	});
	
var catalogoUnidadTiempo = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : 'CapturaSolicitudext.data.jsp',
		baseParams: {	informacion: 'catologoUnidadTiempo'},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo, 
		load:{ fn:function(){
								if(banderaRecotiza)
									Ext.getCmp('pInteres').setValue(Ext.get('cg_ut_ppi').getValue());
									
				}
			}
		}
	});
var catalogoUnidadTiempo2 = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : 'CapturaSolicitudext.data.jsp',
		baseParams: {	informacion: 'catologoUnidadTiempo'},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo, 
		load:{ fn:function(){
								if(banderaRecotiza){
									Ext.getCmp('pCapital').setValue(Ext.get('cg_ut_ppc').getValue());
									//Ext.getCmp('pInteres').setValue(Ext.get('cg_ut_ppi').getValue());
									}
					}
				}	
			}
	});
	
var storeDisp = new Ext.data.JsonStore({
		fields: [
			{name:'NO_DISP'},{name:'MONTO'},{name:'FECHA_DISP'},{name:'FECHA_VENC'}
			
		],
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
	
var storePago = new Ext.data.JsonStore({
		fields: [
			{name:'NO_PAGO'},{name:'FECHA_PAGO'},{name:'MONTO'}			
		],
		listeners: {exception: NE.util.mostrarDataProxyError,load: function(){
							var montoT=0;
								storePago.each(function(record){
									montoT+=parseFloat(record.get('MONTO'));
								});
								Ext.getCmp('montoPagos').setValue(Ext.util.Format.number(montoT,'0.00'));
							
			
		}}
	});
//-----------------Contenedores------------------------------------
var dt= Date();
//fechaSolicitud

var fecha=Ext.util.Format.date(dt,'d/m/Y');

var grid = new Ext.grid.EditorGridPanel({
		id:'grid',enableColumnMove:false ,store: storeDisp,	clicksToEdit:1,	columLines:true,  hidden:false,
		stripeRows:true,	loadMask:true,	height:300,	width:800,	style:'margin:0 auto;',	frame:true,
		columns: [
			{
				header:'No. Disp',	tooltip:'No. Disp', hideable:false,	dataIndex:'NO_DISP',	sortable:false,	width:170,	align:'center'
			},{
				header:'Monto',	tooltip:'Monto',	dataIndex:'MONTO',	sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				editor: {xtype : 'numberfield', id: 'montos', name:'montos',maxLength:11
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(Ext.util.Format.number(value,'0.00'),metadata,registro);
							}
				
			},{
				header:'Fecha de disposici�n:<br/>dd/mm/aaaa',	tooltip:'Fecha de disposici�n:<br/>dd/mm/aaaa',	dataIndex:'FECHA_DISP',
				sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				editor: {xtype : 'datefield', id: 'fechaDisp', name:'fechaDisp', allowBlank: true, startDay: 0,	width: 100,
								msgTarget: 'side', margins: '0 20 0 0',minValue: fechaSolicitud
							},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);
							}
			},{
				header:'Fecha de Vencimiento:<br/>dd/mm/aaaa',	tooltip:'Fecha de Vencimiento:<br/>dd/mm/aaaa',	dataIndex:'FECHA_VENC',
				sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				editor: {xtype : 'datefield', id: 'fechaVenc', name:'fechaVenc', allowBlank: true, startDay: 0,	width: 100,
								msgTarget: 'side', margins: '0 20 0 0',minValue: fechaSolicitud},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);
							}
			}
		],
		listeners:{ 
			afteredit:{ 
				fn:	function(obj){
							var montoT=0;
							if (obj.field == 'MONTO') {
								storeDisp.each(function(record){
									montoT+=parseFloat(record.get('MONTO'));
								});
								Ext.getCmp('montoT').setValue(Ext.util.Format.number(montoT,'0.00'));
							}
							if (obj.field === 'FECHA_DISP' ) {
								
								if(obj.value!=''){
								validaFecha(obj);
								}
							}
						}
			}
							
		}
		});
var gridPagos = new Ext.grid.EditorGridPanel({
		id:'gridPagos',enableColumnMove:false ,store: storePago,	clicksToEdit:1,	columLines:true,  hidden:false,
		stripeRows:true,	loadMask:true,	height:300,	width:600,	style:'margin:0 auto;',	frame:true,
		columns: [
			{
				header:'No. de Pago',	tooltip:'No. de Pago', hideable:false,	dataIndex:'NO_PAGO',	sortable:false,	width:170,	align:'center'
			},{
				header:'Fecha de Pago:<br/>dd/mm/aaaa',	tooltip:'Fecha de Pago:<br/>dd/mm/aaaa',	dataIndex:'FECHA_PAGO',
				sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				editor: {xtype : 'datefield', id: 'fechaPago', name:'fechaPago', allowBlank: true, startDay: 0,	width: 100,
								msgTarget: 'side', margins: '0 20 0 0',minValue: fechaSolicitud
							},
							renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(banderaCargaArchivo)
								return NE.util.colorCampoEdit(value,metadata,registro);
								else
                        return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);
							}
                        
			},{
				header:'Monto',	tooltip:'Monto',	dataIndex:'MONTO',	sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				editor: {xtype : 'numberfield', id: 'montoPago', name:'montoPago',maxLength:11
				
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return NE.util.colorCampoEdit(Ext.util.Format.number(value,'0.00'),metadata,registro);
							}
				
			}
		],
		listeners:{ 
			afteredit:{ 
				fn:	function(obj){
							var montoT=0;
							if (obj.field == 'MONTO') {
								storePago.each(function(record){
									montoT+=parseFloat(record.get('MONTO'));
								});
								Ext.getCmp('montoPagos').setValue(Ext.util.Format.number(montoT,'0.00'));
							}
							if (obj.field == 'FECHA_PAGO') {
								
																
							}
						}
			}
							
		}
		});
		
var grid2 = new Ext.grid.GridPanel({
		id:'grid2',enableColumnMove:false,align: 'left', view:new Ext.grid.GridView({forceFit:true,markDirty: false}) ,store: storeDisp,	clicksToEdit:1,	columLines:true,  hidden:false,
		stripeRows:true,title: 'Detalle de Disposiciones',	loadMask:true,	height:140,	width:800,frame:true,
		columns: [
			{
				header:'No. Disp',	tooltip:'No. Disp', hideable:false,	dataIndex:'NO_DISP',	sortable:false,	width:170,	align:'center'
			},{
				header:'Monto',	tooltip:'Monto',	dataIndex:'MONTO',	sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				editor: {xtype : 'numberfield', id: 'montos', name:'montos',maxLength:11
				
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return Ext.util.Format.number(value,'$0.00');
							}
				
			},{
				header:'Fecha de disposici�n:<br/>dd/mm/aaaa',	tooltip:'Fecha de disposici�n:<br/>dd/mm/aaaa',	dataIndex:'FECHA_DISP',
				sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return Ext.util.Format.date(value,'d/m/Y');
							}
			},{
				header:'Fecha de Vencimiento:<br/>dd/mm/aaaa',	tooltip:'Fecha de Vencimiento:<br/>dd/mm/aaaa',	dataIndex:'FECHA_VENC',
				sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return Ext.util.Format.date(value,'d/m/Y');
							}
			}
		]
		});
var gridPagos2 = new Ext.grid.GridPanel({
		id:'gridPagos2',enableColumnMove:false ,hidden: true,store: storePago, stripeRows:true,view:new Ext.grid.GridView({forceFit:true,markDirty: false}),title: 'Plan de Pagos Capital',	loadMask:true,	height:150,	width:800,	frame:true,
		columns: [
			{
				header:'No. de Pago',	tooltip:'No. de Pago', hideable:false,	dataIndex:'NO_PAGO',	sortable:false,	width:250,	align:'center'
			},{
				header:'Fecha de Pago:<br/>dd/mm/aaaa',	tooltip:'Fecha de Pago:<br/>dd/mm/aaaa',	dataIndex:'FECHA_PAGO',
				sortable:false,	width:250,	hidden:false, hideable:false,	align:'right',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(banderaCargaArchivo)
								return value;
								else
                        return Ext.util.Format.date(value,'d/m/Y');
							}
				
			},{
				header:'Monto',	tooltip:'Monto',	dataIndex:'MONTO',	sortable:false,	width:250,	hidden:false, hideable:false,	align:'right',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return Ext.util.Format.number(value,'$0.00');
							}
			}
		]
		});
		

var elementosDatosCotizacion=[{
		xtype: 'displayfield',
		name: 'numSolic',
		id: 'numSolic',
		allowBlank: false,
		fieldLabel: 'N�mero de Solicitud',
		value:''
	},{
		xtype: 'displayfield',
		name: 'fSolicitud',
		id: 'fSolicitud',
		allowBlank: false,
		fieldLabel: 'Fecha de Solicitud',
		value:''
	},{
		xtype: 'displayfield',
		name: 'horaSo',
		id: 'horaSo',
		allowBlank: false,
		fieldLabel: 'Hora de Solicitud',
		value:''
	},{
		xtype: 'displayfield',
		name: 'Usuario',
		id: 'Usuario',
		allowBlank: false,
		fieldLabel: 'N�mero y Nombre de usuario',
		value:''
	}
]
var elementosSolicitud = [		
					{
					xtype: 'displayfield',
					fieldLabel: 'Ejecutivo NAFIN',
					id: 'nombreEjecutivo',
					name: 'nombreEjecutivo',
					value: '-'
					}
				
		,
					{
					xtype: 'displayfield',
					fieldLabel: 'Fecha de solicitud',
					id: 'fechaSolic',
					name: 'fechaSolic',
					value: fecha
					}
				]
	

var elementosOperacion=[
	{
		xtype: 'textfield',
		name: 'nomProg',
		id: 'nomProg',
		allowBlank: false,
		anchor:'50%',
		fieldLabel: 'Nombre del Programa y/o Acreditado Final'
	},{
		xtype: 'combo',
		fieldLabel: 'Moneda',
		anchor:'50%',
		displayField: 'descripcion',
		valueField: 'clave',
		triggerAction: 'all',
		typeAhead: true,
		allowBlank: false,
		minChars: 1,
		name:'cbMoneda',
		id: 'cbMoneda',
		mode: 'local',
		forceSelection : true,
		allowBlank: false,
		hiddenName: 'HcbMoneda',
		hidden: false,
		emptyText: 'Seleccionar Moneda',
		store: catalogoMoneda,
		tpl: NE.util.templateMensajeCargaCombo,
		listeners: {
						select: {
							fn: function(combo) {
								catalogoTasaInteres.load({
														params: {
															moneda: combo.getValue()
														}
												});
								if(combo.getValue()==1){
									Ext.getCmp('mon').setValue(' Pesos');
								}else{
									Ext.getCmp('mon').setValue(' D�lares');
								}
							}
						}
				}
	},{
		xtype: 'compositefield',
		//fieldLabel: 'Fecha de solicitud',
		combineErrors: false,
		allowBlank: false,
		msgTarget: 'side',
		items: [
				{
					xtype: 'textfield',
					name: 'monto',
					id: 'monto',
					value: '0.00',
					fieldLabel: 'Monto total requerido',
					anchor:'50%',
					maxLength: 20,
					listeners:{
								change: function(field){  
														field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, '0.00'), '0,0.00'));  
														if(field.getValue()==''){
															field.setValue('0.00');
														}
								}
					}
				},{
					xtype: 'displayfield',
					value: '',
					name: 'mon',
					id: 'mon'
					}
	]
	
	},{
		xtype: 'combo',
		name: 'tipoTasa',
		id:	'tipoTasa',
		fieldLabel: 'Tasa de inter�s requerida',
		emptyText: 'Seleccione Tasa',
		mode: 'local',
		displayField: 'descripcion',
		valueField: 'clave',
		hiddenName : 'HtipoTasa',
		forceSelection : false,
		allowBlank: false,
		triggerAction : 'all',
		typeAhead: true,
		minChars : 1,
		store: catalogoTasaInteres,
		tpl : NE.util.templateMensajeCargaCombo
	},{
		xtype: 'compositefield',
		combineErrors: false,
		allowBlank: false,
		
		msgTarget: 'side',
		items: [
				{
					xtype: 'numberfield',
					name: 'numDisp',
					id: 'numDisp',
					fieldLabel: 'N�mero de Disposiciones',
					allowBlank: false,
					anchor:'50%',
					maxLength: 2,
					listeners:{
								change: function(field){
									if(Ext.getCmp('plazoOp').isVisible()){
										if(field.getValue()>1){
											Ext.getCmp('tTasa').setVisible(true);
											Ext.getCmp('radioGp').setVisible(true);
											Ext.getCmp('dTasaInteres').setVisible(true);
											banderaAccion=false;
										}else{
											Ext.getCmp('tTasa').setVisible(false);
											Ext.getCmp('radioGp').setVisible(false);
											Ext.getCmp('dTasaInteres').setVisible(false);
											banderaAccion=false;
										}
									}
								}
							}
				},{
						xtype: 'button',
						text: 'Ver tabla de disposiciones',
						id: 'teblaDisp',
						
						handler: function(boton, evento) {									
							
							
							
							
							
							pantallaSolicitud(false);
							var temp=Ext.getCmp('numDisp').getValue();
							if(temp>1)
								Ext.getCmp('nota').setVisible(true);
							else
								Ext.getCmp('nota').setVisible(false);
							panelDisposiciones.setVisible(true);
							
							
							
											while(temp<storeDisp.data.length){
													storeDisp.removeAt(storeDisp.data.length-1);
												}
												var regi = Ext.data.Record.create(['NO_DISP', 'MONTO','FECHA_DISP','FECHA_VENC']);
												var temp2=storeDisp.data.length;
												while(temp>temp2){
													if(temp>1){
													Ext.getCmp('montoT').setValue('0.00');
													for(var i=0;temp>temp2;temp2++){
														storeDisp.add(	new regi({NO_DISP:(temp2+1),MONTO: '0.00',FECHA_DISP: '',FECHA_VENC: ''}) );
														numStore++;
													}
												}else{
														numStore++;
														temp2++;
														storeDisp.add(	new regi({NO_DISP:'1',MONTO: Ext.getCmp('monto').getValue(),FECHA_DISP: '',FECHA_VENC: ''}) );
														Ext.getCmp('montoT').setValue(Ext.getCmp('monto').getValue());
													}
												}
													var montoT=0;
													storeDisp.each(function(record){
														montoT+=parseFloat(record.get('MONTO'));
													});
													Ext.getCmp('montoT').setValue(Ext.util.Format.number(montoT,'0.00'));
												
						}
					}
			]
		},{
			xtype: 'displayfield',
			id: 'tTasa',
			name: 'tTasa',
			hidden: true,
			value: 'Para el caso de disposiciones m�ltiples a tasa fija, indique si requiere una tasa de inter�s �nica � si pueden ser distintas para cada una de ella.'
		},{
			xtype:'radiogroup',
			fieldLabel: '',
			id:	'radioGp',
			columns: 2,
			hidden: true,
			items:[
				{boxLabel: 'Unica',id:'unica', name: 'stat', inputValue: 'R', checked: true,
					listeners:{
						check:	function(radio){
										if (radio.checked){
										
										}
									}
					}
				},
				{boxLabel: 'Varias', name: 'stat', inputValue: 'Cl',checked: false,
					listeners:{
						check:	function(radio){
										if (radio.checked){
								
										}
									}
					}
				}
			]
		}
		,{
		xtype: 'compositefield',
		fieldLabel: 'Plazo de la Operaci�n',
		combineErrors: false,
		hidden: true,
		id: 'plazoOp',
		msgTarget: 'side',
		items: [
					{
					xtype: 'displayfield',
					id: 'igPlazo',
					name: 'igPlazo',
					value: '-'
					}
				]
		},{
		xtype: 'compositefield',
		fieldLabel: 'Esquema de recuperaci�n del capital',
		combineErrors: false,
		hidden: true,
		id: 'esquemaRec',
		msgTarget: 'side',
		items: [{
					xtype: 'combo',
					name: 'tipoEsquema',
					id:	'tipoEsquema',
					//fieldLabel: 'Tasa de inter�s requerida',
					emptyText: 'Seleccione',
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					hiddenName : 'HtipoEsquema',
					forceSelection : false,
					//allowBlank: false,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoEsquema,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners:{
								select: function(field){  
									procesaTipoEsquema(field);
								}
							}
				},{
					xtype: 'displayfield',
					id: 'tEsquema',
					name: 'tEsquema',
					value: '-',
					height:50
					}
			]
		},{
		xtype: 'compositefield',
		fieldLabel: 'Periodicidad del pago de Inter�s',
		combineErrors: false,
		hidden: true,
		id: 'perInt',
		msgTarget: 'side',
		items: [
		{
					xtype: 'displayfield',
					value: 'Unidad de Tiempo:'
					
					},
		{
					xtype: 'combo',
					name: 'pInteres',
					id:	'pInteres',
					fieldLabel: 'Unidad de Tiempo',
					emptyText: 'Seleccione',
					mode: 'local',
					anchor:'50%',
					displayField: 'descripcion',
					valueField: 'clave',
					hiddenName : 'HpInteres',
					forceSelection : false,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoUnidadTiempo,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners:{
							select:function(cmb){
								if(Ext.getCmp('tipoEsquema').getValue()==2){
								catalogoUnidadTiempo2.load({params: {
													fechas:cmb.getValue(),
													bandera: true
												}
												});
								}
							}
					}
				}
			]
		},{
		xtype: 'compositefield',
		fieldLabel: 'Periodicidad de capital',
		hidden: true,
		id: 'perCap',
		combineErrors: false,
		msgTarget: 'side',
		items: [
				{
					xtype: 'displayfield',
					value: 'Unidad de Tiempo:'
					
					},
				{
					xtype: 'combo',
					name: 'pCapital',
					id:	'pCapital',
					anchor:'50%',
					fieldLabel: '',
					emptyText: 'Seleccione',
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					hiddenName : 'HpCapital',
					forceSelection : false,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoUnidadTiempo2,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},{
		xtype: 'compositefield',
		fieldLabel: 'Periodicidad de capital',
		hidden: true,
		id: 'perCapPagos',
		combineErrors: false,
		msgTarget: 'side',
		items: [{
					xtype: 'displayfield',
					value: 'No Pagos:'
					
					},{
		
					xtype: 'numberfield',
					name: 'noPagos',
					id: 'noPagos',
					//anchor:'50%',
					maxLength: 4
				}
			]
		}
]

var elementosFechaPago = [
							
								{
								xtype: 'datefield',
								name: 'fechaPagoCap',
								id: 'fechaPagoCap',
								startDay: 0,
								fieldLabel: 'Primer fecha<br/> de pago de Capital',
								width: 100,
								allowBlank: false,
								msgTarget: 'side',
								//vtype: 'rangofecha', 
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							,
								{
								xtype: 'datefield',
								name: 'fechaPagoCap2',
								id: 'fechaPagoCap2',
								allowBlank: false,
								fieldLabel: 'Pen�ltima fecha<br/> de pago de Capital',
								startDay: 0,
								width: 100,
								msgTarget: 'side',
								//vtype: 'rangofecha', 
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
]

var elementosCargaMasiva = [
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			defaults: {
				bodyStyle:'padding:6px'
			},
			items:[
				{
					xtype: 'button',	columnWidth: .05,	autoWidth: true,	autoHeight: true,	iconCls: 'icoAyuda',
					handler: function() {
						ventanaAyuda.show();
					}
				},{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 105,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'fileuploadfield',
							id: 'form_file',
							anchor: '95%',
							allowBlank: false,
							blankText:	'Debe seleccionar una ruta de archivo.',
							emptyText: 'Seleccione un archivo',
							fieldLabel: 'Cargar Archivo de',
							name: 'txtarchivo',
							regex:	/^.+\.([tT][xX][tT])$/,
							regexText:'El archivo debe tener extensi�n .txt',
							buttonText: 'Examinar...'
						}
					],
					buttons: [
						{
							text: 'Aceptar',	iconCls: 'correcto',
							handler: function(boton, evento) {
										var cargaArchivo = Ext.getCmp('form_file');
										Ext.getCmp('botonAceptarCarga').setVisible(false);
										if (!cargaArchivo.isValid()){
											cargaArchivo.focus();
											return;
										}

										panelCargaMasiva.el.mask('Procesando...', 'x-mask-loading');
										panelCargaMasiva.getForm().submit({
											url: 'CargaMasivaExt.data.jsp',
											waitMsg: 'Enviando datos...',
											waitTitle:'Procesando registros',
											success: function(form, action) {
													
													panelCargaMasiva.el.unmask();
													//panelCargaMasiva.hide();
													panelDatosMasiva.setVisible(true);
													if(action.result.datos.errores==''){
														Ext.getCmp('botonAceptarCarga').setVisible(true);
														banderaCargaArchivo=true;
														storePago.loadData(action.result.datos.registros);
														banderaCargaArchivo=false;
													}
													
													var montoT=0;
							
													storePago.each(function(record){
														montoT+=parseFloat(record.get('MONTO'));
													});
													Ext.getCmp('montoPagos').setValue(Ext.util.Format.number(montoT,'0.00'));
							
													panelDatosMasiva.setVisible(true);
													idPagos=action.result.datos.id_pagos;
													//alert(idPagos);
													Ext.getCmp('areaLineas').setValue(action.result.datos.resumen);
													Ext.getCmp('areaErr').setValue(action.result.datos.errores);
												
											},
											failure: NE.util.mostrarSubmitError
										})
							}
						},{
							text: 'Cancelar',	iconCls: 'icoRechazar',
							handler: function() {
											panelCargaMasiva.getForm().reset();
											panelCargaMasiva.setVisible(false);
											panelPagos.setVisible(true);
											panelDatosMasiva.setVisible(false);
										}
						}
					]
				}
			]
		},
		{xtype:'hidden',id:'hidDesde',name:'desde',value:''},
		{xtype:'hidden',id:'hidMoneda',name:'moneda',value:''},
		{xtype:'hidden',id:'hidDf_pago',name:'df_pago',value:''},
		{xtype:'hidden',id:'hidFn_monto_disp',name:'fn_monto_disp',value:''},
		{xtype:'hidden',id:'hidDf_disposicion',name:'df_disposicion',value:''},
		{xtype:'hidden',id:'hidDf_vencimiento',name:'df_vencimiento',value:''},
		{xtype:'hidden',id:'hidNumDisp',name:'numDisp',value:''},
		{xtype:'hidden',id:'hidNumPagos',name:'numPagos',value:''},
		{xtype:'hidden',id:'hidMontoPagos',name:'montoPagos',value:''},
		{xtype:'hidden',id:'hidMonto',name:'monto',value:''}
]
var elementosAreaCargaMasiva=[
//Resumen del proceso:
		{
		xtype: 'compositefield',
		
		combineErrors: false,
		msgTarget: 'side',
		items: [
					{
					xtype: 'displayfield',
					value: 'Resumen del proceso:',
					width: 250
					
					},{
					xtype: 'displayfield',
					value: 'Detalle de pagos con errores:'
					
					}
		]},
		{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
		items: [
				{xtype: 'textarea',
				name: 'areaLineas',
				id: 'areaLineas',
				//allowBlank: false,
				readOnly:true,
				anchor:'100%',
				width: 250,
				height: 300,
				value:''
			},
				{xtype: 'textarea',
				name: 'areaErr',
				readOnly:true,
				id: 'areaErr',
				width: 250,
				height: 300,
				//allowBlank: false,
				anchor:'100%',
				value:''
			}
		]
		}

]

var elementosDespliega =[{
		xtype: 'displayfield',
		name: 'dIntermediario',
		id: 'dIntermediario',
		allowBlank: false,
		//anchor:'70%',
		fieldLabel: 'Intermediario / Acreditado',
		value:''
	},{
		xtype: 'panel',
		allowBlank: false,
		title:'Datos del Ejecutivo Nafin',
		value: ''
	},{
		xtype: 'displayfield',
		name: 'dEjecutivo',
		id: 'dEjecutivo',
		allowBlank: false,
		//anchor:'70%',
		fieldLabel: 'Nombre del Ejecutivo',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dCorreo',
		id: 'dCorreo',
		allowBlank: false,
		anchor:'70%',
		hidden: true,
		fieldLabel: 'Correo Electr�nico',
		value:''
	},{
		xtype: 'displayfield',
		name: 'telefono',
		id: 'telefono',
		allowBlank: false,
		anchor:'70%',
		hidden: true,
		fieldLabel: 'Telefono',
		value:''
	},{
		xtype: 'displayfield',
		name: 'fax',
		id: 'fax',
		allowBlank: false,
		anchor:'70%',
		hidden: true,
		fieldLabel: 'Fax',
		value:''
	},{
		xtype: 'panel',
 		allowBlank: false,
		hidden:true,
		id: 'panelInt',
		title: 'Datos del Intermediario Financiero/Acreditado'
	},{
		xtype: 'displayfield',
		name: 'tipoOperacion',
		id: 'tipoOperacion',
		allowBlank: false,
		anchor:'70%',
		hidden: true,
		fieldLabel: 'Tipo de operaci�n',
		value:''
	},{
		xtype: 'displayfield',
		name: 'Int',
		id: 'Int',
		allowBlank: false,
		anchor:'70%',
		hidden: true,
		fieldLabel: 'Nombre del Intermediario',
		value:''
	},{
		xtype: 'panel',
		allowBlank: false,
		title: 'Caracter�sticas de la Operaci�n'
	},{
		xtype: 'displayfield',
		name: 'dPrograma',
		id: 'dPrograma',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Nombre del programa o acreditado Final',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dMonto',
		id: 'dMonto',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Monto total requerido',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dTasa',
		id: 'dTasa',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Tipo de tasa requerida',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dDisposiciones',
		id: 'dDisposiciones',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'N�mero de Disposiciones',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dTasaInteres',
		id: 'dTasaInteres',
		allowBlank: false,
		anchor:'70%',
		hidden: true,
		fieldLabel: 'Disposiciones m�ltiples. Tasa de Inter�s',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dPlazo',
		id: 'dPlazo',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Plazo de la operaci�n',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dCapital',
		id: 'dCapital',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Esquema de recuperaci�n del capital',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dPeriodicidadI',
		id: 'dPeriodicidadI',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Periodicidad del pago de Inter�s',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dPeriodicidadC',
		id: 'dPeriodicidadC',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Periodicidad de capital',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dFechaPago',
		hidden:true,
		id: 'dFechaPago',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Primer fecha de pago de Capital',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dFechaPago2',
		hidden:true,
		id: 'dFechaPago2',
		allowBlank: false,
		anchor:'70%',
		fieldLabel: 'Pen�ltima fecha de pago de Capital',
		value:''
	}
]

//--------------------Lista de PANEL'S------------------------


	
	var panelCotizador = new Ext.FormPanel({
		id: 'panelCotizador',
		fileUpload: true,
		hidden: true,
		frame: true,
		frame: false,
		align: 'center',
		width: 940,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 4px',
		style: 'margin: 0 auto',
		items: [elementosDatosCotizacion]
		});
		
		
		var panelMuestraDatos2 = new Ext.Panel({
		id: 'panelDatos2',
		hidden: true,
		labelWidth: 250,
		frame: true,
		width: 940,
		bodyStyle: 'padding: 4px',
		items: [
		
			grid2,gridPagos2
			
		],
		buttons:
		[
			{
				text:'Salir',
				id: 'salir',
				hidden: true,
				handler: function() {
						 window.location = 'CapturaSolicitudext.jsp';
					}
			 },			 
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Confirmar',
			  name: 'btnBuscar',
			  id: 'btnBusc',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{
						// Insert de los datos
						if(Ext.getCmp('noPagos').getValue()!=''){
						var intTemp=Ext.getCmp('noPagos').getValue();
						intTemp=parseInt(intTemp);
						}else{
						intTemp=0;
						}
						var tasa='S';
						if(Ext.getCmp('numDisp').getValue()>1){
							if(Ext.getCmp('unica').getValue())
							tasa='S';
							else
							tasa='N';
						}
						
						
						Ext.Ajax.request({
								url: 'CapturaSolicitudext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),panelOperacion.getForm().getValues(),{informacion:'insertaDatos',
								df_pago: pagos,
								fechaPagoCap:Ext.util.Format.date(Ext.getCmp('fechaPagoCap').getValue(),'d/m/Y'),
								fechaPagoCap2:Ext.util.Format.date(Ext.getCmp('fechaPagoCap2').getValue(),'d/m/Y'),
								fn_monto_disp: fn_monto_disp,
								df_disposicion:df_disposicion,
								df_vencimiento:df_vencimiento,
								numDisp:Ext.getCmp('numDisp').getValue(),
								numPagos:intTemp,
								montoPagos:montoPagos,
								plazo:igPlazo,
								cotizador:cotizador,
								Htasa:tasa,
								idPagos:idPagos,
								banderaCargaMasiva:banderaCargaMasiva
								}),
								callback: procesaInserta
								});
						/*
						panelCotizador.setVisible(true);
						Ext.getCmp('dCorreo').setVisible(true);
						Ext.getCmp('telefono').setVisible(true);
						Ext.getCmp('fax').setVisible(true);
						Ext.getCmp('panelInt').setVisible(true);
						Ext.getCmp('tipoOperacion').setVisible(true);
						Ext.getCmp('Int').setVisible(true);
						//Ext.getCmp('btnBusc').setVisible(false);
						//Ext.getCmp('btnReg').setVisible(false);
						Ext.getCmp('btnPDF').setVisible(true);
						Ext.getCmp('salir').setVisible(true);
						Ext.getCmp('toolBar').setVisible(false);*/
					}
			 },{
				text:'Regresar',
				id: 'btnReg',
				handler: function() {
						//Ext.getCmp('dTasaInteres').setVisible(false);
						Ext.getCmp('dFechaPago').setVisible(false);
						Ext.getCmp('dFechaPago2').setVisible(false);
						gridPagos2.setVisible(false);
						pantallaSolicitud(true);
						panelMuestraDatos.setVisible(false);
						panelMuestraDatos2.setVisible(false);
					}
			 }
			 
		]
		
	});
	var panelMuestraDatos = new Ext.FormPanel({
		id: 'panelDatos',
		hidden: true,
		labelWidth: 350,
		frame: true,
		width: 940,
		bodyStyle: 'padding: 4px',
		items: [elementosDespliega
		]
		
	});
	var panelCargaMasiva = new Ext.FormPanel({
		id: 'formaCarga',
		fileUpload: true,
		hidden: true,
		frame: true,
		width: 940,
		title: 'Carga masiva de tabla de amortizaci�n',
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 4px',
		style: 'margin: 0 auto',
		items: [elementosCargaMasiva]
	});
	
	var panelDatosMasiva = new Ext.FormPanel({
		id: 'formDatosMasiva',
		fileUpload: true,
		hidden: true,
		frame: true,
		width: 940,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 4px',
		style: 'margin: 0 auto',
		items: [elementosAreaCargaMasiva],
		buttons: [
						{
							text: 'Aceptar',
							id:'botonAceptarCarga',
							hidden: true,
							handler: function(boton, evento) {
								
								muestraDatos();
								gridPagos2.setVisible(true);
								panelCargaMasiva.setVisible(false);
								panelDatosMasiva.setVisible(false);
								banderaCargaMasiva=true;
								Ext.getCmp('dPeriodicidadC').setVisible(false);
								panelPagos.setVisible(false);
								panelMuestraDatos.setVisible(true);
								panelMuestraDatos2.setVisible(true);
							}
							},
							{
							text: 'Cancelar',
							handler: function(boton, evento) {
									panelCargaMasiva.getForm().reset();
									panelCargaMasiva.setVisible(false);
									pantallaSolicitud(true);
									panelDatosMasiva.setVisible(false);
							}
							}
		]
	});


var panelPagos = new Ext.FormPanel({  
		hidden: true,
		frame: true,
		title: 'Tabla de Amortizaci�n/Esquema: Plan de Pagos',
		bodyStyle:'padding: 10px',
		items: [gridPagos,NE.util.getEspaciador(30),{
					xtype: 'textfield',
					name: 'montoPagos',
					id: 'montoPagos',
					value: '0.00',
					fieldLabel: 'Monto total',
					anchor:'45%',
					maxLength: 20,
					disabled: true
				}],
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Continuar',	 
			  name: 'btnBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
							var regi;
							var gridValido=false;
							var gridVacio=false;
							var regEdit=0;
							for(var i=0;i<storePago.getCount();i++){
								regi = storePago.getAt(i);
								gridPagos.startEditing(i,1);
								if(regi.get('FECHA_PAGO')==''){
									gridVacio=true;
									return false;
								}
								if(!Ext.getCmp('fechaPago').isValid()&&gridValido==false){
									regEdit=i;
									gridValido=true;	
								}
							}
							gridPagos.startEditing(0,0);
							if(!gridValido){
								if(!gridVacio){
									if( Ext.getCmp('montoPagos').getValue()==Ext.getCmp('monto').getValue()){
										regi= storePago.getAt(Ext.getCmp('noPagos').getValue()-1);
										var regi2 =storeDisp.getAt(Ext.getCmp('numDisp').getValue()-1);
										if(!(regi.get('FECHA_PAGO')<regi2.get('FECHA_VENC'))){
											pagos='';
											montoPagos='';
											storePago.each(function(record){
												pagos+=Ext.util.Format.date(record.get('FECHA_PAGO'),'d/m/Y')+"|";
												montoPagos+=record.get('MONTO')+"|";
										});
									
									
											Ext.Ajax.request({
													url: 'CapturaSolicitudext.data.jsp',
													params: Ext.apply({
														informacion: 'validaCargaMasiva',
														moneda: Ext.getCmp('cbMoneda').getValue(),
														df_pago: pagos,
														fn_monto_disp: fn_monto_disp,
														df_disposicion:df_disposicion,
														df_vencimiento:df_vencimiento,
														numDisp:Ext.getCmp('numDisp').getValue(),
														numPagos:Ext.getCmp('noPagos').getValue(),
														montoPagos:montoPagos
														}),
														callback: procesarFechaMasiva
													});
										
										}else{
											Ext.MessageBox.alert('Error',"La ultima fecha de pago debe coincidir con la de vencimiento");
										}
									
									}else{
										Ext.MessageBox.alert('Error',"El total de los montos es diferente al Monto Total Requerido, favor de corregirlo");
									
									}
								
							}else{
								Ext.MessageBox.alert('Error',"Por favor captura todas las fechas");
							}
					}else{
						
						//regEdit
						Ext.MessageBox.alert('Error',"La fecha de pago debe de ser posterior a la de solicitud",function(){
						gridPagos.startEditing(regEdit,1)
						
						});
						;
					}
				}
			 },{
				text:'Limpiar',
				handler: function() {
						 var reg;
						 for(var i=0;i<storePago.getCount();i++){
							 reg = storePago.getAt(i);
							 //['NO_DISP', 'MONTO','FECHA_DISP','FECHA_VENC']);
							 reg.set('MONTO','0.00');
							 reg.set('FECHA_PAGO','');
						 }
						 Ext.getCmp('montoPagos').setValue('0.00');
				}
			 },
			 {
				text:'Regresar',
				handler: function() {
						pantallaSolicitud(true);
						panelPagos.setVisible(false);
				}
			 },
			 {
				text:'Cargar Archivo',
				handler: function() {
						Ext.getCmp('hidMoneda').setValue(Ext.getCmp('cbMoneda').getValue());
						Ext.getCmp('hidDesde').setValue(Ext.getCmp('pInteres').getValue());
						Ext.getCmp('hidDf_pago').setValue(pagos);
						Ext.getCmp('hidFn_monto_disp').setValue(fn_monto_disp);
						Ext.getCmp('hidDf_disposicion').setValue(df_disposicion);
						Ext.getCmp('hidDf_vencimiento').setValue(df_vencimiento);
						Ext.getCmp('hidNumDisp').setValue(Ext.getCmp('numDisp').getValue());
						Ext.getCmp('hidNumPagos').setValue(Ext.getCmp('noPagos').getValue());
						Ext.getCmp('hidMontoPagos').setValue(montoPagos);
						Ext.getCmp('hidMonto').setValue(Ext.getCmp('montoT').getValue());
						panelCargaMasiva.setVisible(true);
						
						panelPagos.setVisible(false);
				}
			 }
		]
		});
var panelSolicCot = new Ext.Panel({  
		hidden: false,
		layout: 'form',
		id: 'panelSolicCot',
		title: 'Solicitud de Cotizaciones',
		bodyStyle:'padding: 10px',
		items: elementosSolicitud,
		monitorValid: true,
		height: 'auto',
		width: 940,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true
		
		 
		});

var panelOperacion = new Ext.FormPanel({  
		hidden: false,
		frame: true,
		labelWidth: 250,
		monitorValid: true,
		title: 'Caracter�sticas de la Operaci�n',
		bodyStyle:'padding: 10px',
		items: [elementosOperacion],
		buttons:
		[
			{
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'COTIZAR AHORA',
			  name: 'btnCotizar',
			  id: 'btnCotizar',
			  hidden: true,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
							var field=Ext.getCmp('monto');
							field.setValue(replaceAll(field.getValue(),',',''));							
							if(Ext.getCmp('numDisp').getValue()>0){
								if(Ext.getCmp('monto').getValue()>0){
									cotizador='S';
									Ext.Ajax.request({
									url: 'CapturaSolicitudext.data.jsp',
									params: Ext.apply({
										informacion: 'valoresIniciales',
										moneda: Ext.getCmp('cbMoneda').getValue()
										
										}),
									callback: procesarDatos
								});
							}else{
										Ext.MessageBox.alert('Mensaje de p�gina web','Error en el monto total requerido');
							}
						}else{
							Ext.MessageBox.alert('Mensaje de p�gina web','El numero de Disposiciones debe de ser mayor a cero');
						}
					}
			 },
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Continuar',
			  name: 'btnBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{		
							var field=Ext.getCmp('monto');
							field.setValue(replaceAll(field.getValue(),',',''));
							if(Ext.getCmp('numDisp').getValue()>0){
								if(Ext.getCmp('monto').getValue()>0){
									Ext.Ajax.request({
									url: 'CapturaSolicitudext.data.jsp',
									params: Ext.apply({
										informacion: 'valoresIniciales',
										moneda: Ext.getCmp('cbMoneda').getValue()
										
										}),
									callback: procesarDatos
								});
								estado++;
							}else{
										Ext.MessageBox.alert('Mensaje de p�gina web','Error en el monto total requerido');
							}
						}else{
							Ext.MessageBox.alert('Mensaje de p�gina web','El numero de Disposiciones debe de ser mayor a cero');
						}
					}
			 },
			 {
			    text:'Cancelar',handler: function() {
				window.location = 'CapturaSolicitudext.jsp?idMenu=22CAPSOLIC';
			    }
			 }
			 
		]
		});
		

var panelDisposiciones = new Ext.FormPanel({  
		hidden: true,
		monitorValid: true,
		title: 'Disposiciones',
		frame: true,
		bodyStyle:'padding: 10px',
		items: [grid,{
					xtype: 'textfield',
					name: 'montoT',
					id: 'montoT',
					value: '0.00',
					fieldLabel: 'Monto total',
					anchor:'49%',
					maxLength: 20,
					disabled: true
				},
				{	width:500,id:'nota',html: '<b>Nota:</b> Para poder capturar <b>Plan de pagos</b> debe ser s&oacute;lo una disposici&oacute;n'	}
				],
		buttons:
		[
		
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Continuar',	 
			  name: 'btnBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{
							var regi;
							var gridVacio=false;
							for(var i=0;i<storeDisp.getCount();i++){
								regi = storeDisp.getAt(i);
								if(regi.get('FECHA_DISP')==''||regi.get('FECHA_VENC')==''){
									gridVacio=true;
								}
							}
							if(!gridVacio){
							
							var regi;
							var regi2;
							var bandera=true;
							for(var i=0;i<storeDisp.getCount();){
								
								regi = storeDisp.getAt(i);
								if(regi.get('FECHA_DISP')>=regi.get('FECHA_VENC')){
									 fila=i;
									 bandera=false;
									 Ext.MessageBox.alert('Error',"La fecha de vencimiento debe ser posterior a la de disposici�n",function(){
										grid.startEditing(fila,3);
									 }); 
									}
								i++;
								if(i<storeDisp.getCount()){
									regi2 = storeDisp.getAt(i);
									if(regi.get('FECHA_DISP')>regi2.get('FECHA_DISP')){
										bandera=false;
										fila=i;
										Ext.MessageBox.alert('Error',"La fecha de disposicion debe ser mayor a la anterior",function(){
										grid.startEditing(fila,2);
									 });
									}
								}
							 }
							
						if(bandera){
							reg2 = storeDisp.getAt(Ext.getCmp('numDisp').getValue()-1);
							reg = storeDisp.getAt(0);
							if(Ext.getCmp('monto').getValue()==Ext.getCmp('montoT').getValue()){
								igPlazo=calculaDias(Ext.util.Format.date(reg.get('FECHA_DISP'),'d/m/Y'),Ext.util.Format.date(reg2.get('FECHA_VENC'),'d/m/Y'));
								Ext.getCmp('igPlazo').setValue(igPlazo);
								if(plazoMax>igPlazo){
										fn_monto_disp='';
										df_disposicion='';
										df_vencimiento='';
										for(var i=0;i<storeDisp.getCount();i++){
											reg = storeDisp.getAt(i);
											fn_monto_disp+=reg.get('MONTO')+"|";
											df_disposicion+=Ext.util.Format.date(reg.get('FECHA_DISP'),'d/m/Y')+"|";
											df_vencimiento+=Ext.util.Format.date(reg.get('FECHA_VENC'),'d/m/Y'+"|");
										}
										
										Ext.getCmp('teblaDisp').setVisible(true);
										pantallaSolicitud(true);
										panelDisposiciones.setVisible(false);
										Ext.getCmp('plazoOp').setVisible(true);
										Ext.getCmp('esquemaRec').setVisible(true);
										Ext.getCmp('tipoEsquema').allowBlank=false;
										var field =Ext.getCmp('numDisp');
										
									//	if(Ext.getCmp('plazoOp').isVisible()){
										if(field.getValue()>1){
											Ext.getCmp('tTasa').setVisible(true);
											Ext.getCmp('radioGp').setVisible(true);
											Ext.getCmp('dTasaInteres').setVisible(true);
											banderaAccion=false;
										}else{
											Ext.getCmp('tTasa').setVisible(false);
											Ext.getCmp('radioGp').setVisible(false);
											Ext.getCmp('dTasaInteres').setVisible(false);
											banderaAccion=false;
										}
									//}
										procesaTipoEsquema(Ext.getCmp('tipoEsquema'));
										banderaAccion=true;
										Ext.Ajax.request({
												url: 'CapturaSolicitudext.data.jsp',
												params: Ext.apply({
													informacion: 'cotizarAhora',
													moneda: Ext.getCmp('cbMoneda').getValue(),
													fn_monto_disp: fn_monto_disp,
													df_disposicion:df_disposicion,
													df_vencimiento:df_vencimiento,
													plazo:igPlazo,
													plazoMax:plazoMax,
													numDisp:Ext.getCmp('numDisp').getValue(),
													tipoTasa:Ext.getCmp('tipoTasa').getValue()
													}),
												callback: procesarCotizarAhora
											});
											estado=100;
										
										if(banderaRecotiza){
											
											Ext.getCmp('noPagos').setValue(Ext.get('ig_ppc').getValue());
										}
									}else{
										Ext.MessageBox.alert('Error',"El plazo de la operacion excede al parametro. El plazo maximo es de "+plazoMax+" d�as, favor de corregirlo");
									}
							}else{
								Ext.MessageBox.alert('Error',"El total de los montos es diferente al Monto Total Requerido, favor de corregirlo");
							}
						}
					}else{
						Ext.MessageBox.alert('Error',"Por favor captura todas las fechas");
					}
				}
			 },{
				text:'Limpiar',
				handler: function() {
						 var reg;
						 for(var i=0;i<storeDisp.getCount();i++){
							 reg = storeDisp.getAt(i);
							 //['NO_DISP', 'MONTO','FECHA_DISP','FECHA_VENC']);
							 reg.set('MONTO','0.00');
							 reg.set('FECHA_DISP','');
							 reg.set('FECHA_VENC','');
						 }
						 Ext.getCmp('montoT').setValue('0.00');
				}
			 },
			 {
				text:'Regresar',
				handler: function() {
				estado--;
				
						if(banderaRecotiza==true&&estado==0){
						    window.location.href='../22if/22consulta1ext.jsp?idMenu=22CAPSOLIC&ic_solicitud='+ ic_solicitud+'&banderaRecotiza=true';
						
						}else{
							pantallaSolicitud(true);
							panelDisposiciones.setVisible(false);
						}
				}
			 }
			 
		]
		});
		
function pantallaFechas(visible){
			panelFechas.setVisible(visible);
			panelBotonesFecha.setVisible(visible);
		
		}
		
var panelFechas= new Ext.FormPanel({  
    hidden: true,
	 frame: true,
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px', 
    html: '<table width="650" cellpadding="1" cellspacing="2" border="0">'+
			'<tr>'+
					'<td class="titulos">'+
						'Favor de capturar las fechas que se le solicitan a continuaci&oacute;n.'+
					'</td>'+
				'</tr>'+
				'<tr>'+
					'<td class="textos">'+
						'<br>'+
						'<ul>'+
							'<li>Para fines exclusivos de c&aacute;lculo, le pedimos que el d&iacute;a que capture en ambas fechas sea el mismo (si alguna de las fechas es d&iacute;a inh&aacute;bil, le recordamos que su pago lo realizar&aacute; el siguiente d&iacute;a h&aacute;bil).<br></li><br>'+
							'<li>Si en el esquema solicitado solo existen 2 pagos de capital (uno adem&aacute;s del vencimiento), favor de capturar la misma fecha en ambas cajas de texto.</li>'+
						'</ul>'+
					'</td>'+
					'<tr>'+
					'<td class="titulos"><br/>Fechas de Pago de Capital</td>'+
				'</tr>'+
				'</tr>'+
				'</table>'
		
	 });
var panelAyuda= new Ext.FormPanel({  
    hidden: false,
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px', 
    html: '<TD ALIGN="center"><img src="carga_masiva1.jpg" alt="Tama�o original" border="0"></TD'+
			'<table BORDER="0" CELLSPACING="1" CELLPADDING="5" WIDTH="90%" align="center">'+
       '<TR ALIGN="center">'+
	      '<UL TYPE=DISC>'+
             '<LI>El documento debe ser un archivo TXT.'+
             '<LI>Los campos debe ser divididos por pipes <strong><font color="#0000FF">"|"</font></strong> y sin espacios'+
			 '<LI>N�mero de pagos debe ser:'+
			     '<UL TYPE=DISC>'+
			       '<LI>Enteros'+
				   '<LI>No debe de ser mayor de tres d�gitos. por ejemplo: <strong><font color="#0000FF">999</font></strong>'+
				   '<LI>Deben ser consecutivos.'+
			     '</UL>'+
                   '<LI>La fecha debe tener el formato.'+
			     '<UL TYPE=DISC>'+
			       '<LI> <strong><font color="#0000FF">dd/mm/aaaa</font></strong>, donde <strong><font color="#0000FF">dd</font></strong>: indica el d�a, <strong><font   color="#0000FF">mm</font></strong>: indica el mes y <strong><font color="#0000FF">aaaa</font></strong> indica el a�o, por ejemplo: <strong><font color="#0000FF">01/02/2005</font></strong>'+
				 '</UL>'+
              '<LI>El monto estar� formado por enteros y decimales para indicar los decimales es necesario un punto, por ejemplo: <strong><font color="#0000FF">1000000.25</font></strong>'+
           '</UL>'+
	  '</TR>'+	
'</table>'
		
	 });
var panelBotonesFecha = new Ext.FormPanel({
	hidden: true,
	frame: true,
	id: 'panelBotonesFechas',
	defaults:{xtype:'textfield'},   
   bodyStyle:'padding: 10px', 
	items:elementosFechaPago,
	buttons:
	[
		 {
		  //Bot�n BUSCAR
		  xtype: 'button',
		  text: 'Continuar',
		 
		  name: 'btnBuscar',
		  hidden: false,
		  formBind: true,
		  handler: function(boton, evento) 
				{	
					muestraDatos();
					panelMuestraDatos.setVisible(true);	
					panelMuestraDatos2.setVisible(true);
					pantallaFechas(false);
				}
		 },{
			text:'Regresar',
			handler: function() {
					
					 pantallaSolicitud(true);
					 pantallaFechas(false);
					 Ext.getCmp('dFechaPago').setVisible(false);
						Ext.getCmp('dFechaPago2').setVisible(false);
					 }
		 }
		 
	]
});
		

var panel= new Ext.FormPanel({  
    hidden: false,
	 title: 'instrucciones',
		defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px', 
    html: '<table width="150" cellpadding="1" cellspacing="2" border="0">'+
	  '<tr>'+
	     '<td class="formas" >'+
'<pre>'+
'<font face="Arial" size=1>'+
'<br/><br/>1. La Tesorer�a  de  Nacional  Financiera  dar�  respuesta  a  la  solicitud de cotizaci�n espec�fica, con estricto apego<br/>'+
   'a la  informaci�n contenida en el  formato  electr�nico, dentro  de los horarios  que se indican en el punto 4b. <br/>'+
		
'2. La respuesta considerar� un  n�mero de referencia  �nico para cada solicitud y dos tasas indicativas conforme a  lo <br/>'+
   'siguiente:<br/><br/>'+
  
'	a)   La primera tasa se denomina <font color="#0000FF"><strong>"Tasa Indicativa con Oferta Mismo D�a"</strong></font>, que significa que el<br/>'+
            '	intermediario financiero tiene la opci�n de tomarla  "en firme"  el  mismo d�a que haya recibido<br/>'+
            '	respuesta.<br/><br/>'+
		
'	b)   La  segunda  tasa que se proporcionar�  se  denomina  <font color="#0000FF"><strong>"Tasa  Indicativa  con  Oferta a 48<br/>'+
  '	horas"</strong></font>, que  significa  que  el  intermediario  financiero  tiene la opci�n  de tomarla "en firme" <br/>'+
 '	dentro  de los dos d�as  h�biles  siguientes  a la  fecha  en  que haya recibido respuesta.<br/><br/><br/>'+
	
'3. Cuando el intermediario reciba respuesta a su solicitud,  puede  decidir  quedarse con la tasa indicativa  para  fines<br/>'+
   'de  promoci�n  y dejar expirar  el  plazo de la  oferta, sin  que  ello involucre ninguna obligaci�n ni tr�mite adicional<br/>'+
  'ante Nafin. Cuando  el  intermediario  desee  utilizar  la  tasa proporcionada para cerrar alguna operaci�n crediticia,<br/>'+
 'forzosamente deber� tomarla <font color="#0000FF"><strong>"en firme"</strong></font> dentro de los horarios y plazo establecido para la oferta.<br/><br/><br/>'+

'4. <font color="#0000FF"><strong>Horarios de Servicio</strong></font> (todas son horas de la Ciudad de M�xico):<br/><br/><br/>'+
 
       '	a)   Recepci�n de solicitudes de tasa en la Tesorer�a:  10:00 a 18:00 horas.<br/><br/>'+
	  
       '	b)   Respuesta  para  Operaciones de  Segundo  Piso: solicitudes  recibidas hasta las 12:30 horas, el<br/>'+
         '	mismo d�a a  m�s tardar  a las 18:00  horas;  solicitudes  recibidas  despu�s de las  12:30  horas<br/>'+
        '	siguiente d�a h�bil a m�s tardar a las 11:00 horas.<br/><br/>'+
	
       '	c) <font color="#0000FF"><strong>"Toma en firme" oferta mismo d�a:</strong></font> A m�s tardar a  las  12:30 horas del d�a en que la  Tesorer�a<br/>'+
		'	haya proporcionado la cotizaci�n indicativa.<br/><br/>'+
		  
     '	d)   <font color="#0000FF"><strong>"Toma en firme"</strong></font> oferta 48 horas: A m�s tardar dos  d�a  h�biles  despu�s  de  que la  Tesorer�a<br/>'+
    '	haya proporcionado la cotizaci�n  indicativa  antes  de las 12:30  horas del segundo d�a h�bil.<br/>'+
		
   '	e)   La Tesorer�a se  reserva el derecho de  modificar los tiempos establecidos en el punto 5b para la<br/>'+
  '	entrega de los  precios de  aquellas  operaciones  que  por  su  monto,  plazo  o  caracter�sticas<br/>'+
 '	requieran de autorizaci�n de la alta Direcci�n.<br/><br/>'+
		  
'5.  En el caso de que la disposici�n de los recursos de una operaci�n tomada  en  firme no se lleve a cabo en la fecha<br/>'+
  'estipulada, Nacional Financiera aplicar� el cobro de una comisi�n por no disposici�n de recursos sobre los montos<br/>'+
 'no dispuestos, conforme a las pol�ticas de cr�dito aplicables a cada tipo de operaci�n.<br/><br/><br/>'+
    	
'6.  <font color="#0000FF"><strong>Las  cotizaciones  proporcionadas</strong></font> por  la  Tesorer�a  de  Nacional  Financiera  <font color="#0000FF"><strong>son de  uso  reservado para el<br/>'+
 'intermediario solicitante</strong></font> y ser� responsabilidad de dicho intermediario el mal uso de la informaci�n  proporcionada.<br/>'+
   
'</font>'+
'</pre>'+
'	</td>'+
'</tr>'+
'</table>'

});  



//--------------------Fin de Lista de PANEL'S------------------------

var ventanaAyuda = new Ext.Window({
			width: 800,
			height: 'auto',
			//maximizable: true,
			modal: true,
			closable: true,
			closeAction: 'hide',
			resizable: true,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			layout: 'fit',
			items: [panelAyuda]
			
});

var ventanaInstrucciones = new Ext.Window({
			width: 920,
			height: 'auto',
			//maximizable: true,
			modal: true,
			closable: false,
			resizable: true,
			minWidth: 500,
			minHeight: 450,
			maximized: false,
			constrain: true,
			items: [panel,
			{
			xtype: 'compositefield',
			combineErrors: false,
			forceSelection: false,
			align: 'center',
			msgTarget: 'side',
			items: [
				{
				xtype: 'displayfield',
				value: '',
				width: 400
				},
				{
			  //Bot�n Aceptar
			  xtype: 'button',
			  text: 'Aceptar',
			  name: 'Aceptar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						var cs_instrucciones="S";
						if(Ext.getCmp('instruccion').getValue()){
							cs_instrucciones="N";
							}
						Ext.Ajax.request({
									url: 'CapturaSolicitudext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Instrucciones',
									cs_instrucciones: cs_instrucciones
									}),
									callback: procesarInstrucciones
									});	
					}
				},
				{
							xtype: 'button',
							name: 'Imprimir',
							hidden: false,
							formBind: true,
							text:'Imprimir',
							handler: function() {
									var forma = Ext.getDom('formAux');
									forma.action = '/nafin/00archivos/22cotizador/instrucciones.pdf';
									forma.submit();
							}
						 }
				]
			 },
			{
			xtype: 'compositefield',
			combineErrors: false,
			forceSelection: false,
			msgTarget: 'side',
			items: [
						
						{
						xtype: 'checkbox',
						//fieldLabel: 'No volver a mostrar este formato',
						name: 'instruccion',
						id: 'instruccion',
						hiddenName:'instruccion'
						},
						{
						xtype: 'displayfield',
						value: 'No volver a mostrar este formato',
						width: 200
						}
					]
				}
			]
});



var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 940,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 200,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		tbar: {
				id:'toolBar',
				renderTo:fp,
				items: [
					'->',
					{
						xtype: 'button',
						text: 'Instrucciones',
						id: 'btnInstrucciones',
						hidden: false,
						handler: function(boton, evento) {									
							ventanaInstrucciones.show();
							
						}
					}
				]
		},
		items: [
		]
		
			 
		
	});




var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  bodyStyle: 'padding: 10px',
	  width: 940,
	  items: 
	    [ 
	  fp,ventanaInstrucciones,ventanaAyuda,panelSolicCot,panelOperacion,panelDisposiciones
		,panelFechas
		,panelBotonesFecha
		,panelPagos
		,panelCargaMasiva
		,panelCotizador
		,panelMuestraDatos
		,panelDatosMasiva
		,panelMuestraDatos2
		 ]
  });
  
  
 
						
//RE-COTIZAR
if(Ext.get('fn_monto').getValue()!=''){
banderaRecotiza=true;
}
else{
banderaRecotiza=false;
}
if(banderaRecotiza){
estado++;
//Igualar variables
			pantallaSolicitud(false);
			Ext.getCmp('monto').setValue(Ext.get('fn_monto').getValue());
			
			ic_solicitud=Ext.get('ic_solicitud').getValue();
			Ext.getCmp('numDisp').setValue(Ext.get('in_num_disp').getValue());
			Ext.getCmp('nomProg').setValue(Ext.get('cg_programa').getValue());
			

			
			
			
			Ext.get('in_dia_pago').getValue();
			
			Ext.get('ig_periodos_gracia').getValue();
			Ext.get('cg_ut_periodos_gracia').getValue();
			Ext.get('cg_razon_social_if').getValue();
			Ext.get('ig_riesgo_if').getValue();
			Ext.get('cg_acreditado').getValue();
			Ext.get('ig_riesgo_ac').getValue();
			Ext.get('diasIrreg').getValue();
			array=Ext.get('fn_monto_disp').getValue();
			array = array.split(",");

			

			Ext.get('df_disposicion').getValue();
			Ext.get('df_vencimiento').getValue();

			

}

	 banderaInicio = Ext.get('bandera').getValue();
	if(banderaInicio=='false'){
		ventanaInstrucciones.show();	
	}else{
		Ext.getCmp('instruccion').setValue(true);
	}
	
	catalogoMoneda.load();
	catalogoTasaInteres.load();
	catalogoEsquema.load();
	catalogoUnidadTiempo.load();
	Ext.getCmp('teblaDisp').setVisible(false);
	panelBotonesFecha.setVisible(false);
	catalogoUnidadTiempo2.load({params: {
													bandera: true
												}
												});
	Ext.Ajax.request({
		url: 'CapturaSolicitudext.data.jsp',
		params: Ext.apply({
			informacion: 'valoresIniciales'}),
		callback: procesarValoeresIniciales
	});
						
  });