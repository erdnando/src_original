<%@ page
contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		com.netro.cotizador.*,
		netropology.utilerias.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/22cotizador/22secsession_extjs.jspf" %>

<%
	JSONObject datos = new JSONObject();
	boolean flag = true;
	ParametrosRequest req = null;
	String rutaArchivoTemporal = null;
	//String itemArchivo = "", resumen="", errores="";
	int idPagos=0;
	int tamanio = 0;
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(strDirectorioTemp));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		String moneda = (req.getParameter("moneda") == null)? "" : req.getParameter("moneda");
			String disp 					=(req.getParameter("numDisp")    != null) ?   req.getParameter("numDisp") :"";
		String monto =(req.getParameter("monto")    != null) ?   req.getParameter("monto") :"";
			int disposicion=0;
			if(!disp.equals("")){
				disposicion=Integer.parseInt(disp);
			}
				String fn_monto_disp			=(req.getParameter("fn_monto_disp")    != null) ?   req.getParameter("fn_monto_disp") :"";
				String df_disposicion			=(req.getParameter("df_disposicion")    != null) ?   req.getParameter("df_disposicion") :"";
				String df_vencimiento			=(req.getParameter("df_vencimiento")    != null) ?   req.getParameter("df_vencimiento") :"";
				//String df_pagos 						=(req.getParameterValues("df_pago") !=null)? req.getParameter("df_pago"):"";
				int numPagos							=((req.getParameterValues("numPagos") !=null)&&!(req.getParameterValues("numPagos").equals("")))? Integer.parseInt(req.getParameter("numPagos")):0;
				//String montoPagos						=(req.getParameterValues("montoPagos") !=null)? req.getParameter("montoPagos"):"";
				
				/*String[] dfPagos= new String[numPagos];
				String[] fn_monto_pago=new String[numPagos];*/
				String[] fn_montoDisp= new String[disposicion];
				String[] dfDisposicion= new String[disposicion];
				String[] dfVencimiento= new String[disposicion];
				
				StringTokenizer tokens1 = new StringTokenizer(fn_monto_disp,"|");
				StringTokenizer tokens2 = new StringTokenizer(df_disposicion,"|");
				StringTokenizer tokens3 = new StringTokenizer(df_vencimiento,"|");
				/*StringTokenizer tokens4 = new StringTokenizer(df_pagos,"|");
				StringTokenizer tokens5 = new StringTokenizer(montoPagos,"|");
				
				for(int i=1;i<=numPagos;i++){
					dfPagos[i-1]=tokens4.nextToken();
					fn_monto_pago[i-1]=tokens5.nextToken();
				}*/
				for(int i =1;i<=disposicion;i++){
					fn_montoDisp[i-1]=tokens1.nextToken();
					dfDisposicion[i-1]=tokens2.nextToken();
					dfVencimiento[i-1]=tokens3.nextToken();
				}
			
		
		
		try{
			int tamaPer = 2097152;
			
			FileItem fItem = (FileItem)req.getFiles().get(0);
			//itemArchivo		= (String)fItem.getName();
			InputStream archivoTXT = fItem.getInputStream();
			tamanio			= (int)fItem.getSize();

			if (tamanio > tamaPer){
				flag = false;
			}
			if (flag){
				SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
				Solicitud captura = new Solicitud();
				captura.setIc_moneda(moneda);
				captura.setStrTipoUsuario(strTipoUsuario);
				captura = solCotEsp.cargaDatos(iNoCliente,iNoUsuario,strTipoUsuario,captura);
				String linea = "";
				ArrayList alPagos = new ArrayList();
				BufferedReader br=new BufferedReader(new InputStreamReader(archivoTXT));
				captura.setFn_monto_disp(fn_montoDisp);
				captura.setDf_disposicion(dfDisposicion);
				captura.setDf_vencimiento(dfVencimiento);
				captura.setIg_ppc(numPagos+"");
				captura.setFn_monto(Double.parseDouble(monto));
				captura.setFn_monto1(monto);
				String desde 		=(req.getParameter("desde") == null)? "0" : req.getParameter("desde");
				captura.setCg_ut_ppi(desde+"");
		//String cot_en_linea = (request.getParameter("cot_en_linea")==null)?"":request.getParameter("cot_en_linea");
			
				while((linea=br.readLine())!=null) {
					if(!"".equals(linea))
						alPagos.add(linea);
				}
				captura	= solCotEsp.guardaPagosMasiva(captura,alPagos);
				
				HashMap	datosMap;
				List reg=new ArrayList();
				
				
				String temporal="";
				StringTokenizer tok;
				if(captura.getErrores().toString().equals(""))
				while(!alPagos.isEmpty()){
					
					tok = new StringTokenizer(alPagos.remove(0).toString(),"|");
					datosMap=new HashMap();
					datosMap.put("NO_PAGO",tok.nextToken());
					datosMap.put("FECHA_PAGO",tok.nextToken());
					datosMap.put("MONTO",tok.nextToken());
					reg.add(datosMap);
				}
				idPagos 	= captura.getIc_proc_pago();
				datos.put("registros", reg);
				datos.put("resumen", captura.getResumen().toString());
				datos.put("errores", captura.getErrores().toString());
				datos.put("id_pagos", idPagos+"");
			}
		}catch (Exception e){
			System.err.println(e);
			datos.put("registros", "{}");
			datos.put("resumen", "");
			datos.put("id_pagos", "");
			datos.put("errores","Error: El archivo es muy grande o no corresponde al formato");
		}
	}
	
%>
{
	"success": true,
	"flag":	<%=(flag)?"true":"false"%>,
	"datos":	<%=datos%>
}