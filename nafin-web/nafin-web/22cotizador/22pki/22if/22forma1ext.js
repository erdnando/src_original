Ext.onReady(function() {
var textoFirmado;
var ic_solic;
var valHoraToma;
var tipoUsuario;
var intermediario;
var recibo;
//--------------------HANDLERS--------------------------
var procesarConsultaData = function(store,arrRegistros,opts){
		
			if(arrRegistros!=null){
				var el = grid.getGridEl();
				if(store.getTotalCount()>0){
					tipoUsuario=store.reader.jsonData.tipoUsuario;
					Ext.getCmp('btnPDF').setVisible(tipoUsuario);
					el.unmask();
				}else{
					el.mask('Esta opci&oacute;n no est&aacute; disponible por el momento. Favor de comunicarse con su ejecutivo', 'x-mask');
					}
			}
		}
function procesarArchivoPdfSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			Ext.getCmp('btnPDF').setIconClass('');
			Ext.getCmp('btnPDF').enable();
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fpCotizacion.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpCotizacion.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
var procesarDetalle = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		grid.setVisible(false);
		fpCotizacion.setVisible(true);
		grid2.setVisible(true);
		fpMensajes.setVisible(true);
		ic_solic=registro.get('IC_SOLIC');
					Ext.Ajax.request({
							url: '22forma1ext.data.jsp',
							params: {informacion:'consultaCotizacion',ic_solic:ic_solic},
							callback: procesaConsulta
						});
	}

var revisaDatos = function(cmp){
	if(cmp.getValue()==''){
		cmp.setVisible(false);
	}
}

	var confirmarAcuse = function(pkcs7, vtextoFirmado){
	
		if (Ext.isEmpty(pkcs7) ) {
			Ext.Msg.alert('Firmar','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;	//Error en la firma. Termina...
		}else  {
			Ext.Ajax.request({
				url : '22forma1ext.data.jsp',
				params: Ext.apply({
					informacion: 'Firma',
					pkcs7: pkcs7, 
					textoFirmado: vtextoFirmado,
					ic_solic:ic_solic,
					valHoraToma:valHoraToma
				}),
				callback: procesaFirma
			});		
		}
	}
	
var firma= function(){
	
	NE.util.obtenerPKCS7(confirmarAcuse, textoFirmado );
		
}
var procesaFirma = function(opts,success,response){
	if(success == true){
		if(Ext.util.JSON.decode(response.responseText).success === false && Ext.util.JSON.decode(response.responseText).Error!==''){
                    Ext.Msg.alert('Error',Ext.util.JSON.decode(response.responseText).Error,function(){
                        location.reload();
                    });
		}
                
		if(Ext.util.JSON.decode(response.responseText).success === true ){
		
			Ext.getCmp('labelAutentificacion').setText(Ext.util.JSON.decode(response.responseText).Exito,false);
			Ext.getCmp('FECHA').body.update(Ext.util.JSON.decode(response.responseText).Fecha);
			Ext.getCmp('fechaConf').setVisible(true);
			Ext.getCmp('btnPDF').setVisible(true);
			
			recibo =Ext.util.JSON.decode(response.responseText).recibo;
			
                    if(Ext.util.JSON.decode(response.responseText).Exito!==''){
                        Ext.Msg.alert('Exito',Ext.util.JSON.decode(response.responseText).Exito,function(){       });
                    }	
		}
	}
}
var procesaConsulta = function(opts,success,response){
	
	if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
		storeDisp.loadData(Ext.util.JSON.decode(response.responseText).registros);
		storePago.loadData(Ext.util.JSON.decode(response.responseText).registros2);
		if(storePago.getTotalCount()!=0){
			gridPagos2.setVisible(true);
		}
		Ext.getCmp('numSolic').body.update('<div align="left">'+Ext.util.JSON.decode(response.responseText).numeroSolic+'</div>');
		Ext.getCmp('fechaSolic').body.update('<div align="left">'+Ext.util.JSON.decode(response.responseText).fechaSolic+'</div>');
		Ext.getCmp('horaSolic').body.update('<div align="left">'+Ext.util.JSON.decode(response.responseText).horaSolic+'</div>');
		Ext.getCmp('fechaCot').body.update('<div align="left">'+Ext.util.JSON.decode(response.responseText).fechaCot+'</div>');
		Ext.getCmp('horaCot').body.update('<div align="left">'+Ext.util.JSON.decode(response.responseText).horaCot+'</div>');
		Ext.getCmp('numUsuario').body.update('<div align="left">'+Ext.util.JSON.decode(response.responseText).icUsuario+'</div>');
		
		Ext.getCmp('tipoCotizacion').body.update('<div align="left">'+Ext.util.JSON.decode(response.responseText).mensaje+'</div>');
		Ext.getCmp('tasaCotizada').body.update(Ext.util.JSON.decode(response.responseText).mensajeTasa);//tasaCotizada
		Ext.getCmp('mensaje2').setValue('<div align="center"><font color="#FF0000">'+Ext.util.JSON.decode(response.responseText).mensaje2+'</font></div>');

		Ext.getCmp('dIntermediario').setValue(Ext.util.JSON.decode(response.responseText).Intermediario);
		intermediario=(Ext.util.JSON.decode(response.responseText).Intermediario);
		Ext.getCmp('dEjecutivo').setValue(Ext.util.JSON.decode(response.responseText).ejecutivo);
		Ext.getCmp('dCorreo').setValue(Ext.util.JSON.decode(response.responseText).correo);
		Ext.getCmp('telefono').setValue(Ext.util.JSON.decode(response.responseText).telefono);
		Ext.getCmp('fax').setValue(Ext.util.JSON.decode(response.responseText).fax);
		Ext.getCmp('dPrograma').setValue(Ext.util.JSON.decode(response.responseText).programa);
		Ext.getCmp('dMonto').setValue(Ext.util.JSON.decode(response.responseText).monto);
		Ext.getCmp('dTasa').setValue(Ext.util.JSON.decode(response.responseText).tasa);
		Ext.getCmp('dDisposiciones').setValue(Ext.util.JSON.decode(response.responseText).numDisp);
		if(Ext.util.JSON.decode(response.responseText).numDisp>1){
			Ext.getCmp('dTasaInteres').setVisible(true);
		}
		
		Ext.getCmp('dTasaInteres').setValue(Ext.util.JSON.decode(response.responseText).unica);
		Ext.getCmp('dPlazo').setValue(Ext.util.JSON.decode(response.responseText).plazo);
		Ext.getCmp('dEsquema').setValue(Ext.util.JSON.decode(response.responseText).esquema);
		Ext.getCmp('dPeriodicidadI').setValue(Ext.util.JSON.decode(response.responseText).pInteres);
		Ext.getCmp('dPeriodicidadC').setValue(Ext.util.JSON.decode(response.responseText).pCapital);
		Ext.getCmp('dFechaPago').setValue(Ext.util.JSON.decode(response.responseText).fechaPagoCap);
		Ext.getCmp('dFechaPago2').setValue(Ext.util.JSON.decode(response.responseText).fechaPagoCap2);
		var icEsquema=Ext.util.JSON.decode(response.responseText).icEsquema;
		if(icEsquema==2||icEsquema==3){
			Ext.getCmp('dPeriodicidadI').setVisible(true);
		}
		if(icEsquema==2||icEsquema==4){
			
			Ext.getCmp('dPeriodicidadC').setVisible(true);
		}
		if(icEsquema==2){
			Ext.getCmp('dFechaPago').setVisible(true);
			Ext.getCmp('dFechaPago2').setVisible(true);
		}
		if(Ext.util.JSON.decode(response.responseText).boton){
			Ext.getCmp('btnTomarFirme').setVisible(true);
		}else if(!Ext.util.JSON.decode(response.responseText).boton){
                    Ext.getCmp('btnTomarFirme').setVisible(true);  
                    Ext.getCmp('btnTomarFirme').disable();
                    Ext.getCmp('tasaCoti').hide();
                    

                }
		textoFirmado=Ext.util.JSON.decode(response.responseText).textoFirmar;
		valHoraToma=Ext.util.JSON.decode(response.responseText).valHoraToma;
	}
}
//------------------Stores-------------------
var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '22forma1ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'FECHA_SOLIC'},
				{name: 'FECHA_COT'},
				{name: 'EJECUTIVO'},
				{name: 'NUM_SOLIC'},
				{name: 'MONEDA'},
				{name: 'MONTO'},
				{name: 'PLAZO'},
				{name: 'IC_SOLIC'}
				
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
						procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
var storeDisp = new Ext.data.JsonStore({
		fields: [
			{name:'NO_DISP'},{name:'MONTO'},{name:'FECHA_DISP'},{name:'FECHA_VENC'}
			
		],
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
	
var storePago = new Ext.data.JsonStore({
		fields: [
			{name:'NO_PAGO'},{name:'FECHA_PAGO'},{name:'MONTO'}			
		],
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

//----------------Componentes---------------------------
var elementosDespliega =[{
		xtype: 'displayfield',
		name: 'dIntermediario',
		id: 'dIntermediario',
		allowBlank: false,
		fieldLabel: 'Intermediario / Acreditado',
		value:''
	},{
		xtype: 'panel',
		allowBlank: false,
		title:'Datos del Ejecutivo Nafin',
		value: ''
	},{
		xtype: 'displayfield',
		name: 'dEjecutivo',
		id: 'dEjecutivo',
		allowBlank: false,
		fieldLabel: 'Nombre del Ejecutivo',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dCorreo',
		id: 'dCorreo',
		allowBlank: false,
		fieldLabel: 'Correo Electr�nico',
		value:''
	},{
		xtype: 'displayfield',
		name: 'telefono',
		id: 'telefono',
		allowBlank: false,
		fieldLabel: 'Telefono',
		value:''
	},{
		xtype: 'displayfield',
		name: 'fax',
		id: 'fax',
		allowBlank: false,
		fieldLabel: 'Fax',
		value:''
	},{
		xtype: 'panel',
		allowBlank: false,
		title: 'Caracter�sticas de la Operaci�n'
	},{
		xtype: 'displayfield',
		name: 'dPrograma',
		id: 'dPrograma',
		allowBlank: false,
		fieldLabel: 'Nombre del programa o acreditado Final',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dMonto',
		id: 'dMonto',
		allowBlank: false,
		fieldLabel: 'Monto total requerido',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dTasa',
		id: 'dTasa',
		allowBlank: false,
		fieldLabel: 'Tipo de tasa requerida',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dDisposiciones',
		id: 'dDisposiciones',
		allowBlank: false,
		fieldLabel: 'N�mero de Disposiciones',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dTasaInteres',
		id: 'dTasaInteres',
		allowBlank: false,
		hidden:true,
		fieldLabel: 'Disposiciones m�ltiples. Tasa de Inter�s',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dPlazo',
		id: 'dPlazo',
		allowBlank: false,
		fieldLabel: 'Plazo de la operaci�n',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dEsquema',
		id: 'dEsquema',
		allowBlank: false,
		fieldLabel: 'Esquema de recuperaci�n del capital',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dPeriodicidadI',
		id: 'dPeriodicidadI',
		hidden:true,
		allowBlank: false,
		fieldLabel: 'Periodicidad del pago de Inter�s',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dPeriodicidadC',
		id: 'dPeriodicidadC',
		hidden:true,
		allowBlank: false,
		fieldLabel: 'Periodicidad de capital',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dFechaPago',
		id: 'dFechaPago',
		hidden:true,
		allowBlank: false,
		fieldLabel: 'Primer fecha de pago de Capital',
		value:''
	},{
		xtype: 'displayfield',
		name: 'dFechaPago2',
		id: 'dFechaPago2',
		hidden:true,
		allowBlank: false,
		fieldLabel: 'Pen�ltima fecha de pago de Capital',
		value:''
	}
	
]
var elementosMensajes =[
	{
			xtype: 'panel',frame: true,layout:'table',	width:800,	border:false,	layoutConfig:{ columns: 2 },	style: 'margin:0 auto;',
			defaults: {frame:false, border: true,width:400	, height: 35,	bodyStyle:'padding:6px'},
			items:[
					{	colspan: 2,id:'tipoCotizacion',html: '&nbsp;'	},
					{	id:'tasaCoti',	html:'<div align="left"><strong>TASA COTIZADA</strong></div>'	},
					{	hidden: true,id: 'fechaConf',			html:'<div align="left"><strong>FECHA Y HORA DE CONFIRMACION</strong></div>'	},
					{	width:500, id:'tasaCotizada',html: '&nbsp;'	},
					{	id:'FECHA',html: '&nbsp;'	}
					
				]
	},
	{	id:'observaciones',html: '&nbsp;'	}
	,
	{
		xtype: 'displayfield',
		align: 'center',
		name: 'mensaje2',
		id: 'mensaje2',
		anchor:'85%',
		allowBlank: false
	}

]
var elementosCotizacion = [
		
				{
			xtype: 	'label',
			id:	 	'labelAutentificacion',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:15px;',
			html:  	''
		},
		{
			xtype: 'panel',frame: false,title: '<center>Datos de la cotizacion</center>',layout:'table',	width:600,	border:false,	layoutConfig:{ columns: 2 },	style: 'margin:0 auto;',
			defaults: {frame:false, border: true,width:170	, height: 35,	bodyStyle:'padding:6px'},
			items:[
				//{	html:''	},
				//{	width:450,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Datos de la Cotizaci�n</div>'	},
				{	html:'<div class="formas" align="left">N�mero de Solicitud:</div>'	},
				{	width:500,id:'numSolic',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Fecha de Solicitud:</div>'	},
				{	width:500,id:'fechaSolic',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Hora de Solicitud:</div>'	},
				{	width:500,id:'horaSolic',	html:'&nbsp;'},
				{	html:'<div class="formas" align="left">Fecha de Cotizaci�n:</div>'	},
				{	width:500,id:'fechaCot',html: '&nbsp;'	},
				{	html:'<div class="formas" align="left">Hora de Cotizaci�n:</div>'	},
				{	width:500,id:'horaCot',html: '&nbsp;'	},
				{	html:'<div class="formas" align="left">N�mero y Nombre de usuario:</div>'	},
				{	width:500,id:'numUsuario',html: '&nbsp;'	},
				{	width:500,height: 60,	colspan:2,	border:false,	frame:true,	html:'<div align="center">En caso de que la disposici&oacute;n de los recursos no se lleve a cabo total o parcialmente en la'+
                                                ' fecha estipulada, Nacional Financiera aplicar&aacute; el cobro de una comisi&oacute;n sobre los'+
                                                ' montos no dispuestos, conforme a las pol&iacute;ticas de cr&eacute;dito aplicables a este tipo de'+
                                                ' operaciones.</div>'		}
			]
		}
	];
var fpCotizacion = new Ext.FormPanel({	style: 'margin:0 auto;',labelWidth: 250,	height:'auto',	width:800,	border:true,	frame:false,	
items:[elementosCotizacion,NE.util.getEspaciador(25),elementosDespliega],	hidden:true 	});
var fpMensajes = new Ext.FormPanel({	style: 'margin:0 auto;',	height:'auto',	width:800,	border:true,	frame:false,	
items:[elementosMensajes],
buttons:
		  [
			 {
			  //Bot�n
			  xtype: 'button',
			  text: 'Tomar en Firme',
			  name: 'btnTomarFirme',
			  id: 'btnTomarFirme',
			  hidden: true,
			  formBind: true,
			  handler: function(boton, evento) {	
                          
                              Ext.Ajax.request({
                                url: '22forma1ext.data.jsp',
                                params: Ext.apply({
                                    informacion: 'validaAnteFirme',
                                    ic_solic:ic_solic,
                                    valHoraToma:valHoraToma                   
                                }),
                                success : function(response) {
                                    var infoR = Ext.util.JSON.decode(response.responseText);
                            
                                    if (infoR.Error ==='' ){
                                    
                                        boton.setVisible(false);
                                        Ext.Msg.show({
                                                            title:'Importante',
                                                            msg: "Acaba de iniciar el proceso de confirmaci�n (toma en firme) de la tasa de inter�s<br/>"+
                                            "proporcionada por el cotizador, por lo que es necesario que lea con cuidado el<br/>"+
                                            "siguiente mensaje antes de proseguir:<br/><br/>"+
                                            "Verifique que los datos capturados correspondan a las caracter�sticas del cr�dito que<br/>"+
                                            "desea operar.<br/><br/>"+
                                            "Al continuar con el proceso de confirmaci�n usted manifiesta su conformidad con <br/>"+
                                            "la tasa de inter�s proporcionada, y en caso de concluir con �xito este proceso asume:<br/>"+
                                            "El compromiso de operar el cr�dito en la fecha de disposici�n que aparecer� impresa<br/>"+
                                            "en la hoja de confirmaci�n correspondiente.<br/>"+
                                            "Al obtener su hoja de confirmaci�n, Nacional Financiera asume el compromiso de<br/>"+
                                            "respetar la tasa de inter�s tomada en firme hasta la fecha de disposici�n de los<br/>"+
                                            "recursos. En caso de que la disposici�n no se lleve a cabo total o parcialmente en la<br/>"+
                                            "fecha estipulada, Nacional Financiera aplicar� el cobro de una comisi�n sobre los<br/>"+
                                            "montos no dispuestos, conforme a las pol�ticas de cr�dito aplicables a este tipo de<br/>"+
                                            "operaciones.<br/>",
                                                                    buttons: Ext.Msg.OKCANCEL,
                                                                    fn: firma,
                                                                    animEl: 'elId',
                                                                    icon: Ext.MessageBox.QUESTION
                                                            });
                                                
                                        }else  {
                                            Ext.Msg.alert('Error',Ext.util.JSON.decode(response.responseText).Error,function(){
                                                location.reload();
                                            });                                    
                                        }						
                                    }
                                });
                            }
			 },
                         {
				 xtype: 'button',
			  text: 'Generar Archivo PDF',
			  name: 'btnPDF',
			  id: 'btnPDF',
			  iconCls: 'icoPdf',
			  hidden: false,
			  handler: function(boton, evento) 
			  {
			   boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '../../22ConsultaSolicitud1pdfext.jsp',
					params: Ext.apply({
						ic_solicitud:ic_solic,
						estatus_solicitud:5,
						intermediario:intermediario,
						origen:'TF',
						recibo:recibo						
					}),
					callback: procesarArchivoPdfSuccess
				});
			  }
			 },
			 {
			  
			  xtype: 'button',
			  text: 'Salir',
			  name: 'btnLimpiar',
			  hidden: false,
			  handler: function(boton, evento) 
			  {
			   location.reload();
			  }
			 }
		  ],	hidden:true 	});
var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: false,
				header:true,
				title: '',
				store: consulta,
				style: 'margin:0 auto;',
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				columns:[
				//CAMPOS DEL GRID
						{
						header:'Fecha de Solicitud',
						tooltip: 'Fecha de Solicitud',
						sortable: true,
						dataIndex: 'FECHA_SOLIC',
						width: 130,
						align: 'center'
						},
						{
						header: 'Fecha y Hora de Cotizaci&oacute;n',
						tooltip: 'Fecha y Hora de Cotizaci&oacute;n',
						sortable: true,
						dataIndex: 'FECHA_COT',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Ejecutivo',
						tooltip: 'Ejecutivo',
						sortable: true,
						dataIndex: 'EJECUTIVO',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'N&uacute;mero de Solicitud',
						tooltip: 'N&uacute;mero de Solicitud',
						sortable: true,
						dataIndex: 'NUM_SOLIC',
						width: 150,
						align: 'center'
						},
						{
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'MONEDA',
						width: 130,
						align: 'center'
						
						},
						{
						align:'center',
						header: 'Monto Total Requerido',
						tooltip: 'Monto Total Requerido',
						sortable: true,
						dataIndex: 'MONTO',
						width: 130
						},{
						
						header:'Plazo de la Operaci&oacute;n',
						tooltip: 'Plazo de la Operaci&oacute;n',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 130,
						align: 'center'
						},
						{
						xtype: 'actioncolumn',
						header: 'Consultar<br/>Cotizaci�n',
						tooltip: 'Consultar',
						sortable: true,
						dataIndex: 'IC_SOLIC',
						width: 150,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							       return 'iconoLupa';	
								 }
								 ,handler: procesarDetalle
					      }]	
						}
				]
			});
var grid2 = new Ext.grid.GridPanel({
		style: 'margin:0 auto;', id:'grid2',enableColumnMove:false, view:new Ext.grid.GridView({forceFit:true,markDirty: false}) ,store: storeDisp,	clicksToEdit:1,	columLines:true,  hidden:true,
		stripeRows:true,title: 'Detalle de Disposiciones',	loadMask:true,	height:130,	width:800,frame:true,
		columns: [
			{
				header:'No. Disp',	tooltip:'No. Disp', hideable:false,	dataIndex:'NO_DISP',	sortable:false,	width:170,	align:'center'
			},{
				header:'Monto',	tooltip:'Monto',	dataIndex:'MONTO',	sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				editor: {xtype : 'numberfield', id: 'montos', name:'montos',maxLength:11
				
				}
				
			},{
				header:'Fecha de disposici�n:<br/>dd/mm/aaaa',	tooltip:'Fecha de disposici�n:<br/>dd/mm/aaaa',	dataIndex:'FECHA_DISP',
				sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return value;
							}
			},{
				header:'Fecha de Vencimiento:<br/>dd/mm/aaaa',	tooltip:'Fecha de Vencimiento:<br/>dd/mm/aaaa',	dataIndex:'FECHA_VENC',
				sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return value;
							}
			}
		]
		});
var gridPagos2 = new Ext.grid.GridPanel({
		style: 'margin:0 auto;', id:'gridPagos2',enableColumnMove:false ,hidden: true,store: storePago, view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	clicksToEdit:1,	columLines:true,  
		stripeRows:true,title: 'Plan de Pagos Capital',	loadMask:true,	height:130,	width:800,	frame:true,
		columns: [
			{
				header:'No. de Pago',	tooltip:'No. de Pago', hideable:false,	dataIndex:'NO_PAGO',	sortable:false,	width:250,	align:'center'
			},{
				header:'Fecha de Pago:<br/>dd/mm/aaaa',	tooltip:'Fecha de Pago:<br/>dd/mm/aaaa',	dataIndex:'FECHA_PAGO',
				sortable:false,	width:250,	hidden:false, hideable:false,	align:'right',
				renderer:function(value,metadata,registro, rowIndex, colIndex){
                        return value;
							}
				
			},{
				header:'Monto',	tooltip:'Monto',	dataIndex:'MONTO',	sortable:false,	width:250,	hidden:false, hideable:false,	align:'right'
			}
		]
		});
			
var fp = new Ext.Panel
  ({
		hidden: false,
		height: 'auto',
		width: 940,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		labelWidth: 200,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
	
		//monitorValid: true,
		items: [grid,fpCotizacion,grid2,gridPagos2,fpMensajes]
		
			 
		
	});
//----------------Principal---------------------------
var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		frame:true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 10px',
		height: 'auto',
		items: [
			fp
			
		]
	});
	
	consulta.load();
	
	});