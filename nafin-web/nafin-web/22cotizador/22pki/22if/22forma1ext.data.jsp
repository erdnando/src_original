	<%@ page contentType="application/json;charset=UTF-8" import="   
	java.util.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONObject,
	net.sf.json.JSONArray,        
        org.apache.commons.logging.Log,
	com.netro.cotizador.*"	
	errorPage="/00utils/error_extjs.jsp"%>
	
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
	<%@ include file="../certificado.jspf" %>
        <%! private final Log LOG = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>
	<%
	
	JSONObject jsonObj 	      = new JSONObject();
	String infoRegresar        = "";
	String informacion         =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
	
        
	
	if(informacion.equals("Consulta")){
		
		AutorizaSolCot solCot = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
		ArrayList alConsulta = null;
		HashMap	datos;
		List reg=new ArrayList();
		int i = 0;
		int cont = 0;
		boolean pintar = true;
				alConsulta = solCot.consultaTomaEnFirme(iNoCliente);
			for(i=0;i<alConsulta.size();i++){
				AutSolic autSol = (AutSolic)alConsulta.get(i);
				if(strTipoUsuario.equals("IF")){
					String ic = autSol.getIc_moneda();
					//pintar = validaEstatus(ic);
					String estat = "";
					ResultSet rs = null;
					AccesoDB con = new AccesoDB();
					String qry = "select estatus from parametrossistema where ic_moneda = "+ ic;
						try{
							con.conexionDB();
							rs = con.queryDB(qry);
							if(rs.next()){
								estat = rs.getString("estatus");
								if(estat.equals("1")){
									pintar = false;
								}else
									pintar = true;
							}
							rs.close();
						}catch(Exception e){
							e.printStackTrace();
						}finally{
							if(con.hayConexionAbierta()){
								con.cierraConexionDB();
							}
						}
				}
				if(pintar){
					datos=new HashMap();
						datos.put("FECHA_SOLIC",autSol.getFecha_solicitud());
						datos.put("FECHA_COT",autSol.getFecha_cotizacion());
						datos.put("EJECUTIVO",autSol.getEjecutivo());
						datos.put("NUM_SOLIC",autSol.getNum_solicitud());
						datos.put("MONEDA",autSol.getMoneda());
						datos.put("MONTO","$"+Comunes.formatoDecimal(autSol.getFn_monto(),2));
						datos.put("PLAZO",autSol.getPlazo_oper()+" días");
						datos.put("IC_SOLIC",autSol.getIc_solic_esp());
						reg.add(datos);
				}
		
			}
			jsonObj.put("tipoUsuario",new Boolean(!strTipoUsuario.equals("IF")));
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("registros", reg);
			infoRegresar=jsonObj.toString();
                        
		}else if(informacion.equals("consultaCotizacion")){
                
			Solicitud consulta = new Solicitud();
			String ic_solic_esp = request.getParameter("ic_solic");
			String periodo = "";
			String tec = "";
			boolean mismoDiaOk;
			int     valHoraToma = 0;
			SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
			AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
			boolean ok=false;
			String estat = "";
			ResultSet rs = null;
			AccesoDB con = new AccesoDB();
			String qry = "select NVL(cs_linea,'N') as linea from cot_solic_esp where ic_solic_esp = "+ic_solic_esp;
				try{
					con.conexionDB();
					rs = con.queryDB(qry);
					if(rs.next()){
						estat = rs.getString("linea");
						if(estat.equals("N"))
							ok = true;
						else
							ok = false;
					}
					rs.close();
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					if(con.hayConexionAbierta()){
						con.cierraConexionDB();
					}
				}
				
			autSol.setEstatusSolic(ic_solic_esp,"5","2");
			consulta = solCotEsp.consultaSolicitud(ic_solic_esp);
			if("3".equals(consulta.getIc_tipo_tasa())){
				tec = solCotEsp.techoTasaProtegida();
			}
			String clase="";
			int tipoClase=0;
			HashMap hmDatos = autSol.DiasParaSolicRecursos(ic_solic_esp,consulta.getIc_moneda(),consulta.getIc_if());
			System.out.println("******getCs_vobo_ejec:"+consulta.getCs_vobo_ejec()+"*");
			System.out.println("******getTipoCotizacion:"+consulta.getTipoCotizacion()+"*");
			System.out.println("******MOSTRAR_TF:"+hmDatos.get("MOSTRAR_TF")+"*");
			System.out.println("******Hora: "+hmDatos.get("HORA")+"*");
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("numeroSolic", consulta.getNumero_solicitud());
			jsonObj.put("fechaSolic", consulta.getDf_solicitud());
			jsonObj.put("horaSolic", consulta.getHora_solicitud());
			jsonObj.put("fechaCot",consulta.getDf_cotizacion() );
			jsonObj.put("horaCot", consulta.getHora_cotizacion());
			jsonObj.put("icUsuario", consulta.getIc_usuario()+"-"+consulta.getNombre_usuario());
			jsonObj.put("Intermediario", strNombre);
			jsonObj.put("ejecutivo", consulta.getNombre_ejecutivo());
			jsonObj.put("correo", consulta.getCg_mail());
			jsonObj.put("telefono", consulta.getCg_telefono());
			jsonObj.put("fax", consulta.getCg_fax());
			jsonObj.put("programa", consulta.getCg_programa());
			jsonObj.put("monto", Comunes.formatoDecimal(consulta.getFn_monto(),2)+(consulta.getIc_moneda().equals("1")?" Pesos":" Dólares"));
			jsonObj.put("tasa", consulta.getTipo_tasa());
			jsonObj.put("numDisp", consulta.getIn_num_disp());
			jsonObj.put("unica", ("S".equals(consulta.getCs_tasa_unica())?"Unica":"Varias"));
			jsonObj.put("plazo", consulta.getIg_plazo()+" Días");
			jsonObj.put("esquema", consulta.getEsquema_recup());
			jsonObj.put("icEsquema", consulta.getIc_esquema_recup());
			jsonObj.put("pInteres",consulta.periodoTasa(consulta.getCg_ut_ppi()) );
			jsonObj.put("pCapital", consulta.periodoTasa(consulta.getCg_ut_ppc()));
			jsonObj.put("fechaPagoCap", consulta.getCg_pfpc());
			jsonObj.put("fechaPagoCap2",consulta.getCg_pufpc() );
			
			HashMap datos;
			List reg=new ArrayList();
			for(int i=0;i<Integer.parseInt(consulta.getIn_num_disp());){
					datos=new HashMap();
					i++;
					datos.put("NO_DISP",i+"");
					datos.put("MONTO","$"+Comunes.formatoDecimal(consulta.getFn_monto_disp(i-1),2));
					datos.put("FECHA_DISP",consulta.getDf_disposicion(i-1));
					datos.put("FECHA_VENC",consulta.getDf_vencimiento(i-1));
					reg.add(datos);
			}
			jsonObj.put("registros", reg);
			
			reg=new ArrayList();
			 ArrayList alFilas=new ArrayList();
			if((consulta.getPagos()).size() > 0)
				alFilas = consulta.getPagos();
			for(int i=0;i<alFilas.size();i++){
					ArrayList alColumnas = (ArrayList)alFilas.get(i);
					datos=new HashMap();
					datos.put("NO_PAGO",(String)alColumnas.get(0));
					datos.put("FECHA_PAGO",(String)alColumnas.get(1));
					datos.put("MONTO","$"+Comunes.formatoDecimal(alColumnas.get(2).toString(),2));
					reg.add(datos);
			}
			jsonObj.put("registros2", reg);
			
                      
                        
			
			 if("1".equals(consulta.getIc_esquema_recup())){
						periodo = "Al Vencimiento";
			 }else if("4".equals(consulta.getIc_esquema_recup())&&"0".equals(consulta.getCg_ut_ppc())){
						periodo = "Al Vencimiento";                                             
			 }else if( !consulta.periodoTasa(consulta.getCg_ut_ppi()).equals("") ){
						periodo = consulta.periodoTasa(consulta.getCg_ut_ppi())+"mente";
			 }
			 else if( !consulta.periodoTasa(consulta.getCg_ut_ppc()).equals("") ){
						periodo = consulta.periodoTasa(consulta.getCg_ut_ppc())+"mente";
			 }else{
						periodo = "Al Vencimiento";
			 }
			 String mensaje="";
			 if("I".equals(consulta.getTipoCotizacion())){
				mensaje="COTIZACION INDICATIVA, POR EL MOMENTO NO ES POSIBLE TOMAR EN FIRME LA OPERACION, FAVOR DE CONSULTAR A SU EJECUTIVO.";
			 }else if("N".equals(consulta.getCs_vobo_ejec())){
				mensaje="Para realizar la toma en firme favor de ponerse en contacto con su ejecutivo";
			 }
				
				
				valHoraToma = autSol.validaHoraToma(ic_solic_esp);
				jsonObj.put("valHoraToma", valHoraToma+"");
				
				
				
				
				
				if(!consulta.getCg_observaciones().trim().equals("") ){
					jsonObj.put("observaciones","Observaciones<br/>"+consulta.getCg_observaciones());
				}else{
					jsonObj.put("observaciones","");
				}
				String mensajeTasa ="";
                                boolean boton=false;                              
                                                                                                
                                
                             //REQUEMIENTO 11 consultaCotizacion
                            boolean  validaHorario =  autSol.esAntesFinConfirmacion();       //valida el horario parametrizado                            
                            boolean mismoDia =  autSol.mismoDiaFCotizacion(ic_solic_esp); //si fecha de cotización es mismo día  toma firme
                            String fechaDisposicion =  autSol.fechaDisposicion( ic_solic_esp);
                            int fcotiVSFDispo =   autSol.validaFechaCotiVsDispo(ic_solic_esp, fechaDisposicion ); //fecha disposición contra  fecha cotizacion
                             //1 = mismo dia   2 =  Fecha cotización es menor  fecha disposición
                             
                            Fecha  com = new Fecha();  
                            int  diasDif=   com.restaFechas(fechaDisposicion,  consulta.getDf_cotizacion()) ;                          
                            int maxFDisposicion =  autSol.maximoFDisposicion(ic_solic_esp );
                          
                            LOG.debug ("ic_solic_esp ==> "+ic_solic_esp );
                            LOG.debug ("validaHorario  ==>  "+validaHorario );
                            LOG.debug ("fcotiVSFDispo  ==>  "+fcotiVSFDispo );
                            LOG.debug ("mismoDia  ==>  "+mismoDia );                          
                            LOG.debug ("diasDif  ==>  "+diasDif );
                            LOG.debug ("maxFDisposicion  ==>  "+maxFDisposicion );
                                                        
                             // Mismo día 
                            if(mismoDia ==true && validaHorario==true &&  fcotiVSFDispo==1  & diasDif ==0) {
                                boton=true; 
                                mensajeTasa="Tasa con oferta mismo día:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+consulta.getDescTasaIndicativa()+"&nbsp;"+consulta.getIg_tasa_md()+" % anual pagadera "+periodo+" "+tec;
			
                            }else   if(mismoDia ==true && validaHorario==false  && fcotiVSFDispo==1  & diasDif ==0 ) {
                                boton=false;  
                                mensajeTasa  ="No es posible ejecutar la toma en firme debido a que no se encuentra dentro de los parámetros para ejecutar la operación. Favor de solicitar una nueva cotización.";
                            
                            } else  if(mismoDia ==true && validaHorario==true  && fcotiVSFDispo==2  && diasDif <0 ) {  
                                boton=true; 
                                mensajeTasa="Tasa con oferta mismo día:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+consulta.getDescTasaIndicativa()+"&nbsp;"+consulta.getIg_tasa_md()+" % anual pagadera "+periodo+" "+tec;
			
                             } else  if(mismoDia ==true && validaHorario==false && fcotiVSFDispo==2 && diasDif <0 ) {  
                                boton=true; 
                                mensajeTasa="Tasa con oferta 48 horas:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+consulta.getDescTasaIndicativa()+consulta.getIg_tasa_spot()+"% anual pagadera "+periodo+" "+tec;
                             
                             //consultar una cotización de días anteriores 
                            
                            }else  if(mismoDia ==false  &&  validaHorario==true &&  maxFDisposicion ==1 && fcotiVSFDispo==2 && diasDif <0 ) {  
                               boton=true;  
                               mensajeTasa="Tasa con oferta 48 horas:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+consulta.getDescTasaIndicativa()+consulta.getIg_tasa_spot()+"% anual pagadera "+periodo+" "+tec;
                            
                            }else  if(mismoDia ==false  &&  validaHorario==false &&  maxFDisposicion ==1  && fcotiVSFDispo==2 && diasDif <0 ) {  
                              boton=false;  
                              mensajeTasa="No es posible tomar en firme debido a que excedió los días permitidos para realizar la operación. Favor de solicitar una nueva cotización.";
                         
                            }else  if(mismoDia ==false  &&   (maxFDisposicion ==1  || maxFDisposicion ==3)  && fcotiVSFDispo==2 && diasDif <0 ) {  
                              boton=true;  
                              mensajeTasa="Tasa con oferta 48 horas:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+consulta.getDescTasaIndicativa()+consulta.getIg_tasa_spot()+"% anual pagadera "+periodo+" "+tec;
                            
                            }                            
                                
                               jsonObj.put("mensaje", mensaje); 
                                jsonObj.put("boton",new Boolean(boton));
				jsonObj.put("mensajeTasa",mensajeTasa);
				
				String mensaje2;
				if(hmDatos.get("DIA").equals("HOY")){
						 mensaje2="Una vez Confirmada su cotizaci&oacute;n podr&aacute; solicitar los recursos"
						 +" en Nafin Electr&oacute;nico hasta el dia de HOY a las  "+hmDatos.get("HORA")+" hrs."
						 +" de lo contrario aplicar&aacute; una comisi&oacute;n por no disposici&oacute;n de recursos.";
						 }else{
						 mensaje2="Una vez Confirmada su cotizaci&oacute;n podr&aacute; solicitar los recursos"+
						 " en Nafin Electr&oacute;nico hasta  "+hmDatos.get("HORA")+" hrs. del  "+hmDatos.get("DIA")+" de "+hmDatos.get("DESC_MES")+" de  "+hmDatos.get("ANIO")+","+
						 " de lo contrario aplicar&aacute; una comisi&oacute;n por no disposici&oacute;n de recursos";	
						 }
				jsonObj.put("mensaje2", mensaje2);
				String textoFirmar="Número de Solicitud:"+ consulta.getNumero_solicitud()+"\n"+
                        "Monto: "+consulta.getFn_monto()+"\n"+
                        "Fecha de Solicitud:"+consulta.getDf_solicitud()+"\n"+
                        "Hora de Solicitud:"+consulta.getHora_solicitud()+"\n"+
                        "Fecha de Cotización:"+consulta.getDf_cotizacion()+"\n"+
                        "Hora de Cotización:"+consulta.getHora_cotizacion()+"\n"+
                        "Número y Nombre de usuario:"+iNoUsuario+"-"+strNombreUsuario;
				jsonObj.put("textoFirmar", textoFirmar);
				infoRegresar=jsonObj.toString();
		
                }else if ("validaAnteFirme".equals(informacion)   ) { 
               
                    jsonObj = new JSONObject();
                    
                    AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
                    Solicitud consulta = new Solicitud();
                    
                    String ic_solic_esp = request.getParameter("ic_solic");
                    String valHoraTomaAnt 	= (request.getParameter("valHoraToma")==null)?"":request.getParameter("valHoraToma");
                    int 	valHoraToma = 0;
                    valHoraToma = autSol.validaHoraToma(ic_solic_esp);
                    //REQUEMIENTO 11 Firma   
                    boolean tomaFirmemismoDia =false;     
                    boolean  validaHorario =  autSol.esAntesFinConfirmacion();       //valida el horario parametrizado                            
                    boolean mismoDia =  autSol.mismoDiaFCotizacion(ic_solic_esp); //si fecha de cotización es mismo día  toma firme
                    String fechaDisposicion =  autSol.fechaDisposicion( ic_solic_esp);
                    int fcotiVSFDispo =   autSol.validaFechaCotiVsDispo(ic_solic_esp, fechaDisposicion ); //fecha disposición contra  fecha cotizacion
                    //1 = mismo dia   2 =  Fecha cotización es menor  fecha disposición
                             
                    Fecha  com = new Fecha();  
                    int  diasDif=   com.restaFechas(fechaDisposicion,  consulta.getDf_cotizacion()) ;                          
                    int maxFDisposicion =  autSol.maximoFDisposicion(ic_solic_esp );
                          
                    LOG.debug ("ic_solic_esp  ==>  "+ic_solic_esp );
                    LOG.debug ("validaHorario ==>  "+validaHorario );
                    LOG.debug ("fcotiVSFDispo  ==>  "+fcotiVSFDispo );
                    LOG.debug ("mismoDia  ==>  "+mismoDia );                          
                    LOG.debug ("diasDif  ==>  "+diasDif );
                    LOG.debug ("maxFDisposicion  ==>  "+maxFDisposicion );
                                                        
                    // Mismo día 
                    if(mismoDia ==true && validaHorario==true &&  fcotiVSFDispo==1  & diasDif ==0) {
                        tomaFirmemismoDia =true;                            
                    } else  if(mismoDia ==true && validaHorario==true  && fcotiVSFDispo==2  && diasDif <0 ) {  
                        tomaFirmemismoDia =true; 
                    } else  if(mismoDia ==true && validaHorario==false && fcotiVSFDispo==2 && diasDif <0 ) {  
                        tomaFirmemismoDia =false; 
                        //consultar una cotización de días anteriores                             
                    }else  if(mismoDia ==false  &&  validaHorario==true &&  maxFDisposicion ==1 && fcotiVSFDispo==2 && diasDif <0 ) {  
                        tomaFirmemismoDia =false; 
                    }else  if(mismoDia ==false  &&   (maxFDisposicion ==2  || maxFDisposicion ==3)  && fcotiVSFDispo==2 && diasDif <0 ) {  
                        tomaFirmemismoDia =false; 
                    }                       
                
                    if(mismoDia ==true && validaHorario==false  && fcotiVSFDispo==1  & diasDif ==0 ) {                                                      
                    jsonObj.put("success", new Boolean(false));                                                        
                    jsonObj.put("Error","No es posible ejecutar la toma en firme debido a que no se encuentra dentro de los parámetros para ejecutar la operación. Favor de solicitar una nueva cotización.");
                                              
                    }else if(mismoDia ==false  &&  validaHorario==false &&  maxFDisposicion ==1  && fcotiVSFDispo==2 && diasDif <0 ) {   
                              
                        jsonObj.put("success", new Boolean(false));                                                        
                        jsonObj.put("Error","No es posible tomar en firme debido a que excedió los días permitidos para realizar la operación. Favor de solicitar una nueva cotización.");
                    }else      if(valHoraToma==2&&valHoraTomaAnt.equals("1")) {
                        jsonObj.put("success", new Boolean(false));										
			jsonObj.put("Error", "Excedió el tiempo para la confirmación de la operación");
							  		
                    }else if(valHoraToma==4&&valHoraTomaAnt.equals("3")){
                        jsonObj.put("success", new Boolean(false));									
			jsonObj.put("Error", "Excedió el tiempo para la confirmación de la operación, favor de cotizar nuevamente");
                    }else  {
                     jsonObj.put("success", new Boolean(true));									
                     jsonObj.put("Error","");
                    }
                        
                    infoRegresar=jsonObj.toString();                          
                  
                   
                }else if(informacion.equals("Firma")){
			try{
					Solicitud consulta = new Solicitud();
					String ic_solic_esp = request.getParameter("ic_solic");
					String valHoraTomaAnt 	= (request.getParameter("valHoraToma")==null)?"":request.getParameter("valHoraToma");
					//String origen = (request.getParameter("origen")==null)?"":request.getParameter("origen");
					String periodo = "";
					String tec = "";
					SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);
					AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
					autSol.setEstatusSolic(ic_solic_esp,"5","2");
					consulta = solCotEsp.consultaSolicitud(ic_solic_esp);
					
					String pkcs7 = (request.getParameter("pkcs7")==null)?"":request.getParameter("pkcs7");
					String serial = "";
					String folioCert = "";
					String externContent = (request.getParameter("textoFirmado")==null)?"":request.getParameter("textoFirmado");
					//FODEA023-2010 FVR
					//externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
					char getReceipt = 'Y';
					String _acuse = "500";
					int 	valHoraToma = 0;
							try	{
							 //java.security.cert.X509Certificate certs [] = (java.security.cert.X509Certificate []) request.getAttribute("javax.servlet.request.X509Certificate");
							 /*EGOYjava.security.cert.X509Certificate certs [] 	  = (java.security.cert.X509Certificate [])	request.getAttribute("javax.servlet.request.X509Certificate");
								for(int _i = 0; _i<certs.length; _i++) {
                                                                    if(strSerial.length()>20){
                                                                        System.out.println("SHA2---------------------------"+certs[_i].getSerialNumber().toString(16) );
                                                                        _serial=""+certs[_i].getSerialNumber().toString(16);
                                                                    }else{
                                                                        byte x [] = certs[_i].getSerialNumber().toByteArray();
                                                                        _serial = new String (x);
                                                                    }
								}*/
							}
							catch(Exception e) {
								System.out.println(e+"\nError al leer el certificado");
							}
							                                                        
							valHoraToma = autSol.validaHoraToma(ic_solic_esp);
                                                        
                       //REQUEMIENTO 11 Firma   
                            boolean tomaFirmemismoDia =false;     
                            boolean  validaHorario =  autSol.esAntesFinConfirmacion();       //valida el horario parametrizado                            
                            boolean mismoDia =  autSol.mismoDiaFCotizacion(ic_solic_esp); //si fecha de cotización es mismo día  toma firme
                            String fechaDisposicion =  autSol.fechaDisposicion( ic_solic_esp);
                            int fcotiVSFDispo =   autSol.validaFechaCotiVsDispo(ic_solic_esp, fechaDisposicion ); //fecha disposición contra  fecha cotizacion
                             //1 = mismo dia   2 =  Fecha cotización es menor  fecha disposición
                             
                            Fecha  com = new Fecha();  
                            int  diasDif=   com.restaFechas(fechaDisposicion,  consulta.getDf_cotizacion()) ;                          
                            int maxFDisposicion =  autSol.maximoFDisposicion(ic_solic_esp );
                          
                            LOG.debug ("ic_solic_esp  ==>  "+ic_solic_esp );
                            LOG.debug ("validaHorario ==>  "+validaHorario );
                            LOG.debug ("fcotiVSFDispo  ==>  "+fcotiVSFDispo );
                            LOG.debug ("mismoDia  ==>  "+mismoDia );                          
                            LOG.debug ("diasDif  ==>  "+diasDif );
                            LOG.debug ("maxFDisposicion  ==>  "+maxFDisposicion );
                                                        
                             // Mismo día 
                            if(mismoDia ==true && validaHorario==true &&  fcotiVSFDispo==1  & diasDif ==0) {
                               tomaFirmemismoDia =true;                            
                            } else  if(mismoDia ==true && validaHorario==true  && fcotiVSFDispo==2  && diasDif <0 ) {  
                               tomaFirmemismoDia =true; 
                            } else  if(mismoDia ==true && validaHorario==false && fcotiVSFDispo==2 && diasDif <0 ) {  
                               tomaFirmemismoDia =false; 
                             //consultar una cotización de días anteriores                             
                            }else  if(mismoDia ==false  &&  validaHorario==true &&  maxFDisposicion ==1 && fcotiVSFDispo==2 && diasDif <0 ) {  
                                tomaFirmemismoDia =false; 
                            }else  if(mismoDia ==false  &&   (maxFDisposicion ==2  || maxFDisposicion ==3)  && fcotiVSFDispo==2 && diasDif <0 ) {  
                                tomaFirmemismoDia =false; 
                            }                       
                                                
                                                       
                                            if(mismoDia ==true && validaHorario==false  && fcotiVSFDispo==1  & diasDif ==0 ) {                                                      
                                                jsonObj.put("success", new Boolean(false));                                                        
						jsonObj.put("Error","No es posible ejecutar la toma en firme debido a que no se encuentra dentro de los parámetros para ejecutar la operación. Favor de solicitar una nueva cotización.");
                                              
                                              }else if(mismoDia ==false  &&  validaHorario==false &&  maxFDisposicion ==1  && fcotiVSFDispo==2 && diasDif <0 ) {   
                              
                                                jsonObj.put("success", new Boolean(false));                                                        
                                                jsonObj.put("Error","No es posible tomar en firme debido a que excedió los días permitidos para realizar la operación. Favor de solicitar una nueva cotización.");
                            
                            
                                             }else      if(valHoraToma==2&&valHoraTomaAnt.equals("1")) {
									//if(confirm("Excedió el tiempo para la confirmación de la operación, ¿desea tomar en firme la Tasa 48 horas?")) {
									
										jsonObj.put("success", new Boolean(false));										
										jsonObj.put("Error", "Excedió el tiempo para la confirmación de la operación");
										//location.href="22forma1a.jsp?ic_solic_esp=<%=ic_solic_esp";
									
										//location.href="22forma1.jsp";
							  		
						}else if(valHoraToma==4&&valHoraTomaAnt.equals("3")){
								
									//alert("Excedió el tiempo para la confirmación de la operación, favor de cotizar nuevamente");
									jsonObj.put("success", new Boolean(false));									
									jsonObj.put("Error", "Excedió el tiempo para la confirmación de la operación, favor de cotizar nuevamente");
									infoRegresar=jsonObj.toString();
                                                                        
                                                                        LOG.debug ("paso 3 " );  
							}else if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {                                       
                                serial=_serial;
                                                                serial=_serial;
								folioCert = "TOMAENFIRME"+consulta.getNumero_solicitud();
								netropology.utilerias.Seguridad s = new netropology.utilerias.Seguridad();
								
								if (s.autenticar(folioCert, serial, pkcs7, externContent, getReceipt)) {
									_acuse = s.getAcuse();
									autSol.tomaEnFirmeIF(ic_solic_esp,_acuse, tomaFirmemismoDia);
								}else{
									System.out.println("22forma1ext.data.jsp(Exception): " +
											s.mostrarError());
									throw new NafinException("GRAL0021");
								}
							}else{
                                                       
								/*System.out.println("22forma1ext.data.jsp(Exception):" +
										"!_serial.equals(\"\")=" + (!_serial.equals("")) + 
										"|!externContent.equals(\"\")=" + (!externContent.equals("")) +
										"|!pkcs7.equals(\"\")=" + (!pkcs7.equals("")) );
								throw new NafinException("GRAL0021");
                                                                */
							}
					consulta = solCotEsp.consultaSolicitud(ic_solic_esp);
					
					if("3".equals(consulta.getIc_tipo_tasa())){
						tec = solCotEsp.techoTasaProtegida();
					}
					
                                          
					if(!"".equals(folioCert)){
                                            Map<String,Object> datos = new HashMap<>();
                                            
                                            datos.put("icSolicitud", ic_solic_esp);
                                            datos.put("recibo", _acuse);
                                            datos.put("strDirectorioTemp", strDirectorioTemp);
                                            datos.put("strPais", (String)session.getAttribute("strPais"));
                                            datos.put("iNoNafinElectronico", session.getAttribute("iNoNafinElectronico").toString());
                                            datos.put("sesExterno", (String)session.getAttribute("sesExterno"));
                                            datos.put("strNombre", (String)session.getAttribute("strNombre"));
                                            datos.put("strNombreUsuario", (String)session.getAttribute("strNombreUsuario"));
                                            datos.put("strLogo", (String)session.getAttribute("strLogo"));
                                            datos.put("strDirectorioPublicacion", strDirectorioPublicacion);						
                                            datos.put("strTipoUsuario", strTipoUsuario);
                                            datos.put("origen", "TF");					
                                            datos.put("pantalla", "CotizaFirme");	
                                            datos.put("remitente", "cotizador@nafin.com");
                                            datos.put("asunto", "Tasa en Firme");
                                            
                                            
                                            try{							
                                                solCotEsp.enviaCorreoCotizador(datos);
                                            
                                            }catch(Exception e){
                                                jsonObj.put("Error", "Hubo un error  al enviar correo ");
                                            }	
					
                                            jsonObj.put("success", new Boolean(true));                                           
                                            jsonObj.put("Fecha",consulta.getDf_aceptacion());
                                            jsonObj.put("recibo",_acuse);
                                            jsonObj.put("Exito", "La autentificaci&oacute;n  se llev&oacute; a cabo con &eacute;xito<br>Recibo:"+_acuse);
                                        }
                                        
					infoRegresar=jsonObj.toString();                                        
                                        
			}catch(NafinException ne){
				System.out.println(ne.getMsgError());
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("success2", new Boolean(false));
				jsonObj.put("Error", ne.getMsgError());
				infoRegresar=jsonObj.toString();
			}
					
					
		}
	
	
	%>
	
	<%=infoRegresar%>
        
        
       