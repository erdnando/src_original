Ext.onReady(function(){

//**************************textoCompleto
var textoCompleto = function (value, metadata, record, rowIndex, colIndex, store){
  metadata.attr = 'style="white-space: normal; word-wrap: break-word; "';
  return value;
};
//**************************textoCompleto

//------------------------------------------------------------ procesarFecha ----------------------------------------------------------------
function procesarFecha(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
		var info = Ext.util.JSON.decode(response.responseText);
		Ext.getCmp('fechaRegistro').setValue(info.fechaHoy);
		Ext.getCmp('fechaFinRegistro').setValue(info.fechaHoy);
	}
};
//------------------------------------------------------------ Fin procesarFecha ------------------------------------------------------------

//---------------------------------------------------------------------Moneda----------------------------------------------------------------
var Moneda = Ext.data.Record.create([ 
 {name: "CLAVE", type: "string"}, 
 {name: "DESCRIPCION", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//------------------------------------------------------------------Fin Moneda---------------------------------------------------------------

//--------------------------------------------------------------- procesarMoneda -------------------------------------------------------------
var procesarMoneda = function(store, arrRegistros, opts) {
	store.insert(0,new Moneda({ 
	CLAVE: "", 
	DESCRIPCION: "Seleccione Moneda", 
	loadMsg: ""})); 		
	Ext.getCmp('cmbmon').setValue("");
	store.commitChanges(); 
};	
//---------------------------------------------------------------Fin procesarMoneda-----------------------------------------------------------

//-----------------------------------------------------------------Catalogo Moneda-----------------------------------------------------------
var catalogoMoneda = new Ext.data.JsonStore({
  id					: 'catalogoMoneda',
  root 				: 'registros',
  fields 			: ['CLAVE', 'DESCRIPCION', 'loadMsg'],
  url 				: '22consulta1Ext01.data.jsp',
  baseParams		: {
	 informacion	: 'catalogomon'  
						  },
  totalProperty 	: 'total',
  //autoLoad			: true,
  listeners			: {
	 load				: procesarMoneda,
	 beforeload		: NE.util.initMensajeCargaCombo,
	 exception		: NE.util.mostrarDataProxyError								
						 }
});//-----------------------------------------------------------Fin Catalogo Moneda----------------------------------------------------------

//-----------------------------------------------------------------Intermediario----------------------------------------------------------------
var Intermediario = Ext.data.Record.create([ 
 {name: "CLAVE", type: "string"}, 
 {name: "DESCRIPCION", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//------------------------------------------------------------------Fin Intermediario---------------------------------------------------------------

//--------------------------------------------------------------- procesarInter -------------------------------------------------------------
var procesarInter = function(store, arrRegistros, opts) {
	store.insert(0,new Intermediario({ 
	CLAVE: "", 
	DESCRIPCION: "Seleccione Intermediario", 
	loadMsg: ""})); 		
	Ext.getCmp('cmbti').setValue(""); 
	store.commitChanges(); 
};	
//---------------------------------------------------------------Fin procesarInter-----------------------------------------------------------

//--------------------------------------------------------------Catalogo Intermediario------------------------------------------------------
var catalogoIntermediario = new Ext.data.JsonStore({
  id					: 'catalogoIntermediario',
  root 				: 'registros',
  fields 			: ['CLAVE', 'DESCRIPCION', 'loadMsg'],
  url 				: 'AdmonUsuariosExt01.data.jsp',
  baseParams		: {
   informacion		: 'catalogoint'  
							},
  totalProperty 	: 'total',
  autoLoad			: true,
  listeners			: {			
   load				: procesarInter,
   beforeload		: NE.util.initMensajeCargaCombo,
   exception		: NE.util.mostrarDataProxyError								
              }
});//---------------------------------------------------------Fin Catalogo Intermediario-------------------------------------------------------


//-------------------------------------------------------------------- Estatus -------------------------------------------------------------
var Estatus = Ext.data.Record.create([ 
 {name: "CLAVE", type: "string"}, 
 {name: "DESCRIPCION", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//------------------------------------------------------------------Fin Estatus---------------------------------------------------------------

//--------------------------------------------------------------- procesarEstatus -------------------------------------------------------------
var procesarEstatus = function(store, arrRegistros, opts) {
	store.insert(0,new Estatus({ 
	CLAVE: "", 
	DESCRIPCION: "Seleccione un Estatus", 
	loadMsg: ""})); 		
	Ext.getCmp('cmbTipo').setValue(""); 
	store.commitChanges(); 
};	
//---------------------------------------------------------------Fin procesarEstatus --------------------------------------------------------

//---------------------------------------------------------------- Catalogo estatus ---------------------------------------------------------
var estatus = new Ext.data.JsonStore({
  id					: 'catalogoEstatus',
  root 				: 'registros',
  fields 			: ['CLAVE', 'DESCRIPCION', 'loadMsg'],
  url 				: '22consulta1Ext01.data.jsp',
  baseParams		: {
   informacion		: 'catalogoEstatus'  
							},
  totalProperty 	: 'total',
  //autoLoad			: true,
  listeners			: {			
   load				: procesarEstatus,
   beforeload		: NE.util.initMensajeCargaCombo,
   exception		: NE.util.mostrarDataProxyError								
              }
});//--------------------------------------------------------- Fin Catalogo estatus ---------------------------------------------------------

//-------------------------------------------------------------------- Persona -------------------------------------------------------------
var Persona = Ext.data.Record.create([ 
 {name: "CLAVE", type: "string"}, 
 {name: "DESCRIPCION", type: "string"}, 
 {name: "loadMsg", type: "string"}
]);
//------------------------------------------------------------------Fin Persona---------------------------------------------------------------

//--------------------------------------------------------------- procesarPersona -------------------------------------------------------------
var procesarPersona = function(store, arrRegistros, opts) {
Ext.getCmp('cmbPer').setValue(1);  
};	
//---------------------------------------------------------------Fin procesarPersona --------------------------------------------------------

//---------------------------------------------------------------- catalogoPersona ---------------------------------------------------------
var catalogoPersona = new Ext.data.JsonStore({
  id					: 'catalogoPersona',
  root 				: 'registros',
  fields 			: ['CLAVE', 'DESCRIPCION', 'loadMsg'],
  url 				: 'AdmonUsuariosExt01.data.jsp',
  baseParams		: {
   informacion		: 'catalogoPersona'  
							},
  totalProperty 	: 'total',
  autoLoad			: true,
  listeners			: {			
   load				: procesarPersona,
   beforeload		: NE.util.initMensajeCargaCombo,
   exception		: NE.util.mostrarDataProxyError								
              }
});//--------------------------------------------------------- Fin catalogoPersona ---------------------------------------------------------

//----------------------------------------------------------- Procesara Descarga Archivos---------------------------------------------------
function procesarDescargaArchivos(opts, success, response) {
  if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
    var infoR = Ext.util.JSON.decode(response.responseText);
    var archivo = infoR.urlArchivo;				
    archivo = archivo.replace('/nafin','');
    var params = {nombreArchivo: archivo};				
    fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
    fp.getForm().getEl().dom.submit();
    fp.el.unmask();
    var el = gridConsulta.getGridEl();
    el.unmask();
	 var btncsv = Ext.getCmp('btnArchivoCSV');
	 btncsv.setIconClass('icoXls');     
    btncsv.setDisabled(false);
    var btnpdf = Ext.getCmp('btnArchivoPDF');
	 btnpdf.setIconClass('icoPdf');     
    btnpdf.setDisabled(false);	

  }else {
    NE.util.mostrarConnError(response,opts);
    fp.el.unmask();
  }
};
//------------------------------------------------------------------ Fin Descarga Archivos -----------------------------------------------


//------------------------------------------------------------------- procesarConsultaData --------------------------------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  var fp = Ext.getCmp('formcon');
  fp.el.unmask();
  var jsonData = store.reader.jsonData;
  var gridConsulta = Ext.getCmp('gridConsulta');	
  var el = gridConsulta.getGridEl();	

  if (arrRegistros != null) {
    if (!gridConsulta.isVisible()) {
      gridConsulta.show();
    }	
    if(store.getTotalCount() > 0) {//////////Verifica que existan registros
      Ext.getCmp('btnArchivoPDF').enable();
      Ext.getCmp('btnArchivoCSV').enable();
      el.unmask();					

      
    } else {	
      Ext.getCmp('btnArchivoPDF').disable();
      Ext.getCmp('btnArchivoCSV').disable();
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
};
//-----------------------------------------------------procesarConsultaData-----------------------------------------------------------------

//------------------------------------------------------Consulta Data------------------------------------------------------------------
var consultaData   = new Ext.data.JsonStore({ 
  root : 'registros',
  url : 'AdmonUsuariosExt01.data.jsp',
  baseParams: {
    informacion: 'ConsultarAsignacion'
  },		
  fields: [	
    {	name: 'CLAVE'},
    {	name: 'DESCRIPCION'},	
    {	name: 'CLAVE_INTER'},
    {	name: 'DESC_INTER'}	
  ],		
  totalProperty : 'total',
  messageProperty: 'msg',
  //autoLoad: false,
  listeners: {
    load: procesarConsultaData,
    exception: {
      fn: function(proxy, type, action, optionsRequest, response, args) {
        NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
        //LLama procesar consulta, para que desbloquee los componentes.
        procesarConsultaData(null, null, null);					
      }
    }
  }		
});
//---------------------------------------------------------Fin ConsultaData-------------------------------------------------------------


//----------------------------------------------------------- Store infoUno -------------------------------------------------------------
var infoUno = new Ext.data.ArrayStore({ //new Ext.data.JsonStore({
  autoDestroy: true,
  storeId: 'infoUno',
  idIndex: 0, 
	 fields		: [
			  {name : 'uno'},
			  {name : 'dos' }
			  ]
	});
//----------------------------------------------------------- Fin Store infoUno -------------------------------------------------------------

//--------------------------------------------------------------  gridUno -------------------------------------------------------------
var gridUno = {// new Ext.grid.EditorGridPanel({
	xtype: 'grid',
	id: 'gridUno',
	store: infoUno,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: '<center>Datos de la Cotizaci�n</center>',
	height: 185,
	width: 480,
	hidden:false,
	stripeRows: true,
	columns: [
		{
		width : 180 ,
		dataIndex: 'uno',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 287 ,
		dataIndex: 'dos',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridUno -------------------------------------------------------------
	
//-------------------------------------------------------------- Store infoDos -------------------------------------------------------------
	var infoDos = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoDos',
     idIndex: 0, 
		 fields		: [
				  ]
		});
//----------------------------------------------------------- Fin Store infoDos -------------------------------------------------------------

//--------------------------------------------------------------  gridDos -------------------------------------------------------------
var gridDos = {
	xtype: 'grid',
	id: 'gridDos',
	store: infoDos,
	title:'...',
	height: 55,
	width: 600,
	hidden:false,
	columns: [
		]
};
//-------------------------------------------------------------- Fin  gridDos -------------------------------------------------------------
	
//-------------------------------------------------------------- Store infoTres -------------------------------------------------------------
	var infoTres = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoTres',
     idIndex: 0, 
		 fields		: [
              {name : 'tres'},
              {name : 'cuatro' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoTres -------------------------------------------------------------

//--------------------------------------------------------------  gridTres -------------------------------------------------------------
var gridTres = {
	xtype: 'grid',
	id: 'gridTres',
	store: infoTres,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Datos del Ejecutivo Nafin',
	height: 125,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 180 ,
		dataIndex: 'tres',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 410 ,
		dataIndex: 'cuatro',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridTres -------------------------------------------------------------

//-------------------------------------------------------------- Store infoCuatro -------------------------------------------------------------
	var infoCuatro = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoCuatro',
     idIndex: 0, 
		 fields		: [
              {name : 'cinco'},
              {name : 'seis' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoCuatro -------------------------------------------------------------

//--------------------------------------------------------------  gridCuatro -------------------------------------------------------------
var gridCuatro = {
	xtype: 'grid',
	id: 'gridCuatro',
	store: infoCuatro,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Datos del Intermediario Financiero / Acreditado',
	height: 175,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 180 ,
		dataIndex: 'cinco',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 410 ,
		dataIndex: 'seis',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridCuatro -------------------------------------------------------------

//-------------------------------------------------------------- Store infoCinco -------------------------------------------------------------
	var infoCinco = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoCinco',
     idIndex: 0, 
		 fields		: [
              {name : 'siete'},
              {name : 'ocho' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoCinco -------------------------------------------------------------

//--------------------------------------------------------------  gridCinco -------------------------------------------------------------
var gridCinco = {
	xtype: 'grid',
	id: 'gridCinco',
	store: infoCinco,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Caracteristicas de la Operaci�n',
	height:250,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 220 ,
		dataIndex: 'siete',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 370 ,
		dataIndex: 'ocho',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridCinco -------------------------------------------------------------

//-------------------------------------------------------------- Store infoSeis -------------------------------------------------------------
	var infoSeis = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoSeis',
     idIndex: 0, 
		 fields		: [
              {name : 'nueve'},
              {name : 'diez' },
				  {name : 'once'},
              {name : 'doce' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoSeis -------------------------------------------------------------

//--------------------------------------------------------------  gridSeis -------------------------------------------------------------
var gridSeis = {
	xtype: 'grid',
	id: 'gridSeis',
	store: infoSeis,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Detalle de Disposiciones',
	height:80,
	width: 600,
	hidden:false,
	columns: [
		{
		header:'N�mero de Disposiciones',
		width : 150 ,
		dataIndex: 'nueve',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		header:'Monto',
		width : 145 ,
		dataIndex: 'diez',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		header:'Fecha de Disposiciones',
		width : 145 ,
		dataIndex: 'once',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		header:'Fecha de Vencimiento',
		width : 140 ,
		dataIndex: 'doce',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridSeis -------------------------------------------------------------


//-------------------------------------------------------------- Store infoSiete -------------------------------------------------------------
	var infoSiete = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoSiete',
     idIndex: 0, 
		 fields		: [
              {name : 'trece'},
              {name : 'catorce' },
				  {name : 'quince'},
              {name : 'dieciseis' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoSiete -------------------------------------------------------------

//--------------------------------------------------------------  gridSiete -------------------------------------------------------------
var gridSiete = {
	xtype: 'grid',
	id: 'gridSiete',
	store: infoSiete,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Plan de Pagos Capital',
	height:150,
	width: 600,
	hidden:false,
	columns: [
		{
		header:'N�mero de Pago',
		width : 150 ,
		dataIndex: 'trece',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		header:'Fecha de Pago',
		width : 145 ,
		dataIndex: 'catorce',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		header:'Monto a Pagar',
		width : 145 ,
		dataIndex: 'quince',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 140 ,
		dataIndex: 'dieciseis',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridSiete -------------------------------------------------------------

//-------------------------------------------------------------- Store infoOcho -------------------------------------------------------------
	var infoOcho = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoOcho',
     idIndex: 0, 
		 fields		: [
				  ]
		});
//----------------------------------------------------------- Fin Store infoOcho -------------------------------------------------------------

//--------------------------------------------------------------  gridOcho -------------------------------------------------------------
var gridOcho = {
	xtype: 'grid',
	id: 'gridOcho',
	store: infoOcho,
	title:'...',
	height: 42,
	width: 600,
	hidden:false,
	columns: [
		]
};
//-------------------------------------------------------------- Fin  gridOcho -------------------------------------------------------------

//-------------------------------------------------------------- Store infoNueve -------------------------------------------------------------
	var infoNueve = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoNueve',
     idIndex: 0, 
		 fields		: [
              {name : 'nueveUno'},
              {name : 'nueveDos' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoNueve -------------------------------------------------------------

//--------------------------------------------------------------  gridNueve -------------------------------------------------------------
var gridNueve = {
	xtype: 'grid',
	id: 'gridNueve',
	store: infoNueve,
	//plugins: grupoDolar,
	//clicksToEdit: 1,
	title: 'Tasa Indicativa',
	height:250,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 200 ,
		dataIndex: 'nueveUno',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 395 ,
		dataIndex: 'nueveDos',
		sortable: false,
		resizable: false,
		align: 'left',
		renderer: textoCompleto
		}
		]
};
//-------------------------------------------------------------- Fin  gridNueve -------------------------------------------------------------

//-------------------------------------------------------------- Store infoDiez -------------------------------------------------------------
	var infoDiez = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoDiez',
     idIndex: 0, 
		 fields		: [
              {name : 'diezUno'},
              {name : 'diezDos' }
				  ]
		});
//----------------------------------------------------------- Fin Store infoDiez -------------------------------------------------------------

//--------------------------------------------------------------  gridDiez -------------------------------------------------------------
var gridDiez = {
	xtype: 'grid',
	id: 'gridDiez',
	store: infoDiez,
	height:80,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 375 ,
		dataIndex: 'diezUno',
		sortable: false,
		resizable: false,
		align: 'left'
		},{
		width : 210 ,
		dataIndex: 'diezDos',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridDiez -------------------------------------------------------------

//-------------------------------------------------------------- Store infoOnce -------------------------------------------------------------
	var infoOnce = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'infoOnce',
     idIndex: 0, 
		 fields		: [
              {name : 'onceUno'}
				  ]
		});
//----------------------------------------------------------- Fin Store infoOnce -------------------------------------------------------------

//--------------------------------------------------------------  gridOnce -------------------------------------------------------------
var gridOnce = {
	xtype: 'grid',
	id: 'gridOnce',
	title:'Observaciones',
	store: infoOnce,
	height:80,
	width: 600,
	hidden:false,
	columns: [
		{
		width : 595 ,
		dataIndex: 'onceUno',
		sortable: false,
		resizable: false,
		align: 'left'
		}
		]
};
//-------------------------------------------------------------- Fin  gridOnce -------------------------------------------------------------


//------------------------------------------------------------ gridConsulta -----------------------------------------------------------------
var gridConsulta = new Ext.grid.EditorGridPanel({	
  store: consultaData,
  id: 'gridConsulta',
  margins: '20 0 0 0',		
  style: 'margin:0 auto;',
  title: '<center>Asignaci�n de Tipo de Riesgo e Intermediario </center>',
  clicksToEdit: 1,
  hidden: true,
  columns: [	
    {
      header: 'Intermediario',
      tooltip: 'Intermediario',
      dataIndex: 'DESCRIPCION',
      sortable: true,
      width: 400,			
      resizable: true,				
      align: 'center',
		renderer:function(value){
			return "<div align='left'>"+value+"</div>";
		}
    },
    {
      header: 'Tipo de Intermediario',
      tooltip: 'Tipo de Intermediario',
      dataIndex: 'DESC_INTER',
      sortable: true,
      width: 200,			
      resizable: true,				
      align: 'center'
    },{
		xtype		: 'actioncolumn',
      header: 'Editar',
      tooltip: 'Editar',
      dataIndex: '',
      sortable: true,
      width: 100,			
      resizable: true,				
      align: 'center',
		items		: [{
			getClass: function(value,metadata,record,rowIndex,colIndex,store){		
				this.items[0].tooltip = 'Editar';
					 return 'icoModificar';							
			},handler:function(record,rowIndex,colIndex,metadata,value) {
				var grid = Ext.getCmp('gridConsulta');				
				var registro = grid.getStore().getAt(rowIndex);
				var claveInt = registro.get('CLAVE'); 
				var tipoInt	 = registro.get('CLAVE_INTER'); 
				Ext.getCmp('cmbti').setValue(claveInt);
				Ext.getCmp('cmbPer').setValue(tipoInt);
				gridConsulta.hide();
			}
		}]
    }	
  ],			
  displayInfo: true,		
  emptyMsg: "No hay registros.",		
  loadMask: true,
  stripeRows: true,
  height: 430,
  width: 750,
  align: 'center',
  frame: true,
  bbar: {
    //xtype: 'paging',
    pageSize: 15,
    buttonAlign: 'left',
    id: 'barraPaginacion',
    displayInfo: false,
    store: consultaData,
    displayMsg: '{0} - {1} de {2}',
    emptyMsg: "No hay registros.",
    items: [
      '->','-',
        {
        xtype: 'button',
        text: 'Generar Informaci�n',					
        tooltip:	'Generar Archivo',
        iconCls: 'icoXls',
        id: 'btnArchivoCSV',
		  hidden: true,
        handler: function(boton, evento) {
        fp.el.mask('Generando Archivo...', 'x-mask-loading');
        var el = gridConsulta.getGridEl();	
        el.mask('Generando Archivo...', 'x-mask-loading');
       
        boton.setIconClass('loading-indicator');
        boton.setDisabled(true);
          var barraPaginacionA = Ext.getCmp("barraPaginacion");						
            Ext.Ajax.request({
            url: '22consulta1Ext01.data.jsp',
            params: Ext.apply(fp.getForm().getValues(),{							
              informacion: 'ArchivoCSV'
            }),
            success : function(response) {
              boton.setIconClass('icoXls');     
              boton.setDisabled(false);
            },	
            callback: procesarDescargaArchivos
          });						
        }
      },
      {
        xtype: 'button',
        text: 'Generar Archivo PDF',					
        tooltip:	'Imprimir',
        iconCls: 'icoPdf',
        id: 'btnArchivoPDF',
		  hidden:true,
        handler: function(boton, evento) {
        fp.el.mask('Generando Archivo...', 'x-mask-loading');
        var el = gridConsulta.getGridEl();	
        el.mask('Generando Archivo...', 'x-mask-loading');
       
        boton.setIconClass('loading-indicator');
        boton.setDisabled(true);
          var barraPaginacionA = Ext.getCmp("barraPaginacion");						
            Ext.Ajax.request({
            url: '22consulta1Ext01.data.jsp',
            params: Ext.apply(fp.getForm().getValues(),{							
              informacion: 'ArchivoPDF',
              start: barraPaginacionA.cursor,
              limit: barraPaginacionA.pageSize
            }),
            success : function(response) {
              boton.setIconClass('icoPdf');     
              boton.setDisabled(false);
            },
            callback: procesarDescargaArchivos
          });						
        }
      }
    ]
  }
});//----------------------------------Fin GridConsulta

//-------------------------------Elementos Forma--------------------------------
var  elementosForma =  [
{
  xtype				: 'compositefield',
  id					:'filaUno',
  combineErrors	: false,
  hidden:true,
  msgTarget		: 'side',
  items			:[{
    xtype		: 'datefield',
    fieldLabel	: 'Fecha de Solicitud',
    name			: '_fechaReg',
    id			: 'fechaRegistro',
    hiddenName: 'fecha_registro',
    allowBlank: true,
    startDay	: 0,
    width			: 140,
    msgTarget	: 'side',
    margins		: '0 20 0 0'
    },	{
    xtype			: 'displayfield',
    value			: ' a: ',
    width			: 30
    },	{
    xtype			: 'datefield',
    name			: '_fechaFinReg',
    id				: 'fechaFinRegistro',
    hiddenName: 'fecha_fin_reg',
    allowBlank: true,
    startDay	: 0,
    width			: 140,
    msgTarget	: 'side',
    margins		: '0 20 0 0'
    },{
    xtype			: 'displayfield',
    value			: 'Moneda: ',
    width			: 102
    },{
	  xtype			: 'combo',	
	  name			: 'cmb_mon',	
	  hiddenName	: 'cmb_mon_h',	
	  id				: 'cmbmon',	
	  displayField : 'DESCRIPCION',
	  valueField 	: 'CLAVE',
	  //emptyText		: 'Seleccionar',
	  mode				: 'local',
	  forceSelection : true,	
	  triggerAction  : 'all',	
	  editable    : true,
	  typeAhead		: true,
	  minChars 		: 1,	
	  store 			: catalogoMoneda
	}]
  },{
  xtype				: 'compositefield',
  id					:'filaDos',
  combineErrors	: false,
  hidden:true,
  msgTarget		: 'side',
  items			:[
		{
		  xtype			: 'textfield',
		  fieldLabel	: 'N�mero de solicitud',
		  id			   : 'numSol',
		  name		   : '_numSol',  
		  hiddenName	: 'h_numSol',
		  allowBlank	: true,
		  //maxLength 	: 10,
		  width			: 140
	  },{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 205
		},{
		 xtype			: 'displayfield',
		 value			: 'Monto del Cr�dito:',
		 width			: 102
		},{
		  xtype			: 'numberfield',
		  id			   : 'monto1',
		  name		   : 'montoIni',  
		  hiddenName	: 'h_montoIni',
		  allowBlank	: true,
		  maxLength 	: 10,
		  width			: 113
	  },{
		 xtype			: 'displayfield',
		 value			: 'a:',
		 width			: 10
		},{
		  xtype			: 'numberfield',
		  id			   : 'monto2',
		  name		   : 'montoFin',  
		  hiddenName	: 'h_montoFin',
		  allowBlank	: true,
		  maxLength 	: 10,
		  width			: 113
	  }
  
  ]},{
  xtype				: 'compositefield',
  id					:'filaTres',
  combineErrors	: false,
  msgTarget		: 'side',
  items			:[{
	  xtype			: 'combo',	
	  name			: 'cmb_ti',	
	  hiddenName	: 'cmb_ti',	
	  id				: 'cmbti',	
	  fieldLabel	: 'Intermediario', 
	  displayField	: 'DESCRIPCION',
	  valueField 	: 'CLAVE',
	  emptyText		: 'Seleccionar',
	  forceSelection : true,	
	  triggerAction  : 'all',	
	  mode				: 'local',
	  editable    : true,
	  minChars 		: 1,
	  typeAhead		: true,
	  store 			: catalogoIntermediario,
	  width			: 700
  },{
	 xtype			: 'displayfield',
	 value			: '',
	 width			: 10
	},{
	 xtype			: 'displayfield',
	 value			: '',
	 width			: 102
	},{
  xtype				: 'combo',
  name			 	: 'cmb_tipo',
  hiddenName		: 'cmb_tipo',
  id          		: 'cmbTipo', 
  displayField		: 'DESCRIPCION',
  valueField 		: 'CLAVE',
  emptyText		: 'Seleccionar',
  forceSelection : true,	
  triggerAction 	: 'all',	
  mode				: 'local',
  editable    : true,
  typeAhead		: true,
  hidden:true,
  minChars 		: 1,	
  store 			: estatus,	
  listeners		: {
    select		: {
      fn			: function(combo) {
                }// FIN fn
              }//FIN select
            }//FIN listeners
  }
  ]},{
	  xtype			: 'combo',	
	  name			: 'cmb_per',	
	  hiddenName	: 'cmb_per',	
	  id				: 'cmbPer',	
	  fieldLabel	: 'Tipo de Intermediario', 
	  displayField : 'DESCRIPCION',
	  valueField 	: 'CLAVE',
	  emptyText		: 'Seleccionar',
	  forceSelection : true,	
	  triggerAction  : 'all',	
	  mode				: 'local',
	  editable     : false,
	  minChars 		: 1,
	  typeAhead		: true,
	  store 			: catalogoPersona,
	  anchor			:'55.5%'
  }
];//-----------------------------Fin Elementos Forma------------------------------


//-------------------------------Panel Consulta---------------------------------
var fp2 = new Ext.form.FormPanel({
  id					:'fp2',
  width				:910,
  heigth				:'auto',
  frame				:true,
  collapsible		:true,
  hidden				: true,
  titleCollapse	:false,
  style				:'margin:0 auto;',
  bodyStyle			:'padding: 6px',
  labelWidth		:150,
  //monitorValid	:true,	
  defaults			:{
            msgTarget: 'side',
            anchor: '-20'
            },
  items		:[
	  {
	  xtype				: 'compositefield',
	  id					:'primerFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 50
		},gridUno
		]},{
	  xtype				: 'compositefield',
	  id					:'primerMsgGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridDos
		]},{
	  xtype				: 'compositefield',
	  id					:'terceraFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridTres
		]},{
	  xtype				: 'compositefield',
	  id					:'cuartaFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridCuatro
		]},{
	  xtype				: 'compositefield',
	  id					:'quintaFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridCinco
		]},{
	  xtype				: 'compositefield',
	  id					:'sextaFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridSeis
		]},{
	  xtype				: 'compositefield',
	  id					:'sieteFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridSiete
		]},{
	  xtype				: 'compositefield',
	  id					:'segundoMsgGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridOcho
		]},{
	  xtype				: 'compositefield',
	  id					:'nueveFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridNueve
		]},{
	  xtype				: 'compositefield',
	  id					:'diezFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridDiez
		]},{
	  xtype				: 'compositefield',
	  id					:'onceFilaGrid',
	  combineErrors	: false,
	  msgTarget		: 'side',
	  items			:[
		{
		 xtype			: 'displayfield',
		 value			: ' ',
		 width			: 1
		},gridOnce
		]}
            ],
  buttons		:[{
            text				:'Re-Cotizar',
            id					:'recotizar',
            iconCls			:'icoModificar',
            handler			:function(boton, evento){
					window.location  = "/nafin/22cotizador/SolCotEsp/CapturaDisp.jsp"; 
					}//fin handler
            },{
            text				:'Generar Archivo PDF',
            id					:'btnDetallePdf',
            iconCls			:'icoPdf',
            handler			:function(boton, evento){
                           
                      }
            },{
            text				:'Salir.',
            id					:'detalleSalir1',
            iconCls			:'icoRegresar',
            handler			:function(boton, evento){    
					window.location  = "/nafin/22cotizador/SolCotEsp/CapturaSolicitud.jsp"; 
            }//fin handler
            },{
            text				:'Salir',
            id					:'detalleSalir',
            iconCls			:'icoRegresar',
            handler			:function(boton, evento){     
					fp2.hide();
					fp.show();
					gridConsulta.show();
            }//fin handler
            }]
});//------------------------------Fin Panel Consulta------------------------------

//-------------------------------Panel Consulta---------------------------------
var fp = new Ext.form.FormPanel({
  id					:'formcon',
  width				:910,
  heigth				:'auto',
  title				:'<center>Asignaci�n de Tipo de Riesgo e Intermediario</center>',
  frame				:true,
  collapsible		:true,
  titleCollapse	:false,
  style				:'margin:0 auto;',
  bodyStyle			:'padding: 6px',
  labelWidth		:150,
  //monitorValid	:true,	
  defaults			:{
            msgTarget: 'side',
            anchor: '-20'
            },
  items				:[ 
            elementosForma
            ],
  buttons		:[{
            text				:'Buscar',
            id					:'consultar',
            iconCls			:'icoBuscar',
            handler			:function(boton, evento){   
						fp.el.mask('Cargando...', 'x-mask-loading');	
						consultaData.load({
							params		:Ext.apply(fp.getForm().getValues(),{
							informacion	:'ConsultarAsignacion',
							operacion	:'Generar',
							start	:0,
							limit	:15
							})
						});					
            }//fin handler
            },{
            text				:'Nuevo',
            id					:'btnNuevo',
            iconCls			:'icoModificar',
            handler			:function(boton, evento){ 
									Ext.getCmp('cmbti').reset();
									Ext.getCmp('cmbPer').reset();
									Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoint'}});
									Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoPersona'},	callback:procesarPersona });
									Ext.getCmp('gridConsulta').hide();
                      }//handler
            },{
            text				:'Guardar',
            id					:'btnGuardar',
            iconCls			:'icoGuardar',
            handler			:function(boton, evento){
							 Ext.Ajax.request({
								url: 'AdmonUsuariosExt01.data.jsp',	
								params	: Ext.apply(fp.getForm().getValues(),{
									informacion	:'guardar'
								 }),
								success : function(response) {
									var info = Ext.util.JSON.decode(response.responseText);
									var valor  =info.guardar; 
									var obtenV = parseInt(valor);
									if(obtenV===1){
										Ext.Msg.show({
										title: 'Guardar',
										msg: 'Intermediario guardado con �xito.',
										modal: true,
										icon: Ext.Msg.INFO,
										buttons: Ext.Msg.OK,
										fn: function (){
											Ext.getCmp('formcon').getForm().reset(); 
											Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoint'}});
											Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoPersona'},	callback:procesarPersona });
											Ext.getCmp('gridConsulta').hide();
											}
										});
									} else {
										Ext.Msg.show({
										title: 'Guardar',
										msg: 'Ocurrio un error.',
										modal: true,
										icon: Ext.Msg.ERROR,
										buttons: Ext.Msg.OK,
										fn: function (){
											Ext.getCmp('formcon').getForm().reset(); 
											Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoint'}});
											Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoPersona'},	callback:procesarPersona });
											Ext.getCmp('gridConsulta').hide();
											}
										});
									}
							  }
							 });
                      }//handler
            },{
            text				:'Eliminar',
            id					:'btnEliminar',
            iconCls			:'icoEliminar',
            handler			:function(boton, evento){
							 Ext.Ajax.request({
								url: 'AdmonUsuariosExt01.data.jsp',	
								params	: Ext.apply(fp.getForm().getValues(),{
									informacion	:'eliminar'
								 }),
								success : function(response) {
									var info = Ext.util.JSON.decode(response.responseText);
									var valor  =info.eliminar; 
									var obtenV = parseInt(valor);
									if(obtenV===1){
										Ext.Msg.show({
										title: 'Eliminar',
										msg: 'Intermediario eliminado con �xito.',
										modal: true,
										icon: Ext.Msg.INFO,
										buttons: Ext.Msg.OK,
										fn: function (){
											Ext.getCmp('formcon').getForm().reset(); 
											Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoint'}});
											Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoPersona'},	callback:procesarPersona });
											Ext.getCmp('gridConsulta').hide();
											}
										});
									} else {
										Ext.Msg.show({
										title: 'Eliminar',
										msg: 'Ocurrio un error.',
										modal: true,
										icon: Ext.Msg.ERROR,
										buttons: Ext.Msg.OK,
										fn: function (){
											Ext.getCmp('formcon').getForm().reset(); 
											Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoint'}});
											Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoPersona'},	callback:procesarPersona });
											Ext.getCmp('gridConsulta').hide();
											}
										});
									}
							  }
							 });
                      }//handler
            },{
            text				:'Cancelar',
            id					:'btnLimpiar',
            iconCls			:'icoCancelar',
            handler			:function(boton, evento){
									Ext.getCmp('cmbti').reset();
									Ext.getCmp('cmbPer').reset();
									Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoint'}});
									Ext.Ajax.request({	url: '22consulta1Ext01.data.jsp',	params: {informacion: 'catalogoPersona'},	callback:procesarPersona });
									Ext.getCmp('gridConsulta').hide();
                      }//handler
            }]
});//------------------------------Fin Panel Consulta------------------------------


//----------------------------Contenedor Principal------------------------------
var pnl = new Ext.Container({
  id			:'contenedorPrincipal',
  applyTo	:'areaContenido',
  width		: 949,
  style		:'margin:0 auto;',
  items		:[
        NE.util.getEspaciador(20),
        fp,
        NE.util.getEspaciador(20),
        gridConsulta, 
		  NE.util.getEspaciador(20),
		  fp2
        ]
});//-----------------------------Fin Contenedor Principal-------------------------



});//-----------------------------------------------Fin Ext.onReady(function(){}