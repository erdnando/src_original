Ext.onReady(function() { 
	
     function validaCorreos(correo){
     
        var errores = 0;
	var expr = /^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*(.[a-zA-Z]{2,3})$/;
	if(correo !==''){ 
            // Verifico el formato de los correos
            var vecCorreos = correo.split(',');
	    for(var i = 0; i < vecCorreos.length; i++){
                if (!expr.test(vecCorreos[i])){
                    errores++;
                }
            }		    
        }          
        return errores;
     }
 
    function mostrarAyuda(parametro){
	var titulo = '';
	var descripcion =[];
	if(parametro==='1') {
	    titulo = "Tesorer�a";
	    descripcion = ["<table width='580' cellpadding='3' cellspacing='1' border='0'>"+
		           "<tr><td class='formas' style='text-align: justify;'  colspan='3'>"+
		           "<BR>A estos correos electr�nicos se les notificar� cuando el Intermediario Financiero realize alguna cotizaci�n de una solicitud y toma en firme."  +
                           "<BR>* Los correos tendr�n que estar separados por ',' y no deber�n tener espacios en blanco." +
                           "<BR>* El formato de captura de correo electr�nico deber� ser el siguiente usuario@dominio.com,usuario2@dominio.com "+
		           "</td></tr>"+
		           "<tr><td class='formas' style='text-align: justify;'  colspan='3'>&nbsp;" + 
		           "</td></tr></table>"];
						    
	}else if(parametro==='2'  ||  parametro==='3' ) {
	    if(parametro==='2' ){
		titulo = "Administraci�n de Productos Electr�nicos";
	    }else if(parametro==='3' ){
		titulo = "Operaciones";
	    }
	    
            descripcion = ["<table width='580' cellpadding='3' cellspacing='1' border='0'>"+
		           "<tr><td class='formas' style='text-align: justify;'  colspan='3'>"+
		           '<BR>A estos correos electr�nicos se les notificar� cuando el Intermediario Financiero realize alguna toma en firme.'  +
                           "<BR>* Los correos tendr�n que estar separados por ',' y no deber�n tener espacios en blanco." +
                           "<BR>* El formato de captura de correo electr�nico deber� ser el siguiente usuario@dominio.com,usuario2@dominio.com "+
		           "</td></tr>"+
		           "<tr><td class='formas' style='text-align: justify;'  colspan='3'>&nbsp;" +
		           "</td></tr></table>"];
                           
                           
	}
	var winVen = Ext.getCmp('ventanaAyuda');
	if (winVen){
            winVen.show();										
	}else{
            new Ext.Window({ 
                id:'ventanaAyuda',     modal: true,	   width: 602,	  autoHeight: true,    resizable: false,   constrain: true,
                closable:false,   autoScroll:true,	    closeAction: 'hide',  html: descripcion.join(''),				
                bbar: {	xtype: 'toolbar',	buttonAlign:'center',	
                    buttons: [
                        '->',
                        {	xtype: 'button',	text: 'Salir',	iconCls: 'icoLimpiar', 	id: 'btnCerraP', handler: function(){Ext.getCmp('ventanaAyuda').destroy();} },
                        '-'							
                    ]
                }
            }).show().setTitle(titulo);
        }
    };
	
	
	/********** Inicializa Valores **********/
	function inicializar(){
		var fp = Ext.getCmp('formaPrincipal');
		fp.el.mask('Cargando datos...', 'x-mask-loading');
		Ext.Ajax.request({
			url: 'parametrosCotizador01ext.data.jsp',
			params: Ext.apply({
				informacion: 'Inicializacion'
			}),
			callback: procesarInicializacion
		});
	}

	/********** Carga los datos en el form **********/
	function procesarInicializacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var fp = Ext.getCmp('formaPrincipal');
				fp.el.unmask();
				Ext.getCmp('formaPrincipal').show();
				if(jsonData.mensaje != ''){
					Ext.Msg.alert('Error', jsonData.mensaje);
				} else{
					Ext.getCmp('inicio_consulta_hh_id').setValue(jsonData.inicio_consulta_hh_id);
					Ext.getCmp('inicio_consulta_mm_id').setValue(jsonData.inicio_consulta_mm_id);
					Ext.getCmp('termino_consulta_hh_id').setValue(jsonData.termino_consulta_hh_id);
					Ext.getCmp('termino_consulta_mm_id').setValue(jsonData.termino_consulta_mm_id);
					Ext.getCmp('inicio_confirmacion_hh_id').setValue(jsonData.inicio_confirmacion_hh_id);
					Ext.getCmp('inicio_confirmacion_mm_id').setValue(jsonData.inicio_confirmacion_mm_id);
					Ext.getCmp('termino_confirmacion_hh_id').setValue(jsonData.termino_confirmacion_hh_id);
					Ext.getCmp('termino_confirmacion_mm_id').setValue(jsonData.termino_confirmacion_mm_id);
					Ext.getCmp('redondeo_id').setValue(jsonData.redondeo_id);
					
					Ext.getCmp('monto_externos_mn_id').setValue(jsonData.monto_externos_mn_id);
					Ext.getCmp('monto_externos_usd_id').setValue(jsonData.monto_externos_usd_id);
					Ext.getCmp('plazo_externos_mn_id').setValue(jsonData.plazo_externos_mn_id);
					Ext.getCmp('plazo_externos_usd_id').setValue(jsonData.plazo_externos_usd_id);
					Ext.getCmp('estatus_mn_id').setValue(jsonData.estatus_mn_id);
					Ext.getCmp('estatus_usd_id').setValue(jsonData.estatus_usd_id);
					Ext.getCmp('plazo_operacion_mn_id').setValue(jsonData.plazo_operacion_mn_id);
					Ext.getCmp('plazo_operacion_usd_id').setValue(jsonData.plazo_operacion_usd_id);
					Ext.getCmp('max_disposiciones_mn_id').setValue(jsonData.max_disposiciones_mn_id);
					Ext.getCmp('max_disposiciones_usd_id').setValue(jsonData.max_disposiciones_usd_id);
					Ext.getCmp('tasa_impuesto_mn_id').setValue(jsonData.tasa_impuesto_mn_id);
					Ext.getCmp('tasa_impuesto_usd_id').setValue(jsonData.tasa_impuesto_usd_id);
					Ext.getCmp('dias_habiles_mn_id').setValue(jsonData.dias_habiles_mn_id);
					Ext.getCmp('dias_habiles_usd_id').setValue(jsonData.dias_habiles_usd_id);
					Ext.getCmp('tasas_protegidas_mn_id').setValue(jsonData.tasas_protegidas_mn_id);
					
					Ext.getCmp('plazo_linea_mn_id').setValue(jsonData.plazo_linea_mn_id);
					Ext.getCmp('plazo_linea_usd_id').setValue(jsonData.plazo_linea_usd_id);
					Ext.getCmp('disposiciones_linea_mn_id').setValue(jsonData.disposiciones_linea_mn_id);
					Ext.getCmp('disposiciones_linea_usd_id').setValue(jsonData.disposiciones_linea_usd_id);
					Ext.getCmp('monto_linea_mn_id').setValue(jsonData.monto_linea_mn_id);
					Ext.getCmp('monto_linea_usd_id').setValue(jsonData.monto_linea_usd_id);
					Ext.getCmp('plazo_recuperacion_mn_id').setValue(jsonData.plazo_recuperacion_mn_id);
					Ext.getCmp('plazo_recuperacion_usd_id').setValue(jsonData.plazo_recuperacion_usd_id);
					Ext.getCmp('interpolacion_mn_id').setValue(jsonData.interpolacion_mn_id);
					Ext.getCmp('interpolacion_usd_id').setValue(jsonData.interpolacion_usd_id);
					Ext.getCmp('metodologia_calculo_mn_id').setValue(jsonData.metodologia_calculo_mn_id);
					Ext.getCmp('metodologia_calculo_usd_id').setValue(jsonData.metodologia_calculo_usd_id);
					Ext.getCmp('tasa_disponible_fija_mn_id').setValue(jsonData.tasa_disponible_fija_mn_id);
					Ext.getCmp('tasa_disponible_fija_usd_id').setValue(jsonData.tasa_disponible_fija_usd_id);
					Ext.getCmp('tasa_disponible_variable_usd_id').setValue(jsonData.tasa_disponible_variable_usd_id);
					Ext.getCmp('correoMnId').setValue(jsonData.correoMnId);	
					Ext.getCmp('correoUsdId').setValue(jsonData.correoUsdId);
					
					Ext.getCmp('correopelecMnId').setValue(jsonData.correopelecMnId);	
					Ext.getCmp('correopelecUsdId').setValue(jsonData.correopelecUsdId);	
					Ext.getCmp('correoopeMnId').setValue(jsonData.correoopeMnId);	
					Ext.getCmp('correoopeUsdId').setValue(jsonData.correoopeUsdId);
					
					
					/**
					Se procesan las variables de la secci�n: Esquemas de recuperaci�n disponibles para IFs de Cotizaci�n en L�nea
					*/
					if(jsonData.cuponCero == true || jsonData.tradicional == true || jsonData.planPagos == true || jsonData.renta == true){
						Ext.getCmp('titulos_3').show();
						Ext.getCmp('titulos_4').show();
					} else{
						Ext.getCmp('titulos_3').hide();
						Ext.getCmp('titulos_4').hide();
					}
					if(jsonData.cuponCero == true){
						Ext.getCmp('seccion_19').show();
						Ext.getCmp('cupon_cero_mn_id').setValue(jsonData.cupon_cero_mn_id);
						Ext.getCmp('cupon_cero_usd_id').setValue(jsonData.cupon_cero_usd_id);
					} else{
						Ext.getCmp('seccion_19').hide();
					}
					if(jsonData.tradicional == true){
						Ext.getCmp('seccion_20').show();
						Ext.getCmp('tradicional_mn_id').setValue(jsonData.tradicional_mn_id);
						Ext.getCmp('tradicional_usd_id').setValue(jsonData.tradicional_usd_id);
					} else{
						Ext.getCmp('seccion_20').hide();
					}
					if(jsonData.planPagos == true){
						Ext.getCmp('seccion_21').show();
						Ext.getCmp('plan_pagos_mn_id').setValue(jsonData.plan_pagos_mn_id);
						Ext.getCmp('plan_pagos_usd_id').setValue(jsonData.plan_pagos_usd_id);
					} else{
						Ext.getCmp('seccion_21').hide();
					}
					if(jsonData.renta == true){
						Ext.getCmp('seccion_22').show();
						Ext.getCmp('renta_mn_id').setValue(jsonData.renta_mn_id);
						Ext.getCmp('renta_usd_id').setValue(jsonData.renta_usd_id);
					} else{
						Ext.getCmp('seccion_22').hide();
					}
				}
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	/********** Valida que los minutos no excedan los 59 **********/
	function validaMinutos(valor){
		var minutoMax = 0;
		minutoMax = parseInt(valor);
		if(minutoMax < 59){
			return true;
		} else{
			return false;
		}
	}
	
	/********** Valida que las horas de inicio sean menores que las horas de t�rmino **********/
	function validaHorarios(hh_ini, mm_ini, hh_fin, mm_fin){
		var hh_ini_int = 0, mm_ini_int = 0, hh_fin_int = 0, mm_fin_int = 0;
		hh_ini_int = parseInt(hh_ini);
		mm_ini_int = parseInt(mm_ini);
		hh_fin_int = parseInt(hh_fin);
		mm_fin_int = parseInt(mm_fin);
		
		if(hh_ini_int < hh_fin_int){
			return true;
		} else if(hh_ini_int == hh_fin_int){
			if(mm_ini_int < mm_fin_int){
				return true;
			} else{
				return false;
			}
		} else{
			return false;
		}
	}
	
	/********** Valida los campos y guarda los datos **********/
	function procesarGuardar(){               
           
            
		if (Ext.getCmp('formaPrincipal').getForm().isValid()){
                
                      if( validaCorreos(Ext.getCmp('correoMnId').getValue()) >0 ) {
                        Ext.getCmp('correoMnId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
                        return;
                      }
                      if( validaCorreos(Ext.getCmp('correoUsdId').getValue()) >0 ) {
                        Ext.getCmp('correoUsdId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
                        return;
                      }
                      if( validaCorreos(Ext.getCmp('correopelecMnId').getValue()) >0 ) {
                        Ext.getCmp('correopelecMnId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
                        return;
                      }
                      if( validaCorreos(Ext.getCmp('correopelecUsdId').getValue()) >0 ) {
                        Ext.getCmp('correopelecUsdId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
                        return;
                      }
                      if( validaCorreos(Ext.getCmp('correoopeMnId').getValue()) >0 ) {
                        Ext.getCmp('correoopeMnId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
                        return;
                      }
                      if( validaCorreos(Ext.getCmp('correoopeUsdId').getValue()) >0 ) {
                        Ext.getCmp('correoopeUsdId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
                        return;
                      }
                
                
			if(!validaHorarios(Ext.getCmp('inicio_consulta_hh_id').getValue(), Ext.getCmp('inicio_consulta_mm_id').getValue(), Ext.getCmp('termino_consulta_hh_id').getValue(), Ext.getCmp('termino_consulta_mm_id').getValue())){
				Ext.Msg.alert('Error', 'La Hora de Inicio de Consulta debe ser menor que la Hora de T�rmino de Consulta.');
			} else{
				if(!validaHorarios(Ext.getCmp('inicio_confirmacion_hh_id').getValue(), Ext.getCmp('inicio_confirmacion_mm_id').getValue(), Ext.getCmp('termino_confirmacion_hh_id').getValue(), Ext.getCmp('termino_confirmacion_mm_id').getValue())){
					Ext.Msg.alert('Error', 'La Hora de Inicio de Confirmaci�n debe ser menor que la Hora de T�rmino de Confirmaci�n.');
				} else{
					var fp = Ext.getCmp('formaPrincipal');
					fp.el.mask('Procesando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: 'parametrosCotizador01ext.data.jsp',
						params: Ext.apply(formaPrincipal.getForm().getValues(),{
							informacion: 'Guardar_Cambios'
						}),
						callback: procesarGuardarCambios
					});
				}
			}
		} else{
			Ext.Msg.alert('Error', 'Uno o m�s campos de captura tienen errores.');
		}
	}

	/********** Muestra el resultado despues de guardar los datos **********/
	function procesarGuardarCambios(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('formaPrincipal');
			fp.el.unmask();
			if (jsonData != null){
				if(jsonData.mensaje != ''){
					Ext.getCmp('formaPrincipal').hide();
					Ext.getCmp('formaNotificacion').show();
					Ext.getCmp('operacion_exitosa_id').hide();
					Ext.getCmp('operacion_fallida_id').show();
					Ext.Msg.alert('Error', jsonData.mensaje);
				} else{
					Ext.getCmp('formaPrincipal').hide();
					Ext.getCmp('formaNotificacion').show();
					if(jsonData.accion == 'exito'){
						Ext.getCmp('operacion_exitosa_id').show();
						Ext.getCmp('operacion_fallida_id').hide();
					} else{
						Ext.getCmp('operacion_exitosa_id').hide();
						Ext.getCmp('operacion_fallida_id').show();
					}
				}
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	/********** Elementos de la forma principal **********/	
	var elementosForma = [{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_0',
		layout:              'table',
		border:              false,
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            235,
			xtype:            'displayfield',
			value:            '&nbsp;&nbsp; Hora de Inicio de Consulta:'
		},{
			width:            50,
			xtype:            'numberfield',
			id:               'inicio_consulta_hh_id',
			name:             'inicio_consulta_hh',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			allowBlank:       false,
			minValue:         0,
			maxValue:         23
		},{
			width:            25,
			xtype:            'displayfield',
			value:            '&nbsp; : &nbsp;'
		},{
			width:            50,
			xtype:            'textfield',
			id:               'inicio_consulta_mm_id',
			name:             'inicio_consulta_mm',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			allowBlank:       false,
			regex:            /^[0-9]*$/,
			maxLength:        2,
			listeners: {
				'blur': function(textfield){
					if(!validaMinutos(textfield.getValue())){
						Ext.getCmp('inicio_consulta_mm_id').setValue('00');
						Ext.getCmp('inicio_consulta_mm_id').markInvalid('El valor para este campo es incorrecto');
					}
				}
			}
		},{
			width:            45,
			xtype:            'displayfield',
			value:            '&nbsp; hrs. &nbsp;'
		},{
			width:            50,
			border:           false,
			html:             '&nbsp;<br>&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_1',
		layout:              'table',
		border:              false,
		layoutConfig: {
			columns:          6
		},
		defaults: { 
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            235,
			xtype:            'displayfield',
			value:            '&nbsp;&nbsp; Hora de T�rmino de Consulta:'
		},{
			width:            50,
			xtype:            'numberfield',
			id:               'termino_consulta_hh_id',
			name:             'termino_consulta_hh',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			allowBlank:       false,
			minValue:         0,
			maxValue:         23
		},{
			width:            25,
			xtype:            'displayfield',
			value:            '&nbsp; : &nbsp;'
		},{
			width:            50,
			xtype:            'textfield',
			id:               'termino_consulta_mm_id',
			name:             'termino_consulta_mm',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			allowBlank:       false,
			regex:            /^[0-9]*$/,
			maxLength:        2,
			listeners: {
				'blur': function(textfield){
					if(!validaMinutos(textfield.getValue())){
						Ext.getCmp('termino_consulta_mm_id').setValue('00');
						Ext.getCmp('termino_consulta_mm_id').markInvalid('El valor para este campo es incorrecto');
					}
				}
			}
		},{
			width:            45,
			xtype:            'displayfield',
			value:            '&nbsp; hrs. &nbsp;'
		},{
			width:            50,
			border:           false, 
			html:             '&nbsp;<br>&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_2',
		layout:              'table',
		border:              false,
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            235,
			xtype:            'displayfield',
			value:            '&nbsp;&nbsp; Hora de Inicio de Confirmaci�n:'
		},{
			width:            50,
			xtype:            'numberfield',
			id:               'inicio_confirmacion_hh_id',
			name:             'inicio_confirmacion_hh',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			allowBlank:       false,
			minValue:         0,
			maxValue:         23
		},{
			width:            25,
			xtype:            'displayfield',
			value:            '&nbsp; : &nbsp;'
		},{
			width:            50,
			xtype:            'textfield',
			id:               'inicio_confirmacion_mm_id',
			name:             'inicio_confirmacion_mm',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			allowBlank:       false,
			regex:            /^[0-9]*$/,
			maxLength:        2,
			listeners: {
				'blur': function(textfield){
					if(!validaMinutos(textfield.getValue())){
						Ext.getCmp('inicio_confirmacion_mm_id').setValue('00');
						Ext.getCmp('inicio_confirmacion_mm_id').markInvalid('El valor para este campo es incorrecto');
					}
				}
			}
		},{
			width:            45,
			xtype:            'displayfield',
			value:            '&nbsp; hrs. &nbsp;'
		},{ 
			width:            50,
			border:           false, 
			html:             '&nbsp;<br>&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_3',
		layout:              'table',
		border:              false,
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            235,
			xtype:            'displayfield',
			value:            '&nbsp;&nbsp; Hora de T�rmino de Confirmaci�n:'
		},{
			width:            50,
			xtype:            'numberfield',
			id:               'termino_confirmacion_hh_id',
			name:             'termino_confirmacion_hh',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			allowBlank:       false,
			minValue:         0,
			maxValue:         23
		},{
			width:            25,
			xtype:            'displayfield',
			value:            '&nbsp; : &nbsp;'
		},{
			width:            50,
			xtype:            'textfield',
			id:               'termino_confirmacion_mm_id',
			name:             'termino_confirmacion_mm',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			allowBlank:       false,
			regex:            /^[0-9]*$/,
			maxLength:        2,
			listeners: {
				'blur': function(textfield){
					if(!validaMinutos(textfield.getValue())){
						Ext.getCmp('termino_confirmacion_mm_id').setValue('00');
						Ext.getCmp('termino_confirmacion_mm_id').markInvalid('El valor para este campo es incorrecto');
					}
				}
			}
		},{
			width:            45,
			xtype:            'displayfield',
			value:            '&nbsp; hrs. &nbsp;'
		},{ 
			width:            50,
			border:           false, 
			html:             '&nbsp;<br>&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_4',
		layout:              'table',
		border:              false,
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            235,
			xtype:            'displayfield',
			value:            '&nbsp;&nbsp; N�mero de decimales a redondear:'
		},{
			width:            50,
			xtype:            'combo',
			name:             'redondeo',
			id:               'redondeo_id',
			mode:             'local',
			triggerAction:    'all',
			forceSelection:   true,
			typeAhead:        true,
			allowBlank:       false,
			store: [
				['1','1'], ['2','2'], ['3','3'], ['4','4'], ['5','5'], ['6','6'], ['7','7'], ['8','8'], ['9','9'], ['10','10']
			]
		},{
			width:            50,
			border:           false, 
			html:             '&nbsp;<br>&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'titulos_1',
		layout:              'table',
		border:              true,
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [
			{ width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br>&nbsp;<br>&nbsp;</div>'},
			{ width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br><b> Moneda Nacional <br>&nbsp;</div>'},
			{ width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br><b> D�lares <br>&nbsp;</div>'}
		]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_5',
		layout:              'table',
		layoutConfig: {
			columns:          8
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Monto L�mite para Externos: </b><br>&nbsp;</div>'
		},{
			width:            20,
			xtype:            'displayfield',
			value:            '&nbsp; $ '
		},{
			width:            100,
			xtype:            'numberfield',
			id:               'monto_externos_mn_id',
			name:             'monto_externos_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         999999999999.99,
			allowBlank:       false
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp; MN &nbsp;'
		},{
			width:            40,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            20,
			xtype:            'displayfield',
			value:            '&nbsp; $ '
		},{
			width:            100,
			xtype:            'numberfield',
			id:               'monto_externos_usd_id',
			name:             'monto_externos_usd',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         999999999999.99,
			allowBlank:       false
		},{
			width:            45,
			xtype:            'displayfield',
			value:            '&nbsp; USD &nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_6',
		layout:              'table',
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Plazo L�mite para externos:</b><br>&nbsp;</div>'
		},{
			width:            120,
			xtype:            'numberfield',
			id:               'plazo_externos_mn_id',
			name:             'plazo_externos_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowBlank:       false,
			allowDecimals:    false
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp;d�as '
		},{
			width:            40,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            120,
			xtype:            'numberfield',
			fieldLabel:       '',
			id:               'plazo_externos_usd_id',
			name:             'plazo_externos_usd',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowBlank:       false,
			allowDecimals:    false
		},{
			width:            45,
			xtype:            'displayfield',
			value:            '&nbsp;d�as '
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_7',
		layout:              'table',
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Estatus del sistema:</b><br>&nbsp;</div>'
		},{
			width:            120,
			xtype:            'combo',
			id:               'estatus_mn_id',
			name:             'estatus_mn',
			mode:             'local',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			triggerAction:    'all',
			forceSelection:   true,
			typeAhead:        true,
			store: [
				['0','Abierto'], ['1','Bloqueado']
			]
		},{
			width:            90,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            120,
			xtype:            'combo',
			id:               'estatus_usd_id',
			name:             'estatus_usd',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			mode:             'local',
			triggerAction:    'all',
			forceSelection:   true,
			typeAhead:        true,
			store: [
				['0','Abierto'], ['1','Bloqueado']
			]
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_8',
		layout:              'table',
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Plazo M�ximo de la Operaci�n(Solicitud):</b><br>&nbsp;</div>'
		},{
			width:            120,
			xtype:            'numberfield',
			id:               'plazo_operacion_mn_id',
			name:             'plazo_operacion_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowBlank:       false,
			allowDecimals:    false
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp; d�as'
		},{
			width:            40,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            120,
			xtype:            'numberfield',
			fieldLabel:       '',
			id:               'plazo_operacion_usd_id',
			name:             'plazo_operacion_usd',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowBlank:       false,
			allowDecimals:    false
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp; d�as'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_9',
		layout:              'table',
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> N�mero m�ximo de disposiciones:</b><br>&nbsp;</div>'
		},{
			width:            120,
			xtype:            'numberfield',
			id:               'max_disposiciones_mn_id',
			name:             'max_disposiciones_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowBlank:       false
		},{
			width:            90,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            120,
			xtype:            'numberfield',
			fieldLabel:       '',
			id:               'max_disposiciones_usd_id',
			name:             'max_disposiciones_usd',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowBlank:       false
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_10',
		layout:              'table',
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Tasa impuesto:</b><br>&nbsp;</div>'
		},{
			width:            120,
			xtype:            'numberfield',
			id:               'tasa_impuesto_mn_id',
			name:             'tasa_impuesto_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			decimalPrecision: 4,
			minValue:         0,
			maxValue:         99,
			allowBlank:       false
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp; %'
		},{
			width:            40,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            120,
			xtype:            'numberfield',
			fieldLabel:       '',
			id:               'tasa_impuesto_usd_id',
			name:             'tasa_impuesto_usd',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			decimalPrecision: 4,
			minValue:         0,
			maxValue:         99,
			allowBlank:       false
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp; %'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_11',
		layout:              'table',
		layoutConfig: {
			columns:          5
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> D�as h�biles para solicitud de recursos antes de la fecha de disposici�n:</b><br>&nbsp;</div>'
		},{
			width:            120,
			xtype:            'numberfield',
			id:               'dias_habiles_mn_id',
			name:             'dias_habiles_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowBlank:       false,
			allowDecimals:    false
		},{
			width:            90,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            120,
			xtype:            'numberfield',
			fieldLabel:       '',
			id:               'dias_habiles_usd_id',
			name:             'dias_habiles_usd',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowBlank:       false,
			allowDecimals:    false
		},{ 
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_12',
		layout:              'table',
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Techo para Tasas Protegidas:</b><br>&nbsp;</div>' //Soporta 4 decimales
		},{
			width:            120,
			xtype:            'numberfield',
			id:               'tasas_protegidas_mn_id',
			name:             'tasas_protegidas_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			decimalPrecision: 4,
			minValue:         0,
			maxValue:         99
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp; %'
		},{ 
			width:            60,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{ 
			width:            120,
			xtype:            'displayfield',
			value:            '&nbsp; N/A &nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'titulos_2',
		layout:              'table',
		border:              true,
		layoutConfig: {
			columns:          1
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [
			{ width: 600, frame: true, border: false, html: '<div class="formas" align="center"><br><b> Par�metros para Cotizaci�n en l�nea <br>&nbsp;</div>'}
		]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_13',
		layout:              'table',
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Plazo m�ximo para cotizar en l�nea:</b><br>&nbsp;</div>'
		},{
			width:            120,
			xtype:            'numberfield',
			id:               'plazo_linea_mn_id',
			name:             'plazo_linea_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowDecimals:    false
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp; d�as'
		},{
			width:            40,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            120,
			xtype:            'numberfield',
			fieldLabel:       '',
			id:               'plazo_linea_usd_id',
			name:             'plazo_linea_usd',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowDecimals:    false
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp; d�as'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_14',
		layout:              'table',
		layoutConfig: {
			columns:          5
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> N�mero m�ximo de disposiciones para cotizar en l�nea:</b><br>&nbsp;</div>'
		},{
			width:            120,
			xtype:            'numberfield',
			id:               'disposiciones_linea_mn_id',
			name:             'disposiciones_linea_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999
		},{
			width:            90,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            120,
			xtype:            'numberfield',
			fieldLabel:       '',
			id:               'disposiciones_linea_usd_id',
			name:             'disposiciones_linea_usd',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999
		},{ 
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_15',
		layout:              'table',
		layoutConfig: {
			columns:          7
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Monto m�ximo para cotizar en l�nea:</b><br>&nbsp;</div>'
		},{
			width:            20,
			xtype:            'displayfield',
			value:            '&nbsp; $'
		},{
			width:            100,
			xtype:            'numberfield',
			id:               'monto_linea_mn_id',
			name:             'monto_linea_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         9999999999999.99
		},{
			width:            90,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            20,
			xtype:            'displayfield',
			value:            '&nbsp; $'
		},{
			width:            100,
			xtype:            'numberfield',
			fieldLabel:       '',
			id:               'monto_linea_usd_id',
			name:             'monto_linea_usd',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         9999999999999.99
		},{ 
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_16',
		layout:              'table',
		layoutConfig: {
			columns:          6
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Plazo m�ximo para cotizaciones en l�nea esquema de recuperaci�n cup�n cero:</b><br>&nbsp;</div>'
		},{
			width:            120,
			xtype:            'numberfield',
			id:               'plazo_recuperacion_mn_id',
			name:             'plazo_recuperacion_mn',
			fieldLabel:       '',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowDecimals:    false
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp; d�as'
		},{
			width:            40,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            120,
			xtype:            'numberfield',
			fieldLabel:       '',
			id:               'plazo_recuperacion_usd_id',
			name:             'plazo_recuperacion_usd',
			msgTarget:        'side',
			margins:          '0 20 0 0',
			minValue:         0,
			maxValue:         99999,
			allowDecimals:    false
		},{ 
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp; d�as'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_17',
		layout:              'table',
		layoutConfig: {
			columns:          5
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> M�todo de interpolaci�n:</b><br>&nbsp;</div>'
		},{
			width:            150,
			xtype:            'radiogroup',
			id:               'interpolacion_mn_id',
			name:             'interpolacion_mn',
			align:            'center',
			cls:              'x-check-group-alt',
			style: {
				width:         '80%',
				marginLeft:    '10px'
			},
			columns:[300, 300, 300],
			items: [
				{boxLabel: 'Lineal',    name: 'interpolacion_mn', inputValue: 'L'},
				{boxLabel: 'Alambrado', name: 'interpolacion_mn', inputValue: 'A'}
			]
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            150,
			xtype:            'radiogroup',
			id:               'interpolacion_usd_id',
			name:             'interpolacion_usd',
			align:            'center',
			cls:              'x-check-group-alt',
			style: {
				width:         '80%',
				marginLeft:    '10px'
			},
			columns:[300, 300, 300],
			items: [
				{boxLabel: 'Lineal',    name: 'interpolacion_usd', inputValue: 'L'},
				{boxLabel: 'Alambrado', name: 'interpolacion_usd', inputValue: 'A'}
			]
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_18',
		layout:              'table',
		layoutConfig: {
			columns:          5
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Metodolog�a de c�lculo:</b><br>&nbsp;</div>'
		},{
			width:            150,
			xtype:            'radiogroup',
			id:               'metodologia_calculo_mn_id',
			name:             'metodologia_calculo_mn',
			align:            'center',
			cls:              'x-check-group-alt',
			style: {
				width:         '80%',
				marginLeft:    '10px'
			},
			columns:[300, 300, 300],
			items: [
				{boxLabel: 'Plazo Promedio de vida', name: 'metodologia_calculo_mn', inputValue: 'P'},
				{boxLabel: 'Flujos',                 name: 'metodologia_calculo_mn', inputValue: 'D'},
				{boxLabel: 'Duraci�n',               name: 'metodologia_calculo_mn', inputValue: 'U'}
			]
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            150,
			xtype:            'radiogroup',
			id:               'metodologia_calculo_usd_id',
			name:             'metodologia_calculo_usd',
			align:            'center',
			cls:              'x-check-group-alt',
			style: {
				width:         '80%',
				marginLeft:    '10px'
			},
			columns:[300, 300, 300],
			items: [
				{boxLabel: 'Plazo Promedio de vida', name: 'metodologia_calculo_usd', inputValue: 'P'},
				{boxLabel: 'Flujos',                 name: 'metodologia_calculo_usd', inputValue: 'D'},
				{boxLabel: 'Duraci�n',               name: 'metodologia_calculo_usd', inputValue: 'U'}
			]
		},{
			width:            50,
			xtype:            'displayfield',
			value:            '&nbsp;'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'titulos_3',
		layout:              'table',
		border:              true,
		hidden:              true,
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [
			{ width: 600, frame: true, border: false, html: '<div class="formas" align="center"><br><b> Esquemas de recuperaci�n disponibles para IFs de Cotizaci�n en L�nea  <br>&nbsp;</div>'}
		]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'titulos_4',
		layout:              'table',
		border:              true,
		hidden:              true,
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [
			{ width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br>&nbsp;<br>&nbsp;</div>'},
			{ width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br><b> Moneda Nacional <br>&nbsp;</div>'},
			{ width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br><b> D�lares <br>&nbsp;</div>'}
		]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_19',
		layout:              'table',
		hidden:              true,
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            300,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Cup�n cero:</b><br>&nbsp;</div>'
		},{
			width:            150,
			xtype:            'checkbox',
			id:               'cupon_cero_mn_id',
			name:             'cupon_cero_mn',
			boxLabel:         '',
			align:            'center'
		},{
			width:            150,
			xtype:            'checkbox',
			id:               'cupon_cero_usd_id',
			name:             'cupon_cero_usd',
			boxLabel:         '',
			align:            'center'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_20',
		layout:              'table',
		hidden:              true,
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            300,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Tradicional:</b><br>&nbsp;</div>'
		},{
			width:            150,
			xtype:            'checkbox',
			id:               'tradicional_mn_id',
			name:             'tradicional_mn',
			boxLabel:         '',
			align:            'center'
		},{
			width:            150,
			xtype:            'checkbox',
			id:               'tradicional_usd_id',
			name:             'tradicional_usd',
			boxLabel:         '',
			align:            'center'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_21',
		layout:              'table',
		hidden:              true,
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            300,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Plan de pagos:</b><br>&nbsp;</div>'
		},{
			width:            150,
			xtype:            'checkbox',
			id:               'plan_pagos_mn_id',
			name:             'plan_pagos_mn',
			boxLabel:         '',
			align:            'center'
		},{
			width:            150,
			xtype:            'checkbox',
			id:               'plan_pagos_usd_id',
			name:             'plan_pagos_usd',
			boxLabel:         '',
			align:            'center'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_22',
		layout:              'table',
		hidden:              true,
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            300,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Renta:</b><br>&nbsp;</div>'
		},{
			width:            150,
			xtype:            'checkbox',
			id:               'renta_mn_id',
			name:             'renta_mn',
			boxLabel:         '',
			align:            'center'
		},{
			width:            150,
			xtype:            'checkbox',
			id:               'renta_usd_id',
			name:             'renta_usd',
			boxLabel:         '',
			align:            'center'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'titulos_5',
		layout:              'table',
		border:              true,
		layoutConfig: {
			columns:          1
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [
			{ width: 600, frame: true, border: false, html: '<div class="formas" align="center"><br><b> Tipo de Tasa Disponible para IF en Cotizaciones en L�nea <br>&nbsp;</div>'}
		]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'titulos_6',
		layout:              'table',
		border:              true,
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [
			{ width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br>&nbsp;<br>&nbsp;</div>'},
			{ width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br><b> Moneda Nacional <br>&nbsp;</div>'},
			{ width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br><b> D�lares <br>&nbsp;</div>'}
		]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_23',
		layout:              'table',
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Fija:</b><br>&nbsp;</div>'
		},{
			width:            200,
			xtype:            'checkbox',
			id:               'tasa_disponible_fija_mn_id',
			name:             'tasa_disponible_fija_mn',
			boxLabel:         '',
			align:            'center'
		},{
			width:            200,
			xtype:            'checkbox',
			id:               'tasa_disponible_fija_usd_id',
			name:             'tasa_disponible_fija_usd',
			boxLabel:         '',
			align:            'center'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'seccion_24',
		layout:              'table',
		layoutConfig: {
			columns:          4
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [{
			width:            200,
			frame:            false,
			border:           false,
			html:             '<div class="formas" align="left"><br><b> Variable:</b><br>&nbsp;</div>'
		},{ 
			width:            80,
			xtype:            'displayfield',
			value:            '&nbsp;'
		},{
			width:            120,
			xtype:            'displayfield',
			value:            '&nbsp; N/A &nbsp;'
		},{
			width:            200,
			xtype:            'checkbox',
			id:               'tasa_disponible_variable_usd_id',
			name:             'tasa_disponible_variable_usd',
			boxLabel:         '',
			align:            'center'
		}]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'titulos_7',
		layout:              'table',
		border:              true,
		layoutConfig: {
			columns:          1
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [
			{ width: 600, frame: true, border: false, html: '<div class="formas" align="center"><br><b> Direcciones para Notificaci�n de Solicitudes<br>&nbsp;</div>'}
		]
	},{
		width:               600,
		xtype:               'panel',
		id:                  'titulos_8',
		layout:              'table',
		border:              true,
		layoutConfig: {
			columns:          3
		},
		defaults: {
			align:            'center',
			bodyStyle:        'padding: 2px,'
		},
		items: [
		    { width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br>&nbsp;<br>&nbsp;</div>'},
		    { width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br><b> Moneda Nacional <br>&nbsp;</div>'},
		    { width: 200, frame: false, border: false, html: '<div class="formas" align="center"><br><b> D�lares <br>&nbsp;</div>'}
		]
	},
	{
	    width:  600,    xtype:  'panel',   id:     'seccion_25',    layout:  'table',
	    layoutConfig: {   columns:  6   },
	    defaults: {	align: 'center', bodyStyle:  'padding: 2px,' },
	    items: [
		{ xtype: 'button',  id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){ mostrarAyuda('1');  } },				
		{ width: 200,  frame: false, border: false,   html: '<div class="formas" align="left"><br><b> Tesoreria :</b><br>&nbsp;</div>'     },
		{ width: 160, xtype:'textarea', id:   'correoMnId', name: 'correoMn', msgTarget:   'side',  margins:  '0 20 0 0', maxLength: "400",
		    listeners:     {
			blur:       {
			    fn: function(objTxt) {				
				var correo = Ext.getCmp('correoMnId').getValue();
				if(correo !==''){ 				  				   
				    if(validaCorreos(Ext.getCmp('correoMnId').getValue()) > 0){
					Ext.getCmp('correoMnId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
					Ext.getCmp('btnGuardar').disable();
				    }else  {
					Ext.getCmp('btnGuardar').enable();
				    }				    
				   }else  {
                                    Ext.getCmp('btnGuardar').enable();   
                                   }
				}
			    }
			}						    
		    },
		    {  width: 30,   xtype: 'displayfield',    value: '&nbsp;'   },
		    {  width: 160,  xtype: 'textarea',   id: 'correoUsdId',   name:'correoUsd', msgTarget:'side', margins: '0 20 0 0',  maxLength:"400",
			listeners:     {
			    blur:       {
				fn: function(objTxt) {
				      var correo = Ext.getCmp('correoUsdId').getValue();				    
				    if(correo !==''){ 				    	
					if(validaCorreos(Ext.getCmp('correoUsdId').getValue()) > 0){ 
					    Ext.getCmp('correoUsdId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
					    Ext.getCmp('btnGuardar').disable();
					}else  {
					    Ext.getCmp('btnGuardar').enable();
					}				    
				   }else  {
                                    Ext.getCmp('btnGuardar').enable();   
                                   }
				}
			    }
			}
			
		    },
		    { width: 30,  xtype: 'displayfield',  value: '&nbsp;'  }
		]
	    },
	    {	width: 600, xtype: 'panel',  id: 'seccion_26', layout: 'table', layoutConfig: {	columns:  6 } ,
		defaults: { align:   'center',bodyStyle:  'padding: 2px,'},
		items: [
		    { xtype: 'button',  id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){	mostrarAyuda('2');  } 	},
		    { width: 200, frame: false, border: false,	html: '<div class="formas" align="left"><br><b> Administraci�n de Productos Electr�nicos:</b><br>&nbsp;</div>'   },
		    { width: 160, xtype: 'textarea',  id: 'correopelecMnId',  name: 'correopelecMn', msgTarget: 'side',  margins: '0 20 0 0',	maxLength: "400",
			listeners:     {
			    blur:       {
				fn: function(objTxt) {
				     var correo = Ext.getCmp('correopelecMnId').getValue();				    
				    if(correo !==''){ 	
					if(validaCorreos(Ext.getCmp('correopelecMnId').getValue()) > 0){ 
					    Ext.getCmp('correopelecMnId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
					   Ext.getCmp('btnGuardar').disable();
					}else  {
					    Ext.getCmp('btnGuardar').enable();
					}				    
				    }else  {
                                        Ext.getCmp('btnGuardar').enable();   
                                   }
				}
			    }
			}						    
		    },
		    {  width:30, xtype:'displayfield', value: '&nbsp;'  },
		    {  width: 160, xtype:'textarea',  id: 'correopelecUsdId',  name: 'correopelecUsd',  msgTarget: 'side', margins: '0 20 0 0',   maxLength:"400",
			listeners:     {
			    blur:       {
				fn: function(objTxt) {
                                    var correo = Ext.getCmp('correopelecUsdId').getValue();
				    if(correo !==''){  
					   
					if(validaCorreos(Ext.getCmp('correopelecUsdId').getValue()) > 0){ 
					    Ext.getCmp('correopelecUsdId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
					    Ext.getCmp('btnGuardar').disable();
					}else  {
					    Ext.getCmp('btnGuardar').enable();
					}				    
				    }else  {
                                        Ext.getCmp('btnGuardar').enable();   
                                   }
				}
			    }
			}			
		    },
		    {   width: 30, xtype: 'displayfield',  value: '&nbsp;'  }
		]
	    },
	    {	width: 600,  xtype: 'panel',  id: 'seccion_27',  layout: 'table',layoutConfig: {  columns: 6  },
		defaults: { align:'center', bodyStyle: 'padding: 2px,' },
		items: [
		    { xtype: 'button',	id: 'btnAyuda',	iconCls: 'icoAyuda', handler: function(){mostrarAyuda('3');  } 	},				
		    { width: 200,  frame: false, border:   false, html: '<div class="formas" align="left"><br><b> Operaciones :</b><br>&nbsp;</div>'   },
		    { width: 160, xtype: 'textarea', id: 'correoopeMnId', name: 'correoopeMn',	msgTarget:'side', margins: '0 20 0 0', maxLength:"400",
			listeners:     {
			    blur:       {
				fn: function(objTxt) {
				      var correo = Ext.getCmp('correoopeMnId').getValue();		    
				   
				    if(correo !==''){ 									   
					if(validaCorreos(Ext.getCmp('correoopeMnId').getValue()) > 0){ 
					    Ext.getCmp('correoopeMnId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
					   Ext.getCmp('btnGuardar').disable();
					}else  {
					    Ext.getCmp('btnGuardar').enable();
					}				    
				    }else  {
                                        Ext.getCmp('btnGuardar').enable();   
                                   }			    
				}
			    }
			}						    
		    },
		    {   width: 30,  xtype: 'displayfield', value:'&nbsp;'    },
		    {   width: 160, xtype: 'textarea', id: 'correoopeUsdId',  name: 'correoopeUsd',  msgTarget: 'side', margins: '0 20 0 0',  maxLength: "400",
			listeners:     {
			    blur:       {
				fn: function(objTxt) {
                                    var correo = Ext.getCmp('correoopeUsdId').getValue();
				    if(correo !==''){  									   
					if(validaCorreos(Ext.getCmp('correoopeUsdId').getValue()) > 0){ 
					    Ext.getCmp('correoopeUsdId').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
					    Ext.getCmp('btnGuardar').disable();
					}else  {
					    Ext.getCmp('btnGuardar').enable();
					}					
				    }else  {
                                        Ext.getCmp('btnGuardar').enable();   
                                   }			    
				}
			    }
			}			
		    },
		    {  width:  30,  xtype: 'displayfield', value:  '&nbsp;' }		
		]
	    }	
	];

	/********** Form Panel Notificaciones **********/
	var formaNotificacion = new Ext.form.FormPanel({
		width:       515,
		id:          'formaNotificacion',
		layout:      'form',
		style:       'margin:0 auto;',
		frame:       true,
		autoHeight:  true,
		hidden:      true,
		items: [{
			width:  500,
			xtype:  'panel',
			layout: 'table',
			border: true,
			layoutConfig: {
				columns: 1
			},
			defaults: {
				align:     'center',
				bodyStyle: 'padding: 2px,'
			},
			items: [
				{ width: 500, id: 'operacion_exitosa_id',  frame: true, border: false, html: '<div class="formas" align="center"><br><b> Los cambios se guardaron con &eacute;xito <br>&nbsp;</div>'},
				{ width: 500, id: 'operacion_fallida_id',  frame: true, border: false, html: '<div class="formas" align="center"><br><b> No se pudieron guardar los cambios <br>&nbsp;</div>'}
			]
		}]
	});
	
	/********** Form Panel Principal **********/
	var formaPrincipal = new Ext.form.FormPanel({
		width:        602,
		labelWidth:   230,
		id:           'formaPrincipal',
		title:        'Par�metros del Cotizador de Cr�ditos',
		layout:       'form',
		style:        'margin:0 auto;',
		defaults:     {xtype: 'textfield', msgTarget: 'side'},
		monitorValid: true,
		frame:        false,
		autoHeight:   true,
		items:        elementosForma,
		buttons: [
			{
				text:    'Guardar Cambios',
				id:      'btnGuardar',
				iconCls: 'icoAceptar',
				handler: procesarGuardar
			}
		]
	});
	
	/********** Contenedor Principal **********/
	var pnl = new Ext.Container({
		width:   949,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			formaNotificacion
		]
	});
	
	inicializar();
	
});