<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		java.text.SimpleDateFormat,
		java.math.*,
		netropology.utilerias.negocio.ParaSistemaBean,
		netropology.utilerias.usuarios.Usuario"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion                 = request.getParameter("informacion"                 ) == null?"":(String)request.getParameter("informacion"                 );
String inicio_consulta_hh          = request.getParameter("inicio_consulta_hh"          ) == null?"":(String)request.getParameter("inicio_consulta_hh"          );
String inicio_consulta_mm          = request.getParameter("inicio_consulta_mm"          ) == null?"":(String)request.getParameter("inicio_consulta_mm"          );
String termino_consulta_hh         = request.getParameter("termino_consulta_hh"         ) == null?"":(String)request.getParameter("termino_consulta_hh"         );
String termino_consulta_mm         = request.getParameter("termino_consulta_mm"         ) == null?"":(String)request.getParameter("termino_consulta_mm"         );
String inicio_confirmacion_hh      = request.getParameter("inicio_confirmacion_hh"      ) == null?"":(String)request.getParameter("inicio_confirmacion_hh"      );
String inicio_confirmacion_mm      = request.getParameter("inicio_confirmacion_mm"      ) == null?"":(String)request.getParameter("inicio_confirmacion_mm"      );
String termino_confirmacion_hh     = request.getParameter("termino_confirmacion_hh"     ) == null?"":(String)request.getParameter("termino_confirmacion_hh"     );
String termino_confirmacion_mm     = request.getParameter("termino_confirmacion_mm"     ) == null?"":(String)request.getParameter("termino_confirmacion_mm"     );
String redondeo                    = request.getParameter("redondeo"                    ) == null?"":(String)request.getParameter("redondeo"                    );
String monto_externos_mn           = request.getParameter("monto_externos_mn"           ) == null?"":(String)request.getParameter("monto_externos_mn"           );
String monto_externos_usd          = request.getParameter("monto_externos_usd"          ) == null?"":(String)request.getParameter("monto_externos_usd"          );
String plazo_externos_mn           = request.getParameter("plazo_externos_mn"           ) == null?"":(String)request.getParameter("plazo_externos_mn"           );
String plazo_externos_usd          = request.getParameter("plazo_externos_usd"          ) == null?"":(String)request.getParameter("plazo_externos_usd"          );
String estatus_mn                  = request.getParameter("estatus_mn"                  ) == null?"":(String)request.getParameter("estatus_mn"                  );
String estatus_usd                 = request.getParameter("estatus_usd"                 ) == null?"":(String)request.getParameter("estatus_usd"                 );
String plazo_operacion_mn          = request.getParameter("plazo_operacion_mn"          ) == null?"":(String)request.getParameter("plazo_operacion_mn"          );
String plazo_operacion_usd         = request.getParameter("plazo_operacion_usd"         ) == null?"":(String)request.getParameter("plazo_operacion_usd"         );
String max_disposiciones_mn        = request.getParameter("max_disposiciones_mn"        ) == null?"":(String)request.getParameter("max_disposiciones_mn"        );
String max_disposiciones_usd       = request.getParameter("max_disposiciones_usd"       ) == null?"":(String)request.getParameter("max_disposiciones_usd"       );
String tasa_impuesto_mn            = request.getParameter("tasa_impuesto_mn"            ) == null?"":(String)request.getParameter("tasa_impuesto_mn"            );
String tasa_impuesto_usd           = request.getParameter("tasa_impuesto_usd"           ) == null?"":(String)request.getParameter("tasa_impuesto_usd"           );
String dias_habiles_mn             = request.getParameter("dias_habiles_mn"             ) == null?"":(String)request.getParameter("dias_habiles_mn"             );
String dias_habiles_usd            = request.getParameter("dias_habiles_usd"            ) == null?"":(String)request.getParameter("dias_habiles_usd"            );
String tasas_protegidas_mn         = request.getParameter("tasas_protegidas_mn"         ) == null?"":(String)request.getParameter("tasas_protegidas_mn"         );
String plazo_linea_mn              = request.getParameter("plazo_linea_mn"              ) == null?"":(String)request.getParameter("plazo_linea_mn"              );
String plazo_linea_usd             = request.getParameter("plazo_linea_usd"             ) == null?"":(String)request.getParameter("plazo_linea_usd"             );
String disposiciones_linea_mn      = request.getParameter("disposiciones_linea_mn"      ) == null?"":(String)request.getParameter("disposiciones_linea_mn"      );
String disposiciones_linea_usd     = request.getParameter("disposiciones_linea_usd"     ) == null?"":(String)request.getParameter("disposiciones_linea_usd"     );
String monto_linea_mn              = request.getParameter("monto_linea_mn"              ) == null?"":(String)request.getParameter("monto_linea_mn"              );
String monto_linea_usd             = request.getParameter("monto_linea_usd"             ) == null?"":(String)request.getParameter("monto_linea_usd"             );
String plazo_recuperacion_mn       = request.getParameter("plazo_recuperacion_mn"       ) == null?"":(String)request.getParameter("plazo_recuperacion_mn"       );
String plazo_recuperacion_usd      = request.getParameter("plazo_recuperacion_usd"      ) == null?"":(String)request.getParameter("plazo_recuperacion_usd"      );
String interpolacion_mn            = request.getParameter("interpolacion_mn"            ) == null?"":(String)request.getParameter("interpolacion_mn"            );
String interpolacion_usd           = request.getParameter("interpolacion_usd"           ) == null?"":(String)request.getParameter("interpolacion_usd"           );
String metodologia_calculo_mn      = request.getParameter("metodologia_calculo_mn"      ) == null?"":(String)request.getParameter("metodologia_calculo_mn"      );
String metodologia_calculo_usd     = request.getParameter("metodologia_calculo_usd"     ) == null?"":(String)request.getParameter("metodologia_calculo_usd"     );
String cupon_cero_mn               = request.getParameter("cupon_cero_mn"               ) == null?"":(String)request.getParameter("cupon_cero_mn"               );
String cupon_cero_usd              = request.getParameter("cupon_cero_usd"              ) == null?"":(String)request.getParameter("cupon_cero_usd"              );
String tradicional_mn              = request.getParameter("tradicional_mn"              ) == null?"":(String)request.getParameter("tradicional_mn"              );
String tradicional_usd             = request.getParameter("tradicional_usd"             ) == null?"":(String)request.getParameter("tradicional_usd"             );
String plan_pagos_mn               = request.getParameter("plan_pagos_mn"               ) == null?"":(String)request.getParameter("plan_pagos_mn"               );
String plan_pagos_usd              = request.getParameter("plan_pagos_usd"              ) == null?"":(String)request.getParameter("plan_pagos_usd"              );
String renta_mn                    = request.getParameter("renta_mn"                    ) == null?"":(String)request.getParameter("renta_mn"                    );
String renta_usd                   = request.getParameter("renta_usd"                   ) == null?"":(String)request.getParameter("renta_usd"                   );
String tasa_disponible_fija_mn     = request.getParameter("tasa_disponible_fija_mn"     ) == null?"":(String)request.getParameter("tasa_disponible_fija_mn"     );
String tasa_disponible_fija_usd    = request.getParameter("tasa_disponible_fija_usd"    ) == null?"":(String)request.getParameter("tasa_disponible_fija_usd"    );
String tasa_disponible_variable_usd= request.getParameter("tasa_disponible_variable_usd") == null?"":(String)request.getParameter("tasa_disponible_variable_usd");
String correo_mn                   = request.getParameter("correoMn"                   ) == null?"":(String)request.getParameter("correoMn"                   );
String correo_usd                  = request.getParameter("correoUsd"                  ) == null?"":(String)request.getParameter("correoUsd"                  );

String correopelecMn   = request.getParameter("correopelecMn") == null?"":(String)request.getParameter("correopelecMn");
String correopelecUsd   = request.getParameter("correopelecUsd") == null?"":(String)request.getParameter("correopelecUsd");
String correoopeMn   = request.getParameter("correoopeMn") == null?"":(String)request.getParameter("correoopeMn");
String correoopeUsd   = request.getParameter("correoopeUsd") == null?"":(String)request.getParameter("correoopeUsd");
	
String mensaje       = "";
String accion        = "";
String infoRegresar  = "";
JSONObject resultado = new JSONObject();
ParaSistemaBean parametrosSistema = new ParaSistemaBean();

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("Inicializacion")){
	
	int ig_decimales                = 0;
	int iHoraInicioConsultaHrs      = 0;
	int iHoraInicioConsultaMin      = 0;
	int iHoraTerminoConsultaHrs     = 0;
	int iHoraTerminoConsultaMin     = 0;
	int iHoraInicioConfirmacionHrs  = 0;
	int iHoraInicioConfirmacionMin  = 0;
	int iHoraTerminoConfirmacionHrs = 0;
	int iHoraTerminoConfirmacionMin = 0;
	int iPlazoLimiteExternos        = 0;
	int iPlazoLimiteExternosDl      = 0;
	int ig_plazo_max_oper           = 0;
	int ig_plazo_max_oper_dl        = 0;
	int ig_num_max_disp             = 0;
	int ig_num_max_disp_dl          = 0;
	int ig_dias_solic_rec           = 0;
	int ig_dias_solic_rec_dl        = 0;
	int ig_plazo_max_cot            = 0;
	int ig_num_max_disp_cot         = 0;
	int ig_plazo_max_cupon          = 0;
	int ig_plazo_max_cot_dl         = 0;
	int ig_num_max_disp_cot_dl      = 0;
	int ig_plazo_max_cupon_dl       = 0;
	double dMontoLimiteExternos     = 0d;
	double dMontoLimiteExternosDl   = 0d;
	double ig_tasa_impuesto         = 0;
	double ig_tasa_impuesto_dl      = 0;
	double ig_techo_tasa_p          = 0;
	double ig_monto_max_cot         = 0;
	double ig_monto_max_cot_dl      = 0;
	String sHoraInicioConsultaHrs      = "";
	String sHoraInicioConsultaMin      = "";
	String sHoraTerminoConsultaHrs     = "";
	String sHoraTerminoConsultaMin     = "";
	String sHoraInicioConfirmacionHrs  = "";
	String sHoraInicioConfirmacionMin  = "";
	String sHoraTerminoConfirmacionHrs = "";
	String sHoraTerminoConfirmacionMin = "";
	String cg_correo                   = "";
	String cg_correo_dl                = "";
	String cg_tasa_f                   = "N";
	String cg_tasa_f_dl                = "N";
	String cg_tasa_v                   = "N";
	String cg_tasa_v_dl                = "N";
	String cg_metodo_inter             = "A";
	String cg_metodo_inter_dl          = "L";
	String cg_metodo_calc              = "D";
	String cg_metodo_calc_dl           = "P";
	boolean bolEstatus                 = false;
	boolean bolEstatusDl               = false;
	List lEsquemasRecup                = null;
	try{
		if(parametrosSistema.load()){
			if(parametrosSistema.loadParametros()){
				
				iHoraInicioConsultaHrs      = parametrosSistema.getHoraInicioConsulta().get(parametrosSistema.getHoraInicioConsulta().HOUR_OF_DAY);
				iHoraInicioConsultaMin      = parametrosSistema.getHoraInicioConsulta().get(parametrosSistema.getHoraInicioConsulta().MINUTE);
				iHoraTerminoConsultaHrs     = parametrosSistema.getHoraTerminoConsulta().get(parametrosSistema.getHoraTerminoConsulta().HOUR_OF_DAY);
				iHoraTerminoConsultaMin     = parametrosSistema.getHoraTerminoConsulta().get(parametrosSistema.getHoraTerminoConsulta().MINUTE);
				iHoraInicioConfirmacionHrs  = parametrosSistema.getHoraInicioConfirmacion().get(parametrosSistema.getHoraInicioConfirmacion().HOUR_OF_DAY);
				iHoraInicioConfirmacionMin  = parametrosSistema.getHoraInicioConfirmacion().get(parametrosSistema.getHoraInicioConfirmacion().MINUTE);
				iHoraTerminoConfirmacionHrs = parametrosSistema.getHoraTerminoConfirmacion().get(parametrosSistema.getHoraTerminoConfirmacion().HOUR_OF_DAY);
				iHoraTerminoConfirmacionMin = parametrosSistema.getHoraTerminoConfirmacion().get(parametrosSistema.getHoraTerminoConfirmacion().MINUTE);
				dMontoLimiteExternos        = parametrosSistema.getMontoLimiteExternos();
				dMontoLimiteExternosDl      = parametrosSistema.getMontoLimiteExternosDl();
				iPlazoLimiteExternos        = parametrosSistema.getPlazoLimiteExternos();
				iPlazoLimiteExternosDl      = parametrosSistema.getPlazoLimiteExternosDl();
				bolEstatus                  = parametrosSistema.getEstatus();
				bolEstatusDl                = parametrosSistema.getEstatusDl();
				ig_plazo_max_oper           = parametrosSistema.getIg_plazo_max_oper();
				ig_plazo_max_oper_dl        = parametrosSistema.getIg_plazo_max_oper_dl();
				ig_num_max_disp             = parametrosSistema.getIg_num_max_disp();
				ig_num_max_disp_dl          = parametrosSistema.getIg_num_max_disp_dl();
				ig_tasa_impuesto_dl         = parametrosSistema.getIg_tasa_impuesto_dl();
				ig_tasa_impuesto            = parametrosSistema.getIg_tasa_impuesto();
				ig_dias_solic_rec           = parametrosSistema.getIg_dias_solic_rec();
				ig_dias_solic_rec_dl        = parametrosSistema.getIg_dias_solic_rec_dl();
				ig_decimales                = parametrosSistema.getIg_decimales();
				cg_metodo_inter             = parametrosSistema.getCg_metodo_inter();
				cg_metodo_inter_dl          = parametrosSistema.getCg_metodo_inter_dl();
				cg_metodo_calc              = parametrosSistema.getCg_metodo_calc();
				cg_metodo_calc_dl           = parametrosSistema.getCg_metodo_calc_dl();
				ig_techo_tasa_p             = parametrosSistema.getIg_techo_tasa_p();
				ig_plazo_max_cot            = parametrosSistema.getIg_plazo_max_cot();
				ig_num_max_disp_cot         = parametrosSistema.getIg_num_max_disp_cot();
				ig_monto_max_cot            = parametrosSistema.getIg_monto_max_cot();
				ig_plazo_max_cupon          = parametrosSistema.getIg_plazo_max_cupon();
				ig_plazo_max_cot_dl         = parametrosSistema.getIg_plazo_max_cot_dl();
				ig_num_max_disp_cot_dl      = parametrosSistema.getIg_num_max_disp_cot_dl();
				ig_monto_max_cot_dl         = parametrosSistema.getIg_monto_max_cot_dl();
				ig_plazo_max_cupon_dl       = parametrosSistema.getIg_plazo_max_cupon_dl();
				lEsquemasRecup              = parametrosSistema.getEsquemasRecup();
				cg_correo                   = parametrosSistema.getCg_correo();
				cg_correo_dl                = parametrosSistema.getCg_correo_dl();
				cg_tasa_f                   = parametrosSistema.getCg_tasa_f();
				cg_tasa_f_dl                = parametrosSistema.getCg_tasa_f_dl();
				cg_tasa_v                   = parametrosSistema.getCg_tasa_v();
				cg_tasa_v_dl                = parametrosSistema.getCg_tasa_v_dl();
				lEsquemasRecup              = parametrosSistema.getEsquemasRecup();
				
				correopelecMn =   parametrosSistema.getCorreopelecMn();
				correopelecUsd = parametrosSistema.getCorreopelecUsd();
				correoopeMn = parametrosSistema.getCorreoopeMn();
				correoopeUsd =  parametrosSistema.getCorreoopeUsd();
				
				
				/***** Se le da formato a las variables de hora y minutos *****/
				if(iHoraInicioConsultaHrs < 10      ){ sHoraInicioConsultaHrs = "0" + iHoraInicioConsultaHrs;           } else { sHoraInicioConsultaHrs = "" + iHoraInicioConsultaHrs;           }
				if(iHoraInicioConsultaMin < 10      ){ sHoraInicioConsultaMin = "0" + iHoraInicioConsultaMin;           } else { sHoraInicioConsultaMin = "" + iHoraInicioConsultaMin;           }
				if(iHoraTerminoConsultaHrs < 10     ){ sHoraTerminoConsultaHrs = "0" + iHoraTerminoConsultaHrs;         } else { sHoraTerminoConsultaHrs = "" + iHoraTerminoConsultaHrs;         }
				if(iHoraTerminoConsultaMin < 10     ){ sHoraTerminoConsultaMin = "0" + iHoraTerminoConsultaMin;         } else { sHoraTerminoConsultaMin = "" + iHoraTerminoConsultaMin;         }
				if(iHoraInicioConfirmacionHrs < 10  ){ sHoraInicioConfirmacionHrs = "0" + iHoraInicioConfirmacionHrs;   } else { sHoraInicioConfirmacionHrs = "" + iHoraInicioConfirmacionHrs;   }
				if(iHoraInicioConfirmacionMin < 10  ){ sHoraInicioConfirmacionMin = "0" + iHoraInicioConfirmacionMin;   } else { sHoraInicioConfirmacionMin = "" + iHoraInicioConfirmacionMin;   }
				if(iHoraTerminoConfirmacionHrs < 10 ){ sHoraTerminoConfirmacionHrs = "0" + iHoraTerminoConfirmacionHrs; } else { sHoraTerminoConfirmacionHrs = "" + iHoraTerminoConfirmacionHrs; }
				if(iHoraTerminoConfirmacionMin < 10 ){ sHoraTerminoConfirmacionMin = "0" + iHoraTerminoConfirmacionMin; } else { sHoraTerminoConfirmacionMin = "" + iHoraTerminoConfirmacionMin; }
				
				/***** Se procesan las variables para llenar los combos ylos checkbox *****/
				String strEstatus = "", strEstatusDl = "";
				boolean bl_cg_tasa_f = false, bl_cg_tasa_f_dl = false, bl_cg_tasa_v = false, bl_cg_tasa_v_dl = false;
				if(bolEstatus==true)        { strEstatus ="1";        } else{ strEstatus ="0";         }
				if(bolEstatusDl==true)      { strEstatusDl ="1";      } else{ strEstatusDl ="0";       }
				if(cg_tasa_f.equals("S"))   { bl_cg_tasa_f = true;    } else{ bl_cg_tasa_f = false;    }
				if(cg_tasa_f_dl.equals("S")){ bl_cg_tasa_f_dl = true; } else{ bl_cg_tasa_f_dl = false; }
				if(cg_tasa_v.equals("S"))   { bl_cg_tasa_v = true;    } else{ bl_cg_tasa_v = false;    }
				if(cg_tasa_v_dl.equals("S")){ bl_cg_tasa_v_dl = true; } else{ bl_cg_tasa_v_dl = false; }
				
				/***** Se procesan las variables para los datos de la lista *****/
				String strTemp = "", disponible_mn = "", disponible_dl = "";
				boolean cuponCero = false, tradicional = false, planPagos = false, renta = false;
				HashMap mColumnas = new HashMap();
				if(lEsquemasRecup.size() > 0){
					for(int i = 0; i < lEsquemasRecup.size(); i++){
						mColumnas = new HashMap();
						mColumnas = (HashMap)lEsquemasRecup.get(i);
						strTemp = (""+mColumnas.get("cg_descripcion")).trim();
						disponible_mn = (""+mColumnas.get("cs_disponible_if_mn")).trim();
						disponible_dl = (""+mColumnas.get("cs_disponible_if_dl")).trim();
						
						if(strTemp.equals("Cupon Cero")){
							cuponCero = true;
							if(disponible_mn.equals("S")){
								resultado.put("cupon_cero_mn_id", new Boolean(true));
							} else{
								resultado.put("cupon_cero_mn_id", new Boolean(false));
							}
							if(disponible_dl.equals("S")){
								resultado.put("cupon_cero_usd_id", new Boolean(true));
							} else{
								resultado.put("cupon_cero_usd_id", new Boolean(false));
							}
						} else if(strTemp.equals("Tradicional")){
							tradicional = true;
							if(disponible_mn.equals("S")){
								resultado.put("tradicional_mn_id", new Boolean(true));
							} else{
								resultado.put("tradicional_mn_id", new Boolean(false));
							}
							if(disponible_dl.equals("S")){
								resultado.put("tradicional_usd_id", new Boolean(true));
							} else{
								resultado.put("tradicional_usd_id", new Boolean(false));
							}
						} else if(strTemp.equals("Plan de Pagos")){
							planPagos = true;
							if(disponible_mn.equals("S")){
								resultado.put("plan_pagos_mn_id", new Boolean(true));
							} else{
								resultado.put("plan_pagos_mn_id", new Boolean(false));
							}
							if(disponible_dl.equals("S")){
								resultado.put("plan_pagos_usd_id", new Boolean(true));
							} else{
								resultado.put("plan_pagos_usd_id", new Boolean(false));
							}
						} else if(strTemp.equals("Renta")){
							renta = true;
							if(disponible_mn.equals("S")){
								resultado.put("renta_mn_id", new Boolean(true));
							} else{
								resultado.put("renta_mn_id", new Boolean(false));
							}
							if(disponible_dl.equals("S")){
								resultado.put("renta_usd_id", new Boolean(true));
							} else{
								resultado.put("renta_usd_id", new Boolean(false));
							}
						}
					}
				}
				
				/**
					Las siguientes 4 lineas determinan que sección se mostrará de:
					Esquemas de recuperación disponibles para IFs de Cotización en Línea 
				*/
				resultado.put("cuponCero",   new Boolean(cuponCero)  );
				resultado.put("tradicional", new Boolean(tradicional));
				resultado.put("planPagos",   new Boolean(planPagos)  );
				resultado.put("renta",       new Boolean(renta)      );
				
				resultado.put("inicio_consulta_hh_id",           sHoraInicioConsultaHrs        );
				resultado.put("inicio_consulta_mm_id",           sHoraInicioConsultaMin        );
				resultado.put("termino_consulta_hh_id",          sHoraTerminoConsultaHrs       );
				resultado.put("termino_consulta_mm_id",          sHoraTerminoConsultaMin       );
				resultado.put("inicio_confirmacion_hh_id",       sHoraInicioConfirmacionHrs    );
				resultado.put("inicio_confirmacion_mm_id",       sHoraInicioConfirmacionMin    );
				resultado.put("termino_confirmacion_hh_id",      sHoraTerminoConfirmacionHrs   );
				resultado.put("termino_confirmacion_mm_id",      sHoraTerminoConfirmacionMin   );
				resultado.put("redondeo_id",                     ""+ig_decimales               );
				resultado.put("monto_externos_mn_id",            ""+dMontoLimiteExternos       );
				resultado.put("monto_externos_usd_id",           ""+dMontoLimiteExternosDl     );
				resultado.put("plazo_externos_mn_id",            ""+iPlazoLimiteExternos       );
				resultado.put("plazo_externos_usd_id",           ""+iPlazoLimiteExternosDl     );
				resultado.put("estatus_mn_id",                   strEstatus                    );
				resultado.put("estatus_usd_id",                  strEstatusDl                  );
				resultado.put("plazo_operacion_mn_id",           ""+ig_plazo_max_oper          );
				resultado.put("plazo_operacion_usd_id",          ""+ig_plazo_max_oper_dl       );
				resultado.put("max_disposiciones_mn_id",         ""+ig_num_max_disp            );
				resultado.put("max_disposiciones_usd_id",        ""+ig_num_max_disp_dl         );
				resultado.put("tasa_impuesto_mn_id",             ""+ig_tasa_impuesto           );
				resultado.put("tasa_impuesto_usd_id",            ""+ig_tasa_impuesto_dl        );
				resultado.put("dias_habiles_mn_id",              ""+ig_dias_solic_rec          );
				resultado.put("dias_habiles_usd_id",             ""+ig_dias_solic_rec_dl       );
				resultado.put("tasas_protegidas_mn_id",          ""+ig_techo_tasa_p            );
				resultado.put("plazo_linea_mn_id",               ""+ig_plazo_max_cot           );
				resultado.put("plazo_linea_usd_id",              ""+ig_plazo_max_cot_dl        );
				resultado.put("disposiciones_linea_mn_id",       ""+ig_num_max_disp_cot        );
				resultado.put("disposiciones_linea_usd_id",      ""+ig_num_max_disp_cot_dl     );
				resultado.put("monto_linea_mn_id",               ""+ig_monto_max_cot           );
				resultado.put("monto_linea_usd_id",              ""+ig_monto_max_cot_dl        );
				resultado.put("plazo_recuperacion_mn_id",        ""+ig_plazo_max_cupon         );
				resultado.put("plazo_recuperacion_usd_id",       ""+ig_plazo_max_cupon_dl      );
				resultado.put("interpolacion_mn_id",             cg_metodo_inter               );
				resultado.put("interpolacion_usd_id",            cg_metodo_inter_dl            );
				resultado.put("metodologia_calculo_mn_id",       cg_metodo_calc                );
				resultado.put("metodologia_calculo_usd_id",      cg_metodo_calc_dl             );
				resultado.put("tasa_disponible_fija_mn_id",      new Boolean(bl_cg_tasa_f)     );
				resultado.put("tasa_disponible_fija_usd_id",     new Boolean(bl_cg_tasa_f_dl)  );
				resultado.put("tasa_disponible_variable_usd_id", new Boolean(bl_cg_tasa_v_dl)  );
				resultado.put("correoMnId",                    cg_correo                     );
				resultado.put("correoUsdId",                   cg_correo_dl                  );
				resultado.put("success",                         new Boolean(true)             );				
				resultado.put("correopelecMnId",               correopelecMn                     );
				resultado.put("correopelecUsdId",              correopelecUsd                  );				
				resultado.put("correoopeMnId",                 correoopeMn                     );
				resultado.put("correoopeUsdId",                correoopeUsd                  );
				
				
				
					
		} else{
			resultado.put("success", new Boolean(false));
			};
		}
	} catch(Exception e){
		resultado.put("success", new Boolean(false));
		mensaje = "Error al abrir la conexion. " + e;
	} finally{
		parametrosSistema.release();
	}
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("Guardar_Cambios")){
	
	/***** Se procesan la variables de la sección: Esquemas de recuperación disponibles para IFs de Cotización en Línea *****/ 
	String strDisponibleIfMn = "";
	String strDisponibleIfDl = "";
	String disponibleIfMn[]  = null;
	String disponibleIfDl[]  = null;
	if(cupon_cero_mn.equals("on"))   { strDisponibleIfMn += "1,"; }
	if(tradicional_mn.equals("on"))  { strDisponibleIfMn += "2,"; }
	if(plan_pagos_mn.equals("on"))   { strDisponibleIfMn += "3,"; }
	if(renta_mn.equals("on"))        { strDisponibleIfMn += "4,"; }
	if(cupon_cero_usd.equals("on"))  { strDisponibleIfDl += "1,"; }
	if(tradicional_usd.equals("on")) { strDisponibleIfDl += "2,"; }
	if(plan_pagos_usd.equals("on"))  { strDisponibleIfDl += "3,"; }
	if(renta_usd.equals("on"))       { strDisponibleIfDl += "4,"; }
	
	if(strDisponibleIfMn.length() > 0 ){
		strDisponibleIfMn = strDisponibleIfMn.substring(0, strDisponibleIfMn.length()-1);
		disponibleIfMn = strDisponibleIfMn.split(",");
	}
	if(strDisponibleIfDl.length() > 0){
		strDisponibleIfDl = strDisponibleIfDl.substring(0, strDisponibleIfDl.length()-1);
		disponibleIfDl = strDisponibleIfDl.split(",");
	}
	
	/***** Se procesan las variables del Estatus del Sistema *****/
	boolean bl_estatus_mn  = false;
	boolean bl_estatus_usd = false;
	if(estatus_mn.equals("1"))  { bl_estatus_mn  = true; }
	if(estatus_usd.equals("1")) { bl_estatus_usd = true; }
	
	/***** Se procesan la variables de la sección Tipo de Tasa Disponible para IF en Cotizaciones en Línea *****/
	String strTasa_disp_fija_mn  = "N";
	String strTasa_disp_fija_usd = "N";
	String strTasa_disp_var_mn   = "N";
	String strTasa_disp_var_usd  = "N";
	if(!tasa_disponible_fija_mn.equals(""))     { strTasa_disp_fija_mn  = "S"; }
	if(!tasa_disponible_fija_usd.equals(""))    { strTasa_disp_fija_usd = "S"; }
	if(!tasa_disponible_variable_usd.equals("")){ strTasa_disp_var_usd  = "S"; }
	
	parametrosSistema.setHoraInicioConsulta(Integer.parseInt(inicio_consulta_hh), Integer.parseInt(inicio_consulta_mm));
	parametrosSistema.setHoraTerminoConsulta(Integer.parseInt(termino_consulta_hh), Integer.parseInt(termino_consulta_mm));
	parametrosSistema.setHoraInicioConfirmacion(Integer.parseInt(inicio_confirmacion_hh), Integer.parseInt(inicio_confirmacion_mm));
	parametrosSistema.setHoraTerminoConfirmacion(Integer.parseInt(termino_confirmacion_hh), Integer.parseInt(termino_confirmacion_mm));
	parametrosSistema.setMontoLimiteExternos(Double.parseDouble(monto_externos_mn));
	parametrosSistema.setMontoLimiteExternosDl(Double.parseDouble(monto_externos_usd));
	parametrosSistema.setPlazoLimiteExternos(Integer.parseInt(plazo_externos_mn));
	parametrosSistema.setPlazoLimiteExternosDl(Integer.parseInt(plazo_externos_usd));
	parametrosSistema.setEstatus(bl_estatus_mn);
	parametrosSistema.setEstatusDl(bl_estatus_usd);
	parametrosSistema.setIg_plazo_max_oper(Integer.parseInt(plazo_operacion_mn));
	parametrosSistema.setIg_plazo_max_oper_dl(Integer.parseInt(plazo_operacion_usd));
	parametrosSistema.setIg_num_max_disp(Integer.parseInt(max_disposiciones_mn));
	parametrosSistema.setIg_num_max_disp_dl(Integer.parseInt(max_disposiciones_usd));
	parametrosSistema.setIg_tasa_impuesto(Double.parseDouble(tasa_impuesto_mn));
	parametrosSistema.setIg_tasa_impuesto_dl(Double.parseDouble(tasa_impuesto_usd));
	parametrosSistema.setIg_decimales(Integer.parseInt(redondeo));
	parametrosSistema.setIg_dias_solic_rec(Integer.parseInt(dias_habiles_mn));
	parametrosSistema.setIg_dias_solic_rec_dl(Integer.parseInt(dias_habiles_usd));
	parametrosSistema.setCg_metodo_calc(metodologia_calculo_mn);
	parametrosSistema.setCg_metodo_calc_dl(metodologia_calculo_usd);
	parametrosSistema.setCg_metodo_inter(interpolacion_mn);
	parametrosSistema.setCg_metodo_inter_dl(interpolacion_usd);
	parametrosSistema.setCs_disponible_if_mn(disponibleIfMn);
	parametrosSistema.setCs_disponible_if_dl(disponibleIfDl);
	parametrosSistema.setIg_techo_tasa_p(Double.parseDouble(tasas_protegidas_mn));
	parametrosSistema.setIg_plazo_max_cot(Integer.parseInt(plazo_linea_mn));
	parametrosSistema.setIg_num_max_disp_cot(Integer.parseInt(disposiciones_linea_mn));
	parametrosSistema.setIg_monto_max_cot(Double.parseDouble(monto_linea_mn));
	parametrosSistema.setIg_plazo_max_cupon(Integer.parseInt(plazo_recuperacion_mn));
	parametrosSistema.setIg_plazo_max_cot_dl(Integer.parseInt(plazo_linea_usd));
	parametrosSistema.setIg_num_max_disp_cot_dl(Integer.parseInt(disposiciones_linea_usd));
	parametrosSistema.setIg_monto_max_cot_dl(Double.parseDouble(monto_linea_usd));
	parametrosSistema.setIg_plazo_max_cupon_dl(Integer.parseInt(plazo_recuperacion_usd));
	parametrosSistema.setCg_correo(correo_mn);
	parametrosSistema.setCg_correo_dl(correo_usd);
	parametrosSistema.setCg_tasa_f(strTasa_disp_fija_mn);
	parametrosSistema.setCg_tasa_f_dl(strTasa_disp_fija_usd);
	parametrosSistema.setCg_tasa_v(strTasa_disp_var_mn);
	parametrosSistema.setCg_tasa_v_dl(strTasa_disp_var_usd);	
	parametrosSistema.setCorreopelecMn(correopelecMn);
	parametrosSistema.setCorreopelecUsd(correopelecUsd);	
	parametrosSistema.setCorreoopeMn(correoopeMn);
	parametrosSistema.setCorreoopeUsd(correoopeUsd);
	
	
	try{
		if(parametrosSistema.load()){
			if(parametrosSistema.guardarCambios()){
				accion = "exito";
			} else{
				accion = "error";
			}		
		} else{
			mensaje = "Error al guardar los cambios.";
		}

	} catch(Exception e){
		accion  = "error";
		mensaje = "Error al guardar los cambios. " + e;
		log.warn("Error al guardar los cambios. " + e);
	} finally{
		parametrosSistema.release();
	}
	
	resultado.put("success", new Boolean(true));
	resultado.put("accion",  accion           );
	resultado.put("mensaje", mensaje          );
	infoRegresar = resultado.toString();
}
%>
<%=infoRegresar%>