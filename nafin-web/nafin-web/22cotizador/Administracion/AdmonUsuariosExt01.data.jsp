<%@ page 
	contentType=
		"application/json;charset=UTF-8" 
	import="
		 com.netro.cotizador.*,
		 net.sf.json.JSONArray, 
		 net.sf.json.JSONObject,
		 java.util.*,
		 org.apache.commons.logging.Log  " 
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="../22secsession_extjs.jspf" %>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%    
String informacion	= (request.getParameter("informacion")	!=null)	?	request.getParameter("informacion")	:	"";
String operacion		= (request.getParameter("operacion")	!=null)	?	request.getParameter("operacion")	:	"";


String intermediario = (request.getParameter("cmb_ti")	!=null)	?	request.getParameter("cmb_ti")	:	"";
String persona	      = (request.getParameter("cmb_per")	!=null)	?	request.getParameter("cmb_per")	:	"";

String infoRegresar	= "";
String consulta   	= "";
int  start= 0, limit =0;

if (informacion.equals("catalogoint") )	{
	
   ConsultaSolicitudes cs = new ConsultaSolicitudes();	
	Registros reg= cs.catalogoIntermediarioDos();
	JSONObject		 jsonObj  = new JSONObject();
	jsonObj.put("total",new Integer( reg.getNumeroRegistros()) );	
	jsonObj.put("registros", reg.getJSONData() );	
   infoRegresar = jsonObj.toString();	

} else if (informacion.equals("catalogoPersona") )	{
	
   ConsultaSolicitudes cs = new ConsultaSolicitudes();	
	Registros reg= cs.tipoIntermediario();
	JSONObject		 jsonObj  = new JSONObject();
	jsonObj.put("total",new Integer( reg.getNumeroRegistros()) );	
	jsonObj.put("registros", reg.getJSONData() );	
   infoRegresar = jsonObj.toString();	

} else if (informacion.equals("ConsultarAsignacion") )	{
	
	ConsultaSolicitudes cs = new ConsultaSolicitudes();
	cs.setIntermediario(intermediario);
	cs.setTipoIntermedario(persona);
	Registros reg= cs.obtenerAsignacion();
	JSONObject		 jsonObj  = new JSONObject();
	jsonObj.put("total",new Integer( reg.getNumeroRegistros()) );	
	jsonObj.put("registros", reg.getJSONData() );	
   infoRegresar = jsonObj.toString();	

} else if (informacion.equals("guardar") )	{

	ConsultaSolicitudes cs = new ConsultaSolicitudes();
	cs.setIntermediario(intermediario);
	cs.setTipoIntermedario(persona);
	boolean guardar= cs.guardar();
	JSONObject		 jsonObj  = new JSONObject();
	int valor=0;
	if(guardar){
		valor=1;
	}
	String consultaUno = 	"{\"success\": true,\"guardar\":"+valor+"}";
	jsonObj = JSONObject.fromObject(consultaUno);
	infoRegresar = jsonObj.toString();
	log.debug("infoRegresar"+infoRegresar);
	
}  else if (informacion.equals("eliminar") )	{

	ConsultaSolicitudes cs = new ConsultaSolicitudes();
	cs.setIntermediario(intermediario);
	cs.setTipoIntermedario(persona);
	boolean eliminar= cs.eliminar();
	JSONObject		 jsonObj  = new JSONObject();
	int valor=0;
	if(eliminar){
		valor=1;
	}
	String strEliminar = 	"{\"success\": true,\"eliminar\":"+valor+"}";
	jsonObj = JSONObject.fromObject(strEliminar);
	infoRegresar = jsonObj.toString();
	log.debug("infoRegresar"+infoRegresar);
	
}

%>
<%=  infoRegresar %>