<%@  page contentType="application/json;charset=UTF-8"
import="java.util.*,
com.netro.cotizador.*,
javax.naming.*,
net.sf.json.JSONObject,
com.netro.exception.*,
com.netro.pdf.*,
netropology.utilerias.*"

errorPage="/00utils/error_extjs.jsp"%> 

	<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
<%
String infoRegresar="";
JSONObject jsonObj 	      = new JSONObject();

SolCotEsp solCotEsp = ServiceLocator.getInstance().lookup("SolCotEspEJB", SolCotEsp.class);

String ic_solicitud = (request.getParameter("ic_solicitud")    != null) ?   request.getParameter("ic_solicitud") :"";
String estatus_solicitud = (request.getParameter("estatus_solicitud")==null)?"":request.getParameter("estatus_solicitud");
String intermediario = (request.getParameter("intermediario")==null)?"":request.getParameter("intermediario");
String origen		= (request.getParameter("origen")==null)?"":request.getParameter("origen");
String recibo		= (request.getParameter("recibo")==null)?"":request.getParameter("recibo");


String	nombreArchivo= "";
String periodo = "";
boolean validaPDF = false;
String clase="";
int tipoClase=0;

try{
Solicitud consulta = new Solicitud();
consulta = solCotEsp.consultaSolicitud(ic_solicitud);

nombreArchivo= consulta.getNumero_solicitud()+".pdf";
boolean muestraCancelado = false;
if("8".equals(estatus_solicitud))
	muestraCancelado = true;
ComunesPDF	pdfDoc			= new ComunesPDF(1,strDirectorioTemp+nombreArchivo,"Solicitud Número: "+ consulta.getNumero_solicitud() +"           Pagina ", muestraCancelado);
float widthsDocs[] = {0.35f,0.75f};
%>	


<%	
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());


    pdfDoc.encabezadoConImagenes(
		pdfDoc,
		(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String)session.getAttribute("strNombre"),
		(String)session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),
		strDirectorioPublicacion);	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
	
	if (!"".equalsIgnoreCase(recibo)   ) { 
	    pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
	    pdfDoc.addText("La autentificación se llevó a cabo con éxito","formas",ComunesPDF.CENTER);
	    pdfDoc.addText("Recibo: "+recibo,"formas",ComunesPDF.CENTER);
	    pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
	}
	pdfDoc.setTable(2,65);
	pdfDoc.setCell("Datos de la Cotización","celda04",ComunesPDF.CENTER,2,1,1);
	pdfDoc.setCell("Número de Solicitud:","formas",ComunesPDF.LEFT,1,1,1);
	pdfDoc.setCell(consulta.getNumero_solicitud(),"formas",ComunesPDF.LEFT,1,1,1);

	pdfDoc.setCell("Fecha de Solicitud:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
	pdfDoc.setCell(consulta.getDf_solicitud(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
	pdfDoc.setCell("Hora de Solicitud:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
	pdfDoc.setCell(consulta.getHora_solicitud(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);		
	
	if( !estatus_solicitud.equals("1") & !estatus_solicitud.equals("3") & !estatus_solicitud.equals("4") ){//1=Solicictada,3=Rechazada,4=En proceso
		String auxFecha = "Cotización";
		if("8".equals(estatus_solicitud))
			auxFecha = "Cancelación";		
		pdfDoc.setCell("Fecha de "+auxFecha+":",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
		pdfDoc.setCell(consulta.getDf_cotizacion(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);		
		pdfDoc.setCell("Hora de "+auxFecha+":",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
		pdfDoc.setCell(consulta.getHora_cotizacion(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
	}
		pdfDoc.setCell("Usuario:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
		pdfDoc.setCell(consulta.getIc_usuario() +" - "+consulta.getNombre_usuario(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
	if(estatus_solicitud.equals("7")||estatus_solicitud.equals("8")||estatus_solicitud.equals("9")||origen.equals("TF")){
		if(!origen.equals("TF")){
			pdfDoc.setCell("Número de Recibo:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);
			pdfDoc.setCell(consulta.getIg_num_referencia(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,1);	
		}
		pdfDoc.setCell(
			"En caso de que la disposición de los recursos no se lleve a cabo total o parcialmente\n"+
			"en la fecha estipulada, Nacional Financiera aplicará el cobro de una comisión\n"+
			"sobre los montos no dispuestos, conforme a las políticas de crédito aplicables\n"+
			"a este tipo de operaciones."
		,"formas",ComunesPDF.CENTER,2,1,1);
	}
	pdfDoc.addTable();
	pdfDoc.addText("\n","formas",ComunesPDF.LEFT);
	pdfDoc.setTable(2,90,widthsDocs);
/*	pdfDoc.setCell("Intermediario / Acreditado","formas",ComunesPDF.LEFT,1,1,0);
	if("IF".equals(strTipoUsuario)){
		intermediario=strNombre;
	}
	pdfDoc.setCell(intermediario + ((!"".equals(intermediario)&&!"".equals(consulta.getCg_acreditado()))?" / ":"")+consulta.getCg_acreditado() ,"formas",ComunesPDF.LEFT,1,1,0);
*/	
	tipoClase=0;
	
	if(!origen.equals("TFE")){
		pdfDoc.setCell("Datos del Ejecutivo Nafin","celda04",ComunesPDF.LEFT,2,1,0);
		pdfDoc.setCell("Nombre del Ejecutivo","formas",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(consulta.getNombre_ejecutivo(),"formas",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Correo electrónico",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(consulta.getCg_mail(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Teléfono",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(consulta.getCg_telefono(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Fax",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(consulta.getCg_fax(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	}
	
	tipoClase=0;
	String tipooper = consulta.getTipoOper();
	
	if(!origen.equals("TFE")){
		pdfDoc.setCell("Datos del Intermediario Financiero/Acreditado","celda04",ComunesPDF.LEFT,2,1,0);
		pdfDoc.setCell("Tipo de operación","formas",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(tipooper,"formas",ComunesPDF.LEFT,1,1,0);
		
		if(!"Primer Piso".equals(tipooper)){
			pdfDoc.setCell("Nombre del Intermediario",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell((!"".equals(consulta.getNombre_if_ci()))?consulta.getNombre_if_ci():consulta.getNombre_if_cs(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			if(!"IF".equals(strTipoUsuario)&&!"".equals(consulta.getRiesgoIf())){
				pdfDoc.setCell("Riesgo del Intermediario",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(consulta.getRiesgoIf(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			}
		}	
	}
	
	if(!"IF".equals(strTipoUsuario)&&!"".equals(consulta.getCg_acreditado())){
		pdfDoc.setCell("Nombre del Acreditado",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(consulta.getCg_acreditado(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		if("Primer Piso".equals(tipooper)){
			if(!origen.equals("TFE")){
				pdfDoc.setCell("Riesgo del Acreditado",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(consulta.getRiesgoAc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			}
  		}
	}
	
	tipoClase=1;
	
	pdfDoc.setCell("Características de la Operación","celda04",ComunesPDF.LEFT,2,1,0);

	if(!"".equals(consulta.getCg_programa())){
		pdfDoc.setCell("Nombre del programa o\nacreditado final:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(consulta.getCg_programa(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);	
	}
	pdfDoc.setCell("Monto total requerido:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(consulta.getFn_monto(),2)+(consulta.getIc_moneda().equals("1")?" Pesos":" Dólares"),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell("Tipo de tasa requerida:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell(consulta.getTipo_tasa(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell("Número de Disposiciones",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell(consulta.getIn_num_disp(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	if(Integer.parseInt(consulta.getIn_num_disp())>1){
		pdfDoc.setCell("Disposiciones múltiples. Tasa de Interés:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(("S".equals(consulta.getCs_tasa_unica())?"Única":"Varias"),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	}
	pdfDoc.setCell("Plazo de la operación:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell(consulta.getIg_plazo()+" Días",((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell("Esquema de recuperación del capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell(consulta.getEsquema_recup(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
	if(!"1".equals(consulta.getIc_esquema_recup())){
		String ic_esquema_recup = consulta.getIc_esquema_recup();//Valores que puede tomar ic_esquema_recup :
																 //1=Cupon cero,2=Tradicional,3=Plan de pagos,4=Rentas
		if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("3")){
			pdfDoc.setCell("Periodicidad del pago de Interés:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			//pdfDoc.setCell(consulta.getIg_ppi()+" "+consulta.descripcionUT(consulta.getCg_ut_ppi()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(consulta.periodoTasa(consulta.getCg_ut_ppi()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		}
		if(!"3".equals(consulta.getIc_esquema_recup())){
			if( ic_esquema_recup.equals("2") || ic_esquema_recup.equals("4")){
				pdfDoc.setCell("Periodicidad del Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				//pdfDoc.setCell(consulta.getIg_ppc()+" "+consulta.descripcionUT(consulta.getCg_ut_ppc()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(consulta.periodoTasa(consulta.getCg_ut_ppc()),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			}
		}
		if( ic_esquema_recup.equals("2") && !"".equals(consulta.getCg_pfpc())){
			pdfDoc.setCell("Primer fecha de pago de Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(consulta.getCg_pfpc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Penúltima fecha de pago de Capital:",((tipoClase%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(consulta.getCg_pufpc(),((tipoClase++%2==0)?"celda01":"formas"),ComunesPDF.LEFT,1,1,0);
		}
	}
	pdfDoc.addTable();
	
	pdfDoc.setTable(4,90);
	pdfDoc.setCell("Detalle de Disposiciones","celda04",ComunesPDF.LEFT,4,1,0);
	pdfDoc.setCell("Número de Disposición","formas",ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell("Monto","formas",ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell("Fecha de Disposición","formas",ComunesPDF.LEFT,1,1,0);
	pdfDoc.setCell("Fecha de Vencimiento","formas",ComunesPDF.LEFT,1,1,0);
	for(int i=0;i<Integer.parseInt(consulta.getIn_num_disp());i++){
		clase = ((i%2)!=0)?"formas":"celda01";
		pdfDoc.setCell((i+1)+"",clase,ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(consulta.getFn_monto_disp(i),2),clase,ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(consulta.getDf_disposicion(i),clase,ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell(consulta.getDf_vencimiento(i),clase,ComunesPDF.LEFT,1,1,0);
	}
	pdfDoc.addTable();
	if((consulta.getPagos()).size() > 0){
		ArrayList alFilas = consulta.getPagos();
		pdfDoc.setTable(3,90);
		pdfDoc.setCell("Plan de Pagos Capital","celda04",ComunesPDF.LEFT,3,1,0);
		pdfDoc.setCell("Número de Pago","formas",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Fecha de Pago","formas",ComunesPDF.LEFT,1,1,0);
		pdfDoc.setCell("Monto a pagar","formas",ComunesPDF.LEFT,1,1,0);
		for(int i=0;i<alFilas.size();i++){
			ArrayList alColumnas = (ArrayList)alFilas.get(i);
			clase = ((i%2)!=0)?"formas":"celda01";
			pdfDoc.setCell(alColumnas.get(0).toString(),clase,ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(alColumnas.get(1).toString(),clase,ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(Comunes.formatoDecimal(alColumnas.get(2).toString(),2),clase,ComunesPDF.LEFT,1,1,0);
		}
		pdfDoc.addTable();
	}
    //Obteniendo el periodo de las tasas
	if("1".equals(consulta.getIc_esquema_recup())){
		periodo = "Al Vencimiento";
	}else if("4".equals(consulta.getIc_esquema_recup())&&"0".equals(consulta.getCg_ut_ppc())){
		periodo = "Al Vencimiento";
	}else if( !consulta.periodoTasa(consulta.getCg_ut_ppi()).equals("") ){
		periodo = consulta.periodoTasa(consulta.getCg_ut_ppi())+"mente";
	}
	else if( !consulta.periodoTasa(consulta.getCg_ut_ppc()).equals("") ){
		periodo = consulta.periodoTasa(consulta.getCg_ut_ppc())+"mente";
	}else{
		periodo = "Al Vencimiento";
	}

	if(!"TF".equals(origen) && !"TFE".equals(origen) ){
		if( estatus_solicitud.equals("2") || estatus_solicitud.equals("5") ){
			pdfDoc.setTable(1,90);
			pdfDoc.setCell("TASA INDICATIVA","celda04",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Tasa Indicativa con oferta mismo día:  "+consulta.getDescTasaIndicativa()+" "+consulta.getIg_tasa_md() + " % anual pagadera "+periodo+"\n"+
						   "Tasa Indicativa con oferta 48 horas:     "+consulta.getDescTasaIndicativa()+" "+consulta.getIg_tasa_spot()+ " % anual pagadera "+periodo
						   ,"celda01",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Nota:\n\n","formas",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Tasa con oferta Mismo Día: El Intermediario Financiero tiene la opción de tomarla en firme el mismo día que haya recibido la cotización indicativa a más tardar a las 12:30 horas.\n\n"
				,"formas",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("Tasa con oferta 48 Horas: El Intermediario Financiero tiene la opción de tomarla en firme a más tardar dos días hábiles después de que la Tesorería haya proporcionado la cotización indicativa antes de las 12:30 horas del segundo día hábil."
				,"formas",ComunesPDF.LEFT,1,1,0);				
			pdfDoc.setCell("\nLas cotizaciones indicativas proporcionadas por la Tesorería de Nacional Financiera son de uso reservado para el intermediario solicitante y será responsabilidad de dicho intermediario el mal uso de la información proporcionada."
				,"formas",ComunesPDF.LEFT,1,1,0);				
	//		pdfDoc.setCell("Tasa Mismo Día: El intermediario Financiero tiene la opción de tomarla en firme el mismo día que haya recibido la respuesta antes de las 12:30 pm.","formas",ComunesPDF.LEFT,1,1,1);
			//pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
	//		pdfDoc.setCell("Tasa 48 Horas: El intermediario Financiero tiene la opción de tomarla en firme a mas tardar dos días hábiles después contados a partir de la fecha que haya recibido respuesta.","formas",ComunesPDF.LEFT,1,1,1);
			pdfDoc.addTable();
		}
		
		if( estatus_solicitud.equals("7") || estatus_solicitud.equals("8") || estatus_solicitud.equals("9")){
			pdfDoc.setTable(2,90);
			pdfDoc.setCell("TASA CONFIRMADA","celda04",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("FECHA Y HORA DE "+("8".equals(estatus_solicitud)?"CANCELACION":"CONFIRMACION"),"celda04",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);
			if(consulta.getCs_aceptado_md().equals("S"))
				pdfDoc.setCell("Tasa Confirmada " +consulta.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(consulta.getIg_tasa_md(),2) + " % anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
			else
				pdfDoc.setCell("Tasa Confirmada " +consulta.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2) + "% anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
			pdfDoc.setCell(("8".equals(estatus_solicitud)?consulta.getDf_cotizacion()+" "+consulta.getHora_cotizacion():consulta.getDf_aceptacion()) ,"celda01",ComunesPDF.LEFT,1,1,1);
			pdfDoc.addTable();
		}
	}else{
			AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
			boolean mismoDiaOk = autSol.validaMismoDia(ic_solicitud);
			pdfDoc.setTable(1,90);
			pdfDoc.setCell("TASA COTIZADA","celda04",ComunesPDF.LEFT,1,1,0);
			
			if(mismoDiaOk)
				pdfDoc.setCell("Tasa con oferta mismo dia" +consulta.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(consulta.getIg_tasa_md(),2) + " % anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
			else
				pdfDoc.setCell("Tasa con oferta 48 horas" +consulta.getDescTasaIndicativa()+" "+Comunes.formatoDecimal(consulta.getIg_tasa_spot(),2) + "% anual pagadera " + periodo,"celda01",ComunesPDF.LEFT,1,1,1);
			pdfDoc.addTable();
	}
	
	//pdfDoc.newPage();
	if(!consulta.getCg_observaciones().trim().equals("")){
		pdfDoc.setTable(3,90);
		pdfDoc.setCell("Observaciones:","celda04",ComunesPDF.LEFT,3,1,0);
		pdfDoc.setCell(consulta.getCg_observaciones(),"formas",ComunesPDF.LEFT,3,1,0);
		pdfDoc.addTable();
	}

	pdfDoc.endDocument();
	validaPDF = true;
  }catch(Exception e){
	System.out.println("22ConsultaSolicitud1.jsp:Exepcion:"+e);
	e.printStackTrace();
%>

<%
}

if(validaPDF) {
jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
jsonObj.put("success",new Boolean(true));


					
}else{

jsonObj.put("success", new Boolean(false));

}
infoRegresar=jsonObj.toString();
%>

<%=infoRegresar%>
