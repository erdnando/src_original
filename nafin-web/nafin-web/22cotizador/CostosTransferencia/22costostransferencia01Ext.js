/*
PERFIL	:	ADMIN TESORERIA
Pantalla		:	Tasa de credito / Repotes Tesorer�a / Costos de Tranferancia

*/
Ext.onReady(function(){
//TEST Ext.Msg.alert('EXTJS','FUNCIONA');
//VARIABLES GLOBALES
var _URL='22costostransferencia01Ext.data.jsp';
var meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
//------------------------------------------------------------------------------
//---------------------------MAQUINA DE ESTADOS---------------------------------
//------------------------------------------------------------------------------
	var procesarEstados = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						pantalla(resp.estadoSiguiente,resp);
					}
				);
			} else {
				pantalla(resp.estadoSiguiente,resp);
			}
		}else{
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
		}
	}
	var pantalla = function(estado,respuesta){
		if(estado ==	"INICIALIZACION"	){
			catalogoMoneda.load();
		}else if(	estado ==	"CARGAR_CATALOGO_CURVA"	){
			catalogoCurva.load({
				params:{
					moneda : respuesta
				}
			});
		}else if(	estado == 	"CONSULTAR"	){
			
			consultaData.load({
					params: Ext.apply(respuesta,{
						operacion: 	'NuevaConsulta', //Generar datos para la consulta
						nombre:		Ext.getCmp('cboCurva').getRawValue(),
						start: 		0,
						limit: 		15
					})
				});
			
		}else if(estado == "ESPERAR" ){
		
		}
	}

//------------------------------------------------------------------------------	
//-----------------------------Handdlers----------------------------------------
//------------------------------------------------------------------------------	

	//----------------------Descargar Archivos--------------------------
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('botonImprimirPDF').setIconClass('icoPdf');
			Ext.getCmp('botonGenerarArchivoCSV').setIconClass('icoXls');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		fp.el.unmask();
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
					if (!grid.isVisible()) {
						grid.show();
					}			
			if(store.getTotalCount() > 0) {
				//Acciones si hay registros	
				var rec = store.getAt(0);
				var dia = rec.data.FECHA.substring(6);
				
				var mes = rec.data.FECHA.substring(4,6)-1;
				var anio = rec.data.FECHA.substring(0,4)
				grid.setTitle('Curva '+ Ext.getCmp('cboCurva').getRawValue()+' del '+dia+' de '+meses[mes] +' de '+anio);
				Ext.getCmp('titulo').setValue('Curva '+ Ext.getCmp('cboCurva').getRawValue()+' del '+dia+' de '+meses[mes] +' de '+anio);				
				grid.el.unmask();
				
			} else {
				//No existen datos
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
//------------------------------------------------------------------------------	
//-----------------------------Stores-------------------------------------------
//------------------------------------------------------------------------------
  var catalogoMoneda = new Ext.data.JsonStore ({
	   id			: 'catologoMonedaDataStore',
		root 		: 'registros',
		fields 	: ['clave', 'descripcion', 'loadMsg'],
		url 		: _URL,
		baseParams: 
			{
				informacion	:	'COSTOS_TRANSFER.cargaCatalogoMoneda'
			},
		totalProperty	: 'total',
		autoLoad	: false,
		listeners:
			{
				exception	:	NE.util.mostrarDataProxyError,
				beforeload	:	NE.util.initMensajeCargaCombo
			}
  });
  var catalogoCurva = new Ext.data.JsonStore ({
	   id			: 'catologoCurvaDataStore',
		root 		: 'registros',
		fields 	: ['clave', 'descripcion', 'loadMsg'],
		url 		: _URL,
		baseParams: 
			{
				informacion	:	'COSTOS_TRANSFER.cargaCatalogoCurva'
			},
		totalProperty	: 'total',
		autoLoad	: false,
		listeners:
			{
				exception	:	NE.util.mostrarDataProxyError,
				beforeload	:	NE.util.initMensajeCargaCombo
			}
  });
var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : _URL,
		baseParams: {
			informacion: 'COSTOS_TRANFER.Consultar'
		},
			fields: [
					{name: 'PLAZO'},//
					{name: 'TASA'},//
					{name: 'NOMBRE'},//
					{name: 'FECHA'}//
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});

//------------------------------------------------------------------------------
//----------------------------Elementos Pantalla--------------------------------
//------------------------------------------------------------------------------
	var elementosForma = [
		{
			xtype:"combo",
			id:"cboMoneda",
			name:"moneda",
			hiddenName:"moneda",
			allowBlank: false,
			fieldLabel: 'Moneda',
			displayField: "descripcion",
			valueField	: "clave",
			triggerAction: 'all',
			typeAhead	: true,
			minChars		: 1,
			mode			: 'local',
			forceSelection : true,
			emptyText	: 'Seleccionar Moneda',
			store			: catalogoMoneda,
			tpl			: NE.util.templateMensajeCargaCombo,
			listeners:{
				'select': function(v){
					var forma = fp.getForm().getValues();
						pantalla('CARGAR_CATALOGO_CURVA',v.getValue());
				}
			}
		},
		{
			xtype:"combo",
			id:"cboCurva",
			name:"curva",
			hiddenName:"curva",
			allowBlank: false,
			fieldLabel: 'Curva',
			displayField: "descripcion",
			valueField	: "clave",
			triggerAction: 'all',
			typeAhead	: true,
			minChars		: 1,
			mode			: 'local',
			forceSelection : true,
			emptyText	: 'Seleccionar curva...',
			store			: catalogoCurva,
			tpl			: NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha',
					id: 'txtFechaMin',
					//value : new Date(),
					allowBlank: true,
					startDay: 0,
					vtype: 'rangofecha',
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaMax',
					margins: '0 20 0 0',
					regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
					regex     : /.*[1-9][0-9]{3}$/
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 5,margins:{top:0, right:10, bottom:0, left:-15}
					
				},
				{
					xtype: 'datefield',
					name: 'fechaA',
					id: 'txtFechaMax',
					//value : new Date(),
					allowBlank: true,
					startDay: 1,
					vtype: 'rangofecha',
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaMin',
					margins: '0 20 0 0',
					regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
					regex     : /.*[1-9][0-9]{3}$/
				}
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'titulo', 	value: '' }		
	]
	var fp =new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: '<center>Consulta de Costos de transferencia</center>',
		layout		: 'form',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',  
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind	:true,
				handler: function (boton, evento){
					var fechaCargoDE = Ext.getCmp('txtFechaMin');
					var fechaCargoA = Ext.getCmp('txtFechaMax');
					if(!Ext.isEmpty(fechaCargoDE.getValue()) || !Ext.isEmpty(fechaCargoA.getValue())){
						if(Ext.isEmpty(fechaCargoDE.getValue())){
							fechaCargoDE.markInvalid('Debe capturar un rango de fechas');
							fechaCargoDE.focus();
							return;
						}   
						if(Ext.isEmpty(fechaCargoA.getValue())){
							fechaCargoA.markInvalid('Debe capturar un rango de fechas');
							fechaCargoA.focus();
							return;
						}
					}
					var forma = fp.getForm().getValues();
					fp.el.mask('Procesando');
						pantalla('CONSULTAR',forma);
						/*consultaData.load({
								params: Ext.apply(forma,{
									operacion: 	'NuevaConsulta', //Generar datos para la consulta
									start: 		0,
									limit: 		15
								})
							});
						*/
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function (boton, evento){
					var forma = fp.getForm().reset();
					Ext.getCmp('grid').hide();
				}//fin Handler
			}
		]
	});

var grid = new Ext.grid.EditorGridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consultaData,
				style: 'margin:0 auto;',
				clicksToEdit : 1,
				columns:[
						{
						
						header:'Plazo',
						tooltip: 'Plazo',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 200,
						align: 'center'
						},
						{
						
						header: 'Tasa',
						tooltip: 'Tasa',
						sortable: true,
						dataIndex: 'TASA',
						width: 200,
						align: 'center',
						renderer:function(v){
							return Ext.util.Format.number((v*100),'0.000000%');
						}
						}
				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 400,
				style: 'margin:0 auto;',
				frame: false,				
				bbar: {				
			xtype: 			'paging',
			pageSize: 		15,
			buttonAlign: 	'left',
			id: 				'barraPaginacion',			
			displayInfo: 	true,
			store: 			consultaData,
			displayMsg: 	'{0} - {1} de {2}',
			emptyMsg: 		"No hay registros.",
			items: [  
						'->',
					{
						xtype: 	'button',
						text: 	'',
						tooltip:	'Descargar Archivo ',
						id: 		'botonGenerarArchivoCSV',
						iconCls: 'icoXls',
						handler: function(boton, evento) {							
							Ext.Ajax.request({
								url: 			_URL,
								params: 		Ext.apply( fp.getForm().getValues(), { 
									informacion: 	'COSTO_TRANFRE.Generar.Archivo',
									nombre:		Ext.getCmp('cboCurva').getRawValue(),
									tipo	:'CSV'									
								}),
								callback: 	descargaArchivo
							});
							 
						}
					},
					{
						xtype:	'button',
						text: 	'',
						tooltip:	'Descargar PDF ',
						id: 		'botonImprimirPDF',
						iconCls: 'icoPdf',
						handler: function(boton, evento) {						
							Ext.Ajax.request({
								url: 			_URL,
								params: 		Ext.apply( fp.getForm().getValues(), { 
									informacion: 	'COSTO_TRANFRE.Generar.Archivo',
									nombre:		Ext.getCmp('cboCurva').getRawValue(),
									tipo	:'PDF',
									titulo:Ext.getCmp('titulo').getValue()
								
								}),callback: 	descargaArchivo
							});
						}
					}
				]
			}
		});
	

//------------------------------------------------------------------------------
//----------------------------contenedorPrincipal-------------------------------
//------------------------------------------------------------------------------
	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		renderHidden:	true,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp
			,NE.util.getEspaciador(10),grid
		],
		ocultaMascaras: function(){
			var element = null;
         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			// Derefenrencia ultimo elemento...
			element = null;
		}

	});
///INICIALIZAMOS PANTALLA
	pantalla("INICIALIZACION",null);

});