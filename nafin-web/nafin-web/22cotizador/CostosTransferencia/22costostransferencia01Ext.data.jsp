<%@ page language="java"
contentType="application/json;charset=UTF-8"
import="java.util.*,
	com.netro.cotizador.*,
	com.netro.exception.*,
	com.netro.pdf.*,
	org.apache.commons.logging.Log,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,  
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%> 

<%@ include file="/appComun.jspf" %>

<%@ include file="/22cotizador/22secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
//Parametros 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 

String moneda = (request.getParameter("moneda")!=null)?request.getParameter("moneda"):""; 
String curva = (request.getParameter("curva")!=null)?request.getParameter("curva"):""; 
String nombre = (request.getParameter("nombre")!=null)?request.getParameter("nombre"):""; 
String fechaCurva = (request.getParameter("fecha")!=null)?request.getParameter("fecha"):""; 
String fechaCurvaA = (request.getParameter("fechaA")!=null)?request.getParameter("fechaA"):""; 
String titulo = (request.getParameter("titulo")!=null)?request.getParameter("titulo"):""; 


//variables utiles
String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
JSONObject jsonObj = new JSONObject();
String estadoSiguiente = null;
// Leer posicion de los registros
		int start = 0;
		int limit = 0;

HashMap registro = new HashMap();
JSONArray registros = new JSONArray();

if(informacion.equals("COSTOS_TRANSFER.cargaCatalogoMoneda")){

	estadoSiguiente =	"ESPERAR";
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre"); 
	cat.setValoresCondicionIn("1,54", Integer.class);
	cat.setOrden("CLAVE");	
	JSONArray  catalogo = cat.getJSONArray();
	consulta	= "{\"success\":true,\"total\":"+catalogo.size()+",\"registros\":"+catalogo.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	jsonObj.put("estadoSiguiente",estadoSiguiente);
		
	infoRegresar =jsonObj.toString(); //cat.getJSONElementos();	
		
}else if(informacion.equals("COSTOS_TRANSFER.cargaCatalogoCurva")){
	estadoSiguiente =	"ESPERAR";
	CatalogoCurva cat = new CatalogoCurva();
	cat.setClave("idcurva");
	cat.setDescripcion("nombre");
	cat.setMoneda(moneda);
	JSONArray  catalogo = cat.getJSONArray();
	consulta	= "{\"success\":true,\"total\":"+catalogo.size()+",\"registros\":"+catalogo.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	jsonObj.put("estadoSiguiente",estadoSiguiente);
		
	infoRegresar =jsonObj.toString(); //cat.getJSONElementos();	
		
		
}else if(informacion.equals("COSTOS_TRANFER.Consultar")|| informacion.equals("COSTO_TRANFRE.Generar.Archivo")){
	String mascara ="yyyyMMdd";
	SimpleDateFormat formato = new SimpleDateFormat(mascara);
	
	ConsultaCostosTransferencia clase = new ConsultaCostosTransferencia();
	clase.setIdCurva(curva);
	clase.setNombre(nombre);
	clase.setMoneda(moneda);
	clase.setTitulo(titulo); 
	
	try{
		if(fechaCurva==null||fechaCurva.equals("")||fechaCurvaA==null||fechaCurvaA.equals("")){
			
			Registros reg = clase.fechaCurva();
			while(reg.next()){
				fechaCurva = reg.getString("FECHA");
			}
			clase.setFechaCurva(fechaCurva);	
		}else{
			java.util.Date now = new java.util.Date(fechaCurva);
			String fechaAux [] = fechaCurva.split("/");
			clase.setFechaCurva(fechaAux[2]+fechaAux[1]+fechaAux[0]);
		}
	}catch(Exception e){
		e.printStackTrace();
		throw new AppException("Error en los parametros recibidos", e);
	}		
	CQueryHelperRegExtJS queryHelper =  new  CQueryHelperRegExtJS(clase);
		
	if(informacion.equals("COSTOS_TRANFER.Consultar")  ||  informacion.equals("Bitacoras.ArchivoPDF") )  {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
										
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	}
	if(informacion.equals("COSTOS_TRANFER.Consultar") ){
		try {
			if (operacion.equals("NuevaConsulta")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
				
		jsonObj = JSONObject.fromObject(consulta);
			
	}else  if(informacion.equals("COSTO_TRANFRE.Generar.Archivo") )  {
		
		String tipo = (request.getParameter("tipo")==null)?"":request.getParameter("tipo");
		
		try {
		
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipo);
			jsonObj.put("success", new Boolean(true));				
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				
		} catch(Throwable e) {   
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
	
	infoRegresar = jsonObj.toString();
	
}
	
%>

<%=infoRegresar%>
