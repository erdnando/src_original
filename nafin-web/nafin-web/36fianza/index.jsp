<%@ page import="java.sql.*, java.text.*, com.netro.fianza.*, netropology.utilerias.*"%>
<%
//El siguiente c�digo debe ser lo primero en ejecutarse, el cual determina si la sesi�n est� activa.
//De lo contrario en lugar de mostrar "Su sesi�n ha expirado..." lanzar� un NullPointer Exception
String gsCveUsuario = (String) session.getAttribute("sesCveUsuario");
if(gsCveUsuario == null){
%>
		<script language="JavaScript">
			alert("Su sesión ha expirado, por favor vuelva a entrar.");
			top.location="/nafin/15cadenas/15salir.jsp"
		</script>
<%
	return;
}


try {
	String strTipoUsuario = (String)session.getAttribute("strTipoUsuario"); // POSIBLES VALORES NAFIN-EPO-PYME-IF
	String strLogin = (String)session.getAttribute("Clave_usuario");//LOGIN DEL USUARIO QUE SE LOGUEA
	String strSerial = (String)session.getAttribute("strSerial"); //VARIABLE QUE CONTIENE EL NUMERO DE SERIE DEL CERTIFICADO
	
	String gsParSistema = request.getParameter("parSistema");

	System.out.println("strTipoUsuario----->"+strTipoUsuario);
	System.out.println("gsParSistema----->"+gsParSistema);
	System.out.println("strSerial----->"+strSerial);
	//String  gsIdioma = "";
	//gsIdioma= (String)session.getAttribute("sesIdiomaUsuario");
	FianzaElectronica fianza = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	
	String perfil = (String)request.getSession().getAttribute("sesPerfil");
	String cveFiado = (String)session.getAttribute("iNoCliente");
	String terminos ="N"; 
	if(perfil.equals("ADMIN PYME")  || perfil.equals("CONS FIADO") ) {
		terminos =fianza.consultaTerminos(cveFiado);
	}
		
	if ("EPO".equals(strTipoUsuario)) {
		if(strSerial.equals("")) {
			%>
				<script language="JavaScript">
					window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
				</script>
			<%
			//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
		} else {
			response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
		}
	} else if ("PYME".equals(strTipoUsuario)) {

		if(strSerial.equals("")) {
			%>
				<script language="JavaScript">
					window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
				</script>
			<%
			//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
		} else {
		
		if(perfil.equals("ADMIN PYME")  ) {	
					if(terminos.equals("N") ) {
						response.sendRedirect("36TerCondiciones.jsp");
					}else {
						response.sendRedirect("../14seguridad/Ventana.jsp?parSistema=" + gsParSistema);
					}
			}else{
					response.sendRedirect("../14seguridad/Ventana.jsp?parSistema=" + gsParSistema);
			}
				
			//response.sendRedirect("../14seguridad/Ventana.jsp?parSistema=" + gsParSistema);
		}
	} else if ("IF".equals(strTipoUsuario)) {
		if(strSerial.equals("")) {
			%>
				<script language="JavaScript">
					window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
				</script>
			<%
			//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
		} else {
			response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
		}
	} else if("NAFIN".equals(strTipoUsuario)) {
		if(strSerial.equals("")) {
			%>
				<script language="JavaScript">
					window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
				</script>
			<%
			//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
		} else {			
			response.sendRedirect("../14seguridad/Ventana.jsp?parSistema=" + gsParSistema);
		}
	}else if("AFIANZADORA".equals(strTipoUsuario) ) {
	if(strSerial.equals("")) {
			%>
				<script language="JavaScript">
					window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
				</script>
			<%
			//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
		} else {			
			response.sendRedirect("../14seguridad/Ventana.jsp?parSistema=" + gsParSistema);
		}
	}else{
		
		if(perfil.equals("CONS FIADO")  ) {	
					if(terminos.equals("N") ) {
						response.sendRedirect("36TerCondiciones.jsp");
					}else {
						response.sendRedirect("../14seguridad/Ventana.jsp?parSistema=" + gsParSistema);
					}
		}else{
			response.sendRedirect("../14seguridad/Ventana.jsp?parSistema=" + gsParSistema);
		}
		
	}
} finally {

}
%>