<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,
		java.sql.*,	
		net.sf.json.JSONObject,
		java.util.Vector,
		netropology.utilerias.*,
		com.netro.fianza.*, 
		com.netro.exception.*"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>

<%
	String infoRegresar ="";
	FianzaElectronica fianza = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	String terminos ="N";
	String version   = (request.getParameter("version")!=null)?request.getParameter("version"):"";
	
	terminos = fianza.capturaTerminos("S", version, iNoCliente);
	
	List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
	pantallasNavegacionComplementaria.remove("/36fianza/36TerCondiciones.jsp");  //El nombre debe coincidir con el especificado en Navegacion

	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("Terminos",terminos);	 	
	jsonObj.put("urlPagina","/nafin"+pantallasNavegacionComplementaria.get(0));	 
	infoRegresar = jsonObj.toString();	
%>

<%=infoRegresar%>