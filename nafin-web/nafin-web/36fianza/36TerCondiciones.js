		
	Ext.onReady(function() {
	
	var version =Ext.getDom('version').value;	
	
	var texto = [
	'<table width="700" >',
	'<tr><td>&nbsp;</td></tr>',
	'<tr><td align="center" ><img src="../00archivos/15cadenas/15archcadenas/logos/nafin.gif" border="0" width="50%"></td></tr>',
	'<tr><td>&nbsp;</td></tr>',
	'<tr><td>&nbsp;</td></tr>',
	'<tr><td align="center" ><h2>Aceptación de Términos y Condiciones<h2></td><td align="left"></td></tr>',
	'<tr><td class="celdaV"  align="center">'+version+'</td></tr>',
	'<tr><td>&nbsp;&nbsp;</td></tr>',
	'<tr><td>&nbsp;&nbsp;</td></tr>',
	'<tr><td class="celdaV" align="justify" >Por medio del presente me doy por enterado de los Términos y Condiciones <a href="javascript:mostrar()"><img src="../00utils/gif/pdfdoc.png" border="0" width="15"></a> para el uso  del módulo de Fianza Electrónica. </td></tr>',
	'<tr><td>&nbsp;</td></tr>',
	'<tr><td class="celdaV" align="justify" >Al firmar electrónicamente dicha información estoy aceptando mi conformidad con los mismos.</td></tr>',
 '</table>',
	'<p>',
	'<p>',
	'<p>',
	'<p>'
	];
	
	var procesarSuccessFailureTerminos =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var datos = Ext.util.JSON.decode(response.responseText);
			var terminos =datos.Terminos;	
			var fp = Ext.getCmp('forma');		
			var forma = fp.getForm();
			if(	terminos =='S'){		
				window.location = datos.urlPagina;					
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		
	//panel para mostrar  el texto 
		var fp = new Ext.form.FormPanel({		
			id: 'forma',
			width: 700,	
			align: 'center',
			frame: true,		
			titleCollapse: false,
			bodyStyle: 'padding: 6px',			
			style: 'margin:0 auto;',
			defaultType: 'textfield',		
			html: texto.join(''),
				buttons: [
			{
				text: 'Aceptar',				
				formBind: true,
				handler: function(boton, evento) {						
											
					Ext.Ajax.request({
						url: '36TerCondiciones.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Terminos',
							version:version
							}),
							callback: procesarSuccessFailureTerminos
						});
				}
			},
			{
				text: 'Cancelar ',				
				handler: function() {					
					close();
				}
			}
		]
		
	});
	
	//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 900,
		height: 'auto',
		items: [
			fp			
		]
		});
			
	
	});