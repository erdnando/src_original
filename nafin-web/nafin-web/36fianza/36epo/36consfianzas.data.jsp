<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalgoAfianzadora,
		com.netro.model.catalogos.CatalogoDestinatario,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fianza.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
int start = 0;
int limit = 0;	

try{
if (informacion.equals("valoresIniciales")) {
	JSONObject jsonObj = new JSONObject();
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	String firmaMancomunada = fianzaElectronica.validaFirmaMancomunada(iNoCliente)?"S":"N";
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("firmaManc", firmaMancomunada);
	jsonObj.put("usuario", strNombreUsuario);
	jsonObj.put("strTipoAfiliado", iTipoAfiliado);
	jsonObj.put("strPerfil", strPerfil);
	

	infoRegresar= jsonObj.toString();
}else if (informacion.equals("catalogoAfianza")) {
	CatalgoAfianzadora cat = new CatalgoAfianzadora();
	cat.setCampoClave("ic_afianzadora");
	cat.setCampoDescripcion("CG_RAZON_SOCIAL");
	cat.setOrden("CG_RAZON_SOCIAL");
	
	if("E".equals(iTipoAfiliado))
		cat.setClaveEpo(iNoCliente);
	if("F".equals(iTipoAfiliado))
		cat.setClaveFiado(iNoCliente);
	if("P".equals(iTipoAfiliado))
		cat.setClaveFiado(sesClaveFiado);
	
	infoRegresar = cat.getJSONElementos();
}else if (informacion.equals("catalogoDest")) {
	CatalogoDestinatario cat = new CatalogoDestinatario();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setProducto("10");
	
	if("A".equals(iTipoAfiliado))
		cat.setClaveAfianza(iNoCliente);
	if("F".equals(iTipoAfiliado))
		cat.setClaveFiado(iNoCliente);
	if("P".equals(iTipoAfiliado))
		cat.setClaveFiado(sesClaveFiado);
	
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
}else if (informacion.equals("catalogoFiado")) {
	CatalogoFiado cat = new CatalogoFiado();
	cat.setCampoClave("ic_fiado");
	cat.setCampoDescripcion("cg_nombre");
	cat.setOrden("cg_nombre");
	
	if("E".equals(iTipoAfiliado))
		cat.setClaveEpo(iNoCliente);
	if("A".equals(iTipoAfiliado))
		cat.setClaveAfianza(iNoCliente);
		
	infoRegresar = cat.getJSONElementos();
}else if (informacion.equals("catalogoEstatus")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("fecat_estatus");
	cat.setCampoClave("cc_estatus");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setOrden("cg_descripcion");
	infoRegresar = cat.getJSONElementos();
}else if (
		informacion.equals("Consultar") || 
		informacion.equals("ArchivoCSV") ||
		informacion.equals("ArchivoPDF") ) {

	com.netro.fianza.ConsFianzaElec paginador = new com.netro.fianza.ConsFianzaElec();
	System.out.println("iNoCliente======="+iNoCliente);
	System.out.println("strTipoUsuario======="+iTipoAfiliado);
	
	if("E".equals(iTipoAfiliado))
		paginador.setIcEpo(iNoCliente);
	else
		paginador.setIcEpo(request.getParameter("ic_destinatario")==null?"":request.getParameter("ic_destinatario"));
	if("A".equals(iTipoAfiliado))
		paginador.setIcAfianzadora(iNoCliente);
	else
		paginador.setIcAfianzadora(request.getParameter("ic_afianzadora")==null?"":request.getParameter("ic_afianzadora"));
	
	if("F".equals(iTipoAfiliado))
		paginador.setIcFiado(iNoCliente);
	else if("P".equals(iTipoAfiliado))
		paginador.setIcFiado(sesClaveFiado);
	else
		paginador.setIcFiado(request.getParameter("ic_fiado")==null?"":request.getParameter("ic_fiado"));
		
	if("AUTORIZADOR EPO".equals(strPerfil) || "ADMIN AUTO EPO".equals(strPerfil)){
		paginador.setAutorizador(iNoUsuario);
		paginador.setPerfil(strPerfil);
	}
	
	if("REVISOR EPO".equals(strPerfil)){
		paginador.setUsuario(iNoUsuario);
	}
		
	
	
	
	paginador.setIcEstatus(request.getParameter("cc_estatus")==null?"":request.getParameter("cc_estatus"));
	paginador.setNumFianza(request.getParameter("cg_numero_fianza")==null?"":request.getParameter("cg_numero_fianza"));
	paginador.setNumContrato(request.getParameter("cg_numero_contrato")==null?"":request.getParameter("cg_numero_contrato"));
	paginador.setDfSolicIni(request.getParameter("df_solicitud_ini")==null?"":request.getParameter("df_solicitud_ini"));
	paginador.setDfSolicFin(request.getParameter("df_solicitud_fin")==null?"":request.getParameter("df_solicitud_fin"));
	paginador.setDfVencIni(request.getParameter("df_vencimiento_ini")==null?"":request.getParameter("df_vencimiento_ini"));
	paginador.setDfVencFin(request.getParameter("df_vencimiento_fin")==null?"":request.getParameter("df_vencimiento_fin"));
	paginador.setDfAutorizaIni(request.getParameter("df_autoriza_ini")==null?"":request.getParameter("df_autoriza_ini"));
	paginador.setDfAutorizaFin(request.getParameter("df_autoriza_fin")==null?"":request.getParameter("df_autoriza_fin"));
	paginador.setDfRechazaIni(request.getParameter("df_rechazo_ini")==null?"":request.getParameter("df_rechazo_ini"));
	paginador.setDfRechazaFin(request.getParameter("df_rechazo_fin")==null?"":request.getParameter("df_rechazo_fin"));
	
	String vPantalla = request.getParameter("vPantalla")==null?"":request.getParameter("vPantalla");
	
	if("Monitor".equals(vPantalla)){
		System.out.println("valor del ramo =============== "+(request.getParameter("vRamo")==null?"":request.getParameter("vRamo")));
		System.out.println("valor del subramo =============== "+(request.getParameter("vSubRamo")==null?"":request.getParameter("vSubRamo")));
		paginador.setRamo(request.getParameter("vRamo")==null?"":request.getParameter("vRamo"));
		paginador.setSubramo(request.getParameter("vSubRamo")==null?"":request.getParameter("vSubRamo"));
		paginador.setEstrato(request.getParameter("vEstrato")==null?"":request.getParameter("vEstrato"));
		paginador.setIcEstatus(request.getParameter("vEstatus")==null?"":request.getParameter("vEstatus"));
	}

	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	if (informacion.equals("Consultar")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);															   		
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	
	}else if (informacion.equals("ArchivoCSV")) {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
	} else if (informacion.equals("ArchivoPDF")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}
} else if(informacion.equals("busquedaAvanzada")){
	
	String rfcFiado = request.getParameter("rfcFiado")==null?"":request.getParameter("rfcFiado");
	String nombreFiado = request.getParameter("nombreFiado")==null?"":request.getParameter("nombreFiado");
	
	CatalogoFiado cat = new CatalogoFiado();
	cat.setCampoClave("ic_fiado");
	cat.setCampoDescripcion("cg_nombre");
	if("E".equals(iTipoAfiliado))
		cat.setClaveEpo(iNoCliente);
	if("A".equals(iTipoAfiliado))
		cat.setClaveAfianza(iNoCliente);
		
	if(!nombreFiado.equals(""))
		cat.setNombreFiado(nombreFiado);
	if(!rfcFiado.equals(""))
		cat.setRfcFiado(rfcFiado);
	cat.setOrden("cg_nombre");
	infoRegresar = cat.getJSONElementos();

} else if (informacion.equals("fianzaPDF")) {
	JSONObject jsonObj = new JSONObject();
	try{
		String cveFianza = request.getParameter("cveFianza");
		FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
		String nombreArchivo = fianzaElectronica.getPDFfianzas(cveFianza,strDirectorioTemp);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}catch(Exception e){
		e.printStackTrace();
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}
	infoRegresar = jsonObj.toString();

}

// FODEA 028-2012: Fianzas Electrónicas (garellano)
else if (informacion.equals("PdfErrores")) {
	JSONObject jsonObj = new JSONObject();
	try{
		String cveFianza = request.getParameter("cveFianza")==null?"":request.getParameter("cveFianza");
		FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
		String nombreArchivo = fianzaElectronica.getPDFfianzasErrores(cveFianza,strDirectorioTemp);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}catch(Exception e){
		e.printStackTrace();
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	}
	infoRegresar = jsonObj.toString();

}

}catch(Exception e){
	e.printStackTrace();

}
System.out.println("infoRegresar = " + infoRegresar);

%>
<%=infoRegresar%>