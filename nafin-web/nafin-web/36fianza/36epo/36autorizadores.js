Ext.onReady(function() {

//**************************HANDLERS***********************************
var procesarSuccessValoresIni = function(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
		nafElecUsr = resp.nafelec;
		strUsuario = resp.strUsuario;
		titulo = 'USUARIOS REGISTRADOS PARA FIANZA ELECTR�NICA<br>'+strUsuario+' N@E: '+nafElecUsr;
		fp.setTitle(titulo);
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

var procesarConsultaData = function(store, arrRegistros, opts) {

		fp.el.unmask();
		//if (arrRegistros != null) {
			if (!grid.isVisible()) {	
				var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
				contenedorPrincipalCmp.add(grid);
				contenedorPrincipalCmp.doLayout();
			}
			
			var btnDescargarArchivo = Ext.getCmp('btnDescargarArchivo');
			var btnImprimir = Ext.getCmp('btnImprimir');
			
			grid.show();
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				btnDescargarArchivo.setHandler(onDescarga);
				btnDescargarArchivo.setText('Descargar Archivo');
				btnImprimir.setHandler(onImprimir);
				btnImprimir.setText('Descargar PDF');
				btnDescargarArchivo.enable();
				btnImprimir.enable();
				el.unmask();
				
			} else {
				btnDescargarArchivo.disable();
				btnImprimir.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		//}
		
	}

var procesarSuccessFailureGenerarArchivo = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			tipoArchivo = Ext.util.JSON.decode(response.responseText).tipoArchivo;
			if(tipoArchivo=="csv"){
				var btnDescargarArchivo = Ext.getCmp('btnDescargarArchivo');
				btnDescargarArchivo.setIconClass('');
				btnDescargarArchivo.enable();
				btnDescargarArchivo.el.highlight('FFF700', {duration: 8, easing:'bounceOut'});
				btnDescargarArchivo.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
				btnDescargarArchivo.setText("Abrir Archivo");
			}
			if(tipoArchivo=="pdf"){
				var btnImprimir = Ext.getCmp('btnImprimir');
				btnImprimir.setIconClass('');
				btnImprimir.enable();
				btnImprimir.el.highlight('FFF700', {duration: 8, easing:'bounceOut'});
				btnImprimir.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
				btnImprimir.setText("Abrir PDF");
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

var onBuscar = function(){
	fp.el.mask('Buscando...', 'x-mask-loading');
	
	//AL INCIAR GARGA EL GRID REALIZANDO LA CONSULTA MEDIANTE AJAX.
	consultaData.load({
		params: Ext.apply(fp.getForm().getValues())
	});	
}

var onDescarga = function(btn) {
	var gridUsers = Ext.getCmp('gridUsers');
	var store = gridUsers.getStore();
	var registrosEnviar = [];
	btn.setIconClass('loading-indicator');
	btn.disable();

	store.each(function(record){
		registrosEnviar.push(record.data);
	});
	
	registrosEnviar = Ext.encode(registrosEnviar);
	
	Ext.Ajax.request({
		url: '36autorizadoresfiles.jsp',
		params: {
				registros : registrosEnviar,
				tipoArchivo : 'csv'
				},
		callback: procesarSuccessFailureGenerarArchivo
	});
}

var onImprimir = function(btn) {

	var gridUsers = Ext.getCmp('gridUsers');
	var store = gridUsers.getStore();
	var registrosEnviar = [];
	
	btn.setIconClass('loading-indicator');
	btn.disable();
	
	store.each(function(record){
		registrosEnviar.push(record.data);
	});
	
	registrosEnviar = Ext.encode(registrosEnviar);
	
	Ext.Ajax.request({
		url: '36autorizadoresfiles.jsp',
		params: {
				registros : registrosEnviar,
				tipoArchivo : 'pdf'
				},
		callback: procesarSuccessFailureGenerarArchivo
	});
}

//***********************STORES*****************************************
var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '36autorizadores.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'PERFIL'},
			{name: 'NOMBREUSUARIO'},
			{name: 'CORREOELEC'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
		
	});
	
	var storePerfil = new Ext.data.ArrayStore({
	  fields: ['ID','ACCION'],
	  data: [['', 'Todos...'],
				['ADMIN AUTO EPO', 'ADMIN AUTO EPO'],
				['ADMIN EPO','ADMIN EPO'],
				['AUTORIZADOR EPO', 'AUTORIZADOR EPO'],
				['REVISOR EPO', 'REVISOR EPO']
			  ]
	});

//**********************************************************************
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'USUARIOS REGISTRADOS PARA FIANZA ELECTR�NICA',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'combo',
				name: 'comboPerfiles',
				id: 'comboPerfiles1',
				fieldLabel: 'Perfil',
				mode: 'local', 
				valueField : 'ID',
				displayField : 'ACCION',
				editable:true,
				hiddenName : 'comboPerfiles',
				value:'',
				width: 400,
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : storePerfil
			}
		],
		buttons: [
			{
			text: 'Buscar',
			iconCls: 'icoBuscar',
			handler: onBuscar
			},
			{
			text: 'Limpiar',
			iconCls: 'icoLimpiar',
			handler: function() {
					fp.getForm().reset();
					grid.hide();
				}
			}
		]
	});
	
	var grid = new Ext.grid.GridPanel({
		id: 'gridUsers',
		store: consultaData,
		style: 'margin:0 auto;',
		//margins: '20 0 0 0',
		columns: [
			{
				header: 'Perfil',
				tooltip: 'Perfil de usuario',
				dataIndex: 'PERFIL',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{
				header: 'Nombre',
				tooltip: 'Nombre',
				dataIndex: 'NOMBREUSUARIO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header: 'Correo Electr�nico',
				tooltip: 'Correo',
				dataIndex: 'CORREOELEC',
				sortable: true,
				hideable: false,
				width: 200,
				align: 'left'
			}
		],
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Descargar Archivo',
					id: 'btnDescargarArchivo',
					disables: true,
					handler: onDescarga
				},
				'-',
				{
					text: 'Descargar PDF',
					id: 'btnImprimir',
					disables: true,
					handler: onImprimir
				}
			]
		},
		stripeRows: true,
		loadMask: true,
		height: 300,
		width: 600,
		title: '',
		frame: true
	});


//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		/*layoutConfig: {
			align:'center'
		},*/
		items: [
			fp
			//grid
		]
	});


//-------------------------------- ----------------- -----------------------------------

	Ext.Ajax.request({
		url: '36autorizadores.data.jsp',
		params: {
				informacion:'valoresIniciales'
				},
		callback: procesarSuccessValoresIni
	});


});