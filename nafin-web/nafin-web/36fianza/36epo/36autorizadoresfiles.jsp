<%@ page import="java.sql.*, java.text.*, java.util.*, java.math.*,
				netropology.utilerias.*, com.netro.exception.*,	
				javax.naming.*,com.netro.descuento.*,com.netro.pdf.*,
				net.sf.json.*"
			contentType="application/json;charset=UTF-8"	
			errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
try {
	//	DECLARACION DE VARIABLES
	//	Par�metros provenienen de la p�gina anterior
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String  tipoArchivo = (request.getParameter("tipoArchivo") == null)?"":request.getParameter("tipoArchivo");
	System.out.println("jsonRegistros=="+jsonRegistros);
	List arrRegistrosModificados = JSONArray.fromObject(jsonRegistros);
	
	Iterator itReg = arrRegistrosModificados.iterator();
	
	//	De despliegue
	String nombreUsuario	= "";
	String correoElec		= "";
	String perfil			= "";

	
	//INICIA GENERACION DEL TIPO DE ARCHIVO
	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = "";
	if("pdf".equals(tipoArchivo)){
		nombreArchivo = archivo.nombreArchivo()+"."+tipoArchivo;	
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	

		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		//String	fechaAct = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		//String	horaAct = (String)session.getAttribute("horaAct");
		
		 pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
												session.getAttribute("iNoNafinElectronico").toString(),
												(String)session.getAttribute("sesExterno"),
												(String) session.getAttribute("strNombre"),
												(String) session.getAttribute("strNombreUsuario"),
												(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

		pdfDoc.addText("\n\nUSUARIOS REGISTRADOS PARA FIANZA ELECTRONICA","formasB",ComunesPDF.CENTER);
		pdfDoc.addText(strNombre,"formasB",ComunesPDF.CENTER);
		pdfDoc.setTable(3,80);
		pdfDoc.setCell("Perfil","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Nombre","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Correo El�ctronico","celda01",ComunesPDF.CENTER,1,2);

		int index = 0;
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();			
			nombreUsuario = registro.getString("NOMBREUSUARIO");
			correoElec = registro.getString("CORREOELEC");
			perfil = registro.getString("PERFIL");
			
			pdfDoc.setCell(perfil,"formas",ComunesPDF.LEFT,1,2);
			pdfDoc.setCell(nombreUsuario,"formas",ComunesPDF.LEFT,1,2);
			pdfDoc.setCell(correoElec,"formas",ComunesPDF.LEFT,1,2);

			index++;
		} //fin del for
		
		
		pdfDoc.addTable();
		pdfDoc.endDocument();
	}else if("csv".equals(tipoArchivo)){
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		contenidoArchivo.append("PERFIL, NOMBRE , CORREO ELECTRONICO\n");
		int index = 0;
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();			
			nombreUsuario = registro.getString("NOMBREUSUARIO");
			correoElec = registro.getString("CORREOELEC");
			perfil = registro.getString("PERFIL");
			contenidoArchivo.append(perfil+","+nombreUsuario+","+correoElec+"\n");
			index++;
		} //fin del for
		
		if(archivo.make(contenidoArchivo.toString(), strDirectorioTemp, "."+tipoArchivo))
			nombreArchivo = archivo.nombre;
		
	}
		System.out.println("strDirecVirtualTemp+nombreArchivo == "+strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("tipoArchivo", tipoArchivo);

} catch (Exception e) {
	e.printStackTrace();
	jsonObj.put("success", new Boolean(false));
	jsonObj.put("msg", "Error al generar el archivo");
}
%>

<%=jsonObj%>