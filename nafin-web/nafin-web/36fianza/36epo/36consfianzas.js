Ext.onReady(function() {
var strPerfil;
var strTipoAfil;
//HANDLERS----------------------------------------------------------------------
	var procesarSuccessValoresIni = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			ifirmaMan = resp.firmaManc;
			iusuario = resp.usuario;
			strPerfil = resp.strPerfil;
			strTipoAfil = resp.strTipoAfiliado;
			
			if(strTipoAfil=='A'){
				comboAfianza = fp.findById('ic_afianzadora1');
				comboAfianza.hide();
			}
			if(strTipoAfil=='E'){
				comboDest = fp.findById('ic_destinatario1');
				comboDest.hide();
			}
			if(strTipoAfil=='P' || strTipoAfil=='F'){
				comboFiado = fp.findById('ic_fiado1');
				comboFiado.hide();
				btnBusqAvan = Ext.getCmp('regBusqueda');
			//	btnBusqAv = fp.findById('btnBusqAv');
				btnBusqAvan.hide();
			}
			
			if(objConsIni.vPantalla=='Monitor'){
				fp.el.mask('Enviando...', 'x-mask-loading');
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar', //Generar datos para la consulta
						vRamo: objConsIni.vRamo,
						vSubRamo: objConsIni.vSubRamo,
						vEstrato: objConsIni.vEstrato,
						vEstatus: objConsIni.vEstatus,
						vPantalla: objConsIni.vPantalla,
						start: 0,
						limit: 15
					})
				});
				
				Ext.getCmp('btnGenerarArchivo').disable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnImprimirPDF').disable();
				Ext.getCmp('btnBajarPDF').hide();
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				contenedorPrincipalCmp.add(grid);
				contenedorPrincipalCmp.doLayout();
			}
			
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			
			if(objConsIni.vPantalla=='Monitor'){
				Ext.getCmp('cc_estatus1').setValue(objConsIni.vEstatus);
			}
			
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				if(!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}
				if(!btnBajarPDF.isVisible()) {
					btnImprimirPDF.enable();
				} else {
					btnImprimirPDF.disable();
				}
				el.unmask();
			}else {
				btnGenerarArchivo.disable();
				btnImprimirPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	}
	
	var procesarSuccessFailureGenerarArchivo = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			tipoArchivo = Ext.util.JSON.decode(response.responseText).tipoArchivo;
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarArchivoCsv =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarArchivoPDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//STORES------------------------------------------------------------------------

var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '36consfianzas.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'CVEFIANZA'},
			{name: 'CVEAFIANZA'},
			{name: 'ESTATUS'},
			{name: 'RAMOS'},
			{name: 'SUBRAMO'},
			{name: 'NOMAFINZA'},
			{name: 'NUMFIANZA'},
			{name: 'NUMCONTRATO'},
			{name: 'MONTOTOTAL', type: 'float'},
			{name: 'NOMRAMO'},
			{name: 'NOMSUBRAMO'},
			{name: 'TIPOFIANZA'},
			{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'FECVIGENCIA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'NOMFIADO'},
			{name: 'NOMDEST'},
			{name: 'NOMBENEF'},
			{name: 'NOMESTATUS'},
			{name: 'URLAFIANZA'},
			{name: 'PDF'},
			{name: 'ACCION'},
			{name: 'OBSERVACIONES'},
			{name: 'AUTORIZADOR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
		
	});

	var catalogoAfianza = new Ext.data.JsonStore({
		id: 'catalogoAfianzaStore',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '36consfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoAfianza'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoDestinatario = new Ext.data.JsonStore({
		id: 'catalogoDestStore',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '36consfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoDest'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoFiado = new Ext.data.JsonStore({
		id: 'catalogoFiadoStore',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '36consfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoFiado'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatusStore',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '36consfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeBusqAvanzFiado = new Ext.data.JsonStore({
		id: 'storeBusqAvanzFiado',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '36consfianzas.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//ELEMENTOS DE COMPONENTES------------------------------------------------------
var elementosForm =
	[
		{
			xtype: 'combo',
			name: 'ic_afianzadora',
			id: 'ic_afianzadora1',
			fieldLabel: 'Afianzadora',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_afianzadora',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoAfianza
		},
		{xtype: 'combo',
			name: 'ic_destinatario',
			id: 'ic_destinatario1',
			fieldLabel: 'Destinatario',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_destinatario',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoDestinatario
		},
		{
			xtype: 'combo',
			name: 'ic_fiado',
			id: 'ic_fiado1',
			fieldLabel: 'Fiado',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_fiado',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoFiado
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			id:'regBusqueda',
			msgTarget: 'side',
			combineErrors: false,
			items: [
			{
				xtype: 'button',
				text: 'Busqueda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 			fpBusqAvanzada
							}).show();
					}
				}
			},
			{
					xtype: 'displayfield',
					value: ''
			}
			]
		},
		{
			xtype: 'combo',
			name: 'cc_estatus',
			id: 'cc_estatus1',
			fieldLabel: 'Estatus',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cc_estatus',
			emptyText: 'Seleccione...',
			autoSelect :true,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEstatus
		},
		{
			xtype: 'textfield',
			name: 'cg_numero_fianza',
			id: 'cg_numero_fianza1',
			fieldLabel: 'No. de Fianza',
			allowBlank: true,
			maxLength: 50,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'cg_numero_contrato',
			id: 'cg_numero_contrato1',
			fieldLabel: 'No. de Contrato',
			allowBlank: true,
			maxLength: 30,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Emisi�n Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_solicitud_ini',
					id: 'df_solicitud_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_solicitud_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_solicitud_fin',
					id: 'df_solicitud_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_solicitud_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Vencimiento Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_vencimiento_ini',
					id: 'df_vencimiento_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_vencimiento_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_vencimiento_fin',
					id: 'df_vencimiento_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_vencimiento_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Autorizaci�n Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_autoriza_ini',
					id: 'df_autoriza_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_autoriza_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_autoriza_fin',
					id: 'df_autoriza_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_autoriza_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Rechazo Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_rechazo_ini',
					id: 'df_rechazo_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_rechazo_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_rechazo_fin',
					id: 'df_rechazo_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_rechazo_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		}
	];
	
	var elementsFormBusq = [
		{ 
			xtype:   'label',  
			html:		'<b>Campos de B�squeda:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'textfield',
			name: 'nombreFiado',
			id: 'nombreFiado1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcFiado',
			id: 'rfcFiado1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbFiados1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);
							//fpBusqAvanzada.el.mask('Buscando...','x-mask-loading');
							storeBusqAvanzFiado.load({
									params: Ext.apply(fpBusqAvanzada.getForm().getValues())
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{ 
			xtype:   'label',  
			html:		'<hr>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{ 
			xtype:   'label',  
			html:		'<b>Resultados:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'combo',
			name: 'cbFiados',
			id: 'cbFiados1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'ic_fiado',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzFiado
		}
	];

//COMPONENTES-------------------------------------------------------------------

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'CONSULTA DE FIANZAS',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForm,
		//monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {

					if(!Ext.getCmp('forma').getForm().isValid()){
						return;
					}

					//Valido los campos de fechas
					//Fecha Emisi�n Fianza
					if(Ext.getCmp('df_solicitud_ini').getValue() == ''  && Ext.getCmp('df_solicitud_fin').getValue() != ''){
						Ext.getCmp('df_solicitud_ini').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					if(Ext.getCmp('df_solicitud_ini').getValue() != ''  && Ext.getCmp('df_solicitud_fin').getValue() == ''){
						Ext.getCmp('df_solicitud_fin').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					//Fecha Vencimiento Fianza
					if(Ext.getCmp('df_vencimiento_ini').getValue() == ''  && Ext.getCmp('df_vencimiento_fin').getValue() != ''){
						Ext.getCmp('df_vencimiento_ini').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					if(Ext.getCmp('df_vencimiento_ini').getValue() != ''  && Ext.getCmp('df_vencimiento_fin').getValue() == ''){
						Ext.getCmp('df_vencimiento_fin').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					//Fecha Autorizaci�n Fianza
					if(Ext.getCmp('df_autoriza_ini').getValue() == ''  && Ext.getCmp('df_autoriza_fin').getValue() != ''){
						Ext.getCmp('df_autoriza_ini').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					if(Ext.getCmp('df_autoriza_ini').getValue() != ''  && Ext.getCmp('df_autoriza_fin').getValue() == ''){
						Ext.getCmp('df_autoriza_fin').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					//Fecha Rechazo Fianza
					if(Ext.getCmp('df_rechazo_ini').getValue() == ''  && Ext.getCmp('df_rechazo_fin').getValue() != ''){
						Ext.getCmp('df_rechazo_ini').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					if(Ext.getCmp('df_rechazo_ini').getValue() != ''  && Ext.getCmp('df_rechazo_fin').getValue() == ''){
						Ext.getCmp('df_rechazo_fin').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}

					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar', //Generar datos para la consulta
							start: 0,
							limit: 15
						})
					});
					
					Ext.getCmp('btnGenerarArchivo').disable();
					Ext.getCmp('btnBajarArchivo').hide();
					Ext.getCmp('btnImprimirPDF').disable();
					Ext.getCmp('btnBajarPDF').hide();
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '36consfianzas.jsp';
				}
				
			}
		]
	});
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbFiado= Ext.getCmp("cbFiados1");//ic_fiado1
					var storeCmb = cmbFiado.getStore();
					var cb_fiado = Ext.getCmp('ic_fiado1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbFiado.getValue())) {
						cmbFiado.markInvalid('Seleccione Fiado');
						return;
					}
					var record = cmbFiado.findRecord(cmbFiado.valueField, cmbFiado.getValue());
					record =  record ? record.get(cmbFiado.displayField) : cmbFiado.valueNotFoundText;
					cb_fiado.setValue(cmbFiado.getValue());
					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
				
			}
		]
	});
	
   
   
   function fnDescargaErroresPdf(grid, rowIndex, colIndex, item, e){ // FODEA 028-2012: Fianza Electr�nica (garellano)
      var record = grid.getStore().getAt(rowIndex);
      
      var idFianza = record.data.CVEFIANZA;
		var numFianza = record.data.NUMFIANZA;
		
      Ext.Ajax.request({
         url: '36consfianzas.data.jsp',
         params: {
            informacion:   'PdfErrores',
            cveFianza:     idFianza,
            numeroFianza:  numFianza
         },
         callback: procesarSuccessFailureGenerarArchivo
      });
      
   }
   
   
   
	var grid = new Ext.grid.EditorGridPanel({
	id: 'gridFianzas',
	store: consultaData,
	margins: '20 0 0 0',
	clicksToEdit: 1,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [
		{
			header: 'Afianzadora',
			tooltip: 'Afianzadora',
			dataIndex: 'NOMAFINZA',
			sortable: true,
			width: 250,
			resizable: true,
			hidden: false
		},
		{
			header: 'No. Fianza',
			tooltip: 'Numero de Fianza',
			dataIndex: 'NUMFIANZA',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center'
		},
		{
			header: 'No. Contrato',
			tooltip: 'Documento Fuente',
			dataIndex: 'NUMCONTRATO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center'
		},
		{
			header : 'Monto',
			tooltip: 'Monto Total',
			dataIndex : 'MONTOTOTAL',
			sortable : true,
			width : 150,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Ramo',
			tooltip: 'Ramo',
			dataIndex : 'NOMRAMO',
			width : 150,
			sortable : true
		},
		{
			header : 'Sub Ramo',
			tooltip: 'Sub Ramo',
			dataIndex : 'NOMSUBRAMO',
			width : 150,
			sortable : true
		},
		{
			header : 'Tipo Fianza',
			tooltip: 'Tipo Fianza',
			dataIndex : 'TIPOFIANZA',
			width : 150,
			sortable : true
		},
		{
			header : 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex : 'FECAUTORIZA',
			sortable : true,
			width : 150,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Vigencia',
			tooltip: 'Fecha de Vigencia',
			dataIndex : 'FECVIGENCIA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Fiado',
			tooltip: 'Nombre Fiado',
			dataIndex : 'NOMFIADO',
			width : 150,
			sortable : true
		},
		{
			header : 'Destinatario',
			tooltip: 'Nombre Destinatario',
			dataIndex : 'NOMDEST',
			width : 150,
			sortable : true
		},
		{
			header : 'Beneficiario',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'NOMBENEF',
			width : 150,
			sortable : true
		},
		{
			header : 'Estatus',
			tooltip: 'Estatus Fianza',
			dataIndex : 'NOMESTATUS',
			width : 150,
			sortable : true
		},
		{
			header : 'P�gina Web Afianzadora',
			tooltip: 'P�gina Afianzadora',
			dataIndex : 'URLAFIANZA',
			sortable : true,
			width : 80,
			align: 'center',
			renderer: function(valor,metadata, registro){
					return '<a href="'+valor+'" target="_blank">Ver</a>';
				}
		},
		{
			header : 'PDF Fianza',
			tooltip: 'PDF Fianza',
			dataIndex : 'PDF',
			sortable : true,
			width : 80,
			align: 'center',
			renderer:  function (valor, columna, registro){
					return '<img src="/nafin/00utils/gif/page_white_acrobat.png" alt="Ver.." style="border-style:none" />';
			}
		},
		{
			header : 'Observaciones',
			tooltip: 'Observaciones',
			dataIndex : 'OBSERVACIONES',
			width : 150,
			sortable : true,
			align: 'center',
			renderer:  function (valor, columna, registro){
					var texto = valor;
					if(texto==''){
						texto='No hay observaciones';
					}else{
						texto='Ver'
					}
					return texto;
			}
		},
		{ // FODEA 028-2012: Fianza electr�nica (garellano)
         xtype: 'actioncolumn',
			header : 'PDF Errores',
			tooltip: 'PDF Errores',
			dataIndex : 'CVEFIANZA',
			width : 150,
			sortable : true,
			align: 'center',
         renderer: function(value, metadata, record, rowIndex, colIndex, store){
            var texto = 'N/A';
            if(record.data.ESTATUS=='003'){ // Estatus RECHAZADA = 003
               texto = '';
            }
            return texto;
         },
         items:[
            {
               getClass: function(valor, metadata, record, rowIndex, colIndex, store) {	
                  var ico = '';
                  if(record.data.ESTATUS=='003'){ // Estatus RECHAZADA = 003
                     ico = 'icoPdf';
                  }                  
                  return ico;
               },
               tooltip: 'Ver ...',
               handler: fnDescargaErroresPdf
            }
         ]
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 943,
	title: '',
	frame: true,
	listeners: {
		cellclick :function(grid, rowIndex, colIndex, evento){
				var record = grid.getStore().getAt(rowIndex);  // Se Obtiene objeto Record del store
				var fieldName = grid.getColumnModel().getDataIndex(colIndex); // Se obtine nombre del Campo (Fields del Store)
				if(fieldName == 'PDF'){
					var idFianza = record.get('CVEFIANZA');
					var numFianza = record.get('NUMFIANZA');
					Ext.Ajax.request({
						url: '36consfianzas.data.jsp',
						params: {
								informacion: 'fianzaPDF',
								cveFianza : idFianza,
								numeroFianza : numFianza,
								tipoArchivo : 'pdf'
								},
						callback: procesarSuccessFailureGenerarArchivo
					});
				}
				if(fieldName == 'OBSERVACIONES'){
					var txtObs = record.get('OBSERVACIONES');
					if(txtObs!=''){
						var ventana = Ext.getCmp('winObservaciones');
						if (ventana) {
							var objObserv = ventana.findById('iwObserv');
							objObserv.setValue(txtObs);
							ventana.show();
						} else {
							new Ext.Window({
										layout: 'fit',
										width: 250,
										height: 100,
										id: 'winObservaciones',
										title:'Observaciones',
										closeAction: 'hide',
										draggable :true,
										resizable : false,
										items: [
											{
											xtype:'textarea',
											name:'wObserv',
											id:'iwObserv',
											emptyText: 'No hay Observaciones',
											value:txtObs,
											readOnly : true
											}
										]
									}).show();
						}
					}
				}
		}
	},
	bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
		items: [
			'-',
			{
				xtype: 'button',
				text: 'Generar Archivo',
				id: 'btnGenerarArchivo',
				handler: function(boton, evento) {
					boton.disable();
					Ext.Ajax.request({
						url: '36consfianzas.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV',
							tipo: 'CSV'
						}),
						callback: procesarSuccessFailureGenerarArchivoCsv
					});
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				id: 'btnBajarArchivo',
				hidden: true
			},
			'-',
			{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnImprimirPDF',
				handler: function(boton, evento) {
					boton.disable();
					Ext.Ajax.request({
						url: '36consfianzas.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF',
							tipo: 'PDF'
						}),
						callback: procesarSuccessFailureGenerarArchivoPDF
					});
				}
			},
			{
				xtype: 'button',
				text: 'Bajar PDF',
				id: 'btnBajarPDF',
				hidden: true
			},
			'-'
		]
	}
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		items: [
			fp,
			NE.util.getEspaciador(20)
			//grid
		]
	});
	
//------------------------------------------------------------------------------
catalogoAfianza.load();
catalogoFiado.load();
catalogoEstatus.load();
catalogoDestinatario.load();

Ext.Ajax.request({
	url: '36consfianzas.data.jsp',
	params: {
			informacion:'valoresIniciales'
			},
	callback: procesarSuccessValoresIni
});

});