<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoPYME,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fianza.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>

<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String valuePerfil = (!"".equals(request.getParameter("comboPerfiles")))?request.getParameter("comboPerfiles"):"AUTORIZADOR EPO,ADMIN AUTO EPO,ADMIN EPO, REVISOR EPO";
if (informacion.equals("valoresIniciales")) {
	JSONObject jsonObj = new JSONObject();

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("nafelec", iNoNafinElectronico);
	jsonObj.put("strUsuario", strNombre);

	infoRegresar= jsonObj.toString();
}else if (informacion.equals("Consulta")) {
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);

	try{
		List lstUsersInfo = new ArrayList();
		JSONArray jsObjArray = new JSONArray();
		System.out.println("valuePerfil=="+valuePerfil);
		lstUsersInfo = fianzaElectronica.getAutorizadoresUsuariosxEpo(iNoCliente,"E", valuePerfil);
		jsObjArray = JSONArray.fromObject(lstUsersInfo);
		infoRegresar = "({\"success\": true, \"total\": \"" +
				jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";
	}catch(Throwable t){
		throw new AppException("Error en peticion de usuarios ", t);
	}
}
%>

<%=infoRegresar%>