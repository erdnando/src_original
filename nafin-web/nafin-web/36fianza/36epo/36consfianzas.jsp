<!DOCTYPE html>

<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession.jspf" %>

<% String version = (String)session.getAttribute("version"); %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<% if(version!=null) { %>	
	<%@ include file="/01principal/menu.jspf"%>
<%}%>

<%String vRamo  = request.getParameter("ramo");%>
<%String vSubRamo  = request.getParameter("subramo");%>
<%String vEstrato  = request.getParameter("estrato");%>
<%String vEstatus  = request.getParameter("estatus");%>
<%String vPantalla  = request.getParameter("pantalla");%>

<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
<script type="text/javascript" src="36consfianzas.js?<%=session.getId()%>"></script>
<script language="JavaScript">
var objConsIni={
	vRamo: '<%=vRamo%>',
	vSubRamo: '<%=vSubRamo%>',
	vEstrato: '<%=vEstrato%>',
	vEstatus: '<%=vEstatus%>',
	vPantalla: '<%=vPantalla%>'
}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if( "CONS PYME FIA".equals(strPerfil)  && version!=null) { %>	

	<%@ include file="/01principal/01pyme/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01pyme/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:190px"></div></div>
		</div>
	</div>
	<%@ include file="/01principal/01pyme/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>

<%}else  if( ( "REVISOR EPO".equals(strPerfil) ||  "ADMIN PYME".equals(strPerfil) || "ADMIN EPO".equals(strPerfil) || "ADMIN AUTO EPO".equals(strPerfil)  || "AUTORIZADOR EPO".equals(strPerfil) ) && version!=null) { %>	

	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:190px"></div></div>
		</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
	<%}else {%>	
	<%if( version==null) { %>
		<div id='areaContenido' style="margin-left: 3px; margin-top: 3px;"></div>
		<form id='formAux' name="formAux" target='_new'></form>

<%} else  if( (   "ADMIN AFIANZA".equals(strPerfil)   ||  "CONS AFIANZA".equals(strPerfil)  )   && version!=null ) { %>

	<%@ include file="/01principal/01afianzadora/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01afianzadora/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:190px"></div></div>
		</div>
	</div>
	<%@ include file="/01principal/01afianzadora/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
	
<%} else  if(  "CONS FIADO".equals(strPerfil)  && version!=null ) { %>

<%@ include file="/01principal/01fiado/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01fiado/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:190px"></div></div>
		</div>
	</div>
	<%@ include file="/01principal/01fiado/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>

<%}else  {%> 
	
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>

		<div id="_menuApp"></div>
			<div id="Contcentral">
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
				<div id="areaContenido"><div style="height:230px"></div></div>
			</div>
		</div>
		
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
	
	<%}%>
	
	<%}%>

</body>
</html>