<!DOCTYPE html>
<%@ page import="java.util.*,
			com.netro.fianza.*,
			java.io.*, 
			java.util.*,
			java.math.*,
			java.sql.*,
			javax.naming.*,
			netropology.utilerias.*" 
			contentType="text/html;charset=windows-1252"
			errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession.jspf" %>

<%
	String version =""; 
	String archivo ="";
	String ruta  = strDirectorioPublicacion+"/16archivos/fianza/TerminosyCondiciones/";
	File directorio = new File(ruta);
	String[] ficheros = directorio.list(); 
	if (ficheros != null){	
		int ult = ficheros.length -1;
		archivo  = ficheros[ult];		
	}	
	if(!archivo.equals("")){
	int tamanio2 =	archivo.length();
		version =  archivo.substring(0, tamanio2-4);				
	}
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="36TerCondiciones.js?<%=session.getId()%>"></script>

<script>
function MM_openBrWindow(theURL,winName,features)
{ //v2.0
  window.open(theURL,winName,features);
}

function mostrar(){ 	
	leerArchivo ='S';
  var strURL="../16archivos/fianza/TerminosyCondiciones/"+'<%=archivo%>';
  var strWinName="wndCDoctos";
  var strFeatures="toolbar=no,status=no,location=no,menubar=no,scrollbars=no,resizable=yes,directories=no,fullscreen=no,channelmode=no,top=200,left=300,width=500,height=350";
  MM_openBrWindow(strURL,strWinName,strFeatures);
} 
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01fiado/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01fiado/menuLateralFlotante.jspf"%>
	<div id="areaContenido" style="margin-left: 3px; margin-top: 3px; text-align:center"></div>						
	</div>
	</div>
	<form id='formAux' name="formAux" target='_new'></form>
	<form id='formParametros' name="formParametros">
	<input type="hidden" id="version" name="version" value="<%=version%>"/>
	</form>
	<%@ include file="/01principal/01fiado/pie.jspf"%>
</body>
</html>