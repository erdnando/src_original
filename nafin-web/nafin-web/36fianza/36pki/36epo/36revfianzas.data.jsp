<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalgoAfianzadora,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fianza.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";

if (informacion.equals("valoresIniciales")) {
	JSONObject jsonObj = new JSONObject();
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	String firmaMancomunada = fianzaElectronica.validaFirmaMancomunada(iNoCliente)?"S":"N";

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("firmaManc", firmaMancomunada);
	jsonObj.put("usuario", strNombreUsuario);

	infoRegresar= jsonObj.toString();
	
} else if( informacion.equals("observacionesRetornoConcentrador") ){
	
	String 		rowIndex 		= (request.getParameter("rowIndex")!=null)?request.getParameter("rowIndex"):"-1";
	
	JSONObject 	jsonObj 			= new JSONObject();
	String 		fechaYHora 		= Fecha.getHoraActual("DD/MM/YYYY HH24:MI:SS");
	
	jsonObj.put("success", 		new Boolean(true));
	jsonObj.put("fechaYHora", 	fechaYHora);
	jsonObj.put("rowIndex", 	rowIndex);
	
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("catalogoAfianza")) {
	CatalgoAfianzadora cat = new CatalgoAfianzadora();
	cat.setCampoClave("ic_afianzadora");
	cat.setCampoDescripcion("CG_RAZON_SOCIAL");
	cat.setClaveEpo(iNoCliente);
	cat.setOrden("CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();
}else if (informacion.equals("catalogoFiado")) {
	CatalogoFiado cat = new CatalogoFiado();
	cat.setCampoClave("ic_fiado");
	cat.setCampoDescripcion("cg_nombre");
	cat.setClaveEpo(iNoCliente);
	cat.setOrden("cg_nombre");
	infoRegresar = cat.getJSONElementos();
}else if (informacion.equals("catalogoEstatus")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("fecat_estatus");
	cat.setCampoClave("cc_estatus");
	cat.setCondicionIn("002", "String");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setOrden("cg_descripcion");
	infoRegresar = cat.getJSONElementos();
}else if (informacion.equals("catalogoAutorizador")) {
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	try{
		List lstUsersInfo = new ArrayList();
		JSONArray jsObjArray = new JSONArray();
		lstUsersInfo = fianzaElectronica.getAutorizadoresUsuariosxEpo(iNoCliente,"E", "AUTORIZADOR EPO,ADMIN AUTO EPO");
		//*** Para agregar la opcion de seleccionar todos... F028
		
		HashMap hpusuarios = new HashMap();
		hpusuarios.put("USUARIO","T");
		hpusuarios.put("NOMBREUSUARIO","Seleccionar todos...");
		
		lstUsersInfo.add(hpusuarios);

		jsObjArray = JSONArray.fromObject(lstUsersInfo);
		infoRegresar = "({\"success\": true, \"total\": \"" +
				jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";
	}catch(Throwable t){
		throw new AppException("Error en peticion de usuarios ", t);
	}
}else if (informacion.equals("Consultar")) {
	ParamConsBean params = new ParamConsBean();
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	if("REVISOR EPO".equals(strPerfil)){
		params.setUsuario(iNoUsuario);
	}
	
	//recive parameters
	params.setIcEpo(iNoCliente);
	params.setIcAfianzadora(request.getParameter("ic_afianzadora")==null?"":request.getParameter("ic_afianzadora"));
	params.setIcFiado(request.getParameter("ic_fiado")==null?"":request.getParameter("ic_fiado"));
	//params.setIcEstatus(request.getParameter("cc_estatus")==null?"001":request.getParameter("cc_estatus"));
	params.setIcEstatus("002");
	params.setNumFianza(request.getParameter("cg_numero_fianza")==null?"":request.getParameter("cg_numero_fianza"));
	params.setNumContrato(request.getParameter("cg_numero_contrato")==null?"":request.getParameter("cg_numero_contrato"));
	params.setDfSolicIni(request.getParameter("df_solicitud_ini")==null?"":request.getParameter("df_solicitud_ini"));
	params.setDfSolicFin(request.getParameter("df_solicitud_fin")==null?"":request.getParameter("df_solicitud_fin"));
	params.setDfVencIni(request.getParameter("df_vencimiento_ini")==null?"":request.getParameter("df_vencimiento_ini"));
	params.setDfVencFin(request.getParameter("df_vencimiento_fin")==null?"":request.getParameter("df_vencimiento_fin"));
	params.setDfPublicIni(request.getParameter("df_publicacion_ini")==null?"":request.getParameter("df_publicacion_ini"));
	params.setDfPublicFin(request.getParameter("df_publicacion_fin")==null?"":request.getParameter("df_publicacion_fin"));

	List lstFianzasInfo = new ArrayList();
	JSONArray jsObjArray = new JSONArray();
	lstFianzasInfo = fianzaElectronica.getFianzas(params);
	jsObjArray = JSONArray.fromObject(lstFianzasInfo);


	infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";

} else if(informacion.equals("busquedaAvanzada")){

	String rfcFiado = request.getParameter("rfcFiado")==null?"":request.getParameter("rfcFiado");
	String nombreFiado = request.getParameter("nombreFiado")==null?"":request.getParameter("nombreFiado");

	CatalogoFiado cat = new CatalogoFiado();
	cat.setCampoClave("ic_fiado");
	cat.setCampoDescripcion("cg_nombre");
	cat.setClaveEpo(iNoCliente);
	if(!nombreFiado.equals(""))
		cat.setNombreFiado(nombreFiado);
	if(!rfcFiado.equals(""))
		cat.setRfcFiado(rfcFiado);
	cat.setOrden("cg_nombre");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("acuseRevision")){

	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	JSONArray jsObjArray = new JSONArray();
	String acuse = (request.getParameter("acuse") == null)?"":request.getParameter("acuse");


	List lstRegFianzas = new ArrayList();
	lstRegFianzas = fianzaElectronica.getAcuseConcFianzas(acuse);
	jsObjArray = JSONArray.fromObject(lstRegFianzas);
	infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";


}

System.out.println("infoRegresar = " + infoRegresar);

%>
<%=infoRegresar%>