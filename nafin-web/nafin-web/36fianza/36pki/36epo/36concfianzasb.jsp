<%@ page import="java.sql.*, java.text.*, java.util.*, java.math.*,
				netropology.utilerias.*, com.netro.exception.*,	
				com.netro.fianza.*,
				net.sf.json.*" 
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>
<%@ include file="/36fianza/36pki/certificado.jspf" %>
<%
	
	JSONObject jsonObj = new JSONObject();
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);

	// ----- Variables de seguridad -----
	String pkcs7 = request.getParameter("Pkcs7");
	String folioCert = "";
	String externContent = request.getParameter("TextoFirmado");
	char getReceipt = 'Y';
	String acuseRecibo = "";
	
	// ----- Autentificaci�n -----
	Seguridad s = new Seguridad();
	AcuseFianza acuseFianza = new AcuseFianza(AcuseFianza.ACUSE_REVISION);
	String acuse = acuseFianza.getAcuse();
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		folioCert = acuse;
		
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
				
			String _acuse = s.getAcuse();
			acuseFianza.setReciboElectronico(_acuse);
			
			List arrRegistrosModificados = JSONArray.fromObject(jsonRegistros);
			//String claves = "";
			//String clavesOper = "";
			
			ArrayList lstClaves = new ArrayList();
			Iterator itReg = arrRegistrosModificados.iterator();
			int index = 0;
			while (itReg.hasNext()) {
				JSONObject registro = (JSONObject)itReg.next();
				HashMap mapFianzas = new HashMap();
				mapFianzas.put("CVEFIANZA",registro.getString("CVEFIANZA"));
				mapFianzas.put("ESTATUS","002");
				lstClaves.add(mapFianzas);
			}
			
			fianzaElectronica.setEnvioFianzasBandeja(lstClaves,strLogin,acuseFianza);
			
			jsonObj.put("destinatario", strNombre);
			jsonObj.put("acuse", acuseFianza.getAcuse());
			jsonObj.put("recibo", acuseFianza.getReciboElectronico());
			jsonObj.put("fecha", acuseFianza.getFecha());
			jsonObj.put("hora", acuseFianza.getHora());
			jsonObj.put("usuario", strLogin+"-"+strNombreUsuario);
			jsonObj.put("totalFianzas", String.valueOf(arrRegistrosModificados.size()));
			jsonObj.put("msgExito", "<p align='center'>La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br><p>");
			jsonObj.put("success", new Boolean(true));
		} else {	//autenticaci�n fallida
			String _error = s.mostrarError();
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
		}
	}
%>
<%=jsonObj%>