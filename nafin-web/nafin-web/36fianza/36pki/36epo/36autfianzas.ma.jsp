<%@ page
		import="	java.util.*, 
				netropology.utilerias.*, 
				javax.naming.*,
				java.io.*,				
				org.apache.commons.fileupload.disk.*,
				org.apache.commons.fileupload.servlet.*,
				org.apache.commons.fileupload.*,				
				net.sf.json.*
				"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs_fileupload.jsp"							
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>

<%
String  cveFianza = (request.getParameter("cveFianza")!=null)?request.getParameter("cveFianza"):"";
JSONObject resultadoProceso = new JSONObject();

String rutaArchivoTemporal = null;
ParametrosRequest req = null;
String infoRegresar = "";

if (ServletFileUpload.isMultipartContent(request)) {
	
	// Create a factory for disk-based file items
	DiskFileItemFactory factory = new DiskFileItemFactory();
	
	// Set factory constraints
	//factory.setSizeThreshold(yourMaxMemorySize);
	factory.setRepository(new File(strDirectorioTemp));
	// Create a new file upload handler
	ServletFileUpload upload = new ServletFileUpload(factory);
	// Set overall request size constraint
	upload.setSizeMax(10 * 5242880);	//10 Kb
	
	// Parse the request
	req = new ParametrosRequest(upload.parseRequest(request));
	
	// Process the uploaded items
	FileItem item = (FileItem)req.getFiles().get(0);
	
	//se ingresa la imagen del archivo	
	netropology.utilerias.PDF_correcciones_FianzaElectronica imagenDocumento = new netropology.utilerias.PDF_correcciones_FianzaElectronica();
	String tamanio = Long.toString (item.getSize());
		int  tamaniot = Integer.parseInt(tamanio);
	if(tamaniot <= 5242880) {
		try {
			imagenDocumento.setSize(tamaniot);	
			imagenDocumento.setCveFianza(cveFianza);
			imagenDocumento.guardarArchivo(item.getInputStream());
			
			resultadoProceso.put("success", new Boolean(true));
			resultadoProceso.put("resultado", "ArchivoGuardado");

		} catch (Exception e) {
				e.printStackTrace();
				resultadoProceso.put("success", new Boolean(false));
		  }
	} else {		  
		resultadoProceso.put("success", new Boolean(true));
		resultadoProceso.put("resultado", "Archivomayora5");
	}		
	infoRegresar	 =resultadoProceso.toString();
}

System.out.println("infoRegresar = " + infoRegresar);
%>

<%=infoRegresar%>

