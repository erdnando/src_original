<%@ page import="java.text.*, java.util.*, 
				netropology.utilerias.*, 
				com.netro.exception.*,
				net.sf.json.*, 
				java.io.*, 
				com.netro.fianza.*,
				com.netro.pdf.*"
				contentType="application/json;charset=UTF-8"
				errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
try {
	//	DECLARACION DE VARIABLES
	//	Par�metros provenienen de la p�gina anterior
	String  numeroFianza = (request.getParameter("numeroFianza") == null)?"":request.getParameter("numeroFianza");
	String  tipoArchivo = (request.getParameter("tipoArchivo") == null)?"":request.getParameter("tipoArchivo");
	String  cveFianza = (request.getParameter("cveFianza") == null)?"":request.getParameter("cveFianza");
	String  etapa = (request.getParameter("etapa") == null)?"":request.getParameter("etapa");
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String jsonRegAcuse = (request.getParameter("registrosAcu") == null)?"":request.getParameter("registrosAcu");
	String nombreArchivo = "";

	if("pdf".equals(tipoArchivo) && !"acuse".equals(etapa)){
		
		FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
		nombreArchivo = fianzaElectronica.getPDFfianzas(cveFianza,strDirectorioTemp);
		
	}else if("pdf".equals(tipoArchivo)){
		List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
		List arrDatosAcuse = JSONArray.fromObject(jsonRegAcuse);
		Iterator itReg = arrRegistrosEnviados.iterator();
		Iterator itRegAcu = arrDatosAcuse.iterator();
		
		CreaArchivo archivo = new CreaArchivo();
		nombreArchivo = archivo.nombreArchivo()+"."+tipoArchivo;	
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
												session.getAttribute("iNoNafinElectronico").toString(),
												(String)session.getAttribute("sesExterno"),
												(String) session.getAttribute("strNombre"),
												(String) session.getAttribute("strNombreUsuario"),
												(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
		pdfDoc.setTable(2,50);
		pdfDoc.setCell("ACUSE","celda01",ComunesPDF.CENTER,2,2);
		
		while (itRegAcu.hasNext()) {
			JSONObject registro = (JSONObject)itRegAcu.next();			
			pdfDoc.setCell(registro.getString("etiqueta"),"formas",ComunesPDF.LEFT,1,2);
			pdfDoc.setCell(registro.getString("informacion"),"formas",ComunesPDF.CENTER,1,2);
		} //fin del for
		
		pdfDoc.addTable();
		
		pdfDoc.setTable(15,100);
		pdfDoc.setCell("Afianzadora","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("No. Fianza","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("No. Contrato","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Ramo","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Sub Ramo","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Tipo Fianza","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Vigencia","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Fiado","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Destinatario","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Obervaciones","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Autorizador","celda01",ComunesPDF.CENTER,1,2);
		
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();			
			pdfDoc.setCell(registro.getString("NOMAFINZA"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("NUMFIANZA"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("NUMCONTRATO"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(Comunes.formatoDecimal(registro.getString("MONTOTOTAL"),2),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("NOMRAMO"),"formas",ComunesPDF.CENTER,1,2);
			
			pdfDoc.setCell(registro.getString("NOMSUBRAMO"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("TIPOFIANZA"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("FECAUTORIZA"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("FECVIGENCIA"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("NOMFIADO"),"formas",ComunesPDF.CENTER,1,2);
			
			
			pdfDoc.setCell(registro.getString("NOMDEST"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("NOMBENEF"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("NOMESTATUS"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell("".equals(registro.getString("NEWOBSERVACIONES"))?"No hay Observaciones":registro.getString("NEWOBSERVACIONES"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("NOMBREAUTORIZADOR"),"formas",ComunesPDF.CENTER,1,2);
		} //fin del for
		
		pdfDoc.addTable();
		pdfDoc.endDocument();

	
	
	}
		System.out.println("strDirecVirtualTemp+nombreArchivo == "+strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("tipoArchivo", tipoArchivo);
		jsonObj.put("etapa", etapa);

} catch (Exception e) {
	e.printStackTrace();
	jsonObj.put("success", new Boolean(false));
	jsonObj.put("msg", "Error al generar el archivo");
}
%>

<%=jsonObj%>