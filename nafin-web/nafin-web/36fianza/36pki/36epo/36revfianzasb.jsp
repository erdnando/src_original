<%@ page import="java.sql.*, java.text.*, java.util.*, java.math.*,
				netropology.utilerias.*, com.netro.exception.*,	
				com.netro.fianza.*,
				net.sf.json.*" 
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>
<%@ include file="/36fianza/36pki/certificado.jspf" %>
<%
	
	JSONObject jsonObj = new JSONObject();
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String ifirmaMan = (request.getParameter("ifirmaMan") == null)?"":request.getParameter("ifirmaMan");
	
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);

	// ----- Variables de seguridad -----
	String pkcs7 = request.getParameter("Pkcs7");
	String folioCert = "";
	String externContent = request.getParameter("TextoFirmado");
	String autorizador =  request.getParameter("autorizador"); //F028-2012
	char getReceipt = 'Y';
	String acuseRecibo = "";
	
	// ----- Autentificaci�n -----
	Seguridad s = new Seguridad();
	AcuseFianza acuseFianza = new AcuseFianza(AcuseFianza.ACUSE_AUTORIZACION);
	String acuse = acuseFianza.getAcuse();
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		folioCert = acuse;
		
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
				
			String _acuse = s.getAcuse();
			acuseFianza.setReciboElectronico(_acuse);
			
			List arrRegistrosModificados = JSONArray.fromObject(jsonRegistros);
			//String claves = "";
			//String clavesOper = "";
			
			ArrayList lstClaves = new ArrayList();
			Iterator itReg = arrRegistrosModificados.iterator();
			int index = 0;
			int contAutorizados = 0;
			int contRechazados = 0;
			int contRetornados = 0;
			while (itReg.hasNext()) {
				JSONObject registro = (JSONObject)itReg.next();
				HashMap mapFianzas = new HashMap();
				String estatus = registro.getString("ESTATUS");
				mapFianzas.put("CVEFIANZA",registro.getString("CVEFIANZA"));
				mapFianzas.put("ESTATUS",registro.getString("ESTATUS"));
				mapFianzas.put("OBSERVACIONES",registro.getString("NEWOBSERVACIONES"));
				mapFianzas.put("AUTORIZADOR",registro.getString("AUTORIZADOR"));
				lstClaves.add(mapFianzas);
				
				//CONTADORES RESUMEN ACUSE
				if("001".equals(estatus)) contRetornados++;
				if("003".equals(estatus)) contRechazados++;
				if("004".equals(estatus) || "005".equals(estatus)) contAutorizados++;
			}
			
			acuseFianza.setTotalAutorizadas(String.valueOf(contAutorizados));
			acuseFianza.setTotalRechazadas(String.valueOf(contRechazados));
 			acuseFianza.setTotalRetornadas(String.valueOf(contRetornados));
			
			fianzaElectronica.setRevisionFianzas(lstClaves, ifirmaMan, strLogin,acuseFianza);
			//Recupera los usuarios que cumplan el perfil para enviar los correos de notificacion si selecciono la opcion de TODOS
			//Fodea 028-2012
			if ( autorizador.equals("S") ) {	//Si autorizo para todos
				String num_usuario = "";
				List lstUsersInfo = new ArrayList();
				lstUsersInfo = fianzaElectronica.getAutorizadoresUsuariosxEpo(iNoCliente,"E", "AUTORIZADOR EPO,ADMIN AUTO EPO");

				if(lstUsersInfo!=null && lstUsersInfo.size()>0){				
					for(int i=0; i<lstUsersInfo.size(); i++) {
						HashMap map = new HashMap();
						map = (HashMap)lstUsersInfo.get(i);
						num_usuario = (String) map.get("USUARIO");
						fianzaElectronica.setEnvioCorreoFianzas(acuseFianza.getAcuse(),num_usuario,"revision",iNoCliente);
					}
				}
			} else {
				fianzaElectronica.setEnvioCorreoFianzas(acuseFianza.getAcuse(),iNoUsuario,"revision",iNoCliente);
			}
			
			
			jsonObj.put("destinatario", strNombre);
			jsonObj.put("acuse", acuseFianza.getAcuse());
			jsonObj.put("recibo", acuseFianza.getReciboElectronico());
			jsonObj.put("fecha", acuseFianza.getFecha());
			jsonObj.put("hora", acuseFianza.getHora());
			jsonObj.put("usuario", strLogin+"-"+strNombreUsuario);
			jsonObj.put("totalFianzas", String.valueOf(arrRegistrosModificados.size()));
			jsonObj.put("totalAut", acuseFianza.getTotalAutorizadas());
			jsonObj.put("totalRech", acuseFianza.getTotalRechazadas());
			jsonObj.put("totalRetor", acuseFianza.getTotalRetornadas());
			jsonObj.put("msgExito", "<p align='center'>La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br><p>");
			jsonObj.put("success", new Boolean(true));
		} else {	//autenticaci�n fallida
			String _error = s.mostrarError();
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "La autentificaci�n no se llev� a cabo. " + _error + ".<br>Proceso CANCELADO");
		}
	}
%>
<%=jsonObj%>