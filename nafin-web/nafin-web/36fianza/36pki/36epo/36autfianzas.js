

Ext.onReady(function() {
	var ifirmaMan = '';
	var iusuario = '';
	var variable = 0;
	var pdf_correcciones = false;
//---------------------------HANDLERS-------------------------------

	var procesarSuccessValoresIni = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			ifirmaMan = resp.firmaManc;
			iusuario = resp.usuario;
			
			if(ifirmaMan!='S'){
				var objMsg = Ext.getCmp('mensajes1');
				objMsg.body.update("Favor de remitirse al men� Revisi�n de Fianzas para la autorizaci�n o rechazo de las mismas.");
				objMsg.show();
				fp.hide();
			}else{
				fp.show();
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesaRetornarRevision = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Obtener registro
			var gridFianzas 						= Ext.getCmp('gridFianzas');
			var record 								= gridFianzas.getStore().getAt(resp.rowIndex);
			
			// Actualizar campo de observaciones
			record.data['NEWOBSERVACIONES']	= 'Fianza devuelta por el usuario '+iusuario+' '+resp.fechaYHora;
			record.data['AUTORIZADOR']			= '';
			record.commit();
			
		} else {
			
			// Revertir los cambios	
			var gridFianzas 						= Ext.getCmp('gridFianzas');
			var record 								= gridFianzas.getStore().getAt(opts.params.rowIndex); 
			record.reject();
			
			// Y mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
		
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				contenedorPrincipalCmp.add(grid);
				contenedorPrincipalCmp.doLayout();
			}
			
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	}
	
	var procesarSuccessFailureGenerarArchivo = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			tipoArchivo = Ext.util.JSON.decode(response.responseText).tipoArchivo;
			etapa = Ext.util.JSON.decode(response.responseText).etapa;
			var btnImpr = Ext.getCmp('btnImprimirPDF');
			
			if(tipoArchivo=="pdf" && !btnImpr.isVisible()){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			}else{
			
				btnImpr.setIconClass('');
				var btnAbrirPDF = Ext.getCmp('btnAbrirPDF');
				btnAbrirPDF.show();
				btnAbrirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnAbrirPDF.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	
	var onAutorizar = function(btn) {
		var gridFianzas = Ext.getCmp('gridFianzas');
		var store = gridFianzas.getStore();
		var columnModelGrid = gridFianzas.getColumnModel();
		var noRegistros = false;
		var badRegistros = false;
		var siHayAccion = false;
		var numRegistro = -1;
		var registrosEnviar = [];

		store.each(function(record) {
			numRegistro = store.indexOf(record);
			if(record.data['ACCION']=='1' || record.data['ACCION']=='2' || record.data['ACCION']=='3'){
				siHayAccion = true;
				if(record.data['ACCION']=='2' && (record.data['NEWOBSERVACIONES']=='' && record.data['banderaPDF']!= true ) ){ //F028-2012
					Ext.MessageBox.alert('Mensaje','Favor de capturar los motivos por los que se rechaza la fianza y/o el archivo .PDF con las correcciones solicitadas por cada fianza que se desea rechazar',
							function(){
								gridFianzas.startEditing(numRegistro, columnModelGrid.findColumnIndex('NEWOBSERVACIONES'));
							}
					);
					
					badRegistros = true;
					return false;
				}
				registrosEnviar.push(record);
				noRegistros = true;
			}
		});
		
		if(noRegistros && !badRegistros){
			Ext.MessageBox.confirm('Autorizaci�n de Fianzas','�Esta seguro de autorizar las fianzas seleccionadas?',function(msb){
				if(msb=='yes'){
					//gridFianzas.el.mask('Enviando...', 'x-mask-loading');						
					var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
					//registrosEnviar = Ext.encode(registrosEnviar);
					gridFianzas.stopEditing();
					preAcuseData.add(registrosEnviar);
					if (!gridAcuPreAcu.isVisible()) {
						contenedorPrincipalCmp.remove(fp);
						contenedorPrincipalCmp.remove(grid);
						contenedorPrincipalCmp.add(gridAcuPreAcu);
						contenedorPrincipalCmp.doLayout();
					}
				}//fin if(msb=='NO')
			});//fin Confirm
		}//fin if(noregistros)
		
		if(!siHayAccion){
			Ext.MessageBox.alert('Revisi�n de Fianzas','Es necesario seleccionar alguna Acci�n para continuar');
		}
		
	}//fin handler onEnvio
	
	var onPreacuse = function(btn){
		var gridPreAcuse = Ext.getCmp('gridPreAcu');
		var store = gridPreAcuse.getStore();
		
		
		var textoFirmar = "Nombre Afianzadora|No. de Fianza|Monto Total|Ramo|SubRamo|Tipo Fianza|Fecha Autorizacion|"+
		"Fecha de Vigencia|Fiado|Destinatario|Beneficiario|Estatus|";
		var registrosEnviar = [];

		store.each(function(record) {
			//if(record.data['SELECCION']){
				textoFirmar += "\n"+
									record.data['NOMAFINZA'] + "|" +
									record.data['NUMFIANZA'] + "|" +
									Ext.util.Format.number(record.data['MONTOTOTAL'],'$0,0.00') + "|"+
									record.data['NOMRAMO'] + "|" +
									record.data['NOMSUBRAMO'] + "|" +
									record.data['TIPOFIANZA'] + "|" +
									Ext.util.Format.date(record.data['FECAUTORIZA'],'d/m/Y H:i:s') + "|" +
									Ext.util.Format.date(record.data['FECVIGENCIA'],'d/m/Y H:i:s') + "|" +
									record.data['NOMFIADO'] + "|" +
									record.data['NOMDEST'] + "|" +
									record.data['NOMBENEF'] + "|" +
									record.data['NOMESTATUS'] + "|";
				
				registrosEnviar.push(record.data);
			//}
		});//fin each(modificados)
		
		NE.util.obtenerPKCS7(autorizar, textoFirmar, registrosEnviar);
		
	}//end handler onPreAcuse
	
	var autorizar = function(pkcs7, textoFirmar, registrosEnviar) {
		var gridPreAcuse = Ext.getCmp('gridPreAcu');
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else{
			gridPreAcuse.stopEditing();
			registrosEnviar = Ext.encode(registrosEnviar);
			gridPreAcuse.el.mask('Procesando...', 'x-mask-loading');
			
			Ext.Ajax.request({
				url : '36autfianzasb.jsp',
				params :{
					registros : registrosEnviar,
					Pkcs7: pkcs7,
					TextoFirmado: textoFirmar,
					firmaMan : ifirmaMan
				},
				callback: procesarSuccessFailureAutorizacion
			});
		
		}
	}
	
	var procesarSuccessFailureAutorizacion =  function(opts, success, response) {
		var gridPreAcu = Ext.getCmp('gridPreAcu');
		gridPreAcu.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridPreAcu.getStore().commitChanges();
			var ObjJsonb = Ext.util.JSON.decode(response.responseText);
			var msgAcuse = ObjJsonb.msgExito;
			var acuseGral = [
				['Destinatario',ObjJsonb.destinatario],
				['N�mero de Acuse', ObjJsonb.acuse],
				['N�mero de Recibo', ObjJsonb.recibo],
				['Fecha', ObjJsonb.fecha],
				['Hora', ObjJsonb.hora],
				['Usuario', ObjJsonb.usuario],
				['Fianzas Autorizadas', ObjJsonb.totalAut],
				['Fianzas Rechazadas', ObjJsonb.totalRech],
				['Fianzas Retornadas', ObjJsonb.totalRetor]
			];
			
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var gridAcu = Ext.getCmp('gridAcu');
			var storeAcuse = gridAcu.getStore();
			var btnCancel = Ext.getCmp('btnCancelar');
			var btnCont = Ext.getCmp('btnContinuar');
			var btnImpr = Ext.getCmp('btnImprimirPDF');
			
			//gridAcuPreAcu.el.mask('Cargando...', 'x-mask-loading');
			gridAcuPreAcu.reconfigure(storeRegAcuseData,gridAcuPreAcu.getColumnModel())
			storeRegAcuseData.load({
				params:{
					acuse : ObjJsonb.acuse
				}
			});
			
			storeAcuse.loadData(acuseGral);			
			contenedorPrincipalCmp.insert(0,gridAcu);
			contenedorPrincipalCmp.doLayout();
			
			btnCont.hide();
			btnImpr.show();
			btnCancel.setText('Salir');

			Ext.MessageBox.alert('Autorizaci�n de Fianzas','Autorizaci�n realizada exitosamente');
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var onImprimirPDF = function(btn){
		var gridAcuses = Ext.getCmp('gridAcu');
		var gridAcuseReg = Ext.getCmp('gridPreAcu');
		var storeAcu = gridAcuses.getStore();
		var storeAcuReg = gridAcuseReg.getStore();
		var arrRegistrosAcuse = [];
		var arrRegistros = [];
		
		
		btn.setIconClass('loading-indicator');
		btn.disable();
		
		storeAcu.each(function(record){
			arrRegistrosAcuse.push(record.data);
		});
		storeAcuReg.each(function(record){
			arrRegistros.push(record.data);
		});
		arrRegistrosAcuse = Ext.encode(arrRegistrosAcuse);
		arrRegistros = Ext.encode(arrRegistros);
		
		Ext.Ajax.request({
			url: '36autfianzasfiles.jsp',
			params: {
					registros : arrRegistros,
					registrosAcu : arrRegistrosAcuse,
					etapa : 'acuse',
					tipoArchivo : 'pdf'
					},
			callback: procesarSuccessFailureGenerarArchivo
		});
	}
	
//finge que todo salio bien =)
	var procesarSuccessFailureGuardar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var fp = Ext.getCmp('fcargaPDF');		
			var forma = fp.getForm();
					
			var record =  grid.getStore().getAt(variable);
			var idFianza = record.get('CVEFIANZA');	
			var parametros = "cveFianza="+idFianza;
			
				forma.submit({
					url: '36autfianzas.ma.jsp?'+parametros,
					waitMsg: 'Enviando datos...',
					success: function(form, o) {	
						var jsonData = Ext.util.JSON.decode(o.response.responseText);
						if (jsonData.success == true) {
							if (jsonData.resultado == 'ArchivoGuardado') {
								Ext.MessageBox.alert('Mensaje','Se guard� exitosamente el archivo .PDF con las correcciones solicitadas ');
								reg = grid.getStore().getAt(variable);
							   reg.set('banderaPDF',true);
								
								Ext.getCmp('wincargaPDF').hide();
							} 
							else if (jsonData.resultado == 'Archivomayora5') {
								Ext.MessageBox.alert('Mensaje','Fallo la carga del archivo .PDF con las correcciones solicitadas, el tama�o excede a 5MB ');
								pdf_correcciones = false;
								Ext.getCmp('wincargaPDF').hide();
							}
							
						} //success true
					}
				,failure: NE.util.mostrarSubmitError
				});	
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var myValidFn = function(v) {
		var myRegex = /^.+\.([pP][dD][fF])$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivopdf 		: myValidFn,
		archivopdfText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): PDF.'
	});
	
//finge el guardado del archivo PDF binario (segun Don Gilo) 
	var Guardar = function(boton,evento) {
		
		var archivo = Ext.getCmp("archivo");
		if (Ext.isEmpty(archivo.getValue()) ) {
			archivo.markInvalid('El valor de la Ruta del Archivo es requerido.');	
			return;
		}
		
		Ext.Ajax.request({
			url : '36autfianzas.data.jsp',
			params : { 
				informacion:'Guardar'
			},
			callback: procesarSuccessFailureGuardar
		});	
	}
	
//-----------------------------STORE---------------------------------
	var consultaData = new Ext.data.JsonStore({
		id: 'consultaData',
		root : 'registros',
		url : '36autfianzas.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CVEFIANZA'},
			{name: 'CVEAFIANZA'},
			{name: 'ESTATUS'},
			{name: 'RAMOS'},
			{name: 'SUBRAMO'},
			{name: 'NOMAFINZA'},
			{name: 'NUMFIANZA'},
			{name: 'NUMCONTRATO'},
			{name: 'MONTOTOTAL', type: 'float'},
			{name: 'NOMRAMO'},
			{name: 'NOMSUBRAMO'},
			{name: 'TIPOFIANZA'},
			{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'FECVIGENCIA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'NOMFIADO'},
			{name: 'NOMDEST'},
			{name: 'NOMBENEF'},
			{name: 'NOMESTATUS'},
			{name: 'URLAFIANZA'},
			{name: 'PDF'},
			{name: 'ACCION'},
			{name: 'NEWOBSERVACIONES'},
			{name: 'AUTORIZADOR'},
			{name: 'cargarPDF'},
			{name: 'banderaPDF'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
		
	});

	var catalogoAfianza = new Ext.data.JsonStore({
		id: 'catalogoAfianzaStore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '36autfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoAfianza'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoFiado = new Ext.data.JsonStore({
		id: 'catalogoFiadoStore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '36autfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoFiado'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatusStore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '36autfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var storeRegAcuseData = new Ext.data.JsonStore({
		root : 'registros',
		url : '36autfianzas.data.jsp',
		baseParams: {
			informacion: 'acuseAutorizacion'
		},
		fields: [
			{name: 'CVEFIANZA'},
			{name: 'CVEAFIANZA'},
			{name: 'ESTATUS'},
			{name: 'RAMOS'},
			{name: 'SUBRAMO'},
			{name: 'NOMAFINZA'},
			{name: 'NUMFIANZA'},
			{name: 'NUMCONTRATO'},
			{name: 'MONTOTOTAL', type: 'float'},
			{name: 'NOMRAMO'},
			{name: 'NOMSUBRAMO'},
			{name: 'TIPOFIANZA'},
			{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'FECVIGENCIA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'NOMFIADO'},
			{name: 'NOMDEST'},
			{name: 'NOMBENEF'},
			{name: 'NOMESTATUS'},
			{name: 'URLAFIANZA'},
			{name: 'ACCION'},
			{name: 'NEWOBSERVACIONES'},
			{name: 'AUTORIZADOR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			//load: procesarPreAcuseData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					//procesarPreAcuseData(null, null, null);
				}
			}
		}
	});
	
	var preAcuseData = new Ext.data.ArrayStore({
		fields: [
			{name: 'CVEFIANZA'},
			{name: 'CVEAFIANZA'},
			{name: 'ESTATUS'},
			{name: 'RAMOS'},
			{name: 'SUBRAMO'},
			{name: 'NOMAFINZA'},
			{name: 'NUMFIANZA'},
			{name: 'NUMCONTRATO'},
			{name: 'MONTOTOTAL', type: 'float'},
			{name: 'NOMRAMO'},
			{name: 'NOMSUBRAMO'},
			{name: 'TIPOFIANZA'},
			{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'FECVIGENCIA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'NOMFIADO'},
			{name: 'NOMDEST'},
			{name: 'NOMBENEF'},
			{name: 'NOMESTATUS'},
			{name: 'URLAFIANZA'},
			{name: 'ACCION'},
			{name: 'NEWOBSERVACIONES'},
			{name: 'AUTORIZADOR'}
		]
	});
	
	var storeAccion = new Ext.data.ArrayStore({
		  id: 0,
		  fields: [
				'id',
				'accion'
		  ],
		  data: [[1, 'Autorizar'], [2, 'Rechazar'],[3,'Retornar a Revision']]
		});
	
	var storeAcuseData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	 var storeBusqAvanzFiado = new Ext.data.JsonStore({
		id: 'storeBusqAvanzFiado',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '36autfianzas.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	

//-----------------------------------------------------------------
	var elementosForm =
	[
		{
			xtype: 'combo',
			name: 'ic_afianzadora',
			id: 'ic_afianzadora1',
			fieldLabel: 'Afianzadora',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_afianzadora',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoAfianza
		},
		
		{
			xtype: 'combo',
			name: 'ic_fiado',
			id: 'ic_fiado1',
			fieldLabel: 'Fiado',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_fiado',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoFiado
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			//anchor: '-400',
			msgTarget: 'side',
			combineErrors: false,
			items: [
			{
				xtype: 'button',
				text: 'Busqueda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 			fpBusqAvanzada
							}).show();
					}
				}
			},
			{
					xtype: 'displayfield',
					value: ''
			}
			]
		},
		{
			xtype: 'combo',
			name: 'cc_estatus',
			id: 'cc_estatus1',
			fieldLabel: 'Estatus',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cc_estatus',
			emptyText: 'Seleccione...',
			autoSelect :true,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEstatus
		},
		{
			xtype: 'textfield',
			name: 'cg_numero_fianza',
			id: 'cg_numero_fianza1',
			fieldLabel: 'No. de Fianza',
			allowBlank: true,
			maxLength: 50,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'cg_numero_contrato',
			id: 'cg_numero_contrato1',
			fieldLabel: 'No. de Contrato',
			allowBlank: true,
			maxLength: 30,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud de Fianza',
			combineErrors: false,
			hidden: true,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_solicitud_ini',
					id: 'df_solicitud_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_solicitud_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_solicitud_fin',
					id: 'df_solicitud_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_solicitud_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Emisi�n Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_publicacion_ini',
					id: 'df_publicacion_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_publicacion_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_publicacion_fin',
					id: 'df_publicacion_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_publicacion_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Vencimiento Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_vencimiento_ini',
					id: 'df_vencimiento_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_vencimiento_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_vencimiento_fin',
					id: 'df_vencimiento_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_vencimiento_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		}
	];
	
	var elementsFormBusq = [
		{ 
			xtype:   'label',  
			html:		'<b>Campos de B�squeda:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'textfield',
			name: 'nombreFiado',
			id: 'nombreFiado1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcFiado',
			id: 'rfcFiado1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbFiados1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);
							//fpBusqAvanzada.el.mask('Buscando...','x-mask-loading');
							storeBusqAvanzFiado.load({
									params: Ext.apply(fpBusqAvanzada.getForm().getValues())
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{ 
			xtype:   'label',  
			html:		'<hr>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{ 
			xtype:   'label',  
			html:		'<b>Resultados:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'combo',
			name: 'cbFiados',
			id: 'cbFiados1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'ic_fiado',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzFiado
		}
	];
	
	var comboAccion = new Ext.form.ComboBox({
		id: 'comboAccion1',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		autoSelect : true,	 
		store: storeAccion,
		valueField: 'id',
		displayField: 'accion'
	});
	

//-------------------------------------------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'AUTORIZACION DE FIANZAS',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
                style: 'margin:0 auto;',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForm,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {

					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar' //Generar datos para la consulta
						})
					});	
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href='36autfianzas.jsp';
					//fp.getForm().reset();
				}
				
			}
		]
	});
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementsFormBusq,
		monitorValid: 	true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbFiado= Ext.getCmp("cbFiados1");//ic_fiado1
					var storeCmb = cmbFiado.getStore();
					var cb_fiado = Ext.getCmp('ic_fiado1');
					//var txtNombreProv = Ext.getCmp('ig_nombreProv1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbFiado.getValue())) {
						cmbFiado.markInvalid('Seleccione Fiado');
						return;
					}
					var record = cmbFiado.findRecord(cmbFiado.valueField, cmbFiado.getValue());
					
					record =  record ? record.get(cmbFiado.displayField) : cmbFiado.valueNotFoundText;
					
					cb_fiado.setValue(cmbFiado.getValue());
					//txtNombreProv.setValue(record);
					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
				
			}
		]
	});
	

	
	var grid = new Ext.grid.EditorGridPanel({
	id: 'gridFianzas',
	store: consultaData,
	margins: '20 0 0 0',
	clicksToEdit: 1,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [
		{
			header: 'Afianzadora',
			tooltip: 'Afianzadora',
			dataIndex: 'NOMAFINZA',
			sortable: true,
			width: 250,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header: 'No. Fianza',
			tooltip: 'Numero de Fianza',
			dataIndex: 'NUMFIANZA',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header: 'No. Contrato',
			tooltip: 'Documento Fuente',
			dataIndex: 'NUMCONTRATO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header : 'Monto',
			tooltip: 'Monto Total',
			dataIndex : 'MONTOTOTAL',
			sortable : true,
			width : 150,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Ramo',
			tooltip: 'Ramo',
			dataIndex : 'NOMRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Sub Ramo',
			tooltip: 'Sub Ramo',
			dataIndex : 'NOMSUBRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Tipo Fianza',
			tooltip: 'Tipo Fianza',
			dataIndex : 'TIPOFIANZA',
			width : 150,
			sortable : true
		},
		{
			header : 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex : 'FECAUTORIZA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Vigencia',
			tooltip: 'Fecha de Vigencia',
			dataIndex : 'FECVIGENCIA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Fiado',
			tooltip: 'Nombre Fiado',
			dataIndex : 'NOMFIADO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Destinatario',
			tooltip: 'Nombre Destinatario',
			dataIndex : 'NOMDEST',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
			}
		},
		{
			header : 'Beneficiario',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'NOMBENEF',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Estatus',
			tooltip: 'Estatus Fianza',
			dataIndex : 'NOMESTATUS',
			width : 150,
			sortable : true
		},
		{
			header : 'P�gina Web Afianzadora',
			tooltip: 'P�gina Afianzadora',
			dataIndex : 'URLAFIANZA',
			sortable : true,
			width : 80,
			align: 'center',
			renderer: function(valor,metadata, registro){
					return '<a href="'+valor+'" target="_blank">Ver</a>';
				}
		},
		{
			header : 'PDF Fianza',
			tooltip: 'PDF Fianza',
			dataIndex : 'PDF',
			sortable : true,
			width : 80,
			align: 'center',
			renderer:  function (valor, columna, registro){
					return '<img src="/nafin/00utils/gif/pdfdoc.png" alt="Ver.." style="border-style:none" />';
			}
		},
		{
			header : 'Acci�n',
			tooltip: 'Acci�n',
			dataIndex : 'ACCION',
			width : 150,
			sortable : true,
			align: 'center',
			editor: comboAccion,
			renderer:function(value,metadata,registro){
				var record = comboAccion.findRecord(comboAccion.valueField, value);
				return record ? record.get(comboAccion.displayField) : comboAccion.valueNotFoundText;
			}
		},
		{
			header : 'Observaciones',
			tooltip: 'Observaciones',
			dataIndex : 'NEWOBSERVACIONES',
			width : 150,
			sortable : true,
			align: 'center',
			editor: {
                xtype: 'textarea',
					 enableKeyEvents: true,
					 listeners:{
						keypress: function(field, event){							
							var val = field.getValue();
							if(event.keyCode !=8 && event.keyCode !=46){//ignorar backspace and delete
								if (val.length >= 255 ){
									event.stopEvent();
								}
							}
						}
					 }
            },
			renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
			}
		},
		{
			header : 'Adjuntar PDF',
			tooltip: 'Adjuntar PDF',
			dataIndex : 'cargarPDF',
			//sortable : true,
			width : 80,
			align: 'center'				
		},
		
		{
			header : ' ',
			tooltip: ' ',
			dataIndex : 'banderaPDF',
			hidden:true
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 885,
	title: '',
	frame: true,
	listeners: {
		cellclick :function(grid, rowIndex, colIndex, evento){
				var record = grid.getStore().getAt(rowIndex);  // Se Obtiene objeto Record del store
				var fieldName = grid.getColumnModel().getDataIndex(colIndex); // Se obtine nombre del Campo (Fields del Store)
				if(fieldName == 'PDF'){
					var idFianza = record.get('CVEFIANZA');
					var numFianza = record.get('NUMFIANZA');
					Ext.Ajax.request({
						url: '36autfianzasfiles.jsp',
						params: {
								cveFianza : idFianza,
								numeroFianza : numFianza,
								tipoArchivo : 'pdf'
								},
						callback: procesarSuccessFailureGenerarArchivo
					});
				}
				if(fieldName == 'cargarPDF') {
					var accion=record.get('ACCION');
					if(accion=='2') {
						//popup
						variable = rowIndex;
						var ventana = Ext.getCmp('wincargaPDF');
						if (ventana) {
							fpcargaPDF.getForm().reset();
							ventana.show(); 
						} 
						else 
						{
							new Ext.Window({
									title			: 'Agregar Imagen',
									id				: 'wincargaPDF',
									layout		: 'fit',
									heigth		:	400,										
									width			:	550,
									minWidth		:	400,
									minHeight	: 	200,
									//buttonAlign	: 'center',
									modal			: true,
									closeAction	: 'hide',
									items			: [fpcargaPDF]
								}).show();   
								
						}
					} //end--cargarPDF
				}
		},
		beforeedit : function(e){
				var record = e.record;
				var gridf = e.grid; 
				var fieldName = gridf.getColumnModel().getDataIndex(e.column); // Se obtine nombre del Campo (Fields del Store)
				//var accion = record.data['ACCION'];
				if(fieldName == 'NEWOBSERVACIONES'){
					var accion=record.get('ACCION');
					if(accion =='')return false;

					if(accion=='3'){//retorno
						return false;
					}
				}
		},
		afteredit : function(e){
				var record = e.record;
				var gridf = e.grid; 
				var fieldName = gridf.getColumnModel().getDataIndex(e.column); // Se obtine nombre del Campo (Fields del Store)
				if(fieldName == 'ACCION'){
					var accion=record.get(fieldName);
					
					if(accion=='1'){
						record.data['NEWOBSERVACIONES']='';
						record.commit();
					}
					if(accion=='2'){
						record.data['NEWOBSERVACIONES']='';
						record.data['cargarPDF']='<img src="/nafin/00utils/gif/file_add.png" alt="Cargar PDF" style="border-style:none" />';
						
						record.commit();
					}	
					if(accion=='3'){
						
						Ext.Ajax.request({
							url: '36autfianzas.data.jsp',
							params: {
								informacion: 	'observacionesRetornarRevision',
								rowIndex: 		e.row
							},
							callback: procesaRetornarRevision
						});
						
					}
				}
		}
		
	},
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Aceptar',
			handler: onAutorizar
			}
		]
	}
	});
	
	
	var gridAcuPreAcu = new Ext.grid.EditorGridPanel({
	id: 'gridPreAcu',
	store: preAcuseData,
	margins: '20 0 0 0',
	clicksToEdit: 1,
	columns: [
		{
			header: 'Afianzadora',
			tooltip: 'Afianzadora',
			dataIndex: 'NOMAFINZA',
			sortable: true,
			width: 250,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header: 'No. Fianza',
			tooltip: 'Numero de Fianza',
			dataIndex: 'NUMFIANZA',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header: 'No. Contrato',
			tooltip: 'Documento Fuente',
			dataIndex: 'NUMCONTRATO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header : 'Monto',
			tooltip: 'Monto Total',
			dataIndex : 'MONTOTOTAL',
			sortable : true,
			width : 150,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Ramo',
			tooltip: 'Ramo',
			dataIndex : 'NOMRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Sub Ramo',
			tooltip: 'Sub Ramo',
			dataIndex : 'NOMSUBRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Tipo Fianza',
			tooltip: 'Tipo Fianza',
			dataIndex : 'TIPOFIANZA',
			width : 150,
			sortable : true
		},
		{
			header : 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex : 'FECAUTORIZA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Vigencia',
			tooltip: 'Fecha de Vigencia',
			dataIndex : 'FECVIGENCIA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Fiado',
			tooltip: 'Nombre Fiado',
			dataIndex : 'NOMFIADO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Destinatario',
			tooltip: 'Nombre Destinatario',
			dataIndex : 'NOMDEST',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
			}
		},
		{
			header : 'Beneficiario',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'NOMBENEF',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Estatus',
			tooltip: 'Estatus Fianza',
			dataIndex : 'NOMESTATUS',
			width : 150,
			sortable : true,
			renderer:function(value,metadata,registro){
					var estatus;
					var accion = registro.get('ACCION');
					if(accion==1){
						registro.data['ESTATUS']='005';
						estatus = 'AUTORIZADA';
					}else if(accion==2){
						registro.data['ESTATUS']='003';
						estatus = 'RECHAZADA';
					}else if(accion==3){
						registro.data['ESTATUS']='002';
						estatus = 'POR REVISAR';
					}else{
						estatus = value;
					}
					return estatus;
			}
		},
		/*{
			header : 'Accion',
			tooltip: 'Accion',
			dataIndex : 'ACCION',
			width : 150,
			sortable : true,
			align: 'center',
			renderer:function(value,metadata,registro){
				record = storeAccion.getById(value);
				return record ? record.get(comboAccion.displayField) : comboAccion.valueNotFoundText;
			}
		},*/
		{
			header : 'Observaciones',
			tooltip: 'Observaciones',
			dataIndex : 'NEWOBSERVACIONES',
			width : 150,
			sortable : true,
			align: 'center',
			renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
			}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 885,
	title: '',
	frame: true,
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Continuar',
			id:'btnContinuar',
			handler: onPreacuse
			},
			{
			text: 'Generar PDF',
			id:'btnImprimirPDF',
			hidden:true,
			handler: onImprimirPDF
			},
			{
			text: 'Abrir PDF',
			id:'btnAbrirPDF',
			hidden:true
			},
			'-',
			{
			text: 'Cancelar',
			id: 'btnCancelar',
			handler: function(btn){
					document.location.href  = "36autfianzas.jsp";
				}
			}
		]
	}
	});
	
	
	var gridAcuse = new Ext.grid.GridPanel({
	id: 'gridAcu',
	store: storeAcuseData,
	margins: '20 0 0 0',
	hideHeaders : true,
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 150,
			sortable : true
		},
		{
			header : 'Informacion',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'informacion',
			width : 230,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 400,
	height: 220,
	title: 'ACUSE',
	frame: true
	});
/*-------------------------- Popup  carga PDF errores --------------------------*/
		var elementoscargaPDF = [
		{
			xtype: 		'panel',
			defaults: {	msgTarget: 'side',	anchor: '-20'	},
			bodyStyle: 	'padding: 12px; padding-right:6px;padding-left:6px;',
			layout: 'hbox',
			items: [
				
				{
					xtype: 'fileuploadfield',
					id: 'archivo',
					width: 400,	  
					emptyText: 'Ruta del Archivo',
					fieldLabel: 'Seleccione Imagen',
					name: 'archivoCesion',
					buttonText: '',
					buttonCfg: {  iconCls: 'upload-icon' },
				  anchor: '90%',
				  vtype: 'archivopdf'
				},	
				{	xtype: 'displayfield',	width: 20	},
				{	
					xtype: 'button',			
					text: 'Continuar',
					iconCls: 'icoContinuar',
					width:90,
					anchor: '90%',	
					formBind: true,
					handler: Guardar	
				}
				
			]
		},	
		{
			xtype: 		'panel',
			defaults: {	msgTarget: 'side',	anchor: '-20'	},
			bodyStyle: 	'padding: 12px; padding-right:6px;padding-left:6px;',
			//layout: 'hbox',
			items: [
				{	width:200,frame:false,	border:false,	html:'<div align="left"> <i>El archivo debe tener formato .pdf</i></div>'	},
				{	width:200,frame:false,	border:false,	html:'<div align="left"> <i>El tama�o debe ser menor a 5MB</i></div>'	}
			]
		}
		
	];
	
	var fpcargaPDF = new Ext.form.FormPanel({
		id				: 'fcargaPDF',
      frame			: true,
		width			: 550,
		autoHeight	: true,
		layout		: 'form',
		fileUpload: true,
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		items			: elementoscargaPDF,
		monitorValid: true		
	});
	
	/*-------------------------- Fin Popup carga PDF errores --------------------------*/
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		height: 'auto',
                style: 'margin:0 auto;',
		layoutConfig: {
			align:'center'
		},
		items: [
			{//FUNCION: Panel para mostrar avisos
				xtype: 'panel',
				name: 'mensajes',
				id: 'mensajes1',
				width: 600,
				frame: true,
				hidden: true
			},
			fp,
			NE.util.getEspaciador(20)
		]
	});

	fp.hide();
//--------------------
	catalogoAfianza.load();
	catalogoFiado.load();
	catalogoEstatus.load();
	
	
	Ext.Ajax.request({
		url: '36autfianzas.data.jsp',
		params: {
				informacion:'valoresIniciales'
				},
		callback: procesarSuccessValoresIni
	});



});