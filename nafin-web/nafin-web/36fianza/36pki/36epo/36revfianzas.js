

Ext.onReady(function() {
	var ifirmaMan = '';
	var iusuario = '';
	var todos_autorizan = 'N';

//---------------------------HANDLERS-------------------------------

	
	var procesarSuccessValoresIni = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			ifirmaMan = resp.firmaManc;
			iusuario = resp.usuario;
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesaRetornarAConcentrador = function(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Obtener registro
			var gridFianzas 						= Ext.getCmp('gridFianzas');
			var record 								= gridFianzas.getStore().getAt(resp.rowIndex);
			
			// Actualizar campo de observaciones
			record.data['NEWOBSERVACIONES']	= 'Fianza devuelta por el usuario '+iusuario+' '+resp.fechaYHora;
			record.data['AUTORIZADOR']			= '';
			record.commit();
			
		} else {
			
			// Revertir los cambios	
			var gridFianzas 						= Ext.getCmp('gridFianzas');
			var record 								= gridFianzas.getStore().getAt(opts.params.rowIndex); 
			record.reject();
			
			// Y mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
		
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				contenedorPrincipalCmp.add(grid);
				contenedorPrincipalCmp.doLayout();
			}
			
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	}
	
	var procesarSuccessFailureGenerarArchivo = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			tipoArchivo = Ext.util.JSON.decode(response.responseText).tipoArchivo;
			etapa = Ext.util.JSON.decode(response.responseText).etapa;
			var btnImpr = Ext.getCmp('btnImprimirPDF');
			
			if(tipoArchivo=="pdf" && !btnImpr.isVisible()){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			}else{
			
				btnImpr.setIconClass('');
				var btnAbrirPDF = Ext.getCmp('btnAbrirPDF');
				btnAbrirPDF.show();
				btnAbrirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnAbrirPDF.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	
	var onRevisar = function(btn) {
		var gridFianzas = Ext.getCmp('gridFianzas');
		var store = gridFianzas.getStore();
		var columnModelGrid = gridFianzas.getColumnModel();
		var noRegistros = false;
		var badRegistros = false;
		var siHayAccion = false;
		var numRegistro = -1;
		var registrosEnviar = [];

		store.each(function(record) {
			numRegistro = store.indexOf(record);
			if(record.data['ACCION']=='1' || record.data['ACCION']=='2' || record.data['ACCION']=='3'){
				siHayAccion = true;
				if(record.data['ACCION']=='2' && record.data['NEWOBSERVACIONES']==''){
					Ext.MessageBox.alert('Error Validacion','Favor de capturar los motivos por los que se rechaza la fianza',
							function(){
								gridFianzas.startEditing(numRegistro, columnModelGrid.findColumnIndex('NEWOBSERVACIONES'));
							}
					);
					badRegistros = true;
					return false;
				}
				if(record.data['ACCION']=='1' && record.data['AUTORIZADOR']=='' && ifirmaMan=='S'){
					Ext.MessageBox.alert('Error Validacion','Favor de elegir al Autorizador al que se enviar� la Fianza',
							function(){
								gridFianzas.startEditing(numRegistro, columnModelGrid.findColumnIndex('AUTORIZADOR'));
							}
					);
					badRegistros = true;
					return false;
				}
				
				registrosEnviar.push(record);
				noRegistros = true;
			}
		});
		
		if(noRegistros && !badRegistros){
			Ext.MessageBox.confirm('Revisi�n de Fianzas','�Esta seguro de la revisi�n a las fianzas seleccionadas?',function(msb){
				if(msb=='yes'){
					//gridFianzas.el.mask('Enviando...', 'x-mask-loading');						
					var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
					//registrosEnviar = Ext.encode(registrosEnviar);
					gridFianzas.stopEditing();
					preAcuseData.add(registrosEnviar);
					if (!gridAcuPreAcu.isVisible()) {
						contenedorPrincipalCmp.remove(fp);
						contenedorPrincipalCmp.remove(grid);
						contenedorPrincipalCmp.add(gridAcuPreAcu);
						contenedorPrincipalCmp.doLayout();
					}
				}//fin if(msb=='NO')
			});//fin Confirm
		}
		if(!siHayAccion){
			Ext.MessageBox.alert('Revisi�n de Fianzas','Es necesario seleccionar alguna Acci�n para continuar');
		}
	}//fin handler onEnvio
	
	var onPreacuse = function(btn){
		var gridPreAcuse = Ext.getCmp('gridPreAcu');
		var store = gridPreAcuse.getStore();
		
		
		var textoFirmar = "Nombre Afianzadora|No. de Fianza|Monto Total|Ramo|SubRamo|Tipo Fianza|Fecha Emision|"+
		"Fecha de Vigencia|Fiado|Destinatario|Beneficiario|Estatus|";
		var registrosEnviar = [];

		store.each(function(record) {
			//if(record.data['SELECCION']){
				textoFirmar += "\n"+
									record.data['NOMAFINZA'] + "|" +
									record.data['NUMFIANZA'] + "|" +
									Ext.util.Format.number(record.data['MONTOTOTAL'],'$0,0.00') + "|"+
									record.data['NOMRAMO'] + "|" +
									record.data['NOMSUBRAMO'] + "|" +
									record.data['TIPOFIANZA'] + "|" +
									Ext.util.Format.date(record.data['FECAUTORIZA'],'d/m/Y H:i:s') + "|" +
									Ext.util.Format.date(record.data['FECVIGENCIA'],'d/m/Y H:i:s') + "|" +
									record.data['NOMFIADO'] + "|" +
									record.data['NOMDEST'] + "|" +
									record.data['NOMBENEF'] + "|" +
									record.data['NOMESTATUS'] + "|";
				
				registrosEnviar.push(record.data);
			//}
		});//fin each(modificados)
		
		NE.util.obtenerPKCS7(confirmar, textoFirmar, registrosEnviar);

	}//end handler onPreAcuse
	
	var confirmar = function(pkcs7, textoFirmar, registrosEnviar) {
		var gridPreAcuse = Ext.getCmp('gridPreAcu');
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else{
			gridPreAcuse.stopEditing();
			registrosEnviar = Ext.encode(registrosEnviar);
			gridPreAcuse.el.mask('Procesando...', 'x-mask-loading');
			
			Ext.Ajax.request({
				url : '36revfianzasb.jsp',
				params :{
					registros : registrosEnviar,
					Pkcs7: pkcs7,
					TextoFirmado: textoFirmar,
					firmaMan : ifirmaMan,
					autorizador : todos_autorizan
				},
				callback: procesarSuccessFailureRevision
			});
		}
	}
	
	var procesarSuccessFailureRevision =  function(opts, success, response) {
		var gridPreAcu = Ext.getCmp('gridPreAcu');
		gridPreAcu.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridPreAcu.getStore().commitChanges();
			var ObjJsonb = Ext.util.JSON.decode(response.responseText);
			var msgAcuse = ObjJsonb.msgExito;
			var acuseGral = [
				['Destinatario',ObjJsonb.destinatario],
				['N�mero de Acuse', ObjJsonb.acuse],
				['N�mero de Recibo', ObjJsonb.recibo],
				['Fecha', ObjJsonb.fecha],
				['Hora', ObjJsonb.hora],
				['Usuario', ObjJsonb.usuario],
				['Fianzas Autorizadas', ObjJsonb.totalAut],
				['Fianzas Rechazadas', ObjJsonb.totalRech],
				['Fianzas Retornadas', ObjJsonb.totalRetor]
			];
			
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var gridAcu = Ext.getCmp('gridAcu');
			var storeAcuse = gridAcu.getStore();
			var btnCancel = Ext.getCmp('btnCancelar');
			var btnCont = Ext.getCmp('btnContinuar');
			var btnImpr = Ext.getCmp('btnImprimirPDF');
			
			//gridAcuPreAcu.el.mask('Cargando...', 'x-mask-loading');
			gridAcuPreAcu.reconfigure(storeRegAcuseData,gridAcuPreAcu.getColumnModel())
			storeRegAcuseData.load({
				params:{
					acuse : ObjJsonb.acuse
				}
			});
			
			storeAcuse.loadData(acuseGral);			
			contenedorPrincipalCmp.insert(0,gridAcu);
			contenedorPrincipalCmp.doLayout();
			
			btnCont.hide();
			btnImpr.show();
			btnCancel.setText('Salir');

			Ext.MessageBox.alert('Revisi�n de Fianzas','Revisi�n realizada exitosamente');
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var onImprimirPDF = function(btn){
		var gridAcuses = Ext.getCmp('gridAcu');
		var gridAcuseReg = Ext.getCmp('gridPreAcu');
		var storeAcu = gridAcuses.getStore();
		var storeAcuReg = gridAcuseReg.getStore();
		var arrRegistrosAcuse = [];
		var arrRegistros = [];
		
		
		btn.setIconClass('loading-indicator');
		btn.disable();
		
		storeAcu.each(function(record){
			arrRegistrosAcuse.push(record.data);
		});
		storeAcuReg.each(function(record){
			arrRegistros.push(record.data);
		});
		arrRegistrosAcuse = Ext.encode(arrRegistrosAcuse);
		arrRegistros = Ext.encode(arrRegistros);
		
		Ext.Ajax.request({
			url: '36revfianzasfiles.jsp',
			params: {
					registros : arrRegistros,
					registrosAcu : arrRegistrosAcuse,
					etapa : 'acuse',
					tipoArchivo : 'pdf'
					},
			callback: procesarSuccessFailureGenerarArchivo
		});
	}
	

//-----------------------------STORE---------------------------------
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '36revfianzas.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CVEFIANZA'},
			{name: 'CVEAFIANZA'},
			{name: 'ESTATUS'},
			{name: 'RAMOS'},
			{name: 'SUBRAMO'},
			{name: 'NOMAFINZA'},
			{name: 'NUMFIANZA'},
			{name: 'NUMCONTRATO'},
			{name: 'MONTOTOTAL', type: 'float'},
			{name: 'NOMRAMO'},
			{name: 'NOMSUBRAMO'},
			{name: 'TIPOFIANZA'},
			{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'FECVIGENCIA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'NOMFIADO'},
			{name: 'NOMDEST'},
			{name: 'NOMBENEF'},
			{name: 'NOMESTATUS'},
			{name: 'URLAFIANZA'},
			{name: 'PDF'},
			{name: 'ACCION'},
			{name: 'NEWOBSERVACIONES'},
			{name: 'AUTORIZADOR'},
			{name: 'NOMBREAUTORIZADOR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
		
	});

	var catalogoAfianza = new Ext.data.JsonStore({
		id: 'catalogoAfianzaStore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '36revfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoAfianza'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoFiado = new Ext.data.JsonStore({
		id: 'catalogoFiadoStore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '36revfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoFiado'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatusStore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '36revfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var storeRegAcuseData = new Ext.data.JsonStore({
		root : 'registros',
		url : '36revfianzas.data.jsp',
		baseParams: {
			informacion: 'acuseRevision'
		},
		fields: [
			{name: 'CVEFIANZA'},
			{name: 'CVEAFIANZA'},
			{name: 'ESTATUS'},
			{name: 'RAMOS'},
			{name: 'SUBRAMO'},
			{name: 'NOMAFINZA'},
			{name: 'NUMFIANZA'},
			{name: 'NUMCONTRATO'},
			{name: 'MONTOTOTAL', type: 'float'},
			{name: 'NOMRAMO'},
			{name: 'NOMSUBRAMO'},
			{name: 'TIPOFIANZA'},
			{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'FECVIGENCIA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'NOMFIADO'},
			{name: 'NOMDEST'},
			{name: 'NOMBENEF'},
			{name: 'NOMESTATUS'},
			{name: 'URLAFIANZA'},
			{name: 'ACCION'},
			{name: 'NEWOBSERVACIONES'},
			{name: 'AUTORIZADOR'},
			{name: 'NOMBREAUTORIZADOR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			//load: procesarPreAcuseData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					//procesarPreAcuseData(null, null, null);
				}
			}
		}
	});
	
	var preAcuseData = new Ext.data.ArrayStore({
		fields: [
			{name: 'CVEFIANZA'},
			{name: 'CVEAFIANZA'},
			{name: 'ESTATUS'},
			{name: 'RAMOS'},
			{name: 'SUBRAMO'},
			{name: 'NOMAFINZA'},
			{name: 'NUMFIANZA'},
			{name: 'NUMCONTRATO'},
			{name: 'MONTOTOTAL', type: 'float'},
			{name: 'NOMRAMO'},
			{name: 'NOMSUBRAMO'},
			{name: 'TIPOFIANZA'},
			{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'FECVIGENCIA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'NOMFIADO'},
			{name: 'NOMDEST'},
			{name: 'NOMBENEF'},
			{name: 'NOMESTATUS'},
			{name: 'URLAFIANZA'},
			{name: 'ACCION'},
			{name: 'NEWOBSERVACIONES'},
			{name: 'AUTORIZADOR'},
			{name: 'NOMBREAUTORIZADOR'}
		]
	});
	
	var storeAccion = new Ext.data.ArrayStore({
		  id: 0,
		  fields: [
				'id',
				'accion'
		  ],
		  data: [[1, 'Autorizar'], [2, 'Rechazar'],[3,'Retornar a Concentrador']]
		});
	
	var storeAcuseData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	 var storeBusqAvanzFiado = new Ext.data.JsonStore({
		id: 'storeBusqAvanzFiado',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '36revfianzas.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeAutorizador = new Ext.data.JsonStore({
		id: 0,
		root : 'registros',
		fields : ['USUARIO', 'NOMBREUSUARIO'],
		url : '36revfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoAutorizador'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

//-----------------------------------------------------------------
	var elementosForm =
	[
		{
			xtype: 'combo',
			name: 'ic_afianzadora',
			id: 'ic_afianzadora1',
			fieldLabel: 'Afianzadora',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_afianzadora',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoAfianza
		},
		
		{
			xtype: 'combo',
			name: 'ic_fiado',
			id: 'ic_fiado1',
			fieldLabel: 'Fiado',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_fiado',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoFiado
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			//anchor: '-400',
			msgTarget: 'side',
			combineErrors: false,
			items: [
			{
				xtype: 'button',
				text: 'Busqueda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 			fpBusqAvanzada
							}).show();
					}
				}
			},
			{
					xtype: 'displayfield',
					value: ''
			}
			]
		},
		{
			xtype: 'combo',
			name: 'cc_estatus',
			id: 'cc_estatus1',
			fieldLabel: 'Estatus',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cc_estatus',
			emptyText: 'Seleccione...',
			autoSelect :true,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEstatus
		},
		{
			xtype: 'textfield',
			name: 'cg_numero_fianza',
			id: 'cg_numero_fianza1',
			fieldLabel: 'No. de Fianza',
			allowBlank: true,
			maxLength: 50,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'cg_numero_contrato',
			id: 'cg_numero_contrato1',
			fieldLabel: 'No. de Contrato',
			allowBlank: true,
			maxLength: 30,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud de Fianza',
			combineErrors: false,
			hidden: true,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_solicitud_ini',
					id: 'df_solicitud_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_solicitud_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_solicitud_fin',
					id: 'df_solicitud_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_solicitud_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Emisi�n Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_publicacion_ini',
					id: 'df_publicacion_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_publicacion_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_publicacion_fin',
					id: 'df_publicacion_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_publicacion_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Vencimiento Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_vencimiento_ini',
					id: 'df_vencimiento_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_vencimiento_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_vencimiento_fin',
					id: 'df_vencimiento_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_vencimiento_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		}
		
	];
	
	var elementsFormBusq = [
		{ 
			xtype:   'label',  
			html:		'<b>Campos de B�squeda:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'textfield',
			name: 'nombreFiado',
			id: 'nombreFiado1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcFiado',
			id: 'rfcFiado1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items:[
				  {
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbFiados1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);
							//fpBusqAvanzada.el.mask('Buscando...','x-mask-loading');
							storeBusqAvanzFiado.load({
									params: Ext.apply(fpBusqAvanzada.getForm().getValues())
								});
						},
						style: {
							marginBottom:  '10px'
						}
					}
			 ] 
		},
		{ 
			xtype:   'label',  
			html:		'<hr>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{ 
			xtype:   'label',  
			html:		'<b>Resultados:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'combo',
			name: 'cbFiados',
			id: 'cbFiados1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'ic_fiado',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzFiado
		}
	];
	
	var comboAccion = new Ext.form.ComboBox({
		id: 'comboAccion1',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		autoSelect : true,
		store: storeAccion,
		valueField: 'id',
		displayField: 'accion'
	});
	
	var comboAutorizador = new Ext.form.ComboBox({
		id: 'comboAutorizador1',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		autoSelect : true,	 
		store: storeAutorizador,
		valueField: 'USUARIO',
		displayField: 'NOMBREUSUARIO'
	});
//-------------------------------------------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Revisi�n de Fianzas',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		//defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForm,
		//monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {

					if(!Ext.getCmp('forma').getForm().isValid()){
						return;
					}

					//Valido los campos de fechas
					if(Ext.getCmp('df_solicitud_ini').getValue() == ''  && Ext.getCmp('df_solicitud_fin').getValue() != ''){
						Ext.getCmp('df_solicitud_ini').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					if(Ext.getCmp('df_solicitud_ini').getValue() != ''  && Ext.getCmp('df_solicitud_fin').getValue() == ''){
						Ext.getCmp('df_solicitud_fin').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					if(Ext.getCmp('df_publicacion_ini').getValue() == ''  && Ext.getCmp('df_publicacion_fin').getValue() != ''){
						Ext.getCmp('df_publicacion_ini').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					if(Ext.getCmp('df_publicacion_ini').getValue() != ''  && Ext.getCmp('df_publicacion_fin').getValue() == ''){
						Ext.getCmp('df_publicacion_fin').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					if(Ext.getCmp('df_vencimiento_ini').getValue() == ''  && Ext.getCmp('df_vencimiento_fin').getValue() != ''){
						Ext.getCmp('df_vencimiento_ini').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}
					if(Ext.getCmp('df_vencimiento_ini').getValue() != ''  && Ext.getCmp('df_vencimiento_fin').getValue() == ''){
						Ext.getCmp('df_vencimiento_fin').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
						return;
					}

					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar' //Generar datos para la consulta
						})
					});

				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href='36revfianzas.jsp';
				}
			}
		]
	});

	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementsFormBusq,
		monitorValid: 	true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbFiado= Ext.getCmp("cbFiados1");//ic_fiado1
					var storeCmb = cmbFiado.getStore();
					var cb_fiado = Ext.getCmp('ic_fiado1');
					//var txtNombreProv = Ext.getCmp('ig_nombreProv1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbFiado.getValue())) {
						cmbFiado.markInvalid('Seleccione Fiado');
						return;
					}
					var record = cmbFiado.findRecord(cmbFiado.valueField, cmbFiado.getValue());
					
					record =  record ? record.get(cmbFiado.displayField) : cmbFiado.valueNotFoundText;
					
					cb_fiado.setValue(cmbFiado.getValue());
					//txtNombreProv.setValue(record);
					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
				
			}
		]
	});
	

	
	var grid = new Ext.grid.EditorGridPanel({
	id: 'gridFianzas',
	store: consultaData,
	style: 'margin:0 auto;',
	clicksToEdit: 1,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [
		{
			header: 'Afianzadora',
			tooltip: 'Afianzadora',
			dataIndex: 'NOMAFINZA',
			sortable: true,
			width: 250,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header: 'No. Fianza',
			tooltip: 'Numero de Fianza',
			dataIndex: 'NUMFIANZA',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header: 'No. Contrato',
			tooltip: 'Documento Fuente',
			dataIndex: 'NUMCONTRATO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header : 'Monto',
			tooltip: 'Monto Total',
			dataIndex : 'MONTOTOTAL',
			sortable : true,
			width : 100,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Ramo',
			tooltip: 'Ramo',
			dataIndex : 'NOMRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Sub Ramo',
			tooltip: 'Sub Ramo',
			dataIndex : 'NOMSUBRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Tipo Fianza',
			tooltip: 'Tipo Fianza',
			dataIndex : 'TIPOFIANZA',
			width : 150,
			sortable : true
		},
		{
			header : 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex : 'FECAUTORIZA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Vigencia',
			tooltip: 'Fecha de Vigencia',
			dataIndex : 'FECVIGENCIA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Fiado',
			tooltip: 'Nombre Fiado',
			dataIndex : 'NOMFIADO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Destinatario',
			tooltip: 'Nombre Destinatario',
			dataIndex : 'NOMDEST',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
			}
		},
		{
			header : 'Beneficiario',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'NOMBENEF',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Estatus',
			tooltip: 'Estatus Fianza',
			dataIndex : 'NOMESTATUS',
			width : 150,
			sortable : true
		},
		{
			header : 'P�gina Web Afianzadora',
			tooltip: 'P�gina Afianzadora',
			dataIndex : 'URLAFIANZA',
			sortable : true,
			width : 80,
			align: 'center',
			renderer: function(valor,metadata, registro){
					return '<a href="'+valor+'" target="_blank">Ver</a>';
				}
		},
		{
			header : 'PDF Fianza',
			tooltip: 'PDF Fianza',
			dataIndex : 'PDF',
			sortable : true,
			width : 80,
			align: 'center',
			renderer:  function (valor, columna, registro){
					return '<img src="/nafin/00utils/gif/pdfdoc.png" alt="Ver.." style="border-style:none" />';
			}
		},
		{
			header : 'Acci�n',
			tooltip: 'Acci�n',
			dataIndex : 'ACCION',
			width : 150,
			sortable : true,
			align: 'center',
			editor: comboAccion,
			renderer:function(value,metadata,registro){
				var record = comboAccion.findRecord(comboAccion.valueField, value);
				return record ? record.get(comboAccion.displayField) : comboAccion.valueNotFoundText;
			}
		},
		{
			header : 'Observaciones',
			tooltip: 'Observaciones',
			dataIndex : 'NEWOBSERVACIONES',
			width : 200,
			sortable : true,
			align: 'center',
			editor: {
                xtype: 'textarea',
					 enableKeyEvents: true,
					 listeners:{
						keypress: function(field, event){							
							var val = field.getValue();
							if(event.keyCode !=8 && event.keyCode !=46){//ignorar backspace and delete
								if (val.length >= 255 ){
									event.stopEvent();
								}
							}
						}
					 }
					 //allowBlank: false
            },
			renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
			}
		},
		{
			header : 'Autorizador',
			tooltip: 'Autorizador',
			dataIndex : 'AUTORIZADOR',
			width : 150,
			sortable : true,
			align: 'center',
			editor: comboAutorizador,
			renderer:function(value,metadata,registro){
				var record = comboAutorizador.findRecord(comboAutorizador.valueField, value);
				registro.data['NOMBREAUTORIZADOR'] = record ? record.get(comboAutorizador.displayField) : comboAutorizador.valueNotFoundText;
				return record ? record.get(comboAutorizador.displayField) : comboAutorizador.valueNotFoundText;
				
			}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 800,
	title: '',
	frame: true,
	listeners: {
		cellclick :function(grid, rowIndex, colIndex, evento){
				var record = grid.getStore().getAt(rowIndex);  // Se Obtiene objeto Record del store
				var fieldName = grid.getColumnModel().getDataIndex(colIndex); // Se obtine nombre del Campo (Fields del Store)
				if(fieldName == 'PDF'){
					var idFianza = record.get('CVEFIANZA');
					var numFianza = record.get('NUMFIANZA');
					Ext.Ajax.request({
						url: '36revfianzasfiles.jsp',
						params: {
								cveFianza : idFianza,
								numeroFianza : numFianza,
								tipoArchivo : 'pdf'
								},
						callback: procesarSuccessFailureGenerarArchivo
					});
				}
		},
		beforeedit : function(e){
				var record = e.record;
				var gridf = e.grid; 
				var fieldName = gridf.getColumnModel().getDataIndex(e.column); // Se obtine nombre del Campo (Fields del Store)
				//var accion = record.data['ACCION'];
				if(fieldName == 'NEWOBSERVACIONES'){
					var accion=record.get('ACCION');
					if(accion =='')return false;
					if(accion=='3'){//retorno
						return false;
					}
				}
				if(fieldName == 'AUTORIZADOR'){
					var accion=record.get('ACCION');
					if(accion =='')return false;
					
					if(accion=='1'){
						if(ifirmaMan=='N')return false;
					}
					if(accion=='2'){//rechazo
						return false;
					}
					if(accion=='3'){//retorno
						return false;
					}
				}
		},
		afteredit : function(e){
				var record = e.record;
				var gridf = e.grid; 
				var fieldName = gridf.getColumnModel().getDataIndex(e.column); // Se obtine nombre del Campo (Fields del Store)
				if(fieldName == 'ACCION'){
					var accion=record.get(fieldName);
					if(accion=='1'){
						record.data['NEWOBSERVACIONES']='';
						if(ifirmaMan=='N'){
							record.data['AUTORIZADOR']='';
						}
						record.commit();
					}
					if(accion=='2'){
						record.data['NEWOBSERVACIONES']='';
						record.data['AUTORIZADOR']='';
						record.commit();
					}	
					if(accion=='3'){
						
						Ext.Ajax.request({
							url: '36revfianzas.data.jsp',
							params: {
								informacion: 	'observacionesRetornoConcentrador',
								rowIndex: 		e.row
							},
							callback: procesaRetornarAConcentrador
						});
						
					}
				}
		}
		
	},
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Aceptar',
			handler: onRevisar
			}
		]
	}
	});
	
	
	var gridAcuPreAcu = new Ext.grid.EditorGridPanel({
	id: 'gridPreAcu',
	store: preAcuseData,
	style: 'margin:0 auto;',
	clicksToEdit: 1,
	columns: [
		{
			header: 'Afianzadora',
			tooltip: 'Afianzadora',
			dataIndex: 'NOMAFINZA',
			sortable: true,
			width: 250,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header: 'No. Fianza',
			tooltip: 'Numero de Fianza',
			dataIndex: 'NUMFIANZA',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header: 'No. Contrato',
			tooltip: 'Documento Fuente',
			dataIndex: 'NUMCONTRATO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header : 'Monto',
			tooltip: 'Monto Total',
			dataIndex : 'MONTOTOTAL',
			sortable : true,
			width : 150,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Ramo',
			tooltip: 'Ramo',
			dataIndex : 'NOMRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Sub Ramo',
			tooltip: 'Sub Ramo',
			dataIndex : 'NOMSUBRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Tipo Fianza',
			tooltip: 'Tipo Fianza',
			dataIndex : 'TIPOFIANZA',
			width : 150,
			sortable : true
		},
		{
			header : 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex : 'FECAUTORIZA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Vigencia',
			tooltip: 'Fecha de Vigencia',
			dataIndex : 'FECVIGENCIA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Fiado',
			tooltip: 'Nombre Fiado',
			dataIndex : 'NOMFIADO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Destinatario',
			tooltip: 'Nombre Destinatario',
			dataIndex : 'NOMDEST',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Beneficiario',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'NOMBENEF',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Estatus',
			tooltip: 'Estatus Fianza',
			dataIndex : 'NOMESTATUS',
			width : 150,
			sortable : true,
			renderer:function(value,metadata,registro){
					var estatus;
					var accion = registro.get('ACCION');
					if(accion==1){
						registro.data['ESTATUS']='005';
						estatus = 'AUTORIZADA';
						if(ifirmaMan=='S'){
							registro.data['ESTATUS']='004';
							estatus = 'EN PROCESO';
						}
					}else if(accion==2){
						registro.data['ESTATUS']='003';
						estatus = 'RECHAZADA';
					}else if(accion==3){
						registro.data['ESTATUS']='001';
						estatus = 'DEPOSITADA';
					}else{
						estatus = value;
					}
					return estatus;
			}
		},
		/*{
			header : 'Accion',
			tooltip: 'Accion',
			dataIndex : 'ACCION',
			width : 150,
			sortable : true,
			align: 'center',
			renderer:function(value,metadata,registro){
				record = storeAccion.getById(value);
				return record ? record.get(comboAccion.displayField) : comboAccion.valueNotFoundText;
			}
		},*/
		{
			header : 'Observaciones',
			tooltip: 'Observaciones',
			dataIndex : 'NEWOBSERVACIONES',
			width : 150,
			sortable : true,
			align: 'center',
			renderer:function(value,metadata,registro){
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				return (value=='')? 'No hay Observaciones':value;
			}
		},
		{
			header : 'Autorizador',
			tooltip: 'Autorizador',
			dataIndex : 'AUTORIZADOR', 
			width : 250,
			sortable : true,
			align: 'center',
			renderer:function(value,metadata,registro){
				if('T' == value) {
					todos_autorizan = 'S'
					storeAutorizador.filterBy(function(record,id){ return record.get('USUARIO') >= 1;  });  
					var usuarios = '';				 
					storeAutorizador.each(function(record){ usuarios += record.get(comboAutorizador.displayField) + ', '; },this);  
					storeAutorizador.clearFilter();  
					return usuarios;
					
				} else {
					record = storeAutorizador.getAt(storeAutorizador.findExact('USUARIO',value));
					registro.data['NOMBREAUTORIZADOR'] = record ? record.get(comboAutorizador.displayField) : 'N/A';
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br((record ? record.get(comboAutorizador.displayField) : 'N/A')) + '"';
					return record ? record.get(comboAutorizador.displayField) : 'N/A';
				}
			}
		}
	],
	
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 800,
	title: '',
	frame: true,
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Continuar',
			id:'btnContinuar',
			handler: onPreacuse
			},
			{
			text: 'Generar PDF',
			id:'btnImprimirPDF',
			hidden:true,
			handler: onImprimirPDF
			},
			{
			text: 'Abrir PDF',
			id:'btnAbrirPDF',
			hidden:true
			},
			'-',
			{
			text: 'Cancelar',
			id: 'btnCancelar',
			handler: function(btn){
					document.location.href  = "36revfianzas.jsp";
				}
			}
		]
	}
	});
	
	
	var gridAcuse = new Ext.grid.GridPanel({
	id: 'gridAcu',
	store: storeAcuseData,
	style: 'margin:0 auto;',
	hideHeaders : true,
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 150,
			sortable : true
		},
		{
			header : 'Informacion',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'informacion',
			width : 230,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 400,
	height: 220,
	title: 'ACUSE',
	frame: true
	});

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20)
		]
	});
	
//--------------------
	catalogoAfianza.load();
	catalogoFiado.load();
	catalogoEstatus.load();
	storeAutorizador.load();
	
	Ext.Ajax.request({
		url: '36revfianzas.data.jsp',
		params: {
				informacion:'valoresIniciales'
				},
		callback: procesarSuccessValoresIni
	});



});