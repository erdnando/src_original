<% String version = (String)session.getAttribute("version"); %>
<%if(version!=null){ %>
<!DOCTYPE html>
<%} %>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%if(version!=null){ %>
<%@ include file="/01principal/menu.jspf"%>
<%} %>
<%-- El jsp que contiene el diseño de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
<script type="text/javascript" src="36revfianzas.js?<%=session.getId()%>"></script>
<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="/36fianza/36pki/certificado.jspf"%>
</head>
<%if(version==null){ %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id='areaContenido' style="margin-left: 3px; margin-top: 3px;"></div>
<form id='formAux' name="formAux" target='_new'></form>
</body>
<%} %>
<%if(version!=null){ %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido" style="margin-left: 3px; margin-top: 3px;"></div>
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
<%} %>
</html>