

Ext.onReady(function() {
/*
if (!Ext.grid.GridView.prototype.templates) {
   Ext.grid.GridView.prototype.templates = {};
}
Ext.grid.GridView.prototype.templates.cell = new Ext.Template(
   '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}"
               style="{style}" tabIndex="0" {cellAttr}>',
   '<div class="x-grid3-cell-inner x-grid3-col-{id}" {attr}>{value}</div>',
   '</td>'
);
*/

//---------------------------HANDLERS-------------------------------

	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				contenedorPrincipalCmp.add(grid);
				contenedorPrincipalCmp.doLayout();
			}
			
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	}
	
	var procesarSuccessFailureGenerarArchivo = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			tipoArchivo = Ext.util.JSON.decode(response.responseText).tipoArchivo;
			etapa = Ext.util.JSON.decode(response.responseText).etapa;
			var btnImpr = Ext.getCmp('btnImprimirPDF');
			
			if(tipoArchivo=="pdf" && !btnImpr.isVisible()){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			}else{
			
				btnImpr.setIconClass('');
				var btnAbrirPDF = Ext.getCmp('btnAbrirPDF');
				btnAbrirPDF.show();
				btnAbrirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnAbrirPDF.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarPreAcuseData = function(store, arrRegistros, opts){
		var gridFianzas = Ext.getCmp('gridFianzas');
		if(gridFianzas)gridFianzas.el.unmask();
		if (gridAcuPreAcu.isVisible())gridAcuPreAcu.el.unmask();

		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!gridAcuPreAcu.isVisible()) {
				contenedorPrincipalCmp.remove(fp);
				contenedorPrincipalCmp.remove(grid);
				contenedorPrincipalCmp.add(gridAcuPreAcu);
				contenedorPrincipalCmp.doLayout();
			}
		}
	}
	
	
	var onEnvioBandeja = function(btn) {
		var gridFianzas = Ext.getCmp('gridFianzas');
		var store = gridFianzas.getStore();
		var modificados = store.getModifiedRecords();
		var noRegistros = false;
		var registrosEnviar = [];
		
		if (modificados.length > 0) {
			
			Ext.each(modificados, function(record) {
				if(record.data['SELECCION']){
					registrosEnviar.push(record.data);
					noRegistros = true;
				}
			});
			
			if(noRegistros){
				Ext.MessageBox.confirm('Env�o a Bandeja','�Est� seguro de enviar las fianzas seleccionadas a su bandeja de revisi�n?',function(msb){
					if(msb=='yes'){
												
						registrosEnviar = Ext.encode(registrosEnviar);
						gridFianzas.stopEditing();
						gridFianzas.el.mask('Enviando...', 'x-mask-loading');
						preAcuseData.load({
							params:{
								registros : registrosEnviar
							}
						});
					}//fin if(msb=='NO')
				});//fin Confirm
			}//fin if(noregistros)
		}
		if(!noRegistros){
			Ext.MessageBox.alert('Env�o a Bandeja','No hay registros seleccionados');
		}
	}//fin handler onEnvio
	
	var onPreacuse = function(btn){
		var gridPreAcuse = Ext.getCmp('gridPreAcu');
		var store = gridPreAcuse.getStore();
		
		
		var textoFirmar = "Nombre Afianzadora|No. de Fianza|Monto Total|Ramo|SubRamo|Tipo Fianza|Fecha Emision|"+
		"Fecha de Vigencia|Fiado|Destinatario|Beneficiario|Estatus|";
		var registrosEnviar = [];

		store.each(function(record) {
			//if(record.data['SELECCION']){
				textoFirmar += "\n"+
									record.data['NOMAFINZA'] + "|" +
									record.data['NUMFIANZA'] + "|" +
									Ext.util.Format.number(record.data['MONTOTOTAL'],'$0,0.00') + "|"+
									record.data['NOMRAMO'] + "|" +
									record.data['NOMSUBRAMO'] + "|" +
									record.data['TIPOFIANZA'] + "|" +
									Ext.util.Format.date(record.data['FECAUTORIZA'],'d/m/Y H:i:s') + "|" +
									Ext.util.Format.date(record.data['FECVIGENCIA'],'d/m/Y H:i:s') + "|" +
									record.data['NOMFIADO'] + "|" +
									record.data['NOMDEST'] + "|" +
									record.data['NOMBENEF'] + "|" +
									record.data['NOMESTATUS'] + "|";
				
				registrosEnviar.push(record.data);
			//}
		});//fin each(modificados)
		
		NE.util.obtenerPKCS7(confirmar, textoFirmar, registrosEnviar);

	}//end handler onPreAcuse
	
	var confirmar = function(pkcs7, textoFirmar, registrosEnviar) {
		var gridPreAcuse = Ext.getCmp('gridPreAcu');
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else{
			gridPreAcuse.stopEditing();
			registrosEnviar = Ext.encode(registrosEnviar);
			gridPreAcuse.el.mask('Procesando...', 'x-mask-loading');
			
			Ext.Ajax.request({
				url : '36concfianzasb.jsp',
				params :{
					registros : registrosEnviar,
					Pkcs7: pkcs7,
					TextoFirmado: textoFirmar
				},
				callback: procesarSuccessFailureEnvBandeja
			});
		
		}		
	}
	
	var procesarSuccessFailureEnvBandeja =  function(opts, success, response) {
		var gridPreAcu = Ext.getCmp('gridPreAcu');
		gridPreAcu.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridPreAcu.getStore().commitChanges();
			var ObjJsonb = Ext.util.JSON.decode(response.responseText);
			var msgAcuse = ObjJsonb.msgExito;
			var acuseGral = [
				['Destinatario',ObjJsonb.destinatario],
				['N�mero de Acuse', ObjJsonb.acuse],
				['N�mero de Recibo', ObjJsonb.recibo],
				['Fecha', ObjJsonb.fecha],
				['Hora', ObjJsonb.hora],
				['Usuario', ObjJsonb.usuario],
				['N�mero fianzas procesadas', ObjJsonb.totalFianzas]
			];
			
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var gridAcu = Ext.getCmp('gridAcu');
			var storeAcuse = gridAcu.getStore();
			var btnCancel = Ext.getCmp('btnCancelar');
			var btnCont = Ext.getCmp('btnContinuar');
			var btnImpr = Ext.getCmp('btnImprimirPDF');
			
			gridAcuPreAcu.el.mask('Cargando...', 'x-mask-loading');
			preAcuseData.load({
				params:{
					acuse : ObjJsonb.acuse
				}
			});
			
			storeAcuse.loadData(acuseGral);			
			contenedorPrincipalCmp.insert(0,gridAcu);
			contenedorPrincipalCmp.doLayout();
			
			btnCont.hide();
			btnImpr.show();
			btnCancel.setText('Salir');

			Ext.MessageBox.alert('Env�o a Bandeja','El env�o se ha realizado exitosamente');
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var onImprimirPDF = function(btn){
		var gridAcuses = Ext.getCmp('gridAcu');
		var gridAcuseReg = Ext.getCmp('gridPreAcu');
		var storeAcu = gridAcuses.getStore();
		var storeAcuReg = gridAcuseReg.getStore();
		var arrRegistrosAcuse = [];
		var arrRegistros = [];
		
		
		btn.setIconClass('loading-indicator');
		btn.disable();
		
		storeAcu.each(function(record){
			arrRegistrosAcuse.push(record.data);
		});
		storeAcuReg.each(function(record){
			arrRegistros.push(record.data);
		});
		arrRegistrosAcuse = Ext.encode(arrRegistrosAcuse);
		arrRegistros = Ext.encode(arrRegistros);
		
		Ext.Ajax.request({
			url: '36concfianzasfiles.jsp',
			params: {
					registros : arrRegistros,
					registrosAcu : arrRegistrosAcuse,
					etapa : 'acuse',
					tipoArchivo : 'pdf'
					},
			callback: procesarSuccessFailureGenerarArchivo
		});
	}
	

//-----------------------------STORE---------------------------------
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '36concfianzas.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CVEFIANZA'},
			{name: 'CVEAFIANZA'},
			{name: 'ESTATUS'},
			{name: 'RAMOS'},
			{name: 'SUBRAMO'},
			{name: 'NOMAFINZA'},
			{name: 'NUMFIANZA'},
			{name: 'NUMCONTRATO'},
			{name: 'MONTOTOTAL', type: 'float'},
			{name: 'NOMRAMO'},
			{name: 'NOMSUBRAMO'},
			{name: 'TIPOFIANZA'},
			{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'FECVIGENCIA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'NOMFIADO'},
			{name: 'NOMDEST'},
			{name: 'NOMBENEF'},
			{name: 'NOMESTATUS'},
			{name: 'URLAFIANZA'},
			{name: 'PDF'},
			{name: 'OBSERVACIONES'},
			{name: 'SELECCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
		
	});

	var catalogoAfianza = new Ext.data.JsonStore({
		id: 'catalogoAfianzaStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '36concfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoAfianza'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoFiado = new Ext.data.JsonStore({
		id: 'catalogoFiadoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '36concfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoFiado'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatusStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '36concfianzas.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		
	var preAcuseData = new Ext.data.JsonStore({
		root : 'registros',
		url : '36concfianzas.data.jsp',
		baseParams: {
			informacion: 'preAcuseyAcuse'
		},
		fields: [
			{name: 'CVEFIANZA'},
			{name: 'CVEAFIANZA'},
			{name: 'ESTATUS'},
			{name: 'RAMOS'},
			{name: 'SUBRAMO'},
			{name: 'NOMAFINZA'},
			{name: 'NUMFIANZA'},
			{name: 'NUMCONTRATO'},
			{name: 'MONTOTOTAL', type: 'float'},
			{name: 'NOMRAMO'},
			{name: 'NOMSUBRAMO'},
			{name: 'TIPOFIANZA'},
			{name: 'FECAUTORIZA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'FECVIGENCIA', type: 'date', dateFormat: 'd/m/Y H:i:s'},
			{name: 'NOMFIADO'},
			{name: 'NOMDEST'},
			{name: 'NOMBENEF'},
			{name: 'NOMESTATUS'},
			{name: 'URLAFIANZA'},
			{name: 'OBSERVACIONES'},
			{name: 'SELECCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarPreAcuseData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarPreAcuseData(null, null, null);
				}
			}
		}
	});
	
	var storeAcuseData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	 var storeBusqAvanzFiado = new Ext.data.JsonStore({
		id: 'storeBusqAvanzFiado',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '36concfianzas.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//-----------------------------------------------------------------
	var elementosForm =
	[
		{
			xtype: 'combo',
			name: 'ic_afianzadora',
			id: 'ic_afianzadora1',
			fieldLabel: 'Afianzadora',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_afianzadora',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoAfianza,
			tpl : NE.util.templateMensajeCargaCombo
		},
		
		{
			xtype: 'combo',
			name: 'ic_fiado',
			id: 'ic_fiado1',
			fieldLabel: 'Fiado',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_fiado',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoFiado,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			//anchor: '-400',
			msgTarget: 'side',
			combineErrors: false,
			items: [
			{
				xtype: 'button',
				text: 'Busqueda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 			fpBusqAvanzada
								
							}).show();
					}
				}
			},
			{
					xtype: 'displayfield',
					value: ''
			}
			]
		},
		{
			xtype: 'combo',
			name: 'cc_estatus',
			id: 'cc_estatus1',
			fieldLabel: 'Estatus',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cc_estatus',
			emptyText: 'Seleccione...',
			autoSelect :true,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEstatus,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'textfield',
			name: 'cg_numero_fianza',
			id: 'cg_numero_fianza1',
			fieldLabel: 'No. de Fianza',
			allowBlank: true,
			maxLength: 50,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'cg_numero_contrato',
			id: 'cg_numero_contrato1',
			fieldLabel: 'No. de Contrato',
			allowBlank: true,
			maxLength: 30,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud de Fianza',
			hidden: true,
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_solicitud_ini',
					id: 'df_solicitud_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_solicitud_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_solicitud_fin',
					id: 'df_solicitud_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_solicitud_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Emisi�n Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_publicacion_ini',
					id: 'df_publicacion_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_publicacion_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_publicacion_fin',
					id: 'df_publicacion_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_publicacion_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Vencimiento Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_vencimiento_ini',
					id: 'df_vencimiento_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_vencimiento_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_vencimiento_fin',
					id: 'df_vencimiento_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_vencimiento_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		}
		
	];
	
	var elementsFormBusq = [ 
		{ 
			xtype:   'label',  
			html:		'<b>Campos de B�squeda:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'textfield',
			name: 'nombreFiado',
			id: 'nombreFiado1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcFiado',
			id: 'rfcFiado1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 	'button',
						text: 	'Buscar',
						id: 		'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbFiados1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);
							//fpBusqAvanzada.el.mask('Buscando...','x-mask-loading');
							storeBusqAvanzFiado.load({
									params: Ext.apply(fpBusqAvanzada.getForm().getValues())
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{ 
			xtype:   'label',  
			html:		'<hr>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{ 
			xtype:   'label',  
			html:		'<b>Resultados:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'combo',
			name: 'cbFiados',
			id: 'cbFiados1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'ic_fiado',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzFiado
		}
	];

//-------------------------------------------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'CONCENTRADOR DE FIANZAS',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForm,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {

					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar' //Generar datos para la consulta
						})
					});	
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href='36concfianzas.jsp';
					//fp.getForm().reset();
				}
				
			}
		]
	});
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementsFormBusq,
		monitorValid: 	true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbFiado= Ext.getCmp("cbFiados1");//ic_fiado1
					var storeCmb = cmbFiado.getStore();
					var cb_fiado = Ext.getCmp('ic_fiado1');
					//var txtNombreProv = Ext.getCmp('ig_nombreProv1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbFiado.getValue())) {
						cmbFiado.markInvalid('Seleccione Fiado');
						return;
					}
					var record = cmbFiado.findRecord(cmbFiado.valueField, cmbFiado.getValue());
					
					record =  record ? record.get(cmbFiado.displayField) : cmbFiado.valueNotFoundText;
					
					cb_fiado.setValue(cmbFiado.getValue());
					//txtNombreProv.setValue(record);
					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
				
			}
		]
	});
	

	
	var grid = new Ext.grid.EditorGridPanel({
	id: 'gridFianzas',
	store: consultaData,
	bodyStyle: 'padding: 6px', // Para que el texto no quede pegado al borde de panel
	style: 'margin:0 auto;', // Para centrar el componente
	clicksToEdit: 1,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [
		{
			header: 'Afianzadora',
			tooltip: 'Afianzadora',
			dataIndex: 'NOMAFINZA',
			sortable: true,
			width: 250,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header: 'No. Fianza',
			tooltip: 'Numero de Fianza',
			dataIndex: 'NUMFIANZA',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header: 'No. Contrato',
			tooltip: 'Documento Fuente',
			dataIndex: 'NUMCONTRATO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header : 'Monto',
			tooltip: 'Monto Total',
			dataIndex : 'MONTOTOTAL',
			sortable : true,
			width : 150,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Ramo',
			tooltip: 'Ramo',
			dataIndex : 'NOMRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Sub Ramo',
			tooltip: 'Sub Ramo',
			dataIndex : 'NOMSUBRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Tipo Fianza',
			tooltip: 'Tipo Fianza',
			dataIndex : 'TIPOFIANZA',
			width : 150,
			sortable : true
		},
		{
			header : 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex : 'FECAUTORIZA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Vigencia',
			tooltip: 'Fecha de Vigencia',
			dataIndex : 'FECVIGENCIA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Fiado',
			tooltip: 'Nombre Fiado',
			dataIndex : 'NOMFIADO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Destinatario',
			tooltip: 'Nombre Destinatario',
			dataIndex : 'NOMDEST',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Beneficiario',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'NOMBENEF',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Estatus',
			tooltip: 'Estatus Fianza',
			dataIndex : 'NOMESTATUS',
			width : 150,
			sortable : true
		},
		{
			header : 'P�gina Web Afianzadora',
			tooltip: 'P�gina Afianzadora',
			dataIndex : 'URLAFIANZA',
			sortable : true,
			width : 80,
			align: 'center',
			renderer: function(valor,metadata, registro){
					return '<a href="'+valor+'" target="_blank">Ver</a>';
				}
		},
		{
			header : 'PDF Fianza',
			tooltip: 'PDF Fianza',
			dataIndex : 'PDF',
			sortable : true,
			width : 80,
			align: 'center',
			renderer:  function (valor, columna, registro){
					return '<img src="/nafin/00utils/gif/pdfdoc.png" alt="Ver.." style="border-style:none" />';
					//'<a href="javascript:generaPDF('+valor+')"><img src="/nafin/00utils/gif/pdfdoc.png" alt="Ver.." style="border-style:none" /></a>';
			}
		},
		{
			header : 'Observaciones',
			tooltip: 'Observaciones',
			dataIndex : 'OBSERVACIONES',
			width : 150,
			sortable : true,
			align: 'center',
			renderer:  function (valor, columna, registro){
					var texto = valor;
					if(texto==''){
						texto='No hay observaciones';
					}else{
						texto='Ver';
					}
					return texto;
			}
		},
		{
			xtype: 'checkcolumn',
			header : 'Seleccionar',
			dataIndex : 'SELECCION',
			width : 50,
			sortable : false
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 800,
	title: '',
	frame: true,
	listeners: {
		cellclick :function(grid, rowIndex, colIndex, evento){
				var record = grid.getStore().getAt(rowIndex);  // Se Obtiene objeto Record del store
				var fieldName = grid.getColumnModel().getDataIndex(colIndex); // Se obtine nombre del Campo (Fields del Store)
				if(fieldName == 'PDF'){
					var idFianza = record.get('CVEFIANZA');
					var numFianza = record.get('NUMFIANZA');
					Ext.Ajax.request({
						url: '36concfianzasfiles.jsp',
						params: {
								cveFianza : idFianza,
								numeroFianza : numFianza,
								tipoArchivo : 'pdf'
								},
						callback: procesarSuccessFailureGenerarArchivo
					});
				}
				if(fieldName == 'OBSERVACIONES'){
					var txtObs = record.get('OBSERVACIONES');
					//txtObs = txtObs==''?'No hay observaciones':txtObs;
					if(txtObs!=''){
						var ventana = Ext.getCmp('winObservaciones');
						if (ventana) {
							var objObserv = ventana.findById('iwObserv');
							objObserv.setValue(txtObs);
							ventana.show();
						} else {
							new Ext.Window({
										layout: 'fit',
										width: 250,
										height: 100,
										id: 'winObservaciones',
										title:'Observaciones',
										closeAction: 'hide',
										draggable :true,
										resizable : false,
										items: [
											{
											xtype:'textarea',
											name:'wObserv',
											id:'iwObserv',
											emptyText: 'No hay Observaciones',
											value:txtObs,
											readOnly : true
											}
										]
									}).show();
						}
					}
				}
		}
	},
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Enviar a mi Bandeja',
			handler: onEnvioBandeja
			}
		]
	}
	});
	
	
	var gridAcuPreAcu = new Ext.grid.EditorGridPanel({
	id: 'gridPreAcu',
	store: preAcuseData,
	bodyStyle: 'padding: 6px', // Para que el texto no quede pegado al borde de panel
	style: 'margin:0 auto;', // Para centrar el componente
	clicksToEdit: 1,
	columns: [
		{
			header: 'Afianzadora',
			tooltip: 'Afianzadora',
			dataIndex: 'NOMAFINZA',
			sortable: true,
			width: 250,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header: 'No. Fianza',
			tooltip: 'Numero de Fianza',
			dataIndex: 'NUMFIANZA',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header: 'No. Contrato',
			tooltip: 'Documento Fuente',
			dataIndex: 'NUMCONTRATO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{
			header : 'Monto',
			tooltip: 'Monto Total',
			dataIndex : 'MONTOTOTAL',
			sortable : true,
			width : 150,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Ramo',
			tooltip: 'Ramo',
			dataIndex : 'NOMRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Sub Ramo',
			tooltip: 'Sub Ramo',
			dataIndex : 'NOMSUBRAMO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Tipo Fianza',
			tooltip: 'Tipo Fianza',
			dataIndex : 'TIPOFIANZA',
			width : 150,
			sortable : true
		},
		{
			header : 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex : 'FECAUTORIZA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Vigencia',
			tooltip: 'Fecha de Vigencia',
			dataIndex : 'FECVIGENCIA',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.dateRenderer('d/m/Y H:i:s')
		},
		{
			header : 'Fiado',
			tooltip: 'Nombre Fiado',
			dataIndex : 'NOMFIADO',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Destinatario',
			tooltip: 'Nombre Destinatario',
			dataIndex : 'NOMDEST',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Beneficiario',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'NOMBENEF',
			width : 150,
			sortable : true,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header : 'Estatus',
			tooltip: 'Estatus Fianza',
			dataIndex : 'NOMESTATUS',
			width : 150,
			sortable : true,
			renderer: function(a,b,c){
						return 'POR REVISAR';
				}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 800,
	title: '',
	frame: true,
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Continuar',
			id:'btnContinuar',
			handler: onPreacuse
			},
			{
			text: 'Generar PDF',
			id:'btnImprimirPDF',
			hidden:true,
			handler: onImprimirPDF
			},
			{
			text: 'Abrir PDF',
			id:'btnAbrirPDF',
			hidden:true
			},
			'-',
			{
			text: 'Cancelar',
			id: 'btnCancelar',
			handler: function(btn){
					document.location.href  = "36concfianzas.jsp";
				}
			}
		]
	}
	});
	
	
	var gridAcuse = new Ext.grid.GridPanel({
	id: 'gridAcu',
	store: storeAcuseData,
	bodyStyle: 'padding: 6px', // Para que el texto no quede pegado al borde de panel
	style: 'margin:0 auto;', // Para centrar el componente
	hideHeaders : true,
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 150,
			sortable : true
		},
		{
			header : 'Informacion',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'informacion',
			width : 230,
			sortable : true
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 400,
	height: 210,
	title: 'ACUSE',
	frame: true
	});

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		bodyStyle: 'padding: 6px', // Para que el texto no quede pegado al borde de panel
		style: 'margin:0 auto;', // Para centrar el componente
		width: 890,
		height: 'auto',
		layoutConfig: {
			align:'center'
		},
		items: [
			fp,
			NE.util.getEspaciador(20)
		]
	});
	
//--------------------
	catalogoAfianza.load();
	catalogoFiado.load();
	catalogoEstatus.load();



});