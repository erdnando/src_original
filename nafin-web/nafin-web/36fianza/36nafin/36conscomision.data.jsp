<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalgoAfianzadora,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fianza.*,
		net.sf.json.*,
		com.netro.pdf.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";

if (informacion.equals("valoresIniciales")) {
	JSONObject jsonObj = new JSONObject();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("nomAfianza", strNombre);
	jsonObj.put("strTipoAfiliado", iTipoAfiliado);
	jsonObj.put("strPerfil", strPerfil);
	

	infoRegresar= jsonObj.toString();
}else if (informacion.equals("catalogoAfianza")) {
	CatalgoAfianzadora cat = new CatalgoAfianzadora();
	cat.setCampoClave("ic_afianzadora");
	cat.setCampoDescripcion("CG_RAZON_SOCIAL");
	//cat.setClaveEpo(iNoCliente);
	cat.setOrden("CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();
}else if (informacion.equals("GenerarConsulta")) {
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	List lstFianzasInfo = new ArrayList();
	JSONArray jsObjArray = new JSONArray();
	
	String cveFianza = request.getParameter("ic_afianzadora")==null?"":request.getParameter("ic_afianzadora");
	String mesIni = request.getParameter("cboMes")==null?"":request.getParameter("cboMes");
	String mesFin = request.getParameter("cboMes2")==null?"":request.getParameter("cboMes2");
	String anio = request.getParameter("cboAnio")==null?"":request.getParameter("cboAnio");
	
	if("A".equals(iTipoAfiliado))	cveFianza = iNoCliente;
	
	lstFianzasInfo = fianzaElectronica.getConsComisiones(cveFianza, mesIni, mesFin, anio);
	jsObjArray = JSONArray.fromObject(lstFianzasInfo);
	
	infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";
}else if (informacion.equals("ResumenTotales")) {

	try{
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
		List lstTotales = new ArrayList();
		JSONArray jsObjArray = new JSONArray();

		List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
		Iterator itReg = arrRegistrosEnviados.iterator();
		BigDecimal mComision = new BigDecimal("0.0");
		BigDecimal mIva = new BigDecimal("0.0");
		BigDecimal mTotal = new BigDecimal("0.0");
		String cantidad = "";
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();

			cantidad = registro.getString("MONTOCOMISION");
			if(cantidad == null || cantidad.equals("") || cantidad.equals("null")){
				mComision = mComision.add(new BigDecimal("0"));
			} else{
				mComision = mComision.add(new BigDecimal(cantidad));
			}

			cantidad = registro.getString("IVA");
			if(cantidad == null || cantidad.equals("") || cantidad.equals("null")){
				mIva = mIva.add(new BigDecimal("0"));
			} else{
				mIva = mIva.add(new BigDecimal(cantidad));
			}
			
			cantidad = registro.getString("MONTOTOTAL");
			if(cantidad == null || cantidad.equals("") || cantidad.equals("null")){
				mTotal = mTotal.add(new BigDecimal("0"));
			} else{
				mTotal = mTotal.add(new BigDecimal(cantidad));
			}

		}

		HashMap map = new HashMap();
		map.put("TOTALCOMISION", mComision.toPlainString());
		map.put("TOTALIVA", mIva.toPlainString());
		map.put("TOTAL", mTotal.toPlainString());
		lstTotales.add(map);
		jsObjArray = JSONArray.fromObject(lstTotales);

		infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";
	} catch(Throwable e){
		infoRegresar = "({\"success\": false})";
		throw new AppException("Error al obtener los totales. " + e);
	}

}else if (informacion.equals("Archivos")) {
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String nomAfianzadora = (request.getParameter("nomAfianzadora") == null)?"":request.getParameter("nomAfianzadora");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	JSONArray jsObjArray = new JSONArray();
	JSONObject jsonObj = new JSONObject();
	BigDecimal mComision = new BigDecimal("0.0");
	BigDecimal mIva = new BigDecimal("0.0");
	BigDecimal mTotal = new BigDecimal("0.0");
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	if("csv".equals(operacion)){
		List lstTotales = new ArrayList();
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo="";
		StringBuffer contenidoArchivo= new StringBuffer(2000);
			
		if(!"".equals(nomAfianzadora))
			contenidoArchivo.append(nomAfianzadora+"\n");
		contenidoArchivo.append("Mes, Año, Comisiones Generadas, %IVA, Monto IVA, Total\n");
		while (itReg.hasNext()) {
				JSONObject registro = (JSONObject)itReg.next();			

				String mes = registro.getString("MES");
				String anio = registro.getString("ANIO");
				String comisiones = registro.getString("MONTOCOMISION");
				String iva = registro.getString("IVA");
				String montoIva = registro.getString("MONTOIVA");
				String total = registro.getString("MONTOTOTAL");

			if(comisiones == null || comisiones.equals("") || comisiones.equals("null")){ comisiones = "0"; }
			if(iva        == null || iva.equals("")        || iva.equals("null")       ){ iva        = "0"; }
			if(montoIva   == null || montoIva.equals("")   || montoIva.equals("null")  ){ montoIva   = "0"; }
			if(total      == null || total.equals("")      || total.equals("null")     ){ total      = "0"; }

				contenidoArchivo.append(meses[Integer.parseInt(mes)-1]+","+anio+","+"$"+Comunes.formatoDecimal(comisiones, 2, false)+","+"%"+Comunes.formatoDecimal(iva,2,false)+","+"$"+Comunes.formatoDecimal(montoIva,2,false)+","+"$"+Comunes.formatoDecimal(total,2,false)+"\n");
				
				mComision = mComision.add(new BigDecimal(comisiones));
				mIva = mIva.add(new BigDecimal(iva));
				mTotal = mTotal.add(new BigDecimal(total));
		}
		
		contenidoArchivo.append("Gran Total:,,"+"$"+Comunes.formatoDecimal(mComision.toPlainString(),2,false)+",,,"+"$"+Comunes.formatoDecimal(mTotal.toPlainString(),2,false)+"\n");
		
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		} else {
			nombreArchivo = archivo.nombre;	
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		infoRegresar = jsonObj.toString();
	
	}else if("pdf".equals(operacion)){
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = archivo.nombreArchivo()+".pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
												"",
												(String)session.getAttribute("sesExterno"),
												(String) session.getAttribute("strNombre"),
												(String) session.getAttribute("strNombreUsuario"),
												(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
		pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("COMISIONES PARA FIANZAS ELECTRONICAS","formas",ComunesPDF.CENTER);
		pdfDoc.setTable(6,70);
		if(!"".equals(nomAfianzadora))
			pdfDoc.setCell(nomAfianzadora,"celda01",ComunesPDF.CENTER,6);
		pdfDoc.setCell("Mes","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Año","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Comisiones Generadas","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("% IVA","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Monto IVA","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Total","celda01",ComunesPDF.CENTER,1);
		
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();						
			String mes = registro.getString("MES");
			String anio = registro.getString("ANIO");
			String comisiones = registro.getString("MONTOCOMISION");
			String iva = registro.getString("IVA");
			String montoIva = registro.getString("MONTOIVA");
			String total = registro.getString("MONTOTOTAL");

			if(comisiones == null || comisiones.equals("") || comisiones.equals("null")){ comisiones = "0"; }
			if(iva        == null || iva.equals("")        || iva.equals("null")       ){ iva        = "0"; }
			if(montoIva   == null || montoIva.equals("")   || montoIva.equals("null")  ){ montoIva   = "0"; }
			if(total      == null || total.equals("")      || total.equals("null")     ){ total      = "0"; }

			pdfDoc.setCell(meses[Integer.parseInt(mes)-1],"formas",ComunesPDF.CENTER,1);
			pdfDoc.setCell(anio,"formas",ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(comisiones,2,true),"formas",ComunesPDF.RIGHT,1);

			pdfDoc.setCell(Comunes.formatoDecimal(iva,2,true) + "%","formas",ComunesPDF.CENTER,1);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoIva,2,true),"formas",ComunesPDF.RIGHT,1);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(total,2,true),"formas",ComunesPDF.RIGHT,1);
			
			mComision = mComision.add(new BigDecimal(comisiones));
			mIva = mIva.add(new BigDecimal(montoIva));
			mTotal = mTotal.add(new BigDecimal(total));
		} //fin del for
		
		pdfDoc.setCell("Total","celda01",ComunesPDF.RIGHT,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(mComision.toPlainString(),2,true),"formas",ComunesPDF.RIGHT,3);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(mTotal.toPlainString(),2,true),"formas",ComunesPDF.RIGHT,1);
		
		pdfDoc.addTable();
		pdfDoc.endDocument();
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	}

}


System.out.println("infoRegresar = " + infoRegresar);

%>
<%=infoRegresar%>