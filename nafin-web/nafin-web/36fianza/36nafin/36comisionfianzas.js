Ext.onReady(function() {
var tipoComison = '';
//HANDLERS----------------------------------------------------------------------

var procesarSuccessValoresIni = function(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		objResponse = tipoArchivo = Ext.util.JSON.decode(response.responseText);
		objMonto = Ext.getCmp('montoComision1');
		objIva = Ext.getCmp('ivaComision1');
		
		objMonto.setValue(objResponse.monto);
		objIva.setValue(objResponse.iva);
		tipoComison = objResponse.tipoComision;
		var objMsg = Ext.getCmp('mensajes1');
		if(tipoComison=='F'){
			objMsg.body.update('Parametrizaci�n activa actualmente "Comisi�n Fija"');
			objMsg.show();
		}else if(tipoComison=='R'){
			objMsg.body.update('Parametrizaci�n activa actualmente "Comisi�n por Rangos"');
			objMsg.show();
		}
		
		
		
	}
}

var procesarSuccessSaveComisionFija = function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		objResponse = tipoArchivo = Ext.util.JSON.decode(response.responseText);
		Ext.Ajax.request({
			url: '36comisionfianzas.data.jsp',
			params: {
					informacion:'valoresIniciales'
					},
			callback: procesarSuccessValoresIni
		});
		Ext.MessageBox.alert('Guardado Comisi�n Fija',objResponse.msg);
		
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

var procesarSuccessSaveRangos = function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		objResponse = tipoArchivo = Ext.util.JSON.decode(response.responseText);
		Ext.Ajax.request({
			url: '36comisionfianzas.data.jsp',
			params: {
					informacion:'valoresIniciales'
					},
			callback: procesarSuccessValoresIni
		});
		Ext.MessageBox.alert('Guardado Comisi�n por Rangos',objResponse.msg);
		
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}
 
var procesarConsultaData = function(store, arrRegistros, opts) {
	
	var el = gridRangos.getGridEl();
		if(gridRangos.getStore().getTotalCount() > 0) {
			
			var recordIva = gridRangos.getStore().getAt(0);
			Ext.getCmp('ivaComisionR1').setValue(recordIva.data['IVA']);
			el.unmask();
		}else {
			Ext.getCmp('ivaComisionR1').setValue('');
			var Comision = Ext.data.Record.create([
				{name:'CVECOMISION'},
				{name:'MONTOINI', type: 'float'},
				{name:'MONTOFIN', type: 'float'},
				{name:'PORCENTAJE', type: 'float'},
				{name:'IVA', type: 'float'}
			]);
			
			gridRangos.getStore().add(new Comision({
				 MONTOINI:1.00
			}));
			//el.mask('No se encontr� ning�n registro', 'x-mask');
		}
		
	//}
}

var onAgregarRango = function(){
	
	
	var Comision = Ext.data.Record.create([
		{name:'CVECOMISION'},
		{name:'MONTOINI', type: 'float'},
		{name:'MONTOFIN', type: 'float'},
		{name:'PORCENTAJE', type: 'float'},
		{name:'IVA', type: 'float'}
	]);
		
	gridRangos.getStore().add(new Comision());
}

var onGuardarRangos = function(){
	
	var storeRango = gridRangos.getStore();
	var columnModelGrid = gridRangos.getColumnModel();
	var registrosEnviar = [];
	var correcto = true;
	var valIVA = Ext.getCmp('ivaComisionR1').getValue();
	storeRango.commitChanges();
	
	storeRango.each(function(record) {
		numRegistro = storeRango.indexOf(record);
		var montoI = (record.data['MONTOINI']==undefined)?'':record.data['MONTOINI'];
		var montoF = (record.data['MONTOFIN']==undefined)?'':record.data['MONTOFIN'];
		var porcent = (record.data['PORCENTAJE']==undefined)?'':record.data['PORCENTAJE'];

		//validacion de campos vacios
		if(montoI=='' || montoF=='' || porcent==''){
			var txtColumn = (montoI=='')?'MONTOINI':(montoF=='')?'MONTOFIN':(porcent=='')?'PORCENTAJE':'';
			Ext.MessageBox.alert('Error Validaci�n','Faltan Datos!',
				function(){
					gridRangos.startEditing(numRegistro, columnModelGrid.findColumnIndex(txtColumn));
				}
			);
			correcto = false;
			return false;
		} else if(numRegistro==0 && montoI!=1.00){//validacion de rangos
			Ext.MessageBox.alert('Error Rangos','El monto incial del primer rango debe ser $1.00',
				function(){
					gridRangos.startEditing(numRegistro, columnModelGrid.findColumnIndex('MONTOINI'));
				}
			);
			correcto = false;
			return false;
		} else if(montoI >= montoF){
			Ext.MessageBox.alert('Error Rangos','El Monto del Movimiento DESDE $'+montoI+' debe ser menor al Monto del Movimiento HASTA $'+montoF,
				function(){
					gridRangos.startEditing(numRegistro, columnModelGrid.findColumnIndex('MONTOINI'));
				}
			);
			correcto = false;
			return false;
		} else if(numRegistro>0){//validacion de rangos consecutivos
			var oldRecord = storeRango.getAt(numRegistro-1);

			if( (oldRecord.data['MONTOFIN']+.01)!=montoI ){
				Ext.MessageBox.alert('Error en Rangos','Verifique que los Montos sean consecutivos al alza',
					function(){
						gridRangos.startEditing(numRegistro, columnModelGrid.findColumnIndex('MONTOINI'));
					}
				);
				correcto = false;
				return false;
			}else if( porcent >= oldRecord.data['PORCENTAJE']){
				Ext.MessageBox.alert('Error en Rangos','Verifique que los Porcentajes sean a la baja',
					function(){
						gridRangos.startEditing(numRegistro, columnModelGrid.findColumnIndex('PORCENTAJE'));
					}
				);
				correcto = false;
				return false;
			}
		}

		registrosEnviar.push(record.data);
		
	});
	
	if(!correcto)return;
	
	if(valIVA==''){
		Ext.MessageBox.alert('Error en IVA','Es necesario capturar el IVA');
		correcto = false;
	}
	
	if(!Ext.getCmp('ivaComisionR1').isValid()){
		Ext.MessageBox.alert('Error en IVA','El valor del IVA es incorrecto');
		correcto = false;
	}
	
	
		
	if(correcto){
		registrosEnviar = Ext.encode(registrosEnviar);
		if(tipoComison=='F'){
			Ext.MessageBox.confirm('Mensaje','<p align="center">Existe una parametrizaci�n para Comisi�n Fija previamente capturada,'+
						'dicha parametrizaci�n se eliminar� al guardar la comisi�n por Rangos.'+
						'<br>�Est� seguro que desea continuar?</p>',function(msb){
				if(msb=='yes'){
					Ext.Ajax.request({
						url : '36comisionfianzas.data.jsp',
						params :{
							registros : registrosEnviar,
							valIva: valIVA,
							informacion:'GuardarComisionRangos'
						},
						callback: procesarSuccessSaveRangos
					});
				}
			});
		}else{
			Ext.Ajax.request({
				url : '36comisionfianzas.data.jsp',
				params :{
					registros : registrosEnviar,
					valIva: valIVA,
					informacion:'GuardarComisionRangos'
				},
				callback: procesarSuccessSaveRangos
			});
		}
	}
	

}

var onCancelRangos = function(){
	Ext.MessageBox.confirm('Mensaje','�Est� seguro de cancelar la operaci�n?',function(msb){
		if(msb=='yes'){
			gridRangos.getStore().load();
		}
	})
	
}
//STORES------------------------------------------------------------------------

var storeConsRangos = new Ext.data.JsonStore({
		root : 'registros',
		url : '36comisionfianzas.data.jsp',
		baseParams: {
			informacion: 'ConsultarRangos'
		},
		fields: [
			{name:'CVECOMISION'},
			{name:'MONTOINI', type: 'float'},
			{name:'MONTOFIN', type: 'float'},
			{name:'PORCENTAJE', type: 'float'},
			{name:'IVA', type: 'float'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

//COMPONENTES-------------------------------------------------------------------

	var gridRangos = new Ext.grid.EditorGridPanel({
		id: 'gridFianzas',
		style: 'margin:0 auto;',
		title:'Comisi�n por Monto del Movimiento',
		store: storeConsRangos,
		clicksToEdit: 1,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
				'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
				'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
				'</td>'
				)
			}
		},
		columns: [{
			header: 'DESDE',
			tooltip: 'Monto Inicial',
			dataIndex: 'MONTOINI',
			sortable: true,
			width: 150,
			align: 'right',
			editor: {
				xtype: 'numberfield',
				maxValue : 999999999999.99,
				allowBlank: false
			},
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},{
			width: 150,
			header: 'HASTA',
			tooltip: 'Monto Final',
			dataIndex: 'MONTOFIN',
			sortable: true,
			align: 'right',
			editor: {
				xtype: 'numberfield',
				maxValue : 999999999999.99,
				allowBlank: false
			},
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header : 'PORCENTAJE',
			tooltip: 'Pocentaje',
			dataIndex : 'PORCENTAJE',
			width: 196,
			sortable : true,
			align: 'center',
			editor: {
				xtype: 'numberfield',
				maxValue : 100.00,
				allowBlank: false
			},
			renderer: Ext.util.Format.numberRenderer('0,0.00%')
		}],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 400,
		width: 500,
		bodyStyle:'text-align:left',
		frame: true,
		listeners: {
			cellclick :function(grid, rowIndex, colIndex, evento){}
		},
		bbar: {
			xtype: 'toolbar',
			items: ['->','-',{
				text: 'Especificar Otro Rango',
				handler: onAgregarRango
			},'-',{
				text: 'Guardar',
				handler: onGuardarRangos
			},'-',{
				text: 'Cancelar',
				handler: onCancelRangos
			}]
		}
	});

	var fp = new Ext.form.FormPanel({
		height:        200,
		labelWidth:    260,
		id:            'forma',
		title:         'Comisi�n Fija',
		bodyStyle:     'padding: 10px',
		defaultType:   'textfield',
		frame:         true,
		defaults: {
			msgTarget:  'side'
		},
		items: [{
			width:      250,
			xtype:      'numberfield',
			name:       'montoComision',
			id:         'montoComision1',
			fieldLabel: 'MONTO DE LA COMISI�N POR MOVIMIENTO',
			allowBlank: false,
			maxValue:   9999999999.99,
			msgTarget:  'side'
		},{
			width:      250,
			xtype:      'numberfield',
			name:       'ivaComision',
			id:         'ivaComision1',
			fieldLabel: '% DE IVA',
			maxValue:   100.00,
			allowBlank: false,
			msgTarget:  'side'
		}],
		buttons: [{
			text:       'Guardar',
			iconCls:    'icoAceptar',
			formBind:   true,
			handler:    function(btn){
				// El siguiente IF es solo para validar el formato de las fechas
				if(!Ext.getCmp('forma').getForm().isValid()){
					return;
				}
				if(tipoComison=='R'){
					Ext.MessageBox.confirm('Aviso','<p align="center">Existe una parametrizaci�n de Comisi�n Por Rangos previamente capturada,'+
					'dicha parametrizaci�n se eliminar� al guardar la comisi�n Fija.'+
					'<br>�Est� seguro que desea continuar?</p>',function(msb){
						if(msb=='yes'){
							Ext.Ajax.request({
								url: '36comisionfianzas.data.jsp',
								params:  Ext.apply(fp.getForm().getValues(),{
									informacion: 'GuardarComisionFija' //	
								}),
								callback: procesarSuccessSaveComisionFija
							});
						}
					});
				} else{
					Ext.Ajax.request({
						url: '36comisionfianzas.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'GuardarComisionFija'
						}),
						callback: procesarSuccessSaveComisionFija
					});
				}
			}
		},{
			text:       'Cancelar',
			iconCls:    'icoLimpiar',
			handler:    function() {
				Ext.MessageBox.confirm('Cancelaci�n Comisi�n Fija','�Est� seguro de cancelar la operaci�n?',function(msb){
					if(msb=='yes'){
						fp.getForm().reset();
						Ext.Ajax.request({
							url: '36comisionfianzas.data.jsp',
							params: {
								informacion:'valoresIniciales'
							},
							callback: procesarSuccessValoresIni
						});
					}
				});
			}
		}]
	});

//CONTENEDORES--------------------------------------------------------------------
	var tabs = new Ext.TabPanel({
		activeTab:           0,
		width:               600,
		bodyStyle:           'padding: 6px',
		style:               'margin:0 auto;',
		defaults:{autoHeight: true},
		items:[fp,
		{
			title:            'Comisi�n por Rangos',
			id:               'gridRango',
			layout:           'form',
			defaults: {autoHeight: true},
			items:[gridRangos,
			{
				xtype:         'displayfield',
				name:          'espacio',
				id:            'espacio1',
				value:         ''
			},{
				xtype:         'compositefield',
				msgTarget:     'side',
				combineErrors: false,
				items: [{
					width:      200,
					xtype:      'displayfield',
					name:       'espacio',
					id:         'espacio1',
					value:      ''
				},{
					width:      65,
					xtype:      'displayfield',
					name:       'etiqueta',
					id:         'etiqueta1',
					value:      '% DE IVA:'
				},{
					width:      150,
					xtype:      'numberfield',
					name:       'ivaComisionR',
					id:         'ivaComisionR1',
					maxValue:   100.00,
					allowBlank: false,
					msgTarget:  'side'
				}]
			}]
		}],
		listeners:{
			tabchange:function(tabPanel, panel){
				if(panel.getId()=='forma'){
					Ext.Ajax.request({
						url: '36comisionfianzas.data.jsp',
						params: {
							informacion:'valoresIniciales'
						},
						callback: procesarSuccessValoresIni
					});
				} else if(panel.getId()=='gridRango'){
					storeConsRangos.load();
				}
			}
		}
	});

	var pnl = new Ext.Container({
		width:     890,
		id:        'contenedorPrincipal',
		applyTo:   'areaContenido',
		height:    'auto',
		bodyStyle: 'padding: 6px',
		style:     'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			{//Panel para mostrar avisos
				xtype:  'panel',
				name:   'mensajes',
				id:     'mensajes1',
				style:  'margin:0 auto;',
				width:  600,
				frame:  true,
				hidden: true
			},
			tabs
			
		]
	});

	Ext.Ajax.request({
		url: '36comisionfianzas.data.jsp',
		params: {
			informacion:'valoresIniciales'
		},
		callback: procesarSuccessValoresIni
	});

});