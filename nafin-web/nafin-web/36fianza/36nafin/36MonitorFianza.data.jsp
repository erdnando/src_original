<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,
		java.sql.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		java.util.Vector,
		netropology.utilerias.*,
		com.netro.fianza.*, 
		com.netro.exception.*,
		javax.naming.*"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../36secsession_extjs.jspf" %>
<%
System.out.println("36MonitorFianza (E) ");

String 	 noAfianzadora ="", noEpo="", NoPyme="";
String perfil = (String)request.getSession().getAttribute("sesPerfil");
String informacion = request.getParameter("informacion") == null?"Monitor":(String)request.getParameter("informacion");
String infoRegresar = "";

if (informacion.equals("Monitor")) {

		if( "ADMIN AFIANZA".equals(perfil) ||  "CONS AFIANZA".equals(perfil) ) {
			noAfianzadora =iNoCliente;		 
		}		 
		if( "CONS FIADO".equals(perfil) ||  "ADMIN PYME".equals(perfil) ) {
			NoPyme =iNoCliente;	
		}		
		if( "ADMIN EPO".equals(perfil) || "REVISOR EPO".equals(perfil) ||  "AUTORIZADOR EPO".equals(perfil) ) {
			noEpo =iNoCliente;	
		}		
		

		FianzaElectronica fianza = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	
		infoRegresar  = fianza.getMonitorEXT(noAfianzadora, noEpo, NoPyme);	

		System.out.println("infoRegresar" +infoRegresar);

}	
%>
<%=infoRegresar%>
		
<%System.out.println(" 36MonitorFianza (S) ");%>