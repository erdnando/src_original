<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	java.util.ArrayList,
	
	org.apache.commons.logging.Log,
	org.apache.commons.beanutils.PropertyUtils,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	com.netro.fianza.*,
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	com.netro.exception.NafinException,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/36fianza/36secsession_extjs.jspf" %>
	<%!
		private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	%>
<% 
	String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
	String claveEPO = request.getParameter("claveEPO") == null?"1":(String)request.getParameter("claveEPO");
	String firma= request.getParameter("firma") == null?"1":(String)request.getParameter("firma");
	FianzaElectronica fianza = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	String infoRegresar="";
	String mensaje = "";
	JSONObject jsonObj = new JSONObject();
	
	if (informacion.equals("catalogoEPO") ) {
	   CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");	
		infoRegresar = cat.getJSONElementos();

	}else  if (informacion.equals("ConsultaValores") ) {
		List resultado1 = fianza.ConsParametrosEPO(claveEPO);
		if(resultado1.size()>=1){
			List resultado2 = (List)resultado1.get(0);
			firma 			= (String)resultado2.get(1);
			jsonObj.put("rdgFirma", firma);
		}
		jsonObj.put("success", new Boolean(true));	
		
		jsonObj.put("accion", "C");
		infoRegresar = jsonObj.toString();
		
	}else  if (informacion.equals("Guardar_Datos") ) {
		
		List resultado  = new ArrayList();	
		List datos  = new ArrayList();
		datos.add(firma);
		
		try{
			String respuesta1 = fianza.AgregarParametrosEPO(claveEPO, datos);
			mensaje = "Los Parametros se actualizaron satisfactoriamente";	
		}catch(Exception e){
			mensaje="ERROR "+e.getMessage();
			log.error("ERROR "+ mensaje);
		}
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("mensaje", mensaje);	
		jsonObj.put("claveEPO", claveEPO);
		
		jsonObj.put("accion", "G");
		infoRegresar = jsonObj.toString();
	}
%>
<%=infoRegresar%>
