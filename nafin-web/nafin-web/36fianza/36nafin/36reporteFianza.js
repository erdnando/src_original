Ext.onReady(function() {

 Ext.QuickTips.init();

	var procesarVerObservaciones = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var observaciones  = registro.get('OBSERVACIONES');		
		var ventana = Ext.getCmp('VerObservaciones');
		if (ventana) {	
				var objObserv = ventana.findById('iwObserv');
				objObserv.setValue(observaciones);						
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 250,
				height: 100,		
				id: 'VerObservaciones',
				closeAction: 'hide',
				items: [
				{
					xtype:'textarea',
					name:'wObserv',
					id:'iwObserv',
					emptyText: 'No hay Observaciones',
					value:observaciones,
					readOnly : true
				}
				],										
				title: 'Observaciones Fianza Electr�nica'			
			}).show();
		}		
	}		
	
	
	var procesarSuccessFailureGenerarArchivoCsv =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
		
		var procesarSuccessFailureGenerarArchivoPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
		//HANDLERS GENERAL	
	var procesarGeneralData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		
		var fp = Ext.getCmp('forma');	
		fp.el.unmask();		
		
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
					gridGeneral.show();
			}				
		}		
		var el = gridGeneral.getGridEl();		
		if (arrRegistros == '') {
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('btnBajarPDF').hide();		
				Ext.getCmp('btnGenerarCSV').disable();
				Ext.getCmp('btnBajarCSV').hide();		
					
			el.mask('No existen registros para los criterios de b�squeda seleccionados', 'x-mask');
		}else if (arrRegistros != null){
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnBajarPDF').hide();		
				Ext.getCmp('btnGenerarCSV').enable();
				Ext.getCmp('btnBajarCSV').hide();	
				el.unmask();
		}
	}	
	

var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatusStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '36reporteFianza.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'cc_estatus',
			id: 'cc_estatus1',
			fieldLabel: 'Estatus',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cc_estatus',
			emptyText: 'Seleccione...',
			autoSelect :true,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEstatus,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Emisi�n Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_emision_ini',
					id: 'df_emision_ini1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_emision_fin1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_emision_fin',
					id: 'df_emision_fin1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_emision_ini1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Vencimiento Fianza',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_vencimiento_ini',
					id: 'df_vencimiento_ini1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_vencimiento_fin1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy a',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'df_vencimiento_fin',
					id: 'df_vencimiento_fin1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_vencimiento_ini1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/yyyy',
					width: 50
				}			
			]
		},
		{ 
			xtype:   'label',  
			html:		'* Si no selecciona criterios de b�squeda, se presentaran �nicamente las fianzas que tuvieron movimientos durante el d�a actual', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		}
	];


	
//CONSULTA PARA LOS DEL GRID GENERAL
	var consultaGeneralData   = new Ext.data.GroupingStore({	 
		root : 'registros',
		url : '36reporteFianza.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},		
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			idProperty: 'taskId',
			fields: [
			{name: 'ESTATUS'},
			{name: 'AFIANZADORA'},					
			{name: 'NOFIANZA'},					
			{name: 'NOCONTRATO'},					
			{name: 'MONTO'},					
			{name: 'RAMO'},					
			{name: 'SUB_RAMO'},					
			{name: 'TIPO_FIANZA'},
			{name: 'FECHA_EMISION'},
			{name: 'VIGENCIA'},
			{name: 'FIADO'}	,		
			{name: 'DESTINATARIO'},
			{name: 'BENEFICIARIO'},
			{name: 'OBSERVACIONES'}
			]
		}),
		groupField: 'ESTATUS',
		sortInfo:{field: 'ESTATUS', direction: "ASC" , align: 'left'	 },
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarGeneralData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarGeneralData(null, null, null);
				}
			}
		}		
	});
		
	// define a custom summary function
    Ext.ux.grid.GroupSummary.Calculations['totalCost'] =  function(value, record, field){
        return value + (record.data.MONTO);
    };
	// utilize custom extension for Group Summary
    //var summary = '<b>'+ new Ext.ux.grid.GroupSummary()+'<b>';
		var summary = new Ext.ux.grid.GroupSummary();
		
 
	//CREA EL GRID 
	var gridGeneral = new Ext.grid.GridPanel({
		store: consultaGeneralData,
		id: 'gridGeneral',
		hidden: true,
		title: 'REPORTE FIANZAS POR ESTATUS',
		margins: '20 0 0 0',
		columns: [{
			header: '<div align="center"> Estatus </div>',
			tooltip: 'Estatus',
			dataIndex: 'ESTATUS',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: true,
			align: 'left'
		},{
			header: '<div align="center"> Afianzadora </div>',
			tooltip: 'Afianzadora',
			dataIndex: 'AFIANZADORA',
			sortable: false,
			width: 150,
			resizable: true,
			hidden: false,
			align: 'left',
			summaryType: 'count',
			hideable: false,
			summaryRenderer: function(v, params, data){
				return ((v === 0 || v > 1) ? 'Totales       ' + v +'' : 'Totales     1');
			},
			editor: new Ext.form.TextField({
				allowBlank: false
			})
		},{
			header: 'No. Fianza',
			tooltip: 'No. Fianza',
			dataIndex: 'NOFIANZA',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false,
			align: 'center'
		},{
			header: 'No. Contrato',
			tooltip: 'No. Contrato',
			dataIndex: 'NOCONTRATO',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false,
			align: 'center'
		},{
			header: '<div align="center"> Monto </div>',
			tooltip: 'Monto',
			dataIndex: 'MONTO',
			sortable: true,
			hideable: false,
			width: 150,
			align: 'right',
			summaryType: 'count',
			renderer: function(value, params, record){
				return Ext.util.Format.usMoney(record.data.MONTO);
			},
			summaryType: 'totalCost',
			summaryRenderer: Ext.util.Format.usMoney
		},{
			header: '<div align="center"> Ramo </div>',
			tooltip: 'Ramo',
			dataIndex: 'RAMO',
			sortable: true,
			hideable: false,
			width: 150,
			align: 'left'
		},{
			header: '<div align="center"> Sub Ramo </div>',
			tooltip: 'Sub Ramo',
			dataIndex: 'SUB_RAMO',
			sortable: true,
			hideable: false,
			width: 150,
			align: 'left'
		},{
			header: '<div align="center"> Tipo Fianza </div>',
			tooltip: 'Tipo Fianza',
			dataIndex: 'TIPO_FIANZA',
			sortable: true,
			hideable: false,
			width: 150,
			align: 'left'
		},{
			header: 'Fecha Emisi�n',
			tooltip: 'Fecha Emisi�n',
			dataIndex: 'FECHA_EMISION',
			sortable: true,
			hideable: false,
			width: 150,
			align: 'center'
		},{
			header: 'Vigencia',
			tooltip: 'Vigencia',
			dataIndex: 'VIGENCIA',
			sortable: true,
			hideable: false,
			width: 150,
			align: 'center'
		},{
			header: '<div align="center"> Fiado </div>',
			tooltip: 'Fiado',
			dataIndex: 'FIADO',
			sortable: true,
			hideable: false,
			width: 150,
			align: 'left'
		},{
			header: '<div align="center"> Destinatario </div>',
			tooltip: 'Destinatario',
			dataIndex: 'DESTINATARIO',
			sortable: true,
			hideable: false,
			width: 150,
			align: 'left'
		},{
			header: 'Beneficiario',
			tooltip: 'Beneficiario',
			dataIndex: 'BENEFICIARIO',
			sortable: true,
			hideable: false,
			width: 150,
			align: 'center'
		},{
			xtype: 'actioncolumn',
			header: 'Observaciones',
			tooltip: 'Observaciones',
			dataIndex: 'OBSERVACIONES',
			width: 150,
			align: 'center',
			renderer: function(value, metadata, record, rowindex, colindex, store) {
				var mensaje;
				if (record.get('OBSERVACIONES') =='') {
					mensaje = "No hay Observaciones";
				} else {
					mensaje = "Ver";
				}
				return mensaje;
			},
			items: [{
				getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
					if (registro.get('OBSERVACIONES') !='') {
						this.items[0].tooltip = 'Ver';
						return 'iconoLupa';
					}
				},
				handler: procesarVerObservaciones
			}]
		}],
		view: new Ext.grid.GroupingView({
		groupTextTpl: '{text}'
		}),
		displayInfo: true,
		emptyMsg: 'No hay registros.',
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 943,
		frame: false,
		bodyStyle:'text-align:left',
		plugins: summary,
		bbar: {
			xtype: 'toolbar',
			items: [
				'-',
				'->',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
					boton.disable();
					Ext.Ajax.request({
						url: '36reporteFianza.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF'
						}),
						callback: procesarSuccessFailureGenerarArchivoPDF
					});
				}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar CSV',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
					boton.disable();
					Ext.Ajax.request({
						url: '36reporteFianza.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'
						}),
						callback: procesarSuccessFailureGenerarArchivoCsv
					});
				}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSV',
					hidden: true
				}
			]
		}
	});

	var fp = new Ext.form.FormPanel({
		width:         700,
		labelWidth:    170,
		id:            'forma',
		title:         'REPORTE FIANZAS POR ESTATUS',
		style:         'margin:0 auto;',
		bodyStyle:     'padding: 6px',
		defaultType:   'textfield',
		defaults: {
			msgTarget:  'side',
			anchor:     '-20'
		},
		items:         elementosForma,
		frame:         true,
		collapsible:   true,
		titleCollapse: false,
		buttons: [{
			text:       'Consultar',
			iconCls:    'icoBuscar',
			formBind:   true,
			handler:    function(btn){
				// El siguiente IF es solo para validar el formato de las fechas
				if(!Ext.getCmp('forma').getForm().isValid()){
					return;
				}
				// VALIDA LOS CAMPOS DE FECHA
				if( Ext.getCmp('df_emision_ini1').getValue() == '' && Ext.getCmp('df_emision_fin1').getValue() != '' ){
					Ext.getCmp('df_emision_ini1').markInvalid('Debe capturar ambas fechas emisi�n fianza o dejarlas en blanco.');
					return;
				}
				if( Ext.getCmp('df_emision_ini1').getValue() != '' && Ext.getCmp('df_emision_fin1').getValue() == '' ){
					Ext.getCmp('df_emision_fin1').markInvalid('Debe capturar ambas fechas emisi�n fianza o dejarlas en blanco.');
					return;
				}
				if( Ext.getCmp('df_vencimiento_ini1').getValue() == '' && Ext.getCmp('df_vencimiento_fin1').getValue() != '' ){
					Ext.getCmp('df_vencimiento_ini1').markInvalid('Debe capturar ambas fechas vencimiento fianza o dejarlas en blanco.');
					return;
				}
				if( Ext.getCmp('df_vencimiento_ini1').getValue() != '' && Ext.getCmp('df_vencimiento_fin1').getValue() == '' ){
					Ext.getCmp('df_vencimiento_fin1').markInvalid('Debe capturar ambas fechas vencimiento fianza o dejarlas en blanco.');
					return;
				}
				fp.el.mask('Enviando...', 'x-mask-loading');
				consultaGeneralData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar',
						operacion: 'Generar'
					})
				});
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('btnBajarPDF').hide();
				Ext.getCmp('btnGenerarCSV').disable();
				Ext.getCmp('btnBajarCSV').hide();
			}
		},{
			text:       'Limpiar',
			iconCls:    'icoLimpiar',
			handler:    function() {
				document.location.href  = "36reporteFianza.jsp";
			}
		}]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		layoutConfig: {
			align:'center'
		},
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridGeneral,
			NE.util.getEspaciador(20)
		]
	});
	
	catalogoEstatus.load();
	

});