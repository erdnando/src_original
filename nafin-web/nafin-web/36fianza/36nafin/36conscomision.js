Ext.onReady(function() {

//VARIABLES--------------------------------------------------------------------
var f=new Date();
var nombreAfianzadora;
var strTipoAfil;
var anio = f.getFullYear();
var dataMeses = [["1","Enero"], ["2","Febrero"],
					  ["3","Marzo"], ["4","Abril"],
					  ["5","Mayo"],  ["6","Junio"],
					  ["7","Julio"], ["8","Agosto"],
					  ["9","Septiembre"], ["10","Octubre"],
					  ["11","Noviembre"], ["12","Diciembre"]
					 ];

var anios = [];
for(x=2011;x<=anio;x++){
	anios.push([x,x]);
	
}

//HANDLERS---------------------------------------------------------------------
var procesarSuccessValoresIni = function(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
		nomAfianza = resp.nomAfianza;
		strTipoAfil = resp.strTipoAfiliado;
		
		if(strTipoAfil=='A'){
			nombreAfianzadora = nomAfianza;
			comboAfianza = fp.findById('ic_afianzadora1');
			comboAfianza.hide();
			comboAfianza.allowBlank = true;
		}
		
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

	var procesarDataComisiones = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		if (arrRegistros != null) {
			if (!gridComisiones.isVisible()) {
				Ext.getCmp('gridComision').show();
			}
			
			var el = gridComisiones.getGridEl();
			Ext.getCmp('btnAbrirArc').hide();
			Ext.getCmp('btnAbrirPDF').hide();
			if(store.getTotalCount() > 0) {
				el.unmask();
				Ext.getCmp('btnDescargaArc').enable();
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnTotales').enable();
			}else {
				Ext.getCmp('btnDescargaArc').disable();
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('btnTotales').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	}

	var procesarDataTotales = function(store, arrRegistros, opts){
		if (arrRegistros != null){
			if (!gridTotales.isVisible()){
				Ext.getCmp('gridTotales').show();
			}
		}
	}

var onDescargaCSV = function(btn){

	var storeComi = gridComisiones.getStore();
	var arrRegistros = [];
	btn.setIconClass('loading-indicator');
	btn.disable();
	
	storeComi.each(function(record){
		arrRegistros.push(record.data);
	});
	
	arrRegistros = Ext.encode(arrRegistros);
	Ext.Ajax.request({
		url: '36conscomision.data.jsp',
		params: {
				informacion: 'Archivos',
				registros : arrRegistros,
				operacion : 'csv',
				nomAfianzadora: nombreAfianzadora
				},
		callback: procesarSuccessGeneraCSV
	});
}

var procesarSuccessGeneraCSV = function(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
		var btnDescCSV = Ext.getCmp('btnDescargaArc');
		btnDescCSV.setIconClass('');
		var btnAbrCSV = Ext.getCmp('btnAbrirArc');
		btnAbrCSV.show();
		btnAbrCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnAbrCSV.setHandler( function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
			
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

var onGeneraPDF = function(btn){

	var storeComi = gridComisiones.getStore();
	var arrRegistros = [];
	btn.setIconClass('loading-indicator');
	btn.disable();
	
	storeComi.each(function(record){
		arrRegistros.push(record.data);
	});
	
	arrRegistros = Ext.encode(arrRegistros);
	Ext.Ajax.request({
		url: '36conscomision.data.jsp',
		params: {
				informacion: 'Archivos',
				registros : arrRegistros,
				operacion : 'pdf',
				nomAfianzadora: nombreAfianzadora
				},
		callback: procesarSuccessGeneraPDF
	});
}

var procesarSuccessGeneraPDF = function(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
		var btnGenPDF = Ext.getCmp('btnGenerarPDF');
		btnGenPDF.setIconClass('');
		var btnAbrPDF = Ext.getCmp('btnAbrirPDF');
		btnAbrPDF.show();
		btnAbrPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnAbrPDF.setHandler( function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
			
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

//STORES-----------------------------------------------------------------------
var catalogoAfianza = new Ext.data.JsonStore({
		id: 'catalogoAfianzaStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '36conscomision.data.jsp',
		baseParams: {
			informacion: 'catalogoAfianza'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
var storeCatMeses = new Ext.data.ArrayStore({
		id: 'catalogoMeses',
		fields : ['clave', 'descripcion'],
		data: dataMeses
	});

var storeCatAnio = new Ext.data.ArrayStore({
		id: 'catalogoAnio',
		fields : ['clave', 'descripcion'],
		data: anios
	});
	
var storeDataComision = new Ext.data.JsonStore({
		id:'consultaComisones',
		root : 'registros',
		url : '36conscomision.data.jsp',
		baseParams: {
			informacion: 'GenerarConsulta'
		},
		fields: [
			{name: 'MES'},
			{name: 'ANIO'},
			{name: 'MONTOCOMISION', type: 'float'},
			{name: 'IVA'},
			{name: 'MONTOIVA', type: 'float'},
			{name: 'MONTOTOTAL', type: 'float'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDataComisiones,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDataComisiones(null, null, null);
				}
			}
		}
	});
	
	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '36conscomision.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'TOTALCOMISION', type: 'float'},
			{name: 'TOTALIVA', type: 'float'},
			{name: 'TOTAL', type: 'float'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarDataTotales,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDataTotales(null, null, null);
				}
			}
		}
		
	});
//------------------------------------------------------------------------------

	var fp = new Ext.form.FormPanel({
		width:                540,
		height:               150,
		id:                   'forma',
		title:                'Comisi�n Para Fianza Electr�nica',
		bodyStyle:            'padding: 10px',
		style:                'margin:0 auto;',
		frame:                true,
		monitorValid:         true,
		layoutCongif:         {align:'center'},
		items: [{
			width:             300,
			xtype:             'combo',
			name:              'ic_afianzadora',
			id:                'ic_afianzadora1',
			fieldLabel:        'Afianzadora',
			mode:              'local', 
			displayField:      'descripcion',
			valueField:        'clave',
			hiddenName:        'ic_afianzadora',
			emptyText:         'Seleccione...',
			msgTarget:         'side',
			triggerAction:     'all',
			minChars:          1,
			typeAhead:         true,
			forceSelection:    true,
			allowBlank:        true,
			store:             catalogoAfianza,
			tpl:               NE.util.templateMensajeCargaCombo
		},{
			xtype:             'compositefield',
			fieldLabel:        'Mes de C�lculo',
			msgTarget:         'side',
			combineErrors:     false,
			items: [{
				width:          110,
				xtype:          'combo',
				name:           'cboMes',
				id:             'cboMes1',
				mode:           'local',
				displayField:   'descripcion',
				valueField:     'clave',
				hiddenName:     'cboMes',
				emptyText:      'Seleccione...',
				msgTarget:      'side',
				triggerAction:  'all',
				minChars:       1,
				forceSelection: true,
				typeAhead:      true,
				allowBlank:     false,
				store:          storeCatMeses
			},{
				width:          10,
				xtype:          'displayfield',
				value:          ''
			},{
				width:          10,
				xtype:          'displayfield',
				value:          ' A '
			},{
				width:          110,
				xtype:          'combo',
				name:           'cboMes2',
				id:             'cboMes21',
				mode:           'local',
				displayField:   'descripcion',
				valueField:     'clave',
				hiddenName:     'cboMes2',
				emptyText:      'Seleccione...',
				msgTarget:      'side',
				forceSelection: true,
				triggerAction:  'all',
				typeAhead:      true,
				minChars:       1,
				allowBlank:     false,
				store:          storeCatMeses
			},{
			
				width:          10,
				xtype:          'displayfield',
				value:          ''
			},{
				width:          100,
				xtype:          'combo',
				name:           'cboAnio',
				id:             'cboAnio1',
				mode:           'local',
				displayField:   'descripcion',
				valueField:     'clave',
				hiddenName:     'cboAnio',
				emptyText:      'Seleccione...',
				msgTarget:      'side',
				triggerAction:  'all',
				minChars:       1,
				forceSelection: true,
				typeAhead:      true,
				allowBlank:     false,
				store:          storeCatAnio
			}]
		}],
		buttons: [{
			text:              'Consultar',
			iconCls:           'icoBuscar',
			formBind:          true,
			handler:           function(btn){
			var recCatAf = catalogoAfianza.getAt(catalogoAfianza.findExact('clave',Ext.getCmp('ic_afianzadora1').getValue()));
				if(strTipoAfil !='A'){
					nombreAfianzadora = recCatAf.get('descripcion');
				}
				Ext.getCmp('gridTotales').hide();
				gridComisiones.setTitle(nombreAfianzadora);
				fp.el.mask('Enviando...', 'x-mask-loading');
				storeDataComision.load({
					params: Ext.apply(fp.getForm().getValues())
				});
			}
		},{
			text:              'Limpiar',
			iconCls:           'icoLimpiar',
			handler: function() {
				document.location.href  = "36conscomision.jsp";
			}
		}]
	});

	var gridComisiones = new Ext.grid.GridPanel({
		width:        800,
		height:       400,
		id:           'gridComision',
		margins:      '20 0 0 0',
		style:        'margin:0 auto;',
		title:        '-',
		bodyStyle:    'text-align:left',
		hidden:       true,
		stripeRows:   true,
		columnLines:  true,
		loadMask:     true,
		frame:        false,
		store:        storeDataComision,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [{
			header:    'Mes',
			tooltip:   'Mes del a�o',
			dataIndex: 'MES',
			sortable:  true,
			width:     150,
			align:     'center',
			renderer: function(valor,metadata, registro){
				var recd = storeCatMeses.getAt(storeCatMeses.findExact('clave',valor));
				return recd.get('descripcion');
			}
		},{
			header:    'A�o',
			tooltip:   'A�o ',
			dataIndex: 'ANIO',
			sortable:  true,
			width:     100,
			align:     'center'
		},{
			header:    'Comisiones Generadas',
			tooltip:   'Comisiones de Fianzas',
			dataIndex: 'MONTOCOMISION',
			width:     150,
			sortable:  true,
			align:     'right',
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header:    '% IVA',
			tooltip:   'IVA por Comision',
			dataIndex: 'IVA',
			width:     100,
			sortable:  true,
			align:     'center',
			renderer:  Ext.util.Format.numberRenderer('0,0.00%')
		},{
			header:    'Monto IVA',
			tooltip:   'Monto del % de IVA',
			dataIndex: 'MONTOIVA',
			width:     150,
			sortable:  true,
			align:     'right',
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header:    'Total',
			tooltip:   'Monto total de fianzas',
			dataIndex: 'MONTOTOTAL',
			width:     150,
			sortable:  true,
			align:     'right',
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		}],
		bbar: {
			xtype:     'toolbar',
			items: ['->','-',{
				text:   'Totales',
				id:     'btnTotales',
				hidden: false,
				handler: function(boton, evento) {
					Ext.getCmp('gridTotales').hide();
					var storeComi = gridComisiones.getStore();
					var registrosEnviar = [];
					storeComi.each(function(record){
						registrosEnviar.push(record.data);
					});
					registrosEnviar = Ext.encode(registrosEnviar);
					resumenTotalesData.load({
						params: {
							registros: registrosEnviar //Generar datos para la consulta
						}
					});
				}
			},'-',{
				text:   'Descargar Archivo',
				id:     'btnDescargaArc',
				handler:onDescargaCSV
			},{
				text:   'Abrir Archivo',
				id:     'btnAbrirArc',
				hidden: true
			},'-',{
				text:   'Generar PDF',
				id:     'btnGenerarPDF',
				handler:onGeneraPDF
			},{
				text:   'Abrir PDF',
				id:     'btnAbrirPDF',
				hidden: true
			}]
		}
	});

	var gridTotales = new Ext.grid.GridPanel({
		width:        800,
		//height:       210,
		autoHeight:    true,
		id:           'gridTotales',
		title:        'Totales',
		style:        'margin:0 auto;',
		hidden:       true,
		frame:        false,
		store:        resumenTotalesData,
		columns: [{
			width:     200,
			header:    '<div align="center">Comisiones Generadas</div>',
			tooltip:   'Monto Total Comisones',
			dataIndex: 'TOTALCOMISION',
			align:     'right',
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		},{
			width:     300,
			header:    '<div align="center">Monto IVA</div>',
			tooltip:   'Monto Total IVA',
			dataIndex: 'TOTALIVA',
			align:     'right',
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		},{
			width:     295,
			header:    '<div align="center">Total</div>',
			tooltip:   'Monto Total',
			dataIndex: 'TOTAL',
			align:     'right',
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		}]
	});

//CONTENEDORES------------------------------------------------------------------

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		layoutConfig: {
			align:'center'
		},
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridComisiones,
			gridTotales
		]
	});

	catalogoAfianza.load();

	Ext.Ajax.request({
		url: '36conscomision.data.jsp',
		params: {
			informacion:'valoresIniciales'
		},
		callback: procesarSuccessValoresIni
	});

});