<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalgoAfianzadora,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fianza.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
if (informacion.equals("valoresIniciales")) {
	JSONObject jsonObj = new JSONObject();
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	HashMap map = fianzaElectronica.getComisionFija();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("monto", (String)map.get("MONTO"));
	jsonObj.put("iva", (String)map.get("IVA"));
	jsonObj.put("tipoComision", fianzaElectronica.getTipoComisionFE());
	System.out.println("fianzaElectronica.getTipoComisionFE()==="+fianzaElectronica.getTipoComisionFE());

	infoRegresar= jsonObj.toString();
}else if (informacion.equals("ConsultarRangos")) {
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	
	List lstFianzasInfo = new ArrayList();
	JSONArray jsObjArray = new JSONArray();
	lstFianzasInfo = fianzaElectronica.getComisionPorRangos();
	jsObjArray = JSONArray.fromObject(lstFianzasInfo);
	
	infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";

}else if (informacion.equals("GuardarComisionFija")) {
	JSONObject jsonObj = new JSONObject();
	String monto =  request.getParameter("montoComision");
	String iva  = request.getParameter("ivaComision");
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	fianzaElectronica.setComisionFija(monto, iva);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("msg", "La parametrización se almacenó correctamente");
	
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("GuardarComisionRangos")) {
	JSONObject jsonObj = new JSONObject();
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String iva  = request.getParameter("valIva");
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	
	List arrRegistrosModificados = JSONArray.fromObject(jsonRegistros);
	fianzaElectronica.setComisionPorRangos(arrRegistrosModificados, iva);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("msg", "La parametrización se almacenó correctamente");
	
	infoRegresar = jsonObj.toString();

}

System.out.println(infoRegresar);
%>

<%=infoRegresar%>