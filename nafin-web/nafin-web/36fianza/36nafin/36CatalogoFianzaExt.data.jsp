<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalgoAfianzadora,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fianza.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";

	String catalogo = (request.getParameter("catalogo")!=null)?request.getParameter("catalogo"):""; 
	String clave = (request.getParameter("clave")!=null)?request.getParameter("clave"):""; 
	String catalogoRamo = (request.getParameter("catalogoRamo")!=null)?request.getParameter("catalogoRamo"):""; 
	String descripcion = (request.getParameter("descripcion")!=null)?request.getParameter("descripcion"):""; 
	String subRamo = (request.getParameter("subRamo")!=null)?request.getParameter("subRamo"):""; 
	
	JSONObject jsonObj = new JSONObject();
	HashMap datos = new HashMap();
	
	String respuesta="";
	
	FianzaElectronica fianzaElectronica = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB", FianzaElectronica.class);
	

if (informacion.equals("Consultar")){
	List info = new ArrayList();
		String clave1 = (request.getParameter("clave1")!=null)?request.getParameter("clave1"):""; 
		String clave2 = (request.getParameter("clave2")!=null)?request.getParameter("clave2"):""; 
		
		String clave_ = ((catalogo.equals("1"))?clave1:clave2);
	if(catalogo.equals("3")){
		info = fianzaElectronica.getConsCatalogo(catalogo, clave_, catalogoRamo , subRamo);
	}else{
		info = fianzaElectronica.getConsCatalogo(catalogo, clave_, catalogoRamo , descripcion);
	}	
	Iterator it = info.iterator();
	 datos = new HashMap();
	List registros= new ArrayList();
	if(!catalogo.equals("3")){
		while(it.hasNext()) {
			List campos = (List) it.next();
			String id = (String) campos.get(0);
			String desc = (String) campos.get(1);
			datos = new HashMap();
			datos.put("clave", id );
			datos.put("descripcion", desc );
			registros.add(datos);
		}
	}else if(catalogo.equals("3")){
		while(it.hasNext()) {
			List campos = (List) it.next();
			String idramo = (String) campos.get(0);
			String id = (String) campos.get(1);
			String ramo = (String) campos.get(2);
			String subramo = (String) campos.get(3);
			datos = new HashMap();
			datos.put("claveramo", idramo );
			datos.put("clavesubramo", id );
			datos.put("descripcionramo", ramo );
			datos.put("descripcionsubramo", subramo );
			registros.add(datos);
		}
	} 
	
	
	jsonObj = new JSONObject();
	jsonObj.put("success",  new Boolean(true)); 
	jsonObj.put("registros", registros);
	infoRegresar = jsonObj.toString();
	System.out.println("Informacion "+informacion+"\nresultados "+infoRegresar);

}else if(informacion.equals("catalogoRamo")){
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("FECAT_RAMO");
	cat.setCampoClave("CC_RAMO");
	cat.setCampoDescripcion("CG_DESCRIPCION"); 
	infoRegresar = cat.getJSONElementos();
	System.out.println("catalogoRamos "+infoRegresar);
	
}else if (informacion.equals("Aceptar")){
		String clave1 = (request.getParameter("clave1")!=null)?request.getParameter("clave1"):""; 
		String clave2 = (request.getParameter("clave2")!=null)?request.getParameter("clave2"):""; 
		String clave_ = ((catalogo.equals("1"))?clave1:clave2);
		String valor =fianzaElectronica.getValidaCatalogo(catalogo, clave_, catalogoRamo );
		
	if(operacion.equals("modificar")){
		if(catalogo.equals("3")){
			respuesta = fianzaElectronica.getUpdateCatalogo(catalogo, clave_,  catalogoRamo, subRamo );
		}else{
			respuesta = fianzaElectronica.getUpdateCatalogo(catalogo, clave_,  catalogoRamo, descripcion );
		}
		
	
		JSONObject resultado = new JSONObject();
		resultado.put("success", new Boolean(true));
		resultado.put("msg", respuesta);
		infoRegresar = resultado.toString();		
		System.out.println("Aceptar "+infoRegresar);
	} else if(operacion.equals("insertar")){
		
			if(valor.equals("0")){
				
				if(catalogo.equals("3")){
					respuesta = fianzaElectronica.getAgregarCatalogo(catalogo,clave_,catalogoRamo,subRamo );
				}else{
					respuesta = fianzaElectronica.getAgregarCatalogo(catalogo,clave_,catalogoRamo,descripcion );
				}
			}else if(!valor.equals("0")){
				respuesta ="EXISTE";
			}	
	
		JSONObject resultado = new JSONObject();
		resultado.put("success", new Boolean(true));
		resultado.put("msg", respuesta);
		infoRegresar = resultado.toString();		
		System.out.println("Aceptar "+infoRegresar);
	}
	
	
}else if (informacion.equals("eliminar")){
		respuesta = fianzaElectronica.getDeleteCatalogo(catalogo,  clave,  catalogoRamo );
			
		
			JSONObject resultado = new JSONObject();
		resultado.put("success", new Boolean(true));
		resultado.put("msg", respuesta);
		infoRegresar = resultado.toString();		
		System.out.println("Aceptar "+infoRegresar);
}
		
		


%>

<%=infoRegresar%>
