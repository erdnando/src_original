Ext.onReady(function() {
	//---------------------------------- HANDLERS -------------------------------
	function procesaConsultaValores(opts, success, response) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		var jsonData = Ext.util.JSON.decode(response.responseText);
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			if (jsonData != null){	
				
				Ext.getCmp('btnGrabar').enable();		
				if(jsonData.accion=='G') {
					Ext.MessageBox.alert('Mensaje',"Se guard� con �xito la parametrizaci�n de esta EPO");		
						Ext.Ajax.request({
							url: '36Parametrizacion01ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{ 
								informacion: "ConsultaValores",
								claveEPO:jsonData.claveEPO
							}),
								callback: procesaConsultaValores
							});	
				}else  if(jsonData.accion=='C') {
					Ext.getCmp('rdgFirma').setValue(jsonData.rdgFirma);
				}
			}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}	
	// ***********************++Funci�n para guardar los datos  *********************
	function procesarGrabar() {	 
		if(Ext.getCmp('rdgFirma').getValue()==null) {
			Ext.getCmp('rdgFirma').markInvalid('El campo es obligatorio');
			return;	
		}
		Ext.getCmp('btnGrabar').disable();	
		fp.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '36Parametrizacion01ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{  
			informacion: "Guardar_Datos"
			}),
			callback: procesaConsultaValores
		});
	}

	//-------------------------------- STORES -----------------------------------

	var catalogo = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '36Parametrizacion01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {	
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});

	//-------------------------------- COMPONENTES -----------------------------------
	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'claveEPO',	
			id: 'cmbClaveEPO',	
			fieldLabel:'Nombre de la EPO',  
			mode: 'local',	
			hiddenName : 'claveEPO',
			emptyText: 'Seleccionar...',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catalogo,	
			tpl : NE.util.templateMensajeCargaCombo,
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 400,		
			listeners: {
				select: {
					fn: function(combo) {
						if(combo.getValue()!=''){
							Ext.getCmp('seccion_1').show();	
							Ext.getCmp('btnGrabar').show();
							fp.el.mask('Procesando...', 'x-mask-loading');	
							Ext.Ajax.request({
								url: '36Parametrizacion01ext.data.jsp',
								params: {
									informacion: "ConsultaValores",
									claveEPO:combo.getValue()
								},
								callback: procesaConsultaValores  
							});	

						}else  {
							Ext.getCmp("seccion_1").hide();
							Ext.getCmp("btnGrabar").hide();						
						}
						
					}
				}
			}	
	},			
	{			
		xtype:'panel',  id: 'seccion_1', hidden:true,	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
		defaults: {align:'center', bodyStyle:'padding:2px,'},
		items: [
				{	width:50,	frame:false,	border:false, html:'<div class="formas" align="left"><br> 1 <br><br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false, html:'<div class="formas" align="center"><br> Opera Firma Mancomunada<br><br>&nbsp;</div>'	},
				{	width:200, xtype:'radiogroup', border:false,	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'rdgFirma',	name:'firma',	cls:'x-check-group-alt',	fils:[100, 100, 100],
					items: 
						[  
							{	boxLabel	: 'SI    ',	name : 'firma', inputValue: 'I' },
							{	boxLabel	: 'NO ',	name : 'firma',  inputValue: 'A' }							
						]
						
				}				
				]
	}
	];

	

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame			: false,
		frame			: false,
		width: 620,		
		autoHeight	: true,
		title: 'Par�metros por EPO',				
		layout		: 'form',
		style: 'margin:0 auto;',
		bodyStyle:'padding: 10px',
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		items: elementosForma,		
		buttons: [		
			{
				text: 'Grabar',
				id: 'btnGrabar',
				hidden:true,
				iconCls: 'icoAceptar',
				handler: procesarGrabar
			}		
		]
	});
	//-------------------------------- PRINCIPAL -----------------------------------
	var pnl = new Ext.Container({
		
		applyTo: 'areaContenido',
		width: 1049,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),			
			NE.util.getEspaciador(20)			
		]
	});
	
	//-------------------------------- INICIALIZACION -----------------------------


});
