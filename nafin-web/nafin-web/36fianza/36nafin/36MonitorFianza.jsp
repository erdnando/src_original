<!DOCTYPE html>
<%@ page import="
			java.util.*, 
			javax.naming.*, 
			com.netro.descuento.*, 
			com.netro.exception.*" %>
<%@ include file="../36secsession_extjs.jspf" %>
<%@ include file="/appComun.jspf" %>
<%@ page 	contentType	= "text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<% String version = (String)session.getAttribute("version"); %>
<html:html>	
<HEAD>
<title>Nafin@net</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<link rel="stylesheet" href="<%=strDirecVirtualCSS%>/<%=strClase%>">
	<%@ include file="/extjs.jspf" %>
	<%if( version!=null ) { %>	
	<%@ include file="/01principal/menu.jspf"%>
	<%}%>
	<script type="text/javascript" src="36MonitorFianza.js?<%=session.getId()%>"></script>
</HEAD>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

<%if(  "ADMIN PYME".equals(strPerfil) && version!=null) { %>	

	<%@ include file="/01principal/01pyme/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01pyme/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:190px"></div></div>
		</div>
	</div>
	<%@ include file="/01principal/01pyme/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>

<%}else  if( ( "ADMIN EPO".equals(strPerfil)  || "AUTORIZADOR EPO".equals(strPerfil) ) && version!=null) { %>	

	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:190px"></div></div>
		</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>


<%} else  if( (   "ADMIN AFIANZA".equals(strPerfil)   ||  "CONS AFIANZA".equals(strPerfil)  )   && version!=null ) { %>

	<%@ include file="/01principal/01afianzadora/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01afianzadora/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:190px"></div></div>
		</div>
	</div>
	<%@ include file="/01principal/01afianzadora/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form> 

<%} else  if(  "CONS FIADO".equals(strPerfil)  && version!=null ) { %>

<%@ include file="/01principal/01fiado/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01fiado/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:190px"></div></div>
		</div>
	</div>
	<%@ include file="/01principal/01fiado/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>


<%}else{%>	

	<%if( version==null) { %>
		<div id='areaContenido' style="margin-left: 3px; margin-top: 3px"></div>
	<form id='forma' name="forma" target='_new'></form>
	<form id='formAux' name="formAux" target='_new'></form>
		
<%}else  {%>	
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>

		<div id="_menuApp"></div>
			<div id="Contcentral">
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
				<div id="areaContenido"><div style="height:230px"></div></div>
			</div>
		</div>
		
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
	
	<%}%>
	
	<%}%>
	
</body>

</html:html>