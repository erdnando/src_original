<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalgoAfianzadora,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fianza.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/36fianza/36secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String fechaHoy		= "";
	try{
		fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());	
	}catch(Exception e){
		fechaHoy = "";	
	}
	

String infoRegresar =""; 
int start = 0;
int limit = 0;	

if (informacion.equals("catalogoEstatus")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("fecat_estatus");
	cat.setCampoClave("cc_estatus");
	//cat.setCondicionIn("001", "String");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setOrden("cg_descripcion");
	infoRegresar = cat.getJSONElementos();
	
}else if ( informacion.equals("Consultar") || 
		informacion.equals("ArchivoCSV") ||
		informacion.equals("ArchivoPDF") ) {
		
	String perfil = (String)request.getSession().getAttribute("sesPerfil");
	String icAfianzadora ="", icEpo ="", icFiado =""; 

	
	com.netro.fianza.RepFianzaElec paginador = new com.netro.fianza.RepFianzaElec();
	
	String cc_estatus = request.getParameter("cc_estatus")==null?"":request.getParameter("cc_estatus");
	String df_emision_ini = request.getParameter("df_emision_ini")==null?"":request.getParameter("df_emision_ini");
	String df_emision_fin  = request.getParameter("df_emision_fin")==null?"":request.getParameter("df_emision_fin");
	String df_vencimiento_ini  = request.getParameter("df_vencimiento_ini")==null?"":request.getParameter("df_vencimiento_ini");
	String df_vencimiento_fin  = request.getParameter("df_vencimiento_fin")==null?"":request.getParameter("df_vencimiento_fin");
	if(	cc_estatus.equals("")  && df_emision_ini.equals("")  && df_emision_fin.equals("")
	&& df_vencimiento_ini.equals("")  && df_vencimiento_fin.equals("")  ) {
		paginador.setFechaHoy(fechaHoy);
	}else {
		paginador.setFechaHoy("");
	}
	paginador.setIcEstatus(cc_estatus);
	paginador.setDfEmisionIni(df_emision_ini);
	paginador.setDfEmisionFin(df_emision_fin);
	paginador.setDfVencIni(df_vencimiento_ini);
	paginador.setDfVencFin(df_vencimiento_fin);

	if("E".equals(iTipoAfiliado)){
		paginador.setIcEpo(iNoCliente);
	} if("A".equals(iTipoAfiliado) ) {
		paginador.setIcAfianzadora(iNoCliente);
	}	if("F".equals(iTipoAfiliado) ) {
		paginador.setIcFiado(iNoCliente);
	}	if("P".equals(iTipoAfiliado)) {
		paginador.setIcFiado(iNoCliente);
	}		
	if("AUTORIZADOR EPO".equals(strPerfil)  || "ADMIN AUTO EPO".equals(strPerfil) ) {
		paginador.setAutorizador(iNoUsuario);
		paginador.setPerfil(strPerfil);
		
	}else{ 	
	paginador.setAutorizador(""); 
	paginador.setPerfil("");
	}	
	if("REVISOR EPO".equals(strPerfil) ){
		paginador.setUsuario(iNoUsuario);
	}else{ 	
		paginador.setUsuario(""); 
	}


	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		Registros reg = new Registros();
	if (informacion.equals("Consultar")) {
		try {
		//	start = Integer.parseInt(request.getParameter("start"));
			//limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				//queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				reg = queryHelper.doSearch();			
			}
			//infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);															   		
			infoRegresar =  "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData() + "}";
			
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	
	}else if (informacion.equals("ArchivoCSV")) {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
	}else if (informacion.equals("ArchivoPDF")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}
	
	
	
}

System.out.println(infoRegresar);
%>

<%=infoRegresar%>