Ext.onReady(function(){
var operacion="insertar";
var manejoConsulta=0;
//------------------------------------------------------------------------------
//------------------------------HANDLER's---------------------------------------
//------------------------------------------------------------------------------

	var procesarAceptar = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		var respuesta = Ext.util.JSON.decode(opts.responseText).msg;
		var mensaje ='';
		if (arrRegistros != null) {
			if(Ext.getCmp('cboCatalogo').getValue()!=3){
				if(Ext.getCmp('cboCatalogo').getValue()==2){
					//Ext.getCmp('txtClave1').setValue('').enable();		
					if(Ext.getCmp('cboCatalogo').getValue() ==1){
						Ext.getCmp('txtClave1').setValue('').enable();		
					}else{
						Ext.getCmp('txtClave2').setValue('').enable();		
					}
					Ext.getCmp('txtDescripcion_FE').setValue();	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
				}else{
					//Ext.getCmp('txtClave1').setValue('').enable();		
					if(Ext.getCmp('cboCatalogo').getValue() ==1){
						Ext.getCmp('txtClave1').setValue('').enable();		
					}else{
						Ext.getCmp('txtClave2').setValue('').enable();		
					}
					Ext.getCmp('txtDescripcion').setValue();	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
				}
					
				operacion="insertar";
			}else if(Ext.getCmp('cboCatalogo').getValue()==3){
			////si la modificacion se hace desde la consulta general, se carga toda la consulta 
				if(manejoConsulta==0){
					Ext.getCmp('txtClave2').setValue('').enable();		
					Ext.getCmp('txtSubRamo').setValue('');
					Ext.getCmp('cboCatalogoRamo').setValue();
					consultaDataSubRamo.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
					operacion="insertar";
				}else{//solo se carga la consulta dependiendo del ramo
					Ext.getCmp('txtClave2').setValue('').enable();		
					Ext.getCmp('txtSubRamo').setValue('');
					//Ext.getCmp('cboCatalogoRamo').setValue();
					consultaDataSubRamo.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
					operacion="insertar";
				}
			}
			if(respuesta=='M'){
				mensaje='El Registro fue Actualizado';
			}else if(respuesta=='EXISTE'){
				mensaje='La Clave capturada ya existe, favor de verificar';
			}else if(respuesta=='A'){
				mensaje='El Registro fue Agregado ';
			}
			Ext.Msg.alert('Mensaje',mensaje);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		
		var gridConsultaR = Ext.getCmp('gridConsultaSubRamo');	
		var elR = gridConsultaR.getGridEl();	
	
		if (arrRegistros != null) {
			if(Ext.getCmp('cboCatalogo').getValue()!=3){
				gridConsultaR.hide();
				if (!gridConsulta.isVisible()) {
					gridConsulta.show();
				}
				if(store.getTotalCount() > 0) {					
					el.unmask();
				} else {		
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}else if(Ext.getCmp('cboCatalogo').getValue()==3){
				gridConsulta.hide();
				if (!gridConsultaR.isVisible()) {
					gridConsultaR.show();
				}
				if(store.getTotalCount() > 0) {					
					elR.unmask();
				} else {		
					elR.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	}
	
		var procesarConsultaDataR = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		
		
		var gridConsultaR = Ext.getCmp('gridConsultaSubRamo');	
		var el = gridConsultaR.getGridEl();	
	
		if (arrRegistros != null) {
		
			if(Ext.getCmp('cboCatalogo').getValue()==3){
				if (!gridConsulta.isVisible()) {
					gridConsulta.hide();
					gridConsultaR.show();
					
				}
				if(store.getTotalCount() > 0) {					
					el.unmask();
				} else {		
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	}
	var procesarModificar = function(grid, rowIndex, colIndex, item, event) {
			operacion='modificar';
		var idCatalogo= Ext.getCmp('cboCatalogo').getValue();
		

			var registro = grid.getStore().getAt(rowIndex);
			var clave = registro.get('clave'); 
			var descripcion = registro.get('descripcion'); 
			if(idCatalogo ==1){
				Ext.getCmp('txtClave1').setValue(clave).disable();		
			}else{
				Ext.getCmp('txtClave2').setValue(clave).disable();		
			}
			if(idCatalogo == 2){
			Ext.getCmp('txtDescripcion_FE').setValue(descripcion);
			}else{
				Ext.getCmp('txtDescripcion').setValue(descripcion);		
			}
			
		
	}
	var procesarModificarR = function(grid, rowIndex, colIndex, item, event) {
		operacion='modificar';
		var idCatalogo= Ext.getCmp('cboCatalogo').getValue();
		
			var registro = grid.getStore().getAt(rowIndex);
			var ramo = registro.get('claveramo'); 
			var clave = registro.get('clavesubramo'); 
			var descripcion = registro.get('descripcionsubramo'); 
			
			Ext.getCmp('cboCatalogoRamo').setValue(ramo);
			Ext.getCmp('txtClave2').setValue(clave).disable();		
			Ext.getCmp('txtSubRamo').setValue(descripcion);		
		
	}
	var procesarBorrar = function(grid, rowIndex, colIndex, item, event) {//Modificar
		var registro = grid.getStore().getAt(rowIndex);
		var catalogo = Ext.getCmp('cboCatalogo').getValue();
		var clave="";
		Ext.MessageBox.confirm('', '�Esta seguro de querer eliminar el Registro?', function(btn){
			if(btn === 'yes'){
				 if(catalogo==3){
					clave = registro.get('clavesubramo');	
				}else{
					clave = registro.get('clave');	
				}
				  
				var catalogoRamo=registro.get('claveramo');  
				Ext.Ajax.request({
						url: '36CatalogoFianzaExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion	: 'eliminar',
							catalogo	:catalogo,
							clave		:clave,
							catalogoRamo:catalogoRamo
						}),
						callback: TransmiteBorrar
					});
			}
		});
	}
	function TransmiteBorrar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);			
			var mensaje ='';
			if(info.msg=='E'){
				mensaje='El Registro fue Eliminado';
			}else if (info.msg=='N'){
				mensaje='"El Registro no puede ser Eliminado';
			}
			Ext.MessageBox.alert('Mensaje',mensaje,
				function(){
					var catalogo = Ext.getCmp('cboCatalogo').getValue();
					if(catalogo!=3){
						consultaData.load({	
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar'
											
							})
						});	
					}else if (catalogo==3){
						consultaDataSubRamo.load({	
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar'
											
							})
						});
					}
				}	
			);

		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
//------------------------------------------------------------------------------
//------------------------------STORES's----------------------------------------
//------------------------------------------------------------------------------
	var catalogo = new Ext.data.SimpleStore({
		fields: ['clave', 'descripcion'],
		data : [
			//['0','Seleccionar...'],
			['1','Tipo de Movimiento'],
			['2','FE Ramo'],
			['3','FE Sub-Ramo'],
			['4','FE Estatus']
		]
	});
	var catalogoRamo= new Ext.data.JsonStore({
		id: 'catalogoRamo',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'], 
		url: '36CatalogoFianzaExt.data.jsp',
		baseParams: {
			informacion: 'catalogoRamo'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {				
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '36CatalogoFianzaExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [
			{	name: 'clave'},
			{	name: 'descripcion'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});
	
	var consultaDataSubRamo = new Ext.data.JsonStore({
		root : 'registros',
		url : '36CatalogoFianzaExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'			
		},
		hidden: true,
		fields: [
			{	name:	'claveramo'},
			{	name: 'clavesubramo'},
			{	name: 'descripcionramo'},
			{	name: 'descripcionsubramo'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataR(null, null, null);					
				}
			}
		}					
	});
//------------------------------------------------------------------------------
//------------------------------COMPONENTES-------------------------------------
//------------------------------------------------------------------------------
	var gridConsulta = {
		xtype: 'grid',
		store: consultaData,
		id: 'gridConsulta',
		frame:true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		hidden: true,
		columns: [
			{
				header: 'Clave',
				tooltip: 'Clave',
				dataIndex: 'clave',//cc_tipo_movimiento
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex: 'descripcion',
				sortable: true,
				width: 250,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return'<div align=left>'+value+'</div>';
				}
			},
			{
				xtype		: 'actioncolumn',
				header: 'Seleccionar',
				tooltip: 'Seleccionar ',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Modificar';
							return 'modificar';
						}
						,handler: procesarModificar
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[1].tooltip = 'Borrar';
							return 'borrar';
						}
						,handler: procesarBorrar
					}
					
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 360,
		width: 600,		
		frame: true
	}
	
	var gridConsultaSubRamo = {
		xtype: 'grid',
		store: consultaDataSubRamo,
		id: 'gridConsultaSubRamo',	
		frame:true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		hidden: true,
		columns: [
			{
				header: 'Clave',
				tooltip: 'Clave',
				dataIndex: 'clavesubramo',//cc_tipo_movimiento
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Ramo',
				tooltip: 'Descripci�n',
				dataIndex: 'descripcionramo',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return'<div align=left>'+value+'</div>';
				}			
			},
			{
				header: 'Sub Ramo',
				tooltip: 'Descripci�n',
				dataIndex: 'descripcionsubramo',
				sortable: true,
				width: 200,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return'<div align=left>'+value+'</div>';
				}			
			},
			{
				xtype		: 'actioncolumn',
				header: 'Seleccionar',
				tooltip: 'Seleccionar ',				
				sortable: true,
				width: 120,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Modificar';
							return 'modificar';
						}
						,handler: procesarModificarR//Modificar ramo
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[1].tooltip = 'Borrar';
							return 'borrar';
						}
						,handler: procesarBorrar
					}
					
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 360,
		width: 600,		
		frame: true
	}
	
	var elementosForma=[
		{
			xtype				: 'combo',
			id					: 'cboCatalogo',
			name				: 'catalogo',
			hiddenName 		: 'catalogo', 
			fieldLabel		: 'Cat�logos',
			//width				: 100,
			forceSelection	: true,
			allowBlank		: false,
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'clave',
			displayField	: 'descripcion',
			emptyText		: 'Seleccionar...',	
			store				: catalogo,
			tabIndex			: 1,
			listeners: {
				select : function(combo, record, index) {
					var idCatalogo =Ext.getCmp('cboCatalogo').getValue();
					if(idCatalogo==2){
						Ext.getCmp('txtDescripcion').setValue('').hide();
						Ext.getCmp('txtDescripcion_FE').setValue('').show();
						
					}else{
						Ext.getCmp('txtDescripcion_FE').setValue('').hide();
						Ext.getCmp('txtDescripcion').setValue('').show();
					}
					if(idCatalogo!=3){//1,2,4
						Ext.getCmp('cboCatalogoRamo').setValue('').hide();
						Ext.getCmp('txtSubRamo').setValue('').hide('');
						Ext.getCmp('txtClave').setValue('').enable();
						//Ext.getCmp('txtDescripcion').setValue('').show();
					}else if(idCatalogo==3){
						Ext.getCmp('cboCatalogoRamo').setValue('').show();
						catalogoRamo.load();
						//Ext.getCmp('txtClave').setValue('').enable();
						Ext.getCmp('txtClave').hide();
						Ext.getCmp('txtClave1').hide();
						Ext.getCmp('txtClave2').show();
						Ext.getCmp('txtClave2').setValue('').enable();
						Ext.getCmp('txtSubRamo').setValue('').show();
						Ext.getCmp('txtDescripcion').setValue('').hide();
					} if(idCatalogo==2 || idCatalogo==4){////////////////////////////////////////
						Ext.getCmp('txtClave').hide();
						Ext.getCmp('txtClave1').hide();
						Ext.getCmp('txtClave2').show();
						Ext.getCmp('txtClave2').setValue('').enable();
						
					} if(idCatalogo==1){
						Ext.getCmp('txtClave').hide();
						Ext.getCmp('txtClave2').hide();
						Ext.getCmp('txtClave1').show();
						Ext.getCmp('txtClave1').setValue('').enable();
					}
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridConsultaSubRamo').hide();
				}
			}
		},
		{		////visible solo si el value del combo catalogo == 3
			xtype				: 'combo',
			id					: 'cboCatalogoRamo',			
			hiddenName 		: 'catalogoRamo', 
			fieldLabel		: 'Ramo',
			displayField	: 'descripcion',
			valueField		: 'clave',
			hiddenName		: 'catalogoRamo',
			
			forceSelection	: true,
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'clave',
			displayField	: 'descripcion',
			minChars 		: 1,
			emptyText		: 'Seleccionar...',	
			store				: catalogoRamo,
			tpl				: NE.util.templateMensajeCargaCombo,
			hidden 			: true,
			tabIndex			: 2
		},
		{
			xtype			:'textfield',
			name			: 'clave',
			id				: 'txtClave',
			fieldLabel	: 'Clave',
			width			: 200,
			allowBlank	: true,
			tabIndex		:	3
		},
		{
			xtype			:'textfield',
			name			: 'clave1',
			id				: 'txtClave1',//para recibir solo 2 digitos alfanumericos
			fieldLabel	: 'Clave',
			width			: 200,
			allowBlank	: true,
			hidden 			: true,
			maxLength	:2,
			maxLengthText : 'El tama�o de la clave debe de ser de 2 digitos ',
			tabIndex		:	3
		},
		{
			xtype			:'textfield',
			name			: 'clave2',
			id				: 'txtClave2',//para recibir solo 3 digitos alfanumericos
			fieldLabel	: 'Clave',
			width			: 200,
			allowBlank	: true,
			maxLength	:3,
			maxLengthText : 'El tama�o de la clave debe de ser de 3 digitos ',
			hidden 			: true,
			tabIndex		:	3
		},
		{
			xtype			:'textfield',
			name			: 'descripcion',
			id				: 'txtDescripcion',
			fieldLabel	: 'Descripci�n',
			maxLength	: 30,
			//width			: 100,
			allowBlank	: true,
			tabIndex		:	4
		},
		{
			xtype			:'textfield',
			name			: 'descripcion',
			id				: 'txtDescripcion_FE',
			fieldLabel	: 'Descripci�n',
			maxLength	: 20,//?????
			//width			: 100,
			hidden		:true,
			allowBlank	: true,
			tabIndex		:	4
		},
		{
			xtype			:'textfield',
			name			: 'subRamo',
			id				: 'txtSubRamo',
			fieldLabel	: 'Sub Ramo',
			maxLength	: 30,
			//width			: 100,
			allowBlank	: true,
			hidden		:true,
			tabIndex		:	4
		}
		
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Cat�logos',
		frame: true,
		collapsible: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			//anchor: '-20',
			width				: 450,
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				tabIndex		: 5,
				formBind: true,				
				handler: function(boton, evento) {	
					var elemento=Ext.getCmp('txtClave');
					if(Ext.getCmp('cboCatalogo').getValue()!=''){
						if(Ext.getCmp('cboCatalogo').getValue()!=3){//1,2,4
							if(Ext.getCmp('cboCatalogo').getValue()==1){
								if(elemento.getValue().length>2){
									elemento.markInvalid('El tama�o de la clave debe de ser de 2 digitos ');
									return;
								}
							}else{
								if(elemento.getValue().length>3 ){
									elemento.markInvalid('El tama�o de la clave debe de ser de 3 digitos ');
									return;
								}
							}
							fp.el.mask('Enviando...', 'x-mask-loading');		
							consultaData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consultar'
								})
							});					
						}else if(Ext.getCmp('cboCatalogo').getValue()==3){
						//Ex.Msg.alrt('',);
							if(elemento.getValue().length>3 ){
								elemento.markInvalid('El tama�o de la clave debe de ser de 3 digitos ');
								return;
							}
							if(Ext.getCmp('cboCatalogoRamo').getValue()!=''){
								manejoConsulta=1;
							}
							fp.el.mask('Enviando...', 'x-mask-loading');		
							consultaDataSubRamo.load({
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consultar'
								})
							});					
						}
					}else {
						Ext.getCmp('cboCatalogo').markInvalid('Seleccione un cat�logo ');
					}	
				}
			}
		]	
		
	});
	var botones =new Ext.Container ({
		layout: 'table',
		width: '100',
		heigth: 'auto',
		style: 'margin: 0 auto',
		items: [	
					{
						xtype: 'button',
						text: 'Limpiar',
						id: 'limpiar',
						iconCls: 'icoLimpiar',
						handler: function() {
							window.location = '36CatalogoFianzaExt.jsp';					
						}
					},
					{
						xtype: 'button',
						text: 'Guardar',
						id: 'btnAceptar',
						iconCls: 'icoGuardar',
						formBind: true,				
						handler: function(boton, evento) {
						
							var catalogo=Ext.getCmp('cboCatalogo');
							var catalogoR=Ext.getCmp('cboCatalogoRamo');
							var clave1=Ext.getCmp('txtClave1');
							var clave2=Ext.getCmp('txtClave2');
							var txtDescripcion=Ext.getCmp('txtDescripcion');
							var txtDescripcion_FE=Ext.getCmp('txtDescripcion_FE');
							var txtDescripcionR=Ext.getCmp('txtSubRamo');
							var todoCorrecto=false;
							var fpa = Ext.getCmp('forma');
							
							if(fpa.getForm().isValid( )){
							
								if(operacion=='insertar'){//Validaciones para Capturar
									if(catalogo.getValue()==''){
										catalogo.markInvalid('Seleccione un cat�logo')
										return;
									}else if(catalogo.getValue()!=3){
										if(catalogo.getValue()==1){
											if(clave1.getValue()==''){
												clave1.markInvalid('Faltan datos');
												return;
											}else if(clave1.getValue().length>2){
												clave1.markInvalid('El tama�o de la clave debe de ser de 2 digitos ');
											}else if(txtDescripcion.getValue()==''){
												txtDescripcion.markInvalid('Faltan datos');
												return;
											}else {
												todoCorrecto=true;
											}
										}else if(catalogo.getValue()==2){
											if(clave2.getValue()==''){
												clave2.markInvalid('Faltan datos');
												return;
											}else if(clave2.getValue().length>3){
												clave1.markInvalid('El tama�o de la clave debe de ser de 3 digitos ');
											}else if(txtDescripcion_FE.getValue()==''){
												txtDescripcion_FE.markInvalid('Faltan datos');
												return;
											}else {
												todoCorrecto=true;
											}
										}else{
											if(clave2.getValue()==''){
												clave2.markInvalid('Faltan datos');
												return;
											}else if(clave2.getValue().length>3){
												clave.markInvalid('El tama�o de la clave debe de ser numerico de 3 digitos ');
												return;
											}else if(txtDescripcion.getValue()==''){
												txtDescripcion.markInvalid('Faltan datos');
												return;
											}else {
												todoCorrecto=true;
											}
										}
										
									}else if (catalogo.getValue()==3){
										if (catalogoR.getValue()==''){
											catalogoR.markInvalid('Faltan datos');
											return;
										}else if(clave2.getValue()==''){
											clave2.markInvalid('Faltan datos');
											return;
										}else if(clave2.getValue().length>3){
											clave2.markInvalid('El tama�o de la clave debe de ser numerico de 3 digitos ');
											return;
										}else if(txtDescripcionR.getValue()==''){
											txtDescripcionR.markInvalid('Faltan datos');
											return;
										}else {
											todoCorrecto=true;
										}
									}
								}else if(operacion=='modificar'){//Validaciones para modificar
								
									if((catalogo.getValue()!=3)  ){//1,4
										if(catalogo.getValue()==2){
											if(txtDescripcion_FE.getValue()==''){
												txtDescripcion_FE.markInvalid('Faltan datos');
												return;
											}else {
												todoCorrecto=true;
											}

										}else{
											if(txtDescripcion.getValue()==''){
												txtDescripcion.markInvalid('Faltan datos');
												return;
											}else {
												todoCorrecto=true;
											}

										}
									}else if (catalogo.getValue()==3){
										if (catalogoR.getValue()==''){
											catalogoR.markInvalid('Faltan datos');
											return;
										}else if(txtDescripcionR.getValue()==''){
											txtDescripcionR.markInvalid('Faltan datos');
											return;
										}else {
											todoCorrecto=true;
										}
									}
								}

							}
						
							if(todoCorrecto){
								var descripcion=""
								fp.el.mask('Enviando...', 'x-mask-loading');	
								if( Ext.getCmp('cboCatalogo').getValue()!=3){
									descripcion=Ext.getCmp('txtDescripcion').getValue();
								}else if( Ext.getCmp('cboCatalogo').getValue()==3){
									descripcion=Ext.getCmp('txtSubRamo').getValue()
								}
								Ext.Ajax.request({
									url: '36CatalogoFianzaExt.data.jsp',
									params:	Ext.apply(fp.getForm().getValues(),{
										informacion	: 'Aceptar',
										operacion	:operacion,
										clave1:clave1.getValue(),
										clave2:clave2.getValue()/*,
										//catalogo		: Ext.getCmp('cboCatalogo').getValue(),
										//catalogoRamo: Ext.getCmp('cboCatalogoRamo').getValue(),
											clave			:Ext.getCmp('txtClave').getValue(),
										descripcion	:descripcion*/
									}),callback		:procesarAceptar
								});
							}
						}
					}
				]
      
  });
//-----------------Contenedor Principal----------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta	,gridConsultaSubRamo	,
			botones,
			NE.util.getEspaciador(20)
		]
	});
	
	
});