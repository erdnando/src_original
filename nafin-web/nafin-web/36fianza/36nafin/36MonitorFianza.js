var verConsulta;

Ext.onReady(function() {

//_--------------------------------- PARA EL PANEL  GENERAL -------------------------------

 var 	mostrarArbol =  function() {
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			contenedorPrincipalCmp.removeAll();			
			contenedorPrincipalCmp.add(tree);
			contenedorPrincipalCmp.doLayout();
	}


//-------------------------------- STORES -----------------------------------
	var loader = new Ext.tree.TreeLoader({	  
		url: '36MonitorFianza.data.jsp',
		baseParams: {
			informacion: 'Monitor'
		},
		listeners: {
			loadexception: NE.util.mostrarTreeLoaderError,			
			load: {
				fn: function(tree, node, response) {
					if (Ext.util.JSON.decode(response.responseText).success == false) {
						NE.util.mostrarTreeLoaderError(tree, node, response);
					}
				}
			}				
		}
	});	
function abrir(ramo) { 
	
	window.location = '/nafin/36fianza/36epo/36consfianzas.jsp?'+ramo;	
}
	 var tree = new Ext.ux.tree.TreeGrid({
        title: 'Resumen de Fianzas',
        width: 700,
        //height: 500,
				style: 'margin:0 auto;',
        enableDD: false,
			  useArrows: true,				
				animate: true,
				listeners: {
					click: function(nodo, evento) {					
					var valor1=nodo.isLeaf();
					var	valor = nodo.attributes.NOFIANZA;
					var	ramo = nodo.attributes.NORAMO;	
					var	subramo = nodo.attributes.NOSUBRAMO;
					var	estrato = nodo.attributes.NOESTRATO
					var	estatus = nodo.attributes.NOESTATUS;	
					var	datos = "";					
					var nodoEnTurno = nodo;
					while (nodoEnTurno) {
						var nod = nodoEnTurno.attributes.RAMO;
						if(nod!=undefined){
							datos = "ramo="+ramo+"&subramo="+subramo+"&estrato="+estrato+"&estatus="+estatus+"&pantalla=Monitor";
						}						
						nodoEnTurno = nodoEnTurno.parentNode;
					}					
					
					if(valor>0 && valor1==true){					
						abrir(datos);
					}
				}					
				},	
        columns:[
				{
            header: 'Ramo',
            dataIndex: 'RAMO',
            width: 600,
						align: 'left'	
        },
				{
           //xtype: 'actioncolumn',
					 header: 'No. Fianza',
           dataIndex: 'NOFIANZA',
           width: 100,
					 align: 'center'				
				}
				
			 ],
        loader: loader
    });

	
	
//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},		
		items: [
			tree
		]
	});

//-------------------------------- ----------------- -----------------------------------


});