<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>

<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
	
	body{
		background-color:white;
		margin-left: 0px;
		margin-top: 0px;
		margin-right: 0px;
		margin-bottom: 0px;
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-size: 12px;
		font-weight: normal;
		/*color: #999999;*/
		color: #000000;
		text-decoration: none;
		background-position: 0px 0px;
		height:100%;
		background-image: url(../imagenes/fondo_gral.png);
		background-repeat: repeat-x;/**/
	}

</style>
<script type="text/javascript" src="33consultaDesembolsos01ext.js"></script>
<%-- El jsp que contiene el diseño de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<%if(esEsquemaExtJS)  { %>
		<%@ include file="/01principal/menu.jspf"%>
<%} %>
</head>

 <%if(esEsquemaExtJS)  { %>
  
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
      <div id='areaContenido'></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
  
<%}else  {%>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id='areaContenido' style="margin-left: 3px; margin-top: 3px; text-align:center"></div>
<form id='formAux' name="formAux" target='_new'></form>

</body>
<%}%>
</html>