Ext.onReady(function() {
 //OBJETO DATOS GENERALES///////////////////////////////////////////////////////
 var ObjGral = {
	numProc: '0',
	recibo: '-'
 };

 //HANDLERS PARA OBJETOS BUTTON/////////////////////////////////////////////////
 var onAceptar = function(btn){
	var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
	var forma = Ext.getCmp('forma');
	var formaCarga = Ext.getCmp('formaCarga');
	var objRadio = Ext.getCmp("tipoAjuste");
	var tipoModifi = objRadio.getValue().inputValue;

	forma.hide();

	var infoLayout = [
		['1','N�mero de Prestamo','Num�rico','12','Si'],
		['2','N�mero de Cuota','Num�rico','5','Si'],
		['3','Monto Total Venc.','Num�rico','19,2','Si'],
		['4','Fecha Vencimiento','Fecha','dd/mm/aaaa','Si']
	];
	
	var infoLayoutB = [
		['1','N�mero de Pr�stamo','Num�rico','12','Si'],
		['2','Monto de Capital','Num�rico','19,2','Si'],
		['2','Monto de Inter�s','Num�rico','19,2','Si']
		//['2','Monto a Actualizar','Num�rico','19,2','Si']
	];

	
	if(tipoModifi!='F'){
		storeLayoutData.loadData(infoLayout);
	}else{
		storeLayoutData.loadData(infoLayoutB);
	}
	gridLayout.hide();

	contenedorPrincipalCmp.add(fpCarga);
	contenedorPrincipalCmp.add(gridLayout);
	contenedorPrincipalCmp.doLayout();


 };

 var barraProgreso = new Ext.ProgressBar({
	text:'Iniciando Validaci�n...',
   id:'pbar2',
	cls:'left-align',
	width: 690
	//hidden: true
 });

 var ajaxPeticion = function(numProceso, flag){

		var objRadio = Ext.getCmp("tipoAjuste");
		var tipoModifi = objRadio.getValue().inputValue;

		if(flag=='1'){
			gridLayout.hide();
		}

		Ext.Ajax.request({
			url: '13ajustefondojr.data.jsp',
			params: {
				informacion: 'ValidaCargaArchivo',
				numProc: numProceso,
				tipoModifi: tipoModifi,
				flagThread: flag
			},
			callback: procesarSuccessValidaCarga
		});

	};

 var ajaxPeticionZip = function(numProceso, csValido, flag){

	if(flag=='1'){
		pnl.el.mask('Descargando Archivo...', 'x-mask-loading');
	}

	Ext.Ajax.request({
		url: '13ajustefondojr.data.jsp',
		params: {
				informacion:'GeneraZipCargaArchivo',
				numProc: numProceso,
				csValido: csValido,
				flagZipThread: flag
				},
		callback: procesarSuccessGeneraZip
	});

 };

 var ajaxPeticionCarga = function(numProc,tipoModifi,flag){
	if(flag=='1'){
		pnl.el.mask('Procesando...', 'x-mask-loading');
	}

	Ext.Ajax.request({
			url: '13ajustefondojr.data.jsp',
			params: {
				informacion: 'TransferirCargaArchivo',
				numProc: numProc,
				tipoModifi: tipoModifi,
				flagCargaThread: flag
			},
			callback: procesarSuccessTransferCarga
		});

 }

 var procesarSuccessValidaCarga = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var procentaje = 0;
			if(resp.numReg!='0'){
				procentaje = parseFloat(resp.numProcReg)/parseFloat(resp.numReg);
				procentaje = Math.round(procentaje * Math.pow(10, 1)) / Math.pow(10, 1);
			}

			if(resp.numProcReg == '0'){
				var pnlArchivo = Ext.getCmp('pnlArchivo');
				var btnAyuda = Ext.getCmp('btnAyuda');
				fpCarga.setTitle('Proceso de Validaci�n');
				pnlArchivo.hide();
				btnAyuda.hide();
				fpCarga.add(barraProgreso);
				fpCarga.doLayout();
			}


			barraProgreso.updateProgress(procentaje,'Validando '+resp.numProcReg+' de '+resp.numReg);

			if(resp.flagThread == '2'){
				ajaxPeticion(resp.numProc, resp.flagThread);
			}
			if(resp.flagThread == '3'){
				barraProgreso.updateText('Finalizado');
				storeResumValidData.loadData(resp);
				contenedorPrincipalCmp.add(gridResumValid);
				contenedorPrincipalCmp.doLayout();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

 var btnContinuarCarga = function(){
	var objRadio = Ext.getCmp("tipoAjuste");
	var tipoModifi = objRadio.getValue().inputValue;
	var record = storeResumValidData.getAt(0);

	if(record.data['TOTALREG']=='0'){
		Ext.MessageBox.alert('Aviso','No hay registros que procesar');
	}else{

		ajaxPeticionCarga(ObjGral.numProc,tipoModifi,'1');

	}
  }

 var procesarSuccessTransferCarga  = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var cmodel = gridResumValid.getColumnModel();

			if(resp.flagCargaThread=='3'){
				cmodel.setHidden(3,true);
				gridResumValid.setWidth(455);
				gridResumValid.setTitle('Resumen de la Operaci�n');


				var btnContinuarC = Ext.getCmp('btnContinuarC');
				var btnCancelar = Ext.getCmp('btnCancelar');
				var btnGenararPDF = Ext.getCmp('btnGenararPDF');
				btnContinuarC.hide();
				btnGenararPDF.show();
				btnCancelar.setText('Salir');
				pnl.el.unmask();

				var cargaResData = [
					['Fecha de Carga', resp.fecha],
					['Hora de Carga', resp.hora],
					['Usuario de Captura', resp.nomUser]
				];

				storeCargaResData.loadData(cargaResData);
				gridCargaRes.setTitle('N�mero de Recibo: '+resp.recibo);
				ObjGral.recibo = resp.recibo;

				fpCarga.hide();
				contenedorPrincipalCmp.insert(0, NE.util.getEspaciador(20));
				contenedorPrincipalCmp.insert(0,gridCargaRes);
				contenedorPrincipalCmp.doLayout();
			}else if(resp.flagCargaThread=='2'){
				ajaxPeticionCarga(resp.numProc, resp.tipoModifi, resp.flagCargaThread);
			}else {
				pnl.el.unmask();
				Ext.MessageBox.alert('Error Inesperado', 'No se realizo la carga ');
			}


		} else {
			NE.util.mostrarConnError(response,opts);
		}
 }

 var descargaZip = function(grid, rowIndex, colIndex, item, event) {
	var registro = grid.getStore().getAt(rowIndex);
	var numProceso = registro.data['NUMPROC'];
	var csValido = registro.data['CSVALIDO'];

	ajaxPeticionZip(numProceso,csValido,'1');

 }

 var procesarSuccessGeneraZip = function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = Ext.util.JSON.decode(response.responseText);

		if(resp.flagZipThread=='3'){
			pnl.el.unmask();
			var forma = Ext.getDom('formAux');
			forma.action = resp.urlArchivo;
			forma.submit();
		}else if(resp.flagZipThread=='2'){
			ajaxPeticionZip(resp.numProc,resp.csValido,resp.flagZipThread);
		}else  if(resp.flagZipThread=='4'){
			pnl.el.unmask();
			Ext.MessageBox.alert('Error','No se pudo generar el archivo Zip');
		}

	} else {
		NE.util.mostrarConnError(response,opts);
	}
 }


 var procesarSuccessGenerarArchivoPDF = function(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var btnGenararPDF = Ext.getCmp('btnGenararPDF');

		btnGenararPDF.setIconClass('');
		btnGenararPDF.enable();
		btnGenararPDF.el.highlight('FFF700', {duration: 8, easing:'bounceOut'});
		btnGenararPDF.setHandler( function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
		btnGenararPDF.setText("Abrir PDF");
	}else{
		NE.util.mostrarConnError(response,opts);
	}

 }

//STORES////////////////////////////////////////////////////////////////////////
 var storeLayoutData = new Ext.data.ArrayStore({
	  fields: [
		  {name: 'NUMCAMPO'},
		  {name: 'DESC'},
		  {name: 'TIPODATO'},
		  {name: 'LONGITUD'},
		  {name: 'OBLIGATORIO'}
	  ]
 });

 var storeResumValidData = new Ext.data.JsonStore({
	  root : 'registros',
	  fields: [
		  {name: 'CSVALIDO'},
		  {name: 'TOTALREG'},
		  {name: 'MONTOREG'},
		  {name: 'NUMPROC'}
	  ]
 });

 var storeCargaResData = new Ext.data.ArrayStore({
	  fields: [
		  {name: 'etiqueta'},
		  {name: 'informacion'}
	  ]
 });

//OBJETOS AGREGADOS EN CONTENEDORES/////////////////////////////////////////////
 var radioTipoTasa = new  Ext.form.RadioGroup({
	//fieldLabel: 'Operar Descuento Autom�tico',
	hideLabel: true,
	name: 'tipoAjuste',
	id: 	'tipoAjuste',
	columns: 1,
	items: [
				{

					boxLabel:    'De No Pagados a No Pagados Rechazados',
					name:        'tipoAjuste',
					inputValue: 'A',
					checked: true
				},
				{
					boxLabel:    'De Reembolsos a Desembolsos',
					name:        'tipoAjuste',
					inputValue: 'B'
				},
				{
					boxLabel:    'De Desembolsos a Reembolsos',
					name:        'tipoAjuste',
					inputValue: 'C'
				},
				{
					boxLabel:    'De No Pagados Rechazados a No Pagados',
					name:        'tipoAjuste',
					inputValue: 'D'
				},
				{
					boxLabel:    'De Pagados a No Pagados',
					name:        'tipoAjuste',
					inputValue: 'E'
				},
				{
					boxLabel:    'Actualizar Monto del Prepago Total',
					name:        'tipoAjuste',
					inputValue: 'F'
				}
			],
	style: { paddingLeft: '10px' } ,
	listeners:{
		change: function(grupo, radio){
			var lstTipoTasa = radio.inputValue;
			if(lstTipoTasa=='N' || lstTipoTasa=='P'){
				window.location.href='/nafin/13descuento/13pki/13if/13forma8.jsp?lstTipoTasa='+lstTipoTasa;
			}
		}
	}
 });

 var elementosForma = [
	{
		xtype:'fieldset',
		name: 'modificaOpcion',
		id: 'modificaOpcion1',
		columnWidth: 0.5,
		title: 'Modificaci�n a Realizar',
		collapsible: false,
		autoHeight:true,
		labelWidth: 200,
		defaults: {
			anchor: '-20' // leave room for error icon
		},
		defaultType: 'textfield',
		items :[	radioTipoTasa ]
	},
	{
		xtype:'fieldset',
		name: 'cifrasCtrl',
		id: 'cifrasCtrl1',
		columnWidth: 0.5,
		title: 'Cifras de Control',
		collapsible: false,
		autoHeight:true,
		labelWidth: 200,
		defaults: {
			anchor: '-20' // leave room for error icon
		},
		defaultType: 'textfield',
		items :[
			{
				xtype: 'numberfield',
				name: 'numtotalvenc',
				id: 'numtotalvenc1',
				fieldLabel: 'N�mero total de registros',
				allowBlank: false,
				allowDecimals: false,
				allowNegative: false,
				maxValue: 99999,
				width: 20,
				msgTarget: 'side',
				margins: '0 20 0 0',  //necesario para mostrar el icono de error
				renderer: true
			},
			{
				xtype: 'numberfield',
				name: 'montototalvenc',
				id: 'montototalvenc1',
				fieldLabel: 'Monto total de registros',
				allowBlank: false,
				allowNegative: false,
				maxValue: 999999999.99,
				width: 20,
				msgTarget: 'side',
				margins: '0 20 0 0',  //necesario para mostrar el icono de error
				renderer: true
			}
		]
	}

 ];

 var elementosFormCarga = [
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){
						var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');

						if (!gridLayout.isVisible()) {
							gridLayout.show();
						}else{
							gridLayout.hide();
						}

					}
				},{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 100,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo1',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									width: 290,
									emptyText: 'Ruta del Archivo',
									fieldLabel: 'Ruta del Archivo',
									name: 'archivoCesion',
									buttonText: '',
									buttonCfg: {
									  iconCls: 'upload-icon'
									},
									//anchor: '95%',
									regex: /^.*\.(zip|ZIP)$/,
									regexText:'Solo se admiten archivos ZIP'
								},
								{
									xtype: 'hidden',
									id:	'hidExtension',
									name:	'hidExtension',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidNumTotal',
									name:	'hidNumTotal',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidTotalMonto',
									name:	'hidTotalMonto',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidTipoModif',
									name:	'hidTipoModif',
									value:	''
								},
								{
									xtype: 	'button',
									text: 	'Continuar',
									id: 		'btnContinuar',
									iconCls: 'icoContinuar',
									style: {
										  marginBottom:  '10px'
									},
									handler: function(){
										var cargaArchivo = Ext.getCmp('archivo');
										if (cargaArchivo.getValue()==''){
											cargaArchivo.focus();
											return;
										}
										
										if (!cargaArchivo.isValid()){
											cargaArchivo.focus();
											return;
										}
										var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
										var extArchivo = Ext.getCmp('hidExtension');
										var numTotal = Ext.getCmp('hidNumTotal');
										var totalMonto = Ext.getCmp('hidTotalMonto');
										var tipoModif = Ext.getCmp('hidTipoModif');
										var objMessage = Ext.getCmp('pnlMsgValid1');
										
										var objRadio = Ext.getCmp("tipoAjuste");
										var tipoModifi = objRadio.getValue().inputValue;

										objMessage.hide();
										numTotal.setValue(Ext.getCmp('numtotalvenc1').getValue());
										totalMonto.setValue(Ext.getCmp('montototalvenc1').getValue());
										tipoModif.setValue(tipoModifi);

										if (/^.*\.(zip)$/.test(ifile)){
											extArchivo.setValue('zip');
										}

										fpCarga.getForm().submit({
											url: '33ajustefondojr_file.data.jsp',
											waitMsg: 'Enviando datos...',
											success: function(form, action) {
												var resp = action.result;
												var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
												var objMsg = Ext.getCmp('pnlMsgValid1');

												if(!resp.flag){
													//SE MOSTRARAN LEYENDAS DE VALIDACIONES
													objMsg.show();
													//objMsg.body.update('El tama�o del archivo ZIP es mayor al permitido');
													objMsg.body.update(resp.msgError);
												}else if(!resp.flagExt){
													objMsg.show();
													objMsg.body.update('El archivo contenido dentro del ZIP no es un archivo TXT');
												}else if(!resp.flagReg){
													objMsg.show();
													objMsg.body.update('El archivo TXT no puede contener mas de 30000 registros');
												}else{
													ObjGral.numProc = resp.numProceso;
													ajaxPeticion(resp.numProceso, '1');

												}

											},
											failure: NE.util.mostrarSubmitError
										})
									}
								}
							]
						}

					]
				},
				{
					xtype: 'panel',
					name: 'pnlMsgValid',
					id: 'pnlMsgValid1',
					width: 500,
					style: 'margin:0 auto;',
					frame: true,
					hidden: true

				}
			]
		}
		//barraProgreso
	];

 //CONTENEDORES/////////////////////////////////////////////////////////////////
 var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 555,
		title: 'Modificaci�n de Registros',
		frame: true,
		//collapsible: true,
		//titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px; text-align:left;',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptar1',
				iconCls: 'icoContinuar',
				formBind: true,
				handler: onAceptar
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar1',
				iconCls: 'icoLimpiar',
				handler: function() {
					document.location.href  = "33ajustefondojr.jsp";
				}
			}
		]
	});


	 var fpCarga = new Ext.form.FormPanel({
		id: 'formaCarga',
		width: 555,
		title: 'Modificaci�n de Registros',
		frame: true,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px; text-align:left;',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormCarga,
		monitorValid: true
	});

	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'No. de Campo',
				dataIndex : 'NUMCAMPO',
				width : 100,
				sortable : true
			},
			{
				header : 'Descripci�n',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'DESC',
				width : 140,
				sortable : true
			},
			{
				header : 'Tipo de Dato',
				dataIndex : 'TIPODATO',
				width : 100,
				sortable : true
			},
			{
				header : 'Longitud',
				dataIndex : 'LONGITUD',
				width : 99,
				sortable : true
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 100,
				sortable : true
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 555,
		height: 173,
		title: '<div align="left">Nota:<br>Se deber� cargar un archivo de texto(.txt) comprimido (.zip), el cual deber� contener un layout con los datos de los registros que se ajustar�n; los datos deben estar separados por pipe  "|"</div>',
		frame: true
	});


	var gridResumValid = new Ext.grid.GridPanel({
		id: 'gridResumValid1',
		store: storeResumValidData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header : ' ',
				dataIndex : 'CSVALIDO',
				width : 155,
				sortable : true,
				renderer:  function (causa, columna, registro){
						var info;
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						if(causa=='S')
							info = 'Registro sin Errores';
						if(causa=='N')
							info = 'Registro con Errores';
						return info;
					}
			},
			{
				header : 'Totales',
				tooltip: 'N�mero total de registros',
				dataIndex : 'TOTALREG',
				width : 142,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Monto Total',
				tooltip: 'Monto total de registros',
				dataIndex : 'MONTOREG',
				width : 142,
				sortable : true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype:	'actioncolumn',
				header : 'Descargar', tooltip: 'Descarga de archivo zip',
				id: 'columnDescarga',
				width:	100,	align: 'center', hidden: false,
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver Detalle';
							return 'icoZip';

						},
						handler:	descargaZip
					}
				]
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 555,
		autoHeight : true,
		title: 'Resumen de Validaci�n',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Continuar',
					id :'btnContinuarC',
					handler: btnContinuarCarga
				},
				{
					text: 'Generar PDF',
					id: 'btnGenararPDF',
					hidden: true,
					handler: function(boton, evento) {

						var registrosEnviar = [];
						var regEnviarRecibo = [];
						storeResumValidData.each(function(record){
							registrosEnviar.push(record.data);
						});

						storeCargaResData.each(function(record){
							regEnviarRecibo.push(record.data);
						});

						registrosEnviar = Ext.encode(registrosEnviar);
						regEnviarRecibo = Ext.encode(regEnviarRecibo);

						boton.disable();
						boton.setIconClass('loading-indicator');

						Ext.Ajax.request({
							url: '13ajustefondojr.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								registros: registrosEnviar,
								registrosRecibo: regEnviarRecibo,
								recibo: ObjGral.recibo,
								informacion: 'ImprimirPDF',
								tipoArchivo: 'PDF'
							}),
							callback: procesarSuccessGenerarArchivoPDF
						});
					}

				},
				'-',
				{
					text: 'Cancelar',
					id: 'btnCancelar',
					handler: function(btn){
						window.location.href='33ajustefondojr.jsp';
					}
				}
			]
		}
	});


	var gridCargaRes = new Ext.grid.GridPanel({
		id: 'gridCargaRes1',
		store: storeCargaResData,
		margins: '20 0 0 0',
		hideHeaders : true,
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 150,
				sortable : true
			},
			{
				header : 'Informacion',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'informacion',
				width : 230,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 400,
		style: 'margin:0 auto;',
		autoHeight : true,
		title: 'N�mero de Recibo:',
		frame: true
	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20)
		]
	});


});