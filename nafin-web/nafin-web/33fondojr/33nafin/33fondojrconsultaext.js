Ext.onReady(function() { 

	var dt = Ext.util.Format.date(Date(),'d/m/Y');
	var subtotal = false;
	var fecha ;
/*----------------------------------------- Hanlers -----------------------------------------*/
			
	var procesarConsultaData = function(store, arrRegistros, opts) 
	{
		pnl.el.unmask();
		var fp = Ext.getCmp('forma');
		
		/*si el arrRegistros es distinto de nulo*/
		if (arrRegistros != null )
		{
			/*si el grid no es visible, lo muestro*/
			if (!grid.isVisible()) 
			{ 
				grid.show();
			}
			
			/*declaro botones y una variable para manipular el grid*/
			var btnGenerarArchivo = Ext.getCmp("btnGenerarArchivo");
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDF'); 
			var btnBajarArchivo 	 = Ext.getCmp('btnBajarArchivo');
			var btnBajarPDF 		 = Ext.getCmp('btnBajarPDF');
			var el = grid.getGridEl();
			btnBajarArchivo.hide();
			btnBajarPDF.hide();
			
			
			if (store.getTotalCount() > 0 ) 	// si la consulta tiene un store
			{	btnGenerarArchivo.enable();	// habilito los botones
				btnImprimirPDF.enable();
				el.unmask();						//quito la mascara
				
				if (subtotal == true)			// si pidio ver los subtotales realizo una peticion ajax y muestro el grid
				{
					consultaDataTotales.load({ params: Ext.apply(fp.getForm().getValues(), {informacion: 'ConsultaSubtotales'})  });
					gridTotales.show();
					
				}
				else									//sino marco los subtotales si el grid es visible, lo escondo
				{
					if (gridTotales.isVisible()) 
					{ 
						gridTotales.hide();
					}
				}
				
			}
			
			/*Si el registro viene vacio desactiva los botones para generar archivo*/
			
			if (arrRegistros == '')  
			{
				grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				btnGenerarArchivo.disable();
				btnImprimirPDF.disable();
			}
			
			/* Sino activa los botones para generar archivo */
			//else if (arrRegistros != null)
			else if (arrRegistros != '')
			{
				btnGenerarArchivo.enable();
				btnImprimirPDF.enable();
			}
		}	
	}
	
	
	var muestraDetalle = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		var fecha = registro.get('FECHAPROBABLEPAGO'); 
		
		var gridDetalle = new Ext.grid.GridPanel({
		id: 'gridDetalle',
		store: resDetalleDias,		
		columns: [
			{
				header: 'N�mero Operacion',
				tooltip: 'N�mero  Operacion',
				dataIndex: 'NUMERO_OPERACION',
				sortable: true,
				width: 110,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header: 'Detalle',
				tooltip: 'Detalle',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 350,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 65,
		width: 466,
		style: 'margin:0 auto;',
		frame: false
	});
		var winCambia = new Ext.Window ({
										id:	'winCambia',
										autoHeight: true,
										width: 490,
										modal: true,
										closeAction: 'destroy',
										resizable: false,
										//layout: 'fit',
										title:'Detalle D�as Vencimiento',
										items: gridDetalle,
										monitorValid: true,
										buttonAlign:	'center',
										buttons: [
												
												{
													//xtype: 'button',
													text: 'Salir',
													id: 'Salir',
													
													formBind: true,
													handler: function(boton, evento) {
														Ext.getCmp('winCambia').destroy();
													}
												}
										]
										
									}).show();
		resDetalleDias.load({
			params:{
				fechaProbablePago : fecha
			}
		});
	}
	
	function leeRespuesta(){
		window.location = '33fondojrconsultaext.jsp';
	}
	
	
	function verificaPanel()
	{
		var myPanel = Ext.getCmp('forma');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype != 'label')	{	//El metodo isValid() no aplica para los tipos de objetos label.
				if (!panelItem.isValid())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}
	var procesarSuccessFailureGenerarCSV = function(opts, success, response) 
	{
		 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		 var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		 btnGenerarArchivo.setIconClass('');
		 btnBajarArchivo.setIconClass('icoXls');
		 
		 
		 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		 {
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler(function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		 } else {
			 btnGenerarArchivo.enable();
			 NE.util.mostrarConnError(response,opts);
		}
	}
							
	
	var procesarSuccessFailureGenerarPDF = function(opts, success, response) 
   {
		var btnBajarPDF = Ext.getCmp('btnBajarPDF');
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnBajarPDF.setIconClass('icoPdf');
		btnImprimirPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureNombrePyme = function(opts, success, response) 
   {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			var nombreP = Ext.util.JSON.decode(response.responseText).nombrePyme;
			if(nombreP != null && nombreP != "")
			{
				var nombre = Ext.getCmp("id_clienteSirac");			
				nombre.setValue(nombreP); //sin comillas
			}
		}
	}
	
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success)
		{
			if (jsonData.msgRegistros!='')
			{
				Ext.Msg.alert('Buscar',jsonData.msgRegistros);
				Ext.getCmp('fBusqAvanzada').getForm().reset();
				return;
			}
			else
			{
				//si trajo datos entonces que muestre el combo
				Ext.getCmp('id_nombreAvanz').show();
				Ext.getCmp('btnAceptar').show();
				Ext.getCmp('btnCancelar').show();
			}

			Ext.getCmp('id_nombreAvanz').focus();
		}
	}
	
/*--------------------------------- STORE'S -------------------------------*/
	
	/****** Store�s Grid�s *******/	
	var consultaData = new Ext.data.JsonStore
	({		
		root:'registros',	
		url:'33fondojrconsultaext.data.jsp',	
		baseParams:{informacion:'ConsultaGrid'},	
		
			totalProperty: 'total',		
				fields: [	
					{name: 'FEC_VENC'}, 	
					{name: 'FEC_FIDE'},
					{name: 'FEC_CLIENTE'},
					{name: 'PRESTAMO'},
					{name: 'NUM_CLIENTE'},
					{name: 'NOMBRECLIENTE'},
					{name: 'NUM_CUOTA'},
					{name: 'DIAS_TRANSCURRIDOS_VENCIMIENTO'},
					{name: 'DIAS_LIMITE'},
					{name: 'CLAVE_ESTATUS'},
					{name: 'FECHAPROBABLEPAGO'},
					{name: 'AMORT'},
					{name: 'INTERES'},
					{name: 'SALDO'},
					{name: 'ESTATUS'},
					{name: 'ORIGEN_DESC'},
					{name: 'ESTATUS_VAL'},
					{name: 'PREPAGO_VALIDACION'} 
					]
				,
		messageProperty: 'msg',
		autoLoad: false,
		listeners: 
		{
			load: procesarConsultaData,
			exception: NE.util.mostrarDataProxyError
		}
		
	});
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '33fondojrconsultaext.data.jsp',
		baseParams: {
			informacion: 'ConsultaSubtotales'
		},
		hidden: true,
		fields: [	
			{name: 'TOTAL_AMORT'},			
			{name: 'TOTAL_INTERES'},
			{name: 'TOTAL_SALDO'}
							
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});
	/****** End Store�s Grid�s *******/
	
	var resDetalleDias = new Ext.data.JsonStore({
		root : 'registros',
		url : '33fondojrconsultaext.data.jsp',
		baseParams: {
			informacion: 'DetalleDiasVencimiento'
		},
		fields: [
			{name: 'HAY_DETALLE'},
			{name: 'NUMERO_OPERACION'},
			{name: 'DESCRIPCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false		
	});

	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg' ],
		url : '33fondojrconsultaext.data.jsp',
		baseParams: { informacion: 'CatalogoBuscaAvanzada'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoRegistros = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['P','Pagados'],
			['NP','No Pagados'],
			['R','Reembolsos'],
			['T','Prepagos Totales'],
			['RF','Desembolsos'],
			['RC','Recuperaci�n de Fondo']
		 ]
	}) ;
	
	
	var catalogoValidacion = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['P','En Proceso'],
			['A','Validado'],
			['R','Rechazado']
			
		 ]
	}) ;
	
	var  catalogoOrden= new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['E','Estatus'],
			['V','Fecha Autorizaci�n Nafin'],
			['F','Fecha Operaci�n']
			
		 ]
	}) ;
	
	 
	/*------------------------------------------ Grids ------------------------------------------*/
var color = "black"; 
	var grid = new Ext.grid.GridPanel
	({
		title:'',
		id: 'grid',
		store: consultaData,
		style: ' margin:0 auto;',
		stripeRows: true,
		loadMask:false,
		hidden: true,
		height:430,
		width:900,	
		frame:true, 
		header:true,
		columns: [
		{
				header:		'Fecha Amortizaci�n Nafin',
				tooltip:		'Fecha Amortizaci�n Nafin',			
				dataIndex:	'FEC_VENC',	
				sortable:true,	
				resizable:true,	
				width:80
				
			},
		
			{
				header:		'Fecha Operaci�n',
				tooltip:		'Fecha Operaci�n',			
				dataIndex:	'FEC_FIDE',	
				sortable:true,	
				resizable:true,	
				width:80
				
			},
			{
				header:		'Fecha Pago Cliente FIDE',
				tooltip: 	'Fecha Pago Cliente FIDE',
				dataIndex:	'FEC_CLIENTE',	
				sortable:true,
				resizable:true,	
				width:80, 
				align:"center"
				
			},
			{
				header:		'Pr�stamo', 
				tooltip: 	'Pr�stamo',	
				dataIndex: 	'PRESTAMO',	
				sortable:true, 
				width:95, 	
				align:"center"
				
			},
			{
				header:		'N�m. Cliente SIRAC',
				tooltip:		'N�m. Cliente SIRAC',
				dataIndex:	'NUM_CLIENTE',		
				sortable:true, 
				width:95, 
				align:"center"
				
			},
			{
				header:		'Cliente',
				tooltip:		'Cliente',	
				dataIndex:  'NOMBRECLIENTE',	
				sortable:true,	
				width:100,	
				align:"left"				
			},
			
			
			{
				header:		'N�m. Cuota a Pagar', 		
				tooltip:		'N�m. Cuota a Pagar',			
				dataIndex:  'NUM_CUOTA',		
				sortable:true,	
				width:40,	
				align:"center"
				
			},
			{
				header:		'', 		
				dataIndex:  'FECHAPROBABLEPAGO',		
				hidden:true
				
			},
			{
				xtype:	'actioncolumn',
				header:		'D�as Transcurridos <br>de Vencimiento', 		
				tooltip:		'D�as Transcurridos <br> de Vencimiento',			
				dataIndex:  'DIAS_TRANSCURRIDOS_VENCIMIENTO',		
				sortable:true,	
				width:100,	
				align:"center",
				
				renderer: function(value, metadata, record, rowindex, colindex, store){
				/////////////////////////////////////////////////////////////////////
				var dias_ = record.get('DIAS_TRANSCURRIDOS_VENCIMIENTO');
				var dias_limite = record.get('DIAS_LIMITE');
				var estatus = record.get('CLAVE_ESTATUS');
				var fechaPropago = record.get('FECHAPROBABLEPAGO');
				var valor;
			
				
				if('NP' != estatus){
					color = "black";//return color;
				}
				
				if( dias_ != 0 &&  dias_limite!=0  ){ 
					if(dias_ > dias_limite){
						color = 'red';
					}
				}
								
				if(record.get('PREPAGO_VALIDACION')=='N') {
				
					var diasTranscurridos;
					if( 'NP' == (record.get("CLAVE_ESTATUS")))
						diasTranscurridos = record.get("DIAS_TRANSCURRIDOS_VENCIMIENTO")
					else
						diasTranscurridos = 'N/A';
				
				
					if('red' != color){
						value = diasTranscurridos;
						
					}else  if(fechaPropago != null){					
						value=  diasTranscurridos;
						
					}else  if(diasTranscurridos =='N/A' ){
						value=  diasTranscurridos;
					}									
				}else  {
					value =  'N/A';
				
				}				
				return '<span style="color:'+color+';">' + value + '</span>';
									
				},//fin renderer				
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
									
							
							if (record.get('DIAS_TRANSCURRIDOS_VENCIMIENTO') != 0 && color =='red'){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else	{
								return '';
							}
							
						},
						
						handler:	muestraDetalle
					}
				]
				
			},
			{
				header:		'', 		
				tooltip:		'',			
				dataIndex:  'DIAS_LIMITE',		
				hidden:true
				
			},
			{
				header:		'', 		
				tooltip:		'',			
				dataIndex:  'CLAVE_ESTATUS',		
				hidden:true
				
			},
			{
				header:		'Capital',
				tooltip:		'Capital',	
				dataIndex:	'AMORT',		
				sortable:true,	
				width:90,	
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
				
			},
			{
				header:		'Interes',
				tooltip: 	'Interes',
				dataIndex:	'INTERES',	
				sortable:true,
				resizable:true,	
				width:90, 
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
				
			},
			{
				header:		'Total a Pagar', 
				tooltip: 	'Total a Pagar',	
				dataIndex: 	'SALDO',	
				sortable:true, 
				width:90, 	
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header:		'Estatus del Registro',
				tooltip:		'Estatus del Registro',
				dataIndex:	'ESTATUS',		
				sortable:true, 
				width:100, 
				align:'center'
				
			},
			
			{
				header:		'Origen', 		
				tooltip:		'Origen',			
				dataIndex:  'ORIGEN_DESC',		
				sortable:true,	
				width:100,	
				align:'center'
				
			},
			{
				header:		'Estatus de Validaci�n',
				tooltip:		'Estatus de Validaci�n',	
				dataIndex:	'ESTATUS_VAL',		
				sortable:true,	
				width:100,	
				align:'center' //
				
			}
			
		],
		bbar: {
					xtype: 'paging',
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion1',
					displayInfo: true,
					store: consultaData,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "",
					
					items: ['->','-',
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										totalProperty:'total',
										Ext.Ajax.request({
											url: '33fondojrconsultaext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'GenerarCSV'}),
											callback: procesarSuccessFailureGenerarCSV
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivo',
									hidden: true
								},
								'-',
								{
				
									xtype: 'button',
									text: 'Imprimir PDF',	
									id: 'btnImprimirPDF',
									handler: function(boton, evento) 
									{
										boton.disable();
										boton.setIconClass('loading-indicator');
										totalProperty:'total',
										Ext.Ajax.request({
											url: '33fondojrconsultaext.data.jsp',
											params: Ext.apply (fp.getForm().getValues(),{
												informacion: 'GenerarPDF',
												start : 0,
												limit: 15 
												
												}),
												
											callback: procesarSuccessFailureGenerarPDF
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarPDF',
									hidden: true
								},
								'-'
					]
				}

		
	});
	

	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		title:'',
		stripeRows: true,
		height: 55 ,
		width:  900,
		frame: false,
		hidden: true,
		columns: [
			{
										
				header : '',
				tooltip: '',
				dataIndex : '',
				sortable: true,
				width: 545,
				resizable: true,				
				align: 'right'	
			},	
			{							
				header : 'Subtotales',
				tooltip: 'Subtotales',
				dataIndex : '',
				sortable: true,
				width: 80,
				resizable: true,				
				align: 'right'	
			},		
			{							
				header : '',
				tooltip: '',
				dataIndex : 'TOTAL_AMORT',
				sortable: true,
				width: 90,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : '',
				tooltip: '',
				dataIndex : 'TOTAL_INTERES',
				sortable: true,
				width: 90,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : '',
				tooltip: '',
				dataIndex : 'TOTAL_SALDO',
				sortable: true,
				width: 90,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		]
	});
	
	
/*******************Componentes*******************************/
		
		var elementsFormBusq = [
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: 
			{ 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 'displayfield',
			value: '  '		
		},
		
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		
		
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 8px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			
			  buttons: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	85,
						handler: function(boton, evento) 
						{
							//aqui los escondo
							Ext.getCmp('id_nombreAvanz').hide();
							Ext.getCmp('btnAceptar').hide();
							Ext.getCmp('btnCancelar').hide();
						
							catalogoNombreData.load({	
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{	})																	
								});
						}
					},	
					//boton de cancelar
					{
						xtype: 'button',
						text: 'Cancelar',
						iconCls: 'icoLimpiar',
						handler: function()
						{					
							var ventana = Ext.getCmp('winBusqAvan');
							fpBusqAvanzada.getForm().reset();
							ventana.hide();
						}				
					}
			  ]
		},
		
		{
			xtype: 'displayfield',
			value: '  '		
		},
		{
			xtype: 'displayfield',
			value: '  '		
		},
		
		{		
				xtype: 'combo',
				name: 'nombreAvanz',
				id: 'id_nombreAvanz',
				hiddenName: 'nombreAvanz',
				fieldLabel: 'Nombre',
				hidden: true,
				forceSelection: true,
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				store: catalogoNombreData
				
		},	
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',			 
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			
			  buttons: [
					{
						xtype: 'button',
						text: 'Aceptar',
						id: 'btnAceptar',
						hidden: true,
						width: 	85,
						handler: function(boton, evento) {
							//pregunto si viene informacion
							var combo  = Ext.getCmp('id_nombreAvanz');
							var indice = catalogoNombreData.findExact('clave',combo.getValue());
							
							//trae un registro del store
							var record = catalogoNombreData.getAt(indice);
							var numero = Ext.getCmp('id_numSirac');
							var nombre = Ext.getCmp('id_clienteSirac');
							numero.setValue(record.data['clave']);
							nombre.setValue(record.data['descripcion']);
							
							var ventana = Ext.getCmp('winBusqAvan');
							fpBusqAvanzada.getForm().reset();
							ventana.hide();						
							
						}
					},
					{
						xtype: 'button',
						text: 'Cancelar',
						id: 'btnCancelar',
						hidden: true,
						handler: function()
						{					
							var ventana = Ext.getCmp('winBusqAvan');
							fpBusqAvanzada.getForm().reset();
							ventana.hide();
						}				
					}
			  ]
		}
	];
	
	
		var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true
	});
	
	
	var elementosForma = [
	{
		xtype: 'panel',
		labelWidth:166,
		layout: 'form',
		labelAlign:'left',
		items: [
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			items: [
			{
				xtype: 'datefield',
				name: 'txtFchVenc1',
				id: 'txtFchVenc1',
				allowBlank: true,
				startDay: 0,
				width: 130,
				msgTarget: 'side',
				minValue: '01/01/1901',
				vtype: 'rangofecha', 
				campoFinFecha: 'txtFchVenc2',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},
			{
				xtype: 'displayfield',
				value: 'a',
				width: 24
			},
			{
					xtype: 'datefield',
					name: 'txtFchVenc2',
					id: 'txtFchVenc2',
					minValue: '01/01/1901',					
					allowBlank: true,
					startDay: 1,
					width: 130,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFchVenc1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
			}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
			{
				xtype: 'datefield',
				name: 'txtFchOper1',
				id: 'txtFchOper1',
				minValue: '01/01/1901',				
				allowBlank: true,
				startDay: 0,
				width: 130,
				msgTarget: 'side',
				vtype: 'rangofecha', 
				campoFinFecha: 'txtFchOper2',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},
			{
				xtype: 'displayfield',
				value: 'a',
				width: 24
			},
			{
				xtype: 'datefield',
				name: 'txtFchOper2',
				id: 'txtFchOper2',
				minValue: '01/01/1901',				
				allowBlank: true,
				startDay: 1,
				width: 130,
				msgTarget: 'side',
				vtype: 'rangofecha', 
				campoInicioFecha: 'txtFchOper1',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			}
			]
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Pago FIDE',
			combineErrors: false,
			msgTarget: 'side',
			items: [
			{
				xtype: 'datefield',
				name: 'txtFchPago1',
				id: 'txtFchPago1',
				minValue: '01/01/1901',				
				allowBlank: true,
				startDay: 0,
				width: 130,
				msgTarget: 'side',
				vtype: 'rangofecha', 
				campoFinFecha: 'txtFchPago2',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},
			{
				xtype: 'displayfield',
				value: 'a',
				width: 24
			},
			{
					xtype: 'datefield',
					name: 'txtFchPago2',
					id: 'txtFchPago2',
					minValue: '01/01/1901',
					allowBlank: true,
					startDay: 1,
					width: 130,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFchPago1',
					margins: '0 20 0 0'  
			}]
			
		},
		{
				xtype: 'combo',
				id: 'id_estatus_reg',
				name: 'estatus_reg',
				hiddenName: 'estatus_reg',
				fieldLabel: 'Estatus Registro',
				width:145,
				mode: 'local',
				emptyText: 'Seleccionar ...',  
				displayField: 'descripcion',
				valueField: 'clave',
				forceSelection: true,
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoRegistros,
				listeners: 
				{
						blur: {
							fn: function(combo) {
								//aqui hago la verificacion para habilitar el checkbox
								var sub = Ext.getCmp("id_subtotales");
								if(combo.getValue()!='')
										{	sub.enable(); }
								else 	{	
									sub.disable();
									//subtotal = false;
								}
								
							}
						}
				}
	   },
		{
				xtype: 'combo',
				name: 'estatus_val',
				id: 'id_estatus_val',
				hiddenName: 'estatus_val',
				fieldLabel: 'Estatus Validaci�n',
				width:105,
				emptyText: 'Seleccionar ...',  
				forceSelection: true,
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				store: catalogoValidacion
	   },
		{
				xtype        	: 'numberfield',
				id					: 'id_prestamo',
				name    			: 'prestamo',
				fieldLabel     : 'N�m. Pr�stamo',
				allowBlank		: true,
				width				: 80,
				maxLength 		: 10,
				msgTarget		: 'side',
				margins			: '0 20 0 0'  //necesario para mostrar el icono de error
			},
				
		{	
			xtype: 'panel',
			layout: 'hbox',
			anchor:'100%',
			items: 
			[
				{
					xtype:'panel', 
					layout:'form', 
					msgTarget:'side',
					bodyStyle: 'padding: 2px',
					items:[{
							xtype        	: 'numberfield',
							id					: 'id_numSirac',
							name    			: 'numSirac',
							fieldLabel     : 'N�m.Cliente SIRAC',
							allowBlank		: true,
							width				: 90,
							maxLength 		: 12,
							msgTarget		: 'side',
							margins			: '0 20 0 0',  //necesario para mostrar el icono de error
							listeners: 
							{
									blur: {
										fn: function() {
											//aqui hago la verificacion para habilitar el checkbox
											var nombre = Ext.getCmp("id_clienteSirac");
											//aqui va la peticion Ajax informacion:'buscaNombreProveedor'
											Ext.Ajax.request({
													url: '33fondojrconsultaext.data.jsp',
													params: Ext.apply(fp.getForm().getValues(),{
														informacion: 'buscaNombreProveedor'}),
														
													callback: procesarSuccessFailureNombrePyme
												});
												
										}
									}
							}
					}]
				},
				{
					xtype:'panel',
					//layout: 'form',
					msgTarget:'side',
					bodyStyle: 'padding: 2px',
					items: 
					[
						{
							xtype: 'textfield',
							id: 'id_clienteSirac',
							name: 'clienteSirac',
							fieldLabel:'',
									
							allosBlank: true,
							maxLength: 40,
							width: 220,
							msgTarget:'side',
							margins: ' 0 20 0 0'
						}
					]
				},	
				{
					xtype: 'panel',
					msgTarget:'side',
					bodyStyle: 'padding: 2px',
					items:
					[
						{
							xtype: 'button',
							text: 'B�squeda Avanzada...',
							id: 'btnBusqAv',
							handler: function(boton, evento) 
							{
							
								var ventana = Ext.getCmp('winBusqAvan');
								if (ventana) {
									ventana.show();
								} 
								else 
								{
									new Ext.Window({
											title: 			'B�squeda Avanzada',
											layout: 			'fit',
											width: 			400,
											height: 			400,
											minWidth: 		400,
											minHeight: 		400,
											buttonAlign: 	'center',
											id: 				'winBusqAvan',
											closeAction: 	'hide',
											items: 	fpBusqAvanzada
										}).show();
								}
							}
						}
					]
				}
			]
		},

		{
				xtype: 'combo',
				id: 'id_ordenar',
				name: 'ordenar',
				hiddenName: 'ordenar',
				fieldLabel: 'Ordenar por',
				width:145,
				mode: 'local',
				emptyText: 'Seleccionar ...',  
				displayField: 'descripcion',
				valueField: 'clave',
				forceSelection: true,
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoOrden
				
	   },
		{
			xtype: 'checkbox',
			name: 'subtotales',
			id: 'id_subtotales',
			hiddenName:'subtotales',
			fieldLabel: 'Agregar Subtotales ',
			disabled : true,
			listeners: 
			{
				check: function(field) {
				//peticion ajax
					subtotal=!(subtotal);//subtotal.setValue(true);
				}
			
			}
				
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: 'Cr�ditos con Vencimientos',
			combineErrors: false,
			//msgTarget: 'side',
			defaults: {	msgTarget: 'side',	anchor: '-20'	},
			items: [
			{
				xtype: 'displayfield',
				value: 'Desde',
				width: 34
			},
			{
				xtype        		: 'numberfield',
				name			 		: 'diasDesde',
				id 					: 'diasDesde',
				allowBlank   		: true,
				width:45,
				maxLength : 3,
				msgTarget: 'side',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},
			{
				xtype: 'displayfield',
				value: 'd�as',
				width: 50
			},
			{
				xtype: 'displayfield',
				value: 'Hasta',
				width: 34
			},
			{
				xtype        		: 'numberfield',
				name			 		: 'diasHasta',
				id 					: 'diasHasta',
				allowBlank   		: true,
				width:45,
				maxLength : 3,
				msgTarget: 'side',
				margins: '0 20 0 0'   //necesario para mostrar el icono de error
			},
			{
				xtype: 'displayfield',
				value: 'd�as',
				width: 24
			}
			]
		}
		
		
		]
	}];
			
	var textoPrograma = new Ext.Container({
		layout: 'table',		
		id: 'textoPrograma',							
		width:	'700',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensaje', 	value: '' }				
		]
	});
	
	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var  jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
						
				Ext.getCmp('mensaje').setValue(jsonValoresIniciales.programa);
				Ext.getCmp('textoPrograma').show();
			}			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var fp = new Ext.form.FormPanel
	({
		id: 'forma',
		width: 680,
		style: ' margin:0 auto;',
		title:	'<div align="center">Consulta Hist�rica de Movimientos</div>',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					//grid.hide();
					var fechaVenc1 = Ext.getCmp('txtFchVenc1');
					var fechaVenc2 = Ext.getCmp('txtFchVenc2');
					var fechaOper1 = Ext.getCmp('txtFchOper1');
					var fechaOper2 = Ext.getCmp('txtFchOper2');
					var fechaPago1 = Ext.getCmp('txtFchPago1');
					var fechaPago2 = Ext.getCmp('txtFchPago2');
					
					var diasDesde = Ext.getCmp('diasDesde');
					var diasHasta = Ext.getCmp('diasHasta');
					var prestam   =  Ext.getCmp('id_prestamo');
					var numsir    =  Ext.getCmp('id_numSirac');
					
					
					//vuelve a empezar!!! //Fodea 027-2011(E)
					if (Ext.isEmpty(fechaVenc1.getValue())  && Ext.isEmpty(fechaVenc2.getValue()) ) {
						if( Ext.isEmpty(diasDesde.getValue()) &&  Ext.isEmpty(diasHasta.getValue()) && Ext.isEmpty(prestam.getValue()) && Ext.isEmpty(numsir.getValue()) ) {
							Ext.MessageBox.alert('Mensaje de p�gina web','Debe especificar el rango de las Fechas de Vencimiento y/o el rango de las Fechas de Operaci�n' );
							return;
						}
					}
					
					//Fodea 027-2011(S)
					
					if (!Ext.isEmpty(fechaVenc1.getValue()) || !Ext.isEmpty(fechaVenc2.getValue()) ) {
						if(Ext.isEmpty(fechaVenc1.getValue()))	{
							fechaVenc1.markInvalid('El valor de la Fecha de Vencimiento inicial es requerido');
							fechaVenc1.focus();
							return;
						}else if (Ext.isEmpty(fechaVenc2.getValue())){
							fechaVenc2.markInvalid('El valor de la Fecha de Vencimiento final es requerido.');
							fechaVenc2.focus();
							return;
						}
					}
					
					if (!Ext.isEmpty(fechaPago1.getValue()) || !Ext.isEmpty(fechaPago2.getValue()) ) {
						if(Ext.isEmpty(fechaPago1.getValue()))	{
							fechaPago1.markInvalid('El valor de la Fecha de Pago FIDE inicial es requerido.');
							fechaPago1.focus();
							return;
						}else if (Ext.isEmpty(fechaPago2.getValue())){
							fechaPago2.markInvalid('El valor de la Fecha de Pago FIDE final es requerido.');
							fechaPago2.focus();
							return;
						}
					}
					
					if (!Ext.isEmpty(fechaOper1.getValue()) || !Ext.isEmpty(fechaOper2.getValue()) ) {
						if(Ext.isEmpty(fechaOper1.getValue()))	{
							fechaOper1.markInvalid('El valor de la Fecha de Operaci�n inicial es requerido.');
							fechaOper1.focus();
							return;
						}else if (Ext.isEmpty(fechaOper2.getValue())){
							fechaOper2.markInvalid('El valor de la Fecha de Operaci�n final es requerido.');
							fechaOper2.focus();
							return;
						}
					}
					/**********************/
						if (!Ext.isEmpty(diasDesde.getValue()) || !Ext.isEmpty(diasHasta.getValue()) ) {
							// Si el dia inicial viene especificado, realizar validacion
							if(Ext.isEmpty(diasDesde.getValue()))	{
								diasDesde.markInvalid('El D�a Inicial del Vencimiento del Cr�dito es requerido.');
								diasDesde.focus();
								return;
							}else if (Ext.isEmpty(diasHasta.getValue())){
								diasHasta.markInvalid('El D�a Final del Vencimiento del Cr�dito es requerido.');
								diasHasta.focus();
								return;
							}
							
							if (diasDesde.getValue() > diasHasta.getValue()) {
								Ext.MessageBox.alert('Mensaje de p�gina web','El D�a Inicial no puede ser mayor al D�a Final del Vencimiento del Cr�dito.' );
								return;
							}	
						
					}
					
					pnl.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{	
						operacion: 'Generar',
						start : 0,
						limit: 15 })
					});

				} //fin handler
			}, //fin boton Consultar
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					leeRespuesta();
				}
			}
		]
	});
	
	
	var pnl = new Ext.Container
	({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			NE.util.getEspaciador(20),
			textoPrograma,
			NE.util.getEspaciador(20),
			fp,	NE.util.getEspaciador(20),			
			grid,
			gridTotales,
			NE.util.getEspaciador(10)
		]
	});
	
	
	
	Ext.Ajax.request({
		url: '33fondojrconsultaext.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});
	
});