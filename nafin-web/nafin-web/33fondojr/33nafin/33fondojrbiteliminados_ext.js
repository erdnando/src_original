Ext.onReady(function() {


var retornaRegEliminados = function(){
		var bOk = false;
		var indiceSm = 0;
		var registrosEnviar = [];
		
		gridBitacora.getStore().each(function(record) {
			if(selectModel.isSelected(indiceSm)){
				registrosEnviar.push(record.data);
				bOk = true;
			}
			indiceSm = indiceSm+1;				
		});
		
		if(bOk){
			Ext.MessageBox.confirm('Confirmaci�n','�Esta seguro de retornar los vencimientos?',function(msb){
				if(msb=='yes'){
					
					Ext.Ajax.request({
						url: '33fondojrbiteliminados_ext.data.jsp',
						params: {
							informacion: 'retornarRegErroneos',
							registros: Ext.encode(registrosEnviar)
						},
						callback: processSuccessFailureBorrarRegErroneos
					});
				}
			});
		}else{
			Ext.MessageBox.alert('Mensaje','Favor de seleccionar un registro');
		}
		
	};

var processSuccessFailureBorrarRegErroneos =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			fpBitacora.el.mask('Consultando...', 'x-mask-loading');
					
			storeBitData.load({
				params: Ext.apply(fpBitacora.getForm().getValues(),{
					operacion:'Generar',
					start: 0,
					limit: 10
				})
			});
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

var procesarConsultaVencData = function(store, arrRegistros, opts) {
		fpBitacora.el.unmask();
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		
		gridBitacora.show();
		if (arrRegistros != null) {
		
			if (!gridBitacora.isVisible()) {
				contenedorPrincipalCmp.add(gridBitacora);
				contenedorPrincipalCmp.doLayout();
			}
			
			var el = gridBitacora.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenArchivo').urlArchivo='';
				Ext.getCmp('btnGenArchivo').setText('Generar Archivo');
				Ext.getCmp('btnGenArchivo').enable();
				Ext.getCmp('btnRetornar').enable();
				el.unmask();
			}else {
				Ext.getCmp('btnGenArchivo').disable();
				Ext.getCmp('btnRetornar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}


	var processSuccessFailureArchivo = function(opts, success, response) {
		var btnGenArchivo = Ext.getCmp('btnGenArchivo');
		btnGenArchivo.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			btnGenArchivo.setIconClass('icoXls');
			btnGenArchivo.setText('Abrir Excel');
			
			var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};

			
			btnGenArchivo.urlArchivo =  NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			//btnGenArchivo.urlArchivo =  archivo;
			btnGenArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			
		
		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						btnGenArchivo.setIconClass('icoXls');
						btnGenArchivo.setText('Generar Archivo');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				btnGenArchivo.setIconClass('icoXls');
				btnGenArchivo.setText('Generar Archivo');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
	};

//-----------------------------------------------------------------------------
	var storeBitData = new Ext.data.JsonStore({
		url : '33fondojrbiteliminados_ext.data.jsp',
		id: 'storeBitData1',
		baseParams:{
			informacion: 'consultaBitacora'
		},
		root : 'registros',
		fields: [
			{name: 'IC_BIT_MONITOR'},
			{name: 'COM_FECHAPROBABLEPAGO'},
			{name: 'DF_FECHAOPERA'},
			{name: 'DF_PAGO_CLIENTE'},
			{name: 'IG_PRESTAMO'},
			{name: 'IG_CLIENTE'},
			{name: 'CG_NOMBRECLIENTE'},
			{name: 'IG_DISPOSICION'},
			{name: 'FG_TOTALVENCIMIENTO'},
			{name: 'CG_USUARIO'},
			{name: 'DF_FECHA_BIT'},
			{name: 'CG_ERROR'},
			{name: 'CG_CAUSAS'},
			{name: 'CS_TIPO_INFO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaVencData,
			beforeload:{
				fn: function(store, Obj) {
					
					store.baseParams.fec_venc_ini = Ext.getCmp('fec_venc_ini')?Ext.getCmp('fec_venc_ini').getValue():'';
					store.baseParams.fec_venc_fin = Ext.getCmp('fec_venc_fin')?Ext.getCmp('fec_venc_fin').getValue():'';
					store.baseParams.estatusCarga = Ext.getCmp('estatusCarga1')?Ext.getCmp('estatusCarga1').getValue():'';
				}		
			},
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaVencData(null, null, null);
				}
			}
		}
	});

//-----------------------------------------------------------------------------
	var selectModel = new Ext.grid.CheckboxSelectionModel({
		checkOnly: false
   });
	
	var gridBitacora = new Ext.grid.EditorGridPanel({
		id: 'gridBitacora',
		store: storeBitData,
		style:	'margin:0 auto;',
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		sm: selectModel,
		columns: [
			selectModel,
			{
				header: 'Fecha de Vencimiento Nafin',
				tooltip: 'Fecha de Vencimiento Nafin',
				dataIndex: 'COM_FECHAPROBABLEPAGO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
				//hidden: (ini_tipoMonitor=='')?false:true
			},
			{
				header: 'Fecha Operaci�n',
				tooltip: 'Fecha Operaci�n',
				dataIndex: 'DF_FECHAOPERA',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
				//hidden: (ini_tipoMonitor=='')?false:true
				
			},
			{
				header: 'Fecha pago cliente FIDE',
				tooltip: 'Fecha pago cliente FIDE',
				dataIndex: 'DF_PAGO_CLIENTE',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
				//hidden: (ini_tipoMonitor=='')?false:true
			},
			{
				header: 'Prestamo',
				tooltip: 'Prestamo',
				dataIndex: 'IG_PRESTAMO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
				//hidden: (ini_tipoMonitor=='')?false:true
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'N�m. cliente SIRAC',
				tooltip: 'N�m. cliente SIRAC',
				dataIndex: 'IG_CLIENTE',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
				//hidden: (ini_tipoMonitor=='')?false:true
			},
			{
				header: 'Cliente',
				tooltip: 'Cliente',
				dataIndex: 'CG_NOMBRECLIENTE',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'left'
				//hidden: (ini_tipoMonitor=='')?false:true
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'N�m. Cuota a Pagar',
				tooltip: 'N�m. Cuota a Pagar',
				dataIndex: 'IG_DISPOSICION',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
			},
			{
				header: 'Capital m�s Inter�s',
				tooltip: 'Capital m�s Inter�s',
				dataIndex: 'FG_TOTALVENCIMIENTO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				//hidden: (ini_tipoMonitor=='')?false:true
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Nombre del Validador',
				tooltip: 'Nombre del Validador',
				dataIndex: 'CG_USUARIO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'left'
				//hidden: (ini_tipoMonitor=='')?false:true
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de Validaci�n',
				tooltip: 'Fecha de Validaci�n',
				dataIndex: 'DF_FECHA_BIT',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
				//hidden: (ini_tipoMonitor=='')?false:true
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Error',
				tooltip: 'Error',
				dataIndex: 'CG_ERROR',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'left'
				//hidden: (ini_tipoMonitor=='')?false:true
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Causas de Eliminaci�n',
				tooltip: 'Causas de Eliminaci�n',
				dataIndex: 'CG_CAUSAS',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'left'
				//hidden: (ini_tipoMonitor=='')?false:true
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}
			
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 300,
		width: 837,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		bbar: {
				xtype:		'paging',
				autoScroll:	true,
				height:		30,
				pageSize:	10,
				buttonAlign:'left',
				id:			'barraPaginacion1',
				displayInfo:true,
				store:		storeBitData,
				displayMsg:	'{0} - {1} de {2}',
				emptyMsg:	"No hay registros.",
				items:		[
					'->','-',
					{
							xtype:	'button',
							text:		'Generar Archivo',
							id:		'btnGenArchivo',
							iconCls: 'icoXls',
							hidden:	false,
							urlArchivo: '',
							handler:	function(boton, evento) {
								if(Ext.isEmpty(boton.urlArchivo)){
									boton.disable();
									boton.setIconClass('loading-indicator');
									
									Ext.Ajax.request({
										url: '33fondojrbiteliminados_ext.data.jsp',
										params:Ext.apply(fpBitacora.getForm().getValues(),{
											informacion: 'generarArchivoMonitor'
										}),
										callback: processSuccessFailureArchivo
									});
								}else{
									//var forma 		= Ext.getDom('formAux');
									//forma.action 	= 
									//forma.submit();
									
									var fp = Ext.getCmp('fpBitacora1');
									fp.getForm().getEl().dom.action = boton.urlArchivo;
									fp.getForm().getEl().dom.submit();
									
								}
							}
					},
					'-',
					{
						xtype:	'button',
						text:		'Deshacer Eliminaci�n',
						id:		'btnRetornar',
						//iconCls: 'icoReturn',
						hidden:	true,
						handler: function(){
							retornaRegEliminados();
						}
					}
				]
			}
	});
	
	
	var fpBitacora = new Ext.form.FormPanel({
		id: 'fpBitacora1',
		width: 600,
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: [
			{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fec_venc_ini',
					id: 'fec_venc_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'fec_venc_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fec_venc_fin',
					id: 'fec_venc_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fec_venc_ini',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}
		],
		//monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {

					var objFecIni = Ext.getCmp('fec_venc_ini');
					var objFecFin = Ext.getCmp('fec_venc_fin');
					
					if(!(objFecIni.getValue()!='' && objFecFin.getValue()!='') && (objFecIni.getValue()!='' || objFecFin.getValue()!='')){
						if(objFecIni.getValue()==''){
							objFecIni.markInvalid('El valor de la fecha inicial es requerido');
						}
						if(objFecFin.getValue()==''){
							objFecFin.markInvalid('El valor de la fecha final es requerido');
						}
					}else{
						objFecIni.clearInvalid();
						objFecFin.clearInvalid();
						fpBitacora.el.mask('Consultando...', 'x-mask-loading');
						storeBitData.load({
							params: Ext.apply(fpBitacora.getForm().getValues(),{
								operacion:'Generar',
								start: 0,
								limit: 10
							})
						});
					}
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href='33fondojrbiteliminados_ext.jsp';
				}
				
			}
		]
	});
	
//------------------------------------------------------------------------------

	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		renderHidden: true,
		layoutConfig: {
			align:'center'
		},
		items: [
			//textoPrograma,
			NE.util.getEspaciador(20),
			fpBitacora,
			NE.util.getEspaciador(20)
		]
	});

});