Ext.onReady(function() {
 
	//Descarga Archivo
 
 	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var desArchivoAcuse = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var folio = registro.get('ACUSE');
		
		Ext.Ajax.request({
			url: '33ConsDesemFide.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'desArchivoAcuse',
				folio:folio,
				tipoArchivo:'Acuse',
				extension:'pdf'
				
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	var desArchivoDesembolso = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var folio = registro.get('ACUSE');
		
		Ext.Ajax.request({
			url: '33ConsDesemFide.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'desArchivoDesembolso',						
				folio: folio,
				tipoArchivo:'Desembolso',
				extension:'xls'
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var griConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			if(store.getTotalCount() > 0) {		
				Ext.getCmp('btnArchivoCSV').enable();
				el.unmask();					
			} else {		
					Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '33ConsDesemFide.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [
			{	name: 'FECHA_HORA'},	
			{	name: 'FECHA_HONRADA'},	
			{	name: 'USUARIO'},			
			{	name: 'ACUSE'}					
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});	
	
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [				
			{
				header: 'Fecha y Hora de Carga',
				tooltip: 'Fecha y Hora de Carga',
				dataIndex: 'FECHA_HORA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Honrada',
				tooltip: 'Fecha Honrada',
				dataIndex: 'FECHA_HONRADA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Usuario',
				tooltip: 'Usuario',
				dataIndex: 'USUARIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},	
			{				
				header: 'Folio',
				tooltip: 'Folio',
				dataIndex: 'ACUSE',
				width: 130,
				align: 'center'				
			},	
			
			{
				xtype: 'actioncolumn',
				header: 'Archivo de Desembolso',
				tooltip: 'Archivo de Desembolso',
				dataIndex: 'DESEMBOLSO',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoXls';									
						}
						,handler: desArchivoDesembolso
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Acuse',
				tooltip: 'Acuse',
				dataIndex: 'ACUSE',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
								return 'icoPdf';																
						}
						,handler: desArchivoAcuse
					}
				]				
			}						
		],						
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {
						
						Ext.Ajax.request({
							url: '33ConsDesemFide.data.jsp',							
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'GeneraArchivoCSV'
							}),							
							callback: procesarDescargaArchivos
						});
					
					}
				}				
			]
		}
	});
	
	
	var  elementosForma  = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Folio',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					fieldLabel: 'Folio',
					name: 'folio',
					id: 'folio1',
					allowBlank: true,
					maxLength: 30,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Honrada',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaHonradaIni',
					id: 'fechaHonradaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'fechaHonradaFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaHonradaFin',
					id: 'fechaHonradaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fechaHonradaIni',
					margins: '0 20 0 0'  
				}
			]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Carga',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaIni',
					id: 'fechaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'fechaFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaFin',
					id: 'fechaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fechaIni',
					margins: '0 20 0 0'  
				}
			]
		}
	];	

	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		monitorValid: true,
		title: 'Consulta de Desembolsos',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {

					var fechaIni = Ext.getCmp('fechaIni');
					var fechaFin = Ext.getCmp('fechaFin');
					
					if(!Ext.isEmpty(fechaIni.getValue()) || !Ext.isEmpty(fechaFin.getValue())){
						if(Ext.isEmpty(fechaIni.getValue())){
							fechaIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaFin.getValue())){
							fechaFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaFin.focus();
							return;
						}
					}
					
					
					var fechaHonradaIni = Ext.getCmp('fechaHonradaIni');
					var fechaHonradaFin = Ext.getCmp('fechaHonradaFin');
					
					if(!Ext.isEmpty(fechaHonradaIni.getValue()) || !Ext.isEmpty(fechaHonradaIni.getValue())){
						if(Ext.isEmpty(fechaHonradaIni.getValue())){
							fechaHonradaIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaHonradaIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaHonradaFin.getValue())){
							fechaHonradaFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaHonradaFin.focus();
							return;
						}
					}				
					
					
					fp.el.mask('Enviando...', 'x-mask-loading');							
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15						
						})
					});
					
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '33ConsDesemFide.jsp';					
				}
			}
		]
	});

	
	
 
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,			
			NE.util.getEspaciador(20),
			gridConsulta,			
			NE.util.getEspaciador(20)
		]
	});
	
 

});