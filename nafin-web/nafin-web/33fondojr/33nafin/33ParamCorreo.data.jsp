<%@ page import=
	"java.util.*, java.sql.*, 
	netropology.utilerias.*, 
	com.netro.exception.*, 
	org.apache.commons.logging.Log,
	netropology.utilerias.Registros,
	net.sf.json.*,
	com.netro.fondojr.*"%>
<%@ page contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

String nombre = (request.getParameter("nombre")!=null)?request.getParameter("nombre"):"";
String correo = (request.getParameter("correo")!=null)?request.getParameter("correo"):"";
String tipoDestinatario = (request.getParameter("tipoDestinatario")!=null)?request.getParameter("tipoDestinatario"):"";
String ic_correo = (request.getParameter("ic_correo")!=null)?request.getParameter("ic_correo"):"";


String infoRegresar = "";
JSONObject jsonObj = new JSONObject();
JSONArray regConsul = new JSONArray();
List parametros  = new ArrayList();

FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);


 
if(informacion.equals("Consultar") )  {

	List registros  =   fondoJunior.consParamCorreo();
	
	regConsul = JSONArray.fromObject(registros);

	String consulta =  "{\"success\": true, \"total\": \"" + regConsul.size() + "\", \"registros\": " + regConsul.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();
	
			
} else if( informacion.equals("Agregar") )  {

	try  {
		parametros.add(nombre);
		parametros.add(correo);
		parametros.add(tipoDestinatario);
		
		fondoJunior.guardaParamCorreo(parametros);
		
		jsonObj.put("success", new Boolean(true));	
		
	} catch(Exception e) {
			throw new AppException("Error al Agregar Correos ", e);
	}
		
	infoRegresar  = jsonObj.toString();			
		
} else if( informacion.equals("Borrar") )  {
	
	try  {

		fondoJunior. eliminarParamCorreo(ic_correo );
	
		jsonObj.put("success", new Boolean(true));			
	
	} catch(Exception e) {
			throw new AppException("Error al Agregar Correos ", e);
	}
	
	infoRegresar  = jsonObj.toString();		
}
%>
<%=infoRegresar%>

