<!DOCTYPE html>
<%@ page import="java.util.*,
		com.jspsmart.upload.*" 
		contentType="text/html;charset=windows-1252"
		errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
</style>

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS) {%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>

<script type="text/javascript" src="33ajustefondojr.js?<%=session.getId()%>"></script>
</head>
<%if(esEsquemaExtJS) {%>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">	
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
				<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
	</body>
	
<%}else  { %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id='areaContenido' style="margin-left: 3px; margin-top: 3px; text-align:center"></div>
<form id='formAux' name="formAux" target='_new'></form>
<% } %>
</body>
</html>