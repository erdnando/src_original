<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalgoAfianzadora,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fondojr.*,
		com.netro.fondosjr.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String tipoMonitor  = request.getParameter("tipoMonitor")==null?"":request.getParameter("tipoMonitor");

/*	SE CREA NUEVO OBJETO PARA ACCESAR EL EJB */
FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

if ((informacion.equals("consultaBitacora") || (informacion.equals("generarArchivoMonitor")  ))) {
	JSONObject jsonObj = new JSONObject();
	
	//String tipoMonitor  = request.getParameter("tipoMonitor")==null?"":request.getParameter("tipoMonitor");
	String fec_venc_ini  = request.getParameter("fec_venc_ini")==null?"":request.getParameter("fec_venc_ini");
	String fec_venc_fin  = request.getParameter("fec_venc_fin")==null?"":request.getParameter("fec_venc_fin");
	
		//Se crea la instacia del Paginador
		ConsBitacoraEliminados paginador = new ConsBitacoraEliminados();
		paginador.setCvePrograma(cveProgramaFondoJR);
		paginador.setDescPrograma(descProgramaFondoJR);
		paginador.setFec_venc_ini(fec_venc_ini);
		paginador.setFec_venc_fin(fec_venc_fin);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if (informacion.equals("consultaBitacora")) {	//Datos para la Consulta con Paginacion
			int start = 0;
			int limit = 0;
			//String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request);
				}
				infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		
		} else if (informacion.equals("generarArchivoMonitor") ) {	
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
			infoRegresar = jsonObj.toString();		
		
		}
	
} else if(informacion.equals("retornarRegErroneos")){
	JSONObject jsonObj = new JSONObject();
	List lstRegElim = new ArrayList();
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		HashMap mp = new HashMap();
		
		System.out.println("1---"+registro.getString("IC_BIT_MONITOR"));
		System.out.println("2---"+registro.getString("CS_TIPO_INFO"));
		
		mp.put("CVEBIT", registro.getString("IC_BIT_MONITOR"));
		mp.put("TIPOREG", registro.getString("CS_TIPO_INFO"));
		
		lstRegElim.add(mp);

	} 
	
	fondoJunior.restableceRegMonitorValid( lstRegElim);
	
	String fec_Actual = fHora.format(new java.util.Date());
	JSONArray jsObjArray = new JSONArray();
	jsonObj.put("success", new Boolean(true));
	
	infoRegresar = jsonObj.toString();

}

System.out.println("infoRegresar = " + infoRegresar);

%>
<%=infoRegresar%>