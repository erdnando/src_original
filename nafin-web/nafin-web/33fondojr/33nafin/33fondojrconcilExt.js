Ext.onReady(function() {
var archivo="";

////////////////////////////HANDLERS//////////////////////////////
	
	var mostrartitulo= function(store,	arrRegistros, opts) {
		pnl.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var jsonData = store.reader.jsonData;
			Ext.getCmp('gridSaldo').setTitle('<DIV >FONDO CONCENTRADOR</DIV><DIV> SALDO FONDO JUNIOR AL'+jsonData.fec_Actual+'</DIV> ');//<DIV>'+jsonData.nombrePrograma+'</DIV>');
			archivo= jsonData.URLArchivoCSV;
			//Ext.getCmp('ConsultaDeta').setTitle(jsonData.nombrePrograma);	
			//Ext.getCmp('ConsultaDeta').setTitle(jsonData.nombrePrograma);	
			if(store.getTotalCount() > 0) {
				grid.el.unmask();
			}else{
				grid.el.mask('No encontro ning�n registro');
			}
		}
	}
	
	var procesarconsultaDetalle = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('ConsultaDeta');
		
		fp.el.unmask(); 
		
		var gridConsulta = Ext.getCmp('gridSaldoDet');
		var gridTotales = Ext.getCmp('gridTotales');
		var btnGenerarArchivoDetalle = Ext.getCmp('btnGenerarArchivoDetalle');
		var btnDescargarZipDetalle = Ext.getCmp('btnDescargarZipDetalle');
		
		var el = gridConsulta.getGridEl();	

		if (arrRegistros != null) {
			gridConsulta.el.unmask();
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
				
			}		
			if(store.getTotalCount() > 0) {
				el.unmask();	
			//AQUI
			//Obtener el estatus
			var registro = store.getAt(0);
			var estatus = registro.get('ESTATUS');
			
			if (estatus == 'INICIAL'){	
				Ext.getCmp('gridSaldoDet').setTitle('<DIV a>Saldo inicial del Fondo: '+ registro.get('SALDO') +'</DIV><br><DIV> Observaciones: '+ registro.get('OBSERVACIONES') +'</DIV> ');			
				//store.insert(	0,new reg({	FEC_VENC: 'Saldo inicial del Fondo',	SALDO: registro.get('SALDO'),	OBSERVACIONES:registro.get('OBSERVACIONES'),DIAS_TRANSCURRIDOS_VENCIMIENTO:''	})	);
			}else{
			  Ext.getCmp('gridSaldoDet').setTitle('<DIV>Saldo inicial del Fondo: $0.0 </DIV><br><DIV></DIV> ');			
				//store.insert(	0,new reg({	FEC_VENC: 'Saldo inicial del Fondo',	SALDO: '0',	DIAS_TRANSCURRIDOS_VENCIMIENTO:'nada de nada', OBSERVACIONES: ''	})	);
			}
					
			//Ext.getCmp('btnGenerarArchivoDetalle').enable();
			//Ext.getCmp('btnDescargarZipDetalle').enable();
			//btnGenerarArchivoDetalle.setIconClass('');
			//btnDescargarZipDetalle.setIconClass('');
			
			consultaDataTotales.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarTotales'	
					})
				});	
			Ext.getCmp('btnGenerarArchivoDetalle').enable();
			Ext.getCmp('btnDescargarZipDetalle').enable();
			gridTotales.show();
			}else {					
				gridTotales.hide();			
				el.mask('No se encontr� ning�n registro', 'x-mask');	
				gridConsulta.el.mask('No se encontr� ning�n registro', 'x-mask');	
				
				Ext.getCmp('btnGenerarArchivoDetalle').disable();
				Ext.getCmp('btnDescargarZipDetalle').disable();
			}
		}
	}


var procesarConDetalleData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('ConsultaDeta');
		//fp.el.unmask();
	
		var gridDetalle = Ext.getCmp('gridDetalle');
	
		if (arrRegistros != null) {
			if (!gridDetalle.isVisible()) {
				gridDetalle.show();
			}					
	
			var jsonData = store.reader.jsonData;	
			
			if(jsonData.estatus=='P'){
					gridDetalle.setTitle('<div>PAGADO</div');					
			}
			if(jsonData.estatus=='NP'){
					gridDetalle.setTitle('<div>NO PAGADO</div');					
			}
			if(jsonData.estatus=='R'){
					gridDetalle.setTitle('<div>REEMBOLSO FIDE</div');					
			}
			if(jsonData.estatus=='RF'){
					gridDetalle.setTitle('<div>DESEMBOLSOS</div');					
			}
			if(jsonData.estatus=='T'){
					gridDetalle.setTitle('<div>PREPAGOS TOTALES</div');					
			}
		}
	}
	
	var procesarConDetalleDias = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('ConsultaDeta');
		//fp.el.unmask();
	
		var gridDetalleDias = Ext.getCmp('gridDetalleDias');
	
		if (arrRegistros != null) {
			if (!gridDetalleDias.isVisible()) {
				gridDetalleDias.show();
			}					
		}
	}
	
	var mostrarZip = function(opts, success, response) {
		var btnGenerarZip = Ext.getCmp('btnGenerarZip');
		btnGenerarZip.setIconClass('');

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true
		&& Ext.util.JSON.decode(response.responseText).isRunning==false) {	
		
			var btnBajarZip = Ext.getCmp('btnBajarZip');
			btnBajarZip.show();
			btnBajarZip.el.highlight('FFF700', {duration: 20, easing:'bounceOut'});
			btnBajarZip.setHandler( function(boton, evento){
				var fp = Ext.getCmp('formDownload');
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.URLArchivoZip;				  
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
				});
			
		}else{ 	
					var btnGenerarZip = Ext.getCmp('btnGenerarZip');
					btnGenerarZip.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url:'33fondojrconcilExt.data.jsp',
						params:{
						operacion:'SeguirLeyendo'				
						},
						callback: mostrarZip
					});		
		}
	}
	
	
	var mostrarArchivoDetalle = function(opts, success, response) {	
			var btnGenerarArchivoDetalle = Ext.getCmp('btnGenerarArchivoDetalle');
			btnGenerarArchivoDetalle.setIconClass('');
						
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			
			var btnBajarArchivoDetalle = Ext.getCmp('btnBajarArchivoDetalle');
			btnBajarArchivoDetalle.show();
			btnBajarArchivoDetalle.el.highlight('FFF700', {duration: 20, easing:'bounceOut'});
			btnBajarArchivoDetalle.setHandler( function(boton, evento){
					var fp = Ext.getCmp('ConsultaDeta');
					var infoR = Ext.util.JSON.decode(response.responseText);
					var archivo = infoR.URLarchivoCSVDet;				  
					archivo = archivo.replace('/nafin','');
					var params = {nombreArchivo: archivo};				
					fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					fp.getForm().getEl().dom.submit();
				});	
		
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	

	
	
		var mostrarArchivoDetalleZIP = function(opts, success, response) {	
			var btnDescargarZipDetalle = Ext.getCmp('btnDescargarZipDetalle');
			btnDescargarZipDetalle.setIconClass('');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var btnBajarZipDetalle = Ext.getCmp('btnBajarZipDetalle');
			btnBajarZipDetalle.show();
			btnBajarZipDetalle.el.highlight('FFF700', {duration: 20, easing:'bounceOut'});
			btnBajarZipDetalle.setHandler( function(boton, evento){			
					var fp = Ext.getCmp('ConsultaDeta');
					var infoR = Ext.util.JSON.decode(response.responseText);
					var archivo = infoR.URLarchivoZIPDet;				  
					archivo = archivo.replace('/nafin','');
					var params = {nombreArchivo: archivo};				
					fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					fp.getForm().getEl().dom.submit();
			  });		
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
		var mostrarArchivoDetMov = function(opts, success, response) {	
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
				var fp = Ext.getCmp('ConsultaDeta');
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.URLArchivoCSVPOP;				  
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			}else {
				NE.util.mostrarConnError(response,opts);
			}
		
		}

	var verDetallePagado = function(grid, rowIndex, colIndex, item, event) {
	
		var registro = grid.getStore().getAt(rowIndex);
		var prestamo = registro.get('PRESTAMO');
		var estatus = registro.get('ESTATUS');
	
		consultaDetalleData.load({
				params:	{
					prestamo :prestamo,
					estatus: estatus
				}
			});	
		
			var ventana = Ext.getCmp('verDetalle');	
			if (ventana) {	
				//ventana.show();
				ventana.destroy();
				
			} // else {	
			new Ext.Window({
				layout: 'fit',
				width: 830,
				height: 'auto',			
				id: 'verDetalle',
				closeAction: 'hide',
				autoDestroy:true,
				closable:false,
				items: [	
				gridDetalle
				],
				title: 'Detalle de Movimientos',
				bbar: {
					xtype: 'toolbar',	
					buttons: [
						'-',
						'->',
						{
							xtype: 'button', 	buttonAlign:'right', 	text: 'Cerrar',id: 'btnCerrarDe', 
							handler: function(){
								Ext.getCmp('gridDetalle').hide();
								Ext.getCmp('verDetalle').destroy();
							} 
						},
						'-',
						{
							xtype: 'button',
							text: 'Generar Archivo',
							tooltip:	'Generar Archivo',
							id: 'btnArchiDetalleMov',	
							handler: function(boton, evento) {
									
								Ext.Ajax.request({
									url: '33fondojrconcilExt.data.jsp',
									params:	{
										informacion:'ArchivoDetMov',
									prestamo :	prestamo,
									estatus: estatus
									}					
									,callback: mostrarArchivoDetMov
								});
							}
						}
					]
				}
			}).show();
	//	}			
	}
	

	var verDetalleDiasTrans = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var fechaProbablePago = registro.get('FEC_VENC');
			
		consultaDetalleDiasTrans.load({
				params:	{
					fechaProbablePago :fechaProbablePago
				}
			});	
		
		var ventana2 = Ext.getCmp('verDetalleDias');	
		if (ventana2) {	
			ventana2.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 830,
				height: 'auto',			
				id: 'verDetalleDias',
				closeAction: 'hide',
				autoDestroy:true,
				closable:false,
				items: [	
				gridDetalleDias
				],
				title: 'Detalle Dias Vencimiento',
				bbar: {
					xtype: 'toolbar',	
					buttons: [
						'-',
						'->',
						{
							xtype: 'button', 	buttonAlign:'right', 	text: 'Cerrar',id: 'btnCerrarDe', 
							handler: function(){
								Ext.getCmp('gridDetalleDias').hide();
								Ext.getCmp('verDetalleDias').destroy();
							} 
						}
					]
				}
			}).show();
		}			
	}

var getDiferenciaEnDias = function(fechaIni, fechaFin) { 
		fechaIni =	Ext.util.Format.date(fechaIni,'d/m/Y');
		fechaFin =	Ext.util.Format.date(fechaFin,'d/m/Y');
		var string1 			= "";
		var temp 				= "";
		var diferenciaEnDias	= "";
		string1 = fechaIni;
		string = "" + string1;
		splitstring = string.split("/");
		var fechaInicial= new Date();
		fechaInicial.setDate(splitstring[0]);
		fechaInicial.setMonth(splitstring[1]-1);
		fechaInicial.setYear(splitstring[2]);
		string1 = fechaFin;
		string = "" + string1;
		splitstring = string.split("/");
		var fechaFinal= new Date();
		fechaFinal.setDate(splitstring[0]);
		fechaFinal.setMonth(splitstring[1]-1);
		fechaFinal.setYear(splitstring[2]);
		//Calcular el numero de milisegundos de un dia
		var milisegundosPorDia=1000*60*60*24;
		//Calculate difference btw the two dates, and convert to days
		diferenciaEnDias = Math.ceil((fechaFinal.getTime()-fechaInicial.getTime())/(milisegundosPorDia));
		return diferenciaEnDias;
	};
	
		var consultaDetalleDiasTrans = new Ext.data.JsonStore({
		root : 'registros',
		url : '33fondojrconcilExt.data.jsp',
		baseParams: {
			informacion: 'ConsultarDetalleDias'
		},
		fields: [
			{name: 'IC_NUM_OPERACION'},	
			{name: 'CG_DETALL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConDetalleDias,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConDetalleDias(null, null, null);						
				}
			}
		}
	});
	
	var consultaDetalleData = new Ext.data.JsonStore({
		root : 'registros',
		url : '33fondojrconcilExt.data.jsp',
		baseParams: {
			informacion: 'ConsultarDetalleMov'
		},
		fields: [
			{name: 'FEC_VENC'},	
			{name: 'FEC_FIDE'},
			{name: 'FEC_CLIENTE'},	
			{name: 'PRESTAMO'},	
			{name: 'NUM_CLIENTE'},	
			{name: 'NOMBRECLIENTE'},	
			{name: 'NUM_CUOTA'},	
			{name: 'ESTATUS'},	
			{name: 'DIAS_TRANSCURRIDOS_VENCIMIENTO'},	
			{name: 'AMORT',type: 'float'},	
			{name: 'INTERES'},
			{name: 'SALDO',type: 'float'},
			{name: 'ORIGEN_DESC'},
			{name: 'DIAS_LIMITE'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConDetalleData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConDetalleData(null, null, null);						
				}
			}
		}
	});	
	
	
			var gridDetalleDias = {
		xtype: 'editorgrid',
		store: consultaDetalleDiasTrans,
		id: 'gridDetalleDias',		
		hidden: true,
		columns: [		
			{
				header: 'No. Operaci�n', 
				tooltip: 'No. Operaci�n',
				dataIndex: 'IC_NUM_OPERACION',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Detalle', 
				tooltip: 'Detalle',
				dataIndex: 'CG_DETALL',
				sortable: true,
				resizable: true,
				width: 600,			
				align: 'left'
			}	
		],
		height: 150,
		width: 800
	};
	
		var gridDetalle = {
		xtype: 'editorgrid',
		store: consultaDetalleData,
		id: 'gridDetalle',		
		hidden: true,
		title: 'Titulo',
		columns: [		
			{
				header: 'Fecha Amortizaci�n Nafin', 
				tooltip: 'Fecha Amortizaci�n Nafin',
				dataIndex: 'FEC_VENC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Fecha Operaci�n', 
				tooltip: 'Fecha Operaci�n',
				dataIndex: 'FEC_FIDE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Fecha Pago Cliente FIDE', 
				tooltip: 'Fecha Pago Cliente FIDE',
				dataIndex: 'FEC_CLIENTE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Prestamo ', 
				tooltip: 'Prestamo',
				dataIndex: 'PRESTAMO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Num. Cliente SIRAC', 
				tooltip: 'Num. Cliente SIRAC ',
				dataIndex: 'NUM_CLIENTE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Cliente', 
				tooltip: 'Cliente',
				dataIndex: 'NOMBRECLIENTE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Num. Cuota a Pagar', 
				tooltip: 'Num. Cuota a Pagar',
				dataIndex: 'NUM_CUOTA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'D�as Transcurridos de Vencimiento', 
				tooltip: 'D�as Transcurridos de Vencimiento',
				dataIndex: 'DIAS_TRANSCURRIDOS_VENCIMIENTO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center',renderer:function(value,metadata,registro,rowIndex){		
								if (registro.get('DIAS_TRANSCURRIDOS_VENCIMIENTO')>registro.get('DIAS_LIMITE')){	
								  return '<span style="color:red;">'+value+'</span>';				
							 }else{
								  return '<span style="color:black;">'+value+'</span>';
							 }	
				},
				items: [
							{ 
								getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {									
											 if (registro.get('DIAS_TRANSCURRIDOS_VENCIMIENTO')>registro.get('DIAS_LIMITE')){									
												this.items[0].tooltip = 'Ver';
												return 'iconoLupa';
											}																															
								}
								,handler: verDetalleDiasTrans				
							} 
						 ]		
			},	
			{
				header: 'Capital', 
				tooltip: 'Capital',
				dataIndex: 'AMORT',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Interes', 
				tooltip: 'Interes',
				dataIndex: 'INTERES',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Total a Pagar', 
				tooltip: 'Total a Pagar',
				dataIndex: 'SALDO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Origen', 
				tooltip: 'Origen ',
				dataIndex: 'ORIGEN_DESC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right'
			}
		],
		height: 150,
		width: 800
	};
	
	
var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '33fondojrconcilExt.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},
		hidden: true,
		fields: [	
			{name: 'TOTAL_CAPITAL'},
			{name: 'TOTAL_INTERES'},
			{name: 'TOTAL_SALDO',type: 'float'},
			{name: 'TOTAL_PAGADO'},
			{name: 'TOTAL_NO_PAGADO'},
			{name: 'TOTAL_REEMBOLSO'},
			{name: 'TOTAL_REEMBOLSO_FINI'},
			{name: 'TOTAL_REEMBOLSO_TOT'},
			{name: 'SALDO_OTROS_MOV'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
					//load: procesarconsultaDetalle
			}			
	});
	

	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		title:'',
		hidden: true,
		columns: [
			{	
				header : 'Totale Capital',
				tooltip: 'Total Capital',
				dataIndex : 'TOTAL_CAPITAL',
				sortable: true,
				width: 150,						
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Total Interes',
				tooltip: 'Total Interes',
				dataIndex : 'TOTAL_INTERES',
				sortable: true,
				width: 150,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Total Saldo',
				tooltip: 'Total Saldo',
				dataIndex : 'TOTAL_SALDO',
				sortable: true,
				width: 150,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Total Pagado',
				tooltip: 'Total Pagado',
				dataIndex : 'TOTAL_PAGADO',
				sortable: true,
				width: 150,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Total No Pagado',
				tooltip: 'Total No Pagado',
				dataIndex : 'TOTAL_NO_PAGADO',
				sortable: true,
				width: 150,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Total Reembolso Fide',
				tooltip: 'Total Reembolso Fide',
				dataIndex : 'TOTAL_REEMBOLSO',
				sortable: true,
				width: 150,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Desembolso',
				tooltip: 'Desembolso',
				dataIndex : 'TOTAL_REEMBOLSO_FINI',
				sortable: true,
				width: 150,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Prepagos Totales',
				tooltip: 'Prepagos Totales',
				dataIndex : 'TOTAL_REEMBOLSO_TOT',
				sortable: true,
				width: 150,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Total Otros Movimientos',
				tooltip: 'Total Otros Movimientos',
				dataIndex : 'SALDO_OTROS_MOV',
				sortable: true,
				width: 150,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 100,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});




var consultaDetalle= new Ext.data.JsonStore({ 
	   root : 'registros',
		url : '33fondojrconcilExt.data.jsp',
		baseParams: {
			informacion: 'consultarDetalle'
		},		
		fields: [	
			{name: 'FEC_VENC'},
			{name: 'FEC_FIDE'},
			{name: 'AMORT'},
			{name: 'INTERES'},
			{name: 'SALDO'},
			{name: 'DIAS_TRANSCURRIDOS_VENCIMIENTO'},
			{name: 'OBSERVACIONES'},
			{name: 'FEC_VAL'},
			{name: 'USUARIO'},
			{name: 'ESTATUS'},
			{name: 'PRESTAMO'},
			{name: 'DIAS_LIMITE'}
	],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{		
					beforeLoad:	{fn: function(store, options){
							
							
							Ext.apply(options.params, {							
								df_fecha_venci_:  Ext.util.Format.date(Ext.getCmp('df_fecha_venci_').getValue(),'d/m/Y'),
								df_fecha_venciMax_: Ext.util.Format.date (Ext.getCmp('df_fecha_venciMax_').getValue(),'d/m/Y'),
								df_fecha_pago_:  Ext.util.Format.date(Ext.getCmp('df_fecha_pago_').getValue(),'d/m/Y'),
								df_fecha_pagoMax_: Ext.util.Format.date (Ext.getCmp('df_fecha_pagoMax_').getValue(),'d/m/Y'),
								df_fecha_ope_:  Ext.util.Format.date(Ext.getCmp('df_fecha_ope_').getValue(),'d/m/Y'),
								df_fecha_opeMax_: Ext.util.Format.date (Ext.getCmp('df_fecha_opeMax_').getValue(),'d/m/Y'),
								dias_:  Ext.getCmp('dias').getValue(),
								diasMax_: Ext.getCmp('diasMax').getValue()	
								
								
								
							});
							}	},
				load: procesarconsultaDetalle,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					}
				}

		}
	});

var gridConsultaDet= new Ext.grid.GridPanel ({
	id: 'gridSaldoDet',
	store: consultaDetalle,
	title:'DETALLE DE MOVIMIENTO',
	frame: true, hidden:true,
	height: 400,
	width:900,
	columns: [
		{
		header:'Fecha de Vencimiento',
		tooltip:'Fecha de Vencimiento',
		dataIndex:'FEC_VENC',
		hidden: false
		},
		{
		header:'Fecha Pago FIDE',
		tooltip:'Fecha Pago FIDE',
		dataIndex:'FEC_FIDE',
		hidden: false
		},
		{
		header:'Capital',
		tooltip:'Capital',
		dataIndex:'AMORT',
		hidden: false,
		align: 'right',
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
		header:'Interes',
		tooltip:'Interes',
		dataIndex:'INTERES',
		hidden: false,
		align: 'right',
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
		header:'Saldo',
		tooltip:'Saldo',
		dataIndex:'SALDO',
		hidden: false,
		align: 'right',
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
		xtype: 'actioncolumn',
		header:'Pagado',
		tooltip:'Pagado',
		dataIndex:'SALDO',
		align: 'right',
		renderer:	function(value, metadata, registro, rowindex, colindex, store) {
							if(registro.get('ESTATUS') =='P'){
							 saldo= Ext.util.Format.number((registro.get('SALDO')), '$0,0.00');
							
							 return saldo;
							}	
						},							
		items: [
					{ 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {									
									if(registro.get('ESTATUS') =='P'){									
										this.items[0].tooltip = 'Ver';
										return 'iconoLupa';
									}																															
						}
						,handler: verDetallePagado					
					} 
				 ]		
		},
		{
		xtype: 'actioncolumn',
		header:'No Pagado',
		tooltip:'No Pagado',
		dataIndex:'SALDO',
		align: 'right',
		renderer:	function(value, metadata, registro, rowindex, colindex, store) {
							if(registro.get('ESTATUS') =='NP'){
								saldo= Ext.util.Format.number((registro.get('SALDO')), '$0,0.00');
							
							 return saldo;		
							}
						},							
		items: [
					{ 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {									
									if(registro.get('ESTATUS') =='NP'){									
										this.items[0].tooltip = 'Ver';
										return 'iconoLupa';
									}																															
						}
						,handler: verDetallePagado					
					} 
				 ]		
		},
		{
		xtype: 'actioncolumn',
		header:'Reembolso FIDE',
		tooltip:'Reembolso FIDE',
		dataIndex:'SALDO',
		align: 'right',
		renderer:	function(value, metadata, registro, rowindex, colindex, store) {
							if(registro.get('ESTATUS') =='R'){	
							 saldo= Ext.util.Format.number((registro.get('SALDO')), '$0,0.00');
							
							 return saldo;	
							}
						},							
		items: [
					{ 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {									
									if(registro.get('ESTATUS') =='R'){									
										this.items[0].tooltip = 'Ver';
										return 'iconoLupa';
									}																															
						}
						,handler: verDetallePagado					
					} 
				 ]		
		},
		{
		xtype: 'actioncolumn',
		header:'Desembolsos',
		tooltip:'Desembolsos',
		dataIndex:'SALDO',
		align: 'right',
		renderer:	function(value, metadata, registro, rowindex, colindex, store) {
							if(registro.get('ESTATUS') =='RF'){
							 saldo= Ext.util.Format.number((registro.get('SALDO')), '$0,0.00');
							
							 return saldo;		
							}
						},							
		items: [
					{ 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {									
									if(registro.get('ESTATUS') =='RF'){									
										this.items[0].tooltip = 'Ver';
										return 'iconoLupa';
									}																															
						}
						,handler: verDetallePagado					
					} 
				 ]		
		},
		{
		xtype: 'actioncolumn',
		header:'Prepagos Totales',
		tooltip:'Prepagos Totales',
		dataIndex:'SALDO',
		align: 'right',
		renderer:	function(value, metadata, registro, rowindex, colindex, store) {
							if(registro.get('ESTATUS') =='T'){	
							 saldo= Ext.util.Format.number((registro.get('SALDO')), '$0,0.00');
							
							 return saldo;		
							}
						},							
		items: [
					{ 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {									
									if(registro.get('ESTATUS') =='T'){									
										this.items[0].tooltip = 'Ver';
										return 'iconoLupa';
									}																															
						}
						,handler: verDetallePagado					
					} 
				 ]		
		},
		{
		xtype: 'actioncolumn',
		header:'Otros Movimientos',
		tooltip:'Otros Movimientos',
		align: 'right',
		renderer:	function(value, metadata, registro, rowindex, colindex, store) {
							if(registro.get('ESTATUS') =='SUMA'||registro.get('ESTATUS') =='suma'||registro.get('ESTATUS') =='RESTA'||registro.get('ESTATUS') =='resta'){	
								return registro.get('SALDO');	
							}
						}				
		},
		{
		xtype: 'actioncolumn',
		header:'D�as Transcurridos de Vencimiento',
		tooltip:'D�as Transcurridos de Vencimiento',
		dataIndex:'DIAS_TRANSCURRIDOS_VENCIMIENTO',
		hidden: false,
		renderer:function(value,metadata,registro,rowIndex){
		
							if (registro.get('DIAS_TRANSCURRIDOS_VENCIMIENTO')>registro.get('DIAS_LIMITE')){	
								  return '<span style="color:red;">'+value+'</span>';				
							 }else{
								  return '<span style="color:black;">'+value+'</span>';
							 }
				
					
		},
		items: [
					{ 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {									
									 if (registro.get('DIAS_TRANSCURRIDOS_VENCIMIENTO')>registro.get('DIAS_LIMITE')){									
										this.items[0].tooltip = 'Ver';
										return 'iconoLupa';
									}																															
						}
						,handler: verDetalleDiasTrans				
					} 
				 ]		
		},
		{
		header:'Observaciones',
		tooltip:'Observaciones',
		dataIndex:'OBSERVACIONES',
		hidden: false
		},
		{
		header:'Fecha Validaci�n',
		tooltip:'Fecha Validaci�n',
		dataIndex:'FEC_VAL',
		hidden: false
		},
		{
		header:'Usuario',
		tooltip:'Usuario',
		dataIndex:'USUARIO',
		hidden: false
		}
	],
	bbar: {
			xtype: 'paging',
			id: 'barraPaginacion',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			store: consultaDetalle,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo Detalle',
					id: 'btnGenerarArchivoDetalle',
					handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					
					var fp = Ext.getCmp('ConsultaDeta');
							
								Ext.Ajax.request({
								url:'33fondojrconcilExt.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{					
										operacion:'GenerarArchivoDetalle',
										tipo:'CSV'
										
									}),
									callback: mostrarArchivoDetalle
								});		
							
					}
				},
				{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivoDetalle',
									hidden: true
				},
				{
					xtype: 'button',
					text: 'Descargar Zip',
					id: 'btnDescargarZipDetalle',
					handler: function(boton, evento) {
					var fp = Ext.getCmp('ConsultaDeta');
					boton.disable();
					boton.setIconClass('loading-indicator');
						{
								Ext.Ajax.request({
								url:'33fondojrconcilExt.data.jsp',
									params:Ext.apply(fp.getForm().getValues(),{					
										operacion:'GenerarArchivoDetalle',
										tipo:'ZIP'
									}),
									callback: mostrarArchivoDetalleZIP
								});		
							}					
					}
				},
				{
									xtype: 'button',
									text: 'Bajar Zip',
									id: 'btnBajarZipDetalle',
									hidden: true
				}
		]	
	}
});

///////////////////////////////////////////////////////////////////////////////
	var textoPrograma = new Ext.Container({
		layout: 'form',		
		id: 'textoPrograma',							
		width:	'700',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensaje', 	value: '' }				
		]
	});
	
	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var  jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
						
				Ext.getCmp('mensaje').setValue(jsonValoresIniciales.programa);
				//Ext.getCmp('estatusCarga1').setValue('N');
				var variable = Ext.util.JSON.decode(response.responseText).programa;
				Ext.getCmp('textoPrograma').show();
			}			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
///////////////////////////////////////////////////////////////////////////////

var consultaFondo = new Ext.data.JsonStore({
		root : 'registros',
		url : '33fondojrconcilExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'FECHA_ULT_MOV'},
			{name: 'SALDO_INICIAL',type: 'float'},
			{name: 'TOTAL_RETIROS',type: 'float'},
			{name: 'TOTAL_REE_FIDE',type: 'float'},
			{name: 'DESE_PENDIENTES',type: 'float'},
			{name: 'PREPAGO_TOTALES',type: 'float'},
			{name: 'OTROS_MOVI',type: 'float'},
			{name: 'SALDO_FINAL',type: 'float'},
			{name: 'RETIROS_PEN',type: 'float'}
		],
		listeners: {
			load: mostrartitulo,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					mostrartitulo(null, null, null);
				}
			}
		}
	});


var grid= new Ext.grid.GridPanel ({
	id: 'gridSaldo',
	store: consultaFondo,
	title:'SALDO FONDO JUNIOR',
	frame: true,
	height: 200,
	columns: [
		{
		header:'Fecha �ltimo movimiento',
		tooltip:'Fecha �ltimo movimiento',
		dataIndex:'FECHA_ULT_MOV',
		hidden: false
		},
		{
		header:'Saldo Inicial',
		tooltip:'Saldo Inicial',
		dataIndex:'SALDO_INICIAL',
		hidden: false,
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
		header:'Total Retiros',
		tooltip:'Total Retiros',
		dataIndex:'TOTAL_RETIROS',
		hidden: false,
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
		header:'Total Reembolsos FIDE',
		tooltip:'Total Reembolsos FIDE',
		dataIndex:'TOTAL_REE_FIDE',
		hidden: false,
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
		header:'Desembolsos Pendientes de Recuperar',
		tooltip:'Desembolsos Pendientes de Recuperar',
		dataIndex:'DESE_PENDIENTES',
		hidden: false,
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
		header:'Prepagos Totales',
		tooltip:'Prepagos Totales',
		dataIndex:'PREPAGO_TOTALES',
		hidden: false,
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
		header:'Otros Movimientos',
		tooltip:'Otros Movimientos',
		dataIndex:'OTROS_MOVI',
		hidden: false,
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
		header:'Saldo Final',
		tooltip:'Saldo Final',
		dataIndex:'SALDO_FINAL',
		hidden: false,
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
		header:'Retiros Pendientes de Desembolsos',
		tooltip:'Retiros Pendientes de Desembolsos',
		dataIndex:'RETIROS_PEN',
		hidden: false,
		renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
	],
	bbar: [
		{
		xtype:'button',
		text:'Generar Archivo',
		id:'btnGenerarArchivo',
		handler: function (boton,evento){
				var fp = Ext.getCmp('formDownload');
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
		
			}
		},
		{
		xtype:'button',
		text:'Ver detalle Movimiento',
		id:'btnVerdetalle',
		handler: function (boton,evento){

					var consultarDet = function() {
					
						var ventana = Ext.getCmp('verDetalle');	
							if (ventana) {	
								ventana.destroy();	
							} 
						
						var btnBajarArchivoDetalle = Ext.getCmp('btnBajarArchivoDetalle');
						btnBajarArchivoDetalle.hide();
						
						var btnBajarZipDetalle = Ext.getCmp('btnBajarZipDetalle');
						btnBajarZipDetalle.hide();
						
						var df_fecha_vencimiento = Ext.getCmp("df_fecha_venci_");
						var df_fecha_vencimientoMax = Ext.getCmp("df_fecha_venciMax_");
						var df_fecha_pagos = Ext.getCmp("df_fecha_pago_");
						var df_fecha_pagosMax = Ext.getCmp("df_fecha_pagoMax_");
						var df_fecha_operacion = Ext.getCmp("df_fecha_ope_");
						var df_fecha_operacionMax = Ext.getCmp("df_fecha_opeMax_");
						var diass = Ext.getCmp("dias");
						var diassMax = Ext.getCmp("diasMax");
					
							if (Ext.isEmpty(df_fecha_vencimiento.getValue()) && Ext.isEmpty(df_fecha_vencimientoMax.getValue()) ){
								df_fecha_vencimiento.markInvalid('Por favor capture la Fecha inicial.');
								df_fecha_vencimientoMax.markInvalid('Por favor capture la Fecha final.');
								return;
							}
							
							if (Ext.isEmpty(df_fecha_vencimiento.getValue()) ){
								df_fecha_vencimiento.markInvalid('Por favor capture la Fecha incial.');
								return;
							}
							if (Ext.isEmpty(df_fecha_vencimientoMax.getValue()) ){
								df_fecha_vencimientoMax.markInvalid('Por favor capture la Fecha final.');
								return;
							}
							
							
							if (!Ext.isEmpty(df_fecha_pagos.getValue()) || !Ext.isEmpty(df_fecha_pagosMax.getValue()) ){
								if (Ext.isEmpty(df_fecha_pagos.getValue())){
									df_fecha_pagos.markInvalid('Por favor capture la fecha inicial.');
								return;
								}
								if (Ext.isEmpty(df_fecha_pagosMax.getValue()) ){
									df_fecha_pagosMax.markInvalid('Por favor capture la fecha final.');
								return;
								}
							}
							
								if (!Ext.isEmpty(df_fecha_operacion.getValue()) || !Ext.isEmpty(df_fecha_operacionMax.getValue()) ){
								if (Ext.isEmpty(df_fecha_operacion.getValue())){
									df_fecha_operacion.markInvalid('Por favor capture la fecha inicial.');
								return;
								}
								if (Ext.isEmpty(df_fecha_operacionMax.getValue()) ){
									df_fecha_operacionMax.markInvalid('Por favor capture la fecha final.');
								return;
								}
							}
							
							var Diferencia = Math.abs(getDiferenciaEnDias(df_fecha_vencimientoMax.getValue(),df_fecha_vencimiento.getValue() ));
													
											if ( Diferencia >7 )		{
												df_fecha_vencimientoMax.markInvalid('El rango de fechas de solicitud no debe sobrepasar 7 dias');
												return;
											}
					
							
							if (!Ext.isEmpty(diass.getValue()) || !Ext.isEmpty(diassMax.getValue()) ){
								if (Ext.isEmpty(diass.getValue())){
									diass.markInvalid('El D�a Inicial del Vencimiento del Cr�dito es requerido.');
								return;
								}
								if (Ext.isEmpty(diassMax.getValue()) ){
									diassMax.markInvalid('El D�a Final del Vencimiento del Cr�dito es requerido.');
								return;
								}
								if (diass.getValue() >diassMax.getValue()){
									diass.markInvalid('El D�a Inicial no puede ser mayor al D�a Final del Vencimiento del Cr�dito.');
								return;
								}
							}
							
							
						var fp = Ext.getCmp('ConsultaDeta');	
						var gridConsulta = Ext.getCmp('gridSaldoDet');
						fp.el.mask('Procesando...', 'x-mask-loading');	
						
						gridConsulta.el.mask('Procesando', 'x-mask-loading');
							consultaDetalle.load({
									params: Ext.apply(fp.getForm().getValues(),{
										informacion:'consultarDetalle',
										operacion:'Generar',
										start:0,
										limit:15
									})
								});	
						
					};
					var elementosFormaConsultaDet=[
						{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de Vencimiento de',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'df_fecha_venci_',
									id: 'df_fecha_venci_',
									allowBlank: true,
									startDay: 0,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'df_fecha_venciMax_',
									margins: '0 20 0 0' 
								},
								{
									xtype: 'displayfield',
									value: 'a',
									width: 15
								},
								{
									xtype: 'datefield',
									name: 'df_fecha_venciMax_',
									id: 'df_fecha_venciMax_',
									allowBlank: true,
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: 'df_fecha_venci_',
									margins: '0 20 0 0'					
								}		
							]	
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Fecha Pago FIDE de',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'df_fecha_pago_',
									id: 'df_fecha_pago_',
									allowBlank: true,
									startDay: 0,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'df_fecha_pagoMax_',
									margins: '0 20 0 0' 					
								},
								{
									xtype: 'displayfield',
									value: 'a',
									width: 15
								},
								{
									xtype: 'datefield',
									name: 'df_fecha_pagoMax',
									id: 'df_fecha_pagoMax_',
									allowBlank: true,
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: 'df_fecha_pago_',
									margins: '0 20 0 0'					
								}		
							]	
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de Operaci�n de',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'df_fecha_ope_',
									id: 'df_fecha_ope_',
									allowBlank: true,
									startDay: 0,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'df_fecha_opeMax_',
									margins: '0 20 0 0' 					
								},
								{
									xtype: 'displayfield',
									value: 'a',
									width: 15
								},
								{
									xtype: 'datefield',
									name: 'df_fecha_opeMax_',
									id: 'df_fecha_opeMax_',
									allowBlank: true,
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: 'df_fecha_ope_',
									margins: '0 20 0 0'					
								}		
							]	
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Cr�ditos con Vencimiento',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'displayfield',
									value: 'Desde',
									width: 34
								},
								{
									xtype: 'numberfield',
									name: 'dias_',
									id: 'dias',
									allowBlank: true,
									startDay: 0,
									width: 45,
									maxLength : 3,
									msgTarget: 'side',
									campoFinFecha: 'diasMax',
									margins: '0 0 0 0' 					
								},
								{
									xtype: 'displayfield',
									value: 'd�as',
									width: 25
								},
								{
									xtype: 'displayfield',
									value: 'Hasta',
									width: 50
								},
								{
									xtype: 'numberfield',
									name: 'diasMax_',
									id: 'diasMax',
									allowBlank: true,
									startDay: 1,
									width: 45,
									maxLength : 3,
									msgTarget: 'side',
									campoInicioFecha: 'dias',
									margins: '0 0 0 0'
								},
								{
									xtype: 'displayfield',
									value: 'd�as',
									width: 24
								}
							]	
						}
						
					];
						var consultaDet = new Ext.form.FormPanel({
							title:'DETALLE DE MOVIMIENTO',
							id: 'ConsultaDeta',
							width: 500,
							style: ' margin:0 auto;',
							frame: true,
							titleCollapse: false,
							style: 'margin:0 auto;',
							bodyStyle: 'padding: 6px',
							labelWidth: 180,
							defaults: {msgTarget: 'side',	anchor: '-20'},
							items: elementosFormaConsultaDet,
							monitorValid: true,
							buttons:	[
								{
									formBind: true,	text: 'Consultar', iconCls: 'icoBuscar',
									handler: consultarDet
								},
								{
									text: 'Regresar', id: 'btnRegresar', iconCls:'icoLimpiar',
									handler: function() {
										window.location = '33fondojrconcilExt.jsp';
									}
						
								}
								//aqui va el boton d
							]
						});
						pnl.insert(3,consultaDet);
						pnl.doLayout();
						//NE.util.getEspaciador (20),
						Ext.getCmp('gridSaldo').hide();
		}
		},
		{
		xtype:'button',
		text:'Generar Zip',
		id:'btnGenerarZip',
		handler: function (boton,evento){	
				boton.disable();
				boton.setIconClass('loading-indicator');
			{
				Ext.Ajax.request({
					url:'33fondojrconcilExt.data.jsp',
					params:{
					
					operacion:'GenerarZip'				
					 },
					callback: mostrarZip
				});		
			}
		}
		},
		{
				xtype: 'button',
				text: 'Bajar Archivo',
			   id: 'btnBajarZip',
				hidden: true
		}
		]
});

 var formDownload=new Ext.form.FormPanel({
					hidden:true,
					id: 'formDownload'
					});

var pnl=new Ext.Container ({
	id:'ContenedorPrincipal',
	applyTo: 'areaContenido',
	width: 890,
	items: [
	NE.util.getEspaciador(20),
			textoPrograma,
	NE.util.getEspaciador (20),
	grid,
	NE.util.getEspaciador (20),
	gridConsultaDet,
	gridTotales,
	formDownload
	]

});

Ext.Ajax.request({
		url: '33fondojrconcilExt.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});
	

grid.el.mask('Procesando', 'x-mask-loading');
pnl.el.mask('');	
consultaFondo.load();

});