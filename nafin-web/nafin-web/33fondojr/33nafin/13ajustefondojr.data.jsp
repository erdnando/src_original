<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.descuento.*,
		com.netro.fondojr.*,
		com.netro.exception.*,
		com.netro.seguridadbean.*,
		javax.naming.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>

<%
String infoRegresar = "";
String datosIniciales = (request.getParameter("datosIniciales")!=null)?request.getParameter("datosIniciales"):"";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clavePrograma = (String) session.getAttribute("cveProgramaFondoJR");

if (informacion.equals("ValidaCargaArchivo")) {
	JSONObject jsonObj = new JSONObject();
	try{
		String numProc = (request.getParameter("numProc")!=null)?request.getParameter("numProc"):"";
		String tipoModifi = (request.getParameter("tipoModifi")!=null)?request.getParameter("tipoModifi"):"";
		String flagThread = (request.getParameter("flagThread")!=null)?request.getParameter("flagThread"):"";
		FondoJunior fondojrBean = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
		
		System.out.println("flagThread===="+numProc);
		
		jsonObj.put("numProc", numProc);
		// Realizar la validacion mediante un thread
		if("1".equals(flagThread)){
			session.removeAttribute("validaThread");
			ValidaDatosFondoJrThread validaThread = new ValidaDatosFondoJrThread();
			validaThread.setProcessID(Integer.parseInt(numProc));
			validaThread.setTipoModificacion(tipoModifi);
			validaThread.setCveProgramaFjr(clavePrograma);
			validaThread.setRunning(true);
			new Thread(validaThread).start();
			
			session.setAttribute("validaThread", validaThread);
			
			int percent = (int) validaThread.getPercent();
			
			jsonObj.put("numReg", String.valueOf(validaThread.getNumberOfRegisters()));
			jsonObj.put("numProcReg", String.valueOf(validaThread.getProcessedRegisters()));
			jsonObj.put("porcentaje", String.valueOf(percent));
			jsonObj.put("flagThread", "2");
			System.out.println("INICIA EL THREAD");
		}
		else
		if("2".equals(flagThread)){
			ValidaDatosFondoJrThread validaThread = (ValidaDatosFondoJrThread)session.getAttribute("validaThread");
			int percent = (int) validaThread.getPercent();
			if (validaThread.isRunning()) {
				jsonObj.put("flagThread", "2");
				jsonObj.put("numReg", String.valueOf(validaThread.getNumberOfRegisters()));
				jsonObj.put("numProcReg", String.valueOf(validaThread.getProcessedRegisters()));
				jsonObj.put("porcentaje", String.valueOf(percent));
			}
			
			
			if(!validaThread.hasError()){
				jsonObj.put("numReg", String.valueOf(validaThread.getNumberOfRegisters()));
				jsonObj.put("numProcReg", String.valueOf(validaThread.getProcessedRegisters()));
				jsonObj.put("porcentaje", String.valueOf(percent));
			}
			
			if (validaThread.isCompleted() && !validaThread.hasError()) {
				
				FondoJunior fondoJr = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
				
				List lstTotalesValid =  fondoJr.getResumenValidacion(numProc);
				JSONArray jsObjArray = new JSONArray();
				jsObjArray = JSONArray.fromObject(lstTotalesValid);
				jsonObj.put("registros", jsObjArray.toString());
				System.out.println("jsObjArray.toString()=="+jsObjArray.toString());
				
				jsonObj.put("flagThread", "3");
				jsonObj.put("numReg", String.valueOf(validaThread.getNumberOfRegisters()));
				jsonObj.put("numProcReg", String.valueOf(validaThread.getProcessedRegisters()));
				jsonObj.put("porcentaje", String.valueOf(percent));
			}
			
		}
		
		jsonObj.put("success", new Boolean(true));
	}catch(Throwable t){
		throw new AppException("Error inesperado...", t);
	}
	
	informacion = jsonObj.toString();
}

if(informacion.equals("TransferirCargaArchivo")){
	JSONObject jsonObj = new JSONObject();
	try{
		String numProc = (request.getParameter("numProc")!=null)?request.getParameter("numProc"):"";
		String tipoModifi = (request.getParameter("tipoModifi")!=null)?request.getParameter("tipoModifi"):"";
		String flagCargaThread = (request.getParameter("flagCargaThread")!=null)?request.getParameter("flagCargaThread"):"";
		
		System.out.println("flagCargaThread===="+flagCargaThread);
		/*
		FondoJuniorHome _fondojrHome = (FondoJuniorHome)netropology.utilerias.ServiceLocator.getInstance().getEJBHome("FondoJuniorEJB", FondoJuniorHome.class);
		FondoJunior fondojrBean = _fondojrHome.create();
		
		HashMap mpDataRes = fondojrBean.realizaAjusteFondoJr(numProc,tipoModifi, clavePrograma, iNoUsuario, strNombreUsuario);
		
		jsonObj.put("recibo", (String)mpDataRes.get("RECIBO"));
		jsonObj.put("fecha", (String)mpDataRes.get("FECHA"));
		jsonObj.put("hora", (String)mpDataRes.get("HORA"));
		jsonObj.put("nomUser", strNombreUsuario);
		jsonObj.put("totalreg", (String)mpDataRes.get("TOTALREG"));
		jsonObj.put("totalregc", (String)mpDataRes.get("TOTALREGC"));
		jsonObj.put("totalregi", (String)mpDataRes.get("TOTALREGI"));
		jsonObj.put("success", new Boolean(true));
		*/
		
		jsonObj.put("tipoModifi", tipoModifi);
		jsonObj.put("numProc", numProc);
		
		if("1".equals(flagCargaThread)){
			session.removeAttribute("cargaThread");
			CargaDatosFondoJrThread cargaThread = new CargaDatosFondoJrThread();
			cargaThread.setNumProc(numProc);
			cargaThread.setTipoMod(tipoModifi);
			cargaThread.setCvePrograma(clavePrograma);
			cargaThread.setINoUsuario(iNoUsuario);
			cargaThread.setStrNombreUsuario(strNombreUsuario);
			
			cargaThread.setRunning(true);
			new Thread(cargaThread).start();
			
			session.setAttribute("cargaThread", cargaThread);
			
			jsonObj.put("flagCargaThread", "2");
			System.out.println("INICIA EL THREAD");
		
		}else if("2".equals(flagCargaThread)){
			CargaDatosFondoJrThread cargaThread = (CargaDatosFondoJrThread)session.getAttribute("cargaThread");
			if (cargaThread.isRunning()) {
				jsonObj.put("flagCargaThread", "2");
			}else
			if(cargaThread.hasError()){
				jsonObj.put("flagCargaThread", "4");
				//jsonObj.put("msgErrorZip", "Error al generar Zip");
			}else
			if(cargaThread.getFolio()==null && cargaThread.isRunning()){
				jsonObj.put("flagCargaThread", "2");
			}else
			if(!cargaThread.isRunning() && cargaThread.getFolio()!=null){
				jsonObj.put("flagCargaThread", "3");
				jsonObj.put("recibo",cargaThread.getFolio());
				jsonObj.put("fecha", cargaThread.getFecha());
				jsonObj.put("hora", cargaThread.getHora());
				jsonObj.put("nomUser", strNombreUsuario);
			}	
		}
		jsonObj.put("success", new Boolean(true));
	
	}catch(Throwable t){
		throw new AppException("Error inesperado...", t);
	}
	
	informacion = jsonObj.toString();
}

if(informacion.equals("GeneraZipCargaArchivo")){
	JSONObject jsonObj = new JSONObject();
	try{
		String numProc = (request.getParameter("numProc")!=null)?request.getParameter("numProc"):"";
		String csValido = (request.getParameter("csValido")!=null)?request.getParameter("csValido"):"";
		String flagZipThread = (request.getParameter("flagZipThread")!=null)?request.getParameter("flagZipThread"):"";
		
		jsonObj.put("numProc", numProc);
		jsonObj.put("csValido", csValido);
		
		if("1".equals(flagZipThread)){
			session.removeAttribute("zipThread");
			GeneraArchivoCargaZipThread zipThread = new GeneraArchivoCargaZipThread();
			zipThread.setRutaFisica(strDirectorioTemp);
			zipThread.setRutaVirtual(strDirectorioTemp);
			zipThread.setNumProc(numProc);
			zipThread.setCsReg(csValido);
			zipThread.setRunning(true);
			new Thread(zipThread).start();
			
			session.setAttribute("zipThread", zipThread);
			
			jsonObj.put("flagZipThread", "2");
			System.out.println("INICIA EL THREAD");
		
		}else if("2".equals(flagZipThread)){
			GeneraArchivoCargaZipThread zipThread = (GeneraArchivoCargaZipThread)session.getAttribute("zipThread");
			if (zipThread.isRunning()) {
				jsonObj.put("flagZipThread", "2");
			}else
			if(zipThread.hasError()){
				jsonObj.put("flagZipThread", "4");
				//jsonObj.put("msgErrorZip", "Error al generar Zip");
			}else
			if(zipThread.getNombreArchivo()==null && zipThread.isRunning()){
				jsonObj.put("flagZipThread", "2");
			}else
			if(!zipThread.isRunning() && zipThread.getNombreArchivo()!=null){
				jsonObj.put("flagZipThread", "3");
				jsonObj.put("urlArchivo", strDirecVirtualTemp+zipThread.getNombreArchivo());
			}	
		}
		
		jsonObj.put("success", new Boolean(true));
	}catch(Throwable t){
		throw new AppException("error al generar archivo ZIP", t);
	}
	
	informacion = jsonObj.toString();
}

if(informacion.equals("ImprimirPDF")){
	JSONObject jsonObj = new JSONObject();
	String jsonDataRecibo = (request.getParameter("registrosRecibo") == null)?"":request.getParameter("registrosRecibo");
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String  tipoArchivo = (request.getParameter("tipoArchivo") == null)?"":request.getParameter("tipoArchivo");
	String  recibo = (request.getParameter("recibo") == null)?"":request.getParameter("recibo");
	try{
	
		List arrRegistrosModificados = JSONArray.fromObject(jsonRegistros);
		List arrDataRecibo = JSONArray.fromObject(jsonDataRecibo);
		Iterator itReg = arrRegistrosModificados.iterator();
		Iterator itRegRecibo = arrDataRecibo.iterator();
		
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = "";
		if("PDF".equals(tipoArchivo)){
			nombreArchivo = archivo.nombreArchivo()+"."+tipoArchivo;	
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
	
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
													"",
													"",
													(String) session.getAttribute("strNombre"),
													(String) session.getAttribute("strNombreUsuario"),
													(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(2,70);
			pdfDoc.setCell("Número de Recibo: "+recibo,"celda01",ComunesPDF.CENTER,2,2);
			
			
			int index = 0;
			String etiqueta = "";
			String informa = "";
			
			while (itRegRecibo.hasNext()) {
				JSONObject registro = (JSONObject)itRegRecibo.next();			
				etiqueta = registro.getString("etiqueta");
				informa = registro.getString("informacion");
				
				pdfDoc.setCell(etiqueta,"formas",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell(informa,"formas",ComunesPDF.CENTER,1,2);
	
				index++;
			} //fin del for
			
			pdfDoc.addTable();
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(3,90);
			pdfDoc.setCell("Resumen de la Operación","celda01",ComunesPDF.CENTER,3,2);
			pdfDoc.setCell("-","celda01",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell("Totales","celda01",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell("Monto Total","celda01",ComunesPDF.CENTER,1,2);
			
			String tipoReg = "";
			String numTotalReg = "";
			String montoTotalReg = "";
			while (itReg.hasNext()) {
				JSONObject registro = (JSONObject)itReg.next();			
				
				tipoReg = registro.getString("CSVALIDO");
				numTotalReg = registro.getString("TOTALREG");
				montoTotalReg = registro.getString("MONTOREG");
				
				tipoReg = "S".equals(tipoReg)?"Registros sin Errores":"Registros con Errores";
				
				pdfDoc.setCell(tipoReg,"formas",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell(numTotalReg,"formas",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell(Comunes.formatoDecimal(montoTotalReg,2,true) ,"formas",ComunesPDF.CENTER,1,2);
	
				//index++;
			} //fin del for
			
			pdfDoc.addTable();
			
			pdfDoc.endDocument();
			
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
		}
	}catch(Throwable t){
		t.printStackTrace();
		throw new AppException("Error al imprimir PDF", t);
	}

	informacion = jsonObj.toString();
	System.out.println("informacion==="+informacion);
}


%>

<%=informacion%>