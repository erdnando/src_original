<%@ page
	contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		com.netro.fondojr.*,
		com.netro.zip.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>

<%
	JSONObject archivoCSV = new JSONObject();
	JSONObject archivoPDF = new JSONObject();
	JSONObject registroAcuse = new JSONObject();
	JSONArray arrAcuse = new JSONArray();
	boolean flag = true;
	boolean flagExt = true;
	boolean flagReg = true;
	ParametrosRequest req = null;
	String rutaArchivo = "";
	String PATH_FILE	=	strDirectorioTemp;
	String itemArchivo = "";
	String extension = "";
	String folio = "";
	String fechaHora = "";
	String numProceso = "";
	String numTotal = "";
	String totalMonto = "";
	String msgError = "";
	int totalRegistros = 0;
	int tamanio = 0;
	String tipoModif = "";
	
	BigDecimal montoTotal = new BigDecimal("0.0");
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		extension = (req.getParameter("hidExtension") == null)? "" : req.getParameter("hidExtension");
		numTotal = (req.getParameter("hidNumTotal") == null)? "" : req.getParameter("hidNumTotal");
		totalMonto = (req.getParameter("hidTotalMonto") == null)? "" : req.getParameter("hidTotalMonto");
		tipoModif = (req.getParameter("hidTipoModif") == null)? "" : req.getParameter("hidTipoModif");
		
		System.out.println("numTotal=="+numTotal);
		System.out.println("totalMonto=="+totalMonto);
		try{
			int tamaPer = 0;
			if (extension.equals("zip")){
				upload.setSizeMax(200 * 1024);	//700 Kb
				tamaPer = (200 * 1024);
			}

			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName();
			InputStream archivoZip = fItem.getInputStream();
			tamanio			= (int)fItem.getSize();

			rutaArchivo = PATH_FILE+itemArchivo;
			
			System.out.println("tamanio=="+tamanio);
			System.out.println("tamaPer=="+tamaPer);
			System.out.println("itemArchivo=="+itemArchivo);
			
			if (tamanio > tamaPer){
				flag = false;
				msgError = "El tamaño del archivo ZIP es mayor al permitido";
			}
			if (flag){

				fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				try{
					
					
					FondoJunior fondojrBean = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
					
					List lArchivos = ComunesZIP.descomprimir(archivoZip, PATH_FILE);
					for(int i=0;i<lArchivos.size();i++) {
						String nombre 	= lArchivos.get(i).toString();
						String ext 	= nombre.substring(nombre.lastIndexOf('.') + 1, nombre.length()).toUpperCase();
						String rutaTxt = PATH_FILE+nombre;
						if(!("TXT".equals(ext))) {
							//throw new Exception("Error, el formato del Archivo no es el correcto. ("+nombre+") ");
							flagExt = false;
							msgError = "El archivo contenido dentro del ZIP no es un archivo TXT";
						}else{
							BufferedReader brt=  null;
							String linea = "";
							
							java.io.File ft = new java.io.File(rutaTxt);
							brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
							
							String prestamo	= "";
							String cuota		= "";
							String montoCapital	= "";
							String montoInteres	= "";
							String totalVenc	= "";
							String fechaVenc	= "";
							
							VectorTokenizer vtd = null;
							Vector vecdet = null;
			
							while((linea=brt.readLine())!=null){
									
								vtd		= new VectorTokenizer(linea,"|");
								vecdet	= vtd.getValuesVector();
								
								if(!"F".equals(tipoModif)){
								totalVenc	=(vecdet.size()>=3)?vecdet.get(2).toString().trim().toUpperCase():"0.0";
								}else{
									totalVenc = "";
									montoCapital	=(vecdet.size()>=2)?vecdet.get(1).toString().trim().toUpperCase():"0.0";
									montoInteres	=(vecdet.size()>=3)?vecdet.get(2).toString().trim().toUpperCase():"0.0";
									if( Comunes.esDecimalPositivo(montoCapital)){
										totalVenc = montoCapital;
									}
									if( Comunes.esDecimalPositivo(montoInteres) ){
										if(!"".equals(totalVenc)){
											totalVenc = (new BigDecimal(totalVenc).add(new BigDecimal(montoInteres))).toString();
										}else{
											totalVenc = montoInteres;
										}
									}
								}
								
								if(Comunes.esDecimalPositivo(totalVenc)){
									montoTotal = montoTotal.add(new BigDecimal(totalVenc));
								}
								
								totalRegistros++;
							}
							
							
							if(totalRegistros>0 && totalRegistros<=30000){
								if(Integer.parseInt(numTotal)==totalRegistros){
									System.out.println("montoTotal=="+montoTotal.toPlainString());
									System.out.println("totalMonto=="+totalMonto);
									if((montoTotal.compareTo(new BigDecimal(totalMonto))==0)){
										numProceso = fondojrBean.cargaDatosTmpAjusteFondoJr(rutaTxt, tipoModif);
									}else{
										flag = false;
										msgError = "El monto total de los registros no corresponde con el monto total de vencimientos capturado";
									}
								}else{
									flag = false;
									msgError = "El número de registros no corresponde con el número de vencimientos capturado";
								}
							}else{
								flagReg = false;
								msgError = "'El archivo TXT no puede contener mas de 30000 registros";
							}
						}
					}
					

				}catch (Exception e){
						throw new AppException("Ocurrio un error al generar el archivo", e);
				}
			
			}
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}
	
%>	
{
	"success": true,
	"flag":	<%=(flag)?"true":"false"%>,
	"flagExt":	<%=(flagExt)?"true":"false"%>,
	"flagReg":	<%=(flagReg)?"true":"false"%>,
	"numProceso":	'<%=numProceso%>',
	"totalRegistros":	'<%=totalRegistros%>',
	"msgError":	'<%=msgError%>'
	
}