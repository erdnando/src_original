<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>
<%
	String tipoMonitor = (request.getParameter("tipoMonitor")==null)?"":request.getParameter("tipoMonitor");

%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS) {%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<%-- El jsp que contiene el dise?o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/33fondojr/33nafin/33fondojrbiteliminados_ext.js?<%=session.getId()%>"></script>
</head>
<%if(esEsquemaExtJS) {%>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">	
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
				<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
		<!-- Valores iniciales recibidos como parametros-->
		<input type="hidden" id="hidStrUsuario" value="<%=strTipoUsuario%>">
		<input type="hidden" id="hidTipoMonitor" value="<%=tipoMonitor%>">
		<input type="hidden" id="hidDescProgFondoJR" value="<%=descProgramaFondoJR%>">
		<input type="hidden" id="hidCvePrograma" value="<%=cveProgramaFondoJR%>">
		
	</body>

<%}else  { %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

			<div id="areaContenido"><div style="height:190px"></div></div>

<form id='formAux' name="formAux" target='_new'></form>
<!-- Valores iniciales recibidos como parametros-->
<input type="hidden" id="hidStrUsuario" value="<%=strTipoUsuario%>">
<input type="hidden" id="hidTipoMonitor" value="<%=tipoMonitor%>">
<input type="hidden" id="hidDescProgFondoJR" value="<%=descProgramaFondoJR%>">
<input type="hidden" id="hidCvePrograma" value="<%=cveProgramaFondoJR%>">
<%}%>

</body>
</html>