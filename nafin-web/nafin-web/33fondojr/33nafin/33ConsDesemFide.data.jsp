<%@ page import=
	"java.util.*, java.sql.*, 
	netropology.utilerias.*, 
	com.netro.exception.*, 
	org.apache.commons.logging.Log,
	netropology.utilerias.Registros,
	net.sf.json.*,
	com.netro.fondosjr.*"%>
<%@ page contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String folio = (request.getParameter("folio")!=null)?request.getParameter("folio"):"";
String fechaIni = (request.getParameter("fechaIni")!=null)?request.getParameter("fechaIni"):"";
String fechaFin = (request.getParameter("fechaFin")!=null)?request.getParameter("fechaFin"):"";
String fechaHonradaIni = (request.getParameter("fechaHonradaIni")!=null)?request.getParameter("fechaHonradaIni"):"";
String fechaHonradaFin = (request.getParameter("fechaHonradaFin")!=null)?request.getParameter("fechaHonradaFin"):"";
String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
String extension = (request.getParameter("extension")!=null)?request.getParameter("extension"):"";

String infoRegresar = "", consulta ="", nombreArchivo ="";
int  start= 0, limit =0;
JSONObject jsonObj = new JSONObject();


ConsDesemFide paginador  =  new ConsDesemFide ();
paginador.setFolio(folio);
paginador.setFechaIni(fechaIni);
paginador.setFechaFin(fechaFin);
paginador.setFechaHonradaIni(fechaHonradaIni);
paginador.setFechaHonradaFin(fechaHonradaFin);

CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

 
if(informacion.equals("Consultar")   ||  informacion.equals("GeneraArchivoCSV")  )  {

	if(informacion.equals("Consultar")) {
	
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
						
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
				
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);										
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
					
		jsonObj = JSONObject.fromObject(consulta);				
	
	}else  if ( informacion.equals("GeneraArchivoCSV") ) {
	
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}			
	}
	
	infoRegresar = jsonObj.toString();	
		
} else if( informacion.equals("desArchivoAcuse")   ||   informacion.equals("desArchivoDesembolso")  )  {

	try {
		nombreArchivo =	paginador.consDescargaArchivos(strDirectorioTemp,  folio, tipoArchivo,  extension );
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivos ", e);
	}	
	infoRegresar = jsonObj.toString(); 	

	
}
%>
<%=infoRegresar%>

