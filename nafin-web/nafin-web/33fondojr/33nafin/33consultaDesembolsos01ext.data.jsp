<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.fondojr.*,
		net.sf.json.*,
		com.netro.pdf.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>

<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String msgError = "";
try{

	FondoJunior beanFondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

	if (informacion.equals("Consulta")) {
		String numFolio = (request.getParameter("numFolio")!=null)?request.getParameter("numFolio"):"";
		String cgUsuario = (request.getParameter("cgUsuario")!=null)?request.getParameter("cgUsuario"):"";
		String fechaCargaIni = (request.getParameter("fechaCargaIni")!=null)?request.getParameter("fechaCargaIni"):"";
		String fechaCargaFin = (request.getParameter("fechaCargaFin")!=null)?request.getParameter("fechaCargaFin"):"";
		
		HashMap mpCriterios = new HashMap();
		mpCriterios.put("FOLIO_BATCH", numFolio);
		mpCriterios.put("CG_USUARIO", cgUsuario);
		mpCriterios.put("FECHA_CARGA_INI", fechaCargaIni);
		mpCriterios.put("FECHA_CARGA_FIN", fechaCargaFin);
		
		JSONArray jsObjArray = new JSONArray();
		List lstResumenBatch = beanFondoJunior.consultaDesembolsosBatch(mpCriterios);
		jsObjArray = JSONArray.fromObject(lstResumenBatch);
		
		infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";
	
	}else if(informacion.equals("ImprimirPDF")){
		JSONObject jsonObj = new JSONObject();
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
		List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
		Iterator itReg = arrRegistrosEnviados.iterator();
		
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = archivo.nombreArchivo()+".pdf";	
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
												"",
												(String)session.getAttribute("sesExterno"),
												(String) session.getAttribute("strNombre"),
												(String) session.getAttribute("strNombreUsuario"),
												(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
		pdfDoc.setTable(8,100);
		pdfDoc.setCell("No. Folio","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Archivo","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Fecha y Hora de Carga","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Usuario","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("No. Total Registros","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Registros Sin Error","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Registros Con Error","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Registros Pendientes de Procesar","celda01",ComunesPDF.CENTER,1,2);
		
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();			
			pdfDoc.setCell(registro.getString("FOLIO_BATCH"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("NOMBRE_ARCHIVO"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("FECHA_CARGA"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("USUARIO"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("REG_TOTAL"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("REG_CORRECTOS"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("REG_INCORRECTOS"),"formas",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell(registro.getString("REG_PENDIENTES"),"formas",ComunesPDF.CENTER,1,2);
		}
		
		pdfDoc.addTable();
		pdfDoc.endDocument();
		
		System.out.println("strDirecVirtualTemp+nombreArchivo == "+strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("GenerarExcel")){
		JSONObject jsonObj = new JSONObject();
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		String nombreArchivo = "";
		
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
		List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
		Iterator itReg = arrRegistrosEnviados.iterator();
		
		contenidoArchivo.append("No. Folio, Archivo, Fecha y Hora de Carga, Usuario, No. Total Registros, Resgistros Sin Error, registros Con Error, Registros Pedientes de Procesar\n");
		
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();			
			contenidoArchivo.append(registro.getString("FOLIO_BATCH")+",");
			contenidoArchivo.append(registro.getString("NOMBRE_ARCHIVO")+",");
			contenidoArchivo.append(registro.getString("FECHA_CARGA")+",");
			contenidoArchivo.append(registro.getString("USUARIO")+",");
			contenidoArchivo.append(registro.getString("REG_TOTAL")+",");
			contenidoArchivo.append(registro.getString("REG_CORRECTOS")+",");
			contenidoArchivo.append(registro.getString("REG_INCORRECTOS")+",");
			contenidoArchivo.append(registro.getString("REG_PENDIENTES")+"\n");
		}
		
		if(archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
				nombreArchivo = archivo.nombre;
				
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("GeneraZipCargaBatchArchivo")){
		JSONObject jsonObj = new JSONObject();
		try{
			String numFolio = (request.getParameter("numFolio")!=null)?request.getParameter("numFolio"):"";
			String icResBatch = (request.getParameter("icResBatch")!=null)?request.getParameter("icResBatch"):"";
			String tipoReg = (request.getParameter("tipoReg")!=null)?request.getParameter("tipoReg"):"";
			String flagZipThread = (request.getParameter("flagZipThread")!=null)?request.getParameter("flagZipThread"):"";
			
			jsonObj.put("numFolio", numFolio);
			jsonObj.put("icResBatch", icResBatch);
			jsonObj.put("tipoReg", tipoReg);
			
			if("1".equals(flagZipThread)){
				session.removeAttribute("zipThread");
				GeneraArchivoCargaBatchZipThread zipThread = new GeneraArchivoCargaBatchZipThread();
				zipThread.setRutaFisica(strDirectorioTemp);
				zipThread.setRutaVirtual(strDirectorioTemp);
				zipThread.setNumFolio(numFolio);
				zipThread.setIcResBatch(icResBatch);
				zipThread.setTipoReg(tipoReg);
				zipThread.setRunning(true);
				new Thread(zipThread).start();
				
				session.setAttribute("zipThread", zipThread);
				
				jsonObj.put("flagZipThread", "2");
				System.out.println("INICIA EL THREAD");
			
			}else if("2".equals(flagZipThread)){
				GeneraArchivoCargaBatchZipThread zipThread = (GeneraArchivoCargaBatchZipThread)session.getAttribute("zipThread");
				if (zipThread.isRunning()) {
					jsonObj.put("flagZipThread", "2");
				}else
				if(zipThread.hasError()){
					jsonObj.put("flagZipThread", "4");
					//jsonObj.put("msgErrorZip", "Error al generar Zip");
				}else
				if(zipThread.getNombreArchivo()==null && zipThread.isRunning()){
					jsonObj.put("flagZipThread", "2");
				}else
				if(!zipThread.isRunning() && zipThread.getNombreArchivo()!=null){
					jsonObj.put("flagZipThread", "3");
					jsonObj.put("urlArchivo", strDirecVirtualTemp+zipThread.getNombreArchivo());
				}	
			}
			
			jsonObj.put("success", new Boolean(true));
		}catch(Throwable t){
			throw new AppException("error al generar archivo ZIP", t);
		}
		
		infoRegresar = jsonObj.toString();
	}


}catch(Throwable t){
	t.printStackTrace();
}finally{
	System.out.println("infoRegresar = " + infoRegresar);
}
%>


<%=infoRegresar%>