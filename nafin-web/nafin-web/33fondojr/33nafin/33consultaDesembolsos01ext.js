
Ext.onReady(function() {

////////////////////////////HANDLERS//////////////////////////////

var procesarConsultaData = function(store, arrRegistros, opts) {
	var fp = Ext.getCmp('forma');
	var btnImpPDF = Ext.getCmp('btnImpPDF');
	var btnAbrirPDF = Ext.getCmp('btnAbrirPDF');
	var btnGenerarExcel = Ext.getCmp('btnGenerarExcel');
	var btnAbrirCSV = Ext.getCmp('btnAbrirCSV');
	fp.el.unmask();

	var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
	if (arrRegistros != null) {
		if (!grid.isVisible()) {
			contenedorPrincipalCmp.add(grid);
			contenedorPrincipalCmp.doLayout();
		}
		
		var el = grid.getGridEl();
		if(store.getTotalCount() > 0) {
			el.unmask();
		}else {
			btnImpPDF.disable();
			btnGenerarExcel.disable();
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
		
	}
}

var ajaxPeticionZip = function(numFolio, icResBatch, tipoReg, flag){ 
	
	if(flag=='1'){
		//gridLayout.hide();
		pnl.el.mask('Descargando Archivo...', 'x-mask-loading');
	}
	
	Ext.Ajax.request({
		url: '33consultaDesembolsos01ext.data.jsp',
		params: {
				informacion:'GeneraZipCargaBatchArchivo',
				numFolio: numFolio,
				icResBatch: icResBatch,
				tipoReg: tipoReg,
				flagZipThread: flag
				},
		callback: procesarSuccessGeneraZip
	});

}
 
var procesarSuccessGeneraZip = function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var resp = Ext.util.JSON.decode(response.responseText);
		
		if(resp.flagZipThread=='3'){
			pnl.el.unmask();
			var forma = Ext.getDom('formAux');
			forma.action = resp.urlArchivo;
			forma.submit();
		}else if(resp.flagZipThread=='2'){
			ajaxPeticionZip(resp.numFolio,resp.icResBatch,resp.tipoReg, resp.flagZipThread);
		}else  if(resp.flagZipThread=='4'){
			pnl.el.unmask();
			Ext.MessageBox.alert('Error','No se pudo generar el archivo Zip');
		}
		
	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

var muestraRegistrosC = function(grid, rowIndex, colIndex){
	var rec = grid.getStore().getAt(rowIndex);
	ajaxPeticionZip(rec.get('FOLIO_BATCH'), rec.get('CVE_RESUMEN'), 'S', '1');
	
}

var muestraRegistrosI = function(grid, rowIndex, colIndex){
	var rec = grid.getStore().getAt(rowIndex);
	if(rec.get('CS_DEPURADO')!='S'){
		ajaxPeticionZip(rec.get('FOLIO_BATCH'), rec.get('CVE_RESUMEN'), 'N', '1');
	}
	
}

var muestraRegistrosP = function(grid, rowIndex, colIndex){
	var rec = grid.getStore().getAt(rowIndex);
	ajaxPeticionZip(rec.get('FOLIO_BATCH'), rec.get('CVE_RESUMEN'), 'P', '1');
	
}

var onImprimirPDF = function(btn){
	var grid = Ext.getCmp('gridBatch');
	var storeBatch = grid.getStore();
	var registrosGrid = [];
	
	btn.setIconClass('loading-indicator');
	btn.disable();
	storeBatch.each(function(record) {
		registrosGrid.push(record.data);
	});
	
	registrosGrid = Ext.encode(registrosGrid);
	
	Ext.Ajax.request({
		url: '33consultaDesembolsos01ext.data.jsp',
		params: {
			informacion: 'ImprimirPDF',
			registros : registrosGrid
		},
		callback: procesarSuccessFailureImprimirPDF
	});
}

var procesarSuccessFailureImprimirPDF = function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var response = Ext.util.JSON.decode(response.responseText);
		var btnImpr = Ext.getCmp('btnImpPDF');
		var btnAbrirPDF = Ext.getCmp('btnAbrirPDF');
		
		btnImpr.setIconClass('');
		btnAbrirPDF.show();
		btnAbrirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnAbrirPDF.setHandler( function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = response.urlArchivo;
			forma.submit();
		});

	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

var onGenerarExcel = function(btn){
	
	var grid = Ext.getCmp('gridBatch');
	var storeBatch = grid.getStore();
	var registrosGrid = [];
	
	btn.setIconClass('loading-indicator');
	btn.disable();
	storeBatch.each(function(record) {
		registrosGrid.push(record.data);
	});
	
	registrosGrid = Ext.encode(registrosGrid);
	
	Ext.Ajax.request({
		url: '33consultaDesembolsos01ext.data.jsp',
		params: {
			informacion: 'GenerarExcel',
			registros : registrosGrid
		},
		callback: procesarSuccessFailureGenerarExcel
	});

}

var procesarSuccessFailureGenerarExcel = function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var response = Ext.util.JSON.decode(response.responseText);
		var btnGenerarExcel = Ext.getCmp('btnGenerarExcel');
		var btnAbrirCSV = Ext.getCmp('btnAbrirCSV');
		
		btnGenerarExcel.setIconClass('');
		btnAbrirCSV.show();
		btnAbrirCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnAbrirCSV.setHandler( function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = response.urlArchivo;
			forma.submit();
		});

	} else {
		NE.util.mostrarConnError(response,opts);
	}
}

////////////////////////////STORE///////////////////////////////////

var consultaData = new Ext.data.JsonStore({
	root : 'registros',
	url : '33consultaDesembolsos01ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
		{name: 'CVE_RESUMEN'},
		{name: 'CVE_DESEMB'},
		{name: 'FOLIO_BATCH'},
		{name: 'NOMBRE_ARCHIVO'},
		{name: 'FECHA_CARGA'},
		{name: 'USUARIO'},
		{name: 'REG_TOTAL'},
		{name: 'REG_CORRECTOS'},
		{name: 'REG_INCORRECTOS'},
		{name: 'REG_PENDIENTES'},
		{name: 'CS_PROCESADO'},
		{name: 'CS_DEPURADO'}
	],
	totalProperty : 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
			fn: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				//LLama procesar consulta, para que desbloquee los componentes.
				procesarConsultaData(null, null, null);
			}
		}
	}
});

///////////////////////////COMPONENTES//////////////////////////////
var elementosForm =
	[
		{
			xtype: 'textfield',
			name: 'numFolio',
			id: 'numFolio1',
			fieldLabel: 'N�mero de Folio',
			allowBlank: true,
			maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'cgUsuario',
			id: 'cgUsuario',
			fieldLabel: 'Usuario',
			allowBlank: true,
			maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Carga del Archivo',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaCargaIni',
					id: 'fechaCargaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					minValue: '01/01/1901',
					campoFinFecha: 'fechaCargaFin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'hasta',
					width: 40
				},
				{
					xtype: 'datefield',
					name: 'fechaCargaFin',
					id: 'fechaCargaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fechaCargaIni',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		}
	];
///////////////////////////CONTENEDORES/////////////////////////////
var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 550,
		title: 'Consulta de Resultados del Proceso Batch',
		frame: true,
		collapsible: true,
		style: 'margin:0 auto;',
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForm,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {

					
					
					
					var fechaCargaIni = Ext.getCmp('fechaCargaIni');
					var fechaCargaFin = Ext.getCmp('fechaCargaFin');
					
					if(!Ext.isEmpty(fechaCargaIni.getValue()) || !Ext.isEmpty(fechaCargaFin.getValue())){
						if(Ext.isEmpty(fechaCargaIni.getValue())){
							fechaCargaIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaCargaIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaCargaFin.getValue())){
							fechaCargaFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaCargaFin.focus();
							return;
						}
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');
					var btnImpPDF = Ext.getCmp('btnImpPDF');
					var btnAbrirPDF = Ext.getCmp('btnAbrirPDF');
					var btnGenerarExcel = Ext.getCmp('btnGenerarExcel');
					var btnAbrirCSV = Ext.getCmp('btnAbrirCSV');
					
					btnImpPDF.enable();
					btnAbrirPDF.hide();
					btnGenerarExcel.enable();
					btnAbrirCSV.hide();
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues())
					});	
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href='33consultaDesembolsos01ext.jsp';
				}
			}
		]
	});
	
	
	var grid = new Ext.grid.EditorGridPanel({
	id: 'gridBatch',
	store: consultaData,
	margins: '20 0 0 0',
	style: 'margin:0 auto;',
	clicksToEdit: 1,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [
		{
			header: 'No. Folio',
			tooltip: 'N�mero de Folio',
			dataIndex: 'FOLIO_BATCH',
			sortable: true,
			width: 100,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header: 'Archivo',
			tooltip: 'Nombre de Archivo',
			dataIndex: 'NOMBRE_ARCHIVO',
			sortable: true,
			width: 170,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header: 'Fecha de Carga',
			tooltip: 'Fecha de Carga',
			dataIndex: 'FECHA_CARGA',
			sortable: true,
			width: 120,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			header: 'Usuario',
			tooltip: 'Usuario',
			dataIndex: 'USUARIO',
			sortable: true,
			width: 180,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
				return causa;
			}
		},
		{
			xtype:	'actioncolumn',
			header: 'No. Total Registros',
			tooltip: 'No. Total Registros',
			dataIndex: 'REG_TOTAL',
			sortable: true,
			width: 70,
			resizable: true,
			hidden: false,
			align: 'center',
			renderer:  function (valor, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
				return valor;
			}
		},
		{
			xtype:	'actioncolumn',
			header: 'Registros Sin Error',
			tooltip: 'Registros Sin Error',
			dataIndex: 'REG_CORRECTOS',
			sortable: true,
			width: 78,
			resizable: true,
			hidden: false,
			align: 'center',
			renderer:  function (valor, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
				return valor;
			},
			items: [
				{
					getClass: function(value, metadata, record, rowIndex, colIndex, store) {
						if (value > 0){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}else	{
							return "";
						}
					},
					handler:	muestraRegistrosC
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: 'Registros Con Error',
			tooltip: 'Registros Con Error',
			dataIndex: 'REG_INCORRECTOS',
			sortable: true,
			width: 78,
			resizable: true,
			hidden: false,
			align: 'center',
			renderer:  function (valor, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
				return valor;
			},
			items: [
				{
					getClass: function(value, metadata, record, rowIndex, colIndex, store) {
						if (value > 0 && record.get('CS_DEPURADO')!='S'){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}else	{
							return "";
						}
					},
					handler:	muestraRegistrosI
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: 'Registros Pendientes',
			tooltip: 'Registros Pendientes de Procesar',
			dataIndex: 'REG_PENDIENTES',
			sortable: true,
			width: 78,
			resizable: true,
			hidden: false,
			align: 'center',
			renderer:  function (valor, columna, registro){
				columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
				return valor;
			},
			items: [
				{
					getClass: function(value, metadata, record, rowIndex, colIndex, store) {
						if (value > 0){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}else	{
							return "";
						}
					},
					handler:	muestraRegistrosP
				}
			]
		}
		
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 300,
	width: 885,
	title: '',
	frame: true,
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
				text: 'Imprimir PDF',
				id: 'btnImpPDF',
				handler: onImprimirPDF
			},
			{
				text: 'Abrir PDF',
				id: 'btnAbrirPDF',
				hidden: true
				//handler: onAutorizar
			},
			'-',
			{
				text: 'Generar Excel',
				id: 'btnGenerarExcel',
				handler: onGenerarExcel
			},
			{
				text: 'Abrir Excel',
				id: 'btnAbrirCSV',
				hidden: true
				//handler: onAutorizar
			}
		]
	}
	});
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',		
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			{
				xtype: 'buttongroup',
			  columns: 3,
			  style: 'margin:0 auto;',
			  title: '',
			  hideBorders : true,
			  items: [
				{
						xtype: 'button',
						id: 'btnIndividual',
						text: 'Individual',
						scale: 'medium',
						width: 80,
						handler: function(){
							window.location.href='/nafin/33fondojr/33pki/33nafin/33reembolsoPorFiniquito01ext.jsp?tipo_carga=INDIVIDUAL';
						}
					},
					{
						xtype: 'button',
						id: 'btnMasiva',
						text: 'Masiva',
						scale: 'medium',
						width: 80,
						handler: function(){
							window.location.href='/nafin/33fondojr/33pki/33nafin/33reembolsoPorFiniquitoMasivo01ext.jsp?tipo_carga=MASIVA';
						}
					},
					{
						xtype: 'button',
						id: 'btnBatch',
						text: 'Consulta Batch',
						scale: 'medium',
						width: 80
					}]
			},
			fp,
			NE.util.getEspaciador(10)
		]
	});

});