<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,			
		com.netro.descuento.*, 		
		com.netro.model.catalogos.CatalogoSimple,		
		com.netro.fondojr.*,
		
		com.netro.fondosjr.ConsultaHistoricaMovimientosFondoJr,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
		
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>

<%
	String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion     =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
	
	String sFechaVencIni  = (request.getParameter("txtFchVenc1")==null)?"":request.getParameter("txtFchVenc1");
	String sFechaVencFin  = (request.getParameter("txtFchVenc2")==null)?"":request.getParameter("txtFchVenc2");
	String sFechaOperIni  = (request.getParameter("txtFchOper1")==null)?"":request.getParameter("txtFchOper1");
	String sFechaOperFin  = (request.getParameter("txtFchOper2")==null)?"":request.getParameter("txtFchOper2");
	String sFechaPagoIni  = (request.getParameter("txtFchPago1")==null)?"":request.getParameter("txtFchPago1");
	String sFechaPagoFin  = (request.getParameter("txtFchPago2")==null)?"":request.getParameter("txtFchPago2");
	String sRegistro 		= (request.getParameter("estatus_reg")==null)?"":request.getParameter("estatus_reg");
	String sValidacion 	= (request.getParameter("estatus_val")==null)?"":request.getParameter("estatus_val");
	String sPrestamo 		= (request.getParameter("prestamo")==null)?"":request.getParameter("prestamo");
	String sNumCliente   = (request.getParameter("numSirac")==null)?"":request.getParameter("numSirac");
	String sCliente		= (request.getParameter("clienteSirac")==null)?"":request.getParameter("clienteSirac");
	String sOrden			= (request.getParameter("ordenar")==null)?"":request.getParameter("ordenar");
	String sSubtotal		= (request.getParameter("subtotales")==null)?"":request.getParameter("subtotales");
	String sdiasDesde    = (request.getParameter("diasDesde")==null)?"":request.getParameter("diasDesde");
	String sdiasHasta     = (request.getParameter("diasHasta")==null)?"":request.getParameter("diasHasta");
	String clavePrograma =(String)request.getSession().getAttribute("cveProgramaFondoJR");
	//String clavePrograma ="3";
	String val_nombre =  (request.getParameter("nombrePyme")==null)?"":request.getParameter("nombrePyme");
	String val_rfc =  (request.getParameter("rfcPyme")==null)?"":request.getParameter("rfcPyme");
	
							
				
	int start=0; int limit=0;
	String infoRegresar  = "", infoRegresar2 = "", consulta ="";

	JSONObject resultado = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	
	
	if(informacion.equals("valoresIniciales")) {
		
		String programa = "</table align='center'>"+
								" <tr> "+
								"<td class='formas' colspan='1' align='left'><b>Programa: "+descProgramaFondoJR+"</b></td>"+
								"</tr>"+
								"</table>";
						
		resultado.put("success", new Boolean(true));
		resultado.put("programa", programa);	
		infoRegresar =resultado.toString();

	}
	else if (informacion.equals("DetalleDiasVencimiento"))
	{
			
				FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
				
				String fechaProbablePago = request.getParameter("fechaProbablePago")==null?"":request.getParameter("fechaProbablePago");
				HashMap detalle = fondoJunior.getDetalleDiasVencimiento(clavePrograma, fechaProbablePago);
				HashMap hash = new HashMap();
				ArrayList lista_detalle =  new ArrayList();
				
				if("true".equals(detalle.get("HAY_DETALLE"))){
					hash.put("HAY_DETALLE",detalle.get("HAY_DETALLE"));
					hash.put("NUMERO_OPERACION",detalle.get("NUMERO_OPERACION"));
					hash.put("DESCRIPCION",detalle.get("DESCRIPCION"));
				
				}
				lista_detalle.add(hash);
				jsObjArray = JSONArray.fromObject(lista_detalle);
				infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
				System.out.println(infoRegresar);
		}
	else if(informacion.equals("ConsultaGrid")  || informacion.equals("ConsultaSubtotales") || informacion.equals("GenerarPDF") || informacion.equals("GenerarCSV")  || informacion.equals("CatalogoBuscaAvanzada") | informacion.equals("buscaNombreProveedor") )
	{
	

		com.netro.fondosjr.ConsultaHistoricaMovimientosFondoJr paginador = new ConsultaHistoricaMovimientosFondoJr();
		paginador.setClavePrograma(clavePrograma);//vienen datos con el "3" clavePrograma);
		paginador.setFecVencIni(sFechaVencIni);
		paginador.setFecVencFin(sFechaVencFin);
		paginador.setFecOperIni(sFechaOperIni);
		paginador.setFecOperFin(sFechaOperFin); 
		paginador.setFecFideIni(sFechaPagoIni);
		paginador.setFecFideFin(sFechaPagoFin);
		
		//paginador.setPrestamo(sRegistro);
		paginador.setPrestamo(sPrestamo);
		paginador.setEstatus(sRegistro);
		paginador.setEstatusVal(sValidacion);
		paginador.setNumSirac(sNumCliente);
		paginador.setNombrePyme(sCliente);
		paginador.setOrden(sOrden);
		paginador.setDiasDesde(sdiasDesde);
		paginador.setDiasHasta(sdiasHasta);
		
		
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
		
		try
		{
			 start = Integer.parseInt(request.getParameter("start"));
			 limit = Integer.parseInt(request.getParameter("limit"));	
			
		} catch(Exception e)
		  { System.out.println("Error en parametros"); }
		  
		  
		  
		try 
		{
			if(informacion.equals("ConsultaGrid"))
			{
				if(operacion.equals("Generar"))
				{	//Nueva consulta, es decir la primera vez
					cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
			
				String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
				resultado = JSONObject.fromObject(consultar);
				infoRegresar=resultado.toString();
				//System.out.println(infoRegresar);
				
				
			} 
			
			else  if (informacion.equals("ConsultaSubtotales"))
			{
				infoRegresar  = cqhelper.getJSONResultCount(request);	//los saca de sesion
			}
			
			else if (informacion.equals("GenerarPDF"))
			{								
				String nombreArchivo = cqhelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp, "PDF");			
				//String nombreArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");			
				resultado.put("success", new Boolean(true));
				resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = resultado.toString();	
			}
			
			else if (informacion.equals("GenerarCSV"))
			{
				//String nombreArchivo = cqhelper.getCreatePageCustomFile(request, start,limit, strDirectorioTemp, "CSV");			
				String nombreArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
				
				resultado.put("success", new Boolean(true));
				resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = resultado.toString();	
			}
			
		} catch(Exception e) 
		  {	throw new AppException("Error en la paginacion", e);	}
	
		
		
		try 
		{	
			if (informacion.equals("buscaNombreProveedor"))
			{
				String nombrePyme = "";
				try {
			
					if(sNumCliente != null &&  !sNumCliente.equals("")){
						if(!Comunes.esNumeroEnteroPositivo(sNumCliente))	{
							throw new AppException("fondoJuniorValidacionSelPyme :: ERROR ::.. El numero de cliente no es un número valido");//error 
						}
					}
					
					FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
					
					List informacion_proveedores = fondoJunior.busquedaAvanzadaProveedor("", "", sNumCliente);
				   if(!informacion_proveedores.isEmpty()){
						List datos_proveedor = (List)informacion_proveedores.get(0);
						nombrePyme = (String)datos_proveedor.get(1);
					 
				  }
				  resultado.put("nombrePyme",nombrePyme);
					infoRegresar = resultado.toString();
					
			
				} catch(Exception e) {
					throw new AppException("fondoJuniorValidacionSelPyme :: ERROR ::..", e);
			
				  }
				  
				  
			}
			else if (informacion.equals("CatalogoBuscaAvanzada"))
			{
				if(!val_nombre.equals("") && (val_nombre.indexOf("*") == -1)){val_nombre = "*" + val_nombre + "*";}
				if(!val_rfc.equals("") && (val_nombre.indexOf("*") == -1)){val_rfc = "*" + val_rfc + "*";}
				
				//Instanciación para uso del EJB
				FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
				
				List informacion_proveedores = fondoJunior.busquedaAvanzadaProveedor(val_nombre, val_rfc, "");
				List lstInfoProv = new ArrayList();
				if(!informacion_proveedores.isEmpty())
				{
				  if(informacion_proveedores.size() < 500 && informacion_proveedores.size() > 0)
				  {
					 //AQUI BARRIDO DE LISTA PARA FORMAR OTRA LISTA DE HASHMAPS
					for (int i=0; i< informacion_proveedores.size(); i++)
					 { 
						 HashMap hmProv = new HashMap();
					    List lstProve = (List)informacion_proveedores.get(i);
						 hmProv.put("clave", (String)lstProve.get(0));
						 hmProv.put("descripcion",(String)lstProve.get(1));
						 lstInfoProv.add(hmProv);
					 }
					 
					 jsObjArray = JSONArray.fromObject(lstInfoProv);
					 resultado.put("registros",jsObjArray.toString());
					 resultado.put("msgRegistros", "");
					 infoRegresar = resultado.toString();
					 
				  }
				  else
				  {
					 resultado.put("msgRegistros", "Demasiado registros encontrados, por favor sea mas específico en la búsqueda");
					 resultado.put("registros","");
				  }
				}
				else
				{
				   resultado.put("msgRegistros", "No se encontró información");
					resultado.put("registros","");
				}
				
				resultado.put("success",new Boolean(true));
				infoRegresar = resultado.toString();
			}
			
		} catch(Exception e) 
		  {	throw new AppException("Error en Busqueda Avanzada", e);	}
			
			
		
		System.out.println("infoRegresar = "+infoRegresar); 
	}
		
		
	
%>
<%=infoRegresar%>
