Ext.onReady(function() {
 

	function procesarSuccessFailureAgregarEliminar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			
			jsonData = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp("nombre1").setValue('');
			Ext.getCmp("correo1").setValue('');
			Ext.getCmp("tipoDestinatario1").setValue('');
			
			Ext.getCmp("tipoDestinatario1").hide();
			Ext.getCmp("nombre1").hide();
			Ext.getCmp("correo1").hide();
			Ext.getCmp("btnGuardar").hide();
			Ext.getCmp("btnCancelar").hide();
			Ext.getCmp("tipoCatalogo1").show();
		
			
			fp.el.mask('Enviando...', 'x-mask-loading');			
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Consultar'							
				})
			});			
							
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	


	var eliminarRegistro = function(){
		
		var grid	= Ext.getCmp("gridConsulta"); 
		var seleccionados  	= grid.getSelectionModel().getSelections();
		if( seleccionados.length == 0){
			Ext.MessageBox.alert('Aviso','Debe seleccionar un registro.');
			return;
		}
		var ic_base;
		for(var i=0;i< seleccionados.length;i++){
			ic_correo = seleccionados[i].get('IC_CORREO');		
		}
		
		Ext.MessageBox.confirm('Confirmación','¿ Está seguro que desea eliminar el correo electrónico ?', function(resp){
			if(resp =='yes'){			
				Ext.Ajax.request({
					url : '33ParamCorreo.data.jsp',
						params: {
						informacion: 'Borrar',
							ic_correo:ic_correo	
					},
					callback: procesarSuccessFailureAgregarEliminar
				});
			}
		});			
	}
	

	var agregar = function(){
		
		Ext.getCmp('nombre1').show();
		Ext.getCmp('correo1').show();
		Ext.getCmp('tipoDestinatario1').show();
		Ext.getCmp('tipoCatalogo1').hide();
		Ext.getCmp('tipoCatalogo1').hide();
		
		Ext.getCmp('btnGuardar').show();
		Ext.getCmp('btnCancelar').show();	
		
	}
	
	
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var griConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			if(store.getTotalCount() > 0) {		
				el.unmask();					
			} else {		
				el.mask('No se encontró ningún registro', 'x-mask');				
			}
		}
	}
	
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '33ParamCorreo.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [		
			{	name: 'IC_CORREO'},		
			{	name: 'CG_NOMBRE'},		
			{	name: 'CG_CORREO'},		
			{	name: 'TIPO'}				
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
 
 	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Contenido del Catálogo',
		hidden: true,
		tbar:{ 
			xtype: 'toolbar',
			style: {
				borderWidth: 0
			},
			items:	[
				{
					text: 'Agregar Registro',
					iconCls: 'icon-register-add',            
					scope: this,
					handler: agregar
				}, 				
				'-',
				{
					text: 'Eliminar Registro',
					iconCls: 'borrar',           
					scope: this,
					handler: eliminarRegistro
				}
			] 
		},							
		columns: [	
			{
				header: '',
				tooltip: '',
				dataIndex: 'IC_CORREO',
				sortable: true,
				width: 50,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: '<div><div align=center >Nombre</div>',
				tooltip: 'CG_NOMBRE',
				dataIndex: 'CG_NOMBRE',
				sortable: true,
				width: 250,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: '<div><div align=center >Correo Electrónico</div>',
				tooltip: 'Correo Electrónico',
				dataIndex: 'CG_CORREO',
				sortable: true,
				width: 250,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: '<div><div align=center >Tipo</div>',
				tooltip: 'Tipo',
				dataIndex: 'TIPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			}
		],	
		sm: new Ext.grid.RowSelectionModel({singleSelect:false}), // para seleccionar el registro
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 710,		
		frame: true		
	});
	
 
	//**************Criterios de Captura *********************************

	var catTipoCatalogo = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [			
			['1','Parametrización de Correo']			
		 ]
	});
	
	var catTipoDestinatario  = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['D','Destinatiario'],
			['C','CC']			
		 ]
	});
	
	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'tipoCatalogo',	
			id: 'tipoCatalogo1',	
			fieldLabel: 'Catálogo', 
			mode: 'local',	
			hiddenName : 'tipoCatalogo',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catTipoCatalogo,	
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 200,
			listeners: {
				select: {
					fn: function(combo) {	
					
						fp.el.mask('Enviando...', 'x-mask-loading');			
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar'							
							})
						});								
					}
				}
			}
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Nombre',	
			name: 'nombre',
			id: 'nombre1',
			allowBlank: true,
			maxLength: 40,
			width: 200,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true
		},				
		{
			xtype			: 'textfield',
			id				: 'correo1',
			name			: 'correo',
			fieldLabel: ' Correo Electrónico',
			allowBlank	: true,
			maxLength	: 40,
			width			: 230,
			tabIndex		: 14,
			margins		: '0 20 0 0',
			vtype: 'email',
			hidden: true
		},
		{
			xtype: 'combo',	
			name: 'tipoDestinatario',	
			id: 'tipoDestinatario1',	
			fieldLabel: 'Tipo', 
			mode: 'local',	
			hiddenName : 'tipoDestinatario',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catTipoDestinatario,	
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 200,
			hidden: true
		}		
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		monitorValid: true,
		title: 'Catálogos de Parametrización ',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		buttons: [	
			{
				text: 'Guardar',
				id: 'btnGuardar',
				hidden: true,
				iconCls: 'icoGuardar',
				handler: function() {
					
					var nombre = Ext.getCmp("nombre1");
					if (Ext.isEmpty(nombre.getValue()) ) {
						nombre.markInvalid('Este campo es obligatorio.');
						return;
					}
					
					var correo = Ext.getCmp("correo1");
					if (Ext.isEmpty(correo.getValue()) ) {
						correo.markInvalid('Este campo es obligatorio.');
						return;
					}
					var tipoDestinatario = Ext.getCmp("tipoDestinatario1");
					if (Ext.isEmpty(tipoDestinatario.getValue()) ) {
						tipoDestinatario.markInvalid('Este campo es obligatorio.');
						return;
					}
					
					
					Ext.Ajax.request({
						url : '33ParamCorreo.data.jsp',
						params: {
							informacion: 'Agregar',
							nombre:Ext.getCmp("nombre1").getValue(),
							correo:Ext.getCmp("correo1").getValue(),
							tipoDestinatario:Ext.getCmp("tipoDestinatario1").getValue()							
							},
						callback: procesarSuccessFailureAgregarEliminar
					});		
				
								
				}
			},
			{
				text: 'Cancelar',
				id: 'btnCancelar',
				hidden: true,
				iconCls: 'icoCancelar',
				handler: function() {
					window.location = '33ParamCorreo.jsp';					
				}
			}
		]
	});

	
	
 
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,			
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
 

});