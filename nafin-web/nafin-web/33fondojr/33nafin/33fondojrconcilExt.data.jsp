<%@ page import=
					"java.util.*, java.sql.*, netropology.utilerias.*, com.netro.exception.*, java.math.*, netropology.utilerias.Registros,
					net.sf.json.*,com.netro.fondojr.*,com.netro.fondosjr.*"%>
<%@ page contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="../33secsession_extjs.jspf" %>

<%

String clavePrograma = (String) session.getAttribute("cveProgramaFondoJR");
String nombrePrograma = (String)request.getSession().getAttribute("descProgramaFondoJR");
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
String infoRegresar = "";

//Variables de consulta Detallada
String df_fecha_vencimiento= (request.getParameter("df_fecha_venci_") == null)?"":request.getParameter("df_fecha_venci_");
String df_fecha_vencimientoMax= (request.getParameter("df_fecha_venciMax_") == null)?"":request.getParameter("df_fecha_venciMax_");
String df_fecha_pagos= (request.getParameter("df_fecha_pago_") == null)?"":request.getParameter("df_fecha_pago_");
String df_fecha_pagosMax= (request.getParameter("df_fecha_pagoMax_") == null)?"":request.getParameter("df_fecha_pagoMax_");
String df_fecha_operacion= (request.getParameter("df_fecha_ope_") == null)?"":request.getParameter("df_fecha_ope_");
String df_fecha_operacionMax= (request.getParameter("df_fecha_opeMax_") == null)?"":request.getParameter("df_fecha_opeMax_");
String diass= (request.getParameter("dias_") == null)?"":request.getParameter("dias_");
String diassMax= (request.getParameter("diasMax_") == null)?"":request.getParameter("diasMax_");



JSONObject jsonObj = new JSONObject();
JSONArray jsonArr=new JSONArray();
List lstMontosConcentrado = new ArrayList();
String msgValidacion = "";

String fec_Actual = fHora.format(new java.util.Date());
StringBuffer contenidoArchivo = new StringBuffer();
String nombreArchivo = "";
CreaArchivo archivo = new CreaArchivo();
String fec_ultimo_mov = "";

String montoIni="",montoNP="",montoR ="", montoRF="",montoT="",montoOtros ="",montoFinal="",montoPenReem="";

	
	//Crear la intancia al EJB
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
	
	//Obtine la lista de Montos Concentrados
	lstMontosConcentrado = fondoJunior.consConciliacion(clavePrograma);



		if(lstMontosConcentrado!=null && lstMontosConcentrado.size()>0){
			fec_ultimo_mov = (String)lstMontosConcentrado.get(0);
			montoIni = (String)lstMontosConcentrado.get(1);
			montoNP = (String)lstMontosConcentrado.get(2);
			montoR = (String)lstMontosConcentrado.get(3);
			montoRF = (String)lstMontosConcentrado.get(4);
			montoT = (String)lstMontosConcentrado.get(5);
			montoOtros = (String)lstMontosConcentrado.get(6);
			montoFinal = (String)lstMontosConcentrado.get(7);
			montoPenReem = (String)lstMontosConcentrado.get(8);
			
		}


//Primer Grid
if(informacion.equals("valoresIniciales")) {
		
		String programa = "</table align='center'>"+
								" <tr> "+
								"<td class='formas' colspan='1' align='left'><b> "+descProgramaFondoJR+"</b><br> <br></td>"+
								"</tr>"+
							
								"</table>";
						
						
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("programa", programa);	
		infoRegresar =jsonObj.toString();

}
if (informacion.equals("Consultar")){

	try{											
			jsonObj.put ("FECHA_ULT_MOV",fec_ultimo_mov );
			jsonObj.put ("SALDO_INICIAL",Comunes.formatoDecimal(montoIni.toString(),2,true) );
			jsonObj.put ("TOTAL_RETIROS",Comunes.formatoDecimal(montoNP.toString(),2,true) );
			jsonObj.put ("TOTAL_REE_FIDE",Comunes.formatoDecimal(montoR.toString(),2,true) );
			jsonObj.put ("DESE_PENDIENTES",Comunes.formatoDecimal(montoRF.toString(),2,true) );
			jsonObj.put ("PREPAGO_TOTALES",Comunes.formatoDecimal(montoT.toString(),2,true) );
			jsonObj.put ("OTROS_MOVI",Comunes.formatoDecimal(montoOtros.toString(),2,true ));
			jsonObj.put ("SALDO_FINAL",Comunes.formatoDecimal(montoFinal.toString(),2,true));
			jsonObj.put ("RETIROS_PEN",Comunes.formatoDecimal(montoPenReem.toString(),2,true));
		
			jsonArr.add(jsonObj);
			
	contenidoArchivo.append(nombrePrograma + ",\n");
	contenidoArchivo.append("SALDO FONDO JUNIOR AL "+fec_Actual+"\n");
	contenidoArchivo.append("Fecha Último Movimiento, Saldo Inicial, Total Retiros,Total Reembolsos FIDE,Desembolsos Pendientes de Recuperar,");
	contenidoArchivo.append("Prepagos Totales,Otros Movimientos,Saldo Final,Retiros Pendientes de Desembolsos\n");

	contenidoArchivo.append(fec_ultimo_mov+","+Comunes.formatoDecimal(montoIni.toString(),2,false)+","+
															Comunes.formatoDecimal(montoNP.toString(),2,false)+","+
															Comunes.formatoDecimal(montoR.toString(),2,false)+","+
															Comunes.formatoDecimal(montoRF.toString(),2,false)+","+
															Comunes.formatoDecimal(montoT.toString(),2,false)+","+
															Comunes.formatoDecimal(montoOtros.toString(),2,false)+","+
															Comunes.formatoDecimal(montoFinal.toString(),2,false)+","+
															Comunes.formatoDecimal(montoPenReem.toString(),2,false)
															);
															
						if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
							out.print("<--!Error al generar el archivo-->");
						else
							nombreArchivo = archivo.nombre;
		
			infoRegresar= "({\"success\": true, \"registros\": "+jsonArr.toString()+", \"nombrePrograma\":\""+nombrePrograma+ "\" ,\"fec_Actual\":\""+fec_Actual+ "\",\"clavePrograma\":\""+clavePrograma+ "\",\"URLArchivoCSV\":\""+strDirecVirtualTemp + nombreArchivo+"\"})";  

	
	} catch(Exception e) {
			throw new AppException("Error al obetener los datos", e);
	}
}


//Generar ZIP Primer Grid
if (operacion.equals("GenerarZip")){
		//Eliminar lo que hay en session para no mostrar el ZIP descargado anteriomente
		session.removeAttribute("GeneraArchivoConciliacionZipThread"); 
		
		//Crear una nueva session para mostrar el nuevo ZIP
		com.netro.fondojr.GeneraArchivoConciliacionZipThread GeneraArchivoConciliacionZipThread = new com.netro.fondojr.GeneraArchivoConciliacionZipThread();
		session.setAttribute("GeneraArchivoConciliacionZipThread", GeneraArchivoConciliacionZipThread );

		FondoJrConciliacionBean fondojr = new FondoJrConciliacionBean();
	
		fondojr.setClavePrograma(clavePrograma);
	
		GeneraArchivoConciliacionZipThread.setForm(fondojr);
		GeneraArchivoConciliacionZipThread.setForm(fondojr);
		GeneraArchivoConciliacionZipThread.setRutaFisica(strDirectorioTemp);
		GeneraArchivoConciliacionZipThread.setRutaVirtual(strDirectorioTemp);
		GeneraArchivoConciliacionZipThread.setClavePrograma(clavePrograma);
		GeneraArchivoConciliacionZipThread.setNombrePrograma(nombrePrograma);
		GeneraArchivoConciliacionZipThread.setRunning(true);
		new Thread(GeneraArchivoConciliacionZipThread).start();
		infoRegresar= "({\"success\":true,\"isRunning\":true })"; 

}
//Sigue leyendo (para archivos pesados)
if (operacion.equals("SeguirLeyendo")){
 
	 com.netro.fondojr.GeneraArchivoConciliacionZipThread GeneraArchivoConciliacionZipThread = 
	(com.netro.fondojr.GeneraArchivoConciliacionZipThread)session.getAttribute("GeneraArchivoConciliacionZipThread");

 
	if(GeneraArchivoConciliacionZipThread.hasError()){

			msgValidacion="Error inesperado al generar archivo ZIP";
			jsonObj.put("msgValidacion", msgValidacion);
			infoRegresar= jsonObj.toString();
	}

	if(GeneraArchivoConciliacionZipThread.getNombreArchivo()==null && GeneraArchivoConciliacionZipThread.isRunning()){

			infoRegresar= "({\"success\":true,\"isRunning\":true })"; 

	}else if(!GeneraArchivoConciliacionZipThread.isRunning() ){

			infoRegresar= "({\"success\": true,\"isRunning\":false, \"nombrePrograma\":\""+nombrePrograma+ "\" ,\"clavePrograma\":\""+clavePrograma+ "\",\"URLArchivoZip\":\""+strDirecVirtualTemp+GeneraArchivoConciliacionZipThread.getNombreArchivo()+"\"})"; 
	}
	
}
	

//Procesar informacion de la pantalla Ver Movimientos Detalle	
if (informacion.equals("consultarDetalle")||informacion.equals("ConsultarTotales") ){

	int start = 0;
	int limit = 0;
	
	List lstFondoConcentradoDet = new ArrayList();			
		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(); 
		
				//Se crea la instacia del Paginador
				ConsultaDetalleMovimientos consultaDetalleMovimientos= new ConsultaDetalleMovimientos();
				//Instancia para el uso del paginador
				consultaDetalleMovimientos.setClavePrograma(clavePrograma);
				consultaDetalleMovimientos.setFecVencIni(df_fecha_vencimiento);
				consultaDetalleMovimientos.setFecVencFin(df_fecha_vencimientoMax);
				consultaDetalleMovimientos.setFecOperIni(df_fecha_operacion);
				consultaDetalleMovimientos.setFecOperFin(df_fecha_operacionMax);
				consultaDetalleMovimientos.setFecFideIni(df_fecha_pagos);
				consultaDetalleMovimientos.setFecFideFin(df_fecha_pagosMax);
				consultaDetalleMovimientos.setDiasDesde(diass);
				consultaDetalleMovimientos.setDiasHasta(diassMax);
				
				queryHelper = new CQueryHelperRegExtJS(consultaDetalleMovimientos);  
				

		if (informacion.equals("consultarDetalle")){									
			
				try {
				
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));
					
					} catch(Exception e) {
					
					throw new AppException("Error en los parametros recibidos", e);
					}

	
					try {
				
							if (operacion.equals("Generar")) {	
								queryHelper.executePKQuery(request);	
							}
							
				    Registros reg = queryHelper.getPageResultSet(request,start,limit);
					//infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit); 
					
					
						while (reg.next()){
							
							String color = getColor(reg.getString("dias_transcurridos_vencimiento"), reg.getString("dias_limite"),reg.getString("estatus"));
							String dias_trans= getLink(color,("NP".equals(reg.getString("estatus"))?reg.getString("dias_transcurridos_vencimiento"):"N/A"),reg.getString("fec_venc"));
							
							
							reg.setObject ("dias_transcurridos_vencimiento",dias_trans);
							
						
						
							
						
					  }
			
					infoRegresar= "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + 
							"\", \"registros\": " + reg.getJSONData() + "}";
					
				
					} catch(Exception e) {
						
						throw new AppException("Error en la paginacion", e);
					}	
		
		}else if (informacion.equals("ConsultarTotales")) {//Datos para el Resumen de Totales
				//Debe ser llamado despues de Consulta
				queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
				infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion	
			
		}

}

if (operacion.equals("GenerarArchivoDetalle")){


		String tipo= (request.getParameter("tipo") == null)?"":request.getParameter("tipo");
		String contentType = "";
		String archivoCSV = "";
		String archivoZIP = "";
		String URLarchivoCSVDet = "";
		String URLarchivoZIPDet = "";


		try {
		
				FondoJrConciliacionBean fondoJrConciliacionBean = new FondoJrConciliacionBean();
				fondoJrConciliacionBean.setClavePrograma(clavePrograma);
				
				fondoJrConciliacionBean.setStrDirectorioPublicacion(strDirectorioPublicacion);
				fondoJrConciliacionBean.setDescProgramaFondoJR(nombrePrograma);
				
				fondoJrConciliacionBean.setFecVencIni(df_fecha_vencimiento);
				fondoJrConciliacionBean.setFecVencFin(df_fecha_vencimientoMax);
				fondoJrConciliacionBean.setFecOperIni(df_fecha_operacion);
				fondoJrConciliacionBean.setFecOperFin(df_fecha_operacionMax);
				fondoJrConciliacionBean.setFecFideIni(df_fecha_pagos);
				fondoJrConciliacionBean.setFecFideFin(df_fecha_pagosMax);
				fondoJrConciliacionBean.setDiasDesde(diass);
				fondoJrConciliacionBean.setDiasHasta(diassMax);
		
				strDirectorioTemp = strDirectorioPublicacion + "00tmp/33fondojr/";
			
	
			if (tipo != null && tipo.equals("CSV")) {
				
				archivoCSV = fondoJunior.generaCSV(fondoJrConciliacionBean, strDirectorioTemp.trim(), strDirectorioPublicacion);
					
				URLarchivoCSVDet= strDirecVirtualTemp + archivoCSV;
				infoRegresar= "({\"success\": true,\"URLarchivoCSVDet\":\""+URLarchivoCSVDet+"\"})"; 
			
			}
			
			
			if (tipo != null && tipo.equals("ZIP")) {					
				archivoZIP = fondoJunior.generaZIP(fondoJrConciliacionBean, strDirectorioTemp.trim(), strDirectorioPublicacion);
					
				URLarchivoZIPDet= strDirecVirtualTemp + archivoZIP;
				infoRegresar= "({\"success\": true,\"URLarchivoZIPDet\":\""+URLarchivoZIPDet+"\"})"; 
			
			}
							
		} catch(Exception e) {
			throw new AppException("Error al generar el archivo CSV o ZIP", e);
		}
		
}
  
  
  
  
  
String amort ="";
String interes ="";
String saldo ="";
String fecha_venci="";
String  fecha_fide="";
String fecha_cliente="";
String prestamo_re="";
String num_cliente="";
String nom_cliente="";
String num_cuota="";
String estatus_re="";
String origen="";
String titulo = ""; 
String dias_trans= ""; 
String dias_limite= ""; 




if (informacion.equals("ConsultarDetalleMov")||informacion.equals("ArchivoDetMov")){ 


		String prestamo= (request.getParameter("prestamo") == null)?"":request.getParameter("prestamo");
		String estatus= (request.getParameter("estatus") == null)?"":request.getParameter("estatus");

		List lstFondoConcentradoDetPop = new ArrayList();
		lstFondoConcentradoDetPop = fondoJunior.consConciliacionPop(prestamo, estatus, clavePrograma);

		if(lstFondoConcentradoDetPop!=null && lstFondoConcentradoDetPop.size()>0){


				for(int x=0;x<lstFondoConcentradoDetPop.size();x++){
						List lstFondoConcentradoRegPop = (List)lstFondoConcentradoDetPop.get(x);
					
					
						if(x==0){
								if(((String)lstFondoConcentradoRegPop.get(5)).equals("P"))titulo="PAGADO";
								if(((String)lstFondoConcentradoRegPop.get(5)).equals("NP"))titulo="NO PAGADO";
								if(((String)lstFondoConcentradoRegPop.get(5)).equals("R"))titulo="REEMBOLSOS";
								if(((String)lstFondoConcentradoRegPop.get(5)).equals("RF"))titulo="DESEMBOLSOS POR FINIQUITO";
								if(((String)lstFondoConcentradoRegPop.get(5)).equals("T"))titulo="TOTALES";
					
					
								fecha_venci=(String)lstFondoConcentradoRegPop.get(0);
								fecha_fide=(String)lstFondoConcentradoRegPop.get(1);
								fecha_cliente=(String)lstFondoConcentradoRegPop.get(10);
								prestamo_re=(String)lstFondoConcentradoRegPop.get(9);
								num_cliente=(String)lstFondoConcentradoRegPop.get(11);
								nom_cliente=(String)lstFondoConcentradoRegPop.get(12);
								num_cuota=(String)lstFondoConcentradoRegPop.get(14);
								estatus_re=(String)lstFondoConcentradoRegPop.get(5);
								amort =(String)lstFondoConcentradoRegPop.get(2);
								interes =(String)lstFondoConcentradoRegPop.get(3);
								saldo = (String)lstFondoConcentradoRegPop.get(4);
								origen=(String)lstFondoConcentradoRegPop.get(13);
								dias_trans=(String)lstFondoConcentradoRegPop.get(15);
								dias_limite=(String)lstFondoConcentradoRegPop.get(16);
						}
				}//cierre for									
	}//cierre if

	

				if (informacion.equals("ConsultarDetalleMov")){ 
				
					jsonObj.put ("FEC_VENC", fecha_venci);
					jsonObj.put ("FEC_FIDE",fecha_fide);
					jsonObj.put ("FEC_CLIENTE",fecha_cliente);
					jsonObj.put ("PRESTAMO",prestamo_re);
					jsonObj.put ("NUM_CLIENTE",num_cliente);
					jsonObj.put ("NOMBRECLIENTE",nom_cliente);
					jsonObj.put ("NUM_CUOTA",num_cuota);
					jsonObj.put ("ESTATUS",estatus_re);
					jsonObj.put ("AMORT",Comunes.formatoDecimal(amort.toString(),2,true));
					jsonObj.put ("INTERES",Comunes.formatoDecimal(interes.toString(),2,true));
					jsonObj.put ("SALDO",Comunes.formatoDecimal(saldo.toString(),2,true));
					jsonObj.put ("ORIGEN_DESC",origen);
					jsonObj.put ("DIAS_TRANSCURRIDOS_VENCIMIENTO",dias_trans);
					jsonObj.put ("DIAS_LIMITE",dias_limite);
				
					jsonArr.add(jsonObj);
				
					infoRegresar= "({\"success\": true, \"registros\": "+jsonArr.toString()+", \"estatus\": '"+estatus+"'})";  
				}


			if (informacion.equals("ArchivoDetMov")){ 
					try{
						contenidoArchivo.append("DETALLE DE MOVIMIENTOS\n");
						contenidoArchivo.append(titulo+"\n");
						contenidoArchivo.append("Fecha Amortizacion Nafin,Fecha Operación,Fecha Pago Cliente FIDE,Prestamo,Num.");
						contenidoArchivo.append("Cliente SIRAC,Cliente,Num. Cuota a Pagar,Dias Transcurridos de Vencimiento,Capital,Interes,Total a Pagar,Origen\n");
					

						contenidoArchivo.append(fecha_venci+","+
						fecha_fide+","+
						fecha_cliente+","+
						prestamo_re+","+
						(num_cliente!=null?(num_cliente).replaceAll("[,]"," "):"")+","+														
						nom_cliente. replaceAll(",", "")+","+
						num_cuota+","+
						reemplazaCero(dias_trans,"N/A",estatus_re)+","+
						amort +","+
						interes+","+
						saldo+","+
						origen+"\n");
							

						if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
							out.print("<--!Error al generar el archivo-->");
						else
							nombreArchivo = archivo.nombre;	
		
			infoRegresar= "({\"success\": true,\"URLArchivoCSVPOP\":\""+strDirecVirtualTemp + nombreArchivo+"\"})";  
			
					}catch(Exception e){
					throw new AppException("No se puedo obtener datos", e);
					}
			}
}

if (informacion.equals("ConsultarDetalleDias")){ 

			String fechaProbablePago= (request.getParameter("fechaProbablePago") == null)?"":request.getParameter("fechaProbablePago");
			HashMap detalle = fondoJunior.getDetalleDiasVencimiento(clavePrograma, fechaProbablePago);

		try{
			  
				jsonObj.put("IC_NUM_OPERACION",(String) detalle.get("NUMERO_OPERACION"));
				jsonObj.put("CG_DETALL",(String) detalle.get("DESCRIPCION"));
         	
				jsonArr.add(jsonObj);
					
				infoRegresar= "({\"success\": true, \"registros\": "+jsonArr.toString()+"})";  
				
		}catch(Exception e){
			throw new AppException("No se puedo obtener datos", e);
		}

}

%>
<%!




%>
<%=infoRegresar%>
<%!


public String reemplazaCero(String diasTranscurridosVencimiento,String reemplazo, String estatus){
		
		String resultado = null;    
		try {
			int numero = Integer.parseInt(diasTranscurridosVencimiento);	
			if( numero == 0 ){
				if(!"NP".equals(estatus))
					resultado = reemplazo;
				else
					resultado = diasTranscurridosVencimiento;
			}else{
				resultado = diasTranscurridosVencimiento;
			}
		}catch(Exception e){
			resultado = diasTranscurridosVencimiento;
		}
		
		return resultado;
	}

 
%>

<%!

	public String getColor(String diasTranscurridosVencimiento, String diasLimite, String estatus){
 
		String color = "black";
		
		if(!"NP".equals(estatus)){
			return color;
		}
		
		if( diasTranscurridosVencimiento != null && !diasTranscurridosVencimiento.trim().equals("") && diasLimite != null && !diasLimite.trim().equals("")){
			try {
				int numeroDiasTranscurridos 	= Integer.parseInt(diasTranscurridosVencimiento);
				int numeroDiasLimite				= Integer.parseInt(diasLimite);
				
				if(numeroDiasTranscurridos > numeroDiasLimite){
					color = "red";
				}
			}catch(Exception e){
				color							= "black";
			}
		}
 
		return color;
	}
	
	public String reemplazaCero(String diasTranscurridosVencimiento,String reemplazo){
		
		String resultado = null;    
		try {
			int numero = Integer.parseInt(diasTranscurridosVencimiento);	
			if( numero == 0 ){
				resultado = reemplazo;
			}else{
				resultado = diasTranscurridosVencimiento;
			}
		}catch(Exception e){
			resultado = diasTranscurridosVencimiento;
		}
		
		return resultado;
	}
	
	public String getLink(String colorRegistro, String diasTranscurridos, String fechaProbablePago){
		
		if(!"red".equals(colorRegistro)){
			return diasTranscurridos;
		}else	if(fechaProbablePago == null || fechaProbablePago.trim().equals("")){
			return diasTranscurridos;
		}else if(diasTranscurridos.equals("N/A")){
			return diasTranscurridos;
		}else{
			try {
				
				int numero = Integer.parseInt(diasTranscurridos);	
				if( numero == 0 ) return diasTranscurridos;
				
			}catch(Exception e){
				return diasTranscurridos;
			}	
		}
		return diasTranscurridos;
		//return "<a href=\"javascript:showDetalleDiasVencimiento('"+fechaProbablePago+"');\" style=\"color:red;text-decoration:underline;\" >"+diasTranscurridos+"</a>";
		
	}
	
%>