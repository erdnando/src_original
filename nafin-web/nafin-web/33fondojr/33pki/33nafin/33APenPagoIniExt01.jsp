<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>

<html>
<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<%@ include file="/extjs.jspf" %>
		<%@ include file="/01principal/menu.jspf"%>
		<title>Nafin@net - Amortizaciones Pendientes de Pago</title>
		<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
		<%@ include file="/00utils/componente_firma.jspf" %>
		<script type="text/javascript" src="/nafin/33fondojr/33pki/33nafin/33APenPagoIniExt01.js?<%=session.getId()%>"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS)  { %>
	 <%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
      <div id='areaContenido' style="margin-left: 3px; margin-top: 3px; text-align:center"></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
	<input type="hidden" id="hidDescProgFondoJR" value="<%=descProgramaFondoJR%>">
<%}else  {%>
	<div id="areaContenido"><div style="height:190px"></div></div>
	<form id='formAux' name="formAux" target='_new'></form>
	<!-- Valores iniciales recibidos como parametros-->
	<input type="hidden" id="hidDescProgFondoJR" value="<%=descProgramaFondoJR%>">
<%}%>
</body>
</html>