<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.*,
		javax.servlet.http.HttpServletRequest,
		javax.servlet.http.HttpServletResponse,
		netropology.utilerias.usuarios.*,
		java.util.*,
		java.text.SimpleDateFormat,
		java.math.*,
		com.netro.fondojr.*,
		com.netro.fondosjr.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String tipoMonitor  = request.getParameter("tipoMonitor")==null?"":request.getParameter("tipoMonitor");

/*	SE CREA NUEVO OBJETO PARA ACCESAR EL EJB */
FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

if(informacion.equals("descargaArchivosValidacion")) {
	CreaArchivo archivo = new CreaArchivo();
	//File file = null;
	String login_usuario = (String)request.getSession().getAttribute("Clave_usuario");
	String nombre_usuario = (String)request.getSession().getAttribute("strNombreUsuario");
	String nombrePrograma = (String)request.getSession().getAttribute("descProgramaFondoJR");//FODEA 026 - 2010 ACF
	String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");//FODEA 026 - 2010 ACF
	String tipo_archivo = (request.getParameter("tipo_archivo")==null)?"":request.getParameter("tipo_archivo");
	//String contentType = "text/html;charset=ISO-8859-1";
	//strDirectorioTemp = strDirectorioPublicacion + "00tmp/15cadenas/";
	String nombreArchivo = "";
	
//	try{
		
		String fecha_amort_nafin_ini = (request.getParameter("fecha_amort_nafin_ini")==null)?"":request.getParameter("fecha_amort_nafin_ini");
		String fecha_amort_nafin_fin = (request.getParameter("fecha_amort_nafin_fin")==null)?"":request.getParameter("fecha_amort_nafin_fin");
		String estatus_registro = (request.getParameter("estatus_registro")==null)?"":request.getParameter("estatus_registro");
		String numero_prestamo= (request.getParameter("numero_prestamo")==null)?"":request.getParameter("numero_prestamo");
		String clave_if = (request.getParameter("clave_if")==null)?"":request.getParameter("clave_if");
		String numero_sirac = (request.getParameter("numero_sirac")==null)?"":request.getParameter("numero_sirac");
		String fecha_pago_cliente_fide_ini = (request.getParameter("fecha_pago_cliente_fide_ini")==null)?"":request.getParameter("fecha_pago_cliente_fide_ini");
		String fecha_pago_cliente_fide_fin = (request.getParameter("fecha_pago_cliente_fide_fin")==null)?"":request.getParameter("fecha_pago_cliente_fide_fin");
		String fecha_registro_fide_ini = (request.getParameter("fecha_registro_fide_ini")==null)?"":request.getParameter("fecha_registro_fide_ini");
		String fecha_registro_fide_fin = (request.getParameter("fecha_registro_fide_fin")==null)?"":request.getParameter("fecha_registro_fide_fin");
		String fecha_amortizacion = (request.getParameter("fecha_amortizacion")==null)?"":request.getParameter("fecha_amortizacion");
		String cs_subtotales = (request.getParameter("cs_subtotales")==null)?"":request.getParameter("cs_subtotales");
		String seccion_estatus = (request.getParameter("seccion_estatus")==null)?"":request.getParameter("seccion_estatus");
		String tipo_validacion = (request.getParameter("tipo_validacion")==null)?"":request.getParameter("tipo_validacion");
		
		if(tipo_archivo.equals("PREACUSE_PDF")){
			
			String cs_validacion = "";  
		  
			if(cs_subtotales == null){cs_subtotales = "N";}
        
			if (seccion_estatus.equals("PNP")) {
				fecha_amort_nafin_ini = fecha_amortizacion;
				fecha_amort_nafin_fin = fecha_amortizacion;		
			}
			
			StringBuffer texto_plano = new StringBuffer();
			
			List parametros_consulta = new ArrayList();
			parametros_consulta.add(fecha_amort_nafin_ini);
			parametros_consulta.add(fecha_amort_nafin_fin);
			parametros_consulta.add(estatus_registro);
			parametros_consulta.add(numero_prestamo);
			parametros_consulta.add(clave_if);
			parametros_consulta.add(numero_sirac);
			parametros_consulta.add(fecha_pago_cliente_fide_ini);
			parametros_consulta.add(fecha_pago_cliente_fide_fin);
			parametros_consulta.add(fecha_registro_fide_ini);
			parametros_consulta.add(fecha_registro_fide_fin);
			parametros_consulta.add(cs_validacion);
			parametros_consulta.add(clavePrograma);//FODEA 026 - 2010 ACF
			parametros_consulta.add("");//AJUSTE VALIDACION
        
        List operaciones_validacion = fondoJunior.obtenerOperacionesValidacion(parametros_consulta);

        nombreArchivo = archivo.nombreArchivo()+".pdf";
        
        ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
        
        String pais        = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
        String noCliente    = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
        String nombre      = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
        String nombreUsr   = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
        String logo      	 = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
  
        pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
				//FODEA 026 - 2010 ACF (I)
				pdfDoc.addText(nombrePrograma, "titulo", ComunesPDF.CENTER);
				pdfDoc.addText("\n", "formas", ComunesPDF.CENTER);
				//FODEA 026 - 2010 ACF (F)
        List reembolsos = new ArrayList();
        List prepagos = new ArrayList();
        List recuperaciones = new ArrayList();
        BigDecimal monto_capital = new BigDecimal("0.00");
        BigDecimal monto_interes = new BigDecimal("0.00");
        BigDecimal monto_total = new BigDecimal("0.00");
        int num_registros = 0;

        for(int i = 0; i< operaciones_validacion.size(); i++){
          List operaciones_por_fecha = (List)operaciones_validacion.get(i);
          String fecha_vencimiento = (String)operaciones_por_fecha.get(0);
          List operaciones_pagados = (List)operaciones_por_fecha.get(1);
          List operaciones_no_pagados = (List)operaciones_por_fecha.get(2);
          List operaciones_reembolsos = (List)operaciones_por_fecha.get(3);
          List operaciones_prepagos = (List)operaciones_por_fecha.get(4);
          List operaciones_recuperaciones = (List)operaciones_por_fecha.get(5);
          
          if(!operaciones_reembolsos.isEmpty()){
            for(int j = 0; j < operaciones_reembolsos.size(); j++){
              reembolsos.add((List)operaciones_reembolsos.get(j));
            }
          }
          if(!operaciones_prepagos.isEmpty()){
            for(int j = 0; j < operaciones_prepagos.size(); j++){
              prepagos.add((List)operaciones_prepagos.get(j));
            }
          }
          if(!operaciones_recuperaciones.isEmpty()){
            for(int j = 0; j < operaciones_recuperaciones.size(); j++){
              recuperaciones.add((List)operaciones_recuperaciones.get(j));
            }
          }
          
          String dia = fecha_vencimiento.substring(0, fecha_vencimiento.indexOf("/"));
          String mes = fecha_vencimiento.substring(fecha_vencimiento.indexOf("/") + 1, fecha_vencimiento.lastIndexOf("/"));
          String anio = fecha_vencimiento.substring(fecha_vencimiento.lastIndexOf("/") + 1);
          String tot_cap_pag = "";
          String tot_int_pag = "";
          String tot_ven_pag = "";
          String tot_cap_npag = "";
          String tot_int_npag = "";
          String tot_ven_npag = "";
          String registros_pagados = "0";
          String registros_no_pagados = "0";
          int colspan = 1;
          
          if(mes.equals("01")){mes = "Enero";
          }else if(mes.equals("02")){mes = "Febrero";
          }else if(mes.equals("03")){mes = "Marzo";
          }else if(mes.equals("04")){mes = "Abril";
          }else if(mes.equals("05")){mes = "Mayo";
          }else if(mes.equals("06")){mes = "Junio";
          }else if(mes.equals("07")){mes = "Julio";
          }else if(mes.equals("08")){mes = "Agosto";
          }else if(mes.equals("09")){mes = "Septiembre";
          }else if(mes.equals("10")){mes = "Octubre";
          }else if(mes.equals("11")){mes = "Noviembre";
          }else if(mes.equals("12")){mes = "Diciembre";}
          
          if(!operaciones_pagados.isEmpty()){
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            for(int j = 0; j < operaciones_pagados.size(); j++){
              List operacion_por_fecha = (List)operaciones_pagados.get(j);
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
            }
            tot_ven_pag = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
            registros_pagados = Integer.toString(num_registros);
            colspan++;
          }
         
          if(!operaciones_no_pagados.isEmpty()){
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            for(int j = 0; j < operaciones_no_pagados.size(); j++){
              List operacion_por_fecha = (List)operaciones_no_pagados.get(j);
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
            }
            tot_ven_npag = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
            registros_no_pagados = Integer.toString(num_registros);
            colspan++;
          }
          
          if(seccion_estatus.equals("PNP")){
            pdfDoc.addText("Preacuse Validación:" + (tipo_validacion.equals("ACEPTAR")?"Aceptación":"Rechazo"), "titulo", ComunesPDF.CENTER);
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            
            float anchoCelda2[] = {50f, 50f};
            float anchoCelda3[] = {40f, 30f, 30f};
            
            if(colspan == 2){
              pdfDoc.setTable(2, 50, anchoCelda2);
            }else{
              pdfDoc.setTable(3, 50, anchoCelda3);
            }
            
            if(!operaciones_pagados.isEmpty() || !operaciones_no_pagados.isEmpty()){
              pdfDoc.setCell("", "celda01", ComunesPDF.CENTER, 1);
              if(!registros_pagados.equals("0")){
                pdfDoc.setCell("Pagados", "celda01", ComunesPDF.CENTER, 1);
              }
              if(!registros_no_pagados.equals("0")){
                pdfDoc.setCell("No Pagados", "celda01", ComunesPDF.CENTER, 1);
              }
              
              pdfDoc.setCell("No. Total de Creditos " + (tipo_validacion.equals("ACEPTAR")?"Aceptados":"Rechazados"), "formas", ComunesPDF.RIGHT, 1);
              if(!registros_pagados.equals("0")){
                pdfDoc.setCell(registros_pagados, "formas", ComunesPDF.RIGHT, 1);
              }
              if(!registros_no_pagados.equals("0")){
                pdfDoc.setCell(registros_no_pagados, "formas", ComunesPDF.RIGHT, 1);
              }

              pdfDoc.setCell("Monto Total de Creditos " + (tipo_validacion.equals("ACEPTAR")?"Aceptados":"Rechazados"), "formas", ComunesPDF.RIGHT, 1);
              if(!registros_pagados.equals("0")){
                pdfDoc.setCell("$" + tot_ven_pag, "formas", ComunesPDF.RIGHT, 1);
              }
              if(!registros_no_pagados.equals("0")){
                pdfDoc.setCell("$" + tot_ven_npag, "formas", ComunesPDF.RIGHT, 1);
              }
              
              pdfDoc.setCell("Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisión tendrá validez para todos los efectos legales.", "formas", ComunesPDF.JUSTIFIED, colspan);
              pdfDoc.addTable();
            }
            //..:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CREDITOS PAGADOS Y/O NO PAGADOS
            if(!operaciones_pagados.isEmpty() || !operaciones_no_pagados.isEmpty()){
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
              pdfDoc.addText("Fecha de Vencimiento: " + dia + " de " + mes + " de " + anio, "titulo", ComunesPDF.LEFT);
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            }
            //..:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CREDITOS PAGADOS
            if(!operaciones_pagados.isEmpty()){
              float anchoCelda14[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
              pdfDoc.setTable(14, 100, anchoCelda14);
              monto_capital = new BigDecimal("0.00");
              monto_interes = new BigDecimal("0.00");
              monto_total = new BigDecimal("0.00");
              num_registros = 0;
              
              pdfDoc.setCell("Pagados", "celda01", ComunesPDF.CENTER, 14);
              
              pdfDoc.setCell("Fecha de Amortización Nafin", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Fecha de Operación", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Fecha de Pago Cliente FIDE", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Prestamo", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Intereses", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER, 1);
              
              pdfDoc.setHeaders();
              
              for(int j = 0; j < operaciones_pagados.size(); j++){
                List operacion_por_fecha = (List)operaciones_pagados.get(j);
                monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
                monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
                pdfDoc.setCell("" + operacion_por_fecha.get(0), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(1), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(3), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(4), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(5), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(6), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(7), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(8), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(9), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(10), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(11), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("En Proceso", "formas", ComunesPDF.CENTER, 1);
              }
              //pdfDoc.addTable();
              
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
              
              tot_cap_pag = Comunes.formatoDecimal(monto_capital.toPlainString(), 2);
              tot_int_pag = Comunes.formatoDecimal(monto_interes.toPlainString(), 2);
              tot_ven_pag = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
              if(cs_subtotales.equals("S")){
                //float anchoCelda14a[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
                //pdfDoc.setTable(14, 100, anchoCelda14a);

                pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 6);
                pdfDoc.setCell("Subtotal", "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_capital.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_interes.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_total.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("Num. Registros Pagados", "celda01", ComunesPDF.RIGHT, 2);
                pdfDoc.setCell(Integer.toString(num_registros), "celda01", ComunesPDF.RIGHT, 1);
                
                //pdfDoc.addTable();
              }
              pdfDoc.addTable();
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            }
            
            //..:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CREDITOS NO PAGADOS
            if(!operaciones_no_pagados.isEmpty()){
              float anchoCelda14[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
              pdfDoc.setTable(14, 100, anchoCelda14);
              monto_capital = new BigDecimal("0.00");
              monto_interes = new BigDecimal("0.00");
              monto_total = new BigDecimal("0.00");
              num_registros = 0;

              pdfDoc.setCell("No Pagados", "celda01", ComunesPDF.CENTER, 14);
              
              pdfDoc.setCell("Fecha de Amortización Nafin", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Fecha de Operación", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Fecha de Pago Cliente FIDE", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Prestamo", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Intereses", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER, 1);
              
              pdfDoc.setHeaders();

              for(int j = 0; j < operaciones_no_pagados.size(); j++){
                List operacion_por_fecha = (List)operaciones_no_pagados.get(j);
                monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
                monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
                pdfDoc.setCell("" + operacion_por_fecha.get(0), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(1), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(3), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(4), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(5), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(6), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(7), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(8), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(9), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(10), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(11), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("En Proceso", "formas", ComunesPDF.CENTER, 1);
              }
              //pdfDoc.addTable();
              
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
              
              tot_cap_npag = Comunes.formatoDecimal(monto_capital.toPlainString(), 2);
              tot_int_npag = Comunes.formatoDecimal(monto_interes.toPlainString(), 2);
              tot_ven_npag = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
              if(cs_subtotales.equals("S")){
                //float anchoCelda14a[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
                //pdfDoc.setTable(14, 100, anchoCelda14a);

                pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 6);
                pdfDoc.setCell("Subtotal", "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_capital.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_interes.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_total.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("Num. Registros No Pagados", "celda01", ComunesPDF.RIGHT, 2);
                pdfDoc.setCell(Integer.toString(num_registros), "celda01", ComunesPDF.RIGHT, 1);
                
                //pdfDoc.addTable();
              }
              pdfDoc.addTable();
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            }
            
            if(!operaciones_pagados.isEmpty() || !operaciones_no_pagados.isEmpty()){
              //pdfDoc.newPage();
              float anchoCelda5[] = {20f, 20f, 20f, 20f, 20f};
              pdfDoc.setTable(5, 50, anchoCelda5);

              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Capital", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Intereses", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.RIGHT, 1);

              pdfDoc.setCell("Total Pagados", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_cap_pag, "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_int_pag, "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_ven_pag, "celda01", ComunesPDF.RIGHT, 1);

              pdfDoc.setCell("Total No Pagados", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_cap_npag, "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_int_npag, "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_ven_npag, "celda01", ComunesPDF.RIGHT, 1);
              
              pdfDoc.addTable();
            }
          }
        }
        //..:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: REEEMBOLSOS, PREPAGOS TOTALES, RECUPERACIONES FONDO Jr--
        if(seccion_estatus.equals("RTRF")){
          if(!reembolsos.isEmpty() || !prepagos.isEmpty() || !recuperaciones.isEmpty()){
            String tot_ven_rem = "0";
            String tot_ven_pre = "0";
            String tot_ven_rec = "0";
            String registros_reembolsos = "0";
            String registros_prepagos = "0";
            String registros_recuperaciones = "0";
            int colspan = 1;
            
            if(!reembolsos.isEmpty()){
              monto_total = new BigDecimal("0.00");
              num_registros = 0;
              for(int j = 0; j < reembolsos.size(); j++){
                List operacion_por_fecha = (List)reembolsos.get(j);
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
              }
              tot_ven_rem = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
              registros_reembolsos = Integer.toString(num_registros);
              colspan++;
            }
            
            if(!prepagos.isEmpty()){
              monto_total = new BigDecimal("0.00");
              num_registros = 0;
              //for(int j = 0; j < reembolsos.size(); j++){
				  for(int j = 0; j < prepagos.size(); j++){
                List operacion_por_fecha = (List)prepagos.get(j);
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
              }
              tot_ven_pre = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
              registros_prepagos = Integer.toString(num_registros);
              colspan++;
            }
            
            if(!recuperaciones.isEmpty()){
              monto_total = new BigDecimal("0.00");
              num_registros = 0;
              //for(int j = 0; j < reembolsos.size(); j++){
				  for(int j = 0; j < recuperaciones.size(); j++){
                List operacion_por_fecha = (List)recuperaciones.get(j);
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
              }
              tot_ven_rec = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
              registros_recuperaciones = Integer.toString(num_registros);
              colspan++;
            }
            
            pdfDoc.addText("Preacuse Validación:" + (tipo_validacion.equals("ACEPTAR")?"Aceptación":"Rechazo"), "titulo", ComunesPDF.CENTER);
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            
            float anchoCelda2[] = {50f, 50f};
            float anchoCelda3[] = {40f, 30f, 30f};
            float anchoCelda4[] = {25f, 25f, 25f, 25f};
            
            if(colspan == 2){
              pdfDoc.setTable(2, 50, anchoCelda2);
            }else if(colspan == 3){
              pdfDoc.setTable(3, 50, anchoCelda3);
            }else{
              pdfDoc.setTable(3, 50, anchoCelda4);
            }
            
            pdfDoc.setCell("", "celda01", ComunesPDF.CENTER, 1);
            
            if(!registros_reembolsos.equals("0")){
              pdfDoc.setCell("Reembolsos", "celda01", ComunesPDF.CENTER, 1);
            }
            if(!registros_prepagos.equals("0")){
              pdfDoc.setCell("Prepagos Totales", "celda01", ComunesPDF.CENTER, 1);
            }
            if(!registros_recuperaciones.equals("0")){
              pdfDoc.setCell("Recuperaciones Fondo Jr", "celda01", ComunesPDF.CENTER, 1);
            }

            pdfDoc.setCell("No. Total de Creditos " + (tipo_validacion.equals("ACEPTAR")?"Aceptados":"Rechazados"), "formas", ComunesPDF.RIGHT, 1);
            if(!registros_reembolsos.equals("0")){
              pdfDoc.setCell(registros_reembolsos, "formas", ComunesPDF.RIGHT, 1);
            }
            if(!registros_prepagos.equals("0")){
              pdfDoc.setCell(registros_prepagos, "formas", ComunesPDF.RIGHT, 1);
            }
            if(!registros_recuperaciones.equals("0")){
              pdfDoc.setCell(registros_recuperaciones, "formas", ComunesPDF.RIGHT, 1);
            }

            
            pdfDoc.setCell("Monto Total de Creditos " + (tipo_validacion.equals("ACEPTAR")?"Aceptados":"Rechazados"), "formas", ComunesPDF.RIGHT, 1);
            if(!registros_reembolsos.equals("0")){
              pdfDoc.setCell("$" + tot_ven_rem, "formas", ComunesPDF.RIGHT, 1);
            }
            if(!registros_prepagos.equals("0")){
              pdfDoc.setCell("$" + tot_ven_pre, "formas", ComunesPDF.RIGHT, 1);
            }
            if(!registros_recuperaciones.equals("0")){
              pdfDoc.setCell("$" + tot_ven_rec, "formas", ComunesPDF.RIGHT, 1);
            }
            
            pdfDoc.setCell("Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisión tendrá validez para todos los efectos legales.", "formas", ComunesPDF.JUSTIFIED, colspan);
            pdfDoc.addTable();
          }
          
          if(!reembolsos.isEmpty() || !prepagos.isEmpty() || !recuperaciones.isEmpty()){
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            pdfDoc.addText("Reembolsos, Prepagos Totales, Recuperación de Fondo Jr", "titulo", ComunesPDF.CENTER);
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
          }
          
          String tot_cap_rem = "";
          String tot_int_rem = "";
          String tot_ven_rem = "";
          String tot_cap_pre = "";
          String tot_int_pre = "";
          String tot_ven_pre = "";
          String tot_cap_rec = "";
          String tot_int_rec = "";
          String tot_ven_rec = "";
          
          if(!reembolsos.isEmpty()){
            float anchoCelda14[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
            pdfDoc.setTable(14, 100, anchoCelda14);
            monto_capital = new BigDecimal("0.00");
            monto_interes = new BigDecimal("0.00");
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            
            pdfDoc.setCell("Reembolsos", "celda01", ComunesPDF.CENTER, 14);
            
            pdfDoc.setCell("Fecha de Amortización Nafin", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Operación", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Pago Cliente FIDE", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Prestamo", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER, 1);
            
            pdfDoc.setHeaders();

            for(int j = 0; j < reembolsos.size(); j++){
              List operacion_por_fecha = (List)reembolsos.get(j);
              monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
              monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
              pdfDoc.setCell("" + operacion_por_fecha.get(0), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(1), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(3), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(4), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(5), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(6), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(7), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(8), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(9), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(10), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(11), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("En Proceso", "formas", ComunesPDF.CENTER, 1);
            }
            //pdfDoc.addTable();
            
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            
            tot_cap_rem = Comunes.formatoDecimal(monto_capital.toPlainString(), 2);
            tot_int_rem = Comunes.formatoDecimal(monto_interes.toPlainString(), 2);
            tot_ven_rem = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
            if(cs_subtotales.equals("S")){
              //float anchoCelda14a[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
              //pdfDoc.setTable(14, 100, anchoCelda14a);

              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 6);
              pdfDoc.setCell("Subtotal", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_capital.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_interes.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_total.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Num. Reembolsos", "celda01", ComunesPDF.RIGHT, 2);
              pdfDoc.setCell(Integer.toString(num_registros), "celda01", ComunesPDF.RIGHT, 1);
              
              //pdfDoc.addTable();
            }
            pdfDoc.addTable();
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
          }
          
          if(!prepagos.isEmpty()){
            float anchoCelda14[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
            pdfDoc.setTable(14, 100, anchoCelda14);
            monto_capital = new BigDecimal("0.00");
            monto_interes = new BigDecimal("0.00");
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            
            pdfDoc.setCell("Prepagos Totales", "celda01", ComunesPDF.CENTER, 14);
            
            pdfDoc.setCell("Fecha de Amortización Nafin", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Operación", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Pago Cliente FIDE", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Prestamo", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER, 1);
            
            pdfDoc.setHeaders();

            for(int j = 0; j < prepagos.size(); j++){
              List operacion_por_fecha = (List)prepagos.get(j);
              monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
              monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
              pdfDoc.setCell("" + operacion_por_fecha.get(0), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(1), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(3), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(4), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(5), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(6), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(7), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(8), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(9), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(10), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(11), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("En Proceso", "formas", ComunesPDF.CENTER, 1);
            }
            //pdfDoc.addTable();
            
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            
            tot_cap_pre = Comunes.formatoDecimal(monto_capital.toPlainString(), 2);
            tot_int_pre = Comunes.formatoDecimal(monto_interes.toPlainString(), 2);
            tot_ven_pre = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
            if(cs_subtotales.equals("S")){
              //float anchoCelda14a[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
              //pdfDoc.setTable(14, 100, anchoCelda14a);

              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 6);
              pdfDoc.setCell("Subtotal", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_capital.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_interes.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_total.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Num. Prepagos Totales", "celda01", ComunesPDF.RIGHT, 2);
              pdfDoc.setCell(Integer.toString(num_registros), "celda01", ComunesPDF.RIGHT, 1);
              
              //pdfDoc.addTable();
            }
            pdfDoc.addTable();
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
          }
          
          if(!recuperaciones.isEmpty()){
            float anchoCelda14[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
            pdfDoc.setTable(14, 100, anchoCelda14);
            monto_capital = new BigDecimal("0.00");
            monto_interes = new BigDecimal("0.00");
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            
            pdfDoc.setCell("Recuperación de Fondo Jr", "celda01", ComunesPDF.CENTER, 14);
            
            pdfDoc.setCell("Fecha de Amortización Nafin", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Operación", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Pago Cliente FIDE", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Prestamo", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER, 1);
            
            pdfDoc.setHeaders();
            
            for(int j = 0; j < recuperaciones.size(); j++){
              List operacion_por_fecha = (List)recuperaciones.get(j);
              monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
              monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
              pdfDoc.setCell("" + operacion_por_fecha.get(0), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(1), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(3), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(4), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(5), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(6), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(7), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(8), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(9), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(10), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(11), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("En Proceso", "formas", ComunesPDF.CENTER, 1);
            }
            //pdfDoc.addTable();
            
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);

            tot_cap_rec = Comunes.formatoDecimal(monto_capital.toPlainString(), 2);
            tot_int_rec = Comunes.formatoDecimal(monto_interes.toPlainString(), 2);
            tot_ven_rec = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
            if(cs_subtotales.equals("S")){
              //float anchoCelda14a[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
              //pdfDoc.setTable(14, 100, anchoCelda14a);
            
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 6);
              pdfDoc.setCell("Subtotal", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_capital.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_interes.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_total.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Num. Recuperaciones Fondo Jr", "celda01", ComunesPDF.RIGHT, 2);
              pdfDoc.setCell(Integer.toString(num_registros), "celda01", ComunesPDF.RIGHT, 1);
              
              //pdfDoc.addTable();
            }
            pdfDoc.addTable();
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
          }
          
          if(!reembolsos.isEmpty() || !prepagos.isEmpty() || !recuperaciones.isEmpty()){
            //pdfDoc.newPage();
            float anchoCelda5[] = {20f, 20f, 20f, 20f, 20f};
            pdfDoc.setTable(5, 50, anchoCelda5);

			   pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
			   pdfDoc.setCell("Capital", "celda01", ComunesPDF.RIGHT, 1);
			   pdfDoc.setCell("Intereses", "celda01", ComunesPDF.RIGHT, 1);
			   pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.RIGHT, 1);
			   pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.RIGHT, 1);

            pdfDoc.setCell("Total Reembolsos", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_cap_rem, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_int_rem, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_ven_rem, "celda01", ComunesPDF.RIGHT, 1);

            pdfDoc.setCell("Total Prepagos Totales", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_cap_pre, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_int_pre, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_ven_pre, "celda01", ComunesPDF.RIGHT, 1);

            pdfDoc.setCell("Total Recuperaciones Fondo Jr", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_cap_rec, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_int_rec, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_ven_rec, "celda01", ComunesPDF.RIGHT, 1);
            
            pdfDoc.addTable();
          }
        }
        pdfDoc.endDocument();
      }else if(tipo_archivo.equals("ACUSE_PDF")){
        
		  String fecha_validacion = (request.getParameter("fecha_validacion")==null)?"":request.getParameter("fecha_validacion");
		  String hora_validacion = (request.getParameter("hora_validacion")==null)?"":request.getParameter("hora_validacion");
		  String folio_validacion = (request.getParameter("folio_validacion")==null)?"":request.getParameter("folio_validacion");
		  String acuse_validacion = (request.getParameter("acuse_validacion")==null)?"":request.getParameter("acuse_validacion");
		  String igFolioValidacion = (request.getParameter("igfolioValidacion")==null)?"":request.getParameter("igfolioValidacion");
        String cs_validacion = "ACUSE";

        
        if(cs_subtotales == null){cs_subtotales = "N";}
		  
			if (seccion_estatus.equals("PNP")) {
				fecha_amort_nafin_ini = fecha_amortizacion;
				fecha_amort_nafin_fin = fecha_amortizacion;		
			}
			
        StringBuffer texto_plano = new StringBuffer();

        List parametros_consulta = new ArrayList();
        parametros_consulta.add(fecha_amort_nafin_ini);
        parametros_consulta.add(fecha_amort_nafin_fin);
        parametros_consulta.add(estatus_registro);
        parametros_consulta.add(numero_prestamo);
        parametros_consulta.add(clave_if);
        parametros_consulta.add(numero_sirac);
        parametros_consulta.add(fecha_pago_cliente_fide_ini);
        parametros_consulta.add(fecha_pago_cliente_fide_fin);
        parametros_consulta.add(fecha_registro_fide_ini);
        parametros_consulta.add(fecha_registro_fide_fin);
        parametros_consulta.add(cs_validacion);
		parametros_consulta.add(clavePrograma);//FODEA 026 - 2010 ACF
			parametros_consulta.add(igFolioValidacion);//FODEA 026 - 2010 ACF
        
        List operaciones_validacion = fondoJunior.obtenerOperacionesValidacion(parametros_consulta);

        nombreArchivo = archivo.nombreArchivo()+".pdf";
        
        ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
        
        String pais        = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
        String noCliente    = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
        String nombre      = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
        String nombreUsr   = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
        String logo      	 = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
  
        pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
				//FODEA 026 - 2010 ACF (I)
				pdfDoc.addText(nombrePrograma, "titulo", ComunesPDF.CENTER);
				pdfDoc.addText("\n", "formas", ComunesPDF.CENTER);
				//FODEA 026 - 2010 ACF (F)
        List reembolsos = new ArrayList();
        List prepagos = new ArrayList();
        List recuperaciones = new ArrayList();
        BigDecimal monto_capital = new BigDecimal("0.00");
        BigDecimal monto_interes = new BigDecimal("0.00");
        BigDecimal monto_total = new BigDecimal("0.00");
        int num_registros = 0;

        for(int i = 0; i< operaciones_validacion.size(); i++){
          List operaciones_por_fecha = (List)operaciones_validacion.get(i);
          String fecha_vencimiento = (String)operaciones_por_fecha.get(0);
          List operaciones_pagados = (List)operaciones_por_fecha.get(1);
          List operaciones_no_pagados = (List)operaciones_por_fecha.get(2);
          List operaciones_reembolsos = (List)operaciones_por_fecha.get(3);
          List operaciones_prepagos = (List)operaciones_por_fecha.get(4);
          List operaciones_recuperaciones = (List)operaciones_por_fecha.get(5);
          
          if(!operaciones_reembolsos.isEmpty()){
            for(int j = 0; j < operaciones_reembolsos.size(); j++){
              reembolsos.add((List)operaciones_reembolsos.get(j));
            }
          }
          if(!operaciones_prepagos.isEmpty()){
            for(int j = 0; j < operaciones_prepagos.size(); j++){
              prepagos.add((List)operaciones_prepagos.get(j));
            }
          }
          if(!operaciones_recuperaciones.isEmpty()){
            for(int j = 0; j < operaciones_recuperaciones.size(); j++){
              recuperaciones.add((List)operaciones_recuperaciones.get(j));
            }
          }
          
          String dia = fecha_vencimiento.substring(0, fecha_vencimiento.indexOf("/"));
          String mes = fecha_vencimiento.substring(fecha_vencimiento.indexOf("/") + 1, fecha_vencimiento.lastIndexOf("/"));
          String anio = fecha_vencimiento.substring(fecha_vencimiento.lastIndexOf("/") + 1);
          String tot_cap_pag = "";
          String tot_int_pag = "";
          String tot_ven_pag = "";
          String tot_cap_npag = "";
          String tot_int_npag = "";
          String tot_ven_npag = "";
          String registros_pagados = "0";
          String registros_no_pagados = "0";
          int colspan = 1;
          
          if(mes.equals("01")){mes = "Enero";
          }else if(mes.equals("02")){mes = "Febrero";
          }else if(mes.equals("03")){mes = "Marzo";
          }else if(mes.equals("04")){mes = "Abril";
          }else if(mes.equals("05")){mes = "Mayo";
          }else if(mes.equals("06")){mes = "Junio";
          }else if(mes.equals("07")){mes = "Julio";
          }else if(mes.equals("08")){mes = "Agosto";
          }else if(mes.equals("09")){mes = "Septiembre";
          }else if(mes.equals("10")){mes = "Octubre";
          }else if(mes.equals("11")){mes = "Noviembre";
          }else if(mes.equals("12")){mes = "Diciembre";}
          
          if(!operaciones_pagados.isEmpty()){
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            for(int j = 0; j < operaciones_pagados.size(); j++){
              List operacion_por_fecha = (List)operaciones_pagados.get(j);
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
            }
            tot_ven_pag = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
            registros_pagados = Integer.toString(num_registros);
            colspan++;
          }
         
          if(!operaciones_no_pagados.isEmpty()){
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            for(int j = 0; j < operaciones_no_pagados.size(); j++){
              List operacion_por_fecha = (List)operaciones_no_pagados.get(j);
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
            }
            tot_ven_npag = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
            registros_no_pagados = Integer.toString(num_registros);
            colspan++;
          }
          
          if(seccion_estatus.equals("PNP")){
            pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
            pdfDoc.addText("La autentificación se llevó a cabo con éxito \n Recibo: "+acuse_validacion, "titulo", ComunesPDF.CENTER);
            pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
            
            float anchoCelda2[] = {50f, 50f};
            float anchoCelda3[] = {40f, 30f, 30f};
            
            if(colspan == 2){
              pdfDoc.setTable(2, 50, anchoCelda2);
            }else{
              pdfDoc.setTable(3, 50, anchoCelda3);
            }
            
            if(!operaciones_pagados.isEmpty() || !operaciones_no_pagados.isEmpty()){
              pdfDoc.setCell("", "celda01", ComunesPDF.CENTER, 1);
              if(!registros_pagados.equals("0")){
                pdfDoc.setCell("Pagados", "celda01", ComunesPDF.CENTER, 1);
              }
              if(!registros_no_pagados.equals("0")){
                pdfDoc.setCell("No Pagados", "celda01", ComunesPDF.CENTER, 1);
              }
              
              pdfDoc.setCell("No. Total de Creditos " + (tipo_validacion.equals("ACEPTAR")?"Aceptados":"Rechazados"), "formas", ComunesPDF.RIGHT, 1);
              if(!registros_pagados.equals("0")){
                pdfDoc.setCell(registros_pagados, "formas", ComunesPDF.RIGHT, 1);
              }
              if(!registros_no_pagados.equals("0")){
                pdfDoc.setCell(registros_no_pagados, "formas", ComunesPDF.RIGHT, 1);
              }

              pdfDoc.setCell("Monto Total de Creditos " + (tipo_validacion.equals("ACEPTAR")?"Aceptados":"Rechazados"), "formas", ComunesPDF.RIGHT, 1);
              if(!registros_pagados.equals("0")){
                pdfDoc.setCell("$" + tot_ven_pag, "formas", ComunesPDF.RIGHT, 1);
              }
              if(!registros_no_pagados.equals("0")){
                pdfDoc.setCell("$" + tot_ven_npag, "formas", ComunesPDF.RIGHT, 1);
              }
              
              pdfDoc.setCell("Número de Acuse", "formas", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell(folio_validacion, "formas", ComunesPDF.RIGHT, colspan - 1);
              
              pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell(fecha_validacion, "formas", ComunesPDF.RIGHT, colspan - 1);
              
              pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell(hora_validacion, "formas", ComunesPDF.RIGHT, colspan - 1);
          
              pdfDoc.setCell("Usuario", "formas",ComunesPDF.RIGHT, 1);
              pdfDoc.setCell(login_usuario + " - " + nombre_usuario, "formas", ComunesPDF.RIGHT, colspan - 1);
              
              pdfDoc.setCell("Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisión tendrá validez para todos los efectos legales.", "formas", ComunesPDF.JUSTIFIED, colspan);
              pdfDoc.addTable();
            }
            //..:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CREDITOS PAGADOS Y/O NO PAGADOS
            if(!operaciones_pagados.isEmpty() || !operaciones_no_pagados.isEmpty()){
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
              pdfDoc.addText("Fecha de Vencimiento: " + dia + " de " + mes + " de " + anio, "titulo", ComunesPDF.LEFT);
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            }
            //..:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CREDITOS PAGADOS
            if(!operaciones_pagados.isEmpty()){
              float anchoCelda14[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
              pdfDoc.setTable(14, 100, anchoCelda14);
              monto_capital = new BigDecimal("0.00");
              monto_interes = new BigDecimal("0.00");
              monto_total = new BigDecimal("0.00");
              num_registros = 0;
              
              pdfDoc.setCell("Pagados", "celda01", ComunesPDF.CENTER, 14);
              
              pdfDoc.setCell("Fecha de Amortización Nafin", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Fecha de Operación", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Fecha de Pago Cliente FIDE", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Prestamo", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Intereses", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER, 1);
              
              pdfDoc.setHeaders();
              
              for(int j = 0; j < operaciones_pagados.size(); j++){
                List operacion_por_fecha = (List)operaciones_pagados.get(j);
                monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
                monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
                pdfDoc.setCell("" + operacion_por_fecha.get(0), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(1), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(3), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(4), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(5), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(6), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(7), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(8), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(9), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(10), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(11), "formas", ComunesPDF.CENTER, 1);
					 pdfDoc.setCell("" + operacion_por_fecha.get(12), "formas", ComunesPDF.CENTER, 1);
                //pdfDoc.setCell("En Proceso", "formas", ComunesPDF.CENTER, 1);
              }
              //pdfDoc.addTable();
              
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
              
              tot_cap_pag = Comunes.formatoDecimal(monto_capital.toPlainString(), 2);
              tot_int_pag = Comunes.formatoDecimal(monto_interes.toPlainString(), 2);
              tot_ven_pag = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
              if(cs_subtotales.equals("S")){
                //float anchoCelda14a[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
                //pdfDoc.setTable(14, 100, anchoCelda14a);

                pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 6);
                pdfDoc.setCell("Subtotal", "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_capital.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_interes.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_total.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("Num. Registros Pagados", "celda01", ComunesPDF.RIGHT, 2);
                pdfDoc.setCell(Integer.toString(num_registros), "celda01", ComunesPDF.RIGHT, 1);
                
                //pdfDoc.addTable();
              }
              pdfDoc.addTable();
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            }
            
            //..:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CREDITOS NO PAGADOS
            if(!operaciones_no_pagados.isEmpty()){
              float anchoCelda14[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
              pdfDoc.setTable(14, 100, anchoCelda14);
              monto_capital = new BigDecimal("0.00");
              monto_interes = new BigDecimal("0.00");
              monto_total = new BigDecimal("0.00");
              num_registros = 0;

              pdfDoc.setCell("No Pagados", "celda01", ComunesPDF.CENTER, 14);
              
              pdfDoc.setCell("Fecha de Amortización Nafin", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Fecha de Operación", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Fecha de Pago Cliente FIDE", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Prestamo", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Intereses", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER, 1);
              
              pdfDoc.setHeaders();

              for(int j = 0; j < operaciones_no_pagados.size(); j++){
                List operacion_por_fecha = (List)operaciones_no_pagados.get(j);
                monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
                monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
                pdfDoc.setCell("" + operacion_por_fecha.get(0), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(1), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(3), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(4), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(5), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(6), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(7), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(8), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(9), 2), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(10), "formas", ComunesPDF.CENTER, 1);
                pdfDoc.setCell("" + operacion_por_fecha.get(11), "formas", ComunesPDF.CENTER, 1);
					 pdfDoc.setCell("" + operacion_por_fecha.get(12), "formas", ComunesPDF.CENTER, 1);
                //pdfDoc.setCell("En Proceso", "formas", ComunesPDF.CENTER, 1);
              }
              //pdfDoc.addTable();
              
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
              
              tot_cap_npag = Comunes.formatoDecimal(monto_capital.toPlainString(), 2);
              tot_int_npag = Comunes.formatoDecimal(monto_interes.toPlainString(), 2);
              tot_ven_npag = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
              if(cs_subtotales.equals("S")){
                //float anchoCelda14a[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
                //pdfDoc.setTable(14, 100, anchoCelda14a);

                pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 6);
                pdfDoc.setCell("Subtotal", "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_capital.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_interes.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_total.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
                pdfDoc.setCell("Num. Registros No Pagados", "celda01", ComunesPDF.RIGHT, 2);
                pdfDoc.setCell(Integer.toString(num_registros), "celda01", ComunesPDF.RIGHT, 1);
                
                //pdfDoc.addTable();
              }
              pdfDoc.addTable();
              pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            }
            
            if(!operaciones_pagados.isEmpty() || !operaciones_no_pagados.isEmpty()){
              //pdfDoc.newPage();
              float anchoCelda5[] = {20f, 20f, 20f, 20f, 20f};
              pdfDoc.setTable(5, 50, anchoCelda5);

              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Capital", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Intereses", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.RIGHT, 1);

              pdfDoc.setCell("Total Pagados", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_cap_pag, "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_int_pag, "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_ven_pag, "celda01", ComunesPDF.RIGHT, 1);

              pdfDoc.setCell("Total No Pagados", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_cap_npag, "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_int_npag, "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + tot_ven_npag, "celda01", ComunesPDF.RIGHT, 1);
              
              pdfDoc.addTable();
            }
          }
        }
        //..:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: REEEMBOLSOS, PREPAGOS TOTALES, RECUPERACIONES FONDO Jr--
        if(seccion_estatus.equals("RTRF")){
          if(!reembolsos.isEmpty() || !prepagos.isEmpty() || !recuperaciones.isEmpty()){
            String tot_ven_rem = "0";
            String tot_ven_pre = "0";
            String tot_ven_rec = "0";
            String registros_reembolsos = "0";
            String registros_prepagos = "0";
            String registros_recuperaciones = "0";
            int colspan = 1;
            
            if(!reembolsos.isEmpty()){
              monto_total = new BigDecimal("0.00");
              num_registros = 0;
              for(int j = 0; j < reembolsos.size(); j++){
                List operacion_por_fecha = (List)reembolsos.get(j);
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
              }
              tot_ven_rem = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
              registros_reembolsos = Integer.toString(num_registros);
              colspan++;
            }
            
            if(!prepagos.isEmpty()){
              monto_total = new BigDecimal("0.00");
              num_registros = 0;
              //for(int j = 0; j < reembolsos.size(); j++){
				  for(int j = 0; j < prepagos.size(); j++){
                List operacion_por_fecha = (List)prepagos.get(j);
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
              }
              tot_ven_pre = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
              registros_prepagos = Integer.toString(num_registros);
              colspan++;
            }
            
            if(!recuperaciones.isEmpty()){
              monto_total = new BigDecimal("0.00");
              num_registros = 0;
				  //for(int j = 0; j < reembolsos.size(); j++){
              for(int j = 0; j < recuperaciones.size(); j++){
                List operacion_por_fecha = (List)recuperaciones.get(j);
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
              }
              tot_ven_rec = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
              registros_recuperaciones = Integer.toString(num_registros);
              colspan++;
            }
            
            pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
            pdfDoc.addText("La autentificación se llevó a cabo con éxito \n Recibo: "+acuse_validacion, "titulo", ComunesPDF.CENTER);
            pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
            
            float anchoCelda2[] = {50f, 50f};
            float anchoCelda3[] = {40f, 30f, 30f};
            float anchoCelda4[] = {25f, 25f, 25f, 25f};
            
            if(colspan == 2){
              pdfDoc.setTable(2, 50, anchoCelda2);
            }else if(colspan == 3){
              pdfDoc.setTable(3, 50, anchoCelda3);
            }else{
              pdfDoc.setTable(3, 50, anchoCelda4);
            }
            
            pdfDoc.setCell("", "celda01", ComunesPDF.CENTER, 1);
            
            if(!registros_reembolsos.equals("0")){
              pdfDoc.setCell("Reembolsos", "celda01", ComunesPDF.CENTER, 1);
            }
            if(!registros_prepagos.equals("0")){
              pdfDoc.setCell("Prepagos Totales", "celda01", ComunesPDF.CENTER, 1);
            }
            if(!registros_recuperaciones.equals("0")){
              pdfDoc.setCell("Recuperaciones Fondo Jr", "celda01", ComunesPDF.CENTER, 1);
            }

            pdfDoc.setCell("No. Total de Creditos " + (tipo_validacion.equals("ACEPTAR")?"Aceptados":"Rechazados"), "formas", ComunesPDF.RIGHT, 1);
            if(!registros_reembolsos.equals("0")){
              pdfDoc.setCell(registros_reembolsos, "formas", ComunesPDF.RIGHT, 1);
            }
            if(!registros_prepagos.equals("0")){
              pdfDoc.setCell(registros_prepagos, "formas", ComunesPDF.RIGHT, 1);
            }
            if(!registros_recuperaciones.equals("0")){
              pdfDoc.setCell(registros_recuperaciones, "formas", ComunesPDF.RIGHT, 1);
            }

            
            pdfDoc.setCell("Monto Total de Creditos " + (tipo_validacion.equals("ACEPTAR")?"Aceptados":"Rechazados"), "formas", ComunesPDF.RIGHT, 1);
            if(!registros_reembolsos.equals("0")){
              pdfDoc.setCell("$" + tot_ven_rem, "formas", ComunesPDF.RIGHT, 1);
            }
            if(!registros_prepagos.equals("0")){
              pdfDoc.setCell("$" + tot_ven_pre, "formas", ComunesPDF.RIGHT, 1);
            }
            if(!registros_recuperaciones.equals("0")){
              pdfDoc.setCell("$" + tot_ven_rec, "formas", ComunesPDF.RIGHT, 1);
            }
            
            pdfDoc.setCell("Número de Acuse", "formas", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell(folio_validacion, "formas", ComunesPDF.RIGHT, colspan - 1);
            
            pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell(fecha_validacion, "formas", ComunesPDF.RIGHT, colspan - 1);
            
            pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell(hora_validacion, "formas", ComunesPDF.RIGHT, colspan - 1);
        
            pdfDoc.setCell("Usuario", "formas",ComunesPDF.RIGHT, 1);
            pdfDoc.setCell(login_usuario + " - " + nombre_usuario, "formas", ComunesPDF.RIGHT, colspan - 1);
            
            pdfDoc.setCell("Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisión tendrá validez para todos los efectos legales.", "formas", ComunesPDF.JUSTIFIED, colspan);
            pdfDoc.addTable();
          }
          
          if(!reembolsos.isEmpty() || !prepagos.isEmpty() || !recuperaciones.isEmpty()){
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            pdfDoc.addText("Reembolsos, Prepagos Totales, Recuperación de Fondo Jr", "titulo", ComunesPDF.CENTER);
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
          }
          
          String tot_cap_rem = "";
          String tot_int_rem = "";
          String tot_ven_rem = "";
          String tot_cap_pre = "";
          String tot_int_pre = "";
          String tot_ven_pre = "";
          String tot_cap_rec = "";
          String tot_int_rec = "";
          String tot_ven_rec = "";
          
          if(!reembolsos.isEmpty()){
            float anchoCelda14[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
            pdfDoc.setTable(14, 100, anchoCelda14);
            monto_capital = new BigDecimal("0.00");
            monto_interes = new BigDecimal("0.00");
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            
            pdfDoc.setCell("Reembolsos", "celda01", ComunesPDF.CENTER, 14);
            
            pdfDoc.setCell("Fecha de Amortización Nafin", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Operación", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Pago Cliente FIDE", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Prestamo", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER, 1);
            
            pdfDoc.setHeaders();

            for(int j = 0; j < reembolsos.size(); j++){
              List operacion_por_fecha = (List)reembolsos.get(j);
              monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
              monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
              pdfDoc.setCell("" + operacion_por_fecha.get(0), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(1), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(3), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(4), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(5), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(6), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(7), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(8), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(9), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(10), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(11), "formas", ComunesPDF.CENTER, 1);
				  pdfDoc.setCell("" + operacion_por_fecha.get(12), "formas", ComunesPDF.CENTER, 1);
              //pdfDoc.setCell("En Proceso", "formas", ComunesPDF.CENTER, 1);
            }
            //pdfDoc.addTable();
            
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            
            tot_cap_rem = Comunes.formatoDecimal(monto_capital.toPlainString(), 2);
            tot_int_rem = Comunes.formatoDecimal(monto_interes.toPlainString(), 2);
            tot_ven_rem = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
            if(cs_subtotales.equals("S")){
              //float anchoCelda14a[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
              //pdfDoc.setTable(14, 100, anchoCelda14a);

              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 6);
              pdfDoc.setCell("Subtotal", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_capital.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_interes.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_total.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Num. Reembolsos", "celda01", ComunesPDF.RIGHT, 2);
              pdfDoc.setCell(Integer.toString(num_registros), "celda01", ComunesPDF.RIGHT, 1);
              
              //pdfDoc.addTable();
            }
            pdfDoc.addTable();
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
          }
          
          if(!prepagos.isEmpty()){
            float anchoCelda14[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
            pdfDoc.setTable(14, 100, anchoCelda14);
            monto_capital = new BigDecimal("0.00");
            monto_interes = new BigDecimal("0.00");
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            
            pdfDoc.setCell("Prepagos Totales", "celda01", ComunesPDF.CENTER, 14);
            
            pdfDoc.setCell("Fecha de Amortización Nafin", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Operación", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Pago Cliente FIDE", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Prestamo", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER, 1);
            
            pdfDoc.setHeaders();

            for(int j = 0; j < prepagos.size(); j++){
              List operacion_por_fecha = (List)prepagos.get(j);
              monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
              monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
              pdfDoc.setCell("" + operacion_por_fecha.get(0), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(1), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(3), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(4), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(5), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(6), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(7), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(8), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(9), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(10), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(11), "formas", ComunesPDF.CENTER, 1);
				  pdfDoc.setCell("" + operacion_por_fecha.get(12), "formas", ComunesPDF.CENTER, 1);
              //pdfDoc.setCell("En Proceso", "formas", ComunesPDF.CENTER, 1);
            }
            //pdfDoc.addTable();
            
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
            
            tot_cap_pre = Comunes.formatoDecimal(monto_capital.toPlainString(), 2);
            tot_int_pre = Comunes.formatoDecimal(monto_interes.toPlainString(), 2);
            tot_ven_pre = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
            if(cs_subtotales.equals("S")){
              //float anchoCelda14a[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
              //pdfDoc.setTable(14, 100, anchoCelda14a);

              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 6);
              pdfDoc.setCell("Subtotal", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_capital.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_interes.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_total.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Num. Prepagos Totales", "celda01", ComunesPDF.RIGHT, 2);
              pdfDoc.setCell(Integer.toString(num_registros), "celda01", ComunesPDF.RIGHT, 1);
              
              //pdfDoc.addTable();
            }
            pdfDoc.addTable();
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
          }
          
          if(!recuperaciones.isEmpty()){
            float anchoCelda14[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
            pdfDoc.setTable(14, 100, anchoCelda14);
            monto_capital = new BigDecimal("0.00");
            monto_interes = new BigDecimal("0.00");
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            
            pdfDoc.setCell("Recuperación de Fondo Jr", "celda01", ComunesPDF.CENTER, 14);
            
            pdfDoc.setCell("Fecha de Amortización Nafin", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Operación", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Fecha de Pago Cliente FIDE", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Prestamo", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER, 1);
            pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER, 1);
            
            pdfDoc.setHeaders();
            
            for(int j = 0; j < recuperaciones.size(); j++){
              List operacion_por_fecha = (List)recuperaciones.get(j);
              monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
              monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
              pdfDoc.setCell("" + operacion_por_fecha.get(0), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(1), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(3), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(4), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(5), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(6), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(7), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(8), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(operacion_por_fecha.get(9), 2), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(10), "formas", ComunesPDF.CENTER, 1);
              pdfDoc.setCell("" + operacion_por_fecha.get(11), "formas", ComunesPDF.CENTER, 1);
				  pdfDoc.setCell("" + operacion_por_fecha.get(12), "formas", ComunesPDF.CENTER, 1);
              //pdfDoc.setCell("En Proceso", "formas", ComunesPDF.CENTER, 1);
            }
            //pdfDoc.addTable();
            
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);

            tot_cap_rec = Comunes.formatoDecimal(monto_capital.toPlainString(), 2);
            tot_int_rec = Comunes.formatoDecimal(monto_interes.toPlainString(), 2);
            tot_ven_rec = Comunes.formatoDecimal(monto_total.toPlainString(), 2);
            if(cs_subtotales.equals("S")){
              //float anchoCelda14a[] = {7f, 7f, 7f, 7f, 7f, 9f, 7f, 7f, 7f, 7f, 7f, 7f, 7f, 7f};
              //pdfDoc.setTable(14, 100, anchoCelda14a);
            
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 6);
              pdfDoc.setCell("Subtotal", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_capital.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_interes.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(monto_total.toPlainString(), 2), "celda01", ComunesPDF.RIGHT, 1);
              pdfDoc.setCell("Num. Recuperaciones Fondo Jr", "celda01", ComunesPDF.RIGHT, 2);
              pdfDoc.setCell(Integer.toString(num_registros), "celda01", ComunesPDF.RIGHT, 1);
              
              //pdfDoc.addTable();
            }
            pdfDoc.addTable();
            pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
          }
          
          if(!reembolsos.isEmpty() || !prepagos.isEmpty() || !recuperaciones.isEmpty()){
            //pdfDoc.newPage();
            float anchoCelda5[] = {20f, 20f, 20f, 20f, 20f};
            pdfDoc.setTable(5, 50, anchoCelda5);

			   pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
			   pdfDoc.setCell("Capital", "celda01", ComunesPDF.RIGHT, 1);
			   pdfDoc.setCell("Intereses", "celda01", ComunesPDF.RIGHT, 1);
			   pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.RIGHT, 1);
			   pdfDoc.setCell("Total a Pagar", "celda01", ComunesPDF.RIGHT, 1);

            pdfDoc.setCell("Total Reembolsos", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_cap_rem, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_int_rem, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_ven_rem, "celda01", ComunesPDF.RIGHT, 1);

            pdfDoc.setCell("Total Prepagos Totales", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_cap_pre, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_int_pre, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_ven_pre, "celda01", ComunesPDF.RIGHT, 1);

            pdfDoc.setCell("Total Recuperaciones Fondo Jr", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_cap_rec, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_int_rec, "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT, 1);
            pdfDoc.setCell("$" + tot_ven_rec, "celda01", ComunesPDF.RIGHT, 1);
            
            pdfDoc.addTable();
          }
        }
        pdfDoc.endDocument();
      }else if(tipo_archivo.equals("CONS_CSV")){
        
        String cs_validacion = "";
		  
        if(cs_subtotales == null){cs_subtotales = "N";}
        
        if(!seccion_estatus.equals("RTRF")){
			  fecha_amort_nafin_ini = fecha_amortizacion;
			  fecha_amort_nafin_fin = fecha_amortizacion;
        }
        StringBuffer contenidoArchivo = new StringBuffer();
        
        nombreArchivo = archivo.nombreArchivo();

        List parametros_consulta = new ArrayList();
        parametros_consulta.add(fecha_amort_nafin_ini);
        parametros_consulta.add(fecha_amort_nafin_fin);
        parametros_consulta.add(estatus_registro);
        parametros_consulta.add(numero_prestamo);
        parametros_consulta.add(clave_if);
        parametros_consulta.add(numero_sirac);
        parametros_consulta.add(fecha_pago_cliente_fide_ini);
        parametros_consulta.add(fecha_pago_cliente_fide_fin);
        parametros_consulta.add(fecha_registro_fide_ini);
        parametros_consulta.add(fecha_registro_fide_fin);
        parametros_consulta.add(cs_validacion);
		  parametros_consulta.add(clavePrograma);//FODEA 026 - 2010 ACF
		  parametros_consulta.add("");//FODEA 026 - 2010 ACF
        
        List operaciones_validacion = fondoJunior.obtenerOperacionesValidacion(parametros_consulta);
        
        List reembolsos = new ArrayList();
        List prepagos = new ArrayList();
        List recuperaciones = new ArrayList();
        BigDecimal monto_capital = new BigDecimal("0.00");
        BigDecimal monto_interes = new BigDecimal("0.00");
        BigDecimal monto_total = new BigDecimal("0.00");
        int num_registros = 0;
				
				contenidoArchivo.append(nombrePrograma + ",\n");//FODEA 026 - 2010 ACF
				
        for(int i = 0; i< operaciones_validacion.size(); i++){
          List operaciones_por_fecha = (List)operaciones_validacion.get(i);
          String fecha_vencimiento = (String)operaciones_por_fecha.get(0);
          List operaciones_pagados = (List)operaciones_por_fecha.get(1);
          List operaciones_no_pagados = (List)operaciones_por_fecha.get(2);
          List operaciones_reembolsos = (List)operaciones_por_fecha.get(3);
          List operaciones_prepagos = (List)operaciones_por_fecha.get(4);
          List operaciones_recuperaciones = (List)operaciones_por_fecha.get(5);
    
          if(!operaciones_reembolsos.isEmpty()){
            for(int j = 0; j < operaciones_reembolsos.size(); j++){
              reembolsos.add((List)operaciones_reembolsos.get(j));
            }
          }
          if(!operaciones_prepagos.isEmpty()){
            for(int j = 0; j < operaciones_prepagos.size(); j++){
              prepagos.add((List)operaciones_prepagos.get(j));
            }
          }
          if(!operaciones_recuperaciones.isEmpty()){
            for(int j = 0; j < operaciones_recuperaciones.size(); j++){
              recuperaciones.add((List)operaciones_recuperaciones.get(j));
            }
          }
          
          String dia = fecha_vencimiento.substring(0, fecha_vencimiento.indexOf("/"));
          String mes = fecha_vencimiento.substring(fecha_vencimiento.indexOf("/") + 1, fecha_vencimiento.lastIndexOf("/"));
          String anio = fecha_vencimiento.substring(fecha_vencimiento.lastIndexOf("/") + 1);
          String tot_cap_pag = "";
          String tot_int_pag = "";
          String tot_ven_pag = "";
          String tot_cap_npag = "";
          String tot_int_npag = "";
          String tot_ven_npag = "";
          if(mes.equals("01")){mes = "Enero";
          }else if(mes.equals("02")){mes = "Febrero";
          }else if(mes.equals("03")){mes = "Marzo";
          }else if(mes.equals("04")){mes = "Abril";
          }else if(mes.equals("05")){mes = "Mayo";
          }else if(mes.equals("06")){mes = "Junio";
          }else if(mes.equals("07")){mes = "Julio";
          }else if(mes.equals("08")){mes = "Agosto";
          }else if(mes.equals("09")){mes = "Septiembre";
          }else if(mes.equals("10")){mes = "Octubre";
          }else if(mes.equals("11")){mes = "Noviembre";
          }else if(mes.equals("12")){mes = "Diciembre";}
					
          if(!seccion_estatus.equals("RTRF")){
            if(!operaciones_pagados.isEmpty() || !operaciones_no_pagados.isEmpty()){
              contenidoArchivo.append("Fecha de Vencimiento:" + dia + " de " + mes + " de " + anio + "\n");
              contenidoArchivo.append(",\n");
            }
            
            if(!operaciones_pagados.isEmpty()){
              monto_capital = new BigDecimal("0.00");
              monto_interes = new BigDecimal("0.00");
              monto_total = new BigDecimal("0.00");
              num_registros = 0;
              
              contenidoArchivo.append("Pagados\n");
              contenidoArchivo.append("Fecha de Amortización Nafin,");
              contenidoArchivo.append("Fecha de Operación,");
              contenidoArchivo.append("Fecha de Pago Cliente FIDE,");
              contenidoArchivo.append("Prestamo,");
              contenidoArchivo.append("Num. Cliente SIRAC,");
              contenidoArchivo.append("Cliente,");
              contenidoArchivo.append("Num. Cuota a Pagar,");
              contenidoArchivo.append("Capital,");
              contenidoArchivo.append("Intereses,");
              contenidoArchivo.append("Intereses Moratorios,");
              contenidoArchivo.append("Total a Pagar,");
              contenidoArchivo.append("Estatus del Registro,");
              contenidoArchivo.append("Origen,");
              contenidoArchivo.append("Estatus de la Validación\n");
    
              for(int j = 0; j < operaciones_pagados.size(); j++){
                List operacion_por_fecha = (List)operaciones_pagados.get(j);
                monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
                monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;						
                contenidoArchivo.append(operacion_por_fecha.get(0) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(1) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(2) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(3) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(4) + ",");							
                contenidoArchivo.append(operacion_por_fecha.get(5).toString().replace(',',' ') + ",");
                contenidoArchivo.append(operacion_por_fecha.get(6) + ",");
                contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(7)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
                contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(8)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
                contenidoArchivo.append(",");
                contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(9)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(10) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(11) + ",");
                contenidoArchivo.append("En Proceso\n");
              }
              tot_cap_pag = monto_capital.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
              tot_int_pag = monto_interes.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
              tot_ven_pag = monto_total.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
              if(cs_subtotales.equals("S")){
                contenidoArchivo.append(",,,,,,");
                contenidoArchivo.append("Subtotal,");
                contenidoArchivo.append(monto_capital.toPlainString() + ",");
                contenidoArchivo.append(monto_interes.toPlainString() + ",");
                contenidoArchivo.append(",");
                contenidoArchivo.append(monto_total.toPlainString() + ",");
                contenidoArchivo.append("Num. Registros Pagados,");
                contenidoArchivo.append(num_registros + "\n");
              }
              contenidoArchivo.append(",\n");
            }
            
            if(!operaciones_no_pagados.isEmpty()){
              monto_capital = new BigDecimal("0.00");
              monto_interes = new BigDecimal("0.00");
              monto_total = new BigDecimal("0.00");
              num_registros = 0;
              contenidoArchivo.append("No Pagados\n");
              contenidoArchivo.append("Fecha de Amortización Nafin,");
              contenidoArchivo.append("Fecha de Operación,");
              contenidoArchivo.append("Fecha de Pago Cliente FIDE,");
              contenidoArchivo.append("Prestamo,");
              contenidoArchivo.append("Num. Cliente SIRAC,");
              contenidoArchivo.append("Cliente,");
              contenidoArchivo.append("Num. Cuota a Pagar,");
              contenidoArchivo.append("Capital,");
              contenidoArchivo.append("Intereses,");
              contenidoArchivo.append("Intereses Moratorios,");
              contenidoArchivo.append("Total a Pagar,");
              contenidoArchivo.append("Estatus del Registro,");
              contenidoArchivo.append("Origen,");
              contenidoArchivo.append("Estatus de la Validación\n");
              
              for(int j = 0; j < operaciones_no_pagados.size(); j++){
                List operacion_por_fecha = (List)operaciones_no_pagados.get(j);
                monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
                monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
                monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
                num_registros++;
                contenidoArchivo.append(operacion_por_fecha.get(0) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(1) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(2) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(3) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(4) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(5).toString().replace(',',' ') + ",");
                contenidoArchivo.append(operacion_por_fecha.get(6) + ",");
                contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(7)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
                contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(8)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
                contenidoArchivo.append(",");
                contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(9)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(10) + ",");
                contenidoArchivo.append(operacion_por_fecha.get(11) + ",");
                contenidoArchivo.append("En Proceso\n");
              }
              tot_cap_npag = monto_capital.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
              tot_int_npag = monto_interes.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
              tot_ven_npag = monto_total.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
              if(cs_subtotales.equals("S")){
                contenidoArchivo.append(",,,,,,");
                contenidoArchivo.append("Subtotal,");
                contenidoArchivo.append(monto_capital.toPlainString() + ",");
                contenidoArchivo.append(monto_interes.toPlainString() + ",");
                contenidoArchivo.append(",");
                contenidoArchivo.append(monto_total.toPlainString() + ",");
                contenidoArchivo.append("Num. Registros No Pagados,");
                contenidoArchivo.append(num_registros + "\n");
              }
              contenidoArchivo.append(",\n");
            }
            
            if(!operaciones_pagados.isEmpty() || !operaciones_no_pagados.isEmpty()){
              contenidoArchivo.append(",,,,,,Total Pagado,");
              contenidoArchivo.append(tot_cap_pag + ",");
              contenidoArchivo.append(tot_int_pag + ",");
              contenidoArchivo.append(",");
              contenidoArchivo.append(tot_ven_pag + "\n");
              
              contenidoArchivo.append(",,,,,,Total No Pagados,");
              contenidoArchivo.append(tot_cap_npag + ",");
              contenidoArchivo.append(tot_int_npag + ",");
              contenidoArchivo.append(",");
              contenidoArchivo.append(tot_ven_npag + "\n");
              
              contenidoArchivo.append(",\n");
            }
          }
        }
        
        if(seccion_estatus.equals("RTRF")){
          if(!reembolsos.isEmpty() || !prepagos.isEmpty() || !recuperaciones.isEmpty()){
            contenidoArchivo.append("Reembolsos; Prepagos Totales; Recuperación de Fondo Jr\n");
            contenidoArchivo.append(",\n");
          }
          
          String tot_cap_rem = "";
          String tot_int_rem = "";
          String tot_ven_rem = "";
          String tot_cap_pre = "";
          String tot_int_pre = "";
          String tot_ven_pre = "";
          String tot_cap_rec = "";
          String tot_int_rec = "";
          String tot_ven_rec = "";
          
          if(!reembolsos.isEmpty()){
            monto_capital = new BigDecimal("0.00");
            monto_interes = new BigDecimal("0.00");
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            contenidoArchivo.append("Reembolsos\n");
            contenidoArchivo.append("Fecha de Amortización Nafin,");
            contenidoArchivo.append("Fecha de Operación,");
            contenidoArchivo.append("Fecha de Pago Cliente FIDE,");
            contenidoArchivo.append("Prestamo,");
            contenidoArchivo.append("Num. Cliente SIRAC,");
            contenidoArchivo.append("Cliente,");
            contenidoArchivo.append("Num. Cuota a Pagar,");
            contenidoArchivo.append("Capital,");
            contenidoArchivo.append("Intereses,");
            contenidoArchivo.append("Intereses Moratorios,");
            contenidoArchivo.append("Total a Pagar,");
            contenidoArchivo.append("Estatus del Registro,");
            contenidoArchivo.append("Origen,");
            contenidoArchivo.append("Estatus de la Validación\n");
            
            for(int j = 0; j < reembolsos.size(); j++){
            List operacion_por_fecha = (List)reembolsos.get(j);
              monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
              monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
              contenidoArchivo.append(operacion_por_fecha.get(0) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(1) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(2) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(3) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(4) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(5).toString().replace(',',' ') + ",");
              contenidoArchivo.append(operacion_por_fecha.get(6) + ",");
              contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(7)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
              contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(8)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
              contenidoArchivo.append(",");
              contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(9)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(10) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(11) + ",");
              contenidoArchivo.append("En Proceso\n");
            }
            tot_cap_rem = monto_capital.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            tot_int_rem = monto_interes.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            tot_ven_rem = monto_total.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            if(cs_subtotales.equals("S")){
              contenidoArchivo.append(",,,,,,");
              contenidoArchivo.append("Subtotal,");
              contenidoArchivo.append(monto_capital.toPlainString() + ",");
              contenidoArchivo.append(monto_interes.toPlainString() + ",");
              contenidoArchivo.append(",");
              contenidoArchivo.append(monto_total.toPlainString() + ",");
              contenidoArchivo.append("Num. Registros Reembolso,");
              contenidoArchivo.append(num_registros + "\n");
            }
            contenidoArchivo.append(",\n");
          }
          
          if(!prepagos.isEmpty()){
            monto_capital = new BigDecimal("0.00");
            monto_interes = new BigDecimal("0.00");
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            contenidoArchivo.append("Prepagos Totales\n");
            contenidoArchivo.append("Fecha de Amortización Nafin,");
            contenidoArchivo.append("Fecha de Operación,");
            contenidoArchivo.append("Fecha de Pago Cliente FIDE,");
            contenidoArchivo.append("Prestamo,");
            contenidoArchivo.append("Num. Cliente SIRAC,");
            contenidoArchivo.append("Cliente,");
            contenidoArchivo.append("Num. Cuota a Pagar,");
            contenidoArchivo.append("Capital,");
            contenidoArchivo.append("Intereses,");
            contenidoArchivo.append("Intereses Moratorios,");
            contenidoArchivo.append("Total a Pagar,");
            contenidoArchivo.append("Estatus del Registro,");
            contenidoArchivo.append("Origen,");
            contenidoArchivo.append("Estatus de la Validación\n");
            
            for(int j = 0; j < prepagos.size(); j++){
              List operacion_por_fecha = (List)prepagos.get(j);
              monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
              monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
              contenidoArchivo.append(operacion_por_fecha.get(0) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(1) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(2) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(3) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(4) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(5).toString().replace(',',' ') + ",");
              contenidoArchivo.append(operacion_por_fecha.get(6) + ",");
              contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(7)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
              contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(8)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
              contenidoArchivo.append(",");
              contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(9)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(10) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(11) + ",");
              contenidoArchivo.append("En Proceso\n");
            }
            tot_cap_pre = monto_capital.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            tot_int_pre = monto_interes.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            tot_ven_pre = monto_total.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            if(cs_subtotales.equals("S")){
              contenidoArchivo.append(",,,,,,");
              contenidoArchivo.append("Subtotal,");
              contenidoArchivo.append(monto_capital.toPlainString() + ",");
              contenidoArchivo.append(monto_interes.toPlainString() + ",");
              contenidoArchivo.append(",");
              contenidoArchivo.append(monto_total.toPlainString() + ",");
              contenidoArchivo.append("Num. Registros Prepago Total,");
              contenidoArchivo.append(num_registros + "\n");
            }
            contenidoArchivo.append(",\n");
          }
          
          if(!recuperaciones.isEmpty()){
            monto_capital = new BigDecimal("0.00");
            monto_interes = new BigDecimal("0.00");
            monto_total = new BigDecimal("0.00");
            num_registros = 0;
            contenidoArchivo.append("Recuperación Fondo Jr\n");
            contenidoArchivo.append("Fecha de Amortización Nafin,");
            contenidoArchivo.append("Fecha de Operación,");
            contenidoArchivo.append("Fecha de Pago Cliente FIDE,");
            contenidoArchivo.append("Prestamo,");
            contenidoArchivo.append("Num. Cliente SIRAC,");
            contenidoArchivo.append("Cliente,");
            contenidoArchivo.append("Num. Cuota a Pagar,");
            contenidoArchivo.append("Capital,");
            contenidoArchivo.append("Intereses,");
            contenidoArchivo.append("Intereses Moratorios,");
            contenidoArchivo.append("Total a Pagar,");
            contenidoArchivo.append("Estatus del Registro,");
            contenidoArchivo.append("Origen,");
            contenidoArchivo.append("Estatus de la Validación\n");
            
            for(int j = 0; j < recuperaciones.size(); j++){
              List operacion_por_fecha = (List)recuperaciones.get(j);
              monto_capital = monto_capital.add(new BigDecimal((String)operacion_por_fecha.get(7)));
              monto_interes = monto_interes.add(new BigDecimal((String)operacion_por_fecha.get(8)));
              monto_total = monto_total.add(new BigDecimal((String)operacion_por_fecha.get(9)));
              num_registros++;
              contenidoArchivo.append(operacion_por_fecha.get(0) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(1) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(2) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(3) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(4) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(5).toString().replace(',',' ') + ",");
              contenidoArchivo.append(operacion_por_fecha.get(6) + ",");
              contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(7)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
              contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(8)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
              contenidoArchivo.append(",");
              contenidoArchivo.append(new BigDecimal((String)operacion_por_fecha.get(9)).setScale(2, BigDecimal.ROUND_HALF_UP) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(10) + ",");
              contenidoArchivo.append(operacion_por_fecha.get(11) + ",");
              contenidoArchivo.append("En Proceso\n");
            }
            tot_cap_rec = monto_capital.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            tot_int_rec = monto_interes.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            tot_ven_rec = monto_total.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            if(cs_subtotales.equals("S")){
              contenidoArchivo.append(",,,,,,");
              contenidoArchivo.append("Subtotal,");
              contenidoArchivo.append(monto_capital.toPlainString() + ",");
              contenidoArchivo.append(monto_interes.toPlainString() + ",");
              contenidoArchivo.append(",");
              contenidoArchivo.append(monto_total.toPlainString() + ",");
              contenidoArchivo.append("Num. Registros Recuperación,");
              contenidoArchivo.append(num_registros + "\n");
            }
            contenidoArchivo.append(",\n");
          }
          
          if(!reembolsos.isEmpty() || !prepagos.isEmpty() || !recuperaciones.isEmpty()){
            contenidoArchivo.append(",,,,,,Total Reembolsos,");
            contenidoArchivo.append(tot_cap_rem + ",");
            contenidoArchivo.append(tot_int_rem + ",");
            contenidoArchivo.append(",");
            contenidoArchivo.append(tot_ven_rem + "\n");
            
            contenidoArchivo.append(",,,,,,Prepagos Totales,");
            contenidoArchivo.append(tot_cap_pre + ",");
            contenidoArchivo.append(tot_int_pre + ",");
            contenidoArchivo.append(",");
            contenidoArchivo.append(tot_ven_pre + "\n");
            
            contenidoArchivo.append(",,,,,,Total Recuperaciones Fondo Jr,");
            contenidoArchivo.append(tot_cap_rec + ",");
            contenidoArchivo.append(tot_int_rec + ",");
            contenidoArchivo.append(",");
            contenidoArchivo.append(tot_ven_rec + "\n");
          }
        }

        archivo.make(contenidoArchivo.toString(), strDirectorioTemp, nombreArchivo, ".csv");
        
        nombreArchivo = nombreArchivo + ".csv";
      }else if(tipo_archivo.equals("ERROR_TXT")){
        //nombreArchivo =  (String)PropertyUtils.getProperty(form, "nombre_archivo");
      }
      
      //file = new File(strDirectorioTemp+nombreArchivo);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	
}
%>
<%=infoRegresar%>