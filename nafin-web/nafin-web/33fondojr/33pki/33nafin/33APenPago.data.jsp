<%@ page 
		import="net.sf.json.JSONArray,
				  net.sf.json.JSONObject,
				  java.util.*,
				  netropology.utilerias.*,
              com.netro.fondosjr.ConsultaAmortizacionesPendientesPago,
				  com.netro.fondojr.*,
              java.math.BigDecimal,
              org.apache.commons.beanutils.PropertyUtils"
		contentType="application/json;charset=UTF-8"
		errorPage="/00utils/error_extjs.jsp"
%><%--@ include file			= "../../33secsession.jspf" --%><%
	
	//El siguiente c�digo debe ser lo primero en ejecutarse, el cual determina si la sesi�n est� activa. 
	//De lo contrario en lugar de mostrar 
	String gsCveUsuario = (String) session.getAttribute("sesCveUsuario");
	if(gsCveUsuario == null){
		out.println("");
		return;
	}
	
	response.setContentType("text/html; charset=iso-8859-1");
	JSONObject 	resultado 			= new JSONObject();
	boolean 		exitoOperacion		= true;
	
	try {
		
		String 				operacion 			= request.getParameter("operacion");
 
		if("busqueda".equals(operacion) || "cambiodepagina".equals(operacion) ){

			// 1. Leer parametros de busqueda
			String 		diaMinimo			= request.getParameter("diaMinimo");
			String 		diaMaximo			= request.getParameter("diaMaximo");
			String 		numeroPrestamo		= request.getParameter("numeroPrestamo");
			String 		paginaOffset 		= request.getParameter("paginaOffset");
			String 		clavePrograma     = (String) session.getAttribute("cveProgramaFondoJR");
			
			System.out.println("-----> diaMinimo 			= <"+diaMinimo+">");
			System.out.println("-----> diaMaximo 			= <"+diaMaximo+">");
			System.out.println("-----> numeroPrestamo 	= <"+numeroPrestamo+">");
			System.out.println("-----> paginaOffset 	   = <"+paginaOffset+">");
			
			// 2. Realizar Busqueda
			
			// 2.1 Se crea la instacia del Paginador
			ConsultaAmortizacionesPendientesPago consultaAmortizacionesPendientesPago = new ConsultaAmortizacionesPendientesPago();
			// 2.2 Instancia para el uso del paginador
			CQueryHelperReg cqhelper = new CQueryHelperReg(consultaAmortizacionesPendientesPago);
			// 2.3 Se copian las propiedades al bean del Paginador
			numeroPrestamo 					 = (numeroPrestamo != null && !numeroPrestamo.trim().equals(""))?numeroPrestamo.replaceAll("\r\n","\n"):numeroPrestamo;
			ArrayList listaNumerosPrestamo = (numeroPrestamo != null && !numeroPrestamo.trim().equals(""))?Comunes.stringToArrayList(numeroPrestamo,"\n"):new ArrayList();
			// 2.3a Reemplazar por -1 aquellos numeros de prestamos que no sean validos, para evitar que la consulta truene
			if( listaNumerosPrestamo.size() > 0 ){
				for(int i=0;i<listaNumerosPrestamo.size();i++){
					String numero = (String)listaNumerosPrestamo.get(i);
					//numero = String.valueOf(Integer.parseInt(numero));
					if(!Comunes.esNumero(numero)){
						listaNumerosPrestamo.set(i,"-1");
					}
				}
			}
			PropertyUtils.setProperty(consultaAmortizacionesPendientesPago, "diaMinimo", 			diaMinimo);
			PropertyUtils.setProperty(consultaAmortizacionesPendientesPago, "diaMaximo", 			diaMaximo);
			PropertyUtils.setProperty(consultaAmortizacionesPendientesPago, "numeroPrestamo", 	listaNumerosPrestamo);
			PropertyUtils.setProperty(consultaAmortizacionesPendientesPago, "clavePrograma", 	clavePrograma);
			
			// 2.4 Establecer el offset en 0
			if("busqueda".equals(operacion)){
				PropertyUtils.setProperty(consultaAmortizacionesPendientesPago, "paginaOffset", 	"0");
				cqhelper.cleanSession(request);
			}else if("cambiodepagina".equals(operacion)){
				PropertyUtils.setProperty(consultaAmortizacionesPendientesPago, "paginaOffset", 	paginaOffset);
			}
			
			// 2.5 Se realiza la paginaci�n - de 25 registros
			HashMap 		granTotal 		= new HashMap();
			Registros 	registros 		= null;
			List 			indicesPagina	= null;
			HashMap		totalPorPagina = new HashMap();
			boolean  	hayDatos			= false;
			
			registros 	= cqhelper.setPaginacion(request, 25);
			hayDatos		= registros != null && registros.getNumeroRegistros() > 0?true:false;
			
			if(hayDatos){
				
				// 2.6 Calcular total por pagina
				BigDecimal totalPaginaCapital 	 = new BigDecimal("0.00");
				BigDecimal totalPaginaIntereses 	 = new BigDecimal("0.00");
				BigDecimal totalPaginaTotalAPagar = new BigDecimal("0.00");
				while(registros.next()){
					totalPaginaCapital 		= totalPaginaCapital.add(	new BigDecimal(registros.getString("CAPITAL"))			);
					totalPaginaIntereses 	= totalPaginaIntereses.add(		new BigDecimal(registros.getString("INTERESES"))		);
					totalPaginaTotalAPagar 	= totalPaginaTotalAPagar.add(		new BigDecimal(registros.getString("TOTAL_A_PAGAR"))	);
				}
				totalPorPagina.put("CAPITAL",       totalPaginaCapital.toPlainString()    );
				totalPorPagina.put("INTERESES",     totalPaginaIntereses.toPlainString()  );
				totalPorPagina.put("TOTAL_A_PAGAR", totalPaginaTotalAPagar.toPlainString());
				registros.rewind();
				
				// 2.7 Obtener Gran Total
				Registros 	registrosTotal 			= cqhelper.getResultCount(request);
				if(registrosTotal.next()){
					granTotal.put("CAPITAL",			registrosTotal.getString("CAPITAL")			);
					granTotal.put("INTERESES",			registrosTotal.getString("INTERESES")		);
					granTotal.put("TOTAL_A_PAGAR",	registrosTotal.getString("TOTAL_A_PAGAR")	);
					registrosTotal.rewind();
				}else{
					granTotal.put("CAPITAL",			"0.00");
					granTotal.put("INTERESES",			"0.00");
					granTotal.put("TOTAL_A_PAGAR",	"0.00");
				}
				
				// 2.8 Obtener elementos del combo de paginacion
				indicesPagina 	= cqhelper.getSelectOptions();
				indicesPagina 	= indicesPagina == null?new ArrayList():indicesPagina;
				
			}
			
			// 3. Enviar resultados
			StringBuffer consulta 			= new StringBuffer();
 
			// 3.1 Obtener Catalogo de los Origenes de los Programas
			FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);	
		
			HashMap catalogoOrigenPrograma = fondoJunior.getCatalogoOrigenPrograma();
			
			// 3.2 Construir tabla de resultados
			consulta.append(
					"<table align=\"center\" border=\"1\"  bordercolor=\"#A5B8BF\" width=\"600\" cellpadding=\"2\" cellspacing=\"0\" style=\"border-bottom:0px;border-left:0px;border-right:0px;border-top:1px solid #A5B8BF;\"> "  +
					"	<tr>"  +
					"		<td class=\"celda01\" bordercolor=\"#A5B8BF\" align=\"center\" style=\"border-left:2px solid #A5B8BF;\">Fecha Amortizaci&oacute;n Nafin</td>"  +
					"		<td class=\"celda01\" bordercolor=\"#A5B8BF\" align=\"center\">Pr&eacute;stamo</td>"  +
					"		<td class=\"celda01\" bordercolor=\"#A5B8BF\" align=\"center\">Cliente</td>"  +
					"		<td class=\"celda01\" bordercolor=\"#A5B8BF\" align=\"center\">Num.<br>Cuota<br>a<br>Pagar</td>"  +
					"		<td class=\"celda01\" bordercolor=\"#A5B8BF\" align=\"center\">D&iacute;as<br>Transcurridos&nbsp;de<br>Vencimiento</td>"  +
					"		<td class=\"celda01\" bordercolor=\"#A5B8BF\" align=\"center\">Capital</td>"  +
					"		<td class=\"celda01\" bordercolor=\"#A5B8BF\" align=\"center\">Inter&eacute;s</td>"  +
					"		<td class=\"celda01\" bordercolor=\"#A5B8BF\" align=\"center\">Total&nbsp;a<br>Pagar</td>"  +
					"		<td class=\"celda01\" bordercolor=\"#A5B8BF\" align=\"center\" style=\"border-right:2px solid #A5B8BF;\">Origen</td>"  +
					"	</tr> "
			);
			if(hayDatos){
				while(registros.next()){
					consulta.append(
                  "<tr>"  +
						"	<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" style=\"border-left:2px solid #A5B8BF;\">&nbsp;"+registros.getString("FECHA_AMORTIZACION_NAFIN")+"</td>"  +
						"	<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\">&nbsp;"+registros.getString("PRESTAMO")+"</td> "  +
						"	<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\">&nbsp;"+registros.getString("CLIENTE")+"</td>"  +
						"	<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\">&nbsp;"+registros.getString("NUM_CUOTAS_A_PAGAR")+"</td>"  +
						"	<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\">&nbsp;"+registros.getString("DIAS_TRANSCURRIDOS_VENC")+"</td>"  +
						"	<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\">&nbsp;$"+Comunes.formatoDecimal(registros.getString("CAPITAL")        ,2,true)+"</td>"  +
						"	<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\">&nbsp;$"+Comunes.formatoDecimal(registros.getString("INTERESES")      ,2,true)+"</td>"  +
						"	<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\">&nbsp;$"+Comunes.formatoDecimal(registros.getString("TOTAL_A_PAGAR")  ,2,true)+"</td>"  +
						"	<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" style=\"border-right:2px solid #A5B8BF;\">&nbsp;"+fondoJunior.getDescripcionOrigenPrograma(registros.getString("ORIGEN"),catalogoOrigenPrograma)+"</td>"  +
						"</tr>"
					);
				}
				consulta.append(
						"<tr>" +
								"<td align=\"left\"   bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" colspan=\"5\" style=\"border-left:0px;border-bottom:0px;\">&nbsp;Total:</td>" +
								"<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" style=\"border-bottom:2px solid #A5B8BF;\">&nbsp;$"+Comunes.formatoDecimal(totalPorPagina.get("CAPITAL")     	   ,2,true)+"</td>"  +
								"<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" style=\"border-bottom:2px solid #A5B8BF;\">&nbsp;$"+Comunes.formatoDecimal(totalPorPagina.get("INTERESES")        ,2,true)+"</td>"  +
								"<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" style=\"border-bottom:2px solid #A5B8BF;\">&nbsp;$"+Comunes.formatoDecimal(totalPorPagina.get("TOTAL_A_PAGAR")    ,2,true)+"</td>"  +
								"<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" style=\"border-right:0px;border-bottom:0px;\">&nbsp;</td>"  +
						"</tr>" +
						"<tr>" +
								"<td align=\"left\"   bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" colspan=\"5\" style=\"border-left:0px;border-bottom:0px;border-top:0px;\">&nbsp;Gran Total:</td>" +
								"<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" style=\"border-bottom:2px solid #A5B8BF;border-top:0px;\">&nbsp;$"+Comunes.formatoDecimal(granTotal.get("CAPITAL")     		,2,true)+"</td>"  +
								"<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" style=\"border-bottom:2px solid #A5B8BF;border-top:0px;\">&nbsp;$"+Comunes.formatoDecimal(granTotal.get("INTERESES")    	,2,true)+"</td>"  +
								"<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" style=\"border-bottom:2px solid #A5B8BF;border-top:0px;\">&nbsp;$"+Comunes.formatoDecimal(granTotal.get("TOTAL_A_PAGAR")	,2,true)+"</td>"  +
								"<td align=\"center\" bordercolor=\"#A5B8BF\" class=\"formas\" nowrap=\"nowrap\" style=\"border-right:0px;border-bottom:0px;border-top:0px;\">&nbsp;</td>"  +
						"</tr>"
				);
			}else{
				consulta.append(
					"	<tr>"  +
					"		<td class=\"celda01\" bordercolor=\"#A5B8BF\"  align=\"center\" style=\"border-left:2px solid #A5B8BF;border-right:2px solid #A5B8BF;border-bottom:2px solid #A5B8BF;\"  colspan=\"9\" height=\"25px\"><b>No se encontraron registros</b></td>"  +
					"	</tr>"
				);
			}
			consulta.append(
				"</table>"
			);
			// Agregar combo de paginacion
			if(hayDatos && indicesPagina.size() > 0 ){
				consulta.append(
					"<table align=\"center\" width=\"60\" style=\"margin:5px;\" class=\"navigation\" > "  +
					"	<tr> "  +
					"		<td align=\"center\"> "  +
					"			<select id=\"paginaOffset\"  name=\"paginaOffset\" onchange=\"javascript:cambiodepagina();\" class=\"formas\" > "
				);
				for(int i = 0; i < indicesPagina.size(); i++){
					List opcion = (List)indicesPagina.get(i);
					String valorOpcion = opcion.get(0).toString();
					consulta.append(
						"				<option value=\""+valorOpcion+"\" "+(valorOpcion.equals(paginaOffset)?"selected":"")+">"+opcion.get(1).toString()+"</option>" 
					);
				}
				consulta.append(
					"			</select> "   +
					"		</td> "  + 
					"	</tr> "  +
					"</table> "
				);
			}
			resultado.put("tablaResultados",consulta.toString());
			
			// 3.3 Construir Botones de la Tabla de Resultados
			consulta.setLength(0);
			if(hayDatos){
				consulta.append(
					"<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\" >"  +
						"<tr>"  +
							"<td class=\"celda02\" width=\"70\" height=\"28\" align=\"center\" style=\"border-right:3px solid white;\">"  +
								"<a  href=\"javascript:generarArchivo();\">Generar<br>Archivo</a>"  +
							"</td>"  +
							"<td class=\"celda02\" width=\"70\" height=\"28\" align=\"center\" style=\"border-right:3px solid white;\">"  +
								"<a  href=\"javascript:imprimir();\">Imprimir</a>"  +
							"</td>"  +
							"<td class=\"celda02\" width=\"70\" height=\"28\" align=\"center\" style=\"padding-right:0px;\">"  +
								"<a  href=\"javascript:imprimirPDF();\">Imprimir<br>PDF</a>"  +
							"</td>"  +
						"</tr>"  +
					"</table>"
				);
			}
			resultado.put("botonesTablaResultados",consulta.toString());
 
			if("busqueda".equals(operacion)){	
				if( hayDatos && cqhelper.getIdsSize() > 500){
					resultado.put("generarArchivoZip","true");
				}else{
					resultado.put("generarArchivoZip","false");
				}
			}
			
		}else{ // Operacion no valida
			
			exitoOperacion = false;	
			resultado.put("mensaje","Operaci�n no reconocida.");
		}
		
	}catch(Exception e){
      System.out.println("33APenPago.data.jsp");
		e.printStackTrace();
		exitoOperacion = false;
		resultado.put("mensaje","Ocurri� un error al realizar la operaci�n.");
	}
	
	resultado.put("success",String.valueOf(exitoOperacion));
	out.println(resultado.toString());

%>

