
Ext.onReady(function() {

//Ext.onReady(function(){
var programa = (document.getElementById("nombrePrograma").value);
//});
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var jsonData = store.reader.jsonData;
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			if(store.getTotalCount() > 0) {//////////Verifica que existan registros
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
				el.unmask();					
			} else {	
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '33ControlIFJ01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'Transaccion'},
			{	name: 'Total_Transaccion'},	
			{	name: 'Total_Registros'}

		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});

//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
	var programa = new Ext.Container ({
		layout: 'table',
		id: 'labelPrograma',
		width: '481',
		heigth: 'auto',
		style: 'margin: 0 auto',
		align: 'center',
		defaults:{
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
			{
				xtype: 'displayfield',
				value: '<div align=center style="width:481px;"><b>'+programa+'</b></div><br><br><div align=center> <font size="3"><b>Control Fondo Junior</b></font></div>'
			}
		]
	});
	var elementosForma=[
		{
			xtype:'datefield',
			id:	'txtFechaMovimiento',
			name:	'fechaMovimiento',
			fieldLabel: 'Fecha de Movimiento Hasta',
			minValue		: '01/01/1901',
			invalidText: 'El formato de fecha es incorrecto, debe ser (dd/mm/aaaa).'
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 350,
		//title: 'Control Fondo Junior',
		frame: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 160,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		titleCollapse: true,
		items: elementosForma,			
		monitorValid: true,
		buttons: [
		{
				text: 'Buscar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,	
				handler: function(boton, evento) {
					fp.el.mask('Procesando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion : 'Consultar'
						})
					}); 
				}
			}/*,
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
				
					
				}
			}*/
		]
	});
	//grid de resultados de la consulta
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		//title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Transacci�n',
				tooltip: 'Transacci�n',
				dataIndex: 'Transaccion',
				sortable: true,
				width: 115,			
				resizable: true,				
				align: 'left',
				renderer:function(value){
					return value.replace('[','');
				}			
			},
			{
				header: 'Total Transacci�n',
				tooltip: 'Total Transacci�n',
				dataIndex: 'Total_Transaccion',
				sortable: true,
				width: 115,			
				resizable: true,				
				align: 'right'
			},
			{
				header: 'No Registros',
				tooltip: 'No Registros',
				dataIndex: 'Total_Registros',
				sortable: true,
				width: 114,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
					return value;
				}
			}								
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		autoHeight:true,
		width: 350,
		align: 'center',
		frame: false,
		bbar: ['->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',
					hidden: false,
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '33ControlIFJ01Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoCSV'
							}),
							success : function(response) {
								boton.setIconClass('icoXls');     
								boton.setDisabled(false);
							},	
							callback: procesarDescargaArchivos
						});						
					}
				},
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '33ControlIFJ01Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoPDF'
							}),
							success : function(response) {
								boton.setIconClass('icoPdf');     
								boton.setDisabled(false);
							},
							callback: procesarDescargaArchivos
						});						
					}
				}
		]
	});

	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 947,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			programa,
			NE.util.getEspaciador(10),
			fp,		
			NE.util.getEspaciador(10),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});


});	
