<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.*,
		javax.servlet.http.HttpServletRequest,
		javax.servlet.http.HttpServletResponse,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		java.util.*,
		java.text.SimpleDateFormat,
		java.math.*,
		com.netro.fondojr.*,
		com.netro.fondosjr.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		java.io.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String tipoMonitor  = request.getParameter("tipoMonitor")==null?"":request.getParameter("tipoMonitor");

/*	SE CREA NUEVO OBJETO PARA ACCESAR EL EJB */
FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

if (informacion.equals("busquedaAvanzada")) {
  String rfcPyme = (request.getParameter("rfcPyme")!=null)?request.getParameter("rfcPyme"):"";
  String nombrePyme = (request.getParameter("nombrePyme")!=null)?request.getParameter("nombrePyme"):"";
  
	CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" p.cg_razon_social ");	
  cat.setNafin_electronico("");
	
	cat.setRfc_pyme(rfcPyme);	
	cat.setNombre_pyme(nombrePyme);
	//cat.setNafin_electronico(n_electronico);
	cat.setOrden("p.cg_razon_social");		
	infoRegresar = cat.getJSONElementos();

}else
	if(informacion.equals("ObtenCatalogoIF")) {
		CatalogoIF catalogo = new CatalogoIF();
		catalogo.setClave("ic_if");
		catalogo.setDescripcion("cg_razon_social");
		
		infoRegresar = catalogo.getJSONElementos();
	
	}else if(informacion.equals("busquedaProveedor")) {
		String numero_sirac = (request.getParameter("numero_sirac")==null)?"":request.getParameter("numero_sirac");
		String nombre_cliente = "";
		
		if(numero_sirac != null && !numero_sirac.equals("") && Comunes.esNumeroEnteroPositivo(numero_sirac)){
		  List informacion_proveedores = fondoJunior.busquedaAvanzadaProveedor("", "", numero_sirac);
		  
		  if(!informacion_proveedores.isEmpty()){
			 List datos_proveedor = (List)informacion_proveedores.get(0);
			 nombre_cliente = (String)datos_proveedor.get(1);
		  }
		}
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("nombre_cliente", nombre_cliente);
		jsonObj.put("success", new Boolean(true));
		
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("fondoJuniorValidacionInicializar")) {
	
		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formato_fecha = new SimpleDateFormat("dd/MM/yyyy");
		String fecha_actual = formato_fecha.format(calendario.getTime());
		
		//String clavePrograma = (request.getParameter("cveProgramaFondoJR")==null)?"":request.getParameter("cveProgramaFondoJR");//FODEA 026 - 2010 ACF
		String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");
		String fecha_amort_nafin_ini = (request.getParameter("fecha_amort_nafin_ini")==null)?"":request.getParameter("fecha_amort_nafin_ini");
		String fecha_amort_nafin_fin = (request.getParameter("fecha_amort_nafin_fin")==null)?"":request.getParameter("fecha_amort_nafin_fin");
		if(fecha_amort_nafin_ini == null || fecha_amort_nafin_ini.equals("")){fecha_amort_nafin_ini = fecha_actual;}
      if(fecha_amort_nafin_fin == null || fecha_amort_nafin_fin.equals("")){fecha_amort_nafin_fin = fecha_actual;}
		String estatus_registro = (request.getParameter("estatus_registro")==null)?"":request.getParameter("estatus_registro");
		String numero_prestamo= (request.getParameter("numero_prestamo")==null)?"":request.getParameter("numero_prestamo");
		String clave_if = (request.getParameter("clave_if")==null)?"":request.getParameter("clave_if");
		String numero_sirac = (request.getParameter("numero_sirac")==null)?"":request.getParameter("numero_sirac");
		String fecha_pago_cliente_fide_ini = (request.getParameter("fecha_pago_cliente_fide_ini")==null)?"":request.getParameter("fecha_pago_cliente_fide_ini");
		String fecha_pago_cliente_fide_fin = (request.getParameter("fecha_pago_cliente_fide_fin")==null)?"":request.getParameter("fecha_pago_cliente_fide_fin");
		String fecha_registro_fide_ini = (request.getParameter("fecha_registro_fide_ini")==null)?"":request.getParameter("fecha_registro_fide_ini");
		String fecha_registro_fide_fin = (request.getParameter("fecha_registro_fide_fin")==null)?"":request.getParameter("fecha_registro_fide_fin");
		String cs_subtotales = (request.getParameter("cs_subtotales")==null)?"N":request.getParameter("cs_subtotales");
		String flag_inicio = (request.getParameter("flag_inicio")==null)?"S":request.getParameter("flag_inicio");
		String cs_validacion = "";
	
		
		
		//List catalogo_if = this.getCatalogoIF();
		//request.setAttribute("catalogo_if", catalogo_if);
		
		if(fecha_amort_nafin_ini == null || fecha_amort_nafin_ini.equals("")){fecha_amort_nafin_ini = fecha_actual;}
		if(fecha_amort_nafin_fin == null || fecha_amort_nafin_fin.equals("")){fecha_amort_nafin_fin = fecha_actual;}
		if(estatus_registro == null){estatus_registro = "";}
		if(numero_prestamo == null){numero_prestamo = "";}
		if(clave_if == null){clave_if = "";}
		if(numero_sirac == null){numero_sirac = "";}
		if(fecha_pago_cliente_fide_ini == null){fecha_pago_cliente_fide_ini = "";}
		if(fecha_pago_cliente_fide_fin == null){fecha_pago_cliente_fide_fin = "";}
		if(fecha_registro_fide_ini == null){fecha_registro_fide_ini = "";}
		if(fecha_registro_fide_fin == null){fecha_registro_fide_fin = "";}
		if(cs_subtotales == null){cs_subtotales = "N";}
		if(flag_inicio == null){flag_inicio = "S";}
	
		List operaciones_validacion = new ArrayList();	
		if(flag_inicio.equals("S")){
			List parametros_consulta = new ArrayList();
			parametros_consulta.add(fecha_amort_nafin_ini);
			parametros_consulta.add(fecha_amort_nafin_fin);
			parametros_consulta.add(estatus_registro);
			parametros_consulta.add(numero_prestamo);
			parametros_consulta.add(clave_if);
			parametros_consulta.add(numero_sirac);
			parametros_consulta.add(fecha_pago_cliente_fide_ini);
			parametros_consulta.add(fecha_pago_cliente_fide_fin);
			parametros_consulta.add(fecha_registro_fide_ini);
			parametros_consulta.add(fecha_registro_fide_fin);
			parametros_consulta.add(cs_validacion);
			parametros_consulta.add(clavePrograma);//FODEA 026 - 2010 ACF
			parametros_consulta.add("");//AJUSTE VALIDACION
			
			operaciones_validacion = fondoJunior.obtenerOperacionesValidacionEXTJS(parametros_consulta);
			/*
			for(int i = 0; i< operaciones_validacion.size(); i++){
				List operaciones_por_fecha = (List)operaciones_validacion.get(i);
				String fecha_vencimiento = (String)operaciones_por_fecha.get(0);
				List operaciones_pagados = (List)operaciones_por_fecha.get(1);
				List operaciones_no_pagados = (List)operaciones_por_fecha.get(2);
				List operaciones_reembolsos = (List)operaciones_por_fecha.get(3);
				List operaciones_prepagos = (List)operaciones_por_fecha.get(4);
				List operaciones_recuperaciones = (List)operaciones_por_fecha.get(5);

			}*/
			
			flag_inicio = "N";
		}      
	
		request.setAttribute("ver_subtotales", cs_subtotales);
		request.setAttribute("flag_inicio", flag_inicio);
		
		JSONObject jsonObj = new JSONObject();
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(operaciones_validacion);
		jsonObj.put("isValidReembXdia",new Boolean(false));
		jsonObj.put("TodosRegistros", jsObjArray.toString());
		jsonObj.put("total", jsObjArray.size()+"");
		jsonObj.put("csReembTodo", "F");
		jsonObj.put("strDirectorioPublicacion", strDirectorioPublicacion);
		jsonObj.put("success", new Boolean(true));
		
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("fondoJuniorValidacionAcuse")) {
		JSONObject jsonObj = new JSONObject();
		JSONArray jsObjArray = new JSONArray();
		//String cs_validacion = "ACUSE";
		String nombre_usuario = strNombreUsuario;
		String seccion_estatus = (request.getParameter("seccion_estatus")==null)?"":request.getParameter("seccion_estatus");
		String tipo_validacion = (request.getParameter("tipo_validacion")==null)?"":request.getParameter("tipo_validacion");
		String fecha_amortizacion = (request.getParameter("fecha_amortizacion")==null)?"":request.getParameter("fecha_amortizacion");
	
		//String clavePrograma = (request.getParameter("cveProgramaFondoJR")==null)?"":request.getParameter("cveProgramaFondoJR");//FODEA 026 - 2010 ACF
		String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");
		String fecha_amort_nafin_ini = (request.getParameter("fecha_amort_nafin_ini")==null)?"":request.getParameter("fecha_amort_nafin_ini");
		String fecha_amort_nafin_fin = (request.getParameter("fecha_amort_nafin_fin")==null)?"":request.getParameter("fecha_amort_nafin_fin");
		String estatus_registro = (request.getParameter("estatus_registro")==null)?"":request.getParameter("estatus_registro");
		String numero_prestamo= (request.getParameter("numero_prestamo")==null)?"":request.getParameter("numero_prestamo");
		String clave_if = (request.getParameter("clave_if")==null)?"":request.getParameter("clave_if");
		String numero_sirac = (request.getParameter("numero_sirac")==null)?"":request.getParameter("numero_sirac");
		String fecha_pago_cliente_fide_ini = (request.getParameter("fecha_pago_cliente_fide_ini")==null)?"":request.getParameter("fecha_pago_cliente_fide_ini");
		String fecha_pago_cliente_fide_fin = (request.getParameter("fecha_pago_cliente_fide_fin")==null)?"":request.getParameter("fecha_pago_cliente_fide_fin");
		String fecha_registro_fide_ini = (request.getParameter("fecha_registro_fide_ini")==null)?"":request.getParameter("fecha_registro_fide_ini");
		String fecha_registro_fide_fin = (request.getParameter("fecha_registro_fide_fin")==null)?"":request.getParameter("fecha_registro_fide_fin");
		String cs_subtotales = (request.getParameter("cs_subtotales")==null)?"N":request.getParameter("cs_subtotales");
		String flag_inicio = (request.getParameter("flag_inicio")==null)?"S":request.getParameter("flag_inicio");
		String cs_validacion = "";
		boolean respOK = true;
		
		String csReembTodo = request.getParameter("csReembTodo")==null?"":request.getParameter("csReembTodo");//F012-2011 FVR
			
		if(cs_subtotales == null){cs_subtotales = "N";}
		
		if (seccion_estatus.equals("PNP")) {
			fecha_amort_nafin_ini = fecha_amortizacion;
			fecha_amort_nafin_fin = fecha_amortizacion;		
		}
		
		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formato_cert = new SimpleDateFormat("ddMMyyHHmmss");
		SimpleDateFormat formato_fecha = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formato_hora = new SimpleDateFormat("hh:mm aa");
		
		String folio_validacion = "FJ" + formato_cert.format(calendario.getTime());
		String fecha_validacion = formato_fecha.format(calendario.getTime());
		String hora_validacion = formato_hora.format(calendario.getTime());
		
		jsonObj.put("folio_validacion",folio_validacion);
		jsonObj.put("fecha_validacion",fecha_validacion);
		jsonObj.put("hora_validacion",hora_validacion);
		
		if("V".equals(csReembTodo)){
			jsonObj.put("csReembTodo",csReembTodo);
		}
		
		
		String _acuse = "";
		String pkcs7 = (request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
		String serial = _serial;
		String folioCert = "";
		String externContent = (request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		char getReceipt = 'Y';	

		if (!serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
			folioCert = folio_validacion;
			Seguridad s = new Seguridad();
			if (s.autenticar(folioCert, serial, pkcs7, externContent, getReceipt)) {
				_acuse = s.getAcuse();
				jsonObj.put("acuseResp",_acuse);
				
				List parametros_consulta = new ArrayList();
				parametros_consulta.add(fecha_amort_nafin_ini);
				parametros_consulta.add(fecha_amort_nafin_fin);
				parametros_consulta.add(estatus_registro);
				parametros_consulta.add(numero_prestamo);
				parametros_consulta.add(clave_if);
				parametros_consulta.add(numero_sirac);
				parametros_consulta.add(fecha_pago_cliente_fide_ini);
				parametros_consulta.add(fecha_pago_cliente_fide_fin);
				parametros_consulta.add(fecha_registro_fide_ini);
				parametros_consulta.add(fecha_registro_fide_fin);
				parametros_consulta.add(tipo_validacion);
				parametros_consulta.add(seccion_estatus);
				
				
				String igfolioValidacion = fondoJunior.guardarValidacionCreditos(parametros_consulta);//AJUSTE VALIDACION
				jsonObj.put("igfolioValidacion",igfolioValidacion);
				
				parametros_consulta = new ArrayList();
				parametros_consulta.add(fecha_amort_nafin_ini);
				parametros_consulta.add(fecha_amort_nafin_fin);
				parametros_consulta.add(estatus_registro);
				parametros_consulta.add(numero_prestamo);
				parametros_consulta.add(clave_if);
				parametros_consulta.add(numero_sirac);
				parametros_consulta.add(fecha_pago_cliente_fide_ini);
				parametros_consulta.add(fecha_pago_cliente_fide_fin);
				parametros_consulta.add(fecha_registro_fide_ini);
				parametros_consulta.add(fecha_registro_fide_fin);
				parametros_consulta.add(cs_validacion);
				parametros_consulta.add(clavePrograma);//FODEA 026 - 2010 ACF
				parametros_consulta.add(igfolioValidacion);//AJUSTE VALIDACION
				
				int numReemb = 0;
				String masdequinientos = "F";
				List operaciones_validacion = new ArrayList();
				List operaciones_reembTodo = new ArrayList();
				
				if("V".equals(csReembTodo)){
					numReemb = fondoJunior.numReembTotales(clavePrograma,"V",igfolioValidacion);
				}
				if(numReemb<=500){
					masdequinientos = "F";
					operaciones_validacion = fondoJunior.obtenerOperacionesValidacionEXTJS(parametros_consulta);
					jsObjArray = JSONArray.fromObject(operaciones_validacion);
					jsonObj.put("TodosRegistrosVaidados", jsObjArray.toString());
				}else{
					masdequinientos = "V";
					operaciones_reembTodo = fondoJunior.getTotalesReembValida(clavePrograma,"",igfolioValidacion);
					jsObjArray = JSONArray.fromObject(operaciones_reembTodo);
					jsonObj.put("RegReembTodo", jsObjArray.toString());
				}
				jsonObj.put("masdequinientos",masdequinientos);
				
			}else{
				respOK = false;
				//throw new NafinException("GRAL0021");
			}
		}else{
			respOK = false;
			//throw new NafinException("GRAL0021");		
		}
		
		jsonObj.put("strNombreUsuario", strNombreUsuario);
		jsonObj.put("authRespOK", new Boolean(respOK));
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("fondoJuniorValidacionConsultarReemb")) {
		
		String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");
		String fecha_amort_nafin_ini = "";//(String)PropertyUtils.getProperty(form, "fecha_amort_nafin_ini");
		String fecha_amort_nafin_fin = "";//(String)PropertyUtils.getProperty(form, "fecha_amort_nafin_fin");
		String estatus_registro = (request.getParameter("estatus_registro")==null)?"R":request.getParameter("estatus_registro");
		String numero_prestamo= "";//(String)PropertyUtils.getProperty(form, "numero_prestamo");
		String clave_if = (request.getParameter("clave_if")==null)?"":request.getParameter("clave_if");
		String numero_sirac = "";//(String)PropertyUtils.getProperty(form, "numero_sirac");
		String fecha_pago_cliente_fide_ini = "";//(String)PropertyUtils.getProperty(form, "fecha_pago_cliente_fide_ini");
		String fecha_pago_cliente_fide_fin = "";//(String)PropertyUtils.getProperty(form, "fecha_pago_cliente_fide_fin");
		String fecha_registro_fide_ini = "";//(String)PropertyUtils.getProperty(form, "fecha_registro_fide_ini");
		String fecha_registro_fide_fin = "";//(String)PropertyUtils.getProperty(form, "fecha_registro_fide_fin");
		String cs_subtotales = "";//(String)PropertyUtils.getProperty(form, "cs_subtotales");
		String cs_validacion = "";
		
		List operaciones_validacion = null;
		int numReemb = 0;

		List parametros_consulta = new ArrayList();
		parametros_consulta.add(fecha_amort_nafin_ini);
		parametros_consulta.add(fecha_amort_nafin_fin);
		parametros_consulta.add(estatus_registro);
		parametros_consulta.add(numero_prestamo);
		parametros_consulta.add(clave_if);
		parametros_consulta.add(numero_sirac);
		parametros_consulta.add(fecha_pago_cliente_fide_ini);
		parametros_consulta.add(fecha_pago_cliente_fide_fin);
		parametros_consulta.add(fecha_registro_fide_ini);
		parametros_consulta.add(fecha_registro_fide_fin);
		parametros_consulta.add(cs_validacion);
		parametros_consulta.add(clavePrograma);//FODEA 029 - 2010 ACF
		parametros_consulta.add("");//AJUSTE VALIDACION

			
		numReemb = fondoJunior.numReembTotales(clavePrograma,"V","");
		String masdequinientos ="F";
		if(numReemb<=500){
			//request.setAttribute("masdequinientos", "F");
			operaciones_validacion = fondoJunior.obtenerOperacionesValidacionEXTJS(parametros_consulta);
		}else{
			//request.setAttribute("masdequinientos", "V");
			operaciones_validacion = fondoJunior.getTotalesReembValida(clavePrograma,"P","");
		}

		if(!operaciones_validacion.isEmpty()){
		  request.setAttribute("operaciones_validacion", operaciones_validacion);
		  
		}
		
		JSONObject jsonObj = new JSONObject();
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(operaciones_validacion);
		jsonObj.put("isValidReembXdia",new Boolean(true));
		jsonObj.put("masdequinientos",masdequinientos);
		jsonObj.put("TodosRegistros", jsObjArray.toString());
		jsonObj.put("csReembTodo", "V");
		jsonObj.put("success", new Boolean(true));
		
		infoRegresar = jsonObj.toString();

	}
	

System.out.println("infoRegresar = " + infoRegresar);

%>
<%=infoRegresar%>