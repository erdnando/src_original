
Ext.onReady(function() {
	var modificar = "N" ;
	var firma;
	var proceso;
	var numMovimiento="0";
	var nombreMovimientoReg ;
	var regNumero;
	var operacionReg;
	var tipoBoton;
	var numAcuse;
	var modificaCaptura ="";
	var iniProceso =  function() {  
		proceso;
		firma;
	}
	var tipoBotones= function(){
		tipoBoton;
	}
	var iniModifica =  function() {  
		modificar = "N";
		 nombreMovimientoReg ;
		operacionReg;
		regNumero;
	}
	var iniModificaCaptura =  function() {  
		modificaCaptura="";
		numMovimiento="0";
	}
	
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	var ini_hidDescProgFondoJR = Ext.getDom('hidDescProgFondoJR').value;
	var ini_clavePrograma = Ext.getDom('clavePrograma').value;
	
	var processSuccessInicializa = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			proceso	= jsonData.PROCESO;
		}	
	}
	var processSuccessConsultaPreAcuse = function(store, arrRegistros, opts) {
		fp.el.unmask();
		Ext.getCmp('forma').getForm().reset();
		var jsonData = store.reader.jsonData;
		if (arrRegistros != null) {
			var gridAcuse = Ext.getCmp('gridConsultaPreAcuse');
			if(jsonData.accion=='M'){
					Ext.MessageBox.alert('Mensaje',jsonData.mensaje);
				}
			if (!gridAcuse.isVisible()) {
				firma = jsonData.FIRMA; 
				gridAcuse.show();
			}
			var montoResumen = [
						['<b>TOTAL ', '<div align="right">'+Ext.util.Format.number(jsonData.IMPORTE_TOTAL, '$0,0.00')+'</div>' ]
				];
			consTotalesCaptura.loadData(montoResumen);
			Ext.getCmp('gridTotalesCaptura').show();
			var el = gridAcuse.getGridEl();	
			if(store.getTotalCount() > 0) {			
				Ext.getCmp('btnTerminaOperacion').enable();
				Ext.getCmp('btnCancelarAcuse').enable();
				el.unmask();
			} else {
				Ext.getCmp('btnTerminaOperacion').disable();
				Ext.getCmp('btnCancelarAcuse').disable();
				Ext.getCmp('gridTotalesCaptura').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}	
	}
	var processSuccessConsultaAcuse = function(store, arrRegistros, opts) {
		var infoR = store.reader.jsonData;
		if (arrRegistros != null) {
			var mensaje = infoR.mensaje;
			var acuse =infoR.NumeroAcuse;
			Ext.getCmp('btnTerminaOperacion').enable();
			Ext.MessageBox.alert('Mensaje',infoR.mensaje);
			Ext.getCmp('forma').hide();
			Ext.getCmp('gridConsultaPreAcuse').hide();
			Ext.getCmp('toolbarCC').hide();
			Ext.getCmp('gridTotalesCaptura').hide();
			if( acuse!=null  ){ 
				numAcuse =infoR.NumeroAcuse;
				Ext.getCmp('disTitle').body.update('<div align="center"> Recibo:  '+infoR.NumeroAcuse+'</b></div>');
				Ext.getCmp('numeroMovimientos').body.update('<div align="center"> '+infoR.numeroMovimientos+'</div>');
				Ext.getCmp('montoAportaciones').body.update('<div align="center"> '+"$"+infoR.montoAportaciones+'</div>');
				Ext.getCmp('montoRetiron').body.update('<div align="center"> '+"$"+infoR.montoRetiron+'</div>');
				fpAcuse.show();
			}
			var gridAcuse = Ext.getCmp('gridConsultaAcuse');
			if (!gridAcuse.isVisible()) {
				gridAcuse.show();
			}
			var el = gridAcuse.getGridEl();	
			if(store.getTotalCount() > 0) {			
				Ext.getCmp('btnGeneraArchivoAcuse').show();
				el.unmask();
			} else {
				Ext.getCmp('btnGeneraArchivoAcuse').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}	
	}
	function procesarDescargaArchivosAcuse(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnGeneraArchivoAcuse');
		btnImprimirPDF.enable();
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var processSuccessConsultaData = function(store, arrRegistros, opts) {
		fpConsulta.el.unmask();
		Ext.getCmp('formaConsulta').getForm().reset();
		var grid = Ext.getCmp('gridConsulta');
		var jsonData = store.reader.jsonData;
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var montoResumen = [
						['<b>TOTAL ', '<div align="right">'+Ext.util.Format.number(jsonData.IMPORTE_TOTAL, '$0,0.00')+'</div>' ]
				];
			consTotales.loadData(montoResumen);
			Ext.getCmp('gridTotales').show();
			var el = grid.getGridEl();	
			if(store.getTotalCount() > 0) {	
				Ext.getCmp('btnGenerarPDF').enable();
				el.unmask();
			} else {
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('gridTotales').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}	
	}
	function mostrarArchivoPDF1(opts, success, response) {
		Ext.getCmp('btnGenerarPDF').enable();
		var btnImprimirPDF = Ext.getCmp('btnGenerarPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var processSuccessConsultaOperacion = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var gridOpera = Ext.getCmp('gridOperacion');
			if (!gridOpera.isVisible()) {
				gridOpera.show();
			}
			var el = gridOpera.getGridEl();	
			if(store.getTotalCount() > 0) {			
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}	
	}
	function procesaAgregaOperacion(opts, success, response) {
		iniModifica();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var fp = Ext.getCmp('formaOperaciones');
				fp.el.unmask();
				var mensaje = jsonData.mensaje;
				Ext.getCmp('btnAgregarOperacionCat').enable();
				if(jsonData.accion=='G') {
					storeGridOperacion.load({
						params: Ext.apply(fpOperaciones.getForm().getValues(),{
							operacion: 'GenerarOperacion',
							informacion: 'ConsultarOperacion',
							start: 0,
							limit: 15
						})
					});
				}else if(jsonData.accion=='M') {
					Ext.MessageBox.alert('Mensaje',jsonData.mensaje);		
					storeGridOperacion.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'GenerarOperacion',
							informacion: 'ConsultarOperacion',
							start: 0,
							limit: 15
						})
					});
				}
				Ext.getCmp('formaOperaciones').getForm().reset();
			}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}
	function TransmiteBorrarCaptura(opts, success, response) {
		iniModificaCaptura();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var mensaje = jsonData.mensaje;
				if(jsonData.accion=='EC') {
					Ext.MessageBox.alert('Mensaje',jsonData.mensaje);	
				}
				storeConsultaPreAcuse.load({
					params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar',
						tipoInformacion: 'recargaConulta',
						start: 0,
						limit: 15,
						proceso:proceso
					})
				});
			}
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	function TransmiteBorrar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				Ext.MessageBox.alert('Mensaje',jsonData.mensaje);
				if(jsonData.accion=='E') {
					storeGridOperacion.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'GenerarOperacion',
							informacion: 'ConsultarOperacion',
							start: 0,
							limit: 15
						})
					});
				}
			}
		} else {
				NE.util.mostrarConnError(response,opts);				
		}
	}
	var procesarCapturaModificar = function(grid, rowIndex, colIndex, item, event) {
		iniModificaCaptura();
		var registro = grid.getStore().getAt(rowIndex);
		var tipOperacion1Reg = registro.get('OPERACION');
		var fechValorReg = registro.get('FECHA_VALOR');
		var importeReg = registro.get('IMPORTE');
		var referenciaReg = registro.get('REFERENCIA');
		var ObservacionesReg = registro.get('OBSERVACIONES');
		modificaCaptura = registro.get('MOVIMIENTO');
		numMovimiento = registro.get('MOVIMIENTO');
		Ext.getCmp('cmbTipoOperacion').setValue("");
		Ext.getCmp('dfFechaValor').setValue(fechValorReg);
		Ext.getCmp('tfImporte').setValue(importeReg);
		Ext.getCmp('tfReferencia').setValue(referenciaReg);
		Ext.getCmp('txtObservaciones').setValue(ObservacionesReg);
	}
	var procesarCapturaBorrar = function(grid, rowIndex, colIndex, item, event) {
		iniModificaCaptura();
		var registro = grid.getStore().getAt(rowIndex);
		numMovimiento = registro.get('MOVIMIENTO');
		Ext.Msg.show({
			title: 'Confirmar',
			msg: '�Esta seguro que desea eliminar ?',   
			buttons: Ext.Msg.OKCANCEL,
			fn: function peticionAjax(btn){
				if (btn == 'ok'){
					if(modificaCaptura=="S"){
						Ext.getCmp('forma').getForm().reset();
						iniModificaCaptura();
					}
					Ext.Ajax.request({
						url: '33MoviGeneralFondoJRExt01.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'EliminarCaptura',
							numMovimiento:numMovimiento
						}),
						callback: TransmiteBorrarCaptura
					});
				}
			}
		});
	}
	var procesarModificar = function(grid, rowIndex, colIndex, item, event) {
		iniModifica();
		modificar = "S";
		var registro = grid.getStore().getAt(rowIndex);
		nombreMovimientoReg = registro.get('DESCRIPCION');  
		operacionReg = registro.get('OPERACION'); 
		regNumero = registro.get('NUMERO'); 
		Ext.getCmp('tfNombre').setValue(nombreMovimientoReg);
		Ext.getCmp('cmbAgregaOperacion').setValue(operacionReg);
	}
	var procesarBorrar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var nombreMovimientoReg = registro.get('DESCRIPCION');  
		var operacionReg = registro.get('OPERACION'); 
		var regNumero = registro.get('NUMERO'); 
		Ext.Msg.show({
			title: 'Confirmar',
			msg: '�Esta seguro que desea eliminar ?',
			buttons: Ext.Msg.OKCANCEL,
			fn: function peticionAjax(btn){
				if (btn == 'ok'){
					if(modificar=="S"){
						Ext.getCmp('forma').getForm().reset();
						iniModifica();
					}
					Ext.Ajax.request({
						url: '33MoviGeneralFondoJRExt01.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Eliminar',
							nombreMovimientoReg:nombreMovimientoReg,
							operacionReg:operacionReg,
							regNumero:regNumero,
							modificar:modificar
						}),
						callback: TransmiteBorrar
					});
				}
			}
		});
	}
	var procesaResultado= function(opts, success, response){
		Ext.getCmp("btnCancelarAcuse").enable();
		var grid = Ext.getCmp('gridConsultaPreAcuse');
		grid.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			if (info != null){	
				Ext.MessageBox.alert('Mensaje',info.mensaje);
				Ext.getCmp('gridConsultaPreAcuse').hide();
				Ext.getCmp('gridTotalesCaptura').hide();
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesaCancelaOperacion = function(store, arrRegistros, opts){
		var  gridConsulta = Ext.getCmp('gridConsultaPreAcuse');
		var store = gridConsulta.getStore();
		var jsonData = store.data.items;
		gridConsulta.el.mask('Guardando....', 'x-mask-loading');
		Ext.getCmp("btnCancelarAcuse").disable();
				Ext.Ajax.request({
					url: '33MoviGeneralFondoJRExt01.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{  	
						informacion:'CancelarOperacion',
						proceso:proceso
				}),
				callback : procesaResultado
		});
	}
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------
	var catalogoOperacion = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '33MoviGeneralFondoJRExt01.data.jsp',
		baseParams: {
			informacion: 'Inicializar'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: processSuccessInicializa,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					processSuccessInicializa(null, null, null);
				}
			}
		}		
	});
	var storeConsultaPreAcuse = new Ext.data.JsonStore({
		url : '33MoviGeneralFondoJRExt01.data.jsp',
		id: 'storeConsultaPreAcuse',
		baseParams:{
			informacion: 'Consultar'
		},
		root : 'registros',
		fields: [
			{name: 'MOVIMIENTO'},
			{name: 'FOLIO'},
			{name: 'FECHA_MOVIMIENTO'},
			{name: 'FECHA_VALOR'},
			{name: 'IMPORTE'},
			{name: 'REFERENCIA'},
			{name: 'OPERACION'},
			{name: 'OBSERVACIONES'},
			{name: 'PROCESO'},
			{name: 'MODIFICAR'},
			{name: 'ELIMINAR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: processSuccessConsultaPreAcuse,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					processSuccessConsultaPreAcuse(null, null, null);
				}
			}
		}
	});
	var storeConsultaAcuse = new Ext.data.JsonStore({
		url : '33MoviGeneralFondoJRExt01.data.jsp',
		id: 'storeConsultaAcuse',
		baseParams:{
			informacion: 'consultaAcuse'
		},
		root : 'registros',
		fields: [
			{name: 'NUMERO'},
			{name: 'FECHA_MOVIMIENTO'},
			{name: 'FECHA_VALOR'},
			{name: 'IMPORTE'},
			{name: 'REFERENCIA'},
			{name: 'OPERACION'},
			{name: 'OBSERVACIONES'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: processSuccessConsultaAcuse,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					processSuccessConsultaAcuse(null, null, null);
				}
			}
		}
	});
	
	var storeGridCosulta = new Ext.data.JsonStore({
		url : '33MoviGeneralFondoJRExt01.data.jsp',
		id: 'storeGridCosulta',
		baseParams:{
			informacion: 'Consultar'
		},
		root : 'registros',
		fields: [
			{name: 'MOVIMIENTO'},
			{name: 'FOLIO'},
			{name: 'FECHA_MOVIMIENTO'},
			{name: 'FECHA_VALOR'},
			{name: 'IMPORTE'},
			{name: 'REFERENCIA'},
			{name: 'OPERACION'},
			{name: 'OBSERVACIONES'},
			{name: 'PROCESO'},
			{name: 'MODIFICAR'},
			{name: 'ELIMINAR'}
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: processSuccessConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					processSuccessConsultaData(null, null, null);
				}
			}
		}
	});
	var catOperacionAgrega = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['Suma','Suma'],
			['Resta','Resta']
		 ]
	});
	var storeGridOperacion = new Ext.data.JsonStore({
		url : '33MoviGeneralFondoJRExt01.data.jsp',
		id: 'storeGridOperacion',
		baseParams:{
			informacion: 'ConsultarOperacion'
		},
		root : 'registros',
		fields: [
			{name: 'DESCRIPCION'},
			{name: 'OPERACION'},
			{name: 'NUMERO'}
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: processSuccessConsultaOperacion,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					processSuccessConsultaOperacion(null, null, null);
				}
			}
		}
	});
//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
	var menuToolbar = new Ext.Toolbar({
		width: 210,
		style: 'margin:0 auto;',
		id:'toolbarCC',
		defaults: {
			msgTarget: 'side',
			anchor: '-10'
		},
		items: [
			{
				xtype: 'tbbutton',
				id: 'btnTipoCaptura',
				text: 'Captura',
				width: 100,
				handler: function(){
					Ext.getCmp('gridTotales').hide();
					Ext.getCmp('forma').getForm().reset();
				
					Ext.getCmp('forma').show();
					Ext.getCmp('formaConsulta').hide();
					Ext.getCmp('formaOperaciones').hide();
					
					Ext.getCmp('gridConsultaPreAcuse').hide();
					Ext.getCmp('gridConsultaAcuse').hide();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridOperacion').hide();
					Ext.getCmp('gridTotalesCaptura').hide();
					tipoBotones();
					tipoBoton = "captura";
					catalogoOperacion.load();
					firma;
				}	
			},'-',{
				xtype: 'tbbutton',
				text:'Consulta',
				width: 100,
				id: 'btnTipoConsulta',
				handler: function(){//forma
					Ext.getCmp('gridTotales').hide();
					Ext.getCmp('formaConsulta').show();
					Ext.getCmp('idTipoConsulta').show();
					Ext.getCmp('idFechaConsulta').show();
					Ext.getCmp('formaOperaciones').hide();
					Ext.getCmp('forma').hide();
					Ext.getCmp('formaConsulta').getForm().reset();
					Ext.getCmp('gridConsultaPreAcuse').hide();
					Ext.getCmp('gridConsultaAcuse').hide();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridOperacion').hide();
					Ext.getCmp('gridTotalesCaptura').hide();
					
					
				}
			}
		]
	});
	
	
	var confirmarAcuse = function(pkcs7, vfirma){
	
		if (Ext.isEmpty(pkcs7)) {
			Ext.Msg.alert('Verifique que la AC que emiti� el certificado est� instalada o habilitada,\nen Security -> Certificates -> Signers, si no cancel� la firma.');
			Ext.getCmp("btnTerminaOperacion").disable();
			return;
		}else{
			storeConsultaAcuse.load({
				params: Ext.apply(fp.getForm().getValues(),{  	
					operacion: 'Generar',
					informacion: 'consultaAcuse',
					proceso:proceso,
					pkcs7: pkcs7,
					numProceso:	proceso,
					textoFirmado: 	vfirma
				})
			});
		}
	}
	
	
	var gridConsultaPreAcuse = new Ext.grid.GridPanel({
		id: 'gridConsultaPreAcuse',
		store: storeConsultaPreAcuse,
		columns: [
			{
				header: 'Fecha de Movimiento',
				tooltip: 'Fecha de movimiento',
				dataIndex: 'FECHA_MOVIMIENTO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'Fecha Valor',
				tooltip: 'Fecha Valor',
				dataIndex: 'FECHA_VALOR',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: ' Referencia',
				tooltip: ' Referencia',
				dataIndex: 'REFERENCIA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'Capital',
				tooltip: 'Capital',
				dataIndex: 'IMPORTE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'N�m. Movimientos',
				tooltip: 'N�m. Movimientos',
				dataIndex: 'MOVIMIENTO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align :'center'
			},
			{
				header: 'Operaci�n',
				tooltip: 'Operaci�n',
				dataIndex: 'OPERACION',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'Observaciones',
				tooltip: 'Observaciones',
				dataIndex: 'OBSERVACIONES',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align :'left'
			},
			{
				xtype		: 'actioncolumn',
				header: 'Modificar',
				tooltip: 'MODIFICAR ',
				id			:'btnModificar',
				width		:  100,	
				align		: 'center', 
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'MODIFICAR';
							return 'modificar';
						},
						handler:procesarCapturaModificar
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Eliminar',
				tooltip: 'ELIMINAR ',
				id			:'btnElimina',
				width		:  50,	
				align		: 'center', 
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Eliminar';
							return 'borrar';
						},
						handler:procesarCapturaBorrar
					}
				]
			}
		],
		style: 	'margin: 0 auto;',
		hidden : true,
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		height: 		360,
		width: 		820,
		frame: true,
		bbar: {
			items: [
				'->',
				{
					xtype: 'button',
					id: 'btnTerminaOperacion',
					text: 'Termina Operaci�n',   
					handler: function(){
					
					/*
						var pkcs7 	= NE.util.firmar(firma);   
						if (Ext.isEmpty(pkcs7)) {
							Ext.Msg.alert('Verifique que la AC que emiti� el certificado est� instalada o habilitada,\nen Security -> Certificates -> Signers, si no cancel� la firma.');
							Ext.getCmp("btnTerminaOperacion").disable();
							return;
						}else{
							storeConsultaAcuse.load({
								params: Ext.apply(fp.getForm().getValues(),{  	
									operacion: 'Generar',
									informacion: 'consultaAcuse',
									proceso:proceso,
									pkcs7: pkcs7,
									numProceso:	proceso,
									textoFirmado: 	firma
								})
							});
						} 
						
						*/
						NE.util.obtenerPKCS7(confirmarAcuse, firma  );
						
						
					}	
				},'-',{
					xtype: 'button',
					text:'Cancelar',
					iconCls:	'icoCancelar',
					id: 'btnCancelarAcuse',
					handler:procesaCancelaOperacion
				}
			]
		}	
	});
	var gridConsultaAcuse = new Ext.grid.GridPanel({
		id: 'gridConsultaAcuse',
		store: storeConsultaAcuse,
		columns: [
			{
				header: 'Fecha de Movimiento',
				tooltip: 'Fecha de movimiento',
				dataIndex: 'FECHA_MOVIMIENTO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'Fecha Valor',
				tooltip: 'Fecha Valor',
				dataIndex: 'FECHA_VALOR',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'Capital',
				tooltip: 'Capital',
				dataIndex: 'IMPORTE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'N�m. Movimientos',
				tooltip: 'N�m. Movimientos',
				dataIndex: 'NUMERO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align :'center'
			},
			{
				header: 'Operaci�n',
				tooltip: 'Operaci�n',
				dataIndex: 'OPERACION',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'Observaciones',
				tooltip: 'Observaciones',
				dataIndex: 'OBSERVACIONES',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align :'left'
			
			}
		],
		style: 	'margin: 0 auto;',
		hidden : true,
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		height: 		250,
		width: 		650,
		frame: true,
		bbar: {
			items: [
				'->',
				{
					xtype: 'button',
					text:'Genera Archivo ',
					iconCls:	'icoPdf',
					//width: 240,
					hidden	: true,
					id: 'btnGeneraArchivoAcuse',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '33MoviGeneralFondoJRExt01.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'GenereArchivoAcuse',
									proceso:proceso,
									numAcuse:numAcuse
									   
							}),
							callback: procesarDescargaArchivosAcuse
						});
					}
				}
				]
			}	
	});
	
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',
		store: storeGridCosulta,
		columns: [
			{
				header: 'Fecha de Movimiento',
				tooltip: 'Fecha de movimiento',
				dataIndex: 'FECHA_MOVIMIENTO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'Fecha Valor',
				tooltip: 'Fecha Valor',
				dataIndex: 'FECHA_VALOR',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: ' Referencia',
				tooltip: ' Referencia',
				dataIndex: 'REFERENCIA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: '<div align="center">Capital</div>',
				tooltip: 'Capital',
				dataIndex: 'IMPORTE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'N�m. Movimientos',
				tooltip: 'N�m. Movimientos',
				dataIndex: 'MOVIMIENTO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align :'center'
			},
			{
				header: 'Operaci�n',
				tooltip: 'Operaci�n',
				dataIndex: 'OPERACION',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'Observaciones',
				tooltip: 'Observaciones',
				dataIndex: 'OBSERVACIONES',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align :'left'
			
			}
		],
		style: 	'margin: 0 auto;',
		hidden : true,
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		height: 		360,
		width: 		820,
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacionConsulta',
			store: storeGridCosulta,
			displayInfo: true,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No se encontro ning�n registro",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Imprimir PDF',
					iconCls:	'icoPdf',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacionConsulta");
						boton.disable();
						boton.setIconClass('loading-indicator');
						var tipOpera = Ext.getCmp('cmbTipoOperacionConsulta').getValue();
						var fechaValor = Ext.getCmp('dfFechaValorConsulta').getValue();
						Ext.Ajax.request({
							url: '33MoviGeneralFondoJRExt01.data.jsp',
							params: Ext.apply({
								informacion: 'ArchivoPDF',
								tipoOperacion :tipOpera,
								fechaValor:fechaValor,
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: mostrarArchivoPDF1
						});
					}
				}
			]
		}	
	});
	
	var gridOperacion = new Ext.grid.GridPanel({
		id: 'gridOperacion',
		store: storeGridOperacion,
		columns: [
			{
				header: 'Nombre de Movimiento',
				tooltip: 'Nombre de Movimiento',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'Operaci�n',
				tooltip: 'Operaci�n',
				dataIndex: 'OPERACION',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				xtype		: 'actioncolumn',
				header: 'Modificar',
				tooltip: 'Modificar ',
				id			:'btnModificar',
				width		:  100,	
				align		: 'center', 
				hidden	: false,
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'MODIFICAR';
							return 'modificar';
						},
						handler:procesarModificar
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Eliminar',
				tooltip: 'Eliminar ',
				id			:'btnElimina',
				width		:  50,	
				align		: 'center', 
				hidden	: false,
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Eliminar';
							return 'borrar';
						},
						handler:procesarBorrar
					}
				]
			}
		],
		style: 	'margin: 0 auto;',
		hidden : true,
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		height: 		200,
		width: 		450,
		frame: true,
		bbar: {
				xtype: 'paging',
				pageSize: 15,
				buttonAlign: 'left',
				id: 'barraPaginacionOperacion',
				store: storeGridOperacion,
				displayInfo: true,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No se encontro ning�n registro"
		}
	});
	var consTotales = new Ext.data.ArrayStore({
		fields: [			
			{name: 'etiqueta'},
			{name: 'informacion' }			
		]
		
	});
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consTotales,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		hidden: true,		
		columns:[			
			{
				header: '',
				dataIndex: 'etiqueta',
				sortable: true,
				width: 100,
				align: 'center'
			},
			{
				header: '',
				dataIndex: 'informacion',   
				sortable: true,
				width: 200,
				align: 'center'
			}
		],	
		stripeRows: true,
		loadMask: true,
		height: 50,
		width: 350,
		style: 'margin:0 auto;',
		frame: true
	});
	var consTotalesCaptura = new Ext.data.ArrayStore({
		fields: [			
			{name: 'etiqueta'},
			{name: 'informacion' }			
		]
	});
	var gridTotalesCaptura = new Ext.grid.EditorGridPanel({	
		store: consTotalesCaptura,
		id: 'gridTotalesCaptura',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		hidden: true,		
		columns:[			
			{
				header: '',
				dataIndex: 'etiqueta',
				sortable: true,
				width: 100,
				align: 'center'
			},
			{
				header: '',
				dataIndex: 'informacion',   
				sortable: true,
				width: 200,
				align: 'center'
			}
		],	
		stripeRows: true,
		loadMask: true,
		height: 50,
		width: 350,
		style: 'margin:0 auto;',
		frame: true
	});
	var elementosAcuse = [
		{
			xtype: 'panel',layout:'table',	width:500,	border:true,	layoutConfig:{ columns: 2 },
			defaults: {frame:false, border: true,width:160, height: 35,bodyStyle:'padding:4px'},
			items:[
				{	id:'disTitle',	width:500,	colspan:2,	border:false,	height:45,	frame:true,	html:'<div align="center">Recibo N�mero<br>Recibo N�mero:</div>'	},
				{	html:'<div class="formas" align="left">N�mero Total de Movimientos</div>'	},
				{	width:340,id:'numeroMovimientos',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Monto Total de Aportaciones</div>'	},
				{	width:340,id:'montoAportaciones',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="left">Monto Total de Retiros</div>'	},
				{	width:340,id:'montoRetiron',	html:'&nbsp;'},
				{	width:500,	colspan:4,	border:false,	height:60,	frame:true, html:'<div class="formas" align="left">Al transmitir este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al Fondo, aceptando de la forma la responsabilidad de los movimientos registrado del mismo, dicha transmisi�n tendr� validez para todos los efectos legales. </div>'	}
			]
		}
	];
	var elementosForma=[
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de Operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',	
					name: 'tipoOperacion',	
					id: 'cmbTipoOperacion',	
					hiddenName : 'tipoOperacion',
					autoLoad: false,
					msgTarget: 'side',
					mode: 'local',	
					emptyText: 'Seleccionar ... ',
					forceSelection : true,	
					triggerAction: 'all',	
					typeAhead: true,
					minChars: 1,
					allowBlank: true,	
					store: catalogoOperacion,	
					displayField : 'descripcion',   
					valueField : 'clave',	
					width: 200,
					tpl:  '<tpl for=".">' +
                '<tpl if="!Ext.isEmpty(loadMsg)">'+
                '<div class="loading-indicator">{loadMsg}</div>'+
                '</tpl>'+
                '<tpl if="Ext.isEmpty(loadMsg)">'+
                '<div class="x-combo-list-item">' +
                '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                '</div></tpl></tpl>'
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 10
				},
				{
					xtype: 'tbbutton',
					text:'Agregar Operaci�n',
					iconCls: 'icoAgregar',
					id: 'btnAgregaOperacion',
					handler: function(){
						Ext.getCmp('formaOperaciones').show();
						Ext.getCmp('idOperacionNombre').show();
						Ext.getCmp('idOparacionTipo').show();
						Ext.getCmp('forma').hide();
						Ext.getCmp('formaConsulta').hide();
						Ext.getCmp('gridTotales').hide();
						Ext.getCmp('gridConsultaPreAcuse').hide();
						Ext.getCmp('gridConsultaAcuse').hide();
						Ext.getCmp('gridConsulta').hide();
						Ext.getCmp('gridOperacion').hide();
						Ext.getCmp('gridTotalesCaptura').hide();
						Ext.getCmp('formaOperaciones').getForm().reset();
						storeGridOperacion.load({
							params: Ext.apply(fpOperaciones.getForm().getValues(),{
								operacion: 'GenerarOperacion',
								informacion: 'ConsultarOperacion',
								start: 0,
								limit: 15
							})
						});
					}	
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Valor',
			combineErrors: false,
			id:'linea2',
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaValor',
					id: 'dfFechaValor',
					maxLength: 10,
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0',
					regex     : /.*[1-9][0-9]{3}$/,
					regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'"

				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					id: 'formatoFecha',
					width: 20
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Importe',
			id:'linea3',
			hidden:false,
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
				 xtype:'bigdecimal',		
				 maxText:'El tama�o m�ximo del campos es de 17 enteros 2 decimales',				
				 maxValue: '99999999999999999.99', 				 
				 name: 'importe',
				 id: 'tfImporte',
				 allowDecimals: true,
				 allowNegative: true,							
				 hidden: 			false,	
				 allowBlank: true,
				 msgTarget: 		'side',
				 anchor:			'-20',						
				 format:			'00000000000000000.00'
				}
				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Referencia',
			combineErrors: false,
			id:'linea4',
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'referencia',
					id: 'tfReferencia',
					allowBlank: true,
					msgTarget: 'side',
					maxLength: 12,
					width: 100,
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',			
					valueField: 'clave',
					hiddenName : 'referencia',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;Observaciones',
			id:'linea5',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textarea',
					name: 'observaciones',
					id: 'txtObservaciones',
					allowBlank: true,
					msgTarget: 'side',
					width: 200,
					maxLength: 300,
					height: 50
			
				}
			]
		}
	];
	var elementosFormaConsulta=[
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de Operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			id:'idTipoConsulta',
			items: [
				{
					xtype: 'combo',	
					name: 'tipoOperacion',
					id: 'cmbTipoOperacionConsulta',	
					hiddenName : 'tipoOperacion',
					autoLoad: false,
					msgTarget: 'side',
					width: 200,
					mode: 'local',	
					emptyText: 'Seleccionar ... ',
					forceSelection : true,	
					triggerAction: 'all',	
					typeAhead: true,
					minChars: 1,
					
					allowBlank: true,	
					store: catalogoOperacion,	
					displayField : 'descripcion',
					valueField : 'clave',	
					tpl:  '<tpl for=".">' +
                '<tpl if="!Ext.isEmpty(loadMsg)">'+
                '<div class="loading-indicator">{loadMsg}</div>'+
                '</tpl>'+
                '<tpl if="Ext.isEmpty(loadMsg)">'+
                '<div class="x-combo-list-item">' +
                '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                '</div></tpl></tpl>'
				}
				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Valor',
			combineErrors: false,
			id:'idFechaConsulta',
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaValor',
					id: 'dfFechaValorConsulta',
					allowBlank: true,
					width: 100,
					hidden:false,
					startDay: 0,
					msgTarget: 'side',
					margins: '0 20 0 0',
					regex     : /.*[1-9][0-9]{3}$/,
					regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'"
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 5
				}
				
			]
		}
		
	];
	var elementosFormaOperaciones=[
		{
				xtype: 'compositefield',
				fieldLabel: 'Nombre',
				combineErrors: false,
				msgTarget: 'side',
				minChars : 1,
				id:'idOperacionNombre',
				triggerAction : 'all',
				items: [				
					{
						xtype: 'textfield',
						name: 'nombre',
						id: 'tfNombre',
						allowBlank: true,
						maxLength: 100,
						width:200 ,
						mode: 'local',
						autoLoad: false,
						displayField: 'descripcion',			
						valueField: 'clave',
						hiddenName : 'numeroOrden',
						forceSelection : true,
						msgTarget: 'side'// para que se muestre el circulito de error
					}
				]	
		},
		{
				xtype: 'compositefield',
				fieldLabel: 'Tipo de Operaci�n',
				combineErrors: false,
				msgTarget: 'side',
				minChars : 1,
				triggerAction : 'all',
				id:'idOparacionTipo',
				items: [				
					{
					xtype: 'combo',	
					name: 'agregaOperacion',
					id: 'cmbAgregaOperacion',  
					mode: 'local',
					emptyText: 'Seleccionar ... ',
					forceSelection : true,	
					triggerAction: 'all',	
					typeAhead: true,
					minChars: 1,
					allowBlank: true,	
					store: catOperacionAgrega,	
					displayField : 'descripcion',
					valueField : 'clave',	
					width: 200
					}
				]	
		}
		
	];
	
	var fpAcuse=new Ext.FormPanel({	
		style: 'margin:0 auto;',
		height:'auto',
		width:502,	
		border:true,	
		frame:false,	
		items:elementosAcuse,	
		hidden:true 	
	});
	var fpConsulta=new Ext.form.FormPanel({	
		id: 'formaConsulta',
		title: 'Movimientos Generales al Fondo JR ',
		collapsible: true,  
		style: 'margin:0 auto;',
		height:'auto',
		width:420,	
		labelWidth: 150,
		border:true,	
		frame:true,
		bodyStyle: 'padding: 6px',
		defaultType: 'textfield',
		items:elementosFormaConsulta,
		monitorValid: true,
		buttons: [
		{
			text: 'Consultar',
			id: 'btnConsultar',
			iconCls: 'icoBuscar',
			formBind: true,
			handler: function() {
				var tipOpera = Ext.getCmp('cmbTipoOperacionConsulta').getValue();
				var fechaValor = Ext.getCmp('dfFechaValorConsulta').getRawValue();
				fpConsulta.el.mask('Buscando...', 'x-mask-loading');
				storeGridCosulta.load({
					params: Ext.apply({
						tipoOperacion :tipOpera,
						fechaValor:fechaValor,
						operacion: 'Generar',
						informacion: 'Consultar',
						start: 0,
						limit: 15
					})
				});
			}
		}
		]
	});
	var fpOperaciones=new Ext.form.FormPanel({	
		id: 'formaOperaciones',
		title: 'CATALOGO DE OPERACIONES  ',
		collapsible: true,  
		style: 'margin:0 auto;',
		height:'auto',
		width:400,	
		border:true,
		labelWidth: 150,
		frame:true,
		bodyStyle: 'padding: 6px',
		defaultType: 'textfield',
		items:elementosFormaOperaciones,	
		monitorValid: true,
		buttons: [
		{
			xtype: 'button',
			text: 'Agregar',
			id: 'btnAgregarOperacionCat',
			iconCls: 'icoAgregar',
			formBind: true,	
			handler: function(boton, evento) {
				var tipoOperacion = Ext.getCmp('cmbAgregaOperacion');
				var nombre = Ext.getCmp('tfNombre');
				if(Ext.isEmpty(tipoOperacion.getValue())){
					return;
				}
				if(Ext.isEmpty(nombre.getValue())){
					return;
				}
					boton.disable();
					var fp = Ext.getCmp('formaOperaciones');
					fp.el.mask('Procesando...', 'x-mask-loading');	
					Ext.Ajax.request({
						url: '33MoviGeneralFondoJRExt01.data.jsp',
						params: Ext.apply(fpOperaciones.getForm().getValues(),{  
							informacion: "AgregaOperacion",
							modificar : modificar,
							nombreMovimientoReg:nombreMovimientoReg,
							operacionReg:operacionReg,
							regNumero:regNumero
						}),
						callback: procesaAgregaOperacion
					});
			}
		},				
		{
			xtype: 'button',
			text: 'Cancelar',
			iconCls:	'icoCancelar',
			id: 'btnCancelarCat',
			handler: function(boton, evento) {
				window.location = './33MoviGeneralFondoJRExt01.jsp';
			}
		}	
		]
	});
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 550,
		title: 'Movimientos Generales al Fondo JR ',
		frame: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		titleCollapse: false,
		items: elementosForma,	
		monitorValid: true,
		buttons: [
			{
				text: 'Agregar',
				id: 'btnAgregarCaptura',
				iconCls: 'icoAgregar',
				formBind: true,
				handler: function() {
					var tipoOperacion = Ext.getCmp('cmbTipoOperacion');
					var fecha = Ext.getCmp('dfFechaValor');
					var importe = Ext.getCmp('tfImporte');
					var referencia = Ext.getCmp('tfReferencia');
					var observacion = Ext.getCmp('txtObservaciones');
					
					if(Ext.isEmpty(tipoOperacion.getValue())){
						tipoOperacion.markInvalid('Seleccione el Tipo de Operaci�n');
						return;
					}
					if(Ext.isEmpty(fecha.getValue())){
							fecha.markInvalid('El valor de la Fecha Valor es requerido');
							fecha.focus();
							return;
					}
					if(Ext.isEmpty(importe.getValue())){
						importe.markInvalid('El valor de el Importe es requerido');
						return;
					}
					if(Ext.isEmpty(referencia.getValue())){
						referencia.markInvalid('El valor de La Referencia es requerido.');
						return;
					}
					if(Ext.isEmpty(observacion.getValue())){
							observacion.markInvalid("El valor de Las Observaciones es requerido.");
						return;
					}
					fp.el.mask('Agregando ... ','x-mask-loading');
					storeConsultaPreAcuse.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							tipoInformacion: 'AgregaCaptura',
							start: 0,
							limit: 15,
							modificaCaptura:modificaCaptura,
							numMovimiento:numMovimiento,
							proceso:proceso
						})
					});
					iniModificaCaptura();
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiarCaptura',
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('forma').getForm().reset();				
				}
			}
		]
	});
	var textoPrograma = new Ext.Container({
		layout: 'table',		
		id: 'textoPrograma',							
		width:	'450',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: false,
		style: 'margin:0 auto;',
		items: [
		{ 	xtype: 'displayfield',  id: 'mensaje', align :'center',	value: ini_hidDescProgFondoJR }				
		]
	});
	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 950,		
		height: 'auto',
		style: 'margin:0 auto;',
		aling: 'center',
		items: [	
			textoPrograma,
			NE.util.getEspaciador(20),
			menuToolbar,
			NE.util.getEspaciador(10),
			fp,
			fpAcuse,
			fpConsulta,
			fpOperaciones,
			NE.util.getEspaciador(20),
			gridConsultaPreAcuse,
			NE.util.getEspaciador(20),
			gridConsultaAcuse,
			gridConsulta,
			gridOperacion,
			gridTotales,
			gridTotalesCaptura
		]
	});
	catalogoOperacion.load();
	Ext.getCmp('formaOperaciones').hide();
	Ext.getCmp('formaConsulta').hide();
});	
