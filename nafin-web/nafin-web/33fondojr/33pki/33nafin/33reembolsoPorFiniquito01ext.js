Ext.onReady(function() {

/********** Variables globales **********/
	var texto_plano = '';
	var recordType  = Ext.data.Record.create([ {name:'clave'}, {name:'descripcion'} ]);

/********** Inicializar **********/
	var inicializar = function(){
		formaCargaIndividual.el.mask('Cargando Datos...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '33reembolsoPorFiniquito01ext.data.jsp',
			params: Ext.apply({
				informacion: 'Inicializacion'
			}),
			callback: procesarInicializar
		});
	};

/***** Descargar archivos PDF *****/
	function descargaArchivo(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			formaTerminarCaptura.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			formaTerminarCaptura.getForm().getEl().dom.submit();

		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnImprimir').setIconClass('icoPdf');
		Ext.getCmp('btnImprimirAcuse').setIconClass('icoPdf');
		Ext.getCmp('btnGeneraArchivoAcuse').setIconClass('icoXls');
	}

/********** Acciones de Limpiar, Cancelar, Regresar **********/
	function limpiarPantalla(opcion){
		if(opcion.equals('CANCELAR_CARGA_INDIVIDUAL')){
			Ext.getCmp('formaCargaIndividual').getForm().reset();
			Ext.getCmp('formaTerminarCaptura').getForm().reset();
			Ext.getCmp('formaCargaIndividual').show();
			Ext.getCmp('formaTerminarCaptura').hide();
			Ext.getCmp('gridConsulta').hide();
			Ext.getCmp('gridTotal').hide();
			Ext.getCmp('btnAceptar').enable();
			Ext.getCmp('btnModificar').hide();
			Ext.getCmp('btnAceptarCarga').show();
			Ext.getCmp('btnCancelar').show();
			inicializar();
		} else{

		}
	};

/********** Resultado despues de buscar el numero de prestamo **********/
	var procesarInicializar = function(opts, success, response){
		var formaCargaIndividual = Ext.getCmp('formaCargaIndividual');
		formaCargaIndividual.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.getCmp('no_prestamo_aux_id').setValue(Ext.util.JSON.decode(response.responseText).no_prestamo_aux);
			Ext.getCmp('proceso_carga_id').setValue(Ext.util.JSON.decode(response.responseText).proceso_carga);
			Ext.getCmp('programa_id').setValue(Ext.util.JSON.decode(response.responseText).descPrograma);
		}
	};

/********** Valida el n�mero de pr�stamo si es correcto busca los datos **********/
	function buscaDatosPrestamo(){
		if( Ext.getCmp('no_prestamo_id').getValue() == '' ){
			Ext.getCmp('no_prestamo_id').markInvalid('El campo es obligatorio');
		} else if( !/^([0-9])*$/.test(Ext.getCmp('no_prestamo_id').getValue()) ){
			Ext.getCmp('no_prestamo_id').markInvalid('El valor en este campo es inv�lido');
		} else if( (Ext.getCmp('no_prestamo_id').getValue()).length > 12 ){
			Ext.getCmp('no_prestamo_id').markInvalid('El tama�o m�ximo para este campo es de 12');
		} else{
			formaCargaIndividual.el.mask('Buscando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '33reembolsoPorFiniquito01ext.data.jsp',
				params: Ext.apply({
					informacion: 'Datos_Prestamo',
					no_prestamo: Ext.getCmp('no_prestamo_id').getValue()
				}),
				callback: procesarDatosPrestamo
			});
		}
	};

/********** Resultado despues de buscar el numero de prestamo **********/
	var procesarDatosPrestamo = function(opts, success, response) {
		var formaCargaIndividual = Ext.getCmp('formaCargaIndividual');
		formaCargaIndividual.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			valorFormulario = Ext.util.JSON.decode(response.responseText).registro;
			formaCargaIndividual.getForm().setValues(valorFormulario);
			if( Ext.util.JSON.decode(response.responseText).mensaje != '' ){
				Ext.getCmp('btnAceptar').disable();
				Ext.Msg.alert( 'Mensaje', Ext.util.JSON.decode(response.responseText).mensaje );
			} else{
				Ext.getCmp('btnAceptar').enable();
			}
		}
	};

/********** Agrega el reembolso **********/
	function agregaReembolso(){
		if( Ext.getCmp('formaCargaIndividual').getForm().isValid() ){
			if(Ext.getCmp('no_prestamo_aux_id').getValue() == ''){
				Ext.Ajax.request({
					url: '33reembolsoPorFiniquito01ext.data.jsp',
					params: Ext.apply({
						informacion:      'Agrega_Rembolso',
						tipo_carga:       'INDIVIDUAL',
						no_prestamo:      Ext.getCmp('no_prestamo_id').getValue(),
						monto_reembolso:  replaceAll(Ext.getCmp('monto_desembolso_id').getValue(),',',''),
						observaciones:    Ext.getCmp('observaciones_id').getValue(),
						proceso_carga:    Ext.getCmp('proceso_carga_id').getValue()
					}),
					callback: procesarAgregaReembolso
				});
			} else {
				Ext.Ajax.request({
					url: '33reembolsoPorFiniquito01ext.data.jsp',
					params: Ext.apply({
						informacion:        'AgregaMod_Rembolso',
						tipo_carga:         'INDIVIDUAL',
						no_prestamo:        Ext.getCmp('no_prestamo_id').getValue(),
						monto_reembolso:    Ext.getCmp('monto_desembolso_id').getValue(),
						observaciones:      Ext.getCmp('observaciones_id').getValue(),
						no_prestamo_aux:    Ext.getCmp('no_prestamo_aux_id').getValue(),
						proceso_carga:      Ext.getCmp('proceso_carga_id').getValue()
					}),
					callback: procesarAgregaReembolso
				});
			}
		}
	};

/********** Proceso posterior al metodo de agregar reembolsos **********/
	var procesarAgregaReembolso = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			valorFormulario = Ext.util.JSON.decode(response.responseText).registro;
			formaCargaIndividual.getForm().setValues(valorFormulario);
			if( Ext.util.JSON.decode(response.responseText).registros != '' ){
				Ext.getCmp('proceso_carga_id').setValue(Ext.util.JSON.decode(response.responseText).registros.proceso_carga);
				Ext.getCmp('no_prestamo_aux_id').setValue(Ext.util.JSON.decode(response.responseText).registros.no_prestamo_aux);
				consultaData.load({
					params: Ext.apply({
						proceso_carga: Ext.getCmp('proceso_carga_id').getValue()
					})
				});
				if(Ext.util.JSON.decode(response.responseText).datos_prestamo == 'S'){
					Ext.getCmp('btnModificar').show();
				} else{
					Ext.getCmp('btnModificar').hide();
				}
			} else{
				Ext.getCmp('btnModificar').hide();
			}
			if( Ext.util.JSON.decode(response.responseText).mensaje != '' ){
				Ext.Msg.alert( 'Mensaje', Ext.util.JSON.decode(response.responseText).mensaje );
			}
		}
		Ext.getCmp('btnAceptar').disable();
	};

/********** Modifica el reembolso **********/
	function modificaReembolso(){
		if( Ext.getCmp('formaCargaIndividual').getForm().isValid() ){

			Ext.getCmp('no_prestamo_aux_id').setValue(Ext.getCmp('no_prestamo_id').getValue());

			if( Ext.getCmp('proceso_carga_id').getValue() != ''){
				Ext.Ajax.request({
					url: '33reembolsoPorFiniquito01ext.data.jsp',
					params: Ext.apply({
						informacion:        'Modificar_Rembolso',
						no_prestamo:        Ext.getCmp('no_prestamo_id').getValue(),
						no_prestamo_aux:    Ext.getCmp('no_prestamo_aux_id').getValue(),
						observaciones:      Ext.getCmp('observaciones_id').getValue(),
						proceso_carga:      Ext.getCmp('proceso_carga_id').getValue()
					}),
					callback: procesarModificaReembolso
				});
			}
		}
	};

/********** Proceso posterior al metodo de agregar reembolsos **********/
	var procesarModificaReembolso = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			valorFormulario = Ext.util.JSON.decode(response.responseText).registro;
			formaCargaIndividual.getForm().setValues(valorFormulario);
			if( Ext.util.JSON.decode(response.responseText).registros != '' ){
				Ext.getCmp('btnAceptar').enable();
				Ext.getCmp('proceso_carga_id').setValue(Ext.util.JSON.decode(response.responseText).registros.proceso_carga);
				Ext.getCmp('observaciones_id').setValue(Ext.util.JSON.decode(response.responseText).observaciones);
				consultaData.load({
					params : Ext.apply({
						tipo_archivo:  '',
						proceso_carga: Ext.getCmp('proceso_carga_id').getValue()
					})
				});
				if(Ext.util.JSON.decode(response.responseText).datos_prestamo == 'S'){
					Ext.getCmp('btnModificar').show();
				} else{
					Ext.getCmp('btnModificar').hide();
				}
			} else{
				Ext.getCmp('btnModificar').hide();
			}
			if( Ext.util.JSON.decode(response.responseText).mensaje != '' ){
				Ext.Msg.alert( 'Mensaje', Ext.util.JSON.decode(response.responseText).mensaje );
			}
		}
	};
	
	/********** Transmitir desembolsos para terminar la captura **********/	
	var transmitirDesembolsos = function(pkcs7, vtexto_plano){
	
		if (Ext.isEmpty(pkcs7)) {
			Ext.Msg.alert( 'Error', 'Error en el proceso de Firmado.<br> No se puede continuar.');
			return; //Error en la firma. Termina...
		} else{
			Ext.Ajax.request({
				url: '33reembolsoPorFiniquito01ext.data.jsp',
				params: Ext.apply({
					informacion:        'ReembolsosPorFiniquitoAcuse',
					tipo_carga:         'INDIVIDUAL',
					proceso_carga:      Ext.getCmp('proceso_carga_id').getValue(),
					monto_reembolso:    Ext.getCmp('monto_desembolso_id').getValue(),
					disposicion:        Ext.getCmp('disposicion_id').getValue(),
					no_prestamo:        Ext.getCmp('no_prestamo_id').getValue(),
					externContent:      vtexto_plano,
					pkcs7:              pkcs7

				}),
				callback: procesarDesembolsoAcuse
			});
		}		
	}


/********** Proceso posterior al metodo de agregar reembolsos **********/
	var procesarDesembolsoAcuse = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			Ext.getCmp('formaTerminarCaptura').hide();
			Ext.getCmp('formaAcuse').show();

			if( Ext.util.JSON.decode(response.responseText).mensaje != '' ){
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridTotal').hide();
				Ext.getCmp('display1_id').hide();
				Ext.getCmp('folio_acuse_id').hide();
				Ext.getCmp('total_amortizaciones_id').hide();
				Ext.getCmp('monto_total_amortizaciones_id').hide();
				Ext.getCmp('numero_acuse_id').hide();
				Ext.getCmp('fecha_carga_id').hide();
				Ext.getCmp('hora_carga_id').hide();
				Ext.getCmp('usuario_id').hide();
				Ext.getCmp('display2_id').hide();
				Ext.getCmp('display3_id').show();
				Ext.getCmp('btnRegresar').show();
				Ext.getCmp('btnImprimirAcuse').hide();
				Ext.getCmp('btnGeneraArchivoAcuse').hide();
				Ext.getCmp('btnSalirAcuse').hide();
				Ext.getCmp('btnCancelar').show();
				Ext.getCmp('btnAceptarCarga').show();
				Ext.Msg.alert( 'Mensaje', Ext.util.JSON.decode(response.responseText).mensaje );
			} else{
				consultaData.load({
					params: Ext.apply({
						tipo_archivo:  'ACUSE',
						proceso_carga: Ext.getCmp('proceso_carga_id').getValue(),
						disposicion:   Ext.getCmp('disposicion_id').getValue()
					})
				});
				Ext.getCmp('display1_id').show();
				Ext.getCmp('folio_acuse_id').show();
				Ext.getCmp('total_amortizaciones_id').show();
				Ext.getCmp('monto_total_amortizaciones_id').show();
				Ext.getCmp('numero_acuse_id').show();
				Ext.getCmp('fecha_carga_id').show();
				Ext.getCmp('hora_carga_id').show();
				Ext.getCmp('usuario_id').show();
				Ext.getCmp('display2_id').show();
				Ext.getCmp('display3_id').hide();
				Ext.getCmp('btnRegresar').hide();
				Ext.getCmp('btnImprimirAcuse').show();
				Ext.getCmp('btnGeneraArchivoAcuse').show();
				Ext.getCmp('btnSalirAcuse').show();
				Ext.getCmp('btnCancelar').hide();
				Ext.getCmp('btnAceptarCarga').hide();
				Ext.getCmp('folio_acuse_id').setValue(Ext.util.JSON.decode(response.responseText).folio_acuse);
				Ext.getCmp('total_amortizaciones_id').setValue(Ext.util.JSON.decode(response.responseText).numReembolsos);
				Ext.getCmp('monto_total_amortizaciones_id').setValue(Ext.util.JSON.decode(response.responseText).parcialReembolsos);
				Ext.getCmp('numero_acuse_id').setValue(Ext.util.JSON.decode(response.responseText).folio_carga);
				Ext.getCmp('fecha_carga_id').setValue(Ext.util.JSON.decode(response.responseText).fecha_carga);
				Ext.getCmp('hora_carga_id').setValue(Ext.util.JSON.decode(response.responseText).hora_carga);
				Ext.getCmp('usuario_id').setValue(Ext.util.JSON.decode(response.responseText).nombre_usuario);
			}
		}
	};

/********** Proceso para generar el Grid Consulta **********/
	var procesarConsultaData = function(store, arrRegistros, opts){
		var formaCargaIndividual = Ext.getCmp('formaCargaIndividual');
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();

		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			if(jsonData.tipo_archivo == 'ACUSE'){

			/***** Se muestran solo los botones correspondientes *****/
			Ext.getCmp('btnImprimirAcuse').show();
			Ext.getCmp('btnGeneraArchivoAcuse').show();
			Ext.getCmp('btnSalirAcuse').show();
			Ext.getCmp('btnCancelar').hide();
			Ext.getCmp('btnAceptarCarga').hide();

				Ext.getCmp('total_desembolsos_id').setValue(store.getTotalCount());
				Ext.getCmp('cantidad_id').setValue(jsonData.total_desembolso);
				if(store.getTotalCount() > 0) {
					consultaTotal.load({
						params : Ext.apply({
							desembolsos_tot: Ext.getCmp('total_desembolsos_id').getValue(),
							cantidad_total: Ext.getCmp('cantidad_id').getValue()
						})
					});
				} else {
					formaCargaIndividual.el.unmask();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridTotal').hide();
					el.unmask();
				}

			} else{
				/***** Se muestran solo los botones correspondientes *****/
				Ext.getCmp('btnImprimirAcuse').hide();
				Ext.getCmp('btnGeneraArchivoAcuse').hide();
				Ext.getCmp('btnSalirAcuse').hide();
				Ext.getCmp('btnCancelar').show();
				Ext.getCmp('btnAceptarCarga').show();

				Ext.getCmp('total_desembolsos_id').setValue(store.getTotalCount());
				Ext.getCmp('cantidad_id').setValue(jsonData.total_desembolso);
				Ext.getCmp('disposicion_id').setValue(jsonData.disposicion);
				if(  Ext.getCmp('total_desembolsos_id').getValue() != '' && Ext.getCmp('cantidad_id').getValue() != '' ){

					texto_plano  = 'No. Total de Amortizaciones a Desembolsar|' + Ext.getCmp('total_desembolsos_id').getValue() + '\n';
					texto_plano += 'Monto Total de Amortizaciones a Desembolsar|' + Ext.getCmp('cantidad_id').getValue() + '\n';
					texto_plano += 'Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento ' +
										'contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, ' +
										'dicha transmisi�n tendr� validez para todos los efectos legales.\n';
					texto_plano += jsonData.texto_plano;
					texto_plano += 'Total Monto por Desembolsos|' + Ext.getCmp('cantidad_id').getValue() + '\n';
					texto_plano += 'Total Desembolsos|' + Ext.getCmp('total_desembolsos_id').getValue() + '\n';

				}
				if(store.getTotalCount() > 0) {
					consultaTotal.load({
						params: Ext.apply({
							desembolsos_tot: Ext.getCmp('total_desembolsos_id').getValue(),
							cantidad_total: Ext.getCmp('cantidad_id').getValue()
						})
					});
				} else {
					formaCargaIndividual.el.unmask();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridTotal').hide();
					el.unmask();
				}
			}
		}
	};

	var porcesaConsultaTotal = function(store, arrRegistros, opts){
		var jsonData = store.reader.jsonData;
		if(store.getTotalCount() > 0) {
			Ext.getCmp('gridConsulta').show();
			Ext.getCmp('gridTotal').show();
		} else{
			Ext.getCmp('gridConsulta').hide();
			Ext.getCmp('gridTotal').hide();
		}
	}

/********** Se crea el store del Grid Consulta **********/
	var consultaData = new Ext.data.JsonStore({
		root:            'registros',
		url:             '33reembolsoPorFiniquito01ext.data.jsp',
		baseParams:      {
			informacion: 'Consulta_Grid_Carga_Individual'
		},
		fields: [
			{ name: 'FECHA_MOVIMIENTO'  },
			{ name: 'PRESTAMO'          },
			{ name: 'NO_SIRAC'          },
			{ name: 'CLIENTE'           },
			{ name: 'MONTO_DESEMBOLSO'  },
			{ name: 'AMORTIZACION'      },
			{ name: 'ORIGEN'            },
			{ name: 'OBSERVACIONES'     }
		],
		totalProperty:      'total',
		messageProperty:    'msg',
		autoLoad:           false,
		listeners:          {
			load:           procesarConsultaData,
			exception:      {
				fn:         function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

/********** Se crea el store del Grid Totales **********/
	var consultaTotal = new Ext.data.JsonStore({
		root:            'registros',
		url:             '33reembolsoPorFiniquito01ext.data.jsp',
		baseParams:      {
			informacion: 'Consulta_Grid_Total'
		},
		fields: [
			{ name: 'DESCRIPCION'},
			{ name: 'TOTAL'      }
		],
		totalProperty:      'total',
		messageProperty:    'msg',
		autoLoad:           false,
		listeners:          {
			load:           porcesaConsultaTotal,
			exception:      {
				fn:         function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					porcesaConsultaTotal(null, null, null);
				}
			}
		}
	});

/********** Se crea el grid Consulta **********/
	var gridConsulta = new Ext.grid.EditorGridPanel({
		width:             850,
		height:            350,
		id:                'gridConsulta',
		title:             '&nbsp;',
		style:             'margin:0 auto;',
		align:             'center',
		frame:             false,
		border:            true,
		hidden:            true,
		displayInfo:       true,
		loadMask:          true,
		stripeRows:        true,
		store:             consultaData,
		columns:           [
			{
				width:     120,
				header:    'Fecha de Movimiento',
				dataIndex: 'FECHA_MOVIMIENTO',
				align:     'center',
				sortable:  true,
				resizable: false
			},{
				width:     88,
				header:    'Pr�stamo',
				dataIndex: 'PRESTAMO',
				align:     'center',
				sortable:  true,
				resizable: true
			},{
				width:     150,
				header:    'N�mero de Cliente SIRAC',
				dataIndex: 'NO_SIRAC',
				align:     'center',
				sortable:  true,
				resizable: true
			},{
				width:     188,
				header:    'Cliente',
				dataIndex: 'CLIENTE',
				align:     'center',
				sortable:  true,
				resizable: true
			},{
				width:     130,
				header:    'Monto del Desembolso',
				dataIndex: 'MONTO_DESEMBOLSO',
				align:     'center',
				sortable:  true,
				resizable: true
			},{
				width:     88,
				header:    'Amortizaci�n',
				dataIndex: 'AMORTIZACION',
				align:     'center',
				sortable:  true,
				resizable: true
			},{
				width:     88,
				header:    'Origen',
				dataIndex: 'ORIGEN',
				align:     'center',
				sortable:  true,
				resizable: true
			},{
				width:     250,
				header:    'Observaciones',
				dataIndex: 'OBSERVACIONES',
				align:     'left',
				sortable:  true,
				resizable: true
			}
		]
	});

/********** Se crea el grid de totales **********/
	var gridTotal = new Ext.grid.EditorGridPanel({
		width:             850,
		id:                'gridTotal',
		style:             'margin:0 auto;',
		align:             'center',
		frame:             false,
		border:            true,
		hidden:            true,
		displayInfo:       true,
		loadMask:          true,
		stripeRows:        true,
		autoHeight:        true,
		store:             consultaTotal,
		columns:           [
			{
				width:     600,
				header:    'Descripci�n',
				dataIndex: 'DESCRIPCION',
				align:     'right',
				sortable:  true,
				resizable: false

			},{
				width:     250,
				header:    'Cantidad',
				dataIndex: 'TOTAL',
				align:     'left',
				sortable:  true,
				resizable: true
			}
		],
		bbar: {
			items: ['->','-', {
				xtype:   'button',
				text:    'Imprimir PDF',
				id:      'btnImprimirAcuse',
				iconCls: 'icoPdf',
				hidden:  true,
				handler: function(boton, evento) {
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '33reembolsoPorFiniquito01ext.data.jsp',
						params: Ext.apply({  
							informacion:     'Imprimir',
							tipo_archivo:    'ACUSE',
							programa:        Ext.getCmp('programa_id').getValue(),
							proceso_carga:   Ext.getCmp('proceso_carga_id').getValue(),
							cantidad_total:  Ext.getCmp('total_amortizaciones_id').getValue(),
							desembolsos_tot: Ext.getCmp('monto_total_amortizaciones_id').getValue(),
							fecha_carga:     Ext.getCmp('fecha_carga_id').getValue(),
							hora_carga:      Ext.getCmp('hora_carga_id').getValue(),
							folio_acuse:     Ext.getCmp('folio_acuse_id').getValue(),
							folio_carga:     Ext.getCmp('numero_acuse_id').getValue(),
							nombre_usuario:  Ext.getCmp('usuario_id').getValue()
						}),
						callback: descargaArchivo
					});
				}
			},'-', {
				xtype:   'button',
				text:    'Generar Archivo',
				id:      'btnGeneraArchivoAcuse',
				iconCls: 'icoXls',
				hidden:  true,
				handler: function(boton, evento) {
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '33reembolsoPorFiniquito01ext.data.jsp',
						params: Ext.apply({
							informacion:     'Genera_Archivo',
							tipo_archivo:    'ACUSE',
							programa:        Ext.getCmp('programa_id').getValue(),
							proceso_carga:   Ext.getCmp('proceso_carga_id').getValue(),
							cantidad_total:  Ext.getCmp('total_amortizaciones_id').getValue(),
							desembolsos_tot: Ext.getCmp('monto_total_amortizaciones_id').getValue(),
							fecha_carga:     Ext.getCmp('fecha_carga_id').getValue(),
							hora_carga:      Ext.getCmp('hora_carga_id').getValue(),
							folio_acuse:     Ext.getCmp('folio_acuse_id').getValue(),
							folio_carga:     Ext.getCmp('numero_acuse_id').getValue(),
							nombre_usuario:  Ext.getCmp('usuario_id').getValue()
						}),
						callback: descargaArchivo
					});
				}
			},'-', {
				xtype:    'button',
				text:     'Salir',
				id:       'btnSalirAcuse',
				iconCls:  'icoCancelar',
				hidden:   true,
				handler: function(boton, evento) {
					Ext.getCmp('formaCargaIndividual').getForm().reset();
					Ext.getCmp('formaTerminarCaptura').getForm().reset();
					Ext.getCmp('formaAcuse').getForm().reset();
					Ext.getCmp('formaCargaIndividual').show();
					Ext.getCmp('formaTerminarCaptura').hide();
					Ext.getCmp('formaAcuse').hide();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridTotal').hide();
					Ext.getCmp('btnAceptar').enable();
					Ext.getCmp('btnModificar').hide();
					Ext.getCmp('btnAceptarCarga').show();
					Ext.getCmp('btnCancelar').show();
					inicializar();
				}
			},'-', {
				xtype:   'button',
				text:    'Terminar Captura',
				id:      'btnAceptarCarga',
				iconCls: 'icoAceptar',
				handler: function(boton, evento) {
					Ext.getCmp('formaTerminarCaptura').show();
					Ext.getCmp('formaCargaIndividual').hide();
					Ext.getCmp('btnAceptarCarga').hide();
					Ext.getCmp('btnCancelar').hide();
				}
			},'-', {
				xtype:   'button',
				text:    'Cancelar',
				id:      'btnCancelar',
				iconCls: 'icoCancelar',
				handler: function(boton, evento) {
					Ext.getCmp('formaCargaIndividual').getForm().reset();
					Ext.getCmp('formaTerminarCaptura').getForm().reset();
					Ext.getCmp('formaAcuse').getForm().reset();
					Ext.getCmp('formaCargaIndividual').show();
					Ext.getCmp('formaTerminarCaptura').hide();
					Ext.getCmp('formaAcuse').hide();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridTotal').hide();
					Ext.getCmp('btnAceptar').enable();
					Ext.getCmp('btnModificar').hide();
					Ext.getCmp('btnAceptarCarga').show();
					Ext.getCmp('btnCancelar').show();
					inicializar();
				}
			}]
		}
	});

/********** Botones de menu. Indican si la carga es masiva, individual o si es consultal **********/
	var menuToolbar = new Ext.Toolbar({
		width:           500,
		id:              'menuToolbar',
		style:           'margin:0 auto;',
		items: [
			{
				width:   160,
				xtype:   'tbbutton',
				id:      'btnCargaIndividual',
				text:    'Carga Individual',
				handler: function(){
					Ext.getCmp('btnCargaIndividual').setText('<i><B>Carga Individual</i></B>');
				}
			},'-',{
				width:   160,
				xtype:   'tbbutton',
				id:      'btnCargaMasiva',
				text:    'Carga Masiva',
				handler: function(){
					window.location.href='33reembolsoPorFiniquitoMasivo01ext.jsp';
				}
			},'-',{
				width:   160,
				xtype:   'tbbutton',
				id:      'btnConsultaBatch',
				text:    'Consulta Batch',
				handler: function(){
					window.location.href='/nafin/33fondojr/33nafin/33consultaDesembolsos01ext.jsp';
				}
			}
		]
	});

/********** Form Panel Acuse **********/
	var formaAcuse = new Ext.form.FormPanel({
		width:          500,
		labelWidth:     200,
		id:             'formaAcuse',
		title:          'Acuse Desembolsos',
		layout:         'form',
		style:          'margin:0 auto;',
		frame:          true,
		border:         true,
		hidden:         true,
		items: [{
			width:      480,
			id:         'display1_id',
			frame:      false,
			border:     false,
			hidden:     false,
			html:       '<div align="center"><br><b> La autentificaci�n se llev� a cabo con �xito. </b><br>&nbsp;</div>'
		},{
			width:      260,
			xtype:      'textfield', 
			id:         'folio_acuse_id',
			name:       'folio_acuse',
			disabled:  true,
			fieldLabel: '&nbsp;&nbsp; Recibo'
		},{
			width:      260,
			xtype:      'textfield', 
			id:         'total_amortizaciones_id',
			name:       'total_amortizaciones',
			labelAlign: 'right',
			disabled:  true,
			fieldLabel: '&nbsp;&nbsp; No. Total de Amortizaciones <br>&nbsp;&nbsp; Desembolsadas'
		},{
			width:      260,
			xtype:      'textfield', 
			id:         'monto_total_amortizaciones_id',
			name:       'monto_total_amortizaciones',
			labelAlign: 'right',
			disabled:  true,
			fieldLabel: '&nbsp;&nbsp; Monto Total de Amortizaciones <br>&nbsp;&nbsp; Desembolsadas'
		},{
			width:      260,
			xtype:      'textfield', 
			id:         'numero_acuse_id',
			name:       'numero_acuse',
			labelAlign: 'right',
			disabled:  true,
			fieldLabel: '&nbsp;&nbsp; N�mero de Acuse'
		},{
			width:      260,
			xtype:      'textfield', 
			id:         'fecha_carga_id',
			name:       'fecha_carga',
			labelAlign: 'right',
			disabled:  true,
			fieldLabel: '&nbsp;&nbsp; Fecha de Carga'
		},{
			width:      260,
			xtype:      'textfield', 
			id:         'hora_carga_id',
			name:       'hora_carga',
			labelAlign: 'right',
			disabled:  true,
			fieldLabel: '&nbsp;&nbsp; Hora de carga'
		},{
			width:      260,
			xtype:      'textfield', 
			id:         'usuario_id',
			name:       'usuario',
			labelAlign: 'right',
			disabled:  true,
			fieldLabel: '&nbsp;&nbsp; Usuario'
		},{
			width:      480,
			id:         'display2_id',
			frame:      false,
			border:     false,
			html:       '<div align="left"><br><b> Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisi�n tendr� validez para todos los efectos legales. </b><br>&nbsp;</div>'
		},{
			width:      480,
			id:         'display3_id',
			frame:      false,
			border:     false,
			hidden:     false,
			html:       '<div align="center"><br><b> La autentificaci�n no se llev� a cabo. <br> PROCESO CANCELADO </b><br>&nbsp;</div>'
		}],
		buttons: [{
			id:      'btnRegresar',
			text:    'Regresar',
			iconCls: 'icoCancelar',
			hidden:  false,
			handler: function(boton, evento){
				Ext.getCmp('formaCargaIndividual').getForm().reset();
				Ext.getCmp('formaTerminarCaptura').getForm().reset();
				Ext.getCmp('formaAcuse').getForm().reset();
				Ext.getCmp('formaCargaIndividual').show();
				Ext.getCmp('formaTerminarCaptura').hide();
				Ext.getCmp('formaAcuse').hide();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridTotal').hide();
				Ext.getCmp('btnAceptar').enable();
				Ext.getCmp('btnModificar').hide();
				Ext.getCmp('btnAceptarCarga').show();
				Ext.getCmp('btnCancelar').show();
				inicializar();
			}
		}]
	});

/********** Form Panel Terminar Captura **********/
	var formaTerminarCaptura = new Ext.form.FormPanel({
		width:              500,
		id:                 'formaTerminarCaptura',
		title:              'Preacuse Desembolsos',
		layout:             'form',
		style:              'margin:0 auto;',
		frame:              false,
		border:             true,
		hidden:             true,
		items: [{
			width:          500,
			xtype:          'panel',  
			id:             'seccion_1', 
			layout:         'table', 
			border:         true, 
			layoutConfig:   { columns:2 },
			defaults:       {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{
					width:  400,
					frame:  false,
					border: false,
					html:   '<div align="left"><br> No. Total de Amortizaciones a Desembolsar: <br></div>'	
				},{
					width:  100, 
					xtype:  'displayfield', 
					id:     'total_desembolsos_id',
					name:   'total_desembolsos'
				}
			]
		},{
			width:          500,
			xtype:          'panel',  
			id:             'seccion_2', 
			layout:         'table', 
			border:         true, 
			layoutConfig:   { columns:2 },
			defaults:       {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{
					width:  400,
					frame:  false,
					border: false,
					html:   '<div align="left"><br> Monto Total de Amortizaciones a Desembolsar: <br></div>'	
				},{
					width:  100, 
					xtype:  'displayfield',
					id:     'cantidad_id',
					name:   'cantidad'
				}
			]
		},{
			width:          500,
			xtype:          'panel',  
			id:             'seccion_3', 
			layout:         'table', 
			border:         true, 
			layoutConfig:   { columns: 1 },
			defaults:       {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{
					width:  500,
					frame:  false,
					border: false,
					html:   '<div align="left"><br> Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisi�n tendr� validez para todos los efectos legales. <br>&nbsp;</div>'	
				}
			]
		}],
		buttons: [{
			id:             'btnImprimir',
			text:           'Generar Archivo',
			iconCls:        'icoPdf',
			handler:        function(boton, evento){
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '33reembolsoPorFiniquito01ext.data.jsp',
					params: Ext.apply({  
						informacion:     'Imprimir',
						tipo_archivo:    'PREACUSE',
						proceso_carga:   Ext.getCmp('proceso_carga_id').getValue(),
						cantidad_total:  Ext.getCmp('total_desembolsos_id').getValue(),
						desembolsos_tot: Ext.getCmp('cantidad_id').getValue(),
						programa:        Ext.getCmp('programa_id').getValue()
					}),
					callback: descargaArchivo
				});
			}
		},{
			id:      'btnTransDesembolsos',
			text:    'Transmitir Desembolsos',
			iconCls: 'modificar',
			handler: function(boton, evento){
				
				NE.util.obtenerPKCS7(transmitirDesembolsos, texto_plano );
				
			}
		},{
			id:      'btnCancelarPreacuse',
			text:    'Cancelar',
			iconCls: 'icoCancelar',
			handler: function(boton, evento){
				Ext.getCmp('formaCargaIndividual').getForm().reset();
				Ext.getCmp('formaTerminarCaptura').getForm().reset();
				Ext.getCmp('formaAcuse').getForm().reset();
				Ext.getCmp('formaCargaIndividual').show();
				Ext.getCmp('formaTerminarCaptura').hide();
				Ext.getCmp('formaAcuse').hide();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridTotal').hide();
				Ext.getCmp('btnAceptar').enable();
				Ext.getCmp('btnModificar').hide();
				Ext.getCmp('btnAceptarCarga').show();
				Ext.getCmp('btnCancelar').show();
				inicializar();
			}
		}]
	});

/********** Form Panel Carga individual **********/
	var formaCargaIndividual = new Ext.form.FormPanel({
		width:              500,
		labelWidth:         150,
		id:                 'formaCargaIndividual',
		title:              'Carga Individual',
		layout:             'form',
		bodyPadding:        '20 20 20 20',
		style:              'margin: 0px auto 0px auto;',
		frame:              true,
		autoHeight:         true,
		hidden:             false,
		items: [{
			width:          320,
			xtype:         'displayfield',
			id:            'programa_id',
			name:          'programa',
			fieldLabel:    '&nbsp;&nbsp; Programa',
			value:         '&nbsp;'
		},{
			xtype:          'compositefield',
			combineErrors:  false,
			items: [{
				width:      195,
				xtype:      'textfield',
				id:         'no_prestamo_id',
				name:       'no_prestamo',
				fieldLabel: '&nbsp;&nbsp; No. Pr�stamo',
				msgTarget:  'side',
				regex:      /^[0-9]*$/,
				allowBlank: false,
				maxLength:  12
			},{
				width:      10,
				xtype:     'displayfield',
				id:        'display0_id',
				name:      'display0',
				value:     '&nbsp;'
			},{
				xtype:     'button',
				id:        'btnBuscar',
				text:      'Buscar',
				iconCls:   'icoBuscar',
				handler:   function(boton, evento) {
					buscaDatosPrestamo();
				}
			}]
		},{
			width:          280,
			xtype:          'textfield', 
			id:             'cliente_id',
			name:           'cliente',
			fieldLabel:     '&nbsp;&nbsp; Cliente',
			msgTarget:      'side',
			disabled:       true/*,
			allowBlank:     false,
			maxLength:      100*/
		},{
			width:          280,
			xtype:          'textfield', 
			id:             'cliente_sirac_id',
			name:           'cliente_sirac',
			fieldLabel:     '&nbsp;&nbsp; No. Cliente SIRAC',
			msgTarget:      'side',
			disabled:       true/*,
			allowBlank:     false,
			maxLength:      12*/
		},{
			width:          280,
			xtype:          'bigdecimal', 
			id:             'monto_desembolso_id',
			name:           'monto_desembolso',
			fieldLabel:     '&nbsp;&nbsp; Monto del Desembolso',
			msgTarget:      'side',
			maxValue:       '9,999,999,999,999,999,999.99',
			minValue:       '0',
			format:         '0,000.00',
			allowBlank:     false
		},{
			width:          280,
			xtype:          'textfield', 
			id:             'origen_id',
			name:           'origen',
			fieldLabel:     '&nbsp;&nbsp; Origen',
			msgTarget:      'side',
			disabled:       true/*,
			allowBlank:     false,
			maxLength:      100*/
		},{
			width:          280,
			xtype:          'textarea', 
			id:             'observaciones_id',
			name:           'observaciones',
			fieldLabel:     '&nbsp;&nbsp; Observaciones',
			msgTarget:      'side',
			maxLength:      100
		},{
			width:          280,
			xtype:          'textfield', 
			id:             'no_prestamo_aux_id',
			name:           'no_prestamo_aux',
			fieldLabel:     '&nbsp;&nbsp; no prestamo aux',
			hidden:         true
		},{
			width:          280,
			xtype:          'textfield',
			id:             'proceso_carga_id',
			name:           'proceso_carga',
			fieldLabel:     '&nbsp;&nbsp; Proceso carga',
			hidden:         true
		},{
			width:          280,
			xtype:          'textfield',
			id:             'disposicion_id',
			name:           'disposicion',
			fieldLabel:     '&nbsp;&nbsp; Dsiposicion',
			hidden:         true
		}],
		buttons: [{
			id:             'btnAceptar',
			text:           'Agregar',
			iconCls:        'icoAceptar',
			disabled:       true,
			handler:        function(boton, evento){
				agregaReembolso();
			}
		},{
			id:             'btnModificar',
			text:           'Modificar',
			iconCls:        'modificar',
			hidden:         true,
			handler:        function(boton, evento){
				modificaReembolso();
			}
		},{
			id:             'btnLimpiar',
			text:           'Limpiar',
			iconCls:        'icoLimpiar',
			handler:        function(boton, evento){
				Ext.getCmp('formaCargaIndividual').getForm().reset();
				Ext.getCmp('formaTerminarCaptura').getForm().reset();
				Ext.getCmp('formaCargaIndividual').show();
				Ext.getCmp('formaTerminarCaptura').hide();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridTotal').hide();
				Ext.getCmp('btnAceptar').enable();
				Ext.getCmp('btnModificar').hide();
				Ext.getCmp('btnAceptarCarga').show();
				Ext.getCmp('btnCancelar').show();
				inicializar();
			}
		}]
	});

/********** Contenedor Principal **********/
	var pnl = new Ext.Container({
		width:   949,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		items: [
			NE.util.getEspaciador(10),
			menuToolbar,
			formaCargaIndividual,
			formaTerminarCaptura,
			formaAcuse,
			NE.util.getEspaciador(10),
			gridConsulta,
			gridTotal
		]
	});

/********** Inicializaci�n **********/
	Ext.getCmp('btnCargaIndividual').setText('<i><B>Carga Individual</i></B>');
	inicializar();

});