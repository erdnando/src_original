Ext.onReady(function() {
var hidDescProgFondoJR = Ext.getDom('hidDescProgFondoJR').value;

//-----------------------------------------------handlers response from requests

	/**
	*Funcion de respuesta a la peticion para generar los reeembolsos.
	*Se encarga de mostrar los componentes para el Acuse
	*/
	var procesarSuccessGenerarReembolsos = function(options, success, response) {
		main.el.unmask();
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var resp = Ext.JSON.decode(response.responseText);
			
			if(resp.mensaje==''){
				Ext.getCmp('display3_id').hide();
				Ext.getCmp('folio_acuse_id').setValue(resp.folio_acuse);
				Ext.getCmp('total_amortizaciones_id').setValue(Ext.getCmp('total_reembolsos_id').getValue());
				Ext.getCmp('monto_total_amortizaciones_id').setValue(Ext.getCmp('monto_reembolsos_id').getValue());
				Ext.getCmp('numero_acuse_id').setValue(resp.folio_carga);
				Ext.getCmp('fecha_carga_id').setValue(resp.fecha_carga);
				Ext.getCmp('hora_carga_id').setValue(resp.hora_carga);
				Ext.getCmp('usuario_id').setValue(resp.nombre_usuario);
				
				formaPreacuse.hide();
				formaAcuse.show();
				Ext.getCmp('display1_id').show();
				Ext.getCmp('folio_acuse_id').show();
				Ext.getCmp('total_amortizaciones_id').show();
				Ext.getCmp('monto_total_amortizaciones_id').show();
				Ext.getCmp('numero_acuse_id').show();
				Ext.getCmp('fecha_carga_id').show();
				Ext.getCmp('hora_carga_id').show();
				Ext.getCmp('usuario_id').show();
				Ext.getCmp('display2_id').show();
				Ext.getCmp('display3_id').hide();
				Ext.getCmp('btnRegresar').hide();
				
				Ext.getCmp('btnGeneraArchivoAcuse').show();
				Ext.getCmp('btnSalirAcuse').show();
				Ext.getCmp('btnImprimirAcuse').show();
				
			}else{
				Ext.getCmp('display1_id').hide();
				Ext.getCmp('folio_acuse_id').hide();
				Ext.getCmp('total_amortizaciones_id').hide();
				Ext.getCmp('monto_total_amortizaciones_id').hide();
				Ext.getCmp('numero_acuse_id').hide();
				Ext.getCmp('fecha_carga_id').hide();
				Ext.getCmp('hora_carga_id').hide();
				Ext.getCmp('usuario_id').hide();
				Ext.getCmp('display2_id').hide();
				Ext.getCmp('display3_id').show();
				Ext.getCmp('btnRegresar').show();
				formaPreacuse.hide();
				gpPreacuse.hide();
			}
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}

//--------------------------------------------------------------Handlers Botones
	/**
	*Funcion para el boton Buscar, se encarga de la peticion para realizar la
	*consulta por numero de prestamo para obtener sus amortizaciones No Pagadas
	*/
	var fnBtnBuscar = function(btn){
		if(fpCriteriosBusqueda.getForm().isValid()){			
			btn.disable();
			storeListaReembolsos.load({
				params:Ext.apply(fpCriteriosBusqueda.getForm().getValues())
			});
		}
	}
	
	/**
	*Fucion para el Boton Terminar Captura, se encarga de generar el Preacuse
	*de las amortizaciones seleccionadas
	*/
	var fnBtnTerminarCaptura = function(btn){
		var existeSeleccion = false;
		var montoReembolsos = 0;
		var cantidad = 0;
		var selected = [];
		var arrayReg = gpReembolsos.getSelectionModel().getSelection();
		
		Ext.each(arrayReg,function(record){
			selected.push(record);
			existeSeleccion = true;
			montoReembolsos += Number(record.data['MONTO_AMORTIZACION']);
			cantidad++;
		});
		
		if(existeSeleccion){
			fpCriteriosBusqueda.hide();
			gpReembolsos.hide();
			formaPreacuse.show();
			gpPreacuse.show();
			gpTotales.show();
			
			Ext.getCmp('total_reembolsos_id').setValue(cantidad);
			Ext.getCmp('monto_reembolsos_id').setValue(montoReembolsos);
			
			storeReembolsoPreacuse.add(selected);
			
			var acuseCifras = [
					['Total Monto Reembolsos', montoReembolsos],
					['Total Reembolsos', cantidad]
				];
			storeTotales.loadData(acuseCifras);
			
		}else{
			Ext.Msg.alert('Aviso', '<CENTER>No se ha seleccionado ning�n registro, favor de seleccionar uno</CENTER>');
		}
	
	}
	
	
	var confirmarAcuse = function(pkcs7, textoFirmar, selected ){		
		
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else{
			main.el.mask('Generando registros...', 'x-mask-loading');
			
			Ext.Ajax.request({
				url: '33fondojraltareembolsos_ext.data.jsp',
				params : {
					informacion: 'generarReembolsos',
					registros: Ext.encode(selected),
					pkcs7: pkcs7,
					textoFirmar: textoFirmar
				},
				callback: procesarSuccessGenerarReembolsos
			});
		}
	}
	
	
	
	/**
	*Funcion que realiza la peticion para generar los Reembolsos de las 
	*amortizaciones No Pagadas seleccionadas.
	*/
	var transmitirReembolsos = function(btn){
		var selected = [];
		
		var textoFirmar = 'No. Total de Amortizaciones a Reembolsar: '+Ext.getCmp('total_reembolsos_id').getValue();
		textoFirmar += '\nMonto Total de Amortizaciones a Reembolsar: '+Ext.getCmp('monto_reembolsos_id').getValue();
		textoFirmar += '\nAl transmitirse este MENSAJE DE DATOS, '+
							'queda bajo su responsabilidad el '+
							'movimiento contable al fondo, aceptando de esta '+
							'forma la responsabilidad de los movimientos registrados '+
							'del mismo, dicha transmisi�n tendr� validez para todos '+
							'los efectos legales.';
		textoFirmar += '\nFecha de Amortizacion|Prestamo|Numero de Cliente SIRAC|Cliente|Monto del Reembolso|No. Amortizacion|Origen';
		storeReembolsoPreacuse.each(function(record){
			textoFirmar += '\n'+record.data['FECHA_PROBABLEPAGO']+'|'+record.data['PRESTAMO']+'|'+record.data['NUM_CLIENTE_SIRAC']+'|'+
								record.data['NOMBRE_CLIENTE']+'|'+record.data['MONTO_AMORTIZACION']+'|'+record.data['AMORTIZACION']+'|'+
								record.data['ORIGEN'];
			selected.push(record.data);
		});
				
		NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar, selected  );		
		
	}
	
	/**
	*Funcion para respuesta en la generacion de archivos, se encarga de abrir el
	*archivo generado por medio de un Servlet
	*/
	function fnBtnDescargaArchivo(opts, success, response){
		if (success == true && Ext.JSON.decode(response.responseText).success == true){
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			
			var forma = Ext.getDom('formAux');
			forma.action 		= NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self';
			forma.submit();


		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnImprimirPreAcuse').setIconCls('icoPdf');
		Ext.getCmp('btnImprimirAcuse').setIconCls('icoPdf');
		Ext.getCmp('btnGeneraArchivoAcuse').setIconCls('icoXls');
	}
//---------------------------------------------------------------Handlers Stores
	/**
	*Funcion para la respuesta que retorna el Store al consultar la info.
	*por prestamo de las amortizaciones No Pagadas
	*/
	var procesarConsultaReembolsos = function(store,  arrRegistros, success, opts) {
		Ext.getCmp('btnBuscar1').enable();
		if (arrRegistros != null) {
			gpReembolsos.show();
			if(store.getTotalCount() > 0) {
				maskRegenerar.hide();
			}else {
				maskRegenerar.show();
			}
		}
	};
	
//-------------------------------------------------------------------------STORES
	
	Ext.define('ListaReembolsos', {
		 extend: 'Ext.data.Model',
		 fields: [
				{ name: 'FECHA_PROBABLEPAGO', type: 'string' },
				{ name: 'PRESTAMO', type: 'string' },
				{ name: 'AMORTIZACION', type: 'string' },
				{ name: 'MONTO_AMORTIZACION', type: 'string' },
				{ name: 'NUM_CLIENTE_SIRAC', type: 'string' },
				{ name: 'NOMBRE_CLIENTE', type: 'string' },
				{ name: 'ORIGEN', type: 'string' }
		 ]
	});
	
	var storeListaReembolsos = Ext.create('Ext.data.Store', {
		model: 'ListaReembolsos',
		proxy: {
			type: 'ajax',
			url: '33fondojraltareembolsos_ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'listaParaReembolsar'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsultaReembolsos
			}
	});
	
	
	var storeReembolsoPreacuse = Ext.create('Ext.data.ArrayStore', {
		model: 'ListaReembolsos',
		autoLoad: false
	});
	
	var storeTotales = Ext.create('Ext.data.ArrayStore', {
		fields: [
		  {name: 'DESCRIPCION'},
		  {name: 'TOTAL'}
		],
		autoLoad: false
	});
//-------------------------------------------------------------------------GRIDS
	var emptyViewConfig = {
		emptyText: [
			'<div class="empty-grid">',
			'<div class="x-mask-msg x-mask-msg-default"><div style="position:relative" class="mask-icon">',
			'<table border="1" width=600><tr><td>There are no records to display</td></tr></table>',
			'</div></div>',
			'</div>'
		].join(''),
		deferEmptyText: false
	};
    
	var gpReembolsos = Ext.create('Ext.grid.Panel',{
		name: 'gpReembolsos',
		id: 'gpReembolsos1',
		width: 700,
		height: 300,
		hidden: true,
		columnLines: true,
		frame: true,
		//viewConfig: emptyViewConfig,
		store: storeListaReembolsos,
		selType: 'checkboxmodel',
		columns:[
			{
				width:     120, text: 'Fecha de Vencimiento', dataIndex: 'FECHA_PROBABLEPAGO',
				align:     'center',	sortable:  true, resizable: false
			},{
				width:     88, text: 'Pr�stamo', dataIndex: 'PRESTAMO', align: 'center',
				sortable:  true, resizable: true
			},{
				width:     150, text: 'N�mero de Cliente SIRAC', dataIndex: 'NUM_CLIENTE_SIRAC',
				align:     'center',	sortable:  true, resizable: true
			},{
				width:     188, text: 'Cliente', dataIndex: 'NOMBRE_CLIENTE', align: 'center',
				sortable:  true, resizable: true
			},{
				width:     88, text: 'Amortizaci�n', dataIndex: 'AMORTIZACION', align: 'center',
				sortable:  true, resizable: true
			},{
				width:     130, text: 'Monto del Reembolso', dataIndex: 'MONTO_AMORTIZACION',
				align:     'center', sortable:  true, resizable: true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				width:     88, text: 'Origen', dataIndex: 'ORIGEN', align:'center',
				sortable:  true, resizable: true
			}
		],
		dockedItems: [{
				weight: 2,
				xtype: 'toolbar',
				dock: 'bottom',
				items: ['->',
					{
						text: 'Terminar Captura',
						iconCls: 'icoAceptar',
						scope: this,
						handler: fnBtnTerminarCaptura
					},
					{
						text: 'Cancelar',
						iconCls: 'icoCancelar',
						scope: this,
						handler: function(btn){
							window.location = '33fondojraltareembolsos_ext.jsp';
						}
					}
				]
			}
		]
	
	});
	
	var gpPreacuse = Ext.create('Ext.grid.Panel',{
		name: 'gpPreacuse',
		id: 'gpPreacuse1',
		width: 700,
		height: 300,
		hidden: true,
		columnLines: true,
		frame: true,
		store: storeReembolsoPreacuse,
		columns:[
			{
				width:     120, text: 'Fecha de Amortizaci�n', dataIndex: 'FECHA_PROBABLEPAGO',
				align:     'center',	sortable:  true, resizable: false
			},{
				width:     88, text: 'Pr�stamo', dataIndex: 'PRESTAMO', align: 'center',
				sortable:  true, resizable: true
			},{
				width:     150, text: 'N�mero de Cliente SIRAC', dataIndex: 'NUM_CLIENTE_SIRAC',
				align:     'center',	sortable:  true, resizable: true
			},{
				width:     188, text: 'Cliente', dataIndex: 'NOMBRE_CLIENTE', align: 'center',
				sortable:  true, resizable: true
			},{
				width:     88, text: 'Amortizaci�n', dataIndex: 'AMORTIZACION', align: 'center',
				sortable:  true, resizable: true
			},{
				width:     130, text: 'Monto del Reembolso', dataIndex: 'MONTO_AMORTIZACION',
				align:     'center', sortable:  true, resizable: true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				width:     88, text: 'Origen', dataIndex: 'ORIGEN', align:'center',
				sortable:  true, resizable: true
			}
		]
	
	});
	
	var gpTotales = Ext.create('Ext.grid.Panel',{
		width:             700,
		id:                'gridTotal',
		style:             'margin:0 auto;',
		align:             'center',
		frame:             false,
		border:            true,
		hidden:            true,
		loadMask:          true,
		stripeRows:        true,
		autoHeight:        true,
		store:             storeTotales,
		columns:           [
			{
				width:     600,
				header:    'Descripci�n',
				dataIndex: 'DESCRIPCION',
				align:     'right',
				sortable:  true,
				resizable: false

			},{
				width:     250,
				header:    'Cantidad',
				dataIndex: 'TOTAL',
				align:     'left',
				renderer:  function (valor, columna, registro){
					if(registro.data['DESCRIPCION']=='Total Reembolsos'){
						return valor;
					}else{
						return Ext.util.Format.number(valor,'$0,0.00');
					}
					
				}
				//renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		dockedItems: [{
			weight: 2,
			xtype: 'toolbar',
			dock: 'bottom',
			items: ['->',
			{
				xtype:   'button',
				text:    'Imprimir PDF',
				id:      'btnImprimirAcuse',
				iconCls: 'icoPdf',
				hidden:  true,
				handler: function(boton, evento) {
					boton.setIconCls('loading-indicator');
					var selected = [];
					storeReembolsoPreacuse.each(function(record){
						selected.push(record.data);
					});
					
					Ext.Ajax.request({
						url: '33fondojraltareembolsos_ext.data.jsp',
						params: Ext.apply({  
							informacion:     'generarPDF',
							tipo_archivo:    'ACUSE',
							registros: Ext.encode(selected),
							total_reembolsos:  Ext.getCmp('total_amortizaciones_id').getValue(),
							monto_reembolsos: Ext.getCmp('monto_total_amortizaciones_id').getValue(),
							fecha_carga:     Ext.getCmp('fecha_carga_id').getValue(),
							hora_carga:      Ext.getCmp('hora_carga_id').getValue(),
							folio_acuse:     Ext.getCmp('folio_acuse_id').getValue(),
							folio_carga:     Ext.getCmp('numero_acuse_id').getValue(),
							nombre_usuario:  Ext.getCmp('usuario_id').getValue()
						}),
						callback: fnBtnDescargaArchivo
					});
				}
			},{
				xtype:   'button',
				text:    'Generar Archivo',
				id:      'btnGeneraArchivoAcuse',
				iconCls: 'icoXls',
				hidden:  true,
				handler: function(boton, evento) {
					boton.setIconCls('loading-indicator');
					var selected = [];
					storeReembolsoPreacuse.each(function(record){
						selected.push(record.data);
					});
					
					Ext.Ajax.request({
						url: '33fondojraltareembolsos_ext.data.jsp',
						params: Ext.apply({  
							informacion:     'generarCsvAcuse',
							registros: Ext.encode(selected),
							total_reembolsos:  Ext.getCmp('total_amortizaciones_id').getValue(),
							monto_reembolsos: Ext.getCmp('monto_total_amortizaciones_id').getValue(),
							fecha_carga:     Ext.getCmp('fecha_carga_id').getValue(),
							hora_carga:      Ext.getCmp('hora_carga_id').getValue(),
							folio_acuse:     Ext.getCmp('folio_acuse_id').getValue(),
							folio_carga:     Ext.getCmp('numero_acuse_id').getValue(),
							nombre_usuario:  Ext.getCmp('usuario_id').getValue()
						}),
						callback: fnBtnDescargaArchivo
					});
				}
			},{
				xtype:    'button',
				text:     'Salir',
				id:       'btnSalirAcuse',
				iconCls:  'icoCancelar',
				hidden:   true,
				handler: function(boton, evento) {
					window.location= '33fondojraltareembolsos_ext.jsp';
				}
			}]
		}
		]
		
	});
	
//------------------------------------------------------------------------FORMAS
	var fpCriteriosBusqueda = Ext.create('Ext.form.Panel',{
		name: 'fpCriteriosBusqueda',
		id: 'fpCriteriosBusqueda1',
		frame: true,
		width: 400,
		style: 'margin: 0 auto;',
		title: 'Alta de Reembolsos con Amortizaciones no pagadas',
		items: [
			{
				xtype: 'box',
				html: 'Programa: '+hidDescProgFondoJR,
				height: 30
			},
			{
				xtype: 'textfield',
				name: 'igPrestamo',
				allowBlank: false,
				width: '90%',
				maxLength:  12,
				maskRe: /^\d+$/,
				regex: /^\d+$/,
				fieldLabel: 'No. Pr�stamo'
			}
		],
		monitorValid: true,
		buttons: [
			{
				name: 'btnBuscar',
				id: 'btnBuscar1',
				text: 'Buscar',
				formBind: true,
				handler: fnBtnBuscar
			},
			{
				name: 'btnLimpiar',
				id: 'btnLimpiar1',
				text: 'Limpiar',
				handler: function(btn){
					//window.location = '33fondojraltareembolsos_ext.jsp';
					fpCriteriosBusqueda.getForm().reset();
					gpReembolsos.hide();
				}
			}
		]
		
	});
	
	var formaAcuse =  Ext.create('Ext.form.Panel',{
		width:          600,
		labelWidth:     200,
		id:             'formaAcuse',
		title:          'Acuse Reembolsos',
		layout:         'form',
		style:          'margin:0 auto;',
		frame:          true,
		border:         true,
		hidden:         true,
		defaults : {
			labelWidth : 270
		},
		items: [{
			xtype: 'box',
			width:      580,
			id:         'display1_id',
			//frame:      true,
			//border:     false,
			hidden:     false,
			html:       '<div align="center"><br><b> La autentificaci�n se llev� a cabo con �xito. </b><br>&nbsp;</div>'
		},{
			anchor:      '100%',
			xtype:      'textfield', 
			id:         'folio_acuse_id',
			name:       'folio_acuse',
			readOnly:	true,
			//labelAlign: 'right',
			fieldLabel: '&nbsp;&nbsp; Recibo'
		},{
			anchor:      '100%',
			xtype:      'textfield', 
			id:         'total_amortizaciones_id',
			name:       'total_amortizaciones',
			readOnly:	true,
			//labelAlign: 'right',
			fieldLabel: '&nbsp;&nbsp; No. Total de Amortizaciones Reembolsadas'
		},{
			anchor:      '100%',
			xtype:      'textfield', 
			id:         'monto_total_amortizaciones_id',
			name:       'monto_total_amortizaciones',
			readOnly:	true,
			//labelAlign: 'right',
			fieldLabel: '&nbsp;&nbsp; Monto Total de Amortizaciones Reembolsadas'
		},{
			anchor:      '100%',
			xtype:      'textfield', 
			id:         'numero_acuse_id',
			name:       'numero_acuse',
			readOnly:	true,
			//labelAlign: 'right',
			fieldLabel: '&nbsp;&nbsp; N�mero de Acuse'
		},{
			anchor:      '100%',
			xtype:      'textfield', 
			id:         'fecha_carga_id',
			name:       'fecha_carga',
			readOnly:	true,
			//labelAlign: 'right',
			fieldLabel: '&nbsp;&nbsp; Fecha de Carga'
		},{
			anchor:      '100%',
			xtype:      'textfield', 
			id:         'hora_carga_id',
			name:       'hora_carga',
			readOnly:	true,
			//labelAlign: 'right',
			fieldLabel: '&nbsp;&nbsp; Hora de carga'
		},{
			anchor:      '100%',
			xtype:      'textfield', 
			id:         'usuario_id',
			name:       'usuario',
			readOnly:	true,
			//labelAlign: 'right',
			fieldLabel: '&nbsp;&nbsp; Usuario'
		},{
			xtype: 'box',
			width:      580,
			id:         'display2_id',
			frame:      false,
			border:     false,
			html:       '<div align="justify"><br><b> Al transmitirse este MENSAJE DE DATOS, queda bajo su responsabilidad el movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisi�n tendr� validez para todos los efectos legales. </b><br>&nbsp;</div>'
		},{
			xtype: 'box',
			width:      580,
			id:         'display3_id',
			frame:      false,
			border:     false,
			hidden:     false,
			html:       '<div align="center"><br><b> La autentificaci�n no se llev� a cabo. <br> PROCESO CANCELADO </b><br>&nbsp;</div>'
		}],
		buttons: [{
			id:      'btnRegresar',
			text:    'Regresar',
			iconCls: 'icoCancelar',
			hidden:  false,
			handler: function(boton, evento){
				window.location = '33fondojraltareembolsos_ext.jsp';
			}
		}]
	});
//------------------------------------------------------------------------Panels
var formaPreacuse = Ext.create('Ext.form.Panel',{
		width:              500,
		id:                 'formaPreacuse1',
		title:              'Preacuse Reembolsos',
		layout:             'form',
		style:              'margin:0 auto;',
		frame:              false,
		border:             true,
		hidden:             true,
		items: [{
			width:          500,
			xtype:          'panel',  
			id:             'seccion_1', 
			layout:         'table', 
			border:         true, 
			layoutConfig:   { columns:2 },
			defaults:       {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{
					width:  400,
					frame:  false,
					border: false,
					html:   '<div align="left"><br> No. Total de Amortizaciones a Reembolsar: <br></div>'	
				},{
					width:  100, 
					xtype:  'displayfield', 
					id:     'total_reembolsos_id',
					name:   'total_reembolsos'
				}
			]
		},{
			width:          500,
			xtype:          'panel',  
			id:             'seccion_2', 
			layout:         'table', 
			border:         true, 
			layoutConfig:   { columns:2 },
			defaults:       {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{
					width:  400,
					frame:  false,
					border: false,
					html:   '<div align="left"><br> Monto Total de Amortizaciones a Reembolsar: <br></div>'	
				},{
					width:  100, 
					xtype:  'displayfield',
					id:     'monto_reembolsos_id',
					name:   'monto_reembolsos'
				}
			]
		},{
			width:          500,
			xtype:          'panel',  
			id:             'seccion_3', 
			layout:         'table', 
			border:         true, 
			layoutConfig:   { columns: 1 },
			defaults:       {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{
					width:  500,
					frame:  false,
					border: false,
					html:   '<div align="left"><br> Al transmitirse este MENSAJE DE DATOS, queda bajo su responsabilidad el movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisi�n tendr� validez para todos los efectos legales. <br>&nbsp;</div>'	
				}
			]
		}],
		buttons: [{
			id:             'btnImprimirPreAcuse',
			text:           'Generar Archivo',
			iconCls:        'icoPdf',
			handler:        function(boton, evento){
				boton.setIconCls('loading-indicator');
				var selected = [];
				storeReembolsoPreacuse.each(function(record){
					selected.push(record.data);
				});
				Ext.Ajax.request({
					url: '33fondojraltareembolsos_ext.data.jsp',
					params: Ext.apply({  
						informacion:     'generarPDF',
						tipo_archivo:    'PREACUSE',
						registros: Ext.encode(selected),
						total_reembolsos:  Ext.getCmp('total_reembolsos_id').getValue(),
						monto_reembolsos:  Ext.getCmp('monto_reembolsos_id').getValue() 
					}),
					callback: fnBtnDescargaArchivo
				});
				
			}
		},{
			id:      'btnTransReembolsos',
			text:    'Transmitir Reembolsos',
			iconCls: 'modificar',
			handler: function(boton, evento){
				transmitirReembolsos();
			}
		},{
			id:      'btnCancelarPreacuse',
			text:    'Cancelar',
			iconCls: 'icoCancelar',
			handler: function(boton, evento){
				window.location= '33fondojraltareembolsos_ext.jsp'
			}
		}]
	});
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se
//simula con el siguiente contenedor:
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(20),
			fpCriteriosBusqueda,
			formaPreacuse,
			formaAcuse,
			NE.util.getEspaciador(20),
			gpReembolsos,
			gpPreacuse,
			gpTotales
			
		]
	});
	
var maskRegenerar = new Ext.LoadMask(Ext.getCmp("gpReembolsos1"), {msg:"No se encontraron amortizaciones..."});
});