<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>
<%
	
	String cboEpo = (request.getParameter("cboEpo")==null)?"":request.getParameter("cboEpo");
	String estatus_cred = (request.getParameter("estatus_cred")==null)?"":request.getParameter("estatus_cred");
	String fec_venc = (request.getParameter("fec_venc")==null)?"":request.getParameter("fec_venc");
	String fec_Actual = fHora.format(new java.util.Date());

%>

<html> 
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%@ include file="/00utils/componente_firma.jspf" %>

<script type="text/javascript" src="/nafin/33fondojr/33pki/33nafin/33fondojrmonitorval_ext.js"></script>
<script type="text/javascript" src="/nafin/00utils/extjs/ux/GroupSummary.js"></script>
<link rel="stylesheet" href="/nafin/00utils/extjs/ux/GroupSummary.css">

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
	<!-- Valores iniciales recibidos como parametros-->
<input type="hidden" id="hidDescProgFondoJR" value="<%=descProgramaFondoJR%>">
<input type="hidden" id="hidCvePrograma" value="<%=cveProgramaFondoJR%>">
<input type="hidden" id="hidStrUsuario" value="<%=strTipoUsuario%>">
<input type="hidden" id="hidNombreUsuario" value="<%=strNombreUsuario%>">
<input type="hidden" id="hidFecActual" value="<%=fec_Actual%>">
<input type="hidden" id="estatus" value="<%=estatus_cred%>">
<input type="hidden" id="fec_venc" value="<%=fec_venc%>">

</body>

</html> 