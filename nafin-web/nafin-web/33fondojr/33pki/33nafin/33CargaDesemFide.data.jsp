<%@ page import=
	"java.util.*, java.sql.*, 
	netropology.utilerias.*, 
	com.netro.exception.*, 
	java.text.SimpleDateFormat,
	org.apache.commons.logging.Log,
	java.util.Date,	
	netropology.utilerias.Registros,
	net.sf.json.*,	
	com.netro.fondojr.*"%>
<%@ page contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String clavePrograma = (String) session.getAttribute("cveProgramaFondoJR");
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

String nombreArchivo = (request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";
String numDesembolsos = (request.getParameter("numDesembolsos")!=null)?request.getParameter("numDesembolsos"):"";
String fechahonrada = (request.getParameter("fechahonrada")!=null)?request.getParameter("fechahonrada"):"";

String  rutaArchivoDesembolso= strDirectorioTemp+ nombreArchivo;

String fecha	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String hora = (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());	
String fechaHora  =  fecha + " "+hora;

String nombreUsuario = iNoUsuario+" - "+strNombreUsuario;
String noTotalDesembolsos = (request.getParameter("noTotalDesembolsos")!=null)?request.getParameter("noTotalDesembolsos"):"";
String totalFondoNafin = (request.getParameter("totalFondoNafin")!=null)?request.getParameter("totalFondoNafin"):"";
String totalFondoJR = (request.getParameter("totalFondoJR")!=null)?request.getParameter("totalFondoJR"):"";
String totalAdeudo = (request.getParameter("totalAdeudo")!=null)?request.getParameter("totalAdeudo"):"";
String totalDesembolso = (request.getParameter("totalDesembolso")!=null)?request.getParameter("totalDesembolso"):"";
String totalSolicitadoFIDE = (request.getParameter("totalSolicitadoFIDE")!=null)?request.getParameter("totalSolicitadoFIDE"):"";
String _acuse = (request.getParameter("_acuse")!=null)?request.getParameter("_acuse"):"";
String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
String externContent = (request.getParameter("textoFirmado")==null)?"":request.getParameter("textoFirmado");
String folioCert = (request.getParameter("folioCert")==null)?"":request.getParameter("folioCert");
String nombreArchivoError = (request.getParameter("nombreArchivoError")==null)?"":request.getParameter("nombreArchivoError");
String nombreArchivoAcuse = (request.getParameter("nombreArchivoAcuse")==null)?"":request.getParameter("nombreArchivoAcuse");
String ic_desembolso = (request.getParameter("ic_desembolso")==null)?"":request.getParameter("ic_desembolso");

String flagThread = (request.getParameter("flagThread")!=null)?request.getParameter("flagThread"):"";
String numReg = (request.getParameter("numReg")!=null)?request.getParameter("numReg"):"";
String numProcReg = (request.getParameter("numProcReg")!=null)?request.getParameter("numProcReg"):"";
String porcentaje = (request.getParameter("porcentaje")!=null)?request.getParameter("porcentaje"):"";
if (strLogo.equals("") )  { strLogo= "nafin.gif" ;   }
					
String  mensajeError  = "", mensajeExito ="";
StringBuffer mensajePreAcuse = new StringBuffer();

List parametros  =  new ArrayList();
String infoRegresar = "";
List datosError =  new ArrayList();
List datosOK =  new ArrayList();
List cifras =  new ArrayList();
List cifrasControl  =  new ArrayList();
List resultados  =  new ArrayList();


JSONArray jsonArrErr = new JSONArray();
JSONArray jsonArrOK = new JSONArray();
JSONArray jsonCifrasDatos = new JSONArray();
JSONArray jsonCifrasControl = new JSONArray();

int numCorrectos =0,  numErrores = 0, numCifrasDatos = 0, numCifrasControl =0; 
boolean errorCorreo = true;
String mensajeCorreo = "";

HashMap	mapa=new HashMap();
List datos = new ArrayList();	
JSONObject jsonObj = new JSONObject();


//CargaDesembolsosFondoJrThread  carga  = new  CargaDesembolsosFondoJrThread ();


FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

if(informacion.equals("Inicializar") )  {

	ic_desembolso =  fondoJunior.getIc_Desembolso(); // numero de desembolso id´s 
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("ic_desembolso", ic_desembolso);	
	infoRegresar  = jsonObj.toString();			
		
} else  if(informacion.equals("ValidarArchivo") )  {
	
try  {
	
		resultados =	fondoJunior.getValidarArchivo(strDirectorioTemp, nombreArchivo,  Integer.parseInt(numDesembolsos)  , ic_desembolso, fechahonrada );
	
		if(resultados.size()>0){  
			datosError = (ArrayList)resultados.get(0);	 
			datosOK = (ArrayList)resultados.get(1);	
			cifras = (ArrayList)resultados.get(2);		
			cifrasControl = (ArrayList)resultados.get(3);	
		}	
			
		// Para el Store de storeErrorData	
		if(datosError.size()>0){
			numErrores =datosError.size();
			datos = new ArrayList();
			for(int x=0; x<datosError.size(); x++) {	 
				List infor = (ArrayList)datosError.get(x);			
				for(int y=0; y<infor.size(); y++) {
					mapa=new HashMap();
					mapa.put("DESCRIPCION",(String)infor.get(y));				
					datos.add(mapa);
				}
			}	
			jsonArrErr = JSONArray.fromObject(datos);
		}
		
		// Para el Store de storeErrorCifrasData		
		if(cifras.size()>0){
			numCifrasDatos =cifras.size();
			datos = new ArrayList();		
			for(int x=0; x<cifras.size(); x++) {
				mapa=new HashMap();
				mapa.put("DESCRIPCION",(String)cifras.get(x));				
				datos.add(mapa);
			}				
			jsonCifrasDatos = JSONArray.fromObject(datos);
		}
				
		// Para el Store de storeBienData		
		if(datosOK.size()>0){
			numCorrectos =datosOK.size();
			datos = new ArrayList();
			for(int x=0; x<datosOK.size(); x++) {
				mapa=new HashMap();
				mapa.put("NUMERO_DESEMBOLSO",(String)datosOK.get(x));				
				datos.add(mapa);
			}							
			jsonArrOK = JSONArray.fromObject(datos);
		}
		
		// para formar el archivo de Errores
		if(datosError.size()>0 ||  cifras.size()>0  ) {
		 nombreArchivoError = fondoJunior.archivoErrores(datosError, cifras,  strDirectorioTemp );
		}
		
		//***Para el PreAcuse  y Acuse    Cifras de Control 
		numCifrasControl = cifrasControl.size();
		if(numCifrasControl>0 )  {		
			jsonCifrasControl = JSONArray.fromObject(cifrasControl);
			mensajePreAcuse.append("<table align='center'  style='text-align: center;' border='0' width='500' cellpadding='0' cellspacing='0'> "+
					" <tr> <td align='justify' class='formas'>  <b> Cifras de Control   </td></tr><table> <br>");				
		}	
	
	} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		
	}



} else if(informacion.equals("ArchivoErrores") )  {

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoError);	
	infoRegresar  = jsonObj.toString();		
	
	
} else if(informacion.equals("TransmitirRegistros") )  {

	Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");	
	char getReceipt = 'Y';
	
	totalFondoNafin.replaceAll(",","");
	totalFondoJR.replaceAll(",","");
	totalAdeudo.replaceAll(",","");
	totalDesembolso.replaceAll(",","");
	totalSolicitadoFIDE.replaceAll(",",""); 
	
	try  {
	
		
		if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
			folioCert = acuse.toString();			
			Seguridad s = new Seguridad();
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			
				_acuse = s.getAcuse();
		
				parametros  =  new ArrayList();
				parametros.add(strDirectorioTemp);
				parametros.add(iNoCliente);
				parametros.add(strNombre);
				parametros.add(strNombreUsuario);
				parametros.add(strLogo);
				parametros.add(strDirectorioPublicacion);	
				parametros.add(strPerfil);	
				parametros.add(_acuse);		
				parametros.add(noTotalDesembolsos);	
				parametros.add(totalFondoNafin);	
				parametros.add(totalFondoJR);	
				parametros.add(totalAdeudo);	
				parametros.add(totalDesembolso);	
				parametros.add(totalSolicitadoFIDE);	
				parametros.add(fecha);	
				parametros.add(hora);	
				parametros.add(nombreUsuario);
				parametros.add((String)session.getAttribute("strPais"));
				parametros.add(fechahonrada);
				
							
				nombreArchivoAcuse = fondoJunior.generarPDFAcuseDesembolso(parametros);  //Genera Archivo PDF
				
				parametros  =  new ArrayList();
				parametros.add(_acuse);
				parametros.add(nombreUsuario);
				parametros.add(folioCert);
				parametros.add(noTotalDesembolsos);	
				parametros.add(totalFondoNafin);	
				parametros.add(totalFondoJR);	
				parametros.add(totalAdeudo);	
				parametros.add(totalDesembolso);	
				parametros.add(totalSolicitadoFIDE);	
				parametros.add(fecha);	
				parametros.add(hora);
				parametros.add(rutaArchivoDesembolso);
				parametros.add( strDirectorioTemp +nombreArchivoAcuse);	
				parametros.add(ic_desembolso);
				parametros.add(fechahonrada);
				
				errorCorreo =  fondoJunior.guardarAcuseyArchivo( parametros );   // Inserta Acuse 
				if(errorCorreo==false)  {
					mensajeCorreo = "El correo no ha sido enviado debido a que no se encuentran parametrizados los correos en la ruta siguiente: Fondo Junior – Desembolsos FIDE – Parametrización Correo.";					
				}
				mensajePreAcuse.append("<table align='center'  style='text-align: center;' border='0' width='500' cellpadding='0' cellspacing='0'> "+
						" <tr> <td align='justify' class='formas'>  <b> Su Solicitud  fue recibida con el número de folio: "+_acuse +"</td></tr> "+
						" <tr> <td align='justify' class='formas'>  <br>  <b> Cifras de Control   <br> </td></tr> <br>");					
	
	
			mapa=new HashMap();
			mapa=new HashMap();	mapa.put("CAMPO1","Operación ");	 						mapa.put("CAMPO2","Desembolsos FIDE ");  mapa.put("CAMPO3","TEXTO"); 	cifrasControl.add(mapa);
			mapa=new HashMap();	mapa.put("CAMPO1","Fecha Honrada ");	 				mapa.put("CAMPO2",fechahonrada); 		  mapa.put("CAMPO3","TEXTO"); 	cifrasControl.add(mapa);			
			mapa=new HashMap();	mapa.put("CAMPO1","No. Total de desembolsos ");	 	mapa.put("CAMPO2",noTotalDesembolsos);   mapa.put("CAMPO3","TEXTO"); 	cifrasControl.add(mapa);
			mapa=new HashMap();	mapa.put("CAMPO1","Total Fondo Nafin ");	 			mapa.put("CAMPO2",totalFondoNafin );     mapa.put("CAMPO3","MONTO"); 	cifrasControl.add(mapa);
			mapa=new HashMap();	mapa.put("CAMPO1","Total Fondo JR ");	 				mapa.put("CAMPO2",totalFondoJR );		  mapa.put("CAMPO3","MONTO"); 	cifrasControl.add(mapa);
			mapa=new HashMap();	mapa.put("CAMPO1","Total Adeudo ");	 					mapa.put("CAMPO2",totalAdeudo );			  mapa.put("CAMPO3","MONTO"); 	cifrasControl.add(mapa);
			mapa=new HashMap();	mapa.put("CAMPO1","Total Desembolso ");	 			mapa.put("CAMPO2",totalDesembolso );	  mapa.put("CAMPO3","MONTO"); 	cifrasControl.add(mapa);
			mapa=new HashMap();	mapa.put("CAMPO1","Total Solicitado FIDE ");	 		mapa.put("CAMPO2",totalSolicitadoFIDE ); mapa.put("CAMPO3","MONTO"); 	cifrasControl.add(mapa);								
			mapa=new HashMap();	mapa.put("CAMPO1","Fecha de Carga ");	 				mapa.put("CAMPO2",fecha );					  mapa.put("CAMPO3","TEXTO"); 	cifrasControl.add(mapa);								
			mapa=new HashMap();	mapa.put("CAMPO1","Hora de Carga");	 					mapa.put("CAMPO2",hora );					  mapa.put("CAMPO3","TEXTO"); 	cifrasControl.add(mapa);								
			mapa=new HashMap();	mapa.put("CAMPO1","Usuario ");	 						mapa.put("CAMPO2",nombreUsuario );	     mapa.put("CAMPO3","TEXTO");    cifrasControl.add(mapa);								
			
			jsonCifrasControl = JSONArray.fromObject(cifrasControl);
			
			
			}else  {
				mensajeError = "<br>La autentificación no se llevo acabo con éxito"+s.mostrarError()+"<br>";
			}
		}else  {
			mensajeError = " <br>La autentificación no se llevo acabo con éxito"+"<br>";
		}

	} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
	}

	
	
} else if(informacion.equals("ImprimirAcuse") )  {

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoAcuse);	
	infoRegresar  = jsonObj.toString();			
		

} else if(informacion.equals("borraTemporales") )  {

	fondoJunior.borraTemporales(ic_desembolso );
	
	jsonObj.put("success", new Boolean(true));	
	infoRegresar  = jsonObj.toString();			

}



%>

<%if (informacion.equals("ValidarArchivo")   || informacion.equals("TransmitirRegistros")      ){ %>

 {
	 "success": true,
	 
	 "todOK":{
	 "registros":<%=jsonArrOK.toString()%>,
	 "total":<%=numCorrectos%>},
	 
	 "todError":{
	 "registros":<%=jsonArrErr.toString()%>,
	 "total":<%=numErrores%>},
	
	
	"todCifrasDatos":{
	 "registros":<%=jsonCifrasDatos.toString()%>,
	 "total":<%=numCifrasDatos%>},
	 
	 "todCifrasControl":{
	 "registros":<%=jsonCifrasControl.toString()%>,
	 "total":<%=numCifrasControl%>},
	 
	 
	 "numCorrectos":"<%=numCorrectos%>",
	 "numErrores":"<%=numErrores%>",
	 "numCifrasDatos":"<%=numCifrasDatos%>",
	 "nombreArchivoError":"<%=nombreArchivoError%>",
	 "mensajePreAcuse":"<%=mensajePreAcuse%>",
	 "mensajeError":"<%=mensajeError%>",
	"mensajeExito":"<%=mensajeExito%>",
	"nombreArchivoAcuse":"<%=nombreArchivoAcuse%>",
	"ic_desembolso":"<%=ic_desembolso%>",
	"mensajeCorreo":"<%=mensajeCorreo%>",
	"numReg":"<%=numReg%>"	,
	"numProcReg":"<%=numProcReg%>",	
	"porcentaje":"<%=porcentaje%>",	
	"flagThread":"<%=flagThread%>"	
	

 }
 
  
 <%}else { %>
 <%=infoRegresar%>
 
 <%}%>


