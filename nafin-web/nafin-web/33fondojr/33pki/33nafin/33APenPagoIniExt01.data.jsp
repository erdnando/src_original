<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalgoAfianzadora,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fondojr.*,
		com.netro.fondosjr.*,
		java.math.BigDecimal,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>
<%

	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
	String numeroPrestamo  = request.getParameter("numPrestamo")==null?"":request.getParameter("numPrestamo");
	String diaMinimo = (request.getParameter("diaMinimo")!=null)?request.getParameter("diaMinimo"):"";
	String diaMaximo = (request.getParameter("diaMaximo")!=null)?request.getParameter("diaMaximo"):"";
	String clavePrograma = (String) session.getAttribute("cveProgramaFondoJR");
	boolean  	hayDatos			= false;
	String infoRegresar = "",  consulta = "";
	JSONObject jsonObj = new JSONObject();
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
	ConsultaAmortizacionesPendientesPago paginador = new ConsultaAmortizacionesPendientesPago();
	numeroPrestamo  = (numeroPrestamo != null && !numeroPrestamo.trim().equals(""))?numeroPrestamo.replaceAll("\r\n","\n"):numeroPrestamo;
	ArrayList listaNumerosPrestamo = (numeroPrestamo != null && !numeroPrestamo.trim().equals(""))?Comunes.stringToArrayList(numeroPrestamo,"\n"):new ArrayList();
	if( listaNumerosPrestamo.size() > 0 ){
		for(int i=0;i<listaNumerosPrestamo.size();i++){
			String numero = (String)listaNumerosPrestamo.get(i);
			if(!Comunes.esNumero(numero)){
				listaNumerosPrestamo.set(i,"-1");
			}
		}
	}		
	paginador.setDiaMinimo(diaMinimo);
	paginador.setDiaMaximo(diaMaximo);
	paginador.setNumeroPrestamo(listaNumerosPrestamo);
	paginador.setClavePrograma(clavePrograma);//consTotales
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	if(informacion.equals("Consultar") ||informacion.equals("Generar") ||informacion.equals("consTotales") ||informacion.equals("GenerarArchivoCSV")||informacion.equals("ArchivoPDF")) {
		
		int start = 0;
		int limit = 0;
		Registros registrosC =null;
		if (informacion.equals("Consultar")  )  {
			
			BigDecimal totalPaginaCapital 	 = new BigDecimal("0.00");
			BigDecimal totalPaginaIntereses 	 = new BigDecimal("0.00");
			BigDecimal totalPaginaTotalAPagar = new BigDecimal("0.00");
			JSONObject 	resultado	= new JSONObject();
			HashMap catalogoOrigenPrograma 		= fondoJunior.getCatalogoOrigenPrograma();
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));				
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			try {
				if (operacion.equals("Generar")) {
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				Registros registros = queryHelper.getPageResultSet(request,start,limit);
				while(registros.next()){
					totalPaginaCapital 	= totalPaginaCapital.add(     new BigDecimal(registros.getString("CAPITAL"))		);
					totalPaginaIntereses 	= totalPaginaIntereses.add(   new BigDecimal(registros.getString("INTERESES"))		);
					totalPaginaTotalAPagar 	= totalPaginaTotalAPagar.add( new BigDecimal(registros.getString("TOTAL_A_PAGAR"))	);
					String origen			  = (registros.getString("ORIGEN") == 	null) ? "" : registros.getString("ORIGEN");
					String auxOrigen = fondoJunior.getDescripcionOrigenPrograma(origen,catalogoOrigenPrograma);
					registros.setObject("ORIGEN",auxOrigen);
				}
				String consulta2	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
				resultado = JSONObject.fromObject(consulta2);
				resultado.put("CAPITAL_TOTAL",       totalPaginaCapital.toPlainString()     );
				resultado.put("INTERESES_TOTAL",     totalPaginaIntereses.toPlainString()   );
				resultado.put("TOTAL_A_PAGAR_TOTAL", totalPaginaTotalAPagar.toPlainString() );
				infoRegresar = resultado.toString();
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		}else  if(informacion.equals("consTotales") ) {
			
			consulta = queryHelper.getJSONResultCount(request); 
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("GenerarArchivoCSV")){
			
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}else if (informacion.equals("ArchivoPDF")	){
		
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");

				JSONObject jsonObj3 = new JSONObject();
				jsonObj3.put("success", new Boolean(true));
				jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj3.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
	}

System.out.println("infoRegresar = " + infoRegresar);

%>
<%=infoRegresar%>