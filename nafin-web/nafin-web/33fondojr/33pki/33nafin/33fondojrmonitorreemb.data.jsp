<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.text.SimpleDateFormat,		
		netropology.utilerias.*,	
		javax.naming.*,
		com.netro.fondosjr.*,	
		com.netro.fondojr.*,			
		 java.util.Map.*,	
		 java.net.*, 
		 java.text.*,		
		 com.netro.exception.*,
		 net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/33fondojr/33secsession_extjs.jspf"%>
<%@ include file="../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String fec_venc_ini = (request.getParameter("fec_venc_ini")!=null)?request.getParameter("fec_venc_ini"):"";
String fec_venc_fin = (request.getParameter("fec_venc_fin")!=null)?request.getParameter("fec_venc_fin"):"";
String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");
String estatus = (request.getParameter("estatus")!=null)?request.getParameter("estatus"):"";
String fec_venc = (request.getParameter("fec_venc")!=null)?request.getParameter("fec_venc"):"";
String infoRegresar ="", consulta ="";
 
int start = 0, limit = 0;
JSONObject jsonObj = new JSONObject();
List registros	= new ArrayList();
List datos	= new ArrayList();	
HashMap datosExt = new HashMap();
JSONArray registrosExt = new JSONArray();


FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

if(informacion.equals("valoresIniciales")) {

String programa = "</table align='center'>"+
						" <tr> "+
						"<td class='formas' colspan='1' align='left'><b>Programa: "+descProgramaFondoJR+"</b></td>"+
						"</tr>"+
						"</table>";
				
jsonObj.put("success", new Boolean(true));
jsonObj.put("programa", programa);	
infoRegresar =jsonObj.toString();

} else if(informacion.equals("Consultar") || informacion.equals("ArchivoCSV")  ) {

	ConsultaMonitorReembFJR paginador = new ConsultaMonitorReembFJR();
	paginador.setCvePrograma(clavePrograma);
	paginador.setDescPrograma(descProgramaFondoJR);
	paginador.setFec_venc_ini(fec_venc_ini);
	paginador.setFec_venc_fin(fec_venc_fin);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	if (informacion.equals("Consultar")  ) {	
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}		
		
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);	
			}
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	
	
	} else if (informacion.equals("ArchivoCSV") ) {	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
		infoRegresar = jsonObj.toString();		
		
	}	
	
}else if (informacion.equals("ConsultaDetalle")  ) {
	
	registros = fondoJunior.consDetMonitor(estatus ,fec_venc,clavePrograma);
	for(int x=0; x<registros.size();x++){
		datos = (List)registros.get(x);		
		String fechaVen=!datos.get(0).equals("")?(String)datos.get(0):"";
		String prestamo = !datos.get(1).equals("")?(String)datos.get(1):"";
		String noCliente =  !datos.get(2).equals("")?(String)datos.get(2):"";
		String montoAmor= !datos.get(4).equals("")?(String)datos.get(4):"";
		String montoInteres=!datos.get(5).equals("")?(String)datos.get(5):"";
		String TotalVen =!datos.get(6).equals("")?(String)datos.get(6):"";
		String FechaPago =!datos.get(7).equals("")?(String)datos.get(7):"";
		String estatus1 =!datos.get(8).equals("")?(String)datos.get(8):"";
		String fechaPerio= !datos.get(9).equals("")?(String)datos.get(9):"";
		String noDocto =!datos.get(10).equals("")?(String)datos.get(10):"";
		String fechaFide =!datos.get(11).equals("")?(String)datos.get(11):"";
					
		datosExt = new HashMap();
		datosExt.put("FECHA_VEN_NA", fechaVen);
		datosExt.put("NO_PRESTAMO", prestamo);
		datosExt.put("NO_CLIENTE", noCliente);
		datosExt.put("MONTO_AMORTIZACIO", montoAmor);
		datosExt.put("MONTO_INTERES", montoInteres);
		datosExt.put("TOTAL_VENC", TotalVen);
		datosExt.put("FECHA_PAGO", FechaPago);
		datosExt.put("ESTATUS", estatus1);
		datosExt.put("FECHA_PERIODO", fechaPerio);
		datosExt.put("NO_DOCTO", noDocto);
		datosExt.put("FECHA_REGISTRO", fechaFide);
		registrosExt.add(datosExt);	
	}
	
	consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();

}

%>

<%=infoRegresar%>
