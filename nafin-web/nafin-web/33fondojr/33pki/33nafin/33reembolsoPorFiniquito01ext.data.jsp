<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject, 
		com.netro.pdf.ComunesPDF,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.fondojr.*,
		com.netro.fondosjr.*,
		java.text.SimpleDateFormat,
		java.math.*,
		netropology.utilerias.usuarios.Usuario"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>
<%@ include file="../certificado.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion     = request.getParameter("informacion")     == null?"":(String)request.getParameter("informacion");
String programa        = request.getParameter("programa")        == null?"":(String)request.getParameter("programa");
String tipo_archivo    = request.getParameter("tipo_archivo")    == null?"":(String)request.getParameter("tipo_archivo"); //Determina si es Preacuse o Acuse
String no_prestamo     = request.getParameter("no_prestamo")     == null?"":(String)request.getParameter("no_prestamo");
String no_prestamo_aux = request.getParameter("no_prestamo_aux") == null?"":(String)request.getParameter("no_prestamo_aux");
String proceso_carga   = request.getParameter("proceso_carga")   == null?"":(String)request.getParameter("proceso_carga");
String tipo_carga      = request.getParameter("tipo_carga")      == null?"":(String)request.getParameter("tipo_carga");
String monto_reembolso = request.getParameter("monto_reembolso") == null?"":(String)request.getParameter("monto_reembolso");
String observaciones   = request.getParameter("observaciones")   == null?"":(String)request.getParameter("observaciones");
String desembolsos_tot = request.getParameter("desembolsos_tot") == null?"":(String)request.getParameter("desembolsos_tot");
String cantidad_total  = request.getParameter("cantidad_total")  == null?"":(String)request.getParameter("cantidad_total");
String disposicion     = request.getParameter("disposicion")     == null?"":(String)request.getParameter("disposicion");
String name_file       = request.getParameter("name_file")       == null?"":(String)request.getParameter("name_file");
/*****Reembolso Finiquito Acuse *****/
String pkcs7           = request.getParameter("pkcs7")           == null?"":(String)request.getParameter("pkcs7");
String externContent   = request.getParameter("externContent")   == null?"":(String)request.getParameter("externContent");
String fecha_carga     = request.getParameter("fecha_carga")     == null?"":(String)request.getParameter("fecha_carga");
String hora_carga      = request.getParameter("hora_carga")      == null?"":(String)request.getParameter("hora_carga");
String folio_carga     = request.getParameter("folio_carga")     == null?"":(String)request.getParameter("folio_carga");
String folio_acuse     = request.getParameter("folio_acuse")     == null?"":(String)request.getParameter("folio_acuse");
String nombre_usuario  = request.getParameter("nombre_usuario")  == null?"":(String)request.getParameter("nombre_usuario");

String cvePrograma   = "";
String nombreArchivo = "";
String consulta      = "";
String mensaje       = "";
String infoRegresar  = "";

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("Inicializacion")){

	JSONObject resultado = new JSONObject();
	no_prestamo_aux = (String)request.getAttribute("numero_prestamo_aux");
	proceso_carga   = (String)request.getAttribute("proceso_carga");
	tipo_carga      = (String)request.getAttribute("tipo_carga");
	//String programa = (String)(request.getSession().getAttribute("descProgramaFondoJR") == null?"":request.getSession().getAttribute("descProgramaFondoJR"));
	
	resultado.put("success",         new Boolean(true)   );
	resultado.put("no_prestamo_aux", no_prestamo_aux     );
	resultado.put("proceso_carga",   proceso_carga       );
	resultado.put("tipo_carga",      tipo_carga          );
	resultado.put("descPrograma",    descProgramaFondoJR );
	infoRegresar = resultado.toString();

} else if(informacion.equals("Datos_Prestamo")){

	HashMap datos         = new HashMap();
	JSONObject resultado  = new JSONObject();
	JSONArray registros   = new JSONArray();
	String datos_prestamo = "N";
	
	cvePrograma = (String)(request.getSession()).getAttribute("cveProgramaFondoJR");//FODEA 026-2010
	//Instancia del EJB
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

	List datosPrestamo = fondoJunior.obtenerDatosPrestamo(no_prestamo, cvePrograma);//FODEA 026-2010

	if(fondoJunior.existeReembolso(no_prestamo, cvePrograma)){
		if(!datosPrestamo.isEmpty()){
			datos.put("fecha_movimiento", (String)datosPrestamo.get(0));
			datos.put("cliente_sirac",    (String)datosPrestamo.get(1));
			datos.put("cliente",          (String)datosPrestamo.get(2));
			datos.put("origen",           (String)datosPrestamo.get(4));
			datos.put("no_prestamo",      no_prestamo                 );
			datos.put("monto_desembolso", ""                          );
			datos_prestamo = "S";
		} else{
			datos_prestamo = "N";
			mensaje = "El Número de Préstamo ya se encuentra registrado";
		}
	} else{
		mensaje = "El Número de Préstamo no existe";
	}

	resultado.put("success",        new Boolean(true) );
	resultado.put("registro",       datos             );
	resultado.put("mensaje",        mensaje           );
	resultado.put("datos_prestamo", datos_prestamo    );
	infoRegresar = resultado.toString();

} else if(informacion.equals("Agrega_Rembolso")){

	String datos_prestamo = "N";
	HashMap datos         = new HashMap();
	JSONObject resultado  = new JSONObject();

	cvePrograma = (String)(request.getSession()).getAttribute("cveProgramaFondoJR");//FODEA 026-2010
	
	//Instanciación para uso del EJB
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

	if(proceso_carga == null || proceso_carga.equals("")){
		proceso_carga = fondoJunior.getProcesoReembolso();
	}

	List datosPrestamo = fondoJunior.obtenerDatosPrestamo(no_prestamo, cvePrograma);//FODEA 026-2010
   if(!datosPrestamo.isEmpty()){
		datos.put("fecha_movimiento", (String)datosPrestamo.get(0) );
		datos.put("cliente_sirac",    (String)datosPrestamo.get(1) );
		datos.put("cliente",          (String)datosPrestamo.get(2) );
		datos.put("origen",           (String)datosPrestamo.get(4) );
		datos.put("no_prestamo",      no_prestamo                  );
		datos.put("proceso_carga",    proceso_carga                );
		datos_prestamo = "S";
	}

	if(monto_reembolso != null || !monto_reembolso.equals("")){
		if(monto_reembolso.indexOf(",") != -1){
			monto_reembolso = monto_reembolso.replaceAll(",", "");
			datos.put("monto_reembolso", 	Comunes.formatoDecimal(monto_reembolso, 2));
		}
	}

	if(!fondoJunior.existeReembolsoFin(no_prestamo, cvePrograma)){

		if(!fondoJunior.existeReembolsoTmp(proceso_carga, no_prestamo)){

			List datos_reembolso = new ArrayList();
			datos_reembolso.add(proceso_carga);
			datos_reembolso.add(no_prestamo);
			datos_reembolso.add(monto_reembolso);
			datos_reembolso.add(observaciones);
			fondoJunior.agregaReembolso(datos_reembolso);

		} else{
			mensaje = "El prestamo ya ha sido agregado.";
		}
	} else{
		mensaje = "El prestamo ya ha sido finiquitado.";
	}

	resultado.put("success",         new Boolean(true));
	resultado.put("registros",       datos            );
	resultado.put("datos_prestamo",  datos_prestamo   );
	//resultado.put("proceso_carga", proceso_carga    );
	resultado.put("tipo_carga",      tipo_carga       );
	resultado.put("mensaje",         mensaje          );
	infoRegresar = resultado.toString();

} else if(informacion.equals("AgregaMod_Rembolso")){

	HashMap datos         = new HashMap();
	JSONObject resultado  = new JSONObject();
	JSONArray registros   = new JSONArray();
	String datos_prestamo = "N";

	cvePrograma = (String)(request.getSession()).getAttribute("cveProgramaFondoJR");//FODEA 026-2010
	//Instancia del EJB
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

	fondoJunior.modificaReembolsoTmp(proceso_carga, no_prestamo_aux, observaciones);
	List datosPrestamo = fondoJunior.obtenerDatosReembolso(proceso_carga, no_prestamo_aux, cvePrograma);//FODEA 026-2010
	if(!datosPrestamo.isEmpty()){
		datos.put("fecha_movimiento", (String)datosPrestamo.get(0));
		//datos.put("no_prestamo",    (String)datosPrestamo.get(1));
		datos.put("cliente_sirac",    (String)datosPrestamo.get(2));
		datos.put("cliente",          (String)datosPrestamo.get(3));
		datos.put("origen",           (String)datosPrestamo.get(5));
		datos.put("observaciones",    (String)datosPrestamo.get(6));
		datos.put("no_prestamo_aux",  ""                          );
		datos.put("proceso_carga",    proceso_carga               );
		datos_prestamo = "S";
	}

	resultado.put("success",         new Boolean(true));
	resultado.put("registros",       datos            );
	resultado.put("datos_prestamo",  datos_prestamo   );
	//resultado.put("proceso_carga", proceso_carga    );
	resultado.put("tipo_carga",      tipo_carga       );
	resultado.put("mensaje",         mensaje          );
	infoRegresar = resultado.toString();

} else if(informacion.equals("Modificar_Rembolso")){

	HashMap datos         = new HashMap();
	JSONObject resultado  = new JSONObject();
	JSONArray registros   = new JSONArray();
	String datos_prestamo = "N";

	cvePrograma = (String)(request.getSession()).getAttribute("cveProgramaFondoJR");//FODEA 026-2010
	//Instancia del EJB
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

	List datosPrestamo = fondoJunior.obtenerDatosReembolso(proceso_carga, no_prestamo_aux, cvePrograma);//FODEA 026-2010
	if(!datosPrestamo.isEmpty()){
		datos.put("fecha_movimiento", (String)datosPrestamo.get(0));
		datos.put("no_prestamo",      (String)datosPrestamo.get(1));
		datos.put("cliente_sirac",    (String)datosPrestamo.get(2));
		datos.put("cliente",          (String)datosPrestamo.get(3));
		datos.put("origen",           (String)datosPrestamo.get(5));
		datos.put("observaciones",    (String)datosPrestamo.get(6));
		datos.put("no_prestamo_aux",  no_prestamo_aux             );
		datos.put("proceso_carga",    proceso_carga               );
		datos_prestamo = "S";
		observaciones = (String)datosPrestamo.get(6);
	}

	resultado.put("success",        new Boolean(true));
	resultado.put("observaciones",  observaciones    );
	resultado.put("registros",      datos            );
	resultado.put("tipo_carga",     tipo_carga       );
	resultado.put("datos_prestamo", datos_prestamo   );
	resultado.put("mensaje",        mensaje          );
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("Consulta_Grid_Carga_Individual")){

	HashMap datos            = new HashMap();
	HashMap datosGrid        = new HashMap();
	JSONObject resultado     = new JSONObject();
	JSONArray registros      = new JSONArray();
	StringBuffer texto_plano = new StringBuffer();
	double total_desembolso	 = 0;

	cvePrograma = (String)(request.getSession()).getAttribute("cveProgramaFondoJR");//FODEA 026-2010

	//Instanciación para uso del EJB
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

	if(tipo_archivo.equals("ACUSE")){

		List reembolsosTemp = fondoJunior.obtenerReembolsosCarga(proceso_carga, cvePrograma, disposicion);
		if(!reembolsosTemp.isEmpty()){
			total_desembolso = 0;
			for( int i = 0; i< reembolsosTemp.size(); i++){
				/*****	Armo la lista para generar el grid	*****/
				List reembolso = (List)reembolsosTemp.get(i);
				total_desembolso += Double.parseDouble((String)reembolso.get(4));
				datosGrid = new HashMap();
				datosGrid.put("FECHA_MOVIMIENTO", reembolso.get(0) );
				datosGrid.put("PRESTAMO",         reembolso.get(1) );
				datosGrid.put("NO_SIRAC",         reembolso.get(2) );
				datosGrid.put("CLIENTE",          reembolso.get(3) );
				datosGrid.put("MONTO_DESEMBOLSO", "$" + Comunes.formatoDecimal(reembolso.get(4), 2));
				datosGrid.put("AMORTIZACION",     reembolso.get(7) );
				datosGrid.put("ORIGEN",           reembolso.get(5) );
				datosGrid.put("OBSERVACIONES",    reembolso.get(6) );
				registros.add(datosGrid);
			}
		}
		String monto = "$"+Comunes.formatoDecimal(total_desembolso,2);
		resultado.put("success",          new Boolean(true)   );
		resultado.put("total_desembolso", monto               );
		resultado.put("registros",        registros.toString());
		resultado.put("total",            ""+registros.size() );
		resultado.put("tipo_archivo",     "ACUSE"             );
		resultado.put("texto_plano",      ""                  );
		resultado.put("disposicion",      ""                  );

	} else{

		List reembolsosTemp = fondoJunior.obtenerReembolsosTemp(proceso_carga, cvePrograma);
		if(!reembolsosTemp.isEmpty()){
			total_desembolso = 0;
			texto_plano.append("Fecha de Movimiento|Prestamo|Número de Cliente SIRAC|Cliente|Monto del Desembolso|No. Amortizacion|Origen|Observaciones\n");
			for( int i = 0; i< reembolsosTemp.size(); i++){

				/*****	Armo la lista para generar el grid	*****/
				List reembolso = (List)reembolsosTemp.get(i);
				total_desembolso += Double.parseDouble((String)reembolso.get(4));
				datosGrid = new HashMap();
				datosGrid.put("FECHA_MOVIMIENTO", reembolso.get(0));
				datosGrid.put("PRESTAMO",         reembolso.get(1));
				datosGrid.put("NO_SIRAC",         reembolso.get(2));
				datosGrid.put("CLIENTE",          reembolso.get(3));
				datosGrid.put("MONTO_DESEMBOLSO", "$" + Comunes.formatoDecimal(reembolso.get(4), 2));
				datosGrid.put("ORIGEN",           reembolso.get(5));
				datosGrid.put("OBSERVACIONES",    reembolso.get(6));
				datosGrid.put("AMORTIZACION",     reembolso.get(7));
				registros.add(datosGrid);

				/*****	Armo el texto plano	*****/
				texto_plano.append(reembolso.get(0) + "|"	);
				texto_plano.append(reembolso.get(1) + "|"	);
				texto_plano.append(reembolso.get(2) + "|"	);
				texto_plano.append(reembolso.get(3) + "|"	);
				texto_plano.append("$" + Comunes.formatoDecimal(reembolso.get(4), 2) + "|"	);
				texto_plano.append(reembolso.get(7) + "|"	);
				texto_plano.append(reembolso.get(5) + "|"	);
				texto_plano.append(reembolso.get(6) + "\n");

				/*****	Armo la variable disposición	*****/
				datos.put(reembolso.get(7), reembolso.get(7));

			}
			Iterator it = datos.entrySet().iterator();
			while (it.hasNext()){
				Map.Entry e = (Map.Entry)it.next();
				disposicion+= e.getValue()+",";
			}
			if(!disposicion.equals("")){
				int tamanio2 =	disposicion.length();
				String valor =  disposicion.substring(tamanio2-1, tamanio2);
				if(valor.equals(",")){
					disposicion =disposicion.substring(0,tamanio2-1);
				}
			}

		}
		String monto = "$"+Comunes.formatoDecimal(total_desembolso,2);
		resultado.put("success",          new Boolean(true)     );
		resultado.put("total_desembolso", monto                 );
		resultado.put("texto_plano",      texto_plano.toString());
		resultado.put("total",            ""+registros.size()   );
		resultado.put("tipo_archivo",     ""                    );
		resultado.put("registros",        registros.toString()  );
		resultado.put("disposicion",      disposicion           );
	}

	infoRegresar = resultado.toString();

} else if(informacion.equals("Consulta_Grid_Total")){

	JSONObject resultado  = new JSONObject();
	String cadena = "[{\"DESCRIPCION\":\"Total Monto por Desembolsos\",\"TOTAL\":\""+cantidad_total+"\"},{\"DESCRIPCION\":\"Total Desembolsos\",\"TOTAL\":\""+desembolsos_tot+"\"}]";

	resultado.put("success",   new Boolean(true) );
	resultado.put("total",     "2"               );
	resultado.put("registros", cadena            );
	infoRegresar = resultado.toString();

} else if(informacion.equals("Imprimir")){

	Registros registros = new Registros();
	JSONObject jsonObj  = new JSONObject();
	String usuario      = "";

	cvePrograma = (String)(request.getSession()).getAttribute("cveProgramaFondoJR");

	//Instanciación para uso del EJB
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
	if(proceso_carga == null || proceso_carga.equals("")){
		proceso_carga = fondoJunior.getProcesoReembolso();
	}

	ReembolsoPorFiniquito paginador = new ReembolsoPorFiniquito();
	if(tipo_archivo.equals("PREACUSE")){

		paginador.setTipoArchivo(tipo_archivo);
		paginador.setStrPrograma(programa);
		paginador.setCvePrograma(cvePrograma);
		paginador.setProcesoCarga(Integer.parseInt(proceso_carga));
		paginador.setTotalDesembolsos(desembolsos_tot);	//$0,000.00
		paginador.setCantidadDesembolsos(Integer.parseInt(cantidad_total)); //lista.size()

	} else if(tipo_archivo.equals("ACUSE")){

		paginador.setTipoArchivo(tipo_archivo);
		paginador.setStrPrograma(programa);
		paginador.setCvePrograma(cvePrograma);
		paginador.setProcesoCarga(Integer.parseInt(proceso_carga));
		paginador.setTotalDesembolsos(desembolsos_tot);	//$0,000.00
		paginador.setCantidadDesembolsos(Integer.parseInt(cantidad_total)); //lista.size()
		paginador.setFechaCarga(fecha_carga);
		paginador.setHoraCarga(hora_carga);
		paginador.setAcuseCarga(folio_acuse);
		paginador.setFolioCarga(folio_carga);
		paginador.setNombreUsuario(nombre_usuario);

	}

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	try {
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo", e);
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Genera_Archivo")){

	Registros registros = new Registros();
	JSONObject jsonObj  = new JSONObject();
	String usuario      = "";

	cvePrograma = (String)(request.getSession()).getAttribute("cveProgramaFondoJR");

	//Instanciación para uso del EJB
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
	if(proceso_carga == null || proceso_carga.equals("")){
		proceso_carga = fondoJunior.getProcesoReembolso();
	}

	ReembolsoPorFiniquito paginador = new ReembolsoPorFiniquito();
	paginador.setTipoArchivo(tipo_archivo);
	paginador.setStrPrograma(programa);
	paginador.setCvePrograma(cvePrograma);
	paginador.setProcesoCarga(Integer.parseInt(proceso_carga));
	paginador.setTotalDesembolsos(desembolsos_tot);	//$0,000.00
	paginador.setCantidadDesembolsos(Integer.parseInt(cantidad_total)); //lista.size()
	paginador.setFechaCarga(fecha_carga);
	paginador.setHoraCarga(hora_carga);
	paginador.setAcuseCarga(folio_acuse);
	paginador.setFolioCarga(folio_carga);
	paginador.setNombreUsuario(nombre_usuario);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	try {
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo", e);
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("ReembolsosPorFiniquitoAcuse")){

	HashMap datos         = new HashMap();
	JSONObject resultado  = new JSONObject();
	JSONArray registros   = new JSONArray();

	Seguridad s = new Seguridad();

	int numReembolsos    = 0;
	boolean errorUsuario = true;
	char getReceipt;
	String folioCert     = "";
	String _acuse        = "";
	String login_usuario = (String)request.getSession().getAttribute("Clave_usuario");
	String name_usuario  = (String)request.getSession().getAttribute("strNombreUsuario");
	cvePrograma          = (String)(request.getSession()).getAttribute("cveProgramaFondoJR");//FODEA 026-2010

	if(monto_reembolso.indexOf(",") != -1){
		monto_reembolso = monto_reembolso.replaceAll(",", "");
	}

	BigDecimal totalReembolsos   = new BigDecimal(monto_reembolso);
	BigDecimal parcialReembolsos = new BigDecimal("0.00");

	if(!_serial.equals("") && externContent!=null && pkcs7!=null){
		errorUsuario = false;
		getReceipt	 = 'Y';

		if(monto_reembolso.indexOf(",") != -1){
			monto_reembolso = monto_reembolso.replaceAll(",", "");
		}

		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formato_cert  = new SimpleDateFormat("ddMMyyHHmmss");
		SimpleDateFormat formato_fecha = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formato_hora  = new SimpleDateFormat("hh:mm aa");

		//Instanciación para uso del EJB
		FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

		folioCert = "RF" + formato_cert.format(calendario.getTime());
		fecha_carga = formato_fecha.format(calendario.getTime());
		hora_carga = formato_hora.format(calendario.getTime());

		if (errorUsuario == false && s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)){
			
			_acuse        = s.getAcuse();
			fondoJunior.guardarReembolsosPorFiniquito(proceso_carga, no_prestamo, totalReembolsos.toPlainString() , login_usuario + " - " + name_usuario, tipo_carga, cvePrograma);

			List reembolsosTemp = fondoJunior.obtenerReembolsosCarga(proceso_carga, cvePrograma, disposicion);//FODEA 026-2010

			for(int i = 0; i < reembolsosTemp.size(); i++){
				List reembolso = (List)reembolsosTemp.get(i);
				parcialReembolsos = parcialReembolsos.add(new BigDecimal((String)reembolso.get(4))).setScale(2, BigDecimal.ROUND_HALF_UP);
				numReembolsos++;
			}
		} else{
			mensaje = "La autentificación no se llevó a cabo.";
		}
	}

	resultado.put("success",           new Boolean(true)                   );
	resultado.put("disposicion",       disposicion                         );
	resultado.put("folio_acuse",       _acuse                              );
	resultado.put("nombre_usuario",    login_usuario + " - " + name_usuario);
	resultado.put("folioCert",         folioCert                           );
	resultado.put("fecha_carga",       fecha_carga                         );
	resultado.put("hora_carga",        hora_carga                          );
	resultado.put("folio_carga",       folioCert                           ); // Es lo mismo que folioCert
	resultado.put("totalReembolsos",   totalReembolsos.toPlainString()     );
	resultado.put("numReembolsos",     Integer.toString(numReembolsos)     );
	resultado.put("parcialReembolsos", parcialReembolsos.toPlainString()   );
	resultado.put("numero_prestamo",   no_prestamo                         );
	resultado.put("proceso_carga",     proceso_carga                       );
	resultado.put("mensaje",           mensaje                             );
	resultado.put("tipo_carga",        "INDIVIDUAL"                        );
	infoRegresar = resultado.toString();

} else if(informacion.equals("Subir_Archivo")){

	JSONObject resultado = new JSONObject();
	boolean 	  success 	= true;
	Map 		  mapa		= null;
	List 		  lDatos 	= null;
	List 		  listaOk	= null;
	List 		  listaFail = null;
	String file_name	 	= "";
	String st_archivo	 	= "";
	String folioBatch	 	= "";
	String cargaBatch	 	= "N";
	String archivo_error = "";
	List reembolsos_cargados = null;

	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType);
	request.setAttribute("myContentType", myContentType);
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(2097152);
		myUpload.upload();
	} catch(Exception e){
		success = false;
		log.warn("SubirArchivo(Exception): Cargar en memoria el contenido del archivo: " + e);
		if(myUpload.getSize() > 2097152){
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 2 MB.");
		} else{
			throw new AppException("Problemas al Subir el Archivo."); 
		}
	}
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest = myUpload.getRequest();
	// Guardar Archivo en Disco
	int numFiles = 0;
	try {
		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		//myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," "));
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," "));
		file_name = myFile.getFileName().replaceAll("\\s+"," ");
		//resultado.put("NOMBRE_ARCHIVO", myFile.getFileName().replaceAll("\\s+"," "));
	}catch(Exception e){
		success = false;
		log.warn("SubirArchivo(Exception): Guardar Archivo en Disco: " + e);
		throw new AppException("Ocurrió un error al guardar el archivo");
	}

	/*****	Si ar archivo se cargó correctamente, se procede a la validación	*****/
	if(success == true){
		String cgUsuario = (String)(request.getSession()).getAttribute("iNoUsuario");
		String nombreUsr = (String)(request.getSession()).getAttribute("strNombreUsuario");

		cvePrograma = (String)(request.getSession()).getAttribute("cveProgramaFondoJR");
		//String rutaArchivo = strDirectorioTemp + nombreArchivo;
		String rutaArchivo = strDirectorioTemp + iNoUsuario + "." + file_name ;
		FileInputStream fstream = new FileInputStream(rutaArchivo);// Se abre el archivo
		DataInputStream entrada = new DataInputStream(fstream);// Creamos el objeto de entrada

		/***** Esta linea es para la carga batch *****/
		FileReader lineas = new FileReader(rutaArchivo);

		ReembolsosPorFiniquitoCargaMasiva carga_masiva = new ReembolsosPorFiniquitoCargaMasiva();
		try{
			reembolsos_cargados = carga_masiva.leeArchivoReembolsosPorFiniquito(entrada);
		} catch(Exception e){
			log.warn("Falló mi intento. " +  e);
		}

		if(reembolsos_cargados.size()>20000){
			mensaje = "Se excedió el número de registros ( > 20000 )";
		} else if(reembolsos_cargados.size()<=1000){

			//PROCESO PARA CARGAS DE ARCHIVOS CON 1000 REGISTROS COMO MAXIMO
			List registros_correctos = new ArrayList();
			StringBuffer correctos	 = new StringBuffer();
			StringBuffer errores		 = new StringBuffer();
			StringBuffer registros_erroneos = new StringBuffer();

			HashMap validacion_reembolsos = carga_masiva.validaReembolsosPorFiniquito(reembolsos_cargados, cvePrograma);

			List resultado_validacion = (List)validacion_reembolsos.get("resultado_validacion");
			String total_reembolsos	  = (String)validacion_reembolsos.get("total_reembolsos");

			for(int i = 0; i < resultado_validacion.size(); i++){
				List validacion_registro = (List)resultado_validacion.get(i);
				correctos.append((StringBuffer)validacion_registro.get(0));
				registros_correctos.add((List)validacion_registro.get(1));
				errores.append((StringBuffer)validacion_registro.get(2));
				registros_erroneos.append((StringBuffer)validacion_registro.get(3));
				no_prestamo = carga_masiva.getNumeroPrestamo();
				monto_reembolso = carga_masiva.getMontoTotalReembolso();
			}

			if(!errores.toString().equals("")){
				CreaArchivo archivo = new CreaArchivo();
				String nombre_archivo = archivo.nombreArchivo();
				archivo.make(registros_erroneos.toString(), strDirectorioTemp, nombre_archivo, ".txt");
				resultado.put("NOMBRE_ARCHIVO", nombre_archivo + ".txt");
			}

			//Instanciación para uso del EJB
			FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

			proceso_carga = fondoJunior.getProcesoReembolso();
			int num_reg_ok = 0;
			for(int i = 0; i < registros_correctos.size(); i++){
				List registro_correcto = (List)registros_correctos.get(i);
				if(!registro_correcto.isEmpty()){
					List datos_reembolso = new ArrayList();
					datos_reembolso.add(proceso_carga);
					datos_reembolso.add(registro_correcto.get(0));
					datos_reembolso.add(registro_correcto.get(2));
					datos_reembolso.add(registro_correcto.get(3));
					fondoJunior.agregaReembolso(datos_reembolso);
					num_reg_ok++;
				}
			}

			if(!errores.toString().equals("")){
				archivo_error = "S";
			}else{
				archivo_error = "N";
			}

			st_archivo = "CARGADO";
			resultado.put("numero_prestamo", no_prestamo                  );
			resultado.put("totalReembolsos", total_reembolsos             );
			resultado.put("correctos",       correctos.toString()         );
			resultado.put("errores",         errores.toString()           );
			resultado.put("num_reg_ok",      Integer.toString(num_reg_ok) );
			//resultado.put("proceso_carga", proceso_carga                );
			resultado.put("tipo_carga",      tipo_carga                   );

		} else if(reembolsos_cargados.size() > 1000 && reembolsos_cargados.size()<20000){
			ReembolsoPorFiniquito paginador = new ReembolsoPorFiniquito();
			folioBatch = paginador.cargaTmpDesembolsosBatch(lineas, file_name, cgUsuario, nombreUsr, cvePrograma );
			st_archivo = "CARGADO";
			cargaBatch = "S";
		}
	}
	String folio_batch_txt = "El archivo se cargó correctamente, favor de conservar el No. de Folio "+ folioBatch +".";

	/*****	Fin del proceso de validación	*****/
	resultado.put("success", 			new Boolean(success) );
	resultado.put("st_archivo", 		st_archivo				);
	resultado.put("folioBatch", 		folioBatch				);
	resultado.put("folio_batch_txt", folio_batch_txt		);
	resultado.put("cargaBatch", 		cargaBatch				);
	resultado.put("archivo_error", 	archivo_error			);
	resultado.put("nemsaje", 			mensaje					);
	resultado.put("monto_reembolso", monto_reembolso		);
	resultado.put("proceso_carga",	proceso_carga			);

	infoRegresar = resultado.toString();

} else if(informacion.equals("ReembolsosPorFiniquitoPreacuse")){

	HashMap datos 			 = new HashMap();
	JSONObject resultado  = new JSONObject();
	JSONArray registros 	 = new JSONArray();
	int numReembolsos 	 = 0;

	cvePrograma = (String)(request.getSession()).getAttribute("cveProgramaFondoJR");//FODEA 026-2010
	if(monto_reembolso.indexOf(",") != -1){
		monto_reembolso = monto_reembolso.replaceAll(",", "");
	}
	BigDecimal totalReembolsos = new BigDecimal(monto_reembolso);
	BigDecimal parcialReembolsos = new BigDecimal("0.00");

	//Instanciación para uso del EJB
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

	resultado.put("success",			new Boolean(true)	);
	resultado.put("proceso_carga", 	proceso_carga		);
	resultado.put("tipo_carga", 		tipo_carga	 		);
	resultado.put("monto_reembolso", monto_reembolso	);
	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>