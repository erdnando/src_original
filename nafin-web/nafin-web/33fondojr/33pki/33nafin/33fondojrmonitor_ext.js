Ext.onReady(function() {

	var ini_tipoMonitor = Ext.getDom('hidTipoMonitor').value;
	var ini_hidDescProgFondoJR = Ext.getDom('hidDescProgFondoJR').value;
	var ini_cvePrograma = Ext.getDom('hidCvePrograma').value;
	var tituloFormCons = 'Monitor - Cr�ditos (PFAEE 2)'
	var ini_masdequinientos = '';
	
	if(ini_tipoMonitor=='R') tituloFormCons = 'Monitor - Reembolsos';
	if(ini_tipoMonitor=='NPR') tituloFormCons = 'Monitor - No Pagados Rechazados';
	
	var gbl_estatus_val = '';
	var gbl_pantalla = '';
	var gbl_folio_val = '';
	var gbl_fec_venc = '';
//----------------------------------------------------------HANDLERS OR FUNCTION
	var generaZipMonitor = function(valPantalla){
	
		if(valPantalla != 'M'){
			gbl_estatus_val = '';
			gbl_pantalla = '';
			gbl_folio_val = '';
			gbl_fec_venc = '';
		}
		
		var ventana = Ext.getCmp('winDescZIP');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				modal: true,
				resizable: false,
				layout: 'form',
				x: 100,
				width: 200,
				id: 'winDescZIP',
				closable: true,
				closeAction: 'hide',
				items: [pnlGeneraZip],
				title: 'Descarga ZIP'
			}).show();
		}
	}
	
	function processSuccessFailureArchivoZip(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			if(resp.faseProcGenZip=='2') {
				Ext.Ajax.request({
					url:'33fondojrmonitor_ext.data.jsp',
					params:{
						informacion:'generarArchivoMonitorZip',
						faseProcGenZip: resp.faseProcGenZip
					},
					callback: processSuccessFailureArchivoZip
				});
				
			}else if(resp.faseProcGenZip=='3') {	
				//Ext.MsgBox.alert
				var ventana = Ext.getCmp('winDescZIP');
				ventana.hide();
										
			}else if(resp.faseProcGenZip=='4') {

				var boton = Ext.getCmp('btnDescZIP');
				
				boton.setIconClass('icoZip');
				boton.setText('Descargar ZIP');
				var ventana = Ext.getCmp('winDescZIP');
				ventana.hide();
				var fp = Ext.getCmp('fpMonitor1');
				var archivo = resp.urlArchivo;				  
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();	
				//});
			}
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var pnlGeneraZip = new Ext.Panel({
		name: 'pnlGeneraZip',
		id: 'pnlGeneraZip1',
		width: 200,
		style: 'margin:0 auto;',
		frame: true,
		items:[
			{
				xtype: 'button',
				id: 'btnDescZIP',
				iconCls: 'icoZip',
				style: 'margin:0 auto;',
				text:'Descargar ZIP',
				handler: function(boton){
					boton.setIconClass('loading-indicator');
					boton.setText('Generando Archivo ZIP...');
					
					Ext.Ajax.request({
						url: '33fondojrmonitor_ext.data.jsp',
						params:{
							informacion: 'generarArchivoMonitorZip',
							estatus_val: gbl_estatus_val,
							pantalla: gbl_pantalla,
							folio_val: gbl_folio_val,
							fecVenc: gbl_fec_venc,
							faseProcGenZip: '1'
							//gbl_pantalla: gbl_pantalla
						},
						callback: processSuccessFailureArchivoZip
					});
				}
			}
		]
	});
	
	var verDetalle = function(estatus, dato){
		
		if(estatus=='NPR' && ini_tipoMonitor!='NPR'){
			window.location.href='33fondojrmonitor_ext.jsp?tipoMonitor=NPR';
		}else{
			
			gbl_estatus_val = estatus;
			gbl_pantalla = 'M';
			gbl_fec_venc = dato;
			
			storeDetMonitorData.load({
				url: '33fondojrmonitor_ext.data.jsp',
				params:{
					estatus: estatus,
					fecVenc: dato
				}
			});
		}
	}
	
	
	var processSuccessFailureDetMonitor = function(store, arrRegistros, opts) {
		
		if (arrRegistros != null) {
			var numReg = store.reader.jsonData.numRegistros;
			var ventana = Ext.getCmp('winDetMonitor');
			if (ventana) {
				ventana.show();
			} else {
				new Ext.Window({
					modal: false,
					resizable: false,
					layout: 'form',
					x: 100,
					width: 780,
					id: 'winDetMonitor',
					closable: false,
					closeAction: 'hide',
					items: [gridDetMonitor,
								{//Panel para mostrar avisos
									xtype: 'panel',
									name: 'avisoNumReg',
									id: 'avisoNumReg1',
									height: 30,
									width: 770,
									frame: true,
									style: 'margin:0 auto;',
									bodyStyle: 'text-align:center',
									hidden: false
								}
					],
					title: 'FONDO JUNIOR MONITOR DETALLE'
				}).show();
				
				ventana = Ext.getCmp('winDetMonitor');
			}
			
			var el = gridDetMonitor.getGridEl();
			if(store.getTotalCount() > 0) {
				if(numReg >1000){ 
					Ext.getCmp('btnGeneraZipMon1').show();
					Ext.getCmp('avisoNumReg1').body.update('<p align="left">El n�mero de registros se excede de los 1000, descargar archivo para ebtener el detalle completo</p>');
				}else{
					Ext.getCmp('btnGeneraZipMon1').hide();
					Ext.getCmp('avisoNumReg1').body.update(' ');
				}
				el.unmask();
			}else {
				//Ext.getCmp('avisoNumReg1').hide();
				Ext.getCmp('btnGeneraZipMon1').hide();
				Ext.getCmp('avisoNumReg1').body.update(' ');
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
			
	var procesarConsultaVencData = function(store, arrRegistros, opts) {
		//var fpMonitor = Ext.getCmp('fpMonitor1');
		fpMonitor.el.unmask();
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		
		gridMonitor.show();
		if (arrRegistros != null) {
		
			if (!gridMonitor.isVisible()) {
				contenedorPrincipalCmp.add(gridMonitor);
				contenedorPrincipalCmp.doLayout();
			}
			
			ini_masdequinientos = store.reader.jsonData["MASDEQUINIENTOS"];
			//ini_masdequinientos = 'V';
			
			var el = gridMonitor.getGridEl();
			
			if(ini_tipoMonitor=='NPR'){
				gridDetMonitor.setTitle('Detalle de Cr�ditos No Pagados Rechazados');
				gridMonitor.setWidth(600);
				Ext.getCmp('btnRegresar').enable();
				Ext.getCmp('btnLayout').disable();
			} else if(ini_tipoMonitor=='R'){
				gridDetMonitor.setTitle('Detalle de Cr�ditos Enviados');
				gridMonitor.setWidth(640);
				Ext.getCmp('btnRegresar').enable();
				Ext.getCmp('btnLayout').disable();
			}else {
				gridDetMonitor.setTitle('Detalle de Cr�ditos Enviados');
				gridMonitor.setWidth(837);
				fpMonitor.setWidth(600);
				Ext.getCmp('btnRegresar').disable();
				Ext.getCmp('btnLayout').enable();
			}
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenArchivo').urlArchivo='';
				Ext.getCmp('btnGenArchivo').setText('Generar Archivo');
				Ext.getCmp('btnGenArchivo').enable();
				el.unmask();
			}else {
				Ext.getCmp('btnGenArchivo').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var processSuccessFailureArchivo = function(opts, success, response) {
		var btnGenArchivo = Ext.getCmp('btnGenArchivo');
		btnGenArchivo.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			btnGenArchivo.setIconClass('icoXls');
			btnGenArchivo.setText('Abrir Excel');
			
			var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};

			
			btnGenArchivo.urlArchivo =  NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			//btnGenArchivo.urlArchivo =  archivo;
			btnGenArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			
		
		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						btnGenArchivo.setIconClass('icoXls');
						btnGenArchivo.setText('Abrir Excel');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				btnGenArchivo.setIconClass('icoXls');
				btnGenArchivo.setText('Abrir Excel');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
	};

//--------------------------------------------------------------STORE
	var storeCatIfData = new Ext.data.JsonStore({
		id: 'storeCatIfData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '33fondojrmonitor_ext.data.jsp',
		baseParams: {
			informacion: 'consCatIf'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, oprion){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
						if(objGral.inicial=='S'){
							iniRequest();
						}
					}
			}
		}
	});
	
	var procesarCatalogoIFData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_if = Ext.getCmp('ic_if1');
			if(ic_if.getValue()==''){
				ic_if.setValue(records[0].data['clave']);
			}
		}
  } 
	
	var procesarCatEstatusData = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catEstatus = Ext.getCmp('estatusCarga1');
			if(catEstatus.getValue()==''){
				catEstatus.setValue(records[0].data['clave']);
			}
		}
  } 
  
	var storeCatIfData = new Ext.data.JsonStore({
		id: 'storeCatIfData',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '33fondojrmonitor_ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoIFData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var storeCatEstatusData = new Ext.data.ArrayStore({
		  id: 0,
		  fields: [
				'clave',
				'descripcion'
		  ],
		  //data: [['N', 'Por validar'], ['V', 'Validado'],['B','Borrado'],['NPR','NP Rechazados']],
		  autoLoad: false,
		  listeners: {
				load: procesarCatEstatusData
		  }
		});
	
	
	var storeVencData = new Ext.data.JsonStore({
		url : '33fondojrmonitor_ext.data.jsp',
		id: 'storeVencData1',
		baseParams:{
			informacion: 'consultaMonitor'
		},
		root : 'registros',
		fields: [
			{name: 'MESTATUS'},
			{name: 'MTIPO_REG'},
			{name: 'MFECHA_VENC'},
			{name: 'MULTIMO_REG'},
			{name: 'MFOLIO'},
			{name: 'MTOTAL_REG'},
			{name: 'MMONTO_TOT'},
			{name: 'MESTATUS_PROC'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaVencData,
			beforeload:{
				fn: function(store, Obj) {
					
					store.baseParams.fec_venc_ini = Ext.getCmp('fec_venc_ini')?Ext.getCmp('fec_venc_ini').getValue():'';
					store.baseParams.fec_venc_fin = Ext.getCmp('fec_venc_fin')?Ext.getCmp('fec_venc_fin').getValue():'';
					store.baseParams.estatusCarga = Ext.getCmp('estatusCarga1')?Ext.getCmp('estatusCarga1').getValue():'';
					store.baseParams.tipoMonitor = ini_tipoMonitor;
				}		
			},
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaVencData(null, null, null);
				}
			}
		}
	});
	
	var storeDetMonitorData = new Ext.data.JsonStore({
		url : '33fondojrmonitor_ext.data.jsp',
		id: 'storeDetMonitorData1',
		baseParams:{
			informacion: 'consultaDetalleMonitor'
		},
		root : 'registros',
		fields: [
			{name: 'FECHA_VENC'},
			{name: 'IG_PRESTAMO'},
			{name: 'IG_CLIENTE'},
			{name: 'IG_DISPOSICION'},
			{name: 'FG_AMORTIZACION'},
			{name: 'FG_INTERES'},
			{name: 'FG_TOTALVENCIMIENTO'},
			{name: 'DF_PAGO_CLIENTE'},
			{name: 'CG_ESTATUS'},
			{name: 'DF_PERIODOFIN'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'DF_REGISTRO_FIDE'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: processSuccessFailureDetMonitor,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					processSuccessFailureDetMonitor(null, null, null);
				}
			}
		}
	});
	

//--------------------------------------------------------------------ELEMENTOS
	var elementosForm =
	[
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'ic_if1',
			fieldLabel: 'Nombre del IF',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_if',
			emptyText: 'Seleccione...',
			//width: 400,
			anchor: '100%',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			hidden: ini_tipoMonitor==''?false:true,
			store : storeCatIfData
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fec_venc_ini',
					id: 'fec_venc_ini',
					minValue: '01/01/1901',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fec_venc_fin',
					margins: '0 20 0 0',  //necesario para mostrar el icono de error
					listeners:{
						blur: function(obj){
							/*var objFecIni = Ext.getCmp('fec_venc_ini');
							var objFecFin = Ext.getCmp('fec_venc_fin');
							
							if(objFecIni.getValue()!='' || objFecFin.getValue()!=''){
								objFecIni.allowBlank= false;
								objFecFin.allowBlank= false;
							}else{
								objFecIni.allowBlank= true;
								objFecFin.allowBlank= true;
							}*/
						}
					}
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fec_venc_fin',
					id: 'fec_venc_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fec_venc_ini',
					margins: '0 20 0 0',  //necesario para mostrar el icono de error
					listeners:{
						blur: function(obj){
							/*
							var objFecIni = Ext.getCmp('fec_venc_ini');
							var objFecFin = Ext.getCmp('fec_venc_fin');
							
							if(objFecIni.getValue()!='' || objFecFin.getValue()!=''){
								objFecIni.allowBlank= false;
								objFecFin.allowBlank= false;
							}else{
								objFecIni.allowBlank= true;
								objFecFin.allowBlank= true;
							}
							*/
						}
					}
				}
			]
		},
		{
			xtype: 'combo',
			name: 'estatusCarga',
			id: 'estatusCarga1',
			fieldLabel: 'Estatus',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'estatusCarga',
			emptyText: 'Seleccione...',
			autoSelect :true,
			//width: 400,
			anchor: '50%',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			hidden: ini_tipoMonitor==''?false:true,
			store : storeCatEstatusData,
		   listeners:{
				select:{ 
					fn:function (combo) {
						if(combo.getValue()=='NPR') {					 
							Ext.getCmp('chkDesembolso1').show();				
						}else {
							Ext.getCmp('chkDesembolso1').hide();	
						}
					}	
				}
			}
		},
		{
			xtype: 'checkbox',
			boxLabel:	'Mostrar NPR con Desembolsos',
			value:	'S',
			hidden: true,
			name: 'chkDesembolso',
			id: 'chkDesembolso1'
		}
	];

var fpMonitor = new Ext.form.FormPanel({
		id: 'fpMonitor1',
		width: 600,
		title: tituloFormCons,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForm,
		monitorValid: true,
		buttons: [
			{
				text: 'Actualizar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
				
					var objFecIni = Ext.getCmp('fec_venc_ini');
					var objFecFin = Ext.getCmp('fec_venc_fin');
					
					if(!(objFecIni.getValue()!='' && objFecFin.getValue()!='') && (objFecIni.getValue()!='' || objFecFin.getValue()!='')){
						if(objFecIni.getValue()==''){
							objFecIni.markInvalid('El valor de la fecha inicial es requerido');
						}
						if(objFecFin.getValue()==''){
							objFecFin.markInvalid('El valor de la fecha final es requerido');
						}
					}else{
						objFecIni.clearInvalid();
						objFecFin.clearInvalid();
						fpMonitor.el.mask('Consultando...', 'x-mask-loading');
						
						storeVencData.load({
							params: Ext.apply(fpMonitor.getForm().getValues(),{
								operacion:'Generar',
								tipoMonitor: ini_tipoMonitor,
								start: 0,
								limit: 10
							})
						});
					}
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href='33fondojrmonitor_ext.jsp?tipoMonitor='+ini_tipoMonitor;
					//fp.getForm().reset();
				}
				
			}
		]
	});
	
	
var gridMonitor = new Ext.grid.EditorGridPanel({
	id: 'gridMonitor',
	store: storeVencData,
	style:	'margin:0 auto;',
	margins: '20 0 0 0',
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [
		{//1
			header: 'Fecha de Vencimiento Nafin',
			tooltip: 'Fecha de Vencimiento Nafin',
			dataIndex: 'MFECHA_VENC',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false,
			renderer:  function (valor, columna, registro){
				var dato = ''
				if(  !(  registro.data['MESTATUS']=='P' || registro.data['MESTATUS']=='T' || 
								registro.data['MESTATUS']=='R' || registro.data['MESTATUS']=='RC' || 
								registro.data['MESTATUS']=='NPR'
							)){
					if(registro.data['MESTATUS']=='BRC' || registro.data['MESTATUS']=='BT'){
						dato = registro.data['MTIPO_REG'];
					}else{
						dato = registro.data['MFECHA_VENC'];
					}
					if(registro.data['MESTATUS']=='BR'){
						valor='Reembolsos '+dato;
					}else{
						valor = dato;
					}
				
				}else if( registro.data['MESTATUS']=='P' ){

				}else if( registro.data['MESTATUS']=='T' ){
					valor='Prepagos Totales al d�a';
					
				}else if( registro.data['MESTATUS']=='R' && ini_tipoMonitor!='R' ){
					valor='Reembolsos al d�a';
					
				}else if( registro.data['MESTATUS']=='RC' ){
					valor='Recuperaciones al d�a';
					
				}else if( registro.data['MESTATUS']=='NPR' ){
					if(ini_tipoMonitor!='NPR'){
						valor='*NP Rechazados al d�a';
					}
					
					
				}
				
				return valor;
			}
		},
		{//2
			header: 'Fecha y Hora de �ltimo Registro Enviado por FIDE',
			tooltip: 'Fecha y Hora de �ltimo Registro Enviado por FIDE',
			dataIndex: 'MULTIMO_REG',
			sortable: true,
			hideable: false,
			width: 130,
			align: 'left',
			hidden: (ini_tipoMonitor=='')?false:true
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//3
			header: 'Folio de Operaci�n',
			tooltip: 'Folio de Operaci�n',
			dataIndex: 'MFOLIO',
			sortable: true,
			hidden: (ini_tipoMonitor=='')?false:true,
			width: 80,
			align: 'left',
			renderer:  function (valor, columna, registro){
				if(  !(  registro.data['MESTATUS']=='P' || registro.data['MESTATUS']=='T' || 
								registro.data['MESTATUS']=='R' || registro.data['MESTATUS']=='RC' || 
								registro.data['MESTATUS']=='NPR'
							)){
					if(valor=='0'){
						valor = '';
					}
					
				}else if( registro.data['MESTATUS']=='P' ){
					valor = '';
				}else if( registro.data['MESTATUS']=='T' ){
					valor = '';
				}else if( registro.data['MESTATUS']=='R' ){
					valor = '';
				}else if( registro.data['MESTATUS']=='RC' ){
					valor = '';
					
				}else if( registro.data['MESTATUS']=='NPR' ){
					valor = 'N/A';
					
				}
				
				return valor;
			}
		},
		{//4
			header : (ini_tipoMonitor=='NPR')?'NP Rechazados':(ini_tipoMonitor=='R'?'Registros Pagados':'Total de Registros'),
			tooltip: 'Total de Registros',
			dataIndex : 'MTOTAL_REG',
			sortable : true,
			width : 100,
			align: 'left'
		},
		{//5
			header : 'Monto',
			tooltip: 'Monto',
			dataIndex : 'MMONTO_TOT',
			width : 130,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			xtype: 		'actioncolumn',
			header: 		'Detalle',
			tooltip: 	'Detalle',
			width: 		80,
			align:		'center',
			hidden: 		false,
			renderer: function(valor, metadata, registro, rowindex, colindex, store) {
				if(  !(  registro.data['MESTATUS']=='P' || registro.data['MESTATUS']=='T' || 
									registro.data['MESTATUS']=='R' || registro.data['MESTATUS']=='RC' || 
									registro.data['MESTATUS']=='NPR'
								)){
						if(registro.data['MESTATUS_PROC']=='N'){
							valor='Ver';
						}else{
							valor='';
						}
					
					}else if( registro.data['MESTATUS']=='P' ){
						if(registro.data['MESTATUS_PROC']=='N'){
							valor='Ver';
						}else{
							valor='';
						}
					}else if( registro.data['MESTATUS']=='T' ){
						if(registro.data['MESTATUS_PROC']='N'){
							valor='Ver';
						}else{
							valor='';
						}
					}else if( registro.data['MESTATUS']=='R' ){
						if(registro.data['MESTATUS_PROC']=='N'){
							valor='Ver';
						}else{
							valor='';
						}
					}else if( registro.data['MESTATUS']=='RC' ){
						if(registro.data['MESTATUS_PROC']=='N'){
							valor='Ver';
						}else{
							valor='';
						}
					}else if( registro.data['MESTATUS']=='NPR' ){
						valor='Ver';
					}
					
					return valor;
			},
			items: [
				{
					getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						
						if(  !(  registro.data['MESTATUS']=='P' || registro.data['MESTATUS']=='T' || 
									registro.data['MESTATUS']=='R' || registro.data['MESTATUS']=='RC' || 
									registro.data['MESTATUS']=='NPR'
								)){
							if(registro.data['MESTATUS_PROC']=='N'){
								this.items[0].tooltip = 'Ver detalle';
								return 'iconoLupa';
							}
						
						}else if( registro.data['MESTATUS']=='P' ){
							if(registro.data['MESTATUS_PROC']=='N'){
								this.items[0].tooltip = 'Ver detalle';
								return 'iconoLupa';
							}
						}else if( registro.data['MESTATUS']=='T' ){
							if(registro.data['MESTATUS_PROC']='N'){
								this.items[0].tooltip = 'Ver detalle';
								return 'iconoLupa';
							}
						}else if( registro.data['MESTATUS']=='R' ){
							if(registro.data['MESTATUS_PROC']=='N'){
								this.items[0].tooltip = 'Ver detalle';
								return 'iconoLupa';
							}
						}else if( registro.data['MESTATUS']=='RC' ){
							if(registro.data['MESTATUS_PROC']=='N'){
								this.items[0].tooltip = 'Ver detalle';
								return 'iconoLupa';
							}
						}else if( registro.data['MESTATUS']=='NPR' ){
							this.items[0].tooltip = 'Ver detalle';
							return 'iconoLupa';
						}
					
						return valor;
						//return 'iconoLupa';
					},
					handler: function(grid, rowIndex, colIndex) {
						var registro = Ext.StoreMgr.key('storeVencData1').getAt(rowIndex);
						
						var dato = ''
						if(  !(  registro.data['MESTATUS']=='P' || registro.data['MESTATUS']=='T' || 
										registro.data['MESTATUS']=='R' || registro.data['MESTATUS']=='RC' || 
										registro.data['MESTATUS']=='NPR'
									)){
							if(registro.data['MESTATUS']=='BRC' || registro.data['MESTATUS']=='BT'){
								dato = registro.data['MTIPO_REG'];
							}else{
								dato = registro.data['MFECHA_VENC'];
							}
							
							verDetalle(registro.data['MESTATUS'], dato);
						
						}else if( registro.data['MESTATUS']=='P' ){
							verDetalle(registro.data['MESTATUS'], registro.data['MFECHA_VENC']);
						}else if( registro.data['MESTATUS']=='T' ){
							verDetalle(registro.data['MESTATUS'], '');
						}else if( registro.data['MESTATUS']=='R' ){
							if (ini_tipoMonitor=='R'){
								verDetalle(registro.data['MESTATUS'], registro.data['MFECHA_VENC']);
							}else{
								if(ini_masdequinientos=='V'){
									generaZipMonitor(registro, '');
								}else{
									verDetalle(registro.data['MESTATUS'], '');
								}
							}
						}else if( registro.data['MESTATUS']=='RC' ){
							verDetalle('RT', '');
						}else if( registro.data['MESTATUS']=='NPR' ){
							verDetalle(registro.data['MESTATUS'], registro.data['MFECHA_VENC']);
						}
						
					}
				}
			]
		},
		{//7
			header : 'Estatus',
			tooltip: 'Estatus',
			dataIndex : 'MESTATUS_PROC',
			width : 160,
			sortable : true,
			align: 'center',
			hidden: (ini_tipoMonitor=='NPR')?true:false,
			renderer:  function (valor, columna, registro){
					
					if(  !(  registro.data['MESTATUS']=='P' || registro.data['MESTATUS']=='T' || 
									registro.data['MESTATUS']=='R' || registro.data['MESTATUS']=='RC' || 
									registro.data['MESTATUS']=='NPR'
								)){
						var dataRequest = 'fec_venc='+registro.data['MFECHA_VENC'];
						if(valor=='N'){
							valor='<a href="33fondojrmonitorval_ext.jsp?'+dataRequest+'">Por Validar</a>';
						}else{
							valor= valor=='V'?'Validado':'Borrado';
						}
					
					}else if( registro.data['MESTATUS']=='P' ){
						var dataRequest = 'fec_venc='+registro.data['MFECHA_VENC'];
						if(valor=='N'){
							valor='<a href="33fondojrmonitorval_ext.jsp?'+dataRequest+'">Por Validar</a>';
						}else{
							valor= valor=='V'?'Validado':'Borrado';
						}
					}else if( registro.data['MESTATUS']=='T' ){
						var dataRequest = 'fec_venc=T';
						if(valor='N'){
							valor='<a href="33fondojrmonitorval_ext.jsp?'+dataRequest+'">Por Validar</a>';
						}else{
							valor='Validado';
						}
					}else if( registro.data['MESTATUS']=='R' ){
						if (ini_tipoMonitor=='R'){
							var dataRequest = 'estatus_cred=R&fec_venc='+registro.data['MFECHA_VENC'];
							if(valor=='N'){
								valor='<a href="33fondojrmonitorval_ext.jsp?'+dataRequest+'">Por Validar</a>';
							}else{
								valor= valor=='V'?'Validado':'Borrado';
							}
						}else{
							var dataRequest = 'estatus_cred=R&fec_venc=R';
							if(valor=='N'){
								valor='<a href="33fondojrmonitor_ext.jsp?tipoMonitor=R">Por Validar</a>/<a href="33fondojrmonitorval_ext.jsp?'+dataRequest+'">Por Validar Todo</a>';
							}else{
								valor='Validado';
							}
						}
					}else if( registro.data['MESTATUS']=='RC' ){
						var dataRequest = 'fec_venc=RC';
						if(valor=='N'){
							valor='<a href="33fondojrmonitorval_ext.jsp?'+dataRequest+'">Por Validar</a>';
						}else{
							valor='Validado';
						}
					}else if( registro.data['MESTATUS']=='NPR' ){
						return 'N/A';
					}
					
					return valor;
				}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 300,
	width: 837,
	style: 'margin:0 auto;',
	title: '',
	frame: true,
	bbar: {
			xtype:		'paging',
			autoScroll:	true,
			height:		30,
			pageSize:	10,
			buttonAlign:'left',
			id:			'barraPaginacion1',
			displayInfo:true,
			store:		storeVencData,
			displayMsg:	'{0} - {1} de {2}',
			emptyMsg:	"No hay registros.",
			items:		[
				'->','-',
				{
						xtype:	'button',
						text:		'Generar Archivo',
						id:		'btnGenArchivo',
						iconCls: 'icoXls',
						hidden:	false,
						urlArchivo: '',
						handler:	function(boton, evento) {
							if(Ext.isEmpty(boton.urlArchivo)){
								boton.disable();
								boton.setIconClass('loading-indicator');
								
								Ext.Ajax.request({
									url: '33fondojrmonitor_ext.data.jsp',
									params:Ext.apply(fpMonitor.getForm().getValues(),{
										informacion: 'generarArchivoMonitor',
										tipoMonitor: ini_tipoMonitor,
										cvePrograma: ini_cvePrograma
									}),
									callback: processSuccessFailureArchivo
								});
							}else{
								/*var forma 		= Ext.getDom('formAux');
								forma.action 	= 
								forma.submit();*/
								
								var fp = Ext.getCmp('fpMonitor1');
								fp.getForm().getEl().dom.action = boton.urlArchivo;
								fp.getForm().getEl().dom.submit();
								
							}
						}
				},'-',
				{
					xtype:	'button',
					text:		'Layout',
					id:		'btnLayout',
					iconCls: 'icoTxt',
					hidden:	true,
					urlArchivo: '',
					handler:	function(boton, evento) {
						var ventana = Ext.getCmp('winLayout');
						if (ventana) {
							ventana.show();
						} else {
							new Ext.Window({
								modal: true,
								resizable: false,
								layout: 'form',
								x: 100,
								width: 780,
								id: 'winLayout',
								closable: false,
								closeAction: 'hide',
								items: [gridLayout],
								title: ''
							}).show();
						}
		
					}
				},'-',
				{
					xtype:	'button',
					text:		'Regresar',
					id:		'btnRegresar',
					//iconCls: 'icoReturn',
					hidden:	true,
					handler: function(){
						window.location.href='33fondojrmonitor_ext.jsp';
					}
				}
			]
		}
	});

	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: new Ext.data.ArrayStore({
			  id: 0,
			  fields: [
					'NUM',
					'NOMBRE',
					'DESC',
					'TIPODATO',
					'LONG',
					'OBSERV'
			  ],
			  data: [['1', 'com_fechaprobabalepago', 'Fecha Ral de Pago NAFIN', 'DATE', '10', 'Fecha real de pago'],
						['2', 'ig_prestamo', 'Prestamo', 'Number(12,0)', '12', 'N�mero del pr�stamo'],
						['3', 'ig_cliente', 'N�mero SIRAC', 'Number(11,0)', '11', 'C�digo del cliente'],
						['4', 'ig_disposicion', 'N�mero de Cuota a Pagar', 'Number(5,0)', '5', 'N�mero de disposici�n'],
						['5', 'fg_amortizacion', 'Amortizaci�n de Capital', 'Number(19,2)', '17.2', 'Amortizaci�n'],
						['6', 'fg_interes', 'Interes', 'Number(19,2)', '17.2', 'Intereses normales'],
						['7', 'fg_totalvencimiento', 'Total a Pagar', 'Number(19,2)', '17.2', 'Total Vencimiento' ],
						['8', 'df_pago_cliente', 'Fecha del Cliente Pago FIDE', 'DATE', '10', 'Pago de CFE a FIDE'],
						['9', 'cg_estatus', 'Estatus del Registro', 'Number(19,2)', '17.2', 'P:Pagados, NP:No Pagados, R:Reembolsos, <br> RF:Desembolsos, T:Prepagos Totales' ],
						['10', 'ig_origen', 'Origen', 'Varchar2(150)', '150', '0:CFE 2:Luz y Fuerza' ],
						['11', 'df_periodofin', 'Periodo Fin', 'DATE', '10', 'Periodo Fin'],
						['12', 'ig_numero_docto', 'N�mero de Documento', 'Varchar2(13)', '13', 'N�mero de Documento' ]
					  ]
			}),
		style:	'margin:0 auto;',
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{//1
				header: 'No.',
				tooltip: 'Numero',
				dataIndex: 'NUM',
				sortable: true,
				width: 50,
				resizable: true,
				hidden: false
			},
			{
				header: 'Nombre del Campo',
				tooltip: 'Nombre del Campo',
				dataIndex: 'NOMBRE',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{
				header: 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex: 'DESC',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{
				header: 'Tipo de Dato',
				tooltip: 'Tipo de Dato',
				dataIndex: 'TIPODATO',
				sortable: true,
				width: 90,
				resizable: true,
				hidden: false
			},
			{
				header: 'Longitud',
				tooltip: 'Longitud',
				dataIndex: 'LONG',
				sortable: true,
				width: 60,
				resizable: true,
				hidden: false
			},
			{
				header: 'Observaciones',
				tooltip: 'Observaciones',
				dataIndex: 'OBSERV',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 360,
		width: 770,
		title: '<p align="center">LAYOUT DE CREDITOS PAGADOS, NO PAGADOS Y RECUPERADOS DE DIAS ANTERIORES</p>',
		frame: true,
		bbar: {
		xtype: 'toolbar',
		buttonAlign: 'center',
		items: [
			'-',
			{
				text: 'Cerrar',
				handler: function(){
					Ext.getCmp('winLayout').hide();
				}
			},
			'-'
		]
	}
	});
	
	
	var gridDetMonitor = new Ext.grid.GridPanel({
		id: 'gridDetMonitor1',
		store: storeDetMonitorData,
		columns: [
			{//1
				header: 'Fecha de Vencimiento Nafin',
				tooltip: 'Fecha de Vencimiento Nafin',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'No. Pr�stamo',
				tooltip: 'No. Pr�stamo',
				dataIndex: 'IG_PRESTAMO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'No. Cliente',
				tooltip: 'No. Cliente',
				dataIndex: 'IG_CLIENTE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'Monto Amortizaci�n',
				tooltip: 'Monto Amortizaci�n',
				dataIndex: 'FG_AMORTIZACION',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Inter�s',
				tooltip: 'Monto Inter�s',
				dataIndex: 'FG_INTERES',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Venc',
				tooltip: 'Total Venc',
				dataIndex: 'FG_TOTALVENCIMIENTO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Fecha Pago Cliente',
				tooltip: 'Fecha Pago Cliente',
				dataIndex: 'DF_PAGO_CLIENTE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'CG_ESTATUS',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'Fecha Periodo Fin',
				tooltip: 'Fecha Periodo Fin',
				dataIndex: 'DF_PERIODOFIN',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'N�mero Docto',
				tooltip: 'N�mero Docto',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'Fecha Registro FIDE',
				tooltip: 'Fecha Registro FIDE',
				dataIndex: 'DF_REGISTRO_FIDE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 360,
		width: 770,
		title: 'Detalle de Cr�ditos Enviados',
		frame: true,
		bbar: {
		xtype: 'toolbar',
		buttonAlign: 'center',
		items: [
			'-',
			{
				text: 'Cerrar',
				iconCls:'icoRechazar',
				handler: function(){
					Ext.getCmp('winDetMonitor').hide();
				}
			},
			'-',
			{
				text: 'Generar Archivo',
				id: 'btnGeneraZipMon1',
				iconCls:'icoGenerarDocumento',
				hidden : true,
				handler: function(){
					generaZipMonitor(gbl_pantalla);
				}
			}
		]
	}
	});

	var textoPrograma = new Ext.Container({
		layout: 'table',		
		id: 'textoPrograma',							
		width:	'600',
		heigth:	'auto',
		hidden: false,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensaje', 	value: 'Programa: '+ini_hidDescProgFondoJR }				
		]
	});
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		renderHidden: true,
		layoutConfig: {
			align:'center'
		},
		items: [
			textoPrograma,
			NE.util.getEspaciador(20),
			fpMonitor,
			NE.util.getEspaciador(20)
		]
	});
	
	storeCatIfData.load();
	storeCatEstatusData.loadData([['N', 'Por validar'], ['V', 'Validado'],['B','Borrado'],['NPR','NP Rechazados']]);

});