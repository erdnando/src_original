Ext.onReady(function() {
 
	//Descarga Archivo
 
 	function procesarDescargaArch(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarCancelar(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
			
				window.location = '33CargaDesemFide.jsp';		
							
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	function procesarAcuse(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
			
				storeCifrasControlData.loadData(jsonData.todCifrasControl);
				Ext.getCmp('botonTransmitir').hide();
				Ext.getCmp('botonCancelarPre').hide();	
				Ext.getCmp('botonImprimir').show();	
				Ext.getCmp('btnSalir').show();					
				Ext.getCmp('mensajePreAcuse').update(jsonData.mensajePreAcuse);		
				Ext.getCmp('nombreArchivoAcuse').setValue(jsonData.nombreArchivoAcuse);				
				
				Ext.getCmp('panelResultadosAcuse').setTitle('Acuse');	
				
				if(jsonData.mensajeCorreo!='') {
					Ext.MessageBox.alert("Mensaje",jsonData.mensajeCorreo);
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	var confirmarAcuse = function(pkcs7, textoFirmar,  fechahonrada , noTotalDesembolsos , totalFondoNafin , totalFondoJR , totalAdeudo, totalDesembolso , totalSolicitadoFIDE  ){
	
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("botonTransmitir").enable();	
			return;	//Error en la firma. Termina...
		}else  {	
			fp.el.mask('Procesando Solicitud...', 'x-mask-loading');		
			Ext.getCmp("botonTransmitir").disable();				
			Ext.Ajax.request({
				url: '33CargaDesemFide.data.jsp',
				params: {
					informacion: 'TransmitirRegistros',
					fechahonrada:fechahonrada,
					noTotalDesembolsos:noTotalDesembolsos,
					totalFondoNafin:totalFondoNafin,
				   totalFondoJR:totalFondoJR,
				   totalAdeudo:totalAdeudo,
				   totalDesembolso:totalDesembolso,
				   totalSolicitadoFIDE:totalSolicitadoFIDE,
					pkcs7: pkcs7,
					textoFirmado: textoFirmar,
					nombreArchivo: Ext.getCmp('nombreArchivo').getValue(),
					ic_desembolso:	Ext.getCmp('ic_desembolso').getValue()
				},
				callback: procesarAcuse 
			});
		}	
	
	}
	
	
	
	function procesoTransmitirRegistros() {
	
		var  gridCifras = Ext.getCmp('gridCifras');
		var store = gridCifras.getStore();	
		var textoFirmar ="";		
		var  noTotalDesembolsos; 
		var  totalFondoNafin;
		var  totalFondoJR;
		var  totalAdeudo;
		var  totalDesembolso;
		var  totalSolicitadoFIDE;
		var  fechahonrada;
		var i  =  0;
		
		store.each(function(record) {
		
			textoFirmar +=  record.data['CAMPO1'] +": "+record.data['CAMPO2']+" \n";
			if(i ==1 ) fechahonrada =  record.data['CAMPO2'];
			if(i ==2 ) noTotalDesembolsos =  record.data['CAMPO2'];
			if(i ==3 ) totalFondoNafin =  record.data['CAMPO2'];
			if(i ==4 ) totalFondoJR =  record.data['CAMPO2'];
			if(i ==5 ) totalAdeudo =  record.data['CAMPO2'];
			if(i ==6 ) totalDesembolso =  record.data['CAMPO2'];
			if(i ==7 ) totalSolicitadoFIDE =  record.data['CAMPO2'];
			i++;			
		});		
	
		
		
		NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar,  fechahonrada , noTotalDesembolsos , totalFondoNafin , totalFondoJR , totalAdeudo, totalDesembolso , totalSolicitadoFIDE  );
		
		
	}
	
	
	
	
 //********************* Acuse ************************
 
 	var storeCifrasControlData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'}
		],
		totalProperty : 'total',
		autoLoad: false		
	});
	
	var gridCifras =[{
		id: 'gridCifras',
		hidden: false,
		xtype:'grid',
		title:'',
		store: storeCifrasControlData,
		style: 'margin:0 auto;',
		columns:[			
			{
				dataIndex: 'CAMPO1',
				sortable: true,
				width: 150,
				align: 'left'
			},
			{
				dataIndex: 'CAMPO2',
				sortable: true,
				width: 300,
				align: 'right',
				renderer:function(value,metadata,registro){
					if(registro.get('CAMPO3') =='MONTO' ) {					
						return Ext.util.Format.number(value,'$0,0.00');
					}else  {
						return value;
					}
				}
			}	
		],
		stripeRows: true,
		loadMask: true,
		height: 280,
		width: 500,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	
	var elementosAcuseCarga = [
		{
			width		: 500,	
			xtype: 'label',
			id: 'mensajePreAcuse',	
			align: 'center',
			text: ''
		},	
		 gridCifras,	
		{
			xtype: 			'container',
			width: 			'100%',
			style: 			'margin: 8px;padding-top:15px;',
			renderHidden: 	true,
			layout: {
				type: 		'hbox',
				pack: 		'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Imprimir PDF',
					iconCls: 	'icoImprimir',
					id: 			'botonImprimir',
					margins: 	' 3',
					handler:    function(boton, evento) {
					
						Ext.Ajax.request({
							url: '33CargaDesemFide.data.jsp',
							params: {
								informacion: 'ImprimirAcuse',
								nombreArchivoAcuse:  Ext.getCmp('nombreArchivoAcuse').getValue()
							},
							callback: procesarDescargaArch
						});						
					}					
				},							
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonCancelarPre',
					margins: 	' 3',
					handler:    function(boton, evento) {					
						
						Ext.Ajax.request({
							url: '33CargaDesemFide.data.jsp',
							params: {
								informacion: 'borraTemporales',
								ic_desembolso:  Ext.getCmp('ic_desembolso').getValue()
							},
							callback: procesarCancelar
						});					
												
					}
					
				},	
				{
					xtype: 		'button',					
					text: 		'Salir',
					iconCls: 	'icoAceptar',
					id: 			'btnSalir',
					margins: 	' 3',
					handler:    function(boton, evento) {
						window.location = '33CargaDesemFide.jsp';		
					}					
				},			
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Transmitir Registros',
					iconCls: 	'icoContinuar',
					id: 			'botonTransmitir',
					margins: 	' 3',
					handler:    procesoTransmitirRegistros
					
				}
				
			] 
		} 
	];
	
	
 	var panelResultadosAcuse = {
		title:			'Pre - Acuse',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosAcuse',
		width: 			500, 
		frame: 			true,
		style: 			'margin:  0 auto',
		bodyStyle:		'padding: 10px',
		items: 			elementosAcuseCarga
	};
	
	
	// ***Errores vs. Cifras de Control***********	
	
	var storeErrorCifrasData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'DESCRIPCION'}			
		],
		totalProperty : 'total',
		autoLoad: false		
	});
	
	var gridErrorCifras =[{
		id: 'gridErrorCifras',
		hidden: false,
		xtype:'grid',
		title:'',
		store: storeErrorCifrasData,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 650,
				align: 'left'
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 660,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	
 
	 // ***Registros con Errores **************
 
 	var storeErrorData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'DESCRIPCION'}			
		],
		totalProperty : 'total',
		autoLoad: false		
	});
	
	var gridError =[{
		id: 'gridError',
		hidden: false,
		xtype:'grid',
		title:'',
		store: storeErrorData,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 650,
				align: 'left'
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 660,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	
 // Grid de Registros sin Error
 
 	var storeBienData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'NUMERO_DESEMBOLSO'}			
		],
		totalProperty : 'total',
		autoLoad: false		
	});
	
	var gridBien =[{
		id: 'gridBien',
		hidden: false,
		xtype:'grid',
		title:'',
		store: storeBienData,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'N�mero de Desembolso',
				tooltip: 'N�mero de Desembolso',
				dataIndex: 'NUMERO_DESEMBOLSO',
				sortable: true,
				width: 650,
				align: 'left'
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 660,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	//***********Elementos Resultado Validaci�n 
 	var elementosResultadosValidacion = [
		{
			xtype: 'tabpanel',		
			id:'TabGrid',
			activeTab:0,
			width:	670,
			plain:	true,
			style: 'margin:0 auto;',
			bodyStyle: 'padding: 6px',
			//hidden: true,
			defaults:	{autoHeight: true},
			items:[
			/*	{
					title:	'Registros sin Errores',
					id:		'tabRegSinErrores',
					style:	'margin:0 auto;',
					hidden:true,
					items: [
						NE.util.getEspaciador(10),
						gridBien,
						NE.util.getEspaciador(10)
					]
				},
				*/
				{
					title:	'Registros con Errores',
					id:		'tabRegConErrores',
					style:	'margin:0 auto;',
					items: [
						NE.util.getEspaciador(10),
						gridError,
						NE.util.getEspaciador(10)
					]
				},
				{
					title:	'Errores vs. Cifras de Control',
					id:		'tabErrorCifras',
					style:	'margin:0 auto;',
					items: [
						NE.util.getEspaciador(10),
						gridErrorCifras,
						NE.util.getEspaciador(10)
					]
				}
			]
		},
		{
			xtype: 		'container',
			layout:		'form',
			labelWidth: 200,
			style:		"padding:8px;",
			items: [
				{ 
					xtype: 			'textfield',
					fieldLabel:		'Total de N�mero de Desembolsos',
					name: 			'totalNumerosDesembolsos',
					id: 				'totalNumerosDesembolsos1',
					readOnly:		true,
					hidden: 			false,
					maxLength: 		15,	
					width: 200,
					msgTarget: 		'side',
					style: {
						textAlign: 'right'
					}
				}
			]
		},
		
		{
			xtype: 			'container',
			width: 			'100%',
			style: 			'margin: 8px;padding-top:15px;',
			renderHidden: 	true,
			layout: {
				type: 		'hbox',
				pack: 		'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonCancelar',
					margins: 	' 3',
					handler:    function(boton, evento) {
					
						Ext.Ajax.request({
								url: '33CargaDesemFide.data.jsp',
								params: {
									informacion: 'borraTemporales',
									ic_desembolso:  Ext.getCmp('ic_desembolso').getValue()
								},
								callback: procesarCancelar
							});		
						
					}
					
				},
				{
					xtype: 		'button',					
					text: 		'Extraer Errores',
					iconCls: 	'icoXls',
					id: 			'btnArchivoError',
					margins: 	' 3',
					handler:    function(boton, evento) {
						
						Ext.Ajax.request({
							url: '33CargaDesemFide.data.jsp',
								params: {
								informacion: 'ArchivoErrores',
								nombreArchivoError:Ext.getCmp("nombreArchivoError").getValue()
							},
								callback: procesarDescargaArch
						});			
						
					}					
				},
				
				{
					xtype: 		'button',					
					text: 		'Continuar Carga',
					iconCls: 	'icoContinuar',
					id: 			'botonContinuar',
					margins: 	' 3',
					handler:    function(boton, evento) {
					
						Ext.getCmp("panelResultadosValidacion").hide();
						Ext.getCmp("forma").hide();
						Ext.getCmp("botonImprimir").hide();
						Ext.getCmp("btnSalir").hide();
						Ext.getCmp("panelResultadosAcuse").show();
						Ext.getCmp('gridLayout').hide();
					
					}					
				}
			] 
		}
		
	];
	
	
	
	var panelResultadosValidacion = {
		title:			'Resultado de la Validaci�n',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosValidacion',
		width: 			700, 
		frame: 			true,
		style: 			'margin:  0 auto',
		bodyStyle:		'padding: 10px',
		items: 			elementosResultadosValidacion
	};
	
	
	
	
	//********Panel de Validaci�n del Proceso **************+
	
	function procesaCargaArchivo(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
			
				hideWindowAvanceValidacion();	
				
				storeErrorData.loadData(jsonData.todError);	
				storeErrorCifrasData.loadData(jsonData.todCifrasDatos);
				storeBienData.loadData(jsonData.todOK);				
				storeCifrasControlData.loadData(jsonData.todCifrasControl);
				
				Ext.getCmp('panelResultadosValidacion').show();
				
				Ext.getCmp('nombreArchivoError').setValue(jsonData.nombreArchivoError);
				Ext.getCmp('totalNumerosDesembolsos1').setValue(jsonData.numCorrectos);
				
				if(jsonData.numErrores>0  ||  jsonData.numCifrasDatos>0 ) {			
					Ext.getCmp('totalNumerosDesembolsos1').hide();
					Ext.getCmp('botonContinuar').disable();
				}else  {			
					Ext.getCmp('botonContinuar').enable();
					Ext.getCmp('btnArchivoError').hide();						
					Ext.getCmp('mensajePreAcuse').update(jsonData.mensajePreAcuse);		
					
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	
	
	// funci�n para ocultar la venta de Proceso
	var hideWindowAvanceValidacion = function(){
		
		var ventanaAvanceValidacion = Ext.getCmp('windowAvanceValidacion');
		
		if(ventanaAvanceValidacion && ventanaAvanceValidacion.isVisible() ){
			var barrarAvanceValidacion	 = Ext.getCmp("barrarAvanceValidacion");
			barrarAvanceValidacion.reset(); 			
			
			if (ventanaAvanceValidacion) {
				ventanaAvanceValidacion.hide();
			}
      }      
	}
	
	
	var elementosAvanceValidacion = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
				text: 	'Validando Desembolsos...'
		},
		{
			xtype: 'progress',
			id:	 'barrarAvanceValidacion',
			name:	 'barrarAvanceValidacion',
			cls:	 'center-align',
			value: 0.00,
			text:  '0.00%'
		}
	];
	
	var panelAvanceValidacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelAvanceValidacion',
		frame: 			true,
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		items: 			elementosAvanceValidacion
	});
	
	
	//Ventana para mostrar Proceso 
	var showWindowAvanceValidacion = function(){
		
		var barrarAvanceValidacion		= Ext.getCmp("barrarAvanceValidacion");
 
		barrarAvanceValidacion.updateProgress(0.00,"0.00 %",true);
		barrarAvanceValidacion.wait({
            interval:	200,
            increment:	15
      });
 
		var ventana = Ext.getCmp('windowAvanceValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				width:			300,
				resizable:		false,
				id: 				'windowAvanceValidacion',
				modal:			true,
				closable: 		false,
				items: [
					Ext.getCmp("panelAvanceValidacion")
				]
			}).show();
			
		}
		
	}	
	
		
	///********************	 Carga ***************************+
	
	
	var enviarArchivoCarga = function() {
		
		var fp = Ext.getCmp('forma');
		
		var numDesembolsos = Ext.getCmp('numDesembolsos1');
		if(Ext.isEmpty(numDesembolsos.getValue())){
			numDesembolsos.markInvalid('Campo obligatorio');
			numDesembolsos.focus();
			return;
		}
		
		var fechahonrada1 = Ext.getCmp('fechahonrada');
		if(Ext.isEmpty(fechahonrada1.getValue())){
			fechahonrada1.markInvalid('Campo obligatorio');
			fechahonrada1.focus();
			return;
		}
		
				
		var archivo = Ext.getCmp('archivo');
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid('Favor de cargar un archivo');
			archivo.focus();
			return;
		}
		var  numDesembolsos =  Ext.getCmp('numDesembolsos1').getValue();
		var  fechahonrada =  	Ext.util.Format.date( Ext.getCmp('fechahonrada').getValue() ,'d/m/Y'); 
		var  parametros = "numDesembolsos="+numDesembolsos;
		var  ic_desembolso =  Ext.getCmp('ic_desembolso').getValue();
	
		Ext.getCmp('gridLayout').hide();
	
		fp.getForm().submit({
			url: '33CargaDesemFide.ma.jsp?'+parametros,									
			waitMsg: 'Enviando datos...',
			waitTitle :'Por favor, espere',			
			success: function(form, action) {
				var resp = action.result;
				var mensaje = resp.mensaje;
				var nombreArchivo = resp.nombreArchivo;
				
				 Ext.getCmp('nombreArchivo').setValue(nombreArchivo) 
				
				if(mensaje=='') {					
					showWindowAvanceValidacion();
					Ext.getCmp('nombreArchivo').setValue(nombreArchivo);
					
					Ext.Ajax.request({
						url: '33CargaDesemFide.data.jsp',
						params: {
							informacion: "ValidarArchivo",							
							nombreArchivo:nombreArchivo,
							numDesembolsos:numDesembolsos,
							fechahonrada:fechahonrada,
							ic_desembolso:ic_desembolso
						},
						callback: procesaCargaArchivo
					});
				}else {
					Ext.MessageBox.alert("Mensaje",mensaje);
					return;
				}								
			}
			,failure: NE.util.mostrarSubmitError
		});
			
	}



//**************Inicializa Valores **************************************
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
		
				Ext.getCmp('ic_desembolso').setValue(jsonData.ic_desembolso);	
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
		//*************+Ayuda**************
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{  name: 'NUMCAMPO'},
			{  name: 'NOMBRE_CAMPO'},
			{  name: 'TIPODATO'},
			{  name: 'LONGITUD'},
			{  name: 'OBLIGATORIO'},
			{  name: 'OBSERVACIONES'}
	  ]
	});

 var infoLayout = [
		['1','Numero de Pr�stamo','NUMBER','12','Si', 'N�mero de Pr�stamo'],
		['2','Nombre del Acreditado','CHAR','80','Si', 'Nombre Completo del Acreditado']	,
		['3','Fondeo Nafin - Capital','NUMBER','14,2','Si', 'Monto del Capital del Fondo Nafin']	,
		['4','Fondeo Nafin - Interes','NUMBER','14,2','Si', 'Monto del Interes del Fondo Nafin'],
		['5','Fondeo Nafin - Total','NUMBER','14,2','Si', 'Monto Total del Fondo Nafin']	,
		['6','Fondeo JR - Capital','NUMBER','14,2','Si', 'Monto del Capital del Fondeo JR']	,
		['7','Fondeo JR - Inter�s','NUMBER','14,2','Si', 'Monto del Inter�s del Fondeo JR']	,
		['8','Fondeo JR - Total','NUMBER','14,2','Si', 'Monto Total del Fondeo JR']	,
		['9','Total Adeudo - Capital','NUMBER','14,2','Si', 'Monto del Capital del Total Adeudo']	,
		['10','Total Adeudo - Inter�s','NUMBER','14,2','Si', 'Monto del Inter�s del Total Adeudo'],		
		['11','Total Adeudo - Total','NUMBER','14,2','Si', 'Monto Total del Total Adeudo'],
		['12','Total Desembolso Capital ','NUMBER','14,2','Si', 'Monto del  Capital del Total Desembolso'],
		['13','Total Desembolso Inter�s ','NUMBER','14,2','Si', 'Monto  del Inter�s del Total Desembolso'],
		['14','Total Desembolso Total ','NUMBER','14,2','Si', 'Monto Total el Total Desembolso'],
		['15','Solicitad FIDE - Capital ','NUMBER','14,2','Si', 'Monto del Capital del Solicitado FIDE '],
		['16','Solicitad FIDE - Inter�s ','NUMBER','14,2','Si', 'Monto del Inter�s del Solicitado FIDE '],
		['17','Solicitad FIDE - Total ','NUMBER','14,2','Si', 'Monto Total del Solicitado FIDE ']			
	];
	storeLayoutData.loadData(infoLayout);
	
	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'No',
				dataIndex : 'NUMCAMPO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Nombre del Campo',				
				dataIndex : 'NOMBRE_CAMPO',
				width : 200,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Tipo',
				dataIndex : 'TIPODATO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Longitud M�xima',
				dataIndex : 'LONGITUD',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Observaciones',
				dataIndex : 'OBSERVACIONES',
				width : 200,
				sortable : true,
				align: 'left'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 830,
		height: 400,
		frame: true
	});
		
		
	var myValidFnXLS = function(v) {
		var myRegex = /^.+\.([Xx][Ll][Ss])$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivoXls 		: myValidFnXLS,
		archivoXlsText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): XLS.'
	});
	
	
 var elementosForma  = [
	{
		xtype: 'compositefield',
		fieldLabel: 'N�mero de Desembolsos',
		width: 600,		
		combineErrors: false,
		msgTarget : 'side',
		margins: '0 20 0 0',
		items: [						
			{
				xtype : 'numberfield',
				fieldLabel : 'N�mero de Desembolsos',
				name : 'numDesembolsos',
				id : 'numDesembolsos1',
				allowBlank: true,
				maxLength : 12,
				width : 100,
				msgTarget : 'side',
				margins: '0 20 0 0'
			}
		]	
	},
	{
		xtype: 'compositefield',
		fieldLabel: 'Fecha Honrada',
		combineErrors: false,
		msgTarget: 'side',
		items: [
			{
				xtype			: 'datefield',
				id          : 'fechahonrada',
				name			: 'fechahonrada',
				allowBlank	: false,
				minValue: '01/01/1901',
				width			: 100,
				msgTarget	: 'side',				
				margins		: '0 20 0 0'
			},
			{
				xtype: 'displayfield',
				value: 'dd/mm/aaaa',
				width: 20
			}
		]
	},				
	{
		xtype:	'panel',
		layout:	'column',
		width: 600,
		anchor: '100%',
		id:'cargaArchivo1',
		defaults: {	bodyStyle:'padding:4px'	},
		items:	[
			{
				xtype: 'button',
				id: 'btnAyuda',
				columnWidth: .05,
				autoWidth: true,
				autoHeight: true,
				iconCls: 'icoAyuda',
				handler: function(){						
					if (!gridLayout.isVisible()) {
						gridLayout.show();
					}else{
						gridLayout.hide();
					}					
				}
			},
			{
				xtype: 'panel',
				id:		'pnlArchivo',
				columnWidth: .95,
				anchor: '100%',
				layout: 'form',
				fileUpload: true,
				labelWidth: 120,
				defaults: {
					bodyStyle:'padding:5px',
					msgTarget: 'side',
					anchor: '-20'
				},
				items: [
					{
						xtype: 'compositefield',
						fieldLabel: '',
						id: 'cargaArchivo',
						combineErrors: false,
						msgTarget: 'side',
						items: [
							{
								xtype: 'fileuploadfield',
								id: 'archivo',
								emptyText: 'Seleccione...',
								fieldLabel: 'Ruta del Archivo',
								name: 'archivo_ruta',   
								buttonText: '',
								width: 380,	  
								buttonCfg: {
									iconCls: 'upload-icon'
								},
								anchor: '100%',
								vtype: 'archivoXls',
								msgTarget : 'side',
								margins: '0 20 0 0'
							}						
						]
					}
				]
			}	
		]
	},
	{ 
		xtype:   'label',  
		html:		'<b><a href="/nafin/00archivos/33fondojr/EjemploExcel.xls"><font color=blue>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ejemplo Excel </font></a></b>', 
		cls:		'x-form-item', 
		style: { 
			width:   		'100%', 
			textAlign: 		'left',
			marginBottom:  '10px'
		} 
	},
	{
		xtype: 		'panel',
		anchor: 		'100%',
		style: {
			marginTop: 		'10px'
		},
		layout: {
			type: 'hbox',
			pack: 'end',
			align: 'middle'
		},
		items: [
			{
				text: 'Continuar',
				xtype: 			'button',
				id: 'Continuar',
				iconCls: 'icoContinuar',
				formBind: true,				
				handler: enviarArchivoCarga
			}
		]
	},
	{
		xtype: 	'label',
		anchor:  '100%',
		html: 	"<table cellpadding=\"2\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" width=\"100%\" > "  +
						" <tr>  <td class=\"celda01\" align=\"center\" colspan=\"10\"  style=\"height:30px;\" >"  +
						"	 El archivo deber� estar en formato .xls antes de cargar "  +
						" </td>  </tr>"  +
						"</table>" ,
		cls:		'x-form-item',
		style: {
			width: 			'100%',
			marginTop: 		'20px', 
			textAlign:		'center',
			color:			'#ff0000'
		}
	},
	{ 	xtype: 'textfield',  hidden: true, id: 'nombreArchivoError', 	value: '' },
	{ 	xtype: 'textfield',  hidden: true, id: 'nombreArchivoAcuse', 	value: '' },
	{ 	xtype: 'textfield',  hidden: true , id: 'nombreArchivo', 	value: '' },
	{ 	xtype: 'textfield',  hidden: true , id: 'ic_desembolso', 	value: '' }	
	
		
 ];
 
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Carga de Archivos',
		frame: true,		
		titleCollapse: false,
		fileUpload: true,		
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,		
		items: elementosForma		
	});
 
 
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,			
			NE.util.getEspaciador(20),
			gridLayout,
			panelResultadosValidacion,
			panelResultadosAcuse,
			NE.util.getEspaciador(20)
		]
	});
	
 
 //Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '33CargaDesemFide.data.jsp',
		params: {
			informacion: "Inicializar"		
		},
		callback: procesaValoresIniciales
	});
	

});