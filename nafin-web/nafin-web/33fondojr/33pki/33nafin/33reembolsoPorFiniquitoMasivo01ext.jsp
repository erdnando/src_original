<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>

<html>
  <head>
    <%@ include file="/extjs.jspf" %>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
  <link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
	<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
	<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
	<%@ include file="/00utils/componente_firma.jspf" %>

	<script type="text/javascript" src="33reembolsoPorFiniquitoMasivo01ext.js?<%=session.getId()%>"></script>
	 <title>Reembolso por Finiquito</title>
		<%@ include file="/01principal/menu.jspf"%>
 </head>
 <body>
	
 <%if(esEsquemaExtJS)  { %>
  
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
      <div id='areaContenido'></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
  
<%}else  {%>
	
	<div id="Contcentral">
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<form id='formAux' name="formAux" target='_new'></form>	
		
<%}%>
 </body>
</html>