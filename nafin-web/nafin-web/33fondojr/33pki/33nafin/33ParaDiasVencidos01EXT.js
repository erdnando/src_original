Ext.onReady(function(){

function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
      fp.el.unmask();
      
			var boton=Ext.getCmp('btnExportar');
			boton.setIconClass('icoXls');
			boton.enable();
      
      var botonI=Ext.getCmp('btnImprimir');
			botonI.setIconClass('icoPdf');
			botonI.enable();
      
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	};
  
///------
function descargaInfoEliminar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
      var respuestaEli = info.respuestaEli;
		var gridEli = Ext.getCmp('gridEliminar');	
		var maskEli = gridEli.getGridEl(); maskEli.mask('No se encontraron registros.', 'x-mask');		
      if (respuestaEli==="ok"){
			Ext.getCmp('gridEliminar').show();
			Ext.getCmp('griduno').hide();
          var fp=Ext.getCmp('forma');
					fp.getForm().reset();
			 /*
            fp.el.mask('Actualizando...', 'x-mask-loading');	
							consultaData.load({
                params :{
                        informacion	:'consultar',
                        operacion: 'Generar',
                        start: 0,
                        limit: 15
                        }
									});*/
      }else {
			Ext.getCmp('gridEliminar').hide();
			Ext.getCmp('griduno').show();
      }
      
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
//-----

//-----------------------------procesarInformacion------------------------------
	function procesarInformacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
      var respuesta = info.respuesta;
      var operacion = info.noOperacion;
      if (respuesta=="N"){             
         Ext.getCmp('fecha').hide();          Ext.getCmp('fd').reset();   Ext.getCmp('fh').reset();
         Ext.getCmp('diasLimite').hide();     Ext.getCmp('dias').reset();          
         Ext.getCmp('detalle').hide();        Ext.getCmp('areaDetalle').reset();
         Ext.getCmp('botonesCaptura').hide();
         
         Ext.getCmp('noOp').setValue(operacion);
         Ext.getCmp('capEx').show(); 
         Ext.getCmp('numEx').show(); 
         Ext.getCmp('btnCapEx').show();          
         
      }else {
        Ext.getCmp('noOp').setValue(operacion);
        Ext.Msg.show({
        title: 'Conflicto',
        msg: 'El rango de fecha capturado entra en conflicto con la parametrizaci�n <br>n�mero '+respuesta+' favor de modificar las fechas o en su defecto <br> borrar la parametrizaci�n que causa el conflicto.',
        modal: true,
        icon: Ext.Msg.WARNING,
        buttons: Ext.Msg.OK,
        fn: function (){
          }
        });
      }
      if (respuesta=="M"){ 
          var fp=Ext.getCmp('forma');
              fp.el.mask('Actualizando...', 'x-mask-loading');	
							consultaData.load({
              params :{
                        informacion	:'consultar',
                        operacion: 'Generar',
                        numOper:Ext.getCmp('num').getValue(),
                        start: 0,
                        limit: 15
                      }
									});
        Ext.Msg.show({
          title: 'Modificaci�n',
          msg: 'El registro fue modificado exitosamente.',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function (){
            var grid = Ext.getCmp('griduno');
            var store = grid.getStore();
            var jsonData = store.reader.jsonData;
            Ext.getCmp('fdInv').setValue(jsonData.registros[0].FEC_VEN_INI);
            Ext.getCmp('fhInv').setValue(jsonData.registros[0].FEC_VEN_FIN);
            Ext.getCmp('dInv').setValue(jsonData.registros[0].DIASVENCIMIENTO);           
            }
        });
        Ext.getCmp('num').setReadOnly(false);
        Ext.getCmp('fecha').hide();
        Ext.getCmp('fd').reset();
        Ext.getCmp('fh').reset();
        Ext.getCmp('diasLimite').hide();
        Ext.getCmp('dias').reset();
        Ext.getCmp('detalle').hide();
        Ext.getCmp('detalle').reset();
        Ext.getCmp('botonesConsulta').hide();
        Ext.getCmp('btnGuardar').hide();
        
        
        Ext.getCmp('fInv').show();
        Ext.getCmp('dLInv').show();                  
        Ext.getCmp('botonesConsulta').show();      
      }

		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
//---------------------------Fin procesarInformacion----------------------------

//------------------------------ ------------------------------------
 var consultaDataDetalle = new Ext.data.JsonStore({
		root 				: 'registros',
		url 				: '33ParaDiasVencidos01EXT.data.jsp',
		baseParams	: {
                  informacion:'consultar'
                  },
    fields      : [	
                  {	name: 'NOPERACION'},
                  {	name: 'FEC_VEN_INI'},
                  {	name: 'FEC_VEN_FIN'},
                  {	name: 'DIASVENCIMIENTO'},
                  {	name: 'DETALLE'},
                  {	name: 'NOMUSUARIO'},
                  {	name: 'FEC_MODIFICACION'},
                  {	name: 'SELECCIONAR', type: 'bool'}
                ],		
		totalProperty 	: 'total',
    messageProperty: 'msg',
		autoLoad			  : false	
	})

var gridDetalle = //new Ext.grid.GridPanel({
		{
		xtype: 'editorgrid',
		id		: 'gridDetalle',
		store	: consultaDataDetalle,
		height: 100,
		width	: 550,
		columns: [{
				header	: 'No. de Operaci�n.',
				tooltip	: 'No. de Operaci�n.',
				width 	: 100 ,
				dataIndex: 'NOPERACION',
				sortable	: false,
				resizable: false,
				hideable	:false,
				align: 'center'
			},{
				header	: 'Detalle.',
				tooltip	: 'Detalle.',
				width 	: 470 ,
				dataIndex: 'DETALLE',
				sortable	: false,
				resizable: true,
				hideable	:false,
				align: 'center',
				renderer			:function(value,params,record){
					return "<div align='left'>"+value+"</div>";
				}
			}
		]
	};

//-----------------------------procesarVerDetalle-------------------------------
var procesarVerDetalle= function(grid, rowIndex, colIndex, item, event) {
	
	
	var registro = grid.getStore().getAt(rowIndex);
	var operacion=registro.get('NOPERACION');
	var detalle=registro.get('DETALLE');

	consultaDataDetalle.load({
	 params :{
				informacion	:'consultar',
				operacion: 'Generar',
				numOper:operacion,
				start: 0,
				limit: 15
				}
		});

var ventana = Ext.getCmp('verDetalle');	
if (ventana) {	
	ventana.show();	
} else {		
	new Ext.Window({
	layout: 'fit',
	width: 600,
	height: 'auto',
	id: 'verDetalle',
	closeAction: 'hide',
	autoDestroy:true,
	closable:false,
	modal: true,/*
	resizable: false,
	autoScroll :true,
	constrain: true,*/	
	items: [					
	 gridDetalle//elementosDetalle				
	],
  title: 'Detalle.',
  bbar: {
    xtype: 'toolbar',	buttonAlign:'center',	
    buttons: [
      {
      xtype   : 'button',
      text    : 'Salir',
      iconCls : 'icoContinuar',
      id      : 'btnCerrar',
      handler: function(){
            Ext.getCmp('verDetalle').destroy();	
            } 
      }
    ]
   }
	}).show();
 }
 
	
}
//---------------------------FIN procesarVerDetalle-------------------------------


var procesarModificar = function(grid, rowIndex, colIndex, item, event) {
var registro = grid.getStore().getAt(rowIndex);
var noOperacion = registro.get('NOPERACION'); 
var fechaVenIni = registro.get('FEC_VEN_INI'); 
var fechaVenFin = registro.get('FEC_VEN_FIN'); 
var diasVencimi = registro.get('DIASVENCIMIENTO'); 
var detalle = registro.get('DETALLE'); 

            var fp=Ext.getCmp('forma');
            fp.el.mask('Cargando...', 'x-mask-loading');	
							consultaData.load({
                params :{
                        informacion	:'consultar',
                        operacion: 'Generar',
                        numOper:noOperacion,
                        start: 0,
                        limit: 15
                        }
									});
Ext.getCmp('fInv').hide();
Ext.getCmp('dLInv').hide();                  
Ext.getCmp('botonesConsulta').hide();
Ext.getCmp('fecha').show();
Ext.getCmp('diasLimite').show();
Ext.getCmp('detalle').show();
Ext.getCmp('btnGuardar').show();
Ext.getCmp('botonesConsulta').show();

Ext.getCmp('num').setValue(noOperacion);
Ext.getCmp('num').setReadOnly(true);
Ext.getCmp('fd').setValue(fechaVenIni);
Ext.getCmp('fh').setValue(fechaVenFin);
Ext.getCmp('dias').setValue(diasVencimi);
Ext.getCmp('areaDetalle').setValue(detalle);

}

//-----------------------------procesarConsultaData-----------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
   var jsonData = store.reader.jsonData;
Ext.getCmp('fd').clearInvalid();
Ext.getCmp('fh').clearInvalid();
Ext.getCmp('fdInv').clearInvalid();
Ext.getCmp('fhInv').clearInvalid();
   var fp = Ext.getCmp('forma');
		fp.el.unmask();							
		var grid1 = Ext.getCmp('griduno');	
		var el = grid1.getGridEl(); 
    
		if (arrRegistros != null) {
			if (!grid1.isVisible()) {
				grid1.show();
				}				
			if(store.getTotalCount() > 0) {
				el.unmask();
        Ext.getCmp('btnEliminar').show();	
        Ext.getCmp('btnExportar').show();
        Ext.getCmp('btnImprimir').show();	
        Ext.getCmp('btnEG').show();
			} else {		
        Ext.getCmp('btnEliminar').hide();	
        Ext.getCmp('btnExportar').hide();	
        Ext.getCmp('btnImprimir').hide();	
        Ext.getCmp('btnEG').hide();	
				el.mask('No se encontr� ning�n registro', 'x-mask');					
			}
		}
	}
//--------------------------Fin procesarConsultaData----------------------------
var consultaData2 = new Ext.data.JsonStore({});
//------------------------------ ------------------------------------
 var consultaData = new Ext.data.JsonStore({
		root 				: 'registros',
		url 				: '33ParaDiasVencidos01EXT.data.jsp',
		baseParams	: {
                  informacion:'consultar'
                  },
    fields      : [	
                  {	name: 'NOPERACION'},
                  {	name: 'FEC_VEN_INI'},
                  {	name: 'FEC_VEN_FIN'},
                  {	name: 'DIASVENCIMIENTO'},
                  {	name: 'DETALLE'},
                  {	name: 'NOMUSUARIO'},
                  {	name: 'FEC_MODIFICACION'},
                  {	name: 'SELECCIONAR', type: 'bool'}
                ],		
		totalProperty 	: 'total',
    messageProperty: 'msg',
		autoLoad			  : false,
    listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}	
	})
//

var textoCompleto = function (value, metadata, record, rowIndex, colIndex, store){
	metadata.attr = 'style="white-space: normal; word-wrap: break-word; "';
	return value;
};




//----------------------------- Elementos Detalle----------------------------
var  elementosDetalle =  [
		{
		xtype		: 'compositefield',
		id			:'cmpDetalle',
		items		: [{
			xtype	: 'displayfield',
			width : 10
			},gridDetalle
		]}
/*
  {
  xtype			: 'displayfield',
	value			: '',
	width			: 400
	},{
  xtype	: 'compositefield',
  autoScroll:true,
  items	:[
    {
    xtype			: 'displayfield',
		value			: '',
		width			: 50
		},  {
		xtype			: 'displayfield',
		value			: 'No. de Operaci�n',
		width			: 270
		},{
		xtype			: 'displayfield',
		value			: 'Detalle',
		width			: 200
		}]
  },{
  xtype	: 'compositefield',
  items	:[
    {
    xtype			: 'displayfield',
		value			: '',
		width			: 50
		},  {
		xtype			: 'textfield',
		 id         : 'txfNumOper',
		 style		: 'text-align:center;',
		 value      : '0004',
		//disabled		: true,
		readOnly  	: true,
		width			: 100
		},{
		xtype			: 'label',
		id         	: 'txfDetalle',
		//style		  	: 'background: none;background-color: gainsboro;',
		value      	:'Falta de pago',
		readOnly   	:true,
		width			: 400
		}]
  }*/
]
//-----------------------------Fin Elementos Detalle-----------------------------
   			
//-------------------------------Elementos Forma--------------------------------
	var  elementosForma =  [
  {
  xtype : 'compositefield',
  id    :'programa',
  items: [
	  {
		xtype			: 'displayfield',
		width			: 25
		},{
		xtype			: 'displayfield',
    id        : 'miPrograma', 
		width			: 400,
    height 		: 30
		}]
	}, {
	xtype : 'compositefield',
  id    :'botones',
  items: [
	  {
		xtype			: 'displayfield',
		value			: ' ',
		width			: 140
		}, {
		xtype     : 'button',
    text			:'Captura',
    iconCls		:'icoTxt',
    id				:'btnCaptura',
    handler		:function(boton, evento){
       Ext.getCmp('botones').hide();         
       Ext.getCmp('captura').show();
       Ext.getCmp('fecha').show();
       Ext.getCmp('diasLimite').show();
       Ext.getCmp('detalle').show();
       Ext.getCmp('botonesCaptura').show();
      }
		},{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 25
		},{
		xtype     : 'button',
    text			:'Consulta',
    iconCls		:'icoBuscar',
    id				:'btnConsulta',
    handler		:function(boton, evento){
      Ext.getCmp('botones').hide();   
      Ext.getCmp('consulta').show();         
      Ext.getCmp('numOper').show(); 
      Ext.getCmp('fInv').show();          Ext.getCmp('fdInv').reset();   Ext.getCmp('fhInv').reset();
      Ext.getCmp('dLInv').show();     Ext.getCmp('dInv').reset();         
      Ext.getCmp('botonesConsulta').show(); 
      }//FIN handler
		}
  ]},{
  xtype : 'compositefield',
  id    :'captura',
  hidden: true,
  items: [
	  {
		xtype			: 'displayfield',
		value			: '<b><center>Captura de par�metros para la aplicaci�n del l�mite de vencimiento.</center></b>',
		height    : 35,
    width			: 500
		}]
	}, {
  xtype : 'compositefield',
  id    :'consulta',
  hidden: true,
  items: [
	  {
		xtype			: 'displayfield',
		value			: '<b><center>Consulta de par�metros para aplicaci�n del l�mite de vencimiento.</center></b>',
		height    : 35,
    width			: 500
		}]
	},{
  xtype : 'compositefield',
  id    :'numOper',
  hidden: true,
  combineErrors: false,
  items: [
	  {
		xtype			: 'displayfield',
		value			: 'N�mero de Operaci�n:',
		width			: 180
		},{
		xtype			: 'textfield',
    id        : 'num',
    maskRe    : /[0-9]/,
    maxLength : 5,
    //style     : 'text-align:center;',
    msgTarget :	'side',
		width			: 100
		}]
	},{
  xtype     : 'compositefield',
  id        : 'fecha',
  hidden: true,
  combineErrors: false,
  items: [
    {
		xtype			: 'displayfield',
		value			: 'Fecha de Vencimiento desde:',
		width			: 180
		}, {
    xtype   :	'datefield',
    name    :	'fd',
    id      :	'fd',
    allowBlank:		  false,
    //format: 'd-m-Y',
    startDay  :		  0,
    width     :			100,
	 minValue		: '01/01/1901',
    msgTarget :		  'side',
    vtype     :			'rangofecha', 
    campoFinFecha:	'fh',
    margins   :			'0 20 0 0',  //necesario para mostrar el icono de error
    regexText : "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
    regex     : /.*[1-9][0-9]{3}$/
    }, {
    xtype: 'displayfield',
    value: 'hasta:',
    width: 50
    }, {
    xtype   : 		'datefield',
    name    :			'fh',
    id      :			'fh',
    allowBlank:			false,
    startDay  :		1,
    width     :		100,
    msgTarget :		'side',
	 minValue		: '01/01/1901',
    vtype     :		'rangofecha', 
    campoInicioFecha:	'fd',
    margins   :		'0 20 0 0',  //necesario para mostrar el icono de error
    regexText :   "La Fecha es incorrecta. Verfique que el formato sea 'dd/mm/aaaa'",
    regex     : /.*[1-9][0-9]{3}$/
    }]
  },{
  xtype     : 'compositefield',
  id        : 'fInv',
  hidden: true,
  combineErrors: false,
  items: [
    {
		xtype			: 'displayfield',
		value			: 'Fecha de Vencimiento desde:',
		width			: 180
		}, {
    xtype   :	'datefield',
    name    :	'fd',
    id      :	'fdInv',
    width     :			100,
	 minValue		: '01/01/1901',
    msgTarget :		  'side',
    margins   :			'0 20 0 0'  //necesario para mostrar el icono de error
    }, {
    xtype: 'displayfield',
    value: 'hasta:',
    width: 50
    }, {
    xtype   : 		'datefield',
    name    :			'fh',
    id      :			'fhInv',
	 minValue		: '01/01/1901',
    width     :		100,
    msgTarget :		'side',
    margins   :		'0 20 0 0'  //necesario para mostrar el icono de error
    }]
  },{
  xtype : 'compositefield',
  id    :'diasLimite',
  hidden: true,
  combineErrors: false,
  items: [
	  {
		xtype			: 'displayfield',
		value			: 'D�as limite de Vencimiento:',
		width			: 180
		},{
    xtype     : 'textfield',
    id        :'dias',
    allowBlank:false,
    maskRe    : /[0-9]/,
    maxLength : 3,
    //style     : 'text-align:center;',
    msgTarget :	'side',
    width     : 100
    }]
	},{
  xtype : 'compositefield',
  id    :'dLInv',
  hidden: true,
  combineErrors: false,
  items: [
	  {
		xtype			: 'displayfield',
		value			: 'D�as limite de Vencimiento:',
		width			: 180
		},{
    xtype     : 'textfield',
    id        :'dInv',
    maskRe    : /[0-9]/,
    maxLength : 3,
    //style     : 'text-align:center;',
    msgTarget :	'side',
    width     : 100
    }]
	},{
  xtype : 'compositefield',
  id    :'detalle',
  hidden: true,
  combineErrors: false,
  items: [
    {
		xtype			: 'displayfield',
		value			: 'Detalle:',
		width			: 50
		}, {
    xtype   : 'textarea',
    id      : 'areaDetalle',
    name    : 'areaDetalle',
    allowBlank: false,
    maxLength:1000,
    height  :100,
    width   :405,
    msgTarget :'side',
    regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
    regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
    }]
	},{
	xtype : 'compositefield',
  id    :'botonesCaptura',
  hidden: true,
  items: [
	  {
		xtype			: 'displayfield',
		value			: ' ',
		width			: 150
		}, {
		xtype     :'button',
    text			:'Aceptar',
    iconCls		:'aceptar',
    id				:'btnAceptar',
    handler		:function(boton, evento){
      if ( Ext.getCmp('fd').isValid() & Ext.getCmp('fh').isValid() & Ext.getCmp('dias').isValid() & Ext.getCmp('areaDetalle').isValid()) {
            var fd = Ext.getCmp('fd').getValue();
            var fh = Ext.getCmp('fh').getValue();            
            var dateDesde = Ext.getCmp('fd').formatDate(fd);
            var dateHasta = Ext.getCmp('fh').formatDate(fh);
            Ext.Ajax.request({
            url: '33ParaDiasVencidos01EXT.data.jsp',
            params: {
              informacion:'guardar',
              fd  :dateDesde,
              fh  :dateHasta,
              dias:Ext.getCmp('dias').getValue(),
              areaDetalle:Ext.getCmp('areaDetalle').getValue()
            },
            callback:procesarInformacion 
          }); 
      } else {
          Ext.Msg.show({
            title: 'Ingrese Informacion',
            msg: ' Este campo es obligatorio !',
            modal: true,
            icon: Ext.Msg.WARNING,
            buttons: Ext.Msg.OK,
            fn: function (){
               if ( ! Ext.getCmp('areaDetalle').isValid()){
                  var areaDetalle = Ext.getCmp('areaDetalle');	
                      areaDetalle.focus();
               }
               if ( ! Ext.getCmp('dias').isValid()){
                  var dias = Ext.getCmp('dias');	
                      dias.focus();
               }
               if ( ! Ext.getCmp('fh').isValid()){
                  var fh = Ext.getCmp('fh');	
                      fh.focus();
               }
               if ( ! Ext.getCmp('fd').isValid()){
                  var fd = Ext.getCmp('fd');	
                      fd.focus();
               }  
              }//FIN fn: function
            });
        }//FIN else
      }//FIN handler
		},{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 10
		},{
		xtype     : 'button',
    text			:'Cancelar',
    iconCls		:'icoCancelar',
    id				:'btnCancelar',
    handler		:function(boton, evento){
        Ext.Msg.show({
            title: 'Cancelar Informaci�n',
            msg: ' La parametrizaci�n no ser� guardada <br>� Est� seguro de cancelar la operaci�n ?',
            modal: true,
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function (btn){
                if (btn == "ok" || btn == "yes" ){                
                  Ext.getCmp('fd').reset();
                  Ext.getCmp('fh').reset();
                  Ext.getCmp('dias').reset();
                  Ext.getCmp('areaDetalle').reset();
                }
                if (btn=="cancel"){          
                }
            }//FIN function
        });
      }//FIN handler
		},{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 10
		}, {
		xtype     :'button',
    text			:'Salir',
    iconCls		:'icoContinuar',
    id				:'btnSalir',
    width			: 70,
    handler		:function(boton, evento){
       Ext.getCmp('botones').show();         
       Ext.getCmp('captura').hide();
       Ext.getCmp('fecha').hide();          Ext.getCmp('fd').reset();   Ext.getCmp('fh').reset();
       Ext.getCmp('diasLimite').hide();     Ext.getCmp('dias').reset();          
       Ext.getCmp('detalle').hide();        Ext.getCmp('areaDetalle').reset();
       Ext.getCmp('botonesCaptura').hide();
      }
		}]
  },{
	xtype : 'compositefield',
  id    :'botonesConsulta',
  hidden: true,
  items: [
	  {
		xtype			: 'displayfield',
		value			: ' ',
		width			: 100
		}, {
		xtype     :'button',
    text			:'Regresar',
    iconCls		:'icoRegresar',
    id				:'btnRegresar',
    handler		:function(boton, evento){
      Ext.getCmp('consulta').hide();
      Ext.getCmp('numOper').hide();
      Ext.getCmp('fInv').hide();      
      Ext.getCmp('dLInv').hide();
      Ext.getCmp('griduno').hide();
      Ext.getCmp('botonesConsulta').hide();
      
      Ext.getCmp('num').reset();      
      Ext.getCmp('fdInv').reset();
      Ext.getCmp('fhInv').reset();
      Ext.getCmp('dInv').reset();
      
      Ext.getCmp('fecha').hide();
      Ext.getCmp('fd').reset();
      Ext.getCmp('fh').reset();
      Ext.getCmp('diasLimite').hide();
      Ext.getCmp('dias').reset();
       
      Ext.getCmp('detalle').hide();
      Ext.getCmp('areaDetalle').reset();            
      
      Ext.getCmp('btnGuardar').hide();
             
      Ext.getCmp('botones').show();
      }//FIN handler
		},{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 10
		},{
		xtype     : 'button',
    text			:'Consultar',
    iconCls		:'icoBuscar',
    id				:'btnConsultar',
    handler		:function(boton, evento){
         if   (Ext.getCmp('num').isValid() & Ext.getCmp('fdInv').isValid() & Ext.getCmp('fhInv').isValid() & Ext.getCmp('dInv').isValid()  ){          
            var fdInv = Ext.getCmp('fdInv').getValue();
            var fhInv = Ext.getCmp('fhInv').getValue();       
				var fd = Ext.getCmp('fd').getValue();
            var fh = Ext.getCmp('fh').getValue(); 
            var dateDesde = Ext.getCmp('fdInv').formatDate(fdInv);
            var dateHasta = Ext.getCmp('fhInv').formatDate(fhInv);
				if(Ext.getCmp('fdInv').getValue() !='' & Ext.getCmp('fhInv').getValue() ==''){ 
					Ext.getCmp('fhInv').markInvalid('Este campo es obligatorio.');
					return;
				} else if(Ext.getCmp('fhInv').getValue() !='' & Ext.getCmp('fdInv').getValue() ==''){ 
					Ext.getCmp('fdInv').markInvalid('Este campo es obligatorio.');
					return;
				} else if(Ext.getCmp('fdInv').getValue() > Ext.getCmp('fhInv').getValue() ){ 
						Ext.getCmp('fdInv').markInvalid('La fecha de Vencimiento Inicial no debe ser mayor a la fecha de Vencimiento Final.');
					return;
				}
				
				if(Ext.getCmp('fd').getValue() !='' & Ext.getCmp('fh').getValue() ==''){ 
					Ext.getCmp('fh').markInvalid('Este campo es obligatorio.');
					return;
				} else if(Ext.getCmp('fh').getValue() !='' & Ext.getCmp('fd').getValue() ==''){ 
					Ext.getCmp('fd').markInvalid('Este campo es obligatorio.');
					return;
				} else if(Ext.getCmp('fd').getValue() > Ext.getCmp('fh').getValue() ){ 
						Ext.getCmp('fd').markInvalid('La fecha de Vencimiento Inicial no debe ser mayor a la fecha de Vencimiento Final.');
					return;
				}
				if(Ext.getCmp('btnGuardar').isVisible()){
					 dateDesde = Ext.getCmp('fd').formatDate(fd);
					dateHasta = Ext.getCmp('fh').formatDate(fh);
				} 
            var fp=Ext.getCmp('forma');
				Ext.getCmp('gridEliminar').hide();
            fp.el.mask('Cargando...', 'x-mask-loading');	
							consultaData.load({
                params :{
                        informacion	:'consultar',
                        operacion: 'Generar',
                        fd  :dateDesde,
                        fh  :dateHasta,
                        dias:Ext.getCmp('dInv').getValue(),
                        numOper:Ext.getCmp('num').getValue(),
                        start: 0,
                        limit: 15
                        }
									})
        } else {
            if ( ! Ext.getCmp('dInv').isValid()){
                var dInv = Ext.getCmp('dInv');
                    dInv.focus(); 
            }
            if ( ! Ext.getCmp('fhInv').isValid()){
                var fhInv = Ext.getCmp('fhInv');
                    fhInv.focus();               
            }
            if ( ! Ext.getCmp('fdInv').isValid()){
                var fdInv = Ext.getCmp('fdInv');
                    fdInv.focus();                
            }
            if ( ! Ext.getCmp('num').isValid()){
                var num = Ext.getCmp('num');	
                    num.focus();
            }        
        }//Fin isValid else
      }//FIN Handler
		},{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 10
		}, {
		xtype     :'button',
    text			:'Limpiar',
    iconCls		:'icoLimpiar',
    id				:'btnLimpiar',
    handler		:function(boton, evento){
      Ext.getCmp('num').reset();
		Ext.getCmp('num').setReadOnly(false);
      Ext.getCmp('fdInv').reset();
      Ext.getCmp('fhInv').reset();
      Ext.getCmp('dInv').reset();
      Ext.getCmp('fd').reset();
      Ext.getCmp('fh').reset();
      Ext.getCmp('dias').reset();
      Ext.getCmp('btnGuardar').hide();
      Ext.getCmp('griduno').hide();
		Ext.getCmp('gridEliminar').hide();
       }//FIN handler
		},{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 10
		},{
		xtype     : 'button',
    text			:'Guardar',
    iconCls		:'icoGuardar',
    id				:'btnGuardar',
    hidden    :true,
    handler		:function(boton, evento){
    if ( Ext.getCmp('fd').isValid() & Ext.getCmp('fh').isValid() & Ext.getCmp('dias').isValid() & Ext.getCmp('areaDetalle').isValid()) {
            var fd = Ext.getCmp('fd').getValue();
            var fh = Ext.getCmp('fh').getValue();            
            var dateDesde = Ext.getCmp('fd').formatDate(fd);
            var dateHasta = Ext.getCmp('fh').formatDate(fh);
            Ext.Ajax.request({
            url: '33ParaDiasVencidos01EXT.data.jsp',
            params: {
              informacion:'modificar',
              fd  :dateDesde,
              fh  :dateHasta,
              dias:Ext.getCmp('dias').getValue(),
              areaDetalle:Ext.getCmp('areaDetalle').getValue(),
              numOper: Ext.getCmp('num').getValue()
            },
            callback:procesarInformacion 
          }); 
      } else {
          Ext.Msg.show({
            title: 'Ingrese Informacion',
            msg: ' Este campo es obligatorio !',
            modal: true,
            icon: Ext.Msg.WARNING,
            buttons: Ext.Msg.OK,
            fn: function (){
               if ( ! Ext.getCmp('areaDetalle').isValid()){
                  var areaDetalle = Ext.getCmp('areaDetalle');	
                      areaDetalle.focus();
               }
               if ( ! Ext.getCmp('dias').isValid()){
                  var dias = Ext.getCmp('dias');	
                      dias.focus();
               }
               if ( ! Ext.getCmp('fh').isValid()){
                  var fh = Ext.getCmp('fh');	
                      fh.focus();
               }
               if ( ! Ext.getCmp('fd').isValid()){
                  var fd = Ext.getCmp('fd');	
                      fd.focus();
               }  
              }//FIN fn: function
            });
        }//FIN else
      }
		}]
  },{
  xtype : 'compositefield',
  id    :'capEx',
  hidden: true,
  items: [
	  {
		xtype			: 'displayfield',
		value			: '',
		width			: 100
		},{
		xtype			: 'displayfield',
		value			: 'La parametrizaci�n se guard� Exitosamente.',
		width			: 250
		}]
	},{
  xtype : 'compositefield',
  id    :'numEx',
  hidden: true,
  items: [
	  {
		xtype			: 'displayfield',
		value			: '',
		width			: 120
		},{
		xtype			: 'displayfield',
		value			: 'N�mero de Operaci�n:',
		width			: 150
		},{
		xtype			: 'displayfield',
    id        : 'noOp',
		width			: 180
		}]
	},{
	xtype : 'compositefield',
  id    :'btnCapEx',
  hidden: true,
  items: [
    {
		xtype			: 'displayfield',
		value			: ' ',
		width			: 150
		}, {
		xtype     :'button',
    text			:'Salir',
    iconCls		:'icoCancelar',
    id				:'btnCapSalir',
    width			: 75,
    handler		:function(boton, evento){
        Ext.getCmp('captura').hide();
        Ext.getCmp('capEx').hide(); 
        Ext.getCmp('numEx').hide(); 
        Ext.getCmp('btnCapEx').hide(); 
        Ext.getCmp('noOp').setValue('');
        
        Ext.getCmp('botones').show();        
      }//FIN handler
		},{
		xtype			: 'displayfield',
		value			: ' ',
		width			: 10
		},{
		xtype     : 'button',
    text			:'Regresar',
    iconCls		:'icoRegresar',
    id				:'btnCapRegresar',
    handler		:function(boton, evento){
        Ext.getCmp('capEx').hide(); 
        Ext.getCmp('numEx').hide(); 
        Ext.getCmp('btnCapEx').hide(); 
        Ext.getCmp('noOp').setValue('');
        
        Ext.getCmp('fecha').show();
        Ext.getCmp('diasLimite').show();
        Ext.getCmp('detalle').show();
        Ext.getCmp('botonesCaptura').show();
       }//FIN handler
		}]
  }
]
//-----------------------------Fin Elementos Forma------------------------------

var grupos = new Ext.ux.grid.ColumnHeaderGroup({
  id: 'grupoHeaders',
  rows: [
    [
      {header: '', colspan: 1, align: 'center'},
      {header: 'Fecha de Vencimiento', colspan: 2, align: 'center'},
      {header: '', colspan: 1, align: 'center'},
      {header: '', colspan: 1, align: 'center'},
      {header: '', colspan: 1, align: 'center'},
      {header: '', colspan: 1, align: 'center'},
      {header: '', colspan: 1, align: 'center'},
      {header: '', colspan: 1, align: 'center'}
    ]
  ]
});


var gridEliminar = new Ext.grid.GridPanel({
		id		: 'gridEliminar',
		store	: consultaData2,
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta de par�metros para aplicaci�n del l�mite de vencimiento.',
		hidden: true,
		plugins:	grupos,
		height: 300,
		width: 800,
		columns: [	{	header: 'N�mero de Operaci�n'	},{	header: 'Desde'},{header: 'Hasta'},{	header: 'D�as L�mite de Vencimiento'},{xtype		: 'actioncolumn'	},
						{	header: 'Usuario �ltima modificaci�n'	},{	header: 'Fecha �ltima modificaci�n'	},{ xtype		: 'actioncolumn'	},{ xtype: 'checkcolumn'	}],
		displayInfo: false,		emptyMsg: "No hay registros.",				loadMask: true,		stripeRows: true,		align: 'center',		frame: true		});


var griduno = new Ext.grid.GridPanel({
		id: 'griduno',
		store: consultaData,
    margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta de par�metros para aplicaci�n del l�mite de vencimiento.',
    hidden: true,
		plugins:	grupos,
		height: 300,
		width: 800,
		columns: [
				{
				header: 'N�mero de Operaci�n',
				tooltip: 'N�mero de Operaci�n',
				dataIndex: 'NOPERACION',
				sortable: true,
				resizable: true,
				align: 'center',
        width: 140
			},{
				header: 'Desde',
				tooltip: 'Fecha Vencimiento Desde',
				dataIndex: 'FEC_VEN_INI',
				sortable: true,
				resizable: true,
				align: 'center'
			},{
				header: 'Hasta',
				tooltip: 'Fecha Vencimiento Hasta',
				dataIndex: 'FEC_VEN_FIN',
				sortable: true,
				resizable: true,
				align: 'center'
			},{
				header: 'D�as L�mite de Vencimiento',
				tooltip: 'D�as L�mite de Vencimiento',
				dataIndex: 'DIASVENCIMIENTO',
				sortable: true,
				resizable: true,
				align: 'center',
        width: 140
			},{
				xtype		: 'actioncolumn',
				header	: 'Detalle',
				tooltip	: 'Detalle',	
				width		: 80,							
				align		: 'center',
				items		: [
					{
					getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';							
							return 'iconoLupa';
						},
					handler: procesarVerDetalle
					}
				]
			},{
				header: 'Usuario �ltima modificaci�n',
				tooltip: 'Usuario �ltima modificaci�n',
				dataIndex: 'NOMUSUARIO',
				sortable: true,
				resizable: true,
				align: 'center',
        width: 140
			},{
				header: 'Fecha �ltima modificaci�n',
				tooltip: 'Fecha �ltima modificaci�n',
				dataIndex: 'FEC_MODIFICACION',
				sortable: true,
				resizable: true,
				align: 'center',
        width: 140
			},{
				xtype		: 'actioncolumn',
				header	: 'Modificar',
				tooltip	: 'Modificar',	
				width		: 80,							
				align		: 'center',
				items		: [
					{
					getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Modificar';
							
							return 'modificar';
						},
           handler: procesarModificar
          }
				]
			},{
				xtype: 'checkcolumn',
				header: 'Seleccionar',
				tooltip: 'Seleccionar',
				dataIndex: 'SELECCIONAR',
				sortable: false,
				//width: 50,			
				resizable: false,									
				align: 'center'
			}
		],
    displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
    align: 'center',
		frame: true,
    bbar: {
			xtype: 'paging',	
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
        {
					xtype: 'button',
					text: 'Eliminar',					
					tooltip:	'Eliminar',
					iconCls: 'icoEliminar',
					id: 'btnEliminar',
					handler: function(boton, evento) {          
            var existeSeleccion = false;
						consultaData.each(function(record){
							if ( record.data['SELECCIONAR'] ){
                  existeSeleccion = true;
                }
            });
            if(existeSeleccion){
              Ext.Msg.show({
              title: 'Eliminar Parametrizaci�n',
              msg: ' �Esta seguro de querer eliminar la parametrizaci�n seleccionada?',
              modal: true,
              icon: Ext.Msg.QUESTION,
              buttons: Ext.Msg.OKCANCEL,
              fn: function (btn){
                  if (btn == "ok" || btn == "yes" ){
                     var registrosEnviar = [];
                     var numEli=		new Array();
                     consultaData.each(function(record){
                      if ( record.data['SELECCIONAR'] ){
                          numEli.push(record.data['NOPERACION']); 
                          }
                      });
                     Ext.Ajax.request({
                        url: '33ParaDiasVencidos01EXT.data.jsp',
                        params: {
                          informacion:'eliminar',
                          numOperEli:numEli
                        },
                      callback:descargaInfoEliminar //es una funcion
                      });
                  }
                  if (btn=="cancel"){ }
                }//FIN function
              });
						}else{
               Ext.Msg.show({
                title: 'Eliminar Parametrizaci�n',
                msg: ' Seleccione al menos una opcion a eliminar.',
                modal: true,
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
                });
              }//FIN else
            }//FIN handler
          }	,	{
					xtype : 'button',
					id    : 'btnExportar',
					text  : 'Exportar',					
					tooltip:	'Exportar',
					iconCls: 'icoXls',					
					handler: function(boton, evento) {
						var barraPaginacionR = Ext.getCmp("barraPaginacion");
						boton.disable();
						boton.setIconClass('loading-indicator');
            var fdInv = Ext.getCmp('fdInv').getValue();
            var fhInv = Ext.getCmp('fhInv').getValue();            
            var dateDesde = Ext.getCmp('fdInv').formatDate(fdInv);
            var dateHasta = Ext.getCmp('fhInv').formatDate(fhInv);
            var fp=Ext.getCmp('forma');
            fp.el.mask('Exportando...', 'x-mask-loading');	
						Ext.Ajax.request({
							url: '33ParaDiasVencidos01EXT.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
                        informacion: 'ArchivoCSV'	,
                      //  operacion: 'Generar',
                        fd  :dateDesde,
                        fh  :dateHasta,
                        dias:Ext.getCmp('dInv').getValue(),
                        numOper:Ext.getCmp('num').getValue()//,
                       // start: 0,
                       // limit: 15
						}),					
						callback: procesarArchivoSuccess 
						});
						
					}
				}, {
					xtype : 'button',
          id    : 'btnImprimir',
					text  : 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',					
					handler: function(boton, evento) {
						var barraPaginacionR = Ext.getCmp("barraPaginacion");
						boton.disable();
						boton.setIconClass('loading-indicator');
            var fdInv = Ext.getCmp('fdInv').getValue();
            var fhInv = Ext.getCmp('fhInv').getValue();            
            var dateDesde = Ext.getCmp('fdInv').formatDate(fdInv);
            var dateHasta = Ext.getCmp('fhInv').formatDate(fhInv);
						Ext.Ajax.request({
							url: '33ParaDiasVencidos01EXT.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoPDF',
              fd  :dateDesde,
              fh  :dateHasta,
              dias:Ext.getCmp('dInv').getValue(),
              numOper:Ext.getCmp('num').getValue()
						}),					
							callback: procesarArchivoSuccess
						});
						
					}
				}, {
					xtype : 'button',
          id    : 'btnEG',
					text  : 'Salir',					
					tooltip:	'Salir',
					iconCls: 'icoContinuar',					
					handler: function(boton, evento) {
            Ext.getCmp('griduno').hide();
            Ext.getCmp('botonesConsulta').hide();
            Ext.getCmp('btnGuardar').hide();
            Ext.getCmp('detalle').hide();
            Ext.getCmp('areaDetalle').reset();            
            Ext.getCmp('diasLimite').hide();
            Ext.getCmp('dias').reset();
            Ext.getCmp('dLInv').hide();
            Ext.getCmp('dInv').reset();
            Ext.getCmp('fecha').hide();
            Ext.getCmp('fd').reset();
            Ext.getCmp('fh').reset();
            Ext.getCmp('fInv').hide();
            Ext.getCmp('fdInv').reset();
            Ext.getCmp('fhInv').reset();
            Ext.getCmp('numOper').hide();
            Ext.getCmp('num').reset();
            Ext.getCmp('consulta').hide();
            Ext.getCmp('botones').show();
					} // FIN handler
				}	// fin xtype				
			]
		}
	});

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id					:'forma',
		width				: 600,
		heigth			:'auto',
		title				:'Parametrizaci�n de dias de Vencimiento.',
		layout			:'form',
		frame				:true,
		collapsible		:true,
		titleCollapse	:false,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 6px',
		labelWidth		:10,
		monitorValid	:true,	
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosForma
							]
	})
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		: 949,
		items		:[
					NE.util.getEspaciador(20),
					fp,
					NE.util.getEspaciador(20),
					griduno,
					NE.util.getEspaciador(20),
					gridEliminar,
          NE.util.getEspaciador(20)
					]
	})
//-----------------------------Fin Contenedor Principal-------------------------
//-----------------------------descargaInfo------------------------------
	function descargaInfo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
      Ext.getCmp('miPrograma').setValue('<center>Programa: '+info.descPro+'</center>');
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
//---------------------------Fin descargaInfo----------------------------
Ext.Ajax.request({
  url 				: '33ParaDiasVencidos01EXT.data.jsp',
  params      : {
                  informacion:'descPro'
                },
                callback:descargaInfo
}); 
})//-----------------------------------------------Fin Ext.onReady(function(){}