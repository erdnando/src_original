<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.io.*,
		com.netro.pdf.*,
		netropology.utilerias.usuarios.*,
		com.netro.fondojr.*,
		com.netro.fondosjr.ControlIFJ,
		com.netro.exception.*, 
		com.netro.cadenas.*,
		com.netro.descuento.*,
		com.netro.seguridad.*,
		com.netro.afiliacion.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  			
		com.netro.model.catalogos.*"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
	
	
	String infoRegresar="", consulta="",mensajeError ="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
if(informacion.equals("Consultar")){
	JSONObject 	resultado	= new JSONObject();
	List data = new ArrayList();
	boolean hayError = false;
	List parametros = new ArrayList();
	
		String fechaMovimiento  = (request.getParameter("fechaMovimiento")!=null)?request.getParameter("fechaMovimiento"):""; 
		String clavePrograma =(String)request.getSession().getAttribute("cveProgramaFondoJR");
		FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
	
		if(informacion.equals("Consultar")  )  {
			try {
				
				data = fondoJunior.ControlFondoJR(fechaMovimiento, clavePrograma);
				HashMap reg = new HashMap();
				int totalReg=0;
				double total =0.0;
				for(int i = 0; i<data.size(); i++){
				reg = new HashMap();
				String d=data.get(i).toString();
				String  dataAux[]=d.split(",");
				String tipoTrans="";
					for(int j = 0; j<dataAux.length;j++){
					String valor=dataAux[j];
					String campo ="";
						if(j==0){
							campo ="Transaccion";
							tipoTrans=valor.toString();
						}else	if(j==(dataAux.length-1)){
							campo ="Total_Registros";
							totalReg+=Integer.parseInt(valor.toString().replaceAll("]","").trim());
							valor = valor.toString().replaceAll("]","");
							valor = Comunes.formatoDecimal(valor,0);
							System.err.println(totalReg);
						}else{
							campo ="Total_Transaccion";
							if(tipoTrans.equals("[Cancelaciones") || tipoTrans.equals("[No Pagados") ){
								total=total-Double.parseDouble((String)valor);
							}else{
								total=total+Double.parseDouble((String)valor);
							}
							System.err.println(total);
							valor = "$"+Comunes.formatoDecimal(valor,2,true);
						}
						reg.put(campo,valor);
					}
					parametros.add(reg);
				}
				if(data.size()>0){
					reg = new HashMap();
					reg.put("Transaccion","<b>Saldo Disponible</b>");
					reg.put("Total_Transaccion","$"+Comunes.formatoDecimal(total,2,true)+"");
					reg.put("Total_Registros",Comunes.formatoDecimal(totalReg,0)+"");
					parametros.add(reg);
				}	
					
				 consulta = "{\"success\": true, \"total\": \"" + reg.size() + "\", \"registros\": " + parametros+"}";
				 //System.err.println(consulta);
				 jsonObj.put("registros", parametros);
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		infoRegresar=jsonObj.toString();
	}	
}else if(informacion.equals("ArchivoCSV")   ||   informacion.equals("ArchivoPDF")){
	String fechaMovimiento  = (request.getParameter("fechaMovimiento")!=null)?request.getParameter("fechaMovimiento"):""; 
	String clavePrograma =(String)request.getSession().getAttribute("cveProgramaFondoJR");
	String nombrePrograma = (String)request.getSession().getAttribute("descProgramaFondoJR");
	
	ControlIFJ clase = new ControlIFJ();
	clase.setFechaMovimiento(fechaMovimiento);
	clase.setNombrePrograma(nombrePrograma);
	clase.setClavePrograma(clavePrograma);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( clase);
	if(informacion.equals("ArchivoCSV")){
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if(informacion.equals("ArchivoPDF")){
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
	infoRegresar=jsonObj.toString();
	
}

%>


<%=infoRegresar%>


