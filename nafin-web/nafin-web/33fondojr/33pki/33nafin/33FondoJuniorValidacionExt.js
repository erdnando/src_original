

dynamicGridVenc = Ext.extend(Ext.grid.GridPanel,{
	storeInfoDynamic: null,
	totalReg: null,
	margins: '20 0 0 0',
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [	
		{//1
			header: 'Fecha de Amortizaci�n Nafin',
			tooltip: 'Fecha de Amortizaci�n Nafin',
			dataIndex: 'fecha_amort_nafin',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false,
			align: 'center'
		},
		{//2
			header: 'Fecha de Operaci�n',
			tooltip: 'Fecha de Operaci�n',
			dataIndex: 'fecha_operacion',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center'
			
		},
		{//2
			header: 'Fecha de Pago Cliente FIDE',
			tooltip: 'Fecha de Pago Cliente FIDE',
			dataIndex: 'fecha_pag_clfide',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center'
			
		},
		{//4
			header: 'Pr�stamo',
			tooltip: 'Pr�stamo',
			dataIndex: 'numero_prestamo',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{//5
			header : 'N�m. Cliente SIRAC',
			tooltip: 'Num. Cliente SIRAC',
			dataIndex : 'numero_sirac',
			width : 150,
			sortable : true
		},
		{//6
			header : 'Cliente',
			tooltip: 'Cliente',
			dataIndex : 'nombre_cliente',
			width : 150,
			sortable : true
		},
		{//7
			header : 'N�m. Cuota a Pagar',
			tooltip: 'Num. Cuota a Pagar',
			dataIndex : 'numero_cuota',
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//8
			header : 'Capital',
			tooltip: 'Capital',
			dataIndex : 'monto_capital',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//9
			header : 'Intereses',
			tooltip: 'Intereses',
			dataIndex : 'monto_interes',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//10
			header : 'Intereses Moratorios',
			tooltip: 'Intereses Moratorios',
			dataIndex : '',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//10
			header : 'Total a Pagar',
			tooltip: 'Total a Pagar',
			dataIndex : 'monto_total',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//11
			header : 'Campo 1',
			tooltip: 'Adicional 1',
			dataIndex : 'CAMPO0',
			sortable : true,
			hidden: true,
			width : 100,
			align: 'center'
		},
		{//12
			header : 'Estatus del Registro',
			tooltip: 'Estatus del Registro',
			dataIndex : 'estatus_prestamo',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//13
			header : 'Origen',
			tooltip: 'Origen',
			dataIndex : 'origen_prestamo',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//14
			header : 'Estatus de la Validaci�n',
			tooltip: 'Estatus de la Validaci�n',
			dataIndex : 'estatus_validacion',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 250,
	style: 'margin:0 auto;',
	width: 750,
	title: 'Pagados',
	frame: true,
	initComponent : function(){
		 
		 Ext.apply(this, {
			store:  new Ext.data.JsonStore({
					root : 'registros',
					totalProperty : 'totalCount',
					fields: [
						{name: 'fecha_amort_nafin'},
						{name: 'fecha_operacion'},
						{name: 'fecha_pag_clfide'},
						{name: 'numero_prestamo'},
            {name: 'nombre_cliente'},
						{name: 'numero_sirac'},
						{name: 'numero_cuota'},
						{name: 'monto_capital', type: 'float'},
						{name: 'monto_interes', type: 'float'},
						{name: 'monto_total'},
						{name: 'estatus_prestamo'},
						{name: 'origen_prestamo'},
						{name: 'estatus_validacion'}
					],
					autoLoad: false,
					listeners: {
						load:  function (store, records, success, operation, options) {
                 //store.loadData(records.slice(0, 2));
								 //store.loadData(records);
						},
						exception: {
							fn: function(proxy, type, action, optionsRequest, response, args) {
								NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
								//procesarConsultaData(null, null, null);
							}
						}
					}
				})
				
			//store: this.storeDynamic
		});
    
		this.totalReg = this.storeInfoDynamic.registros.length;
    
    if(this.totalReg>500){
      var registros = [];
      for	(index = 0; (index < this.storeInfoDynamic.registros.length && index<501); index++) {
        registros.push(this.storeInfoDynamic.registros[index]);
      }
      var iniRegistros = {registros:registros};
      this.store.loadData(iniRegistros);
    }else{
      this.store.loadData(this.storeInfoDynamic);
    }
    
		
		dynamicGridVenc.superclass.initComponent.call(this);
	},
	setHandlerBtnAceptar: function(fn){
		var btnLoginAcept = Ext.getCmp('btnRevisar')
			btnLoginAcept.setHandler(fn);
	}
});


Ext.onReady(function() {
	var tot_reg_p=0, tot_monto_p=0.0;
	var tot_reg_np=0, tot_monto_np=0.0;
	var tot_reg_r=0, tot_monto_r=0.0;
	var tot_reg_t=0, tot_monto_t=0.0;
	var tot_reg_rc=0, tot_monto_rc=0.0;
	var strDirectorioPublicacion = '';
	var tipo_validacion = '';
	var seccion_estatus = '';
	var fecha_amortizacion = '';
  
  function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}
	
	var consultar = function(){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var arrayFSet = contenedorPrincipalCmp.findByType('fieldset');
		
    if(verificaFechas('fecha_pago_cliente_fide_ini','fecha_pago_cliente_fide_fin') && verificaFechas('fecha_registro_fide_ini','fecha_registro_fide_fin')){
    
      if(restaFechas(Ext.getCmp('fecha_amort_nafin_ini').value, Ext.getCmp('fecha_amort_nafin_fin').value)>7 ){
        Ext.getCmp('fecha_amort_nafin_fin').markInvalid('El valor de el rango de Fechas de Amortizaci�n debe ser m�ximo de 7 d�as');
        return ;
      }
      
      for(var i=0;i<arrayFSet.length;i++){
        contenedorPrincipalCmp.remove(arrayFSet[i]);
        contenedorPrincipalCmp.doLayout();
      }
      
      pnl.el.mask('Consultando...', 'x-mask-loading');
      Ext.Ajax.request({
        url: '33FondoJuniorValidacionExt.data.jsp',
        params: Ext.apply(fp.getForm().getValues(),{
          informacion: 'fondoJuniorValidacionInicializar'
        }),
        callback: procesarSuccessConsultar
      });
    }
	}
	
	var validaRembAlDia = function(){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var arrayFSet = contenedorPrincipalCmp.findByType('fieldset');
		
		for(var i=0;i<arrayFSet.length;i++){
			contenedorPrincipalCmp.remove(arrayFSet[i]);
			contenedorPrincipalCmp.doLayout();
		}
		pnl.el.mask('Procesando...', 'x-mask-loading');
		Ext.getCmp('cmb_estatus_registro1').setValue('R');
		Ext.Ajax.request({
			url: '33FondoJuniorValidacionExt.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'fondoJuniorValidacionConsultarReemb'
			}),
			callback: procesarSuccessConsultar
		});
	}
	
	var generaPreAcuse = function(idFieldSet, idTotaleXfec, idToolBar, tipoPreAcuse, estatusValid){
		fp.hide();
		Ext.getCmp(idToolBar).hide();
		var txtTipoValid = estatusValid=='A'?'Aceptados':'Rechazados';
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var arrayFSet = contenedorPrincipalCmp.findByType('fieldset');
		for(var i=0;i<arrayFSet.length;i++){
			if(idFieldSet!=arrayFSet[i].getId()){
				contenedorPrincipalCmp.remove(arrayFSet[i]);
			}else{
				fecha_amortizacion = arrayFSet[i].fecha_amortizacion;
			}
		}
		contenedorPrincipalCmp.doLayout();
		
		var storeGridTot = Ext.getCmp(idTotaleXfec).getStore();
		storeGridTot.each(function(record){
			if (record.data['estatus_regs']=='P' && Number(record.data['tot_registros'])>0 ){
				tot_reg_p =record.data['tot_registros']; tot_monto_p = record.data['tot_a_pagar'];}
			if (record.data['estatus_regs']=='NP' && Number(record.data['tot_registros'])>0 ){
				tot_reg_np =record.data['tot_registros']; tot_monto_np = record.data['tot_a_pagar'];}
			if (record.data['estatus_regs']=='R' && Number(record.data['tot_registros'])>0 ){
				tot_reg_r =record.data['tot_registros']; tot_monto_r = record.data['tot_a_pagar'];}
			if (record.data['estatus_regs']=='T' && Number(record.data['tot_registros'])>0 ){
				tot_reg_t =record.data['tot_registros']; tot_monto_t = record.data['tot_a_pagar'];}
			if (record.data['estatus_regs']=='RC' && Number(record.data['tot_registros'])>0 ){
				tot_reg_rc =record.data['tot_registros']; tot_monto_rc = record.data['tot_a_pagar'];}
			
		})
		
		var dataTotalesPreAcu = [
					['No. Total de Cr�ditos '+txtTipoValid, tot_reg_p, tot_reg_np, tot_reg_r, tot_reg_t, tot_reg_rc ],
					['Monto Total de Cr�ditos '+txtTipoValid, tot_monto_p, tot_monto_np, tot_monto_r, tot_monto_t, tot_monto_rc ]
				];
		storePreAcuseValid.loadData(dataTotalesPreAcu);
		gridPreAcuseValid.setTitle('Preacuse Validaci�n: '+(estatusValid=='A'?'Aceptaci�n':'Rechazo'));
		var gridColumnMod = gridPreAcuseValid.getColumnModel();
		if(tipoPreAcuse == 'RTRF'){
			gridColumnMod.setHidden(1,true);
			gridColumnMod.setHidden(2,true);
			gridPreAcuseValid.setSize(560,120);
			pnlMensaje.setWidth(560);
		}else if(tipoPreAcuse == 'PNP'){
			gridColumnMod.setHidden(3,true);
			gridColumnMod.setHidden(4,true);
			gridColumnMod.setHidden(5,true);
			gridPreAcuseValid.setSize(420,120);
			pnlMensaje.setWidth(420);
		}
		
		seccion_estatus = tipoPreAcuse;
		tipo_validacion = (estatusValid=='A'?'ACEPTAR':'RECHAZAR') ;
		contenedorPrincipalCmp.insert(0,gridPreAcuseValid );
		contenedorPrincipalCmp.insert(1,pnlMensaje );
		contenedorPrincipalCmp.doLayout();
		
		
		window.scrollTo(0, 0);
		
		//genera archivo preacuse
		Ext.getCmp('btnGenArchPreAcu').setHandler(function(btn){
			btn.setIconClass('loading-indicator');
			btn.disable();
			Ext.Ajax.request({
				url: '33FondoJuniorValidacionArchivosExt.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'descargaArchivosValidacion',
					tipo_validacion: tipo_validacion,
					tipo_archivo:'PREACUSE_PDF',
					seccion_estatus: seccion_estatus,
					fecha_amortizacion: fecha_amortizacion
				}),
				callback: function(opts, success, response){
					procesarSuccessGeneraArchivos(opts, success, response, btn);
				}
			});
		});
		
	}
	
	var crearGridVenc = function(objAllInfoVenc, isValidReembXdia, csReembTodo){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var reg={registros:null, totalCount:0};
		var reg2={registros:null};
		var gridDynam = null;
		var csPrimero = true;
    var csExisteInfo = false;
		
		for(var i=3; i<objAllInfoVenc.length; i++){
			var infoVencXfecha = objAllInfoVenc[i];
			
			if(infoVencXfecha[1].length>0 || infoVencXfecha[2].length>0 ){
				var tot_reg_p = 0, tot_mc_p = 0.0, tot_int_p = 0.0, tot_pagar_p = 0.0;
				
				var tot_reg_pnp = 0;
				var tot_mc_pnp = 0.0;
				var tot_int_pnp = 0.0;
				var tot_pagar_pnp = 0.0;
				
				var fieldSetFecVenc = new Ext.form.FieldSet({
					//checkboxToggle:true,
					id:'fsVencPNP'+i,
					title: 'Fecha de Vencimiento: '+infoVencXfecha[0],
					autoHeight:true,
					collapsed: csPrimero?false:true,
					collapsible: true,
					style: 'margin:0 auto;',
					width: 800,
					frame:true,
					fecha_amortizacion:infoVencXfecha[0]
					/*buttons: [
						{
							text: 'Aceptar Validaci�n',
							//iconCls: 'icoBuscar',
							id:'btnAceptarVal'+i
							//formBind: true,
						}
					]*/
				});
				
				
				
				if(infoVencXfecha[1].length>0){//pagados
					reg.registros=infoVencXfecha[1];
					reg.totalCount=infoVencXfecha[1].length;
					reg2.registros=infoVencXfecha[2];
					//gridDynam = new dynamicGridVenc({title:'<center>Pagados</center>', id:'gridPagos1'+i, storeInfoDynamic:reg, storeInfo2:reg2});
          gridDynam = new dynamicGridVenc({title:'<center>Pagados</center>', id:'gridPagos1'+i, storeInfoDynamic:reg});
					addGridVenc(fieldSetFecVenc,gridDynam);
          csExisteInfo = true;
					
					//var storeV = gridDynam.getStore();
					var registros = reg.registros;
          
          for(var x=0; x<registros.length; x++){
            tot_reg_p = Number(tot_reg_p)+1;
						tot_mc_p = Number(tot_mc_p) + Number(registros[x].monto_capital);
						tot_int_p = Number(tot_int_p) + Number(registros[x].monto_interes);
						tot_pagar_p = Number(tot_pagar_p) + Number(registros[x].monto_total);
          }
					
          /*storeV.each(function(record) {
						tot_reg_p = Number(tot_reg_p)+1;
						tot_mc_p = Number(tot_mc_p) + Number(record.data['monto_capital']);
						tot_int_p = Number(tot_int_p) + Number(record.data['monto_interes']);
						tot_pagar_p = Number(tot_pagar_p) + Number(record.data['monto_total']);
					});*/
					
					
				}
				if(infoVencXfecha[2].length>0){//no pagados
					reg.registros=infoVencXfecha[2];
					gridDynam = new dynamicGridVenc({title:'<center>No Pagados</center>',id:'gridNoPagos2'+i, storeInfoDynamic:reg});
					addGridVenc(fieldSetFecVenc,gridDynam);
          csExisteInfo = true;
					
					//var storeV = gridDynam.getStore();
          var registros = reg.registros;
					
          for(var x=0; x<registros.length; x++){
            tot_reg_pnp = Number(tot_reg_pnp)+1;
						tot_mc_pnp = Number(tot_mc_pnp) + Number(registros[x].monto_capital);
						tot_int_pnp = Number(tot_int_pnp) + Number(registros[x].monto_interes);
						tot_pagar_pnp = Number(tot_pagar_pnp) + Number(registros[x].monto_total);
          }
					/*storeV.each(function(record) {
						tot_reg_pnp = Number(tot_reg_pnp)+1;
						tot_mc_pnp = Number(tot_mc_pnp) + Number(record.data['monto_capital']);
						tot_int_pnp = Number(tot_int_pnp) + Number(record.data['monto_interes']);
						tot_pagar_pnp = Number(tot_pagar_pnp) + Number(record.data['monto_total']);
					});*/
				}

				
				var gridTotalesPNP = new Ext.grid.GridPanel({
					id:'gridTotalesPNP'+i,
					store:  new Ext.data.ArrayStore({
											fields: [
												{name: 'txtTotal'},
												{name: 'estatus_regs'},
												{name: 'tot_registros'},
												{name: 'tot_capital', type: 'float'},
												{name: 'tot_intereses', type: 'float'},
												{name: 'tot_interes_moratorio', type: 'float'},
												{name: 'tot_a_pagar', type: 'float'}
											]
										}),
					stripeRows: true,
					columnLines : true,
					loadMask: true,
					height: 120,
					style: 'margin:0 auto;',
					width: 750,
					frame: true,
					columns: [	
						{
							header: ' ', dataIndex: 'txtTotal', sortable: true, width: 200, resizable: true, hidden: false, align: 'left'
						},
						{
							header: '<center>Capital</center>', tooltip: 'Capita', dataIndex: 'tot_capital', sortable: true, hideable: false, width: 130, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: '<center>Intereses</center>', tooltip: 'Intereses', dataIndex: 'tot_intereses', sortable: true, hideable: false, width: 130, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: '<center>Intereses Moratorios</center>', tooltip: 'Intereses Moratorios', dataIndex: 'tot_interes_moratorio', sortable: true, hideable: false, width: 130, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: '<center>Total a Pagar</center>', tooltip: 'Total a Pagar', dataIndex: 'tot_a_pagar', sortable: true, hideable: false, width: 130, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
						}
					],
					bbar: {
					xtype: 'toolbar',
					id: 'toolBarPNP'+i,
					items: [
						'->',
						'-',
						{
							text: 'Aceptar Validaci�n',
							id: 'btnAceptarPNP'+i,
							idFieldSet:'fsVencPNP'+i,
							idTotaleXfec: 'gridTotalesPNP'+i,
							idToolBar: 'toolBarPNP'+i,
							handler: function(btn){
								generaPreAcuse(btn.idFieldSet, btn.idTotaleXfec, btn.idToolBar,'PNP', 'A');
							}
						},
						'-',
						{
							text: 'Rechazar Validaci�n',
							id: 'btnRechazarPNP'+i,
							idFieldSet:'fsVencPNP'+i,
							idTotaleXfec: 'gridTotalesPNP'+i,
							idToolBar: 'toolBarPNP'+i,
							handler: function(btn){
								generaPreAcuse(btn.idFieldSet, btn.idTotaleXfec, btn.idToolBar,'PNP', 'R');
							}
							//hidden: true
							//handler: 
						},
						'-',
						{
						text: 'Generar Archivo',
						id: 'btnAbrirPdfPNP'+i,
						idFieldSet:'fsVencPNP'+i,
						handler: function(btn){
								btn.setIconClass('loading-indicator');
								btn.disable();
								Ext.Ajax.request({
									url: '33FondoJuniorValidacionArchivosExt.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'descargaArchivosValidacion',
										//tipo_validacion: tipo_validacion,
										csReembTodo: csReembTodo,
										tipo_archivo:'CONS_CSV',
										seccion_estatus: 'PNP',
										fecha_amortizacion: Ext.getCmp(btn.idFieldSet).fecha_amortizacion
									}),
									callback: function(opts, success, response){
										procesarSuccessGeneraArchivos(opts, success, response, btn);
									}
								});

							}
						//hidden: true
						//handler: 
						}
						
					]
				}
				});
				
				var dataTotales = [
					['Total Pagado', 'P', tot_reg_p, tot_mc_p, tot_int_p, '', tot_pagar_p ],
					['Total No Pagado', 'NP', tot_reg_pnp, tot_mc_pnp, tot_int_pnp, '', tot_pagar_pnp ]
				];
				
				
				/*if(csPrimero){
					fieldSetFecVenc.expand();
				}
				*/
				csPrimero = false;
				
				gridTotalesPNP.getStore().loadData(dataTotales);
				addGridVenc(fieldSetFecVenc,gridTotalesPNP);
				contenedorPrincipalCmp.add(fieldSetFecVenc);
				contenedorPrincipalCmp.add(NE.util.getEspaciador(20));
				contenedorPrincipalCmp.doLayout();
			}
			
			pnl.el.unmask();
		}
		
		//reembolsos, recuperaciones, prepagos
		if(objAllInfoVenc[0].length>0 || objAllInfoVenc[1].length>0 || objAllInfoVenc[2].length>0 ){
			
			var tot_reg_reemb = 0;
			var tot_mc_reemb = 0.0;
			var tot_int_reemb = 0.0;
			var tot_pagar_reemb = 0.0;
			
			var tot_reg_prep = 0;
			var tot_mc_prep = 0.0;
			var tot_int_prep = 0.0;
			var tot_pagar_prep = 0.0;
			
			var tot_reg_recup = 0;
			var tot_mc_recup = 0.0;
			var tot_int_recup = 0.0;
			var tot_pagar_recup = 0.0;
			
			if(isValidReembXdia){
				fp.hide();
			}
			
			var fieldSetFecVencRPR = new Ext.form.FieldSet({
						//checkboxToggle:true,
						id:'fsVencRPR',
						title: isValidReembXdia?'Reembolsos':'Reembolsos, Prepagos Totales, Recuperaci�n de Fondo Jr',
						autoHeight:true,
						style: 'margin:0 auto;',
						width: 800,
						collapsed: csPrimero?false:true,
						collapsible: true,
						frame:true
					});
					
			if(objAllInfoVenc[0].length>0){//reembolsos
				reg.registros=objAllInfoVenc[0];
				gridDynam = new dynamicGridVenc({title:'<center>Reembolsos</center>',id:'gridReemb0', storeInfoDynamic:reg});
        csExisteInfo = true;
				//addGridVenc(gridDynam);
				/*var storeV = gridDynam.getStore();
					
				storeV.each(function(record) {
					//alert(record);
					tot_reg_reemb = Number(tot_reg_reemb)+1;
					tot_mc_reemb = Number(tot_mc_reemb) + Number(record.data['monto_capital']);
					tot_int_reemb = Number(tot_int_reemb) + Number(record.data['monto_interes']);
					tot_pagar_reemb = Number(tot_pagar_reemb) + Number(record.data['monto_total']);
				});*/
        
        var registros = reg.registros;
					
        for(var x=0; x<registros.length; x++){
          tot_reg_reemb = Number(tot_reg_reemb)+1;
          tot_mc_reemb = Number(tot_mc_reemb) + Number(registros[x].monto_capital);
          tot_int_reemb = Number(tot_int_reemb) + Number(registros[x].monto_interes);
          tot_pagar_reemb = Number(tot_pagar_reemb) + Number(registros[x].monto_total);
        }
				
				addGridVenc(fieldSetFecVencRPR,gridDynam);
			}
			if(objAllInfoVenc[1].length>0){//prepagos
				reg.registros=objAllInfoVenc[1];
				gridDynam = new dynamicGridVenc({title:'<center>Prepagos Totales</center>',id:'gridPrepago1', storeInfoDynamic:reg});
        csExisteInfo = true;
				//addGridVenc(gridDynam);
				/*var storeV = gridDynam.getStore();
					
				storeV.each(function(record) {
					tot_reg_prep = Number(tot_reg_prep)+1;
					tot_mc_prep = Number(tot_mc_prep) + Number(record.data['monto_capital']);
					tot_int_prep = Number(tot_int_prep) + Number(record.data['monto_interes']);
					tot_pagar_prep = Number(tot_pagar_prep) + Number(record.data['monto_total']);
				});*/
        
        var registros = reg.registros;
					
        for(var x=0; x<registros.length; x++){
          tot_reg_prep = Number(tot_reg_prep)+1;
          tot_mc_prep = Number(tot_mc_prep) + Number(registros[x].monto_capital);
          tot_int_prep = Number(tot_int_prep) + Number(registros[x].monto_interes);
          tot_pagar_prep = Number(tot_pagar_prep) + Number(registros[x].monto_total);
        }
				
				addGridVenc(fieldSetFecVencRPR,gridDynam);
			}
			if(objAllInfoVenc[2].length>0){//recuperaciones
				reg.registros=objAllInfoVenc[2];
				gridDynam = new dynamicGridVenc({title:'<center>Recuperaci�n Fondo Jr</center>',id:'gridRecup2', storeInfoDynamic:reg});
        csExisteInfo = true;
				/*var storeV = gridDynam.getStore();
					
				storeV.each(function(record) {
					tot_reg_recup = Number(tot_reg_recup)+1;
					tot_mc_recup = Number(tot_mc_recup) + Number(record.data['monto_capital']);
					tot_int_recup = Number(tot_int_recup) + Number(record.data['monto_interes']);
					tot_pagar_recup = Number(tot_pagar_recup) + Number(record.data['monto_total']);
				});*/
        
        var registros = reg.registros;
					
        for(var x=0; x<registros.length; x++){
          tot_reg_recup = Number(tot_reg_recup)+1;
          tot_mc_recup = Number(tot_mc_recup) + Number(registros[x].monto_capital);
          tot_int_recup = Number(tot_int_recup) + Number(registros[x].monto_interes);
          tot_pagar_recup = Number(tot_pagar_recup) + Number(registros[x].monto_total);
        }
        
				//addGridVenc(gridDynam);
				addGridVenc(fieldSetFecVencRPR,gridDynam);
			}
			
			var gridTotalesRPR = new Ext.grid.GridPanel({
					id:'gridTotalesRPR',
					store:  new Ext.data.ArrayStore({
						fields: [
							{name: 'txtTotal'},
							{name: 'estatus_regs'},
							{name: 'tot_registros'},
							{name: 'tot_capital', type: 'float'},
							{name: 'tot_intereses', type: 'float'},
							{name: 'tot_interes_moratorio', type: 'float'},
							{name: 'tot_a_pagar', type: 'float'}
						]
					}),
					stripeRows: true,
					columnLines : true,
					loadMask: true,
					height: 140,
					style: 'margin:0 auto;',
					width: 750,
					frame: true,
					columns: [	
						{
							header: ' ', dataIndex: 'txtTotal', sortable: true, width: 200, resizable: true, hidden: false, align: 'left'
						},
						{
							header: '<center>Capital</center>', tooltip: 'Capita', dataIndex: 'tot_capital', sortable: true, hideable: false, width: 130, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: '<center>Intereses</center>', tooltip: 'Intereses', dataIndex: 'tot_intereses', sortable: true, hideable: false, width: 130, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: '<center>Intereses Moratorios</center>', tooltip: 'Intereses Moratorios', dataIndex: 'tot_interes_moratorio', sortable: true, hideable: false, width: 130, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: '<center>Total a Pagar</center>', tooltip: 'Total a Pagar', dataIndex: 'tot_a_pagar', sortable: true, hideable: false, width: 130, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
						}
					],
					bbar: {
					xtype: 'toolbar',
					id: 'toolBarRPR',
					items: [
						'->',
						'-',
						{
							text: 'Aceptar Validaci�n',
							id: 'btnAceptarRPR'+this.id,
							idFieldSet:'fsVencRPR',
							idTotaleXfec: 'gridTotalesRPR',
							idToolBar: 'toolBarRPR',
							handler: function(btn){
								generaPreAcuse(btn.idFieldSet, btn.idTotaleXfec, btn.idToolBar, 'RTRF', 'A');
							}
						},
						'-',
						{
						text: 'Rechazar Validaci�n',
						id: 'btnRechazarRPR'+this.id,
						idFieldSet:'fsVencRPR',
						idTotaleXfec: 'gridTotalesRPR',
						idToolBar: 'toolBarRPR',
						handler: function(btn){
							generaPreAcuse(btn.idFieldSet, btn.idTotaleXfec, btn.idToolBar, 'RTRF', 'R');
						}
						//hidden: true
						//handler: 
						},
						'-',
						{
						text: 'Generar Archivo',
						id: 'btnAbrirPdfRPR'+this.id,
						handler:function(btn){
							btn.setIconClass('loading-indicator');
							btn.disable();
							Ext.Ajax.request({
								url: '33FondoJuniorValidacionArchivosExt.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'descargaArchivosValidacion',
									csReembTodo: csReembTodo,
									tipo_archivo:'CONS_CSV',
									seccion_estatus: 'RTRF'
								}),
								callback: function(opts, success, response){
										procesarSuccessGeneraArchivos(opts, success, response, btn);
									}
							});
						}
						//hidden: true
						//handler: 
						}
						
					]
				}
				});
				
				var dataTotales = [
					['Total Rembolsos', 'R', tot_reg_reemb, tot_mc_reemb, tot_int_reemb, '', tot_pagar_reemb ],
					['Total Prepagos Totales', 'T', tot_reg_prep, tot_mc_prep, tot_int_prep, '', tot_pagar_prep ],
					['Total Recuperaciones Fondo Jr', 'RC', tot_reg_recup, tot_mc_recup, tot_int_recup, '', tot_pagar_recup ]
				];
				gridTotalesRPR.getStore().loadData(dataTotales);
				addGridVenc(fieldSetFecVencRPR,gridTotalesRPR);
			
			contenedorPrincipalCmp.add(fieldSetFecVencRPR);
			contenedorPrincipalCmp.doLayout();
			/*if(csPrimero){
				fieldSetFecVencRPR.expand();
			}*/
			
			csPrimero = false;
		}
    
    
    if(!csExisteInfo){
      pnlMsgSinInfo.show();
    }
	}
	
	var addGridVenc = function(ObjFielSet, nGridDynamic){
		ObjFielSet.add(nGridDynamic);
    if(Number(nGridDynamic.totalReg)>500 ){
      ObjFielSet.add(new Ext.Panel({frame:true, width: 750, style: 'margin:0 auto;', html:'<CENTER>Descargar Archivo para ver detalle completo</CENTER>'}));
    }
    
		ObjFielSet.doLayout();
		
	}
	
	var transmitirVenc= function(btn){
		btn.disable();
		var textoFirmar = "E P O-DATOS|Total Monto Documento|Total Monto Descuento|Total Monto de Inter�s|Total Importe a Recibir\n"+
						"Al solicitar el factoraje electr�nico o descuento electr�nico del documento que selecciono e identifico, transmito los derechos"+ 
						" que sobre el mismo ejerzo, de acuerdo con lo establecido por los art�culos 427 de la ley general de t�tulos y operaciones de cr�dito,"+ 
						" 32 c del c�digo fiscal de la federaci�n y 2038 del c�digo civil federal, haci�ndome sabedor de su contenido y alcance, condicionado a"+
						" que se efect�e el descuento electr�nico o el factoraje electr�nico";
						
		
		NE.util.obtenerPKCS7(realizaConfirmacion, textoFirmar, btn  );
	}
	
	
	var realizaConfirmacion = function(pkcs7, textoFirmar , btn ){
		if (Ext.isEmpty(pkcs7)) {
			btn.enable();
			return;	//Error en la firma. Termina...
		}else  {
			Ext.Ajax.request({
				url: '33FondoJuniorValidacionExt.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'fondoJuniorValidacionAcuse',
					Pkcs7: pkcs7,
					TextoFirmado: textoFirmar,
					tipo_validacion: tipo_validacion,
					seccion_estatus: seccion_estatus,
					fecha_amortizacion: fecha_amortizacion
				}),
				callback: procesarSuccessConfirmacion
			});
		}
	}
	
	var procesarSuccessConfirmacion = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			resp.csReembTodo;
			resp.acuseResp;
			resp.igfolioValidacion;
			resp.TodosRegistrosVaidados;
			resp.RegReembTodo;
			resp.masdequinientos;
			
			
			if(resp.authRespOK){
				
				var colspan = 0;
				var htmlEncabezado = '';
				var htmlTotalCreditos = '';
				var htmlMontoTotalCred = '';
				
				if(seccion_estatus=='PNP'){
					if(tot_reg_p>0){
						htmlEncabezado += '<td class="celda01" align="center">Pagados</td>';
						htmlTotalCreditos += '<td align="right">'+tot_reg_p+'</td>';
						htmlMontoTotalCred += '<td align="right">'+tot_monto_p+'</td>';
						colspan++;
					}
					if(tot_reg_np>0){
						htmlEncabezado += '<td class="celda01" align="center">No Pagados</td>';
						htmlTotalCreditos += '<td align="right">'+tot_reg_np+'</td>';
						htmlMontoTotalCred += '<td align="right">'+tot_monto_np+'</td>';
						colspan++;
					}
				}else if(seccion_estatus=='RTRF'){
					if(tot_reg_r>0){
						htmlEncabezado += '<td class="celda01" align="center">Reembolsos</td>';
						htmlTotalCreditos += '<td align="right">'+tot_reg_r+'</td>';
						htmlMontoTotalCred += '<td align="right">'+tot_monto_r+'</td>';
						colspan++;
					}
					if(tot_reg_t>0){
						htmlEncabezado += '<td class="celda01" align="center">Prepagos Totales</td>';
						htmlTotalCreditos += '<td align="right">'+tot_reg_t+'</td>';
						htmlMontoTotalCred += '<td align="right">'+tot_monto_t+'</td>';
						colspan++;
					}
					if(tot_reg_rc>0){
						htmlEncabezado += '<td class="celda01" align="center">Recuperaciones Fondo Jr</td>';
						htmlTotalCreditos += '<td align="right">'+tot_reg_rc+'</td>';
						htmlMontoTotalCred += '<td align="right">'+tot_monto_rc+'</td>';
						colspan++;
					}
				}
				
				pnlMensaje.hide();
				gridPreAcuseValid.hide();
				pnlAcuseVenc.html='<p align="center"><b>La autentificaci�n se llev� a cabo con �xito <br>Recibo: '+resp.igfolioValidacion+'</b></p>'+
										'<table border="1"  bordercolor="#A5B8BF" style="background:#FFFFFF;">'+
										'<tr><td class="celda01" align="center" >&nbsp;</td>'+htmlEncabezado+'</tr>'+
										'<tr><td>No. Total de Creditos Rechazados</td>'+htmlTotalCreditos+'</tr>'+
										'<tr><td>Monto Total de Creditos Rechazados</td>'+htmlMontoTotalCred+'</tr>'+
										'<tr><td>Numero de Acuse</td><td align="right" colspan="'+colspan+'">'+resp.folio_validacion+'</td></tr>'+
										'<tr><td>Fecha de Carga</td><td align="right" colspan="'+colspan+'">'+resp.fecha_validacion+'</td></tr>'+
										'<tr><td>Hora de Carga</td><td align="right" colspan="'+colspan+'">'+resp.hora_validacion+'</td></tr>'+
										'<tr><td>Usuario</td><td align="right" colspan="'+colspan+'">'+resp.strNombreUsuario+'</td></tr>'+
										'<tr><td colspan="'+(colspan+1)+'">Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad '+
																 'haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad '+
																 'de los movimientos registrados del mismo, dicha transmisi�n tendr� validez para todos los efectos legales.'+
												'</td>'+
										'</tr>'+
										'</table>';
				contenedorPrincipalCmp.insert(0,pnlAcuseVenc);
				contenedorPrincipalCmp.doLayout();
				
				
				Ext.getCmp('btnImprimirPDFAcu').setHandler(function(btn){
					btn.setIconClass('loading-indicator');
					btn.disable();
					Ext.Ajax.request({
						url: '33FondoJuniorValidacionArchivosExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'descargaArchivosValidacion',
							tipo_validacion: tipo_validacion,
							tipo_archivo:'ACUSE_PDF',
							seccion_estatus: seccion_estatus,
							fecha_amortizacion: fecha_amortizacion,
							folio_validacion: resp.folio_validacion,
							fecha_validacion: resp.fecha_validacion,
							hora_validacion: resp.hora_validacion,
							igfolioValidacion: resp.igfolioValidacion
							
						}),
						callback: function(opts, success, response){
							procesarSuccessGeneraArchivos(opts, success, response, btn);
						}
					});
				});
				
				
			}else{
				Ext.MessageBox.alert('Aviso','<b>La autentificaci�n no se llev� a cabo.<br/>PROCESO CANCELADO</b>');
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessBusqProveedor =  function(opts, success, response) {
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('nombre_cliente1').setValue(resp.nombre_cliente);
		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	
	var procesarSuccessGeneraArchivos  =  function(opts, success, response, btn) {
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var archivo = resp.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			btn.setIconClass('');
			btn.enable();
		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
		
	var procesarSuccessConsultar =  function(opts, success, response) {
		pnl.el.unmask();
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			pnl.el.mask('Cargando...', 'x-mask-loading');
			var resp = 	Ext.util.JSON.decode(response.responseText);
			//strDirectorioPublicacion = resp.strDirectorioPublicacion;
      pnlMsgSinInfo.hide();
			crearGridVenc(resp.TodosRegistros, resp.isValidReembXdia, resp.csReembTodo);

		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
//------------------------------------------------------------------------------
	var storeCatEstatusData = new Ext.data.ArrayStore({
	  id				: 'storeCatEstatusData',
		fields 		: ['clave', 'descripcion'],
		data:[['P', 'Pagado'], ['NP', 'No Pagado'], ['R', 'Reembolso'], ['T', 'Prepago Total']],
		listeners	:
		{
			//load:procesarCatalogoEstado,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var storeCatIFData = new Ext.data.JsonStore({
	   id				: 'storeCatIFData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '33FondoJuniorValidacionExt.data.jsp',
		baseParams	: { informacion: 'ObtenCatalogoIF'},
		autoLoad		: true,
		listeners	:
		{
			//load:procesarCatalogoEstado,
			exception: NE.util.mostrarDataProxyError
		}
	});


	var storeVencFIDEData = new Ext.data.JsonStore({
		root : 'registros',
		fields: [
			{name: 'fecha_amort_nafin'},
			{name: 'fecha_operacion'},
			{name: 'fecha_pag_clfide'},
			{name: 'numero_prestamo'},
			{name: 'numero_sirac'},
			{name: 'numero_cuota'},
			{name: 'monto_capital', type: 'float'},
			{name: 'monto_interes', type: 'float'},
			{name: 'monto_total'},
			{name: 'estatus_prestamo'},
			{name: 'origen_prestamo'},
			{name: 'estatus_validacion'}
		],
		autoLoad: false,
		listeners: {
			//load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//procesarConsultaData(null, null, null);
				}
			}
		}
	});
	
	var storePreAcuseValid = new Ext.data.ArrayStore({
				fields: [
					{name: 'txtTotal'},
					{name: 'tot_pagados'},
					{name: 'tot_nopagados'},
					{name: 'tot_reemb'},
					{name: 'tot_prepagos'},
					{name: 'tot_recuperaciones'}
				]
			})

  var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '33FondoJuniorValidacionExt.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//------------------------------------------------------------------------------
	var elementosForma = [
		{
			xtype: 'compositefield',
			id: 'cdf_amort_nafin',
			fieldLabel: 'Fecha de Amortizaci�n NAFIN de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_amort_nafin_ini',
					id: 'fecha_amort_nafin_ini',
					allowBlank: false,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_amort_nafin_fin',
					margins: '0 20 0 0'
					//value: Ext.getDom('hidTxtFechaVencDe').value
				},
				{
					xtype: 'displayfield',
					value: 'hasta',
					width: 30
				},
				{
					xtype: 'datefield',
					name: 'fecha_amort_nafin_fin',
					id: 'fecha_amort_nafin_fin',
					allowBlank: false,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_amort_nafin_ini',
					margins: '0 20 0 0'
					//value: Ext.getDom('hidTxtFechaVenca').value
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		},
		{
			xtype: 'combo',
			name: 'estatus_registro',
			id: 'cmb_estatus_registro1',
			fieldLabel: 'Estatus Registro',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'estatus_registro',
			emptyText: 'Seleccione...',
			allowBlank: true,
			anchor: "50%",
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatEstatusData
			//tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype:		'textfield',
			name:			'numero_prestamo',
			id:			'numero_prestamo1',
			maskRe:		/[0-9]/,
			regex:		/^[0-9]*$/,
			regexText: 'No es un n�mero v�lido',
			allowBlank:	true,
			//disabled:	true,
			hidden:		false,
			anchor: '40%',
			fieldLabel: 'N�mero de Pr�stamo',
			maxLength:	12
		},
		{
			xtype: 'combo',
			name: 'clave_if',
			id: 'cmb_clave_if1',
			fieldLabel: 'Intemediario Financiero',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_if',
			emptyText: 'Seleccione...',
			allowBlank: true,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatIFData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			id: 'cNumSirac',
			fieldLabel: 'N�m. Cliente SIRAC',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype:		'textfield',
					name:			'numero_sirac',
					id:			'numero_sirac1',
					maskRe:		/[0-9]/,
					regex:		/^[0-9]*$/,
					regexText: 'No es un n�mero v�lido',
					allowBlank:	true,
					//disabled:	true,
					hidden:		false,
					//fieldLabel: 'N�mero de Prestamo',
					width:100,
					maxLength:	25,
					listeners:{
						change: function(obj, nVal, oVal){
							if(nVal!='' && nVal!=oVal){
								Ext.Ajax.request({
									url: '33FondoJuniorValidacionExt.data.jsp',
									params: {
										informacion: 'busquedaProveedor',
										numero_sirac: nVal
									},
									callback: procesarSuccessBusqProveedor
								});
							}else if(nVal==''){
								Ext.getCmp('nombre_cliente1').setValue(nVal);
							}
						}
					}
				},
				{
					xtype: 'textfield',
					name: 'nombre_cliente',
					id: 'nombre_cliente1',
					allowBlank: true,
					width:280,
					msgTarget: 'side',
					readOnly: true,
					disabled: true,
					maxLength:	100
				},
        {
					xtype: 'button',
					text: 'B�squeda Avanzada...',
					iconCls: 'icoBuscar',
					id: 'btnBusqAv',
					handler: function(boton, evento) {
						var pymeComboCmp = Ext.getCmp('cbPyme1');
						pymeComboCmp.setValue('');
						pymeComboCmp.setDisabled(true);
						var ventana = Ext.getCmp('winBusqAvan');
						if (ventana) {
							ventana.show();
						} else {
							new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			id: 'cdf_pago_cliente',
			fieldLabel: 'Fecha de pago de Cliente FIDE de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_pago_cliente_fide_ini',
					id: 'fecha_pago_cliente_fide_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_pago_cliente_fide_fin',
					margins: '0 20 0 0'
					//value: Ext.getDom('hidTxtFechaVencDe').value
				},
				{
					xtype: 'displayfield',
					value: 'hasta',
					width: 30
				},
				{
					xtype: 'datefield',
					name: 'fecha_pago_cliente_fide_fin',
					id: 'fecha_pago_cliente_fide_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_pago_cliente_fide_ini',
					margins: '0 20 0 0'
					//value: Ext.getDom('hidTxtFechaVenca').value
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		},
		{
			xtype: 'compositefield',
			id: 'cdf_consultar',
			fieldLabel: 'Fecha a Consultar de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_registro_fide_ini',
					id: 'fecha_registro_fide_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_registro_fide_fin',
					margins: '0 20 0 0'
					//value: Ext.getDom('hidTxtFechaVencDe').value
				},
				{
					xtype: 'displayfield',
					value: 'hasta',
					width: 30
				},
				{
					xtype: 'datefield',
					name: 'fecha_registro_fide_fin',
					id: 'fecha_registro_fide_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_registro_fide_ini',
					margins: '0 20 0 0'
					//value: Ext.getDom('hidTxtFechaVenca').value
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		}
	]
  
  
  var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 		'panel',
			bodyStyle: 	'padding: 10px',
			layout: {
				type: 	'hbox',
				pack: 	'center',
				align: 	'middle'
			},
			items: [
				{
					xtype: 'button',
					text: 'Buscar',
					id: 'btnBuscar',
					iconCls: 'icoBuscar',
					width: 	75,
					handler: function(boton, evento) {
						var pymeComboCmp = Ext.getCmp('cbPyme1');
						pymeComboCmp.setValue('');
						pymeComboCmp.setDisabled(false);
						storeBusqAvanzPyme.load({
							params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
								informacion: 'busquedaAvanzada'					
							})																	
						});				
					},
					style: { 
						marginBottom:  '10px' 
					} 
				}
			]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		}	
	];
  
  var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var numero_sirac = Ext.getCmp('numero_sirac1');
					var nombre_cliente = Ext.getCmp('nombre_cliente1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
					numero_sirac.setValue(cmbPyme.getValue());
					nombre_cliente.setValue(nombre);		
					
							
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 750,
		//hidden:true,
		style: 'margin:0 auto;',
		title: 'Validaci�n',
		frame: true,
		//collapsible: true,
		//titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Disposici�n de Fondo por Contingencia',
				//iconCls: 'icoBuscar',
				id:'btnDispFondo',
				//formBind: true,
				handler: function(){
					window.location.href="33DisposicionDeFondoPreacuseExt.jsp";
				}
			},
			{
				text: 'Validaci�n Reembolsos al D�a',
				//iconCls: 'icoBuscar',
				id:'btnConsultarReemb',
				//formBind: true,
				handler: validaRembAlDia
			},
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				id:'btnConsultar',
				formBind: true,
				handler: consultar
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href="33FondoJuniorValidacionExt.jsp";
				}
				
			}
		]
	});
	
	
	var gridPreAcuseValid = new Ext.grid.GridPanel({
		id: 'gridPreAcuseValid',
		title: '<center>Preacuse Validaci�n</center>',
		store: storePreAcuseValid,
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 115,
		style: 'margin:0 auto;',
		width: 420,//750,
		frame: true,
		columns: [	
			{
				header: ' ', dataIndex: 'txtTotal', sortable: true, width: 200, resizable: true, hidden: false, align: 'left'
			},
			{
				header: '<center>Pagados</center>', tooltip: 'Pagados', dataIndex: 'tot_pagados', sortable: true, hideable: false, width: 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<center>No Pagados</center>', tooltip: 'No Pagados', dataIndex: 'tot_nopagados', sortable: true, hideable: false, width: 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<center>Reembolsos</center>', tooltip: 'Reembolsos', dataIndex: 'tot_reemb', sortable: true, hideable: false, width: 100, align: 'right'//, renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<center>Prepagos Totales</center>', tooltip: 'Prepagos Totales', dataIndex: 'tot_prepagos', sortable: true, hideable: false, width: 100, align: 'right'//, renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '<center>Recuperaciones Fondo Jr</center>', tooltip: 'Recuperaciones Fondo Jr', dataIndex: 'tot_recuperaciones', sortable: true, hideable: false, width: 130, align: 'right'//, renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		]
	});
	
	
	var pnlMensaje = new Ext.Panel({
		id: 'pnlMensaje',
		width: '420',
		frame: true,
		style: 'margin:0 auto;',
		html:'Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, '+
			  'aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisi�n tendr� validez para todos los efectos legales.',
		buttons: [
			{
				text: 'Generar Archivo',
				//iconCls: 'icoBuscar',
				id:'btnGenArchPreAcu',
				handler: consultar
			},
			{
				text: 'Transmitir Aceptaci�n',
				//iconCls: 'icoBuscar',
				id:'btnTransAcepPreAcu',
				handler: transmitirVenc
			},
			{
				text: 'Cancelar',
				hidden: false,
				iconCls: 'icoCancelar',
				handler: function() {
					window.location.href="33FondoJuniorValidacionExt.jsp";
				}
				
			}
		]
	});
	
	var pnlAcuseVenc = new Ext.Panel({
		id: 'pnlAcuseVenc',
		width: '500',
		frame: true,
		style: 'margin:0 auto;',
		buttons: [
			{
				text: 'Imprimir PDF',
				//iconCls: 'icoBuscar',
				id:'btnImprimirPDFAcu'
				//handler: consultar
			},
			{
				text: 'Salir',
				hidden: false,
				iconCls: 'icoCancelar',
				handler: function() {
					window.location.href="33FondoJuniorValidacionExt.jsp";
				}
				
			}
		]
		//hidden: true
	})
  
  var pnlMsgSinInfo = new Ext.Panel({
		id: 'pnlAcuseVenc',
		width: '750',
		frame: true,
		style: 'margin:0 auto;',
		html: '<CENTER>No existe informaci�n</CENTER>',
    hidden: true
		//hidden: true
	})

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		layoutConfig: {
			align:'center'
		},
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
      pnlMsgSinInfo
			
		]
	});
	

});