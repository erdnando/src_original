Ext.onReady(function() {
	
	//Procesar Validar cuando el tipo de estatus es R
	var procesoValidar = function(grid, rowIndex, colIndex, item, event) {
			
		window.location = '33fondojrmonitorreembExt.jsp';
	}
	
	
	
		
	var procesoPorValidar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var fec_venc =registro.get('MFECHA_VENC');
		var estatus  = registro.get('MESTATUS');
		if(estatus=='T'  ||  estatus=='R' ||  estatus=='RC' ){
			fec_venc = estatus;
		}		
		window.location = '33fondojrmonitorvalext.jsp?informacion=MostrarPreAcuse&fec_venc='+fec_venc+'&estatus='+estatus;
					
	}

	
	
	
	
	//****************************CONSULTA PRINCIPAL****************************************************
	
			//GENERACION DEL ARCHIVO ZIP
	function procesarGenerarZip(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if(Ext.util.JSON.decode(response.responseText).isRunning==false) {	
				var fp = Ext.getCmp('forma');
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.URLArchivoZip;				  
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();	
				
			}else if(Ext.util.JSON.decode(response.responseText).isRunning==true) {	
			
				Ext.Ajax.request({
					url:'33fondojrmonitor.data.jsp',
					params:{
						informacion:'SeguirLeyendo'						
					},
					callback: procesarGenerarZip
				});						
			}
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesoGenerarZIP = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var fec_venc = registro.get('MFECHA_VENC');
		var estatus_val = registro.get('MESTATUS');
		var fec_venc  = registro.get('MFECHA_VENC');
		var folio_val = registro.get('MFOLIO');  
		
		Ext.Ajax.request({
			url: '33fondojrmonitor.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'procesoGenerarZIP',
				fec_venc:fec_venc,
				estatus_val:estatus_val,
				folio_val:folio_val,
				pantalla:'',
				chkDesembolso1:Ext.getCmp('chkDesembolso1').getValue()
			}),
			callback: procesarGenerarZip
		});
	}
		
	var procesoGenerarNPR = function(grid, rowIndex, colIndex, item, event) {
		var chkDesembolso =	Ext.getCmp('chkDesembolso1').getValue();
		if(chkDesembolso ==true)  { 
			chkDesembolso ="S";  
		} else { 
			chkDesembolso ="N"; 
		}
		window.location = '33fondojrmonitornprExt.jsp?chkDesembolso='+chkDesembolso;
	}
	
	
	// PROCESAR VER DETALLE DE LOS REGISTROS
	var procesoVerDetalle = function(grid, rowIndex, colIndex, item, event) {
	
		var registro = grid.getStore().getAt(rowIndex);
		var estatus = registro.get('MESTATUS');
		var fec_venc  = registro.get('MFECHA_VENC');
				
		var  titulo = 'Detalle de Cr�ditos Enviados';
		if(estatus =='NPR') {
			titulo = 'Detalle de Cr�ditos No Pagados Rechazados';
		}
		if(estatus=='T'  ||  estatus=='R' ||  estatus=='RC' ){ 	fec_venc = '';  }
		
		if(estatus=='RC'  ){ estatus='RT';  }
				
		consultaDetalleData.load({
				params:{
				estatusCarga:estatus,
				fec_venc:fec_venc
				}
			});
			
		var ventana = Ext.getCmp('VerDetalle');
		if (ventana) {
			ventana.show();
		}else {			
			new Ext.Window({			
				width: 800,				
				height: 400,
				autoScroll:true, 
				resizable: false,
				closeAction: 'hide',
				id: 'VerDetalle',
				autoDestroy:true,
				closable:false,
				modal:true,
				align: 'center',				
				items: [
					gridDetalle
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	buttons: [
						{
							xtype: 'button', iconCls: 'icoLimpiar', 	text: 'Cerrar',id: 'btnCerrarDe', 
							handler: function(){								
								Ext.getCmp('VerDetalle').destroy();																
							} 
						}
					]
				},
				title: titulo
			}).show();
		}
	}
	
	
	var procesarConsultaDetaData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridDetalle = Ext.getCmp('gridDetalle');
			var el = gridDetalle.getGridEl();
			var store = consultaDetalleData;
			if(store.getTotalCount()>0){
				el.unmask();
			}else {
				el.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}
	
	
	var consultaDetalleData = new Ext.data.JsonStore({
		root: 'registros',
		url: '33fondojrmonitor.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetalle'
		},
		fields: [
			{name: 'FECHA_VEN_NA'},
			{name: 'NO_PRESTAMO'},
			{name: 'NO_CLIENTE'},
			{name: 'MONTO_AMORTIZACIO'},
			{name: 'MONTO_INTERES'},
			{name: 'TOTAL_VENC'},
			{name: 'FECHA_PAGO'},
			{name: 'ESTATUS'},
			{name: 'FECHA_PERIODO'},
			{name: 'NO_DOCTO'},
			{name: 'FECHA_REGISTRO'}			
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDetaData,
			exception: {
				fn: function(proxy,type,action,optionRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
					procesarConsultaDetaData(null,null,null);
				}
			}
		}
	});
	
	var gridDetalle = {
		xtype: 'editorgrid',
		store: consultaDetalleData,
		id: 'gridDetalle',					
		columns: [
			{
				header : 'Fecha de Vencimiento Nafin',
				tooltip: 'Fecha de Vencimiento Nafin',
				dataIndex : 'FECHA_VEN_NA',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'No. Pr�stamo',
				tooltip: 'No. Pr�stamo',
				dataIndex : 'NO_PRESTAMO',
				width : 150,
				sortable : true
			},
			{
				header : 'No. Cliente',
				tooltip: 'No. Cliente',
				dataIndex : 'NO_CLIENTE',
				width : 150,
				sortable : true
			},
			{
				header : 'Monto Amortizaci�n',
				tooltip: 'Monto Amortizaci�n ',
				dataIndex : 'MONTO_AMORTIZACIO',
				width : 150,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto Inter�s',
				tooltip: 'Monto Inter�s',
				dataIndex : 'MONTO_INTERES',
				width : 150,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Total Venc',
				tooltip: 'Total Venc',
				dataIndex : 'TOTAL_VENC',
				width : 150,
				sortable : true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Fecha Pago CLiente',
				tooltip: 'Fecha Pago CLiente',
				dataIndex : 'FECHA_PAGO',
				width : 150,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				width : 150,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Fecha Periodo Fin',
				tooltip: 'Fecha Periodo Fin',
				dataIndex : 'FECHA_PERIODO',
				width : 150,
				sortable : true,
				align: 'left'
			},
			{
				header : 'N�mero Docto',
				tooltip: 'N�mero Docto',
				dataIndex : 'NO_DOCTO',
				width : 150,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Fecha Registro FIDE',
				tooltip: 'Fecha Registro FIDE',
				dataIndex : 'FECHA_REGISTRO',
				width : 150,
				sortable : true,
				align: 'left'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 780,
		height: 330,  
		frame: true
	}
	
	
	// Ventana que muestra el Layout de la Carga
	var VerLayout = function( ) {
	
		var ventana = Ext.getCmp('layout');
		if (ventana) {
			ventana.show();
		}else {			
			new Ext.Window({			
				width: 900,				
				height: 400,
				autoScroll:true, 
				resizable: false,
				closeAction: 'hide',
				id: 'layout',
				autoDestroy:true,
				closable:false,
				modal:true,
				align: 'center',				
				items: [
				gridLayout
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	buttons: [
						{
							xtype: 'button', iconCls: 'icoLimpiar', 	text: 'Cerrar',id: 'btnCerrar', 
							handler: function(){								
								Ext.getCmp('layout').destroy();																
							} 
						}
					]
				},
				title: 'LAYOUT DE CREDITOS PAGADOS, NO PAGADOS Y RECUPERADOS DE DIAS ANTERIORES '
			}).show();
		}
	}
	
	
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMCAMPO'},		  
			{name: 'NOMBRE'},	
			{name: 'DESC'},
			{name: 'TIPODATO'},
			{name: 'LONGITUD'},
			{name: 'OBSERVACIONES'}
		]
	});
	
	var infoLayout = [
		['1','com_fechaprobabalepago','Fecha Ral de Pago NAFIN','DATE','10','Fecha real de pago'],
		['2','ig_prestamo ','Prestamo','Number(12,0)','12', 'N�mero del pr�stamo'],
		['3','ig_cliente ','N�mero SIRAC','Number(11,0)','11', 'C�digo del cliente'],
		['4','ig_disposicion ','N�mero de Cuota a Pagar','Number(5,0)','5', 'N�mero de disposici�n'],
		['5','fg_amortizacion ','Amortizaci�n de Capital','Number(19,2)','17.2', 'Amortizaci�n'],
		['6','fg_interes ','Interes','Number(19,2)','17,2', 'interes normales'],		
		['7','fg_totalvencimiento ','Total a Pagar','Number(19,2)','17.2', 'Total Vencimiento'],			
		['8','df_pago_cliente ','Fecha del Cliente Pago FIDE','DATE','10', 'Pago de CFE a FIDE'],
		['9','cg_estatus ','Estatus del Registro','Number(19,2)','17.2', 'P:Pagados, NP:No Pagados, R:Reembolsos, RF:Desembolsos, T:Prepagos Totales'],
		['10','ig_origen','Origen','Varchar2(150)','150','0:CFE 2:Luz y Fuerza'],
		['11','df_periodofin','Periodo Fin','DATE','10','Periodo Fin'],
		['12','ig_numero_docto','N�mero de Documento','Varchar2(13)','13','N�mero de Documento']		
	];
	
	storeLayoutData.loadData(infoLayout);
		
	var gridLayout = {
		xtype: 'editorgrid',
		store: storeLayoutData,
		id: 'gridLayout',			
		title: '',
		columns: [
			{
				header : 'No. de Campo',
				dataIndex : 'NUMCAMPO',
				width : 90,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Nombre del Campo',
				tooltip: 'Nombre del Campo',
				dataIndex : 'NOMBRE',
				width : 150,
				sortable : true
			},
			{
				header : 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex : 'DESC',
				width : 180,
				sortable : true
			},
			{
				header : 'Tipo deDato',
				tooltip: 'Tipo deDato',
				dataIndex : 'TIPODATO',
				width : 120,
				sortable : true
			},
			{
				header : 'Longitud',
				dataIndex : 'LONGITUD',
				width : 70,
				sortable : true
			},
			{
				header : 'Observaciones',
				dataIndex : 'OBSERVACIONES',
				width : 250,
				sortable : true,
				align: 'left'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 910,
		height: 330,
		frame: true
	};
	
	
	// GRId de consulta General
	
	 var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarCSV');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarCSV');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			var el = gridConsulta.getGridEl();
				Ext.getCmp('btnBajarCSV').hide();
			
			if(store.getTotalCount() > 0) {	
				el.unmask();					
				Ext.getCmp('btnGenerarCSV').enable();	
			} else {	
				Ext.getCmp('btnGenerarCSV').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
		
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '33fondojrmonitor.data.jsp',
		baseParams: {
			informacion: 'Consultar',
			operacion: 'Generar'
		},
		fields: [
			{name: 'MESTATUS'},
			{name: 'MFECHA_VENC'},		
			{name: 'MULTIMO_REG'},		
			{name: 'MFOLIO'},	
			{name: 'MTOTAL_REG'}	,	
			{name: 'MMONTO_TOT'},
			{name: 'MESTATUS_PROC'},
			{name: 'MTIPO_REG'},
			{name: 'MASREQUERIMIENTO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{
				fn: function(store, options){
					Ext.apply(options.params, {
						ic_if:	Ext.getCmp('ic_if1').getValue(),
						fec_venc_ini:	Ext.util.Format.date(Ext.getCmp('fec_venc_ini').getValue(),'d/m/Y'),						
						fec_venc_fin:	Ext.util.Format.date(Ext.getCmp('fec_venc_fin').getValue(),'d/m/Y'),						
						estatusCarga:	Ext.getCmp('estatusCarga1').getValue(),
						chkDesembolso:	Ext.getCmp('chkDesembolso1').getValue()					
					});
				}		
			},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy,type,action,optionRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});
	
	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '',		
		columns: [
			{
				header: 'Fecha de Vencimiento Nafin',
				tooltip: 'Fecha de Vencimiento Nafin',
				dataIndex: 'MFECHA_VENC',
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('MESTATUS') =='BRC'  || registro.get('MESTATUS') =='BT' ){
						return registro.get('MTIPO_REG');					
					}else if(registro.get('MESTATUS') =='R'){
						return 'Reembolsos al dia';	
					}else if(registro.get('MESTATUS') =='T') {
						return 'Prepagos Totales al d�a';	
					}else if(registro.get('MESTATUS') =='BR') {
						return 'Reembolsos '+value;	
					}else if(registro.get('MESTATUS') =='RC') {
						return 'Recuperaciones al d�a';
					}else if(registro.get('MESTATUS') =='NPR') {
						return '*NP Rechazados al d�a';	
					}else {
						return value;
					}
				}
			},
			{
				header: 'Fecha y Hora del �ltimo Registro Enviado por FIDE',
				tooltip: 'Fecha y Hora del �ltimo Registro Enviado por FIDE',
				dataIndex: 'MULTIMO_REG',
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Folio de Operaci�n',
				tooltip: 'Folio de Operaci�n',
				dataIndex: 'MFOLIO',
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('MESTATUS') =='P' || registro.get('MESTATUS') =='T' || registro.get('MESTATUS') =='R'  ){
						return '';
					} else if(registro.get('MESTATUS') =='NPR') {
						return 'N/A';					
					}else {
						return value;
					}
				}
			},			
			{
				header: 'Total de Registros',
				tooltip: 'Total de Registros',
				dataIndex: 'MTOTAL_REG',
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MMONTO_TOT',
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				xtype: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle',				
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center',
				items: [					
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( ( registro.get('MESTATUS') =='P' ||    registro.get('MESTATUS') =='T' ||  registro.get('MESTATUS') =='BP' ||  registro.get('MESTATUS') =='BR') &&  registro.get('MESTATUS_PROC') =='N') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';									
							}else if(registro.get('MESTATUS') =='R'  &&  registro.get('MASREQUERIMIENTO') =='F' && registro.get('MESTATUS_PROC') =='N') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';								
							}else if(registro.get('MESTATUS') =='RC'  && registro.get('MESTATUS_PROC') =='N') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';								
							}
						}
						,	handler: procesoVerDetalle
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('MESTATUS') =='R'  &&  registro.get('MASREQUERIMIENTO') =='V'  && registro.get('MESTATUS_PROC') =='N' ) {
								this.items[1].tooltip = 'Ver';
								return 'iconoLupa';	
							}							
						}
						,	handler: procesoGenerarZIP
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('MESTATUS') =='NPR') {
								this.items[2].tooltip = 'Ver';
								return 'iconoLupa';	
							}							
						}
						,	handler: procesoGenerarNPR
					}		
				]					
			},			
			{
				xtype: 'actioncolumn',
				header: 'Estatus',
				tooltip: 'Estatus',				
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center',				
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(  (  registro.get('MESTATUS') =='P'  || registro.get('MESTATUS') =='BR' ||  registro.get('MESTATUS') =='BP')					
						&& registro.get('MESTATUS_PROC') !='N' ){
						if(registro.get('MESTATUS_PROC') =='V' ) {
							return 'Validado';	
						}else if(registro.get('MESTATUS_PROC') !='V'   ) {
							return 'Borrado';
						}
					}else if(registro.get('MESTATUS') =='NPR' ){
						return 'N/A';
						
					}					
				},				
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( registro.get('MESTATUS') =='P' ||  registro.get('MESTATUS') =='BP' || registro.get('MESTATUS') =='BR' ) {
								if(registro.get('MESTATUS_PROC') =='N') {
									this.items[0].tooltip = 'Por Validar';
									return 'icoAceptar'								
								}
							}else if( registro.get('MESTATUS') =='R') {		
								if(registro.get('MESTATUS_PROC') =='N') {
									this.items[0].tooltip = 'Por Validar Todo';
									return 'correcto';
								}
							}	else if( registro.get('MESTATUS') =='T' ||  registro.get('MESTATUS') =='RC') {
								if(registro.get('MESTATUS_PROC') =='N') {
									this.items[0].tooltip = 'Por Validar';
									return 'icoAceptar';
								}else if(registro.get('MESTATUS_PROC') !='N') {
									this.items[0].tooltip = 'Validado';
									return 'icoAceptar';
								
								}
							}
						}
						, handler: procesoPorValidar // preAcuse 
					},					
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( registro.get('MESTATUS') =='R'   ){
								if(registro.get('MESTATUS_PROC') =='N') {
									this.items[1].tooltip = 'Por Validar';
									return 'icoAceptar';								
								}else if( registro.get('MESTATUS_PROC') !='N') {																
									this.items[1].tooltip = 'Validado';
									return 'icoAceptar';									
								}
							}
						}
						, handler: procesoValidar   // link de Reembolsos
					}
				]	
			}
			
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",	
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Ver Layout de Carga',
					iconCls: 'icoAyuda',
					id: 'VerLayout',					
					handler: VerLayout
				},
				
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSV',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '33fondojrmonitor.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV'															
							}),
							callback: procesarGenerarCSV
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSV',
					iconCls: 'icoXls',
					hidden: true
				}
			]	
		}
	});

//****************************FORMA****************************************************

	var textoPrograma = new Ext.Container({
		layout: 'table',		
		id: 'textoPrograma',							
		width:	'700',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensaje', 	value: '' }				
		]
	});
	
	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var  jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
						
				Ext.getCmp('mensaje').setValue(jsonValoresIniciales.programa);
				Ext.getCmp('estatusCarga1').setValue('N');
				Ext.getCmp('textoPrograma').show();
			}			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var catalogoEstatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['N','Por Validar'],
			['V','Validado'],
			['B','Borrado'],
			['NPR','NP Rechazados']					
		 ]
	});
	
	var procesarCatalogoIFData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_if = Ext.getCmp('ic_if1');
			if(ic_if.getValue()==''){
				ic_if.setValue(records[0].data['clave']);
			}
		}
  } 
  
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '33fondojrmonitor.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoIFData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma =  [	
		{
			xtype: 'combo',
			fieldLabel: 'Nombre del IF',
			name: 'ic_if',
			id: 'ic_if1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_if',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIF					
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento:',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fec_venc_ini',
					id: 'fec_venc_ini',
					allowBlank: true,
					startDay: 0,
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fec_venc_fin',
					margins: '0 20 0 0' ,
					formBind: true},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'fec_venc_fin',
					id: 'fec_venc_fin',
					allowBlank: true,
					startDay: 1,
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fec_venc_ini',
					margins: '0 20 0 0',
					formBind: true
				}
			]
		},
		{
			xtype: 'combo',
			fieldLabel: 'Estatus',
			name: 'estatusCarga',
			id: 'estatusCarga1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'estatusCarga',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEstatus,
			listeners:{
				select:{ 
					fn:function (combo) {
						if(combo.getValue()=='NPR') {					 
							Ext.getCmp('chkDesembolso1').show();				
						}else {
							Ext.getCmp('chkDesembolso1').hide();	
						}
					}	
				}
			}
		},
		{
			xtype: 'checkbox',
			boxLabel:	'Mostrar NPR con Desembolsos',
			value:	'S',
			hidden: true,
			name: 'chkDesembolso',
			id: 'chkDesembolso1'
		}
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Monitor - Cr�ditos (PFAEE 2)',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Actualizar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton, evento) {	
				
					Ext.getCmp('btnBajarCSV').hide();				
					
					var layout = Ext.getCmp('layout');
					if (layout) {		
						layout.destroy();		
					}
					
					var fec_venc_ini =  Ext.getCmp("fec_venc_ini");
					var fec_venc_fin =  Ext.getCmp("fec_venc_fin");
										
					if ( ( !Ext.isEmpty(fec_venc_ini.getValue())  &&   Ext.isEmpty(fec_venc_fin.getValue()) )
						||  ( Ext.isEmpty(fec_venc_ini.getValue())  &&   !Ext.isEmpty(fec_venc_fin.getValue()) ) ){
						fec_venc_ini.markInvalid('Debe capturar ambas Fechas o dejarlas en blanco');
						fec_venc_fin.markInvalid('Debe capturar ambas Fechas o dejarlas en blanco');
						return;
					}					
				
					fp.el.mask('Enviando...', 'x-mask-loading');	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar',
							start:0,
							limit:15						
						})
					});
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				formBind: true,
				handler: function(boton, evento) {	
					window.location = '33fondojrmonitorExt.jsp';
				}
			}
		]
	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			textoPrograma,
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});

	
	catalogoIF.load();
	
	Ext.Ajax.request({
		url: '33fondojrmonitor.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});
	
});