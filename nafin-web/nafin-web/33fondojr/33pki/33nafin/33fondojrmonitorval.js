Ext.onReady(function() {

	
	var fec_venc =  Ext.getDom('fec_venc').value;
	var estatus =  Ext.getDom('estatus').value;
	var chkDesembolso =  Ext.getDom('chkDesembolso').value;
		
	//GENERACION DEL ARCHIVO ZIP
	function procesarGenerarZipPre(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if(Ext.util.JSON.decode(response.responseText).isRunning==false) {	
				var fp = Ext.getCmp('PanelFoma');
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.URLArchivoZip;				  
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();	
				
			}else if(Ext.util.JSON.decode(response.responseText).isRunning==true) {	
			
				Ext.Ajax.request({
					url:'33fondojrmonitor.data.jsp',
					params:{
						informacion:'SeguirLeyendo'						
					},
					callback: procesarGenerarZipPre
				});						
			}
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesoGenerarZIPPre = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var pantalla = registro.get('VERDETALLE');
		
		Ext.Ajax.request({
			url: '33fondojrmonitor.data.jsp',
			params: {
				informacion: 'procesoGenerarZIP',
				fec_venc:'',
				estatus_val:'',
				folio_val:'',
				pantalla:pantalla,
				chkDesembolso:chkDesembolso
			},
			callback: procesarGenerarZipPre
		});
	}
	
	//DESCARGA ARCHIVO DE ERRORES
	function procesarArchivoError(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('PanelFoma');
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnImprimirAcuse');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarAcuse');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
			
	function validaMonitorAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var  jsonData = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp("PanelFoma").hide();
			Ext.getCmp("grid1").hide();
			Ext.getCmp("grid2").hide();
			Ext.getCmp("grid3").hide();
			Ext.getCmp("grid4").hide();
			Ext.getCmp("grid5").hide();
			Ext.getCmp("fpBotones").show();									
			Ext.getCmp("btnBorrar").hide();
			Ext.getCmp("btnConfirmar").hide();						
			Ext.getCmp("btnCancelar").hide();	
			Ext.getCmp("btnRegMoni").show();
			Ext.getCmp("btnArcError").hide();
			Ext.getCmp("btnArcErrorZip").hide();
			
			if (jsonData.folio !='' ){	
				
				var acuseCifras = [
					['Folio:', jsonData.folio],
					['Nombre de Usuario que Autoriza ', jsonData.usuario],
					['Fecha y Hora de Autorizacion', jsonData.fec_Actual]					
				];					
				storeCifrasData.loadData(acuseCifras);
				
				// para la consulta de Acuse 
				consuAcuseData.load({
					params:{
						usuario:jsonData.usuario,					
						folio:jsonData.folio,
						fec_venc:jsonData.fec_venc
					}
				});
			
				Ext.getCmp("gridCifrasControl").show();
				Ext.getCmp("gridAcuse").show();
				Ext.getCmp("btnImprimirAcuse").show();
				
				
				// Para descargar el archivo 
				var btnImprimirAcuse=	Ext.getCmp("btnImprimirAcuse");			
				btnImprimirAcuse.setHandler(function(boton, evento) {
					btnImprimirAcuse.disable();
					btnImprimirAcuse.setIconClass('loading-indicator');
						
					Ext.Ajax.request({
						url: '33fondojrmonitor.data.jsp',
						params: {																								
							informacion:'generarArchivoAcuse',
							usuario:jsonData.usuario,					
							folio:jsonData.folio,
							fec_venc:jsonData.fec_venc
						},
						callback: procesarGenerarPDF
					});
				});
								
			}else if (jsonData.folio =='' ){			
								
				Ext.getCmp('mensajeErr').update(jsonData.mensaje);		
				Ext.getCmp('btError').update(jsonData.mensajeE);
				
				Ext.getCmp("btnImprimirAcuse").hide();	
				Ext.getCmp("textoError").show();
				Ext.getCmp("btError").show();		
				
			}
									
				
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	
	}
	
	
	var storeCifrasData = new Ext.data.ArrayStore({
		fields: [
			{name: 'etiqueta'},
			{name: 'informacion'}
		]
	});
	 
	
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Confirmacion de Validaci�n de Creditos',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 330,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 140,
		style: 'margin:0 auto;',		
		frame: true
	});
	
	
	//Para los datos del Acuse 
	var consuAcuseData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '33fondojrmonitor.data.jsp',
		baseParams: {
			informacion: 'consuAcuseData'	
		},								
		fields: [			
			{name: 'fec_venc' },
			{name: 'FECHA_FIDE' },
			{name: 'FOLIO_OPERACION' },
			{name: 'TOTAL_REGISTROS' },
			{name: 'MONTO' }
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	// grid para el Acuse 
	var gridAcuse = new Ext.grid.GridPanel({
		id: 'gridAcuse',
		store: consuAcuseData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		align: 'center',
		title: '',	
		hidden: true,
		columns: [	
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'fec_venc',
				width: 150,
				align: 'left'				
			},
			{
				header: 'Fecha y Hora de Ultimo Registro FIDE',
				tooltip: 'Fecha y Hora de Ultimo Registro FIDE',
				dataIndex: 'FECHA_FIDE',
				width: 200,
				align: 'center'				
			},
			{
				header: 'Folio de Operaci�n',
				tooltip: 'Folio de Operaci�n',
				dataIndex: 'FOLIO_OPERACION',
				width: 120,
				align: 'center'				
			},
			{
				header: 'Total de Registros',
				tooltip: 'Total de Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 120,
				align: 'center'				
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				width: 120,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 730,
		height: 200,  
		frame: true
	});
	
	//****************************PREACUSE****************************************************
	
	// PARA LA ELIMINACION DE LOS REGISTROS 
	function eliminaRegMonitor(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var  jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse.mensajeError !='' ){	
				
				Ext.getCmp("mensaje").setValue(jsondeAcuse.mensajeError);				
				Ext.getCmp("PanelFoma").hide();
				Ext.getCmp("grid1").hide();
				Ext.getCmp("grid2").hide();
				Ext.getCmp("grid3").hide();
				Ext.getCmp("fpBotones").show();			
				Ext.getCmp("btnBorrar").hide();
				Ext.getCmp("btnConfirmar").hide();
				Ext.getCmp("btnRegMoni").hide();
				
			}else if (jsondeAcuse.mensajeError =='S' ){		
				window.location = "33fondojrmonitorExt.jsp";
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	

	
	//para mostrar  el Detalle de PreAcuse
	var VerDetaPreA = function(grid, rowIndex, colIndex, item, event) {
	
		var registro = grid.getStore().getAt(rowIndex);
		var dirige  = registro.get('VERDETALLE');
		var fec_venc  = registro.get('FEC_VENC');
		var ver_enlace;
		var estatus;
		var estatus2;
		var proc;
		var estatusC  = registro.get('ESTATUS');		
		// val
		if(estatusC == 'P' ||  estatusC =='BP'   ||  estatusC =='BR'){
			if (dirige == 'Vencimientos'){ 						ver_enlace  = "Vencimientos"; 				estatus = "P";  							proc="N"; }
			else if (dirige == 'Enviados'){						ver_enlace  = "Enviados"; 						estatus = "P"; 	estatus2 = "NP";	proc="N"; }
			else if (dirige == 'Pagados'){ 						ver_enlace  = "Pagados"; 						estatus = "P";	 							proc="N"; }
			else if (dirige == 'NoPagados')	   {				ver_enlace  = "NoPagados";						estatus = "NP"; 							proc="N"; }
			else if (dirige == 'NoEncontrados'){				ver_enlace  = "NoEncontrados";				estatus = "P"; 	estatus2 = "NP";  proc="N"; }
			else if (dirige == 'NoEnviados')	   {				ver_enlace  = "NoEnviados";				 	estatus = "P"; 	estatus2 = "NP"; 	proc="N"; }
			else if (dirige == 'RembolsosPrepagosTotales'){	ver_enlace = "RembolsosPrepagosTotales"; 	estatus = "R"; 	estatus2 = "T";	proc = "N";	}
			else if (dirige == 'Reembolsos')	   {				ver_enlace  = "Reembolsos";					estatus = "R"; 	estatus2 = "R"; 	proc = "N"; }
			else if (dirige == 'Recuperaciones'){				ver_enlace = "Recuperaciones";				estatus = "R"; 							proc = "N";}
			else if (dirige == 'Prepagos')	   {				ver_enlace  = "Prepagos";						estatus = "T";								proc = "N";}
			else if (dirige == 'ConError')	   {				ver_enlace  = "ConError";						estatus = "P"; }
			else if (dirige == 'NoPagadosRech')	  {			ver_enlace  = "NoPagadosRech";				estatus = "NP";							 proc="R"; }
		}
		//valb
		if(estatusC == 'T' || estatusC == 'RC' ){
			if (			dirige == 'Vencimientos'){ 					ver_enlace  = "Vencimientos"; 				estatus = "P"; 									proc="N"; }
				else if (dirige == 'Enviados'){							ver_enlace  = "Enviados"; 						estatus = "R"; 			estatus2 = "T";	proc="N"; }
				else if (dirige == 'Pagados'){ 							ver_enlace  = "Pagados"; 						estatus = "P";	 									proc="N"; }
				else if (dirige == 'NoPagados')	   {					ver_enlace  = "NoPagados";						estatus = "NP";									proc="N"; }
				else if (dirige == 'NoEncontrados'){					ver_enlace  = "NoEncontrados";				estatus = "R"; 			estatus2 = "RC";	proc="N"; }
				else if (dirige == 'NoEnviados')	   {					ver_enlace  = "NoEnviados";					estatus = "P"; 			estatus2 = "NP"; 	proc="N"; }
				else if (dirige == 'RembolsosPrepagosTotales'){		ver_enlace = "RembolsosPrepagosTotales"; 	estatus = fec_venc ; 	estatus2 = "R";	proc = "N";	}
				else if (dirige == 'Reembolsos')	   {					ver_enlace  = "Reembolsos";					estatus = "R";				estatus2 = "R"; 	proc = "N"; }
				else if (dirige == 'Recuperaciones'){					ver_enlace = "Recuperaciones";				estatus = "R"; 									proc = "N";}
				else if (dirige == 'Prepagos')	   {					ver_enlace  = "Prepagos";						estatus = "T";										proc = "N"}
				else{ dirige == 'ConError';	   						ver_enlace  = "ConError";						estatus = fec_venc;  	estatus2="R";
				}
		}	
		//valC
		if(estatusC == 'R'){
		
			if (dirige == 'Vencimientos'){ 							ver_enlace  = "Vencimientos"; 								estatus = "P";  							proc="N"; }
			else if (dirige == 'Enviados'){							ver_enlace  = "Enviados"; 										estatus = "R";	 	estatus2 = "T";	proc="N"; }
			else if (dirige == 'Pagados'){ 							ver_enlace  = "Pagados"; 										estatus = "P";	 							proc="N"; }
			else if (dirige == 'NoPagados')	   {					ver_enlace  = "NoPagados";										estatus = "NP"; 							proc="N"; }
			else if (dirige == 'NoEncontrados'){					ver_enlace  = "NoEncontrados";								estatus = "R"; 	estatus2 = "R"; 	proc="N"; }
			else if (dirige == 'NoEnviados')	   {					ver_enlace  = "NoEnviados";									estatus = "P"; 	estatus2 = "NP"; 	proc="N"; }
			else if (dirige == 'RembolsosPrepagosTotales'){		ver_enlace = "RembolsosPrepagosTotales"; 					estatus = "R"; 	estatus2 = "R";	proc = "N";	}
			else if (dirige == 'Reembolsos')	   {					ver_enlace  = "Reembolsos";									estatus = "R"; 	estatus2 = "R"; 	proc = "N"; }
			else if (dirige == 'Recuperaciones'){					ver_enlace = "Recuperaciones";								estatus = "R"; 							proc = "N";}
			else if (dirige == 'Prepagos')	   {					ver_enlace  = "Prepagos";										estatus = "T";								proc = "N"}
			else{ dirige == 'ConError';	   						ver_enlace  = "ConError";										estatus = "R";
			}
				
		}
		
		consultaDetalleCreData.load({
				params:{
				dirige:dirige,
				ver_enlace:ver_enlace,
				estatus:estatus,
				estatus2:estatus2,
				proc:proc,
				fec_venc:fec_venc
				}
			});
	
		var ventana = Ext.getCmp('VerDetalleCre');
		if (ventana) {
			ventana.show();
		}else {			
			new Ext.Window({			
				width: 800,				
				height: 380,
				autoScroll:true, 
				resizable: false,
				closeAction: 'hide',
				id: 'VerDetalleCre',
				autoDestroy:true,
				closable:false,
				modal:true,
				align: 'center',				
				items: [
					gridDetalleCre
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	buttons: [
						{
							xtype: 'button', iconCls: 'icoLimpiar', 	text: 'Cerrar',id: 'btnCerrarCre', 
							handler: function(){								
								Ext.getCmp('VerDetalleCre').destroy();																
							} 
						}
					]
				},
				title: ''
			}).show();
		}		
	}


	var procesarConsultaDetaCreData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridDetalleCre = Ext.getCmp('gridDetalleCre');
			var el = gridDetalleCre.getGridEl();
			var store = consultaDetalleCreData;
			var jsonData = store.reader.jsonData;	
			
			gridDetalleCre.setTitle(jsonData.titulo);				
			if(store.getTotalCount()>0){			
						
				el.unmask();
			}else {
				el.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}	
	
	var consultaDetalleCreData = new Ext.data.JsonStore({
		root: 'registros',
		url: '33fondojrmonitor.data.jsp',
		baseParams: {
			informacion: 'VerDetalleDeCreditos'
		},
		fields: [
			{name: 'FECHA_AUTORIZACION_NA'},
			{name: 'FECHA_OPERACION'},
			{name: 'FECHA_PAGO_FIDE'},
			{name: 'PRESTAMO'},
			{name: 'NUM_CLI_SIRAC'},
			{name: 'CLIENTE'},
			{name: 'NUM_CUOTA'},
			{name: 'CAPITAL_INTERES'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDetaCreData,
			exception: {
				fn: function(proxy,type,action,optionRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
					procesarConsultaDetaCreData(null,null,null);
				}
			}
		}
	});
	
	var gridDetalleCre = {
		xtype: 'editorgrid',
		store: consultaDetalleCreData,
		id: 'gridDetalleCre',	
		title: 'Detalle Creditos',
		columns: [
			{
				header : 'Fecha  Autorizaci�n Nafin',
				tooltip: 'Fecha  Autorizaci�n Nafin',
				dataIndex : 'FECHA_AUTORIZACION_NA',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Fecha Operaci�n',
				tooltip: 'Fecha Operaci�n',
				dataIndex : 'FECHA_OPERACION',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Fecha pago Cliente FIDE',
				tooltip: 'Fecha pago Cliente FIDE',
				dataIndex : 'FECHA_PAGO_FIDE',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Prestamo',
				tooltip: 'Prestamo',
				dataIndex : 'PRESTAMO',
				width : 150,
				sortable : true,
				align: 'center'				
			},
			{
				header : 'N�m. Cliente SIRAC',
				tooltip: 'N�m. Cliente SIRAC',
				dataIndex : 'NUM_CLI_SIRAC',
				width : 150,
				sortable : true,
				align: 'center'				
			},
			{
				header : 'Cliente',
				tooltip: 'Cliente',
				dataIndex : 'CLIENTE',
				width : 150,
				sortable : true,				
				align: 'left'				
			},
			{
				header : 'N�m. Cuota a Pagar',
				tooltip: 'N�m. Cuota a Pagar',
				dataIndex : 'NUM_CUOTA',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Capital mas Inter�s',
				tooltip: 'Capital mas Inter�s',
				dataIndex : 'CAPITAL_INTERES',
				width : 150,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 780,
		height: 330,  
		frame: true
	}
	
	
	var confirmar = function(pkcs7, textoFirmar){
	
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else  {					
			
			Ext.Ajax.request({
				url: '33fondojrmonitor.data.jsp',
				params: {																								
					informacion:'validaMonitorAcuse',
					fec_venc:jsonValores.lblFechaVen,
					estatus:jsonValores.estatus,
					textoFirmar:textoFirmar,
					pkcs7:pkcs7
				},
				callback: validaMonitorAcuse
			});	
		}
	}
	
	

	var confirmarBorrar = function(pkcs7, textoFirmar){
	
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else  {					
			
			Ext.Ajax.request({
				url: '33fondojrmonitor.data.jsp',
				params: {																								
					informacion:'eliminaRegMonitor',
					fec_venc:jsonValores.lblFechaVen,
					estatus:jsonValores.estatus,
					textoFirmar:textoFirmar,
					pkcs7:pkcs7
				},
				callback: eliminaRegMonitor
			});
		}	
	}

	// Mostrar Grid de PreAcuse 
	function procesarPreAcuseValidar(opts, success, response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValores = Ext.util.JSON.decode(response.responseText);
			if(jsonValores != null){	
			
				var textoFirmar =jsonValores.seguridad;
				Ext.getCmp("lblIF").setValue(jsonValores.lblIF);					
				Ext.getCmp("lblNomVali").setValue(jsonValores.lblNomVali);	
				Ext.getCmp("lblFechaHoraVali").setValue(jsonValores.lblFechaHoraVali);
				if(jsonValores.estatus!='T' &&  jsonValores.estatus!='R'  &&  jsonValores.estatus !='RC'  && jsonValores.estatus !=''){
					Ext.getCmp("lblFechaVen").setValue(jsonValores.lblFechaVen);	
				}
				if(jsonValores.lblFechaVen!='T') {
					estatus='';
				}
				
				if(jsonValores.estatus!='T'  &&  jsonValores.estatus!='R' &&  jsonValores.estatus !='RC'   &&  jsonValores.estatus!=''){
					consu_1Data.load({
						params: {
							fec_venc:jsonValores.lblFechaVen,
							estatus:jsonValores.estatus
							}
					});
						Ext.getCmp("grid1").show();						
				}
				
				if( jsonValores.estatus!='R'  &&  jsonValores.estatus!='RC'){
				
					if(jsonValores.sumaTotalError !='0') {
						Ext.getCmp("btnArcError").show();				
					}else {
						Ext.getCmp("btnArcError").hide();				
					}
								
					consu_2Data.load({
						params:{
							fec_venc:jsonValores.lblFechaVen,
							estatus:jsonValores.estatus
						}
					});
					
					consu_3Data.load({
						params:{
							fec_venc:jsonValores.lblFechaVen,
							estatus:jsonValores.estatus
						}
					});	
					
					Ext.getCmp("grid2").show();
					Ext.getCmp("grid3").show();
				}
				
				
				if( jsonValores.estatus=='R'  ||  jsonValores.estatus=='RC'){
				
					consu_4Data.load({
						params: {
							fec_venc:jsonValores.lblFechaVen,
							estatus:jsonValores.estatus
						}
					});	
					
					
					consu_5Data.load({
						params:{
							fec_venc:jsonValores.lblFechaVen,
							estatus:jsonValores.estatus
						}
					});	
					
									
					Ext.getCmp("grid4").show();
					Ext.getCmp("grid5").show();
					
					if( jsonValores.estatus=='R') {
						if(jsonValores.sumaTotalError !='0') {
							if(jsonValores.masdequinRCE =='V') {					
								Ext.getCmp("btnArcErrorZip").show();
								Ext.getCmp("btnArcError").hide();
							}else {
								Ext.getCmp("btnArcError").show();				
							}						
						}else {
							Ext.getCmp("btnArcError").hide();
						}
					}if( jsonValores.estatus=='RC') {
						if(jsonValores.sumaTotalError !='0') {	
							Ext.getCmp("btnArcError").show();
						}else {
							Ext.getCmp("btnArcError").hide();				
						}						
					}
					//BOTON DE GENERAR ARCHIVO DE ERRORES  COMO ZIP
					var btnArcErrorZip=	Ext.getCmp("btnArcErrorZip");			
					btnArcErrorZip.setHandler(function(boton, evento) {
						Ext.Ajax.request({
							url: '33fondojrmonitor.data.jsp',
							params: {																								
								informacion:'procesoGenerarZIP',								
								estatus_val:'',
								folio_val:'',
								pantalla:'ConError'
							},
							callback: procesarGenerarZipPre
						});
					});				
				}
				
				if(jsonValores.mostrar =='S'){
					Ext.getCmp("btnBorrar").show();
					if(jsonValores.sumaTotalError =='0'){
						Ext.getCmp("btnConfirmar").show();
					}else {
						Ext.getCmp("btnConfirmar").hide()
					}
				}else {
					Ext.getCmp("btnBorrar").hide();
					Ext.getCmp("btnConfirmar").hide();
				}
				
				Ext.getCmp("btnRegMoni").hide();
				Ext.getCmp("PanelFoma").show();				
				Ext.getCmp("fpBotones").show();					
				
				if(jsonValores.estatus=='T'){
					Ext.getCmp("grid1").hide();
				}
				//BOTON DE GENERAR ARCHIVO DE ERRORES 
				var btnArcError=	Ext.getCmp("btnArcError");			
				btnArcError.setHandler(function(boton, evento) {
					Ext.Ajax.request({
						url: '33fondojrmonitor.data.jsp',
						params: {																								
							informacion:'generaXLSCreditos',
							fec_venc:jsonValores.lblFechaVen,
							estatus:jsonValores.estatus
						},
						callback: procesarArchivoError
					});
				});
			
				// BOTON DE BORRAR 			
				var btnBorrar=	Ext.getCmp("btnBorrar");			
				btnBorrar.setHandler(function(boton, evento) {
									
					NE.util.obtenerPKCS7(confirmarBorrar ,  textoFirmar  );	
					
				});
				
				// BOTON PARA CONFIRMAR  			
				var btnConfirmar=	Ext.getCmp("btnConfirmar");			
				btnConfirmar.setHandler(function(boton, evento) {
									
					NE.util.obtenerPKCS7(confirmar ,  textoFirmar  );						
					
				});				
			
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
		
	var procesoPorValidar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var fec_venc =registro.get('MFECHA_VENC');
		var estatus  = registro.get('MESTATUS');
		if(estatus=='T'  ||  estatus=='R' ||  estatus=='RC' ){
			fec_venc = estatus;
		}
		
		Ext.Ajax.request({
			url: '33fondojrmonitor.data.jsp',
			params: {
				informacion: 'MostrarPreAcuse',
				fec_venc:fec_venc,
				estatus:estatus
			},
			callback: procesarPreAcuseValidar
		});
				
	}
		
	
	var elementosFormaPanel  =[
		{
			xtype: 'panel',
			title: 'Informaci�n Resumen',
			width:'auto',
			id:'idInfo'		
		},
		{
			xtype: 'displayfield',
			id: 'lblIF',
			style: 'text-align:left;',
			fieldLabel: 'IF',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFechaVen',
			style: 'text-align:left;',
			fieldLabel: 'Fecha de Vencimiento',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblNomVali',
			style: 'text-align:left;',
			fieldLabel: 'Nombre del Validador',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFechaHoraVali',
			style: 'text-align:left;',
			fieldLabel: 'Fecha y Hora de Validaci�n',
			text: '-'
		}	
	];
		
	
	var PanelFoma = new Ext.FormPanel({
		id: 'PanelFoma',
		width: 600,
		style: ' margin:0 auto;',	
		title: 'DETALLE DE CR�DITO A VALIDAR ',
		hidden: true,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 300,
		defaultType: 'textfield',
		items: elementosFormaPanel	,	
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		monitorValid: false,
		autoScroll: true,
		buttons: [
		
		]
	});
	
		//para los totales del grid Nomal 
	var consu_1Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '33fondojrmonitor.data.jsp',
		baseParams: {
			informacion: 'consu_1Data'	
		},								
		fields: [			
			{name: 'DESCRIPCION' , mapping: 'DESCRIPCION'},
			{name: 'NO_CREDITOS', type: 'float', mapping: 'NO_CREDITOS'},
			{name: 'IMPORTE' , type: 'float', mapping: 'IMPORTE' },
			{name: 'VERDETALLE' , mapping: 'VERDETALLE'},
			{name: 'FEC_VENC' , mapping: 'FEC_VENC'},
			{name: 'ESTATUS' , mapping: 'ESTATUS'}
			
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var grid1 = new Ext.grid.GridPanel({
		id: 'grid1',
		store: consu_1Data,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		align: 'center',
		title: '',	
		hidden: true,
		columns: [	
			{
				header: '',
				dataIndex: 'DESCRIPCION',
				width: 200,
				align: 'left'				
			},
			{
				header: 'No. de Cr�ditos',
				tooltip: 'No. de Cr�ditos',
				dataIndex: 'NO_CREDITOS',
				width: 100,
				align: 'center'				
			},
			{
				header: 'Importe',
				tooltip: 'Importe',
				dataIndex: 'IMPORTE',
				width: 100,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle',				
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center',
				items: [		
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}					
						,handler: VerDetaPreA
					}
				]
			}		
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 600,
		height: 100,  
		frame: true
		
	});
	
	
			
	var consu_2Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '33fondojrmonitor.data.jsp',
		baseParams: {
			informacion: 'consu_2Data'	
		},								
		fields: [			
			{name: 'DESCRIPCION' , mapping: 'DESCRIPCION'},
			{name: 'NO_CREDITOS', type: 'float', mapping: 'NO_CREDITOS'},
			{name: 'IMPORTE' , type: 'float', mapping: 'IMPORTE' },
			{name: 'VERDETALLE' , mapping: 'VERDETALLE'},
			{name: 'FEC_VENC' , mapping: 'FEC_VENC'},
			{name: 'ESTATUS' , mapping: 'ESTATUS'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var grid2 =  new Ext.grid.GridPanel({	
		id: 'grid2',
		store: consu_2Data,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		align: 'center',
		title: 'Validaci�n de la Informaci�n',	
		hidden: true,
		columns: [	
			{
				header: '',
				dataIndex: 'DESCRIPCION',
				width: 200,
				align: 'left'				
			},
			{
				header: 'No. de Cr�ditos',
				tooltip: 'No. de Cr�ditos',
				dataIndex: 'NO_CREDITOS',
				width: 100,
				align: 'center'				
			},
			{
				header: 'Importe',
				tooltip: 'Importe',
				dataIndex: 'IMPORTE',
				width: 100,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle',				
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center',
				items: [		
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}					
						,handler: VerDetaPreA
					}
				]
			}		
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 600,
		height: 100,  
		frame: true
		
	});
	
	
	var consu_3Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '33fondojrmonitor.data.jsp',
		baseParams: {
			informacion: 'consu_3Data'	
		},								
		fields: [			
			{name: 'DESCRIPCION' , mapping: 'DESCRIPCION'},
			{name: 'NO_CREDITOS', type: 'float', mapping: 'NO_CREDITOS'},
			{name: 'IMPORTE' , type: 'float', mapping: 'IMPORTE' },
			{name: 'VERDETALLE' , mapping: 'VERDETALLE'},
			{name: 'FEC_VENC' , mapping: 'FEC_VENC'},
			{name: 'ESTATUS' , mapping: 'ESTATUS'},
			{name: 'VAR_DETALLE' , mapping: 'VAR_DETALLE'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var grid3 =  new Ext.grid.GridPanel({	
		id: 'grid3',
		store: consu_3Data,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		align: 'center',
		title: 'Resumen de Env�o',	
		hidden: true,
		columns: [	
			{
				header: '',
				dataIndex: 'DESCRIPCION',
				width: 200,
				align: 'left'				
			},
			{
				header: 'No. de Cr�ditos',
				tooltip: 'No. de Cr�ditos',
				dataIndex: 'NO_CREDITOS',
				width: 100,
				align: 'center'				
			},
			{
				header: 'Importe',
				tooltip: 'Importe',
				dataIndex: 'IMPORTE',
				width: 100,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle',				
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center',
				items: [		
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						}					
						,handler: VerDetaPreA
					}
				]
			}		
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 600,
		height: 200,  
		frame: true
		
	});
		
	
	var consu_4Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '33fondojrmonitor.data.jsp',
		baseParams: {
			informacion: 'consu_4Data'	
		},								
		fields: [			
			{name: 'DESCRIPCION' , mapping: 'DESCRIPCION'},
			{name: 'NO_CREDITOS', type: 'float', mapping: 'NO_CREDITOS'},
			{name: 'IMPORTE' , type: 'float', mapping: 'IMPORTE' },
			{name: 'VERDETALLE' , mapping: 'VERDETALLE'},
			{name: 'FEC_VENC' , mapping: 'FEC_VENC'},
			{name: 'ESTATUS' , mapping: 'ESTATUS'},
			{name: 'VAR_DETALLE' , mapping: 'VAR_DETALLE'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var grid4 =  new Ext.grid.GridPanel({	
		id: 'grid4',
		store: consu_4Data,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		align: 'center',
		title: 'Validaci�n de la Informaci�n',	
		hidden: true,
		columns: [	
			{
				header: '',
				dataIndex: 'DESCRIPCION',
				width: 200,
				align: 'left'				
			},
			{
				header: '',
				tooltip: '',
				dataIndex: 'NO_CREDITOS',
				width: 100,
				align: 'center'				
			},
			{
				header: '',
				tooltip: '',
				dataIndex: 'IMPORTE',
				width: 100,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle',				
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center',
				items: [		
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( registro.get('VAR_DETALLE') =='V') {
								this.items[0].tooltip = 'Ver Zip';
								return 'iconoLupa';
							}		
						}
						,handler: procesoGenerarZIPPre
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( registro.get('VAR_DETALLE') !='V'){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}	
						}
						,handler: VerDetaPreA
					}
				]
			}		
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 600,
		height: 100,  
		frame: true		
	});
		
	var consu_5Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '33fondojrmonitor.data.jsp',
		baseParams: {
			informacion: 'consu_5Data'	
		},								
		fields: [			
			{name: 'DESCRIPCION' , mapping: 'DESCRIPCION'},
			{name: 'NO_CREDITOS', type: 'float', mapping: 'NO_CREDITOS'},
			{name: 'IMPORTE' , type: 'float', mapping: 'IMPORTE' },
			{name: 'VERDETALLE' , mapping: 'VERDETALLE'},
			{name: 'FEC_VENC' , mapping: 'FEC_VENC'},
			{name: 'ESTATUS' , mapping: 'ESTATUS'},
			{name: 'VAR_DETALLE' , mapping: 'VAR_DETALLE'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var grid5 =  new Ext.grid.GridPanel({	
		id: 'grid5',
		store: consu_5Data,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		align: 'center',
		title: 'Resumen de Env�o',	
		hidden: true,
		columns: [	
			{
				header: '',
				dataIndex: 'DESCRIPCION',
				width: 200,
				align: 'left'				
			},
			{
				header: '',
				tooltip: '',
				dataIndex: 'NO_CREDITOS',
				width: 100,
				align: 'center'				
			},
			{
				header: '',
				tooltip: '',
				dataIndex: 'IMPORTE',
				width: 100,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle',				
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center',
				items: [	
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( registro.get('VAR_DETALLE') =='V') {
								this.items[0].tooltip = 'Ver Zip';
								return 'iconoLupa';
							}		
						}
						,handler: procesoGenerarZIPPre
					},					
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if( registro.get('VAR_DETALLE') !='V') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						}					
						,handler: VerDetaPreA
					}
				]
			}		
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 600,
		height: 200,  
		frame: true		
	});
	
	
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'600',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		items: [	
			{ 	xtype: 'displayfield',  id: 'btError', 	value: '' },	
			
			{
				xtype: 'button',
				text: 'Descarga Archivos Errores',					
				id: 'btnArcError',
				iconCls: 'icoXls'				
			},
			{
				xtype: 'button',
				text: 'Descarga Archivos Errores',					
				id: 'btnArcErrorZip',
				iconCls: 'icoZip',
				hidden: true				
			},			
			{
				xtype: 'button',
				text: 'Borrar',					
				id: 'btnBorrar',
				hidden: true,
				iconCls: 'borrar',
				handler: function() {
					window.location = "33fondojrmonitorExt.jsp";
				}
			},
			{
				xtype: 'button',
				text: 'Confirmar',					
				id: 'btnConfirmar',
				hidden: true,
				iconCls: 'icoAceptar',
				handler: function() {
					window.location = "33fondojrmonitorExt.jsp";
				}
			},			
			{
				xtype: 'button',
				text: 'Cancelar',					
				id: 'btnCancelar',				
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = "33fondojrmonitorExt.jsp";
				}
			},
			{
				xtype: 'button',
				text: 'Regresar a Monitor',					
				id: 'btnRegMoni',
				hidden: true,
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = "33fondojrmonitorExt.jsp";
				}
			},			
			{
				xtype: 'button',
				text: 'Imprimir PDF',					
				id: 'btnImprimirAcuse',
				hidden: true,				
				iconCls: 'icoPdf'				
			},
			{
				xtype: 'button',
				text: 'Bajar PDF',					
				id: 'btnBajarAcuse',
				hidden: true,
				iconCls: 'icoPdf'				
			}			
		]
	});


//****************************FORMA****************************************************

	var textoPrograma = new Ext.Container({
		layout: 'table',		
		id: 'textoPrograma',							
		width:	'700',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensaje', 	value: '' }				
		]
	});
	
	var textoError = new Ext.Container({
		layout: 'table',		
		id: 'textoError',							
		width:	'300',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensajeErr', 	value: '' }				
		]
	});
	
	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var  jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){						
				Ext.getCmp('mensaje').setValue(jsonValoresIniciales.programa);				
				Ext.getCmp('textoPrograma').show();
			}			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	/*
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Monitor - Cr�ditos (PFAEE 2)',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},					
		monitorValid: true		
	});
*/

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			textoPrograma,
			textoError,
			NE.util.getEspaciador(20),			
			gridCifrasControl,
			PanelFoma,			
			grid1,
			grid2,
			grid3,
			grid4,
			grid5,
			gridAcuse, 
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),			
			NE.util.getEspaciador(20)
		]
	});

	
	Ext.Ajax.request({
		url: '33fondojrmonitor.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});
		
	Ext.Ajax.request({
		url: '33fondojrmonitor.data.jsp',
		params:{
			informacion: 'MostrarPreAcuse',
			fec_venc:fec_venc,
			estatus:estatus
		},
		callback: procesarPreAcuseValidar
	});
		
	
});