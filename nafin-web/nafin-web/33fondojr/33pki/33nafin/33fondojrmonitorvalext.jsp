<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>

<html> 
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%@ include file="/00utils/componente_firma.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String fec_venc = (request.getParameter("fec_venc")!=null)?request.getParameter("fec_venc"):"";
String estatus = (request.getParameter("estatus")!=null)?request.getParameter("estatus"):"";
String chkDesembolso = (request.getParameter("chkDesembolso")!=null)?request.getParameter("chkDesembolso"):"";
%>
<script type="text/javascript" src="33fondojrmonitorval.js?<%=session.getId()%>"></script>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
	
	<form id='formParametros' name="formParametros">
	<input type="hidden" id="informacion" name="informacion" value="<%=informacion%>"/>	
	<input type="hidden" id="fec_venc" name="fec_venc" value="<%=fec_venc%>"/>	
	<input type="hidden" id="estatus" name="estatus" value="<%=estatus%>"/>	
	<input type="hidden" id="chkDesembolso" name="chkDesembolso" value="<%=chkDesembolso%>"/>	
	
	
	</form>
	
</body>

</html>