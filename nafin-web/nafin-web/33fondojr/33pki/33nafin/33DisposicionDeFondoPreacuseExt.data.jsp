<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.*,
		javax.servlet.http.HttpServletRequest,
		javax.servlet.http.HttpServletResponse,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		java.util.*,
		java.text.SimpleDateFormat,
		java.math.*,
		com.netro.fondojr.*,
		com.netro.fondosjr.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		java.io.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String tipoMonitor  = request.getParameter("tipoMonitor")==null?"":request.getParameter("tipoMonitor");

/*	SE CREA NUEVO OBJETO PARA ACCESAR EL EJB */
FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
JSONObject jsonObj = new JSONObject();
JSONArray jsObjArray = new JSONArray();
String msgError = "";

try{	
	
	if(informacion.equals("DisposicionDeFondoInicializar")) {
		String nombre_usuario = (String)request.getSession().getAttribute("strNombreUsuario");
		String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");
		
		List dispfondoTemp = fondoJunior.DisposicionDeFondoConsulta(nombre_usuario, clavePrograma);
		List lstDispfondo = new ArrayList();
		if(!dispfondoTemp.isEmpty()){ // si la consulta trae resultados en tonces el request lo mandara al jsp
			for(int i=0; i<dispfondoTemp.size();i++){
				HashMap hmDispfondoTemp = new HashMap();
				List lstRegDisp = (List)dispfondoTemp.get(i);
				
				if(i==0){
					jsonObj.put("totalRegDisp", (String)lstRegDisp.get(10));
					jsonObj.put("montoTotalRegDisp", (String)lstRegDisp.get(11));
					jsonObj.put("montoTotalIntDisp", (String)lstRegDisp.get(12));
					jsonObj.put("montoTotalCapDisp", (String)lstRegDisp.get(13));
				}
				
				hmDispfondoTemp.put("fechaautorizacion", (String)lstRegDisp.get(1));
				hmDispfondoTemp.put("fechaoperacion", (String)lstRegDisp.get(1));
				hmDispfondoTemp.put("fechafide", (String)lstRegDisp.get(2));
				hmDispfondoTemp.put("prestamo", (String)lstRegDisp.get(3));
				hmDispfondoTemp.put("numsirac", (String)lstRegDisp.get(4));
				hmDispfondoTemp.put("cliente", (String)lstRegDisp.get(5));
				hmDispfondoTemp.put("numcuota", (String)lstRegDisp.get(6));
				hmDispfondoTemp.put("capital", (String)lstRegDisp.get(7));
				hmDispfondoTemp.put("interes", (String)lstRegDisp.get(8));
				hmDispfondoTemp.put("totalpagar", (String)lstRegDisp.get(9));
				hmDispfondoTemp.put("registrostotal", (String)lstRegDisp.get(10));
				hmDispfondoTemp.put("capitaltotal", (String)lstRegDisp.get(11));
				hmDispfondoTemp.put("interestotal", (String)lstRegDisp.get(12));
				hmDispfondoTemp.put("sumatotalpagar", (String)lstRegDisp.get(13));
				lstDispfondo.add(hmDispfondoTemp);
			}
		}else{
			dispfondoTemp = new ArrayList();
		}
		
		jsObjArray = JSONArray.fromObject(lstDispfondo);
		jsonObj.put("dispfondoTemp", jsObjArray.toString());
		jsonObj.put("total", jsObjArray.size()+"");
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("DisposicionDeFondoAutorizacion")) {
		String _acuse = "";
		String pkcs7 = (request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
		String serial = _serial;
		String folioCert = "";
		String externContent = (request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		char getReceipt = 'Y';	
		boolean respOK = true;

		if (!serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
			folioCert = "ABCDE123456789";
			Seguridad s = new Seguridad();
			if (s.autenticar(folioCert, serial, pkcs7, externContent, getReceipt)) {
				_acuse = s.getAcuse();
				jsonObj.put("acuseRecibo",_acuse);
				
				String nombre_usuario = (String)request.getSession().getAttribute("strNombreUsuario");
				String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");
				
				//List disp = fondoJunior.DisposicionDeFondoInsert(nombre_usuario, clavePrograma); // Este metodo hace la insersion y regresa dispifondo = "1"
						
			}else{
				respOK = false;
				//throw new NafinException("GRAL0021");
			}
		}else{
			respOK = false;
			//throw new NafinException("GRAL0021");		
		}
		
		
		jsonObj.put("authRespOK", new Boolean(respOK));
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("GeneraArchivosDisp")) {
	
		CreaArchivo archivo = new CreaArchivo();
		String login_usuario = (String)request.getSession().getAttribute("Clave_usuario");
		String nombre_usuario = (String)request.getSession().getAttribute("strNombreUsuario");
		String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");
		String nombrePrograma = (String)request.getSession().getAttribute("descProgramaFondoJR");
		String tipo_archivo = (request.getParameter("tipo_archivo")==null)?"":request.getParameter("tipo_archivo");
		String acuse_carga = (request.getParameter("acuse_carga")==null)?"":request.getParameter("acuse_carga");
		
		String nombreArchivo = "";
		
		//try{
		
		if(tipo_archivo.equals("ACUSE_PDF")){
			String totalPagar ="";
			int numRegistros = 0;
			
			List dispfondoTemp = fondoJunior.DisposicionDeFondoConsulta(nombre_usuario, clavePrograma);
			
			for(int i = 0; i < dispfondoTemp.size(); i++){
				List dispfondo = (List)dispfondoTemp.get(i);
				totalPagar = dispfondo.get(11).toString();
				numRegistros++;
			}
			
			nombreArchivo = archivo.nombreArchivo()+".pdf";
			
			ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
			
			String pais        = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
			String noCliente    = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
			String nombre      = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
			String nombreUsr   = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
			String logo      	 = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
			
			pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
			//FODEA 026 - 2010 ACF (I)
			pdfDoc.addText(nombrePrograma, "titulo", ComunesPDF.CENTER);
			//FODEA 026 - 2010 ACF (F)
			float anchoCelda1[] = {50f, 50f};
			pdfDoc.setTable(2, 50, anchoCelda1);
			
			pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		  pdfDoc.addText("La autentificación se llevó a cabo con éxito \n Recibo: "+acuse_carga, "titulo", ComunesPDF.CENTER);
		  pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		  
		  pdfDoc.setCell("", "celda01",ComunesPDF.CENTER, 2);
		  pdfDoc.setCell("Total de registros por Disposición", "formas", ComunesPDF.LEFT);
		  pdfDoc.setCell(Integer.toString(numRegistros), "formas",ComunesPDF.CENTER);
		
		  pdfDoc.setCell("Monto total de Vencimientos no Pagados", "formas",ComunesPDF.LEFT);
		  pdfDoc.setCell((String)"$" + totalPagar, "formas",ComunesPDF.CENTER);
		  
		  pdfDoc.setCell("Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisión tendrá validez para todos los efectos legales.", "formas", ComunesPDF.LEFT, 2);
		  pdfDoc.addTable();
		  pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		  
		  float anchoCelda2[] = {10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f};
		  
			  pdfDoc.setTable(14, 100, anchoCelda2);
			
			  pdfDoc.setCell("NO PAGADOS", "celda01",ComunesPDF.CENTER, 14);		  
			  pdfDoc.setCell("Fecha Autorización nafin", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Fecha operación", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Fecha pago fide", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Préstamo", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Num. Cliente SIRAC", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Cliente", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Num. Cuota a Pagar", "celda01", ComunesPDF.CENTER); 
			  pdfDoc.setCell("Capital", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Interés", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Intereses Moratorios", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Total a pagar", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Estatus del Registro", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Origen", "celda01", ComunesPDF.CENTER);
			  pdfDoc.setCell("Estatus de la Validación", "celda01", ComunesPDF.CENTER);
			  
			  pdfDoc.setHeaders();
		  
		  for(int i = 0; i < dispfondoTemp.size(); i++){
			 List dispfondo = (List)dispfondoTemp.get(i);
					
			 pdfDoc.setCell((String)dispfondo.get(0), "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell((String)dispfondo.get(1), "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell((String)dispfondo.get(2), "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell((String)dispfondo.get(3), "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell((String)dispfondo.get(4), "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell((String)dispfondo.get(5), "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell((String)dispfondo.get(6), "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell("$ " + Comunes.formatoDecimal(dispfondo.get(7), 2), "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell("$ " + Comunes.formatoDecimal(dispfondo.get(8), 2), "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell("--", "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell("$ " + Comunes.formatoDecimal(dispfondo.get(9), 2), "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell("No pagadas", "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell("No Definido", "formas", ComunesPDF.CENTER);
			 pdfDoc.setCell("En proceso", "formas", ComunesPDF.CENTER);
				
		  }
			int i = 0;
			List dispfondo = (List)dispfondoTemp.get(i);
					
			pdfDoc.setCell("", "formas",ComunesPDF.CENTER, 6);		  
			pdfDoc.setCell("Totales", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("$ " + Comunes.formatoDecimal(dispfondo.get(11), 2), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("$ " + Comunes.formatoDecimal(dispfondo.get(12), 2), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("--", "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("$ " + Comunes.formatoDecimal(dispfondo.get(13), 2), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell("Num. Registros no pagados", "celda01", ComunesPDF.CENTER, 2);
			pdfDoc.setCell((String)dispfondo.get(10), "formas", ComunesPDF.CENTER);
		
			pdfDoc.addTable();
			pdfDoc.endDocument();					
		
			}	
			  else if(tipo_archivo.equals("ACUSE_CSV")){
		
			BigDecimal totalReembolsos = new BigDecimal("0.00");
			int numRegistros = 0;
			String totalPagar ="";
			StringBuffer contenidoArchivo = new StringBuffer();
		  
			nombreArchivo = archivo.nombreArchivo();
			List dispfondoTemp = fondoJunior.DisposicionDeFondoConsulta(nombre_usuario, clavePrograma);
		  
		  for(int i = 0; i < dispfondoTemp.size(); i++){
			 List dispfondo = (List)dispfondoTemp.get(i);
			 totalPagar = dispfondo.get(11).toString();
			 numRegistros++;
		  }
		  
				contenidoArchivo.append(nombrePrograma + ",\n");//FODEA 026 - 2010 ACF
				
		  contenidoArchivo.append("\nLa autentificación se llevó a cabo con éxito \n Recibo: "+acuse_carga+"\n,\n");
		  
		  contenidoArchivo.append("Total de registros por Disposición,");
		  contenidoArchivo.append(Integer.toString(numRegistros) + "\n");
		
		  contenidoArchivo.append("Monto Total de Reembolsos Cargados,");
		  contenidoArchivo.append("$" + totalPagar.toString() + "\n");
		
		  contenidoArchivo.append("Usuario,");
		  contenidoArchivo.append(login_usuario + " - " + nombre_usuario + "\n");
		  
		  contenidoArchivo.append("Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisión tendrá validez para todos los efectos legales.\n,\n");
		
		  contenidoArchivo.append("Fecha Autorización nafin,");
		  contenidoArchivo.append("Fecha operación,");
		  contenidoArchivo.append("Fecha pago fide,");
		  contenidoArchivo.append("Préstamo,");
		  contenidoArchivo.append("Num. Cliente SIRAC,");
		  contenidoArchivo.append("Cliente,");
                    contenidoArchivo.append("Num. Cuota a Pagar,");
                    contenidoArchivo.append("Capital,");
                    contenidoArchivo.append("Interés,");
                    contenidoArchivo.append("Intereses Moratorios,");
                    contenidoArchivo.append("Total a pagar,");
                    contenidoArchivo.append("Estatus del Registro,");
                    contenidoArchivo.append("Origen,");
		  contenidoArchivo.append("Estatus de la Validación\n");
				String nomcliente = "";
		  for(int i = 0; i < dispfondoTemp.size(); i++){
			 List dispfondo = (List)dispfondoTemp.get(i);
					nomcliente = dispfondo.get(5).toString();
		 
					contenidoArchivo.append((String)dispfondo.get(0) + ",");
			 contenidoArchivo.append((String)dispfondo.get(1) + ",");
			 contenidoArchivo.append((String)dispfondo.get(2) + ",");
			 contenidoArchivo.append((String)dispfondo.get(3) + ",");
					contenidoArchivo.append((String)dispfondo.get(4) + ",");
					contenidoArchivo.append(nomcliente.replaceAll(","," ") + ",");
					contenidoArchivo.append((String)dispfondo.get(6) + ",");
					contenidoArchivo.append("$ " + Comunes.formatoDecimal(dispfondo.get(7), 2) + ",");
					contenidoArchivo.append("$ " + Comunes.formatoDecimal(dispfondo.get(8), 2) + ",");
					contenidoArchivo.append("--,");
					contenidoArchivo.append("$ " + Comunes.formatoDecimal(dispfondo.get(9), 2) + ",");
					contenidoArchivo.append("No pagadas,");
					contenidoArchivo.append("No Definido,");
					contenidoArchivo.append("En proceso" + "\n");
		  }
				int i = 0;
				List dispfondo = (List)dispfondoTemp.get(i);
				 
				contenidoArchivo.append(",");
				contenidoArchivo.append(",");
				contenidoArchivo.append(",");
				contenidoArchivo.append(",");
				contenidoArchivo.append(",");
				contenidoArchivo.append(",");
				contenidoArchivo.append("Totales,");
				contenidoArchivo.append("$ " + Comunes.formatoDecimal(dispfondo.get(11), 2) + ",");
				contenidoArchivo.append("$ " + Comunes.formatoDecimal(dispfondo.get(12), 2) + ",");
				contenidoArchivo.append("--" + ",");
				contenidoArchivo.append("$ " + Comunes.formatoDecimal(dispfondo.get(13), 2) + ",");
				contenidoArchivo.append("Num. Registros no pagados" + ",");
				contenidoArchivo.append("$ " + Comunes.formatoDecimal(dispfondo.get(10), 2) + "\n");
			 
				archivo.make(contenidoArchivo.toString(), strDirectorioTemp, nombreArchivo, ".csv");
				
				nombreArchivo = nombreArchivo + ".csv";
			}
			
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	
	}
	
	
}catch(Throwable t){
	msgError =  t.getMessage();
	jsonObj.put("msgError", msgError);
	jsonObj.put("success", new Boolean(false));
	infoRegresar = jsonObj.toString();
}

System.out.println("infoRegresar = " + infoRegresar);

%>
<%=infoRegresar%>