<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalogoOperacionFJR,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		java.text.SimpleDateFormat,
		org.apache.commons.logging.Log,
		com.netro.fondojr.*,
		com.netro.fondosjr.*,
		java.math.BigDecimal,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
	// variables para la iniciación 
	String infoRegresar = "",  consulta = "", mensaje ="", seguridad = "";
	JSONObject jsonObj = new JSONObject();
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String clavePrograma = (String) session.getAttribute("cveProgramaFondoJR");
	FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
	JSONArray registros1 = new JSONArray();
	/// variable para la consulta del preAcuse
	String tipoInformacion = (request.getParameter("tipoInformacion")!=null)?request.getParameter("tipoInformacion"):"";
	InfMovGeneralesFondoJR paginadorCaptura = new InfMovGeneralesFondoJR();
	String proceso     = request.getParameter("proceso")==null?"":request.getParameter("proceso");
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
	String importe         = request.getParameter("importe")==null?"":request.getParameter("importe");
	String referencia     = request.getParameter("referencia")==null?"":request.getParameter("referencia");
	String observaciones  = request.getParameter("observaciones")==null?"":request.getParameter("observaciones");
	String fecha        = request.getParameter("fechaValor")==null?"":request.getParameter("fechaValor");
	String TipoOperacion   = request.getParameter("tipoOperacion")==null?"":request.getParameter("tipoOperacion");
	String numMovimiento     = request.getParameter("numMovimiento")==null?"":request.getParameter("numMovimiento");
	String modificaCaptura     = request.getParameter("modificaCaptura")==null?"":request.getParameter("modificaCaptura");
	String modificar     = request.getParameter("modificar")==null?"":request.getParameter("modificar");
	double  importeTotal = 0;
	double totales = 0;
	// variables para boton termina Operacion 
	String pkcs7     = request.getParameter("pkcs7")==null?"":request.getParameter("pkcs7");
	String numProceso   = request.getParameter("numProceso")==null?"":request.getParameter("numProceso");
	String externContent     = request.getParameter("textoFirmado")==null?"":request.getParameter("textoFirmado");
	String validCert = application.getInitParameter("paramValidaSerialCrt");
	String termina     = strNombreUsuario;
	double total = 0;
	double totales1 = 0;
	InfAcuseMoviGenerales paginadoAcuse = new InfAcuseMoviGenerales();
	CQueryHelperRegExtJS queryHelper1 = new CQueryHelperRegExtJS( paginadoAcuse);
	//variables para el boton consulta 
	InfConsultaMovimientosJR paginador1 = new InfConsultaMovimientosJR();
	//variables para boton agrega oparacion 
	InfCatOperaciones paginadorOpera = new InfCatOperaciones();
	String agregaOperacion   = request.getParameter("agregaOperacion")==null?"":request.getParameter("agregaOperacion");
	String nombre     = request.getParameter("nombre")==null?"":request.getParameter("nombre");
	String regNumero         = request.getParameter("regNumero")==null?"":request.getParameter("regNumero");
	if(informacion.equals("Inicializar")){
		CatalogoOperacionFJR catalogo = new CatalogoOperacionFJR();
      catalogo.setClave("ig_oper_fondo_jr");
      catalogo.setDescripcion("cg_descripcion");
      catalogo.setPrograma(clavePrograma);
		List catalogoOperacion = catalogo.getListaElementos();
		String proceso1 = fondoJunior.Numeroproceso();
		for(int i=0;i<catalogoOperacion.size();i++){
			registros1.add(catalogoOperacion.get(i));	
			}
			consulta =  "{\"success\": true, \"total\": \"" + registros1.size() + "\", \"registros\": " + registros1.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("PROCESO",	proceso1		);
			infoRegresar = jsonObj.toString(); 
	}else	if(informacion.equals("Consultar") ||tipoInformacion.equals("recargaConulta")|| tipoInformacion.equals("AgregaCaptura")||informacion.equals("ArchivoPDF")) {
		if(tipoInformacion.equals("AgregaCaptura") ||tipoInformacion.equals("recargaConulta")){ 
			mensaje="";
			JSONObject 	resultado	= new JSONObject();
			String mod = "";
			if(tipoInformacion.equals("AgregaCaptura")){
				JSONObject jsonObjOp = new JSONObject();
				try {
					if (!fecha.equals("") && !importe.equals("") && !referencia.equals("") && !observaciones.equals("") && !TipoOperacion.equals("") && modificaCaptura.equals("")) {
						fondoJunior.insertMovGeneralesJR(proceso, fecha,importe,referencia,observaciones,TipoOperacion, clavePrograma);	
						mensaje = "G";
						mod = "G";
					}else if (!fecha.equals("") && !importe.equals("") && !referencia.equals("") && !observaciones.equals("") && !TipoOperacion.equals("") && !modificaCaptura.equals("") && !numMovimiento.equals("")) {
						//para Modificar Movimientos Generales Fondo JR	
						fondoJunior.ModificarMovGeneralesJR(numMovimiento,fecha,importe,referencia, observaciones,TipoOperacion,modificar,clavePrograma);
						mensaje = "El registro se ha modificado con éxito";
						mod = "M";
					}
				}catch(Throwable t){
					t.printStackTrace();
					throw t;
				}
		
			}
			if(informacion.equals("Consultar") || tipoInformacion.equals("recargaConulta")){
				double totalesCaptura = 0;
				int start = 0;
				int limit = 0;
				paginadorCaptura.setProceso(proceso);
				paginadorCaptura.setFechaValor(fecha);
				paginadorCaptura.setOperacion(TipoOperacion);
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginadorCaptura);
				try {
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));				
				} catch(Exception e) {
					throw new AppException("Error en los parametros recibidos", e);
				}
				try {
					if (operacion.equals("Generar")) {
						queryHelper.executePKQuery(request); 
					}
					Registros registros = queryHelper.getPageResultSet(request,start,limit);
					while(registros.next()){
						String	totalImporte 		= registros.getString("IMPORTE").toString();
						String	opera 		= registros.getString("OPERACION").toString();;
						importeTotal = ( Double.parseDouble(totalImporte));
						if ( opera.equals("Suma")) { 
							totales  = totales+importeTotal; 
						}else if (opera.equals("Resta")) {
							totales  = totales - importeTotal;	
						}
						seguridad = "E P O-DATOS|Total Monto Documento|Total Monto Descuento|Total Monto de Inter&eacute;s|Total Importe a Recibir\\n"+
										registros.getString("FECHA_MOVIMIENTO")+"|"+registros.getString("FECHA_VALOR")+"|"+registros.getString("REFERENCIA")+"|"+
										Comunes.formatoDecimal(registros.getString("IMPORTE"),2)+"|"+registros.getString("MOVIMIENTO")+"|"+
										registros.getString("OPERACION")+"|"+registros.getString("OBSERVACIONES")+"\\n"+
										"Al solicitar el factoraje electrónico o descuento electrónico del documento que selecciono e identifico, transmito los derechos"+ 
										" que sobre el mismo ejerzo, de acuerdo con lo establecido por los artículos 427 de la ley general de títulos y operaciones de crédito,"+ 
										" 32 c del código fiscal de la federación y 2038 del código civil federal, haciéndome sabedor de su contenido y alcance, condicionado a"+
										" que se efectúe el descuento electrónico o el factoraje electrónico";
					}
					String consulta2	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
					resultado = JSONObject.fromObject(consulta2);
					resultado.put("FIRMA",seguridad);
					resultado.put("accion",mod);
					resultado.put("mensaje", mensaje);
					resultado.put("IMPORTE_TOTAL",	String.valueOf(totales)			);
					infoRegresar = resultado.toString();
				} catch(Exception e) {
					throw new AppException("Error en la paginacion", e);
				}
			}
		}else{
			paginador1.setClavePrograma(clavePrograma);
			paginador1.setFechaValor(fecha);
			paginador1.setOperacion(TipoOperacion);  
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador1);
			int start = 0;
			int limit = 0;
			Registros registrosC =null;
			if (informacion.equals("Consultar")  )  {
				try {
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));				
				} catch(Exception e) {
					throw new AppException("Error en los parametros recibidos", e);
				}
				JSONObject 	resultado	= new JSONObject();
				try {
					if (operacion.equals("Generar")) {
						queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
					}
					Registros registros = queryHelper.getPageResultSet(request,start,limit);
					/*while(registros.next()){
						String	totalImporte 		= registros.getString("IMPORTE").toString();
						String	opera 		= registros.getString("OPERACION").toString();;
						importeTotal = ( Double.parseDouble(totalImporte));
						if ( opera.equals("Suma")) { 
							totales  = totales+importeTotal; 
						}else if (opera.equals("Resta")) {
							totales  = totales - importeTotal;	
						}
					}*/
					String totalConsulta = fondoJunior.totalConMovimientos(TipoOperacion, fecha, clavePrograma);
					String consulta2	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
					resultado = JSONObject.fromObject(consulta2);
					resultado.put("IMPORTE_TOTAL",	String.valueOf(totalConsulta)			);
					resultado.put("TIPO",	"N"	);
					infoRegresar = resultado.toString();
				} catch(Exception e) {
					throw new AppException("Error en la paginacion", e);
				}
			}else if (informacion.equals("ArchivoPDF")	){
				try {
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));
				} catch(Exception e) {
					throw new AppException("Error en los parametros recibidos", e);
				}
				try {
					String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
					JSONObject jsonObj3 = new JSONObject();
					jsonObj3.put("success", new Boolean(true));
					jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					infoRegresar = jsonObj3.toString();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
			}
		}
	}else if(informacion.equals("GenereArchivoAcuse") ||informacion.equals("consultaAcuse")){
		JSONObject jsonObjOp = new JSONObject();
		ArrayList lista = new ArrayList();
		String NumeroAcuse = "";
		paginadoAcuse.setProceso(proceso);
		int i = 0;
		int numeroRegistros =0;
		total = 0;
		totales = 0;
		totales1 = 0;
		boolean   validacion = true;
		String folioCert = "MFJR"+new SimpleDateFormat("ddMMyy").format(new java.util.Date());
		if(informacion.equals("consultaAcuse")){
			try{
				lista.add(0,_serial);
				lista.add(1,externContent);  
				lista.add(2,pkcs7);   
				lista.add(3,folioCert);
				lista.add(4,termina);   
				lista.add(5,numProceso);
				if (!termina.equals("")){
					NumeroAcuse = fondoJunior.TerminaOperacion(null,validacion ,lista);
					if(NumeroAcuse!=null){
						mensaje = " Acuse -La Autentificación se llevo Acabo con exito ";
					}else{
						mensaje = " Acuse -La Autentificación No se llevo Acabo con exito ";
					}
				}
				List datos = fondoJunior.AcuseMoviGenerales(numProceso);
			} catch(Exception e) {
				System.out.println("AcuseMoviGeneralesJRAction :: inicializar :: Exception");
				return ;
			}
			try {
				if (operacion.equals("Generar") ) {	
					Registros reg	=	queryHelper1.doSearch();
					StringBuffer Totales = new StringBuffer();
					while(reg.next()){
						String NUMERO = reg.getString("NUMERO");
						String FECHA_MOVIMIENTO = reg.getString("FECHA_MOVIMIENTO") ;
						String FECHA_VALOR = reg.getString("FECHA_VALOR");
						String IMPORTE1 = reg.getString("IMPORTE");
						String REFERENCIA = reg.getString("REFERENCIA") ;
						String OPERACION = reg.getString("OPERACION");
						String OBSERVACIONES = reg.getString("OBSERVACIONES");
						total =  Double.parseDouble(IMPORTE1);	
						if ( OPERACION.equals("Suma")) { 
							totales1  = totales1+total; 					
						}
						if (OPERACION.equals("Resta")) {
							totales  = totales+total;				
						}				
						i++;
					}
					numeroRegistros = reg.getNumeroRegistros();
					consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
					jsonObj = JSONObject.fromObject(consulta);
					jsonObj.put("mensaje", mensaje);
					jsonObj.put("NumeroAcuse", NumeroAcuse);
					jsonObj.put("numeroMovimientos",String.valueOf(numeroRegistros) );  
					jsonObj.put("montoAportaciones",String.valueOf(totales1));
					jsonObj.put("montoRetiron",String.valueOf(totales) );
					infoRegresar = jsonObj.toString();
				}
			} catch(Exception e) {
				throw new AppException("Error al obtener los datos", e);
			}
		}else if(informacion.equals("GenereArchivoAcuse") ){
			String numAcuse = (request.getParameter("numAcuse")!=null)?request.getParameter("numAcuse"):"";
			try {
				paginadoAcuse.setNumAcuse(numAcuse);
				String nombreArchivo = queryHelper1.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error son demasiados registros", e);
			}
		}
	}else if(informacion.equals("ConsultarOperacion") ||informacion.equals("GenerarOperacion")){
		paginadorOpera.setClavePrograma(clavePrograma);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginadorOpera);
		int start = 0;
		int limit = 0;
		Registros registrosC =null;
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));				
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			JSONObject 	resultado	= new JSONObject();
			try {
				if (operacion.equals("GenerarOperacion")) {
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				Registros registros = queryHelper.getPageResultSet(request,start,limit);
				String consulta2	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
				resultado = JSONObject.fromObject(consulta2);
				infoRegresar = resultado.toString();
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		
	}else if(informacion.equals("AgregaOperacion")){
		JSONObject jsonObjOp = new JSONObject();
		try {
			//para insertar el catalogo de Operaciones
			if (!agregaOperacion.equals("")  && !nombre.equals("") && !modificar.equals("S") ){			
			//para insertar el catalogo de Operaciones
				fondoJunior.insertarCatalagoOperacion(nombre,agregaOperacion,clavePrograma);	
				mensaje = "G";
				jsonObjOp.put("accion", "G");
			}else if (!nombre.equals("") && !agregaOperacion.equals("") && !modificar.equals("N") ){	
			//para modificar el catalogo de Operaciones
				fondoJunior.modifica(regNumero,nombre,modificar,agregaOperacion);
				mensaje = "El registro se ha modificado con éxito";
				jsonObjOp.put("accion", "M");
			}
			jsonObjOp.put("success",new Boolean(true));
			jsonObjOp.put("mensaje", mensaje);
			infoRegresar = jsonObjOp.toString();
		}catch(Throwable t){
			t.printStackTrace();
			throw t;
		}
	}else if(informacion.equals("EliminarCaptura")){
		try {
			if (!numMovimiento.equals("") ) {
				fondoJunior.eliminarMovGeneralesJR( numMovimiento);
				mensaje = "El registro se ha eliminado con éxito";		
			}
			JSONObject jsonObjOp = new JSONObject();
			jsonObjOp.put("success",new Boolean(true));
			jsonObjOp.put("mensaje", mensaje);
			jsonObjOp.put("accion", "EC");
			infoRegresar = jsonObjOp.toString();
		}catch(Throwable t){
			t.printStackTrace();
			throw t;
		}
	}else if(informacion.equals("Eliminar")){
		try {
  
			//para insertar el catalogo de Operaciones
				if (!regNumero.equals("")){	
					fondoJunior.eliminar(regNumero);	
					mensaje = "El registro se ha eliminado con éxito";				
				}
				JSONObject jsonObjOp = new JSONObject();
				jsonObjOp.put("success",new Boolean(true));
				jsonObjOp.put("mensaje", mensaje);
				jsonObjOp.put("accion", "E");
				infoRegresar = jsonObjOp.toString();
		}catch(Throwable t){
				t.printStackTrace();
				throw t;
		}
		
	}else if(informacion.equals("CancelarOperacion")){
		JSONObject jsonObjOp = new JSONObject();
		try {
			fondoJunior.CancelarMovGeneralesJR(proceso);
			jsonObjOp.put("success",new Boolean(true));
			jsonObjOp.put("mensaje", "La Operación Capturada ha sido Cancelada con éxito");
			infoRegresar = jsonObjOp.toString(); 
	 } catch(Exception e){
			mensaje="ERROR "+e.getMessage();
			log.error("ERROR "+ mensaje);   
		}
		
	}

	

%>
<%=infoRegresar%>