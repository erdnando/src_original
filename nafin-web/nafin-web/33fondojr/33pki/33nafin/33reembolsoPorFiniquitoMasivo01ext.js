Ext.onReady(function() {

 	/**********		Variables globales		**********/
	var texto_plano = '';
	var recordType  = Ext.data.Record.create([ {name:'clave'}, {name:'descripcion'} ]);

	/**********		Datos del layout de Cr�ditos Pagados		**********/
	var layoutCreditos = [
		['1', 'ig_pr&eacute;stamo',   'Pr&eacute;stamo', 'NUMBER(12)',   '12',   'N�mero del Pr&eacute;stamo'  ],
		['2', 'ig_cliente',           'N�mero SIRAC',    'NUMBER(11)',   '11',   'C�digo del Cliente'          ],
		['3', 'fg_totalvencimiento',  'Total a Pagar',   'NUMBER(19,2)', '19,2', 'Total Vencimiento'           ],
		['4', 'cg_observaciones',     'Observaciones',   'VARCHAR2',     '200',  'Observaciones del Desembolso']
	];

	/********** Inicializar **********/
	var inicializar = function(){
		Ext.Ajax.request({
			url: '33reembolsoPorFiniquito01ext.data.jsp',
			params: Ext.apply({
				informacion: 'Inicializacion'
			}),
			callback: procesarInicializar
		});	
	};

	/********** Resultado despues del m�todo inicializar **********/
	var procesarInicializar = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('programa_id').setValue(Ext.util.JSON.decode(response.responseText).descPrograma);
		}
	};

	/***** Descargar archivos de errores. No es la llamada normal cono con un pdf o xls. *****/
	function descargaArchivo() {
		var nombre = Ext.getCmp('nombre_archivo_id').getValue();
		var archivoTmp = '/00tmp/33fondojr/' + nombre;
		var params = {nombreArchivo: archivoTmp};
		formaResultados.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
		formaResultados.getForm().getEl().dom.submit();
		Ext.getCmp('btnDescargaRegError').setIconClass('icoTxt');
	};

	/***** Descargar archivos PDF *****/  
	function descargaArchivoPdf(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;	
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			formaTerminarCaptura.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			formaTerminarCaptura.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnImprimir').setIconClass('icoPdf');
		Ext.getCmp('btnImprimirAcuse').setIconClass('icoPdf');
		Ext.getCmp('btnGeneraArchivoAcuse').setIconClass('icoXls');
	};
	
	/********** Proceso para subir el archivo **********/
	function subirArchivo(){
		Ext.getCmp('formaCargaMasiva').getForm().submit({
			url:					'33reembolsoPorFiniquito01ext.data.jsp?informacion=Subir_Archivo',
			waitMsg:				'Cargando archivo...',
			waitTitle:			'Por favor espere',
			clientValidation: true,
			success: function(form, action) {
				procesaArchivo(null,  true,  action.response );
			},
			failure: function(form, action) {
				action.response.status = 200;
				Ext.MessageBox.alert('Error', 'Error al subir el archivo. Por favor int�ntelo de nuevo');
			}
		});
	};

	/********** Valida el archivo que se subi� previamente **********/ 
	var procesaArchivo = function(opts, success, response) {
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('nombre_archivo_id').setValue(resp.NOMBRE_ARCHIVO);
			Ext.getCmp('total_reembolsos_id').setValue(resp.monto_reembolso);
			Ext.getCmp('folio_batch_txt_id').setValue(resp.folio_batch_txt);
			Ext.getCmp('proceso_carga_id').setValue(resp.proceso_carga);
			
			Ext.getCmp('formaResultados').show();
			Ext.getCmp('gridCreditosPagados').hide();
			if(resp.cargaBatch == 'S'){
				Ext.getCmp('formaCargaMasiva').hide();
				Ext.getCmp('carga_correcta_id').show();
				Ext.getCmp('btnAceptarCarga').show();
				Ext.getCmp('titulos_id').hide();
				Ext.getCmp('listas_id').hide();
				Ext.getCmp('totales_id').hide();
				Ext.getCmp('btnDescargaRegError').hide();
				Ext.getCmp('btnContinuarCarga').hide();
				Ext.getCmp('btnCancelarCarga').hide();
			}
			if(resp.st_archivo == 'CARGADO'){
				if(resp.cargaBatch == 'N'){
					Ext.getCmp('titulos_id').show();
					Ext.getCmp('listas_id').show();
					Ext.getCmp('totales_id').show();
					Ext.getCmp('btnDescargaRegError').show();
					Ext.getCmp('carga_correcta_id').hide();
					Ext.getCmp('btnAceptarCarga').hide();
					if(resp.num_reg_ok > 0){
						Ext.getCmp('btnContinuarCarga').show();
					}
					Ext.getCmp('total_sin_errores_id').setValue(resp.num_reg_ok);
					Ext.getCmp('correctos_id').setValue(resp.correctos);
					Ext.getCmp('errores_id').setValue(resp.errores);					
				} else if(resp.cargaBatch == 'S'){
					Ext.getCmp('btnAceptarCarga').show();
				}
				if(Ext.getCmp('errores_id').getValue() == ''){
					Ext.getCmp('btnDescargaRegError').hide();
				} else {
					Ext.getCmp('btnDescargaRegError').show();
				}
			}	
		} else{
			Ext.MessageBox.alert('Error', 'Error al subir el archivo. Por favor int�ntelo de nuevo');
			Ext.getCmp('formaCargaMasiva').getForm().reset();
			Ext.getCmp('formaResultados').hide();
		}
	};

	/********** Continua con el proceso de carga de los reembolsos **********/
	function continuarCarga(){
		Ext.Ajax.request({
			url: '33reembolsoPorFiniquito01ext.data.jsp',
			params: Ext.apply({
				informacion: 'ReembolsosPorFiniquitoPreacuse',
				proceso_carga: Ext.getCmp('proceso_carga_id').getValue(),
				no_prestamo: '',
				monto_reembolso: Ext.getCmp('total_reembolsos_id').getValue(),
				tipo_carga: 'MASIVA'
			}),
			callback: procesarContinuarCarga
		});
	};

	/**********Una vez que se inici� el proceso de carga, se realiza la consulta para llenar el grid**********/ 
	var procesarContinuarCarga = function(opts, success, response) {
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.getCmp('formaTerminarCaptura').show();
			consultaData.load({
				params: Ext.apply({
					proceso_carga: Ext.getCmp('proceso_carga_id').getValue(),
					tipo_archivo:	'PREACUSE',
					tipo_carga:		'MASIVA'
				})
			});
		}
	};

	/********** Proceso para generar el Grid Consulta **********/
	var procesarConsultaData = function(store, arrRegistros, opts){
		var formaCargaMasiva = Ext.getCmp('formaCargaMasiva');
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();

		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			if(jsonData.tipo_archivo == 'ACUSE'){
			
				Ext.getCmp('total_desembolsos_id').setValue(store.getTotalCount());
				Ext.getCmp('cantidad_id').setValue(jsonData.total_desembolso);
				if(store.getTotalCount() > 0) {
					consultaTotal.load({
						params : Ext.apply({
							desembolsos_tot: Ext.getCmp('total_desembolsos_id').getValue(),
							cantidad_total: Ext.getCmp('cantidad_id').getValue()
						})
					});
				} else {
					formaCargaMasiva.el.unmask();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridTotal').hide();
					el.unmask();
				}
				
			} else{

				Ext.getCmp('total_desembolsos_id').setValue(store.getTotalCount());
				Ext.getCmp('cantidad_id').setValue(jsonData.total_desembolso);
				Ext.getCmp('disposicion_id').setValue(jsonData.disposicion);
				if(  Ext.getCmp('total_desembolsos_id').getValue() != '' && Ext.getCmp('cantidad_id').getValue() != '' ){
				
					texto_plano  = 'No. Total de Amortizaciones a Desembolsar|' + Ext.getCmp('total_desembolsos_id').getValue() + '\n';
					texto_plano += 'Monto Total de Amortizaciones a Desembolsar|' + Ext.getCmp('cantidad_id').getValue() + '\n';
					texto_plano += 'Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento ' +
										'contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, ' +
										'dicha transmisi�n tendr� validez para todos los efectos legales.\n';
					texto_plano += jsonData.texto_plano;
					texto_plano += 'Total Monto por Desembolsos|' + Ext.getCmp('cantidad_id').getValue() + '\n';
					texto_plano += 'Total Desembolsos|' + Ext.getCmp('total_desembolsos_id').getValue() + '\n';
					
				}
				if(store.getTotalCount() > 0) {
					formaCargaMasiva.el.unmask();
					el.unmask();
					consultaTotal.load({
						params: Ext.apply({
							desembolsos_tot: Ext.getCmp('total_desembolsos_id').getValue(),
							cantidad_total: Ext.getCmp('cantidad_id').getValue()
						})
					});
				} else {
					formaCargaMasiva.el.unmask();
					el.unmask();
					Ext.getCmp('formaCargaMasiva').show();		
					Ext.getCmp('formaResultados').show();
					Ext.getCmp('formaTerminarCaptura').hide();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridTotal').hide();
				}
			}
		}
	};
	
	/********** Proceso para generar el Grid de Totales **********/
	var porcesaConsultaTotal = function(store, arrRegistros, opts){
		var jsonData = store.reader.jsonData;
		if(store.getTotalCount() > 0) {
			Ext.getCmp('formaCargaMasiva').hide();	
			Ext.getCmp('formaResultados').hide();
			Ext.getCmp('gridConsulta').show();
			Ext.getCmp('gridTotal').show();
		} else{
			Ext.getCmp('formaCargaMasiva').show();		
			Ext.getCmp('formaResultados').show();
			Ext.getCmp('gridConsulta').hide();
			Ext.getCmp('gridTotal').hide();
		}
	};

	

	/********** Transmitir desembolsos para terminar la captura **********/	
	var transmitirDesembolsos = function(pkcs7, vtexto_plano){
		
		if (Ext.isEmpty(pkcs7)) {
			Ext.Msg.alert( 'Error', 'Error en el proceso de Firmado.<br> No se puede continuar.');
			return; //Error en la firma. Termina...
		} else{
			Ext.Ajax.request({
				url: '33reembolsoPorFiniquito01ext.data.jsp',
				params: Ext.apply({
					informacion:		'ReembolsosPorFiniquitoAcuse',
					tipo_carga:			'MASIVA',
					proceso_carga:		Ext.getCmp('proceso_carga_id').getValue(),
					monto_reembolso:	Ext.getCmp('total_reembolsos_id').getValue(),
					disposicion:		Ext.getCmp('disposicion_id').getValue(),
					externContent:		vtexto_plano,
					pkcs7: 				pkcs7,
					no_prestamo:		''
					
				}),
				callback: procesarDesembolsoAcuse
			});
		}
	};
	
	/********** Proceso posterior al metodo de transmitir desembolsos **********/
	var procesarDesembolsoAcuse = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			Ext.getCmp('formaCargaMasiva').hide();
			Ext.getCmp('formaTerminarCaptura').hide();
			Ext.getCmp('formaAcuse').show();
			
			if( Ext.util.JSON.decode(response.responseText).mensaje != '' ){
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridTotal').hide();
				Ext.getCmp('display1_id').hide();
				Ext.getCmp('folio_acuse_id').hide();
				Ext.getCmp('total_amortizaciones_id').hide();
				Ext.getCmp('monto_total_amortizaciones_id').hide();
				Ext.getCmp('numero_acuse_id').hide();
				Ext.getCmp('fecha_carga_id').hide();
				Ext.getCmp('hora_carga_id').hide();
				Ext.getCmp('usuario_id').hide();
				Ext.getCmp('display2_id').hide();
				Ext.getCmp('display3_id').show();
				Ext.getCmp('btnRegresar').show();
				Ext.getCmp('btnImprimirAcuse').hide();
				Ext.getCmp('btnGeneraArchivoAcuse').hide();
				Ext.getCmp('btnSalirAcuse').hide();
				Ext.getCmp('btnAceptarCarga').show();
				Ext.Msg.alert( 'Mensaje', Ext.util.JSON.decode(response.responseText).mensaje );
			} else{
				consultaData.load({
					params: Ext.apply({
						tipo_archivo:	'ACUSE',
						proceso_carga: Ext.getCmp('proceso_carga_id').getValue(),
						disposicion: 	Ext.getCmp('disposicion_id').getValue()
					})
				});
				Ext.getCmp('display1_id').show();
				Ext.getCmp('folio_acuse_id').show();
				Ext.getCmp('total_amortizaciones_id').show();
				Ext.getCmp('monto_total_amortizaciones_id').show();
				Ext.getCmp('numero_acuse_id').show();
				Ext.getCmp('fecha_carga_id').show();
				Ext.getCmp('hora_carga_id').show();
				Ext.getCmp('usuario_id').show();
				Ext.getCmp('display2_id').show();
				Ext.getCmp('display3_id').hide();
				Ext.getCmp('btnRegresar').hide();
				Ext.getCmp('btnImprimirAcuse').show();
				Ext.getCmp('btnGeneraArchivoAcuse').show();
				Ext.getCmp('btnSalirAcuse').show();
				Ext.getCmp('btnAceptarCarga').hide();
				Ext.getCmp('folio_acuse_id').setValue(Ext.util.JSON.decode(response.responseText).folio_acuse);
				Ext.getCmp('total_amortizaciones_id').setValue(Ext.util.JSON.decode(response.responseText).numReembolsos);
				Ext.getCmp('monto_total_amortizaciones_id').setValue(Ext.util.JSON.decode(response.responseText).parcialReembolsos);
				Ext.getCmp('numero_acuse_id').setValue(Ext.util.JSON.decode(response.responseText).folio_carga);
				Ext.getCmp('fecha_carga_id').setValue(Ext.util.JSON.decode(response.responseText).fecha_carga);
				Ext.getCmp('hora_carga_id').setValue(Ext.util.JSON.decode(response.responseText).hora_carga);
				Ext.getCmp('usuario_id').setValue(Ext.util.JSON.decode(response.responseText).nombre_usuario);
			}
		}
	};
	
	/********** Se crea el store del Grid Consulta **********/
	var consultaData = new Ext.data.JsonStore({
		root:					'registros',
		url:					'33reembolsoPorFiniquito01ext.data.jsp',
		baseParams:			{
			informacion:	'Consulta_Grid_Carga_Individual'
		},			
		fields: [
			{ name: 'FECHA_MOVIMIENTO'	},
			{ name: 'PRESTAMO'			},
			{ name: 'NO_SIRAC'			},
			{ name: 'CLIENTE'				},
			{ name: 'MONTO_DESEMBOLSO'	},
			{ name: 'AMORTIZACION'		},
			{ name: 'ORIGEN'				},
			{ name: 'OBSERVACIONES'		}
		],
		totalProperty:		'total',
		messageProperty:	'msg',
		autoLoad:			false,
		listeners:			{
			load:				procesarConsultaData,
			exception:		{
				fn: 			function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	/********** Se crea el store del Grid Totales **********/
	var consultaTotal = new Ext.data.JsonStore({
		root:					'registros',
		url:					'33reembolsoPorFiniquito01ext.data.jsp',
		baseParams:			{
			informacion:	'Consulta_Grid_Total'
		},
		fields: [
			{ name: 'DESCRIPCION'},
			{ name: 'TOTAL'		}
		],
		totalProperty:		'total',
		messageProperty:	'msg',
		autoLoad:			false,
		listeners:			{
			load:				porcesaConsultaTotal,
			exception:		{
				fn: 			function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					porcesaConsultaTotal(null, null, null);
				}
			}
		}
	});
	
	/**********		Se crea el grid Consulta		**********/
	var gridConsulta = new Ext.grid.EditorGridPanel({
		width:				850,
		height:				350,
		id:					'gridConsulta',
		title:				'&nbsp;',
		style:				'margin:0 auto;',
		align:				'center',
		frame:				false,
		border:				true,
		hidden:				true,
		displayInfo:		true,
		loadMask:			true,
		stripeRows:			true,
		store:				consultaData,
		columns:				[
			{
				width:		120,
				header:		'Fecha de Movimiento',
				dataIndex:	'FECHA_MOVIMIENTO',
				align:		'center',
				sortable:	true,
				resizable:	false	
			},{
				width:		88,	
				header:		'Pr�stamo',
				dataIndex:	'PRESTAMO',			
				align:		'center',
				sortable:	true,		
				resizable:	true
			},{
				width:		150,
				header:		'N�mero de Cliente SIRAC',	
				dataIndex:	'NO_SIRAC',			
				align:		'center',
				sortable:	true,		
				resizable:	true				
			},{
				width:		188,	
				header:		'Cliente',
				dataIndex:	'CLIENTE',				
				align:		'center',
				sortable:	true,		
				resizable:	true				
			},{
				width:		130,
				header:		'Monto del Desembolso',
				dataIndex:	'MONTO_DESEMBOLSO',				
				align:		'center',
				sortable:	true,		
				resizable:	true
			},{
				width:		88,
				header:		'Amortizaci�n',
				dataIndex:	'AMORTIZACION',				
				align:		'center',
				sortable:	true,		
				resizable:	true
			},{
				width:		88,
				header:		'Origen',
				dataIndex:	'ORIGEN',				
				align:		'center',
				sortable:	true,		
				resizable:	true
			},{
				width:		250,
				header:		'Observaciones',
				dataIndex:	'OBSERVACIONES',				
				align:		'left',
				sortable:	true,		
				resizable:	true
			}
		]
	});

	/********** Se crea el grid de totales **********/
	var gridTotal = new Ext.grid.EditorGridPanel({
		width:				850,
		id:					'gridTotal',
		//title:				'&nbsp;',
		style:				'margin:0 auto;',
		align:				'center',
		frame:				false,
		border:				true,
		hidden:				true,
		displayInfo:		true,
		loadMask:			true,
		stripeRows:			true,
		autoHeight:			true,
		store:				consultaTotal,
		columns:				[
			{
				width:		600,
				header:		'Descripci�n',
				dataIndex:	'DESCRIPCION',
				align:		'right',
				sortable:	true,
				resizable:	false
				
			},{
				width:		250,
				header:		'Cantidad',
				dataIndex:	'TOTAL',
				align:		'left',
				sortable:	true,
				resizable:	true
			}
		],
		bbar: {
			items: ['->','-', {
				xtype: 		'button',
				text: 		'Imprimir PDF',
				id: 			'btnImprimirAcuse',
				iconCls: 	'icoPdf',
				hidden:		true,
				handler: function(boton, evento) {
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '33reembolsoPorFiniquito01ext.data.jsp',
						params: Ext.apply({  
							informacion: 		'Imprimir',
							tipo_archivo:		'ACUSE',
							programa: 			Ext.getCmp('programa_id').getValue(),
							proceso_carga: 	Ext.getCmp('proceso_carga_id').getValue(),
							cantidad_total: 	Ext.getCmp('total_amortizaciones_id').getValue(),
							desembolsos_tot: 	Ext.getCmp('monto_total_amortizaciones_id').getValue(),
							fecha_carga:		Ext.getCmp('fecha_carga_id').getValue(),
							hora_carga:			Ext.getCmp('hora_carga_id').getValue(),
							folio_acuse:		Ext.getCmp('folio_acuse_id').getValue(),
							folio_carga:		Ext.getCmp('numero_acuse_id').getValue(),
							nombre_usuario:	Ext.getCmp('usuario_id').getValue()
						}),
						callback: descargaArchivoPdf
					});
				}
			},'-', {
				xtype: 		'button',
				text: 		'Generar Archivo',
				id: 			'btnGeneraArchivoAcuse',
				iconCls: 	'icoXls',
				hidden:		true,
				handler: function(boton, evento) {
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '33reembolsoPorFiniquito01ext.data.jsp',
						params: Ext.apply({  
							informacion: 		'Genera_Archivo',
							tipo_archivo:		'ACUSE',
							programa: 			Ext.getCmp('programa_id').getValue(),
							proceso_carga: 	Ext.getCmp('proceso_carga_id').getValue(),
							cantidad_total: 	Ext.getCmp('total_amortizaciones_id').getValue(),
							desembolsos_tot: 	Ext.getCmp('monto_total_amortizaciones_id').getValue(),
							fecha_carga:		Ext.getCmp('fecha_carga_id').getValue(),
							hora_carga:			Ext.getCmp('hora_carga_id').getValue(),
							folio_acuse:		Ext.getCmp('folio_acuse_id').getValue(),
							folio_carga:		Ext.getCmp('numero_acuse_id').getValue(),
							nombre_usuario:	Ext.getCmp('usuario_id').getValue()
						}),
						callback: descargaArchivoPdf
					});
				}
			},'-', {
				xtype: 		'button',
				text: 		'Salir',
				id: 			'btnSalirAcuse',
				iconCls: 	'icoCancelar',
				hidden:		true,
				handler: function(boton, evento) {
					Ext.getCmp('formaCargaMasiva').getForm().reset();
					Ext.getCmp('formaResultados').getForm().reset();
					Ext.getCmp('formaTerminarCaptura').getForm().reset();
					Ext.getCmp('formaAcuse').getForm().reset();
					Ext.getCmp('formaCargaMasiva').show();
					Ext.getCmp('formaResultados').hide();
					Ext.getCmp('formaTerminarCaptura').hide();
					Ext.getCmp('formaAcuse').hide();
					Ext.getCmp('gridCreditosPagados').hide();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridTotal').hide();					
					inicializar();
				}
			}]
		}
	});
	
	/********** Se crea el store del Grid Cr�ditos Pagados **********/
	var storeCreditosPagados = new Ext.data.ArrayStore({
		fields: [
			{ name: 'NUMERO'			},
			{ name: 'NOMBRE'			},
			{ name: 'DESCRIPCION'	},
			{ name: 'TIPO_CAMPO'		},
			{ name: 'LONGITUD'		},
			{ name: 'OBSERVACIONES'	}
		]
	});

	/********** Se crea el grid Cr�ditos Pagados **********/
	var gridCreditosPagados = new Ext.grid.EditorGridPanel({
		width:				700,
		id:					'gridCreditosPagados',
		style:				'margin:0 auto;',
		align:				'center',
		title:				'LAYOUT DE CR�DITOS PAGADOS, NO PAGADOS Y RECUPERADOS EN D�AS ANTERIORES <br> (TBL: com_cred_pend_reembolso_fide)',
		frame:				false,
		border:				true,
		hidden:				true,
		autoHeight:			true,
		closable:			true,
		store:				storeCreditosPagados,
		columns:				[
			{
				width:		50,
				header:		'No.',
				dataIndex:	'NUMERO',
				align:		'center',
				sortable:	false,
				resizable:	false
			},{
				width:		150,
				header:		'Nombre del Campo',
				dataIndex:	'NOMBRE',
				align:		'center',
				sortable:	false,
				resizable:	false
			},{
				width:		110,
				header:		'Descripci�n',
				dataIndex:	'DESCRIPCION',
				align:		'left',
				sortable:	false,
				resizable:	false
			},{
				width:		100,
				header:		'Tipo de Campo',
				dataIndex:	'TIPO_CAMPO',
				align:		'center',
				sortable:	false,
				resizable:	false
			},{
				width:		90,
				header:		'Longitud',
				dataIndex:	'LONGITUD',
				align:		'center',
				sortable:	false,
				resizable:	false
			},{
				width:		200,
				header:		'Observaciones',
				dataIndex:	'OBSERVACIONES',
				align:		'left',
				sortable:	false,
				resizable:	false
			}
		],
		bbar: {
			items: ['->','-',{
				xtype: 		'button',
				text: 		'Cerrar',
				id: 			'btnCerrarLayout',
				iconCls: 	'icoRegresar',
				handler: function(boton, evento) {
					Ext.getCmp('gridCreditosPagados').hide();
				}
			}]
		}
	});

	/**********		Form Panel Acuse		**********/
	var formaAcuse = new Ext.form.FormPanel({
		width: 			600,
		labelWidth:		275,
		id:				'formaAcuse',
		title:			'Acuse Desembolsos',			
		layout:			'form',
		style: 			'margin:0 auto;',
		frame: 			true,	
		border: 			true,
		hidden: 			true,			
		items: [{	
			width:		580,	
			id:			'display1_id',
			frame:		false,	
			border:		false,	
			hidden:		false,
			html:			'<div align="center"><br><b> La autentificaci�n se llev� a cabo con �xito. </b><br>&nbsp;</div>'
		},{		
			width:		280,	
			xtype:		'textfield', 
			id:			'folio_acuse_id',	
			name:			'folio_acuse',
			fieldLabel:	'&nbsp;&nbsp; Recibo'
		},{		
			width:		280,	
			xtype:		'textfield', 
			id:			'total_amortizaciones_id',	
			name:			'total_amortizaciones',
			labelAlign: 'right',
			fieldLabel:	'&nbsp;&nbsp; No. Total de Amortizaciones Desembolsadas'
		},{				
			width:		280,	
			xtype:		'textfield', 
			id:			'monto_total_amortizaciones_id',	
			name:			'monto_total_amortizaciones',
			labelAlign: 'right',
			fieldLabel:	'&nbsp;&nbsp; Monto Total de Amortizaciones Desembolsadas'
		},{				
			width:		280,	
			xtype:		'textfield', 
			id:			'numero_acuse_id',	
			name:			'numero_acuse',
			labelAlign: 'right',
			fieldLabel:	'&nbsp;&nbsp; N�mero de Acuse'
		},{				
			width:		280,	
			xtype:		'textfield', 
			id:			'fecha_carga_id',	
			name:			'fecha_carga',
			labelAlign: 'right',
			fieldLabel:	'&nbsp;&nbsp; Fecha de Carga'
		},{				
			width:		280,	
			xtype:		'textfield', 
			id:			'hora_carga_id',	
			name:			'hora_carga',
			labelAlign: 'right',
			fieldLabel:	'&nbsp;&nbsp; Hora de carga'
		},{				
			width:		280,	
			xtype:		'textfield', 
			id:			'usuario_id',	
			name:			'usuario',
			labelAlign: 'right',
			fieldLabel:	'&nbsp;&nbsp; Usuario'
		},{	
			width:		580,	
			id:			'display2_id',
			frame:		false,	
			border:		false,	
			html:			'<div align="left"><br><b> Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisi�n tendr� validez para todos los efectos legales. </b><br>&nbsp;</div>'
		},{	
			width:		580,	
			id:			'display3_id',
			frame:		false,	
			border:		false,	
			hidden:		false,
			html:			'<div align="center"><br><b> La autentificaci�n no se llev� a cabo. <br> PROCESO CANCELADO </b><br>&nbsp;</div>'
		}],				
		buttons: [{		
			id:			'btnRegresar',
			text:			'Regresar',
			iconCls:		'icoCancelar',
			hidden:		false,
			handler:		function(boton, evento){
				Ext.getCmp('formaCargaMasiva').getForm().reset();
				Ext.getCmp('formaResultados').getForm().reset();
				Ext.getCmp('formaTerminarCaptura').getForm().reset();
				Ext.getCmp('formaAcuse').getForm().reset();
				Ext.getCmp('formaCargaMasiva').show();
				Ext.getCmp('formaResultados').hide();
				Ext.getCmp('formaTerminarCaptura').hide();
				Ext.getCmp('formaAcuse').hide();
				Ext.getCmp('gridCreditosPagados').hide();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridTotal').hide();					
				inicializar();
			}
		}]
	});
	
	/********** Form Panel Terminar Captura **********/
	var formaTerminarCaptura = new Ext.form.FormPanel({
		width: 				600,
		id:					'formaTerminarCaptura',
		title:				'Preacuse Desembolsos',
		layout:				'form',
		style: 				'margin:0 auto;',
		frame: 				false,
		border: 				true,
		hidden: 				true,
		items: [{
			width: 			600,
			xtype: 			'panel',
			id: 				'seccion_1',
			layout: 			'table', 
			border:			true,
			layoutConfig: 	{ columns:2 },
			defaults: 		{align:'center', bodyStyle:'padding:2px,'},
			items: [
				{
					width:	500,
					frame:	false,
					border:	false,
					html:		'<div align="left"><br> No. Total de Amortizaciones a Desembolsar: <br></div>'
				},{
					width:	100, 
					xtype:	'displayfield', 
					id:		'total_desembolsos_id',
					name:		'total_desembolsos'
				}
			]
		},{
			width:			600,
			xtype:			'panel',
			id:				'seccion_2',
			layout:			'table',
			border:			true,
			layoutConfig: 	{ columns:2 },
			defaults: 		{align:'center', bodyStyle:'padding:2px,'},
			items: [
				{
					width:	500,
					frame:	false,
					border:	false,
					html:		'<div align="left"><br> Monto Total de Amortizaciones a Desembolsar: <br></div>'
				},{
					width:	100,
					xtype:	'displayfield',
					id:		'cantidad_id',
					name:		'cantidad'
				}
			]
		},{
			width:			600,
			xtype:			'panel',
			id:				'seccion_3',
			layout:			'table',
			border:			true,
			layoutConfig: 	{ columns: 1 },
			defaults: 		{align:'center', bodyStyle:'padding:2px,'},
			items: [
				{
					width:	580,
					frame:	false,
					border:	false,
					html:		'<div align="left"><br> Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad de los movimientos registrados del mismo, dicha transmisi�n tendr� validez para todos los efectos legales. <br>&nbsp;</div>'
				}
			]
		}],
		buttons: [{
			id:				'btnImprimir',
			text:				'Generar Archivo',
			iconCls: 		'icoPdf',
			handler:			function(boton, evento){
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '33reembolsoPorFiniquito01ext.data.jsp',
					params: Ext.apply({  
						informacion:		'Imprimir',
						tipo_archivo:		'PREACUSE',
						proceso_carga: 	Ext.getCmp('proceso_carga_id').getValue(),
						cantidad_total: 	Ext.getCmp('total_desembolsos_id').getValue(),
						desembolsos_tot: 	Ext.getCmp('cantidad_id').getValue(),
						programa: 			Ext.getCmp('programa_id').getValue()
					}),
					callback: descargaArchivoPdf
				});
			}
		},{
			id:				'btnTransDesembolsos',
			text:				'Transmitir Desembolsos',
			iconCls: 		'modificar',
			handler:			function(boton, evento){
				
				NE.util.obtenerPKCS7(transmitirDesembolsos, texto_plano);
			}
		},{
			id:				'btnCancelarPreacuse',
			text:				'Cancelar',
			iconCls: 		'icoCancelar',
			handler:			function(boton, evento){
				Ext.getCmp('formaCargaMasiva').getForm().reset();
				Ext.getCmp('formaResultados').getForm().reset();
				Ext.getCmp('formaTerminarCaptura').getForm().reset();
				Ext.getCmp('formaAcuse').getForm().reset();
				Ext.getCmp('formaCargaMasiva').show();
				Ext.getCmp('formaResultados').hide();
				Ext.getCmp('formaTerminarCaptura').hide();
				Ext.getCmp('formaAcuse').hide();
				Ext.getCmp('gridCreditosPagados').hide();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridTotal').hide();		
				inicializar();
			}
		}]
	});
	
	/********** Form Panel Mostrar resultados **********/
	var formaResultados = new Ext.FormPanel({
		width:				600,
		id:					'formaResultados',
		layout:				'form',
		bodyPadding:		'20 20 20 20',
		style:				'margin: 0px auto 0px auto;',
		frame:				true,
		autoHeight:			true,
		hidden:				true,
		items: [{
			width:			600,
			xtype:			'panel',
			id:				'carga_correcta_id',
			layout:			'table',
			layoutConfig:	{columns: 1},
			defaults:		{align: 'center', bodyStyle: 'padding:2px,'},
			hidden:			true,
			border:			true,
			items:[{
				width:		585,
				xtype:		'displayfield',
				id:			'folio_batch_txt_id',
				name:			'folio_batch_txt',
				hideLabel: 	true
			},{
				width:		585,
				xtype:		'displayfield',
				id:			'folio_batch_txt1_id',
				name:			'folio_batch_txt1',
				value:		'Usted podr� consultar el resultado del proceso Batch el siguiente ' +
								'd�a h�bil a partir de la fecha de carga en la pantalla Consulta Batch.',
				hideLabel: 	true
			}]
		},{
			width:			600,
			xtype:			'panel',
			id:				'titulos_id',
			layout:			'table',
			layoutConfig:	{columns: 2},
			defaults:		{align: 'center', bodyStyle: 'padding:2px,'},
			border:			true,
			hidden:			true,
			items:[{
				width:		293,
				html:			'<div align="center"><b>Registros sin Errores</b><br>&nbsp;</div>',	
				frame:		true,
				border:		false
			},{
				width:		293,
				html:			'<div align="center"><b>Registros con Errores</b><br>&nbsp;</div>',
				border:		false,
				frame:		true
			}]
		},{
			width:			600,
			xtype:			'panel',
			id:				'listas_id',
			layout:			'table',
			layoutConfig:	{columns: 2},
			defaults:		{align: 'center', bodyStyle: 'padding:2px,'},
			border:			true,
			hidden:			true,
			items:[{
				width: 		293,
				height: 		200,
				xtype: 		'textarea',
				id:			'correctos_id',
				name: 		'correctos',
				readOnly:	true
			},{
				width:		293,
				height:		200,
				xtype:		'textarea',
				id:			'errores_id',
				name: 		'errores',
				readOnly:	true
			}]
		},{
			width:			600,
			xtype:			'panel',
			id:				'totales_id',
			layout:			'table',
			layoutConfig:	{columns: 3},
			defaults:		{align: 'center', bodyStyle: 'padding:2px,'},
			border:			false,
			hidden:			true,
			items:[{	
				width:		400,
				html:			'<div align="right">Total Registros sin Errores:&nbsp<br></div>',	
				frame:		false,	
				border:		false
			},{		
				width:		180,	
				xtype:		'textfield', 
				id:			'total_sin_errores_id',	
				name:			'total_sin_errores',
				readOnly:	true	
			},{	
				width:		10,
				html:			'<div align="right">&nbsp<br>&nbsp</div>',	
				frame:		false,	
				border:		false
			}]
		}],
		buttons: [{
			id:				'btnDescargaRegError',
			text:				'Descargar Registros con Error',
			iconCls: 		'icoTxt',
			hidden:			true,
			handler:			function(boton, evento){
				boton.setIconClass('loading-indicator');
				descargaArchivo();
			}
		},{
			id:				'btnCancelarCarga',
			text:				'Cancelar',
			iconCls: 		'icoCancelar',
			hidden:			false,
			handler:			function(boton, evento){
				Ext.getCmp('formaCargaMasiva').getForm().reset();
				Ext.getCmp('formaResultados').getForm().reset();
				Ext.getCmp('formaTerminarCaptura').getForm().reset();
				Ext.getCmp('formaAcuse').getForm().reset();
				Ext.getCmp('formaCargaMasiva').show();
				Ext.getCmp('formaResultados').hide();
				Ext.getCmp('formaTerminarCaptura').hide();
				Ext.getCmp('formaAcuse').hide();
				Ext.getCmp('gridCreditosPagados').hide();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridTotal').hide();
				inicializar();
			}
		},{
			id:				'btnContinuarCarga',
			text:				'Continuar Carga',
			iconCls: 		'icoAceptar',
			hidden:			true,
			handler:			function(boton, evento){
				continuarCarga();
			}
		},{
			id:				'btnAceptarCarga',
			text:				'Aceptar',
			iconCls: 		'icoAceptar',
			hidden:			true,
			handler:			function(boton, evento){
				Ext.getCmp('formaCargaMasiva').getForm().reset();
				Ext.getCmp('formaResultados').getForm().reset();
				Ext.getCmp('formaTerminarCaptura').getForm().reset();
				Ext.getCmp('formaAcuse').getForm().reset();
				Ext.getCmp('formaCargaMasiva').show();
				Ext.getCmp('formaResultados').hide();
				Ext.getCmp('formaTerminarCaptura').hide();
				Ext.getCmp('formaAcuse').hide();
				Ext.getCmp('gridCreditosPagados').hide();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridTotal').hide();
				inicializar();
			}
		}]
	});
	
	/********** Form Panel Carga Masiva **********/
	var formaCargaMasiva = new Ext.form.FormPanel({
		width:				600,
		labelWidth:			170,
		id:					'formaCargaMasiva',
		title:				'Carga Masiva',
		layout:				'form',
		bodyPadding:		'20 20 20 20',
		style:				'margin: 0px auto 0px auto;',
		frame:				true,
		autoHeight:			true,
		hidden:				false,
		fileUpload: 		true,
		items: [{
			width:			350,
			xtype:			'displayfield', 
			id:				'programa_id',
			name:				'programa',
			fieldLabel:		'&nbsp;&nbsp; Programa'
		},{
			width:			280,
			xtype: 			'fileuploadfield',
			id: 				'archivo',
			name: 			'archivo',
			emptyText: 		'Seleccione...',
			fieldLabel: 	'&nbsp;&nbsp; Ruta del Archivo de Origen', 
			anchor: 			'90%',
			msgTarget:		'side',
			buttonCfg: { iconCls: 'upload-icon' },
			buttonText: null
		},{
			width:			590,
			xtype:			'displayfield',
			id:				'display1_id',
			name:				'display1',
			value:			'&nbsp;&nbsp; Se acepta un m�ximo de 1,000 registros para cargas en l�nea y m�ximo 20,000 registros para cargas <br> &nbsp;&nbsp; mediante proceso batch. ',
			hideLabel: 		true
		},{
			width:			280,
			xtype:			'textfield', 
			id:				'nombre_archivo_id',
			name:				'nombre_archivo',
			fieldLabel:		'&nbsp;&nbsp; Nombre archivo',
			hidden:			true
		},{
			width:			280,
			xtype:			'textfield', 
			id:				'total_reembolsos_id',
			name:				'total_reembolsos',
			fieldLabel:		'&nbsp;&nbsp; Total reembolsos',
			hidden:			true
		},{
			width:			280,
			xtype:			'textfield', 
			id:				'proceso_carga_id',
			name:				'proceso_carga',
			fieldLabel:		'&nbsp;&nbsp; Proceso de carga',
			hidden:			true
		},{
			width:			280,
			xtype:			'textfield', 
			id:				'disposicion_id',
			name:				'disposicion',
			fieldLabel:		'&nbsp;&nbsp; Disposicion',
			hidden:			true
		}],
		buttons: [{
			id:				'btnConsultaLayout',
			text:				'Consulta Layout',
			iconCls: 		'icoBuscar',
			handler:			function(boton, evento){
				Ext.getCmp('gridCreditosPagados').show();
			}
		},{
			id:				'btnContinuar',
			text:				'Continuar',
			iconCls: 		'icoAceptar',
			handler:			function(boton, evento){
				// Validar que se haya especificado un archivo
				var archivo = Ext.getCmp("archivo");
				if( Ext.isEmpty( archivo.getValue() ) ){
					archivo.markInvalid("Debe especificar un archivo");
					return;
				}
				// Revisar el tipo de extension del archivo
				var myRegexTXT = /^.+\.([tT][xX][tT])$/;
				
				var nombreArchivo = archivo.getValue();
				if( !myRegexTXT.test(nombreArchivo) ) {
					archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n txt.");
					return;
				}
				// Cargar archivo
				subirArchivo();
			}
		}]
	});

	/********** Botones de menu. Indican si la carga es masiva, individual o si es consultal **********/
	var menuToolbar = new Ext.Toolbar({
		width: 			600,
		id:	 			'menuToolbar',
		style: 			'margin:0 auto;',
		items: [
			{
				width: 	192,
				xtype:	'tbbutton',
				id:		'btnCargaIndividual',
				text:		'Carga Individual',
				handler: function(){
					window.location.href='33reembolsoPorFiniquito01ext.jsp';
				}	
			},'-',{
				width:	192,
				xtype:	'tbbutton',
				id:		'btnCargaMasiva',
				text:		'Carga Masiva',
				handler: function(){
					Ext.getCmp('btnCargaMasiva').setText('<i><B>Carga Masiva</i></B>');
				}
			},'-',{
				width:	192,
				xtype:	'tbbutton',
				id:		'btnConsultaBatch',
				text:		'Consulta Batch',
				handler: function(){
					window.location.href='/nafin/33fondojr/33nafin/33consultaDesembolsos01ext.jsp';
				}
			}
		]
	});
	
	/********** Contenedor Principal **********/
	var pnl = new Ext.Container({
		width:	949,
		id:		'contenedorPrincipal',
		applyTo:	'areaContenido',
		items: [
			NE.util.getEspaciador(10),
			menuToolbar,
			formaCargaMasiva,
			formaResultados,
			formaTerminarCaptura,
			formaAcuse,
			NE.util.getEspaciador(10),
			gridCreditosPagados,
			gridConsulta,
			gridTotal
		]
	});

	/**********		Inicializaci�n		**********/
	Ext.getCmp('btnCargaMasiva').setText('<i><B>Carga Masiva</i></B>');
	storeCreditosPagados.loadData(layoutCreditos);
	inicializar();
	
});	