<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalgoAfianzadora,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fondojr.*,
		com.netro.fondosjr.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String tipoMonitor  = request.getParameter("tipoMonitor")==null?"":request.getParameter("tipoMonitor");
System.out.println("tipoMonitortipoMonitor==="+tipoMonitor);
/*	SE CREA NUEVO OBJETO PARA ACCESAR EL EJB */
FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

if(informacion.equals("catalogoIF")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setTabla("comcat_if");
	cat.setOrden("ic_if");
	cat.setValoresCondicionIn("103", Integer.class);
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();	

}else if ((informacion.equals("consultaMonitor") || (informacion.equals("generarArchivoMonitor") && !"".equals(tipoMonitor) ))) {
	JSONObject jsonObj = new JSONObject();
	int numReemb = 0;
	
	//String cveProgramaFondoJR  = request.getParameter("cveProgramaFondoJR")==null?"":request.getParameter("cveProgramaFondoJR");
	//String cvePrograma  = request.getParameter("cvePrograma")==null?"":request.getParameter("cvePrograma");
	//String tipoMonitor  = request.getParameter("tipoMonitor")==null?"":request.getParameter("tipoMonitor");
	String fec_venc_ini  = request.getParameter("fec_venc_ini")==null?"":request.getParameter("fec_venc_ini");
	String fec_venc_fin  = request.getParameter("fec_venc_fin")==null?"":request.getParameter("fec_venc_fin");
	String estatusCarga  = request.getParameter("estatusCarga")==null?"":request.getParameter("estatusCarga");
	String chkDesembolso  = request.getParameter("chkDesembolso")==null?"":request.getParameter("chkDesembolso");
	
	if("NPR".equals(tipoMonitor)){
		//Se crea la instacia del Paginador
		ConsultaMonitorNPR paginador = new ConsultaMonitorNPR();
		paginador.setCvePrograma(cveProgramaFondoJR);
		paginador.setDescPrograma(descProgramaFondoJR);
		paginador.setFec_venc_ini(fec_venc_ini);
		paginador.setFec_venc_fin(fec_venc_fin);
		paginador.setChkDesembolso(chkDesembolso);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if (informacion.equals("consultaMonitor")) {	//Datos para la Consulta con Paginacion
			int start = 0;
			int limit = 0;
			//String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request);
				}
				infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		
		} else if (informacion.equals("generarArchivoMonitor") ) {	
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
			infoRegresar = jsonObj.toString();		
		
		}
		
	}else	if("R".equals(tipoMonitor)){
		//Se crea la instacia del Paginador
		ConsultaMonitorReembFJR paginador = new ConsultaMonitorReembFJR();
		paginador.setCvePrograma(cveProgramaFondoJR);
		paginador.setDescPrograma(descProgramaFondoJR);
		paginador.setFec_venc_ini(fec_venc_ini);
		paginador.setFec_venc_fin(fec_venc_fin);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if (informacion.equals("consultaMonitor")) {	//Datos para la Consulta con Paginacion
			int start = 0;
			int limit = 0;
			//String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request);
				}
				infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		
		} else if (informacion.equals("generarArchivoMonitor") ) {	
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
			infoRegresar = jsonObj.toString();		
		
		}
	}else{
	
		if (informacion.equals("consultaMonitor")) {	//Datos para la Consulta con Paginacion
			numReemb = fondoJunior.numReembTotales(cveProgramaFondoJR,"","");
			String masdequinientos = "";
					
			if(numReemb<=500){
				masdequinientos= "F";
			}else{
				masdequinientos= "V";
			}
			
			//Se crea la instacia del Paginador
			ConsultaMonitorFondoJR paginador = new ConsultaMonitorFondoJR();
			paginador.setCvePrograma(cveProgramaFondoJR);
			paginador.setDescPrograma(descProgramaFondoJR);
			paginador.setFec_venc_ini(fec_venc_ini);
			paginador.setFec_venc_fin(fec_venc_fin);
			paginador.setEstatusCarga(estatusCarga);
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
			
			//if (informacion.equals("consultaMonitor")) {	//Datos para la Consulta con Paginacion
				int start = 0;
				int limit = 0;
				//String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request);
				}
				jsonObj = JSONObject.fromObject(queryHelper.getJSONPageResultSet(request,start,limit));
				jsonObj.put("MASDEQUINIENTOS", masdequinientos);
				infoRegresar = jsonObj.toString();
		
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		}
	}

	
} else if(informacion.equals("generarArchivoMonitor")){
	
	String fec_venc_ini  = request.getParameter("fec_venc_ini")==null?"":request.getParameter("fec_venc_ini");
	String fec_venc_fin  = request.getParameter("fec_venc_fin")==null?"":request.getParameter("fec_venc_fin");
	String estatusCarga  = request.getParameter("estatusCarga")==null?"":request.getParameter("estatusCarga");
	String chkDesembolso = request.getParameter("chkDesembolso")==null?"":request.getParameter("chkDesembolso");
	String tipoArchivo = request.getParameter("tipoArchivo")==null?"":request.getParameter("tipoArchivo");
	
	String nombreArchivo = fondoJunior.generaCSVMonitor( fec_venc_ini, fec_venc_fin, estatusCarga, cveProgramaFondoJR, descProgramaFondoJR, 
											   chkDesembolso, tipoArchivo, strDirectorioTemp, strDirectorioPublicacion);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("consultaDetalleMonitor")){
	JSONArray jsObjArray = new JSONArray();
	List lstConsDetMonitor = new ArrayList();
	String estatus  = request.getParameter("estatus")==null?"":request.getParameter("estatus");
	String fecVenc  = request.getParameter("fecVenc")==null?"":request.getParameter("fecVenc");
	int numRegistros = 0;
	try{
		numRegistros = fondoJunior.numRegDetMonitorJS(estatus ,fecVenc, cveProgramaFondoJR);
		lstConsDetMonitor = fondoJunior.consDetMonitorJS(estatus ,fecVenc, cveProgramaFondoJR, numRegistros);
		
	}catch(Throwable t){
		throw t;
	}
	
	jsObjArray = JSONArray.fromObject(lstConsDetMonitor);
	infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"numRegistros\": \"" + numRegistros + "\", \"registros\": " + jsObjArray.toString()+"})";

} else if(informacion.equals("validaMonitor")){
	JSONArray jsObjArray = new JSONArray();
	JSONObject jsonObj = new JSONObject();
	List lstResumenMonitor = new ArrayList();
	List lstRegResumenMonitor = new ArrayList();
	String sumaTotalVista		= "0", montoTotalVista		= "0.00";
	String sumaTotalPagado		= "0", montoTotalPagado		= "0.00";
	String sumaTotalNoPagado	= "0", montoTotalNoPagado	= "0.00";
	String sumaTotalNoPagadoR	= "0", montoTotalNoPagadoR	= "0.00";
	String sumaTotalTmp 			= "0", montoTotalTmp			= "0.00";
	String sumaTotalReemb		= "0", montoTotalReemb		= "0.00";
	String sumaTotalRec			= "0", montoTotalRec			= "0.00";
	String sumaTotalNoEncon		= "0", montoTotalNoEncon	= "0.00";
	String sumaTotalNoEnv		= "0", montoTotalNoEnv		= "0.00";
	String sumaTotalError		= "0", montoTotalError		= "0.00";
	String sumaTotalPrepago		= "0", montoTotalPrepago	= "0.00";
	String estatus  = request.getParameter("estatus")==null?"":request.getParameter("estatus");
	String fecVenc  = request.getParameter("fec_venc")==null?"":request.getParameter("fec_venc");
	String tipoValida = "";
	String masdequinRT = "V";
	String masdequinR = "V";
	String masdequinRNE = "V";
	String masdequinRCE = "V";
	int numReemb = 0;
	try{
		
			if((fecVenc).equals("RC") || (fecVenc).equals("T")){
				tipoValida = "B";
				lstResumenMonitor = fondoJunior.validaMonitorRCyT(fecVenc, cveProgramaFondoJR);
			}else if("R".equals(estatus)){				
				tipoValida = "C";				
				lstResumenMonitor = fondoJunior.validaMonitorR(fecVenc, cveProgramaFondoJR);
				
				numReemb = fondoJunior.numReembTotales(cveProgramaFondoJR,"RembolsosPrepagosTotales","");
				if(numReemb<=500){
					masdequinRT = "F";
				}
				
				numReemb = fondoJunior.numReembTotales(cveProgramaFondoJR,"Reembolsos","");
				if(numReemb<=500){
					masdequinR = "F";
				}
				
				numReemb = fondoJunior.numReembTotales(cveProgramaFondoJR,"NoEncontrados","");
				if(numReemb<=500){
					masdequinRNE = "F";
				}
				
				numReemb = fondoJunior.numReembTotales(cveProgramaFondoJR,"ConError","");
				if(numReemb<=500){
					masdequinRCE = "F";
				}
			
				
			}else{
				tipoValida = "A";
				lstResumenMonitor = fondoJunior.validaMonitorPyNP(fecVenc, cveProgramaFondoJR);
			}
			
			jsonObj.put("masdequinRT", masdequinRT);
			jsonObj.put("masdequinRNE", masdequinRNE);
			jsonObj.put("masdequinRCE", masdequinRCE);
			jsonObj.put("tipoValida", tipoValida);
			
			for(int x=0; x<lstResumenMonitor.size(); x++){
				lstRegResumenMonitor = (List)lstResumenMonitor.get(x);
				if(lstRegResumenMonitor.get(2)!=null)
					System.out.println("(String)lstRegResumenMonitor.get(2)>>>>"+(String)lstRegResumenMonitor.get(2));
					if(((String)lstRegResumenMonitor.get(2)).equals("vista")){
						sumaTotalVista = (String)lstRegResumenMonitor.get(0);
						montoTotalVista = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalVista", sumaTotalVista);
						jsonObj.put("montoTotalVista", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}else if(((String)lstRegResumenMonitor.get(2)).equals("pagados")){
						sumaTotalPagado = (String)lstRegResumenMonitor.get(0);
						montoTotalPagado = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalPagado", sumaTotalPagado);
						jsonObj.put("montoTotalPagado", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}else if(((String)lstRegResumenMonitor.get(2)).equals("no_pagados")){
						sumaTotalNoPagado = (String)lstRegResumenMonitor.get(0);
						montoTotalNoPagado = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalNoPagado", sumaTotalNoPagado);
						jsonObj.put("montoTotalNoPagado", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}else if(((String)lstRegResumenMonitor.get(2)).equals("tmp")){
						sumaTotalTmp = (String)lstRegResumenMonitor.get(0);
						montoTotalTmp = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalTmp", sumaTotalTmp);
						jsonObj.put("montoTotalTmp", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}else if(((String)lstRegResumenMonitor.get(2)).equals("reemb")){
						sumaTotalReemb = (String)lstRegResumenMonitor.get(0);
						montoTotalReemb = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalReemb", sumaTotalReemb);
						jsonObj.put("montoTotalReemb", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}else if(((String)lstRegResumenMonitor.get(2)).equals("error")){
						sumaTotalError = (String)lstRegResumenMonitor.get(0);
						montoTotalError = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalError", sumaTotalError);
						jsonObj.put("montoTotalError", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}else if(((String)lstRegResumenMonitor.get(2)).equals("recuperacion")){
						sumaTotalRec = (String)lstRegResumenMonitor.get(0);
						montoTotalRec = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalRec", sumaTotalRec);
						jsonObj.put("montoTotalRec", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}else if(((String)lstRegResumenMonitor.get(2)).equals("no_encontrados")){
						sumaTotalNoEncon = (String)lstRegResumenMonitor.get(0);
						montoTotalNoEncon = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalNoEncon", sumaTotalNoEncon);
						jsonObj.put("montoTotalNoEncon", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}else if(((String)lstRegResumenMonitor.get(2)).equals("no_enviados")){
						sumaTotalNoEnv = (String)lstRegResumenMonitor.get(0);
						montoTotalNoEnv = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalNoEnv", sumaTotalNoEnv);
						jsonObj.put("montoTotalNoEnv", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}else if(((String)lstRegResumenMonitor.get(2)).equals("prepagos")){
						sumaTotalPrepago = (String)lstRegResumenMonitor.get(0);
						montoTotalPrepago = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalPrepago", sumaTotalPrepago);
						jsonObj.put("montoTotalPrepago", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}else if(((String)lstRegResumenMonitor.get(2)).equals("no_pagados_rech")){//F027-2011 FVR
						sumaTotalNoPagadoR = (String)lstRegResumenMonitor.get(0);
						montoTotalNoPagadoR = Comunes.formatoDecimal(lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0",2);
						jsonObj.put("sumaTotalNoPagadoR", sumaTotalNoPagadoR);
						jsonObj.put("montoTotalNoPagadoR", lstRegResumenMonitor.get(1)!=null?(String)lstRegResumenMonitor.get(1):"0");
					}
			}
		
	}catch(Throwable t){
		throw t;
	}
	
	jsonObj.put("success", new Boolean(true));
	//jsonObj.put("registros", jsObjArray.toString());
	//jsObjArray = JSONArray.fromObject(lstResumenMonitor);
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("eliminaRegMonitor")){
	JSONObject jsonObj = new JSONObject();
	String tipoValida  = request.getParameter("tipoValida")==null?"":request.getParameter("tipoValida");
	String estatus  = request.getParameter("estatus")==null?"":request.getParameter("estatus");
	String fecVenc  = request.getParameter("fec_venc")==null?"":request.getParameter("fec_venc");
	String folio  = request.getParameter("folio")==null?"":request.getParameter("folio");
	
	FondoJrMonitorBean fondoJrMonitorBean = new FondoJrMonitorBean();
	
	if("A".equals(tipoValida) || "B".equals(tipoValida) || "C".equals(tipoValida)){
		fondoJrMonitorBean.setEstatus_cred(estatus);
		fondoJrMonitorBean.setFec_venc(fecVenc);
		fondoJrMonitorBean.setUsuario(strNombreUsuario);
	}
	
	fondoJunior.eliminaRegMonitor(fondoJrMonitorBean,true, cveProgramaFondoJR);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("validaMonitorAcuse")){
	JSONObject jsonObj = new JSONObject();
	String tipoValida  = request.getParameter("tipoValida")==null?"":request.getParameter("tipoValida");
	String estatus  = request.getParameter("estatus")==null?"":request.getParameter("estatus");
	String fecVenc  = request.getParameter("fec_venc")==null?"":request.getParameter("fec_venc");
	String textoFirmado = request.getParameter("TextoFirmado")==null?"":request.getParameter("TextoFirmado");
	String pkcs7 = request.getParameter("Pkcs7")==null?"":request.getParameter("Pkcs7");
	String serial  = "";
	List lstInfo = new ArrayList();
	List lstCabecera = new ArrayList();
	String mostrarError = "";
	
	FondoJrMonitorBean fondoJrMonitorBean = new FondoJrMonitorBean();
	
	if("A".equals(tipoValida) || "B".equals(tipoValida) || "C".equals(tipoValida)){
		fondoJrMonitorBean.setEstatus_cred(estatus);
		fondoJrMonitorBean.setFec_venc(fecVenc);
		fondoJrMonitorBean.setUsuario(strNombreUsuario);
		fondoJrMonitorBean.setTextoFirmado(textoFirmado);
		fondoJrMonitorBean.setPkcs7(pkcs7);
		fondoJrMonitorBean.setSerial(_serial);
	}
	
	List lstAcuseGral = fondoJunior.autorizaValidacionMonitor(fondoJrMonitorBean, true, cveProgramaFondoJR, "JS");
	if(lstAcuseGral!=null && lstAcuseGral.size()==1){
		
		mostrarError = (String)lstAcuseGral.get(0);
	
	}else if(lstAcuseGral!=null && lstAcuseGral.size()==2){
		lstCabecera = (List)lstAcuseGral.get(0);
		lstInfo = (List)lstAcuseGral.get(1);
		
		HashMap hm = (HashMap)lstCabecera.get(0);
		jsonObj.put("folio", (String)hm.get("FOLIO"));
		jsonObj.put("usuario", (String)hm.get("USUARIO"));
	}
	
	String fec_Actual = fHora.format(new java.util.Date());
	JSONArray jsObjArray = new JSONArray();
	jsObjArray = JSONArray.fromObject(lstInfo);
	
	jsonObj.put("fecActual", fec_Actual);
	jsonObj.put("mostrarError", mostrarError);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", jsObjArray.toString());
	
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("consultaDetValidaMonitor")){
	JSONArray jsObjArray = new JSONArray();
	List lstConsDetValidaMonitor = new ArrayList();
	String ver_enlace  = request.getParameter("ver_enlace")==null?"":request.getParameter("ver_enlace");
	String estatus  = request.getParameter("estatus")==null?"":request.getParameter("estatus");
	String estatus2  = request.getParameter("estatus2")==null?"":request.getParameter("estatus2");
	String proc  = request.getParameter("proc")==null?"":request.getParameter("proc");
	String fec_venc  = request.getParameter("fec_venc")==null?"":request.getParameter("fec_venc");
	int numRegistros = 0;

	try{
		numRegistros = fondoJunior.numRegDetalleDeCreditosJS(ver_enlace ,estatus, estatus2, proc, fec_venc, cveProgramaFondoJR);
		lstConsDetValidaMonitor = fondoJunior.ObtenerDetalleDeCreditosJS(ver_enlace ,estatus, estatus2, proc, fec_venc, cveProgramaFondoJR, numRegistros);
	}catch(Throwable t){
		throw t;
	}
	
	jsObjArray = JSONArray.fromObject(lstConsDetValidaMonitor);
	//infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";
	infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"numRegistros\": \"" + numRegistros + "\", \"registros\": " + jsObjArray.toString()+"})";

}else if (informacion.equals("generaXLSCreditos")) {
	JSONObject jsonObj = new JSONObject();
	String estatus  = request.getParameter("estatus")==null?"":request.getParameter("estatus");
	String fecVenc  = request.getParameter("fec_venc")==null?"":request.getParameter("fec_venc");
	ComunesXLS xls = null;
	CreaArchivo 	archivo			= new CreaArchivo();
	String	nombreArchivo	= archivo.nombreArchivo()+".xls";
	xls = new ComunesXLS(strDirectorioTemp+nombreArchivo);
		
	List lstErrorGral = fondoJunior.ObtenerDetalleDeCreditos("ConError", estatus, "","N",fecVenc,cveProgramaFondoJR);
		
	xls.setTabla(9);
	if(lstErrorGral!=null && lstErrorGral.size()>1){
		for(int x=0; x<lstErrorGral.size();x++){
			List lstErrorReg = (List)lstErrorGral.get(x);
			if(x==0){
				/*TITULO*/
				xls.setCelda("CREDITOS CON ERROR", "celda01", ComunesXLS.CENTER, 9);
				/*REGISTROS*/
				xls.setCelda("FECHA AUTORIZACION NAFIN", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("FECHA OPERACION", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("FECHA PAGO CLIENTE FIDE", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("PRESTAMO", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("NUM. CLIENTE SIRAC", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("NOMBRE CLIENTE", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("NUM. CUOTA A PAGAR", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("CAPITAL", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("ERROR", "celda01", ComunesXLS.CENTER, 1);						
			}else{						
				xls.setCelda(lstErrorReg.get(0)!=null?(String)lstErrorReg.get(0):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(1)!=null?(String)lstErrorReg.get(1):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(2)!=null?(String)lstErrorReg.get(2):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(3)!=null?(String)lstErrorReg.get(3):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(4)!=null?(String)lstErrorReg.get(4):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(5)!=null?(String)lstErrorReg.get(5):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(6)!=null?(String)lstErrorReg.get(6):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(7)!=null?"$"+Comunes.formatoDecimal((String)lstErrorReg.get(7),2):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(8)!=null?(String)lstErrorReg.get(8):"--", "formas", ComunesXLS.CENTER, 1);
			}
		}
	}
			
	xls.cierraTabla();
	xls.cierraXLS();
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();
	
}else if ( informacion.equals("generarArchivoAcuse")  )  {
	JSONObject jsonObj = new JSONObject();
	String folio = (request.getParameter("folio") != null) ? request.getParameter("folio") : "";
	String fec_venc  = request.getParameter("fec_venc")==null?"":request.getParameter("fec_venc");
	String fec_Actual  = request.getParameter("fec_Actual")==null?"":request.getParameter("fec_Actual");
	String montoTotal = "0";
	String ultimo_reg = "";
	String sumaTotal = "0";
	String usuario = strNombreUsuario;
	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";	
	ComunesPDF docPdf = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
	docPdf.encabezadoConImagenes(docPdf,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	
	docPdf.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	docPdf.addText(" ","formas",ComunesPDF.RIGHT);
		
	docPdf.addText("Programa: "+descProgramaFondoJR,"formasmen",ComunesPDF.CENTER);
	docPdf.addText(" ");
	docPdf.addText("La Autentificación fue Realizada con Exito","formasG",ComunesPDF.CENTER);
	docPdf.addText("Folio: "+folio,"formasG",ComunesPDF.CENTER);
	docPdf.addText(" ");
	docPdf.addText("Nombre del Usuario que Autoriza: "+usuario,"formasmen",ComunesPDF.CENTER);
	docPdf.addText("Fecha y Hora de Actualizacion: "+fec_Actual,"formasmen",ComunesPDF.CENTER);
	
	
	docPdf.setTable(5,80);//.addText()
	docPdf.setCell("Fecha de Vencimiento","formasmenB",ComunesPDF.CENTER);		
	docPdf.setCell("Fecha y Hora de Ultimo Registro FIDE","formasmenB",ComunesPDF.CENTER);
	docPdf.setCell("Folio de Operación","formasmenB",ComunesPDF.CENTER);
	docPdf.setCell("Total de Registros","formasmenB",ComunesPDF.CENTER);		
	docPdf.setCell("Monto","formasmenB",ComunesPDF.CENTER);
			
			
	List lstAcuseGral = fondoJunior.consAcuseMonitorValidado(folio, usuario, fec_venc);
	if(lstAcuseGral!=null && lstAcuseGral.size()>0){
		for(int x=0; x<lstAcuseGral.size(); x++){
			List lstRegAcuse =  (List)lstAcuseGral.get(x);
			if(((String)lstRegAcuse.get(2)).equals("encabezado")){
				folio = (String)lstRegAcuse.get(0);
				usuario = (String)lstRegAcuse.get(1);
			}else{
				montoTotal = (String)lstRegAcuse.get(1);
				ultimo_reg = (String)lstRegAcuse.get(3);	
				if(((String)lstRegAcuse.get(2)).equals("total"))
					fec_venc = (String)lstRegAcuse.get(4);
				if(((String)lstRegAcuse.get(2)).equals("reembolsos"))
					fec_venc = "Reembolsos";
				if(((String)lstRegAcuse.get(2)).equals("recuperacion"))
					fec_venc = "Recuperaciones";
				if(((String)lstRegAcuse.get(2)).equals("prepagos"))
					fec_venc = "Prepagos";
							
				docPdf.setCell(fec_venc,"formasmen",ComunesPDF.CENTER);		
				docPdf.setCell(ultimo_reg,"formasmen",ComunesPDF.CENTER);
				docPdf.setCell(folio,"formasmen",ComunesPDF.CENTER);
				docPdf.setCell(sumaTotal,"formasmen",ComunesPDF.CENTER);		
				docPdf.setCell("$"+Comunes.formatoDecimal(montoTotal,2),"formasmen",ComunesPDF.CENTER);											
			}					
		}
	}
	
	docPdf.addTable();			
	docPdf.endDocument();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();					

} else if(informacion.equals("borrarRegErroneos")){
	JSONObject jsonObj = new JSONObject();
	List lstRegElim = new ArrayList();
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String  tipoRegistros = (request.getParameter("tipoRegistros") == null)?"":request.getParameter("tipoRegistros");
	String  causasEliminacion = (request.getParameter("causasEliminacion") == null)?"":request.getParameter("causasEliminacion");
	
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		HashMap mp = new HashMap();
		/*
		System.out.println("1---"+registro.getString("FECVENC"));
		System.out.println("2---"+registro.getString("PRESTAMO"));
		System.out.println("3---"+registro.getString("NUMCUOTA"));
		System.out.println("4---"+registro.getString("MONTOVENC"));
		System.out.println("5---"+registro.getString("ESTATUS"));
		*/
		mp.put("FECVENC", registro.getString("FECVENC"));
		mp.put("PRESTAMO", registro.getString("PRESTAMO"));
		mp.put("NUMCUOTA", registro.getString("NUMCUOTA"));
		mp.put("MONTOVENC", registro.getString("MONTOVENC"));
		mp.put("ESTATUS", registro.getString("ESTATUS"));
		
		lstRegElim.add(mp);

	} 
	
	fondoJunior.eliminaRegMonitorValid( lstRegElim, tipoRegistros, causasEliminacion, strNombreUsuario);
	String tipoDet = "";
	if("NA".equals(tipoRegistros)) tipoDet = "NoEnviados";
	if("FI".equals(tipoRegistros)) tipoDet = "NoEncontrados";
	
	String fec_Actual = fHora.format(new java.util.Date());
	JSONArray jsObjArray = new JSONArray();
	jsonObj.put("tipoDet", tipoDet);
	jsonObj.put("success", new Boolean(true));
	
	infoRegresar = jsonObj.toString();

}else if ( informacion.equals("generarArchivoMonitorZip")  )  {
	JSONObject jsonObj = new JSONObject();

	String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");
	String nombrePrograma = (String)request.getSession().getAttribute("descProgramaFondoJR");
	String faseProcGenZip = (String)request.getParameter("faseProcGenZip");
	String estatus_val = (String)request.getParameter("estatus_val");
	String pantalla = (String)request.getParameter("pantalla");
	String folio_val = (String)request.getParameter("folio_val");
	String fecVenc = request.getParameter("fecVenc")==null?"":(String)request.getParameter("fecVenc");
	String chkDesembolso = request.getParameter("chkDesembolso")==null?"":(String)request.getParameter("chkDesembolso");
	
	String  estatus = (request.getParameter("estatus") == null)?"":request.getParameter("estatus");
	String  estatus2 = (request.getParameter("estatus2") == null)?"":request.getParameter("estatus2");
	String  proc = (request.getParameter("proc") == null)?"":request.getParameter("proc");
	
	System.out.println("pantalla=="+pantalla+pantalla+pantalla);
	if("1".equals(faseProcGenZip)){
		session.removeAttribute("GeneraArchivoGralZipThread");
		GeneraArchivoGralZipThread generaArchivoGralZipThread = new GeneraArchivoGralZipThread();
		
		generaArchivoGralZipThread.setRutaFisica(strDirectorioTemp);
		generaArchivoGralZipThread.setRutaVirtual(strDirectorioTemp);
		generaArchivoGralZipThread.setClavePrograma(clavePrograma);
		generaArchivoGralZipThread.setNombrePrograma(nombrePrograma);
		generaArchivoGralZipThread.setPantalla(pantalla);
		generaArchivoGralZipThread.setEstatus_val(estatus_val);
		generaArchivoGralZipThread.setFolio_val(folio_val);
		generaArchivoGralZipThread.setFecVencimiento(fecVenc);
		
		generaArchivoGralZipThread.setEstatusDetVal(estatus);
		generaArchivoGralZipThread.setEstatus2DetVal(estatus2);
		generaArchivoGralZipThread.setProcDetVal(proc);
		
		generaArchivoGralZipThread.setRunning(true);
		new Thread(generaArchivoGralZipThread).start();
		session.setAttribute("GeneraArchivoGralZipThread", generaArchivoGralZipThread);
		jsonObj.put("faseProcGenZip", "2");
		
	}else if("2".equals(faseProcGenZip)){
		String mensaje = "";
		GeneraArchivoGralZipThread generarZip = (GeneraArchivoGralZipThread)session.getAttribute("GeneraArchivoGralZipThread");
		if(generarZip.hasError()){
			mensaje="Error inesperado al generar archivo ZIP";
			jsonObj.put("mensaje", mensaje);
			jsonObj.put("faseProcGenZip", "3");
		} 
		if(generarZip.getNombreArchivo()==null && generarZip.isRunning()){
			mensaje="Generando Archivo ZIP";	
			jsonObj.put("faseProcGenZip", "2");
		}else if(!generarZip.isRunning() ){
			String archivo =strDirecVirtualTemp+generarZip.getNombreArchivo();		
			jsonObj.put("faseProcGenZip", "4");
			jsonObj.put("urlArchivo", archivo);
		}	
	
	}else if("4".equals(faseProcGenZip)){
	
	}
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();					

}else if ( informacion.equals("generarArchivoMonitorXls")  )  {
	JSONObject jsonObj = new JSONObject();

	String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");
	String nombrePrograma = (String)request.getSession().getAttribute("descProgramaFondoJR");
	String faseProcGenZip = (String)request.getParameter("faseProcGenZip");
	String estatus_val = (String)request.getParameter("estatus_val");
	String pantalla = (String)request.getParameter("pantalla");
	String folio_val = (String)request.getParameter("folio_val");
	String fecVenc = request.getParameter("fecVenc")==null?"":(String)request.getParameter("fecVenc");
	String chkDesembolso = request.getParameter("chkDesembolso")==null?"":(String)request.getParameter("chkDesembolso");
	
	String  estatus = (request.getParameter("estatus") == null)?"":request.getParameter("estatus");
	String  estatus2 = (request.getParameter("estatus2") == null)?"":request.getParameter("estatus2");
	String  proc = (request.getParameter("proc") == null)?"":request.getParameter("proc");
	
	System.out.println("pantalla=="+pantalla+pantalla+pantalla);
	
	String archivo = fondoJunior.crearXLSDetValidMonitor(pantalla, estatus,  estatus2, proc, fecVenc, clavePrograma, strDirectorioTemp);

	archivo =strDirecVirtualTemp+archivo;		
	jsonObj.put("urlArchivo", archivo);	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();					

}

System.out.println("infoRegresar = " + infoRegresar);

%>
<%=infoRegresar%>