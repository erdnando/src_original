<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>
<%
String typeNav = request.getHeader("User-Agent");
boolean isNavChrome = false;

if (typeNav != null) {
	isNavChrome = ( typeNav.indexOf("Chrome/") != -1 );
}
%>
<html>
  <head>
    <%@ include file="/extjs.jspf" %>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
  <link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
	<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
	

	<script type="text/javascript" src="33CargaDesemFide.js?<%=session.getId()%>"></script>
	 <title>Conciliacion</title>
<%if(esEsquemaExtJS)  { %>
		<%@ include file="/01principal/menu.jspf"%>
<%}%>		
 </head>
 <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
 <%if(!isNavChrome){%>
  <div id="_ObjFirma" style="display:none;">
 <%}%>
 <%@ include file="/00utils/componente_firma.jspf" %>
 <%if(!isNavChrome){%>
</div>
 <%}%>
 <%if(esEsquemaExtJS)  { %>
  
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
      <div id='areaContenido' style="margin-left: 3px; margin-top: 3px; text-align:center"></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
  
<%}else  {%>
	
	<div id="Contcentral">
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<form id='formAux' name="formAux" target='_new'></form>	
		
<%}%>
 </body>
</html>
