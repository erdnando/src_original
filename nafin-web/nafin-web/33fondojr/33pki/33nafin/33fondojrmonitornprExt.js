Ext.onReady(function() {
	
	var chkDesembolso =  Ext.getDom('chkDesembolso').value;
	
	//GENERACION DEL ARCHIVO ZIP
	function procesarGenerarZip(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if(Ext.util.JSON.decode(response.responseText).isRunning==false) {	
				var fp = Ext.getCmp('forma');
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.URLArchivoZip;				  
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();	
				
			}else if(Ext.util.JSON.decode(response.responseText).isRunning==true) {	
			
				Ext.Ajax.request({
					url:'33fondojrmonitornpr.data.jsp',
					params:{
						informacion:'SeguirLeyendo'						
					},
					callback: procesarGenerarZip
				});						
			}
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesoGenerarZIP = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var fec_venc = registro.get('MFECHA_VENC');
		var pantalla  ='NoPagadosRechazados';
		
		
		Ext.Ajax.request({
			url: '33fondojrmonitornpr.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'procesoGenerarZIP',
				fec_venc:fec_venc,
				pantalla:pantalla,
				chkDesembolso:chkDesembolso
			}),
			callback: procesarGenerarZip
		});
	}
	
		
	//VER  CONSULTA DE  ETALLE 	
	var procesoVerDetalle = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var estatus = 'NPR';
		var fec_venc  = registro.get('MFECHA_VENC');
		titulo = 'Detalle de Cr�ditos No Pagados Rechazados';
		consultaDetalleData.load({
				params:{
				estatus:estatus,
				fec_venc:fec_venc
				}
			});
			
		var ventana = Ext.getCmp('VerDetalle');
		if (ventana) {
			ventana.show();
		}else {			
			new Ext.Window({			
				width: 800,				
				height: 400,
				autoScroll:true, 
				resizable: false,
				closeAction: 'hide',
				id: 'VerDetalle',
				autoDestroy:true,
				closable:false,
				modal:true,
				align: 'center',				
				items: [
					gridDetalle
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	buttons: [
						{
							xtype: 'button', iconCls: 'icoLimpiar', 	text: 'Cerrar',id: 'btnCerrarDe', 
							handler: function(){								
								Ext.getCmp('VerDetalle').destroy();																
							} 
						}
					]
				},
				title: titulo
			}).show();
		}
	}
	
	
	var procesarConsultaDetaData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridDetalle = Ext.getCmp('gridDetalle');
			var el = gridDetalle.getGridEl();
			var store = consultaDetalleData;
			if(store.getTotalCount()>0){
				el.unmask();
			}else {
				el.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}
		
	var consultaDetalleData = new Ext.data.JsonStore({
		root: 'registros',
		url: '33fondojrmonitornpr.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetalle'			
		},
		fields: [
			{name: 'FECHA_VEN_NA'},
			{name: 'NO_PRESTAMO'},
			{name: 'NO_CLIENTE'},
			{name: 'MONTO_AMORTIZACIO'},
			{name: 'MONTO_INTERES'},
			{name: 'TOTAL_VENC'},
			{name: 'FECHA_PAGO'},
			{name: 'ESTATUS'},
			{name: 'FECHA_PERIODO'},
			{name: 'NO_DOCTO'},
			{name: 'FECHA_REGISTRO'}			
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDetaData,
			exception: {
				fn: function(proxy,type,action,optionRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
					procesarConsultaDetaData(null,null,null);
				}
			}
		}
	});
	
	var gridDetalle = {
		xtype: 'editorgrid',
		store: consultaDetalleData,
		id: 'gridDetalle',					
		columns: [
			{
				header : 'Fecha de Vencimiento Nafin',
				tooltip: 'Fecha de Vencimiento Nafin',
				dataIndex : 'FECHA_VEN_NA',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'No. Pr�stamo',
				tooltip: 'No. Pr�stamo',
				dataIndex : 'NO_PRESTAMO',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'No. Cliente',
				tooltip: 'No. Cliente',
				dataIndex : 'NO_CLIENTE',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Monto Amortizaci�n',
				tooltip: 'Monto Amortizaci�n ',
				dataIndex : 'MONTO_AMORTIZACIO',
				width : 150,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto Inter�s',
				tooltip: 'Monto Inter�s',
				dataIndex : 'MONTO_INTERES',
				width : 150,
				sortable : true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Total Venc',
				tooltip: 'Total Venc',
				dataIndex : 'TOTAL_VENC',
				width : 150,
				sortable : true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Fecha Pago CLiente',
				tooltip: 'Fecha Pago CLiente',
				dataIndex : 'FECHA_PAGO',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Fecha Periodo Fin',
				tooltip: 'Fecha Periodo Fin',
				dataIndex : 'FECHA_PERIODO',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'N�mero Docto',
				tooltip: 'N�mero Docto',
				dataIndex : 'NO_DOCTO',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Fecha Registro FIDE',
				tooltip: 'Fecha Registro FIDE',
				dataIndex : 'FECHA_REGISTRO',
				width : 150,
				sortable : true,
				align: 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 780,
		height: 330,  
		frame: true
	}
	
	
//****************************CONSULTA ****************************************************

	
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarCSV');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarCSV');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			var el = gridConsulta.getGridEl();
			Ext.getCmp('btnBajarCSV').hide();	
			
			if(store.getTotalCount() > 0) {					
				el.unmask();		
				Ext.getCmp('btnGenerarCSV').enable();	
			} else {	
				Ext.getCmp('btnGenerarCSV').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '33fondojrmonitornpr.data.jsp',
		baseParams: {
			informacion: 'Consultar',
			operacion: 'Generar'
		},
		fields: [
			{name: 'MESTATUS'},
			{name: 'MFECHA_VENC'},		
			{name: 'MULTIMO_REG'},		
			{name: 'MFOLIO'},
			{name: 'MTOTAL_REG'},
			{name: 'MMONTO_TOT'},
			{name: 'MESTATUS_PROC'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{
					fn: function(store, options){
						Ext.apply(options.params, {						
							fec_venc_ini:	Ext.util.Format.date(Ext.getCmp('fec_venc_ini').getValue(),'d/m/Y'),						
							fec_venc_fin:	Ext.util.Format.date(Ext.getCmp('fec_venc_fin').getValue(),'d/m/Y')									
						});
					}		
				},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy,type,action,optionRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
					procesarConsultaData(null,null,null);
				}
			}
		}
	});
	
	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '',		
		columns: [	
			{
				header: 'Fecha de Vencimiento Nafin',
				tooltip: 'Fecha de Vencimiento Nafin',
				dataIndex: 'MFECHA_VENC',
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center'
			},
			{
				header: 'NP Rechazados',
				tooltip: 'NP Rechazados',
				dataIndex: 'MTOTAL_REG',
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center'
			},			
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MMONTO_TOT',
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				xtype: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle',				
				sortable: true,
				width: 150,				
				resizable: true,				
				align: 'center',
				items: [					
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							var  numReg =  parseInt(registro.get('MTOTAL_REG'));  
							if( numReg <500) {								
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';									
							}
						}
						,	handler: procesoVerDetalle
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							var  numReg =  parseInt(registro.get('MTOTAL_REG'));  
							if( numReg >500) {								
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';									
							}
						}
						,	handler: procesoGenerarZIP
					}
				]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 600,		
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSV',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '33fondojrmonitornpr.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV'															
							}),
							callback: procesarGenerarCSV
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSV',
					iconCls: 'icoXls',
					hidden: true
				},
				{
					xtype: 'button',
					text: 'Regregsar',
					id: 'btnRegregsar',
					iconCls: 'icoLimpiar',					
					handler: function(boton, evento) {	
						window.location = '33fondojrmonitorExt.jsp';
					}
				}
			]
		}
	});
	
//****************************FORMA****************************************************

	
	var textoPrograma = new Ext.Container({
		layout: 'table',		
		id: 'textoPrograma',							
		width:	'700',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensaje', 	value: '' }				
		]
	});
	
	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var  jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
						
				Ext.getCmp('mensaje').setValue(jsonValoresIniciales.programa);
				Ext.getCmp('textoPrograma').show();
			}			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var elementosForma =  [		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento:',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fec_venc_ini',
					id: 'fec_venc_ini',
					allowBlank: true,
					startDay: 0,
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fec_venc_fin',
					margins: '0 20 0 0' ,
					formBind: true},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'fec_venc_fin',
					id: 'fec_venc_fin',
					allowBlank: true,
					startDay: 1,
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fec_venc_ini',
					margins: '0 20 0 0',
					formBind: true
				}
			]
		}		
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Monitor - No Pagados Rechazados ',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Actualizar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton, evento) {									
					
					var fec_venc_ini =  Ext.getCmp("fec_venc_ini");
					var fec_venc_fin =  Ext.getCmp("fec_venc_fin");
					if ( ( !Ext.isEmpty(fec_venc_ini.getValue())  &&   Ext.isEmpty(fec_venc_fin.getValue()) )
						||  ( Ext.isEmpty(fec_venc_ini.getValue())  &&   !Ext.isEmpty(fec_venc_fin.getValue()) ) ){
						fec_venc_ini.markInvalid('Debe capturar ambas Fecha  o dejarlas en blanco');
						fecha_vigencia_fin.markInvalid('Debe capturar ambas Fecha  o dejarlas en blanco');
						return;
					}
					
					Ext.getCmp('btnBajarCSV').hide();
					
					fp.el.mask('Enviando...', 'x-mask-loading');	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar',
							start:0,
							limit:15					
						})
					});
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				formBind: true,
				handler: function(boton, evento) {	
					window.location = '33fondojrmonitornprExt.jsp';
				}
			}
		]
	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			textoPrograma,
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)			
		]
	});

	
	Ext.Ajax.request({
		url: '33fondojrmonitornpr.data.jsp',
		params: {informacion: 'valoresIniciales'},
		callback: procesaValoresIniciales
	});
	
});