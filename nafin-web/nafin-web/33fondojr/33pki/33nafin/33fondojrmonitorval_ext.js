Ext.onReady(function() {
	var ini_hidDescProgFondoJR = Ext.getDom('hidDescProgFondoJR').value;
	var ini_strUsuario = Ext.getDom('hidStrUsuario').value;
	var ini_nombreUsuario = Ext.getDom('hidNombreUsuario').value;
	var ini_fecActual = Ext.getDom('hidFecActual').value;
	var ini_estatus = Ext.getDom('estatus').value;
	var ini_fec_venc = Ext.getDom('fec_venc').value;
	//alert(ini_estatus);
	//alert(ini_fec_venc);
	var gbltxtFirmar = '';
	var gbltipoValida = '';
	var gblfolio = '';
	var gbltituloDetValida = '';
	var sumTotalError = '0';
	var csEliminoAlgo = 'N';
	
	var gbl_estatus_val = '';
	var gbl_pantalla = '';
	var gbl_folio_val = '';
	var gbl_fec_venc = '';
	
	var gbl_det_estatus = '';
	var gbl_det_estatus2 = '';
	var gbl_det_proc = '';
	
	var gbl_montoTotDet = '0.0';

//------------------------------------------------------------------------------

	var generaXlsValidMonitor = function(btn, valPantalla){
		btn.setIconClass('loading-indicator');
		btn.setText('Generando Archivo...');
		
		Ext.Ajax.request({
			url: '33fondojrmonitor_ext.data.jsp',
			params:{
				informacion: 'generarArchivoMonitorXls',
				estatus_val: gbl_estatus_val,
				pantalla: gbl_pantalla,
				folio_val: gbl_folio_val,
				fecVenc: gbl_fec_venc,
				faseProcGenZip: '1',
				estatus: gbl_det_estatus,
				estatus2: gbl_det_estatus2,
				proc: gbl_det_proc
				//gbl_pantalla: gbl_pantalla
			},
			callback: processSuccessFailureArchivoXls
		});
	
	}
	
	
	var generaZipMonitor = function(valPantalla){
		
		var ventana = Ext.getCmp('winDescZIP');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				modal: true,
				resizable: false,
				layout: 'form',
				x: 100,
				width: 200,
				id: 'winDescZIP',
				closable: true,
				closeAction: 'hide',
				items: [pnlGeneraZip],
				title: 'Descarga ZIP'
			}).show();
		}
	}
	
	var pnlGeneraZip = new Ext.Panel({
		name: 'pnlGeneraZip',
		id: 'pnlGeneraZip1',
		width: 200,
		style: 'margin:0 auto;',
		frame: true,
		items:[
			{
				xtype: 'button',
				id: 'btnDescZIP',
				iconCls: 'icoZip',
				style: 'margin:0 auto;',
				text:'Descargar ZIP',
				handler: function(boton){
					boton.setIconClass('loading-indicator');
					boton.setText('Generando Archivo ZIP...');
					
					Ext.Ajax.request({
						url: '33fondojrmonitor_ext.data.jsp',
						params:{
							informacion: 'generarArchivoMonitorZip',
							estatus_val: gbl_estatus_val,
							pantalla: gbl_pantalla,
							folio_val: gbl_folio_val,
							fecVenc: gbl_fec_venc,
							faseProcGenZip: '1',
							estatus: gbl_det_estatus,
							estatus2: gbl_det_estatus2,
							proc: gbl_det_proc
							//gbl_pantalla: gbl_pantalla
						},
						callback: processSuccessFailureArchivoZip
					});
				}
			}
		]
	});
	
	function processSuccessFailureArchivoZip(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			if(resp.faseProcGenZip=='2') {
				Ext.Ajax.request({
					url:'33fondojrmonitor_ext.data.jsp',
					params:{
						informacion:'generarArchivoMonitorZip',
						faseProcGenZip: resp.faseProcGenZip
					},
					callback: processSuccessFailureArchivoZip
				});
				
			}else if(resp.faseProcGenZip=='3') {	
				//Ext.MsgBox.alert
				var ventana = Ext.getCmp('winDescZIP');
				ventana.hide();
										
			}else if(resp.faseProcGenZip=='4') {

				var boton = Ext.getCmp('btnDescZIP');
				
				boton.setIconClass('icoZip');
				boton.setText('Descargar ZIP');
				var ventana = Ext.getCmp('winDescZIP');
				ventana.hide();
				var fp = Ext.getCmp('PanelFoma');
				var archivo = resp.urlArchivo;				  
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();	
				//});
			}
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function processSuccessFailureArchivoXls(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			var boton = Ext.getCmp('btnGeneraXlsMon1');
			
			boton.setIconClass('icoXls');
			boton.setText('Descargar Archivo');
			var fp = Ext.getCmp('PanelFoma');
			var archivo = resp.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();	
				//});
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var ingresaCausas = function(){
		var ventana = Ext.getCmp('winDetValidaMonitorCausas');
		
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				modal: true,
				resizable: false,
				layout: 'form',
				x: 100,
				width: 400,
				id: 'winDetValidaMonitorCausas',
				closable: false,
				closeAction: 'hide',
				items: [fpCausasElim],
				title: ''
			}).show();
		}
	};
	
	
	var fnEliminarDet = function(){
		var bOk = false;
		var indiceSm = 0;
		var registrosEnviar = [];
		
		gridDetValidaMonitor.getStore().each(function(record) {
			if(selectModel.isSelected(indiceSm)){
				registrosEnviar.push(record.data);
				bOk = true;
			}
			indiceSm = indiceSm+1;				
		});
		
		if(bOk){
			var causas = Ext.getCmp('causasEliminacion1');
			
			if(causas.getValue()==''){
				Ext.MessageBox.alert('Aviso', 'Favor de capturar las causas de eliminaci�n');
			}else{
				var tipoRegistros = '';
				if(gbltituloDetValida == "Registros No encontrados"){
					tipoRegistros = 'FI';
				} else if(gbltituloDetValida == "Cr�ditos No enviados del d�a"){
					tipoRegistros = 'NA';
				}
				
				pnl.el.mask('Eliminando registros...', 'x-mask-loading');
				Ext.Ajax.request({
					url: '33fondojrmonitor_ext.data.jsp',
					params: {
						informacion: 'borrarRegErroneos',
						registros: Ext.encode(registrosEnviar),
						tipoRegistros: tipoRegistros,
						causasEliminacion: causas.getValue()
					},
					callback: processSuccessFailureBorrarRegErroneos
				});
			}
		}
		
	};
	
	var processSuccessFailureBorrarRegErroneos =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			//storeDataTextLogo.load();
			csEliminoAlgo = 'S';
			Ext.getCmp('causasEliminacion1').setValue('');
			Ext.getCmp('winDetValidaMonitorCausas').hide();
			pnl.el.unmask();
			muestraDetValidacion(resp.tipoDet, gbl_montoTotDet);
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var muestraDetValidacion = function(tipoDet, montoTot){
		var dirige = tipoDet;
		var ver_enlace="";
		var estatus="";
		var estatus2="";
		var proc="";
		var fec_venc = ini_fec_venc;
		gbl_montoTotDet = montoTot;
		
		gbl_pantalla = dirige;

		if(gbltipoValida=='A'){
			if (dirige == 'Vencimientos'){ 	ver_enlace  = "Vencimientos"; estatus = "P";  proc="N"; }
			else if (dirige == 'Enviados'){	ver_enlace  = "Enviados"; estatus = "P"; estatus2 = "NP";	 proc="N"; }
			else if (dirige == 'Pagados'){ 	ver_enlace  = "Pagados"; estatus = "P";	 proc="N"; }
			else if (dirige == 'NoPagados')	   {ver_enlace  = "NoPagados";		estatus = "NP"; proc="N"; }
			else if (dirige == 'NoEncontrados'){ver_enlace  = "NoEncontrados";	estatus = "P"; estatus2 = "NP"; proc="N"; }
			else if (dirige == 'NoEnviados')	   {ver_enlace  = "NoEnviados";		estatus = "P"; estatus2 = "NP"; proc="N"; }
			else if (dirige == 'RembolsosPrepagosTotales'){	ver_enlace = "RembolsosPrepagosTotales"; estatus = "R"; estatus2 = "T";	proc = "N";	}
			else if (dirige == 'Reembolsos')	   {ver_enlace  = "Reembolsos";		estatus = "R"; estatus2 = "R"; proc = "N"; }
			else if (dirige == 'Recuperaciones'){ver_enlace = "Recuperaciones";	estatus = "R"; proc = "N";}
			else if (dirige == 'Prepagos')	   {ver_enlace  = "Prepagos";	estatus = "T";	proc = "N";}
			else if (dirige == 'ConError')	   {ver_enlace  = "ConError";		estatus = "P"; }
			else if (dirige == 'NoPagadosRech')	   {ver_enlace  = "NoPagadosRech";		estatus = "NP"; proc="R"; }
			else if (dirige == 'Prepagados')	   {ver_enlace  = "Prepagados";	estatus = "P"; estatus2 = "NP"; proc="P"; }
			
			gbl_det_estatus = estatus;
			gbl_det_estatus2 = estatus2;
			gbl_det_proc = proc;
			gbl_fec_venc = ini_fec_venc;
			
		}
		
		if(gbltipoValida=='B'){
			if (dirige == 'Vencimientos'){ 	ver_enlace  = "Vencimientos"; estatus = "P";  proc="N"; }
			else if (dirige == 'Enviados'){	ver_enlace  = "Enviados"; estatus = "R"; estatus2 = "T";	 proc="N"; }
			else if (dirige == 'Pagados'){ 	ver_enlace  = "Pagados"; estatus = "P";	 proc="N"; }
			else if (dirige == 'NoPagados')	   {ver_enlace  = "NoPagados";		estatus = "NP"; proc="N"; }
			else if (dirige == 'NoEncontrados'){ver_enlace  = "NoEncontrados";	estatus = "R"; estatus2 = "RC"; proc="N"; }
			else if (dirige == 'NoEnviados')	   {ver_enlace  = "NoEnviados";		estatus = "P"; estatus2 = "NP"; proc="N"; }
			else if (dirige == 'RembolsosPrepagosTotales'){	ver_enlace = "RembolsosPrepagosTotales"; estatus = fec_venc ; estatus2 = "R";	proc = "N";	}
			else if (dirige == 'Reembolsos')	   {ver_enlace  = "Reembolsos";		estatus = "R"; estatus2 = "R"; proc = "N"; }
			else if (dirige == 'Recuperaciones'){ver_enlace = "Recuperaciones";	estatus = "R"; proc = "N";}
			else if (dirige == 'Prepagos')	   {ver_enlace  = "Prepagos";	estatus = "T";	proc = "N"}
			else{ dirige == 'ConError';	   ver_enlace  = "ConError";		estatus = fec_venc;  estatus2="R"; }
			
			gbl_det_estatus = estatus;
			gbl_det_estatus2 = estatus2;
			gbl_det_proc = proc;
			gbl_fec_venc = ini_fec_venc;
		}
		
		if(gbltipoValida=='C'){
			if (dirige == 'Vencimientos'){ 	ver_enlace  = "Vencimientos"; estatus = "P";  proc="N"; }
			else if (dirige == 'Enviados'){	ver_enlace  = "Enviados"; estatus = "R"; estatus2 = "T";	 proc="N"; }
			else if (dirige == 'Pagados'){ 	ver_enlace  = "Pagados"; estatus = "P";	 proc="N"; }
			else if (dirige == 'NoPagados')	   {ver_enlace  = "NoPagados";		estatus = "NP"; proc="N"; }
			else if (dirige == 'NoEncontrados'){ver_enlace  = "NoEncontrados";	estatus = "R"; estatus2 = "R"; proc="N"; }
			else if (dirige == 'NoEnviados')	   {ver_enlace  = "NoEnviados";		estatus = "P"; estatus2 = "NP"; proc="N"; }
			else if (dirige == 'RembolsosPrepagosTotales'){	ver_enlace = "RembolsosPrepagosTotales"; estatus = "R"; estatus2 = "R";	proc = "N";	}
			else if (dirige == 'Reembolsos')	   {ver_enlace  = "Reembolsos";		estatus = "R"; estatus2 = "R"; proc = "N"; }
			else if (dirige == 'Recuperaciones'){ver_enlace = "Recuperaciones";	estatus = "R"; proc = "N";}
			else if (dirige == 'Prepagos')	   {ver_enlace  = "Prepagos";	estatus = "T";	proc = "N"}
			else if (dirige == 'Prepagados')	   {ver_enlace  = "Prepagados";	estatus = "R"; estatus2 = "R"; proc="P"; }
			else{ dirige == 'ConError';	   ver_enlace  = "ConError";		estatus = "R";}
			
			gbl_det_estatus = estatus;
			gbl_det_estatus2 = estatus2;
			gbl_det_proc = proc;
			gbl_fec_venc = ini_fec_venc;
		}

		if("Recuperaciones" == ver_enlace){
			gbltituloDetValida = "Recuperaciones Recibidas";
		}else if("NoEncontrados" == ver_enlace){
			gbltituloDetValida = "Registros No encontrados";
		}else if("NoEnviados" == ver_enlace){
			gbltituloDetValida = "Cr�ditos No enviados del d�a";
		}else if("Vencimientos" == ver_enlace){
			gbltituloDetValida = "Vencimientos del d�a";
		}
		
		if("Enviados" == ver_enlace){
			gbltituloDetValida = "Total de Registros Enviados";
		}
		if("RembolsosPrepagosTotales" == ver_enlace){
			if("R" == estatus){
				gbltituloDetValida = "Total Reembolsos";
			}else if("RC" == estatus){
				gbltituloDetValida = "Total Recuperaciones";
			}else if("T" == estatus){
				gbltituloDetValida = "Total Prepragos";
			}
		}
		
		if("Prepagos" == ver_enlace){
			gbltituloDetValida = "Prepagos Totales";
		}
		if("Pagados" == ver_enlace ){
			gbltituloDetValida = "Cr�ditos Pagados del D�a";
		}
		if ("NoPagados" == ver_enlace){
			gbltituloDetValida = "Cr�ditos No Pagados del D�a";
		}
		if ("Prepagados" == ver_enlace){
			gbltituloDetValida = "Registros Rechazados Cr�ditos Prepagados";
		}
		if ("NoPagadosRech" == ver_enlace){
			gbltituloDetValida = "Cr�ditos No Pagados Rechazados del D�a";
		}
		if("Reembolsos" == ver_enlace){
			gbltituloDetValida = "Reembolsos Totales";
		}
		if("ConError" == ver_enlace){
			gbltituloDetValida = "Cr�ditos con ERROR";
		}
		
		storeDetValidaData.load({
			params:{
				ver_enlace: ver_enlace,
				estatus: estatus,
				estatus2: estatus2,
				proc: proc,
				fec_venc:fec_venc
			}
		});
	}
	
	var processSuccessFailureDetValidaData = function(store, arrRegistros, opts) {
		
		if (arrRegistros != null) {
			var totaldetValid = 0.0;
			var ventana = Ext.getCmp('winDetValidaMonitor');
			gridDetValidaMonitor.setTitle(gbltituloDetValida);
			
			
			if (ventana) {
				ventana.show();
			} else {
				new Ext.Window({
					modal: true,
					resizable: false,
					layout: 'form',
					x: 100,
					width: 780,
					id: 'winDetValidaMonitor',
					closable: false,
					closeAction: 'hide',
					items: [
						gridDetValidaMonitor,
						gridTotalDet,
						new Ext.Panel({
							id: 'msjRegDel',
							style:	'margin:0 auto;',
							title: '',
							width: 770,
							frame: true,
							hidden: true,
							html:'<p align="left"><b>Nota:</b> Los registros eliminados se podr�n consultar en la pantalla Bit�cora pr�stamos eliminados</p>'
						}),
						{//Panel para mostrar avisos
							xtype: 'panel',
							name: 'avisoNumReg',
							id: 'avisoNumReg1',
							height: 30,
							width: 770,
							frame: true,
							style: 'margin:0 auto;',
							bodyStyle: 'text-align:center',
							hidden: false
						}
					],
					title: ''
				}).show();
			}
			
			var el = gridDetValidaMonitor.getGridEl();
			var gridColumnMod = gridDetValidaMonitor.getColumnModel();
			var btnEliminar = Ext.getCmp('btnEliminarDetValid');
			var pnlmsjRegDel = Ext.getCmp('msjRegDel');
			var numReg = store.reader.jsonData.numRegistros;
			if(store.getTotalCount() > 0) {
				//if(gbltipoValida=='C'){
					if(numReg >1000){ 
						Ext.getCmp('btnGeneraZipMon1').show();
						Ext.getCmp('btnGeneraXlsMon1').hide();
						Ext.getCmp('avisoNumReg1').body.update('<p align="left">El n�mero de registros se excede de los 1000, descargar archivo para ebtener el detalle completo</p>');
					}else{
						Ext.getCmp('btnGeneraZipMon1').hide();
						Ext.getCmp('btnGeneraXlsMon1').show();
						Ext.getCmp('avisoNumReg1').body.update(' ');
					}
				//}
				
				if(gbltituloDetValida == "Registros No encontrados"){
					//Ext.getCmp('chkSelectDet').show();
					gridColumnMod.setHidden(0,false);
					btnEliminar.show();
					pnlmsjRegDel.show();
				} else if(gbltituloDetValida == "Cr�ditos No enviados del d�a"){
					gridColumnMod.setHidden(0,false);
					btnEliminar.show();
					pnlmsjRegDel.show();
				} else {
					//Ext.getCmp('chkSelectDet').hide();
					btnEliminar.hide();
					pnlmsjRegDel.hide();
					gridColumnMod.setHidden(0,true);
				}
				//gridTotalDet.show();
				/*store.each(function(record) {
					totaldetValid += Number(record.data['MONTOVENC']);
				});*/
				storeTotalDetData.loadData([['Monto Total:', gbl_montoTotDet]]);
				el.unmask();
			}else {
				//gridTotalDet.hide();
				btnEliminar.hide();
				Ext.getCmp('btnGeneraXlsMon1').hide();
				pnlmsjRegDel.hide();
				gridColumnMod.setHidden(0,true);
				storeTotalDetData.loadData([['Monto Total:', totaldetValid]]);
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var borrarCargaVencimiento = function(pkcs7, vtextoFirmar , vtipoValida){  
	
		if(vtipoValida=='A' || vtipoValida=='B' || vtipoValida=='C'){
			
			if (Ext.isEmpty(pkcs7)) {
				return;	//Error en la firma. Termina...
			}else{
				pnl.el.mask('Borrando registros...', 'x-mask-loading');
				Ext.Ajax.request({
					url: '33fondojrmonitor_ext.data.jsp',
					params: {
						informacion: 'eliminaRegMonitor',
						tipoValida: vtipoValida,
						estatus: ini_estatus,
						fec_venc: ini_fec_venc,
						Pkcs7: pkcs7,
						TextoFirmado: vtextoFirmar
					},
					callback: procesarSuccessFailureBorrarVenc
				});  
			}
		}
	};
		
	
	var confirmarCargaVencimiento = function(pkcs7, vtextoFirmar , vtipoValida){
	
		if(vtipoValida=='A' || vtipoValida=='B' || vtipoValida=='C'){		
			
			if (Ext.isEmpty(pkcs7)) {
				return;	//Error en la firma. Termina...
			}else{
				pnl.el.mask('Confirmando registros...', 'x-mask-loading');
				Ext.Ajax.request({
					url: '33fondojrmonitor_ext.data.jsp',
					params: {
						informacion: 'validaMonitorAcuse',
						tipoValida: vtipoValida,
						estatus: ini_estatus,
						fec_venc: ini_fec_venc,
						Pkcs7: pkcs7,
						TextoFirmado: vtextoFirmar
					},
					callback: procesarSuccessFailureConfirmaVenc
				});  
			}						
		}
	};
	
	
	
	var procesarSuccessFailureBorrarVenc = function(opts, success, response) {
		pnl.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			
			if(gbltipoValida=='A' || gbltipoValida=='B' || gbltipoValida=='C'){
				Ext.MessageBox.alert('Aviso','SE HAN ELIMINADO LOS REGISTROS CON EXITO!!!', function(){
					window.location.href='33fondojrmonitor_ext.jsp';
				});
			}//close if tipoValida
		
		} else {
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				NE.util.mostrarConnError(response,opts);
			}
 
		}
	};
	
	
	var procesarSuccessFailureConfirmaVenc = function(opts, success, response) {
		pnl.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipal = Ext.getCmp('contenedorPrincipal');
			var pnlResumenValida = Ext.getCmp('pnlResumenValida');
			var pnlAcuse = Ext.getCmp('pnlAcuse');
			
			if(resp.mostrarError==''){
				if(gbltipoValida=='A' || gbltipoValida=='B' || gbltipoValida=='C'){
					gblfolio = resp.folio;
					storeAcuseValida.loadData(resp);
					pnlResumenValida.hide();
					pnlAcuse.insert(0,new Ext.Panel({
						id: 'msjX',
						style:	'margin:0 auto;',
						title: '',
						width: 618,
						frame: true,
						hidden: false,
						html:'<p align="center">Confirmaci�n de Validaci�n de Cr�ditos <br>'+
							 'Folio: '+resp.folio+'<br><br>'+
							 'Nombre de Usuario que Autoriza: '+resp.usuario+' <br>'+
							 'Fecha y Hora de Autorizacion: '+resp.fecActual+' <br>'+
							 '</p>'
					}));
					
					pnlAcuse.doLayout();
					pnlAcuse.show();
					//pnlAcuse.add(gridAcuse);
					
					//contenedorPrincipal.add(pnlAcuse);
					//contenedorPrincipal.doLayout();
	
				}//close if tipoValida
			}else{
				
				Ext.Msg.alert(
					'Mensaje',
					resp.mostrarError,
					function(btn, text){
						//NE.util.mostrarConnError(response,opts);
					}
				);
			}
		
		} else {
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				NE.util.mostrarConnError(response,opts);
			}
 
		}
	};
	
	var processSuccessFailureValidaMonitor = function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipal = Ext.getCmp('contenedorPrincipal');
			var pnlResumenValida = Ext.getCmp('pnlResumenValida');
			gbltipoValida = resp.tipoValida;
			sumTotalError = resp.sumaTotalError;
			//alert(gbltipoValida);
			if(gbltipoValida=='A'){
				//Ext.getCmp('msjProgama1').show();
				ini_estatus = 'P';
				
				gbltxtFirmar='Informaci�n de Cr�ditos \n'+
							'Fecha y Hora de Validaci�n: '+ini_fecActual+' \n'+
							'Fecha de vencimiento: '+ini_fec_venc+' \n'+
							'Total Reg: '+resp.sumaTotalTmp+' \n'+
							'Monto Total: '+resp.montoTotalTmp;
							
			
				pnlResumenValida.add(pnlCabecera);
				var dataCabecera = [['Vencimientos del D�a',resp.sumaTotalVista,resp.montoTotalVista, 'Vencimientos']];
				storeCabeceraVencData.loadData(dataCabecera);
				pnlResumenValida.add(gridCabeceraVenc);
				pnlResumenValida.add(NE.util.getEspaciador(10));
				
				var dataValidaInfo = [['Total de Registros Enviados', resp.sumaTotalTmp, resp.montoTotalTmp, 'Enviados']];
				storeValidaInfoData.loadData(dataValidaInfo);
				pnlResumenValida.add(gridValidaInfo);
				
				var dataResumenValid= [['Cr�ditos Pagados del D�a', resp.sumaTotalPagado, resp.montoTotalPagado, 'Pagados'],
										  ['Cr�ditos No Pagados del D�a', resp.sumaTotalNoPagado, resp.montoTotalNoPagado, 'NoPagados'],
										  ['Registros Rechazados Cr�ditos Prepagados', resp.sumaTotalPrepago, resp.montoTotalPrepago, 'Prepagados'],//MOF F006-2014 REQ-1
										  ['NP Rechazados al D�a', resp.sumaTotalNoPagadoR, resp.montoTotalNoPagadoR, 'NoPagadosRech'],
										  ['Registros no Encontrados', resp.sumaTotalNoEncon, resp.montoTotalNoEncon, 'NoEncontrados'],
										  ['Cr�ditos no Enviados del D�a', resp.sumaTotalNoEnv, resp.montoTotalNoEnv, 'NoEnviados'],
										  ['Cr�ditos con Error', resp.sumaTotalError, resp.montoTotalError, 'ConError']
										 ];
										 
				storeResumenValidaData.loadData(dataResumenValid);
				pnlResumenValida.add(gridResumenValida);
				pnlResumenValida.doLayout();
			}else if(gbltipoValida=='B'){//close if tipoValida
				gbltxtFirmar='Informaci�n de Cr�ditos \n'+
							'Fecha y Hora de Validaci�n: '+ini_fecActual+' \n'+
							//'Fecha de vencimiento: '+ini_fec_venc+' \n'+
							'Fecha de vencimiento: \n'+
							'Total Reg: '+resp.sumaTotalTmp+' \n'+
							'Monto Total: '+resp.montoTotalTmp;
			
				pnlResumenValida.add(pnlCabecera);
				pnlResumenValida.add(NE.util.getEspaciador(10));
				
				var dataValidaInfo = [['Total de Registros Enviados', resp.sumaTotalTmp, resp.montoTotalTmp, 'RembolsosPrepagosTotales']];
				storeValidaInfoData.loadData(dataValidaInfo);
				pnlResumenValida.add(gridValidaInfo);
				
				var dataResumenValid= [['Prepagos Totales', resp.sumaTotalPrepago, resp.montoTotalPrepago, 'Prepagos'],
										  ['Cr�ditos con Error', resp.sumaTotalError, resp.montoTotalError, 'ConError']
										 ];
										 
				storeResumenValidaData.loadData(dataResumenValid);
				pnlResumenValida.add(gridResumenValida);
				pnlResumenValida.doLayout();
				
				pnlCabecera.body.update('<p align="center">Informaci�n Resumen <br>'+
										'IF : FIDE <br>'+
										'Fecha de vencimiento: <br>'+
										'Nombre del Validador: '+ini_nombreUsuario+' <br>'+
										'Fecha y Hora de Validaci�n: '+ini_fecActual+
										'</p>'
				);
			
			}else if(gbltipoValida=='C'){//close if tipoValida
				gbltxtFirmar='Informaci�n de Cr�ditos \n'+
							'Fecha y Hora de Validaci�n: '+ini_fecActual+' \n'+
							'Fecha de vencimiento: \n'+
							'Total Reg: '+resp.sumaTotalTmp+' \n'+
							'Monto Total: '+resp.montoTotalTmp;
				
				pnlResumenValida.add(pnlCabecera);
				pnlResumenValida.add(NE.util.getEspaciador(10));
				
				var dataValidaInfo = [['Total de Registros Enviados', resp.sumaTotalTmp, resp.montoTotalTmp, 'RembolsosPrepagosTotales']];
				storeValidaInfoData.loadData(dataValidaInfo);
				pnlResumenValida.add(gridValidaInfo);
				
				var dataResumenValid= [['Reembolsos Recibidos', resp.sumaTotalReemb, resp.montoTotalReemb, 'Reembolsos'],
										  ['Registros Rechazados Cr�ditos Prepagados', resp.sumaTotalPrepago, resp.montoTotalPrepago, 'Prepagados'],//MOF F006-2014 REQ-1
										  //['Registros no Encontrados', resp.sumaTotalNoEncon, resp.montoTotalNoEncon, 'NoEncontrados'],
										  ['Cr�ditos con Error', resp.sumaTotalError, resp.montoTotalError, 'ConError']
										 ];
										 
				storeResumenValidaData.loadData(dataResumenValid);
				pnlResumenValida.add(gridResumenValida);
				pnlResumenValida.doLayout();
				pnlCabecera.body.update('<p align="center">Informaci�n Resumen <br>'+
										'IF : FIDE <br>'+
										'Fecha de vencimiento: <br>'+
										'Nombre del Validador: '+ini_nombreUsuario+' <br>'+
										'Fecha y Hora de Validaci�n: '+ini_fecActual+
										'</p>'
				);
			
			}
			
			if(Number(sumTotalError)>0){
				Ext.getCmp('btnArchErroresValid').enable();
				Ext.getCmp('btnConfirmarValid').disable();
			}else{
				Ext.getCmp('btnArchErroresValid').disable();
				Ext.getCmp('btnConfirmarValid').enable();
			}
			
			if(gbltipoValida=='B'){
				Ext.getCmp('btnBorrarValid').disable();
			}else{
				Ext.getCmp('btnBorrarValid').enable();
			}
			
		
		} else {
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				NE.util.mostrarConnError(response,opts);
			}
 
		}
	};
	
	function procesarArchivoError(opts, success, response) {
		var btnArchErroresValid = Ext.getCmp('btnArchErroresValid');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnArchErroresValid.setIconClass('');
			btnArchErroresValid.enable();
			var infoR = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('PanelFoma');
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			btnArchErroresValid.setIconClass('');
			btnArchErroresValid.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPDF =  function(opts, success, response) {
		var btnImprimirAcuse = Ext.getCmp('btnImprimirAcuse');
		//btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnImprimirAcuse.setIconClass('');
			btnImprimirAcuse.enable();
			/*var btnBajar = Ext.getCmp('btnBajarAcuse');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});*/
			var infoR = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('PanelFoma');
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit()
			
		} else {
			btnImprimirAcuse.setIconClass('');
			btnImprimirAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
//------------------------------------------------------------------------------
	var storeCabeceraVencData = new Ext.data.ArrayStore({
		id: 'storeCabeceraVencData1',
		fields: [
			'TITULOS',
			'NOCREDITOS',
			'MONTOVENCDIA',
			'TIPODET'
		]
	});
	
	var storeValidaInfoData = new Ext.data.ArrayStore({
		id: 'storeValidaInfoData1',
		fields: [
			'TITULOS',
			'NOREGISTROS',
			'MONTOFIDE',
			'TIPODET'
		]
	});
	
	var storeResumenValidaData = new Ext.data.ArrayStore({
		id: 'storeResumenValidaData1',
		fields: [
			'TITULOS',
			'NOREGISTROS',
			'MONTOFIDE',
			'TIPODET'
		]
	});
	
	var storeAcuseValida = new Ext.data.JsonStore({
		id: 'storeAcuseValida1',
		root : 'registros',
		fields: [
			{name: 'TOTALREG'},
			{name: 'MONTO'},
			{name: 'TITULO'},
			{name: 'FECULTIMOREG'},
			{name: 'FECVENC'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false
		/*listeners: {
			load: processSuccessFailureDetMonitor,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					processSuccessFailureDetMonitor(null, null, null);
				}
			}
		}*/
	});
	
	var storeDetValidaData = new Ext.data.JsonStore({
		url : '33fondojrmonitor_ext.data.jsp',
		id: 'storeDetValidaData1',
		root : 'registros',
		baseParams: {
			informacion: 'consultaDetValidaMonitor'
		},
		fields: [
			{name: 'FECVENC'},
			{name: 'FECOPER'},
			{name: 'FECPAGO'},
			{name: 'PRESTAMO'},
			{name: 'NUMSIRAC'},
			{name: 'CLIENTE'},
			{name: 'NUMCUOTA'},
			{name: 'MONTOVENC'},
			{name: 'ERROR'},
			{name: 'ESTATUS'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: processSuccessFailureDetValidaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					processSuccessFailureDetValidaData(null, null, null);
				}
			}
		}
	});
	
	var storeTotalDetData = new Ext.data.ArrayStore({
		  id: 0,
		  fields: [
				'TITULOS',
				'MONTO'
		  ],
		  autoLoad: false
		});

//------------------------------------------------------------------------------
	var gridCabeceraVenc = new Ext.grid.EditorGridPanel({
		id: 'gridCabeceraVenc',
		store: storeCabeceraVencData,
		style:	'margin:0 auto;',
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header: ' ',
				dataIndex: 'TITULOS',
				sortable: true,
				width: 200,
				resizable: true,
				hidden: false
			},
			{
				header: 'No. de Cr�ditos',
				tooltip: 'No. de Cr�ditos',
				dataIndex: 'NOCREDITOS',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{
				header: 'Importe',
				tooltip: 'Importe',
				dataIndex: 'MONTOVENCDIA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 		'actioncolumn',
				header: 		'Detalle',
				tooltip: 	'Detalle',
				index:		'TIPODET',
				width: 		100,
				align:		'center',
				hidden: 		false,
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver detalle';
							return 'iconoLupa';
						},
						handler: function(grid, rowIndex, colIndex) {
							var registro = Ext.StoreMgr.key('storeCabeceraVencData1').getAt(rowIndex);
							muestraDetValidacion(registro.data['TIPODET'], registro.data['MONTOVENCDIA']);
						}
					}
				]
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 100,
		width: 618,
		style: 'margin:0 auto;',
		title: '',
		frame: true
	});
	
	var gridValidaInfo = new Ext.grid.EditorGridPanel({
		id: 'gridValidaInfo',
		store: storeValidaInfoData,
		style:	'margin:0 auto;',
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header: ' ',
				dataIndex: 'TITULOS',
				sortable: true,
				width: 200,
				resizable: true,
				hidden: false
			},
			{
				header: 'No. de Registros',
				tooltip: 'No. de Registros',
				dataIndex: 'NOREGISTROS',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{
				header: 'Importe',
				tooltip: 'Importe',
				dataIndex: 'MONTOFIDE',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 		'actioncolumn',
				header: 		'Detalle',
				tooltip: 	'Detalle',
				index:		'TIPODET',
				width: 		100,
				align:		'center',
				hidden: 		false,
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver detalle';
							return 'iconoLupa';
						},
						handler: function(grid, rowIndex, colIndex) {
							var registro = Ext.StoreMgr.key('storeValidaInfoData1').getAt(rowIndex);
							muestraDetValidacion(registro.data['TIPODET'], registro.data['MONTOFIDE']);
						}
					}
				]
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 100,
		width: 618,
		style: 'margin:0 auto;',
		title: '<p align="center">Validaci�n de la Informaci�n</p>',
		frame: true
	});
	
	var gridResumenValida = new Ext.grid.EditorGridPanel({
		id: 'gridResumenValida',
		store: storeResumenValidaData,
		style:	'margin:0 auto;',
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header: ' ',
				dataIndex: 'TITULOS',
				sortable: true,
				width: 200,
				resizable: true,
				hidden: false
			},
			{
				header: 'No. de Registros',
				tooltip: 'No. de Registros',
				dataIndex: 'NOREGISTROS',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{
				header: 'Importe',
				tooltip: 'Importe',
				dataIndex: 'MONTOFIDE',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 		'actioncolumn',
				header: 		'Detalle',
				tooltip: 	'Detalle',
				index:		'TIPODET',
				width: 		100,
				align:		'center',
				hidden: 		false,
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver detalle';
							return 'iconoLupa';
						},
						handler: function(grid, rowIndex, colIndex) {
							var registro = Ext.StoreMgr.key('storeResumenValidaData1').getAt(rowIndex);
							muestraDetValidacion(registro.data['TIPODET'], registro.data['MONTOFIDE']);
						}
					}
				]
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 250,
		width: 618,
		style: 'margin:0 auto;',
		title: '<p align="center">Resumen de Env�o</p>',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'center',
			items: [
				'-',
				{
					text: 'Descargar Archivo Errores',
					id: 'btnArchErroresValid',
					handler: function(btn){
						btn.setIconClass('loading-indicator');
						btn.disable();
						Ext.Ajax.request({
							url: '33fondojrmonitor_ext.data.jsp',
							params: {																								
								informacion:'generaXLSCreditos',
								fec_venc: ini_fec_venc,
								estatus: ini_estatus
							},
							callback: procesarArchivoError
						});
					}
				},
				'-',
				{
					text: 'Borrar',
					id: 'btnBorrarValid',
					handler: function(){
												
						NE.util.obtenerPKCS7(borrarCargaVencimiento, gbltxtFirmar , gbltipoValida );
					}
				},
				'-',
				{
					text: 'Confirmar',
					id: 'btnConfirmarValid',
					handler: function(){					
					
						NE.util.obtenerPKCS7(confirmarCargaVencimiento, gbltxtFirmar , gbltipoValida );
						
					}
				},
				'-',
				{
					text: 'Cancelar',
					handler: function(){
						window.location.href='33fondojrmonitor_ext.jsp';
					}
				},
				'-'
			]
		}
	});

	var gridAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridAcuse',
		store: storeAcuseValida,
		style:	'margin:0 auto;',
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECVENC',
				sortable: true,
				width: 200,
				resizable: true,
				hidden: false,
				renderer:  function (valor, columna, registro){
					if(registro.data['TITULO']=='prepagos'){
						return 'Prepagos';
					}else if(registro.data['TITULO']=='reembolsos'){
						return 'Reembolsos';
					}else if(registro.data['TITULO']=='recuperacion'){
						return 'Recuperaciones';
					}else{
						return valor;
					}
					
				}
				
			},
			{
				header: 'Fecha y Hora de Ultimo Registro FIDE',
				tooltip: 'Fecha y Hora de Ultimo Registro FIDE',
				dataIndex: 'FECULTIMOREG',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{
				header: 'Folio de Operaci�n',
				tooltip: 'Folio de Operaci�n',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				renderer:  function (valor, columna, registro){
					return gblfolio;
				}
				
			},
			{
				header: 'Total de Registros',
				tooltip: 'Total de Registros',
				dataIndex: 'TOTALREG',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 250,
		width: 618,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'center',
			items: [
				'-',
				{
					text: 'Regresar a Monitor',
					handler: function(){
						window.location.href='33fondojrmonitor_ext.jsp';
					}
				},
				'-',
				{
					text: 'Imprimir PDF',
					id: 'btnImprimirAcuse',
					handler: function(btn){
						btn.setIconClass('loading-indicator');
						btn.disable();
						Ext.Ajax.request({
							url: '33fondojrmonitor.data.jsp',
							params: {																								
								informacion:'generarArchivoAcuse',
								usuario: ini_nombreUsuario,					
								folio: gblfolio,
								fec_venc: ini_fec_venc,
								fec_actual: ini_fecActual
							},
							callback: procesarGenerarPDF
						});
					}
				},
				'-'
			]
		}
	});
	
	
	var selectModel = new Ext.grid.CheckboxSelectionModel({
		//id:'chkSelectDet',
		checkOnly: false
   });
	
	var gridDetValidaMonitor = new Ext.grid.GridPanel({
		id: 'gridDetValidaMonitor1',
		store: storeDetValidaData,
		sm: selectModel,
		columns: [
			selectModel,
			{//1
				header: 'Fecha Autorizaci�n Nafin',
				tooltip: 'Fecha Autorizaci�n Nafin',
				dataIndex: 'FECVENC',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'Fecha Operaci�n',
				tooltip: 'Fecha Operaci�n',
				dataIndex: 'FECOPER',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'Fecha Pago Cliente FIDE',
				tooltip: 'Fecha Pago Cliente FIDE',
				dataIndex: 'FECPAGO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'Pr�stamo',
				tooltip: 'Pr�stamo',
				dataIndex: 'PRESTAMO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'N�m. Cliente SIRAC',
				tooltip: 'N�m. Cliente SIRAC',
				dataIndex: 'NUMSIRAC',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
				
			},
			{
				header: 'Cliente',
				tooltip: 'Cliente',
				dataIndex: 'CLIENTE',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'N�m. Cuota a Pagar',
				tooltip: 'N�m. Cuota a Pagar',
				dataIndex: 'NUMCUOTA',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{
				header: 'Capital m�s Inter�s',
				tooltip: 'Capital m�s Inter�s',
				dataIndex: 'MONTOVENC',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 360,
		width: 770,
		title: ' ',
		frame: true
		/*bbar: {
			xtype: 'toolbar',
			buttonAlign: 'center',
			items: [
				'-',
				{
					text: 'Cerrar',
					iconCls:'icoRechazar',
					handler: function(){
						Ext.getCmp('winDetValidaMonitor').hide();
					}
				},
				'-',
				{
					text: 'Eliminar',
					id: 'btnEliminarDetValid',
					iconCls:'icon-register-delete',
					handler: function(){
						var bOk = false;
						var indiceSm = 0;
						gridDetValidaMonitor.getStore().each(function(record) {
							if(selectModel.isSelected(indiceSm)){
								bOk = true;
							}			
							indiceSm = indiceSm+1;				
						});
	
						if(bOk){
						
							Ext.MessageBox.confirm('Confirmaci�n','�Esta seguro de eliminar los registrsos seleccionados?',function(msb){
								if(msb=='yes'){
									ingresaCausas();
								}
							});
						}else{
							Ext.MessageBox.alert('Mensaje','Favor de seleccionar un registro');
						}
					}
				},
				'-'
			]
		}*/
	});
	
	
	var gridTotalDet = new Ext.grid.EditorGridPanel({
		id: 'gridTotalDet1',
		store: storeTotalDetData,
		style:	'margin:0 auto;',
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header: '',
				dataIndex: 'TITULOS',
				sortable: true,
				width: 590,
				resizable: true,
				align: 'right',
				hidden: false
			},
			{
				header: '',
				tooltip: 'Importe',
				dataIndex: 'MONTO',
				sortable: true,
				width: 160,
				resizable: true,
				align: 'right',
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		hideHeaders: true,
		height: 70,
		width: 770,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'center',
			items: [
				'-',
				{
					text: 'Cerrar',
					iconCls:'icoRechazar',
					handler: function(){
						
						if(csEliminoAlgo=='S'){
							var dataRequest = '?fec_venc='+ini_fec_venc+'&estatus_cred='+ini_estatus;
							window.location.href='33fondojrmonitorval_ext.jsp'+dataRequest;
						}else{
							Ext.getCmp('winDetValidaMonitor').hide();
						}
					}
				},
				'-',
				{
					text: 'Eliminar',
					id: 'btnEliminarDetValid',
					iconCls:'icon-register-delete',
					handler: function(){
						var bOk = false;
						var indiceSm = 0;
						gridDetValidaMonitor.getStore().each(function(record) {
							if(selectModel.isSelected(indiceSm)){
								bOk = true;
							}			
							indiceSm = indiceSm+1;				
						});
	
						if(bOk){
						
							Ext.MessageBox.confirm('Confirmaci�n','�Esta seguro de eliminar los registros seleccionados?',function(msb){
								if(msb=='yes'){
									ingresaCausas();
								}
							});
						}else{
							Ext.MessageBox.alert('Mensaje','Favor de seleccionar un registro');
						}
					}
				},
				'-',
				{
					text: 'Descargar Archivo',
					id: 'btnGeneraXlsMon1',
					iconCls:'icoXls',
					hidden : false,
					handler: function(btn){
						generaXlsValidMonitor(btn, gbl_pantalla);
					}
				},
				{
					text: 'Generar Archivo',
					id: 'btnGeneraZipMon1',
					iconCls:'icoGenerarDocumento',
					hidden : true,
					handler: function(){
						generaZipMonitor(gbl_pantalla);
					}
				}
			]
		}
	});

//-----------------------------------------------------------------------------
	var fpCausasElim = new Ext.form.FormPanel({
		id: 'fpCausasElim',
		width: 400,
		//hidden:true,
		style: 'margin:0 auto;',
		title: 'Ingresa causas de eliminaci�n',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 1,
		defaultType: 'textfield',
		monitorValid: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'textarea',
				name: 'causasEliminacion',
				id:'causasEliminacion1',
				enableKeyEvents: true,
				maxLength: 400,
				allowBlank: false,
				listeners:{
					keypress: function(field, event){							
						var val = field.getValue();
						if(event.keyCode !=8 && event.keyCode !=46){//ignorar backspace and delete
							if (val.length >= 400 ){
								event.stopEvent();
							}
						}
					}
				}
			}
		],
		buttons: [
			{
				text: 'Cerrar',
				iconCls: 'icoRechazar',
				id:'btnConsultar',
				handler: function() {
					Ext.getCmp('causasEliminacion1').setValue('');
					Ext.getCmp('winDetValidaMonitorCausas').hide();
				}
			},
			{
				text: 'Aceptar',
				hidden: false,
				formBind: true,
				iconCls: 'icoAceptar',
				handler: function() {
					fnEliminarDet();
				}
				
			}
		]
	});
	
	var PanelFoma = new Ext.FormPanel({
		id: 'PanelFoma',
		width: 600,
		style: ' margin:0 auto;',	
		hidden: true,
		bodyBorder: false, 
		border: false, 
		hideBorders: true
	});
	var pnlResumenValida = new Ext.Panel({
		id: 'pnlResumenValida',
		height: 'auto',
		style: 'margin:0 auto;',
		border: false,
		width: 630,
		frame:true
	});
	
	var pnlAcuse = new Ext.Panel({
		id: 'pnlAcuse',
		height: 'auto',
		style: 'margin:0 auto;',
		border: false,
		width: 630,
		frame:true,
		hidden:true,
		items:[gridAcuse]
	});
	
	var pnlCabecera = new Ext.Panel({
		name: 'msjProgama',
		id: 'msjProgama1',
		style:	'margin:0 auto;',
		title: '<p align="center">DETALLE DE CR�DITO A VALIDAR</p>',
		width: 618,
		frame: true,
		hidden: false,
		html:'<p align="center">Informaci�n Resumen <br>'+
			 'IF : FIDE <br>'+
			 'Fecha de vencimiento: '+ini_fec_venc+' <br>'+
			 'Nombre del Validador: '+ini_nombreUsuario+' <br>'+
			 'Fecha y Hora de Validaci�n: '+ini_fecActual+
			 '</p>'
	});
	
	var textoPrograma = new Ext.Container({
		layout: 'table',		
		id: 'textoPrograma',							
		width:	'600',
		heigth:	'auto',
		hidden: false,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensaje', 	value: 'Programa: '+ini_hidDescProgFondoJR }				
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		layoutConfig: {
			align:'center'
		},
		items: [
			PanelFoma,
			textoPrograma,
			NE.util.getEspaciador(20),
			pnlResumenValida,
			pnlAcuse
			//gridCabeceraVenc,
			//gridValidaInfo,
			//gridResumenValida
		]
	});
	
	
	Ext.Ajax.request({
		url: '33fondojrmonitor_ext.data.jsp',
		params:{
			informacion: 'validaMonitor',
			estatus: ini_estatus,
			fec_venc: ini_fec_venc
		},
		callback: processSuccessFailureValidaMonitor
	});

});