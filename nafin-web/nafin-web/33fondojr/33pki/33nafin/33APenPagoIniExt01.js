Ext.onReady(function() {
/*--------------------------------- HANDLERS -------------------------------*/
	var ini_hidDescProgFondoJR = Ext.getDom('hidDescProgFondoJR').value;
	var validaNum = false;
	var processSuccessConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var grid = Ext.getCmp('gridConsulta');
			if (!grid.isVisible()) {
				grid.show();
			}
			var jsonData = store.reader.jsonData;
			var capitalTotal = jsonData.CAPITAL_TOTAL; 
			var interesTotal = jsonData.INTERESES_TOTAL; 
			var pagarTotal = jsonData.TOTAL_A_PAGAR_TOTAL; 
			var btnImprimirCSV = Ext.getCmp('btnGenerarArchivoCSV');	
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');	
			var el = grid.getGridEl();	
			if(store.getTotalCount() > 0) {			
				btnImprimirCSV.enable();
				Ext.getCmp('gridTotales').show();	
				btnGenerarPDF.enable();	
				el.unmask();
				consTotales.load({
					params: Ext.apply(fp.getForm().getValues(),{					
						informacion: 'consTotales'						
					})
				});
			} else {
				Ext.getCmp('gridTotales').hide();
				btnImprimirCSV.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}	
	}
	function mostrarArchivoCSV(opts, success, response) {
		Ext.getCmp('btnGenerarArchivoCSV').enable();
		var btnImprimirCSV = Ext.getCmp('btnGenerarArchivoCSV');
		btnImprimirCSV.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}		
	function mostrarArchivoPDF1(opts, success, response) {
		Ext.getCmp('btnGenerarPDF').enable();
		var btnImprimirPDF = Ext.getCmp('btnGenerarPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function trim(str){
		var cadena = str;
		// Si la cadena proporcionada es NULL no realizar ninguna operacion
		if (str == null ){
			return str;
		}
		// Suprimir los caracteres "vacios" al inicio y al final
		cadena = cadena.replace(/^\s+/, "");
		cadena = cadena.replace(/\s+$/, "");
		return cadena;
	}
	function formateaNumPrestamos(textarea){
		var MAX_LINES = 300;
		// <%-- Convertir espacios en saltos de linea --%>
		var prestamos = trim(textarea.getValue());
		if(prestamos == ""){
			textarea.setValue(prestamos);
			return;
		}
		textarea.setValue(prestamos.replace(/\s+/g, "\n"));	 
		// <%-- Limitar numero de registros hasta 300 --%>
		var lineas = textarea.getValue().replace(/\r/g,"").split('\n');
		if(lineas.length > MAX_LINES){//markInvalid
			textarea.setValue(lineas.slice(0,MAX_LINES).join('\n'));	
			textarea.markInvalid("S�lo se permiten como m�ximo, "+MAX_LINES+" registros.");
			validaNum = true;
		}else{
			validaNum = false;
		}
	}
//-------------------------------- STORES -----------------------------------
//---------------------------------------------------------------------------
	var storeGridCosulta = new Ext.data.JsonStore({
		url : '33APenPagoIniExt01.data.jsp',
		id: 'storeGridCosulta',
		baseParams:{
			informacion: 'Consultar'
		},
		root : 'registros',
		fields: [
			{name: 'FECHA_AMORTIZACION_NAFIN'},
			{name: 'PRESTAMO'},
			{name: 'CLIENTE'},
			{name: 'NUM_CUOTAS_A_PAGAR'},
			{name: 'DIAS_TRANSCURRIDOS_VENC'},
			{name: 'CAPITAL'},
			{name: 'INTERESES'},
			{name: 'TOTAL_A_PAGAR'},
			{name: 'ORIGEN'},
			{name: 'FECHA_AMORT_MAS_ANTIGUA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: processSuccessConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					processSuccessConsultaData(null, null, null);
				}
			}
		}
	});
	var consTotales  = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '33APenPagoIniExt01.data.jsp',
		baseParams: {
			informacion: 'consTotales'
		},		
		fields: [			
			{	name: 'CAPITAL'},
			{	name: 'INTERESES'},			
			{	name: 'TOTAL_A_PAGAR'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);					
				}
			}
		}		
	});
//--------------------------------ELEMENTOS---------------------------------------
//--------------------------------------------------------------------------------
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',
		store: storeGridCosulta,
		columns: [
			{
				header: 'Fecha Amortizaci�n Nafin',
				tooltip: 'Fecha Amortizaci�n Nafin',
				dataIndex: 'FECHA_AMORTIZACION_NAFIN',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'Pr�stamo',
				tooltip: 'Pr�stamo',
				dataIndex: 'PRESTAMO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: ' Cliente',
				tooltip: ' Cliente',
				dataIndex: 'CLIENTE',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				align : 'left'
			},
			{
				header: 'N�m. Cuota a Pagar',
				tooltip: 'N�m. Cuota a Pagar',
				dataIndex: 'NUM_CUOTAS_A_PAGAR',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			},
			{
				header: 'D�as Transcurridos de Vencimiento',
				tooltip: 'D�as Transcurridos de Vencimiento',
				dataIndex: 'DIAS_TRANSCURRIDOS_VENC',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align :'center'
			},
			{
				header: 'Capital',
				tooltip: 'Capital',
				dataIndex: 'CAPITAL',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Inter�s',
				tooltip: 'Inter�s',
				dataIndex: 'INTERESES',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align :'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			
			},
			{
				header: 'Total a Pagar',
				tooltip: 'Total a Pagar',
				dataIndex: 'TOTAL_A_PAGAR',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Origen',
				tooltip: 'Origen',
				dataIndex: 'ORIGEN',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				align : 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 360,
		hidden : true,
		align: 'center',
		width: 820,
		style: 'margin:0 auto;',
		title: 'Consulta',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
                        store: storeGridCosulta,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No se encontro ning�n registro",
			items: [
				'->',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivoCSV',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url:'33APenPagoIniExt01.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{					
								informacion:'GenerarArchivoCSV',
								tipo:'CSV'
							}),
							callback: mostrarArchivoCSV
						});		
							
					}
				},				
				{
					xtype: 'button',
					text: 'Imprimir PDF',
					iconCls:	'icoPdf',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '33APenPagoIniExt01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: mostrarArchivoPDF1
						});
					}
				}	
			]
		}	
	});
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consTotales,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Total:',
		clicksToEdit: 1,
		hidden: true,		
		columns: [
			{
				header: 'Capital',
				tooltip: 'Capital',
				dataIndex: 'CAPITAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'Inter�s',
				tooltip: 'Inter�s',
				dataIndex: 'INTERESES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'Total a Pagar',
				tooltip: 'Total a Pagar',
				dataIndex: 'TOTAL_A_PAGAR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 100,
		width: 460,
		align: 'center',
		frame: false
	});
	var elementosForm =[
		{
			xtype: 'compositefield',
			fieldLabel: 'N�m Pr�stamo',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textarea',
					name: 'numPrestamo',
					id: 'txtNumPrestamo',
					allowBlank: true,
					hidden: false,
					msgTarget: 'side',
					width: 150,
					height: 100
			
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Cr�dito con Vencimientos',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'displayfield',
					value: 'Desde',
					width: 40,
					margins: '0 0 0 0'
				},
				{
					xtype: 'textfield',
					name: 'diaMinimo',
					id: 'tfDiaMinimo',
					allowBlank: true,
					maxLength: 3,
					width: 50,
					msgTarget: 'side',
					margins: '0 0 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'd�as',
					width: 30,
					margins: '0 0 0 0'
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 25,
					margins: '0 0 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'hasta',
					width: 30,
					margins: '0 0 0 0'
				},
				{
					xtype: 'textfield',
					name: 'diaMaximo',
					id: 'tfDiaMaximo',
					fieldLabel: 'hasta',
					allowBlank: true,
					maxLength: 3,
					width: 50,
					msgTarget: 'side',
					margins: '0 0 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'd�as',
					width: 30,
					margins: '0 0 0 0'
				}
			]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: "Amortizaciones Pendientes de Pago",
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		style: 'margin:0 auto;',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForm,
		monitorValid: true,
		buttons: [
			{
				xtype:'button',
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var numero = Ext.getCmp('txtNumPrestamo');
					var diaMinimo = Ext.getCmp('tfDiaMinimo');
					var diaMaximo = Ext.getCmp('tfDiaMaximo');
					var numPrestamo = Ext.getCmp('txtNumPrestamo');
					//formateaNumPrestamos
					
					if(!Ext.isEmpty(numPrestamo.getValue())){
						formateaNumPrestamos(numPrestamo);
						if(validaNum==true){
							return;
						}
					}
					if(!Ext.isEmpty(diaMinimo.getValue())&&!isdigit(diaMinimo.getValue())){
						diaMinimo.markInvalid('El parametro de d�as de vencimiento inicial no es v�lido.');
						return;
					}
					if(!Ext.isEmpty(diaMaximo.getValue())&&!isdigit(diaMaximo.getValue())){
						diaMaximo.markInvalid('El parametro de d�as de vencimiento final no es v�lido.');
						return;
					}
					if(!Ext.isEmpty(diaMinimo.getValue())&&!Ext.isEmpty(diaMaximo.getValue())){
						if( diaMinimo.getValue() > diaMaximo.getValue()){
							alert("El d�a de vencimiento inicial no puede ser mayor al d�a de vencimiento final.");
							return;
						}
					}
					
				fp.el.mask('Buscando...', 'x-mask-loading');
					storeGridCosulta.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start: 0,
							limit: 15
						})
					});
				}
			}
		]
	});
	var textoPrograma = new Ext.Container({
		layout: 'table',		
		id: 'textoPrograma',							
		width:	'350',
		heigth:	'auto',
		hidden: false,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensaje', align :'center',	value: ini_hidDescProgFondoJR }				
		]
	});
	//-------------------------------- PRINCIPAL -----------------------------------
	//--------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 960,
		style: 'margin:0 auto;',
		height: 'auto',
		align:'center',
		renderHidden: true,
		layoutConfig: {
			align:'center'
		},
		items: [
			NE.util.getEspaciador(10),
			textoPrograma,
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),
			gridTotales
		]
	});
});