<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<%@ include file="/extjs4.jspf" %>
<%if(esEsquemaExtJS) {%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<%-- El jsp que contiene el dise�o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>

<script language="JavaScript" src="/nafin/00utils/valida.js"></script>
<script type="text/javascript" src="/nafin/33fondojr/33pki/33nafin/33fondojraltareembolsos_ext.js"></script>

</head>


<%if(esEsquemaExtJS) {%>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<div id="_ObjFirma" style="display:none;">
	<%@ include file="/00utils/componente_firma4.jspf" %>
	</div>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">	
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
				<div id="areaContenido"></div>                                                                                     
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
		
		<!-- Valores iniciales recibidos como parametros-->
		<input type="hidden" id="hidDescProgFondoJR" value="<%=descProgramaFondoJR%>">
		<input type="hidden" id="hidCvePrograma" value="<%=cveProgramaFondoJR%>">

	</body>
<%}else  {%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id="_ObjFirma" style="display:none;">
<%@ include file="/00utils/componente_firma4.jspf" %>
</div>
			<div id="areaContenido"><div style="height:190px"></div></div>

<form id='formAux' name="formAux" target='_new'></form>
<!-- Valores iniciales recibidos como parametros-->
<input type="hidden" id="hidDescProgFondoJR" value="<%=descProgramaFondoJR%>">
<input type="hidden" id="hidCvePrograma" value="<%=cveProgramaFondoJR%>">

<%}%>
</body>
</html>