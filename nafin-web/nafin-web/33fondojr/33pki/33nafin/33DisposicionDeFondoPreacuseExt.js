
Ext.onReady(function() {
	var dias_semana = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
	var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre", "Diciembre");
	var fecha_actual = new Date(); 
	var fechaHoy = fecha_actual.getDate() + " " + meses[fecha_actual.getMonth()] + " " + fecha_actual.getFullYear();
//------------------------------------------------------------------------------

	var procesarSuccessConsultar =  function(opts, success, response) {
		pnl.el.unmask();
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			storeRegDispData.loadData(resp);
	
		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	
	var procesarCapturaData = function(store, arrRegistros, opts) 	{
		pnl.el.unmask();
		var el = gridDispFondo.getGridEl();		
		if (arrRegistros != null) {	
			var jsonData = store.reader.jsonData;
			
			if(store.getTotalCount() > 0) {
				el.unmask();
				gridDispFondo.show();
				//pnlPreAcuse.show();
				labelPreAcuse.html= '<p align="center"><b>Preacuse Disposici�n de Fondo por falta de Archivo</b></p>';
				pnlPreAcuse.add(labelPreAcuse);
				pnlPreAcuse.html= '<table border="1"  bordercolor="#A5B8BF" style="background:#FFFFFF;">'+
										'<tr><td class="celda01" align="center" >&nbsp;</td><td class="celda01" align="center">Moneda Nacional</td></tr>'+
										'<tr><td>Total de Registros por Disposici�n</td><td align="center">'+jsonData.totalRegDisp+'</td></tr>'+
										'<tr><td>Monto Total de Vencimientos no Pagados</td><td align="center">'+jsonData.montoTotalRegDisp+'</td></tr>'+
										'<tr><td colspan="2">Al transmitirse este MENSAJE DE DATOS, usted esta bajo su responsabilidad '+
																 'haciendo un movimiento contable al fondo, aceptando de esta forma la responsabilidad '+
																 'de los movimientos registrados del mismo, dicha transmisi�n tendr� validez para todos los efectos legales.'+
												'</td>'+
										'</tr>'+
										'</table>';
				
				//labelFechaHoy.html= 'Fecha de Operaci�n: '+fechaHoy;
				gridDispFondo.setTitle('Fecha de Operaci�n: '+fechaHoy+'<center>No Pagado</center>');
				pnl.insert(1,pnlPreAcuse)
				pnl.insert(4, labelFechaHoy);
				pnl.doLayout();
				
			} else {
				Ext.getCmp('btnAutorizacion').hide();
				Ext.getCmp('btnCancelar').hide();
				Ext.getCmp('btnRegresarDisp').show();
				gridDispFondo.show();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}	
	
	var realizaConfirmacion = function(pkcs7, textoFirmar, btn ){
	
		if (Ext.isEmpty(pkcs7)) {
			btn.enable();
			return;	//Error en la firma. Termina...
			
		}else{			
		
			Ext.Ajax.request({
				url: '33DisposicionDeFondoPreacuseExt.data.jsp',
				params: {
					informacion: 'DisposicionDeFondoAutorizacion',
					Pkcs7: pkcs7,
					TextoFirmado: textoFirmar
				},
				callback: procesarSuccessConfirmacion
			});
		}
	}
	
	var fnAutorizacionDisp = function(btn){
		btn.disable();
		var textoFirmar = "E P O-DATOS|Total Monto Documento|Total Monto Descuento|Total Monto de Inter�s|Total Importe a Recibir\n"+
						"Al solicitar el factoraje electr�nico o descuento electr�nico del documento que selecciono e identifico, transmito los derechos"+ 
						" que sobre el mismo ejerzo, de acuerdo con lo establecido por los art�culos 427 de la ley general de t�tulos y operaciones de cr�dito,"+ 
						" 32 c del c�digo fiscal de la federaci�n y 2038 del c�digo civil federal, haci�ndome sabedor de su contenido y alcance, condicionado a"+
						" que se efect�e el descuento electr�nico o el factoraje electr�nico";
						
				
		NE.util.obtenerPKCS7(realizaConfirmacion, textoFirmar, btn );
		
	}
	

	
	var procesarSuccessConfirmacion = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var acuseRecibo = resp.acuseRecibo;
			if(resp.authRespOK){
				
				Ext.getCmp('btnImprimirPDF').show();
				Ext.getCmp('btnGenerarArch').show();
				Ext.getCmp('btnRegresarDisp').show();
				Ext.getCmp('btnCancelar').hide();
				Ext.getCmp('btnAutorizacion').hide();
				
				labelAcuse.html= '<p align="center"><b>Acuse.- La autentificaci�n se llev� a cabo con �xito<br>Recibo: '+acuseRecibo+'</b></p>';
				labelPreAcuse.hide();
				pnlPreAcuse.insert(0,labelAcuse);
				pnlPreAcuse.doLayout();
				window.scrollTo(0, 0);
			}else{
				Ext.MessageBox.alert('Aviso','<b>La autentificaci�n no se llev� a cabo. PROCESO CANCELADO</b>');
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var generaArchivoAcuse = function(btn, tipo_archivo){
		Ext.Ajax.request({
			url: '33DisposicionDeFondoPreacuseExt.data.jsp',
			params: {
				informacion: 'GeneraArchivosDisp',
				tipo_archivo: tipo_archivo
			},
			callback: function(opts, success, response) {
				if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
					var resp = 	Ext.util.JSON.decode(response.responseText);
					var archivo = resp.urlArchivo;				
					archivo = archivo.replace('/nafin','');
					var params = {nombreArchivo: archivo};				
					fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					fp.getForm().getEl().dom.submit();
					btn.setIconClass('');
					btn.enable();
		
				} else {
					NE.util.mostrarConnError(response,opts);
				}
			}
		});
	}

//------------------------------------------------------------------------------

	var storeRegDispData = new Ext.data.JsonStore({
		root : 'dispfondoTemp',
		url : '33DisposicionDeFondoPreacuseExt.data.jsp',
		baseParams: {
			informacion: 'DisposicionDeFondoInicializar'		
		},
		fields: [
			{name: 'fechaautorizacion'},
			{name: 'fechaoperacion'},
			{name: 'fechafide'},
			{name: 'prestamo'},
			{name: 'numsirac'},
			{name: 'cliente'},
			{name: 'numcuota'},
			{name: 'capital'},
			{name: 'interes'},
			{name: 'totalpagar'},
			{name: 'registrostotal'},
			{name: 'capitaltotal'},
			{name: 'interestotal'},
			{name: 'sumatotalpagar'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarCapturaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarCapturaData(null, null, null);					
				}
			}
		}					
	});

//------------------------------------------------------------------------------
	var gridDispFondo = new Ext.grid.GridPanel({
		id: 'gridDispFondo',
		title: '<center>No Pagados</center>',
		store: storeRegDispData,
		hidden: true,
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 300,
		style: 'margin:0 auto;',
		width: 700,
		frame: true,
		columns: [	
			{
				header: '<center>Fecha de Autorizaci�n</Center>', dataIndex: 'fechaautorizacion', sortable: true, width: 120, resizable: true, hidden: false, align: 'center'
			},
			{
				header: '<center>Fecha Operaci�n</center>', tooltip: 'Fecha Operaci�n', dataIndex: 'fechaoperacion', sortable: true, hideable: false, width: 100, align: 'center'
			},
			{
				header: '<center>Fecha Pago Cliente FIDE</center>', tooltip: 'Fecha Pago Cliente FIDE', dataIndex: 'fechafide', sortable: true, hideable: false, width: 100, align: 'center'
			},
			{
				header: '<center>Pr�stamo</center>', tooltip: 'Pr�stamo', dataIndex: 'prestamo', sortable: true, hideable: false, width: 100, align: 'center'
			},
			{
				header: '<center>Num. Cliente SIRAC</center>', tooltip: 'Num. Cliente SIRAC', dataIndex: 'numsirac', sortable: true, hideable: false, width: 100, align: 'center'
			},
			{
				header: '<center>Cliente</center>', tooltip: 'Cliente', dataIndex: 'cliente', sortable: true, hideable: false, width: 100, align: 'center'
			},
			{
				header: '<center>Num. Cuota a Pagar</center>', tooltip: 'Num. Cuota a Pagar', dataIndex: 'numcuota', sortable: true, hideable: false, width: 100, align: 'center'
			},
			{
				header: '<center>Capital</center>', tooltip: 'Capital', dataIndex: 'capital', sortable: true, hideable: false, width: 100, align: 'center'
			},
			{
				header: '<center>Inter�s</center>', tooltip: 'Inter�s', dataIndex: 'interes', sortable: true, hideable: false, width: 100, align: 'center'
			},
			{
				header: '<center>Intereses Moratorios</center>', tooltip: 'Intereses Moratorios', dataIndex: '', sortable: true, hideable: false, width: 100, align: 'center'
			},
			{
				header: '<center>Total a Pagar</center>', tooltip: 'Total a Pagar', dataIndex: 'totalpagar', sortable: true, hideable: false, width: 100, align: 'center'
			},
			{
				header: '<center>Estatus del Registro</center>', tooltip: 'Estatus del Registro', dataIndex: '', sortable: true, hideable: false, width: 100, align: 'center',
				renderer:  function (causa, columna, registro){
					return 'No pagadas';
				}
			},
			{
				header: '<center>Origen</center>', tooltip: 'Origen', dataIndex: '', sortable: true, hideable: false, width: 100, align: 'center',
				renderer:  function (causa, columna, registro){
					return 'No Definido';
				}
			},
			{
				header: '<center>Estatus Validaci�n</center>', tooltip: 'Estatus Validaci�n', dataIndex: '', sortable: true, hideable: false, width: 100, align: 'center',
				renderer:  function (causa, columna, registro){
					return 'En proceso';
				}
			}
		],
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				{
					text: 'Autorizaci�n Disposici�n de Fondo',
					id: 'btnAutorizacion',
					handler: fnAutorizacionDisp
				},
				'-',
				{
					text: 'Cancelar',
					id: 'btnCancelar',
					handler: function(btn){
						window.location.href="33FondoJuniorValidacionExt.jsp";
					}
					//hidden: true
					//handler: 
				},
				{
					text: 'Imprimir PDF',
					id: 'btnImprimirPDF',
					hidden: true,
					handler: function(btn){
						btn.setIconClass('loading-indicator');
						btn.disable();
						generaArchivoAcuse(btn, "ACUSE_PDF");
					}
				},
				{
					text: 'Generar Archivo',
					id: 'btnGenerarArch',
					hidden: true,
					handler: function(btn){
						btn.setIconClass('loading-indicator');
						btn.disable();
						generaArchivoAcuse(btn, "ACUSE_CSV");
					}
				},
				{
					text: 'Regresar',
					id: 'btnRegresarDisp',
					hidden: true,
					handler: function(){
						window.location.href="33FondoJuniorValidacionExt.jsp";
					}
				}
				
			]
		}
	});
	
	var labelAcuse = new Ext.form.Label({
		id: 'labelAcuse',
		width: '500',
		style: 'margin:0 auto;'
	});
	
	var labelPreAcuse = new Ext.form.Label({
		id: 'labelPreAcuse',
		width: '500',
		style: 'margin:0 auto;'
	});
	
	var labelFechaHoy = new Ext.form.Label({
		id: 'labelFechaHoy',
		width: 700,
		frame: true,
		style: 'margin:0 auto;'
	});

	var pnlPreAcuse = new Ext.Panel({
		id: 'pnlPreAcuse',
		width: '500',
		frame: true,
		style: 'margin:0 auto;'
		//html: 'seeee'
		//hidden: true
	})
//------------------------------------------------------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: 'margin:0 auto;',
		title: 'Validaci�n',
		frame: true,		
		hidden: true
	});
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		layoutConfig: {
			align:'center'
		},
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridDispFondo,
			NE.util.getEspaciador(20)
			
		]
	});

//------------------------------------------------------------------------------
	/*Ext.Ajax.request({
		url: '33DisposicionDeFondoPreacuseExt.data.jsp',
		params: {
			informacion: 'fondoJuniorValidacionInicializar'
		},
		callback: procesarSuccessConsultar
	});
	*/
	pnl.el.mask('Consultando...','x-loading');
	storeRegDispData.load();
});