<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.text.SimpleDateFormat,		
		netropology.utilerias.*,	
		javax.naming.*,
		com.netro.fondosjr.*,	
		com.netro.fondojr.*,			
		com.netro.model.catalogos.*,
		 java.util.Map.*,
		 com.netro.pdf.*,
		 java.net.*, 
		 java.text.*,	
		 com.netro.xls.*,
		 com.netro.exception.*,
		 net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/33fondojr/33secsession_extjs.jspf"%>
<%@ include file="../certificado.jspf" %>  
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String fec_venc_ini = (request.getParameter("fec_venc_ini")!=null)?request.getParameter("fec_venc_ini"):"";
String fec_venc_fin = (request.getParameter("fec_venc_fin")!=null)?request.getParameter("fec_venc_fin"):"";
String estatusCarga = (request.getParameter("estatusCarga")!=null)?request.getParameter("estatusCarga"):"N";
String ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
String chkDesembolso = (request.getParameter("chkDesembolso")!=null)?request.getParameter("chkDesembolso"):"N";
String dirige = (request.getParameter("dirige")!=null)?request.getParameter("dirige"):"";
String ver_enlace = (request.getParameter("ver_enlace")!=null)?request.getParameter("ver_enlace"):"";
String estatus = (request.getParameter("estatus")!=null)?request.getParameter("estatus"):"";
String proc = (request.getParameter("proc")!=null)?request.getParameter("proc"):"";
String estatus2 = (request.getParameter("estatus2")!=null)?request.getParameter("estatus2"):"";
String clavePrograma = (String)request.getSession().getAttribute("cveProgramaFondoJR");
String fec_venc = (request.getParameter("fec_venc")!=null)?request.getParameter("fec_venc"):"";
String textoFirmar = (request.getParameter("textoFirmar") != null) ? request.getParameter("textoFirmar") : "";
String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
String folio = (request.getParameter("folio") != null) ? request.getParameter("folio") : "";
String usuario = (request.getParameter("usuario") != null) ? request.getParameter("usuario") : "";
String pantalla = (request.getParameter("pantalla")!=null)?request.getParameter("pantalla"):"";
String folio_val = (request.getParameter("folio_val")!=null)?request.getParameter("folio_val"):"";
			
String fec_Actual = fHora.format(new java.util.Date());	
int start = 0, limit = 0;
String infoRegresar ="", sumaTotalVista	= "0", montoTotalVista= "0.00", sumaTotalPagado	= "0", montoTotalPagado	= "0.00", sumaTotalNoPagado= "0", montoTotalNoPagado	= "0.00",
	sumaTotalNoPagadoR	= "0", montoTotalNoPagadoR	= "0.00", sumaTotalTmp 	= "0", montoTotalTmp	= "0.00", sumaTotalReemb= "0", montoTotalReemb	= "0.00",
	sumaTotalRec	= "0", montoTotalRec	= "0.00", sumaTotalNoEncon	= "0", montoTotalNoEncon = "0.00", sumaTotalNoEnv	= "0", montoTotalNoEnv		= "0.00",
	sumaTotalError = "0", montoTotalError		= "0.00", sumaTotalPrepago	= "0", montoTotalPrepago = "0.00",
	mensaje ="",ultimo_reg = "",  montoTotal = "0", sumaTotal = "", montoReemb = "0", sumaReemb = "", montoAntic = "0", sumaAntic = "",
  consulta ="", mensajeError ="S", mensajeE ="";
  
JSONObject jsonObj = new JSONObject();
StringBuffer contenidoArchivo = new StringBuffer();
List registros	= new ArrayList();
List datos	= new ArrayList();	
HashMap datosExt = new HashMap();
JSONArray registrosExt = new JSONArray();

FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

if(informacion.equals("valoresIniciales")) {

String programa = "</table align='center'>"+
						" <tr> "+
						"<td class='formas' colspan='1' align='left'><b>Programa: "+descProgramaFondoJR+"</b></td>"+
						"</tr>"+
						"</table>";
				
jsonObj.put("success", new Boolean(true));
jsonObj.put("programa", programa);	
infoRegresar =jsonObj.toString();

}else if(informacion.equals("CatalogoIF")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setTabla("comcat_if");
	cat.setOrden("ic_if");
	cat.setValoresCondicionIn("103", Integer.class);
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("Consultar") ) {

	int numReemb = fondoJunior.numReembTotales(clavePrograma,"","");

	String masdequinientos ="F";
	if(numReemb<=500){ 
		masdequinientos = "F";
	}else{
		masdequinientos = "V";
	}		
	System.out.println("clavePrograma "+clavePrograma);
	System.out.println("fec_venc_ini "+fec_venc_ini);
	System.out.println("fec_venc_fin "+fec_venc_fin);
	System.out.println("chkDesembolso "+chkDesembolso);
	System.out.println("estatusCarga "+estatusCarga);
	System.out.println("operacion "+operacion);
	registrosExt = new JSONArray();	
	
	ConsultaMonitorFondoJR paginador = new ConsultaMonitorFondoJR();
	paginador.setCvePrograma(clavePrograma);
	paginador.setFec_venc_ini(fec_venc_ini);
	paginador.setFec_venc_fin(fec_venc_fin);
	paginador.setChkDesembolso(chkDesembolso);
	paginador.setEstatusCarga(estatusCarga);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	try {
		if (operacion.equals("Generar")) {	
		
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		
			//Nueva consulta
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				
			Registros rs = queryHelper.getPageResultSet(request,start,limit);
				while(rs.next()){		
					datosExt = new HashMap();
					datosExt.put("MESTATUS",rs.getString("MESTATUS"));	
					if(  (rs.getString("MESTATUS").equals("BRC")) || (rs.getString("MESTATUS").equals("BT"))  ) 
						datosExt.put("MTIPO_REG",rs.getString("MTIPO_REG"));
					else 
					datosExt.put("MTIPO_REG",rs.getString("MFECHA_VENC"));				
					datosExt.put("MFECHA_VENC",rs.getString("MFECHA_VENC"));				
					datosExt.put("MULTIMO_REG",rs.getString("MULTIMO_REG"));
					datosExt.put("MFOLIO",rs.getString("MFOLIO"));
					datosExt.put("MTOTAL_REG",rs.getString("MTOTAL_REG"));
					datosExt.put("MMONTO_TOT",rs.getString("MMONTO_TOT"));
					datosExt.put("MESTATUS_PROC",rs.getString("MESTATUS_PROC"));	
					datosExt.put("MASREQUERIMIENTO",masdequinientos);			
					registrosExt.add(datosExt);
				}
			
		  consulta =  "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize()  + "\", \"registros\": " + registrosExt.toString()+"}";
		  jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString();
		}
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
		 
		
}else if (informacion.equals("ArchivoCSV")  ) {
	 
	CreaArchivo creaArchivo	= new CreaArchivo();	
	String nombreArchivo ="";
	registros	= new ArrayList();
	datos	= new ArrayList();
	registros = fondoJunior.consultaMonitor(fec_venc_ini, fec_venc_fin, estatusCarga, chkDesembolso, clavePrograma);
	
	if(registros!=null && registros.size()>0){
		for(int x=0;x<registros.size();x++){
			datos = (List)registros.get(x);
			if(x==0){
				contenidoArchivo.append("Programa:,"+descProgramaFondoJR+"\n");
				contenidoArchivo.append("Fecha de Vencimiento Nafin,");
				contenidoArchivo.append("Fecha y Hora del Ultimo Registro Enviado por FIDE,");
				contenidoArchivo.append("Folio de Operacion,");
				contenidoArchivo.append("Total de Registros,");
				contenidoArchivo.append("Monto");
				contenidoArchivo.append("\n");
			}
			contenidoArchivo.append((datos.get(1)==null?"":(String)datos.get(1))+",");
			contenidoArchivo.append((datos.get(2)==null?"":(String)datos.get(2))+",");
			if("NPR".equals(estatusCarga))
				contenidoArchivo.append("N/A,");
			else
				contenidoArchivo.append((datos.get(3)==null?"":(String)datos.get(3))+",");
				contenidoArchivo.append((datos.get(4)==null?"":(String)datos.get(4))+",");
				contenidoArchivo.append((datos.get(5)==null?"":(String)datos.get(5)));
				contenidoArchivo.append("\n");
			}
		}
		
		creaArchivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv");
		nombreArchivo = creaArchivo.getNombre();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		infoRegresar = jsonObj.toString();
			
}else if (informacion.equals("ConsultaDetalle")  ) {
	
	registros = fondoJunior.consDetMonitor(estatusCarga ,fec_venc,clavePrograma);//FODEA 026-2010 fvr
	for(int x=0; x<registros.size();x++){
		datos = (List)registros.get(x);
		String fechaVen=!datos.get(0).equals("")?(String)datos.get(0):"";
		String prestamo = !datos.get(1).equals("")?(String)datos.get(1):"";
		String noCliente =  !datos.get(2).equals("")?(String)datos.get(2):"";
		String montoAmor= !datos.get(4).equals("")?(String)datos.get(4):"";
		String montoInteres=!datos.get(5).equals("")?(String)datos.get(5):"";
		String TotalVen =!datos.get(6).equals("")?(String)datos.get(6):"";
		String FechaPago =!datos.get(7).equals("")?(String)datos.get(7):"";
		String estatus1 =!datos.get(8).equals("")?(String)datos.get(8):"";
		String fechaPerio= !datos.get(9).equals("")?(String)datos.get(9):"";
		String noDocto =!datos.get(10).equals("")?(String)datos.get(10):"";
		String fechaFide =!datos.get(11).equals("")?(String)datos.get(11):"";
				
		datosExt = new HashMap();
		datosExt.put("FECHA_VEN_NA", fechaVen);
		datosExt.put("NO_PRESTAMO", prestamo);
		datosExt.put("NO_CLIENTE", noCliente);
		datosExt.put("MONTO_AMORTIZACIO", montoAmor);
		datosExt.put("MONTO_INTERES", montoInteres);
		datosExt.put("TOTAL_VENC", TotalVen);
		datosExt.put("FECHA_PAGO", FechaPago);
		datosExt.put("ESTATUS", estatus1);
		datosExt.put("FECHA_PERIODO", fechaPerio);
		datosExt.put("NO_DOCTO", noDocto);
		datosExt.put("FECHA_REGISTRO", fechaFide);
		registrosExt.add(datosExt);	
	}
	consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();


}else if (informacion.equals("procesoGenerarZIP")   ) {
	
	//Eliminar lo que hay en session para no mostrar el ZIP descargado anteriomente
	session.removeAttribute("GeneraArchivoGralZipThread"); 
	//Crear una nueva session para mostrar el nuevo ZIP
	GeneraArchivoGralZipThread generarZip = new GeneraArchivoGralZipThread();
	session.setAttribute("GeneraArchivoGralZipThread", generarZip );			
	generarZip.setRutaFisica(strDirectorioTemp);
	generarZip.setRutaVirtual(strDirectorioTemp);
	generarZip.setClavePrograma(clavePrograma);
	generarZip.setNombrePrograma(descProgramaFondoJR);
	generarZip.setPantalla(pantalla);
	generarZip.setEstatus_val(estatus);
	generarZip.setFolio_val(folio_val);
	generarZip.setFecVencimiento(fec_venc);
	if(chkDesembolso.equals("S")) {
		generarZip.setChkDesembolso(chkDesembolso);
	}
	generarZip.setRunning(true);
	new Thread(generarZip).start();
	session.setAttribute("generarZip", generarZip);
	infoRegresar= "({\"success\":true,\"isRunning\":true })"; 
		
}else  if (informacion.equals("SeguirLeyendo")   ) {
			
	GeneraArchivoGralZipThread generarZip = (GeneraArchivoGralZipThread)session.getAttribute("generarZip");
	if(generarZip.hasError()){
		mensaje="Error inesperado al generar archivo ZIP";
		jsonObj.put("mensaje", mensaje);
		infoRegresar= jsonObj.toString();		
	} 
	if(generarZip.getNombreArchivo()==null && generarZip.isRunning()){
		mensaje="Generando Archivo ZIP";	
		infoRegresar= "({\"success\":true,\"isRunning\":true })"; 
	}else if(!generarZip.isRunning() ){
		String archivo =strDirecVirtualTemp+generarZip.getNombreArchivo();		
		infoRegresar= "({\"success\": true,\"isRunning\":false, \"URLArchivoZip\":\""+archivo+"\"})"; 
	}	

}else if (informacion.equals("MostrarPreAcuse") ||  informacion.equals("consu_1Data") || informacion.equals("consu_2Data")   || informacion.equals("consu_3Data") 	|| informacion.equals("consu_4Data")  	|| informacion.equals("consu_5Data")  ) {
		
	int numReemb = 0;
	String ruta_jsp = "", masdequinRT ="", masdequinR ="", masdequinRNE ="", masdequinRCE ="", mostrar = "N";
	StringBuffer seguridad = new StringBuffer();
	registros	= new ArrayList();
	datos	= new ArrayList();
	
	if( fec_venc.equals("RC") || fec_venc.equals("T") ){	
		registros = fondoJunior.validaMonitorRCyT(fec_venc, clavePrograma);
		ruta_jsp = "successrt";
	}else if("R".equals(estatus)) {		
		ruta_jsp = "successr";	
		registros = fondoJunior.validaMonitorR(fec_venc, clavePrograma);
		numReemb = fondoJunior.numReembTotales(clavePrograma,"RembolsosPrepagosTotales","");
		if(numReemb<=500){
			masdequinRT= "F";
		}else{
			masdequinRT= "V";
		}
		numReemb = fondoJunior.numReembTotales(clavePrograma, "Reembolsos","");
		if(numReemb<=500){
			masdequinR="F";
		}else{
			masdequinR= "V";
		}
		numReemb = fondoJunior.numReembTotales(clavePrograma,"NoEncontrados","");
		if(numReemb<=500){
			masdequinRNE= "F";
		}else{
			masdequinRNE= "V";
		}
		numReemb = fondoJunior.numReembTotales(clavePrograma,"ConError","");
		if(numReemb<=500){
			masdequinRCE ="F";
		}else{
			masdequinRCE= "V";
		}
	}else{
		ruta_jsp = "success";		
		registros = fondoJunior.validaMonitorPyNP(fec_venc, clavePrograma);
	}
	
	HashMap perfiles = new HashMap();
	perfiles.put("PNAADMFJR","33FDOJRMONIT");
	perfiles.put("PGAADMFJR","33FDOJRMONIT");
	perfiles.put("PNAPAGFJR","33FDOJRMONIT");
		
	com.netro.seguridadbean.Seguridad BeanSegFacultad = null;
	Iterator itr = perfiles.entrySet().iterator();
	while (itr.hasNext()) {
		Map.Entry e = (Map.Entry)itr.next();		
		try {
			BeanSegFacultad.validaFacultad( (String) strLogin, (String) strPerfil, (e.getKey()).toString(), (e.getValue()).toString(), (String) session.getAttribute("sesCveSistema"), (String) request.getRemoteAddr(), (String) request.getRemoteHost() );
			mostrar = "S";					
		}catch(Exception ex){}		
	}
	
	if(registros!=null && registros.size()>0){	
		for(int x=0; x<registros.size(); x++){
			datos = (List)registros.get(x);
			if(datos.get(2)!=null)			
				System.out.println("(String)datos.get(2)>>>>"+(String)datos.get(2));							
			if(((String)datos.get(2)).equals("vista")){
				sumaTotalVista = (String)datos.get(0);
				montoTotalVista = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}else if(((String)datos.get(2)).equals("pagados")){
				sumaTotalPagado = (String)datos.get(0);
				montoTotalPagado = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}else if(((String)datos.get(2)).equals("no_pagados")){
				sumaTotalNoPagado = (String)datos.get(0);
				montoTotalNoPagado = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}else if(((String)datos.get(2)).equals("tmp")){
				sumaTotalTmp = (String)datos.get(0);
				montoTotalTmp = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}else if(((String)datos.get(2)).equals("reemb")){
				sumaTotalReemb = (String)datos.get(0);
				montoTotalReemb = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}else if(((String)datos.get(2)).equals("error")){
				sumaTotalError = (String)datos.get(0);
				montoTotalError = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}else if(((String)datos.get(2)).equals("recuperacion")){
				sumaTotalRec = (String)datos.get(0);
				montoTotalRec = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}else if(((String)datos.get(2)).equals("no_encontrados")){
				sumaTotalNoEncon = (String)datos.get(0);
				montoTotalNoEncon = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}else if(((String)datos.get(2)).equals("no_enviados")){
				sumaTotalNoEnv = (String)datos.get(0);
				montoTotalNoEnv = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}else if(((String)datos.get(2)).equals("prepagos")){
				sumaTotalPrepago = (String)datos.get(0);
				montoTotalPrepago = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}else if(((String)datos.get(2)).equals("no_pagados_rech")){//F027-2011 FVR
				sumaTotalNoPagadoR = (String)datos.get(0);
				montoTotalNoPagadoR = Comunes.formatoDecimal(datos.get(1)!=null?(String)datos.get(1):"0",2);
			}				
		}
	}
	seguridad.append("Informacion de Creeditos \n");
	seguridad.append("Fecha y Hora de Validacion: "+fec_Actual+"\n");
	seguridad.append("Fecha de Venc: "+fec_venc+" \n");
	seguridad.append("Total Reg: 	 	"+sumaTotalTmp+" \n");
	seguridad.append("Monto Total: 	$"+montoTotalTmp+" \n");
				
	if ( informacion.equals("consu_1Data") &&  ruta_jsp.equals("success")  ) {
		registrosExt = new JSONArray();		
		for(int t =0; t<1; t++) {		
			datosExt = new HashMap();				
			datosExt.put("DESCRIPCION", "Vencimientos del Día");			
			datosExt.put("NO_CREDITOS", sumaTotalVista );	
			datosExt.put("IMPORTE", montoTotalVista);		
			datosExt.put("VERDETALLE", "Vencimientos");	
			datosExt.put("FEC_VENC", fec_venc);	
			datosExt.put("ESTATUS", estatus);	
			registrosExt.add(datosExt);
		}
		consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
			
	}else if ( informacion.equals("consu_2Data")  &&  ruta_jsp.equals("success")   ) {			
		registrosExt = new JSONArray();		
		for(int t =0; t<1; t++) {		
			datosExt = new HashMap();				
			datosExt.put("DESCRIPCION", "Total de Registros Enviados");			
			datosExt.put("NO_CREDITOS", sumaTotalTmp );	
			datosExt.put("IMPORTE", montoTotalTmp);	
			datosExt.put("VERDETALLE", "Enviados");
			datosExt.put("FEC_VENC", fec_venc);	
			datosExt.put("ESTATUS", estatus);
			registrosExt.add(datosExt);
		}
		consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		
	}else if ( informacion.equals("consu_3Data") &&  ruta_jsp.equals("success")  ) {			
		registrosExt = new JSONArray();		
		for(int t =0; t<6; t++) {		
			datosExt = new HashMap();	
			if(t==0){
				datosExt.put("DESCRIPCION", "Créditos Pagados del Día");			
				datosExt.put("NO_CREDITOS", sumaTotalPagado );	
				datosExt.put("IMPORTE", montoTotalPagado);	
				datosExt.put("VERDETALLE", "Pagados");	
				datosExt.put("FEC_VENC", fec_venc);
				datosExt.put("ESTATUS", estatus);
			}
			if(t==1){
				datosExt.put("DESCRIPCION", "Créditos No Pagados del Día");			
				datosExt.put("NO_CREDITOS", sumaTotalNoPagado );	
				datosExt.put("IMPORTE", montoTotalNoPagado);
				datosExt.put("VERDETALLE", "NoPagados");	
				datosExt.put("fec_venc", fec_venc);
				datosExt.put("ESTATUS", estatus);
			}
			if(t==2){
				datosExt.put("DESCRIPCION", "NP Rechazados al Día");			
				datosExt.put("NO_CREDITOS", sumaTotalNoPagadoR );	
				datosExt.put("IMPORTE", montoTotalNoPagadoR);
				datosExt.put("VERDETALLE", "NoPagadosRech");	
				datosExt.put("FEC_VENC", fec_venc);	
				datosExt.put("ESTATUS", estatus);
			}
			if(t==3){
				datosExt.put("DESCRIPCION", "Registros no Encontrados");			
				datosExt.put("NO_CREDITOS", sumaTotalNoEncon );	
				datosExt.put("IMPORTE", montoTotalNoEncon);	
				datosExt.put("VERDETALLE", "NoEncontrados");	
				datosExt.put("FEC_VENC", fec_venc);	
				datosExt.put("ESTATUS", estatus);
			}
			if(t==4){
				datosExt.put("DESCRIPCION", "Créditos no Enviados del Día");			
				datosExt.put("NO_CREDITOS", sumaTotalNoEnv );	
				datosExt.put("IMPORTE", montoTotalNoEnv);	
				datosExt.put("VERDETALLE", "NoEnviados");	
				datosExt.put("FEC_VENC", fec_venc);	
				datosExt.put("ESTATUS", estatus);
			}
			if(t==5){
				datosExt.put("DESCRIPCION", "Créditos con Error");			
				datosExt.put("NO_CREDITOS", sumaTotalError );	
				datosExt.put("IMPORTE", montoTotalError);	
				datosExt.put("VERDETALLE", "ConError");
				datosExt.put("FEC_VENC", fec_venc);	
				datosExt.put("ESTATUS", estatus);
			}			
			registrosExt.add(datosExt);
		}
		consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
			
		
	}else if ( informacion.equals("consu_2Data")  &&  ruta_jsp.equals("successrt")   ) {			
		registrosExt = new JSONArray();		
		for(int t =0; t<1; t++) {		
			datosExt = new HashMap();				
			datosExt.put("DESCRIPCION", "Total de Registros Enviados");			
			datosExt.put("NO_CREDITOS", sumaTotalTmp );	
			datosExt.put("IMPORTE", montoTotalTmp);	
			datosExt.put("VERDETALLE", "RembolsosPrepagosTotales");
			datosExt.put("FEC_VENC", fec_venc);	
			datosExt.put("ESTATUS", estatus);
			registrosExt.add(datosExt);
		}
		consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		
	}else if ( informacion.equals("consu_3Data") &&  ruta_jsp.equals("successrt")  ) {		
		int reg =1;
		if("RC".equals(fec_venc)){  reg=3; } 
		if("T".equals(fec_venc)){  reg=2; } 
		
		registrosExt = new JSONArray();			
		for(int t =0; t<reg; t++) {		
			datosExt = new HashMap();	
			if("RC".equals(fec_venc)){
				if(t==0){
					datosExt.put("DESCRIPCION", "Recuperaciones Recibidas");			
					datosExt.put("NO_CREDITOS", sumaTotalRec );	
					datosExt.put("IMPORTE", montoTotalRec);	
					datosExt.put("VERDETALLE", "Recuperaciones");
					datosExt.put("FEC_VENC", fec_venc);
					datosExt.put("ESTATUS", estatus);
				}
				if(t==1){
					datosExt.put("DESCRIPCION", "Registros no Encontrados");			
					datosExt.put("NO_CREDITOS", sumaTotalNoEncon );	
					datosExt.put("IMPORTE", montoTotalNoEncon);	
					datosExt.put("VERDETALLE", "NoEncontrados");
					datosExt.put("FEC_VENC", fec_venc);	
					datosExt.put("ESTATUS", estatus);
				}	
				if(t==2){
					datosExt.put("DESCRIPCION", "Créditos con Error");			
					datosExt.put("NO_CREDITOS", sumaTotalError );	
					datosExt.put("IMPORTE", montoTotalError);	
					datosExt.put("VERDETALLE", "ConError");
					datosExt.put("FEC_VENC", fec_venc);		
					datosExt.put("ESTATUS", estatus);
				}
			}else  if("T".equals(fec_venc)){
				if(t==0){
					datosExt.put("DESCRIPCION", "Prepagos Totales");			
					datosExt.put("NO_CREDITOS", sumaTotalPrepago );	
					datosExt.put("IMPORTE", montoTotalPrepago);	
					datosExt.put("VERDETALLE", "Prepagos");
					datosExt.put("FEC_VENC", fec_venc);	
					datosExt.put("ESTATUS", estatus);
				}
				if(t==1){
					datosExt.put("DESCRIPCION", "Créditos con Error");			
					datosExt.put("NO_CREDITOS", sumaTotalError );	
					datosExt.put("IMPORTE", montoTotalError);	
					datosExt.put("VERDETALLE", "ConError");
					datosExt.put("FEC_VENC", fec_venc);		
					datosExt.put("ESTATUS", estatus);
				}
			}else  if(!"RC".equals(fec_venc)  &&  !"T".equals(fec_venc)){
				if(t==0){
					datosExt.put("DESCRIPCION", "Créditos con Error");			
					datosExt.put("NO_CREDITOS", sumaTotalError );	
					datosExt.put("IMPORTE", montoTotalError);	
					datosExt.put("VERDETALLE", "ConError");
					datosExt.put("FEC_VENC", fec_venc);	
					datosExt.put("ESTATUS", estatus);
				}					
			}	
			registrosExt.add(datosExt);
		}
		consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);		
		
		}else if ( informacion.equals("consu_4Data")  &&  ( ruta_jsp.equals("successr") || ruta_jsp.equals("successrt") )  ) {			
			registrosExt = new JSONArray();		
			for(int t =0; t<1; t++) {		
				datosExt = new HashMap();				
				datosExt.put("DESCRIPCION", "Total de Registros Enviados");			
				datosExt.put("NO_CREDITOS", sumaTotalTmp );	
				datosExt.put("IMPORTE", montoTotalTmp);	
				datosExt.put("VERDETALLE", "RembolsosPrepagosTotales");
				datosExt.put("FEC_VENC", fec_venc);	
				datosExt.put("ESTATUS", estatus);
				datosExt.put("VAR_DETALLE", masdequinRT);
				registrosExt.add(datosExt);
			}
			consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);
		
		}else if ( informacion.equals("consu_5Data")  &&   ( ruta_jsp.equals("successr") || ruta_jsp.equals("successrt") )    ) {			
			registrosExt = new JSONArray();		
			for(int t =0; t<3; t++) {		
				datosExt = new HashMap();	
				if("RC".equals(fec_venc)){
					if(t==0) {
						datosExt.put("DESCRIPCION", "Recuperaciones Recibidas");			
						datosExt.put("NO_CREDITOS", sumaTotalRec );	
						datosExt.put("IMPORTE", montoTotalRec);	
						datosExt.put("VERDETALLE", "Recuperaciones");
						datosExt.put("FEC_VENC", fec_venc);	
						datosExt.put("ESTATUS", estatus);
						datosExt.put("VAR_DETALLE", masdequinR);	
					}
					if(t==1) {
						datosExt.put("DESCRIPCION", "Registros no Encontrados");			
						datosExt.put("NO_CREDITOS", sumaTotalNoEncon );	
						datosExt.put("IMPORTE", montoTotalNoEncon);	
						datosExt.put("VERDETALLE", "NoEncontrados");
						datosExt.put("FEC_VENC", fec_venc);	
						datosExt.put("ESTATUS", estatus);
						datosExt.put("VAR_DETALLE", masdequinRNE);	
					}
					if(t==2) {
						datosExt.put("DESCRIPCION", "Créditos con Error");			
						datosExt.put("NO_CREDITOS", sumaTotalError );	
						datosExt.put("IMPORTE", montoTotalError);	
						datosExt.put("VERDETALLE", "ConError");
						datosExt.put("FEC_VENC", fec_venc);	
						datosExt.put("ESTATUS", estatus);
						datosExt.put("VAR_DETALLE", masdequinRCE);	
					}
				}else {
								
				if(t==0) {
						datosExt.put("DESCRIPCION", "Reembolsos Recibidos");			
						datosExt.put("NO_CREDITOS", sumaTotalReemb );	
						datosExt.put("IMPORTE", montoTotalReemb);	
						datosExt.put("VERDETALLE", "Reembolsos");
						datosExt.put("FEC_VENC", fec_venc);	
						datosExt.put("ESTATUS", estatus);
						datosExt.put("VAR_DETALLE", masdequinR);	
					}
					if(t==1) {
						datosExt.put("DESCRIPCION", "Registros no Encontrados");			
						datosExt.put("NO_CREDITOS", sumaTotalNoEncon );	
						datosExt.put("IMPORTE", montoTotalNoEncon);	
						datosExt.put("VERDETALLE", "NoEncontrados");
						datosExt.put("FEC_VENC", fec_venc);	
						datosExt.put("ESTATUS", estatus);
						datosExt.put("VAR_DETALLE", masdequinRNE);	
					}
					if(t==2) {
						datosExt.put("DESCRIPCION", "Créditos con Error");			
						datosExt.put("NO_CREDITOS", sumaTotalError );	
						datosExt.put("IMPORTE", montoTotalError);	
						datosExt.put("VERDETALLE", "ConError");
						datosExt.put("FEC_VENC", fec_venc);	
						datosExt.put("ESTATUS", estatus);
						datosExt.put("VAR_DETALLE", masdequinRCE);	
					}
				}
				registrosExt.add(datosExt);
			}
			consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);			
		}else if(informacion.equals("MostrarPreAcuse") ) {
					
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("lblIF", "FIDE");	
			jsonObj.put("lblFechaVen", fec_venc);	
			jsonObj.put("lblNomVali", strNombreUsuario);	
			jsonObj.put("lblFechaHoraVali", fec_Actual);		
			jsonObj.put("sumaTotalError", sumaTotalError);	
			jsonObj.put("mostrar", mostrar );	
			jsonObj.put("estatus", estatus );
			jsonObj.put("seguridad", seguridad.toString());
			jsonObj.put("masdequinRCE",masdequinRCE);
	}
	
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("VerDetalleDeCreditos")) {
	registros	= new ArrayList();
	datos	= new ArrayList();	
	String capital = "0", titulo ="";
	registrosExt = new JSONArray();	
	
	registros = fondoJunior.ObtenerDetalleDeCreditos(ver_enlace, estatus, estatus2, proc, fec_venc, clavePrograma);
	
	if(registros.size()>0){	
		for(int j= 0; j < 1; j++){
			datos = (List)registros.get(j);			
			titulo =	datos.get(0).toString();
		}
		for(int i= 1; i < registros.size(); i++){
			datos = (List)registros.get(i);
			capital  =datos.get(7)!=null?(String)datos.get(7):"0";		
				
			datosExt = new HashMap();
			datosExt.put("FECHA_AUTORIZACION_NA", datos.get(0));
			datosExt.put("FECHA_OPERACION", datos.get(1));
			datosExt.put("FECHA_PAGO_FIDE", datos.get(2));
			datosExt.put("PRESTAMO", datos.get(3));
			datosExt.put("NUM_CLI_SIRAC", datos.get(4));
			datosExt.put("CLIENTE", datos.get(5));
			datosExt.put("NUM_CUOTA", datos.get(6));
			datosExt.put("CAPITAL_INTERES", capital);			
			registrosExt.add(datosExt);		
		}	
	}
	consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("titulo", titulo );	
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("generaXLSCreditos")) {
		
	ComunesXLS xls = null;
	CreaArchivo 	archivo			= new CreaArchivo();
	String	nombreArchivo	= archivo.nombreArchivo()+".xls";
	xls = new ComunesXLS(strDirectorioTemp+nombreArchivo);
		
	List lstErrorGral = fondoJunior.ObtenerDetalleDeCreditos("ConError", estatus, "","N",fec_venc,clavePrograma);
		
	xls.setTabla(9);
	if(lstErrorGral!=null && lstErrorGral.size()>1){
		for(int x=0; x<lstErrorGral.size();x++){
			List lstErrorReg = (List)lstErrorGral.get(x);
			if(x==0){
				/*TITULO*/
				xls.setCelda("CREDITOS CON ERROR", "celda01", ComunesXLS.CENTER, 9);
				/*REGISTROS*/
				xls.setCelda("FECHA AUTORIZACION NAFIN", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("FECHA OPERACION", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("FECHA PAGO CLIENTE FIDE", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("PRESTAMO", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("NUM. CLIENTE SIRAC", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("NOMBRE CLIENTE", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("NUM. CUOTA A PAGAR", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("CAPITAL", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("ERROR", "celda01", ComunesXLS.CENTER, 1);						
			}else{						
				xls.setCelda(lstErrorReg.get(0)!=null?(String)lstErrorReg.get(0):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(1)!=null?(String)lstErrorReg.get(1):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(2)!=null?(String)lstErrorReg.get(2):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(3)!=null?(String)lstErrorReg.get(3):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(4)!=null?(String)lstErrorReg.get(4):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(5)!=null?(String)lstErrorReg.get(5):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(6)!=null?(String)lstErrorReg.get(6):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(7)!=null?"$"+Comunes.formatoDecimal((String)lstErrorReg.get(7),2):"--", "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(lstErrorReg.get(8)!=null?(String)lstErrorReg.get(8):"--", "formas", ComunesXLS.CENTER, 1);
			}
		}
	}
			
	xls.cierraTabla();
	xls.cierraXLS();
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("eliminaRegMonitor")  )  {

	Seguridad s = new Seguridad();
	AccesoDB con = new AccesoDB();
	String folioCert = "";
	char getReceipt = 'Y';
	
	FondoJrMonitorBean monitor = new FondoJrMonitorBean();
	monitor.setFec_venc(fec_venc);
	monitor.setUsuario(strNombreUsuario);
	monitor.setEstatus_cred(estatus);
	
	if (!_serial.equals("") && textoFirmar!=null && pkcs7!=null) {
		if (s.autenticar(folioCert, _serial, pkcs7, textoFirmar, getReceipt))	{			
			fondoJunior.eliminaRegMonitor(monitor,true, clavePrograma);				
		}else {
			mensajeError ="La Eliminación no se llevo acabo con exito";
		}
	}else {
		mensajeError ="La Eliminación no se llevo acabo con exito";
	}
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensajeError", mensajeError);	
	infoRegresar = jsonObj.toString();

	
}else if ( informacion.equals("validaMonitorAcuse")  )  {
		
	FondoJrMonitorBean monitor = new FondoJrMonitorBean();
	monitor.setFec_venc(fec_venc);
	monitor.setUsuario(strNombreUsuario);
	monitor.setEstatus_cred(estatus);
	monitor.setSerial(_serial);
	monitor.setTextoFirmado(textoFirmar);
	monitor.setPkcs7(pkcs7);	
	
	List lstAcuseGral = fondoJunior.autorizaValidacionMonitor(monitor, true, clavePrograma);
	if(lstAcuseGral.size()==0){
		mensajeError = "Error en la validacion del certificado";
		
	}else if(lstAcuseGral!=null && lstAcuseGral.size()>0){
		for(int x=0; x<lstAcuseGral.size(); x++){
			List lstRegAcuse =  (List)lstAcuseGral.get(x);
			if(((String)lstRegAcuse.get(2)).equals("encabezado")){
				folio = (String)lstRegAcuse.get(0);
				usuario = (String)lstRegAcuse.get(1);
			}
		}		
	}		
	if(folio.equals("")) {
	 mensaje = "</table align='center'>"+
						" <tr> "+
						"<td class='formas' colspan='1' align='left'><b>Error en la validacion del certificado</b></td>"+
						"</tr>"+
						"</table>";
	 mensajeE = "</table width='300' align='center'>"+
						" <tr> "+
						"<td class='formas' width='500' colspan='5' align='left'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>"+
						"<td class='formas' width='500' colspan='5' align='left'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>"+
						"<td class='formas' width='500' colspan='5' align='left'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>"+
						"<td class='formas' width='500' colspan='5' align='left'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>"+
						"<td class='formas' width='500' colspan='5' align='left'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>"+
						"<td class='formas' width='500' colspan='5' align='left'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>"+
						"</tr>"+
						"</table>";
						
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("folio", folio);	
	jsonObj.put("usuario", usuario);	
	jsonObj.put("fec_Actual", fec_Actual);
	jsonObj.put("fec_venc", fec_venc);
	jsonObj.put("mensaje", mensaje);	
	jsonObj.put("mensajeE", mensajeE);	
	
	infoRegresar = jsonObj.toString();	

	
}else if ( informacion.equals("consuAcuseData")  )  {

	List lstAcuseGral = fondoJunior.consAcuseMonitorValidado(folio, usuario, fec_venc);
		
	if(lstAcuseGral!=null && lstAcuseGral.size()>0){
		for(int x=0; x<lstAcuseGral.size(); x++){
			List lstRegAcuse =  (List)lstAcuseGral.get(x);
			if(!((String)lstRegAcuse.get(2)).equals("encabezado")){
				sumaTotal = (String)lstRegAcuse.get(0);
				montoTotal = (String)lstRegAcuse.get(1);
				ultimo_reg = (String)lstRegAcuse.get(3);	
				if(((String)lstRegAcuse.get(2)).equals("total"))
					fec_venc = (String)lstRegAcuse.get(4);
				if(((String)lstRegAcuse.get(2)).equals("reembolsos"))
					fec_venc = "Reembolsos";
				if(((String)lstRegAcuse.get(2)).equals("recuperacion"))
					fec_venc = "Recuperaciones";
				if(((String)lstRegAcuse.get(2)).equals("prepagos"))
					fec_venc = "Prepagos";
			
				datosExt = new HashMap();
				datosExt.put("fec_venc", fec_venc);
				datosExt.put("FECHA_FIDE", ultimo_reg);
				datosExt.put("FOLIO_OPERACION", folio);
				datosExt.put("TOTAL_REGISTROS",sumaTotal);
				datosExt.put("MONTO", montoTotal);				
				registrosExt.add(datosExt);
			}
		}
	}
	
	consulta =  "{\"success\": true, \"total\": \"" + registrosExt.size() + "\", \"registros\": " + registrosExt.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();
	

}else if ( informacion.equals("generarArchivoAcuse")  )  {

	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";	
	ComunesPDF docPdf = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
	docPdf.encabezadoConImagenes(docPdf,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	
	docPdf.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	docPdf.addText(" ","formas",ComunesPDF.RIGHT);
		
	docPdf.addText("Programa: "+descProgramaFondoJR,"formasmen",ComunesPDF.CENTER);
	docPdf.addText(" ");
	docPdf.addText("La Autentificación fue Realizada con Exito","formasG",ComunesPDF.CENTER);
	docPdf.addText("Folio: "+folio,"formasG",ComunesPDF.CENTER);
	docPdf.addText(" ");
	docPdf.addText("Nombre del Usuario que Autoriza: "+usuario,"formasmen",ComunesPDF.CENTER);
	docPdf.addText("Fecha y Hora de Actualizacion: "+fec_Actual,"formasmen",ComunesPDF.CENTER);
	
	
	docPdf.setTable(5,80);//.addText()
	docPdf.setCell("Fecha de Vencimiento","formasmenB",ComunesPDF.CENTER);		
	docPdf.setCell("Fecha y Hora de Ultimo Registro FIDE","formasmenB",ComunesPDF.CENTER);
	docPdf.setCell("Folio de Operación","formasmenB",ComunesPDF.CENTER);
	docPdf.setCell("Total de Registros","formasmenB",ComunesPDF.CENTER);		
	docPdf.setCell("Monto","formasmenB",ComunesPDF.CENTER);
			
			
	List lstAcuseGral = fondoJunior.consAcuseMonitorValidado(folio, usuario, fec_venc);
	if(lstAcuseGral!=null && lstAcuseGral.size()>0){
		for(int x=0; x<lstAcuseGral.size(); x++){
			List lstRegAcuse =  (List)lstAcuseGral.get(x);
			if(((String)lstRegAcuse.get(2)).equals("encabezado")){
				folio = (String)lstRegAcuse.get(0);
				usuario = (String)lstRegAcuse.get(1);
			}else{
				sumaTotal = (String)lstRegAcuse.get(0);
				montoTotal = (String)lstRegAcuse.get(1);
				ultimo_reg = (String)lstRegAcuse.get(3);	
				if(((String)lstRegAcuse.get(2)).equals("total"))
					fec_venc = (String)lstRegAcuse.get(4);
				if(((String)lstRegAcuse.get(2)).equals("reembolsos"))
					fec_venc = "Reembolsos";
				if(((String)lstRegAcuse.get(2)).equals("recuperacion"))
					fec_venc = "Recuperaciones";
				if(((String)lstRegAcuse.get(2)).equals("prepagos"))
					fec_venc = "Prepagos";
							
				docPdf.setCell(fec_venc,"formasmen",ComunesPDF.CENTER);		
				docPdf.setCell(ultimo_reg,"formasmen",ComunesPDF.CENTER);
				docPdf.setCell(folio,"formasmen",ComunesPDF.CENTER);
				docPdf.setCell(sumaTotal,"formasmen",ComunesPDF.CENTER);		
				docPdf.setCell("$"+Comunes.formatoDecimal(montoTotal,2),"formasmen",ComunesPDF.CENTER);											
			}					
		}
	}
	
	docPdf.addTable();			
	docPdf.endDocument();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();					
}

			
%>
<%=infoRegresar%> 

