<%@ page 
  contentType=
    "application/json;charset=UTF-8"
	import=
    "java.sql.*, java.io.*,  java.util.* , com.netro.fondosjr.ConsultaParamDiasVencidosExt,
    netropology.utilerias.*, com.netro.fondojr.*, com.netro.cadenas.FondoJr, 
    net.sf.json.JSONObject, org.apache.commons.logging.Log"
	errorPage=
  "/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
  String infoRegresar   = "";
  String consulta       = "";
	String informacion 	  = (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
  String accion         = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
  String numOper     	  = (request.getParameter("numOper") != null) ? request.getParameter("numOper") :"";
  String fd 	          = (request.getParameter("fd")   != null) ? request.getParameter("fd") :"";
  String fh 	          = (request.getParameter("fh")   != null) ? request.getParameter("fh") :"";
  String dias 	        = (request.getParameter("dias") != null) ? request.getParameter("dias") :"";
  String detalle 	      = (request.getParameter("areaDetalle") != null) ? request.getParameter("areaDetalle") :"";
  String descricionPro  = (String)request.getSession().getAttribute("descProgramaFondoJR");
  String programa       = (String)request.getSession().getAttribute("cveProgramaFondoJR");
  String usuarioAlta    = strNombreUsuario;
  String noOperacion    = "";
  String operacion      = ""; 
  String validaFechas   = "";
  int    start =0;
  int    limit =0;
  
if (informacion.equals("descPro")){
  JSONObject  jsonObj   = new JSONObject(); 
  jsonObj.put("success" , new Boolean(true));
  jsonObj.put("descPro" , descricionPro ); 
  infoRegresar = jsonObj.toString();
 
}else if(informacion.equals("guardar")) {
   
  FondoJunior       fondoJunior       = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
  JSONObject        jsonObj           = new JSONObject(); 
  ArrayList         param             = new ArrayList();  
  FondoJr           f                 = new FondoJr();
  
  validaFechas = fondoJunior.validaRangoFechas( fd, fh,programa, operacion);		
  
  f.setFechas(validaFechas);
  String respuesta    = f.validaFechas();
  if (respuesta.equals("N")){
      param.add(fd);
      param.add(fh);
      param.add(dias);
      param.add(detalle);
      param.add(programa);
      param.add(usuarioAlta);
      noOperacion =  fondoJunior.paraDiasVencimiento(param);
  } else {
    noOperacion="-";
  }  
  jsonObj.put("success"     , new Boolean(true)); 
  jsonObj.put("respuesta"   , respuesta); 
  jsonObj.put("noOperacion" , noOperacion ); 
  infoRegresar = jsonObj.toString();  

} else if(informacion.equals("consultar") ) {
  JSONObject jsonObj  = new JSONObject();
  ConsultaParamDiasVencidosExt paginador = new ConsultaParamDiasVencidosExt();
  paginador.setAccion(accion);
  paginador.setFechaVenDesde(fd);
  paginador.setFechaVenA(fh);
  paginador.setDiasVencimiento(dias);
  paginador.setOperacion(numOper);
  paginador.setPrograma(programa);
  
  CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	consulta="";
  try {
    start = Integer.parseInt(request.getParameter("start"));
    limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos start-limit", e);
	}
  try  {
    if (accion.equals("Generar")) {	//Nueva consulta
        queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
    }    
    //Registros reg=queryHelper.getPageResultSet(request,start,limit);
	 consulta = queryHelper.getJSONPageResultSet(request,start,limit);
    //consulta	=	"{\"success\": true, \"total\": \""	+	queryHelper.getIdsSize() + "\", \"registros\": " + reg.getJSONData()+ "}";
    jsonObj = JSONObject.fromObject(consulta);
    infoRegresar = jsonObj.toString(); 					
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);		
    }

} else if(informacion.equals("ArchivoCSV")){
  
  JSONObject jsonObj  = new JSONObject();
  ConsultaParamDiasVencidosExt paginador = new ConsultaParamDiasVencidosExt();
  CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
  paginador.setFechaVenDesde(fd);
  paginador.setFechaVenA(fh);
  paginador.setDiasVencimiento(dias);
  paginador.setOperacion(numOper);
  paginador.setPrograma(programa);
  try {
      String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
  } catch(Exception e) {
   log.debug(" new AppException: "); 
  } 

} else if(informacion.equals("ArchivoPDF")){
  
  JSONObject jsonObj  = new JSONObject();
  ConsultaParamDiasVencidosExt paginador = new ConsultaParamDiasVencidosExt();
  CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
  paginador.setFechaVenDesde(fd);
  paginador.setFechaVenA(fh);
  paginador.setDiasVencimiento(dias);
  paginador.setOperacion(numOper);
  paginador.setPrograma(programa);

  try {
      String nombreArchivo = queryHelper.getCreatePageCustomFile(request,0,15,strDirectorioTemp,"PDF");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
  } catch(Exception e) {
   log.debug(" new AppException: "); 
  } 

}else if(informacion.equals("modificar")) {
   
  FondoJunior       fondoJunior       =  ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
  JSONObject        jsonObj           = new JSONObject(); 
  ArrayList         param             = new ArrayList();  
  FondoJr           f                 = new FondoJr();
  String            respModificar     = "";  
  
  validaFechas = fondoJunior.validaRangoFechas( fd, fh,programa, operacion);		
  
  f.setFechas(validaFechas);
  String respuesta    = f.validaFechas();
  if (respuesta.equals("N") || respuesta.equals(numOper+',')){
      param             = new ArrayList();  
      param.add(fd);					
      param.add(fh);
      param.add(dias);
      param.add(detalle);
      param.add(numOper);
      param.add(usuarioAlta);
      respModificar =fondoJunior.modificarParaDiasVencidos(param);
      respuesta="M"; 
  } else {
    noOperacion="-";
    respModificar="-";
  }  
  
  jsonObj.put("success"     , new Boolean(true)); 
  jsonObj.put("respuesta"   , respuesta); 
  jsonObj.put("noOperacion" , noOperacion );   
  jsonObj.put("respModificar", respuesta); 
  infoRegresar = jsonObj.toString();
  
}else if(informacion.equals("eliminar")) {
  FondoJunior       fondoJunior       =  ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
  JSONObject        jsonObj           = new JSONObject(); 
  String[] numOperEli	  = request.getParameterValues("numOperEli");	
  List listnumOperEli 	= Arrays.asList(numOperEli);
  String respuestaEli   ="";
  
  respuestaEli = fondoJunior.eliminarParaDiasVencidos(listnumOperEli);
  
  if(respuestaEli.equals("E"))    
    jsonObj.put("respuestaEli", "ok"); 
  else 
    jsonObj.put("respuestaEli", "error"); 
    
  jsonObj.put("success"     , new Boolean(true)); 
  infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>
