<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession.jspf" %>

<html> 
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<%@ include file="/00utils/componente_firma.jspf" %>

<script type="text/javascript" src="33DisposicionDeFondoPreacuseExt.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/00utils/extjs/ux/GroupSummary.js"></script>
<link rel="stylesheet" href="/nafin/00utils/extjs/ux/GroupSummary.css">

</head>

<body id="docBody" name ="docBody" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
<%if(esEsquemaExtJS){%>
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
<%}%>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
<%if(esEsquemaExtJS){%>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>

</html>