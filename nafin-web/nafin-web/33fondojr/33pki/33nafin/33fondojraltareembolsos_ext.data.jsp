<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.CatalgoAfianzadora,
		com.netro.model.catalogos.CatalogoFiado,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.fondojr.*,
		com.netro.fondosjr.*,
		com.netro.xls.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>
<%@ include file="/33fondojr/33pki/certificado.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String login_usuario = (String)request.getSession().getAttribute("Clave_usuario");
String nombre_usuario  = (String)request.getSession().getAttribute("strNombreUsuario");

JSONArray jsonArray = new JSONArray();
JSONObject jsonObj = new JSONObject();
/*	SE CREA NUEVO OBJETO PARA ACCESAR EL EJB */
FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);

if(informacion.equals("listaParaReembolsar")){
	
	String numeroPrestamo  = request.getParameter("igPrestamo")==null?"":request.getParameter("igPrestamo");
	
	List lstNpAreembolsar = fondoJunior.obtenerNPaReembolsarXprestamo(numeroPrestamo,  cveProgramaFondoJR);
	jsonArray = JSONArray.fromObject(lstNpAreembolsar);
	
	jsonObj.put("registros", jsonArray.toString());
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("generarReembolsos")){
	char getReceipt = 'Y';
	String _acuse  ="";
	Seguridad s = new Seguridad();
	String externContent = (request.getParameter("textoFirmar")!=null)?request.getParameter("textoFirmar"):"";
	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";	
	String mensaje = "";
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistros = JSONArray.fromObject(jsonRegistros);
	List lstRegistros = new ArrayList();
	
	Iterator itReg = arrRegistros.iterator();
	int index = 0;
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		
		HashMap hmRegistro = new HashMap();
		hmRegistro.put("FECHA_PROBABLEPAGO",registro.getString("FECHA_PROBABLEPAGO")==null?"":registro.getString("FECHA_PROBABLEPAGO"));
		hmRegistro.put("PRESTAMO",registro.getString("PRESTAMO")==null?"":registro.getString("PRESTAMO"));
		hmRegistro.put("AMORTIZACION",registro.getString("AMORTIZACION")==null?"":registro.getString("AMORTIZACION"));
		hmRegistro.put("MONTO_AMORTIZACION",registro.getString("MONTO_AMORTIZACION")==null?"":registro.getString("MONTO_AMORTIZACION"));
		lstRegistros.add(hmRegistro);
	}
	
	
	Calendar calendario = Calendar.getInstance();
	SimpleDateFormat formato_cert  = new SimpleDateFormat("ddMMyyHHmmss");
	SimpleDateFormat formato_fecha = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat formato_hora  = new SimpleDateFormat("hh:mm aa");
	
	String folioCert = "RF" + formato_cert.format(calendario.getTime());
	String fecha_carga = formato_fecha.format(calendario.getTime());
	String hora_carga = formato_hora.format(calendario.getTime());
	
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			_acuse = s.getAcuse();
			fondoJunior.generarReembolsos(lstRegistros,cveProgramaFondoJR);
		}else{
			mensaje = "La autentificación no se llevó a cabo.";
		}
	}
	
	jsonObj.put("folio_acuse", _acuse);
	jsonObj.put("folio_carga", folioCert);
	jsonObj.put("nombre_usuario", login_usuario + " - " + nombre_usuario);
	jsonObj.put("fecha_carga", fecha_carga);
	jsonObj.put("hora_carga", hora_carga);
	jsonObj.put("mensaje", mensaje);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("generarPDF")){
	String total_reembolsos = (request.getParameter("total_reembolsos") == null)?"":request.getParameter("total_reembolsos");
	String monto_reembolsos = (request.getParameter("monto_reembolsos") == null)?"":request.getParameter("monto_reembolsos");
	String tipoArchivo = (request.getParameter("tipo_archivo") == null)?"":request.getParameter("tipo_archivo");
	
	ComunesPDF pdfDoc    			= new ComunesPDF();
	StringBuffer contenidoArchivo = new StringBuffer();
	CreaArchivo creaArchivo 		= new CreaArchivo();
	OutputStreamWriter writer		= null;
	BufferedWriter buffer 			= null;
	String path = strDirectorioTemp;
	String nombreArchivo = "";
	try {
		Comunes comunes		= new Comunes();
		nombreArchivo 			= Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc 					= new ComunesPDF(2, path + nombreArchivo);
	
		String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    	= fechaActual.substring(0,2);
		String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   	= fechaActual.substring(6,10);
		String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText( "	","formas",ComunesPDF.RIGHT);
		
		pdfDoc.addText( "Programa: " + descProgramaFondoJR, 	  "formas", ComunesPDF.LEFT);
		
		if(tipoArchivo.equals("PREACUSE")){
			pdfDoc.addText( "Preacuse Reembolsos", "titulo", ComunesPDF.CENTER);
			pdfDoc.setTable(2,50);
			pdfDoc.setCell("No. Total de Amortizaciones a Reembolsar",	"formas", ComunesPDF.LEFT			);
			pdfDoc.setCell(""+total_reembolsos, "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("Monto Total de Amortizaciones a Reembolsar", "formas", ComunesPDF.LEFT			);
			pdfDoc.setCell(monto_reembolsos,	"formas", ComunesPDF.RIGHT	);
			pdfDoc.setCell("Al transmitirse este MENSAJE DE DATOS, "+
								"queda bajo su responsabilidad el "+
								"movimiento contable al fondo, aceptando de esta "+
								"forma la responsabilidad de los movimientos registrados "+
								"del mismo, dicha transmisión tendrá validez para todos "+
								"los efectos legales.","formas", ComunesPDF.LEFT, 2, 1	);
			pdfDoc.addTable();
		} else if(tipoArchivo.equals("ACUSE")){
			//String total_reembolsos = (request.getParameter("total_reembolsos") == null)?"":request.getParameter("total_reembolsos");
			//String monto_reembolsos = (request.getParameter("monto_reembolsos") == null)?"":request.getParameter("monto_reembolsos");
			String fecha_carga = (request.getParameter("fecha_carga") == null)?"":request.getParameter("fecha_carga");
			String hora_carga = (request.getParameter("hora_carga") == null)?"":request.getParameter("hora_carga");
			String folio_acuse = (request.getParameter("folio_acuse") == null)?"":request.getParameter("folio_acuse");
			String folio_carga = (request.getParameter("folio_carga") == null)?"":request.getParameter("folio_carga");
			//String nombre_usuario = (request.getParameter("nombre_usuario") == null)?"":request.getParameter("nombre_usuario");
			
			
			pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
			pdfDoc.addText("La autentificación se llevó a cabo con éxito \n Recibo: " + folio_acuse, "titulo", ComunesPDF.CENTER);				
			pdfDoc.setTable(2,50);
			pdfDoc.setCell("No. Total de Amortizaciones Reembolsadas",			"formas", ComunesPDF.RIGHT		);
			pdfDoc.setCell(""+total_reembolsos, "formas", ComunesPDF.LEFT		);
			pdfDoc.setCell("Monto Total de Amortizaciones Reembolsadas",		"formas", ComunesPDF.RIGHT		);
			pdfDoc.setCell("$" + monto_reembolsos,								"formas", ComunesPDF.LEFT		);
			pdfDoc.setCell("Número de Acuse",											"formas", ComunesPDF.RIGHT		);
			pdfDoc.setCell(folio_carga,												"formas", ComunesPDF.LEFT		);					
			pdfDoc.setCell("Fecha de Carga",												"formas", ComunesPDF.RIGHT		);
			pdfDoc.setCell(fecha_carga,												"formas", ComunesPDF.LEFT		);		
			pdfDoc.setCell("Hora de Carga",												"formas", ComunesPDF.RIGHT		);
			pdfDoc.setCell(hora_carga,												"formas", ComunesPDF.LEFT		);		
			pdfDoc.setCell("Usuario",														"formas", ComunesPDF.RIGHT		);
			pdfDoc.setCell(login_usuario+" - "+nombre_usuario,											"formas", ComunesPDF.LEFT		);					
			pdfDoc.setCell("Al transmitirse este MENSAJE DE DATOS, "+
								"queda bajo su responsabilidad el "+
								"movimiento contable al fondo, aceptando de esta "+
								"forma la responsabilidad de los movimientos registrados "+
								"del mismo, dicha transmisión tendrá validez para todos "+
								"los efectos legales.",	"formas", ComunesPDF.CENTER, 2);
			pdfDoc.addTable();	
		}
		
		pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
				
		pdfDoc.setTable(7,80);
		pdfDoc.setCell("Fecha de Amortización",		"celda01", ComunesPDF.CENTER		);
		pdfDoc.setCell("Préstamo",						"celda01", ComunesPDF.CENTER		);
		pdfDoc.setCell("Número de Cliente SIRAC",	"celda01", ComunesPDF.CENTER		);
		pdfDoc.setCell("Cliente",						"celda01", ComunesPDF.CENTER		);
		pdfDoc.setCell("Monto del Reembolso",		"celda01", ComunesPDF.CENTER		);
		pdfDoc.setCell("No. Amortización",			"celda01", ComunesPDF.CENTER		);
		pdfDoc.setCell("Origen",						"celda01", ComunesPDF.CENTER		);
		
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
		List arrRegistros = JSONArray.fromObject(jsonRegistros);
		
		Iterator itReg = arrRegistros.iterator();
		int index = 0;
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();
			
			HashMap hmRegistro = new HashMap();
			String fecha = registro.getString("FECHA_PROBABLEPAGO")==null?"":registro.getString("FECHA_PROBABLEPAGO");
			String prestamo = registro.getString("PRESTAMO")==null?"":registro.getString("PRESTAMO");
			String amortizacion = registro.getString("AMORTIZACION")==null?"":registro.getString("AMORTIZACION");
			String origen = registro.getString("ORIGEN")==null?"":registro.getString("ORIGEN");
			String cliente = registro.getString("NOMBRE_CLIENTE")==null?"":registro.getString("NOMBRE_CLIENTE");
			String reembolso = registro.getString("MONTO_AMORTIZACION")==null?"":registro.getString("MONTO_AMORTIZACION");
			String clienteSirac = registro.getString("NUM_CLIENTE_SIRAC")==null?"":registro.getString("NUM_CLIENTE_SIRAC");	
			
			
			pdfDoc.setCell(fecha,												"formas",	ComunesPDF.CENTER);			
			pdfDoc.setCell(""+prestamo,										"formas",	ComunesPDF.CENTER);
			pdfDoc.setCell(""+clienteSirac,									"formas",	ComunesPDF.CENTER);
			pdfDoc.setCell(cliente,												"formas",	ComunesPDF.LEFT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(reembolso,2),"formas",	ComunesPDF.RIGHT);
			pdfDoc.setCell(""+amortizacion,									"formas",	ComunesPDF.CENTER);
			pdfDoc.setCell(origen,												"formas",	ComunesPDF.CENTER);
		
		}
		
		pdfDoc.setCell("Total Monto por Reembolsos",    "formas", ComunesPDF.RIGHT, 4, 1);	
		pdfDoc.setCell(monto_reembolsos, "formas", ComunesPDF.LEFT,  3, 1);
		pdfDoc.setCell("Total Reembolsos", "formas", ComunesPDF.RIGHT, 4, 1);
		pdfDoc.setCell(""+total_reembolsos, "formas", ComunesPDF.LEFT,  3, 1);
		
		pdfDoc.addTable();	
		pdfDoc.endDocument();	
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo ", e);
	}

	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("generarCsvAcuse")){
	try{
		String path = strDirectorioTemp;
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		StringBuffer contenidoArchivo = new StringBuffer();
		String nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		int total = 0;
		
		writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);		
	
		String total_reembolsos = (request.getParameter("total_reembolsos") == null)?"":request.getParameter("total_reembolsos");
		String monto_reembolsos = (request.getParameter("monto_reembolsos") == null)?"":request.getParameter("monto_reembolsos");
		String fecha_carga = (request.getParameter("fecha_carga") == null)?"":request.getParameter("fecha_carga");
		String hora_carga = (request.getParameter("hora_carga") == null)?"":request.getParameter("hora_carga");
		String folio_acuse = (request.getParameter("folio_acuse") == null)?"":request.getParameter("folio_acuse");
		String folio_carga = (request.getParameter("folio_carga") == null)?"":request.getParameter("folio_carga");
	
		
		contenidoArchivo = new StringBuffer();	
		contenidoArchivo.append("\nLa autentificación se llevó a cabo con éxito \n Recibo: "+folio_acuse+"\n,\n");
		contenidoArchivo.append("Programa:,");
		contenidoArchivo.append(descProgramaFondoJR + "\n");
		contenidoArchivo.append("No. Total de Amortizaciones Reembolsadas,");
		contenidoArchivo.append(total_reembolsos + "\n");
		contenidoArchivo.append("Monto Total de Amortizaciones Reembolsadas,");
		contenidoArchivo.append("$" + monto_reembolsos + "\n");
		contenidoArchivo.append("Número de Acuse,");
		contenidoArchivo.append(folio_carga + "\n");
		contenidoArchivo.append("Fecha de Carga,");
		contenidoArchivo.append(fecha_carga + "\n");
		contenidoArchivo.append("Hora de Carga,");
		contenidoArchivo.append(hora_carga + "\n");
		contenidoArchivo.append("Usuario,");
		contenidoArchivo.append(login_usuario+" - "+nombre_usuario + "\n");
		contenidoArchivo.append(("Al transmitirse este MENSAJE DE DATOS, "+
									"queda bajo su responsabilidad el "+
									"movimiento contable al fondo, aceptando de esta "+
									"forma la responsabilidad de los movimientos registrados "+
									"del mismo, dicha transmisión tendrá validez para todos "+
									"los efectos legales.").replace(',',' ')+"\n,\n");				
		
		contenidoArchivo.append("Fecha de Movimiento,");
		contenidoArchivo.append("Préstamo,");
		contenidoArchivo.append("Número de Cliente SIRAC,");
		contenidoArchivo.append("Cliente,");
		contenidoArchivo.append("Monto del Reembolso,");
		contenidoArchivo.append("No. Amortización,");
		contenidoArchivo.append("Origen\n");
		
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
		List arrRegistros = JSONArray.fromObject(jsonRegistros);
		
		Iterator itReg = arrRegistros.iterator();
		int index = 0;
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();
			
			HashMap hmRegistro = new HashMap();
			String fecha = registro.getString("FECHA_PROBABLEPAGO")==null?"":registro.getString("FECHA_PROBABLEPAGO");
			String prestamo = registro.getString("PRESTAMO")==null?"":registro.getString("PRESTAMO");
			String amortizacion = registro.getString("AMORTIZACION")==null?"":registro.getString("AMORTIZACION");
			String origen = registro.getString("ORIGEN")==null?"":registro.getString("ORIGEN");
			String cliente = registro.getString("NOMBRE_CLIENTE")==null?"":registro.getString("NOMBRE_CLIENTE");
			String reembolso = registro.getString("MONTO_AMORTIZACION")==null?"":registro.getString("MONTO_AMORTIZACION");
			String clienteSirac = registro.getString("NUM_CLIENTE_SIRAC")==null?"":registro.getString("NUM_CLIENTE_SIRAC");
			
			
			contenidoArchivo.append(fecha + ",");
			contenidoArchivo.append(prestamo+ ",");
			contenidoArchivo.append(clienteSirac + ",");
			contenidoArchivo.append(cliente.replace(',',' ') + ",");
			contenidoArchivo.append("$" + reembolso + ",");
			contenidoArchivo.append(amortizacion + ",");
			contenidoArchivo.append(origen + "\n");
		}
		
		contenidoArchivo.append("Total Monto por Reembolsos,");
		contenidoArchivo.append("$" + monto_reembolsos + "\n");
		contenidoArchivo.append("Total Reembolsos" + ",");
		contenidoArchivo.append(total_reembolsos + "\n");
		
		buffer.write(contenidoArchivo.toString());
		buffer.close();	
		contenidoArchivo = new StringBuffer();
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo ", e);
	}
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>