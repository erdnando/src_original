<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*,  
	java.io.*,  
	java.text.*,  
	com.netro.exception.*,
	java.util.Date,
	
	com.netro.cadenas.*, 
	java.sql.*,  
	netropology.utilerias.*,
	com.netro.seguridad.*,
	javax.naming.*,
	netropology.utilerias.usuarios.*,
	com.netro.model.catalogos.*,
	org.apache.commons.logging.Log,
	com.netro.cedi.*,
	net.sf.json.JSONArray,
	org.apache.commons.fileupload.disk.*,
	org.apache.commons.fileupload.servlet.*,
	org.apache.commons.fileupload.*,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/39CediIntermediarioNB/39secsession.jspf"%>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<% 
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	
	String infoRegresar="", consulta="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	
	Cedi cediEjb = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 

	log.debug("informacion -- >>> "+informacion );
	if(informacion.equals("datosGrales")){
		JSONObject jsonObjS = new JSONObject();
		Usuario datosCargados = sesUsuario;
		String obtRFC = datosCargados.getRfc();
		log.debug("obtRFC *** "+obtRFC);
		String ic_aspirante = cediEjb.getClaveAspirante(obtRFC);
		log.debug("ic_aspirante *** "+ic_aspirante);
		
	
		Map InfoInicial = null;
		Map pestConInf = null;// Obtiene la informacion de parametrización de la preguntas
		Map InfoGeneral = null;
		InfoInicial = cediEjb.getDatosInicialesCalificate(obtRFC);
		
		InfoGeneral = cediEjb.getInformacionInicialCalificate(obtRFC,informacion);
		String clave_solicitud = cediEjb.getClaveSolicitud(ic_aspirante);
		log.debug("clave_solicitud *** "+clave_solicitud);
		pestConInf = cediEjb.informacionInicialTp(clave_solicitud);
		jsonObjS.put("success", new Boolean(true));
		jsonObjS.put("InfoInicial", InfoInicial);
		jsonObjS.put("pestania", informacion);
		jsonObjS.put("InfoGeneral", InfoGeneral);
		jsonObjS.put("pestConInf", pestConInf);
		infoRegresar =	jsonObjS.toString();
		
	}if(informacion.equals("infCualitativa")||informacion.equals("ceditoVinculado")||informacion.equals("infoFinanciera")||informacion.equals("infoSaldos")||informacion.equals("IndicadorFinanciero")){
		JSONObject jsonObjS = new JSONObject();
		Usuario datosCargados = sesUsuario;
		String obtRFC = datosCargados.getRfc();
		String ic_aspirante = cediEjb.getClaveAspirante(obtRFC);
		
		Map InfoInicial = null;
		boolean ban_Valida = false;
		boolean exito = true;
		Map InfoGeneral = null;// Obtiene la informacion de parametrización de la preguntas
		Map resulbot = null;
		List respBoton = null;
		int existe_rechazo_aux = 0;
		Map pestConInf = null;
		InfoGeneral = cediEjb.getInformacionInicialCalificate(obtRFC,informacion);
		String clave_solicitud = cediEjb.getClaveSolicitud(ic_aspirante);
		pestConInf = cediEjb.informacionInicialTp(clave_solicitud);
		String existe_rech = "";
		String valor_dictamen = "";
		String click_finalizar = "";
		
		
		if(informacion.equals("IndicadorFinanciero")){
			String existe_rechazo = cediEjb.existeRechazoMovimientos(clave_solicitud);
			
			if(existe_rechazo.equals("")){
			    existe_rechazo_aux = 0;
			}else{
			    existe_rechazo_aux = Integer.parseInt(existe_rechazo);
			}
			if(existe_rechazo_aux>0){
			    existe_rech = "S";
			}else{
			    existe_rech = "N";
			}
			
			String tipo_cartera = cediEjb.getTipoCartera(clave_solicitud,"4");
			ban_Valida = cediEjb.validaViabilidadCedula(clave_solicitud,tipo_cartera);
			if(ban_Valida==false){
				valor_dictamen = "N";
			}else{
				valor_dictamen = "S";
			}
			
		}
		
		InfoInicial = cediEjb.getDatosInicialesCalificate(obtRFC);
		jsonObjS.put("success", new Boolean(true));
		jsonObjS.put("InfoGeneral", InfoGeneral);
		jsonObjS.put("pestania", informacion);
		jsonObjS.put("InfoInicial", InfoInicial);
		jsonObjS.put("existe_rechazo", existe_rech);
		jsonObjS.put("pestConInf", pestConInf);
		jsonObjS.put("total_rechazo", String.valueOf(existe_rechazo_aux));
		jsonObjS.put("valor_dictamen", valor_dictamen);
		jsonObjS.put("click_finalizar", "N");
		infoRegresar =	jsonObjS.toString();
	}else if(informacion.equals("catalogoEstado")){
	
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("CODIGO_DEPARTAMENTO");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setClavePais("24");//Mexico clave 24
		cat.setOrden("cd_nombre");
		infoRegresar = cat.getJSONElementos();
		
	}else if(informacion.equals("catalogoMunicipio")){
		String cmbEdo= (request.getParameter("cmbEdo")==null)?"":request.getParameter("cmbEdo");
		JSONArray jsonArr = new JSONArray();
	
	if(!cmbEdo.equals("")){
		CatalogoMunicipio cat=new CatalogoMunicipio();
		cat.setClave("ic_municipio");
		cat.setDescripcion("cd_nombre");
		cat.setPais("24");
		cat.setEstado(cmbEdo);
		cat.setOrden("cd_nombre");
		List elementos=cat.getListaElementos();
		
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
	}else if(informacion.equals("GuardarInformacion")){
		String ic_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):""; 
		String estatus_solic_inicial = (request.getParameter("estatus_solic_inicial")!=null)?request.getParameter("estatus_solic_inicial"):""; 
		String txtRazon = (request.getParameter("txtRazon")!=null)?request.getParameter("txtRazon"):""; 
		String txtNomCom = (request.getParameter("txtNomCom")!=null)?request.getParameter("txtNomCom"):""; 
		String txtRFC = (request.getParameter("txtRFC")!=null)?request.getParameter("txtRFC"):""; 
		String txtDomi = (request.getParameter("txtDomi")!=null)?request.getParameter("txtDomi"):""; 
		String txtColonia = (request.getParameter("txtColonia")!=null)?request.getParameter("txtColonia"):""; 
		String cmbEdo = (request.getParameter("cmbEdo")!=null)?request.getParameter("cmbEdo"):""; 
		String cmbDeleg = (request.getParameter("cmbDeleg")!=null)?request.getParameter("cmbDeleg"):""; 
		String txtCP = (request.getParameter("txtCP")!=null)?request.getParameter("txtCP"):""; 
		String txtNomContacto = (request.getParameter("txtNomContacto")!=null)?request.getParameter("txtNomContacto"):"";
		String txtEmailEmp = (request.getParameter("txtEmailEmp")!=null)?request.getParameter("txtEmailEmp"):"";
		String txtTel = (request.getParameter("txtTel")!=null)?request.getParameter("txtTel"):"";
		Usuario datosCargados = sesUsuario;
		String obtRFC = datosCargados.getRfc();
		String obtRazon = datosCargados.getNombreOrganizacionUsuario();
		String ic_aspirante = cediEjb.getClaveAspirante(obtRFC);
		
		SolicitudArgus solicitud = new SolicitudArgus();
		
		UtilUsr usr = new UtilUsr();
		Usuario usuario = usr.getUsuario(strLogin);
		
		solicitud.setUsuario(usuario);
		solicitud.setCompania(obtRazon);
		solicitud.setRFC(obtRFC);
		solicitud.setDireccion(txtDomi);
		solicitud.setColonia(txtColonia);
		solicitud.setCodigoPostal(txtCP);
		solicitud.setMunicipio(cmbDeleg);
		solicitud.setEstado(cmbEdo);
		solicitud.setPais("24");
		solicitud.setTelefono(txtTel);
		usr.actualizarUsuario(solicitud);
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		
		boolean exito = cediEjb.completarInfoAsp(solicitud,txtRazon,txtNomCom,txtNomContacto,txtEmailEmp,ic_aspirante,ic_solicitud,strLogin,estatus_solic_inicial);
		
		String activa_actualiza="";
		String existe_rechazo = cediEjb.existeRechazoMovimientos(ic_solicitud);
	    int existe_rechazo_aux = 0;
	    if(existe_rechazo.equals("")){
	        activa_actualiza = "N";
	    }else{
			 
	        existe_rechazo_aux = Integer.parseInt(existe_rechazo);
	    }
	    if(existe_rechazo_aux>0){
	        activa_actualiza = "S";
	    }
		
		if(exito==true){
			jsonObj.put("exito", "&nbsp;&nbsp;&nbsp;&nbsp;La información se guardó con éxito.");
		}else{
			jsonObj.put("exito", "Error al guardar los datos");
		}
		jsonObj.put("activa_actualiza", activa_actualiza);
		infoRegresar =	jsonObj.toString();
		
	}else if(informacion.equals("GuardarInfoCualitativa")){
	
		List lstRespuestas = new ArrayList();
		String numPregReal = (request.getParameter("numPregReal")!=null)?request.getParameter("numPregReal"):"";
		//String ic_aspirante = (request.getParameter("ic_aspirante")!=null)?request.getParameter("ic_aspirante"):"";
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):""; 
		String estatus_solic_inicial = (request.getParameter("estatus_solic_inicial")!=null)?request.getParameter("estatus_solic_inicial"):""; 
		
		String opcion_resp = "";
		String resp_asoci = "";
		String resp_otro = "";
		if(Integer.parseInt(numPregReal)>0){
			HashMap dataRespuestas = new HashMap();
			int numPregunta = 0;
			int contResp =0;
			String IC_RESP_RADCK = "";
			// Se obtinen las respuestas de las preguntas
			for(int x=1; x<=Integer.parseInt(numPregReal); x++){
				String fprNumPreg = (request.getParameter(x+"_numPreg")!=null)?request.getParameter(x+"_numPreg"):"";
				String fprNumOpc = (request.getParameter(x+"_numOpc")!=null)?request.getParameter(x+"_numOpc"):"0";
				String fprTipoResp = (request.getParameter(x+"_tipoResp")!=null)?request.getParameter(x+"_tipoResp"):"";
				String valor_inicial_radio = (request.getParameter(x+"_resp_inicial_radio")!=null)?request.getParameter(x+"_resp_inicial_radio"):"";
				String radio_chek = (request.getParameter(x+"_opcion_check")!=null)?request.getParameter(x+"_opcion_check"):"";
				if(fprTipoResp.equals("1")){
					IC_RESP_RADCK = (request.getParameter(fprNumPreg+"ic_opcion_resp")!=null)?request.getParameter(fprNumPreg+"ic_opcion_resp"):"";
				}else{
					IC_RESP_RADCK ="";
				}
				int numOpciones = Integer.parseInt(fprNumOpc);
				
					for(int y=0; y<numOpciones;y++){
					contResp ++;
					dataRespuestas = new HashMap();
					
					String RESP_LIBRE = (request.getParameter(x+"cajaTexto"+y)!=null)?request.getParameter(x+"cajaTexto"+y):"";
					String nombre_otro = (request.getParameter(x+"addCajaTexto"+y)!=null)?request.getParameter(x+"addCajaTexto"+y):"";
					String opcion_resp_inicial = (request.getParameter(x+"opcion_resp_inicial"+y)!=null)?request.getParameter(x+"opcion_resp_inicial"+y):"";
					if(fprTipoResp.equals("2")||fprTipoResp.equals("3")){
						
						if(fprTipoResp.equals("3")){
							IC_RESP_RADCK = (request.getParameter(fprNumPreg+"ic_opcion_resp")!=null)?request.getParameter(fprNumPreg+"ic_opcion_resp"):"";
							String radio_chek1 = (request.getParameter(x+"_opcion_check")!=null)?request.getParameter(x+"_opcion_check"):"";
							int opc = y;
							String sele_opc = String.valueOf(y);
							if(!radio_chek1.equals(sele_opc)){
								dataRespuestas.put("resp_inicial",opcion_resp_inicial);
								dataRespuestas.put("IC_RESP","");
							}else{
								dataRespuestas.put("IC_RESP",IC_RESP_RADCK);
								dataRespuestas.put("resp_inicial",opcion_resp_inicial);
							}
						}else if(fprTipoResp.equals("2")){
							IC_RESP_RADCK = (request.getParameter(fprNumPreg+"ic_opcion_resp"+y)!=null)?request.getParameter(fprNumPreg+"ic_opcion_resp"+y):"";
							if(IC_RESP_RADCK.equals("")){
								dataRespuestas.put("resp_inicial",opcion_resp_inicial);
							}else{
								dataRespuestas.put("resp_inicial",valor_inicial_radio);
							}
							dataRespuestas.put("IC_RESP",IC_RESP_RADCK);
						}
						dataRespuestas.put("RESP_LIBRE",RESP_LIBRE);
						dataRespuestas.put("nombre_otro",nombre_otro);
						dataRespuestas.put("num_opcion",fprNumOpc);
						dataRespuestas.put("tipo_resp",fprTipoResp);
						lstRespuestas.add(dataRespuestas);
					}else{
						
						dataRespuestas.put("IC_RESP",IC_RESP_RADCK);
						dataRespuestas.put("resp_inicial",valor_inicial_radio);
						dataRespuestas.put("RESP_LIBRE",RESP_LIBRE);
						dataRespuestas.put("nombre_otro",nombre_otro);
						dataRespuestas.put("num_opcion",fprNumOpc);
						dataRespuestas.put("tipo_resp",fprTipoResp);
						lstRespuestas.add(dataRespuestas);
					}
				}
				
			}
		}
		boolean mensaje1 = cediEjb.actualizarInfoCualitativa(clave_solicitud,lstRespuestas,strLogin,estatus_solic_inicial);
		String activa_actualiza="";
		String existe_rechazo = cediEjb.existeRechazoMovimientos(clave_solicitud);
	    int existe_rechazo_aux = 0;
	    if(existe_rechazo.equals("")){
	        activa_actualiza = "N";
	    }else{
			 
	        existe_rechazo_aux = Integer.parseInt(existe_rechazo);
	    }
	    if(existe_rechazo_aux>0){
	        activa_actualiza = "S";
	    }
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("exito", "&nbsp;&nbsp;&nbsp;&nbsp;La información se guardó con éxito");
		jsonObj.put("activa_actualiza", activa_actualiza);
		infoRegresar =	jsonObj.toString();
		
	}else if(informacion.equals("GuardarCreditoVinculado")){
	
		String ic_aspirante = (request.getParameter("ic_aspirante")!=null)?request.getParameter("ic_aspirante"):"";
		String tieneCredVinc = (request.getParameter("tieneCredVinc")!=null)?request.getParameter("tieneCredVinc"):"N";
		String montoCredVinc = (request.getParameter("montoCredVinc")!=null)?request.getParameter("montoCredVinc"):"";
		
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):""; 
		String estatus_solic_inicial = (request.getParameter("estatus_solic_inicial")!=null)?request.getParameter("estatus_solic_inicial"):""; 
		
		boolean mensaje1 = cediEjb.actualizaInfoCreditoVinculado(clave_solicitud,tieneCredVinc,montoCredVinc,strLogin,estatus_solic_inicial);
		String activa_actualiza="";
		
		String existe_rechazo = cediEjb.existeRechazoMovimientos(clave_solicitud);
	    int existe_rechazo_aux = 0;
	    if(existe_rechazo.equals("")){
	        activa_actualiza = "N";
	    }else{
			 
	        existe_rechazo_aux = Integer.parseInt(existe_rechazo);
	    }
	    if(existe_rechazo_aux>0){
	        activa_actualiza = "S";
	    }


		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("activa_actualiza", activa_actualiza);
		jsonObj.put("exito", "&nbsp;&nbsp;&nbsp;&nbsp;La información se guardó con éxito");
		infoRegresar =	jsonObj.toString();
		
	}else if(informacion.equals("fechaInformacionFinanciera")){
		HashMap datos;
      List reg = new ArrayList();  
		Date fechaActual = new Date();
		
		Calendar fecha = Calendar.getInstance();
		String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		int año_actual = fecha.get(Calendar.YEAR);
		int mes_actual = fecha.get(Calendar.MONTH);
		
		int mes_ini =0;
		int mes_fin =11;
		
		int mes_ant = 0;
		int año_ant = 0;
		
		for(int i = 0; i<=2;i++){
			datos = new HashMap();
			datos.put("descripcion",meses[mes_actual]+" - "+año_actual);
			datos.put("clave",mes_actual+"-"+año_actual);
			reg.add(datos);
			if(mes_actual==0){
				mes_ant = mes_fin;
				año_ant = fecha.get(Calendar.YEAR)-1;
				mes_actual = mes_ant;
				año_actual = año_ant;
			}else if(mes_actual==1){
				mes_ant = mes_ini;
				mes_actual = mes_ant;
			}else{
				año_ant = mes_actual -1;
				mes_actual = año_ant;
			}
			
		}
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("Consulta_Grid_InfoFinanciera")){
		JSONObject jsonObjS = new JSONObject();
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):""; 
		
		List datosReg = cediEjb.getStoreGridInfoFinanciera(clave_solicitud);
		HashMap datos = new HashMap();
		JSONArray reg = new JSONArray(); 
		if(datosReg.size()>0){
			List listColumna1 = (ArrayList)datosReg.get(0);
			List listColumna2 = (ArrayList)datosReg.get(1);
			List listColumna3 = (ArrayList)datosReg.get(2);
			for(int i = 0; i< listColumna1.size(); i++){
				HashMap hmInfo = (HashMap)listColumna1.get(i);
				Iterator it = hmInfo.keySet().iterator();
				HashMap hmInfo1 = (HashMap)listColumna2.get(i);
				Iterator it1 = hmInfo1.keySet().iterator();
				HashMap hmInfo2 = (HashMap)listColumna3.get(i);
				Iterator it2 = hmInfo2.keySet().iterator();
				if(i==2){
					String key = "FG_ACTIVOS_TOTALES";
					datos = new HashMap();
					datos.put("CAMPO","FG_ACTIVOS_TOTALES");
					datos.put("CS_CONCEPTO","Activos Totales");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_ACTIVOS_TOTALES");
					reg.add(datos);
				}else if(i==3){
					String key = "FG_INVERSION_CAJA";
					datos = new HashMap();
					datos.put("CAMPO","FG_INVERSION_CAJA");
					datos.put("CS_CONCEPTO","Disponibles");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_INVERSION_CAJA");
					reg.add(datos);
				}else if(i==4){
					String key = "FG_CARTERA_VIGENTE";
					datos = new HashMap();
					datos.put("CAMPO","FG_CARTERA_VIGENTE");
					datos.put("CS_CONCEPTO","Cartera Vigente");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_CARTERA_VIGENTE");
					reg.add(datos);
				}else if(i==5){
					String key = "FG_CARTERA_VENCIDA";
					datos = new HashMap();
					datos.put("CAMPO","FG_CARTERA_VENCIDA");
					datos.put("CS_CONCEPTO","Cartera Vencida");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_CARTERA_VENCIDA");
					reg.add(datos);
				}else if(i==6){
					String key = "FG_CARTERA_TOTAL";
					datos = new HashMap();
					datos.put("CAMPO","FG_CARTERA_TOTAL");
					datos.put("CS_CONCEPTO","Cartera Total");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_CARTERA_TOTAL");
					reg.add(datos);
				}else if(i==7){
					String key = "FG_ESTIMACION_RESERVAS";
					datos = new HashMap();
					datos.put("CAMPO","FG_ESTIMACION_RESERVAS");
					datos.put("CS_CONCEPTO","Estimación de Reservas");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_ESTIMACION_RESERVAS");
					reg.add(datos);
				}else if(i==8){
					String key = "FG_PASIVOS_BANCARIOS";
					datos = new HashMap();
					datos.put("CAMPO","FG_PASIVOS_BANCARIOS");
					datos.put("CS_CONCEPTO","Pasivos Bancarios");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_PASIVOS_BANCARIOS");
					reg.add(datos);
				}else if(i==9){
					String key = "FG_PASIVOS_TOTALES";
					datos = new HashMap();
					datos.put("CAMPO","FG_PASIVOS_TOTALES");
					datos.put("CS_CONCEPTO","Pasivos Totales");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_PASIVOS_TOTALES");
					reg.add(datos);
				}else if(i==10){
					String key = "FG_CAPITAL_SOCIAL";
					datos = new HashMap();
					datos.put("CAMPO","FG_CAPITAL_SOCIAL");
					datos.put("CS_CONCEPTO","Capital Social");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_CAPITAL_SOCIAL");
					reg.add(datos);
				}else if(i==11){
					String key = "FG_CAPITAL_CONTABLE";
					datos = new HashMap();
					datos.put("CAMPO","FG_CAPITAL_CONTABLE");
					datos.put("CS_CONCEPTO","Capital Contable");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_CAPITAL_CONTABLE");
					reg.add(datos);
				}else if(i==12){
					String key = "FG_UTILIDAD_NETA";
					datos = new HashMap();
					datos.put("CAMPO","FG_UTILIDAD_NETA");
					datos.put("CS_CONCEPTO","Utilidad Neta");
					datos.put("ANIO_1",hmInfo.get(key));
					datos.put("ANIO_2",hmInfo1.get(key));
					datos.put("ANIO_3",hmInfo2.get(key));
					datos.put("ID_CAMPO","FG_UTILIDAD_NETA");
					reg.add(datos);
				}
				
			}
		
		}else{
			String descConcepto="";
			descConcepto= "Activos Totales";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_ACTIVOS_TOTALES");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
			descConcepto= "Disponibles";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_INVERSION_CAJA");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
			descConcepto= "Cartera Vigente";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_CARTERA_VIGENTE");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
			descConcepto= "Cartera Vencida";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_CARTERA_VENCIDA");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
			descConcepto= "Cartera Total";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_CARTERA_TOTAL");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
			descConcepto= "Estimación de Reservas";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_ESTIMACION_RESERVAS");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
			descConcepto= "Pasivos Bancarios";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_PASIVOS_BANCARIOS");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
			descConcepto= "Pasivos Totales";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_PASIVOS_TOTALES");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
			descConcepto= "Capital Social";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_CAPITAL_SOCIAL");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
			descConcepto= "Capital Contable";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_CAPITAL_CONTABLE");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
			descConcepto= "Utilidad Neta";
			datos.put("CS_CONCEPTO",descConcepto);
			datos.put("CAMPO","FG_UTILIDAD_NETA");
			datos.put("ANIO_1","");
			datos.put("ANIO_2","");
			datos.put("ANIO_3","");
			reg.add(datos);
		}
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
		
	}else if(informacion.equals("GuardarInfoFinanciera")){
	
		//String ic_aspirante = (request.getParameter("ic_aspirante")!=null)?request.getParameter("ic_aspirante"):"";
		String anio_selec = (request.getParameter("anio_selec")!=null)?request.getParameter("anio_selec"):"";
		String numeroRegistros  =(request.getParameter("numeroRegistros")!=null) ? request.getParameter("numeroRegistros"):"";
		String fechaInfinanc  =(request.getParameter("fechaInfinanc")!=null) ? request.getParameter("fechaInfinanc"):"";
				
		int i_numReg = Integer.parseInt(numeroRegistros);
		int anio_actual = Integer.parseInt(anio_selec.replace(" ",""));
		int anio_1 = anio_actual -1;
		int anio_2 = anio_actual -2;
		//String clave_solicitud = cediEjb.getClaveSolicitud(ic_aspirante);
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):""; 
		String estatus_solic_inicial = (request.getParameter("estatus_solic_inicial")!=null)?request.getParameter("estatus_solic_inicial"):""; 
		
		
		
		VectorTokenizer vtd = null;
		Vector vecdet = null;
		vtd		= new VectorTokenizer(fechaInfinanc,"-");
		vecdet	= vtd.getValuesVector();
		String mes_IF = vecdet.get(0).toString().trim();
		String anio_IF = vecdet.get(1).toString().trim();
		int aux = Integer.parseInt(mes_IF);
		mes_IF = String.valueOf(aux + 1) ;
		boolean exito = true;
		boolean  actualizaMov = true;
		String activa_actualiza="";
		String existe_rechazo = cediEjb.existeRechazoMovimientos(clave_solicitud);
		int existe_rechazo_aux = 0;
		if(existe_rechazo.equals("")){
			activa_actualiza = "N";
		    existe_rechazo_aux = 0;
		}else{
		    existe_rechazo_aux = Integer.parseInt(existe_rechazo);
		}
			
		if(existe_rechazo_aux>0){
			activa_actualiza = "S";
		    boolean actualiza = cediEjb.actualizaEstatusSolicitud(clave_solicitud,"1");
			boolean actualizaD = cediEjb.actualizaDictamenConfirmacion(clave_solicitud,"N");
		}
		
		
	
			boolean guarda_fecha = cediEjb.InsertarAnioInfFinaciera(clave_solicitud,anio_IF,mes_IF);
			boolean resp_anio_actual = cediEjb.guardaInfoAnio(String.valueOf(anio_actual),clave_solicitud);
			boolean resp_anio_1 = cediEjb.guardaInfoAnio(String.valueOf(anio_1),clave_solicitud);
			boolean resp_anio_2 = cediEjb.guardaInfoAnio(String.valueOf(anio_2),clave_solicitud);
			for(int i = 0; i< i_numReg; i++){ 
				String listAnioInfFinan[] = request.getParameterValues("listAnioInfFinan");
				String listAnio_1[] = request.getParameterValues("listAnio_1");
				String listAnio_2[] = request.getParameterValues("listAnio_2");
				String listCampo[] = request.getParameterValues("listCampo");
				
				boolean guarda_actual = cediEjb.guardaInformacionFinanciera(listCampo[i], listAnioInfFinan[i],String.valueOf(anio_actual), clave_solicitud);
				boolean guarda_anio_1 = cediEjb.guardaInformacionFinanciera(listCampo[i], listAnio_1[i],String.valueOf(anio_1), clave_solicitud);
				boolean guarda_anio_2 = cediEjb.guardaInformacionFinanciera(listCampo[i], listAnio_2[i],String.valueOf(anio_2), clave_solicitud);
				
			}
			if(existe_rechazo_aux>0){
				actualizaMov = cediEjb.setMovimientoSolicitus(clave_solicitud,strLogin, "1", "11","");
			}else{
				actualizaMov = cediEjb.setMovimientoSolicitus(clave_solicitud,strLogin, "1", "2","");
			}
		
			
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		if(exito==true){
			jsonObj.put("exito", "&nbsp;&nbsp;&nbsp;&nbsp;La información se guardó con éxito");
		}else{
			jsonObj.put("exito", "Error al guardar la información");
		}
		jsonObj.put("activa_actualiza", activa_actualiza);
		infoRegresar =	jsonObj.toString();
	}else if(informacion.equals("GuardarSaldo")){
	
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
		String estatus_solic_inicial = (request.getParameter("estatus_solic_inicial")!=null)?request.getParameter("estatus_solic_inicial"):""; 
		
		String montoPf = (request.getParameter("montoPf")!=null)?request.getParameter("montoPf"):"";
		String montoPm = (request.getParameter("montoPm")!=null)?request.getParameter("montoPm"):"";
		String numReAcre  =(request.getParameter("numReAcre")!=null) ? request.getParameter("numReAcre"):"";
		int i_numRegA = Integer.parseInt(numReAcre);
		String numReInc  =(request.getParameter("numReInc")!=null) ? request.getParameter("numReInc"):"";
		int i_numRegI = Integer.parseInt(numReInc);
		boolean exito = true;
		String activa_actualiza="";
		String existe_rechazo = cediEjb.existeRechazoMovimientos(clave_solicitud);
		int existe_rechazo_aux = 0;
		if(existe_rechazo.equals("")){
		    existe_rechazo_aux = 0;
			activa_actualiza = "N";
		}else{
		    existe_rechazo_aux = Integer.parseInt(existe_rechazo);
		}
		boolean actualizaMov = true;
		if(existe_rechazo_aux>0){
			activa_actualiza = "S";
		    boolean actualiza = cediEjb.actualizaEstatusSolicitud(clave_solicitud,"1");
			boolean actualizaD = cediEjb.actualizaDictamenConfirmacion(clave_solicitud,"N");
		}else{
		}
		
			
			for(int i = 0; i< i_numRegA; i++){ 
					String listSaldoAcre[] = request.getParameterValues("listSaldoAcre");
					String listSaldoA[] = request.getParameterValues("listSaldoA");
					boolean mensaje1 = cediEjb.guardarInformacionSaldos(clave_solicitud,"V",String.valueOf(i+1),listSaldoA[i]);
					
			}
			for(int i = 0; i< i_numRegI; i++){ 
					String listSaldoAcreI[] = request.getParameterValues("listSaldoAcreI");
					String listSaldoI[] = request.getParameterValues("listSaldoI");
					boolean mensaje1 = cediEjb.guardarInformacionSaldos(clave_solicitud,"I",String.valueOf(i+1),listSaldoI[i]);
						
			}		
			boolean mensaje1 = cediEjb.guardarSaldoPMF(clave_solicitud,montoPm,montoPf);
			if(existe_rechazo_aux>0){
				actualizaMov = cediEjb.setMovimientoSolicitus(clave_solicitud,strLogin, "1", "11","");
			}else{
				actualizaMov = cediEjb.setMovimientoSolicitus(clave_solicitud,strLogin, "1", "2","");
			}
			
		
		
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("activa_actualiza", activa_actualiza);
		
		if(exito==true){
			jsonObj.put("exito", "&nbsp;&nbsp;&nbsp;&nbsp;La información se guardó con éxito.");
		}else{
			jsonObj.put("exito", "Error al guardar la información");
		}
		infoRegresar =	jsonObj.toString();
	}else if(informacion.equals("Finaliza_Registro")){
	
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
		String estatus_solic_inicial = (request.getParameter("estatus_solic_inicial")!=null)?request.getParameter("estatus_solic_inicial"):""; 
		String ban_actualizar = "N";
		String dictame = "";
		String estatus = "";
		String click_finalizar = "N";
		boolean exito = true;
		boolean ban_Valida = false;
		boolean exito_indicadores = false;
		String tipoUsuario = "4";
		String claveAfiliado ="N";
		String numeroElectronico="0";
		String perfil = "GESTOR CEDI";
		try{
			ConsUsuariosNafinSeg clase = new  ConsUsuariosNafinSeg();
						
			 String existe_rechazo = cediEjb.existeRechazoMovimientos(clave_solicitud);
			int existe_rechazo_aux = 0;
			
			if(existe_rechazo.equals("")){
				existe_rechazo_aux = 0;
				click_finalizar = "S";
			}else{
				
				existe_rechazo_aux = Integer.parseInt(existe_rechazo);
				if(existe_rechazo_aux==0){
					click_finalizar = "S";
				}
			}
			String tipo_cartera = cediEjb.getTipoCartera(clave_solicitud,"4");
			ban_Valida = cediEjb.validaViabilidadCedula(clave_solicitud,tipo_cartera);
			if(ban_Valida==false){
				
				if(existe_rechazo_aux<=0){
					exito_indicadores = cediEjb.guardaIndicadoresFinancieros(clave_solicitud,"4",strLogin,estatus_solic_inicial);
					
				}
			}
			
			if(existe_rechazo_aux>0){
				ban_actualizar = "S";
			}else{
				/*if(existe_rechazo_aux<=0){
					if(ban_Valida==true){
						cediEjb.actualizaDictamenConfirmacion(clave_solicitud,"S");
					}else{
						cediEjb.actualizaDictamenConfirmacion(clave_solicitud,"N");
					}
				}*/
			}
			if(ban_Valida==true){
				
			}
			//dictame = cediEjb.consultaDictamenSolicitud(clave_solicitud);
			if(exito_indicadores==true){
				// Guardara la cédula de la solicitud
				String estatus_F = cediEjb.getEstatusSoli(clave_solicitud);
				List parametros = new ArrayList();
				parametros.add((String)session.getAttribute("strPais"));
				parametros.add("1");
				parametros.add(strNombre);
				parametros.add(strNombreUsuario);
				parametros.add(strLogo);
				parametros.add(strDirectorioTemp);
				parametros.add(strDirectorioPublicacion);	
				parametros.add(clave_solicitud);
				parametros.add(strPerfil);
				parametros.add(strTipoUsuario);
				parametros.add(estatus_F);
				parametros.add("CALIFICATE");
				
				String nombreCedula  = cediEjb.generarArchivoCedula(parametros);
				boolean guarda_arch1 = cediEjb.guardarArchEdoFinan(clave_solicitud, strDirectorioTemp+nombreCedula, "CEDULA_PREVIA");
				
				// Guardara PDF DE LA CEDULA CON DATOS COMPLETOS
				parametros = new ArrayList();
				parametros.add((String)session.getAttribute("strPais"));
				parametros.add("1");
				parametros.add(strNombre);
				parametros.add(strNombreUsuario);
				parametros.add(strLogo);
				parametros.add(strDirectorioTemp);
				parametros.add(strDirectorioPublicacion);	
				parametros.add(clave_solicitud);
				parametros.add(strPerfil);
				parametros.add(strTipoUsuario);
				parametros.add(estatus_F);
				parametros.add("OTRO_ARCHIVOS");
					
				String nombreCedulaC  = cediEjb.generarArchivoCedula(parametros);
				boolean guarda_cedC = cediEjb.guardarArchEdoFinan(clave_solicitud, strDirectorioTemp+nombreCedulaC, "CEDULA_COMPLETA");
				
				boolean resu = false;
				if(ban_Valida== true){
					boolean actualiza = cediEjb.actualizaDictamenConfirmacion(clave_solicitud,"S");
					estatus = estatus_solic_inicial;
				}else if(ban_Valida== false){
					
					resu = cediEjb.actualizaEstatusSolicitud(clave_solicitud, "7");
					estatus = "7";
					boolean actualiza = cediEjb.actualizaDictamenConfirmacion(clave_solicitud,"N");
				}
					
				if(resu==true){
					//para obtener los datos del usuario   
					clase.setTipoUsuario(tipoUsuario);
					clase.setNumeroElectronico(numeroElectronico);
					List registros =  clase.getConsultar(tipoUsuario, "",  perfil);// Me regresa los usuarios  que pertenecen a perfil Gestor Cedi
					cediEjb.enviaCorreoNotiCalificate(registros,clave_solicitud,"7");
				}
			}
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("ban_actualizar", ban_actualizar);
			jsonObj.put("click_finalizar", click_finalizar);
	
			if(exito==true){
				jsonObj.put("exito", "<center>Registro Finalizado</center>");
			}else{
				jsonObj.put("exito", "Error al guardar la información");
			}
			infoRegresar =	jsonObj.toString();
		} catch(Exception e) {
					  throw new AppException("Error ", e);
		}
	}else if(informacion.equals("Consulta_lista_acreditados")){
		String tipo_acreditado = (request.getParameter("tipo_acreditado")!=null)?request.getParameter("tipo_acreditado"):"";
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
	
		HashMap datos = new HashMap();
		JSONArray reg = new JSONArray();  
		
		Registros datosReg = cediEjb.getStoreGridAcreditado(clave_solicitud,tipo_acreditado);
		if(datosReg.getNumeroRegistros()>0){
			consulta	=	"{\"success\": true, \"total\": \""	+	datosReg.getNumeroRegistros() + "\", \"registros\": " + datosReg.getJSONData()+ "}";
		}else{
			datos = new HashMap();
			datos.put("IC_SOLICITUD","FG_UTILIDAD_NETA");
			datos.put("CC_TIPO_ACREDITADO","");
			datos.put("IC_SALDO_ACREDITADO","");
			datos.put("CS_SALDO_ACREDITADO","Acreditado 1");
			datos.put("FG_SALDO","");
			reg.add(datos);
			datos = new HashMap();
			datos.put("IC_SOLICITUD","FG_UTILIDAD_NETA");
			datos.put("CC_TIPO_ACREDITADO","");
			datos.put("IC_SALDO_ACREDITADO","");
			datos.put("CS_SALDO_ACREDITADO","Acreditado 2");
			datos.put("FG_SALDO","");
			reg.add(datos);
			datos = new HashMap();
			datos.put("IC_SOLICITUD","FG_UTILIDAD_NETA");
			datos.put("CC_TIPO_ACREDITADO","");
			datos.put("IC_SALDO_ACREDITADO","");
			datos.put("CS_SALDO_ACREDITADO","Acreditado 3");
			datos.put("FG_SALDO","");
			reg.add(datos);
			datos = new HashMap();
			datos.put("IC_SOLICITUD","FG_UTILIDAD_NETA");
			datos.put("CC_TIPO_ACREDITADO","");
			datos.put("IC_SALDO_ACREDITADO","");
			datos.put("CS_SALDO_ACREDITADO","Acreditado 4");
			datos.put("FG_SALDO","");
			reg.add(datos);	 		
			datos = new HashMap();
			datos.put("IC_SOLICITUD","FG_UTILIDAD_NETA");
			datos.put("CC_TIPO_ACREDITADO","");
			datos.put("IC_SALDO_ACREDITADO","");
			datos.put("CS_SALDO_ACREDITADO","Acreditado 5");
			datos.put("FG_SALDO","");
			reg.add(datos);
			datos = new HashMap();
			datos.put("IC_SOLICITUD","FG_UTILIDAD_NETA");
			datos.put("CC_TIPO_ACREDITADO","");
			datos.put("IC_SALDO_ACREDITADO","");
			datos.put("CS_SALDO_ACREDITADO","Acreditado 6");
			datos.put("FG_SALDO","");
			reg.add(datos);
			datos = new HashMap();
			datos.put("IC_SOLICITUD","FG_UTILIDAD_NETA");
			datos.put("CC_TIPO_ACREDITADO","");
			datos.put("IC_SALDO_ACREDITADO","");
			datos.put("CS_SALDO_ACREDITADO","Acreditado 7");
			datos.put("FG_SALDO","");
			reg.add(datos);
			datos = new HashMap();
			datos.put("IC_SOLICITUD","FG_UTILIDAD_NETA");
			datos.put("CC_TIPO_ACREDITADO","");
			datos.put("IC_SALDO_ACREDITADO","");
			datos.put("CS_SALDO_ACREDITADO","Acreditado 8");
			datos.put("FG_SALDO","");
			reg.add(datos);
			datos = new HashMap();
			datos.put("IC_SOLICITUD","FG_UTILIDAD_NETA");
			datos.put("CC_TIPO_ACREDITADO","");
			datos.put("IC_SALDO_ACREDITADO","");
			datos.put("CS_SALDO_ACREDITADO","Acreditado 9");
			datos.put("FG_SALDO","");
			reg.add(datos);
			datos = new HashMap();
			datos.put("IC_SOLICITUD","FG_UTILIDAD_NETA");
			datos.put("CC_TIPO_ACREDITADO","");
			datos.put("IC_SALDO_ACREDITADO","");
			datos.put("CS_SALDO_ACREDITADO","Acreditado 10");
			datos.put("FG_SALDO","");
			reg.add(datos);
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";

		}
		//consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
	}else if(informacion.equals("Consulta_Indicador_Financiero")){
		HashMap datos = new HashMap();
		JSONArray reg = new JSONArray(); 
			
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
	
		
		List datosReg = cediEjb.getStoreIndicadorFinan(clave_solicitud);	
		List datosFinales = new ArrayList(); 
		HashMap aux = new HashMap();
		HashMap data_fila = new HashMap();
		HashMap hmInfoSelec = new HashMap();
		HashMap hmInfo1 = new HashMap();
		HashMap hmInfo2 = new HashMap();
			
		if(datosReg.size()>0)  {
			hmInfoSelec = (HashMap)datosReg.get(0);
			hmInfo1 = (HashMap)datosReg.get(1);
			hmInfo2 = (HashMap)datosReg.get(2);
		}
		Iterator it = hmInfoSelec.keySet().iterator();
		while(it.hasNext()){
			String key =(String)it.next();
			//INDICADOR
			if(key.equals("COVERTURA_RESERVA")){
				data_fila = new HashMap();
				String coveS = (String)hmInfoSelec.get(key);
				double  coverS = Double.parseDouble(coveS);
				String cove1 = (String)hmInfo1.get(key);
				double  cover1 = Double.parseDouble(cove1);
				String cove2 = (String)hmInfo2.get(key);
				double  cover2 = Double.parseDouble(cove2);
				if(coverS < 100 ){
				  data_fila.put("INDICADOR_S","N" );
				 
				}else{
				  data_fila.put("INDICADOR_S","S" );
				 
				}
				if( cover1 < 100 ){
				  data_fila.put("INDICADOR_1","N" );
				}else{
				  data_fila.put("INDICADOR_1","S" );
				}
				if( cover2 < 100 ){
				 
				  data_fila.put("INDICADOR_2","N" );
				}else{
				 
				  data_fila.put("INDICADOR_2","S" );
				}
				
				data_fila.put("RAZON_FINANCIERA","Cobertura de Reserva" );
				data_fila.put("INDICADOR_CUMPLI", "= >100");
				data_fila.put("ANIO_2",hmInfo2.get(key)+" %"  );
				data_fila.put("ANIO_1",hmInfo1.get(key)+" %"  );
				data_fila.put("ANIO_ELEGIDO",hmInfoSelec.get(key)+" %"  );
				data_fila.put("INDICADOR","S" );
				data_fila.put("ic_solicitud",hmInfoSelec.get("ic_solicitud") );
				datosFinales.add(1,data_fila);
			}else if(key.equals("MOROSIDAD")){
				data_fila = new HashMap();
				String moroS = (String)hmInfoSelec.get(key);
				double  morosiS = Double.parseDouble(moroS);
				String moro1 = (String)hmInfo1.get(key);
				double  morosi1 = Double.parseDouble(moro1);
				String moro2 = (String)hmInfo2.get(key);
				double  morosi2 = Double.parseDouble(moro2);
				
				if(morosiS < 5 && morosiS >= 0  ){
				  
				  data_fila.put("INDICADOR_S","S" );
				 
				}else{
				  data_fila.put("INDICADOR_S","N" );
				 
				}
				if(morosi1< 5  && morosi1 >= 0 ){
				  data_fila.put("INDICADOR_1","S" );
				}else{
				  data_fila.put("INDICADOR_1","N" );
				}
				if(morosi2< 5  && morosi2 >= 0){
				 
				  data_fila.put("INDICADOR_2","S" );
				}else{
				  
				  data_fila.put("INDICADOR_2","N" );
				}
				
				data_fila.put("RAZON_FINANCIERA","Morosidad" );
				data_fila.put("INDICADOR_CUMPLI", "< 5 %");
				data_fila.put("ANIO_2",hmInfo2.get(key)+" %"  );
				data_fila.put("ANIO_1",hmInfo1.get(key) +" %" );
				data_fila.put("ANIO_ELEGIDO",hmInfoSelec.get(key)+" %"  );
				data_fila.put("ic_solicitud",hmInfoSelec.get("ic_solicitud") );
				datosFinales.add(1,data_fila);
			}else if(key.equals("CAPITALIZACION")){
				data_fila = new HashMap();
				String capiS =(String)hmInfoSelec.get(key);
				double  capitaS = Double.parseDouble(capiS);
				String capi1 =(String)hmInfo1.get(key);
				double  capita1 = Double.parseDouble(capi1);
				String capi2 =(String)hmInfo2.get(key);
				double  capita2 = Double.parseDouble(capi2);
				if(capitaS < 12 ){
				  data_fila.put("INDICADOR_S","N" );
				
				}else{
					data_fila.put("INDICADOR_S","S" );
				  
				}
				if(capita1 < 12 ){
				  data_fila.put("INDICADOR_1","N" );
				}else{
				  data_fila.put("INDICADOR_1","S" );
				}
				if(capita2 < 12 ){
				  data_fila.put("INDICADOR_2","N" );
				}else{
				  data_fila.put("INDICADOR_2","S" );
				}
				
				data_fila.put("RAZON_FINANCIERA","Capitalización" );
				data_fila.put("INDICADOR_CUMPLI", "= > 12 %");
				data_fila.put("ANIO_2",hmInfo2.get(key)+" %" );
				data_fila.put("ANIO_1",hmInfo1.get(key)+" %"  );
				data_fila.put("ANIO_ELEGIDO",hmInfoSelec.get(key)+" %"  );
				data_fila.put("ic_solicitud",hmInfoSelec.get("ic_solicitud") );
				datosFinales.add(0,data_fila);
			}
		
			
		}
		JSONArray json = JSONArray.fromObject(datosFinales);
		consulta= "{\"success\": true, \"total\": \"" + json.size() + "\", \"registros\": " + json.toString()+"}";	
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
	}else if(informacion.equals("GuardarIndicadoresFinancieros")){
		String ic_aspirante = (request.getParameter("ic_aspirante")!=null)?request.getParameter("ic_aspirante"):"";
		String clave_solicitud = cediEjb.getClaveSolicitud(ic_aspirante);
		boolean exito = true;
		
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("exito", "Los datos se guardaron con exito");
		infoRegresar =	jsonObj.toString();
		
	}else if(informacion.equals("Consulta_anio_dictaminado")){
		JSONObject jsonObjS = new JSONObject();
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
		
		List datosReg = cediEjb.getStoreIndicadorResultado(clave_solicitud,"4");
		HashMap datos = new HashMap();
		JSONArray reg = new JSONArray();
		if(datosReg.size()>0) {
		 reg = JSONArray.fromObject(datosReg);
		}
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
		
	}else if(informacion.equals("informacion_aspirante_store")){
		JSONObject jsonObjS = new JSONObject();
		Usuario datosCargados = sesUsuario;
		String obtRFC = datosCargados.getRfc();
		String nombreUsuario = datosCargados.getNombreCompleto();
		String ic_aspirante = cediEjb.getClaveAspirante(obtRFC);
		String clave_solicitud = cediEjb.getClaveSolicitud(ic_aspirante);
		String estatus_solic_inicial = (request.getParameter("estatus_solic_inicial")!=null)?request.getParameter("estatus_solic_inicial"):""; 
		
		HashMap data_fila = new HashMap();
		List datosAsp = new ArrayList();
		
		List datosReg = cediEjb.getInformacionAspirante(clave_solicitud,estatus_solic_inicial);
		
		
		for(int i=0; i < datosReg.size(); i++ ){
			HashMap hmInfo = (HashMap)datosReg.get(i);
			Iterator it = hmInfo.keySet().iterator();
			while(it.hasNext()){
				String key =(String)it.next();
				if(i==0){
					data_fila = new HashMap();
					data_fila.put("REQUISITO_INFO","Folio Solicitud");
					data_fila.put("DATOS_INFO",hmInfo.get(key));
					datosAsp.add(data_fila);
				}else if(i==1){
					data_fila = new HashMap();
					data_fila.put("REQUISITO_INFO","Razón Social");
					data_fila.put("DATOS_INFO",hmInfo.get(key));
					datosAsp.add(data_fila);
				}else if(i==2){
					data_fila = new HashMap();
					data_fila.put("REQUISITO_INFO","R.F.C");
					data_fila.put("DATOS_INFO",hmInfo.get(key));
					datosAsp.add(data_fila);
				}else if(i==3){
					data_fila = new HashMap();
					data_fila.put("REQUISITO_INFO","Estado");
					data_fila.put("DATOS_INFO",hmInfo.get(key));
					datosAsp.add(data_fila);
				}else if(i==4){
					data_fila = new HashMap();
					data_fila.put("REQUISITO_INFO","Teléfono");
					data_fila.put("DATOS_INFO",hmInfo.get(key));
					datosAsp.add(data_fila);
				}else if(i==5){
					data_fila = new HashMap();
					data_fila.put("REQUISITO_INFO","Correo Electrónico");
					data_fila.put("DATOS_INFO",hmInfo.get(key));
					datosAsp.add(data_fila);
				}else if(i==6){
					data_fila = new HashMap();
					data_fila.put("REQUISITO_INFO","Destino de los Recursos solicitados");
					data_fila.put("DATOS_INFO",hmInfo.get(key));
					datosAsp.add(data_fila);
				}else if(i==7){
					data_fila = new HashMap();
					data_fila.put("REQUISITO_INFO","Fecha de registro");
					data_fila.put("DATOS_INFO",hmInfo.get(key));
					datosAsp.add(data_fila);
				}else if(i==8){
					data_fila = new HashMap();
					data_fila.put("REQUISITO_INFO","Hora de registro");
					data_fila.put("DATOS_INFO",hmInfo.get(key));
					datosAsp.add(data_fila);
				}else if(i==9){
					data_fila = new HashMap();
					data_fila.put("REQUISITO_INFO","Usuario");
					data_fila.put("DATOS_INFO",hmInfo.get(key)+"-"+nombreUsuario);
					datosAsp.add(data_fila);
				}
			}
			
		}
		
		HashMap datos = new HashMap();
		JSONArray reg = JSONArray.fromObject(datosAsp);
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
	}else if(informacion.equals("informacion_indicadores")){
		JSONObject jsonObjS = new JSONObject();
		Usuario datosCargados = sesUsuario;
		String obtRFC = datosCargados.getRfc();
		String ic_aspirante = cediEjb.getClaveAspirante(obtRFC);
		String clave_solicitud = cediEjb.getClaveSolicitud(ic_aspirante);
		
		
		List datosReg = cediEjb.getInformacionIndicadores(clave_solicitud,"CALIFICATE");
		HashMap datos = new HashMap();
		JSONArray reg = JSONArray.fromObject(datosReg);
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
	
	}else if(informacion.equals("informacion_cualitativa_store")){
		JSONObject jsonObjS = new JSONObject();
		Usuario datosCargados = sesUsuario;
		String obtRFC = datosCargados.getRfc();
		String ic_aspirante = cediEjb.getClaveAspirante(obtRFC);
		String clave_solicitud = cediEjb.getClaveSolicitud(ic_aspirante);
		
		
		List datosReg = cediEjb.getInformacionInfoCualitativa(clave_solicitud);
		HashMap datos = new HashMap();
		JSONArray reg = JSONArray.fromObject(datosReg);
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.size() + "\", \"registros\": " + reg.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar =jsonObj.toString();
	}else if(informacion.equals("Descarga_ArchivoPDF_Cedula")){
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):""; 
		String estatus_solic_inicial = (request.getParameter("estatus_solic_inicial")!=null)?request.getParameter("estatus_solic_inicial"):""; 
		JSONObject resultado = new JSONObject();
		
		
		String nombreCedula  = cediEjb.descargaArchivosEdoFinan(clave_solicitud, strDirectorioTemp, "CEDULA_PREVIA");
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreCedula);
		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();
		
	}else if(informacion.equals("cargaArchivosEstadoFinanciero")){
		String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):""; 
		JSONObject resultado = new JSONObject();
		Usuario datosCargados = sesUsuario;
		String obtRFC = datosCargados.getRfc();
		String ic_aspirante = cediEjb.getClaveAspirante(obtRFC);
		String clave_solicitud = cediEjb.getClaveSolicitud(ic_aspirante);
		int   tamanio = 0;
		
		String mensajeL ="", itemArchivo = "", resultadoT ="N";
		ParametrosRequest req = null;
		boolean ok_file = true;
		int tamanioMB = 5; 
		int tamaniArch_1 =1048576;
		mensajeL = "El tamaño del archivo excede el límite permitido que es de 5MB";
		
		String myContentType = "text/html;charset=UTF-8";
		response.setContentType(myContentType						);
		request.setAttribute("myContentType", myContentType	);
		
		resultado	= new JSONObject();
		boolean		success		= true;
		String nombreArchivo = "";
		String  rutaArchivo ="",   error_tam =""; 
		String PATH_FILE	=	strDirectorioTemp;
		String rutaArchivoTemporal ="";
		Comunes comunes = new Comunes();
		jsonObj.put("success", new Boolean(true));
		if(ServletFileUpload.isMultipartContent(request)) {
			try {
				// Create a factory for disk-based file items
				DiskFileItemFactory factory = new DiskFileItemFactory();
				// Set factory constraints		
				factory.setRepository(new File(strDirectorioTemp));
				// Create a new file upload handler
				ServletFileUpload upload = new ServletFileUpload(factory);
				// Set overall request size constraint
				upload.setSizeMax(tamanioMB * tamaniArch_1);	//MB
				req = new ParametrosRequest(upload.parseRequest(request));
				
				FileItem fileItem = null;
				if(tipoArchivo.equals("Balance")){
					
					fileItem = (FileItem)req.getFiles().get(0);
					itemArchivo		= (String)fileItem.getName();
					tamanio			= (int)fileItem.getSize();
					
				}else if(tipoArchivo.equals("Edo_Resultado")){
					fileItem = (FileItem)req.getFiles().get(1);
					itemArchivo		= (String)fileItem.getName();
					tamanio			= (int)fileItem.getSize();
					
				}else if(tipoArchivo.equals("RFC")){
					fileItem = (FileItem)req.getFiles().get(2);
					itemArchivo		= (String)fileItem.getName();
					tamanio			= (int)fileItem.getSize();
					
				}
				if(tamanio > 5242880) {					    
					mensaje= mensajeL;
					ok_file=false;
				}else{
					rutaArchivo = PATH_FILE+itemArchivo;
					nombreArchivo= Comunes.cadenaAleatoria(16)+ ".pdf";
						
					rutaArchivoTemporal = PATH_FILE + nombreArchivo;
					fileItem.write(new File(rutaArchivoTemporal));
					jsonObj.put("nombreArchivo",nombreArchivo);
				
				}
						
			} catch(Exception e) {		
				mensaje= mensajeL;
				ok_file=false;
				e.printStackTrace();  
				log.debug("Error al actualizar archivo"+e);
		  }
				
		}
		jsonObj.put("tipoArchivo",tipoArchivo);
		jsonObj.put("mensaje", mensaje);
		jsonObj.put("ok_file", ok_file);
		infoRegresar	 =jsonObj.toString();
		
	}else if(informacion.equals("Descarga_Archivos_Solicitud")){
		String tipoArchivo    = request.getParameter("tipo_archivo") == null?"":(String)request.getParameter("tipo_archivo");
		String rutaBalance    = request.getParameter("rutaBalance")    == null?"":(String)request.getParameter("rutaBalance");
		String rutaEdoResultados    = request.getParameter("rutaRFC")    == null?"":(String)request.getParameter("rutaEdoResultados");
		String rutaRFC    = request.getParameter("tipo_archivo")    == null?"":(String)request.getParameter("rutaRFC");
		JSONObject resultado = new JSONObject();
		if(tipoArchivo.equals("Balance")){
			resultado.put("urlArchivo", strDirecVirtualTemp+rutaBalance);
		}else if(tipoArchivo.equals("Edo_Resultado")){
			resultado.put("urlArchivo", strDirecVirtualTemp+rutaEdoResultados);
		}else if(tipoArchivo.equals("RFC")){
			resultado.put("urlArchivo", strDirecVirtualTemp+rutaRFC);
		}
		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();

}else if(informacion.equals("Enviar_estado_Finnancieros")){
		String nombreBalance    = request.getParameter("nombreBalance")    == null?"":(String)request.getParameter("nombreBalance");
		String nombreResultado    = request.getParameter("nombreResultado")    == null?"":(String)request.getParameter("nombreResultado");
		String nombreRegFed    = request.getParameter("nombreRegFed")    == null?"":(String)request.getParameter("nombreRegFed");
		String clave_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
		String estatus = (request.getParameter("estatus_solic_inicial")!=null)?request.getParameter("estatus_solic_inicial"):"";
		JSONObject resultado = new JSONObject();
		HashMap 			archivoAdjunto 		 = new HashMap();
		boolean exito = true;
		boolean resul = false;
		String estatus_nueva = "";
		String tipoUsuario = "4";
		String claveAfiliado ="N";
		String numeroElectronico="0";
		String perfil = "GESTOR CEDI";
		ArrayList listaDeArchivos = new ArrayList();
		//*******
		try{
			boolean exito_indicadores =false;
			ConsUsuariosNafinSeg clase = new  ConsUsuariosNafinSeg();
			clase.setTipoUsuario(tipoUsuario);
			clase.setNumeroElectronico(numeroElectronico);
			List registros =  clase.getConsultar(tipoUsuario, "",  perfil);// Me regresa los usuarios  que pertenecen a perfin Gestor Cedi
			archivoAdjunto 		 = new HashMap();
			archivoAdjunto.put("FILE_FULL_PATH", strDirectorioTemp+nombreBalance );
			listaDeArchivos.add(archivoAdjunto);
			boolean guarda_arch1 = cediEjb.guardarArchEdoFinan(clave_solicitud, strDirectorioTemp+nombreBalance, "Balance");
			archivoAdjunto 		 = new HashMap();
			archivoAdjunto.put("FILE_FULL_PATH", strDirectorioTemp+nombreResultado );
			listaDeArchivos.add(archivoAdjunto);
			boolean guarda_arch2 = cediEjb.guardarArchEdoFinan(clave_solicitud, strDirectorioTemp+nombreResultado, "Edo_Resultado");
				
			archivoAdjunto 		 = new HashMap();
			archivoAdjunto.put("FILE_FULL_PATH", strDirectorioTemp+nombreRegFed );
			listaDeArchivos.add(archivoAdjunto);
			boolean guarda_arch3 = cediEjb.guardarArchEdoFinan(clave_solicitud, strDirectorioTemp+nombreRegFed, "RFC");
			exito_indicadores = cediEjb.guardaIndicadoresFinancieros(clave_solicitud,"4",strLogin,estatus); 
			
			if(exito_indicadores==true){
				
				
				 resul = cediEjb.cambiaEstatusSolic(registros,strLogin,clave_solicitud,listaDeArchivos,estatus);
				 boolean actualiza = cediEjb.actualizaDictamenConfirmacion(clave_solicitud,"S");
				 
				 // Guardara la cédula de la solicitud
				String estatus_F = cediEjb.getEstatusSoli(clave_solicitud);
				List parametros = new ArrayList();
				parametros.add((String)session.getAttribute("strPais"));
				parametros.add("1");
				parametros.add(strNombre);
				parametros.add(strNombreUsuario);
				parametros.add(strLogo);
				parametros.add(strDirectorioTemp);
				parametros.add(strDirectorioPublicacion);	
				parametros.add(clave_solicitud);
				parametros.add(strPerfil);
				parametros.add(strTipoUsuario);
				parametros.add(estatus_F);
				parametros.add("CALIFICATE");
				
				String nombreCedula  = cediEjb.generarArchivoCedula(parametros);
				boolean guarda_ced = cediEjb.guardarArchEdoFinan(clave_solicitud, strDirectorioTemp+nombreCedula, "CEDULA_PREVIA");
				
				// Guardara PDF DE LA CEDULA CON DATOS COMPLETOS
				parametros = new ArrayList();
				parametros.add((String)session.getAttribute("strPais"));
				parametros.add("1");
				parametros.add(strNombre);
				parametros.add(strNombreUsuario);
				parametros.add(strLogo);
				parametros.add(strDirectorioTemp);
				parametros.add(strDirectorioPublicacion);	
				parametros.add(clave_solicitud);
				parametros.add(strPerfil);
				parametros.add(strTipoUsuario);
				parametros.add(estatus_F);
				parametros.add("OTRO_ARCHIVOS");
					
				String nombreCedulaC  = cediEjb.generarArchivoCedula(parametros);
				boolean guarda_cedC = cediEjb.guardarArchEdoFinan(clave_solicitud, strDirectorioTemp+nombreCedulaC, "CEDULA_COMPLETA");
				
				
				// *** TERMINA DE GUARDAR LA CÉDULA
				 cediEjb.enviaCorreoNotiCalificateAceptacion(registros, clave_solicitud, listaDeArchivos, "2");
				 estatus_nueva = "2";
			}
			String existe_rech = "";
			String existe_rechazo = cediEjb.existeRechazoMovimientos(clave_solicitud);
			int existe_rechazo_aux = 0;
			if(existe_rechazo.equals("")){
				existe_rechazo_aux = 0;
			}else{
				existe_rechazo_aux = Integer.parseInt(existe_rechazo);
			}
			if(existe_rechazo_aux>0){
				existe_rech = "S";
			}else{
				existe_rech = "N";
			}
				
			if(resul==true){
				resultado.put("mns", "Archivos enviados exitosamente. En breve un ejecutivo se pondrá en contacto para dar seguimiento a su solicitud.");
			}else{
				resultado.put("mns", "Error al enviar los archivos");
			}
			String credito = cediEjb.existeCredVinculado(clave_solicitud); 
			
			resultado.put("estatus", estatus_nueva);
			resultado.put("credito_vinculado", credito);
			resultado.put("existe_rech", existe_rech);
		} catch(Exception e) {
					  throw new AppException("Error ", e);
		}
		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();

}else if(informacion.equals("actualizar_saldos")){
		String ic_solicitud    = request.getParameter("ic_solicitud") == null?"":(String)request.getParameter("ic_solicitud");
		
		JSONObject resultado = new JSONObject();
		//boolean exito = cediEjb.actualizaSaldos(ic_solicitud);
		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();

}else if(informacion.equals("Aceptar_proceso")){
		String ic_solicitud    = request.getParameter("ic_solicitud") == null?"":(String)request.getParameter("ic_solicitud");
		String estatus_solic_inicial = (request.getParameter("estatus_solic_inicial")!=null)?request.getParameter("estatus_solic_inicial"):""; 
		String existe_rech = "";
		String dictame = "";
		boolean exito = true;
		String estatus = "";
		String tipoUsuario = "4";
		String claveAfiliado ="N";
		String numeroElectronico="0";
		String perfil = "GESTOR CEDI";
		ConsUsuariosNafinSeg clase = new  ConsUsuariosNafinSeg();
		boolean exito_indicadores = false;
		
		try{
		
			exito_indicadores = cediEjb.guardaIndicadoresFinancieros(ic_solicitud,"4",strLogin,estatus_solic_inicial);
			String tipo_cartera = cediEjb.getTipoCartera(ic_solicitud,"4");
			boolean ban_Valida = cediEjb.validaViabilidadCedula(ic_solicitud,tipo_cartera);
			if(ban_Valida==false){
				dictame = "N";
					
			}else{
				
				dictame = "S";
			}
						
		
				String existe_rechazo = cediEjb.existeRechazoMovimientos(ic_solicitud);
				int existe_rechazo_aux = 0;
				if(existe_rechazo.equals("")){
					existe_rechazo_aux = 0;
				}else{
					existe_rechazo_aux = Integer.parseInt(existe_rechazo);
				}
				if(existe_rechazo_aux>0){
					existe_rech = "S";
				}else{
					existe_rech = "N";
				}
				
				if(exito_indicadores==true){
					// Guardara la cédula de la solicitud
					String estatus_F = cediEjb.getEstatusSoli(ic_solicitud);
					List parametros = new ArrayList();
					parametros.add((String)session.getAttribute("strPais"));
					parametros.add("1");
					parametros.add(strNombre);
					parametros.add(strNombreUsuario);
					parametros.add(strLogo);
					parametros.add(strDirectorioTemp);
					parametros.add(strDirectorioPublicacion);	
					parametros.add(ic_solicitud);
					parametros.add(strPerfil);
					parametros.add(strTipoUsuario);
					parametros.add(estatus_F);
					parametros.add("CALIFICATE");
					
					String nombreCedula  = cediEjb.generarArchivoCedula(parametros);
					boolean guarda_arch1 = cediEjb.guardarArchEdoFinan(ic_solicitud, strDirectorioTemp+nombreCedula, "CEDULA_PREVIA");
					
					// Guardara PDF DE LA CEDULA CON DATOS COMPLETOS
					parametros = new ArrayList();
					parametros.add((String)session.getAttribute("strPais"));
					parametros.add("1");
					parametros.add(strNombre);
					parametros.add(strNombreUsuario);
					parametros.add(strLogo);
					parametros.add(strDirectorioTemp);
					parametros.add(strDirectorioPublicacion);	
					parametros.add(ic_solicitud);
					parametros.add(strPerfil);
					parametros.add(strTipoUsuario);
					parametros.add(estatus_F);
					parametros.add("OTRO_ARCHIVOS");
					
					String nombreCedulaC  = cediEjb.generarArchivoCedula(parametros);
					boolean guarda_cedC = cediEjb.guardarArchEdoFinan(ic_solicitud, strDirectorioTemp+nombreCedulaC, "CEDULA_COMPLETA");
					// ************
				
					boolean resu = false;
					if(dictame.equals("S")){
						estatus = "2";
					}else if(dictame.equals("N")){
						resu = cediEjb.actualizaEstatusSolicitud(ic_solicitud, "7");
						boolean actualiza = cediEjb.actualizaDictamenConfirmacion(ic_solicitud,"N");
						estatus = "7";
					}
					
					if(resu==true){
						//para obtener los datos del usuario   
						clase.setTipoUsuario(tipoUsuario);
						clase.setNumeroElectronico(numeroElectronico);
						List registros =  clase.getConsultar(tipoUsuario, "",  perfil);// Me regresa los usuarios  que pertenecen a perfil Gestor Cedi
						cediEjb.enviaCorreoNotiCalificate(registros,ic_solicitud,"7");
					}
				}
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("estatus", estatus);
			jsonObj.put("dictame", dictame);
			jsonObj.put("rechazo", existe_rech);
			infoRegresar =	jsonObj.toString();
				
		} catch(Exception e) {
					  throw new AppException("Error ", e);
		}	
		
		
	

}
		

%>

<%= infoRegresar%>

