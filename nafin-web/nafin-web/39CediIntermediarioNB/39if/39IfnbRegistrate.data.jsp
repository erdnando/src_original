<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*,  
	java.io.*,  
	java.text.*,  
	com.netro.exception.*,
	com.netro.cadenas.*, 
	java.sql.*,  
	netropology.utilerias.*,
	com.netro.seguridad.*,
	javax.naming.*,
	netropology.utilerias.usuarios.*,
	com.netro.model.catalogos.CatalogoSimple,
	org.apache.commons.logging.Log,
	com.netro.cedi.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/39CediIntermediarioNB/39secsession.jspf"%>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<% 
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	
	String infoRegresar="", consulta="", msg ="", mensaje ="";
	JSONObject jsonObj = new JSONObject();
	JSONObject jsonObjRet= new JSONObject();
	
	Cedi cediEjb = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 
	

	if(informacion.equals("ExisteUsuario")){
	
		jsonObjRet= new JSONObject();
			
		String txtRFC = (request.getParameter("txtRFC")!=null)?request.getParameter("txtRFC"):""; 
		boolean bandera = cediEjb.getExisteUsuarioIFCEDI(txtRFC);
		if(bandera== false){
			jsonObjRet.put("mnsExiste", "N");
		}else{
			jsonObjRet.put("mnsExiste", "S");
		}
		jsonObjRet.put("success", new Boolean(true));
		infoRegresar =	jsonObjRet.toString();

	}else if(informacion.equals("AgregarUsuario")){
		
			 jsonObjRet= new JSONObject();
			String perfil =(request.getParameter("perfil") != null) ? request.getParameter("perfil") : "";
			
			String txtRazon =(request.getParameter("txtRazon") != null) ? request.getParameter("txtRazon") : "";
			String txtRFC =(request.getParameter("txtRFC") != null) ? request.getParameter("txtRFC") : "";
			String txtEmail =(request.getParameter("txtEmail") != null) ? request.getParameter("txtEmail") : "";
			String existeRFC =(request.getParameter("existeRFC") != null) ? request.getParameter("existeRFC") : "";

			boolean genera_solicitud=true;
			boolean existe_director=false;
			String mensajeError="";
		try{
			com.netro.seguridadbean.Seguridad objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			if(genera_solicitud){
				SolicitudArgus solicitud = generarSolicitudArgus(request);
				//Las solicitudes son preautorizadas para el usuario Nafin, para los demás usuarios
				//se requiere una autorizacion del administrador ARGUS.
				solicitud.setPreautorizacion(true);
				
				UtilUsr utilUsr = new UtilUsr();
				String ic_usuario_tentativo = utilUsr.registrarSolicitudAltaUsuario(solicitud);
				
				
				boolean exito = cediEjb.guardaDatosIF(txtRazon,txtRFC,strLogin,existeRFC);
				
				if(exito==false){
					mensajeError ="Error al guardar la información";
				}
			}//si genera_solicitud
			
			 jsonObjRet.put("success", new Boolean(true));
			 jsonObjRet.put("msj_error", mensajeError);
			 infoRegresar =	jsonObjRet.toString();
		} catch(Exception e) {
					  throw new AppException("Error al agregar usuario", e);
				}
				
				
			  mensajeError ="";
			 jsonObjRet.put("success", new Boolean(true));
			 jsonObjRet.put("msj_error", mensajeError);
			 infoRegresar =	jsonObjRet.toString();
			 
			 
	}
			

%>
<%!
/**
 * Genera la solicitud de alta de usuario al sistema ARGUS
 * Si los datos estan incompletos, genera un error.
 * Los datos para generar la solicitud los toma de los parámetros del
 * request
 * @param request request de la página
 */
public SolicitudArgus generarSolicitudArgus(HttpServletRequest request) 	throws Exception {

		log.debug("generarSolicitudArgus (E)");

	Usuario usr = new Usuario();
	SolicitudArgus solicitud = new SolicitudArgus();
	
	String tipoAfiliado = "CE";
	
	String txtRazon = request.getParameter("txtRazon");
	String txtRFC = request.getParameter("txtRFC");
	String txtConfRFC = request.getParameter("txtConfRFC");
	String nombre = request.getParameter("nombre");
	String txtApellidoP = request.getParameter("txtApellidoP");
	String txtApellidoM = request.getParameter("txtApellidoM");
	String txtEmail = request.getParameter("txtEmail");
	String txtConmail = request.getParameter("txtConmail");
	String perfil = "IF CEDI";
	String claveAfiliado = perfil;
		
	 
	usr.setNombre(nombre);
	usr.setApellidoPaterno(txtApellidoP);
	usr.setApellidoMaterno(txtApellidoM);
	usr.setEmail(txtEmail);
	usr.setClaveAfiliado(claveAfiliado);//***********************
	usr.setTipoAfiliado(tipoAfiliado);
	usr.setPerfil(perfil);
	usr.setRfc(txtRFC);
	
	solicitud.setUsuario(usr);
	
	solicitud.setCompania(txtRazon);
	solicitud.setRFC(txtRFC);
	solicitud.setDireccion("falta");
	solicitud.setColonia("falta");
	solicitud.setCodigoPostal("falta");
	solicitud.setMunicipio("falta");
	solicitud.setEstado("falta");
	solicitud.setPais("falta");
	solicitud.setTelefono("falta");
	solicitud.setFax("falta");
	
	log.debug("solicitud (S)"+solicitud);
		
	return solicitud;
	
	
}
%>
<%= infoRegresar%>

