<%@ page contentType="application/json;charset=UTF-8" 
import="nl.captcha.Captcha,
net.sf.json.JSONArray,net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%> 
<% 
JSONObject jsonObj = new JSONObject();

String inCaptchaChars = (request.getParameter("inCaptchaChars")!=null)?request.getParameter("inCaptchaChars"):""; 
String  validacion ="N", infoRegresar ="";

Captcha captcha = (Captcha) session.getAttribute(Captcha.NAME);
request.setCharacterEncoding("UTF-8"); // Do this so we can capture non-Latin chars


if (captcha.isCorrect(inCaptchaChars)) { 
   validacion = "S";
 } else { 
      validacion = "N";
 } 

 jsonObj.put("success", new Boolean(true));
 jsonObj.put("validacion", validacion);
 infoRegresar =	jsonObj.toString();


%>


<%= infoRegresar%>