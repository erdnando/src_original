<!DOCTYPE html>
<%@ page import="java.util.*" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/39CediIntermediarioNB/39secsession.jspf" %>
<% 
	String version = (String)session.getAttribute("version");
	
	

%>

<html>
<head>
<title>Nafi@net - Autorizaciones de Solicitudes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 
<%@ include file="/extjs4.jspf" %>
<%if(version!=null){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<script type="text/javascript" src="/nafin/00utils/extjs4/FileUploadField.js"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs4/ux/BigDecimal.js"></script>
<script language="JavaScript" src="/nafin/39CediIntermediarioNB/39if/39InfCualitativa.js"></script>
<script type="text/javascript" src="39IfnbCalificate.js"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(version!=null){%>
	<%@ include file="/01principal/01cedi/cabeza.jspf"%>
<%}%>
<div id="_menuApp"></div>
	<div id="Contcentral">
		<%if(version!=null){%>
			<%@ include file="/01principal/01cedi/menuLateralFlotante.jspf"%>
		<%}%>
		<div id="areaContenido"></div>                                                                                   
   </div>
</div>
<%if(version!=null){%>
<div id="areaLeyenda"></div>
<%@ include file="/01principal/01cedi/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>