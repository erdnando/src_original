Ext.onReady(function(){ 
	
	var txtRazon    = Ext.getDom('txtRazon').value;
	var txtRFC    = Ext.getDom('txtRFC').value;
	var nombre    = Ext.getDom('nombre').value;
	var txtApellidoP    = Ext.getDom('txtApellidoP').value;
	var txtApellidoM    = Ext.getDom('txtApellidoM').value;
	var txtEmail    = Ext.getDom('txtEmail').value;
	
	
														
	var filterPanel = Ext.create('Ext.panel.Panel', {
    width: 950,
	 bodyPadding: '10 10 10 10',
	 align:'center',
    html: '<center><table  width:950 ><tr><td><p style="text-align:justify; background:#F3F4F5; font-size:110%">Si el objeto social del Intermediario Financiero que desea formar parte de la red de Intermediarios de Nacional Financiera, S.N.C. apoya el financiamiento a los '+
				'<strong>Micronegocios, as� como a las Micro, Peque�as y Medianas empresas de los Sectores Industria, Comercio y Servicios,</strong> por favor ingrese a la herramienta ' + 
				'electr�nica que le permitir� realizar un ejercicio de viabilidad para su incorporaci�n como Intermediario Financiero. </p></td></tr></table> </center>'
	});
	//*****************************************FUNCION CALBACK GENERAL INGRESAR*********************************
	var procesarAltaUsuario = function(opts, success, response) {
		var cmpForma = Ext.ComponentQuery.query('#forma')[0];
		 cmpForma.unmask();
		 var json=Ext.JSON.decode(response.responseText);
		 cmpForma.getForm().reset();
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			 if(json.msj_error=="")  {
				window.location ='39IfnbRegistrate.jsp';		
			 } else  {
				Ext.MessageBox.alert('Mensaje',json.msj_error);
			 }
		} else {
			NE.util.mostrarConnError( 
				response,
				opts, 
				function(){	} 	
			); 
		}
		
	}
	var procesarExiste = function(opts, success, response) {
		 var json=Ext.JSON.decode(response.responseText);
		 var cmpForma = Ext.ComponentQuery.query('#forma')[0];
		 cmpForma.unmask();
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			if(json.mnsExiste!='S'){
				Ext.MessageBox.alert('Aviso','<center>Registro completado con �xito.</center><CENTER>En breve recibir� un correo electr�nico con una clave de acceso y las indicaciones para activarla; posteriormente deber� ingresar a la secci�n �Ingresa y Calif�cate� desde el portal de nafin.com, secci�n IFNB o bien, dar clic en la siguiente <a  style="color:blue" href="http://cadenas.nafin.com.mx/nafin/home/seccion.jsp?seccion=1&"><b><u>liga</u></b></a> para continuar con el proceso de viabilidad de incorporaci�n a Nacional Financiera, S.N.C.</CENTER>' ,	function() {
					
						cmpForma.getEl().mask("Procesando", 'x-mask-loading');
						var params = (cmpForma)?cmpForma.getForm().getValues():{};				
						Ext.Ajax.request({
							url: '39IfnbRegistrate.data.jsp',
							params: Ext.apply(params,{
								
							informacion: 'AgregarUsuario',
							tipoAfiliado:'15',
							tipo_afiliado:'IFNB CEDI',
							existeRFC : 'N'
						}),
						callback: procesarAltaUsuario
						});
					
				});//
			 }else{ 
				
				Ext.Msg.confirm('Aviso','Ya cuenta con un usuario,  <br> <br/> �desea generar uno nuevo? ' ,	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							Ext.MessageBox.alert('Aviso','<center>Registro completado con �xito.</center><CENTER>En breve recibir� un correo electr�nico con una clave de acceso y las indicaciones para activarla; posteriormente deber� ingresar a la secci�n �Ingresa y Calif�cate� desde el portal de nafin.com, secci�n IFNB o bien, dar clic en la siguiente <a  style="color:blue" href="http://cadenas.nafin.com.mx/nafin/home/seccion.jsp?seccion=1&"><b><u>liga</u></b></a> para continuar con el proceso de viabilidad de incorporaci�n a Nacional Financiera, S.N.C.</CENTER>',	function() {								
									cmpForma.getEl().mask("Procesando", 'x-mask-loading');
									var params = (cmpForma)?cmpForma.getForm().getValues():{};				
									Ext.Ajax.request({
										url: '39IfnbRegistrate.data.jsp',
										params: Ext.apply(params,{
											
										informacion: 'AgregarUsuario',
										tipoAfiliado:'15',
										tipo_afiliado:'IFNB CEDI',
										existeRFC : 'S'
									}),
									callback: procesarAltaUsuario
									});
								
						});//
						}else{
							Ext.Msg.confirm('Avio', 'De clic en la siguiente  <a  style="color:blue" href="http://cadenas.nafin.com.mx/nafin/home/seccion.jsp?seccion=1&"><b><u>liga</u></b></a>  si desea continuar con el proceso de viabilidad de incorporaci�n a Nacional Financiera');
							
						}
					});//
			 }
			
			 
		} else {
			NE.util.mostrarConnError( 
				response,
				opts, 
				function(){	} 	
			);
		}
		
	}
	
	function procesarValidaCaptcha() {
			
		var forma  	= Ext.getDom('formAux'); 		
		var inCaptchaChars = Ext.ComponentQuery.query('#inCaptchaChars')[0];
				
		if(Ext.isEmpty(inCaptchaChars.getValue()) || inCaptchaChars.getValue() ==''){
			inCaptchaChars.markInvalid('Debe capturar el c�digo captcha');					
			return;					
		}else  {
			Ext.Ajax.request( {				
			   url: 'captchaSubmit.jsp',
				params: {
					inCaptchaChars: inCaptchaChars.getValue()					
				},
				method: 'POST',
				success: function (response) {
					var  jsonObj = Ext.JSON.decode(response.responseText);				
					if(jsonObj.validacion=='S') {
						Ext.ComponentQuery.query('#mnsValid')[0].update("<FONT COLOR='blue'>Validaci�n correcta </FONT>");	
						Ext.ComponentQuery.query('#btnEnviar')[0].enable();
						Ext.ComponentQuery.query('#validaCaptcha')[0].setValue('S');
						
					}else if(jsonObj.validacion=='N') {
						Ext.ComponentQuery.query('#mnsValid')[0].update("<FONT COLOR='red'>Validaci�n incorrecta </FONT>");	
						Ext.ComponentQuery.query('#btnEnviar')[0].disable();
						Ext.ComponentQuery.query('#validaCaptcha')[0].setValue('N');
					}				
				},
				failure: function (response) {				
					Ext.ComponentQuery.query('#mnsValid')[0].update("<FONT COLOR='red'>Validaci�n incorrecta </FONT>");	
					Ext.ComponentQuery.query('#btnEnviar')[0].disable();
					Ext.ComponentQuery.query('#validaCaptcha')[0].setValue('N');
				}
			});
		}		
	} 
	
	
	function procesarActuImag() {	
	
		Ext.ComponentQuery.query('#inCaptchaChars')[0].setValue('');
	
		Ext.ComponentQuery.query('#txtRazon')[0].setValue(txtRazon);
		Ext.ComponentQuery.query('#txtRFC')[0].setValue(txtRFC);
		Ext.ComponentQuery.query('#txtConfRFC')[0].setValue(txtRFC);
		Ext.ComponentQuery.query('#nombre')[0].setValue(nombre);
		Ext.ComponentQuery.query('#txtApellidoP')[0].setValue(txtApellidoP);
		Ext.ComponentQuery.query('#txtApellidoM')[0].setValue(txtApellidoM);
		Ext.ComponentQuery.query('#txtEmail')[0].setValue(txtEmail);
		Ext.ComponentQuery.query('#txtConmail')[0].setValue(txtEmail);	
				
		Ext.Ajax.request( {			
		  url:'/nafin/simpleCaptcha.png',			
			method: 'GET',
			success: function (response) {			
			Ext.ComponentQuery.query('#imagenCaptcha')[0].update('<img src=/nafin/simpleCaptcha.png   border="1" alt="" width="180" height="50" >');			
			Ext.ComponentQuery.query('#mnsValid')[0].update("&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;");	
			},
			failure: function (response) {
				 
			}
		});
		
	}
	
	   
	function mostrarAyuda(){	
		var btnAyuda =   Ext.ComponentQuery.query('#btnAyuda')[0];
		btnAyuda.setTooltip('Escribe las letras que ves en la imagen.Si no est�s seguro de las letras que se muestran en la imagen, puedes hacer clic en el bot�n de actualizar.');					
				
	}
	
	var elementosForma=[
		{
			xtype:		'textfield',
			name:			'txtRazon',
			itemId:		'txtRazon',
			allowBlank:	false,
			msgTarget:		'side',
			width: 300,
			maxLength:100,
			labelAlign:'top',
			fieldLabel: 'Raz�n Social'
			
		},
		{
			xtype:		'textfield',
			name:			'txtRFC',
			itemId:		'txtRFC',
			allowBlank:	false,
			msgTarget:		'side',
			width: 300,
			maxLength:20,
			msgTarget:     'side',
			margins: '0 20 0 0',
			regex			:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
			regexText	:'Por favor escriba correctamente el RFC en may�sculas  separado por '+
			'guiones en el formato NNN-AAMMDD-XXX donde:<br>'+
			'NNN:son las iniciales del nombre de la empresa<br>'+
			'AAMMDD: es la fecha de creaci�n de la empresa menor al d�a de hoy.<br>'+
			'XXX:es la homoclave',
			labelAlign:'top',
			fieldLabel: 'R.F.C empresarial'			
		},
		{
			xtype:		'textfield',
			name:			'txtConfRFC',
			itemId:		'txtConfRFC',
			allowBlank:	false,
			width: 300,
			maxLength:20,
			msgTarget:     'side',
			margins: '0 20 0 0',
			labelAlign:'top',
			regex			:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
			regexText	:'Por favor escriba correctamente el RFC en may�sculas  separado por '+
			'guiones en el formato NNN-AAMMDD-XXX donde:<br>'+
			'NNN:son las iniciales del nombre de la empresa<br>'+
			'AAMMDD: es la fecha de creaci�n de la empresa menor al d�a de hoy.<br>'+
			'XXX:es la homoclave',
			fieldLabel: 'Confirmar R.F.C empresarial',
			enableKeyEvents:true,
			listeners: {
				keydown: function(field, e){
               if((e.getKey() ==86||e.getKey() ==67) && e.ctrlKey){
                 e.stopEvent();
                }
              }
         }
		},
		{
			xtype:		'textfield',
			name:			'nombre',
			itemId:		'nombre',
			allowBlank:	false,
			width: 300,
			maxLength:100,
			msgTarget:     'side',
			margins: '0 20 0 0',
			labelAlign:'top',
			fieldLabel: 'Nombre(s)'
			
		},
		{
			xtype:		'textfield',
			name:			'txtApellidoP',
			itemId:		'txtApellidoP',
			allowBlank:	false,
			width: 300,
			msgTarget:     'side',
			margins: '0 20 0 0',
			labelAlign:'top',
			maxLength:100,
			fieldLabel: 'Apellido Paterno'
			
		},
		{
			xtype:		'textfield',
			name:			'txtApellidoM',
			itemId:		'txtApellidoM',
			allowBlank:	false,
			width: 300,
			msgTarget:     'side',
			margins: '0 20 0 0',
			labelAlign:'top',
			maxLength:100,
			fieldLabel: 'Apellido Materno'
			
		},
		{
			xtype:		'textfield',
			name:			'txtEmail',
			itemId:		'txtEmail',
			allowBlank:	false,
			width: 300,
			labelAlign:'top',
			maxLength:100,
			msgTarget:     'side',
			margins: '0 20 0 0',
			vtype			: 'email',
			fieldLabel: 'E-mail empresarial'
			
		},
		{
			xtype:		'textfield',
			name:			'txtConmail',
			itemId:		'txtConmail',
			allowBlank:	false,
			width: 300,
			maxLength:100,
			msgTarget:     'side',
			margins: '0 20 0 0',
			labelAlign:'top',
			vtype			: 'email',
			fieldLabel: 'Confirmar E-mail empresarial',
		   enableKeyEvents:true,
			listeners: {
				keydown: function(field, e){
               if((e.getKey() ==86||e.getKey() ==67) && e.ctrlKey){
                 e.stopEvent();
                }
              }
         }
		},
		{
			xtype:'panel',
			frame: false,
			border: false,	
			layout:'form',
			width:  250,
			layoutConfig:{ columns: 3 },
			bodyPadding: '0 10 0 10',
			items: [
				{
					xtype: 'container',			
					layout: 'hbox', 
					width: 250,					
					items: [
						{
							xtype:  'label',
							itemId:'imagenCaptcha',						
							html: '',
							width:	'180',
							height: '50'																					
						},								
						{
							xtype:'button',
							name:'btnValidar',
							tooltip:'Validar',
							border: true,	
							itemId:'btnValidar',
							text: 'Validar',
							handler:procesarValidaCaptcha
						}	
					]
				},					
				{
					xtype:'panel',
					frame: false,
					border: false,	
					layout:'table',
					layoutConfig:{ columns: 3 },
					width:  250,					
					bodyPadding: '0 0 0 0',
					items: [
						{
							xtype: 'textfield',
							itemId:'inCaptchaChars',
							name: 'inCaptchaChars',
							width: 180
						},						
						{
							xtype: 'hidden',
							itemId:'validaCaptcha',
							name: 'validaCaptcha',
							value:'N'
						},						
						{
							xtype:'button',
							tooltip: 'Actualizar',
							name:'btnActualizar',
							border: false,	
							itemId:'btnActualizar',
							iconCls: 'icoActualizar',
							width:25,
							heigth: 40,
							align:'center',							
							handler: function() {
								
							  var txtRazon = Ext.ComponentQuery.query('#txtRazon')[0].getValue();
								var txtRFC = Ext.ComponentQuery.query('#txtRFC')[0].getValue();
								var nombre = Ext.ComponentQuery.query('#nombre')[0].getValue();
								var txtApellidoP = Ext.ComponentQuery.query('#txtApellidoP')[0].getValue();
								var txtApellidoM = Ext.ComponentQuery.query('#txtApellidoM')[0].getValue();
								var txtEmail = Ext.ComponentQuery.query('#txtEmail')[0].getValue(); 
								
								var parametros = "txtRazon="+txtRazon+"&txtRFC="+txtRFC+"&nombre="+nombre+
														"&txtApellidoP="+txtApellidoP+"&txtApellidoM="+txtApellidoM+
														"&txtEmail="+txtEmail;										
								document.location.href = "39IfnbRegistrate.jsp?"+parametros;	
								
							}
						}
						,{
							xtype:'button',
							name:'btnAyuda',
							border: true,	
							itemId:'btnAyuda',
							iconCls: 'icoAyuda',
							tooltip: 'Escribe las letras que ves en la imagen.Si no est�s seguro de las letras que se muestran en la imagen, puedes hacer clic en el bot�n de actualizar.'	,				
							handler:mostrarAyuda 
						}
					]
				},
				{
					xtype:'panel',
					frame: false,
					border: false,	
					layout:'table',
					layoutConfig:{ columns: 3 },
					width:  250,					
					bodyPadding: '0 0 0 0',
					items: [
						{
							itemId:'mnsValid',
							xtype:'label',
							style: {  'font-size' : '12px'  },
							width:  180							
						}			
					]
				}
			]
		}
		
	];
	
	var fp = Ext.create('Ext.form.Panel',	{
		layout: 'form',
		itemId: 'forma',
		frame: true,
		border: true,
		bodyPadding: '15 20 12 20',
		title: '<left>REGISTRO</left>',
		width: 400,
		style: 'margin: 0px auto 0px auto;',
		hidden: false,
		align:'right',
		toFront: false, 
		fieldDefaults: {
			msgTarget: 'side'
		},
		monitorValid: true,
		items: elementosForma,
		buttons: [				
			{
				text: 'Enviar',
				itemId: 'btnEnviar',
				formBind: true,				
				handler: function() {
					var cmpForma = Ext.ComponentQuery.query('#forma')[0];
					var rfc = Ext.ComponentQuery.query('#txtRFC')[0];
					var confRfc = Ext.ComponentQuery.query('#txtConfRFC')[0];
					var inCaptchaChars = Ext.ComponentQuery.query('#inCaptchaChars')[0];
					var validaCaptcha = Ext.ComponentQuery.query('#validaCaptcha')[0];
										
					 var txtRazon = Ext.ComponentQuery.query('#txtRazon')[0];
					 var txtRFC = Ext.ComponentQuery.query('#txtRFC')[0];
					 var nombre = Ext.ComponentQuery.query('#nombre')[0];
					 var txtApellidoP = Ext.ComponentQuery.query('#txtApellidoP')[0];
					 var txtApellidoM = Ext.ComponentQuery.query('#txtApellidoM')[0];
					 var txtEmail = Ext.ComponentQuery.query('#txtEmail')[0]; 					
					var txtConmail = Ext.ComponentQuery.query('#txtConmail')[0];
								
					var params = (cmpForma)?cmpForma.getForm().getValues():{};	
					
				  if(Ext.isEmpty(txtRazon.getValue()) || txtRazon.getValue() ==''){
						txtRazon.markInvalid('El campo es Obligatorio');							
						return;
					}
					if(Ext.isEmpty(txtRFC.getValue()) || txtRFC.getValue() ==''){
						txtRFC.markInvalid('El campo es Obligatorio');							
						return;
					}
					if(Ext.isEmpty(confRfc.getValue()) || confRfc.getValue() ==''){
						confRfc.markInvalid('El campo es Obligatorio');							
						return;
					}
					
					if(rfc.getValue()!=confRfc.getValue()){
						confRfc.markInvalid(" La confirmaci�n del RFC no <BR> conincide con el RFC");
						return;
					}	
					
					if(Ext.isEmpty(nombre.getValue()) || nombre.getValue() ==''){
						nombre.markInvalid('El campo es Obligatorio');							
						return;
					}
					if(Ext.isEmpty(txtApellidoP.getValue()) || txtApellidoP.getValue() ==''){
						txtApellidoP.markInvalid('El campo es Obligatorio');							
						return;
					}
					if(Ext.isEmpty(txtApellidoM.getValue()) || txtApellidoM.getValue() ==''){
						txtApellidoM.markInvalid('El campo es Obligatorio');							
						return;
					}
					
					if ( txtConmail.getValue() !=txtEmail.getValue() ){
						txtConmail.markInvalid('La confirmaci�n del E-mail <BR>con coincide con el E-mail');
						valida++;
						return;
					}
					if ( txtConmail.getValue() !=txtEmail.getValue() ){
						txtConmail.markInvalid('La confirmaci�n del E-mail <BR>con coincide con el E-mail');
						valida++;
						return;
					}
					
					 if(Ext.isEmpty(inCaptchaChars.getValue()) || inCaptchaChars.getValue() ==''){
						inCaptchaChars.markInvalid('Debe capturar el c�digo captcha');							
						return;
					}
					
					if(validaCaptcha.getValue() =='N'){
						inCaptchaChars.markInvalid('Debe validar el c�digo captcha');							
						return;
					}
				
					cmpForma.getEl().mask("Procesando", 'x-mask-loading');
					Ext.Ajax.request({
						url: '39IfnbRegistrate.data.jsp',
						params: Ext.apply(params,{
							informacion: 'ExisteUsuario'
						}),
						callback: procesarExiste
					});
				
				}
			},
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		]
	});
	var mensaje1 = Ext.create('Ext.container.Container', {
		renderTo: 'mensaje1',
		width: 50,
		height: 'auto',
		defaults:{
			style: {margin: '30 auto'}
			
		},
		items: [	
			filterPanel
			
		]
	});

	var main = Ext.create('Ext.container.Container', {
		itemId: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 950,
		height: 700,
		defaults:{
			style: {margin: '0 150'}
		},
		items: [				
			NE.util.getEspaciador(10),
			fp
			
		]
	});
	Ext.ComponentQuery.query('#forma')[0].setPosition( 250, 20 ) ;
	
	procesarActuImag();
	
	
});